from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from .models import Page, Volume, Entity, Person, Place, Organisation, Term

@registry.register_document
class PageDocument(Document):
    volume = fields.ObjectField(properties={
        'name': fields.TextField(),
        'start_date': fields.DateField()
    })

    volume_metadata = fields.ObjectField()
    metadata = fields.ObjectField()
    iiif_url = fields.TextField()
    language_confidence = fields.FloatField()

    language = fields.KeywordField()
    section_type = fields.KeywordField()
    feature = fields.KeywordField()

    def prepare_language(self, instance):
        return instance.language

    def prepare_language_confidence(self, instance):
        return instance.language_confidence

    def prepare_section_type(self, instance):
        return instance.section_type

    def prepare_feature(self, instance):
        return instance.feature

    def prepare_metadata(self, instance):
        return instance.metadata

    def prepare_volume_metadata(self, instance):
        return instance.volume.metadata

    def prepare_iiif_url(self, instance):
        return instance.iiif_url()

    class Index:
        # Name of the Elasticsearch index
        name = 'pages'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0,
                    'max_result_window': 20000}

    class Django:
        model = Page # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
            'text',
            'page_identifier'

        ]
        related_models = [Volume]


    def get_queryset(self):
        """Not mandatory but to improve performance we can select related in one sql request"""
        return super(PageDocument, self).get_queryset().select_related(
            'volume'
        )

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Car instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Volume):
            return related_instance.page_set.all()

        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        # ignore_signals = True

        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False

        # Paginate the django queryset used to populate the index with the specified size
        # (by default it uses the database driver's default setting)
    queryset_pagination = 35

@registry.register_document
class EntityDocument(Document):

    entity_type = fields.TextField()

    pages = fields.NestedField(properties={
        'pk': fields.IntegerField()
    })

    def get_instances_from_related(self, related_instance):
        """If related_models is set, define how to retrieve the Car instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Page):
            return related_instance.entities.all()
    
    def prepare_entity_type(self, instance):
        return instance.__class__.__name__    

    class Index:
        # Name of the Elasticsearch index
        name = 'entities'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0,
                    'max_result_window': 20000}

    class Django:
        model = Entity # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'id',
            'text'

        ]
        related_models = [Page]

    queryset_pagination = 35
