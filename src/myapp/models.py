from django.contrib.postgres.fields import ArrayField, JSONField
from polymorphic.models import PolymorphicModel
from django.db import models
from django.db.models import Count
from django.contrib.postgres.indexes import GinIndex
from django.contrib.auth.models import User
import django.contrib.postgres.search as pg_search
import spacy
from spacy.tokens import Doc
from spacy.vocab import Vocab

def get_default_data():
    return { }
    
class Volume(models.Model):
    name = models.CharField(max_length = 512)
    text = models.TextField()
    metadata = JSONField()
    key = models.CharField(max_length = 512, null=True)
    ld_identifier = models.CharField(max_length = 512)
    metadata = JSONField(default=get_default_data)
    link = models.CharField(max_length = 512)
    start_date = models.DateField(null=True, db_index=True)
    end_date = models.DateField(null=True, db_index=True)
    attendees = models.ManyToManyField('Attendee', through='AttendanceEvent')

    def start_year():
        return start_date.strftime('%Y')
    def end_year():
        return end_date.strftime('%Y')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('show_volume', args=[self.pk])
    class Meta:
        ordering = ['start_date']
    
            

class Page(models.Model):
    name = models.CharField(max_length = 512)
    page_identifier = models.CharField(max_length = 512)
    ld_identifier = models.CharField(max_length = 512)
    page_index = models.IntegerField(blank=True)
    metadata = JSONField(null=True)
    section_type = models.CharField(max_length = 512, null = True, db_index = True)
    language = models.CharField(max_length = 5, null = True, db_index = True)
    language_confidence =models.DecimalField(max_digits=4, decimal_places=2, null=True)
    feature = models.CharField(max_length = 512, null = True, db_index = True)
    spacy_doc = models.BinaryField(null=True)
    spacy_vocab = models.BinaryField(null=True)
    sentences = JSONField(null=True)
    text = models.TextField()
    lemmatized_text = models.TextField()
    stemmed_text = models.TextField()
    tokens = ArrayField(models.CharField(max_length=512), null=True)
    bow_text = models.TextField()
    sv = pg_search.SearchVectorField(null=True)
    class Meta:
        indexes = [GinIndex(fields=['sv'])]
        ordering = ['pk', 'page_identifier']
    metadata = JSONField(default=get_default_data)
    volume = models.ForeignKey(Volume, on_delete=models.PROTECT)
    entities = models.ManyToManyField('Entity', through='Mention')

    def doc(self):
        nlp = spacy.load("./trained/")
        doc = nlp(self.text)
        return doc

    def __str__(self):
        composed_name = self.volume.name
        composed_name = composed_name + '\n'
        composed_name = composed_name +'page '+ self.page_identifier
        return composed_name
    
    def iiif_url(self):
        return f"https://digilib.mpiwg-berlin.mpg.de/digitallibrary/digilib.html?fn=permanent/measuring_the_earth/{self.volume.name}/{self.page_identifier}.jpg"

class Entity(PolymorphicModel):
    name = models.CharField(max_length = 512)
    text = models.TextField()
    metadata = JSONField(default=get_default_data)
    language = models.CharField(max_length = 5, null = True, db_index = True)
    sv = pg_search.SearchVectorField(null=True)
    pages = models.ManyToManyField(Page, through='Mention')
    class Meta:
        indexes = [GinIndex(fields=['sv'])]
        ordering = ['pk', 'text']
    def __str__(self):
        return self.text
    def term_type(self):
        return self.__class__.__name__
    def merge_with(self, source_entity):
        for mention in self.mention_set.all():
            mention.entity = source_entity
            mention.save()

class Mention(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    entity = models.ForeignKey(Entity, on_delete=models.CASCADE)
    offset_start = models.IntegerField(null=True)
    offset_end = models.IntegerField(null=True)

class Term(Entity):
    ld_identifier = models.CharField(max_length = 512)

class Organisation(Entity):
    ld_identifier = models.CharField(max_length = 512)

class Person(Entity):
    title = models.CharField(max_length = 256, blank=True)
    first_name = models.CharField(max_length = 512)
    last_name = models.CharField(max_length = 512)
    person_identifier = models.CharField(max_length = 512)
    ld_identifier = models.CharField(max_length = 512)

class Place(Entity):
    ld_identifier = models.CharField(max_length = 512)
    # lat = models.DecimalField(max_digits=9, decimal_places=6, blank=True)
    # long = models.DecimalField(max_digits=9, decimal_places=6, blank=True)
 


# class Sentence(models.Model):
#     name = models.CharField(max_length = 512)
#     sentence_identifier = models.CharField(max_length = 512)
#     ld_identifier = models.CharField(max_length = 512)
#     sentence_index = models.IntegerField(blank=True)
#     metadata = JSONField()
#     text = models.TextField()
#     sv = pg_search.SearchVectorField(null=True) 
#     class Meta:
#         indexes = [GinIndex(fields=['sv'])]
#         ordering = ['pk', 'sentence_identifier']
#     metadata = JSONField(default=get_default_data)
#     page = models.ForeignKey(Volume, on_delete=models.PROTECT)
# 
#     def __str__(self):
#         composed_name = self.volume.name
#         composed_name = composed_name + ' > '
#         composed_name = composed_name + self.page_identifier
#         return composed_name

class Batch(models.Model):
    
    name = models.CharField(max_length = 512)
    ld_identifier = models.CharField(max_length = 512)
    pages = models.ManyToManyField(Page)
    creator = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.name

    def text(self):
        all_text = ""
        for page in self.pages.all():
            all_text += page.text
        return all_text
    def doc(self):
        nlp = spacy.load("./trained/")
        doc = nlp(self.text())
        return doc
    def entities(self):
        entities_array = []
        for page in self.pages.all():
            doc = page.doc()
            for ent in doc.ents:
                entities_array.append(ent)
        return entities_array

class Attendee(models.Model):
    surname = models.CharField(max_length = 512)
    alt_surname = models.CharField(max_length = 512)
    name = models.CharField(max_length = 512)
    function = models.CharField(max_length = 512)
    bio = models.TextField()
    # todo: make relationship to place model instead?
    country = models.CharField(max_length = 512)
    active_since  = models.DateField(null=True, db_index=True)
    active_until = models.DateField(null=True, db_index=True)
    # tolloid wikidata doesn't work for some reason docker?
    wikidata = models.CharField(null=True, max_length = 512)
    volumes = models.ManyToManyField('Volume', through='AttendanceEvent')

    # tolloid attendance = models.ManyToManyField(AttendanceEvent)
    
    def __str__(self):
        return self.name
    class Meta:
        ordering = ['pk', 'surname']

class AttendanceEvent(models.Model):
    attendee = models.ForeignKey(Attendee, on_delete=models.CASCADE)
    volume = models.ForeignKey(Volume, on_delete=models.CASCADE)
    event_year =  models.DateField(null=True, db_index=True)

