from .models import Volume, Batch

def volumes_processor(request):
    first_volume = None
    if Volume:
        first_volume = Volume.objects.all().first()           
    return {'first_volume': first_volume}

def batches_processor(request):
    current_batch = None
    if('current_batch_id' in request.session and request.session['current_batch_id'] != 0):
        current_batch_id = request.session['current_batch_id']
        current_batch = Batch.objects.get(pk=current_batch_id)
    toggles = None
    if('toggles' in request.session):
        toggles = request.session['toggles']

    else:
        current_batch = Batch.objects.filter(creator_id=request.user.id)
    return {'current_batch': current_batch, 'toggles': toggles}