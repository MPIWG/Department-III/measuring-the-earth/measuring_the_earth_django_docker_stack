from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.core.paginator import Paginator
from .tasks import ingest_sources
from django.contrib import messages
from .models import Volume, Page, Batch, Person, Organisation, Place, Term, Entity
from django.db.models import Count
from .documents import PageDocument
from .forms import SearchForm
from .forms import BatchForm
from datetime import datetime
from elasticsearch_dsl.query import SimpleQueryString
from elasticsearch_dsl import FacetedSearch, TermsFacet, DateHistogramFacet
import json
from django.views.generic import DeleteView
from django.urls import reverse_lazy
from django.db.models import Q
import pprint
import csv
from django.http import JsonResponse, HttpResponse
from iso639 import languages

pp = pprint.PrettyPrinter(indent=4)

from django.utils.functional import LazyObject

class ShowHome(TemplateView):
    template_name = 'home.html'
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['volumes'] = Volume.objects.all()
        context['nbar'] = 'home'
        return context

# https://djangotricks.blogspot.com/2018/06/data-filtering-in-a-django-website-using-elasticsearch.html
class SearchResults(LazyObject):
    def __init__(self, search_object):
        self._wrapped = search_object

    def __len__(self):
        return self._wrapped.count()

    def __getitem__(self, index):
        search_results = self._wrapped[index]
        if isinstance(index, slice):
            search_results = list(search_results)
        return search_results

class DeleteBatch(DeleteView):
    model = Batch
    success_message = 'Batch Deleted'
    success_url = reverse_lazy('show_batches')
    def delete(self, request, *args, **kwargs):
        if 'current_batch_id' in request.session:
            if(request.session['current_batch_id'] == self.get_object().id):
                del request.session['current_batch_id']
        messages.success(self.request, self.success_message)
        return super(DeleteBatch, self).delete(request, *args, **kwargs)

class ShowAnnotate(TemplateView):
    language = 'german'
    template_name = 'annotate.html'
    def get(self, *args, **kwargs):
        self.language = self.kwargs['language']
        return super().get(*args, **kwargs)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['nbar'] = 'entities'
        context['language'] = self.language
        return context

class ShowNotebooks(TemplateView):
    template_name = 'notebooks.html'
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['nbar'] = 'notebooks'
        return context

class ShowPersons(TemplateView):
    template_name = 'persons.html'
    persons = []
    def get(self, request, *args, **kwargs):
        page = request.GET.get('page')
        ranked_persons = Person.objects.annotate(q_count=Count('pages')).order_by('-q_count')
        paginator = Paginator(ranked_persons, 35)
        self.persons = paginator.get_page(page)
        return super().get(request, *args, **kwargs)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['persons'] = self.persons
        context['persons_count'] = self.persons.paginator.count
        context['nbar'] = 'entities'
        context['sbar'] = 'persons'
        return context

class ShowTerms(TemplateView):
    template_name = 'terms.html'
    terms = []
    def get(self, request, *args, **kwargs):
        page = request.GET.get('page')
        ranked_terms = Term.objects.annotate(q_count=Count('pages')).order_by('-q_count')
        paginator = Paginator(ranked_terms, 35)
        self.terms = paginator.get_page(page)
        return super().get(request, *args, **kwargs)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['terms'] = self.terms
        context['terms_count'] = self.terms.paginator.count
        context['nbar'] = 'entities'
        context['sbar'] = 'terms'
        return context

class ShowOrganisations(TemplateView):
    template_name = 'organisations.html'
    organisations = []
    def get(self, request, *args, **kwargs):
        page = request.GET.get('page')
        ranked_organisations = Organisation.objects.annotate(q_count=Count('pages')).order_by('-q_count')
        paginator = Paginator(ranked_organisations, 35)
        self.organisations = paginator.get_page(page)
        return super().get(request, *args, **kwargs)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['organisations'] = self.organisations
        context['organisations_count'] = self.organisations.paginator.count
        context['nbar'] = 'entities'
        context['sbar'] = 'organisations'
        return context

class ShowPlaces(TemplateView):
    template_name = 'places.html'
    places = []
    def get(self, request, *args, **kwargs):
        page = request.GET.get('page')
        paginator = Paginator(Place.objects.all(), 35)
        ranked_places = Place.objects.annotate(q_count=Count('pages')).order_by('-q_count')
        paginator = Paginator(ranked_places, 35)
        self.places = paginator.get_page(page)
        return super().get(request, *args, **kwargs)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['places'] = self.places
        context['places_count'] = self.places.paginator.count
        context['nbar'] = 'entities'
        context['sbar'] = 'places'
        return context

class ShowBatches(TemplateView):
    template_name = 'batches.html'
    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs)
    def post(self, request, *args, **kwargs):
        es_query_json = request.POST.get('es_query')
        es_query = json.loads(es_query_json)
        s = PageDocument.search()
        print(es_query)
        s = s.from_dict(es_query)
        s.source(["id"])
        total = s.count()
        s = s[0:total]
        results = s.execute()
        page_ids = [o.id for o in results]
        print(page_ids)
        messages.add_message(request, messages.INFO, 'Batch created')
        batch = Batch(name=request.POST['name'])
        batch.creator = request.user
        batch.save()
        batch.pages.add(*page_ids)
        return super().get(request, *args, **kwargs)        

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['batches'] = Batch.objects.all()
        context['batch_form'] = BatchForm
        context['nbar'] = 'batches'
        return context

class ShowEntity(DetailView):
    model = Entity
    menu_item = 'stats'
    
    def get(self, request, *args, **kwargs):
        self.menu_item = self.kwargs['menu_item']
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['nbar'] = 'entities'
        context['menu_item'] = self.menu_item
        context['batches'] = Batch.objects.all()
        return context

def pull_entity_network(request, pk):
    toggles = request.POST.getlist('toggles[]')
    request.session['toggles'] = toggles
    batch_pk = int(request.POST['Batch'])
    entity_pk = int(request.POST['entity_pk'])
    entity = Entity.objects.get(pk=entity_pk)
    pages = entity.pages.all().prefetch_related('mention_set')
    request.session['current_batch_id'] = batch_pk
    if batch_pk != 0:
        pages = entity.pages.filter(batch__pk=batch_pk).prefetch_related('mention_set')
    match_els = []
    page_els = [{'group': 'nodes', 'data': { 'id': entity.text }, 'classes': [f'center-{entity.term_type()}','center-node'], 'position': { 'x': 300, 'y': 300 } }]
    for page in pages:
        page_els.append({'group': 'nodes', 'data': { 'id': str(page) }, 'classes': ['Page', 'extra'], 'position': { 'x': 300, 'y': 300 } }) 
        page_els.append({'group': 'edges', 'data': { 'id': f"{entity} {page}" ,'source': entity.text, 'target': str(page) }  })
        for mention in page.mention_set.all().prefetch_related('entity').prefetch_related('page'):
            if mention.entity.term_type() in toggles:
                match_els.append({'group': 'nodes', 'data': { 'id': mention.entity.text,  'href': f"/entities/{mention.entity.pk}/cooccurrence" }, 'classes': [mention.entity.term_type(), 'extra', 'entity'], 'position': { 'x': 300, 'y': 300 } }) 
                match_els.append({'group': 'edges', 'data': { 'id': f"{page} {mention.entity}" ,'source': str(page), 'target': mention.entity.text }  })
    return JsonResponse({'page_els': page_els, 'match_els': match_els})

class ShowVolume(DetailView):
    model = Volume
    pages = []

    def get(self, request, *args, **kwargs):
        page = request.GET.get('page') 
        paginator = Paginator(self.get_object().page_set.all(), 35)   
        self.pages = paginator.get_page(page)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['volumes'] = Volume.objects.all()
        context['pages'] = self.pages
        context['pages_count'] = self.pages.paginator.count
        context['nbar'] = 'browse'
        return context

class ShowBatch(DetailView):
    model = Batch
    pages = []
    language_stats = []
    places = []
    persons = []
    organisations = []
    places = []
    terms = []

    def get(self, request, *args, **kwargs):
        page = request.GET.get('page') 
        paginator = Paginator(self.get_object().pages.all(), 35)   
        language_stats_raw = self.get_object().pages.all().values('language').annotate(total=Count('language')).order_by('total')
        self.language_stats = [entry for entry in language_stats_raw]
        self.pages = paginator.get_page(page)
        # for page in self.get_object().pages.all():
        #     for place in page.place_set.all():
        #         self.places.append(place)
        #     for person in page.person_set.all():
        #         self.persons.append(person)
        #     for organisation in page.organisation_set.all():
        #         self.organisations.append(organisation)
        #     for term in page.term_set.all():
        #         self.terms.append(term)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pages'] = self.pages
        context['pages_count'] = self.pages.paginator.count
        context['nbar'] = 'browse'
        context['language_stats'] = self.language_stats
        context['places'] = list(dict.fromkeys(self.places))
        context['terms'] = self.terms
        context['organisations'] = list(dict.fromkeys(self.organisations))
        context['persons'] = list(dict.fromkeys(self.persons))
        return context

def export_batch(request, pk):
    batch = Batch.objects.get(pk=pk)
    file_data = ""
    for page in batch.pages.all():
        file_data += page.text
        file_data += '\n'
    response = HttpResponse(file_data, content_type='application/text charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename="'+ batch.name+'.txt"'
    return response

def export_batch_en(request, pk):
    batch = Batch.objects.get(pk=pk)
    file_data = ""
    for page in batch.pages.all():
        file_data += page.bow_text
        file_data += '\n'
    response = HttpResponse(file_data, content_type='application/text charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename="'+ batch.name+'_en.txt"'
    return response

def export_batch_entities(request, pk):
    batch = Batch.objects.get(pk=pk)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="'+batch.name+ '_'+ 'entities'+'.csv"'
    writer = csv.writer(response)
    writer.writerow(['Volume', 'Page', 'People', 'Places', 'Organisations', 'Geodetical Terms'])
    for page in batch.pages.all():
        places = []
        people = []
        organisations = []
        terms = []
        for place in page.place_set.all():
            places.append(place.name)
        for person in page.person_set.all():
            people.append(person.text)
        for organisation in page.organisation_set.all():
            organisations.append(organisation.name)
        for term in page.term_set.all():
            terms.append(term.name)
        writer.writerow([page.volume, page.page_identifier, ", ".join(people), ", ".join(places), ", ".join(organisations), ", ".join(terms)])
    return response

def export_batch_as_jsonl(request, pk):
    batch = Batch.objects.get(pk=pk)
    file_data = ""
    for page in batch.pages.all():
        file_data += "{'text': "+page.text.replace('\n', ' ').replace('\r', '')+"}"
        file_data += '\n'
    response = HttpResponse(file_data, content_type='application/text charset=utf-8')
    response['Content-Disposition'] = 'attachment; filename="'+ batch.name+'.jsonl"'
    return response

def add_page_to_batch(request, batch_id, page_id):
    batch = Batch.objects.get(pk=batch_id)
    page = Page.objects.get(pk=page_id)
    print(batch)
    print(page)
    batch.pages.add(page)
    data = {'status':'ok'}
    return JsonResponse(data)

def set_current_batch(request, batch_id):
    request.session['current_batch_id'] = batch_id
    data = {'status':'ok'}
    return JsonResponse(data)

def remove_page_from_batch(request, batch_id, page_id):
    batch = Batch.objects.get(pk=batch_id)
    page = Page.objects.get(pk=page_id)
    batch.pages.remove(page)
    data = {'status':'ok'}
    return JsonResponse(data)

def page_in_batch(request, batch_id, page_id):
    batch = Batch.objects.get(pk=batch_id)
    data = {'in_batch': (page_id in batch.pages.values_list('id',flat=True))}
    return JsonResponse(data)

class ShowSearch(TemplateView):
    template_name = 'search.html'
    model = Page
    q = ''
    search_terms = []
    selected_volumes = []
    results = []
    first_year = ''
    last_year = ''
    first_bound_string = ''
    last_bound_string = ''
    total_results = []
    es_query = None
    current_user_id = None
    languages = []
    section_types = []
    features = []
    selected_language = ''
    selected_section_type = ''
    selected_feature = ''

    def get(self, request, *args, **kwargs):
        self.current_user_id = request.user.id
        all_docs = Volume.objects.all()
        self.volumes = []
        self.first_year = all_docs.first().start_date.strftime('%Y')
        self.last_year = all_docs.last().start_date.strftime('%Y')
        for doc in all_docs:
            self.volumes.append(doc.start_date.strftime('%Y'))
        page = request.GET.get('page')
        range = request.GET.get('range')
        if range:
            self.first_bound_string, self.last_bound_string = range.split(' - ')
        else:
            self.first_bound_string = self.first_year
            self.last_bound_string = self.last_year

        first_bound = datetime.strptime(self.first_bound_string, '%Y').date()
        last_bound = datetime.strptime(self.last_bound_string, '%Y').date()
        s = PageDocument.search().filter('range', volume__start_date={'from': first_bound, 'to' : last_bound }).sort('id')
        self.selected_volumes = request.GET.getlist('volume_filter')
        self.selected_language = request.GET.get('language','')
        self.selected_section_type = request.GET.get('section_type','')
        self.selected_feature = request.GET.get('feature','')
        if self.selected_feature:
            s = s.filter('term', feature=self.selected_feature)
        if self.selected_language:
            s = s.filter('term', language=self.selected_language)
        if self.selected_section_type:
            s = s.filter('term', section_type=self.selected_section_type)
        self.q = request.GET.get('q','')
        if self.q:
            # s = s.query('simple_query_string',query=self.q, fuzziness='AUTO')
            s = s.filter(SimpleQueryString(
                query = self.q, 
                fields= ["text"],
                default_operator= "and"
            ))
            
            s = s.highlight('text', number_of_fragments=0)
        self.es_query = s.to_dict()
        self.total_results = SearchResults(s)
        # array = []
        # for item in result:
        #     array.append(item.id)
        # print(array)
        paginator = Paginator(self.total_results, 35)
        self.results = paginator.get_page(page)
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['first_year'] = self.first_year
        context['batches'] = Batch.objects.filter(creator_id=self.current_user_id)
        context['last_year'] = self.last_year
        context['total_volumes'] = int(self.last_year) - int(self.first_year)
        context['first_bound'] = self.first_bound_string
        context['last_bound'] = self.last_bound_string
        context['volumes'] = self.volumes
        context['total_results'] = [self.total_results.count()]
        context['results'] = self.results
        context['ids'] = list(self.total_results)
        context['results_count'] = self.results.paginator.count
        context['q'] = self.q
        context['search_terms'] = self.search_terms
        context['selected_language'] = self.selected_language
        context['selected_section_type'] = self.selected_section_type
        context['selected_feature'] = self.selected_feature
        context['languages'] = Page.objects.values_list('language', flat=True).distinct().order_by('language')
        context['section_types'] = Page.objects.values_list('section_type', flat=True).distinct().order_by('section_type')
        context['features'] = Page.objects.values_list('feature', flat=True).distinct().order_by('feature')
        context['search_form'] = SearchForm(initial={'q': self.q})
        context['es_query'] = json.dumps(self.es_query, default=str)
        batch_name = ''
        if self.q:
            batch_name += self.q + ' ' 
        context['batch_form'] = BatchForm(initial={'name': batch_name + self.first_bound_string + '-' + self.last_bound_string + ' ' + self.selected_language + ' ' + self.selected_section_type + ' ' + self.selected_feature})
        context['nbar'] = 'search'
        return context
