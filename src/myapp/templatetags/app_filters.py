from django import template
from django.utils.safestring import mark_safe
from iso639 import languages
import unidecode

register = template.Library()

@register.filter
def language_name(code):
    try:
        language_name = languages.get(alpha2=code).name
    except:
        language_name = 'n/a'
    return language_name

@register.filter
def get_obj_attr(obj, attr):
    fragments = ''
    for fragment in (obj[str(attr)]):
        fragments = fragments + '<p>' + fragment + '</p>'
    return mark_safe(fragments)

