from __future__ import absolute_import, unicode_literals
import logging
import os
import natsort
from datetime import datetime, date
import requests

from django.conf import settings
from collections import defaultdict
from mydjango.celery import app
import json
import pandas as pd
import numpy as np
import django
import csv
django.setup()

from myapp.models import Volume
from myapp.models import Page
from myapp.models import Person
from myapp.models import Place
from myapp.models import Organisation
from myapp.models import Term
from myapp.models import Mention, Entity, Attendee
from myapp.models import AttendanceEvent
from langid.langid import LanguageIdentifier, model

logger = logging.getLogger("celery")


@app.task
def ingest_sections():
    with open('./media/MtE_PageSplitsByLanguage - Sections.tsv', newline='\n', encoding='utf-8-sig') as csvfile:
        csv_reader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
        for row in csv_reader:
            volume_name = row['Volume']
            try:
                volume = Volume.objects.get(name=volume_name)
            except Volume.DoesNotExist:
                volume = None            
            if volume:
                print(volume_name)
                for field_name in row:
                    if field_name != 'Volume':
                        section_name = field_name
                        section_values = row[field_name].replace(" ", "")
                        intervals = section_values.split(',')
                        for interval in intervals:
                            if interval != '':
                                range_values = interval.split('-')
                                print(range_values)
                                if (len(range_values) == 2) and (range_values[1]!= ''):
                                    for i in range(int(range_values[0]), int(range_values[1])+1):
                                        page_id = str(i).zfill(4)
                                        try:
                                            page = volume.page_set.get(page_identifier=page_id)
                                        except Page.DoesNotExist:
                                            page = None
                                        if page:
                                            print(page)
                                            page.section_type = section_name
                                            page.save()
                                            print(page.section_type)
                                else:
                                    page_id = str(range_values[0]).zfill(4)
                                    try:
                                        page = volume.page_set.get(page_identifier=page_id)
                                    except Page.DoesNotExist:
                                        page = None
                                    if page:
                                        print(page)
                                        page.section_type = section_name
                                        page.save()
                                        print(page.section_type)


@app.task
def cleanup_sections():
    intro = Page.objects.filter(section_type__icontains = 'Intro')
    for page in intro:
        page.section_type = 'Intro'
        page.save()
    minutes = Page.objects.filter(section_type__icontains = 'Minutes')
    for page in minutes:
        page.section_type = 'Minutes'
        page.save()
    generalbericht = Page.objects.filter(section_type__icontains = 'Generalbericht')
    for page in generalbericht:
        page.section_type = 'Generalbericht'
        page.save()

    annexes = Page.objects.filter(section_type__icontains = 'Annexes')
    for page in annexes:
        page.section_type = 'Annexes'
        page.save()

# clean pages that have no tokens
@app.task
def cleanup_pages():
    all_pages = Page.objects.all()
    for page in all_pages:
        if not page.tokens:
            page.text = ''
            page.save()
            print('cleaned up')
            print(page)

@app.task
def ingest_features():
    with open('./media/MtE_PageSplitsByLanguage - Features.tsv', newline='\n', encoding='utf-8-sig') as csvfile:
        csv_reader = csv.DictReader(csvfile, delimiter='\t', quotechar='"')
        for row in csv_reader:
            volume_name = row['Volume']
            try:
                volume = Volume.objects.get(name=volume_name)
            except Volume.DoesNotExist:
                volume = None            
            if volume:
                print(volume_name)
                for field_name in row:
                    if field_name != 'Volume':
                        section_name = field_name
                        section_values = row[field_name].replace(" ", "")
                        intervals = section_values.split(',')
                        for interval in intervals:
                            if interval != '':
                                range_values = interval.split('-')
                                print(range_values)
                                if (len(range_values) == 2) and (range_values[1]!= ''):
                                    for i in range(int(range_values[0]), int(range_values[1])+1):
                                        page_id = str(i).zfill(4)
                                        try:
                                            page = volume.page_set.get(page_identifier=page_id)
                                        except Page.DoesNotExist:
                                            page = None
                                        if page:
                                            print(page)
                                            page.feature = section_name
                                            page.save()
                                            print(page.feature)
                                else:
                                    page_id = str(range_values[0]).zfill(4)
                                    try:
                                        page = volume.page_set.get(page_identifier=page_id)
                                    except Page.DoesNotExist:
                                        page = None
                                    if page:
                                        print(page)
                                        page.feature = section_name
                                        page.save()
                                        print(page.feature)


@app.task
def detect_language():
    identifier = LanguageIdentifier.from_modelstring(model, norm_probs=True)
    identifier.set_languages(['fr', 'en', 'it', 'de'])  # ISO 639-1 codes
    for page in Page.objects.all():
        try:
            language, confidence = identifier.classify(page.text)
        except:
            language = 'n/a'
        page.language = language
        page.language_confidence = confidence
        page.save()
        print(page.language)
        print(page.language_confidence)

@app.task
def save_en_files():
    for page in Page.objects.all():
        filename = "./media/translated_texts/"+page.volume.name+"/"+page.page_identifier+".txt"
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "w") as f:
            f.write(page.bow_text)

@app.task
def translate_pages():
    from argostranslate import package, translate
    package.install_from_path('./media/translation_models/de_en.argosmodel')
    package.install_from_path('./media/translation_models/it_en.argosmodel')
    package.install_from_path('./media/translation_models/fr_en.argosmodel')
    installed_languages = translate.get_installed_languages()
    print([str(lang) for lang in installed_languages])
    translations = {}
    translations['de'] = installed_languages[2].get_translation(installed_languages[0])
    translations['fr'] = installed_languages[1].get_translation(installed_languages[0])
    translations['it'] = installed_languages[3].get_translation(installed_languages[0])
    for page in Page.objects.all():
        print(page)
        if page.language == 'en':
            translation = page.text
            page.bow_text = translation
            page.save()
        else:
            if page.bow_text == '':
                translation = translations[page.language].translate(page.text)
                print(translation)
                page.bow_text = translation
                page.save()
            else:
                print('already_translated')        
       

@app.task
def tokenize_pages():
    import spacy
    all_pages = Page.objects.all()
    languages = ['de','fr','it','en']
    for language in languages:
        if(language == 'de'):
            nlp = spacy.load("de_core_news_lg")
        if(language == 'fr'):
            nlp = spacy.load("fr_core_news_lg")
        if(language == 'it'):
            nlp = spacy.load("it_core_news_lg")
        if(language == 'en'):
            nlp = spacy.load("en_core_web_trf")
        for page in all_pages.filter(language=language):
            print(page)
            tokens = nlp(page.text)
            tokens = [token.lemma_ for token in tokens if (
                token.is_stop == False and \
                token.is_punct == False and \
                token.is_digit == False and \
                token.is_alpha == True and \
                len(token.lemma_) > 3 and \
                token.lemma_.strip()!= '')]
            page.tokens = tokens
            page.save()
            print(page.tokens)

@app.task
def perform_and_store_spacy_models():
    import spacy
    nlp = spacy.load("./trained/")
    all_pages = Page.objects.all()    
    for page in all_pages:
        print(page)
        doc = nlp(page.text)
        vocab = doc.vocab
        page.spacy_doc = doc.to_bytes()
        page.spacy_vocab = doc.vocab.to_bytes()
        page.save()  

@app.task
def coocurrence():
    entities_per_page = []
    for page in Page.objects.all():
        entities_per_page.append([str(ent) for ent in page.entity_set.all()])
    df = pd.DataFrame(entities_per_page)
    u = (pd.get_dummies(df, prefix='', prefix_sep='')
        .groupby(level=0, axis=1)
        .sum())
    v = u.T.dot(u)
    v.values[(np.r_[:len(v)], ) * 2] = 0
    matrix = v


@app.task  
def run_ner():
    Mention.objects.all().delete()
    Person.objects.all().delete()
    Place.objects.all().delete()
    Organisation.objects.all().delete()
    Term.objects.all().delete()

    import spacy
    all_pages = Page.objects.all()
    languages = ['de','fr','it','en']
    for language in languages:
        if(language == 'de'):
            nlp = spacy.load("de_core_news_lg")
        if(language == 'fr'):
            nlp = spacy.load("fr_core_news_lg")
        if(language == 'it'):
            nlp = spacy.load("it_core_news_lg")
        if(language == 'en'):
            nlp = spacy.load("en_core_web_trf")
        #nlp = spacy.load("/app/models/"+language+"/")
        for page in all_pages.filter(language=language):
            print(page)
            doc = nlp(page.text)
            for ent in doc.ents:
                if(ent.label_ == 'MISC'):
                    continue
                print(ent.start)
                print(ent.end)
                print("LABEL:-----")
                print(ent.label_)
                clean_text = ent.text.replace("\n", "").replace("\r", "")
                print("TEXT:------")
                print(clean_text)
                if (ent.label_ == 'PER') or (ent.label_ == 'PERSON'):
                    entity, created = Person.objects.get_or_create(text=clean_text)
                if (ent.label_ == 'LOC') or (ent.label_ == 'LOCATION') or (ent.label_ == 'GPE'):
                    entity, created = Place.objects.get_or_create(text=clean_text)
                if (ent.label_ == 'ORG') or (ent.label_ == 'ORGANISATION'):
                    entity, created = Organisation.objects.get_or_create(text=clean_text)
                if (ent.label_ == 'TERM') or (ent.label_ == 'GEO_TERM'):
                    entity, created = Term.objects.get_or_create(text=clean_text)
                entity.language = language
                entity.save()
                mention = Mention()
                mention.offset_start = ent.start_char
                mention.offset_end = ent.end_char
                mention.page = page
                mention.entity = entity
                mention.save()
                print(mention.page)
                print(ent.text, ent.start_char, ent.end_char, ent.label_)

@app.task
def export_jsonl():
    import jsonlines
    # all_pages = Page.objects.all()
    languages = ['de','it', 'fr', 'en']
    for language in languages:
        pages = Page.objects.filter(language=language, language_confidence=1.0).order_by('?')[:4000]
        with jsonlines.open('/app/annotations/'+language+'.jsonl', mode='w') as outfile:
            for page in pages:
                print("OUTPUTTING: "+ page.language+" Page"+str(page)+" Confidence: "+ str(page.language_confidence))
                outfile.write({'text': page.text})

@app.task
def ingest_metadata():
    with open('./media/metadata_latest.csv', newline='\n', encoding='utf-8-sig') as csvfile:
        csv_reader = csv.DictReader(csvfile, delimiter=';', quotechar='"')
        for row in csv_reader:
            key = row['ID']
            print('KEY')
            print(key)
            metadata = row
            metadata =  {k.lower(): v for k, v in metadata.items()}
            metadata =  {k.replace(" ", "_"): v for k, v in metadata.items()}
            metadata =   {k: v for k, v in metadata.items() if v is not ''}
            clean_metadata = {}
            for k,v in metadata.items():
                if k == 'extra' or k == 'notes' or k == 'date_modified' or k == 'manual_tags' or k == 'date_added' or k == 'item_type' or k == 'volume':
                    next
                else:
                    clean_metadata[k] = v
            metadata = clean_metadata
            try:
                volume = Volume.objects.get(name=key)
            except Volume.DoesNotExist:
                volume = None            
            if volume:
                volume.metadata = {}
                volume.save()
                volume.metadata.update(clean_metadata)
                volume.save()
                print('METADATA SAVED')
                print(volume.metadata)
            row['Item Type']
            row['Publication Year']
            row['Author']
            row['Title']
            row['Publication Title']
            row['ISBN']
            row['ISSN']
            row['DOI']
            row['Url']
            row['Abstract Note']
            row['Date']
            row['Date Added']
            row['Date Modified']
            row['Access Date']
            row['Pages']
            row['Num Pages']
            row['Issue']
            row['Volume']
            row['Number Of Volumes']
            row['Journal Abbreviation']
            row['Short Title']
            row['Series']
            row['Series Number']
            row['Series Text']
            row['Series Title']
            row['Publisher']
            row['Place']
            row['Language']
            row['Rights']
            row['Type']
            #print(row)
            #print('\n')

@app.task
def ingest_attendees():
    AttendanceEvent.objects.all().delete()
    Attendee.objects.all().delete()
    

    with open('./media/attendees.csv', newline='\n', encoding='utf-8-sig') as csvfile:
        csv_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        for row in csv_reader:
            surname = row['Surname']
            name = row['Name']
            alternate_surname = row['Alternate surname']
            function = row['Function']
            bio = row['Bio']
            country = row['Country']
            # role = row['Role']
            # first_mention = row['First mention']
            # last_mention = row['Last mention']
            since = row['Since']
            until = row['Until']
            # wikidata = row['Wikidata']
            if since:
                since_date = datetime.strptime(since, '%Y').date()
                # attendee.active_since = since_date
            if until:
                until_date = datetime.strptime(until, '%Y').date()
                # attendee.active_until = until_date
            
            attendee, created = Attendee.objects.get_or_create(
                surname = surname,
                name = name,
                alt_surname = alternate_surname,
                function = function,
                bio = bio,
                country = country,
                # tolloid NOT BEING RECOGNIZED wikidata = wikidata,
                active_since = since_date,
                active_until = until_date
            )

            print("my id is ", attendee.id)
            for x in range (attendee.active_since.year, attendee.active_until.year+1):
                if x == 1882:
                    # tolloid 1881_82 how do you avoid this lame hardcoding 1882?
                    multiyear = Volume.objects.filter(start_date__year=1881).get()
                    e, created = AttendanceEvent.objects.get_or_create(
                    attendee = attendee,
                    event_year = date(1882,1,1),
                    volume = multiyear
                    )
                else:
                    try:
                        v = Volume.objects.filter(start_date__year=int(x)).get()
                    except Volume.MultipleObjectsReturned: 
                        # if x == 1909, 1909_1_2 and 1909_3 case
                        # if x == 1912, 1912_1 and 1912_2 case
                        # multivolume = Volume.objects.filter(start_date__year=int(x))
                        v = None
                        for volumeobject in Volume.objects.filter(start_date__year=int(x)):
                            e, created = AttendanceEvent.objects.get_or_create(
                            attendee = attendee,
                            event_year = date(volumeobject.start_date.year,1,1),
                            volume = volumeobject
                            )
                            print('--------begin------------')
                            print('eventyear', x)
                            print('attendeeid', attendee.id)
                            print('volumekey', volumeobject.__dict__)
                            print('attendeekey', attendee.__dict__)
                            print('attendanceeventkey', e.__dict__)
                            print('--------again------------')
                    except Volume.DoesNotExist:
                        v = None
                        # tolloid or should we create an event without a volume? is volume_key required?
                    if v:
                        e, created = AttendanceEvent.objects.get_or_create(
                        attendee = attendee,
                        # event_year = date(x,1,1),
                        event_year = v.start_date,
                        volume = v
                        )
                        print('--------begin------------')
                        print('eventyear', x)
                        print('attendeeid', attendee.id)
                        print('volumekey', v.__dict__)
                        print('attendeekey', attendee.__dict__)
                        print('attendanceeventkey', e.__dict__)
                        print('--------again------------')

    # girl = Attendee.objects.all()[:1].get()
    # girl = Attendee.objects.get(surname__startswith='Launhardt')
    # girl = Attendee.objects.get(surname__startswith='testperson')



# try:
#     v = Volume.objects.filter(start_date__year=1909).get()
# except Volume.MultipleObjectsReturned:
#     print("ok")
#     v = Volume.objects.filter(start_date__year=1909)
#     for volumeobject in v:
#         print(volumeobject)
#         print(volumeobject.start_date)
# except  Volume.DoesNotExist:
#     v = None


            # a = Attendee.objects.get(id=girl.id)
            # use filter instead of get because it can return more than one result, but add get so you don't get a queryset back but actually the objects

# testing notes
    # for x in range(since, until+1):
        # print(x)
        
    # print("since: ", attendee.active_since.year)
    # print("until: ", attendee.active_until.year)

    # example since = 1901, until  1903
    # attendee1 - event_1_1901 - volume_1901_1
    # attendee1 - event_2_1901 - volume 1901_2
    # attendee1 - event_1_1902 - volume 1902
    # attendee1 - event_1_1903 - volume 1903

    # a = Volume.objects.get(name='1912_2')
    # print (a.name, a.start_date.year)
    # print("kimtest")


@app.task
def ingest_sentences():
    # Page.objects.all().delete()
    # Volume.objects.all().delete()
    path = './media/gold_standard_sentences/'
    years = os.listdir(path)
    sorted_years = natsort.natsorted(years)
    for year in sorted_years:
        if os.path.isdir(os.path.join(path,year)):
            #logger.info(year)
            filenames = os.listdir(os.path.join(path,year))
            sorted_filenames = natsort.natsorted(filenames)
            date_info = year.split('_')
            key_year_dict = {
                '1862': 'G2M6TSTF',
                '1863': 'CEVYX6VU',
                '1864': '3I6IVAFZ',
                '1865': '5YKR9EGY',
                '1866': '3ZZEZPL2',
                '1867': 'HFJYX4PF',
                #'1868': 'CEVYX6VU',
                #'1869': 'CEVYX6VU',
                #'1870': 'CEVYX6VU',
                #'1871': 'CEVYX6VU',
                #'1872': 'CEVYX6VU',
                #'1873': 'CEVYX6VU',
            }
            key = key_year_dict.get(date_info[0], None)
            print(key)
            start_date = datetime.strptime(date_info[0], '%Y').date()
            # end_date = datetime.strptime('18'+end_date_string, '%Y')
            volume, created = Volume.objects.get_or_create(name=year, start_date=start_date, key=key)
            index = 0
            for filename in sorted_filenames:
                identifier, extension =  os.path.splitext(filename)
                if extension == '.json':
                    file =  open(path+'/'+ year +'/'+filename, 'r')
                    page = volume.page_set.get(page_identifier=identifier)
                    # page.text = file.read().replace('<', ' ')
                    sentence_data = json.load(file)
                    page.sentences = sentence_data['contains']
                    print(page.sentences)
                    if 'contains' in sentence_data:
                        del sentence_data['contains']
                    if 'contains' in sentence_data:
                        del sentence_data['contains']
                    if 'filename' in sentence_data:
                        del sentence_data['filename']
                    page.metadata = sentence_data
                    page.volume = volume
                    page.save()
                    index = index + 1
                    logger.info('-'*10 + volume.name + ' > ' + identifier + '-'*10)
                    # volume.content = my_file.read().replace('\n', '')
                    # volume.metadata = {}
                    # volume.save()
                    # logger.info("-"*25)
                    # logger.info(volume)
                    # logger.info("-"*25)


@app.task
def ingest_texts():
    Page.objects.all().delete()
    Volume.objects.all().delete()
    path = './media/ocred_sources/'
    years = os.listdir(path)
    sorted_years = natsort.natsorted(years)
    for year in sorted_years:
        if os.path.isdir(os.path.join(path,year)):
            #logger.info(year)
            filenames = os.listdir(os.path.join(path,year))
            sorted_filenames = natsort.natsorted(filenames)
            date_info = year.split('_')
            key_year_dict = {
                '1862': 'G2M6TSTF',
                '1863': 'CEVYX6VU',
                '1864': '3I6IVAFZ',
                '1865': '5YKR9EGY',
                '1866': '3ZZEZPL2',
                '1867': 'HFJYX4PF',
                #'1868': 'CEVYX6VU',
                #'1869': 'CEVYX6VU',
                #'1870': 'CEVYX6VU',
                #'1871': 'CEVYX6VU',
                #'1872': 'CEVYX6VU',
                #'1873': 'CEVYX6VU',
            }
            key = key_year_dict.get(date_info[0], None)
            print(key)
            start_date = datetime.strptime(date_info[0], '%Y').date()
            # end_date = datetime.strptime('18'+end_date_string, '%Y')
            volume, created = Volume.objects.get_or_create(name=year, start_date=start_date, key=key)
            index = 0
            for filename in sorted_filenames:
                identifier, extension =  os.path.splitext(filename)
                if extension == '.txt':
                    file =  open(path+'/'+ year +'/'+filename, 'r')
                    page = Page(page_identifier=identifier,page_index=index)
                    page.text = file.read().replace('<', ' ')
                    page.volume = volume
                    page.save()
                    index = index + 1
                    logger.info('-'*10 + volume.name + ' > ' + identifier + '-'*10)
                    # volume.content = my_file.read().replace('\n', '')
                    # volume.metadata = {}
                    # volume.save()
                    # logger.info("-"*25)
                    # logger.info(volume)
                    # logger.info("-"*25)


@app.task
def ingest_translations():
    path = './media/translated_texts/'
    years = os.listdir(path)
    sorted_years = natsort.natsorted(years)
    for year in sorted_years:
        if os.path.isdir(os.path.join(path,year)):
            #logger.info(year)
            filenames = os.listdir(os.path.join(path,year))
            sorted_filenames = natsort.natsorted(filenames)
            date_info = year.split('_')
            volume = Volume.objects.get(name=year)
            index = 0
            for filename in sorted_filenames:
                identifier, extension =  os.path.splitext(filename)
                print(filename)
                if extension == '.txt':
                    file =  open(path+'/'+ year +'/'+filename, 'r')
                    page = volume.page_set.get(page_identifier=identifier)
                    page.bow_text = file.read().replace('<', ' ')
                    page.save()
                    index = index + 1
                    logger.info('-'*10 + volume.name + ' > ' + identifier + '-'*10)
                    # volume.content = my_file.read().replace('\n', '')
                    # volume.metadata = {}
                    # volume.save()
                    # logger.info("-"*25)
                    # logger.info(volume)
                    # logger.info("-"*25)

@app.task
def ingest_sources():
    Page.objects.all().delete()
    Volume.objects.all().delete()
    path = './media/gold_standard_text/'
    years = os.listdir(path)
    sorted_years = natsort.natsorted(years)
    for year in sorted_years:
        if os.path.isdir(os.path.join(path,year)):
            #logger.info(year)
            filenames = os.listdir(os.path.join(path,year))
            sorted_filenames = natsort.natsorted(filenames)
            date_info = year.split('_')
            key_year_dict = {
                '1862': 'G2M6TSTF',
                '1863': 'CEVYX6VU',
                '1864': '3I6IVAFZ',
                '1865': '5YKR9EGY',
                '1866': '3ZZEZPL2',
                '1867': 'HFJYX4PF',
                #'1868': 'CEVYX6VU',
                #'1869': 'CEVYX6VU',
                #'1870': 'CEVYX6VU',
                #'1871': 'CEVYX6VU',
                #'1872': 'CEVYX6VU',
                #'1873': 'CEVYX6VU',
            }
            key = key_year_dict.get(date_info[0], None)
            print(key)
            start_date = datetime.strptime(date_info[0], '%Y').date()
            # end_date = datetime.strptime('18'+end_date_string, '%Y')
            volume, created = Volume.objects.get_or_create(name=year, start_date=start_date, key=key)
            index = 0
            for filename in sorted_filenames:
                identifier, extension =  os.path.splitext(filename)
                if extension == '.txt':
                    file =  open(path+'/'+ year +'/'+filename, 'r')
                    page = Page(page_identifier=identifier,page_index=index)
                    page.text = file.read().replace('<', ' ')
                    page.volume = volume
                    page.save()
                    index = index + 1
                    logger.info('-'*10 + volume.name + ' > ' + identifier + '-'*10)
                    # volume.content = my_file.read().replace('\n', '')
                    # volume.metadata = {}
                    # volume.save()
                    # logger.info("-"*25)
                    # logger.info(volume)
                    # logger.info("-"*25)