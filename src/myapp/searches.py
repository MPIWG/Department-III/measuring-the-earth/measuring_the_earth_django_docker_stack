
from .models import Page
from elasticsearch_dsl import FacetedSearch, TermsFacet, DateHistogramFacet

class FacetSearch(FacetedSearch):
    doc_types = [Page, ]
    # fields that should be searched
    fields = ['text']

    facets = {
        # use bucket aggregations to define facets
        'year_frequency': DateHistogramFacet(field='volume.start_date', interval='year')
    }

    def search(self):
        # override methods to add custom pieces
        s = super().search()
        return s.filter('range', publish_from={'lte': 'now/h'})