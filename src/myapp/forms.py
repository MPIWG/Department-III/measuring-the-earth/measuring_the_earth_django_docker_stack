from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.layout import Field
from .models import Batch, Volume

class BatchForm(forms.ModelForm):
    class Meta:
        model = Batch
        fields = ['name']

    def __init__(self, *args, **kwargs):
        super(BatchForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.form_action = '/batches/'
        self.helper.add_input(Submit('', 'Create a new batch'))
        self.fields['name'].required = False
        self.fields['name'].label = "Create a new batch from this selection of pages:"

class SearchForm(forms.Form):
    q = forms.CharField(
        label = 'Query:',
        max_length = 256,
        required = False,
        widget= forms.Textarea(
            attrs={'rows': 2, 'cols': 25}
        )
    )

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'get'
        self.helper.form_action = '/search/'
        self.helper.add_input(Submit('', 'Submit'))

    def save(self, commit=True):
        image = self.cleaned_data.pop('image')
        instance = super().save(commit=True)
        return instance