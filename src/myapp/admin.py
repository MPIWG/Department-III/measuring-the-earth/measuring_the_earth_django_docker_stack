from django.contrib import admin
from .models import Person
from .models import Organisation
from .models import Place
from .models import Volume
from .models import Page

admin.site.register(Person)
admin.site.register(Organisation)
admin.site.register(Place)
admin.site.register(Volume)
admin.site.register(Page)
