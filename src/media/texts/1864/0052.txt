 

u an

Substituirt man die hieraus für 0’, 0”, 6"! hervorgehenden Werthe in das erste Glied auf der

rechten Seite der Gleichung (1.), was am besten durch Differentiation geschehen kann, so

findet man
! " mt Pts 1! m > !
s 6/— cos 6//cos o coss!’ — coss’!’coss sins :
Pee arene —=)) SCRA FRILL PET SS UI { cos n/".0s! +- cosn".os!""— os! | :
sino’ sin o!” sins!’ sins! sins’ sins

Das erste Glied auf der rechten Seite dieses Ausdrucks entspricht wieder einem sphärischen
Dreieck und ist gleich dem Cosinus des sphärischen Winkels, der von den Deiten',s”, s’" ein-

geschlossen wird. Bezeichnen wir denselben durch cos(n'+ On’) und setzen den so gefun-
cos 6/— cos 6” cos 6?

2 ; in die Gleichune (1.), so geht dieselbe über in
sin 6// sin o// 8 ( ); 8

denen Werth von

ins! À
cosn! — cos(n + On!) + a { cos!” ds! + cos n!.ds!"! — cs'}

sins’ sins!!!
cosuw/ cos u!!

DT ME GI [cos (a! — w!!) — cos w'].

Nun ist cos (n’ + On!) = cosn'— sinn’.On. Wir finden daher

(a.) OW = como cosn!'Os"!!— Os'} +

sins’ sins! sin n!

 

cosu! cosu!! . (w+a'— w!'
——  —— :OLsin| ——- .
N 2

Setzt man sins”sins’’sinn —=M, so ergeben sich die Reductionen der drei Winkel auf die

Sphäre wie folgt:

sin s! | À
On! == ee cosmos" 8) + Gk,
" sin s’! rs in a a) ptt
(6.) on! = ar (cos s'’ + cosn!" ds! — ds" + ok",
sin si"! ih et I gel 1 Arm
On = (cos .os' + cosn!. os! —Os!"} +- ORM".

Die Winkel des sphärischen Dreiecks, welches sich auf eine Kugel vom Radius (1.)
oder auch vom Radius a — der halben grossen Axe des Sphäroids bezieht, sind daher
n+ On!, n+ On", nl" + Onl".

Es bleibt jetzt nur noch die Bestimmung der Gréssen o', 0", o'

éco! fl brie... die
aber leicht nach den bekannten Besselschen Formeln und Hülfstafeln gefunden werden können

Da die Besselschen Tafeln aber nur bis zu Entfernungen von 14 Grad im Erdboden
ausreichen, bei der mitteleuropäischen Gradmessung aber wohl doppelt so grosse Bögen vor-
kommen, so füge ich noch meine Formeln hinzu, die unter Umständen wenigstens zur Con-
trole dienen können. Entwickelt sind dieselben in: Messen auf der sphäroidischen Erdober-
fläche, 8.14. und 8.15. In 8.14. haben sich aber einige Fehler in den Zeichen einge-

schlichen, die ich in No. 1425 der Astron. Nachrichten verbessert habe. Man erhält hiernach

für die halbe grosse Axe — 1 und für jede Entfernung gültig:
7 s' = Alo! —(B'+ C'sin’u"!-+ D'sin*u!’...)sinu" cosu!! cosa’
a — (B+ C'sin®u! + D'sin*u! ...) sinu! cosu! cosa!

    

me hie

BR 77]

EL

1 |

ju bee Les au Pb Ba LA da le mann

FEIERN

 
