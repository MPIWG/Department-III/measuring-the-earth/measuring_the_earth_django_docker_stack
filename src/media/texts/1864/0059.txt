rn eee.

I PTT ATP re |

Jimi ini

a

em I hihi

su a

gegebenen Ausdruck, noch nicht genau genug bestimmt ist; weil, wie die nachfolgende Unter-

suchung zeigt, ungeachtet der Kleinheit des Winkels, doch die mit (ee)” behafteten Glieder
noch einen bemerkbaren Einfluss erlangen.

Um jede Unsicherheit über die erwähnte Azimuthal-Differenz zu ee. werde
ich dieselbe ohne Einschränkung zu bestimmen suchen.

Zieht man in meiner Schrift: das Messen auf der sphäroidischen Erdoberfläche, die
Gleichung für cote’ auf Seite 64 von der Gleichung (77.) auf Seite 62 ab, so erhält man

 

 

 

 

cosul teu ‘ ee cos u’ (sinu' — sinu
cotaD cor a) — : as sin y’ cote 4 + vo 2 (1— eecos’w) ?
sin w COS 4 sin
cos u'tou ‘
= a sin 4! cotg ©
sin ® 7 5

nun Ist

1.960
D rate ! 2 3
(1— eecos’ u) = 1+ keecos’u = = (ce) COS U + ——— - Te (ee)’cos’uw-+ --
m
Man erhält daher

 

 

 

 

 

1 ee cosu’ (sin W — sin u)
à Aral an ’ ga ! pes ee
cotg v'— cotg a’ = cosu tgu(——— N sin #' (cotg 4 — cotg w) + nn
dae
au, ; ene
—— )sın
1 1 … 2eos( 5 5
sinw sino sin & Sin 4
sin (@ — 4%)
COS D — coig@ = ue
Sin © Sin 40

 

 

: wtur. ful—u4u
sinu!—sinu = 2cos( a sin ( 5 )

sin (@'— v)

cote®' — cotg a’ = — -
5 5 sin e’sinv’

Vertauscht man sin(a'—v) mit dem Bogen, was stets erlaubt ist, und setzt o =w-—+@ so
findet man

A sine’ sinv! {oo Be RS
a gi a 1 2 EL

sın(w- =)
: wW-uN à: (W—u 2
+ 2eecosu cos ( 5 )sin( 5 )+eecos w{ |

sin # sin 1”

 

 

 

+5 (ee) costa E 14]

ee cosu'

sr (sinw’— sin).

8 *

| ] = cosw’ tgu— sinw’ cosw-+

 
