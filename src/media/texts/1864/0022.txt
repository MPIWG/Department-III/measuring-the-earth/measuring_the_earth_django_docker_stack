 

ae.

Bericht des Director Schiaparelli.
Plenar-Sitzung vom 18. October 1864.

Herr Schiaparelli giebt einen kurzen Bericht über die in Italien schon ausgeführten
astronomischen Operationen. Diese bestehen hauptsächlich: 1° in’ dem Bogen des mittleren
Parallels, welcher sich zwischen den Savoyischen Grenzen und Fiume in Istrien ausdehnt,
und westwärts sich bis Bordeaux erstreckt, östlich aber mit geringer Schwierigkeit bis zum
Schwarzen Meere verlängert werden könnte. Die Dreieckskette ist sehr gut; aber die astro-
nomischen Operationen, welche vor 40 Jahren von Plana und Oarlini ausgeführt worden sind,
zeigen sehr beträchtliche Abweichungen von den geodätischen. Ein Theil dieser Abweichun-
gen kann dadurch erklärt werden, dass zu jener Zeit der Einfluss der persönlichen Gleichung
auf die Durchgangsbeobachtungen noch nicht genug bekannt war; ein anderer Theil kann
nur durch die Local-Ablenkungen eine hinreichende Erklärung finden. 2° in drei Meridian-
bogen, welche den oben genannten Parallel durchschneiden. Der erste ist der berühmte Grad
von Beccaria, wo bei einer Ausdehnung von 68 Breitenminuten eine Discordanz von 48”
zwischen der astronomischen und der geodätischen Amplitude stattfindet. Diese Meridiankette
ist von den piemontesischen Geodäten mit grosser Schärfe wieder gemessen, und die Breiten
von Carlini bestimmt worden. Eine zweite Meridianlinie durch Genua und Mailand bis
Zürich ist von Oarlini berechnet worden, konnte aber keine bestimmte Resultate liefern, weil
die Züricher Breite zu jener Zeit nicht hinreichend genau bekannt war. Die dritte Kette
ist von Marieni berechnet, und grösstentheils auch von ihm gemessen worden: sie geht durch
Rom, Rimini, Padua und S. Salvatore, und hat eine Ausdehnung von 4 Graden. Sie ist
nichts anderes, als die alte Operation von Lemaire und Boscovich, welche neu gemessen,
und ungefähr ums Doppelte erweitert worden ist.

Aus allen diesen geodätisch-astronomischen Bestimmungen geht als unbezweifelte
Thatsache hervor, dass Ober-Italien wegen des höchst unregelmässigen Ganges der Lothlinie
fast keinen Beitrag zur Bestimmung der allgemeinen Krümmung der Erdoberfläche in Mittel-
Europa liefern kann; dass vielmehr die Wellen dieser Oberfläche hier so stark und so wenig
ausgedelint sind, dass sie nur durch eine grosse Menge astronomischer Bestimmungen genau
untersucht werden können, Herr Schiaparelli führt mehrere Beispiele aus der Parallelgrad-
messung von Plana und Carlini, und aus Marieni's trigonometrischen Vermessungen an, welche
‚beweisen, dass, wenn das Studium der Störungen der Lothlinie für das Fortschreiten der
Geodäsie einigen Nutzen haben kann, dasselbe in Ober-Italien vielleicht den geeignetsten
Ort finden dürfte.

Was die vorhandenen astronomischen Punkte betrifft, so findet man in ganz Italien
ausserhalb der Sternwarten nur wenige; auch sind alle diese vielleicht nicht mit der jetzt
erforderlichen Schärfe bestimmt, so dass möglicherweise Alles von vorneherein anzufangen
ist. Da aber nach dem Gesagten die Anzahl dieser Punkte, besonders im nördlichen Theile
des Landes eine sehr grosse sein müsste, so erscheint es von Wichtigkeit, die Punkte nicht

zufällig, sondern in der Weise zu wählen, welche dem gegenwärtigen Zwecke am meisten

 

et

a
x
x

el 1

EL

to ind |

dc vba ds A hs dh a Ya)

 
