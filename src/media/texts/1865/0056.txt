 

16. Wilhelmsthurm, Postament auf der Plateform des Thurmes.

R log. L LY corr. R!
oe. Tr Ei
Süntel BOX 0 4,1958570 15698,46 — 0,994
Wittekindstein 69:10: 8,920 4,1926140 15581,67 — 1,245
Rahden 140.497 28,318 4,3264951 21207,78 — 0,251

17. Wittekindstein, markirte Mitte auf der Plateforme des Thurmes.

Köterberg O10): 0 4,4318864 27032,51 + 0,868
Hünenburg 82 40 6,770 4,2958044 19738,06 + 0,177
Nonnenstein 128 18 47,066 4,1594004 14434,46 — 0,400
Rahden 142 26 56,862 4,1588381 14415,78 0,202
Wilhelmsthurm 262 20 34,915 4,1926 140 15581,67 — 1,061
Süntel 31849. 14,475 4,2484079 EA 2 +0,210

D LB exlze11 en.

Am 1. April ist die betrübende Nachricht hier eingetroffen, dass der Königlich Bel-
gische Oberst im Generalstabe, Chef der Vermessungen des Dépôt de la Guerre, Herr
Diedenhoven, am 29. März in dem rüstigen Alter von 56 Jahren, einer längeren Krankheit,
von der er zum Theil wieder genesen war, endlich doch erlegen ist.

Die mitteleuropäische Gradmessung hat in ihm einen ausgezeichneten Geodäten und

einen ihrer eifrigsten Mitarbeiter, der Unterzeichnete aber einen hochgeachteten lieben
Freund verloren.

3aever.

1 Spahıem

Da das Unternehmen der mitteleuropäischen Gradmessung ursprünglich auf das Areal
zwischen den Meridianen von Brüssel und Warschau beschränkt werden sollte, so war bisher
an die Königl. Spanische Regierung keine direkte Aufforderung zur Betheiligung an der in-
ternationalen Organisation gerichtet worden.

Es kann jedoch nur mit der grössten Freude begrüsst werden, dass die Königl. Spa-
nische Regierung in dem lebendigen Gedeihen und der vermehrten Tragweite des organisa-
torischen Zusammenwirkens innerhalb der mitteleuropäischen Gradmessung eine Aufforderung
erkannt hat, sich dieser umfassenden Gemeinschaft anzuschliessen, wodurch wiederum ein

wichtiger Schritt zu der Vereinigung aller europäischen Interessen dieser Art gethan
worden ist.

Se. Excellenz der Herr Ministerpräsident des Königreichs Spanien hatte für diesen Zweck

zu der diesjährigen Versammlung der permanenten Commission in Neuenburg den Herrn

 

È
>
>
=

1 1181

ut nich a, u, Hebitadu a mn

 
