 

Wa

schen Wahrnehmung derjenigen Geschäfte beauftragt werden, welche dem vorgelegten
Plan gemäss dem Central-Büreau zufallen würden.

Baden-Baden den 30sten August 1865.
gez. ‚Wilhelm

für den Finanzminister und für den Minister
geg. z. v. Roon, v. Itzenplitz, der geistlichen etc. Angelegenheiten
Gratz. Dulenburg.
An
die Minister des Krieges, für Handel etc.
und der geistlichen etc. Angelegenheiten.

Ich habe diese Allerhöchste Cabinetsordre mit lebhafter Freude begrüsst, weil sie
mir, was ich seit 1862 vergebens erstrebte, wissenschaftliche Kräfte zu Hülfe giebt, und weil
meine Thätigkeit unter dem neuen Chef sich einer wissenschaftlichen Würdigung und bereit-
willigen Unterstützung zu erfreuen hat. Es sind auch bereits die wichtigsten Vorkehrungen
so getroffen, dass das Centralbüreau seine Thätigkeit beginnen kann so wie der Geldetat be-
willigt ist. — Zwei 10zöllige Universalinstrumente habe ich schon zu Anfange des vorigen
Jahres bei den Herren Pistor und Martins bestellt, die im April fertig werden. Ferner ist
durch die Vorsorge Sr. Excellenz des Herrn Cultusministers der Professor Sadebeck in
Breslau von Seiten des dortigen Magistrats vom 1. April ab auf ein Jahr beurlaubt worden,
um bei der Organisation des Centralbüreaus thätig zu sein und um im Laufe des Sommers die
Beobachtungen mit einem Universalinstrument zu übernehmen, während der Hr. Dr. Bremiker
voraussichtlich die Beobachtungen mit dem anderen wird übernehmen können. Auf diese
Weise kann ich mich mit Zuversicht der Hoffnung hingeben, bald in den Stand gesetzt zu wer-
den, wieder Triangulationen auf der Höhe der Wissenschaft ausführen und auch die dringend

nothwendigen Maassvergleichungen bald vornehmen zu können.

I. Ueber die im Jahr 1865 ausgeführten Arbeiten.

Es ist neuerdings in dem Selbstverlage des Büreaus der militärischen Landestrian-
gulation ein Werk unter dem Titel: Die Königlich Preussische Landestriangulation. —
Hauptdreiecke. — Erster Theil, Hauptdreiecke in der Provinz Preussen — Berlin, 1866. er-
schienen. Allein ich muss die Verantwortlichkeit für diese Arbeit ablehnen, indem ich zwar
durch die Königlich Preussische Allerhöchste Cabinetsordre vom 10. Juli 1863 mit der Ober-
leitung. der militärischen Haupttriangulation beauftragt worden war, dieser Befehl aber bis
jetzt noch nicht zur Ausführung gekommen ist, ich im Gegentheil über die N utzbarmachung
jener Arbeit für die mitteleuropäische Gradmessung Bedenken hege, die, eben so wie der
auf Seite 196 der gedachten Schrift gegen die Besselsche Gradmessung ausgesprochene Tadel,

durch die permanente Commission in ihrer bevorstehenden Zusammenkunft in Neuenburg

ihre Erledigung finden werden.

 

ial

I dm ins, Allin nn ma)

à: tata meinen Su |

 
