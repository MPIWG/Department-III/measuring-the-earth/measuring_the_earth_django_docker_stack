HS

SPP em |

(Pa

ar TT TRA VT TEE ATTY RTT TTT

a

Eine Grundlinie ist nicht gemessen worden. Von den sechs Preussischen Anschluss-
seiten ist das arithmetische Mittel der logarithmischen Differenzen = + 10 in der siebenten
Decimalstelle. Dieser Betrag wurde dem Log. der Preussischen Anschlussseite Ruhn-Hoebeck
hinzugefügt, und damit sind die Entfernungen in dem nachstehenden Verzeichniss der Polar-

Coordinaten berechnet worden.

Zusammenstellzne

der Resultate der trigonometrischen Messungen I. Ordnung in den Grossher-

zogthümern Mecklenburg-Schwerin und Strelitz.

Station: Lüneburg.

Michaelis-Thurm, Verticale der Helmstange.

 

Objecte. | Grad. Min, | Bee. nee ir,
Hohenhorn à 0 0 0,0000 | 4,1133186,1
Lauenburg, Mecklb. ZN 41 27 41,1124 | 3,9615631,7
Granzin A D 9 | 91,4500 | 4,2755886,0

Station: Lauenburg. Mecklb. A

 

 

Lüneburg à 0 0 0,0000 | 3,9615631,7
Hohenhorn & 93 | 49 | 44,1410 | 3,9352222,6
Granzin A 209 | 19! 49,6918 | 4,0145941,5
Glienitz A 20 7 | 51,8980 I 4,1948072,6
Breetze A 289 | 25 | 46,8362 | 3,9014234,3
Lauenburg, Dän. A 282 01.20 9,78888

Station: Breetze.

Bleckede & 0 1 0,0000

 

Karenz A 57 | 10 | 54,7674 | 4,3568405,6
Glienitz À 121) 88 6,8350 | 3,9279909,8
Lüneburg & 221 | 49 2,1715 | 3,9972874,3
Lauenburg, Mecklb. A 288 6 | 41,5236 | 3,9014234,3

 

Granzin À 346 | 49 | 27,3109 | 4,0763280,3

Die Identität des Mecklenburgischen mit dem Hannöverischen Dreieckspunkt zu Breetze Jässt sich nicht sicher
constatiren. — Das Zeichen A bedeutet Signal.

 

 
