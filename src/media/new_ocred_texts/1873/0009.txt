nn nn

1 PPP PT Ogr een To

Im

werner

i
23
a
A
=

5
=

[3
iR

:

È

è

i

i i ne

d) Ludwigslust—Grabow—Warnow (Preuss. Nivellementsbolzen)
so dass nunmehr die früheren trigonometrischen Höhenbestimmungen nahe zu zwei
Drittheilen der gewünschten Controle durch geometrische Nivellements unterzogen sind.
Leider verbietet die voraussichtlich nur noch kurze Dauer ihrer Thätigkeit
der Commission, wie überhaupt die Ausführung irgend welcher praktischen Arbeiten,

so auch die Fortsetzung des Nivellements über das ganze Land.

Grossherzogliche Landes-Vermessungs-Commission.

Köhler. Kundt.

910 eisot) eormeeli erh.

Bericht über die im Jahre 1873 für Zwecke der europäischen Gradmessung

ausgeführten Arbeiten.

I AufGrund eines im Jahre 1868 zwischen den italienischen und österreichischen
Gradmessungs-Commissären getroffenen Uebereinkommens wurde heuer die Verbindung
der österreichischer Seits im Jahre 1869 gemessenen Dreiecke in Albanien mit jenen
auf terra d’Otranto in Italien ausgeführt und zugleich die Dreieckskette bis Corfu
verlängert.

Die Einleitungen so wie der Vorgang zur Ausführung dieser Arbeit wurden
gemeinschaftlich von Herrn General de Vecchi, Oberstlieutenant Chiö und mir festgestellt
und als Uebungsstationen Leece, Pagliore, Fanö und Saseno gewählt. Für jede Station
wurde ein Beobachter bestimmt, so dass die Messungen zu gleicher Zeit stattfinden
konnten. Die ersten zwei Punkte wurden den italienischen, die letzten zwei den öster-
reichischen Beobachtern überlassen.

Italien hatte hiezu die zwei Generalstabshauptleute Ottavio Almici und Gio-
vanni Maggia, Oesterreich den Hauptmann Robert von Sterneck und Oberstlieutenant
Johann Steffan bestimmt.

Bezüglich der Ausführung der Beobachtungen wurde beschlossen, dass jede
Richtung mindestens 60mal, gleich vertheilt auf die beiden Kreislagen, gemessen wer-
den müsse und dass die Messungen womöglich des Morgends und Abends stattfinden sollen.

Ferner wurde die Messung von gleichzeitigen Zenithdistanzen auf allen Linien
an einem Tage u. z. von früh 5'/, Uhr an in Intervallen von einer halben Stunde in
Aussicht genommen.

Am 15. Mai begannen die Beobachtungen auf allen Punkten, die aber durch
ungünstiges Wetter unterbrochen wurden. Nach einigen Tagen machte man auch noch
die Erfahrung, dass die Sicht Lecee—Fanö nicht bestehe und dieselbe nur durch eine
Erhöhung der Standpunkte um mindestens 20 metres zu erreichen wäre. Die Com-

mission beschloss daher in das Verbindungsnetz einen fünften Punkt, nemlich Serrano

 
