 

|
|

A SR PR A pe wer

nn a 5
» me

Re ER SES NOTE

—

Fee

SEELE

SE

as ee

a cs

 

4

RE

Chaussee über Wr.-Neustadt, dem nördlichen Endpunkte der dortigen Grund-
linie bis zum südlichen, dann von Wr.-Neustadt nach Neudörfel doppelt.
b) Von Adelsberg über Laibach nach Saule, dann von Gonobitz über Windisch-
Feistritz, Pragerhof, Kranichsfeld nach Schleinitz doppelt mit einem Seiten-
nivellement zu den Endpunkten der dortigen Grundlinie.
Auf diesen nivellirten Strecken befinden sich 43 Höhenmarken und 215 andere
in das Nivellement einbezogene Punkte.
Von den drei Parthien war eine dreieinhalb, die zweite drei und die dritte zwei
Monate thätig.
Im nächsten Sommer werden fünf Parthien jede sechs Monate zum Nivellement
verwendet werden.
Wien, im Februar 1874. Ganahl, Oberst.
Dobner.

Bericht über die in Oesterreich ausgeführten astronomischen Gradmessungs-
arbeiten im Jahre 1873.

1. Bericht abgestattet vom Regierungsrathe Professor Theodor Ritter v. Oppolzer.

Die Gradmessungsarbeiten in Oesterreich-Ungarn sind mit diesem Jahre ihrer
Durchfiihrung um ein Wesentliches näher gerückt.

Se. Excellenz der Unterrichtsminister v. Stremayr, bestrebt die Gradmessungs-
arbeiten in Oesterreich möglichst zu fördern, hat auf meinen Vorschlag für die Bestim-
mung der astronomischen Punkte erster Ordnung für das Jahr 1873 die Summe von
27,000 FL in das Budget eingestellt und für die Bewilligung dieses Betrages die aller-
höchste kaiserliche Sanction und die Zustimmung der beiden Häuser erlangt; für die
nachfolgenden Jahre werden 17,000 Fl. jährlich eingestellt werden. Da dieser bewilligte
Betrag ausschliesslich auf die Bestimmung der Punkte erster Ordnung verwendet
werden soll, so ist in der That ein rasches Vorschreiten der astronomischen Gradmessungs-
arbeiten in Oesterreich in den nächsten Jahren zu erwarten.

Das erste Jahr, welches wegen Anschaffung von Instrumenten um 10,000 Fl.
reicher dotirt war, konnte nicht völlig ausgenützt werden, indem die Beischaffung der
nöthigen Instrumente von Seite der Mechaniker nicht rechtzeitig erlangt werden konnte
und ausserdem die hinlängliche Zahl der Beobachter nicht vorhanden war. Um letz-
terem Mangel für die Zukunft abzuhelfen, habe ich mit Bewilligung eines hohen Mini-
steriums ein Gradmessungsbureau im Monate November 1873 zusammengestellt, in
welchem unter meiner unmittelbaren Aufsicht und Mitwirkung die Reductionsarbeiten
durchgeführt werden und die Ausbildung der Observatoren erstrebt wird.

In diesem Bureau sind ausser mir beschäftigt als Observator Herr Ferdinand
Anton, als Assistenten die Herren Lieutenant Alois Nahlik, Robert Schram und Ludwig

 

4
3
i
i

i ll as nn

shout unie
