 

Ni
a
i

rt

eo ee oe.

 

 

 

 

 

 

 

 

AR. 18700 Epoche der/Anzahl der Ge- Decl. 1870.0 Epoche de r Anzahl der| Ge-
Catalog. NT. (ohne Beobach- | Beobach- | \icht (ohne Beobach- | Beobach- nt
Eigenbewegung). tungen. | tungen. Eigenbewegung). | tun gen. | tungen.
38. Carrington 3621.
Bradley 2 . ; 3.488 . [814 23h 27m 49:07 | 1751.0 4 3/, [+ 86° 35’ 24”8 | 1751.0 7 it
Groombridge . . . . {4101 45.11 | 1807.7 6 oe 23.8} 1807.7 6 Ms
BEE. ae b BOF 44.98 | 1815.2 8 Er a _ — ey
Memwerd s ., „1,70: .11864 46.82 | 1827.5 6 X 24.5| 1827.5 6 1
Greenwich 12 Y. . .. . |2112 48.50 | 1839.5 5 L — _ — be
Greenwich 12 Y. 3°.) . (2112 47,79 | 1842.9 5 1 208) 18425 D 1
Bayelited.. rar. [0129 48.47 | 1850.0 38 Ly, 24.8| 1849.5 al 1t/,
Greenwich 6 Y. . ...|1533 48.97 | 1851.1 me lt 24.0] 1851.6 10H,
Camineton . 1. 3601 49.04 | 1854.7 21.12 254| 18547 Di eas
Greenwich 7 Y. . . .11971 49.58 | 1860.9 1 25 24.1] 1860.8 2 8],
Washington . . . ,Jıoaa 49.48 | 1863.2 19 2 25.1) 18642 13 11},
Radcliffe II. . NDS 48.66 | 1858.2 6 1 23.1| 1857.6 4 3],
Radcliffe 1862 — 1864 | — 51.31 | 1863.6 2 Ay 25.1} 1863.6 16 qu
Greenwich N.7Y. . . [2698 49.66 | 1863.5 14 27, 24.6 | 1863.5 14 11},
Radcliffe 1865—1867 .| — 48.55 | 1865.8 1 uh 24.8} 1865.6 2 ip
Greenwich 1868 . ; ar 50.62 | 1868.3 2 = 25.2| 1868.3 2 ie
Radcliffe 1868 — 1870 .| — 49,76 | 1870.3 1 Ns 25.4| 1869.8 4 Bi
99. Carrington 3693.

Bradley . . .°.°0.: .182941 23h 53m 93598 | 1751.0 2 ie — — —
Groombridge . . .' . 14193 25.46 | 1807.9 5 3/, 1+ 85° 58'58”6 | 1807.9 6 oe
SAVE à AINE Oa 25.60 | 1815.2 3 IM — — — en
Sohwerd:. . 2 min), 11888 2591 | 18219 | 2 op 12 | 10210 | a ab
Radeltiel.. . .-. 2. 16253 27.14 | 1848.7 22 ty 58.1 |, 18189 18 11
Greenwich 6 Y. . . .11557 27.38 | 1850.9 8 Lh 57.5| 1851.1 10 A1
Carrington. . ..... . 13693 27.51 | 1854.4 4 1 59.3| 1854.4 4 1
Greenwich 7 Y. . . .12005 26.79 | 1860.3 | 2 Un 57.0 | 1860.3 3 a
Washington . ...... - 110597 27.60 | 1862.0 11 Li D7.1.) 1856.1 4 1
RouchielIL |: 7." .10378 26.76 | 1858.3 M PL 56.8 | 1859.6 Re
Radcliffe 1862—1864 .| — — — = = 58.8| 1863.6 7 1
Radcliffe 1865—1867 .| — | — — = — Did. 18663 4 DR
Radcliffe 1868—1870 .| — | 9937.1 1870.3 2 1}, | 58.3 | 1870.0 3 37,

 

 

 

 

 

Die 2 eingeklammerten Rectascensionen: Stern No. 12 Groombridge und
Stern No. 14 Groombridge sind bei Ableitung der definitiven Positionen und Eigen-
bewegungen ausgeschlossen worden, weil dieselben um resp. 3°%,7 und 555 von den
definitiven Ergebnissen abweichen. Ein Gleiches ist bezüglich folgender Declinationen
geschehen: Stern No. 2 Radcliffe 1862—1864, Stern No. 13 Radcliffe 1865—1867,
Stern No. 14 Radcliffe 1862—1864 und Stern No. 23 Berlin, welche um resp.
3”.0, 2”.7, 3".3 und 2”.0 differiren. Hingegen wurden die beiden um resp. + 253 und
— 159 abweichenden Rectascensionen: Stern No. 23 Radcliffe 1865—1867 und
Radcliffe 1868—1870 nicht ausgeschlossen, weil sich diese Abweichungen nahezu
compensiren und des geringen Gewichts der betreffenden Positionen w egen auch nur
wenig auf die Eigenbewegung influiren.

Es galt nun auf Grundlage der in der obigen Tabelle gegebenen Daten die
wahrscheinlichsten Werthe der Positionen für 1870.0 und der Eigenbewegungen her-
zuleiten. Da jede Catalogposition eine Gleichung von der Form:

A—LT—n.e

liefert, in welcher a die Abweichung der Catalogposition von dem zu Grunde gelegten

Näherungswerthe, + die gesuchte Verbesserung der Rectascension oder der Declination

i
i
i

 

ll os se

a hall du Ma Aa ad

 
