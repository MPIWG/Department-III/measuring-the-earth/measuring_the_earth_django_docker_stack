 

;
|
i

SR ee Se See OR EE. IE SA e

SS SSS TRS eT a

NS SDE EE ERTEILT ETIKETTE IS

|
4

t
i
i
i
j
{

coe ae
8: “Maeiek ben ba rig.

Bericht der Mecklenburgischen Landes-Vermessungs-Commission über die 1873
ausgeführten Arbeiten und Berechnungen.

Bevor die Commission über ihre Thätigkeit im Laufe des Jahres 1873 berichtet,
hat dieselbe mit tiefstem Bedauern eines Umstandes zu gedenken, der den Fortgang der
Arbeiten wesentlich verzögerte.

Am 24. August a. p. starb der Geheime Canzleirath Paschen. Seit Errichtung
der Commission im Jahre 1853 Mitglied derselben und speciell mit der Leitung der
astronomischen und geodätischen Rechnungen betraut, hat er sich dieser Thätigkeit mit
unermüdlichem Fleisse und treuester Hingabe gewidmet. Ebenfalls seit Bestand der
europäischen Grad-Messungs-Commission Bevollmächtigter Mecklenburgs zu derselben,
beseelte ihn der lebhafteste wärmste Eifer für alle Interessen auch dieses Instituts.
Schon lange sehr leidend, war er dennoch bemüht, sich neben seiner sonstigen Berufs-
thätigkeit, in letzter Zeit speciell den Vorarbeiten für die photographische Beobachtung
des Venusdurchganges, mit denen er als Mitglied der bestellten Reichscommission betraut
war, nicht zu entziehen und schied so mit seinem Tode aus der Mitte seines vollen

ersteren Wirkens und Schaffens.

Es wird sich an anderer Stelle der Ort finden, den Verdiensten des Verstor- -

benen um die Mecklenburgische Landes-Vermessung in vollerem Maasse gerecht zu wer-
den; es möge uns aber auch hier gestattet sein, auszusprechen, dass, wie gewiss sein
Dahinscheiden in den weitesten Kreisen beklagt wird, dasselbe für die Mecklenburgische
Landes-Vermessung einen unersetzlichen Verlust gebracht hat.

Die letzte Krankheit und der Tod des Geheimen Canzleirath Paschen brachten

die von ihm geleiteten Vorarbeiten zur Publikation der trigonometrischen Messungen

und der astronomischen Bestimmungen, welche pro 1873 die Hauptthätigkeit des Bü- |

reaus bildeten, ins Stocken, so dass von einer produetiven Thätigkeit des Büreaus nach
dieser Seite hin nichts zu berichten ist; die Vorarbeiten sind inzwischen wieder auf-
genommen und werden voraussichtlich zu Ende des Jahres soweit gefördert sein, dass
das Werk dem Druck übergeben werden kann.

‘An sonstigen Büreauarbeiten sind nur ausgeführt:

Die Berechnungen der geometrischen Nivellements des Jahres 1872 nach bekannter
unveränderter Vorschrift.

An Messungen sind ausgeführt:
die geometrischen Nivellements

a) Schutow—Rostock— Lage — Güstrow—J ehna— Dobbertin— Goldberg— Lübz—

Marnitz (Ruhneberg).
b) Giistrow—Sternberg—Briiel—Schwerin.

c) Grevesmühlen— Wismar.

 

j
j
4
i
3
i
{
j

al Ak lh a le

 
