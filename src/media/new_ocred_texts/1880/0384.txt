 

16

Hartmann, J. G. F: Ueber die Correct. der mit dem doppelt repetirenden Theodolith

Haupt.

gemess. Winkel u. s. w. (A. N. VIL, 157.)
Ueber die Correct. der gemess. Horizontalwinkel wegen excentrischer Auf-
stellung des Instrumentes. (Ebenda.)
Ueber die Ausgleichung des Einflusses der Excentrieität bei getheilten
Kreisen. (Ebenda.)
Ueber die Ausgleichung des Fehlers in gemessenen Horizontalwinkeln.
(Ebenda.)
Ueber die Theilungen der Instrumente von Ertel und Hohnbaum. (A. N. 235.)
Arbeitete von 1823—33 unter Gauss an der hannöverschen Gradmessung.
Ueber die Ablenkung des Lothes in der Höhe und den dadurch herbeigeführten
Fehler geometrischer Nivellements. (A. N. 1996.)

Heine, H. E. Theorie der Anziehung eines Ellipsoids. (Crelle’s Journal XLII, 1851.)
Helmert, F. R. Studien über rationelle Vermessungen im Gebiete der höheren Geo-

däsie. Inaugur.-Diss. Mit einer Figurentafel. Leipzig, 1868.

Die Ausgleichungsrechnung nach der Methode der kleinsten Quadrate mit
Anwendungen auf die Geodäsie und die Theorie der Mess-Instrumente.
Leipzig, 1872.

Ueber den Maximalfehbler einer Beobachtung. (Z. f. V. VI, p. 131.)

Bestimmung des mittl. Fehlers der Längenmessungen aus den Dilterenzen
von Doppelmessungen. (A. N. 1924.)

Beiträge zur Theorie der Ausgleichung trigonom. Messungen. (Zeitschr. für
Mathem. u. Phys. XIV, pp. 174—208.)

Theorie der Libellenachse: (Z. £ V. VII p. 185.)

Das Theorem von Clairaut. (Z. f. V. VII, p. 120.)

Ueber die Formeln für den Durchschnittsfehler. (A. N. 2039.)

Ueber die Berechnung des währscheinlichen Fehlers aus einer endlichen An-
zahl wahrer Beobachtungsfehler. (Schlömilch’s Zeitschr. XX, p. 300.)

Goniometrische Formeln. (Z. £. V. 1876, p. 297 u. 1877, p. 32.)

Ueber die Wahrscheinlichkeit der Potenzsummen der Beobachtungsfehler.
(Schlömilch’s Zeitsch. XXI, p. 192.)

Die Genauigkeit der Formel von Peters zur Berechn. des wahrscheinlichen
Beobachtungsfehlers direeter Beobachtungen gleicher Genauigkeit. (A. N.
2096. u. 97.)

Untersuchungen über den Einfluss eines regelmässigen Fehlers im Gang der
Ocularröhre des Visirfernrohrs auf Messungen, insbesondere auf das geometr.
Nivellement. (Zeitschr. des Architecten- und Ingenieur-Vereins in Han-
nover, XXII, 417.)

Einfache Ableitung Gaussischer Formeln für die Auflösung einer Hauptaufgabe
der sphärischen Geodäsie. (Z. f. V. IV, p. 153).

Zur Untersuchung der Nivellir-Fernröhre. (Z. £. V. V, p. 34.)

hr ad a Mkt A al ae ah a a

 

i
i
|
i
i
j

 
