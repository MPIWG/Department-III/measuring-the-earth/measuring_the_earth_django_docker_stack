SI On TRETEN

ITEM LUN

HT

TRENNT PT

N
sa

Vorwort.

N achdem mir in der i. J. 1879 in Genf abgehaltenen Conferenz der perma-
nenten Commission die „Vervollständigung der Literatur der Gradmessungs-
Arbeiten“ übertragen worden war, habe ich diesen Auftrag in der Weise aus-
geführt, dass ich die vom Centralbüreau i. J. 1876 herausgegebene Schrift
„Zusammenstellung der Literatur der Gradmessungsarbeiten“ zu Grunde ge-
legt habe. |

In Betreff der Methode der kleinsten Quadrate habe ich geglaubt, mich
mit einem Hinweise auf Merriman’s Arbeit ,A List of writings relating to
the method of least squares (8S. p. 4) begnügen zu können, da dieselbe nicht
nur höchst vollständig ist, sondern auch die Leistungen der einzelnen Autoren
kritisch beleuchtet. Ich habe nur einige wenige Schriften, welche mir von
hervorragender Bedeutung erschienen und in denen hauptsächlich die Geodäsie
berücksichtigt wird, ausserdem aber neuere Arbeiten, welche in Merriman’s.
Zusammenstellung noch nicht enthalten sein konnten, aufgenommen.

Aehnliches gilt auch von den Schriften über barometrische und physi-
kalische Höhenmessung, für welche M. F. Kunze ein sehr vollständiges Literatur-
verzeichniss in Jordan’s Zeitschrift für Vermessungswesen (Band VIII, Ergän-
zungsheft I) geliefert hat.

Gleichwohl würde es mir kaum möglich 'gewesen sein, in der kurzen
Zeit von wenigen Monaten mich des erhaltenen Auftrages zu entledigen, wenn
mir nicht gründliche Vorarbeiten zu Gebote gestanden hätten. Ich erwähne
hauptsächlich: Ä

1) Poggendorff, J. C., Biographisch-literarisches Handwörterbuch zur Ge-

schichte der exacten Wissenschaften u. s. w. 2 Bände. Leipzig 1863.

 

 
