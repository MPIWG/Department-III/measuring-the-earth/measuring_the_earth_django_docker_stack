an ae FRI. TT ITNT ni 01 |

ME €

pre

pet

= à | smith Anka hl: BR A hd hide 1h
RQ FHT tit

35

 

Fünfte Sitzung.

München, den 16. September 1880.

Anwesend dieselben Herren wie in der vorigen Sitzung.

Anfang der Sitzung 3 Uhr.

Herr Faye, da er gehört habe, dass Herr Siemens einige Methoden angegeben
hat, um bei Messungen möglichst genau die Temperaturen bestimmen und in Rechnung
ziehen zu können, bittet Herrn Siemens, darüber einige Aufklärung geben zu wollen.

Herr Siemens erwidert, dass er schon vor mehreren Jahren, um die Temperatur
einer Messstange genau zu ermitteln, vorgeschlagen habe, statt einer massiven Stange
eine hohle röhrenförmige zu nehmen, welche an beiden Enden verschlossen ist, die aber
mit Quecksilber gefüllt wird. Wenn mit der Messstange eine kleine senkrechte Röhre
in Verbindung steht, so wird je nach der Temperatur das Quecksilber mehr oder weni-
ger hoch in dieser Röhre steigen können. Durch Ablesung einer Theilung an der senk-
rechten Röhre, die z. B. aus Glas sein könnte, hätte man sofort die Temperatur, da
der Apparat ein aus Eisen und Quecksilber bestehendes Metallthermometer ist. Bei
der vorjährigen Gewerbeausstellung in Berlin hat auch Jemand eine solche Messstange
ausgestellt, er wisse aber nicht, wie sich dieselbe bewährt habe, Eine andere Tempe-
raturbestimmung könne man erlangen durch Anwendung der Elektrieität, indem man
Thermoelemente aus Tellur und einer Legirung von Zink und Antimon anwende, welche
so energisch wirken, dass dieselben nur Dimensionen von 2—3 mm zu haben brauchten.
Wenn man in die Messstange in verschiedenen Entfernungen kleine Löcher einbohrte
und in denselben solche Thermoelemente anbrächte, würde man mit sehr grosser Ge-
nauigkeit die relative Temperatur der Messstange étaient

Herr Bruhns fragt Herrn Siemens, ob nicht sein Bruder vor einigen Jahren
einen Apparat angegeben habe, um die Intensität der Schwere auf Schiffen, also auf
dem Meere, zu bestimmen.

Herr Siemens bestätigt dies und wird seinen Bruder Wilkam Siemens veran-
lassen, dass derselbe eine Beschreibung dieses Apparates an die soeben gewählte Com-
mission einsende. |

Herr Bruhns bittet noch Herrn von Jolly, kurze Auskunft zu geben über die
von ihm angestellten Wägungen zur Bestimmung der Dichtigkeit der Erde.
5#

 
