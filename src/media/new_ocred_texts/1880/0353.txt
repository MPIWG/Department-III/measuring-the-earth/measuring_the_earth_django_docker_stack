EL

Se RR

mr

ein

OT ENE TREN OO

45

9. Grossherzogthum Hessen.

Decker, A. Lehrbuch der höheren Geodäsie. Mannheim, 1845.

Eckhardt. Ueber die trigonometrische Aufnahme des Grossherzogthums Hessen. (Kri-
tischer Wegweiser im Gebiete der Landkarten- Kunde. Berlin, 1829 und
1830. Band I Seite 249—59 und 313—31, Band II Seite 49—58.)

Vorläufige Nachrichten von den geodätischen Operationen zur Verbindung der
Observatorien von Göttingen, Seeberg, Darmstadt, Mannheim, Speier und
Strassburg. Stuttgart, 1834.

Fischer, Ph. Lehrbuch der höheren Geodäsie. Darmstadt, 1845 —46.

— Untersuchungen über die Gestalt und Grösse der Erde. Darmstadt, 1868.

Nell, A. M. Schleiermacher’s Methode der Winkelausgleichung in einem Dreiecksnetze.

(2.2. V. X Hefe I und »)
— Zur höheren Geodäsie. (Schlömilch’s Zeitschr. für Mathem. u. Phys, Bd. XIX
S. 324—53.)

10. Italien.

Abetti, A. Sulla determinazione del tempo coll’osservazione dei passaggi delle stelle pel
verticale della polare. Padova, 1881.
Amante. Considerazioni sulle formule adoperate comunemente dai geografi per calco-
lare le posizione geografiche dei vertici dei triangoli geodetici. Napoli, 1852.
Arena, F. Diss. geogr. de dimensione et figura telluris. Panormi, 1757.
Beccaria, G. B. Gradus Taurinensis. Augusta /Taurinorum 1774.
Nota. Il P. Beccaria misura una base di circa 12 Kil. sulla strada reale
di Rivoli presso Torino e sviluppa la medesima tra Mondovi ed Andrate con
una rete trigonom. per dedurre il grado del meridiano di Torino (1762—1764).
Il barone di Zach ha illustrato, corretto e completato questo lavoro con
osservazioni astronomiche. (Mém. de l’acad. des sciences de Turin pour
les années 1811 à 1812.) Il lavoro stesso venne verificato nuovamente
durante la misura dell’arco di parallelo medio nel 1821 a 23.
= Lettere d’un Italiano ad un Parigino intorno alle riflessioni del signor Cassini
de Thury sul grado Torinense. Firenze, 1777. 2
Betocchi, A. Dei mareografi esistenti in Italia lungo i mari Mediterraneo ed Adriatico..
Roma, 1875.
— Dello stato attuale delle operazioni mareografiche in Italia.
Boscovich, R. G. De inaequalitate gravitatis in diversis terrae locis. Romae, 1774.
— De expeditione ad dimetiendos secundi meridiani gradus. Romae, 1755.

 
