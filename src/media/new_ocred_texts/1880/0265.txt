TENE TOTEM TM Ww Ty ME

LL

RS

13

Triangulation du Parallèle de Badajoz.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

ro Indication des : Longi- : Direct
N° 5 | Latitude. = Epoque. ee : :
| Peine | le poq One Instrument Remarques
ES Mollat Le: 38°56'53"| 12°38’ 8” 1862 Ibarreta, Solano. Repsold
(sans marque).
Ti4)Horeettea: 3. 5 38 37 12 | 19:44 19 1861 id. id.
P15 | Duranes~ .2.5...°. 38 50 58 113 1 36 1861 id. id. À
Neimdienn. . 2 38 34 37 113 8 35 1861 id. id.
117 | Sierra-Gorda. . . .| 38 49 46 | 13 25 20 1861 Ibarreta. id.
118 | Inego de Bolos ... | 38 52 27 | 14 34 25 1865 Ibarreta,Jimenez,Penacarrillo id.
119 | Cabera de Bney 38 37 47 | 14 28 9 1865 Ibarreta. id.
120 | Castellanos. . . .. 38 36 41 | 14 47 53 1865 id. id.
121 | Barreros . .-..2 5: 38 57 32 15 10 37 1865 id. id.
122 | Almenaras ..... 88 32 44 |15 13 25 1865 id. id.
123} Roble 5 — it: 38 43 39 | 15 35 28 1865 id, id.
124 PPorror . : :. ... 38 28 17 | 15 47 50 1866 Ibarreta Jimenez,Peñacarrillo id.
D Madrond | 38 98. 93 | 16 88 1866 id. id.
126 |Cabera del Asno . 38 19 39 |16 753 1866 id. id.
ke | Carches: .:. ss 2533.16 0 31 1866 id. id.
28.2 0iva 22. 38 44 16 |16 38 32 1866—69 Ibarreta, Solano, id.
Jimenez, Penacarrillo. Repsold C.
129} Malgmg 1. . -: 8880 7 172299 1865 Ibarreta. Repsold
(sans marque).
F0 Egea ee 36 b9 10 ir 2 44 1866 - 69 Ibarreta, id.
Solano. Repsold C.
131 Altana. ....... 38 38 58 14242953 1865 Ibarreta. Repsold
(sans marque).
132|Monduber...... 3320383 17 2422 1867 —69 Ibarreta, Repsold I.
| Solano. Repsold C.
135 | Mong0...….... 38 48 12 |17 48 7 1867 Ibarreta, Solano. Repsold
(sans marque) ||
Triangulation du Parallele de Madrid.
134 |S. Cornelio |40 21 4 |10 29 31 | 1861 Corcuera, Barraquer. Ertel I.
155 |Jarmelloö .. . „2 |40 35 28 |10 32 24 1861 id. id.
136 | Guinaldo ...... 40 27 9 | 12.05 1860 id. id.
2137| Marofa : 40 51 50 |10 40 52 1861 id. id.
158 | Bevrosa ... ... 49 52 2|11 981 1860 id. ld.
159) Serrota,. . 2.4.2 40 29 57 | 12 35 38 1860 id. id.
1410.| Flores... .... ..: 40 56 27 |12 35 74 1859 Corcuera. id.
141 | Valdihnelo . . ... 40 38 :7 |13 9 49 1859 id, id.
142|Eseusa ....... 20 2175213 253 | 1861 Corcuera, Barraquer. id.
143 | Santos . ...... 40 80 5 | 14 24 46 1860 Ruiz Moreno (D. M.), Monet. Ertel III.
144 | Peñas-Gordas . . «| 40 11 25 | 14 22 29 1860 id. id... =
- 145 |Altomira ......: 40 10 56 | 14 50 51 1863 Barraquer. Repsold I.
- 146 | N.S. de Bienvenida | 40 34 58 | 15 19 20 1863 id. id.
= 147 |Losares. . : . .: 40:13 :0 |15 28 1 1863 id. id,
148 | S, Felipe . ..... 40 24 8 | 15 48 47 1863 id. id.
149 | Berninches . . ... 40 35 29 | 14 51 21 1863 id. © id,
150 | Collado Bajo. . 4010 5|15 5443 | 1863-75 Barraquer, Lopez. Sead,
Puigcerver. Repsold C..
161 | Sierra alta. . . . . 40 29 ,0 |16 5 4} 1863 Barraquer, ee Repsold J.
152 | Javalon . . :.... 40 13 47 | 16 15 14 | 1863-73 a id. ce aids
va ee. Cabello. Repsold A.
153 | Palomera . ..... 40 35 53 |16 28 8 |) : 1863 Barraquer. Repsold I.
154 | Javalambre . . . .. 40 5 49 |16 38 52 | 1864 a id. ‚id.
155|Penaroya...... A0 23°94 (17 0 Qi 8 1863 id, id.
156: Pina 2... 40 143 [17 2 39 |  1863—68 id. 3 aids |
ed Solano. ‘Repsold C. :
157 | Peñagolosa . . . .. 40 13 22 | 17 19 22 1864 Barraquer. ‚ Repsold I.
158 | Espadan ...... 89 54 21 | 17 17 29 1863—68 1S id, © de
Solano. : Repsold :C. :
159 | Desierto . :.. .. 4195.70. 1 22 1864 —76 Barraquer, Repsold I.
| Borrés. Repsold B.
Ann. V. 10

pen ir

PO TOT OR RT Ba ET

 
