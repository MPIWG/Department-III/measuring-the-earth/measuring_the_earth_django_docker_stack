ee

EEE TE eR OE IEA EP EOE

 

| CO

oo

Die permanente Commission .erstattet der Conferenz in ihrer ersten Plenarsitzung
Bericht über ihre Thätigkeit seit der letzten Conferenz und über den Fortschritt der
Europäischen Gradmessung im Allgemeinen und ersucht die Herren Mitglieder, über
den neuesten Stand der Arbeiten in den einzelnen von ihnen vertretenen Staaten Mit-
theilung zu machen.

84.

Die Conferenz hält ihre Plenarsitzungen an den vom Bureau festzusetzenden
Tagen und Stunden.

S15.

Der Präsident handhabt die Ordnung in den Plenarsitzungen und leitet die
Verhandlungen; er setzt in Uebereinstimmung mit dem Bureau die Tagesordnung für
die Plenarsitzungen der einzelnen Tage fest und verkündet sie bei der Eröffnung der
betreffenden Sitzung.

S. 6.

Wenn nach gepflogener Erérterung in der Plenarsitzung Abstimmungen über
die Anträge von Berichterstattern nöthig sein sollten, so erfolgen dieselben durch Aut-
stehen und Sitzenbleiben. — In solchen Fällen sind nur die von den hohen Staats-
resierungen ernannten Conferenzmitglieder stimmberechtigt.

Sn
Anträge, welche nicht Gegenstände des in der ersten Plenarsitzung beschlossenen
Programms betreffen, auch mit diesem nicht im Zusammenhange stehen, sowie etwaige
schriftliche vor die Conferenz zu bringerde Mittheilungen solcher Art sind vorher bei
à © .. .. . . .
dem Bureau einzureichen. Dasselbe entscheidet über deren Zulässigkeit in der laufenden

Sitzungsperiode. Bezüglich solcher Anträge und Mittheilungen kann jederzeit der Antrag
auf Uebergang zum Programm für die laufende Sitzungsperiode gestellt werden.

3
Bei Eröffnung jeder Plenarsitzung der Conferenz bringt das Bureau die in-
zwischen überreichten Vorlagen, welche sich auf die Sache beziehen, zur Kenntniss der
Versammlung. Dergleichen Vorlagen können auf Beschluss der Versammlung, wie auch
des Bureaus in dem gedruckten Rechenschaftsberichte mehr oder weniger vollständig
erwähnt, oder ganz in denselben aufgenommen werden. Sie sind schliesslich dem Archiv
der Europäischen Gradmessung einzuverleiben.

sg.
Die Redaction der Verhandlungen der Conferenz übernimmt die permanente
Commission und sorgt für den Druck und die Vertheilung.

sm.
Die Wahlen für die ausscheidenden Mitglieder aus der permanenten Commission

#

   

PONT PEN PPT POTT)

ah

14:

a a ame as bat a db paie mu 4

 
