 

20 RAPPORT SUR LA QUESTION DU PENDULE,

y correspondant & la résistance de lair, au frottement et a la correction de longueur ;
le dernier terme est la correction due au balancement.

En posant

Yh — Th

1, — move — T’ a ay Ue BD).

et remplaçant au second terme / par à, il en résulte
u a LD, ps le. ag kp
4 = 7 = E SR (h = h )| — 7D V= (1 —— =) .

Pour le second pendule on aura de même

a =: a (1 a? aor),

en désignant par T,, X, p’, ce que deviennent T,, À, p; k reste le même, ne dépendant
que du support, et pourra s’éliminer entre les valeurs de 4 01. pourvu que les

ö D Dp . BE
fractions 4, £ soient inégales.

 

Cela exige que p, p’ soient mesurés avec une exactitude de premier ordre;
mais il n’en sera plus de même si l’on à X = À + Ô, 0 étant assez petit pour qu'on
puisse négliger 0? et ok; alors

a Ve
TT == a TT ——-
V g g a

réunissant à T, le second terme, aisé à évaluer, on aura

a Vi (1 + aa) Ds =-\V/ (

07

 

 

Di

ZU: V

Q
=

 

kp
DR

   

d’où l’on tire

 

a Da. nn à P Gu om
et l'erreur commise sur les poids », p’ se trouvera multipliée par le petit nombre T, — T..

L’amplitude employée pour T et T’ ou pour T, peut différer de celle qui sert
à calculer T,:; de même h, # peuvent différer pour les deux pendules.

24 Procédé. Grace à une propriété singulière du pendule on peut faire l’élimi-
nation sans en employer plus d’un.

   

Hee 44 shh he A AMA Ah MA lk dus À À à. à À

Jhb.

(ists and a ali he das nn à

 
