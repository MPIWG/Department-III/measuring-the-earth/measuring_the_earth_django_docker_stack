LE

TERRE

een nee eter RE

mera

NRE LATENT BRETT TE re et

93

Le Président, M. Faye, réconnait les grands soins avec lesquels l'établissement
de ces cartes à été conduit, pour autant qu’il a été possible de l’achever, et exprime
a M. Ferrero les remerciements de la Conference pour la peine qu'il s'est donnée afin
d’amener cette œuvre à bien.

M. Perrier se permet, avant de communiquer son rapport, de demander, à titre
de renseignement, d’où il vient que dans le dernier catalogue fondamental de la Societé
astronomique, les positions des étoiles données par les précédents catalogues, se trouvent
notablement changées; pour les déclinaisons l'écart atteint souvent jusqu'à 0:3. Il en
résulte qu’il est nécessaire de calculer à nouveau les déterminations de latitude tandis-
qu'il avait cru que les positions de Pulkowa etaient très sûres.

M. Bruhnms répond que les positions données précédemment ont été formellement
indiquées comme provisoires, et qu'il résulte de la publication trimestrielle de la Société
astronomique, que ce n’est que depuis un an que les observations définitives de Pul-
kowa ont été communiquées au Comité de la societé. La commission astronomique qui
s'occupe de la publication du catalogue fondamental, a considéré qu’il était nécessaire
de se servir aussi des nombreuses observations faites dans d’autres observatoires, par
exemple à Greenwich, Cambridge, Leipzig, Leyde, et c’est d’après toutes ces obser-
vations que M. le Professeur Awwers a établi le Catalogue fondamental: il a donné les
explications nécessaires sur le calcul de ces positions dans la publication No. XIV faite
par la société astronomique sous le titre de »Fundamentalkatalog für die Zonen-
beobachtungen am nördlichen Himmel“. Ce catalogue doit étre actuellement considéré
comme le catalogue le plus parfait qui existe; et il croit pouvoir le recommander en
première ligne. La réduction des précédentes hauteurs polaires pourra certainement
se faire assez facilement en comparant les positions jadis assignées aux étoiles avec
celles qui figurent dans le nouveau catalogue fondamental.

M. Albrecht se joint complètement à l'opinion qui vient d’être émise, et il désire
que la conférence décide, qu’on doit employer le catalogue fondamental pour les
déterminations des coordinées géographiques.

La Conférence adopte la motion suivante:

en première ligne on doit employer pour les déterminations des coordonnées
géographiques le catalogue fondamental établi pour les zones.

M. Perrier présente son rapport sur la mesure des bases. (Voir annexe VI.)
La conclusion suivante présentée à la fin de ce rapport:

les règles qui ont servi jusqu'à présent pour mesurer les bases, devront

être controlées avec un étalon international au Bureau international des
poids et mesures à Bréteuil,

est votée, après que M. Hirsch eut expliqué que le Bureau de Bréteuil se mettra en
mesure d'entreprendre ce travail,

M. Hirsch, comme rapporteur sur les travaux de nivellement, présente son rap-
port. (Voir annexe VII.)

 
