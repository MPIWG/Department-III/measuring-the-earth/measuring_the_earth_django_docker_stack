Erste Sitzung,

Munchen, k. Polytechnikum, Montag den 13, September 1880,

Anwesend sind: Se. Excellenz der Herr Ministerpräsident und Minister für
Kirchen- und Schulangelegenheiten Dr. von Lutz, die Herren Bevollmächtigten Adan,
Albrecht, Baeyer, Bakhuyzen, Barraquer, von Bauernfeind, Bruhns, Faye, Ferrero, von
Forsch, Helmholtz, Hirsch, Ibanez, Lorenzoni, Mayo, Nagel, Nell, von Oppolzer, Oudemans,
Perrier, Plantamour, Sadebeck, Schoder, Seidel, Siemens, Villarceau, Zech; die eingelade-
nen Herren: St.-Claire Deville, Gould, Asimont, Bauer, von Beetz, Brill, Decher, Frauen-
holz, Haid, von Jolly, Klein, Merz, Neumeyer, von Orff, Steinheil, Schmidt, Voit.

Anfang der Sitzung: 11 Uhr 20 Minuten.

Herr Ibanez eröffnet als Präsident der permanenten Commission die Sitzung und
Se. Excellenz Herr Ministerpräsident Dr. von Lutz bewillkommnet die Versammlung mit
folgenden Worten: \

Im Namen der bayerischen Regierung gebe ich mir die hohe Ehre, den
zur sechsten Generalversammlung anwesenden Herren der Europäischen Grad-
messungsgesellschaft ein herzliches Willkommen zuzurufen. Zum Willkommen .
füge ich den lebhaften Wunsch hinzu, dass es den Herren wohlgefallen möge
bei uns und dass das Behagen an Ihrem Aufenthalte in München eben so
gross sein möge als die Freude, die wir darüber haben, dass Sie hier tagen.

Wenn ich, meine Herren, von unserer Freude über Ihre Anwesenheit spreche,
so ist das nicht eine Redensart ohne sachlichen Gehalt und Hintergrund.

Die Europäische Gradmessungsgesellschaft ist zum Behufe der Abhaltung
ihrer sechsten Generalversammlung in einem Staate gastlich eingekehrt,
welcher allerdings nicht den Beruf hat, für sich allein durch äussere Macht
zu imponiren und mit gewaltigem Nachdruck seine Anschauungen darüber
zur Geltung zu bringen, wie sich die Völker verhalten sollen, um den grossen
Widerstreit der Interessen zu beseitigen und ein friedliches Beieinanderleben
zu ermöglichen. Aber dies vorausgeschickt, ist die Aufgabe des Staates, in
welchem Sie Ihre Arbeiten zu fördern beabsichtigen, doch von der grössten
und achtbarsten Bedeutung.

 
