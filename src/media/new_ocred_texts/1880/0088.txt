 

76

et l’autre représentant le complément de ce kilogramme au poids de l’eau à 0° contenue
dans le tube contrôle. Une seconde partie du mémoire donne tous les détails de la com-
paraison du cylindre en platine iridié à 202 avec les kilogrammes de Fortin: kilogramme
de l'observatoire et kilogramme des Archives de France, et enfin l’une des masses de platine
jridié à 102 d’iridium présenté par la section française de la commission internationale
au comité international des poids et mesures.

Toutes ces mesures ont été faites avec l'intermédiaire du kilogramme de l'ob-
servatoire de Paris.

La troisième partie du mémoire contient la description de la balance qui a servi
à ces pesées; les auteurs y ont étudié avec de grands détails toutes les causes d'erreur
inhérentes à la matière qui a servi à sa construction, dues à la variation de tempéra-
ture, aux défauts de l’élasticité dans les métaux; ils donnent les moyens de reconnaître
l’origine des différences les plus importantes qu'on observe toujours dans les pesées faites
avec les balances les plus parfaites qu’on puisse construire aujourd'hui.

_ M. Mel‘) présente le rapport pour la Hesse; M. le Général Mayo pour VItalie2);

M. M. Oudemans et Bakhuyzen, pour les Pays-Bas?); et M. von Oppolzer, pour l Autriche*).

' Le Rapport du Bureau central contient déjà les renseignements nécessaires re-
latifs à ce qui à été fait en Prusse.

M. Hirsch fait lecture d’une lettre qui lui est adressée par M. le Professeur
H. Bruns et dans laquelle celui-ci rappelle les vues qu’il avait développées dans une publi-
cation faite, il y a plusieurs années, et intitulée ,,Ueber die Figur der Erde“. D’apres
son opinion, l'importance des déviations du fil à plomb, dans la fermeture des poly-
gones de nivellement, ne dépendrait pas de la grandeur de ces perturbations, mais
plutôt de la rapidité de leurs variations.

M. M. Hirsch et Plantamour font remarquer à ce propos que ce serait, pratique-
ment, un travail impossible que de déterminer partout, le long des lignes de nivellement,
les variations de la pesenteur, ainsi que M. Bruns semble le demander. Il résulte cer-
tainement des expériences faites en Suisse aussi bien que dans d’autres pays, que les
polygones de nivellement se ferment, sans tenir compte des variations de la pesanteur,
d’une manière parfaitement suffisante.

M. Forsch présente le rapport pour la Russied); M. M. Bruhns et Nagel pour
la Saxe*); M. Hirsch pour la. Suisse’); M. Ibanez pour ’Espagne®). M. Perrier présente

1) Voir le Rapport général, Hesse.

2) Voir le Rapport général, Italie.

3) Voir le Rapport général, Pays-Bas.
4) Voir le Rapport général, Autriche.
5) Voir le Rapport général, Russie.

8) Voir le Rapport général, Saxe.

7) Voir le Rapport général, Suisse.

8) Voir le Rapport général, Espagne.

 

|
|

 
