16

Ensuite sous le même titre, publié par C. Bruhms et A. Nagel, 1871,

page 1—16.
Ensuite sous le méme titre, publié par Nagel, 1872, page 1—18; 1873
page 1—9.

I m oe

5. Wurtemberg.
Jusqu'à présent seulement: ,,Die Ergebnisse des Praecisions-Nivellements
I »

der Cirkelbahn, zusammengestellt von Prof. Dr. Schoder: in den Wirtemberg.
naturwissenschaftlichen Jahresheften 1869, Heft 2 et 3.“

6. Autriche.
Jusqu'à present: ,,Pracisions-Nivellement in und um Wien, ausgeführt
in den Jahren 1876 und 1877 von der Triangulirungs-Caleul-Abtheilung des

K. K. militär-geograph. Instituts‘, publié dans la Zeitschrift des Oesterreich.
Ingenieur- und Architecten-Vereins, 1878, Heft 6 und 7“.

7. Belgique.
„Nivellement general du royaume de Belgique, publié par l'Institut car-
tographique militaire“; il comprend:
A. Nivellement de base — 1879 — 1 volume.

B. La récapitulation des points de repére par province et par com-
mune; 1879, 1880.

Les 6 premiers cahiers comportant 5230 reperes et 6 provinces, ont paru.
L’ensemble des 9 cahiers formera un volume.

RP PP THE N: |

el HAP

8. Espagne.
On trouve des renseignements sur les nivellements espagnols dans les
„Memorias del Instituto Géografico y Estadistico“, dont 2 volumes ont paru.

9. Italie.
Jusqu’a present: „Studi sulle nivellazioni géométriche di précisione,
: del’ingegnere Oreste Coari. Roma tipografia Martelli 1879.

10. Portugal.
Rien n’a paru encore.

TT

11. Russie.

Le nivellement du chemin de fer Baltique et d’une partie du chemin de
fer St. Pétersbourg—Varsovie (entre Dunabourg et station Lapi) est publié
par M. Zinguer dans le XXXVI tome des „Memoires du dépôt topographique.‘

| Les autres nivellements n’ont pas encore été publiés.

12. Suisse.
„Nivellement de précision de la Suisse, exécuté par la Commissian géo-
désique fédérale, sous la direction de À. Hirsch et E. Plantamour, Genève et

Bâle chez Georg.“ 1" livraison 1867; 2° 1868; 3™° 1870; 4¢ 1873: 5me 1874;
62° 1877: Tue 1880.

Annexe VII. 3

Tyan

LA, IT.

È
à.

 
