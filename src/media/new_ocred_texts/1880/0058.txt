 

|
nt
Ht
Ki
it

fl

i
i}
i
|
|
i
h

AG

Dritte Sitzung.

München, am 17. September 1880.

Anwesend die Herren: von Bauernfeind, Bruhns, Faye, von Forsch, Hirsch, Ibanez,
Mayo, von Oppolzer. Herr General Bayer lässt sich wegen Unwohlseins entschuldigen.

Anfang der Sitzung: 6 Uhr 30 Minuten.

Präsident: Herr Ibanez.

Secretiire: die Herren Bruhns und Hirsch.

Herr Bruhns theilt ein ihm von Herrn Förster zum Zwecke der Vorlage an die
permanente Commission eingesandtes Schreiben mit, betreffs der zu verschiedenen Zeiten
in Berlin ausgeführten Maassvergleichungen der Bessel’schen Toise mit mehreren Copien.

Herr Hirsch bedauert, dass durch diese Mittheilung von Neuem eine Angelegen-
heit vor die permanente Commission gebracht werde, welche eine interne preussische,
zwischen dem geodätischen Institut und der Landesaufnahme zu erledigende - Frage
betreffe. Soweit dabei ein wissenschaftliches metronomisches Interesse betheiligt sei,
sowie überhaupt für alle die Gradmessung interessirenden Maassvergleichungen, scheine
ihm das internationale Maass- und Gewichtsbureau am geeignetsten, die richtige Lösung
liefern zu können. Er gebe sich der Hoffnung hin, dass Herr Professor Förster mit
Rücksicht hierauf sich dazu verstehen werde, seine Mittheilung zurückzuziehen.

Herr von Oppolzer, ohne sich über die zu Grunde liegende Frage aussprechen
zu wollen, ist der gleichen Ansicht, wünscht aber in jedem Falle, dass der etwas starke
Ausdruck: ‚im Interesse der wissenschaftlichen Logik“ aus dem Schreiben verschwinde.

Nachdem sich noch mehrere Mitglieder in gleichem Sinne ausgesprochen, wird
mit allen gegen 1 Stimme beschlossen:

1. Herrn Professor Förster durch Herrn Hirsch ersuchen zu lassen, sein
Schreiben zurückzuziehen;

9. wenn dem nicht willfahrt werde, jedenfalls zu verlangen, dass der oben
erwähnte Ausdruck in dem Schreiben beseitigt werde;

3. far den Fall der Veröffentlichung des Schreibens die von Herm General
Baeyer etwa beliebte Antwort demselben hinzuzufügen”);

 

*) Da Herr Professor Förster auf Mittheilung seines Schreibens, in welchem er die verlangte
'Aenderung angebracht, beharrt, so lassen wir nachstehend das Schreiben des Herrn Förster, sowie die
Antwort des Herrn General Baeyer folgen.

   

ua kuss haar Aa,

una

id me a u, bh hä aa mu nn us

ur enmeisnshinnnt aueh hi

 
