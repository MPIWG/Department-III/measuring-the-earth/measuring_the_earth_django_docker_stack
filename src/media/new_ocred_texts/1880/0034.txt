 

22
bestehen soll. Das Gewicht ist aus zwei Gylindern zusammengesetzt: der eine soll eine
Masse haben, welche genau gleich ist dem neuen Normalkilogramm des internationalen
_ Comite’s für Maass und Gewicht, und der andere Cylinder soll das Complement. dieses
Kilogramms sein zum Gewichte des Wassers bei 0°, welches in der Control-Röhre ent-
halten ist.

Der zweite Theil des Memoire giebt alle Einzelheiten der Vergleichung des oben
erwähnten Platin-Iridium-Cylinders mit dem Kilogramm von Fortin, dem Kilogramm des
Observatoriums und dem Kilogramm des Archives de France und endlich mit einer Masse
von 102 Iridium enthaltendem Platin-Iridiums, welche von der französischen Section der
internationalen Meter-Commission von 1872 dem Comité international des poids et me-
sures tibergeben worden ist. Alle diese Vergleichungen sind durch Vermittelung des
Kilogramms der Pariser Sternwarte ausgeführt,

Der dritte Theil des Mémoire enthält die Beschreibung der Waage, welche zu
diesen Untersuchungen gedient hat; die Verfasser haben sehr eingehend alle Fehler-
ursachen untersucht, welche von dem Material herrühren, aus dem sie hergestellt ist,
und welche von den Veränderungen der Temperatur und der unvollkommenen Elasticitit
der Metalle abhängen; sie geben die Mittel an, um die Ursachen der beträchtlichsten
Unterschiede zu erkennen, die man immer bei den Arbeiten selbst mit den besten
Waagen findet, welche man heutzutage construiren kann.

Für Hessen erstattet Bericht Herr Nell!), für Italien General Mayo?), für die
Niederlande die Herren Oudemans und Bakhuyzen?), für Oesterreich Herr von Oppolzer®).

Für Preussen sind schon bei Gelegenheit des Berichtes des Centralbureaus die
nöthigen Mittheilungen gegeben.

Herr Hirsch verliest einen ihm von Herrn Professor 4. Bruns übersandten

Brief, worin derselbe eine von ihm schon vor mehreren Jahren in einer damals publi-

eirten Abhandlung ‚Ueber die Figur der Erde‘ ausgesprochene Ansicht in Erinnerung

bringt, wonach es bei dem Schlusse der Nivellementspolygone nicht auf die Grösse der
Lothstörungen, sondern vielmehr auf die Raschheit ihrer Aenderung ankomme.

Die Herren Fäörsch und Plantamour bemerken dazu, dass es zu einer praktisch
unmöglichen Arbeit führen würde, wenn überall entlang den Nivellementslinien die Aen-
derung der Schwere bestimmt werden sollte, wie Herr Bruns dies als nothwendig
bezeichnet. Aus den bisherigen Erfahrungen in der Schweiz sowohl als in den
übrigen Ländern gehe im Gegentheil hervor, dass die Polygonalabschlüsse sich ohne
jede Rücksicht auf die Störungen der Schwere in vollkommen genügender Weise
erzielen lassen.

') Siehe Generalbericht: Hessen.
?2) Siehe Generalbericht: Italien.
3) Siehe Generalbericht: Niederlande.
*) Siehe Generalbericht: Oesterreich.

 

>
3
3
a

Jutta) nme dé ide ad aiid ll Asai na :

 
