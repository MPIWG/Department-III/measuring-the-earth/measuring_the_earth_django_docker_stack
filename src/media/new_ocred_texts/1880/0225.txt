 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Italien.
| | | 5
NE | Indication des Latitude, | LOngi- | Époque. Dineetenn Instrument. Remarques,
| Points. | |. studies ast et Observateurs.
| | |
—— | m nn an ee i : |
| ll | |
#39 Stellan. Gawd 87°. 272071 8a" oF hea 1865 Capit. Marangio. Pistor de 40 em.
| 200| Mezzo Gregorio . .| 36 57 53 | 32 37 % | 1865 id. id. |
15201 |Benna......... | 36 51 31 | 32 20 28 | 1865 id. id, |
202 Paching. .. . . . : | 36 42 52 | 3a 4591 | 1865 id. id. i
sees | Pollina ©... 37 59 29 |31 47 30 | 1865 id. id, |
Niederlande. |
a Se | Pees
| ; : | | = | 2 de el
N: | Indication des | Latitude. | au | Epoque. Directeur Instrument. Remarques.
; | poq q
Points. |= tudes | et Observateurs.
| BE 7 RT me 5 = {|
L | Eommel >: 7: 328 51°13'43"| 22°58'47" a Belgique.
} 2|Nederweert ..../51 17 11 | 23 24 48 | | Fes
IE 3) Helmond ..:... 51 28 44 |28 19 17 | a HSE |
+ 4 Hilvarenbeck ... 5129 8 12248 5. | LEE |
| 5| Hoogstraten ....|51 24 4 | 22 95 35 | an | Belgique.
iF 6 Hertogenbosch... 51 41 18 |22 58 22 45. | Bois-le-Duc
| | | as = | de la grande
| | | = glise).
7|Breda....... .|/51 35 22 | 92 26 98 | ae ei |
| 8 | Willemstad . . . . .| 51 41 32 22 6 9 | Ads a
i 9 | Gorinchem . . . .. | 51 49 48 |22 38 15 | DE au
; 10 Dordrecht ..... 51 48 52 | 22 19 29 | (Se =
: 11) Rotterdam . .... [51 55 19 | 22 18 59 | re a © ä
2 Gouda...::.:: 152 0.40 |22 22 32 | „oe =
13 | Utrecht Be ne sr 52 5 28 22 47 11 | Due = oD |(Clocher de la Cathé-
| = = i = . drale.
if Leen... 2 <: 52 9 23 |22 9 19 | | gel, 2 Tour del’ Hoteldeville,
Lin) Leiden . D2 320,293 9 23 = | A.ErR = Clocher de la ci-devant
5 a . 2
4 | ES ae So a Halle à laine, présen-
5 | SH a a en Eglise Cath. .
5 | (Je) SER, = omaine, SE
= 15 Rheenen ...... 51 57 27 | 23 43-46 | = ade, = & Null Station
= | el locher de l'Égli
| 2 = n ES 8 Nowell, ' ae
16 | Det 52 046 99 199. eee 5
17|Nieuwkoop..... 5299109 9641) ee. : a
# 18 Haarlem ...... 52 2255 122 18 7 2 > ay Clocher occidentale.
5 19| Amsterdam ....|52 22 30 | 22 82 54 | Ar 2 Clocher du Poids.
À. 20 | Alkmaar , ..... 52 87 55 | 29 94 54 | os > 5 Clocher & Canillon.
Meet Edam <2 ee. 52 30 46 | 22 42 43 | a. = Ta ton maintenant
| | | | + 5 r utr -
| | | ee 3 con 0
22 |Hoorm........ 52 88 28 | 22 43 29 EE =
E 23|Schagen ...... 52 47 14 192 9739| und
© 24 |Enkhuizen ..... 52 42 16 | 22 57 28 | Ale
* 925 | Medemblik . . ... 52 46 26 |22 46 6. en, :
© 261 Hindeloopen .. ..|52 56 37 23 8 48 gro Nouvelle station.
= 27 | Naarden ...... 52 17 46 | 22 49 38 | = 5 D |
' 28 | Amersfoort. . ... 52 920 [23 3 9 ee à :
29'| Veluwe... 6 a 6% 52 14 7: 123: 81.24) Om Observatoire au Ve-
| || luwe de Krayenhoff,
| | Rz point conservé par
| une pierre avec in-
| scription fixe dans le:
| i] sol.
| |
Ann, V. : 5

(ARR à à bent à

AM CTOE LE

ha Ai v
PR LE DE RTE

ik

 
