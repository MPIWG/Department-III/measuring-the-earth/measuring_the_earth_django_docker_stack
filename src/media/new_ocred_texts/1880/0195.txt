 

A ER PTT ANNE ST mn

 

 

 

ePID INE PLT REPT

are

Terre

Ni dat

i
ia
Sg
‚a
i
E
8
=
2
nl

 

 

8 |

9 | Aufkirchen

10 | | Augsburg

11
12 |

13 | | Berg

14 |

15 | | Bramberg
16 | | Braunau

|
18 |
19
20

21 |
22 |
23 |

 

24 |
95 |
26 |

27 |

28

29 |
30 |
31 |

32

33 |

34
35
36
37
38
39

A0 |

41
42
43
44

45 | Günzelhofen
46 | Habsberg
|

47 | Haid
48 | Hainberg

49 | Haunsberg
50 [MAU Een ent

| Arber

| Altenhausen .
| Auerberg

|

| Altenburg

| Altmiinster

Indication des
Points.

Asten

Aufhausen

| Banz
| Benedictenwand . .

Lee ee ee el

| Blockstein : ;

 

| Breistöl
Brennberg ..... |
Bruck

Bussen
Castiberg
Coburg... . =: |

Denkendorf .
Dillenberg
Dobra

Doliberg
Dreifaltigkeit . . .|
Edelsberg
Edkor
Eichelberg . . . ..
Eisbrunn
Eschers
Eulbacherhof. . ..
Feldberg . . . . .. |
Friedberg
Fürstenstein . .
Gammersfeld.
Georgenberg .
Gobernauserwald .
| Gorkum
Gross-Gleichberg
Gross-Rettenstein .
Grünten

Nee

 

ei NiO. ations eile oe

 

Latitude. |

>
©

IX
Oo

He ©
HB OU

HR DD OO bo kt OL
OO 19

OÙ Ha
ORB RAD WD Oe

Ou oo

20

14
18

40
14
54
44

42

58 |

12
28

31

tt
19

29

42
47
57

10
43

30
30
al
50

29
53
55
87
20
16
16
42

33
17

18

42
49

OU N I

27

 

Longi-
tude.

29

29
28

26
26
28
30
28
28
30
28
28
29
27

28
29

30
29
30
28

39

22
19

44

38
59
43
20
58
47
15
57

48
1

27
51
39

0 | 1820 — 22—93—

55
39

54 ||

55

49 |

39

13 |
56. ||

48
29
50

59 ||
32 |

8
46 |

25
19

39 |

27

48
22
40
20
40

co

27

84 |

24

||

| Bayern und die Pfalz.
DE EP ee

|
| Epoque.

I: : 1816-10

| 42 —48
| 1806-—16 —18 -
| To
| 1811

| 1810--11-29—54.

1804—7

| 1805-10 -11-15
16-9155 204
| 1805
|1801—54—-56—57

1805 —10

| 1823 —49—48

| 1852-5455

| 1823

1811

| 1848
1810. 11 14
| 1821-22-35
1804 5—10—97

|

1821—42
1820—23— 42
43—49—50

1806

| 182993 30 —

| 48—52

| 1804 —5

| 1822

(1820 2108

| 43-49-50-51-52

| 1820—22—30

| 1804 -6—10

1816-21

| 1852—58—54

|. 1180529

ll 1810611

| 1810—11—16—21

1822 - 35
1822

1810 1116

| 100211

| 1804-11-12

1805 —4—10—11

| 1812

| 1823—42—48

11821-22-23-47-48

1852

 

11811-16-19-21-54

| 1806

| 1803—4—9—21—

| 27-30
1804—10—11

| 1825-5152

| 1811

| 1816-21

 

 

|
I

Direeteur
et Observateurs.

Mader, v. Amman, Hermann.

Mader, Wieland, Rathmayer.

Bonne, Mader, Hermann, von
Amman.

Soldner.

Soldner, Hermann,Rathmayer.

Bonne.

Rathmayer, Mader, Bonne,
Hermann, v. Amman.

Broussean.

Henry, Fritz, Wieland, Bonne,
Rathmayer.

Bonne, Glaser, v. Amman.

Mader, Rathmayer, Wieland.

Rathmayer.

Mader.

Soldner.

Rathmayer, Wieland.

Soldner, Mader.

Mader, von Imsland.

Brousseau, Soldner, Mader.

Mader, Wieland, Soldner.

Wieland, Weiss, Mader, von
Imsland.

Bonne.

Mader, Wieland, v. Brand,
Rathmayer.

Brousseau.

Mader.

Mader, Wieland, Weiss.

Hermann, Mader.

Bonne, Soldner.

Mader, Hermann.

Rathmayer.

Brousseau, Soldner.

v. Amman.

v. Amman, Mader, Hermann.

Mader, v. Imsland.

Mader,

Glaser,

Soldner.

Brousseau, Soldner, v. Amman.

Glaser, Bonne, v. Amman.

Glaser.

Mader, Wieland, Rathmayer.

id.

Mader, v. Amman.

Rathmayer.

Mader, v. Amman, Hermann,
Rathmayer.

Bonne.

Mader, Brousseau, Hermann,
Soldner, Zobel.

Soldner, Bonne.

Mader, Wieland, Rathmayer.

Soldner, Glaser.

Mader, Hermann.

 

Instrument.

Théodolites de 8 pouces d’Ertel.

1 Théodolite de Reichenbach de 12 pouces.
1 Instrument Universel d’Ertel.

2 Cercles de Borda de 13” et de 15”.

Remarques.

„Die Bayerische Lan-
des- Vermessung in
ihrer wissenschaft-
lichen Grundlage.“

Munich 1873.

No. 7, Württemberg.

No. 1, Sachsen.

 

 
