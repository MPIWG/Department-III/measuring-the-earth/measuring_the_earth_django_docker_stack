sat EE a Hiss,

SE

SA nes

Ah Le M 4 À lh
LEN RE RTT BRETT

/weite Sitzung.

München, den 14. September 1880.

Anwesend die Herren Bevollmächtigten: Adan, Albrecht, Bakhuyzen, Barraquer,
von Bauernfeind, Bruhns, Faye, Ferrero, von Forsch, Helmholtz, Hirsch, Ibanez, Lorenzoni,
Mayo, Nagel, Nell, von Oppolzer, Oudemans, Perrier, Plantamour, Sadebeck, Schoder, Seidel,
Siemens, Villarceau, Zech, die eingeladenen Herren: St.-Claire Deville, Gould, Albert,
Asimont, Bauer, von Beetz, Brill, Decher, Frauenholz, Haid, von Jolly, Klein, S. Merz,
Neumeyer, von Orff, Steinheil, Schmidt, Voit.

Anfang der Sitzung 10 Uhr 20 Minuten.

Präsident: Herr von Bauernfeind.

Secretäre: die Herren Bruhns und Hirsch.

Das Protokoll der vorigen Sitzung wird in deutscher und franzôsischer Sprache
verlesen und genehmigt.

Der Präsident theilt mit, dass Herr General Baeyer durch Heiserkeit leider ver-
hindert sei, heute zu erscheinen. Er macht einige geschäftliche Mittheilungen und setzt,
wegen der Fahrt nach dem Würmsee, für morgen den Anfang der Sitzung zu präcis
9 Uhr 30 Minuten mit der alleinigen Tagesordnung „Wahlen in die permanente Com-
mission‘ fest.

Als heutige Tagesordnung ist die Fortsetzung der Berichte angesetzt und der.
Präsident fordert in alphabetischer Reihenfolge die Vertreter der verschiedenen Staaten
auf, ihre Mittheilungen zu machen. /

Für Frankreich berichtet Herr Perrier.)

Herr St.-Olaire Deville macht Mittheilung über die Untersuchungen, welche er in
Gemeinschaft mit Herrn Mascart in einem M&moire niedergelegt hat, worin das Gewicht
bestimmt wird, welches gleich ist dem Gewicht des Wassers bei 0°, das in dem der
geodätischen Messstange als Controle beigegebenen Rohre enthalten ist. Im ersten
Theile dieses M&moire besprechen die Autoren die Zusammensetzung und die physischen
Eigenschaften des 202 Iridium enthaltenden Platin-Iridiums, aus welchem dieses Gewicht

1) Siehe Generalbericht: Frankreich.

 
