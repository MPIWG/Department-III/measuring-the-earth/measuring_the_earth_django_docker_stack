|
|

 

LI LOL HU

nm

en

TOON TOM VOTRE OTTER

ae

 

Annexe VII.

 

EAP EE Oa

SUR

L'ÉTAT ACTUEL DES TRAVAUX DE
NIVELLEMENT DE PRECISION

executes dans les différents pays de l’Association,

présenté à l'Association géodésique internationale dans sa réunion à Munich
le 16 Septembre 1880

PAR

Ad. Hirseh

Messieurs,

Deere par la Commission permanente dans la séance du 19 Septembre 1879 à Genève
comme Rapporteur spécial sur l’état actuel des travaux de nivellement de précision dans
les pays associés, j'ai demandé au mois de Juin aux délégués de tous les pays
de l'Association de bien vouloir me fournir des données précises et complètes sur les
instruments, les méthodes et les résultats obtenus dans leurs pays. Voici le question-
naire que je me suis permis d'adresser à mes collègues.

ASSOCIATION GÉODÉSIQUE :
; Nervcuirer, le 15 Juin 1880.
INTERNATIONALE.

 

Circulaire.

Monsieur et très-honoré collègue,

Chargé du rapport sur les Nivellements de précision pour la prochaine Conférence
générale, je prends la liberté de vous demander de bien vouloir me fournir les ren-
seignements sur les opérations de nivellement dans votre pays dont j'aurai besoin pour
élaborer ce rapport, et de les accompagner d’un croquis du réseau, si possible dessiné
sur une carte à petite échelle, sur laquelle il conviendrait d'indiquer aussi les lignes
projetées qui éventuellement doivent compléter votre réseau.
