sets wo ee re nn

Mb ppt

à
ik
a
à
À
A
z=
cs
5 |

&
4

Beobachtete Linge Berechnete Liinge B—R
Strassburg — Genf OF 24.99 67 27.908 + 0.022
Wien 34 16.53 34 16.472 + 0.058
Berlin 22° 30.23 221 "90 071 — 0.059
Mannheim 2 45.79 2 45.801 — 0.011
Berlin — Göttingen 13 48.56 13 48553 — 0.007
Altona 13 4851 13 48.566 — 0.056
Altona — Göttingen 0; 015 0 0013 — 0.137
Wilhelmshaven 7 11.14 7. 11.096 — 0.044
Göttingen — Brocken 2 42,22 2 42.153 — 0.067
Neuenbure—Simplon 4 16.82 4 16,823 — 0.003
Mailand 8 59.99 8 56.013 — 0.023
Simplon — Mailand 4 39.16 4 39.190 — 0,030
Neuenburg— Zürich 6 22.37 6 22.477 — 0.107
Righi 6 05 6 6.583 — 0.053
Righi — Zürich 0 198% 0 * 15.894 — 0.054
Zürich — Gäbris 3 40.07 3 40.120 — 0.050

Was die einzelnen Werthe der Verbesserungen anbetrifft, so ist der grösste
Werth bei Wien—Bregenz, weil das Dreieck Wien—Bregenz 26% 14°79 und Bregenz-
Paris 29™ 45°29 mit Paris—Wien 56" 0°22 nur bis auf 0°14 schliesst, Paris—Strass-
burg, weil das Dreieck Paris—Strassburg— Wien den Fehler 0°12 übrig lässt, was offenbar
an der 1863 in Strassburg bestimmten persönlichen Gleichung liest. Da Göttingen, wie
oben schon erwähnt, unsicher bestimmt, verwundert es nicht, dass Leipzig—Göttingen
und Göttingen—Altona Correctionen bedürfen, welche bis auf 0°13 und 0°14 steigen.
Auch Herr Professor Albrecht hat schon darauf aufmerksam gemacht und es scheinen mir
die Längenbestimmungen zwischen Göttingen und anderen Orten als Leipzig, Berlin etc.
sehr wünschenswerth. Die Correctionen welche einige schweizerische Stationen
noch erfordern, werden sich wahrscheinlich verlieren, wenn der Anschluss an Paris und
Lyon publicirt ist. Bildet man endlich aus den übrig bleibenden Fehlern den wahrschein-
lichen Fehler einer Längendifferenz, so stellt sich derselbe für eine Längendifferenz
zu =+ 0'035, welcher noch etwas grösser ist, als er nach den Beobachtungen sein
sollte. Wenn man die 4 grössten Verbesserungen ausschliesst, so wird der wahr-
scheinliche Fehler = 0.027. Sobald an die Stelle der vorläufigen Werthe defi-
nitive treten und die hier stark abweichenden revidirt und in den Reduetionswerthen
vielleicht noch verbessert werden, wird sicher eine neue Ausgleichung kleinere Cor-
rectionen ergeben.

Betrachten wir endlich das Längennetz vom allgemeinen Gesichtspunkte, so
muss man gestehen, dass in dieser Richtung ein grosser Theil der Arbeit, welche zur
Europäischen Gradmessung gehört, gethan ist. Wenn Spanien und Portugal und noch
einige andere Staaten ihre projectirten Längenbestimmungen vollendet haben, wird we-
nig zu wünschen übrig sein.

Durch nähere Betrachtung der hinzugefügten Karten über die Längen- und Brei-
tenbestimmungen erlauben wir uns besonders für diejenigen Länder, welche ihre astro-

 
