 

|
|
|

2. die populär gehaltene Druckschrift des k. Directors Dr. von Bauernfeind
über „das Bayerische Präcisionsnivellement und seine Beziehungen zur
Europäischen Gradmessung,‘“ München 1880, Verlag wie Nr. 1;

3. die in den Denkschriften der k. Bayerischen Akademie der Wissenschaften
enthaltene und hieraus besonders abgedruckte Abhandlung desselben Ver-
fassers „Ergebnisse aus Beobachtungen der terrestrischen Refraction,“
München 1880, Verlag der k. Bayer. Akademie der Wissenschaften;

4. der Bericht des k. Directors Dr. von Bauernfeind über ‚die sechste Ge-
neralversammlung der Europäischen “Gradmessung in München“ in den
Nummern 313, 314, 316 der wissenschaftlichen Beilage des Jahrgangs 1880
der Augsburger „Allgemeinen Zeitung‘ und im 12. Hefte der „Zeitschrift
für Vermessungswesen‘; endlich

5. die Habilitationsschrift des Assistenten der k. Bayerischen Gradmessungs-
Commission, Privatdocenten Dr. M. Haid, betitelt „Untersuchungen über
die Beobachtungsfehler und die Genauigkeit des Bayerischen Präeisions-
nivellements,“ München 1880, Selbstverlag des Verfassers.

Die unter den Nummern 1 bis 3 genannten Druckschriften sind durch Vermitt-
lung des Centralbureaus sämmtlichen Herren Commissären der Europäischen Gradmessung
zugesandt worden, von Nr. 5 hat Herr Dr. Haid bei der sechsten Generalversammlung
Exemplare an alle Anwesenden vertheilt und Nr. 4 wurde theils durch die „Allgemeine
Zeitung“, theils durch die „Zeitschrift für Vermessungswesen“ Jedermann zugänglich
gemacht.

An praktischen Arbeiten wurden im Laufe des Jahres 1880 folgende aus-
geführt:

1. Eine vierte Reihe von Refractionsbeobachtungen zwischen dem Döbraberge
im Bayerischen Frankenwalde und dem Kapellenberge im Sächsischen
Voigtlande, unter der Oberleitung des Directors Dr. von Bauernfeind und
der speciellen Leitung des Professors Dr. Schmidt. Diese Beobachtungen
sind bereits in der unter Nr. 3 genannten Abhandlung mit verwerthet.
Dem Wunsche des Herrn d’Abbadie in Paris entsprechend, wurden ferner
ausgeführt

2. Eine Reihe von Beobachtungen über die ‚Schwankungen der Lothlinie“
(les variations de la verticale), wie Herr d’Abbadie sich ausdrückt, oder
über die „Bewegungen des Erdbodens‘ (les mouvements du sol), wie Herr
Ph. Plantamour sie nennt. Diese Beobachtungen sind von Herrn Oberst
C. von Orff mit sehr empfindlichen Libellen auf der k. Sternwarte in Bo-
genhausen angestellt worden und es werden die Ergebnisse derselben wohl
mit denen der correspondirenden Beobachtungen des Herrn d’Abbadie
seinerzeit veröffentlicht werden. Weiter kam zur Ausführung

 

i
i
i

 
