 

REES

EN eee eS

a

24

Im Ganzen wurden beobachtet:
26 Sätze Z. D. südlicher Sterne,
23 0, ., „nördlicher. o,
13 Sterndurchgänge durch den 1. Vertical,
22 Sätze Azimuth (15 Sätze Cernieder und 7 Sätze mit Harterberg).

Die in vielfacher Hinsicht ausgeführten Untersuchungen des Universal-Instru-
mentes ergaben wichtige Anhaltspunkte für die Reduction der Beobachtungen.

Nachdem vom 18. bis 20. August die Reductions-Elemente erhoben und einige
Ergänzungsbeobachtungen, die das fortwährend schlechte Wetter bis dahin nicht zuliess,
ausgeführt worden waren, wurde der Standpunkt des Universale, welcher etwa 7 Meter
nördlich des trigonometrischen Punktes liegt, unterirdisch markirt und durch einen
Erdhaufen geschützt.

Vom 25. bis 29. August wurden die Beobachtungen auf der Station Cwortkowo
brdo in nachfolgender Anzahl ausgeführt:

26 Sätze Z. D. südlicher Sterne,

>» ._,., Dordliclier. ;,

9 Sterndurchgänge durch den 1. Vertical,

20 Sätze Azimuth (14 Sätze mit JZrojnas und 6 Sätze mit Æsseg).

Das Universale stand genau auf dem Punkte, wo im Jahre 1847 der Meridian-
kreis gestanden ist, die Marke wurde unversehrt aufgefunden.

Am 30. nach Erhebung der Reductions-Elemente wurde der Markstein wieder
über die unterirdische Marke gesetzt.

‚Am 3. September wurde mit den Beobachtungen auf Peterwardein begonnen und
am 8. diese Station vollendet. Es sind daselbst beobachtet:

39 Sätze Z. D. südlicher Sterne,

3, .. .. „ aöxdlicher ..

16 Sterndurchgänge durch den 1. Vertical und
24 Sätze Azimuth.

Nachdem der'trigonometrische Punkt Csurog Thurm nur sehr selten gut sichtbar
war, so wurde das Azimuth mit dem sehr gut einstellbaren Thurmkreuze der Spenska
erkva in Neusatz bestimmt und dann der Winkel Spenska crkva—Csurog mit Hilfe eines
auf letzterem Thurme aufgestellten Heliotropen 48mal gemessen. Das Observatorium
musste der vorhandenen Culturen wegen etwa 10 Meter S.W. des trigonometrischen
Punktes placirt werden, und sind die Reductions-Elemente erhoben.

Im Ganzen wurden im Sommer 1880 beobachtet:

176 Sätze Z. D. a& 6 Einstellungen
38 Sterndurchgänge durch den 1 Vertical und
66 Sätze Azimuth.

 

da ah he a a

 
