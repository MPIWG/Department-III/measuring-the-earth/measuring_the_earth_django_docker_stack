MRC

 

mal I m

U

Wh

OOOO ga

Be

45

 

Déjà, en 1861 et 1862, après plusieurs tentatives infructueuses, des Ingénieurs
Anglais et des officiers d'Etat major français avaient relié entr’elles, par dessus le dé
troit du Pas de Calais, les triangulations de la France et de la Grande-Bretagne.
Bientöt apres, les triangles du réseau Espagnol, ramifiés dans toute la péninsule et
poussés jusqu’aux Sierras qui font face au continent africain, venaient se souder par
dessus les Pyrénées, avec l'extrémité australe de la Méridienne de France. De l’autre
côté de la mer, l'Algérie devenue Française, était triangulée. Il ne restait donc plus
qu’à passer d'Espagne en Algérie, c’est-à-dire à franchir la méditerranée pour porter
l'arc Anglo-Franco-Espagnol jusqu'au désert du Sahara et, plus tard, jusqu'au coeur
même de l'Afrique.

Biot et Arago, à leur retour d'Espagne, avaient entrevu la possibilité d'exécuter
ce travail, mais dans un lointain avenir, si jamais, disaient-ils, la civilisation pouvait
s'implanter sur ces rives africaines qu'Arago avait trouvées si peu hospitalières. Dès
cette époque, on soupçonnait la visibilité réciproque des côtes d’Espagne et d'Afrique.
Plus tard, lorsque les triangulations d'Espagne et d'Algérie furent entreprises, les géodé-
siens des deux pays purent se convaincre à leur tour, que cette visibilité était incon-
testable entre lAlgérie et Espagne et qu’on pourrait réunir les deux pays par de
grands triangles méditerranéens; le jour ot il serait possible de produire des signaux
perceptibles a des distances voisines de 300 kilométres.

En 1858, M. M. Ibanez et Laussédat réunis & Madrid, s’étaient préoccupés de
cette question importante, mais sans avoir les moyens nécessaires pour formuler un projet défini.

Bientôt après, 1862, le Colonel Levret publia une notice relative à la jonc-
tion des deux pays.

Enfin, en 1868, M. Perrier, occupé à trianguler la partie occidentale de la pro-
vince dOran, put distinguer plusieurs fois, de plusieurs sommets, nettement à l'œil nu,
les crétes dentelées des Sierras d’Espagne, en visa les principaux pics et proposa un
polygone de jonction dont la possibilité était hors de doute, dans lequel toutefois l’un
des sommets recoupés en Espagne était mal dénommé, par suite de l'absence d’une
bonne carte. |

Comme on le voit, il y a déjà longtemps que, de part et d’autre, on se pré-
parait à exécuter cette opération, mais ce n'est qu’en 1878 que les Gouvernements
d’Espagne et de France ont jugé le moment venu d’en entreprendre l'exécution définitive.

Nous venons aujourd'hui dire à notre association, en notre nom et au nom de
tous nos Collaborateurs, que la jonction des deux pays est enfin réalisée, lui faire con-
naître les moyens mis en œuvre et lui rendre compte des résultats obtenus.

En nous appuyant sur les renseignements recueillis de part et d'autre du dé-
troit et sur les reconnaissances antérieures nous avons arrêté en 1878 le choix de
quatre points ou sommets dont deux situés en Espagne et deux en Algérie, pour former
le quadrilatère de jonction.

Ces points sont: en Espagne, le Mulhacen, point culminant de la Sierra Nevada
dont l'altitude est de 3482 m. et le Tetica de Bacarés (2080 m.); en Algérie, le Djebel

 
