 

32

 

 

 

 

 

 

 

 

 

| Italien.
SS  ——— — — —  —
0 Indication des fe Longi- : Directeur ‘ |
| Points. me tude. une. et Observateurs. nat | Remarques.
Wis | Soleto.......°. Au? 97) 35.52.19”) 1872 Ing. d’Atri. |Repsoldd.27em.
146|Serrano....... A 10 5786 118 1872 id. id,
147 | Torre dell’Orso . . | 40 16 26 36 5 45 1872 2" id. |
148 | Specchia Cristi. .|40 3 43 ‚86 7 24 1802 id. |
149 | Paglierone Russo . | 39 55 42 | 35 57 4 1872 id. |
| 150 | Li Specchi . . . .. 39 57 32 | 35 43 42 1872 id, |
151 | St. Eleuterio. . . .|40 3 2 |35 49 1 1872 id.
152 | Madna dell’Alto ..|40 8 44 | 35 38 45 1872 Capit. Maggia. : Starke de27 cm.)
153 | Torre Scanzano. .| 40 15 22 | 34 24 38 1873 id. | id. |
154 |Nocara ...... 40 5 59 34 8 31 1873 id. nn id. |
155 | Stornara . . . . .. 39 59 53 | 34 15 28 1872 id. id. |
| 126 leAlpi....... 10 7 12 33 38 54 1870-71 | Capits. Maggia, Colucci, id. |
| doy lagola.. ...... 39 54 10 | 88 82 6 1871 Lieut. Mottura. |Pistor de 27 cm. !
158 | Pollino . . . . ... 33 542 25 | 88 61 11 1871 id. ‘Starke de 27 cm. i
159|Mostarico...... 39 53 16 34 9 35 1871 Capit. Magoia, | id. | ;
160 | Buffaloria ..... 39 44 52 |34 6 42 1871 id. | id.
161 | Schiavonia . .'. . .| 39 39 3 | 34 12 17 1871 id. | id. |
160a | Base de Crati. . .| 39 43 43 |34 4 18 1871 Capit. Maggia, Lieut. Pagano. |Pistor de 27 cm.|
(Terme NO.) | |
160b id. 39 42 49 |34 5 58 1871 id. | id. |
| (Terme SE.) | | |
|: 162 | Cassano. . . .... 39 46 48 | 33 57 32 1871 id. | id. |
168 | Pollinara . . . . .. 83941 6 34 2 54 1871 id. | id. |
164 | Montea . . ... … .| 89 39 23 | 38 86 37 1871 Lieut, Mottura, ‘Starke de27 cm.|
165 | S. Salvatore... ./ 39 39 23 | 33 58 5 1871 Capit. Maggia. |Pistor de 27 cm. !
166 | Torre Trionfo . . .| 39 37 14 | 34 25 37 1871 Ing. d’Atri. (Repsold d.27cm |
167 | Terravecchia . . . 39 27 50 | 34 36 43 1871 id. | id. |
168 | Cozzo Sordillo. . .| 39 25 13 | 34 15 33 1871 id. - | id. |
169 | Castellara ..... 39 25 11 | 34 0538 1871 id. | id.
170 | S. Nicola dell’Alto | 39 17 10 | 34 38 38 1 1869 id, | id. |
171 | Montenero . .... 39 13 24 | 34 15 49 1871 Capit. Maggia. Pistor de 27 em |
172 | Bugiafro . ..... 88 56,21 5447 1 1869 Ing. d’Atri. (Repsold d.27em.| j
723 6Coeuz20....... 89 13 3 | 33 47 51 1869 id. id. | 1
174 Serralta . : . . .. 38 45 16 34 2 1 1869 id. id. |
175 Gremi.......- 38 25 40 | 33 58 43 1869 id. id. I
116 Bor0.......... 38 36 9 | 33 34 30 1869 id. id. |
177 | Stromboli. . . ... 88 46 40 | 32 53 24 1865 Capit. Marangio, Lieut. Simi. |Pistor de 40 em.
178 |Montalto . ..... 38 926 [83 85 4 1869 Ing. d’Atri. Repsoldd.27cm.|
179|Castania...... 38 15 55 |83 11 5| 1865 Major Chi. Pistor de 27 cm.|
150 Milazzo....... 88 16 9 |32 53 43 | _ 1865 Capit. Marangio. Pistor de 40 cm.|
181 | Isola Lipari . ...|38 29 19 | 32 35 52 1865 id. Gambeyd.27cm.|
Pistor de 27 em.
182|Poverello...... 38 454/88 1 41 1865 Major Chio. id. |
183 |$S. Angelo di Patti | 38 5 57 | 32 35 4 1865 Capit. de Vita. | id. |
i8s4 M Eina....>.:. 37 45 48 | 32 89 2 1865 Capits. de Vita, Marangio. |Gambeyd.27cm.
15 300... .......: 87 oo 49 |82 21 33 1865 |
186 | Sta. Croce ..... 37 58 31 | 32 2 50 1865 |
187 | S. Salvatore. . . .| 37 50 15 |81 43 8 1871 Capit. Maggio, Pistor de 27 cm.|
188 | Sambuchetta ...|37 50 2/82 2 15 1871 id. id. |
189 | Casana . . . . ... 87 48 4 82 17 19 1871 id. id. |
190 | Altesina . ..... 87 40 16 | 31 57 41 1871 it “id |
191 | Montirossi . . . .. Bi ar 5 82 40 84 1865 Ing. d’Atri, Capit. de Vita. Reichenbach de i
32 cm. | |
102 8imeto .. ..... 37 25 14 | 32 45 20 1865 id. id. | |
193|Perriere ...... 87 23 59 | 82 27 42 1865 id. id. |
194 | Montagna . . . ... 37 24 3 32 5 53 1865 Capit. Marangio. Pistor de 40 em.
195 | Grecuzzo . . .... a ld 0 ol 51 44 1863 Major Chid, Pistor de 27 em.
196 | Caltagirone. . . .. 37 14 20 | 32 10 36 1863 id. id. |
197 | Cavallaro. . . ... 87 7 18 | 82 48 53 1865 Capit. Marangio. Pistor de 40 em.
108 MANTA... .. af 006 98229 6 1865 id. id. |

 

 

 

 

 

 

 

 

 

 
