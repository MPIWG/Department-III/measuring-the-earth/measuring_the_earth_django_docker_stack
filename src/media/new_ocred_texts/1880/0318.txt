 

|
|
i
I
i
h

10

Beigel, 6. W. S. Ueber die trigonometrische Vermessung von Bayern. (Zach, monatl.
Corr. 10.) 1803.

Haid, M. Untersuchung der Beobachtungsfehler und der Genauigkeit des bayerischen
Pricis.-Nivellements. München, 1880.

Henry. Ueber die Landes-Vermessung von Bayern. 1802. (Zach, monatl. Corr. 6.)

Lamont, J. von. Astronom. Bestimm. der Lage des bayerischen Dreiecksnetzes auf dem
Erdsphäroid. (Sitz.-Ber. der math.-phys. Classe der k. bayer. Akad. der
Wiss. Jahrg. 1865, S. 28—66.)

— Astron.-geodätische Bestimmungen für einige Hauptpunkte des bayer. Drei-
ecksnetzes. (Zehnter Supplem.-Band zu den Annalen der k. Sternwarte
zu Bogenhausen bei München, und theilweise in der „bayer. Landesver-
messung“.)

Orff, ©. von. Bestimmung der geograph. Breite der königl. Sternwarte bei München
nach der Talcottschen Methode und mit dem Passagen-Instrumente im
ersten Vertical. Beilage zu den Annalen der königl. Sternwarte, mit einem
Vorworte von dem Conservator Dr. J. v. Lamont. München, 1877.

_ Astronom.-geodätische Ortsbestimmungen in Bayern. Nach Beschluss die
königl. bayer. Commission für die europäische Gradmess. unter Oberleitung
ihres Mitgliedes des Herrn Prof. Dr. von Lamont. München, 1880.

Orff, von, und Bauernfeind, von. Die Bayerische Landesvermessung in ihrer wissen-
schaftlichen Crundlage. Mit höchster Genehmigung von der königl. Steuer-
Cataster-Commission in Gemeinschaft mit dem topographischen Bureau des
k. Generalstabes herausgegeben. München, 1873. Akad. Buchdruckerei
von F. Straub. Enthält alle geodätischen und astronomischen Coordinaten,
so weit sie berechnet sind, und eine vollständige Uebersicht des Haupt-
Dreiecksnetzes mit Angabe der Namen aller Punkte.

Osterwald, P. von. Bericht über die vorgenommene Messung einer Grundlinie von
München bis Dachau. (Abhandl. d. k. bayer. Akad. d. Wiss. Il. Classe, 1764.)

Rothe, H. A. Ueber Pendelschwingungen in grösseren Bogen. (Kastner’s Archiv, II, 1824.)

Schiegg. Ueber die Vermessung von Bayern nebst Bemerkungen von Zach. (Monatl.
Corr. 10). 1804.

Schwentner, D. Geometriae practicae novae libri IV. Nürnberg, 1625—26.

Schwerd, F. M. Die kleine Speierer Basis, oder Beweis, dass man mit einem geringen
Aufwande an Zeit, Mühe und Kosten durch eine kleine genau ge-
messene Linie die Grundlage einer grossen Triangulation bestimmen kann.
Speier, 1822.

Seidel, L. Ueber ein Verfahren die Gleichungen, auf welche die Methode der kleinsten
Quadrate führt, sowie lineare Gleichungen überhaupt durch successive
Annäherung aufzulösen. (Abhandl. d. k. bayer. Akad. II. Classe, Bd. XI
Abth. 3, 1874.)

het kw all add Lan à là aout

 

|
i
|
j

 
