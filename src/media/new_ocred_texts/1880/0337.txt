BU YT ern

WW 01

IPP TENT

Ps

I RL AW

Walker, J. T. On the Indian Pendulum Observ. (Roy. Soc. Proc. XIX, pp. 97—105.)

— Admin. rep. great trigonom. survey of India 1872—3, p. 25.

The like f. 1873—74, pp. 30-1. (Heaviside, Capt. W. S. Append.
pp. 53—9.)
The like f. 1874—75 pp. 23—4,
The like £. 1876—77, p. 25. Herschel, (Major) John. (App. pp. 63—4.)
Waller. The measure of the earth being an account of several observations made by
the members of the acad. at Paris. London, 1688.
Warren, J. An account of experiments made at the observatory near Fort St. George
for determining the length of the simple Pendulum beating seconds of
time etc. (Asiat. researches XI, 1810, ‘pp. 2922-308)
Wright, E. The correction of certain errors in navigation. London, 1599. 24 edit.
1610, 3° edit. (v. J. Moxon) 1657. Enthält das von ihm zum zweiten
Male aufgefundene oder zuerst richtig aufgefasste Princip der Mercator-
Projection.
Young, T. Calculation of the direct attraction of a spheroid and demonstration of
Clairaut’s theorem. (Nicolson, Journ. XX, 1808, pp. 208—14.)

_ Remarks on the probabilities of error in physical observations and on the
density of the earth considered especially with regard to the reduction of
experiments on the pendulum. (Phil. Trans. 1819, pp. T0—95.)

_- Remarks on Laplace’s latest computation of the density and figure of the
earth. (Quart. Journ. Sc. IX, 1820, pp. 32—5.)

~~ The resistance of the air, determined from Capt. Kater’s experiments on the
pendulum (Anonymous). (Quart. Journ. Sc. XV, 1823, pp. 351—6.)

— Considerations on the reduction of the length of the Pendulum to the Level
of the Sea. (Quart. Journ. Se. XXI 1820, DD 100 5),

— Note on Professor Svanberg’s reduction of the length of the Pendulum. (Quart.
Journ. Sc. XXII, 1827, pp. 365—7,) ;

7. Frankreich.

Abbadie, A. de. Resumé géodésique des positions déterminées en Éthiopie. Leipzig, 1859.

— Géodésie dune partie de la haute Éthiopie revue et redigée par R. Radau.
Paris, 1860.

— Géodésie d’Ethiopie ou triangulation d’une partie de la haute Éthiopie exé-

cutée selon des méthodes nouvelles par A. d’Abbadie verifiée et redigée par
R. Radau. Paris, 1873.

 
