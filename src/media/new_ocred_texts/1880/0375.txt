PRE OT

PEUR

AA GA à «À dal au
Tipe ALTE

TEEN INT A
TE ER RT OFT

67
Maassvergleichungen des geodätischen Instituts. Erstes Heft. Berlin, 1872. Enthält:
Anfertigung der Copien No. 12, 13 und 18 von der Bessel’schen Toise und

Vergleichung derselben und der italienischen Toise von Spano mit der Bau-
mann’schen Copie No. 10. Beeyes

}

Astronomische Bestimmungen für die europäische Gradmessung aus den Jahren 1857
bis 1866. Herausgegeben von Baeyer. Leipzig, 1873.
Bestimmung der Längen-Differenz zwischen Berlin und Wien, auf telegraphischem Wege

ausgeführt von Foerster und Weiss, herausgegeben von C. Bruhns. Leipzig,
1871.

_ Bestimmung der Längen-Differenz zwischen Berlin und Lund, ausgeführt von dem Central-

: bureau der europäischen Gradmessung und der Sternwarte in Lund i. J. 1868.
Herausgegeben von C. Bruhns. Lund, 1870.

Beobachtungen mit dem Bessel’schen Pendel-Apparate in Königsberg und Güldenstein
von CG. FE. W. Peters... Hambure, 1874

Das Präcisions-Nivellement. Erster Band, Arbeiten in den Jahren 1867 bis 1875
(Von Börsch, Sadebeck u. Seibt.) Berlin, 1876.

Das Rheinische Dreiecksnetz. Erstes Heft, die Bonner Basis. Mit einer Dreieckskarte.
(Von Baeyer, Sadebeck und Fischer.) Berlin, 1876.

Zweites Heft. Die Richtungsbeobachtungen. (Von Bremiker und Fischer.)
Berlin, 1878.

Astronomisch-geodätische Arbeiten i. J. 1875. Enthält: I. Instruction für die Polhöhen-
und Azimuthbestimmungen. — II. Bestimmung der Polhöhe und des Azimuths
auf Station Hercules bei Cassel. — III. Bestimmung der Polhöhe auf den Sta-
tionen Schildberg, Osterode, Hils, Langelsheim, Mansfeld, Monraburg, Dollmar,
Heldburg, Harzburg, Dienkopf, Craula, Pfarrsberg, Eckartsberga, Sachsenburg,
Kyffhäuser u. Lohberg. Mit einer Uebersichtskarte der Lothablenkungen im
Harz. (Von Albrecht.) Berlin, 1876.

/ Maassvergleichungen. Zweites Heft. (Von Baeyer und Sadebeck.) Berlin, 1876.

Astronomisch-geodätische Arbeiten i. J. 1876. Enthält: I. Instruction für die Längen-
bestimmungen des geodätischen Instituts. — II Bestimmung der Längen-
7 differenzen zwischen Berlin und Strassburg, Mannheim und Strassburg, Strass-
burg und Bonn. — II. Bestimmung der Polhöhe und des Azimuths auf

Station Feldberg im Schwarzwalde. (Von Albrecht.) Berlin, 1877.

Astronomisch-geodätische Arbeiten i. J. 1877. Enthält: Bestimm. der Längendifferenzen
V zw. Berlin u. Paris, Berlin u. Bonn, Bonn u. Paris. (Von Albrecht)
Berlin, 1878.
Präcisions-Nivellement der Elbe. Auf Veranlassung der Elbstrom-Baubehörden von
Preussen, Mecklenburg und Anhalt ausgeführt von Wilhelm Seibt. Berlin, 1878.
gr

 
