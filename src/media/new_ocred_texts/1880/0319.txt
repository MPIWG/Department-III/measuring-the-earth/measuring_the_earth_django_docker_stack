nn

nn me

11
Seidel, L. Ueber die Berechnung der wahrscheinlichsten Werthe solcher Unbekann-
ten, zwischen welchen Bedingungsgleichungen bestehen. (A. N. 2005
u. 2006.)
Seyffer, K. F. von. Polhöhe von Göttingen. (Bode’s Jahrb. 1797.)

— Länge von Göttingen, Berlin, Gotha, Danzig und Harefield, aus d. Sonnen-
finsterniss vom 5. Septbr. 1793 bestimmt. (Ibid. Auch für sich.) Göt-
tingen, 1794. |

-- Ueber die Höhe des Meissners. (Zach’s mon. Corr. I, 1800.)

a Länge und Breite von Hannover. (Ibid. 1803.)

— De altitudine speculae astron. regiae prope Monachium. (Denkschr. d. Akad.
Münch. I, 1808.)

— Super longitud. geogr. speculae astron. regiae Monachii. (Ibid. I, 1808
et 11, 1809.)

— De positu basis et retis triangulorum per totam Bojariam porrectorum.
(Ibid. EHI, 181m. 2.)

Soldner, J. von. Vorschlag zu einer Gradm. in Afrika. (Zach’s monatl. Corr. IX, 1804.)

— Ueber die Länge von München. (Ib. XXVI, 1812)

_ Bestimmung des Azimuths von Altomünster u. s. w. (Ib. XXVIII, 1813.)

— Ueber die schwedische Gradmessung. (Bode’s Jahrb. 1806.)

Späth, J. L. Die höhere Geodäsie. München, 1816.

— Practische Geometrie. Nürnberg, 1819.

Steinheil, C. A. von. Neuer Reflectionskreis mit Prismen statt Glasspiegel. (Schumach.
astr, Nachr, IF, 18257)

— Bestimmung der Längenausdehnung fester Kôrper durch Abwägungen. (Denkschr.
Münch. Accad. XVI, 1843.)

—- Copie des Meter des Archives à Paris. (Abhandl. der mathem.-physik. Cl.
der k. bayer. Akad. d. Wiss. Band IV, Abtheil. I, Jahrg. 1844.)

— Das Heliotrop von Steinheil. (Astr. Jahrb. v. Schum., 1844.)

— Das Messrad zu Basismessungen. (A. N. 1728.)

— Vergleichung der Leistung des Bessel’schen Längen-Comparators mit der des
Fühlspiegel-Comparators von Steinheil. (Sitz.-Ber. d. k. bayerischen Akad,
d. Wiss. Math.-phys. Cl., 1868, Dec. 5.)

— Copie der Bessel’schen Toise du Pérou in 2 Glasstiiben. Wien, 1869. (Vor-
gelegt in der Sitz. d. math.-naturw. Cl. der k. k. Akad. zu Wien, 1869,
April 15.)

— Der Comparator von Steinheil zur Vergleich. der Toise mit dem Meter und
zur Bestimm. der absoluten Längenausdehnung der Maassstäbe. (Gen.-
Ber. f. 1869, 5. 73—80 und Gen.-Ber. f. 1870 S. 51-53.)

Trew (Treu), A. Manuale geometriae practicae ete. Nürnberg 1636.
— Disputatio de immobilitate terrae contra Copernicum. Altdorf, 1636.

9%

FL OTTER RPT BOTTLE EET FORT! tt

 
