{YRS seen wer se er an

 

a pi

PEU LA Bae | Vk a Fw 7

a

127 » | oo
Sur la petite carte ci-jointe*) on peut voir le tracé de nos chaînes géodésiques
du premier ordre et l'indication des points où quelques observations astronomiques ont

été faites. La carte est disposée d’après le modèle recommandé par le Bureau central.

x

Nivellements de précision. Trois lignes ont été nivelées & double pendant la

campagne de 1876:
1. de Madridejos & Cérdoba et Cadiz, avec un troncon a la base d’Arcos
de la Frontera,
. de Sigiienza & Zaragoza, Canfranc et le Somport, sur la frontière française,
de Zaragoza à Barcelona, la AU e et. le „Perthus, egalement sur la
frontiere française.

La longueur totale de ces trois lignes est de 1534 kilomètres, qui ont été nivelés
deux fois, en laissant 1552 repères, dont 319 en bronze. Il serait à désirer qué les deux
repères en bronze que l’Institut géographique d'Espagne a établi sur la ligne frontière
avec la. France soient reliés le plus tôt possible au nivellement de précision de ce pays.

Co RD

La première ligne forme, avec celles d’Alicante & Madrid et d’Albacete @ Bailen,

un nouveau polygone Bailen—Albacete—Madridejos—Bailen de 609 kilometres de déve-
loppement, dont l’erreur de u représentée par la difference de deux altitudes du
repere de Bailen est:

Par la liene d’Albacete,a Ballen 5. | 348°757 + 07034
Par la ligne de Madridejos & Cadiz . . . . 348.882 7 0.021
Différence 0.075 + 0.053

ce ga répond 4287 V4.
Le travail de nivellement de précision fait depuis le commencement nous à déjà

fourni les altitudes de 3 bases mesurées, le moyen d'étudier plus tard la différence de

niveau entre l'Océan et la Méditerranée et la possibilité de rattacher notre réseau hypso-
métrique à celui des autres nations continentales. La longueur totale nivelée:à double
est de 4041 kilomètres avec 4108 repères, dont 756 en bronze.

L'Institut possède maintenant 10 niveaux de précision et autant d’observateurs

bien exercés dans cette sorte de travail, lequel sera continué pendant les prochaines
campagnes géodésiques.

XN

Maréographes. Celui de Santander a commencé à fonctionner a la fin.de beie

Les difficultés qui se sont tout d’abord présentées & cause de la violence des vagues et
de l’entraînement du sable ayant été surmontées, j'espère que cet appareil continuera,
ainsi que la station météorologique installée à côté, à donner sans interruption les
meilleurs résultats.

Les travaux préliminaires commencés au mois de Septembre dans le but d’etablir
un nouveau maréographe à Câdiz viennent d’être terminés par la rédaction du projet

des constructions à exécuter. Nous aurons de la peine à réussir, mais j'espère que nous
parviendrons à installer l'appareil de sorte qu’il fonctionne régulièrement. La petite carte **)

*) Diese Karte wird den Herren Commissaren besonders zugesendet werden.
**) Desgleichen.

 
