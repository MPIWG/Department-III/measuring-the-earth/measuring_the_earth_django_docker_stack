 

dann die Winkelmessung der Dreiecke als gut zu betrachten ist; ist der wirkliche Fehler
gleich dem wahrscheinlichen, dann ist die Winkelmessung mittelmässig; ist aber der
wirkliche Fehler $rösser, als der wahrscheinliche, dann ist die Winkelmessung mindestens
mangelhaft. Die Kenntniss der wirklichen Fehler ist desshalb gewiss sehr wünschens-
werth. Ich stelle daher den Antrag:
Die permanente Commission möge eine besondere Commission aus
erfahrenen Praktikern und anerkannten Theoretikern ernennen, mit dem
Auftrage , diese Fragen gründlich zu erörtern und der nächsten allgemeinen
Conferenz Bericht zu erstatten.“ |

Herr General Daeyer fügt hinzu, es ‘sei nach seiner Ansicht unerlisslich, dass
die Gradmessungs-Arbeiten auf übereinstimmenden Grundlagen beruhen. In der ersten
Zusammenkunft von 1864 sei man übereingekommen, dass eines der Ziele der Vereinigung
sei, in den von den verschiedenen Ländern gelieferten Materialien die gröstmöglichste
Gleichartigkeit zu erreichen. Als Präsident des Central-Bureaus müsse er es als seine
Aufgabe betrachten, zu untersuchen, was die vorhandenen Triangulationen zu wünschen
lassen. Er verwahrt sich gegen den Vorwurf, irgend etwas unnützes oder unzweck-
mässiges beantragt zu haben; er habe es für seine Pflicht gehalten, ein Einverständniss
über so schwierige und hochwichtige Fragen zu erzielen.

Herr von Bauernfeind bedauert, dass die beiden Punkte 5* und 5°, um welche
es sich handelt, so zu sagen ex abrupto und ohne die Mitglieder der Commission zu
befragen, auf das Programm gesetzt worden sind. Er glaubt, dass man eine gewisse
Initiative den Männern lassen müsse, welche die verschiedenen Triangulationen leiten.
Er sieht den Nutzen der Ernennung einer Commission, welche von Herrn General Baeyer
vorgeschlagen worden ist, nicht ein, sondern hält es vielmehr, da die bisherigen Publi-
cationen des Central-Bureaus immer mit Anerkennung aufgenommen worden sind, für
wünschenswerth, dass statt der besonderen Commission das Gentral-Bureau seine Ansichten
über die aufgestellten Fragen mittheile,. welchen dann Jeder, so weit es ihm zweck-
mässig erscheinen wird, Rechnung tragen könne. |

Herr von Bauernfeind stellt in diesem Sinne einen formellen Antrag; wenn aber
die Spezial-Commission angenommen würde, so beantrage er, dass diese Commission ihren
Bericht den Mitgliedern vor der nächsten Conferenz zusende.

Herr. General Baeyer ist der Ansicht, dass die Fragen der wissenschaftlichen
Geodäsie von competenten Commissionen mit Nutzen erörtert werden können; zwar könne
man den Bevollmächtigten nicht die absolute Verpflichtung auferlegen, Regeln, welche
von der Majorität aufgestellt worden sind, als unumstössliche zu befolgen; indessen würde

das angestrebte Ziel gewiss verfehlt werden, wenn man nicht die Conferenzen benutzen :

wollte, um zweifelhafte Fragen der höheren Geodäsie zu besprechen. Was ihn anbetrifft,
so befolgt er bei seinen Arbeiten die Vorschriften, welche er für die besten hält, aber er
hat kein persönliches Interesse daran, dass die verschiedenen Commissare die Regeln
des Central- Bureaus annehmen. Er hält es nur für seine Pflicht, die Wissenschaft zu
fördern, und für zweckmässig, die Untersuchung der beiden Punkte, über welche verhandelt
wird, einem Special-Comité anzuvertrauen. |

   

pe bead 1 Bi

(aby tity)

 

2.) semaines mimes Si |

 
