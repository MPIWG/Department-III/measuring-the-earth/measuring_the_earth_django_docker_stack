 

dil

La latitude du Brocken est |
d’après l'observation directe d’ — 51° 48’ 10°59
d'après le caleul géodésique d =51 48 1.41
"ag + 9718
La différence de longitude entre le Brocken et le Seeberg est
d’après l'observation directe # = 0707 26/4157 à ou
d’après le calcul géodésique À — 0 0 26.986
N— dr = — 0229 = — 31435 ©

Il en résulte apo que le zénith observé est situé par rapport au zenith
vrai sous l’azimut de 13° 36"48 et qu'il en est distant de 94224,

_Ces calculs ont encore donné les résultats suivants:

 

 

 

 

 

en nn Azimut observé. mo a . Azimut corrigé.
i Fallstein DT. Bass: #280, 410541 00) 2 3, 2. | 1° 30° 38476
Woldsberg (Mire). . . 2... 1.04808 24%, 2 10 2.1.0.2
| Magdeburg (Tour)... . |. 62 12 55.197,91, 2,4) 2040) © 2 20:
Hoppel..%; P... :. 19, 41 96.918 .91,,53 2 44) ee
Petersberg T. P.. . .|103. 8.58.474|.90,,54 21/20 0 0 2]
; Burkergroda IP. . .|183 54 5.313 90, 59, a Bun
Seeberg, T,-P,-.,:.,. 175: 18 29,665), 90 50 27 | - 297 1 200
Inselsberg ,T. P..: .::, 185 48:.46.569 90 32 1, ac nn 221

 

 

 

Struch 6 0. 200.008 40) 53200) 00 AU — 2.112 | 198.20 51.118

[ On voit que l'influence la plus forte existe pour l’angle entre Woldsberg et le
E Hoppel, pour lequel elle atteint 0402. | |

2) Mr. Seibt a cherche & établir pour Swinemünde la différence entre la
moyenne annuelle de la hauteur de la mer, telle qu’elle résulte des lectures faites chaque
jour à midi à V’échelle ordinaire, et la moyenne fournie par le maréographe enregistreur,
Il a trouvé pour la hauteur moyenne de l’eau au dessus du point 2670:

UE ANT

 

 

 

 

 

 

Année. ee ee oe Différence.
Poul EO on 07378 + 187
1872 0.942 0.949 + 7-
1873 1.024 1.02% — 2-
1874 £2071 1 072 ot
1875 0.957 0.947 + 10-
Moyenne. 0986 03993 27. pas ia

2*

(1 AU

&
=
=
=
=
re
ë
=
F

 
