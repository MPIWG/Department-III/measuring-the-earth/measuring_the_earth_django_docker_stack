HS

 

ALU A HR

OORT TR TARE INT

x

ee

—(h +h + 2 = + 34 — 2B — C:

th +h — mom) — 24 +4B— C—D

+ (AU + A™ + pv) — A— B+30— D
+ (hv — h*) — B-— C+2D

Celles-ci conduisent à des cotes satisfaisant le plus possible à l’ensemble des
déterminations. '

|

I

|

Tous les cheminements n’ont pas été faits avec les mémes soins ni dans les
circonstances également favorables, la précision de chacun d’eux est différente et des
lors il faut, dans le calcul, les affecter de poids proportionnels aux carrés des mesures
de précision ou inversement proportionnels aux carrés des erreurs moyennes. Une diffi-
culté sérieuse se présente ici; en effet la précision sera fonction de l’habileté de l’opé-
rateur, de l'instrument, des mires, du soin mis à étalonner les mires et de la nature de
l’étalon, de la grandeur des coups de niveau, de leur plus ou moins d'égalité, de la
direction du vent, de l’époque de l’année, etc. On conçoit facilement que si dès le début
les instructions n'ont pas été formulées en vue de la compensation finale, si surtout l’on
n’a pas eu le soin de comparer entre eux les instruments et les mires à mesure que des
perfectionnements étaient apportés dans leur confection, on conçoit l'impossibilité maté-
rielle de calculer avec exactitude les erreurs moyennes. Très-heureusement les effets des
causes influant sur la précision des nivellements se manifestent dans le calcul de la cote
qui reflètera, pour ainsi dire, les précautions prises par l'opérateur et la bonté des
instruments. ;

La méthode employée, la seule pratique, est dite „depuis le milieu“; elle rend
les observations à peu près indépendantes des erreurs instrumentales et de l'influence
de la dépression de l'horizon et de la réfraction. L'opérateur avance en marquant à des
distances de mille mètres environ des points qu'il pourra retrouver au retour, et les
différences des deux cotes obtenues pour ces points kilométriques seront les erreurs
commises successivement. En divisant par leur nombre la somme des carrés des diffé-
rences, on aura le carré de l'erreur moyenne kilométrique dont le rapport inverse repré-
sentera le poids ou limportance du cheminement.

On rendra les différences de niveau comparables entre elles en les ramenant à
la même unité de précision, c’est-à-dire en les multipliant par les racines carrées des
poids. Les équations seront alors:

AV = 4 BVr — 4»
+ MVp = + BVp: — AVm etc. etc.

et les équations normales deviendront:

+ DEP Er) A4 Gp) 800 à

ponent (p +p) A+ (p + pt + pm +p) B—p™ C—p" D

PE ADD + (pt 0 En) 3
== p D #4 0 2 0 SO

— (ph + ph 4 p" he)

+ (ph + ph —p™h™ — ph")
—- (po he + pa het + pr Ju)

+- Co hiv — pr i)

|

ll

 

 
