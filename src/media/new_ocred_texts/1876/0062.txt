 

arena nase ani 2

en

Se nn

TU

52
Bevollmächtigten der betheilisten Staaten, dem Central-Bureau die erforderlichen Er-
ginzungen der Angaben gefälligst zusenden zu wollen.

Herr Bruhns entschuldigt sich gegen Herrn Vellarceau wegen der Weglassung
seines Namens bei den Beobachtungen der Breiten und Azimuthe auf den französischen
Stationen, welche daraus entstanden sei, dass im Generalbericht einfach ein gedrucktes
Blatt wiedergegeben sei, welches aus Paris eingesandt worden, und auf welchem der
Name des Herrn Villarceau nicht vermerkt war. i

Herr Bruhns spricht ferner den Wunsch aus, dass man nicht vergessen möge,
in den Verzeichnissen der astronomischen Beobachtungen die Positionen der Sterne
anzugeben, welche bei den Bestimmungen benutzt sind.

Der Präsident eröffnet hierauf die Discussion über den Punkt 5 des Programmes
(Diseussion über verschiedene wissenschaftliche Fragen).

Herr General de Vecchi schlägt vor, jede Discussion über diesen Gegenstand zu
unterlassen, um nicht die Freiheit der Bevollmächtigten zu beschränken. Er bedaure die
Aufnahme dieser Punkte in das Programm, welches nur fünf Wochen vor der Zusammen-
kunft vertheilt worden sei; wesshalb er die italienische geodätische Commission nicht habe
zu Rathe ziehen können, deren Meinung über die Frage zu erhalten ihm nöthig
scheine. Er glaubt nicht, dass es zweckmässig sei, in den gewöhnlichen Conferenzen der
permanenten Commission über Punkte von einer so hohen Wichtigkeit zu beschliessen,
er würde sogar zu der Lösung solcher Fragen durch vollzählige Versammlungen der
Bevollmächtigten kein Vertrauen haben. Ueberzeugt, dass in jedem Lande besondere

Umstände die Art des Verfahrens bestimmen und die Uebereinstimmung der Methoden nicht

zulassen, stellt er den Antrag, über die Fragen 5* und 5° des Programmes zur Tages-
ordnung überzugehen.

Herr Hirsch weist den Vorwurf zurück, welchen die Worte des Herrn General
de Vecchi dem Bureau der Commission aufzubürden scheinen.

Die Versammlung kann sich zwar nicht das Recht anmaassen, unumstössliche
Regeln aufzustellen; aber nichts desto weniger ist es eine der wesentlichen Bedingungen
des unternommenen Werkes, dass die Elemente, welche später für die Untersuchung der

' Gestalt der Erde in Anwendung kommen werden, auf ein-und denselben Grundlagen beruhen.

Wenn die Aufnahme der Punkte 5° und 5? die Aufstellung unumstösslicher
Regeln zum Zwecke gehabt hätte, so würden die vorgebrachten Kritiken berechtigt sein;
aber es würde nur beabsichtigt, sich über einige Punkte zu verständigen, wo eine Gleich-
förmigkeit wünschenswerth ist.

Herr de Vecchi halt es für unmöglich, durch eine nothwendiger Weise hastige
und nicht gründliche Erörterung in einer Sitzung zu einer befriedigenden Lösung der
aufgestellten Fragen zu gelangen.

Herr Hirsch ist derselben Ansicht; das Bureau hat nicht daran gedacht, in der
gegenwärtigen Sitzung zur Lösung dieser schwierigen Fragen zu gelangen; aber es hat
geglaubt, dass es zweckmässig sei, diejenigen ‘Mitglieder, welche sich speciell mit der

Frage beschäftigt haben, zur Meinungs-Aeusserung aufzufordern. Das Bureau hat, indem

    

et OIL a | Rabe

Lave) mat

Lun to Way

 

z

 
