105

minore a 30,000, avuto riguardo alle osservazioni da scartarsi,
delle condizioni atmosferiche, o per altre cause che manifest
alle medesime la necessaria fiducia.

Di queste 30,000 osservazioni ora ne sono gia fatte pit di 25,000, parte nello
scorso anno e parte nel presente sino all’attuale data (Settembre): ma non pud sperarsi
di compire nel presente anno il lavoro, per la circostanza di essere di un certo numero
di stelle, per ora non visibili, mancanti non poche osservazioni o dirette o riflesse.

0 per decisa contrarietà
amente potessero togliere

I ni ove

Pubblicazioni.
In quest’anno venne pubblicata dal Professore Betocchi una Memoria sui
Mareografi esistenti in Italia.
Dall’Istituto topografico militare vennero pubblicati i secondi fascicoli di lavori
geodetici ed astronomici.
Dal Colonnello Ferrero venne compiuta la pubblicazione di un’Es posizione
del Metodo dei minimi quadrati.

Di queste pubblicazioni parecchi esemplari furono posti a disposizione dei Membri
dell’Associazione e degli Scienziati intervenuti alla riunione di Bruxelles.

:

Ottobre 1876. Il Presidente della Commissione Italiana
E. de’Vecchi.

 

Niederlande.

Le soussigné a l’honneur de présenter à la Commission permanente de l’Asso-
ciation géodésique pour la mesure des degrés en Europe réunie à Bruxelles le rapport
suivant sur l’état actuel des travaux géodésiques dans les Pays-Bas. Ce rapport peut se
faire en peu de mots, quoique les résultats obtenus représentent un assez grand nombre
de jours de travail.

: Apres avoir mesuré la base dans le Haarlemmermeer en 1867 et 1868, je me
suis occupé chaque été, autant que possible, de la révision des triangles du Général
Krayenhoff, pour autant qu'ils étaient nécessaires afin d’obtenir la jonction des triangles

t de la Belgique avec les triangles du Hanovre.

J’ai Vhonneur de communiquer ci-joint à la Commission un tableau *) de mes
triangles, qui s’étendent maintenant de Hoogstraeten et Lommel en Belgique, et de
Nederweert dans les Pays-Bas, jusqu’à la frontière méridionale de la province de Frise.

Les stations de Hindeloopen et Lemmer en Frise sont déterminées, et je me
trouve maintenant à Meppel, où j'espère terminer mes travaux en peu de jours. Je puis
encore consacrer une semaine à cette opération.

U MOTEL, Au

it

*) Diese Karte wird später publicirt werden. St.
14

i
=
=
=
=
=
=
æ
=
=
=
>

 
