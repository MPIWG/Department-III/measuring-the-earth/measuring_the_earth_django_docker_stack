 

106

La station de Hindeloopen a été choisie à la place de celle de Stavoren, où
Krayenhoff avait observé, mais où la tour a été maintenant démolie.

La détermination des directions, ou la mesure des angles aux stations de la
province de Frise et de celle de Groningen, sont encore A faire dans le courant de l’été
prochain ou peut-être en deux étés, à cause des éternels brouillards qui empêchent
presque habituellement chez nous les vues lointaines.

Au commencement de mes travaux, j'ai été assisté jusqu’en 1872 par M. A. JL. con
Hees, Ingénieur Civil. Depuis, M. P. de Wilde, Premier Lieutenant d’artillerie, a succédé
à M. van Hees, jusqu'en 1874, époque à laquelle il a été appelé pour prendre part à
l'instruction à l'École Militaire de Breda. — Pendant une période de six semaines seule-
ment, pendant laquelle M. de Wilde avait été empêché, j'ai été assisté par M. O. Gleuns,
Arpenteur du Cadastre. — Après M. P. de Wilde mon aide a été, jusqu’à la fin de 1875,
M. VIngénieur Civil A. van Chevichaven, qui m’a quitté quand il fut nommé comme
Ingénieur Provincial du Waterstaat, à Hertogenbosch. — Maintenant c'est M. A. L. J.
Bouten, Ingénieur Civil, qui m’assiste dans les opérations trigonométriques.

C’est par suite de ces changements que les calculs sont encore assez en arrière:
je les pousserai autant que possible.

Pour ‘ce qui regarde les nivellements de précision, ils s’exécutent sous la direction
immédiate de M. le Professeur L. Cohen-Stuart, Directeur de l'École Polytechnique à
Delft. Cet été ils ont été faits par deux où trois brigades de jeunes gens formés à
notre École Polytechnique.

Le point de départ du nivellement est & Amsterdam; c’est le point zero,
„Amsterdamsche Peil“, ou simplement AP; puis il s'étend jusqu’& la frontiere de Y’Alle-
magne. Les résultats déjà obtenus sont assez satisfaisants.

Meppel le 4 Octobre 1876. . F. J. Stamkart.

 

Norwegen.
(Es ist kein Bericht eingegangen).

Oesterreich.
K. K. Militair-geographisches Institut

I. Polhöhen und Azimuthbestimmungen

wurden auf den trigonometrischen Punkten „Opehina“ bei Triest und „Liezen“ in Ober-
steyermark ausgeführt.
Auf beiden Punkten war das im IN, Bande der „Astronomisch-geodätischen

%

a WEHEN

À 41 abn An A

De: setutttnns mama DA |

LL Hdi du Add

Lu hl

Lu Ltd.

 

 
