LA mes se se

 

a RT

Li

lite

=
=
x
=
®
x
=
=
=

x

Hs

1. Pola—Wien (2).
2. Pola—Ragusa.
3. Ragusa—Wien.

Pola—Wien wurde bereits im Jahre 1873 durch mich und Palisa bestimmt, ich
habe demnach bei dieser Längenbestimmung den Zusatz „(2)“ gemacht, um anzudeuten,
dass für die Längendifferenz zwei unabhängige Bestimmungen vorhanden sind.

‘Der grossen Zuvorkommenheit und Güte des königlichen Astronomen zu Green-
wich, P. b. Airy, habe ich es zu verdanken, dass uns die englische Regierung ge-
stattete, die so wichtige Längendifferenz Wien — Greenwich durch die österreichischen

Beobachter allein. bestimmen zu lassen, und es wurde uns zu diesem Ende eine auf dem

zum Greenwicher Territorium gehörigen Platze erbaute Beobachtungshütte, in welcher
wir unsere Instrumente und Apparate in geeigneter Weise unterbringen konnten, in der

liberalsten Weise zur Verfügung gestellt. Die englische Telegraphenverwaltung, die
Gesellschaft für die submarinen Kabel zwischen England und Frankreich, ferner die -

französische, deutsche, bayrische und österreichische Telegraphenverwaltung stellten uns
die zur Verbindung nöthigen Linien mit der grössten Bereitwilligkeit zur Verfügung, und
ich ergreife hier mit besonderem Vergnügen die Gelegenheit, denselben’für ihr Entgegen-
kommen den wärmsten Dank auszusprechen. Um diese fundamentale Operation möglichst
zu sichern, habe ich beschlossen, auf beiden Stationen die Uhrkorrektionen durch 2
Beobachter unter Benutzung zweier Uhren auf jeder Station in völlig unabhängiger Weise
bestimmen zu lassen und zwar -durch zwei verschiedene Methoden; nämlich erstens
in der gewöhnlichen Weise durch Meridianbeobachtungen an unseren vorzüglichen
Repsold’schen geraden Passageninstrumenten und zweitens durch die Döllen’sche Methode
im Vertikale des Polarsternes, eine Methode, deren eminente Vorzüge ich im Vorjahr bei
den Längenbestimmungen zwischen Pulkowa— Wien und Warschau— Wien kennen zu lernen
Gelegenheit hatte und meine Anfangs gegen diese Methode gehegten Bedenken behob.
Wir haben diese Methode in ganz unwesentlichem Maasse dahin abgeändert und be-
schränkt, dass die zu einer Zeitbestimmung zu. beobachtenden zwei Zeitsterne bis auf
mindestens 10 Grade gleiche Zenithdistanzen haben. Die für die Anwendung dieser Me-
thode nöthigen Instrumente stellte uns der kaiserl. russische Generalstab durch Ver-
mittelung des Direktors der Pulkowaer Sternwarte O. Sirwe und des Staatsrathes Döllen
mit der dankeswerthesten Bereitwilligkeit zur‘ Verfügung und es wurden uns 2 von
Herbst in der Pulkowaér Werkstätte- vorzüglich gearbeitete nu zu diesem Ende
überlassen.

In Greenwich beobachteten in der ersten Hälfte der ausidecnetl die im "Ganzen
auf etwa 12 mehr oder minder gelungenen Abenden beruhen, am Passageninstrumente

Oberlieutenant Alois Nahlik, am Herbst’schen Instrumente der 2. Observator der k. k. |

Gradmessung Robert Schramm, in Wien am Passageninstrumente der Assistent der k. k
sradmessung Franz Kühnert und am Herbst’schen Instrumente der 1. Observator der
k. k. Gradmessung Ferdinand Anton. Zur Elimination der persönlichen Gleichung wechselten

die Beobachter in der Mitte der Operationen mit Beibehaltung der Methoden ihre Stationen.

15%

 

 
