1 ot AARP Y PT Me 7

AM AN

an

ii
=
à
=
=
x
=
æ
=
=
=
>

101

occidentale della Sicilia, la Pantelleria, e, in giorni eccezionalmente sereni (che si verificano
specialmente in primavera e in autunno inoltrato) il promontorio di Capo Bon colla vieina
Isola di Zembra. !

2. Montagna Grande sull’Isola di Pantelleria — 36°47’ latitudine N.—0°26’ lon-
gitudine O. — quota 830 metri circa. Scopre facilmente la costa occidentale della
Tunisia dal Capo Bon al Golfo di Hammamet coi Monti che vi fanno corona e VIsola di
Zembra, ed in circostanze favorevoli le Egadi, la costa Sud-Ovest della Sicilia da Marsala
a Girgenti e le vette soprastanti, infine la Linosa delle Isole Pelasgiche.

3. Capo Bon punto culminante del promontorio di tal nome in Tunisia a 37°24’
latitudine N. e 1:24’ longitudine O. — quota 400 metri circa. Scopre in condizioni di
visibilita eccezionalmente favorevoli le Isole Egadi (Marittimo e Favignana), alcuni Monti
elevati della Sicilia (Monte San Giuliano, Inice, Bonifato, Busambra, Roccaficuzza) e pit
facilmente la Pantelleria. Il suo orizzonte nel quadrante Sud-Ovest, cioè verso la Tunisia,
e abbastanza sgombro e permette il conveniente sviluppo di una triangolazione Tunisina,
(vantaggio essenzialissimo che non offre la vicina Isola di Zembra, la quale pur vedendo
Marittimo e Pantelleria ha poi Yorizzonte limitato a Sud dai vicini Monti di Sidi
Abder Raman).

4, Monte Sidi Selim Burukba presso la costa orientale Tunisina fra Nebel ed
Hammamet a 36°28’ latitudine N. e°48’ longitudine O. — quota 350 metri circa. Punto
egualmente favorevole pel rattacco come per Vestensione della rete Tunisina.

5. Ad assicurare lo sviluppo di una buona triangolazione in Tunisia si riconobbe

ancora il faro di Sidi bu Said presso il Capo Cartagine nel golfo di Tunisi, a 36°52’

latitudine N. e 2°05’ longitudine O. di Monte Mario. Detto punto fa ottimo triangolo
con Capo Bon e Selim Burukba ed ha inoltre un bellissimo giro dorizzonte.

Il lato Faro Sidi bu Said-Selim Burukba traversa e divide la piana di Suleimen,
la quale presenta ottimo terreno per la misura di una base geodetica, secondo una rico-
noscenza sommaria fatta dal Capitano Maggia.

Compiute le ricognizioni in Tunisia il Capitano predetto ebbe incarico di recarsi
nelle provincie occidentali della Sicilia a definire le modificazioni da apportarsi alla rete
seodetica, che fu stabilita per la Carta ormai compita, per condurla convenientemente al
lato Marittimo—Pantelleria.

Dette modificazioni, che risultano dalla tavola grafica, furono le seguenti:

a) i stabili il nuovo punto di primo ordine Roccaficuzza (Segnale), il quale
ha Vorizzonte perfettamente libero nelle direzioni del rattacco, e 900
metri d’altezza. Esso chiude convenientemente i preesistenti poligoni di
Busambra e Cammarata lasciati pel rimanente intatti.

b) Un nuovo poligono con centro a Salemi (Segnale) e coi vertici a Marsala
(Cupola), 8. Giuliano (Torre), Bonifato (Torre), Roccaficuzza e Granitola
(Faro) venne riconosciuto a ponente, abolendo buona parte degli antichi
punti di primo ordine in tal regione.

 
