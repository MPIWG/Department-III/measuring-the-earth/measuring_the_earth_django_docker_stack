ee oe

 

in!

Ta

i
à
=
=
à
a
=
=
=
5
je

19
Mr. Druhns peut donner ‘quelquesuns des renseignements demandés par son
collègue; il a appris par Mr. Fürster que Mr. Peirce a fait osciller son pendule, tantôt
sur son ineteed: tantöt en fixant le plateau de suspension à un pilier extrêmement solide.
La correction de 025 dont il est question, a été déduite par Mr. Peirce de la différence
des durées doscillation obtenues ainsi dans ces deux modes d'expérience, différence que
Mr. Su attribue à l’effet des mouvements du trépied.

. Bruhns saisit cette occasion de demander si le pendule russe est revenu
de te a si l'invitation de comparer cet instrument à Berlin a été faite au délégué
russe. Il signale encore Popportunité qu’il y aurait à demander sur la question des
renseignements au Portugal, où différentes, déterminations d'intensité de la pesanteur ont .
été faites. : : nN

Mr. le Général de Forsch fait connaître qu'après avoir été rapporté de l'Inde,
Pappareil russe a été envoyé dans le Caucase où il est employé en ce moment; peut-ê être
Mr. Struve le fera-t-il comparer à Berlin, lorsqu'il sera revenu du Caucase.

Mr. von Oppolzer remercie Mr. Bruhms des indications qu'il à bien voulu lui
donner et, sans vouloir mettre en doute la différence trouvée par Mr. Peirce, il ne croit pas
pouvoir, sans de plus amples renseignements, attribuer cette différence à la cause indiquée.

Mr. Bruhns dit qu’il à mis, un niveau sur le trépied de son instrument et qu il
la observé avec une lunette; il n’a reconnu aucun déplacement.

La proposition de Mr.: Hirsch de conserver à l’etude la question du pendule
et d'adresser aux délégués une nouvelle circulaire, est mise aux voix et adoptée.

Mr. le Président, tout en constatant que le point 4 du programme a recu une
solution par la décision intervenue au sujet du paragraphe 3, profite de là présence de
Mr. LL. Suinte-Claire Deville pour le prier de donner à la Commission quelques renseigne-
ments sur la fabrication de la règle géodésique en DURS iridié dont il a bien voulu
accepter la surveillance. ‘

Mr. Sainte-Claire Deville, après avoir brièvement exposé les motifs qui ont
conduit, avec raison d’après lui, au choix du platine iridié comme matière des prototypes
métriques, fait connaître l’état actuel de fabrication de la règle géodésique. En quittant
Bruxelles ‘où ‘il s’est entendu avec son ami et collègue Mr. Sfas, il se rendra à Londres.
pour y surveiller par lui-même la fonte de la règle.

Mr. Bruhns fait ensuite une communication au sujet du paragraphe 4 au pro-
gramme (nombre, construction et résultats des maréographes établis dans les différents
pays). L'Italie et l'Espagne seules ont envoyé les renseignements demandés sur leurs
maréographes.

Mr. Bruhns considère comme désirable .que le Bureau Central adresse aux x dés
une nouvelle demande de lui faire parvenir les données nécessaires pout dresser une
liste Complete de ces instruments fonctionnant actuellement en Europe, en indiquant
pour chacun d'eux le point d'établissement et le principe général de leur construction.

Mr. le Général Baeyer croit qu'il suffira d'indiquer sur les cartes des nivellements
de chaque pays la position des maréographes en fonction, et de publier, dans chaque

3%

 
