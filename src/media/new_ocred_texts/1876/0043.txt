I em nn

 

LLC ART

u

®
=
=
=
a
=
z
z
=
=
ze

 

Re)

 

Wer könnte an dem Gelingen dieser Aufgabe zweifeln, da wir hier an der
Seite gelehrter Repräsentanten der europäischen Armeen die thatkräftigen und un-
ermüdlichen Forscher auf dem Gebiete der Mathematik und Astronomie begrüssen ?

Die belgische Gastfreundschaft erfreut sich, wie ich zu sagen wage, eines alten
Rufes der Liberalität und Herzlichkeit. Ich bin eilt, m. H., Ihnen dieselbe dar-
zubieten und Sie im Namen der Regierung willkommen zu heissen! —

Der Präsident erwidert hierauf folgende Worte:

Herr Minister!
Ich bin sicher, dass ich der Dollmetscher der Gefühle aller meiner Collegen

bin, indem’ ich der Loan Regierung unsern tiefen Dank für den wohlwollenden
D ausspreche, welchen sie der permanenten Commission bereitet hat.

Wir konnten einen solchen hier erwarten; Belgien war eines der ersten Länder,
welches auf den Aufruf des verehrungswürdigen Den der Gradmessung antwortete,
und ihm thatsächlich nachkam, um seine fruchtbare Idee zu verwirklichen. In Belgien
war eS, wo eine denkwiirdigé Arbeit zur Bestimmung geodätischer Maassstäbe unter
des gelehrten und zu früh define General Nerenburger von einer
Commission ausgeführt worden ist, von welcher ich einige Mitglieder hier erblicke. Die
hierüber veröffentlichte Schrift ist ein für derartise Arbeiten klassisches Werk. Ferner
verdanken wir einem Gelehrten aus der belgischen Armee über die Wahrscheinlichkeits-
rechnung und über die Theorie der Fehlerbestimmung eine Abhandlung, welche einen
grossen Einfuss.auf die Verbreitung dieser auf die Geodäsie angewandten Methoden bei
den Nationen ausübte, wo die deutsche Sprache noch wenig verbreitet war. |

Der Tod und der Wechsel der Stellungen in der Armee hatte uns die kostbare
Mitwirkung der belgischen Bevollmächtigten geraubt; aber die Regierung ist unserm
Wunsche nachgekommen und hat jüngst diese Mitwirkung wieder hergestellt; wir dürfen
daher hoffen, dass sie nach wie vor fortfahren wird, dem Unternehmen einer neuen
Bestimmung der Gestalt und Grösse der Erde ihren hohen Schutz zu gewähren. —

Der Herr Minister dankt dem Herrn Präsidenten für die Gefühle, welche er
so eben ausgesprochen hat. „Belgien“, fügt er hinzu, „wird es sich stets zur Ehre rechnen,
seinen wissenschaftlichen Traditionen treu zu bleiben; unser Land, das sich äusserlich nicht
ausdehnen kann, sucht seine Entwickelung um so mehr in der Vertiefung seiner Cultur“.

Herr General Baeyer spricht sich darauf in folgender Weise aus:

Meine Herren!

Belgien ist das erste Land gewesen, in welchem die Fortschritte, welche die
Geodäsie den grossen Arbeiten von Gauss und Bessel verdankt, anerkannt und praktisch
ausgeführt worden sind. Bald nach Veröffentlichung der »Gradmessung in Ostpreussen‘
durch Bessel, nahm das belgische Dépôt de la guerre die neue Methode für seine
Haupttriangulation an, welche darauf unter der Leitung des General Nerenburger aus-

5
