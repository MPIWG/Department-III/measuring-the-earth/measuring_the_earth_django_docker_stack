 

76

Lorsque ces observations seront achevées ainsi que la compensation complete du
réseau des triangles de premier ordre, le Dépét de la Guerre fera recalculer les coor-
données géodésiques de tous les sommets des chaines afin d’établir la comparaison avec
les coordonnées astronomiques de Lommel, de Nieuport et de l'Observatoire temporaire
qui sera choisi dans la province de Luxembourg.

Il emploiera 4 cet effet les dimensions d’un ellipsoide osculateur plus approprié
à notre pays que ne paraît l’étre lellipsoide emprunté & Puissant.

Septembre 1876. E. Adan.

Rapport sur les travaux de nivellement.

Les cheminements du nivellement général ont été commencés en 1847, époque
du levé du Camp de Béverloo et terminés en 1873. Quelques cheminements ont dû être
faits plusieurs fois lorsqu'il y avait des raisons de douter de leur précision, de telle
sorte que la pluspart des points ont des cotes différentes, selon qu’on les considère
comme faisant partie d’une ligne ou d’une autre. Une compensation d’erreurs était done
absolument nécessaire, et c’est de ce travail que s’est occupé le bureau du nivelle-
ment général. ;

La marche suivie est très-simple, elle consiste & égaler chaque différence de
niveau à l'excès de la cote absolue d’une extrémité du cheminement sur celle de
l'autre extrémité.

Soient 4, B, C; . . .. les cotes absolues et À; A, M, . . .. les différences de
niveau; on à par exemple:

+ h difference de niveau de BA A
+ MM id. B and
+ A id. Cia A
+ hu id. Ca 8B
+ A id. D, à pb
+ hy Lo Cia) D etc: etes ete.

les équations seront:
oe ne
al De

ok

u eS eh
+= EL C— B
EME EL D — B
+ A = + C — D etc. ete.

on en déduit les équations normales en nombre égal à celui des inconnues :

  

1184111180 HbMIEN Ba
RE

ve a

al mil

1H NI!

ju theme ds La bal nd sind unie 11:

 
