 

16
TROISIÈME SEANCE,

Bruxezzes, le 7 octobre 1876.

La séance est ouverte à 1° 207.
Présents: Messieurs les Commissaires: Adan, Baeyer, von Bauernfeind, Bruhns,
Faye, Ferrero, de Forsch, Hirsch, Houzeau, Ibanez, Liagre, von Oppolzer, Perrier, de Veccha,
Villarceau. Assistent Messieurs les invités: Ayou, Folie, Hennequn, Lenrionet, Peny,

. Pilloy, H. Sainte-Claire Deville, Schmit, Stas, Terlinden.

Présidence de Mr. Ibanez.

Messieurs Bruhns et Hirsch fonctionnent comme Secrétaires.

Mr. Æirsch donne lecture. du procès-verbal de la 2° séance dont la rédaction
est adoptée après une observation de Mr. von Oppolzer, de laquelle il est tenu compte.

Sur linvitation du Président, Mr. Hirsch donne lecture d’un rapport, en date du
4 courant, que Mr. le Président vient de recevoir et par lequel Mr. Séamkart rend
compte des travaux trigonométriques et des nivellements exécutés en 1876 dans les
Pays-Bas *).

Mr. le Président donne la parole à Mr. Bruhns qui fait lecture du rapport sur

“les travaux de la Saxe **). Ä

. Mr. Hirsch rend compte des travaux de la Suisse ***).

: Mr. le Général Ibanez apres avoir remercié les délégués qui ont eu la parole,
cide-la présidence & Mr. von Bauernfeind pour présenter son rapport sur lhspasne 7")
En terminant, Mr. le Général Jbakez exprime le voeu que le Gouvernement
français fasse procéder le plus tôt possible aux travaux nécessaires pour prolonger le
réseau des nivellements français jusqu'aux repères-frontières récemment établis par les
ingénieurs espagnols.

Mr. Faye ne doute. pas que le gouvernement français ne s'empresse de réaliser le
désir que vient d'exprimer Mr. Ibanez, si ce dernier veut bien en adresser la demande

au Ministre des Travaux publics.

Mr. le Général Ibanez ayant répondu que d'après les usages consacrés dans
Association, les demandes de cette nature sont présentées par MM. les délégués des
pays intéressés, MM. les Commissaires français s'engagent à faire auprès de leur
Gouvernement les démarches nécessaires pour obtenir la jonction, si désirable pour la
science, des nivellements français et espagnols.

Mr. le Président remercie Mr.. le Général Ibanez pour son rapport.

*) Voir dans le Rapport général, Niederlande.

**) Voir dans le Rapport général, Sachsen.
***) Voir dans le Rapport général, Suisse.
#4**) Voir dans le Rapport général, Espagne.

    

wi eam cm i Ati

Lak nil |

1} to |

 

2.) ssusstaæms mama DA |

 
