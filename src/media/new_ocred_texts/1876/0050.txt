 

j
I
K
i

_Struth 5

40

Die Länge des Brockens gegen den Seeberg ist:
nach directer Beobachtung A’ = 0° 0" 26°757 (westlich)

nach der Rechnung

00 2600
Nh = — 0°229 = — 37435

Aus diesen Elementen hat sich zunächst ergeben, dass das gestörte Zenith in
Bezug auf das wahre unter dem Azimuth von 13° 1° 36748 und 94224 von demselben
entfernt liegt. Die Ergebnisse der weiteren Rechnung sind:

 

 

 

 

 

 

 

198 40 53.850

42

 

 

Beobachteter Punkt. oe Zenithdistanz. ee Verbessertes Azimuth.
Fallsten T. P. I >02 1120251 998 93% 98: SS TS 1.50. 88:470
Woldsberg (Marke) 2 0 101109 4 4 | 2.816 2 ı 5.192
Magdeburg (nördl.Domth.) | 62 .12 Boden Ol 2 1195710]! 62 12. 32.627
Hoppel 1.P.. . -. 4 »93 39: 09 5.2414, 19 41 24,499
Eetersbere 1. P.. 10 6 55.414) 90 54 27, 2.550 | 103 & 55.924

- Burkersroda T. P. I. 3»: 5,20 52° 3 | 2.071 | 1338 oF 2136
Seeberg T. P. 1160 1529/0601 00 50 27 | - 2.060572 | [ro 18 27.008
Inselebers T-P.. fem 45 416,509 |) 90 32 1 268 deb 438 43.881

00 AT 2.112 105 AO 51.118

Am stärksten ist hier dieser Einfluss auf den Winkel. zwischen dem Woldsberge

und dem Hoppel, wo er 0/402 beträgt.

2. Der Assistent Herr Sebt hat untersucht, wie gross in Swinemünde der

Unterschied in dem Jahresmittel der täglichen einmaligen Ablesungen des gewöhnlichen.
'Pegels gegen das Jahresmittel des registrirenden Pegels ist.
am gewöhnlichen Pegel Mittags um 12 Uhr gemachte Ablesung benutzt und die mittleren

Er hat dazu die täglich

Wasserstände über dem Nullpunkte des gewöhnlichen Pegels, wie folgt, gefunden:

 

 

 

 

 

 

 

Jahrgang. gr a, Differenz.
oe W907 (0 210 oe Lo.
eo à 0.942 0.949 dre
1873 1.024 1.022 no,
1874 or on. 0 |,
1875 0.057 0.947 10,
Mittel. 07986 07993 Bee

Es ist. auffallend, dass eine einzige tägliche Ordinate

im Jahresmittel mit der

Summe aller so nahe übereinstimmt, weshalb sich die weitere Verfolgung dieses Ergeb-
nisses empfehlen dürfte. |

DL RTE ITR 17

TEL 1

LA ot

(EL LR

MMA mn 11:

1 eld a a

2. suis. mama NA |

 

  
