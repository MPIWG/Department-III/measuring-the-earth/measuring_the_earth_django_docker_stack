 

34

geführt wurde. Im Jahre 1847 entlehnte Belgien den Besselschen Apparat, um die
Grundlinien von Lommel und Ostende zu messen. Nach Beendigung dieses Unter-
nehmens ernannte der belgische Kriegs-Minister eine besondere Commission, in welcher
die Namen der Herren Stas und Liagre glänzten, um eine dritte Vergleichung der
geodätischen Messstangen von Bessel auszuführen, welche zuerst von Bessel selbst im
Jahre 1834, und ein zweites Mal von mir im Jahre 1846 verglichen worden waren, um
ihre Ausdehnungs-Coefficienten zu bestimmen. Sie Alle kennen den so klaren und so
treffenden Bericht des General Nerenburger über diese im Jahre 1854 vollendeten Ar-
beiten. Man verdankt dieser Commission, endgiltig dargethan zu haben, dass sich die
Ausdehnungs-Coefficienten von Eisen und Zink mit der Zeit vermindern, wie ich bereits
seit dem Jahre 1846 vermuthet hatte.

Es geht daraus hervor, wie ich soeben in Erinnerung gebracht habe, dass die
geodätischen Arbeiten in Belgien und Preussen bereits innige Beziehungen zu einander
hatten, selbst vor dem Entstehen der europäischen Gradmessung, und ich ergreife mit
Freuden die Gelegenheit, welche sich mir heute darbietet, um der belgischen Regierung
für die Unterstützung zu danken, welche sie ‚seit dieser Zeit der Entwickelung der
modernen Geodäsie zugewendet hat.

Als im Jahre 1857 Wilhelm Strwe vorschlug, den 52. Parallel. zu messen, han-
delte‘ es sich darum, die Verbindung zwischen den Grundlinien von Bonn und Lommel
auszuführen, welche seit langer Zeit beabsichtigt worden war. Nachdem bei einer Zu-
sammenkunft, welche ich mit meinem Freunde, dem General Nerenburger, in Coln hatte,
die nöthigen Operationen verabredet worden waren, habe ich mit dem General Liagre
die Recognoscirungen der Anschluss-Stationen ausgeführt. Ich erinnere mich stets mit
Vergnügen an die Reisen, welche wir damals zusammen unternommen haben und an den
Genuss, welchen ich von dem Reichthum des Wissens und der Liebenswürdigkeit meines
Collegen hatte, für welchen ich seit damals Gefühle tiefer und aufrichtiger Freundschaft
bewahrt habe. | |

Unglücklicherweise hat die belgische Geodäsie nur allzubald die beiden gelehrten
Offiziere verloren, welche den Aufschwung hervorgerufen hatten, den General Nerenburger
und den Oberst Diedenhoven, deren Andenken der Wissenschaft und ihren Freunden
theuer bleiben wird.

Als der Kriegsminister im letzten Jahre einen belgischen Bevollmächtigten zur
Conferenz nach Paris schickte, wurde diese Maassnahme von uns Allen mit Befriedig-
ung begrüsst, und ich besonders bin glücklich, der belgischen Regierung hier in Brüssel
‚selbst danken zu können, dass sie auf diese Weise alte wissenschaftliche Beziehungen
erneuert hat.

Nachdem hierauf die Herren Minister die Sitzung verlassen hatten, liess der

Präsident folgendes Programm für die Arbeiten der Commission vertheilen:

 
 

Me hdd |) Mae
~

1188!)

yi ied

CL

 

 
