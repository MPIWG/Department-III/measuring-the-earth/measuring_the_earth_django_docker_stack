 

 

Sie erlaubt sich jedoch die Herren Commissare und Beobachter auf nachfolgende Punkte
aufmerksam zu machen.

1. Bei Zeitbestimmungen, auf die Methode der Beobachtung der Sterndurch-
gänge im Vertical des Polsterns anstatt im Meridian. Ein dagegen geäussertes Bedenken,
dass bei dieser Methode die Arbeit der Reduction zu bedeutend sei, wird schwinden durch
Berechnung entsprechender Reductionstafeln.

2. Da es in letzterer Zeit vorgekommen ist, dass bei den Zeitbestimmungen die Pol-
sterne mit Auge und Ohr, die Zeitsterne mit Auge und Hand beobachtet wurden, so wird
bemerkt, dass jeder Beobachter genau prüfen soll ob er die Polsterne mit gleicher Auf-
fassung nach beiden Methoden beobachtet oder nicht. Die etwaige Differenz der nach beiden
Meihoden erhaltenen Resultate ist an die mit Auge und Ohr gemachten Polsternbeobachtun-
gen gehörig anzubringen und dadurch die Gleichförmigkeit der Pol- und Zeitsternbeobach-
tungen herzustellen. In Bezug auf die anzuwendende Vergrösserung der Fernrohre erachtete
die Commission die 60fache Vergrösserung als die untere Grenze festzusetzen.

3. Bei Bestimmung der persönlichen Unterschiede der Beobachter (welche längere
Besprechungen verursachte) findet die Commission auf Antrag des Herrn Hirsch nothwendig
den Beobachtern dringend zu empfehlen darauf zu achten, dass das Ocular für den Beob-
achter immer (also auch bei Zeitbestimmungen) genau eingestellt werde, Die bisherigen
Erfahrungen haben nämlich gezeigt, dass bei nicht centraler Beleuchtung des Gesichtsfeldes
die Vernachlässigung gerade dieser Vorschrift die ergiebigste Fehlerquelle bildet. Die von
Herrn Hirsch erläuterte leichte und bequeme Methode zur genauen Fokalstellung des Okulars
für jeden Beobachter wird empfohlen. (Siehe Anhang 2. Sitzung der astron. Commission.)

Was die Methoden der Bestimmung der persönlichen Unterschiede betrifft, empfiehlt
die Commission in erster Reihe den Wechsel der Beobachter während des Durchganges ein-
zelner Sterne, in zweiter Linie die Bestimmung der persönlichen Unterschiede aus Rectascen-
sionsdifferenzen derselben an zwei aufeinanderfolgenden Abenden wechselweise beobachteten
Sterne, erklärt aber zugleich als wünschenswerth, dass die Variabilität der persönlichen Glei-
chung mittelst verbesserter Zeitcollimatoren bestimmt werde.

4. Bei den Längenbestimmungen werden die Registrirmethode (sowohl der
Durchgänge als der Signale), sodann diejenige der gehörten Coinzidenzen als die besten an-
erkannt und anempfohlen; die letztere besonders in dem Falle, wenn die Aug- und Ohrme-
thode bei den Zeitbestimmungen angewandt ist.

Dagegen, wiewohl die in Russland angestellten Versuche mit optischen mittelst He-
liotropenlichts gegebenen Signalen für die Brauchbarkeit dieser Methode sprechen, hat die
astronomische Commission beschlossen für die Hauptstationen festzustellen:

3. Der $ k des früheren Programms vom Jahre 1867: „Sind die Beobachtungssta-

tionen nur mit grosser Schwierigkeit direct telegraphisch zu verbinden, so sind
Längenbestimmungen auf kurze Strecken durch optische Signale oder durch Zeit-

übertragung mit Chronometern zulässig“, ist aufzuheben.

 

i
i
3
3
3
a
a
|
3
1

at hs

au ca amende à Hd AA a a amas ue ue re

 
