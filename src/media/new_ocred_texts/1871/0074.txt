 

68
finden die Ergebnisse der Anschlüsse an das bayerische Nivellement in dem Werke von
Bauernfeind und man sieht daraus, dass dieser Anschluss durchaus befriedigend ist. Ueber
die Anschlüsse an das westliche Nivellement in Baden haben noch keine Mittheilungen statt-
gefunden und ich kann daher darüber noch nichts berichten.

Herr Bruhns: Ich möchte mir die Frage erlauben, ob von astronomischen Beobach-
tungen in Württemberg noch gar nicht die Rede ist und bemerke, dass schon im Jahre 1864
unter den astronomischen Hauptpunkten Tübingen genannt ist. Wie wünschenswerth ein
astronomischer Hauptpunkt in Württemberg ist, bedarf wohl kaum einer Motivirung.

Herr Baur: Bei den gegenwärtigen Personalverhältnissen unserer Universität in
Tübingen ist kaum zu hoffen, dass in der nächsten Zeit von astronomischen Arbeiten dort
die Rede sein wird. Es wird sich aber fragen, ob nicht in der Nähe von Stuttgart ein
astronomischer Punkt eingerichtet werden soll. Ein Gesuch der Uonferenz an unsere Regie-
rung würde sicherlich fördernd dafür wirken.

Herr Bruhns: Sodann erlaube ich mir den Antrag zu stellen, dass die Conferenz die
königlich württembergische Regierung ersuche, einen astronomischen Hauptpunkt in Tübingen
oder irgend wo sonst in Württemberg zu errichten.

Herr Baeyer: Wäre es nicht zweckmässig daran auch zugleich den Wunsch nach
einer neuen Triangulation in Württemberg anzuschliessen, da ich, nach genauer Durchsicht
des Memoires von Herrn Baur, dessen Ansicht vollständig beipflichte, dass die alte Triangu-
lation unseren Zwecken bei weitem nicht genügt.

Herr Bruhns: Ich formulire den Antrag des Herrn Baeyer und den meinigen in
folgende Worte:

Die Conferenz möge die königlich württembergische Regierung ersuchen eine neue
Triangulation zum Zwecke der Europäischen Gradmessung ausführen und für die
nöthigen astronomischen Bestimmungen einen astronomischen Hauptpunkt (Obser-
vatorium) in Württemberg errichten zu lassen.

Dieser Antrag wird einstimmig angenommen.

Herr Bruhns: Ich erlaube mir noch auf den Bericht des Herrn Schiavont zurückzu-
kommen; da derselbe in meinen Händen, ist es mir möglich gewesen, ihn genauer durch-
zusehen. In diesem Berichte ist erwähnt, dass der italienische Generalstab die Absicht hat,
die Punkte Santa Croce, Laparimontea und Stromboli, welche zwei Polygone der sicilianisch-
calabrischen Kette vervollständigen, zu verbinden, es würde dadurch eine neue Dreieckskette
zu Stande gebracht werden. Ausserdem wird in dem Berichte darauf angetragen, die Terra
d’Otranto Italiens mit den Inseln und Küsten von Albanien zu verbinden und zwar in der
Absicht, einen grossen Parallelbogen von Ponza nach Corfu zu messen, welcher Bogen auch
schon in dem Programm von 1864 vorkémmt. Ich stelle daher den Antrag:

Die königlich italienische Regierung zu ersuchen, diese Triangulation zum Zwecke
der Europäischen Gradmessung ausführen zu lassen.

Der Antrag findet einstimmige Annahme.

 

{
3
3
3
1
4
3
i
3
3

hi

Jb

ins sarah Ada a ld u aan

 
