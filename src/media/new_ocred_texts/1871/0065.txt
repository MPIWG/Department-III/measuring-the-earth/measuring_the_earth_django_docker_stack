107 RP IPP ETT IY PTT yore À

ee

1. Die südliche Dreiecksreihe, welche sich auf die bei Christiania gemessene
Grundlinie stützt, und deren nördlichste Punkte die Namen Skibergfjeld, Jävlekollen,
Kolsäs, Christiania und Hankäs tragen, schliesst sich durch die Seiten des Dreiecks Koster-.
Vagnarberg-Dragonkol an das schwedische Dreiecksnetz. Diese Reihe, in welcher kein
Punkt die Höhe von 2000 Fuss erreicht, wurde schon in den Jahren 1864 bis 1866 von den
Herren Capitain (jetzt Oberstlieutenant) Broch und Observator (jetzt Professor) Mohn gemes-
sen. Es fehlen darin noch, ich weiss nicht aus welcher Ursache, die Beobachtungen auf
dem Punkte Hankäs.

2. Die nördliche Dreiecksreihe stützt sich auf die bei Levanger östlich von
Drontheim gemessene Grundlinie und ist bis jetzt vollständig gemessen bis nach der
Seite Sisselhö - Svärgsöhö, soll aber weiter geführt werden nach Spätind-Näverfjeld,
wozu noch wenigstens ein günstiger Sommer erforderlich sein wird. Die Messungen in die-
sem Theile des Netzes sind von den Herren Mohn, Astrand, Guldberg und Haffner in den
Jahren 1865 bis 1870 ausgeführt, in den zwei Jahren 1868 und 1869 aber ohne Erfolg
wegen schlechter Witterung und Kürze der Operationszeit. Auch darf nicht unerwähnt
bleiben, dass mehrere unter diesen Dreieckspunkten dem Hochgebirge angehören , wo
in unserem Lande schon in 4000 bis 6000 Fuss Höhe Nebel, Regen oder Schnee und
Sturm auch in der schönsten Jahreszeit zur Regel gehören. Im Jahre 1871 ist nicht ge-
messen worden, weil Lieutenant Haffner , der diese Arbeit zum Abschluss bringen soll, zum
Exerciren einberufen ward. ;

3. Die westliche Dreiecksreihe von der Seite Gausla-Johnsknut nach Ber-
gen beruht auf älteren Messungen von Lieutenant (jetzt Oberst) Naeser, welche ohne Zweifel
genügend sein werden zur Bestimmung der geodätischen Lage von Bergen in Bezug auf
Christiania, nicht aber zur Ableitung des Azimuths einer Dreiecksseite bei Bergen. Weil
aber dieser Ort ein Hauptpunkt ist, schien es mir nothwendig, eine derartige Azimuthbestim-
mung durch eine neue Triangulation effectiv zu machen. Diese brauchte aber nur die Ver-
bindung einer für die Azimuthbeobachtungen geeigneten Richtung in der Umgegend von
Bergen (Horviksfjeld-Lövstakken schlug ich dazu vor) mit einer grössern Dreiecksseite (Kvit-
tingen-Thorsnut) herzustellen. Herr Astrand, Director der Sternwarte in Bergen, den ich
im vorigen Sommer veranlasste, diese Triangulirung und Azimuthbestimmung zu tibernehmen,
ist wahrscheinlich jetzt (September 1871) damit fertig. |

4. Das centrale Dreiecksnetz, welches die genannten Reihen mit einander verbindet
und durch sehr grosse Dreiecke vielfache Controlen gewährt, ist in den Jahren 1867 bis 1870
von den Herren Observator Geelmuyden und Candidat Schjédtz gemessen worden. Der Perimeter
dieses Netzes geht durch die Punkte Opknom, Johnsknuten, Gausta, Spätind, Naeverfjeld,
Höstbjörkampen und Höikorset. Auch in diesem centralen Theile hat die grosse Meereshöhe.
einiger Punkte (vier Punkte sind über 4000 Fuss hoch, Gausta 6000 Fuss) wegen der damit
in der Regel verknüpften schlechten atmosphärischen Verhältnisse und ebenfalls die sehr be-

trächtliche Länge der meisten zu beobachtenden Richtungen dem Fortgang der Messungen

8 *

 
