 

a

veröffentlichen, oder an die permanente Commission einzusenden, damit dieser möglichst viel
Material zu einem gründlichen entscheidenden Urtheile geboten werde. Wenn ich auch im-
mer die bayerische Landestriangulation als eine solche angesehen habe, welche für Zwecke
der Gradmessung nicht geeignet ist, so würde es mich doch freuen, wenn die wissenschaft-
liche Kritik des zu erwartenden Werkes feststellen sollte, dass diese Triangulation, welche
der Hauptsache nach im ersten Viertel unseres Jahrhunderts für Zwecke der Landesvermes-
sung vorgenommen wurde, noch für eine Gradmessung passt, die vielleicht mit dem Ende
dieses Jahrhunderts erst ihr Ende erreicht haben wird. Ich würde mich namentlich deshalb
freuen, weil dann die bayerischen Arbeiten für die Gradmessung nahezu vollendet und nur
noch einige astronomische Längenbestimmungen zu machen sein würden, welche dann das
dritte Mitglied der bayerischen Commission, Herr Direetor Lamont, vorzunehmen hätte und
zu deren Vornahme er sich auch (wenigstens was die Bestimmung der noch nothwendigen
geographischen Breiten und Azimuthe an einigen Stellen betrifft) bereit erklärt hat.

Ueber die Bestimmung von Längendifferenzen zwischen München und benachbarten
Sternwarten sind Vereinbarungen noch nicht getroffen.

Was unser Präcisionsnivellement betrifft, so ist‘ dasselbe seiner Vollendung nahe, denn
ausser den Strecken von 815 Kilometer Länge mit 611 Fixpunkten, worüber ich bekanntlich
in meiner besonderen Schrift: „Das bayerische Präcisionsnivellement* berichtet habe, wurden
im Vorjahre noch weitere 183 Kilometer nivellirt. Heuer, oder wenn die Arbeit in diesem
Jahre nicht mehr zu vollenden wäre, im nächsten Frühjahr, sollen noch 464 Kilometer nivel-
Jirt werden und das ganze bayerische Netz wird dann eine Gesammtlänge von 1480 Kilome-
ter oder 200 geographische Meilen erhalten. Dieses Netz ist mit Sachsen auf mehreren
Punkten zwischen Hof und Eger verbunden und schliesst sich an das Höhennetz von Wür-
temberg bei Nördlingen, Ulm und Lindau an, mit der Schweiz ist es durch die Linie Reineck-
Rohrschach, welehe von Bayern aus nivellirt wurde, verbunden und gegenwärtig wird an
dem Anschlusse aller derjenigen Höhennetze, welche die bayerische Provinz Unterfranken
berühren, gearbeitet. Es sind dies die Netze der Staaten Würtemberg, Baden und Hessen,
für Oesterreich haben wir auch verschiedene Anschlusspunkte hergestellt, namentlich bei
Asch, Franzensbad, Eger, Kufstein; ausserdem liessen wir das österreichische Ufer des Bo-
densees zwischen Lindau und der Schweiz über Bregenz durch unsere Ingenieure in der
Absicht nivelliren, um unter Mitwirkung der Schweiz, Badens und Würtembergs, ein voll-
ständiges Polygon um jenen See zu erhalten, welches den grossen Vortheil bieten wird, dass
die Nullpunkte aller Pegel auf eine gemeinsame Horizontalebene sich bezichen lassen.

Schliesslich erlaube ich mir an die Herren Commissare von Oesterreich und Italien
die Bitte zu richten, das nunmehr von der Ostsee ausgehende und ununterbrochen durch
Preussen, Sachsen und Bayern laufende Präcisionsnivellement recht bald von Kufstein aus
über den Brenner an das Adriatische Meer fortzusetzen und beziehungsweise zu vollenden;
denn ohne die Erfüllung dieses Wunsches hätten die bisher ausgeführten Präcisionsnivelle-

ments für die Bestimmung der Erdgestalt nur einen untergeordneten Werth. Ich hoffe übri-

AB Ah bh MRM Ab ahd :à Ads || Asa À

oo 4 staat

di

4 te dati ded

dau dd

ju as banal bb os Mod dh ana mu in

 
