 

28

Essi riguardano la riconoscenza
a) per la misura di una base da farsi in Terra d’Otranto.
b) pel eollegamento tra questa regione Italiana e Albania.
e) per la rete intercetta tra la base indicata e la zona meridiana di Capo Passero.
Essa rete cosi completerebbe l’antica riconoscenza esistente tra la detta zona e l’Isola
di Ponza, ed offrirebbe la misura del parallelo intercetto tra questa Isola e l'Albania.
Questo @ in succinto lo stato de’ lavori di campagna eseguiti dallo Stato Maggiore
italiano dal 1868 sinora, e questo @ lintendimento dietro cui sono iniziati i lavori di calcolo.
Che se su questo ultimo riguardo /’Illustre Assemblea qui riunita trova qualche cosa a mo-
dificare, il Corpo dello Stato Maggiore d'Italia non manchera di far tesoro delle alte vedute
della scienza.
Napoli 12 Settembre 1871.
Präsident v. Fligely: Ich danke im Namen der Versammlung für die gütige Mitthei-
lung; vielleicht hat Herr de Stefanis noch etwas hinzuzufügen.
Herr de Stefanis überreicht die Ausgleichungsrechnungen zwischen der Basis in Apu-
lien und der in Dalmatien. Die Einleitung zu dieser Rechnung lautet:
Corpo di Stato Maggiore Italiano. Caleolo della rete di 1”° Ordine tra la base di
Puglia e la Dalmazia.
Cenno sulla rete Italiana di 1™° Ordine compiuta tra la Puglia e la Dalmazia e cal-

colo di essa.

Generalita.

Se fu ardito il concetto di condurre in Dalmazia la rete meridiana che muove da
Capo Passero e giunge in Puglia; ingegnoso ed intelligente fu il modo di attuarlo, non solo
per la scelta del luogo da traversar lAdriatico, ma pure per lordinamento dato al lavoro di
Campagna. Ed invero rispetto al luogo del passaggio niuno era più acconcio per brevità e
per piccola distanza dalla zona meridiana di quello scelto tra il Gargano e le isole dalmate
poste di rincontro; e rispetto all’ ordinamento, il congegno del quadrilatero Tremiti- Lissa-
Lagosta-Giovannicchio col punto centrale Pelagosa, e la disposizione che Uffiziali dell’ Isti-
tuto geografico di Vienna, e dello Stato maggiore italiano avessero ad eseguire indipendente.
mente le stazioni medesime di tutti i punti di esso quadrilatero fu certo cosa molto lodevole
e maggiormente in quanto che in Italia presso il quadrilatero esisteva una base e la rete cir-
costante che si à completata, ed in Dalmazia esisteva la rete, ed a questora sara stata mi-
surata la base, L’antico concetto adunque del lavoro di Campagna, il quale gia ha avuto
seguito nel 1869, à molto da commendarsi e merita un modo di Calcolo informato a larghe
vedute, il quale potesse stare a lato a tutto il resto del lavoro.

Queste considerazioni indussero lo Stato Maggiore Italiano a compensare la- rete col

numero di condizioni che si poteva maggiori, ed & percid che invece di separare il quadri-

latero Giovannicchio-Tremiti-Lissa-Lagosta dal resto della triangolazione; credette, pur rite-

 

 

Y
|
i
‘
i
3
i

dass,

a a am is dh Äh a mu sun
