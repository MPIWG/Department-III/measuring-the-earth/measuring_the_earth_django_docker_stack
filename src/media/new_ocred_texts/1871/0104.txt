 

5
q
i
q
a
|

=

Herr Baeyer: Ich denke, wenn der Comparator einmal vorhanden ist, wird jede Schwierigkeit leicht
zu überwinden sein. Temperatur- und Ausdehnungscoefficienten werden sich immerhin ändern und der Um-
fang des Messrades müsste vor und nach jeder Anwendung bestimmt werden. Nach den von Herrn Professor
Voit angestellten und in den Astronomischen Nachrichten publicirten Versuchen sind Herr Steinheil und ich
zu der Ueberzeugung gelangt, dass wenn der Messapparat ausgeführt wird, er wenigstens so viel leistet,
als der genaueste Basisapparat; er hat jedenfalls den grossen Vorzug der Leichtigkeit des Gebrauches und
der Schnelligkeit der Messung; und die a in jedem Lande die Grundlinien leicht nachmessen zu
können, darf nicht unterschätzt werden.

Herr Bauernfeind: Wenn der Apparat hergestellt ist, wird man freilich schnell messen können; aber
die nöthigen Schienen und der Transport werden auch beträchtliche Kosten verursachen.

Herr Baeyer: Wenn sich der Apparat bewährt und Resultate erzielt werden, die auf anderem Wege
nicht erreicht werden können, so würde die Conferenz gewiss geneigt sein Zuschüsse zu bewilligen.

Herr Hirsch: Nach der Auseinandersetzung des Herrn Bauernfeind ist es, um die genaue Länge des

 

Umfanges des Messrades zu bestimmen und um über die Genauigkeit ein Urtheil zu gewinnen, nöthig, auch

einen Basisapparat mit Mikroskopen zur Verfügung zu haben. Selbst wenn sich alle Erwartungen, welche

Herr Baeyer von der Leistungsfähigkeit des Messrades hegt, bestätigen sollten, scheint es mir immerhin

nothwendig, noch ausserdem einen anderen Messapparat der vollkommensten Art, wie er etwa in Spanien
verwendet worden ist, zu besitzen und wenn meine Herren Collegen diese Ansicht theilen, möchte ich die
Beschaffung sowohl des Messrades, als eines andern genauen Basisapparates auf gemeinsame Kosten
vorschlagen.

Herr Baeyer: Damit bin ich einverstanden, aber da ein solcher Apparat zu kostspielig ist, möchte

ich den Antrag besonders unterstützen, dass er auf gemeinsame Kosten angeschafft werde.

Herr Hirsch: Mit Bezug auf die letzte Aeusserung des. Herrn Baeyer erlaube ich mir zu bemerken,
dass mein Antrag gerade dahin geht, die Erwerbung des Apparates nicht für das geodätische Institut in
Berlin, sondern für die Europäische Gradmessung zu ermöglichen. : Ich denke mir den Vorgang so, dass

die permanente Commission, welche statutarisch beauftragt ist, die Beschlüsse der Öonferenz zur Ausfüh-

rung zu bringen, das Centralbiireau ersucht, Vorschläge über die Construction eines solchen Apparates zu
machen, dass dann die permanente Commission dariiber beräth und durch Circular den Delegirten sammili-
cher bei der Gradmessung betheiligten Staaten Mittheilung über den Kostenaufwand macht, welchen die
Herstellung eines neuen Apparates, sowie die Erwerbung des Steinhei’schen Messrades erfordern würde und
anfrägt, ob die betreffenden Regierungen geneigt sind, den auf sie entfallenden Kostenantheil zu tragen.
Aus den Antworten würde die permanente Commission erfahren, ob sich die Sache ausführen lässt oder
nicht und ausserdem würde damit auch der internationale Charakter unseres Unternehmens durch die finan«
ziellen Beiträge der verschiedenen bei der Gradmessung betheiligten Staaten gewahrt.

2) Der Antrag des Herın Baeyer lautet: Die Bestimmung des mittleren Niveaus der Nordsee bietet
ganz  besöndere Schwierigkeiten. dar, die durch die Ebbe und Fluth veranlasst werden. Die.Fluthwelle prallt
eines Theils gegen die Küste und bringt dadurch eine Stauung hervor; anderen Theils läuft sie in den
Buchten und Flüssen mehr oder weniger landeinwärts und oft wenn die Rückströmung aus denselben noch
nicht vollendet, langt schon wieder die neue Fluthwelle an. Dadurch erlangen mannigfach complicirte Lo-
calverhältnisse einen Rinfluss auf die Pegelstände, der sich direct nicht beseitigen lässt. Wollte man die
Pegel der Nordseeküste durch genaue Nivellements mit einander verbinden und aus ihren mittleren Ständen
das arithmetische ‚Mittel nehmen, so erhielte man doch nur die mittlere Höhe der Stauung der Fluthwelle
an der Küste, aber nicht die mittlere Höhe der Nordsee. Die Stauung an der Küste ist keineswegs unbe-
deutend, so beträgt z. B. in Cuxhafen der Unterschied zwischen Ebbe und Fluth 10 bis 12, auf Helgoland
6 bis 8 Fuss. Helgoland liegt in der offenen See und so weit von der Küste entfernt, dass hier eine rich-
tigere Bestimmung des mittleren Niveaus mit Sicherheit zu erwarten steht. Die Insel ist mit dem Festlande

 trigonometrisch verbunden (Generalbericht pro 1868). Man würde also durch ein trigonometrisches Nivelle-
p g

ment zwischen Helgoland und Cuxhafen beide Pegel mit einander vergleichen können und dadurch das Mittel
gewinnen, sämmtliche Pegel längs der Küste auf den Helgolande >r zu reduciren. Die Sache hat nicht blos
ein ed, sondern auch ein wissenschaftliches Interesse: sie würde einen nicht unwesentlichen Beitrag
liefern zu den wichtigen Abhandlungen berühmter Gelehrten über Ebbe und Fluth. Es leidet daher keinen

 

i
3
=
3
4
4

Le hdi PP

ss

Jah.

can ah ds à Hdi hd a nu

 
