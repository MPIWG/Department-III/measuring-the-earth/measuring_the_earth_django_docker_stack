 

 

D6

soll, schliesse ich die heutige Plenarsitzung. Auf der Tagesordnung der morgenden Plenar-
sitzung steht die Fortsetzung der Berichterstattung der Bevollmächtigten und die Berichte

der Commissionen.
Schluss der Sitzung um 1 Uhr 45 Minuten.

Fünfte Sitzung

dritten allgemeinen Gonferenz der Europäischen Gradmessung.

Wien, den 27. September 1871.

Anfang der Sitzung 12 Uhr 30 Minuten.

Präsident: Herr v. Fligely.

Schriftführer: die Herren Bruhns und Hirsch.

Präsident v. Fligely: Indem ich die Sitzung eröffne, verkündige ich als heutige Ta-
gesordnung Geschäftliches und die Fortsetzung der Berichterstattung.

Herr Bruhns verliest das Protokoll der letzten Sitzung, welches genehmigt wird.

Präsident v. Fligely: Der nächste Berichterstatter für Sachsen ist Herr Bruhns.

Herr Bruhns: Nachdem Herr Nagel gestern über die trigonometrischen Arbeiten und
die Nivellements in Sachsen berichtet hat, liegt es mir ob, noch Einiges über die astronomi-
schen Arbeiten mitzutheilen.

Nach dem früheren Programm unserer Verhandlung sollten von Leipzig aus ın 8 oder
9) Richtungen Längendifferenzen bestimmt werden. Nach 8 Richtungen ist diese Arbeit be-
reits ausgeführt; die Längendifferenz Leipzig-Berlin wurde 1864 ausgeführt und ist in einer
besonderen Schrift publicirt, Leipzig-Dablitz ist von Herrn Director v. Littrow schon veröf-
fentlicht, Leipzig-Gotha ist von Herrn Auwers und mir unter Mitwirkung des Herrn Hansen
ausgeführt und publicirt, Leipzig-Breslau und Leipzig-Bonn haben die Herren v. Forsch,
Tiele und Zylinski ermittelt und sind die vorläufigen Resultate uns in der vorigen Sitzung von
Herrn v. Forsch mitgetheilt. Leipzig-Göttingen ist auch in den Beobachtungen fertig, die
Reduction noch auszuführen ; die Längendifferenz zwischen Leipzig und Wien wurde im

Jahre 1865 von Weiss und mir ermittelt und ist die Publikation unter der Presse; Leipzig-

 

:
LI
i
3
3
3

rk

iss ach ae Hd a Lana mu un

 
