a YT MT N:

83

Die übrigen Paragraphen der bestehenden Vorschriften wurden eingehend besprochen
und unverändert gelassen.
Die drei Anträge werden einstimmig angenommen.

Herr Weiss: Die Orte, deren Lage astronomisch festgestellt werden soll, wurden
bekanntlich auf der ersten, im Jahre 1864 in Berlin abgehaltenen allgemeinen Conferenz in
zwei Kategorien getheilt: in solche, an welchen alle drei Ooordinaten: Länge, Breite und
Azimuth zur Bestimmung gelangen (astronomische Punkte erster Ordnung) und in solche, bei
denen man sich mit der Ermittelung besonders der beiden letzten Coordinaten begnügt (astro-
nomische Punkte zweiter Ordnung). Die astronomische Commission erkennt auch heute noch
das Wünschenswerthe der Vervollständigung des astronomisch-geodätischen Netzes durch eine
zahlreiche Bestimmung solcher Punkte zweiter Ordnung neben denen der ersten Ordnung an,
besonders an den Endpunkten der zu messenden Bogen und an jenen Orten, wo wegen der
Configuration des Bodens astronomische Beobachtungen wichtig oder wegen vermutheter Lo-
calattractionen lohnend erscheinen, wo aber wegen Mangels einer Telegraphenleitung telegra-
phische Lingenbestimmungen unaustiihrbar sind. Ueber die Auswahl und Vertheilung dieser
Punkte glaubt man indess keine bestimmten Vorschläge machen, sondern dies dem Ermessen
der Commissare der einzelnen Länder überlassen zu sollen. Der von der Commission ge-

stellte Antrag wird daher lauten:

1. Neben der Bestimmung astronomischer Punkte erster Ordnung ist auch eine solche
zahlreicher astronomischer Punkte zweiter Ordnung erwünscht.

Es wäre ferner sehr nützlich, eine Zusammenstellung und übersichtliche Darstellung
der bisher ausgeführten astronomischen Arbeiten zu besitzen. Da jedoch die Beischaffung
des hierzu nöthigen literarischen Materials und Eruirung des in den verschiedenen Ländern
Geleisteten für eine einzelne Persönlichkeit mit sehr grossen Schwierigkeiten verbunden ist,
hat die Commission sich dahin geeinigt:

2. Die Herren Commissare werden ersucht, um eine Zusammenstellung und übersicht-
liche Darstellung der bisherigen astronomischen Arbeiten zu ermöglichen, der per-
manenten Commission die in ihren Ländern ausgeführten Bestimmungen der Länge,
Breite und des Azimuthes sowie der Messungen der Intensität der Schwere — mit
Angabe der dabei verwendeten Methoden und des Ortes der Publikation — zur
Eintragung dieser Daten in eine Karte einzusenden.

Um jedoch hierin einen Anfang zu machen, wurde hier eine Zusammenstellung der
astronomischen Punkte zweiter Ordnung versucht, welche auf Veranlassung der Europäischen
Gradmessung zur Bestimmung gelangten. In dieser Zusammenstellung sind daher nicht auf-
genommen:

1. Alle Orte, wo Längenbestimmungen vorhanden sind (Punkte erster Ordnung),
wobei jedoch zu erwähnen ist, dass an manchem dieser Orte bisher blos die Breite aber

noch kein Azimuth gemessen ist.

167

 
