 

u.

In den meisten Fällen kann noch 2sin4z = sinx gesetzt werden. Wir wollen jetzt nach

dieser Formel die Azimuthal-Differenz @"—2’"’ suchen.

Gegeben ist
u! = 5105424", 81, u" = 49°54' 19", 84; VB — 103°3912",804; ww! — 10°; 0" = 471",6938.

 

 

Setzt man i"
cosw’ te ul cos(w+ 42) = F; 2 eecosu’ cos( D )sin ( = ee ) = 1G,
so ergiebt sich
|. F = 9,8583187; l.sinw'sina’" = 6,2600166; 1. @ = 5,9575873
eplsin(@@w +æ) = 0,7597606 . . . . . . .. .. 0,7597606;  eplcosu! = 0,1910803
2 sin 4x" — 6,3640368 1,0197772, 6,1486676
6,9821161
+-0,0009596570
— 0,0010465915 sin a!” sin Br
1.0,0001408211 wie oC
+-0,0000538866 . . .. 1.5,7314808 eplsinw' = 0,7603298
OIG EO) hee . 6,0498560
3308 ee + 60", 4417
l.cosw’tg ul = 9,8649761; 1.sinw' cos" — 9,8893313,
+.0,7327842
— 0,7750529
— — + 0,0001408
[]= —0,0421279....1.8,6245698,
l.4eecos’u' = 7,1038680
6,0498560
1,7782938, .... —60,0197
+ 0,4220
LP | = 862457,
1.3(ee)’?cos*u! = 4,38383
6,04986
0920,02... 8... — 0,1144
L[ ] = 8,62457,
Der 2
516 (Ce) cos°u! — 1,70954
6,04986
BIBI. ee — 0,0002

an _ Bm — 4.073074
die oben erwähnte Formel gab... ... + 0,3091.

 

111 adhe kat da

 
