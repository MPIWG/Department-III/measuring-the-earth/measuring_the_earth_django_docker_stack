 

Wei.

renz vorlegte, zu ergänzen. Auf dieser Karte sind die alten fünf Basen durch Linien wie
diese: HH bezeichnet; und Bezeichnungen dieser Art finden sich zwischen
den Scheeren südlich von Stockholm, auf Oeland, in Halland, auf dem Wenern- und schliesslich
auf dem Wettern-See. Die Basis auf Oeland ist, wie ich im Bericht erwähnt habe, noch
gültig; aber statt der vier übrigen sollten die drei neuen Basen nach den Zeichnungen ein-
getragen werden, natürlicherweise mit Ausnahme einiger Details, welche auf der Karte nicht
Raum finden können.

8. BSehweız

Die von der Schweiz eingegangenen Berichte:
a) Bericht des Oberingenieurs Denzler über die geodätischen Arbeiten im Jahre 1864,
b) Circular-Schreiben des Präsidenten der Schweizerischen Geodätischen Commission
sind bereits den Herren Bevollmächtigten in besonderem Abdruck übersandt worden. Weitere
Berichte sind nicht eingegangen.

m Wurtemberse

Die mitteleuropäische Gradmessung hatte im vorigen Jahre abermals einen empfind-
lichen Verlust zu beklagen, indem ihr eins ihrer hervorragendsten Mitglieder, der Professor
Dr. Zech (im Monat Juli) durch den Tod entrissen wurde. Die Königl. Würtembergische
Staatsregierung hatte deshalb die Beschickung der Conferenz davon abhängig gemacht, dass
sein Nachfolger bis zum October ernannt sein würde, im andern Falle aber die Zusendung
der Conferenz-Protokolle ausdrücklich beantragt und dadurch ihr lebhaftes Interesse für das
Unternehmen von neuem an den Tag gelegt. Die Ernennung eines neuen Bevollmächtigten
für die mitteleuropäische Gradmessung hatte bis zum Zusammentritt der Conferenz nicht statt-
gefunden, und ist auch dem Centralbureau bis jetzt noch nichts darüber bekannt geworden;
es steht indessen zu hoffen, dass bald ein neuer Bevollmächtigter die vortrefflichen Pläne des
Professor Zech wieder aufnehmen und ihrer Ausführung entgegen führen werde.

 

 

4

{
i
i
|

 
