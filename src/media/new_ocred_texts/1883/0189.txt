 

Diy
Lott,

they
a,
leu
Eye
| hi
ne
ent

Lande
gue
pose
mr Ie
re Out
Il ia

ndant
me IR

t des
MIN:
port
st pas
t par
Jigues
bes;
tel

je, €

pare
st El

, ati
ees
op él

y 8
poll
y gt

een

145

les mouvements des vagues qui ont une période de 20 a 30 secondes ne se fassent
plus sentir.

M. Betocchi attire enfin l’attention de ses collègues sur une disposition qu'il a
employée avec succès dans les maréographes italiens, savoir de construire des canaux
assez larges qui sont munis d’un certain nombre de diaphragmes, lesquels, agissant pour
ainsi dire comme des soupapes, arrêtent les secousses brusques, causées par les vagues.

La discussion étant close, M. le Président met aux voix les quatre propositions
qui terminent le rapport de M. Ibanez et qui sont adoptées avec grande majorité.

Sur la proposition de M. Hirsch on fixe la prochaine séance de la conférence
générale à lundi 22 Oct. 10: du matin, et la séance est levée à 6 heures.

 
