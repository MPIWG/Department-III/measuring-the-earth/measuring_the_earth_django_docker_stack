 

ini
acht
ah
cht,
Dem

chen
el
hen
isch
den

nel
el
ei
ich

ere
re

hie
114

he

Ir

eR TER THIS EEC TRANS

omar

RA AAS ES NIE TI EEE EEE DATE ETE

295

geben für die tägliche Wirkung der Sonne eine sehr regelmässige Curve, fast eine reine
zweimal wiederholte Sinusoide, in welcher die einzige sichtbare Ungleichheit sich ein-
fach durch die tägliche Periode der Windrichtung und des Luftdrucks erklärt. Die
Mondwelle hat eine mehr asymmetrische Gestalt wegen einer, in einem bestimmten Theile
jeder Lunation wiederkehrenden Anomalie, die — wenn ich nicht irre — an vielen
anderen Orten in derselben Weise auftritt und offenbar durch Interferenz erzeugt wird,
nämlich eine Wiederholung des Hochwassers nach Verlauf einer Stunde oder mehr. Was
mich aber besonders erstaunt hat, ist die Thatsache, dass die Springfluth bei Oscarsborg
nicht wie sonst gewöhnlich 1—2, sondern volle 10 Tage nach den Syzygien eintritt.
Dass man auch darin ein Interferenzphänomen erkennen muss und nicht eine wirkliche
— ich möchte sagen naturwidrige — Verzögerung der Fluthwelle, darf wohl keinem
Zweifel unterliegen.

Schliesslich Jist die letzte Publication „Vandstandsobservationer. II Hefte.“
zu erwähnen. Es enthält dieses Heft Mareographenaufzeichnungen und daraus abgeleitete
vorläufige Resultate von vier Häfen, nämlich:

Stavanger in den Jahren 1881—1882
Throndhjem „ ,„ 3 1880—1881
Kabewaae „ ,„ : 1881— 1882
Vardù us : 1880—1882.

Christiania, 4. Juni 1884.
C. Fearnley.

 
