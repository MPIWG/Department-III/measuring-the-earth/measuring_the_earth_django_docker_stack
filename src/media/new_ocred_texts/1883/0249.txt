 

la surface de niveau adoptée pour le nivellement général de la France. Cette hauteur
différe de 0"046 de la moyenne, sans correction, trouvée entre les plus hautes et les
plus basses mers.

En résumé, nous pensons qu’on ne pourra discuter utilement le repère à choisir
pour le nivellement de précision de l’Europe que lorsqu'on aura obtenu les résultats des
observations d’un certain nombre de marégraphes convenablement déterminés, en dehors
des causes d'erreurs que nous avons signalées, et quand ces résultats auront reçu les
corrections à prescrire par la Commission Géodésique internationale, et dont nous venons
d'indiquer les principales. Les points du continent à discuter seront évidemment ceux
pour lesquels les variations du niveau moyen de la mer auront été reconnues nulles, ou
les plus faibles, après un temps d'observation assez long, puisque ce seront ceux qui
présenteront le plus de garanties de fixité.

Paris le 16 Août 1883.

Le Vice-Président de la Commission du Nivellement
signé: Marx.

Renseignements

fournis à la Commission Géodésique internationale sur les Marégraphes.

Tableaux No. 1 et 2 Installation des Marégraphes

No. 3 do.
à No. 4 Résultats par année
No. 5 Résultats Généraux
Nos: bi Résultats corrigés du Marégraphe de Brest
55 manque n No. 6 Dessin du marégraphe du Havre
+ = 24 un dessin manque ‘ No. 7 Dessin du marégraphe de Nice (2 pieces)
858 manque : No. 8 Note sur les corrections à apporter aux

observations marégraphiques.

30*

 
