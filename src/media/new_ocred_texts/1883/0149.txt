rine
their
à to
ject,
TEL,
tion
‘the
ited

sus
[cer

‚u

RB:

 

i
|
E
£
i

OCONEE DN TER

FOREST IEE

SP IO RY REIT PII

sates

ares

2

DE EER ET TER,

135

1. Le chef de section, Conseiller intime de Gouvernement M. le Prof. Dr.
Sadebeck a demande, pour raison de santé, d’étre pensionné A dater du
1° Avril 1883, ce qui lui a été accordé en lui témoignant de la reconnaissance
pour les longs services qu’il à rendus à la mesure des degrés. L’institut
perd en lui un fonctionaire infatigable et consciencieux, qui peut regarder
avec satisfaction sur une vie vouée au service de la science.

La place de chef de section, devenue vacante par cette retraite, a été
occupée depuis le 1% Avril 1883, par l’ancien assistant M. le Dr. Lüw.

3. M. Porrass est entré, depuis le 1% Oct. 1882, en qualité d'assistant

dans l'institut.

vo

B. Travaux géodésiques.

Dans l’année 1882 on a achevé le nivellement trigonométrique qui avait été
commencé entre Helgoland et Neuwerk, et de là jusqu’à la Kugelbake près de Cuxhaven,
pour relier l’île d’Helgoland au continent; et en-se rattachant a ce travail, on a
continué le nivellement géométrique entre la Kugelbake et le phare de Cuxhaven. En»
outre, pour effectuer le rattachement du maréographe enregistreur placé dans le bas de
Vile d’Helgoland avec le continent, on a déterminé trigonométriquement la difference de
niveau entre ce maréographe et le sommet de triangle, prés du vieux phare placé dans
le haut de l’île. :

Pendant l’année 1882 on a commencé les observations pour compléter les chaines
de triangles de la ,Landesaufnahme“ dans la Prusse orientale. Les travaux de
triangulation ont été continués cet été, et on est parvenu à mesurer de nouveau toutes
les stations de la chaîne de 1858.

C. Travaux astronomiques.

Les travaux pratiques de la section astronomique dans les années 1881-82 se
rapportent à la détermination de latitudes et d’azimuts dans les provinces orientales.

On a complété les stations de: Gollenberg pres de Cöslin, Thurmberg près de
Danzig, Goldaper Berg dans la Prusse orientale, Springberg pres de Schneidemühl, Moschin,
Kowalewo et Jauernick. Les résultats sont contenus dans la publication: „Astronomisch-
geodätische Arbeiten in den Jahren 1881 und 1882“. — En outre on a déterminé la
latitude et l’azimut à Kernsdorf. Ensuite on a continué les recherches relatives aux
déviations de la verticale dans les montagnes du Harz. En 1881 on a achevé les stations
suivantes: Teufelsmauer près de Blankenburg, Hüttenrode (signal), Hasselfelde et Nord-
hausen; tandis que cet été on a complêté les stations de Neubau près de Aschersleben
et de Huyseburg près de Halberstadt.

Le problème principal pour la section astronomique pendant l'été actuel était de

 
