 

Installation des Maregraphes de France.

Tableau No. 2.
1 ee SS
| Echelle de la courbe qu'il fournit |
| Valeurs numériques

 

 

 

 

 

Indication Systéme — |
| | obtenues dans le cas
du de | | L
: 0 | 7 Id’ötre graphique et
Marégraphe. l'appareil. Echelle | Echelle | grapoid
du Temps. | des hauteurs. | compteur.
| |
Dunkerque . . ... | Système Chazalon. — Le | 0*001 par minute | 0"05par mètre | Simplement graphiques.
flotteur actionne à l’aide | |
d’un fil tendu par un|
contre poids, une roue | |
| qui, par l'intermédiaire | |
| d'une roue plus petite, | |
| fixée concentriquement | |
| sur le même arbre, met | |
| en mouvement un crayon | |
appuyé sur un cylindre |
porte-carte horizontal |
mu par un mouvement |
d’horlogerie. |
Boulogne. . . . .- do. | 0"001 par minute | 0"05par mètre | do.
Pe Barre .....| do. | do. (0-10 40 | do.
Cherbourg . . ... do | do. F0 107 “do | do.
| | |
SE Servan. ....| do. | do. 10.10 do. | do.
| | ||
2 | | do. | do. 1010 di | do.
| | |
St. Nazaire. .... do. | do. KO || do.
| | | |
Fort Boyard ... do. | do. 0 20 do. | do.
La Pointe deGrave | do. | do. 010 do, do.
|
Arcachon .-... | do. do. "0.10 do. | do.
Le Socoä...... | do. | do. 0. 10> do: | do.
N Les inscriptions se font | 070005 par minute | 0®50par mètre | Le flotteur est attaché à
| sur un papier continu qui | | une chaine sans fin qui
passe du cylindre où il est | | passe d’une part sur
touché par le crayon sur | | une poulie plongeante
un autre cylindre où il| | maintenue par un
s’enroule. | | contre poids et d’autre
| part sur la roue mo-
| || trice.
Nice . ah | Le cylindre porte-carte est | 0700083 par minute | 0720par mötre | do.
| vertical et n’a que 0.235 |

 

de hauteur sur 0.15 de | |
diamètre. |

|
|
|
F

 
