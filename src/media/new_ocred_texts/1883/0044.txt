 

30

 

auf die grossen Vortheile hinzuweisen, welche für die geographischen Wissen-
schaften aus der allgemeinen Annahme eines einzigen Ausgangs-Meridianes erwachsen
würden. Alle diejenigen Gelehrten, welche häufig die Register der geographischen Positionen
nachzusehen haben, und behufs ein und derselben Arbeit Karten von verschiedener
Herkunft vergleichen müssen, wissen aus Erfahrung, welchem Verlust an Zeit und Mühe
sie durch die Nothwendigkeit ausgesetzt sind, fortwährend die Längen eines Systems
in die des anderen zu übertragen. Handelt es sich hierbei auch nur um eine ein-
fache Additions- oder Subtractions-Arbeit, und nicht um eine Multiplication oder Division,
wie bei der Umwandlung der thermometrischen und barometrischen Messungsscalen, zu
| der man besondere Reductionstabellen nicht entbehren kann, so bleibt es doch immer

 

 

 

 

 

 

 

ein unnützer Zeitverlust, den man so zu sagen täglich erleidet, und sämmtliche Geo-
graphen werden es wie eine wahre Erlösung begrüssen, wenn sie sich in ihren allge-
| | meinen, zusammenfassenden Arbeiten nicht mehr durch diese ärgerliche und zeitraubende
I Verschiedenheit der nationalen Längen behindert sehen.

| Für die Geodäsie im Besonderen ist die Unification zwar von geringerer Be-
| deutung, doch immerhin nicht ohne Nutzen. Ist es auch richtig, dass die Geodäten
|| ee immer nur die Differenzen der Längen messen und dass bei der Bestimmung der Krüm-
mung der Parallel-Kreise ebenfalls nur die Differenzen in Betracht kommen, so kann
| man doch für andere theoretische Studien der Geodäsie und Erd-Physik die absoluten
| Coordinaten, welche die geographische Lage in unzweifelhafter Weise festlegen, nicht
| entbehren. Die astronomischen Beobachtungen liefern ebenfalls direct nur die Diffe-
I renzen der Rectascensionen, und gleichwohl muss man dieselben in absolute Coordinaten
| umwandeln, welche von einem Ausgangspunkte aus gerechnet werden, welcher zwar sich
| mit der Zeit ändert, über dessen Wahl jedoch unter den Astronomen von jeher wenig-
stens eine vollständige Uebereinstimmung geherrscht hat.

Für die Astronomie sind die geographischen Längen hauptsächlich wegen der
Ortszeiten von Interesse, in denen sämmtliche Himmelsbeobachtungen nothwendigerweise
ausgedrückt werden. Da man aber die an verschiedenen Orten gemachten Beobachtungen
H | zusammenfassen und mit den Ephemeriden vergleichen muss, welche nothwendig fiir einen
I

 

 

 

 

 

 

 

 

bestimmten Meridian berechnet sind, so ist es für die Astronomen, um die Reductions-
Arbeit zu vermindern, ebenfalls wünschenswerth, überall mit ein und demselben Meridian
zu thun zu haben. Selbst weni man zugiebt, dass eine grössere Anzahl der astrono-
mischen Ephemeriden den Vortheil gegenseitiger Controle bietet, so würde diese Controle
doch um so leichter und wirksamer, wenn die sämmtlichen Ephemeriden für ein und
| _ denselben Meridian berechnet, und damit deren Zahlen-Angaben ohne Weiteres vergleich- |
I bar wären. Uebrigens würden zwei von einander unabhängige Ephemeriden für diese |
| Controle ausreichend sein, und man kénnte die grosse Summe von Arbeit, welche jetzt |
| zur Herstellung zahlreicher nationaler Ephemeriden verwendet wird, fiir den Fortschritt

der Astronomie nutzbarer verwerthen, wenn man dazu gelangte, die Berechnung der

verschiedenen astronomischen Tafeln und Elemente unter den verschiedenen Jahrbiichern

zweckentsprechend zu vertheilen. Auch darf man den Vortheil nicht allzuhoch anschlagen,

 

 

 

 

 

 

 
