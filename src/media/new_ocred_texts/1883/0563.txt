 

da

raten, dh
Tale dit,
atin oe
nl more 1
qe lee
de lange Ë

ms |
a ge.

 

one N
hay
à Jeu |
U Ot |

 

iR

5

Or

en faisant les observations dans les mémes limites des amplitudes pour le poids lourd en
haut et en bas. Les durées d’oscillation ainsi déterminées pour le pendule lourd seront
sensiblement plus exactes, parce que d’un côté la durée d’oscillation de ce pendule dans
les mêmes limites d'amplitude sera plus grande, et d’un autre côté une erreur dans ces
déterminations exercera moins d'influence sur le résultat pour le pendule lourd que qour
le pendule léger. Il semble donc qu'on devrait donner une plus grande exactitude à
l'observation du pendule léger par des mesures plus nombreuses; mais il ne faut pas
oublier qu’il est nécessaire de connaître pour de telles observations la marche de la
pendule.

On ne commettra pas un grande erreur, en estimant à 0°3 l'incertitude dans la
marche diurne, même quand la pendule de comparaison se trouve dans une localité d’une
température constante et a l’abri des variations diurnes, et en évaluant par conséquent
l'incertitude dans la supposition de la durée d’une oscillation de la pendule de comparaison
& la 300000" partie. Cette exactitude pour la durée d’oscillation du pendule à réversion, sera
par une seule série d'observation, vu la précision de la méthode employée des coincidences,
non seulement atteinte, mais même surpassée; car pour le pendule léger — poids lourd en
bas — la durée de l'observation peut être étendue à 48 minutes sans qu’on ait besoin
de récourir à des amplitudes au dessous 20’; lorsque le poids lourd est en haut, cet
intervalle comprendrà à peu près 20 minutes. Si l’on ne se restreint pas à la première
et dernière coïncidence, mais si l’on observe également toutes les coïncidences, on trou-
vera les durées d'oscillation exactes jusqu'à peu près à la 200000™ partie. Mais l’in-
exactitude provenant de la marche de la pendule influence 47 d’une quantité triple et
demie, et cette dernière erreur affecte la pesanteur du double de sa valeur; de sorte que
finalement l’inexactitude se trouve multipliée par 7, ce qui porte ainsi l'incertitude
d’une série d'observations jusqu’à peu près à la 30000 partie de la pesanteur. La ré-
pétition des observations diminuera, il est vrai, l’incertitude causée par la mesure du
temps, et fera atteindre, si elle est renouvelée 10—12 fois, une limite de précision d’un
centmillième de seconde. L’exactitude de la détermination de la durée d’une oscillation
sera donc beaucoup moindre que celle de la mesure de longueur, pourvue qu’on ne veuille
pas trop exiger de l'observateur; car pour atteindre cette dernière exactitude, il faudrait
effectuer au moins cent déterminations du temps d’oscillation pour chacun des quatre modes
indiqués plus haut, et le résultat ne serait, en réalité, nullement en rapport avec le
travail exécuté.

De ce qui a été dit on peut conclure qu’il est possible d'atteindre, dans la de-
termination absolue de la pesanteur, une exactitude allant jusqu’à la 100000” partie de
la quantité cherchée; ce qui correspond à peu près à la centième partie d'un millimètre
pour le pendule à seconde. Mais comme, en général, on estime les effets des erreurs
plutôt trop forts que trop faibles, il en résulte qu’on pourra en moyenne toujours atteindre
cette limite d’exactitude.

Avant de clore la discussion sur la détermination absolue de la pesanteur, le
rapporteur croit devoir indiquer les méthodes qu’on a proposées dans ces derniers temps,

 

 

 

 
