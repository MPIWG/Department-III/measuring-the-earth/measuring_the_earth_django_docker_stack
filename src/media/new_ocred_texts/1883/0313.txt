 

fe,
i,

même pays savoir: 1° entre Tunis (Carthage) et une de stations sur l’île de Sicile,
afin de clore le grand polygone de la Méditerranée; 2° entre Ragusa et Pacchino
ou Naples, pour rattacher l'Italie à la triangulation de la partie méridionale de l’Autriche ;
3° entre Ragusa et Sarajevo et entre Kronstadt et Czernowitz, qui sont indispensables pour
clore le polygone des longitudes en Autriche; 4° entre Lisbonne et une station en
Espagne, afin de controler la longitude de Lisbonne, qui n’a été détermiuée télégraphi-
quement que par un petit nombre d’observations faites par les astronomes Américains,
et dont les résultat differe de plus de huit secondes de la valeur adoptée auparavant.

1*

 
