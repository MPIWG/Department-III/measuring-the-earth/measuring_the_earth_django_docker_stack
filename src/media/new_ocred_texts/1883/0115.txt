 

ET =F

7

de
te
hie
be
ye
je!

 

101

Perrier kann nicht v. Bauernfeind darin beistimmen, dass zwischen der terrestrischen
und astronomischen Refraction nur ein gradueller Unterschied bestehe. Er erinnert in
dieser Beziehung und insbesondere in Bezug auf die Lateralrefraction an die bedeutenden
Arbeiten des Obersten Hossard, welche im neunten Bande der Memoiren des Depot de la
Guerre veröffentlicht sind.

Bei der Abstimmung wird der Antrag Hartls mit 21 Stimmenangenommen *).

Der Präsident ertheilt das Wort nunmehr Schiaparelli, um den Bericht der
Specialcommission, welcher in der zweiten Sitzung der Conferenz der Antrag Fergola’s
zugewiesen wurde, der Versammlung zur Kenntniss zu bringen.

Derselbe lautet (Uebersetzung des französischen Originales):

Bericht

über das von Prof. Fergola in der Sitzung vom 16. Oktober in Vorsehlag gebrachte Beobachtungsprogramm
zum Studium der Lageänderungen der Erd - Rotationsachse im Erdinneren und der damit zusammen-
hängenden Polhöhenänderungen.

Meine Herren! Unser verehrter College Professor Fergola schlägt der Conferenz
vor, Untersuchungen darüber anstellen zu wollen, ob die Pole der Drehungsachse der
Erde als wesentlich unbeweglich gegen deren Oberfläche betrachtet werden dürfen, oder
ob diese durch die Einwirkung verschiedener ursächlicher Momente (hauptsächlich wohl
geologischer Natur) merkliche Bewegungen zeigen, falls wir zur Bestimmung die
besten Instrumente und die genauesten Methoden, welche die moderne Astronomie uns
bietet, in Anwendung ziehen.

Dieser Antrag beschäftigt sich mit einem grossen Problem, welches nicht nur
die Geodäsie, sondern auch die Astronomie und Geologie im hohen Grade interessirt, und er-
scheint daher unserer Beachtung besonders werth; und richtet sich auch auf einen Gegenstand,
welcher manche Unsicherheiten und bedeutende Schwierigkeiten theoretischer und practischer
Natur darbietet. Es war unsere Aufgabe, diese Unsicherheiten und Schwierigkeiten min-
destens theilweise und so weit es uns die für diesen Zweck knapp zugemessene Zeit

- gestattet einer eingehenden Behandlung zu unterziehen. Professor v. Bakhuyzen, der von Seiten

der Speeialcommission zum Präsidenten erwählt wurde, hat Herrn Fergola ersucht, in der
Commissionssitzung zu erscheinen, um, was für die Lösung der uns gestellten Aufgabe
besonders förderlich war, von ihm eine eingehende Auseinandersetzung jener Gesichtspunkte,

*) Fearnley übermittelt gegen das Ende der Sitzung dem Bureau eine auf die eben behandelte
Frage bezügliche Notiz, in welcher er bekannt giebt, dass er bei der Frage über die terrestrische Refraction
eine nicht ganz unwichtige Bemerkung zu machen habe, und sich vorbehalte, dieselbe als Beilage dem
Generalberichte folgen zu lassen.

 
