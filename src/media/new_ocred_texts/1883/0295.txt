 

ë
|
F
i

EEE

résulte — +0"14 par la compensation dans les stations, et = + 0'38 par celle
du réseau.

Il est à remarquer que la nouvelle base donne, pour la longueur du côté de
jonction Chasseral-Röthi, une valeur presque identique à celle que Æschmann avait déduite
dans le temps de l’ancienne base Sugy-Walperswyl; la différence (0°62 + 056) n’est que
d'un +ri00 et dépasse à peine son incertitude. Toutefois, puisque l'équation entre les
règles qui ont servi aux deux mesures de bases, n’est pas suffisament connue, il ne faut
pas s’exagérer l'importance de cet accord.

Tout en faisant la même réserve quant à l’influence de l’équation définitive des
étalons employés dans les mesures des bases Allemandes et Suisses, on doit cependant
envisager comme très satisfaisant l’accord que les calculs de M. Scheiblauer ont montré
pour les parties communes de notre réseau et du ,,Rheinische Dreiecksnetz“. Car si l’on
déduit la longueur des côtés communs, d’une part de la base d’Aarberg et d’autre part
de la base de Bonn, on trouve une différence moyenne de + 041 + 036, tandisque
la différence moyenne des directions mesurées dans les stations limitrophes, Röthi, Wiesen,
Lägern, Feldberg et Hohentwyl, par les ingénieurs allemands et suisses, est seulement
de + 0°29.

Par contre le réseau de la base de Weinfelden, dont la disposition est
bien moins parfaite et qui a été mesuré en 1882 dans des conditions météorologiques tres
défavorables, à montré quelques défauts qui ont engagé la Commission à fair vérifier les
angles dans les stations de Weinfelden et de Hersberg; la répétition du calcul avec ces
nouvelles données qui se fait dans ce moment, fera voir si l’accord avec la base d’Aarberg
qui laissait à désirer, sera ramené à un degré convenable.

Le réseau de la base de Bellinzona a été exécuté, pour la plus grande
partie, pendant l'été de 1883; il reste encore trois stations à faire qui seront terminées
dans la campagne de cette année. Nous y avons employé pour la première fois les ob-
servations de nuit, pour échapper aux difficultés particulières qui s'opposent aux trian-
eulations pendant la saison chaude dans les montagnes du Tessin. Les lampes employées
sont à pétrole, montées dans une boîte mobile autour d’un axe vertical et horizontal,
avec des lentilles biconvexes de 60 de distance focale et 19% de diamètre.

Le II Volume de la „Triangulation Suisse“ est sous presse dans ce moment et
paraitra sous peu. Il comprend la compensation du réseau principal, le calcul des
erreurs, et les réseaux de jonction des observatoires et stations astronomiques.

II. Mivellement.

La 8e livraison du ,Nivellement de précision de la Suisse“ a paru au commen-
cement de Septembre 1883. Elle contient le reste des travaux exécutés dans les cam-
pagnes de 1879, 80, 81 et 82; les 16 lignes qui y sont publiées, complètent 5 anciens
polygones et forment trois nouveaux. Pendant le cours de l'impression nous avons fait
refaire une troisième fois la section, entre Nyon et St. Cergues, de la ligne de jonction

36

 
