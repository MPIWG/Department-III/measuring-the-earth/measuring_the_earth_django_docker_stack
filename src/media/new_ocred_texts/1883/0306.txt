 

 

 

 

 

 

 

 

292

30 Au fond des rades ou bassins où la mer se trouve refoulée par les lames du
large et qui accusent des hauteurs d'eau plus grandes que la hauteur réelle.

II. Ne convient-il pas de soustraire les canaux de communication entre la mer
et les puits des marégraphes à l’action de la houle par un système d'écrans, comme il
est indiqué sur le dessin que nous donnons des dispositions adoptées pour le marégraphe
de Nice?

Les tableaux 4 et 5 donnent les résultats par année et les résultats généraux
que nous avons pu obtenir par le dépouillement des feuilles d'observations, dépouillement
qui n’a pu être terminé dans tous les ports. Les cotes indiquées sont les cotes brutes
et qui n’ont subi aucune correction. On a voulu attendre. avant d'effectuer ces corrections
l'avis de la commission géodésique internationale à l'examen de laquelle nous croyons devoir
soumettre les points suivants:

19 La moyenne des hauteurs maxima et minima differe plus ou moins, mais dans
certains ports de quantités considérables, de la hauteur moyenne réelle. C’est cette
dernière seulement qui doit être recherchée; il importe que ce point soit bien arrêté.

90 La correction due à la pression barométrique est trés-importante et peut
s’opérer facilement. Il est nécessaire pour cette correction de tenir compte de la hauteur
au-dessus du niveau moyen de la mer de la cuvette du baromètre qui sert à déterminer
la pression.

30 Le niveau indiqué par les ordonnées sur l'appareil diffère souvent de la
hauteur observée directement au moyen de l'échelle placée dans le puits du marégraphe
par suite de la dilatation ou de l’extension des fils de suspension du flotteur et de trans-
mission du mouvement de ce flotteur à l'appareil enregistreur. Il y a là une correction
importante à faire, en inscrivant deux ou trois fois par jour les différences constatées.

Au marégraphe de Cette, on a diminué l'erreur due aux variations de longueur
du fil du flotteur en placant ce flotteur sur un fil sans fin qui passe sur la roue motrice.

4° La force du vent et la déclinaison lunaire exercent sur le niveau de la mer
des influences dont on doit tenir compte.

59 Les variations de densité de l’eau de la mer déterminent, outre un enfoncement
plus on moins grand du flotteur, des différences dans la hauteur du niveau moyen. Il
conviendra donc de donner au flotteur une section horizontale assez grande pour qu’il ne
soit pas nécessaire d’avoir égard à la première cause de perturbation. Mais il faudra
en outre déterminer journellement la densité de l’eau dans le puits de l'instrument.

69 Indépendemment de ces causes, il sera utile d'examiner si les différences avec
la moyenne, trouvée après un certain nombre d'années, ne résultent pas de phénomènes
constants et de rechercher sinon la cause, du moins la loi de ces phénomènes. Il peut
se faire que le continent se soulève ou s’abaisse sur la côte où est installé le marégraphe,
et il sera possible d’arriver à le constater en cherchant la loi des variations.

Ces différentes corrections ont été faites pour les résultats fournis par les feuilles
du marégraphe de Brest, par les soins de M. l'Ingénieur en chef Hydrographe Bouquet

 

 

 

 
