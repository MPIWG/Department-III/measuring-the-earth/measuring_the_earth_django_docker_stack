 

| 248

 

| Hessen. Hesse,

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

| Die Arbeiten im Grossherzogthum Hessen beschränken sich, wie schon wieder-
| holt erwähnt wurde, auf Präcisions-Nivellements. Nach Vollendung eines sehr voll-
| ständigen Netzes in den beiden südlichen Provinzen des Landes wurden seit dem Jahre
| 1882 die Nivellements in Oberhessen begonnen und auf Eisenbahn-Dämmen, theilweise
auch auf Chausseen ausgeführt. Die schematische Figur giebt eine Uebersicht dieser

| Linien, von welchen einzelne doppelt nivellirt wurden.

| Die Arbeiten wurden ganz in der gleichen Weise ausgeführt, wie dies im General-
| bericht für 1869 Seite 95 angegeben ist.

| Die Buchstaben in der Figur bedeuten:

| W ....@Westbahnhof in Hanau.

| De @Ü0stbahnhof in Hanau.

| | Ge @Gelnhausen.

| Bd... @Büdingen.

| So ..... [ jStockheim, Zeïichen am Sockel des Stationsgebäudes.

| ee @Ranstadt.

| ne, Zeichen auf der Brücke in Ober-Mockstadt.

| onto à [ jZeichen auf dem Kilometerstein an der Kreuzung der Chaussee
| bei Dornassenheim.

| we QF riedberg.

| BS eee QHeldenbergen.

I een [ Zeichen auf dem Kilometerstein, südlich von Ilbenstadt, an der
Kreuzung der Chaussee nach Stammheim.

| Sa .... [ jZeichen auf einem Stein an der Gartenmauer des Wirthshauses
| in Stammheim.

| A a. [ Zeichen auf der nordöstlichen Ecke der Brücke über die Nidda,

nördlich von Assenheim.
Br .... [[]Zeichen auf der Schwelle des Stationshauses Bruchköbel.
eo . Bolzensteine der Preussischen Landesaufnahme.
|

Wir bezeichnen wieder, wie früher, durch:

h den Höhenunterschied,
| ! die Entfernung,
| p das Gewicht,

| ;
| und bemerken, dass die Strecken 3, 4, 5, 6, 9 und 13 doppelt nivellirt wurden, und dass
; den gemittelten Höhen das doppelte Gewicht wie den einfach nivellirten beigelegt wurde.

 

 

i nme

 

 

 
