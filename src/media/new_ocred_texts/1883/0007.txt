 

r

INHALTSVERZEICHNISS — TABLE DES MATIERES.

Protokolle der zu Rom vom 15. bis 24. Oktober 1883 abgehaltenen Sitzungen
der Siebenten Allgemeinen Conferenz der Europäischen Gradmessung.

Seite
Verzeichniss der an der Conferenz Theilneh-
menden  . . Soo
Programm fiir die en ae Kr D
Berne oe CO
Erste Sitzung der Conferenz am 15. Oktober
Nachmittags.
Verzeichniss der Anwesenden . . . eS
Begrüssung der Versammlung durch Se. Ex-
cellenz den k. italienischen Unterrichts-
mimister ACCES . : 8
Antwort an den Minister von ern Bl
Ibanez : - ae 9
Wahl des et ee = 10
Bericht der permanenten on für de:
Jahr 1883: . ee‘
Mittheilung mehrerer sn eee Be 2 16
Bericht des Centralbureaus resp. des geodäti-
sehen Insttutg - - | 20

Bericht iiber die Unification er a
Längen durch Annahme eines einzigen Aus-
gangs-Meridians und über die Einführung
einer Universalzeit von Herrn Hirsch . . 25

Wahl einer Special-Commission zur Begutach-
tung des über diese Frage erstatteten Be-
BE lo «ihudd

Zweite Sitzung der Conferenz am 16. Oktober

Nachmittags.
Verzeichniss der Anwesenden . . . 45
Begrüssung durch den Syndikus der Stadt Ron
Duca Torlonia. . . . 45

Verlesung eines von Sr. ll ji ‘ee
lienischen Minister des Aeusseren Mancini

eingelangten Begrüssungstelegrammes . . 46 |

 

 

Antrag des Herrn Fergola auf Untersuchung
der Veränderungen der Lage der Erdachse in
Bezug auf die feste Oberfläche des Sphäroids
durch Beobachtung von Polhöhenänderungen

Mittheilung eines Beschlusses der italienischen
Commission betreffend ihr Verhalten bei den
Abstimmungen der Conferenz

Bericht des Herrn v. Bauernfeind über aie
Gradmessungsarbeiten in Bayern (s. General-
bericht, pag. 228) und Discussion hierüber.

Bericht des Herrn Hennequin über die Arbeiten
in Belgien (s. Generalbericht, pag. 227).

Bericht des Herrn Perrier iiber die Arbeiten
in Frankreich (s. Generalbericht, pag. 230)

Bemerkungen des Herrn Villarceau über seinen
Regulator als Schwerebestimmungsmittel

Bericht des Herrn Nell über die hessischen
Arbeiten (s. Generalbericht, pag. 248)

Bericht des Herrn Ferrero über die italienischen
Arbeiten (s. Generalbericht, pag. 252)

Ergänzende Bemerkungen der Herren Stefanis,
Betocchi und Schiaparelli . :

Discussion über das Studium leaner Er-
scheinungen mit Hilfe geodiitischer Opera-
tionen am Aetna.

Dritte Sitzung der Conferenz am 18. Oktober

Nachmittags.

Verzeichniss der Anwesenden y

Ansprache des Präsidenten im Namen der nr
lienischen Commission und Vorlage der von
derselben dem Herrm General Baeyer ge-
widmeten goldenen Ehrenmedaille .

Rede des Herrn General Ibanez, der sich im
Namen der Bevollmächtigten dieser Ehrenbe-
zeugung anzuschliessen erklärt .

Seite

46

47

47

47

47

48

49

49

50

50

52

53

 
