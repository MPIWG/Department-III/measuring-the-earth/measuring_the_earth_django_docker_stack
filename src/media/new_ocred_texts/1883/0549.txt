Eu |

QU en

MEN À
pain |
pat À
ehem |
| ab a |

pg, Xl
es IS A
set app
ati d

mine |

ati

 

désirer. Mais les résultats de ces mesures ont besoin de différentes réductions, basées
en partie sur des données empiriques, en partie sur la théorie. Si l’on parvient à
déterminer pour un appareil donné toutes ces réductions avec une précision égale à celle
que l’on trouve dans la mesure du temps et dans celle de la longueur, alors on aura
obtenu tout ce qu'on peut désirer. Mais on trouve de grandes difficultés à remplir cette
condition. Dans le présent rapport on ne peut s'occuper que des réductions les plus
importantes, mais on ne peut pas entrer dans le détail des réductions générales et de moindre
importance.

Pour la détermination du temps d’une oscillation c’est la méthode des coïn-
cidences, introduite par Borda, qui tiendra toujours le premier rang; l’enrégistrement
des oscillations par l'observation directe est incontestablement moins précise, et l’en-
régistrement automatique est sujet à bien des doutes, à cause de l’échappement de
l'appareil qui peut influencer le temps de l’oscillation du pendule. La méthode des
coincidences à subi différentes modifications et améliorations entre les mains des obser-
vateurs. Je ne rappelle ici que les modifications introduites par Bessel, Basevi, Bredichin,
Vogel et d’autres, auxquelles dans ce dernier temps M. © J. Peirce (Methods and Results
of pendulum experiments, Washington 1882, Appendix No. 16 coast and geodetic survey
Report for 1881) a ajouté une proposition importante. D'après le jugement du rapporteur
et d’après les expériences qu’il à recueillies dans le cours du temps, la methode décrite
par M. H. C. Vogel (Ueber eine Methode, die Schwingungszeit u. s. w. Repertorium für physi-
kalische Technik von Carl, Band XVII, pag. 337) semble mériter la préférence quant a
la simplicité et la précision. Une plaque fixée au pendule de l’horloge et munie d’une fente
verticale passe auprès d’une plaque fixe muni d’une fente semblable, de sorte qu’on peut, à
l’aide d’une lunette, voir le pendule servant à la détermination de la pesanteur à l'instant
précis où les deux fentes sont superposées, ce qui ne donne qu’une image presque instantanée.
A l’aide de cet appareil j'ai pu comparer les phases du pendule de l'horloge et du
pendule à réversion avec une précision de 0°003. Pour l'observation des images
instantanées on a besoin d’un éclairage très intense de l’échelle et de la pointe du
pendule. J’y suis parvenu en projetant la pointe du pendule et les divisions dentées de
l'échelle opaque sur un miroir réfléchissant une surface fortement éclairée.

Ces remarques suffiront, sans qu'il soit nécessaire d'entrer dans plus de détails
pour justifier l'affirmation que la détermination du temps d’une oscillation laisse peu à
désirer sous le rapport de la précision, surtout si l’on dispose d’intervalles qui renferment
mille oscillations ou plus encore. Mais le résultat, auquel on parvient en opérant ainsi, a
a encore besoin de plusieurs corrections.

. La détermination de la réduction a l’arc infiniment petit n’est généralement pas
sujette à de grandes difficultés, et on pourra déduire la correction nécessaire avec une
exactitude suffisante même par des approximations peu exactes. D'ailleurs m’occupant
sérieusement de cette question, je suis parvenu à des formules (Beitrag zur Ermittlung
der Reduction auf den unendlich kleinen Schwingungsbogen, LXXX VI. Band der Sitzungs-

berichte der kais. Akademie der Wissenschaften in Wien, II. Abth., Oktoberheft, Jahr-

Annexe VIb. :

 

 
