27

 

ist als die Luft — die verticale Lage wieder herstellen, um die Continuität des Queck-
silbers am oberen Ende des Fadens wieder aufzuheben.

Wenigstens zwei derartige möglichst gleiche Variationsthermometer, A und B,
müssten gleichzeitig in Gebrauch genommen werden und zwar abwechselnd A 10” über B
und B 10rüber A (Umtausch vielleicht jede Stunde). Ein Handfernrohr zur Ablesung, guter
und immer gleicher Schutz gegen Insolation und Ausstrahlung, rasche und gleichmässige durch
mechanische Mittel erzeugte Ventilation und damit verknüpft ein leichtes rhythmisches

ea : | Schütteln oder Klopfen, um die Wirkung der Friction in der Röhre zu überwinden —
du À sind Erfordernisse und Bedingungen, von welchen der Werth der Beobachtungen wesent-
. lich abhängen würde.

 

 

 

|
NME,
4

4

 

 

 

 
