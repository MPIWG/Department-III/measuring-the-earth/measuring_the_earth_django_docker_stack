 

|
|

 

 

286

Rapport de M. le Colonel Barraquer sur la mesure de la pesanteur.

L’année derniére, a la réunion de la Commission permanente, jai eu l’honneur
d’annoncer a l’Association que je venais d’inaugurer en Espagne les travaux relatifs à
l'intensité de la pesanteur, qui, au nom de l’Institut géographique et statistique, me sont
personnellement confiés. Je possédais, à cette époque, plusieurs séries d'observations
faites au salon de la bibliothèque de l'Observatoire astronomique de Madrid, avec le grand
appareil à réversion construit par MM. Aepsold, et en employant les deux pendules, de
poids très-différents, qui battent à peu près la seconde; séries qui étaient suffisantes pour
en déduire une valeur de l'intensité de la pesanteur avec le degré de précision nécessaire.
Toutefois la circonstance d’avoir déjà alors à ma disposition un exemplaire du petit
appareil, livré par les mêmes constructeurs, et pourvu aussi récemment de ses deux pen-
dules de masses différentes, m'avait suggéré le projet de faire encore avec cet appareil
et sur le même emplacement, des expériences analogues, tächant de la sorte d’accumuler
des données pour une étude intéressante.

Aujourd’hui, j'ose le dire, ce but est atteint, puisque non seulement j’ai réalisé
ainsi une détermination complète de l'intensité avec le petit appareil, mais encore j'ai
eu l’occasion d'y ajouter de nombreuses expériences destinées à évaluer directement, avec
le grand appareil, l'influence du mouvement simultané de son support sur la longueur
calculée du pendule simple.

Je vais donc dire quelques mots sur mes nouvelles observations et exposer le ré-
sumé des résultats obtenus jusqu'a présent.

Rien d’essentiel n’a été changé aux procédés suivis dans les premières expériences
pour effectuer les observations analogues avec le petit appareil. Celles-ci consistent en

16 Séries, employant le pendule lourd, couteaux et plan de suspension en agate.
16 ” 29 7 29 léger, 7 4 ” 29

Chacune de ces séries faites en autant de jours ou séances, est composée d’ob-
servations arrangées d’après un programme symétrique, afin de mesurer alternativement
et dans les quatre modes de suspension des pendules, la distance entre les tranchants
des couteaux et la durée de l’oscillation isochrone; et en outre, au commencement et à
la fin de la série, la situation du centre de gravité du pendule. Pour les comparaisons
de l'intervalle choisi sur l’échelle avec la distance entre les couteaux, on observait ceux-
ci tantôt obscurs sur champ clair, tantôt éclairés sur champ obscur; et quant à la durée d’une
oscillation, elle résulte des valeurs observées de 2000 et 1600 oscillations respectivement pour
les pendules lourd et léger, soit des intervalles de 25 et 20 minutes environ, en enregistrant
toujours, pour obtenir chacun des deux instants qui les comprenaient, 50 passages, dans
le méme sens, du pendule par la verticale. On peut, d’aprés cela, calculer avec les don-
nées d’une seule série, les éléments dont dépend la longueur du pendule simple.

unsre fe Teen ae

 

 

 
