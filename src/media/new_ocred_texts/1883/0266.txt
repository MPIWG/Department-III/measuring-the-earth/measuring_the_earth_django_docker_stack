 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Italien. Italie.

Rapport sur l’état actuel des travaux en Italie.

Travaux trigonométriques.

19 Les travaux trigonométriques de l’Institut géographique militaire ne sont
plus sous ma direction depuis le mois de juillet dernier et sont passés sous la direction
du Lieutenant Colonel De Stefanis. Je me contente donc de vous donner un aperçu
général de la situation actuelle des travaux, en laissant à l'officier supérieur que je viens
de nommer, le soin d’entrer dans de plus grands détails, s’il le croit nécessaire.

Le canevas que vous avez sous les yeux, à l’échelle de 1 : 2 500 000, vous indique
toute la triangulation italienne. Cette triangulation couvre entièrement la superficie du
royaume. La partie qui a été observée avec la précision nécessaire pour les hautes
recherches de Géodésie, est marquée par une teinte rouge; tandis que la partie observée
uniquement dans un but topographique, ou qui n’est pas encore observée, est marquée
par une teinte jaune. Dans le projet des travaux de l’Institut géographique militaire,
déjà approuvé depuis une année par le Ministère de la Guerre, il est établi que toute la
partie péninsulaire de la triangulation doit être observée avec la plus haute précision
avant 1886. D'autre part la Commission géodésique italienne, dans sa dernière réunion
à Padoue, a décidé de faire exécuter, dans le même laps de temps, les stations de la partie
occidentale de la Sicile.

Aïnsi il est à espérer qu’en 1886 tout le réseau italien sera définitivement observé.
Cependant il resterait encore une lacune entre la Sardaigne, la Corse et le continent:

pour la combler, nous nous mettrons d'accord avec nos collègues français, en profitant de
leur présence ici.

Mesures de bases.

2% Les bases actuellement mesurées en Italie (en faisant abstraction des bases
anciennes) sont au nombre de sept, savoir:
1° La base du Tessin, en Piémont,
at x : » de Üdine, dans là Wétile,
29 y 13 } OÜkien, en Sardaione,
0 5 „9,5099, en Qapitanate,
Do, Ne Lecce en Pouille,

69 |, daROran,:en. Calabre,
1%, de Oulane, en Sicile:
On pourrait à la rigueur y’ joindre la base mesurée aux environs de Naples,
laquelle n’atteint pas la longueur d’un kilomètre.

}
|
1
|
|

DDR ramener

EEE WE

 
