 

=

 

 

ae

Frankreich. France.

RKRapport

sur les travaux exécutés en France, en 1883.

Dépot de la Guerre (Service Géographique).

Operations Géodésiques.

Les opérations sur la Nouvelle Méridienne comportent, en 1883, les mesures
dangles en 17 Stations, entre la base de Melun et St. Martin du Tertre.

5 de ces Stations (Montgriffon, Kosny, Belle-Assise, Montgé et St. Martin du
Tertre) appartiennent au réseau méme de la Méridienne.

5 Stations (Chatillon, Butte-Chaumont, Terme-Nord, Terme Sud, Terme Inter-
médiaire) servent au raccordement de la Nouvelle base de Juvisy a Villejuif avec
cette chaine.

4 Stations, convenablement choisies (Chatillon, Mont Valérien, Morlu, Bry), sont
également reliées à la Méridienne et au Panthéon. Elles forment un quadrilatère extérieur
à Paris, et chacune d'elles est destinée à devenir, plus tard, le centre d’une station
astronomique, où l’on mesurera avec le plus grand soin la latitude et un azimut, à l’abri
des fumées et des trépidations du sol, de manière à en conclure ensuite avec une grande
précision la latitude de la Station primordiale du Panthéon.

Enfin, on a rattaché au réseau l'observatoire de Paris, l’observatoire de Mont-
souris, et la station astronomique faite en 1866 par M. Villarceau à St. Martin du Tertre.

Les observations de jour ont présenté des difficultés particulières en raison du
trouble de l'atmosphère provenant des fumées des usines de la banlieue de Paris.

On a dû attendre, pour observer de jour avec de bonnes images, les rares instants
pendant lesquels la pluie ou un vent favorable, chassant pour quelques heures les vapeurs
de Paris, rendaient l'atmosphère calme et transparente.

C’est Surtout quelques heures après le coucher du Soleil, à l’heure où les foyers
s’éteignent et où l’activité industrielle diminue, que l’on a pu trouver des moments
favorables. Sans l'emploi des observations de nuit, les opérations autour de Paris seraient
restées impraticables dans de bonnes conditions de durée et de précision.

La portion triangulée à nouveau de la nouvelle méridienne de France s'étend
maintenant de 42° 30° à 49°. Il ne reste plus que 2 degrés à mesurer pour la prolonger
jusqu'à Dunkerque.

Le calcul provisoire des triangles compris entre la base de Perpignan et celle
de Melun donne, en partant de Perpignan, entre la longueur de la base de Melun
mesurée par Delambre et la longueur déduite de la Triangulation une différence de 02

Base mesurée par Delambre — 11842152
Base calculée — 11841.952

 

 

 
