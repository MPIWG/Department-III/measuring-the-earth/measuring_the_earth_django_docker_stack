 

10

Orff, €. v. Bestimmung der Länge des einfachen Secundenpendels auf der Sternwarte
zu Bogenhausen. München, 1883. (Abhdl. der Kgl. Bayer. Akad. d. Wiss.,
Bd. XIV, Abth.. 3, 2.168)

4. Belgien.

Folie, F. Sur la cause probable des variations de la latitude et du magnétisme terrestre.
(Bulletin de l'Académie Royale des sciences, etc. II. 1881. Bruxelles.)

Francois, J. Traité prat. de nivellement. Bruxelles, 1881.

Institut cartographique militaire. Triangulation du royaume de Belgique. Calcul des
coordonnées geographiques et construction de la carte. Tome III. Ixelles-
Bruxelles, 1881.

—  Nivellement général du royaume de Belgique. Cah. 8. Hainaut, 1880.

5. Dänemark.

A. Nachträge zur ersten Mittheilung der Literatur d. p. u. th. Gradmessungs-
Arbeiten.
Andrae, ©. G. Om de projective Forvandlinger, ved hvilke Fladeinholdene bevares
iiforandrede. (Oversigt over det danske Vid. Selsk. Forhandlinger. 1853,
Mai u. Juni.)
—  Udvidelse af en af Laplace angivet Methode for Bestemmelsen af en übekjendt
Storrelse vedgivene ümiddelbare Jagttagelser. (Oversigt over det Vid.
Selsk. Forhandlinger 1860, Novbr.)
—  Fehlerbestimmung bei der Auflösung der Pothenotschen Aufgabe. (A. N.
No. 1117.)
— Formeln zur Berechnung der geod. Breiten, Längen und Azimuthe. (A.N.
No. 1187 and 1272.)
Thiele, T. N. Om Anveldelse af mindeste Kvadraters Methode i nogle Tilfaelde, hoor
en Komplikation af visse Slags uensar tede tilfaeldige Fejlkilder giver
Fejlene en ,systematik“ Karakter. Kjôbenhavn, 1880.
— Sur la compensation d. qlqs. erreurs quasi-systemat. par la méthode des moindres
carrés. Copenhague 1880 et 1881.
Zachariae. De mindste Quadraters Methode. Nyborg, 1871.

B. Publicationen aus den Jahren. 1881—83.

Andrae, C. G. Problèmes de haute Géodésie. Extraits de l'ouvrage Danois: „Den
Danske Gradmaaling.“

 

 

 
