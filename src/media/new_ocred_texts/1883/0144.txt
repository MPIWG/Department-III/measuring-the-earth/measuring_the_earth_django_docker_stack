 

 

 

 

 

 

 

 

130

observatoires astronomiques, qui doit être situé de façon à ce qu’il offre des communications
faciles avec le reste du monde pour la détermination des longitudes, soit par les lignes
et les câbles télégraphiques, soit par le transport des chronomètres. Pourvu que cette
condition soit remplie, il nous semble qu’il serait pratique d'appuyer le choix du Méridien
qui offrirait le plus de chances d’être accepté généralement.

Après cet exposé succinct de la question, telle qu’elle nous a été soumise, nous
croyons devoir consulter la Commission permanente par correspondance, en priant Messieurs
les membres de bien vouloir répondre aux questions suivantes:

1) L'Association géodésique internationale doit-elle s'occuper de la question
de unification des longitudes par le choix d’un Méridien initial?

2) Consentez-vous à ce que cette question soit mise à l’ordre du jour de la
prochaine Conférence générale qui aura lieu probablement en Octobre 1883,
à Rome?

3) Puisqu’il est évident qu’une entente générale sur l'adoption d’un Méridien
initial ne sera pas possible sans la participation de l'Angleterre et des Etats-
Unis, croyez-vous qu’il faudrait prévenir, par les moyens convenables, les
Anglais et les Américains, que la question sera débattue dans la Conférence
générale, pour qu’ils puissent y prendre part, s’ils le jugent convenable?

4) L'Association géodésique doit-elle se borner à appuyer par ses voeux l’uni-
fication des longitudes, et a préciser les conditions scientifiques que le Premier
Méridien doit remplir, tout en restant neutre dans le choix méme du Premier
Meridien? ou devons nous nous prononcer en faveur d’un Méridien quel-
conque et, dans ce cas, lequel preferez-vous?

5) Voulez-vous charger votre Bureau de préparer un rapport qui pourrait servir
de base a la discussion dans la Conférence générale?

6) Pour faire avancer autant que possible la question dont il s’agit, et pour
obtenir un préavis scientifique sur la matière, aussi complet que possible,
ne serait-il pas indiqué de consulter les grands bureaux de calculs astro-
nomiques qui président, dans les différents pays, aux publications des almanachs
astronomiques et nautiques, pour consigner leur avis dans le rapport?

Nous vous prions, Monsieur et brès honoré collègue, de répondre à ce questionnaire
d'ici au 1% Mars 1883, au plus tard, et d'adresser votre réponse à l’un de nos Secrétaires,
Monsieur le Dr. Hirsch, Directeur de l'Observatoire de Neuchâtel.

Veuillez agréer, Monsieur et très honoré collègue, l'assurance de notre parfaite
considération.

Les Secrétaires, Le Président,
(signé) Dr. Ad. Hirsch, (signé) G" Ibanez.

(signé) v. Oppolzer.

La Commission permanente s’est prononcée, à l'exception d’une seule Voix,
favorablement pour l'admission de ce point dans le programme de la Conférence, et l’on

 

 
