 

le |

Th py |

bent alos & |

gem te

es expen
eel
pnt él ni

his À

tes cat
acer

4

alors éclairé et entrecoup& au milien par une raie sombre, large de 20 microns, qui
correspondait à la distance de Varéte sombre de la ligne du plus grand reflet. Le rap-
porteur croit donc avantageux de renoncer tout-à-fait au pointage des couteaux éclairés,
de ne pointer que les couteaux sombres et d’annihiler l'influence de Virradiation par des
moyens appropriés. On pourrait par exemple mettre un des fils du micromètre presque
en contact avec l’arête, de manière à laisser subsister une ligne de lumière; on peut alors
déterminer la réduction de cette mesure au milieu des deux fils, en tendant dans le
microscope un fil auxiliaire fixe; après avoir rapproché le fil mobile jusqu’à un contact ana-
logue au premier, on pointe ensuite au milieu du fil double; la différence des deux lectures
est la réduction cherchée. Le rapporteur ne croit pas ce procédé inattaquable, mais
l'erreur résultante sera de moins d’un micron et permettra donc d'atteindre pour les ob-
servations du pendule une précision tout à fait suffisante. Il serait préférable de se
soustraire à cette faible inexactitude en employant un comparateur à bout, si cela se pouvait.

Pour compléter l’exposé précédent, on peut encore mentionner une source d’erreur
peu remarquée jusqu'à présent. Le pendule à réversion sera, comme c’est facile à con-
cevoir, plus long quand le poids lourd se trouvera en bas que quand il se trouvera en haut.
On trouve donc aussi la distance des arêtes des couteaux un peu plus grande dans le
premier cas que dans le second, mais cette différence atteint à peine la valeur d’un
micron pour le pendule de Repsold. La faiblesse de cette correction est telle qu’on peut
la négliger entièrement. En tout cas, connaissant approximativement les dimensions,
la distribution des masses et l’élasticité des matières employées, ou pourra calculer la
différence des moments d'inertie dans les deux positions du pendule et déterminer leur in-
fluence à l’aide de la méthode connue.

L’emploi du pendule lourd et léger n’élimine pas cette source d’erreur; il faudrait
pour cela que la distribution des masses des deux pendules soit absolument égale, et que
l’une des matières, tout en ayant le méme coefficient d’élasticité, ait un poids spécifique
moindre que l’autre. L’échelle attachée au pendule sera sujette à des erreurs semblables;
il faudra donc les déterminer dans la position verticale et en outre établir la différence
de longueur dans la position horizontale et verticale, dans le cas où l’on aurait comparé l’échelle
avec l’étalon normal dans la position horizontale. Bruhns a déjà donné une méthode pour
l'emploi de ce procédé (Astron. geodät. Arbeïten im Jahre 1870, pag. 137). La comparaison
de l'échelle avec l’étalon normal sera d'autant plus facile à effectuer, que les longueurs
des deux différeront moins. Le rapporteur croit donc d’un avantage assez précieux l’em-
ploi du pendule d’un mètre, qui facilite, en outre, considérablement, en se servant du pendule
à secondes, l'emploi de la méthode des coincidences. Pour des instruments de voyage
des dimensions plus petites seraient convenables, mais pour des déterminations absolues
qui ne nécessitent que rarement des transports et encore dans des contrées où l'on à pour
ces expéditions à sa disposition toutes les ressources des temps modernes, ces considérations
n’ont que très peu d'importance. a es

On a remarqué dans ces derniers temps que la stabilité que Repsold a donnée au
support du pendule de Bessel laisse beaucoup à désirer et on a tiré de ce fait un nouveau

. wur et ay à fair 21 FIL-
reproche contre ce pendule, quoique le manque de stabilité n’ait rien à faire avec l’instru

7
Annexe VI b.

 

 
