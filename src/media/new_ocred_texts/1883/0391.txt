;
SS ‘+

 

 

 

 

 

 

 

 

 

 

Autriche-Hongrie.
D ssn snuruusresecrasseiuueeunaseensea
INDICATION ck DIRECTEURS
N° LATITUDE. |LONGITUDE.| EPOQUE. z
DES POINTS. ar At One Aneen INSTRUMENTS. REMARQUES.
nenn 45° 97' 16") 37°21'16"| 188 Moe a le
| Ne nicer ae 45 16 28 37 17 48 eo ee id. Le a Sn
| Bassahid . . . ... 45 38 34.138 5 O|| 1880 id. id.
Cerevie (Lipa). - . | 45 12 23 | 87 20 27 | 1880 id. id.
aan. rs = e + 2 2 + | ae Kalmar. . Starke Er de 10p. ||
ee mi AA Ae OS Eon Ss Te + In: as |
Ion D Hr = in 2 2 2 I Waitz. : Starke ae de 10p. |
De nr: 2 SE SS I | oe : (OS ic . |
nn en A a ie 2 = 5) | ns Randhartinger. Starke n° 1 de8p. |
«resaniCa. : A JO | 02 id. i I
an 3. 44 18 9 | 36 1245| 1882 id. Starke ue de 10p. |
Cvrikovac . . . .. a 17 27 35 = | 1882 S id. Starke n° 1 deSp.
Vlasié. . ...... = 17 2 35 = 2 1882 Waitz. Starke n° 5 de 10p.
Vikva......-. | 43 56 21 Be 24 37 | 1882 Iehrl Starke n° 4 de 10p.
Bukovik .... 43 56 = 56 6 59 is = 1889: Randhartinger. Starke n° 5 de 10p. ||
530 | Zcp brdo...... 44 3 50 36 41 46 | 1882-83 | Randhartinger, Lehr]. Id.n’4et5delÖp.
5302| Orlic ..... ..- 43 52 48 Jo 1.46; 1882 Kalmar, Starke n° 2 de 10p.
| Nw. ee: : 43 49 53 | 35 57 30 1882 id. Id. n° 2 et 5 de 10p.
| v. Hlidze |
| Trebevic...... 43 49 9 36 138: . 1889 id. Starke n° 2 de 10p, |
| 86. B. en = 43 48 13 | 35 59 28 1882 id. Starke n° 4 de 10p.
v. Flidze.
Bman.....:. 43 48 59 35 54 54 1882 id. Starke n° 2 de 10p.
| Bone inne E a > > of - ae Lehrl. : Starke n° 4 de 10p.
PHOTOVAC . . . . 3 8.) 36 2 883 id. id.
Poljana. ..: . :.. ook. 2 | 36.47 19 1883 id. id
Magli¢ ....... 43 16 = 36 24 0 1883 Nahlik. Starke n° 5 de 10p.
Mees ik as orl igo | aa ec
ON ER = | Oz 1a. 1a.
Uvestniea...-.. 43.36 0 | 35 33.52 |’ 1889 id. ad.
mir :....... 43 19 40 | 35 41 2 1882 id. id.
PHGCEeut.. : .... 43 5 59 | 35 41 20 1881 id. id
Siljevac. . . . . 49 52 43 | 35 49 55 | 1881 id. id.
Bi. yy 42 54 34 | 35 56 43 |: 1881 id. id.
Bear ......, 42 44 44 | 36 O0 37 | 1881 id. 1d
Gilfertsberg . . . . | 47 16 8 | 29 24 32 | 1851-52 | Fuerstein. Starke de 10 p.
D ctenwand … 32 me 2 I à es Muszynski. Starke n° 3 de 1Op. 1. 19, Baviaro
PÉOKOE SD. . . . .. i 852-53 : id. id. ee
Sitileberg ..... 47 11 35 | 28 59 27 | 1852-53 | Fuerstein, Ganahl. Starke n° 6 de 10p. Ne 80, id,
Hochplatt ..... 47 33 12 | 28 30 28 | 1853 Muszynski. | Starken°®3del0p. | x. 55, a.
Griintenberg. .. . | 47 83 21 | 2759 8}; 1853 Pechmann, Muszynski. Id.ne 3 etd delOp. Wig gaa
Pfender Bg ....| #7 30 30 | 27 26 44 || 1852-53 | Pechmann. Starken®1de10p. ‘| x 95 gnisse
| etErtelast.del2p. | 102
Baepels. ...... 47.22 56 | 27 8 Oi 1852 id. Ertelastron.de 12p.| N. 4, ia.
Hohe Freschen 41 18 28°| 27.26 35 || 1852-53 id. Starke n° 1 de 10p.
emis: 1. iw. Ar 19. 1197:031 1852 Breymann. Bet
553 | Frastenser Sand . | 47 11 44 | 27 14 40 | 1852-53 | Pechmann. u n° 1 de 10p.
| et Ertelast. de 12p.
554: | Kamegg. . ..... A 0 127 GE 1859 Breymann. Reichenb. de 12 p.
555 | Fundel K...... 47.640.| 27 20 3] | 1853 Pechmann. Starke n°5 de 10p.
556 | Schwarzhorn..... | 47 2 ai 2782 8 |» 1858 Breymann. Starke n° 4 de 10p.
ot mores Be eo i ie a a = zy | 1853 | Pechmann. : Starke an de 10p.
5 CRC HELD . . :.. L D 55 | . 1853 id. id.
559 | Stans Kopf .... | 47 oa 28 | 27 58 14 | 1853 Breymann. Starke n° 4 de 10p.
I el an... i Te 7 2 a 2 | ae Muszynski. ' Starke a de 10p. | N. 56, Bavière.
e Mutiekopf . .. . ... 922. 5 id. td.
562-| Birkkogl...... 47 13 58 | 28 39 55 | 1853 ia id.
Mi don or ag an) tee, | Be, ee
allula Sp... 5 id. id,
i .. ... ir De 1853. id. id.

565

 

 

 

28 052

 

 

 

 
