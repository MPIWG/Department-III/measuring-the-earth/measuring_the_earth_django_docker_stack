 

Ei.
aa
Publi
i Eye
de

ent di
te de
l'autre
Tet du
€ I
upeché

scepter
adopté
t zen
ans À
Stert-
ame.

ri li

epirt
antes

np

 

E
5
Ë
i
Ë
F
,
E
5
F

137
mission permanente a décidé d’accepter la proposition faite dans la séance du 17 Sept.
1880 à Münich, c’est à dire que dorenavant il conviendrait de renvoyer & l’examen du
Comité international des poids et mesures non seulement la question soulevée par M.
Förster, mais en général toutes les comparaisons d’étalons qui intéressent la mesure
des dégrés.

Par conséquent les comparaisons d’étalons ont passé du Bureau Central au
Comité nommé.

J’ai dû approuver la décision de la Commission permanente, parce que le Bureau
central ne possède toujours pas de laboratoire spécial pour ces travaux, et parce que
Messieurs Förster et Schreiber ont contesté la réalité des changements des coëfficients
de dilatation que j'avais découverts en 1846 et qui ont été confirmés par une commission
belge en 1854, de sorte qu’il était désirable et même nécessaire de soumettre la question
à un jugement impartial. Ce jugement impartial a eu lieu. La commission géodésiqne
suisse à constaté un changement décisif des coefficients de dilatation adoptés pour la
règle en fer de 4 mètres de l'appareil espagnol avec lequel elle a mesuré les bases
suisses. Ensuite M. le Général Ibanez a determiné à nouveau, pour cette règle, le
coefficient de dilatation qui avait été déjà déterminé 20 ans auparavant, et il à trouvé
une différence considérable. (Voïr Procès-verbal de la Commission géodésique Neuchâtel
1883 page 24.)

Messieurs Forster et Schreiber, en étalonnant les règles de bases de l’appareil de
Bessel auraient bien fait de déterminer de nouveau le coefficient de dilatation.*)

Le Président
de l’Institut géodésique prussien
signé Baeyer.

M. Æirsch regrette de ne pas pouvoir lire la traduction de ce rapport, qui n’est
parvenu à la connaissance du bureau que quelques instants avant l’entrée en séance.

M. le Président, conformément à l’ordre du jour établi par la Commission per-
manente dans sa séance préparatoire d’hier, donne la parole à M. Æirsch pour présenter
le rapport du bureau sur la question de l’unification des longitudes et des heures. Voici
ce rapport :

*) Pour les Publications voir les titres dans le texte Allemand à la page 22 etc.
18
