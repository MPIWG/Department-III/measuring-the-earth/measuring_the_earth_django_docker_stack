 

TEE EEE EN EEG I SO TTD

we
el
Ir
u
Be

   
 

 

ON OPN ae ee 5
ie » Pee

Triangulation durch Mecklenburg.

07 =

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

5 INDICATION | : DIRECTEURS |
N° LATITUDE. LONGITUDE.|  ÉPOQUE. INSTRUMENTS. || REMARQUES.
DES POINTS. | ET OBSERVATEURS. |
| |
126 | Gollwitzerberg . . | 52°21’ 9”| 29°58' 12” Cercled’Ert.de15p. ||
ioe | Arneburg *. ...°. 52 39 36 | 29 40 0 id. |
128 | Landsberg... . . | 52 27 51 | 2920 0 id. |
129 | Dolchau ...... 52 41 48 | 29 9 5 | id. |
130 | Polkern .. =. 1552.49.18 | 29.2336 || id. |
132  Hönbeck.. . .. ... 539345120 622 na |
132 | Woltersdorf....| 53 5 22 | 29 50 0 ice
12 | Wittstock . ...'.: | 93 20, O | 30 4 55 | sd |
134 | Ruhnerberg . . .. | 53 17 6 | 29 32 42 | id. |
| |
Triangulation in Ostpreussen.
135 | Königsberg ...... | 54°42'50") 38° 9'31"|| 1836 | Bessel, Baeyer. Cercle d’Ertel de || Siehe: «Die Grad-
| | | 15 p- ee
foe Wildenhof. . :.... | 542037 | 38 4:29 | 1833 id. id. Die. gen Goes
Ber Irunm.....:.: SES 97 dl 5a 1633 id. id. | dinaten sind von
138 | Galtgarben. .... | 54 48 95 | 87 54. 3 | 1833-34 id. id. RE
Rd Memel. ....... | 55 43 43 |-38 45 39 | 1833-34 id. 10: :
140 | Lattenwalde. . .. | 55 6 16 | 38 24 48 | 1834 id. id.
fal | Nidden...... ©. | 55 18 23 | 88 39 32 | 1833-34 id. id.
er Bepazm.....:.|55 3815 | 3918 27 1833 id. id. N. 421, Russie
Bi,  Alseberg...-.... 55-1741 1239 13.431 1833 Kulenkampf. Theod. de Pistor et
| Martins de 12p.
144 | Kalleningken .. 55, 9:96 38 59. 2 1833 id. id. N. 393, Preussen.
Er giee........ 59.0 50.38 54.21 | 1833 id. id.
146 | Condehnen..... 54 AT 12 |.38 21 32 | 1833 Bessel, Baeyer. coe d’Ertel de
D.
147 | Degitten....... 54 50 41 | 38 41 18 1833 Baeyer, Kulenkampf. Théod. de Pister‘
et Martins de 8 p.
148 | Haferberg . .... 59-41.50.| 38. 9.58 1833 Bessel, Baeyer. Cercle d’Ertel de
| Join:
149 | Fuchsberg . . ... 04 47 21 | 38 4 40 | 1832-33 id. : id. | Nicht in die Karte
150: | Mednicken..... | 54 46 30 | 38 1 40 | 1832-33 id. Cercle d’Ertel. | eingetragen.
Beenmk........ af 45. 5230 258 1832-33 rd id.
152 | Wargelitten . . 54 45 7 | 38 O0 56 | 1832-33 id. id.
Küstenvermessung.
136 | Wildenhof..... Wee : à | | Siehe: « Die’ Kus-
Bm ..:,... j Déjà mentionnés. | N
153 | Sommerfeld . 54° 3790" | 37°35'48/| 1837 Baeyer, v. Mörner. Cercled’Ert. del5p. | ainaten rae on
foe Talpiiten, .....\ 54 0 7 | 37 20 26 | 1837 id. id. Trunz aus gerech-
182 | Brosowken. . . .. | 53 56 34 | 36 52 39 | 1837 id. id. un
155 | Buschkau..... . 54-13 12 | 36 3 46 1837 id. id.
156 | Dohnasberg .... 54 28 15 | 36 6 5 1837 id. id.
Ba Steegen...-... 54 20 39 | 36 46 40 1837 id. id.
158 | Thurmberg .... | 54 13 32 | 35 47 19 1837 id. id.
159 | Schénwalde .... | 54 28 96 | 35 53 44 1837 id. id.
160 | Boschpol......| 5433 2| 3536 4 1838 Baeyer. id.
fol) Kistowo ...... 52 1238 | 35 25.26 1837 Baeyer, v. Mörner: id.
162 Muttrin....... 40 3503 1838 Baeyer, Bertram. id.
Bo Beyekol....... 54.939,23 | 54 52 31 | 1838 id. id. |
164 | Pigow .. | 54 28 98 | 34 11 17 | 1838 id. id. |
165 | Baremberg..... | D4, 549. | 34:95 47 1738 id. id. |
166 | Gollenberg. . . .. | 54 12 98 | 33 53 40 |: 1839 id. id. |
27 Klorberg....,. 53 5149| 3827 51 | 1839 id. id.
ive Colpere. ..... . 54.1086 | 38: 14 28 1841 v. Mörner. id,
169 | Sprengelsberg. . . | 53 54 57 | 32 46 51 | 1841 Baeyer, Bertram. id.

   
    
   
  
  
     
   
   
    
   
 
   
   
   
    
  
  
  
    
     
    
    
    
  
 
   
   

 

   
