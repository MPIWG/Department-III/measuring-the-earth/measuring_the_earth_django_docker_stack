 

 

 

224

2. Das Bayerische Pricisionsnivellement

betreffend, so habe ich bereits in meinem vorjährigen Berichte erwähnt, dass die 243'/,
Kilometer lange Fichtelgebirgsschleife, deren erstes Nivellement einen Schlussfehler von
10.8 Centimeter und einen Kilometerfehler von 7™™ hatte, im Jahre 1882 zum zweiten-
male vollständig und auf das Genaueste doppelt nivellirt wurde, um den fraglichen Fehler,
der unmöglich in solcher Grösse von Störungen in den Schwererichtungen herrühren
konnte, und den ein an der schiefen Ebene zwischen Neuenmarkt und Marktschorgast
schon früher vorgenommenes partielles Nivellement nicht aufzudecken vermochte, aufzu-
finden. Dieses gelang auch, wie meine ebenfalls in den Denkschriften der K. Bayerischen
Akademie der Wissenschaften publieirte und an die Herren Commissare versandte dritte
Abhandlung des Näheren nachweist, nämlich

c) „Das Bayerische Präcisions-Nivellement.“ Sechste Mittheilung. München

1883, Verlag der K. Akademie (Classe I, Bd. XIV, Abtheilung 3), in

Commission bei G. Franz.

Zu dem Entschlusse, die Fichtelgebirgsschleife zum zweitenmale, und in Bezug
auf die des schon erwähnten Fehlers vorzugsweise beschuldigte schiefe Ebene sogar zum
drittenmale doppelt nivelliren zu lassen, bestimmte mich nicht allein der Wunsch nach
vollständiger Aufklärung des in Rede stehenden Fehlers, sondern auch der Beschluss des
im Jahre 1881 in Venedig tagenden internationalen geographischen Congresses, die
internationale Gesellschaft für die Europäische Gradmessung zu ersuchen: „sie möge
das Feld ihrer Untersuchungen auch auf die, durch periodisch zu erneuernde Präcisions-
nivellements messbaren Bewegungen der Erdrinde ausdehnen.“

Das neueste (zweite) Nivellement der Fichtelgebirgsschleife hat nun zwei wich-
tige Resultate geliefert:

Erstens das, dass der grosse Anschlussfehler des älteren im Jahre 1869
hergestellten Nivellements wirklich nur von ungenauen Beobachtungen der damit be-
trauten Ingenieure herrührt. Diese Ungenauigkeit fand aber nicht, wie anfangs ver-
muthet wurde, an der schiefen Ebene der Südnord-Eisenbahn bei Neuenmarkt, sondern in
der Strecke zwischen Haslau und Franzensbad auf der Bahnlinie Oberkotzau-Eger statt,
und zwar in Folge eingetretenen schlechten Wetters, durch das sich die beiden nivelliren-
den Assistenten leider nicht abhalten liessen, den Abschluss der genannten Strecke,
welche bei guter Witterung zu nivelliren begonnen worden war, noch vor Abend her-
beizuführen. Wenn hier eingewendet werden kann, dass nach unserem in der ersten
Mittheilung über das Bayerische Präcisions-Nivellement (Seite 14 bis 24) beschriebenen
Nivellir-Verfahren ein mehrmaliger und gleichgerichteter Irrthum zweier zusammen-
arbeitender Ingenieure fast unmöglich erscheint, so ist dagegen zu erinnern, dass damals
(vor beinahe 15 Jahren) die nur näherungsweise hergestellte Grösse des Einflusses der
geneigten Libelle den Standfehler noch nicht so genau erkennen liess als dieses später
mit Hülfe von Correctionstafeln möglich war, wo von jeder Aufstellung aufs Bestimmteste
gesagt werden konnte, ob die Beobachtungen zu wiederholen sind oder nicht. (Vergl,
Seite 17 der I. Mittheilung.)

 

 

 

 

 

 

 
