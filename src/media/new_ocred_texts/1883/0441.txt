BE ee

 

Vereinigung der Preussischen und Russischen Ketten, A. über Tarnowitz.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

— ._ INDICATION | DIRECTEURS | |
; Ne | LATITUDE. LONGITUDE.| ÉPOQUE. INSTRUMENTS. REMARQUES.
3 DES POINTS. | ET OBSERVATEURS.
y E: : : a | E we ee
i 72 | Trockenberg. . . . | 50°24’ 44"| 36°32! 30” 1852 v. Hesse, v. Tiele. Instr. Univ. de Pi- ee ¢ Din Ver-
In stor et Martins oe ate ue
N de 13 p- et cercle || schen Dreiecksket-
3 d’Ertel de 15 p- ten bei Thorn und
: qf Lubschau. ..'. . . 50 36 48 | 36 39 16 1852 v. Hesse, Stiehle. Cerele d’Ertel de || Ternowitz.>
15 p.
74 | Orzesche...:.... 50.93.59 36°27, 10 1854 id. E id.
75 | Eubetzko...... 50.41 57. 36 18 45 1852 id. id. Die geogr. Coor-
Mor Annabers ....', 027 2711-38) 50 921 1852 id. id. dinaten sind von
fe | Pschow....... "50 2 34 | 56.3 34. 1852 id. id. Po
78 | Markowice. . . .. | 50 33 38 | 36 49 53 | 1854 v. Hesse, v. Morozovicz. id.
Hi ivsee....:.. 00 4126.90 4421) 1852 id. id.
80. | Grodziec . . . . .. DO er IE 964555 |. 1922 id. id.
Vereinigung der Preussischen und Russischen Ketten, B. über Thorn.
el | Birschau....... | 54° 4°42") 36°25'50"| 1853 Braeyer, v. Wrangel. Instr. Univ. de Pi- dd :
stor et Martins || Dis seer, Guard
de 13 P- aus gerechnet.
82 | Gross-Waplitz
(premiérement
Brosowken). . . | 53 56 34 | 36 52 39 1853 id. id.
83 | Krastuden . .... 53 52 26° | 36 49.28 1853 id. id.
et Peplin..:..-.. 5355 45 | 36.19.59! 1853 id. id.
5 Mahren.....v. | 53 41 42 | 36 46 20 1853 v. Hesse, v. Gottberg. Cercle d’Ertel de
Loa:
86 | Rynkowken .... | 53 40 54 | 36 16 39 1853 Baeyer, v. Wrangel, Hesse, | Cercle d’Ertel de
Gottberg. 15 p. et Instr.Univ.
de Pistor et Mar-
tins de 13 p.
Sf | Peterhof...... 09 29 52 | 30.988 1853 v. Hesse, v. Gottberg. Cercle d’Ertel de
: 15 p.
88 | Lopatken...... »8.20=52° 36.39 OF, 1853 id. : id.
Seep Out 33.20.52 | 26° 5: 8 1853 id. id.
90 | Blendowo ..... bo 21 83) 36 25 24 1853 id. id.
JE, Culmsee.....°. . 83.1 13 20.16 40 1893 id. id.
92 | Glascejewo..... be NO DAVE oo 3 1 1853 id. id.
or Geta... . . 09 250 46 9 937 1853 id. id.
ee Bhom........ 53.029186.16 7 1853 id. id.
95 | Dobrezejewice. . . | 53 1 6 | 36 30 37 1853 id. id.
96 | Kowalewo . . ... 53 92%) 36 33 42 1853 id. id.
: er Bubk:!....... 93.324 56.41.12 1853 id. id.
98 | Racionzek . . ... 525] 25 : 36:27 58 1853 id. id.
E Vereinigung der Preussischen und Russischen Ketten, €. Anschluss an das Schlesische Basisnetz.
ws 99 | Bischofskoppe. . . | 50° 15'31"| 35° 5'39") 1854 v. Hesse, v. Gottberg. Cercled’Ert.de 15 p. id.
7 100 | Schneeberg . 50 12 33 | 34 30 48 1854 id, id. N. 11, Autriche-
Ee Kor Essen. .,,.... 50 47 39 | 35 13 21 1854 id. id. Hongrie.
3 207 | Zöbten ....... oo Ol Be) 34 29 94 || +1854-62 | v- Hesse, Habelmann, Bae- | Instr. Univ. de Pi- | Die geogr. Coordi-
k yer, Stiehle. stor et Martins de | a sind v. Bre- |
E 13 p. et Pistor et | slau aus gerechnet.
E Martins de 18 p. |
103 | Rummelsberg . . . 50 42 15 | 34 46 34 | 1854-62 | v. Hesse, Löwe, Staven- | Pistor et Martins
N hagen. de 8 p. et Cercle
"TE d’Ertel de 15p. |
| E Bei... 50 55 28 | 34 54 18 || 1854-62-65 | Baeyer, Stiehle, Löwe, Sta- | Instr. Univ. de Pi-
F | venhagen, Sadebeck. stor et Martins |
3 | | de 13 et 8p.

 
