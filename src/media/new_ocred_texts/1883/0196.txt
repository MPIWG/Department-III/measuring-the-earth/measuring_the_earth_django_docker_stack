 

 

 

182

Sans doute, on peut commettre quelquefois dans cette logique du progres, des
exagérations passageres, parmi lesquelles M. Foerster croit devoir classer la division
decimale du jour; mais l’unification des longitudes et des heures, dans l’esprit modere
des propositions du Rapport, n’appartiennent certainement pas à ces exagérations.

Quant à la suppression de calculs inutiles dans certaines éphémérides, M. Foerster
est d'avis, qu'on ne devrait jamais justifier un travail inutile par la seule considération
qu'il fournit un moyen d'exercice ou qu’il entretient le feu sacré pour le travail astro-
nomique. Il incombe à la science une telle foule de travaux nécessaires, que tous ceux
qui participent à la direction des grands bureaux scientifiques, ont le devoir de simplifier
autant que possible leurs programmes, et de s'entendre avec leurs collègues sur une
division rationelle du travail, afin de gagner la plus grande somme possible de forces
pour les tâches indispensables.

M. Perrier n’est pas opposé à l’adoption d’un méridien initial; mais il doit avouer
tout d’abord que, comme géographe, topographe ou géodésien, la question le laisse bien
indifférent: il n’est pas un partisan chaleureux de cette réforme si préconisée de nos jours.

En géographie, par exemple, on à à considérer des cartes générales embrassant
le globe entier, planisphères ou mappemondes, ou des cartes comprenant des régions
déterminées d’une grande étendue, telles que la surface entière ou une portion considérable
d’un grand pays, et, dans les deux cas, il importe peu qu’on ait adopté tel ou tel
méridien initial. Chaque carte donne, en effet, approximativement, les différences de
longitude et, si l’on veut des renseignements plus précis, on est toujours réduit à les
demander à un tableau général des positions géographiques, analogue à celui qui est
donné dans la ,Connaissance des temps“, et rapporté, pour les longitudes, à la même
origine que la carte.

Pour les cartes topographiques, il n’y a aucun avantage à adopter un même
méridien initial. Chaque feuille de la carte porte, en effet, un canevas de méridiens
gradués et le lecteur peut y trouver, avec une certaine exactitude, les différences de
longitudes des points qui l’intéressent, en même temps qu'il peut se faire une idée bien
nette de la distance en longitude entre la feuille considérée et le méridien de départ
ou méridien national. — Rapporter chaque feuille à un méridien lointain ne pourrait
qu'introduire quelque confusion dans l'esprit. Si même toutes ces feuilles sont assem-
blées sur un panneau commun, comme cela à été fait pour la carte de France en 1878,
nest il pas évident que, pour le topographe, le méridien national fournira une ligne de
repère plus assurée, plus commode que tout autre méridien plus ou moins lointain.

Mais, dit-on, si les cartes ne sont par rapportées à un même méridien, comment
pourra-t-on comparer les régions limitrophes de deux cartes voisines? Cette objection
n’est par fondée; car les cartes topographiques des divers pays ne sont exécutées, ni à
la même échelle, ni d’après le même système de projection, et le seul moyen pratique
de les raccorder entre elles, consiste à dessiner, au delà de chacune d’elles, une bande

supplémentaire d’une certaine étendue, dont les positions et les détails sont fournis par
la carte voisine.

 

 

 

 

 

 

 

 
