a N cee

B. — Dreiecksnetz durch Mecklenburg.

INDICATION DIRECTEURS
: LATITUDE. INSTRUMENTS.
DES POINTS. ET OBSERVATEURS.

v. Quitzow I.

Zehna 53049’49”\ 29° 47’51” 1856
Allmer.

Hoheburg ..... | 53 51 41 | 29 29 Oi 1856
Rostock 54 5231| 29.47.47 |
phipnltz.  . : . . 5214.39 | 30 41. . 1807
Maur. .  |.b4 9171830 1410 |
Hiddensoe 54 35 5% | 31 46 59 |)
Schmoekberg .. . | 53 20. 30..2 50
Hartberg. 53 47 49 | 30 21 23 |
Kraase , 53 6.) 30. 34 47, 1857.
Wesenberg.... 35 | 3040 4 1859 Kundt.
 Keulenberg . .. 37.| 30.50 24 |’. 1859 Allmer.
Fürstenberg. .... 30. 5027: 1859 id»
Gransee 30 48 20 | 1859 Kundt.
Feldberg Ble 7 |< dead. Allmer.
Helpterberg.... 31 16,22 |} 41859 v. Quitzow I.
Friedrichsruh . . . 30 47 4| 1858-59. | Allmer.
Tenzerow ..... 30 51 50 | 1853-59 | Kundt.

v. Quitzow I.
N.-179, Lrusse
N. 18, Dänemark.
1856-57 | v. Quitzow I.
1857-58 10,
Allmer.

Univ. de Pistor et Martins
n° 1 et 2 de 10 p.

Instr.

 

 

 

 

 

 

|
Il

— Ketten der Königl Preussischen Landestriangulation.

1. — Schleswig-Holsteinische Kette.

Siehe: « Die Kö-

Baursberg
Rathkrügen ..
Kaiserberg

|| niglich Preussisc):
Landes-Triangul:-
| tion, Hauptdrei-

|| ecke. II. Theil. Erst-

Schweinhagen .
|| Abtheilung. >»

Nindort........
Hohenhorst ....
Hochhelzberg . ..
Westensee
Rauheberg
Scheelsberg . .
Ostenfeld
Warkshôhe. . . ..
Nordhoehe.....
Scheersberg . . .
Roest. .
Stagehoehe....
Wongshoehe. .
Rangtang
Baerenwalde. . .
Hiigeberg

Instr. Univ. de Pistor et Martins de 10.p.
Cercle d’Ertel de 15 p.

Effnert, Liegnitz, Morsbach, Steinhausen.

 

 

 

 

 

 

 

 

Anmerlung : Die n° 291-304 sind unter Sachsen aufgeführt.

ll. — Die Märkisch-Schlesische Kette.

Brandberg 235,48. 309 13.51
Gr. Radisch.. .

Hochstein bi Al AS
Rückenberg . . . . | 51 36 20
Hutberg 52 0 57
Ziegelberg I ja],
Boosen 21 37

niglich Preussische
Landes-Triangula-
£ 5
= a | tion. Haup tdrei-
47 54 | 0 .
12 19 | a | laff, Neumeister, v. Prond-
49 39 zynski, Schreiber, v. Vie-

5 98 tinhoff. |

Siehe: «Die Kö-
|
|

ecke. II. Th. Zweite
Abtheilung. »

Die Coordinaten
sind von Berlin aus
|| gerechnet.

|

|

CH SIEH
et Mar-

N bo bo bo
Univ. de

Instr
Pistor

© ©
tins de 10 p.

v. Graberg, Haupt, Mitz- |
|
|
|

 
