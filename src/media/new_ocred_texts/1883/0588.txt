 

 

16

Setzt man AM = s und bezeichnet die dem Elemente Um = ds entsprechende
Krümmung mit de, so wird
l

à 5 ‘ dé |
Br= f(—-9$æ%...........:..6
‘ ( ) ds
0
Folglich, wenn man statt s den entsprechenden geocentrischen Winkel g als die
absolut Variable einführt, mit ® die geocentrische Amplitude des ganzen Lichtstrahls be-
zeichnet und dabei bemerkt, dass man ohne merklichen Fehler
BD et

annehmen darf:

D D
®—o do 75 (1 OL :
04 — [= dp dp == 2 / 1 —— = ke dg . een 6
0 0
Kennt man nun von
r dn
BR. -—
“n dr

die beiden Werthe %ı für den Punkt A und 4, fir B, so wird man als einfachste Hy-
pothese annehmen diirfen
ka — kp

kg = ka — xp, Wa=
Dadurch wird
&D

¢ { p 3 7

a 2 fli Er A) (ka FE xp) ap,
0
also
04 = ka D — À x D?

oder endlich, wenn man den Werth von x einsetzt und die Bezeichnungen
Qa = k4®D, 0) = kp®, A == Oa SE 0

einführt, als Ausdruck für die Refraction n A

C4 = Qu — 34

=]

und für die Refraction in B
Qn =o +34

Richtiger wäre es jedoch, unter Voraussetzung eines mit der Höhe gleichmässig
varlirenden %, die Form zu wählen :

ky — ko u (p Ss po) . ee ee Sie ete . 8

 

 
