| 126

| Vu l'importance exceptionelle de cette réunion triennale, il est d’autant
plus a regretter que, pour la première fois depuis que l’association existe,
| nous soyons privés du concours personnel de son illustre fondateur, Son
Excellence monsieur le général Baeyer, retenu & Berlin à cause de la faiblesse
de sa santé. Nous serons donc obligés, bien à regret, de nous contenter de
ses opinions si dignes de respect, nettement exposées, du reste, dans les
II instructions qu’il a communiquées à un des membres auquel il a donné des
Il pouvoirs. Je constate ici notre douleur causée par son absence, et je m’empresse
| d’ajouter que nous tächerons tous de nous inspirer de son large esprit scientifique.
Li Les sentiments que nous éprouvons de nous voir réunis dans la ville
| éternelle, sont si puissants et si généralement ressentis par tout homme cultive,
| qu'il est inutile de les exprimer longuement.

| En tout cas, à ces sentiments nous joignons ceux d’admiration et de
reconnaissance pour les Italiens qui, de tous temps, ont cultivé la géodésie et
| les autres sciences desquelles elle relève. La liste en serait trop longue, mais
| comment ne pas citer le nom glorieux de Galilée et, comme plus spéciaux en
géodésie, ceux de Manfredi, Beccaria, Boscowich, Inghirami, Carlini, Plana,
Fergola, Oriani et Secchi?
| Depuis que notre Association fût créée, l'Italie a été une des nations
ll qui ont le plus contribué aux grands travaux de l’oeuvre commune; et puisque
| la tête de l’entreprise ne se trouve pas parmi nous, permettez, Monsieur le
Ministre, qu'un simple collaborateur remercie le Gouvernement de Sa Majesté
le Roi d'Italie, de son puissant et précieux concours.

 

 

 

—

M. le général Ibanez, conformément a l’article premier du Reglement, propose

|

| . 17e > . . 2

| Ce discours est accueilli également par les applaudissements unanimes de l’assemblée.
| un ;

| au nom de la Commission permanente de constituer le bureau de la maniére suivante:

|

|

|

M. le Colonel A. Ferrero, président.
MM. v. Bauernfeind et Faye, viceprésidents.
MM. Hirsch et v. Oppolzer, secrétaires.

 

|
|
S. E. M. le général Laeyer, président d'honneur.

L'assemblée adopte à l’unanimité cette proposition.
ji M. le Colonel Ferrero prend place au fauteuil du président, et remercie l’assemblée |
| de la confiance qu’elle vient de lui témoigner.

| MM. les Secrétaires donnent lecture, en langue française et allemande, de l’ancien

 

 

 

 

 

 

 

 

il Reglement, qui est adopté par la Conférence.
ll | M. le Président donne la parole aux Secrétaires pour lire, en allemand et en

| francais, le rapport de la Commission permanente sur l’année 1883.
| Ce rapport est ainsi concu: |

 

 

 
