 

gs

(les:

y À
8 (ont ih
nt he
OW
end |
DD |
tt |

DU ik |
dams be |

eb silt |
indé
apparel m

ELLES
raie |
oni |

N de ps
k ras

ur

jo

isi |

47

Les considerations sur l’élimination de la cylindricité des couteaux perdent leur
valeur quand la forme considérée des couteaux ne répond pas aux circonstances réelles.
Des erreurs essentielles dans ce sens se montrent quand la durée des oscillations devient
une fonction de l'amplitude, abstraction faite de la réduction à l'arc d’oscillation in-
finiment petit.

En combinant pour la déduction du résultat les durées d’oscillation correspondant
à des amplitudes égales dans les deux positions du pendule, on éliminera, quant à
l'essentiel, les différences provenant de ces erreurs de forme, pourvu que ces écarts ne
soient pas causés par des défauts irréguliers dans la forme des couteaux. Cette élimi-
nation n’aura plus lieu si l’on combine les résultats de différentes amplitudes dans les
deux positions ; il est donc avantageux de disposer les observations du pendule de telle sorte
qu'on commence les observations toujours avec la même amplitude et qu’on les finisse
toujours avec une certaine amplitude, parce qu’on arrive de cette manière à des résultats
presque entièrement indépendants de la forme des couteaux et, éventuellement, on évitera
aussi l’influence de l’air dont on à parlé plus haut.

Les résultats auxquels nous sommes parvenus jusqu'à présent, ne nous autorisent
pas encore à baser le calcul sur la distance des arètes visibles des couteaux, même si l’on
pouvait considérer les sommets des couteaux, alors qu’ils n’ont aucune charge, comme étant
des lignes parfaitement droites, car le poids du pendule reposant sur les couteaux est suffi-
sant pour déformer, quoique dans une faible mesure, soit les couteaux, soit le support, en
causant dans le premier une certaine convexité, dans le second une dépression plus ou
moins forte.

La détermination de la grandeur et de Vinfluence de cette déformation présente
de grandes difficultés. Les expériences de Sabine et de Bessel, par exemple, ont montré
à quel point les oscillations du même pendule ont donné parfois dans leur durée des
différences sensibles et dépendant de la dureté des supports, même quand ceux-ci ont été
faits de la même matière et construits d’après les même procédés.

Une simple considération qu'on peut aisément traduire en formule mathématique,
montre que la proposition de Oellerier, de faire osciller un second pendule d’un poids
notablement différent, mais d’une distance de couteaux presque égale, peut éliminer
entièrement ces erreurs, à la condition de faire osciller ce second pendule sur les mêmes
couteaux et sur le même support.

Bien que cette proposition n’ait été faite en réalité qu’en vue de la détermination
des corrections dues à l’oscillation du support, elle peut cependant trouver ici son appli-
cation. La petitesse des variations produites et la circonstance qu'elles se trouvent
absolument dans les limites d’elasticité des matières employées, nous permet de supposer
ces corrections proportionelles à la pression exercée. Dans cette hypothèse nous aurons
en désignant par o la veritable valeur de la pesanteur, par S celle trouvée par le pendule
lourd dont la masse soit AZ, par s enfin celle trouvée par le pendule leger, dont la masse soit m:

M + m
Dal a

 

—

ees

SEE

if
i
1
|
|

 

gene nn DT BTE

 
