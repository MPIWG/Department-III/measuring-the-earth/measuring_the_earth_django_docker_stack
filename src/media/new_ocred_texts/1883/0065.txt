 

51

Respighi fragt nach den Methoden, die man bei derartigen Untersuchungen
anwenden könne, da ihm hauptsächlich die genügend genaue Bestimmung der Längen-
differenz schwierig erscheine.

Faye weist auf die analogen Arbeiten in Schottland hin, meint, es sei nur
die Ermittlung der geologischen Verhältnisse mühsam und fügt hinzu, dass es ihm ange-
messen erscheine, bei derartigen Beobachtungen auch die Bestimmung der Pendellänge
am Fusse und am Gipfel des Berges anzuschliessen.

Hirsch bemerkt mit Rücksicht auf die Geschichte der Untersuchungen am
Shehallien, dass eine der grössten Schwierigkeiten in der genügend genauen Berechnung
des Volumens des betreffenden Berges liege; zum Glücke besitzen die Italiener für den
Aetna wenigstens ein sorgfältig gearbeitetes Relief, welches diese Bestimmung erleich-
tern dürfte.

Löwy bemerkt, dass gerade unter solchen Verhältnissen bei genügender Sorgfalt
für die Bestimmung der Längendifferenz eine Genauigkeit von O°O1 bis 0°02 erlangt
werden könne, welche Genauigkeit in keiner Weise der Sicherheit der Polhöhen-
bestimmung nachstehe.

Perrier theilt vollständig die von Löwy geäusserten Ansichten.

In Rücksicht auf die morgen um 10 Uhr stattfindende Sitzung der Commission
für den einheitlichen Meridian und die Weltzeit wird der Antrag Hirsch’s: die nächste
Sitzung der allgemeinen Conferenz auf Donnerstag den 18. Oktober 2 Uhr Nachmittags
zu verlegen, angenommen; der Präsident schliesst die Sitzung um 5 Uhr Nachmittags.

Wee

 
