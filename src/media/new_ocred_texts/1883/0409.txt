    

Triangulation du Paralléle d’Amiens. (Partie Orientale.)

 

RTS Eh

 

 

 

 

 

 

 

 

 

 

 

 

\ | a ’
Pi INDICATION Be DIRECTEURS :
N N° LATITUDE. LONGITUDE.| EPOQUE. INSTRUMENTS. REMARQUES.
ie : DES POINTS. : | ET OBSERVATEURS. 2
. 94 Prémont...... O° 0'53"|.21° 3/25"
me 95) Guise. ....... 49-53 52 |-21 17 16 5 :
D 96 | Laon m ...... 49 33 48 | 21 17 19 FE Ë
F 97 | La Bouteille. . . ..| 49 53 14 | 21 38 16 | 82 2 |
98 | Mainbressy..... 49.43 2) 21 58°51 | oi SEH ae |
99 | Trou-du-Sable. . . | 49 53 23 | 22 6 O| - À | Se 2, |
= 100] Noirtrou...... 49-49 49 | 92 1419 > 5 | C8 a |
= 101 | La Grande-Croix. | 49 56 20 | 222637) 9. | Pas a |
- 102 | Pragnon-de-Pusse- | | en a 1.
h mange... ..- 49 47 49 | 22 31 52 | | ae S |
m 1031 Stonne.....:. 49-09 ee oO
E 104 | St. Valfroy . . . . | 49 34 16 | 22 56.16 | |
Re | { |
F Triangulation du Parallèle d’Amiens. (Partie Occidentale.)
| |
12 | Villers-Breton- }
.. feeeux «. Déjà mentionnés.
=. 14) Vignacourt. . ... | |
me 105 |-Clairy. . . . .... | 49° 51' 25”). 19° 50! 32”) |
m= 106 | Hormoy....... | 49 50 47 1:19 33 45 | | | |
BB: .:.....150 1 38-| 19 95 51 | : | |
- 108 | St.Léger-aux-Bois | 49 50 2 | 19 16 24 | |

109 | Mont-de-l’Aigle. . | 49 56 49 | 1£
110.) Forêt @’Hellet .. | 213 720.19 3 ||
111 | Tourville-la-Cha- | | |
oe | 49 56 38 | 18 55 33 ||
112 | LesGrandes-Ventes | 49 47 11 | 18 53 29
113 | Phare de VAilly . | 49 552.7 1,18.37.20
er st. Laurent... .. | 49 45--9 | 18°32 33 |
115 | Ingouville .. ....-| 49 50 19 | 18 20 58 |

pl
Ne)
co
Oo
bo
1819-20
Commandant Delahaye,
capitaine Peytier.
_ Cercles repetiteurs.

 

 

 

 

 

 

   

Term Pre SL LR TE TIT

Triangulation du Paralléle de Paris. (Partie Orientale.)

  

 

 

 

 

 

 

i
i Panthéon; ...:.-. ) ' |
4 3 | Belle-Assise . . .. Déjà mentionnés. | 3
& 26 | Malvoisine. .... | =
© 116 | Rampilion..... 48° 33' 4”| 20° 43 47” S
we 17 Dons.......: 48 52:9.| 20 49 51 | &
=  118/| Monceaux ..:.. AS AY AA) OG? 1 Ss
= 119 | .Chevandon..... 48 19 99 91.17 9 À
D 120 | Allement . . .... 48 45 40 | 21 97 48 | =
me 121 | Feuges....... 48 23 25 | 21 44-96 | 3
D 122 | Sompuis..-... . . | 48 41 59 | 21 59 25 | as ee H
> 123 | Chassericourt . . . | 48 31 20 | 99 14 2 A as 2
me 194*) Basu... 2... 48 50 16 | 22 19 52 & SE 5
E 125 |.Longeville . . ... BI 2095|). as
=. 196 |. Montiers....... 48 32 23 | 22 53 19 | = oq a
197 | Ménil-la-Horgue . | 48 42 6 3104| & Be 3
Oe to) Grand. ....... 48 22 46 | 23 6 52 & Ss 3
D 129 | Moncel . . .. . .. | 48 95 43 | 23 23 5 = rn ©
D 1301 Bruley....:... 48 42 47 | 23 30 30 | £ =
© 131 | Vaudémont..... 48 94 39 | 93 44 4 A |
er Amanco..,.... 48 45 31 | 93 57 5 5 |
183 | Essay-la-Côte. . . . | 48 95 24 | 24 714| 4
© 134 | Marimont ..... 48 44 9 | 24 93 29 | ä
D 15 | Dönon. .......... 48 30 47 | 24 49 44 =
196 | Bressoirs . . .... | 48 11,24 | 94 48 52 5
137 | Strasbourg... . . 45 35 9 | 25 23 57 =
| |

 

 

 

we SATE NRT ON RET tHe RINT

 
