 

106

efundene Breitenunterschied offenbar unabhängig von der Declination der beobachteten
Sterne sein. Sind überdies die beiden Passagen-Instrumente solid und symmetrisch
construirt und deren Aufstellung durch Miren controllirt, und kommen sie im ersten
Vertical nach der Methode W. Struve’s in Verwendung, so wird man nicht nur die
Fehler der Refraction und den Einfluss der jährlichen Anomalie derselben durch Aus-
dehnung der Beobachtungsreihe auf ein ganzes Jahr vermindern können, sondern auch
unabhängig sein von der Kreistheilung, von den Mikrometerschrauben, von der Biegung des
Fernrohres und schliesslich von der Durchbiegung der Instrumentalachse und der Un-
regelmässigkeit der Zapfen, zumal deren Construction nicht bloss die Umlegung des
Instrumentes, sondern auch jene der Lager gestattet. Betrachtet man eine genügende
Anzahl von Sternen in der Nähe des Zeniths, so kann man die kleinen Breitenunterschiede
mit einem sehr hohen Grade der Genauigkeit ermitteln; diese kann noch dadurch
wesentlich erhöht werden, dass man die dem Beobachter und Instrumente zukommenden
Eigenthümlichkeiten durch Wechsel der Beobachter und Instrumente eliminirt, gerade so
wie dies bei Längenbestimmungen geschieht. Weiter kann sogar, wenn die Eigen-
bewegungen der Sterne genau bekannt sind, ein Beobachter für beide Stationen allein
zur Verwendung kommen, indem er abwechselnd an dem einem und dem anderen Orte beob-
achtet, wenn nur der Zwischenraum zwischen diesen Beobachtungen ein Jahr nicht
überschreitet.

Bei einer guten Organisation des Beobachtungssystems ist es als sicher anzu-
nehmen, dass man den Breitenunterschied mit einer der auf dieselbe Art bestimmten
Aberrationsconstante adäquaten Genauigkeit erhalten wird, das ist bis auf wenige
Hunderttheile der Bogensecunde, eine Genauigkeit, welche wohl jene der absoluten Be-
stimmungen um das Zehnfache überragt. Wiederholt man diese Beobachtungen, so wird
man nach Ablauf von 30 bis 40 Jahren solche Aenderungen mit Sicherheit nachweisen
können, zu deren Erkennen die gewöhnlichen Methoden mehrere Jahrhunderte Zwischen-
zeit bedürften. Natürlich setzt dieser Vorgang voraus, dass die mittleren Verhältnisse
der Beobachtungsstationen in der Zwischenzeit die gleichen geblieben seien, hauptsäch-
lich jene, welche die Refraction in anomaler Weise beeinflussen können. Wenn durch
mehrere solcher Paare von Beobachtungsstationen die Aenderung in den Breitenunter-
schieden nachgewiesen sein wird, so wird die Lösung des vorgelegten Problems über die
Variabilität der Polhöhen nicht die geringste Schwierigkeit bieten.

Dies sind die Grundzüge der von Fergola in Vorschlag gebrachten Operationen;
die weiteren Details können zum Studium jenen Personen überlassen bleiben, welche sich
der Durchführung dieses Projectes unterziehen werden. Es wird sich hierbei auch die
Nothwendigkeit herausstellen, diese Beobachtungen durchaus auf ständigen Observatorien
anzustellen, man kann mehrere Stationspaare auswählen, die fast oder selbst genau auf
demselben Fall in Längen-Intervallen von 3 bis 12 Stunden vertheilt sind. Doch ver-
dient die Betrachtung Fergola’s hierüber hervorgehoben zu werden, dass, da die
Sternwarten dauernde Institutionen sind, man bei Wahl derselben ls Bcohachtnng
station weit mehr Aussicht auf W iederholung der Beobachtungen nach Ablauf grösserer

+

 

 
