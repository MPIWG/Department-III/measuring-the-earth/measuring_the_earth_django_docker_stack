 

4
À
I
|
3
À
{
|
#
i
,
t

co LN OOS

ERROR RELEASE ERNST EEE TEI ES EMRE T ANATOLE BEA NEIL I

|

 

99

über diesen Gegenstand das Wort ergreifen wolle. Da Niemand dasselbe verlangt, geht
der Präsident auf den Programmpunkt III 7, „neue Untersuchungen über die Refraction“,
über und ersucht Herrn v. Bauernfeind seinen diesbezüglichen Bericht der Versammlung
zur Kenntniss zu bringen. Dieser Bericht findet sich als Annex VII den vorliegenden
Verhandlungsprotokollen beigeschlossen. :

Der Präsident eröffnet über denselben die Discussion.

Hartl ergreift das Wort und bemerkt, dass die tägliche Periode der Refraction,
welcher der Bericht des Herrn v. Bauernfeind Erwähnung thut, seit langer Zeit den Geodäten
bekannt sei, dass aber die jährliche Periode nur auf ständigen Sternwarten untersucht
werden könne: doch könne man auch auf nur für kurze Zeit errichteten Observatorien, sogar
auch auf trigonometrischen Stationen, werthvolle Beiträge zum Studium der täglichen
Periode und der meteorologischen Einflüsse auf die terrestrische Refraction erlangen. Er
stellt daher den folgenden Antrag:

Die siebente allgemeine Conferenz spricht den Wunsch aus, dass von den
an der Europäischen Gradmessung betheiligten Staaten möglichst zahlreiche
Untersuchungen über die terrestrische Strahlenbrechung angestellt werden
mögen, um dadurch den Einfluss kennen zu lernen, welchen die localen Ver-
hältnisse (Bodenbeschaffenheit und klimatische Factoren) auf dieses Phänomen
ausüben.

Der Vorgang, welchen Professor v. Bauernfeind bei seinen Beobachtungen
auf der Linie Döbra-Kapellenberg im Fichtelgebirge und neuerdings auf drei
Linien im bayerischen Hochgebirge befolgt hat, ist für derartige Untersuchungen
sehr empfehlenswerth; doch kann auch bei der Vornahme von Triangulirungen,
besonders auf Dreieckspunkten erster Ordnung, sehr Erspriessliches geleistet
werden, wenn der Beobachter im Verlaufe der Beobachtungstage von Zeit zu
Zeit Zenithdistanzen eines oder mehrerer trigonometrischer Signale misst und
dabei Luft-Temperatur, Druck und — wenn thunlich — auch die Feuchtigkeit
bestimmt.

Zur Zeit der stärksten Veränderlichkeit der Refraction, also in den
frühen Morgen- und späteren Abendstunden, wäre es zweckmässig, diese Ze-
nithdistanzen (von denen jede aus etwa zwei Einstellungen bei Kreis rechts
und zwei bei Kreis links zu bestehen hätte) in Intervallen von beiläufig
30 Minuten, in der dem Mittage näher gelegenen Zeit von Stunde zu Stunde,
alle Beobachtungen aber stets an derselben Stelle des Höhenkreises auszuführen.

In analoger Weise könnten auf den zur Bestimmung von Breite und
Azimuth errichteten temporären Observatorien höchst werthvolle Beiträge zum
Studium der täglichen Periode der irdischen Strahlenbrechung, auf den stän-
digen Sternwarten nach dem Vorgange Struve’s (Vergl. Gradmessung in
den Ostseeprovinzen Russlands) für die jährliche Periode gewonnen werden.

v. Bauernfeind unterstützt diesen Antrag Hartl’s mit dem Bemerken, dass die
permanente Commission schon im Jahre 1878 bei ihrer Versammlung in Hamburg ähn-

 
