 

29

Schmidt, M. Triangulirung III. Ordnung im Freiberger Revier. (Jahrbuch für das Berg-
und Hüttenwesen im Königreich Sachsen, J ahrgang 1883. Freiberg.)

21. Schweden.

A. Nachträge und Berichtigungen zur ersten Mittheilung der Literatur d. p.
U. ih. @ A. im G. B.. pro 1880.
Die nachfolgenden drei Werke sind in der Literatur vom Jahr 1880 irrthüm-

lich unter Norwegen aufgeführt, während dieselben unter Schweden gehören.

Agardh, J. M. De methodis, quae ad longitudinem terrestrem per distantias lunares
indagandam sunt adhibitae. Lundae, 1839.

Backlund, A. V. Bestämming of Polhöjden för Lund Observatorium medelst observationer
i första Vertikalen. Lund, 1868. (Lunds. Univ. Ärsskrift.)

Möller, D. M. A. Om Lunds Observatorii longitud. Lund, 1853.

B. Publicationen aus den Jahren 1881—83.

Rosen, P. G. Die astronomisch-geodätischen Arbeiten der topographischen Abtheilung
des Schwedischen Generalstabes. Bd. I, Heft 1. Stockholm, 1882.

22. Schweiz.

Bulletin de la Société Suisse topographique. Genève. Jahrgänge 1880, 81, 82.
Commission géodésique Suisse. Procès-verbaux des séances de la Commission géodé-
sique suisse, tenues à l'observatoire de Neuchâtel, 1881, 1882, 1883.
(Dans les Bulletins de la Société des sciences naturelles de Neuchâtel.)
— Das Schweizerische Dreiecksnetz 1. Band. Winkelmessungen und Stations-
ausgleichungen. Im Auftrage herausgegeben von R. Wolf. Zürich, 1831.
— Nivellement de précision de la Suisse exécuté par la Comm. géod. Suisse sous
la direction de A. Hirsch et E. Plantamour. Huitième livraison. Genève,
Bale et Lyon, 1883.
Hilfiker, J. Die astronomischen Längenbestimmungen mit besonderer Berücksichtigung
der neuen Methoden auf Grundlage der Protokolle der Europäischen Grad-
messung dargestellt. Aarau, 1882.
Hirsch, A. Sur les mouvements du sol, constatés a l’Observatoire de Neuchatel. Extrait
du Bulletin de la Société des sciences naturelles de Neuchâtel. T.
XII, Neuchatel, 1883.
nn De l'influence de la mise au foyer sur la valeur du micromètre d’un micros-
cope. Extrait des Procès-Verbanx du Comité international des poids et
mesures, séances de 1877. Paris, 1878.
Meyer, M. W. Ueber die micro-telephonische Einrichtung der Sternwarte zu Genf.
pie. Bd. 105,.1882, No,2455. 2, 100)
Plantamour, E. Observations limni-métriques. Faites à Genève de 1806 à 1880. Bale, 1881.

 
