 

 

Da a ame

232

On est donc en droit d’esperer que cette correction est exacte a quelques
microns pres.
On a obtenu par une série d’expériences trés concordantes pour la longueur du
pendule simple qui bat la seconde à Paris, à l'Observatoire
4 mo) à 10
M. Peirce (Note à son mémoire de 1882) arrive à la valeur
= tele
Les expériences de Borda et de Biot, discutées par Baily et Saigey, conduisent au
résultat suivant:
1 — 01999908
La discussion des mêmes observations par M. Peirce le conduit à admettre
= 102993915

La concordance de ces quatre nombres paraît assez remarquable pour être signalée.

Calculs.

Les calculs relatifs au triangle de la longitude Paris-Nice-Milan sont très avancés.
La différence de longitude Paris-Nice déjà calculée donne les résultats suivants :

f M. Bassot à Paris

1° Opération | | dem = 19" 515515

M. Perrotin à Nice |

M. Perrotin à Paris

M. Bassot à Nice
Différence 0503

2° Operation | ar 1) 5] 4

Pour la différence de longitude entre Paris et Milan, nos calculs sont aussi
terminés, et dès que nous aurons reçu communication de ceux de notre collègue, M. Celoria,
la longitude pourra être calculée en quelques jours d’une manière définitive.

Les calculs de M. Perrotin, directeur de l'observatoire de Nice, pour la différence
de longitude Nice-Milan sont aussi terminés.

Nous espérons recevoir prochainement les calculs semblables de M. Celoria pour
la station de Milan et fermer ainsi le triangle Paris-Nice-Milan.

Ce n’est pas sans quelque impatience que nous attendons le résultat définitif de
la différence de longitude Paris-Milan; il nous permettra, en effet, en le combinant avec
la difference obtenue entre Vienne et Genève, de découvrir le côté fautif du polygone
des longitudes franco-suisses, où s’est révelée, nous l’avons déjà dit à l'Association, une
erreur inexpliquée de quatre dixiémes de seconde de temps (0°4).

Nos calculs provisoires, relatifs à la station de Guelt-es-Stel, en Algérie, dans le
triangle des longitudes:

 

 

zu

 
