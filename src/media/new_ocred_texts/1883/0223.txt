 

E
Ë
fk
[
E
Ë
|
|
|
i
|
K
i
|
F
Ef
|
F

 

i
i
È
i

209
dans les grands Observatoires, où ces déterminations sont l’objet d’une préoccupation
continuelle et des travaux les plus soignés?

Messieurs, l’Astronomie a fait dans le dernier siècle des progrès si rapides dans
la partie instrumentale et pratique, qu'il n'existe dans aucun observatoire un instrument,
avec lequel on ait déterminé exactement la latitude pendant un intervalle un peu long,
de 50 ans par exemple. Et même lorsque l'instrument est resté le même pendant un
intervalle moindre, les circonstances ont changé souvent; on à refait le bâtiment, élargi
les trappes, changé les cercles où fait des trous dans le cube central, déplacé les
thermométres, etc. Ajoutez les changements dans les méthodes d'observation, dans les
constantes de réduction, les erreurs variables, constatées ou négligées, dans les instru-
ments météorologiques ou dans les tables qui servent au calcul de la réfraction. Enfin les
observateurs, ces instruments vivants, changent aussi par la force inévitable des choses. Il est
évident, d’après cela, que les mesures modernes ne peuvent être comparées aux anciennes,
qu'à l’aide d’une étude toujours plus on moins incertaine et souvent impossible des nom-
breuses causes d'erreur, qui ont toujours rendu si difficile la détermination des latitudes
absolues. Aussi faut il bien avouer, que la diminution indiquée par les latitudes récentes
de plusieurs Observatoires, en comparaison avec les latitudes analogues déterminées au
commencement de ce siècle, ne donnent pas d'indices bien concluants. Mr. l’Astronome
Royal d'Angleterre nous à fait remarquer, que la diminution de la latitude de Greenwich,
accusée par la comparaison des observations actuelles avec celles de Bradley suivant le
calcul de Bessel, n’est point confirmée par la discussion des observations des derniers
40 ans, publiée dans le Tome XLV des Mémoires de l’Astronomical Society. La
seule série qui témoigne avec quelque poids en faveur d’une variation sensible des latitudes,
est celle des latitudes observées à Poulkova pendant à peu prés 25 ans, au grand cercle
vertical d’Ertel: série dont Vhomogénéité ne paraît rien laisser à désirer, à l'exception de
l'identité des observateurs, qui manque ici comme partout ailleurs. En réduisant par une
méthode uniforme toute cette série, M. Nyrèn a trouvé des discordances, qu’il ne saurait
autrement expliquer, que par une diminution progressive et uniforme de la latitude,
d'environ une seconde par siècle. Le résultat, qui est digne d’une grande attention,
pourrait étre vrai, sans étre en contradiction avec la constance de la latitude de Green-
wich qui résulte des recherches de l’Astronome Royal. Car il est évident que les effets
d’un mouvement du pôle ne sont pas les mêmes sous tous les méridiens terrestres; et la
différence de longitude entre Greenwich et Poulkova est d'environ 2 heures.

Le projet de M. Fergola a pour objet principal d'éliminer toutes ces nombreuses
incertitudes et difficultés qui s’attachent aux latitudes absolues, en appliquant ici le même
principe qui a été si utile dans les observations du pendule et dans une foule d’autres
recherches; c'est à dire, en réduisant le question à des déterminations relatives, et à la
mesure facile et exacte de petites différences. Dans ce but, M. F'ergola choisit plusieurs
couples d’observatoires placés, à peu de minutes près, sous le même parallèle, mais très
éloignés en longitude, par exemple Rome et Chicago, dont la différence en longitude est

. . . ° LCA % ya / 1 À ave {7
de 6" 40", tandis qu’en latitude ils different seulement d’environ 4 minntes d’arc. Si
27

 
