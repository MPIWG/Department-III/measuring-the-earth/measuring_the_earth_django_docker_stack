 

35

doch der entschiedene Sieg des Meter-Systems weit mehr den ihm innewohnenden,
zahlreichen Vorzügen zugeschrieben werden zu müssen, vor Allem der systematischen
Durchführung der Decimal-Theilung, ferner der einfachen Relation, welche dasselbe
zwischen der Längen-Einheit und den Oberflächen-, Raum- und Gewichts-Einheiten her-
gestellt hat, und hauptsächlich der äusserst gelungenen Anpassung der gewählten Ein-
heiten an die verschiedenen praktischen Bedürfnisse. So entspricht das Meter vortrefflich
dem Gebrauche des täglichen Lebens als Maass für den Handel, für die Ingenieure, für
die Bauten und die Vermessungen des Bodens; das Centimeter entspricht den Bedürf-
nissen für Techniker und Handwerker, das Kilogramm dem Kleinhandel, das Milli-
meter und Gramm endlich den Bedürfnissen der Präcisions-Messungen.

Andererseits waren die alten Maass- und Gewichts-Systeme weder wissenschaft-
lich noch auf rationelle Weise organisirt und ausserhalb des Landes, dem sie
ihre Entstehung verdankten, zu keinem Einflusse gelangt. Das Gleiche lässt sich keines-
wegs von den hauptsächlichsten Längen-Systemen behaupten, von denen eines heute
schon mehr als die Hälfte der Erde sich erobert hat. Sollte man unter solchen Um-
ständen ernstlich, und zwar nur um gewisse unberechtigte National-Empfindlichkeiten
zu schonen, auf eine jener glücklichen im Ocean verlorenen Inseln zurückkommen
und daselbst auf gemeinschaftliche Kosten ein Observatorium herstellen wollen, nur in
der alleinigen Absicht, einen Ausgangs-Meridian zu schaffen, welcher weder Englisch,
noch Französisch, weder Deutsch noch Amerikanisch wäre? —

Auch das zweite Argument, das von den Anhängern eines oceanischen Meridians
geltend gemacht wird, hält bei näherer Prüfung eben so wenig Stich. Im Interesse der
Geographie, meint man, sei es unzulässig, dass der Ausgangs-Meridian den Continent
durchschneide, und somit Länder in zwei Hälften zerschneide, in denen man dem Uebel-
stande der östlichen und westlichen, positiven und negativen Längen als einer Quelle
steter Irrungen ausgesetzt sein würde. — Was zunächst den Uebelstand der Theilung
eines Landes in östliche und westliche Längen anbetrifft, so würde derselbe sofort ver-
schwinden, wenn man sich, wie wir es vorschlagen, entschliesst, die Längen nicht nach
Westen und nach Osten herum bis zu 180°, sondern lediglich in der östlichen Richtung
von 0° bis zu 360° zu rechnen. Und wollte man selbst auch in Zukunft nach dem
alten System die Längen nach beiden Richtungen hin rechnen, so wären die möglicher-
weise daraus entstehenden Missstände und Irrthümer bei einem continentalen Ausgangs-
Meridian weit weniger zu fürchten für die Ingenieure und Geographen, die noch immer
rechtzeitig darauf aufmerksam werden, als bei einem oceanischen Ausgangs -Meridian
für die Berechnung der Längen auf dem Meere, wo ein Irrthum im Zeichen der Länge
in gewissen Fällen die ernstesten Folgen haben kann. Bei einem oceanischen Meridiane
würden sich auch noch andere ziemlich bedeutende Uebelstände herausstellen, welche
bei der Frage über den Datums-Wechsel zur Sprache kommen sollen. Dies zweite
Argument lässt sich somit eher gegen als für die Wahl eines oceanischen Ausgangs-
Meridians ins Feld führen.

Nach allem bisher Gesagten und unter den gegebenen Verhältnissen scheint uns

5*

 
