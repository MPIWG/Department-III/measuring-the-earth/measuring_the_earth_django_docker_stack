 

40

rapporteur est obligé de faire appel à l’indulgence, s’il lui est arrivé de passer sous silence
quelque travail important; pourvu qu’il en soit informé, il s’empressera d’en parler en
temps et lieu. Sous ce rapport il faut faire remarquer immédiatement qu'il n’a pas été
possible de tenir compte des travaux de MM. Pisati et Pucci (Sulla lunghezza del pendulo à
secondi Reale, academia dei Lincei Anno CCLXXX) dont le rapporteur n’a eu connaissance
qu’au dernier moment, ainsi que du „Report of a conference on gravity determinations,
held at Washington. D. C. in Mai 1882“ que M. le Général Cutts lui a communiqué
pendant la session.

Tl sera convenable d’indiquer ici la précieuse liste bibliographique concernant la
détermination de la pesanteur que M. le Major Herschel à publiée dans le 5e volume des
„Aceounts of the operations of the great trigonometrical Survey of India by S. 7. Walker“
dans le 5™ appendice. La littérature qui se rappporte à cette question s’y trouve
classée avec beaucoup de soin, et elle comprend méme des ouvrages qui ne se rapportent
pas directement à la détermination de la pesanteur; mais on est surpris de ne pas y
trouver l'ouvrage important de Bohmenberger „Astronomie, Tübingen 1811“ qui contient
(pag. 448) une description du pendule à réversion, immaginé par lui, et qui lui assure la
priorité de l'invention de cet appareil ingénieux. Je veux mentionner ici deux publications
importantes qui ne figurent pas non plus dans cette liste quoiqu'ils se rapportent à
la période embrassée par M. Herschel: „Govi G., Metodo per determinare la lunghezza del
pendolo, Academia delle scienze di Torino 1866“ et „Unferdinger, Das Pendel als geodä-
tisches Instrument, Archiv der Mathematik und Physik, Greifswalde und Leipzig, XLIX,
1869, pag. 309.4 M. Herschel cite lui méme le premier de ces deux ouvrages dans son
„Memorandum on Pendulum Research as an Aid to Geodesy.“ Dans le present rapport
on a partagé les matières en deux parties, dont la première traite des determinations ab-
solues, la deuxième des détérminations relatives; mais on trouve dans la première partie
des indications très importantes et concernant la deuxième catégorie d'observations.

i. Partie.

Determinations absolues de la pesanteur.

Les déterminations absolues de la pesanteur reposent généralement sur la
combinaison de deux opérations essentiellement différentes, la mesure du temps et la
mesure de la longueur. Ces opérations sont utilisées presque exclusivement pour des obser-
vations de pendule, dont les resultats sont d’une précision incomparablement plus grande
que celle qu’on peut atteindre par les autres méthodes proposées pour la détermination de
la pesanteur. Nous ne nous occuperons donc dans ce chapitre que des observations
de pendule.

Les méthodes pour la détermination du temps d’une oscillation et des dimensions
du pendule sont susceptibles d’un degré de précision tel qu’il ne laisse presque rien à

 

|
|
|
|

 

 
