 

(ar rem pri

Te

TREE TIR

F
|
E

 

PROCES VERBAUX DE LA COMMISSION PERMANENTE.

PREMIERE SEANCE
de la Commission permanente.

Rome, (Salone dei conservatori) le 14 Octobre 1883.

Présidence de M. le G* Ibanez.

MM. Hirsch et v. Oppolzer remplissent les fonctions de Secrétaires.

Sont présents en outre: les Membres MM. v. Bauernfeind, Faye, v. Forsch.

M. le Président ouvre la séance à 2'/b, en souhaitant la bienvenue aux collègues,
et donne la parole aux Secrétaires pour la lecture du projet de Rapport de la Commission
permanente pour l’année 1853.

Ce rapport, auquel sont jointes les deux lettres d’excuse de MM. Baeyer et Nagel,
est approuvé à l'unanimité par la Commission qui décide qu'il sera lu dans la premiere
séance de la Conférence générale dans les deux langues.

M. le Général Baeyer en regrettant d’étre empêché par la faiblesse de sa santé
de venir assister à la Conférence, charge M. v. Oppolzer de le remplacer dans la Com-
mission. Dans la question du 1° Méridien, M. Baeyer se prononce pour le choix de celui
de Greenwich. Enfin il annonce que le Rapport du Bureau Central sera présenté par
M. le Prof. Fischer, chef de section de ce bureau.

En réponse à l’observation faite par un membre, qu'il aurait été désirable que
ce Rapport du Bureau Central fût communiqué d'avance par M. Fischer au bureau de la
Commission permanente, M. le Président promet qu’il tachera de se le procurer encore à
temps pour pouvoir le faire traduire pour la première séance de la Conférence.

M. Nagel qui se trouve, par une maladie des yeux, empêché pour la première fois,
d'assister à la Conférence générale, déclare que le Gouvernement de Saxe l’a autorisé

 
