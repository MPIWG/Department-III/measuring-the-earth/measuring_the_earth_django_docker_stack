7
Bee ee ee eee eee rene mere pare

8 Kinigtiy ©
pt gedruckt |

É-

Fra à

puis |

ar 2

  

(AN ii ;

er al o 1

I
Ê
\ |
Ë
Ê
[
E
F
&
{
F
|

 

Auwers, A. Mittlere Oerter von 83 südlichen Sternen für 1875.0 zur Fortsetzung
des Fundamental-Cataloges für die Zonenbeobachtungen der astronomischen
Gesellschaft, nebst Untersuchungen über die Relationen zwischen einigen
neueren Sternkatalogen, insbesondere für den in Europa sichtbaren Theil des
südlichen Himmels, (Publication d. astr. Gesellsch. Bd. XVII.) Leipzig, 1883.

Becker, C. Bestimmung des Zeitunterschiedes zwischen dem Meridian von Berlin und
dem Meridian von Greenwich und von Wien im Anschluss an eine gleich-
zeitige Bestimmung des Zeitunterschiedes zwischen Wien und Greenwich
unter Leitung der Professoren Dr. Th. v. Oppolzer und Dr. W. Förster.
Berlin, 1881.

Börsch, 0. Anleitung zur Berechnung geodätischer Coordinaten. 2. Aufl. Kassel, 1884.

Braun, C. Ueber eine Anwendung von Libellen zur Bestimmung der Theilunesfehler
eines Kreises. (A. N. 1882. No. 2448.)

Bruns, H. Bemerkung über die geodätische Linie. (Z. f. V. 1881, p. 298.)

— Ueber eine neue Form des Horizont-Collimators. (A. N. 1882, No. 2459.)

Centralbureau der Europäischen Gradmessung.

1880. Generalbericht für 1880. Verhandlungen der vom 13. bis 16. September
1850 in München abgehaltenen 6. Allgemeinen Conferenz der E. G. und der
am 12., 15. und 17. Sept. 1880 gleichfalls in München abgehaltenen perma-
nenten Commission, nebst einem Schreiben an dieselbe von Herrn Prof. Förster
und einer Erwiderung von Herrn General-Lieutenant Baeyer; redigirt von
den Schriftführern ©. Bruhns und A. Hirsch. Zugleich mit dem Generalberichte
für das Jahr 1880 (Deutsch und Französisch). Mit 9 Anhängen:unà 3 litho-
graphirten Karten. Berlin, 1881.

Anhang I. Note sur la possibilité de calculer a priori le poids et
la précision des résultats d’une triangulation par la simple connaissance de
son canevas. Par M. A. Ferrero.

Anhang II. Rapport sur la 7™° question du programme de la sixième
conférence générale réunie à Munich en 1880.

Tableau résumant par ordre alphabétique l’état actuel des travaux entre-
pris dans les différents pays pour la détermination de la pesanteur a l’aide
du pendule à réversion. Par E. Plantamour et C. Cellérier.

Anhang III. Bericht über astronomische Ortsbestimmungen, Längen-,
Breiten- und Azimuthbestimmungen von Dr. C. Bruhns. (Nebst 2 Karten.)

Anhang IV. Ueber Refractionsbeobachtungen von Carl von Bauernfeind.

Anhang V. Rapport spécial sur les triangulations présenté par le
délégué Colonel A. Ferrero à la conférence de Munich. Avec Planche I, un
canevas général des triangulations a l’échelle de 1 : 10000000.

Anhang VI. Rapport sur les mesures des bases géodésiques par M.

Ey. Perrier.
Anhang VII. Rapport sur l'état actuel des travaux de nivellement

 
