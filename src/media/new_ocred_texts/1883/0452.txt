en —

Russie.

DICATION | ; DIRECTEURS | : : |
N° ne LATITUDE. |LONGITUDE.| TPOQUE. INSTRUMENTS. REMARQUES.
DES POINTS. | ET OBSERVATEURS.

 

 

Triangulation de Bessarabie.

© D 1 © OUR C2 DO

 

35 | bliésd L':
Staro-Nekrassowka | 45° 20' 2) 46° 35’ 37" | ‚Publiesdans eL’are

Ismaël

Barska
Saphianowka .
Kairaklia
Katlabuch-Suchoi.
Taschbunar UI ..
Katlabuch
Karakurt
Pandanlia.....

Paraklia
Kamboli
Banrtschi
Malojaroslawetz-
kaja
Kulmskaja
Baschkalia
Nesselrode
Nikolaiewska .
Mowo-Kanschani .
Dschamana
Reseni
Ssurutscheni. . .

Peressetschino. . .
Ziganeschti .

- Bologan
Sagaikani
Rospopeni
Tsehutuleschti. . .
Unkiteschti .. .

Gwosdantzi. .
Britschani . ..

 

45 20 24
45 22 20
45 24 36
45 28 47
45 29 37
45 33 59
45 36 47
45 37 56
45 47 0
45 47 42
45 55 2
45 58 43
46 7 6

46 5 95
46 15 47
46 19.32
46 27 55
46 20 40
46 37 21
46 47 27
46 43 1
A AY
47 1%
11.
47 15 34
47 19 8
47 27 41
47 99 40
47 43 33
47 45 19
47 53 47
47 50 6
47 59 26
47 59 17
48° 7 20
48 10 0

48.19 5,

48 14 30
48 22 26
48 25 48
48 20 42

-A4 59. 5

 

46 28 40
46 25 33
46 32 25
46 26 32

46 34 49 |

46 30 20
46 35 53
46 23 33
46 34 45
46 18 59
46 25 29
46.11 8

22 eal

38 33
41 13
28 29
46 32
4 52 |

7 35 |

52 32
32 4

1929|
44 12 |

18 41
27 23 |

12 48 |

29 58
11.15
18 14 |
59 25 |
9 40 |
36 49 |
52 42
29 17
44 11
18 22
32 31

45 10 46
44 58 43 |
44 47 10

 

 

 

 

 

Lieut.-Général de Tenner.

Instrument Universel d’Ertel de 13 p.
Theodolite terrestre d’Ertel de 12 p.

 

 

 

du Meridien entre

|| le Danube et la mer
|| Glaciale» par J. G.
|| W. Struve. St. Pé- .
|| tersbourg, 1860.

Triangulation de Volhinie et de Podolie.

Gwosdantzi et | || Publiés dans eL'arc
\du Meridien» de

Britschani. . .. Déjà mentionnés. | Struve
Woltschenetz . : : | 48028 9] 44° 351 0" |

48 44 99 | 44 47 G |
Ssupruhkowzi...| 48 45 3 | 44 27 47 ||
Karatschkowzi . . | 48 53 47 | 44 12 46 |
| Hanowka 48: 55 43 | 44 31 98 |
Tschernowody...| 49 8 3 | 44 17 55)
Baranowka AO 855 | 4438.25 |
Jeltschin . . .... | 49 19 46 | 44 20 50 |
Kriwotschinzy. . . | 49 29 4 | 44 898 |

de
>
epet.

]

|
Il
||

t. de10p.

z

énéra
titeur
n de 14,3

péti
épé

de Tenner.
ét

Lieut.-G
Théod.astron.àr

d’Hrtel de 8 p.
Inst. Univ. d’Ertel de

Troughto
13 p.
Théod. ar

Cercle r

 

 

 

 

 
