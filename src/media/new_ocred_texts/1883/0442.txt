     
  
  
  
   
  
 
   
  
   
 
  
 
 
  
  
  
  
    
  
   
 
  
 
 
 
   
 
 
   
 
 
   

TEEN

   

2

Vereinigung der Preussischen und Russischen Ketten, C. Anschluss an das Schlesische Basisnetz.

 

 

 

 

 

 

 

 

 

 

 

mem ee ee ee
| »
INDICATION 5 DIRECTEURS | N
N° LATITUDE. |LONGITUDE.| ÉPOQUE. | INSTRUMENTS. | REMARQUES.
DES POINTS. | ET OBSERVATEURS. |
ce se ees | ER I a
105 | Leupusch...... 50° 43' 44") 34°59! 30"|| 1854 Baeyer, Stiehle. File et Martins |
| 6 lop; |
106 | Ziegenberg. . . . . 50 45 23 | 34 44 36 | 1854 id. id. |
106a Ruppersdorf. . 50.46 41 | 34 49 12 | 1854 v. Hesse, Stiehle. u. et Martins |
de Sp. I
107 | Küchendorf . . . . | 50 47 27 | 34 53 29 | 1854 id. Cercle d'Ertel de |
15 p |
107a| Obereck . . . . .. 50 44 0.| 34 55 40 1854 Baeyer, Stiehle. ae Univ. de Pi- |
stor et Martins |
de 13p. |
107b| Eisenberg . . . .. 50 44 37 | 34 50 44 | 1854 v. Hesse, Stiehle. Cercle d’Ertel. de |
13 p. |
107c| Base, Terme Ouest | 50 46 35 | 34 51 28 1854 ad: id. |
107d| Base, Terme Est. | 50 45 55 | 34 52 56 | 1854 id. id. |
Anschluss des Punktes Rosenthal an das Schlesische Netz.
108 | Breslau. . . . ... 52 6.561 31241159. 1862 ar er, Sadebeck, Habel- | Instr. Univ. de Pi- || Noch "nicht ver,
| De Martina || veffentlicht.
| mann. stor et Martins |
| | de 13 p. |
108a| Rosenthal ..... 51 8 12 | 34 41 59 | 1865 Baeyer, Sadebeck. Pistor de 8 p. |
108b| Hundsfeld ..... 51 8 50 | 34 46 37 || 1865 | id. id. |
108c| Wüstendorf . . ..| 51 5 43 | 34 51 32 | 1865 | id. id: |

 

ne

Verbindung zwischen Schlesien und Sachsen.

 

 

100 | Schneeberg. . . .. À Déjà Se a | id.
en. aS éja mentionnés précédemment. |
109 | Schneekoppe. . . . | 50°44'21"| 33°24'21"| 1863 Baeyer, Sadebeck, Habel- | Instr. Univ. de Pi- | . 1, Autriche-
| | | tor et Martins | Hongrie
| mann. stor e artins | rie.
110 | Gröditzber 51.10 44 | 33 35 30 | Re
Be 1:88 2550: +: /1868 Habelmann. id. N
111 | Wolfsberg ..... 31.149 | 32 52 37 | 1863 id. id. |
119 | Jauernik . . . ... 51 5 50 | 32 33 48 | 1863-64 id. id. | N. 36, Saxe.
113 | Tschelentnig. . . . | 51 17 41 | 34 52 16| 1854 | v. Wrangel. id. |
114 | Todtenberg .... | 51 31 52 | 9416 50 | Habelmann. id.

 

 

 

 

 

Verbindung von Helgoland mit den Oldenburgischen Dreiecken.

 

      
     
       
   

D Duel 53293 57"| 25047 53"| 1866 | Tietjen. | Instr. Univ. de Pi- || Siche: Generalbe- =
| | | DT nat “ra. a

AB | dewery... ... :.. 53 34 26 | 25 33 52 | 1867 id. "id. Poe L
117 | Langwarden. . . . | 53 36 20 | 25 58 11. 1867 id. id. Die so E
118 | Bremerbake . . … | 53 42 44 | 95 56 42 | . 1856 | Bertram. | Cercle de Gambey. lo am EL
119 | Wangeroog. . 53 47 22 | 25.33 45 | 1857 Baeyer, Bertram. | | Instr. Univ. de Pi- | rechnet. LE i
| | stor et Martins ne he

160 | 8 | : | de 8p. | nn
euwerk . ..... 53 54 54 [96 933 | 1857 a | id .

121 | Helgoland . . . . | 54 10 48 | 95 30 44| 1857 | id. | 0:
122 | Dangast . ... | 53 27 6 | 25 4798] 1866 | Tietjen. | Pistor de 13 p. =
| | ¢

 

 

 

 

 

  

Triangulation durch Mecklenburg.

123 | Eichstidt 52° 41' 24" | 30° 44! 55" | i
Ba ee tS a a | Siehe: G albe-
124 | Gützerberg. . ... | 52 26 30 | 30 23 36 era = del5p. || jcht-äher die Mit
125 Stöllnerberg. 59 44 49 | 30 3 5 | || tel Europaeische
| id. | Gradmessung für

    

| || das Jahr 1863,
