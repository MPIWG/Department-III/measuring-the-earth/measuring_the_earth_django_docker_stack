 

 

 

 

 

 

 

 

 

 

 

 

212

a la fois directement et par réflexion dans un bain de mercure, est préférable; par cette
méthode on s’affranchit de toutes les erreurs qui compromettent les résultats des observations
ordinaires des distances zénithales, telles qu’on les pratique ordinairement dans les obser-
vatoires; car on échappe ainsi aux erreurs de division des cercles, à l'incertitude du nadir,
des déclinaisons et de la réfraction. Aussi les résultats qu’il à obtenus, pour la latitude
de son observatoire, par différents couples d'étoiles zénithales observées directement et par
réflexion, ne différent que de quelques centièmes de secondes, tandisque, avec la méthode
ordinaire, il existe une incertitude de plus d’une seconde. La méthode du 1° vertical,
tant prônée par les astronomes Russes et Allemands, n’est pas appréciée au même point
par les Anglais.

M. Schiaparelli croit que cette question de la valeur relative des deux méthodes
ne peut pas se résoudre ici par des arguments, mais seulement par la comparaison des
résultats bien établis, obtenus par elles. Or, W. Struve a déterminé la constante de
l’aberration, par la méthode du 1° vertical, avec une très haute précision qui ne saurait
être mise en doute. Les observations méridiennes par contre, même de premier ordre.
comme celles de Greenwich, montrent toujours des différences de 1” et plus, entre les
résultats des différentes années.

M. Faye envisage que les deux méthodes mentionnées sont bonnes; celle des passages
au 1° vertical pourrait même être perfectionnée, en éliminant l’équation personnelle par
Vemploi de la photographie.

Quant au fond de la question, M. Faye croit l’idée de M. Fergola heureuse, bien
qu'il doute qu’on trouve ainsi des variations du pôle appréciables; si l’on remonte aux
observations des solstices par les anciens, p. ex. à celles d'Eratosthène à Syéne et à
Alexandrie, qui étaient exactes à quelques minutes près, et qu’on les compare aux
modernes, on arrive à une variation au dessous de '/;” par an. D’un autre côté, M. Faye
croit pouvoir même invoquer la constance des zones de végétation autour du pôle comme
argument pour son invariabilité. Mais tout en étant convaincu de celle-ci, il approuve
les recherches proposées par M. Fergola et recommandées par la Commission.

M. Respighi ne voudrait nullement exclure la méthode du 1* vertical: mais il
croit qu’on devrait laisser les observatoires auxquels on s’adressera pour l'exécution de ces
recherches, libres d'employer les moyens qui leur semblent les plus propres pour arriver aux
meilleurs latitudes absolues. Il aimerait en même temps qu’on leur recommandât de
rechercher si, dans leurs anciennes observations, il ne s’en trouve pas qui puissent être utilisées.

M. Ferrero estime que ce serait rejeter les propositions de la commission qui
réposent essentiellement sur l'identité des moyens employés dans les observatoires de
chaque couple, si l’on voulait combiner les résultats de différentes méthodes et de
différents instruments.

M. Loewy approuve le principe des propositions de la Commission, mais il croit qu'on
pourrait donner satisfaction aussi à celle de M. Respighi. M. Loewy envisage la méthode des
observations méridiennes comme la meilleure pour la détermination des latitudes, pourvu
qu’on sache se préserver de certaines sources d’erreur, parmi lesquelles il croit qu’une

 

|
&

 
