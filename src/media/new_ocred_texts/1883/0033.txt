 

19

Als dritter Brief gelangt ein Schreiben des Superintendenten der nordamerika-
nischen Coast and Geodetic Survey, Herrn G. J. E. Hilgard, an den Präsidenten der per-
manenten Commission zur Verlesung, in welchem er die Ernennung des Generals Richard
Cutts zum Vertreter dieses Institutes anzeigt; derselbe lautet dem englischen Originale
entsprechend übersetzt:

U. S. Coast and Geodetic Survey Office.
Washington, 28. September 1883.

Herr Präsident!

Ich habe das grosse Vergnügen durch diese Zeilen bei Ihnen meinen
Collegen, General Richard D. Cutts, den ersten Assistenten an der Coast
and Geodetic Survey der Vereinigten Staaten einzuführen, welchen ich
im Auftrag des Schatzmeistersekretariats als Delegirten für die allgemeine in
Rom stattfindende Conferenz zu bezeichnen die Ehre habe.

General Cutts war in den letzten 40 Jahren ein Mitglied der Coast Survey
und ist dirigirender Officier dieses Bureaus; er ist auf das beste mit allen
wissenschaftlichen und praktischen Methoden vertraut.

Da Ihr Einberufungsschreiben darauf hinweist, dass die Fragen be-
züglich der Wahl eines Ausgangsmeridians und einer Weltzeit in der Conferenz
besondere Berücksichtigung von Seiten des wissenschaftlichen Standpunktes er-
fahren wird, so ist General Outts beauftragt, die allgemeinen Gesichtspunkte,
welche über diesen Gegenstand in den Vereinigten Staaten Platz gegriffen
haben, zum Ausdrucke zu bringen und des Vortheils theilhaftig zu werden,
welcher aus der Diskussion mit Rücksicht auf jene Gesichtspunkte entsteht.

Ich bin, Herr Präsident, überzeugt, dass die allgemeine Gradmessungs-
Conferenz sich zu Gunsten einer diplomatischen Conferenz rücksichtlich dieser
Fragen entscheiden werde, in welcher Beziehung die Regierung der Ver-
einigten Staaten die einleitenden Schritte gethan hat.

Ich habe die Ehre mich als Ihr ergebenster Diener zu zeichnen.

J. E. Hilgard,
Superintendent der U. S. Coast and Geodetic Survey.

Schliesslich wird das Schreiben des Herrn Majors Robert v. Sterneck zur Kenntniss
der Versammlung gebracht, in welchem er seine Abwesenheit entschuldigt und Herrn
v. Oppolzer ersucht, ihn bei den Verhandlungen zu vertreten.

Der Präsident ertheilt hierauf Herrn v. Oppolzer das Wort, um den folgenden
Bericht des Centralbureaus der Versammlung zur Kenntniss zu bringen.

3*

 
