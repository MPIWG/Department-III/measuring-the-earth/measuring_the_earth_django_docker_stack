 

N

 

55

 

 

Ry, :
| MÉTHODE ET INSTRUMENT

EMPLOYÉS.

ANNÉE
ET
MOIS.

TITRE DE LA PUBLICATION.

REMARQUES.

 

Différences d'azimut entre
quelques étoiles circum-|
polaires et le signal. |

Détermination de l’azimut|
d'une mire méridienne. |
Altazimut d’Ertel |

do.
do. |

Le premier résultat fût ob- |

tenu par la détermina-|

ANN

tion de la difference d’azi- |
mut de la polaire et du|
d'un |
second |
résultat fût obtenu par |

signal au
Altazimut.

moyen
Le

la difference d’azimut du,
signal et d’une mire mé-|
ridienne dont lazimut|
absolu füt determine au
moyen d’un instrument
des passages
Mémes méthodes que 6.
Instrument universel de
Pistor et Martins et in-|
strument des passages de
Repsold
do.

ita, Ani

Mémes méthodes et mémes |
instruments que 8

Vita

Mémes méthodes que 6.
. Instrument des passages de
mee Cook. -
… Instrument universel
Pistor et Martins
do.
do.
do.

de

<.

ast! ip strument des passages de

ih Bamberg. Instrument uni-

i À versel de Pistor et Mar-
D tins

~~ Instrument des passages de

Mêmes méthodes que 6. In-

 

 

Repsold; mire méridienne

wa

vom

1

 

1874, Août, Sept.

do.
do.

1874, Septembre

| 1874, Octobre

1875, Mai, Juin

1875, Nov., Dec.

1878, Sept.

1880, Mai

1882

1876, Aoüt
1880, Septembre
1883, Mai

1882, Juillet

Lorenzoni.
dine e di en azimuth sul’estremo
Nord-Ovest della Base di Lecce.

 

' Pubblieazioni dell’Instituto militare. Par-
te IIa astronomica, fascicolo I°.

Pubblicazioni dell’Instituto militare par-
te LIa astronomica, fascicolo IT°.

do.

Pas encore publié.
do.
do.
do.

 

Determinazione della latitu- |

 

 

 

 

Résultat obtenu par l'instrument des
passages 302’ 22’ 50:69 Æ 0747
au moyen de la polaire dans sa plus
grande digression

802! 22/:46:63:=2E, 0117

Résultat obtenu par l'instrument des
passages 271° 9° 15:03 ÆE 0142,
au moyen de la Polaire dans sa plus
grande digression

271°-.9: 10.85 2 vo

Résultat obtenu par l’instrument des
passages 50° 93° 5587 E 067,
au moyen de la Polaire dans sa plus
grande digression

50° 23" 66°37 a= Og,
Résultat obtenu par l'instrument uni-
versel
156° 50’ 59"72 = 0730,
par instrument des passages
156° 51° 2°96.
