 

 

125

 

NIVELLEMENTS DE PRECISION

C'est encore dans la partie Ouest du territoire espagnol, déjà visée par mon dernier
Rapport, que les travaux de nivellement ont eu lieu, suivant quatre grandes lignes, savoir :
de Lugo à la Coruña, en passant par Betanzos ; de Zamora à la Coruña, par Orense, Ponte-
vedra et Santiago; de Ponferrada à Orense, par Puebla de Trives; enfin d’Avila a la Fre-
geneda, par Salamanca et Vitigudino.

Tous les travaux d’observation et de calcul ont été dirigés par M. le colonel Cabello

et par M. le capitaine Mier, lesquels ont eu sous leurs ordres dix observateurs, qui ont opéré
chacun avec un niveau.

La première de ces lignes, nivelée en double et en sens inverse, comme toutes celles

que nous avons terminées, a une longueur de 95 kilomètres et comprend 28 repères en
bronze.

La seconde, qui comprend 162 repères en bronze, a une longueur de 559 kilo-
metres.

Le troisième, sur 167 kilomètres, comprend 38 repères en bronze.

La quatrième, avec le même nombre de repères en bronze, a une longueur de 295
kilomètres.

Ces quatre lignes ont fourni les altitudes de neuf chefs-lieux de provinces ou villes
importantes.

En outre, le Gouvernement à décidé, sur la proposition de l’Institut géographique
et statistique, de faire exécuter les petits embranchements nécessaires pour passer des lignes
des nivellements de précision aux stations de chemin de fer les plus rapprochées, afin d’y
placer des planches métalliques indiquant altitude au-dessus du niveau moyen de la Médi-
terranée a Alicante. Ce travail a été fait pour 38 stations du réseau des chemins de fer
du midi, et n’a exigé qu’un nivellement de 37 kilometres pour atteindre ce but.

Il est à re marquer que, l’année 1885, la campagne des nivellements n’a pas pu avoir
lieu, à cause de lépidémie cholérique. Le résultat de celle de 1884 a done été le nivelle-
ment double de 1083 kilomètres et la pose de 304 repères en bronze.

Le nivellement de précision de l'Espagne, sans compter ce qui a été fait dans la
campagne de cet automne, qui n’est pas encore terminée, a un développement de 8813 kilo-
mêtres et 1889 repères en bronze. Ce réseau hypsométrique fait la jonction entre les trois
maréographes d’Alicante, Santander et Cädiz, entre soixante chefs-lieux de provinces et villes
importantes, un plus grand nombre d’autres villes et villages, et il offre trois points de rat-
tachement avec les nivellements de la France et trois autres avec ceux du Portugal.

ee

 

 
