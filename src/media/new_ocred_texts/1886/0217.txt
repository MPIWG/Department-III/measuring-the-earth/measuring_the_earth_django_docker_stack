|
|

|

 

 

liger Weise machte sich letzteres zwischen Hamburg und Cuxhaven bemerkbar, und es darf
wohl als ziemlich sicher angesehen werden, dass hier vornehmlich der überaus elastische
Moorboden, welcher es häufig bei grösster Vorsicht nicht gestattete, dem Stative für die
Dauer der Beobachtung eine absolut unveränderliche Aufstellung zu geben, die Ursache
jener konstant und einseitig wirkenden Fehlerquelle gewesen ist. Man darf aber hoffen, dass
im Mittel der entgegengesetzt geführten Nivellements diese Fehlerquelle in der Hauptsache
verschwindet.

Eine Revision der selbstregistrirenden Pegel zu Swinemünde und Travemünde sowie
der Skalenpegel zu Wismar, Warnemünde, Stralsund und Wieck hat im Anschlusse an die
für sie in ihrer Nähe eigens etablirten Normalfestpunkte wiederholt stattgefunden.

Nächst diesen im Programm der Gradmessung liegenden Arbeiten gelangte noch
eine grosse Nivellementslinie zur Erledigung, zu welcher die Anregung durch ein technisches
Interesse gegeben wurde. Der Herr Oberpräsident der Provinz Sachsen wandte sich im Früh-
jahr 1885 unter Zustimmung des Herrn Ministers für die öffentlichen Arbeiten an das Institut
mit dem Ersuchen, das in den Jahren 1876 und 1877 vom Institute im Wesentlichen auf
dem rechten Ufer der Elbe geführte Nivellement, das sich inzwischen bei den hydrometri-
schen Arbeiten der Königlichen Elbstrombauverwaltung als Grundlage bewährt hatte, jetzt
auch in gleicher Weise über das linke Elbufer auszudehnen. Das Geodätische Institut konnte
diesem Wunsche um so eher entsprechen, als dieses zweite Elbnivellement auch für die
Gradmessung als Kontrolle früherer Nivellements von Interesse zu werden versprach. Dieses
Nivellement wurde im Jahre 1885 bei Lauenburg begonnen, von dort aus wurde es im Laufe
des genannten Jahres und des folgenden bis zur preussisch-sächsischen Grenze geführt und
durch ein Doppelnivellement an das Nivellement des Geodätischen Instituts bei Röderau an-
geschlossen. In Verbindung mit dem Elbnivellement von 1876-1877 ist also durch diese
Arbeit eine doppelt und in entgegengesetzter Richtung ausgeführte Messung längs der Elbe
vom Königreich Sachsen an bis Geesthacht gewonnen worden, welcher letztere Punkt mit
einem Nivellementspunkte des Geodätischen Instituts bei Harburg in den Jahren 1877 und
4881 einmal und 1886 ein zweites Mal in Verbindung gebracht wurde.

Neben einer Reihe von Abzweigungen, welche zum Anschluss des Elbnivellements
an ältere Nivellements des Geodätischen Instituts und an solche der Landesaufnahme vorge-
nommen wurden, erfuhr das Elbnivellement eine nicht unwesentliche Erweiterung bei Barby,
indem hier die Messung von der Mündung der Saale auf beiden Ufern des Flusses bis zur
herzoglich anhaltischen Grenze ausgedehnt wurde. Zu erwähnen bleibt hier noch, dass in
Rücksicht auf die hohe Bedeutung, welche den Wasserstandsbeobachtungen der Flüsse als
Grundlage für aufzustellende Hochwasserprognosen, für Untersuchungen über die Veränder-
lichkeit der Wassermengen u. s. w. beigemessen werden muss, ein ganz besonderes Augen-
merk auf die Bestimmung von scharfen Werthen für die Nullpunktslage der Pegel gerichtet
wurde. Durch die bezüglichen Arbeiten 'konnten verschiedene Zweifel in dieser Beziehung
beseitigt werden, so dass nunmehr einer wissenschaftlichen Ausnutzung der oft über viele
Jahrzehnte zurückreichenden betreffenden Wasserstandsbeobachtungen der Boden geebnet ist.

Eine Bearbeitung des ganzen Nivellementscomplexes der Elbe ist im Gange.
25

 
