 

163

Ueber Normal-Null Ueber dem Mittelwasser

 

 

mittgetheilt von der der Adria bei Triest,
: |  trigonometrischen abgeleitet | Differenz.
Nivellementbolzen. _ Abtheilung der kénigl. aus den österreichischen
| Landesaufnahme. Nivellements.
| (p) oO (p) — (0)
| | über Olmütz
|
| 7044 nn 35 m m
gm 5:06 € 50.997 et 0.414 1
350,583
bei Peterwitz über Schönbrunn

 

 

 

 

 

 

 

 

351,022 | — 0,139

2. An die sächsischen Präcisions-Nivellements in Bodenbach, Bünaburg, Eulau,

Schneeberg (Dorf) und « Hoher Schneeberg » (Aussichtsthurm); ferner in Gross-Schönau,
Hainewalde, Scheibe und Zittau.

Die vorläufigen Meereshöhen für diese Anschlusshöhenmarken, abgeleitet aus den
österreichischen Nivellements, und zwar von Triest über Laibach—Graz—Bruck —Wien—
Lundenburg—Znaim—Budweis—Pilsen—Prag —Aussig—Bodenbach sind die nachfolgenden :

Bodenbach ln
Bamabere- See | ou... 0:39 5
Bulaı ee Ai. 9 0 7438
Schneeberg (Dorf). sub... 2096.30:097.08
Hoher Schneeberg (Aussichtsthurm) . . . . 794,14
Gross-Schonau..9 =. 2.5... 022.2990.15
Hamewalle AD >. BUSS eB 231400
Scheibe 908 8 ORT 8
Pie one

3. An die bayerischen Präcisions-Nivellements in Fussach, Bregenz, Lindau und
Nonnenhorn, und zwar haben die österreichischen Nivellements die nachfolgenden Fixpunkte
mit den bayerischen Präeisions-Nivellements gemein : 559 2), 564, 565, 567 (zugl. Hauptfix-

! Man Vergleiche : « Verhandlungen der vom 41. bis zum 43. September 188% im Haag vereinigten
permanenten Commission der Europäischen Gradmessung zugleich mit dem Generalberichte für das Jahr 1884
und 1882, herausgegeben vom Centralbureau der Europäischen Gradmessung », pag. 410.

? Die Nummern beziehen sich auf das bayerische Verzeichniss der Fixpunkte in dem Werke: «Da
Bayerische Präcisions-Nivellement etc. 1. bis 6. Mittheilung von Dr. Carl M. von Bauernfeind » oder in :

« Das Bayerische Präcisions-Nivellement und seine Beziehungen zur Europäischen Gradmessung » von demsel-
ben Verfasser, München 1880.

 
