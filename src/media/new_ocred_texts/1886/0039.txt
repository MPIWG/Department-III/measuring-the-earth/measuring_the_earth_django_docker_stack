er

RR

 

45

Gradmessung », geplante, grosse, fur die Wissenschaft der Erde so fruchtbare Werk, all-
malig nicht nur ganz Europa umfassen, sondern sogar uber dessen Grenzen hinaus, bis
nach Nord-Amerika, und andrerseits durch Englands Beitritt, im Jahre 1884, bis nach Indien
sich erstrecken zu sehen.

Sie erwarten nicht von mir, meine Herren, dass ich an dieser Stelle, und wie
gelegentlich, es unternehmen könnte, durch eingehende Schildrung der reichen Thätigkeit
des General Baeyer ein unserm hochverehrten Altmeister würdiges Denkmal zu setzen. Dazu
wäre hier weder Ort noch Zeit. Indessen lebe ich der Hoffnung, dass wir diese Ehrenpflicht
bald, in den Publikationen der Gradmessung, in einer dem Hingeschiedenen, wie unserer
wissenschaftlichen Vereinigung würdigen Weise erfüllen werden. — Gestatten Sie nur dem-
jenigen seiner Mitarbeiter, dem das Glück und die Ehre zu Theil geworden ist, dem General
Baeyer vom Anbeginn seines internationalen Unternehmens an besonders nahe zu stehen,
dem grossen Manne und dem unvergesslichen, liebenswürdigen Freunde, der uns entrissen
worden ist, noch einmal mit einem Worte tiefgefühlten Schmerzes den Tribut aufrichtigster
Verehrung und treuesten Andenkens zu zollen.

Möge es uns gelingen, den Gefühlen der Dankbarkeit, die unserer aller Herzen für
den General Baeyer erfüllen, vor Allem dadurch Ausdruck zu geben, dass wir sein herrliches
Werk in würdiger und fruchtbringender Weise fortführen und entwickeln.

Dem Berichterstatter liegt nun weiter die Pflicht ob, die übrigen Verluste, welche
der Tod in die Reihen der Gradmessung gerissen hat, ehrend zu erwähnen.

Schon im Jahre 1883 starb unser französischer College Yvon Villarceau, welcher
am 45. Januar 1813 zu Vendöme geboren, auf Grundlage seiner Abhandlung über den Co-
meten von 1845 am Pariser Observatorium angestellt wurde, wo er 1854 Titularastronom
wurde; er gehörte bis zu seinem Lebensende diesem Institute an; 1865 wurde er Mitglied des
Bureau des longitudes, und 1867 Mitglied der Akademie der Wissenschaften. Die Früchte
seiner rastlosen wissenschaftlichen Thätigkeit legte er in mehr als fünfzig Abhandlungen
nieder.

Es würde hier zu weit führen auf die Aufzählung aller dieser Arbeiten einzugehen,
denen fast ohne Ausnahme der Charakter der Originalität und Gediegenheit aufgepragt ist;
wir wollen uns hier auf die werthvollen Beiträge beschränken, welche er unserm Unter-
nehmen geliefert hat. In den Jahren 1861-1865 hat er nach einem systematischen Plane
auf acht Haupttriangularirungspunkten, die Längen, Breiten und Azimuthe bestimmt. Die
Behandlung und Discussion dieser Beobachtungen hat ihn selbständig zur Aufstellung ver-
schiedener Theoreme geführt; unter andern zu einer einfachen Bedingungsgleichung zwi-
schen den astronomischen Längenunterschieden und den Azimulhen. Villarceau war ein
ständiger Besucher unserer Versammlungen und griff. überall in die Discussion anregend
und vertiefend ein. Der Tod entraffte ihn uns am 23. December 1883; wir Alle bewahren
ihm ein treues Andenken.

Der zweite unserer Collegen, den wir im Jahre 1884 verloren haben, ist der
k. k. Ministerialrath Josef Herr, der am 18. November 1819 in Wien geboren war. Er
wendete sich zunächst den juridischen Studien an der Universität Wien zu, erwarb nach

 
