223

sechs Stationen unter der Bedingung zu finden, dass die Summe der Quadrate der übrig-
bleibenden Fehler ein Minimum giebt.

Wenn man die Lage der fünf Punkte gegen « Leipzig B» in der Karte betrachtet und
damit die oben gefundenen relativen Lothabweichungen vergleicht, kann man eine gewisse
Gesetzmässigkeit der letztern nicht verkennen. Die Uebersicht wird erleichtert, wenn man die
Resultate in der beigegebenen Karte graphisch darstellt und zwar durch äquidistante Paralle-
len gleicher Lothabweichungen. Um diejenige Lage der Parallelen, die den gefundenen Loth-
abweichungen am Besten entspricht, zu ermitteln, wird das Azimuth A der Achse des Paral-
lelensystems, welche dasselbe rechtwinklig durchschneidet, nach der Methode der kleinsten
Quadrate bestimmt.

Das Azimuth der geodätischen Linie zwischen « Leipzig B» und irgend einem der
mehrerwähnten fünf Punkte sei «, dann ist A—« der Winkel zwischen dieser Linie und der
zu findenden Achse. Ist ferner D die Länge genannter Linie in km, so repräsentirt.D. cos (A —)
- die Projection derselben auf die Achse, und soll endlich diese Projection die Anzahl Loth-
abweichungssecunden darstellen, welche der betreffenden Abseisse entspricht, so muss die-
selbe noch mil einem zu ermittelnden Verwandlungs-Coefficienten C multiplicirt werden,
» welcher unter der Annahme, dass sich die Lothabweichungen proportional den Entfernungen
ändern, die Anzahl Secunden angiebt, welche der Aenderung der Abscisse um 1 km ent-
‚spricht. Es würde dann |

G.D.cos (A—«)

die Lothabweichung am Endpunkte der Abscisse bedeuten und wenn man die Lothabweichung
im Anfangspunkte derselben, nämlich auf der Station « Leipzig B», mit & bezeichnet, würde
die beobachtete relative Lothabweichung «” durch die Gleichung

a og DD. A on

dargestellt werden. Wegen der unvermeidlichen Beobachtungsfehler ist aber an u noch die
Verbesserung v anzubringen, was zu der allgemeinen Form der Fehlergleichungen

” u. 10) N oo

führt. Löst man darin cos (A---x) auf und werden die dann auftretenden noch unbekannten
Producte ; :
Cxcos\ —— vy und Ges — 2

gesetzt, so geht diese Gleichung über in

my oo Mona,

worin streng genommen x den Fehler in der Lothabweichung Null auf Station « Leipzig B »
bedeutet.

 
