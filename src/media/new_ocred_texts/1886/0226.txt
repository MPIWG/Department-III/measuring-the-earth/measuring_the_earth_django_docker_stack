202

3, Zwei Chronometer-Expeditionen von Stbs.-Cap. Salesski : Samarkand-Buchara und
Buchara-Tschardjui am Amu-Daria, bestimmt 5 Punkte, noch nicht publiciert. Durch diese
_ Expeditionen sind astronomische Bestimmungen im Gebiete von Turkestan mit denjenigen
von Gedeonoff im Transkaspischen Gebiete verbunden, wobei sich nur eine für allgemein
geographische Zwecke unbedeutende Discordanz in der Länge des gemeinschaftlichen Punktes
Tschardjui ergab.

Und 4. Chronometer-Expeditionen von Stbs.-Cap. Nasarieff im Ussuri-Gebiete (östl.
Sibirien), womit die geographische Lage einiger Punkte an unserer Granze mit China be-
stimmt ist.

B. GEODATISCHE ARBEITEN
a) Präcisions-Nivellements.

Im Jahre 1883 ist die doppelte Nivellirung zwischen dem Baltischen und dem
Schwarzen Meere vollendet. Aus den vorläufigen Berechnungen erwies es sich, dass das
Niveau des Schwarzen Meeres bei Odessa um 0,47 Meter tiefer liegt als dasjenige des Balti-
schen Meeres bei Kronstadt.

In den Berichtsjahren sind die Eisenbahnlinien zwischen Moskau und Rostow a. Don
und zwischen Kasatin (über Kiew, Kursk, Orel) und Griazi doppelt nivellirt worden. Die
Länge der sämmtlichen bis jetzt doppelt nivellirten Linien beträgt ungefähr 7500 Kilometer.
Bei den Nivellements der letzten Jahre sind dieselben Instrumente (vergröss. 45) und Latten

wie früher verwendet, nur ist die früher geübte Nivellirungs-Methode durch feste Ver-
bindung der Libellen mit den Fernrohren ein wenig modifieiert. Der Nicht-Parallelismus der
Libellen- und optischen Achsen wird durch Nivellirung aus der Mitte und endlich durch
Rechnung eliminirt. Der W. F. der Nivellements hat sich ergeben zu 3-4um p. Kilom.

b) Triangulirungen.

Die gegenwärtigen trigonometrischen Arbeiten beschränken sich hauptsächlich auf
Triangulationen 21 und Zter Ordnung, die zu speciell topographischen Zwecken in den west-
lichen Gouvernements des europäischen und in einigen Gegenden des asiatischen Russlands
ausgeführt werden. Im europäischen Russland und im Kaukasus werden diese Triangula-
tionen an diejenigen Ater Ordnung angeschlossen. In unsern asiatischen Besitzungen aber, da
wir keine sich bis dorthin ausdehnende continuirliche Triangulation 4te Ordnung haben,
bleiben die bis jetzt ausgeführten trigonometrischen Netze ganz abgesondert, indem jedes
Netz auf eigenen astronomisch bestimmten Punkten begründet ist. Von unsern asiatischen
Triangulationen erwähnen wir hier die in dem Transkaspischen, Fergana- (vormaliger Kha-
nat von Kokand), Transbaikalischen und Ussuri-Gebiete ausgeführten.

Ausser den trigonometrischen Arbeiten werden auch zu Orts-Bestimmungen in

 
