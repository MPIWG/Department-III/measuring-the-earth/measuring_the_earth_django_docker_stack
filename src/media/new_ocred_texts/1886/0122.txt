 

 

98

Non seulement M. le Président donne son approbation à cette manière de voir, mais il
renonce en même temps à renvoyer à une nouvelle Assemblée la solution de cette question.
Il est d'autant plus disposé à agir de la sorte qu'il est assuré que son Gouvernement n'aura
aucune objection à faire à ce sujet.

La discussion terminée’ et résumée en allemand par M. le Secrétaire, M. le Président
soumet au vote la proposition de MM. Ferrero et Ibanez et l’Assemblée prend à l'unanimité
la décision suivante :

Un membre empêché d'assister aux séunces de la Commission permanente n'a pas le
droit de se faire remplacer par un autre savant de son choix; mais il est libre, après en avoir
avisé le Président, de déléquer sa voix, pour la durée de son absence, à un de ses collèques de la
Commission permanente.

La parole est accordée à M. van de Sande-Bakhuyzen pour lire son rapport sur les
travaux exécutés ces derniers temps dans les Pays-Bas. (Voir aux Annexes.)

M. le Président remercie M. Bakhuyzen.

M. le professeur Hirsch fait observer, relativement à l'erreur kilométrique des nivel-
lements, qui a été mentionnée dans le rapport hollandais, que c’est seulement dans des pays
de plaine que l'erreur de ces nivellements de précision peut être envisagée uniquement
comme fonction des longueurs parcourues, tandis que dans des pays montagneux, elle dé-
pend en outre essentiellement des différences de niveau surmontées.

M. le professeur Nagel communique son rapport sur les travaux faits en Saxe; M. le
professeur Rosén, sur les mesurages exéculés en Suéde dans les derniéres années; M. le pro-
fesseur Hirsch, sur l'activité de la Commission géodésique suisse pendant Îles trois dernières
années; M. le général Ibanez, sur les travaux exécutés en Espagne. (Voir aux Annexes.)

M. le Président exprime aux divers rapporteurs les remerciements de l'Assemblée,
pour leurs précieuses communications.

M. le professeur Helmert, se référant à un désir exprimé dans le rapport suisse el
d’après lequel les régles pour la mesure des bases, qui n’ont pas encore été comparées par
le Bureau international des poids et mesures, y soient envoyées le plus tot possible pour y
être étalonnées, considère celte motion comme tellement importante qu'il propose à la Con-
férence de l’'approuver formellement.

Sur la demande du Président, l'Assemblée déclare se ranger à cet avis.

M. le Président donne enfin la parole à M. l'ingénieur Lallemand, présenté par
M. Faye, et qui fait un rapport sur le nouveau nivellement de précision en voie d'exécution
en France, sous sa direction, depuis quelques années. (Voir aux Annexes.)

M. le profeseur Hirsch a suivi la lecture de ce rapport avec erand intérêt et il
attend avec une vive impatience les résultats du nouveau nivellement francais, qui relier:
aussi les hauteurs suisses avec l'Océan et la mer Méditerranée. Tout en remerciant M. Lalle-
mand pour ses communications, il croit cependant que plusieurs changements, appelés par

 

 

 

 
