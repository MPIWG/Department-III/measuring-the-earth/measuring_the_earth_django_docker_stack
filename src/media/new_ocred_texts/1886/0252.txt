Beilage XIV.
SCHWEDEN

BERICHT

über die astronomisch-geodätischen Arbeiten I. Ordnung in Schweden.

Seit den letzten Mittheilungen über die schwedischen Gradmessungsoperationen sind
die astronomisch-geodätischen Arbeiten I. Ordnung in Schweden weiter fortgeschritten und
ich halte es für die mir obliegende Pflicht als Bevollmächtigter jetzt eine kurze Auseinander-
setzung derselben mitzutheilen.

Diese Arbeiten erstrecken sich hauptsächlich über das mittlere und nördliche
Schweden und umfassen einen Zeitraum von circa siebzehn Jahren. Sie sind überhaupt in
Uebereinstimmung mit den allgemeinen Prinzipien und Forderungen, welche bei der Euro-
päischen Gradmessung sich geltend gemacht oder sich bewährt haben, ausgeführt worden.
Es ist jedoch einleuchtend, dass sehr schlechte CGommunicationen, ‚kurze Sommer, die
sehr wechselnden Witterungsverhältnisse unseres Klimas, ein stark bewaldetes Terrain,
nebst anderen Umständen, Hindernisse in den Weg gelegt haben, um die Operationen nach
einem vollständigen, einheitlichen Programm durchführen zu können.

(Für die Uebersicht der astronomisch-geodätischen Arbeiten folgt eine Karte in
kleinem Maassstabe.)

A. ASTRONOMISCHE BESTIMMUNGEN.

Azimuthe sind mit einem Repsold’schen Universalinstrumente von 12zölligem Hori-
zontalkreise ermittelt worden, und zwar auf folgenden zehn Punkten, welche theils den
Hauptdreiecken, theils den Connectionsnetzen der Basislinien und der astronomischen Sta-
tionen angehören, nämlich : Kinnekulle, Bispberg, Frösö, Roknäs, Bredskär, Hernö, Arder-
viksberg, Maderö, Göteborg und Klintebacke.

Bei Roknäs ist das Azimuth auch mit einem Passageninstrumente bestimmt
worden.

 
