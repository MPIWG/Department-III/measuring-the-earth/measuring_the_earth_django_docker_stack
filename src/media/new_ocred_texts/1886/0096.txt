 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

SI
bo

que, tandis que notre Conférence actuelle maintient, après celle de Rome en 1883, la pé-
riode triennale réglementaire, la Commission permanente ne se soit pas réunie dans l’inter-
valle. Dans sa séance tenue à Rome après la clôture de la Conférence générale, le 24 octobre
1883, la Commission avait pensé à Nice pour lieu de réunion de l’année suivante (1884),
par suite d’une invitation très aimable de M. Bischoffsheim, propriétaire du splendide Obser-
vatoire qu'il a fondé près de Nice avec autant d’entente que de générosité. Mais avant que

la Commission permanente prit sur ce point une derniére décision, plusieurs membres ont

émis l’opinion que, eu égard aux délibérations étendues et sur certains points définitives de
la Conférence générale de Rome, il serait opportun de renvoyer la prochaine réunion à lan-
née 1885, d'autant plus qu’au cours de l'été de 1884 l’état sanitaire du Sud-Est de la France
commença à s’assombrir. Le Bureau ayant consulté, par une circulaire du 25 juin 1884, la
Commission, afin de constater son opinion sur ce point d’une manière authentique, celle-ci
s’est prononcée unanimement pour le renvoi, ce qui a été porté à la connaissance de tous
les commissaires par circulaire du 3 juillet.

Apres avoir obtenu la certitude que l'invitation à l’Obervatoire de Nice serait main-
tenue pour l’année suivante, le Bureau, par lettre circulaire du 28 mai 1885, proposa à la
Commission permanente de se réunir à Nice le 1° octobre de la même année; cette proposi-
uon fut agréée à l'unanimité, de sorte que le Bureau put, par lettre du 20 juin, inviter tous
les commissaires de l'Association géodésique à la réunion de Nice pour le 1° octobre 1885.

Malheureusement cette assemblée, préparée déjà en détail, fut encore empéchée au
dernier moment par le choléra, qui éclata au Midi de la France, dés le mois d'août, avec
une grande intensité dans plusieurs villes de la côte méditerranéenne. La Commission per-
manente, consultée par le Bureau au moyen d’une eireulaire en date du 24 août 1885, de-
mandant si elle voulait maintenir sa décision ou renvoyer de nouveau la Conférence, s’est
décidée par six voix contre trois dans le dernier sens.

À peine avions-nous donné connaissance de cette décision à Messieurs les commis-
saires, que la mort de notre cher maître est venue malheureusement justifier la décision prise
par la Commission, comme par le pressentiment du coup du destin le plus dur qui püt frap-
per l’Association géodésique. C’est le 11 septembre que notre Association a perdu son fonda-
deur. Le général Baeyer, qui fut respecté comme savant autant qu'il fut aimé généralement
comme caractère, après une vie riche en travaux et en succés scientifiques, s’est éteint
tranquillement à un âge avancé, aprés avoir eu la grande satisfaction de voir l’œuvre
qu'il avait fondée il y a vingt-deux ans, sous la forme modeste de « l'Association pour me-
surer les degrés dans l’Europe centrale », se développer continuellement, jusqu’à embrasser
d’abord le continent européen tout entier, et s'étendre ensuite au delà de l'Océan jusqu'aux
Etats-Unis d'Amérique, et, par l’accession de l'Angleterre, jusqu'aux Indes orientales.

Vous n’attendez pas de moi, Messieurs, qu’à cette tribune et comme occasionnelle-
ment, j'entreprenne d’ériger un monument digne du général Baeyer, en développant dans
ses détails son œuvre scientifique. Mais j'espère que nous remplirons bientôt ce devoir
d'honneur dans nos publications, d’une manière digne à la fois du grand homme qui a dis-
paru, et de notre Association. Permettez seulement à celui de ses collaborateurs qui à

 
