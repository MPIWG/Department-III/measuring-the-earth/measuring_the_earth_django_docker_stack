rs np ei

 

ee

 

 

Vierte Sitzung.
30. October 1886.

Die Sitzung wird um 2 3/, Uhr eröffnet.
Präsident : Ilerr Professor Förster.
Anwesend die gleichen Mitglieder wie in der Vormittagssitzung.

Der Präsident theilt zunächst mit, dass morgen Sonntag, um 11 Uhr Vormittags, die
Permanente Commission von ihrem Präsidenten zu einer Sitzung auf der Sternwarte einbe-
rufen ist.

Alsdann nimmt der Präsident wieder die Reihe der einzelnen Länderberichte auf.

Zunächst ertheilt er Herrn General Ferrero das Wort, welcher über die Arbeiten
der letzten drei Jahre in seinem Lande ein kurzes, mündliches Referat abstattet. (Siehe
Anhang.)

Hierauf ertheilt der Präsident das Wort dem Herrn General Falcoiano, Delegirten von
Rumänien, welcher mit Bezug auf eine im Berichte des Herrn Generals Ferrero enthaltene
Bemerkung der Gonferenz eine Mittheilung zu machen wünscht.

Herr General Falcoiano erklärt zunächst, dass in Rumänien die geodätischen Ar-
beiten, seit der letzten General-Conferenz, ihren Fortgang genommen haben, und dass er
bereits den Entwurf des Dreiecknetzes erster Ordnung dem Bureau übergeben habe.

Was dann die von Herrn Ferrero erwähnte Lücke in dem grossen europäischen
Erdmessungs-Netze betrifft, so bemerkt General Falcoiano, dass, wenn auch die wissenschalt-
lichen Institute seines Landes dasselbe nocht nicht in den Stand setzen, einen bedeutenden
Beitrag zu dem gemeinschaftlichen Werke zu liefern, er doch die Versicherung geben könne,
dass bis zur nächsten General-Conferenz die erwähnte Lücke in Rumänien ausgefüllt werden
wird. Er fügt hinzu, dass, um Rumänien so viel als möglich zur Aufgabe der internationalen

5

 
