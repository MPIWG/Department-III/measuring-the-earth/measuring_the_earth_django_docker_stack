26

Professor Hirsch dankt seinen CGollegen für das ehrenvolle Zutrauen, welches sie
ihm von Neuem gewährt haben, und dem er nach besten Kräften gerecht zu werden ver-
spricht.

Darauf wird zur Ernennung der neun zeitweiligen Mitglieder der Permanenten
Commission geschritten.

Herr v. Oppolzer spricht den Wunsch aus, dass bei Abgabe der Stimmzettel die ein-
zelnen Länder aufgerufen und deren stimmführende Mitglieder ersucht werden, ihre Stimm-
liste den Stimmzählern einzuhändigen.

Auf diese Weise wird durch den Präsidenten und die beiden Stimmzähler das Resul-
tal der stattgefundenen Abstimmung folgendermassen festgestellt :

Die absolute Majorität, also mehr als neun von den abgegebenen neunzehn Stimmen,
erhielten folgende Herren, welche somit zu Mitgliedern der Permanenten Commission ge-
wählt sind :

v. Oppolzer » 17 »
Stebnitzky » 16 »
Zachariae >» 17 »

1. Bakhuyzen mit 17 Stimmen.
2. Faye > 17 »

3. Ferrero ys »

4. Förster » 14 »

9. Ibafiez > © »

6. Nagel » 14 »

7.

8.

9

Ausserdem haben Stimmen erhalten die Herren :

v. Bauernfeind 7 Stimmen.

v. Struve 4 »
Schreiber 3 »
Perrier 3 »
Seeliger 2 »
Zech 2 »
Hennequin 1 »
Folie 1 »

Zufolge der Aufforderung des Präsidenten, erklären die neun ernannten Herren die
Annahme ihrer Wahl.

Auf Vorschlag des Präsidenten wird beschlossen, dass die soeben ernannte Perma-
nente Commission sich unmittelbar nach Schluss der heutigen Sitzung constituire, dass sie
alsdann eine Sitzung am Freitag um 2 Uhr abhalten werde, und endlich dass die nächste
Plenarsitzung am Sonnabend 10 Uhr Morgens stattfinden solle.

‘Die Sitzung wird um 12 1/, Uhr geschlossen.

 

 

 

REED EEE

 

 

 

 

 

 

 

 

 

 
