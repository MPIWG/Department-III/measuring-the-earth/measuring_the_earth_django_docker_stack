  

 

 

 

 

 

 

 

 

 

 

 
  

  

 

 

 

 

  
 

 

  
  
 

 

  
 
  

 

 

 

 

   

 

 

 

12

 

tution in Paris der Anschluss jener Messungen an die letzten, gemeinsam zu ver-
waltenden Fundamente aller Massbestimmungen in erfreulichster Weise gewonnen.

Nur auf einem Gebiete ist wohl die Entwickelung der Erdmessungsarbeiten
langsamer gewesen, als im Jahre 1864 gehofft wurde, nämlich auf dem Gebiete
der theoretischen und rechnerischen Durchdringung und Verbindung der Lan-

desvermessungs-Ergebnisse unter einander und mit dem System der astronomi-
schen Bestimmungen, also auf d

em eigentlichsten Gebiete der Geodäsie. Wir
wagen indessen zu hoffen, dass gerade die in der

neuen Uebereinkunft geordnete
und gesicherte Stärkung der leitenden Stellung Ihrer Permanenten Kommission
und des Zusammenwirkens derselben mit Ihrem Centralbureau auch in dieser
Beziehung eine fröhliche u

nd gedeihliche Entwickelung baldigst hervorrufen
wird.

Herr von Struve wünscht der Rede des Präsidenten noch einige Worte beizufügen

und äussert sich folgendermassen :
« Als vor dreissig Jahren die erste Vereinigung geodätischer Arbeiten für die dama-

lige Längen-Grad-Messung erzielt wurde, war es vorzugsweise das persönliche Interesse, die
lebhafte Betheiligung des damaligen Prinzen von Preussen, des jetzt glorreich regierenden
Kaisers und Königs, welche diese Vereinigung ermöglichte und zur Wirkung brachte. Es ist
daher gerade S. Majestät der Kaiser Wilhelm, dem wir ganz besonders unsere Huldigung
und unseren Dank darzubringen haben. »

Nachdem die Sitzung während einer Viertelstunde unterbrochen worden und die
erschienenen Herrn Minister und sonstigen hohen Staatsbeamten sich zurückgezogen haben,
nimmt der Präsident dieselbe wieder auf, indem er zunächst einige geschäftliche Mitthei-
lungen und Vorschläge der Versammlung zur Kenntniss bringt.

Was sodann die nähere Feststellung des Arbeitsprogrammes für die einzelnen
Sitzungen betrifft, so schlägt der Präsident vor, die heutige Sitzung wesentlich zur Entgegen-
nahme der Berichte der bisherigen Permanenten Commission, sowie des Gentralbüreau’s zu
bestimmen, um somit den nothigen Zusammenhang zwischen der Vergangenheit der Grad-
messung und der neuen Entwicklungsphase derselben herzustellen.

Die zweite Sitzung würde wesentlich zur Ausführung der neuen, internationalen

Uebereinkunft, durch Vornahme der Wahlen des ständigen Sekretärs und der zeitweiligen

Mitglieder der Permanenten Commission bestimmt sein. In den weiteren Sitzungen würde
Fortschritte der Arbeiten im

die Versammlung die Berichte verschiedener Delegirten über die
ihren Ländern, sowie einige wissenschaftliche Mittheilungen entgegennehmen, an welche sich
etwaige Discussionen knüpfen dürften.

Zwischen den Generalversammlungen oder zum Schlusse d
die Sitzungen der Permanenten Commission vorzusehen.

Unter den für die Conferenz eingegangenen Mittheilungen sind fası ausschliesslich
solche, welche die in der Gradmessungs-Conferenz zu Rom vorgeschlagene, und dann von der
diplomatischen Conferenz in Washington nicht zum Abschluss gebrachte Frage der Univer-

erselben wäre ein Tag für.
