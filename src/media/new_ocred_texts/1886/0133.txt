 

 

 

109

TROISIÈME SEANCE

31 Octobre 1886.

La séance est ouverte à 11 heures !/,.
Présidence de M. le général Ibanez.
Tous les membres de la Commission assistent à la séance.

M. le Secrétaire lit le procès-verbal de la dernière séance et en donne Îe résumé en
français. Il est adopté avec une légère rectification réclamée par M. le professeur Fœrsler.

Relativement à la question, renvoyée à l'examen de la Commission, concernant le
rapport sur les déterminations de la pesanteur, M. Hirsch propose, à la suite de pourparlers
avec les intéressés, de charger M. von Oppolzer du rapport proprement dit sur tes travaux
exécutés, comme cela avait été convenu tout d’abord, et de prier M. le general Stebnitzky
d'élaborer un programme en vue des déterminations générales dela pesanteur. La Commission
partage cet avis.

M. le général Stebnitzky déclare que, pour le moment, il se bornera à dresser un
programme pour les déterminations de la pesanteur en Russie.

M. Hirsch annonce qu’il est chargé par le Comité international des poids et mesures
de proposer à l'Association géodésique qu’elle fasse entreprendre des déterminations de la
pesanteur à Breteuil et dans l’ancien laboratoire de Regnault.

Le général Ibanez estime que ce travail incombe aux oéodésiens français.

M. Faye, sollicité par la Commission, s'engage à faire en France les démarches
nécessaires pour atteindre ce but.

Par suite d’une autre initiative, prise par le Comité international des poids et mesures,
M. Hirsch porte une seconde question devant la Commission permanente : celle de la nécessité
de comparer, d’une manière aussi précise que possible, toutes les toises et étalons qui ont
servi dans les divers pays pendant le dernier siècle. L'Institut international de Breteuil posséde

 

 

 

 

 
