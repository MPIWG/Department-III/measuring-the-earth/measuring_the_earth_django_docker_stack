v
i
+
§
£
}

 

neue

een,

a

 

63

Pour da) Saxe M. A. Nagel, Conseiller de gouvernement, Professeur de géodésie
à l'Ecole polytechnique, à Dresde.
» la Supe: M. le Dr P.-G. Rosén, Professeur à l'État-major, Membre de I’ Aca-
démie, à Stockholm.
fa SUISSE: M. le Dt A. Hirsch, Directeur de l’Observatoire, 4 Neuchatel.
» le WuRTEMBERG : M. le Dr von Zech, Professeur de physique & l’Ecole polytechnique,

M.

. le Dr von Scholz, Ministre des finances.

S

a Stultgart.

Assistaient en outre à la séance :

le Dr Friedberg, Ministre de la justice.

. le Dr Lucius, Ministre de l’agriculture.

. le Ministre d'Etat von Büllicher, Secrétaire d'Etat du Département de l’intérieur

de l’Empire.

. le Dr von Schelling, Secrétaire d'Etat du Département de justice de l'Empire.

. le Général von Stubberg, Inspecteur général de l'instruction militaire.

. le Conseiller Greiff, Directeur au Département de l'instruction.

. le Conseiller Homeyer, Secrétaire au Ministère d'Etat.

. le Conseiller de légation Hellwig, Directeur au Département des affaires étrangères.

. le D' Althoff, Conseiller-Rapporteur au Département de l'instruction.

E. le Ministre d’Etat, Chef du Département des affaires ecclésiastiques, sco-

laires et médicales, M. le Dr von Gossler ouvre la séance à 21/, heures et adresse à l’As-
semblée le discours suivant :

Messieurs les membres de la Conférence,

C’est pour la troisième fois que le gouvernement prussien à l'honneur et le
plaisir de saluer les délégués des Etats fondateurs de l'Association géodésique
européenne, et de souhaiter la bienvenue aux délégués des autres pays qui prêtent
leur appui à cette grande entreprise.

Vingt-deux ans se sont écoulés depuis que, dans cette capitale, quatorze
Etats ont uni leurs efforts pour la mesure des degrés de PEurope centrale, afin
de rattacher celte partie du continent aux contrées de l'Ouest et de PEst qui
avaient devancé le centre par des travaux géodésiques fondamentaux; dix-neuf

 
