HR PERF RS RER

at

 

 

Hl

 

Dans l'intervalle des Assemblées générales ou à la clôture de celles-ci, il faudrait
réserver un jour pour les séances de la Commission permanente.

Parmi les communications parvenues à la Conférence, presque toutes se rapportent
exclusivement à la question du temps universel et du premier méridien, questions proposées
et étudiées à la Conférence géodésique de Rome, et non résolues à la Conférence diploma-
tique de Washington. Malgré les pétitions énergiques qui nous ont été adressées de divers
côtés pour demander à la Conférence actuelle de s’en occuper de nouveau, M. le Président
estime que, dans Pintérêt de la question, la Conférence ferait bien de ne pas s’en occuper
pour le moment. Les documents qui s’y rapportent restent néanmoins déposés sur le
Bureau de l’Assemblée.

En outre, M. le Président annonce aux membres de l'Assemblée que M. Gravelius vient
de publier des tables trigonométriques à cinq décimales, pour la division décimale du quadrant,
avec une préface de M. Foerster et il fait ressortir la grande utilité de ces tables pour la partie
technique des calculs scientifiques.

M. le professeur Helmert, complétant les communications de M. le Président, ajoute que,
par l’ordre de M. le Ministre von Gossler, il a fait parvenir à tous les délégués un exemplaire
de l'ouvrage qui a paru à l’occasion de la dernière réunion, à Berlin, du Congrès des natu-
ralistes. Cette publication a pour titre: « Les institutions scientifiques et médicales de Berlin,
relevant de l'Etat. » En outre, il donne lecture d’une lettre de M. le général J. Walker, Direc-
teur des travaux géodésiques aux Indes, ainsi que de M. Wharton, hydrographe anglais, qui
expriment leurs regrets de ne pouvoir assister à la Conférence. M. le colonel Perrier, de
Paris, l’un des trois délégués français, se fait également excuser, l’état de sa santé ne lui
permettant pas de faire le voyage à Berlin.

M. le Président invite M. Hirsch à présenter son rapport au nom de la Commission
permanente sortant de charge. Ge rapport a la teneur suivante :

Messieurs,

Monsieur le Président de notre Assemblée a cru que, pour mieux rattacher la Con-
férence actuelle aux Conférences générales de l’ancienne Association pour la mesure des de-
grés en Europe, il serait utile qu’un membre de la Commission permanente résumat brié-
vement les principaux faits des derniers trois ans.

En ma qualité de Secrétaire, j'ai l'honneur, au nom de l’ancien Bureau de la Com-
mission, de remplir cette lâche un peu difficile, en ce sens que, par suile des circon-
stances particulières que vous savez, la Commission permanente à, dans la période des
trois dernières années, trouvé peu d'occasions d'exercer son action; je suis donc justifié et
obligé de rendre ce rapport très succinet, d'autant plus que, comme par le passé, les délé-
gu6s des différents Etats se chargeront de rendre compte des travaux eéodésiques accomplis
dernierement dans leur pays.

Permettez-moi d’abord, Messieurs, de rappeler brievement comment il s'est fait

 
