237

des poids et mesures, ce qui a été fait au printemps de 1886. D’après le certificat que nous
avons — au mois de mai, l’équation de la régle de Berne est la suivante :

s (0 —2900"") = 2900” 492 (1 + 0,00001168 se °) + 0" 005.

. ou bien dans cette autre forme :

s (0 — 2900") — 2900, 9900: 03388 (7 — 14.7)

tandis que l’ancienne valeur trouvée par M. Wild avait été :

5 (0 — 2900) — 2901, 102 + 0.029 (1 — 14.7).

Des recherches scrupuleuses m’ayant démontré qu’on pouvait négliger l’écart du
nouveau coefficient de dilatation pour les mires, je suis arrivé à la conclusion qu’il suffisait
de multiplier toutes nos allitudes compensées par le facteur (1—0""095). C’est ce qui a ee
fait en 1886.

Mais il a semblé a la ion géodésique qu'avant de publier le tableau des
altitudes suisses, il fallait procéder à une reconnnaissance sur le terrain, pour constater
dans quelle mesure les repères des deux classes, placés depuis 1865, avaient été détruits ou
endommagés. Cette révision a eu le triste résultat que des 255 repères de [er ordre en bronze,
il n’en existe plus dans un état intact que 299, tandis que 10,20/, ont disparu ou ont été en-
_dommagés ou déplacés. Le rapport est encore bien plus considérable pour les repères de
second ordre, marqués simplement au ciseau sur les rochers ou les monuments; il n’y en a
. guère que 1152 sur 1940, c’est-à-dire 5904, qui aient été retrouvés complètement intacts.
Cette perte est due en grande partie au vandalisme des conducteurs de route, et à l’incurie
des ingénieurs de petites villes et des autorités des villages.

Lorsque, dans la dernière séance, notre Commission géodésique avait à décider à
quel niveau fondamental il fallait reporter les altitudes qu’on va publier, au niveau de la
mer ou simplement à notre point de départ suisse, la Pierre du Niton à Genève, nous avons
dû reconnaitre que, mi les jonctions des réseaux des différents pays, ni les observations maréo-
graphiques n’étaient assez avancées pour permettre à la Conférence de Berlin de choisir
déjà le niveau fondamental pour l’Europe entière. Du reste, notre réseau suisse n’est
rattaché jusqu’à présent directement qu’à la Méditerranée à Marseille, par rapport à laquelle
notre niveau fondamental suisse a une hauteur de 374°06, assez incertaine, parce qu’elle re-
pose encore sur l’ancien nivellement Bourdaloue en France et surtout sur une détermination
insuffisante du niveau moyen de la mer à Marseille. Nous avons donc résolu d'indiquer nos

1 La nouvelle équation de la règle de 3" a été communiquée par M. Ris, directeur du Bureau fédéral
des poids et mesures à Berne, aux nombreux Etats qui avaient envoyé leurs mires à Berne.

 
