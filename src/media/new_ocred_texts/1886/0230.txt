206

een fast aller Länder Europa’s gebracht haben. Aber zu einer einheitlichen Verarbeitung
der gesammelten Materialien und bestimmten Folgerungen aus denselben ist es bis jetzt
eben so wenig gekommen, wie zu einer Systematisirung der Arbeiten behufs Verfolgung
klargestellter Probleme, wenn auch durch das gleichfalls durch Baeyer hier in Berlin hervor-
gerufene geodätische Institut manch werthvolle Einzelarbeit ausgeführt und zur Publication
gelangt ist. Dieses Institut war in seiner ursprünglichen Gestalt ganz und gar das Werk
Baeyers und wurde von ihm auch in solcher Weise dirigirt, dass es zugleich als Organ des
internationalen Unternehmens als dessen Centralbureau fungirte. Dieser Character war jedoch
dem Institute nur durch die persönliche Autoritätsstellung des Begründers gegeben. Erst
neuerdings hat derselbe eine höhere Sanetion erhalten durch den Beschluss der preussischen
Regierung, welche dem Institute eine viel ausgedehntere und wirksamere Thätigkeit in der
angegebenen Richtung obligatorisch macht. Diesen Beschluss, verbunden mit der Aussicht,
dass dem Institute durch Aufführung eines grossartigen zweckmässigen Gebäudes mit ent-
sprechender Ausrüstung auch die erforderlichen Hülfsmittel zur erfolgreichen Arbeit geboten
werden, begrüssen wir um so freudiger als durch die neuerdings eingeführte Bezeichnung
des gemeinsamen Unternehmens als internationale Erdmessung demselben eine klar fassbare
hohe Aufgabe gestellt ist.

Betrachten wir die geodätischen Operationen nur vom Standpunkte ihres utilitari-
schen Zwecks, als Grundlage für die Kartographie und Orographie einzelner Länder zu die-
nen, so übersieht man leicht, dass sie einen bedeutend geringeren Kraftaufwand erfordern
würden, als wie ihn die jetzt gestellte Aufgabe erheischt und dass sie'nicht der Anstrengungen
bedürften, welche jetzt erforderlich sind, um die Arbeiten verschiedener Länder unter einander
zu einem harmonischen Ganzen mit möglichster Schärfe zu vereinigen. Vom wissenschaft-
lichen Standpunkte sind jedoch diese Anstrengungen gewiss vollkommen gerechtfertigt. Alle
menschlichen Forschungen über die auf und in der Erde wirkenden Kräfte und die durch
dieselben hervorgerufenen Erscheinungen setzen mehr oder weniger eine Kenntniss der
Oberflächengestaltung unsres Erdballes voraus. Geologie und Physik der Erde bedürfen der-
selben speciell als unumgängliche Grundlage. Diese Kenntniss, sowohl im grossen Ganzen
wie in allen einzelnen Theilen, in grösster Ausdehnung und Schärfe zu beschaffen, das ist
das hohe Ziel, auf welches unsere gemeinsamen Bestrebungen gerichtet sein sollen.

Selbstverständlich werden diese Bestrebungen um so mehr an Bedeutung gewinnen,
je weiter sie sich über die ganze Erdoberfläche verbreiten. Diesem Gedanken entspricht die
Bezeichnung als internationale Erdmessung, indem dadurch zugleich die Hoffnung ausgedrückt
ist, dass allmälig alle eivilisirten Nationen der Erde unserer Vereinigung beitreten werden.
Für sich allein kann der kleine Continent von Europa offenbar nur einen verhältnissmässig
kleinen Theil an Material für die anzustrebende Aufgabe bieten. Desshalb haben wir auch
schon mit Freuden aus dem Munde unsres geehrten Collegen Faye die Mittheilungen über
die Ausdehnung vernommen, welche die Franzosen ihrem grossen Meridianbogen durch
dessen Fortsetzung nach Süden durch ihre nordafrikanischen Besitzungen bis an die Gränze
der Sahara zu geben unternommen haben. In gleicher Weise dürfen wir im Süden dessel-
ben Continents auf besonders werthvollen Beitrag durch die von Gill geleiteten, vom Cap der

 
