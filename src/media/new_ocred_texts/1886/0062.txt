 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

98

 

Endlich verliest der Präsident das folgende Schreiben Seiner Excellenz, des Herrn
Ministers von Gossler, womit derselbe sich von der Conferenz verabschiedet :

«An den Präsidenten der Conferenz für Internationale Erdmessung,
Herrn Geheimen Regierungsrath Professor Dr. Förster Hochwohlgeboren !

« Ew. Hochwohlgeboren beehre ich mich ergebenst mitzutheilen, dass ich durch
eine unanfschiebbare Reise zu meinem lebhaften Bedauern verhindert bin, am nächsten
Dienstag die Führung der Conferenz bei der Besichtigung der Anstalten auf dem Brauhaus-
berge bei Potsdam zu übernehmen und mich von ihren Mitgliedern persönlich zu verab-
schieden.

« Ich hätte das um so lieber gethan, als die Arbeiten der Gonferenz und der Perma-
nenten Commission das Interesse der Preussischen Staatsregierung in hohem Masse in An-
spruch genommen und zu Beschlüssen geführt haben, welche von ihr nur mit lebhafter An-
erkennung und aufrichtigem Danke begrüsst werden können.

« Die Conferenz wolle sich überzeugt halten, dass die Staatsregierung und ihre Or-
gane sich der Erfüllung der ihr vertrauensvoll übertragenen Aufgaben mit grösster Bereit-
willigkeit unterziehen werden.

« Ew. Hochwohlgeboren ersuche ich ergebenst, der Gonferenz von dem Inhalte dieses
Schreibens gefälligst Kenntniss zu geben und ihren Mitgliedern meinen lebhaften Wunsch
auszusprechen, dass nur angenehme Erinnerungen an den hiesigen Aufenthalt sie in ihre
Heimath begleiten mögen.

« gez. VON GOSSLER. »

Der Prasident, indem er die Reihe des Landes-Berichte wieder aufnimmt, fragt zu-
nächst den Delegirten von Rumänien, ob er der Versammlung einen Bericht über die in
seinem Lande ausgeführten Arbeiten vorlegen wolle.

Der General Falcoiano erwiedert, dass er einen solchen ausführlichen Bericht nicht
vorbereitet habe, da er zum ersten Male an einer General-Conferenz theilnehme, und daher
die in denselben obwaltenden Gebräuche nicht gekannt habe. Uebrigens hätte er der schon
in der vorigen Sitzung gemachten Mittheilung nur wenig hinzuzufügen. Was die astrono-
mischen Ortsbestimmungen anlange, sei in Rumänien nur die Längen-Differenz zwischen
Kronstadt und Bucarest, gemeinsam mit dem militär-geographischen Institute in Wien
ausgeführt worden. Ueber diese Bestimmung habe Herr von Kalmär bereits berichtet. Be-
treff der eigentlich geodätischen Arbeiten habe er schon erwähnt, dass er die Karte des
Dreiecks-Netzes erster Ordnung, welches seit der letzten Conferenz ausgeführt sei, auf dem
Tische der Versammlung niedergelegt habe.

Herr von Struve erhält das Wort zu einem ausführlichen Berichte über die in Russ-
land ausgeführten Arbeiten. (Siehe Anhang.)

In der Einleitung zu seinem Berichte hat Herr von Struve, zum Theil zur Recht-

 

 

 

 

  

 

 

TIES

 

een meer
ee

ere

raie
es i
