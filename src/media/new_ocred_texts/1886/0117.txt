 

a

7

SRT ETRE

 

05

CINQUIEME SEANCE

a

Jer Novembre 1886.

Présidence de M. le professeur Ferster.
Sont présents :

MM. D’Avila, Auwers, von Bauernfeind, van de Sande Bakhuyzen, Faleniano,
Faye, Fearnley, Ferrero, Folie, Golz, Hartel, Helmert, von Helmholtz, Hennequin, Hirsch,
Ibanez, von Kalmär, Kronecker, Nagel, Nell, von Oppolzer, Riimker, Rosén, Schreiber,
Siemens, Stebnitzky, von Sterneck, von Struve, Tisserand, Weierstrass, Zachariae, von Zech.

La séance est ouverte à 10 heures 15 minutes.

Le procès-verbal est lu, puis résumé en français par M. le Secréluire ; il est adopté
après une observation faite par M. le Président.

M. Faye remercie M. le Secrétaire pour la traduction du procès-verbal en français ; il
croit devoir toutefois exprimer le désir que les procès-verbaux soient reproduits à l'avenir
dans les deux langues, comme c'était autrefois l'usage et que cette règle soit suivie une fois
pour toutes, puisqu'il se trouve au sein de l Assemblée plusieurs membres qui ne sont pas a
même de comprendre les comptes-rendus des séances en langue allemande.

Le Président répond qu’en raison de la fréquence des séances qui se sont succédé
d’une manière exceptionnelle, il a été matériellement impossible à M. le Secrélaire de rédi-
ger, dans l’intervalle des séances, les procès-verbaux dans les deux langues ; mais il va de
soi qu'à l'avenir comme par le passé, ils seront rédigés et publiés en allemand et en français.

M. Faye se déclare satisfait de cette explication.

M. le Président désire avant tout revenir sur la question de la représentation des Délé-
gués au sein de la Commission permanente par des suppléants, question soulevée par M. de
Struve dans une des séances précédentes et discutée peut-être un peu trop à la hâte. Il est en
effet, extrêmement important que l’on prenne toutes les mesures nécessaires pour que la
Commission permanente soit toujours réunie aussi complète que possible, afin qu’elle puisse
prendre des décisions valables. D'autre part, le principe paraît être généralement approuvé

 
