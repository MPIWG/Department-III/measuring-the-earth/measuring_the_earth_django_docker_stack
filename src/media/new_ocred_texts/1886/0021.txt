ab

A EN pm

|
|

 

 

XV

l'un des Etats intéressés, et de faire le nécessaire, afin de garantir une coopération plus
étroite et plus fructueuse, entre cel établissement national et la Commission permanente de
l'Association géodésique internationale.

Dans ce dernier sens, on a fait valoir également, comme un des défauts essentiels
de l’ancienne organisation, que la Commission permanente n'avait pas été dotée des moyens
financiers les plus indispensables, de telle sorte que, pour la publication de ses propres
comptes-rendus, par exemple, elle devait recourir aux moyens du Bureau central, qu’elle était
censée diriger.

En effet, la Commission permanente, placée à la tête d’une aussi grande entreprise
scientifique, quel que soit, du reste, le dévouement désintéressé et digne d’éloges avec lequel
ses Membres ont fourni une grande somme de travail, doit évidemment se trouver pénible-
ment arrêtée dans ses efforts, quand elle est privée entièrement des ressources indispensa-
bles à la rédaction, à la publication des résultats de ses recherches, et à la rénumération des
travaux auxiliaires dont on ne saurait se passer, précisément lorsqu'il s’agit de réunir et de
contrôler de vastes études scientifiques.

Lors même que le Bureau central se prêterait volontiers à ces services, ce concours,
d’après la nature des choses, ne pourrait suffire dans bien des cas, ne serait-ce qu’en raison
des grandes distances des différents centres d’études; car souvent il s’agira de se procurer
sur place des aides, observateurs ou calculateurs.

Enfin, on a fait observer que, dans l’intérêt de l'œuvre même, la Commission per-
manente pourrait se trouver quelquefois dans le cas de provoquer ou d’aider certaines re-
cherches scientifiques, que l’on ne saurait non plus exiger des Commissions géodésiques na-
tionales, et pour lesquelles le concours du Bureau central ne pourrait suffire. On a pensé
surtout à des études théoriques, expérimentales ou de calcul, pour lesquelles 1l s’est formé
des spécialistes dans l’un ou l’autre pays de l’Association, ou qui se trouveraient favorisées
d’une manière particulière par les conditions naturelles d’une certaine région.

Pour tous ces motifs, des hommes compétents se sont convaincus que, tout en main-
tenant essentiellement les anciennes attributions du Bureau central et son lien avec l’Institut
ocodésique à Berlin, il serait cependant indispensable, dans lPintérêt de l’œuvre poursuivie,
d'assurer un pelit budget à la Commission permanente, afin qu’elle ful en état de diriger
plus efficacement le Bureau central et d'imprimer plus de vie et de développement à toute
l’entreprise.

Appréciant parfaitement le bien fondé de ces points de vue, le Gouvernement royal
croit devoir recommander instamment l’adoption des propositions concernant l’organisation
future de l'Association géodésique internationale, contenues dans le projet.

Peut-être sera-t-1l utile d'ajouter encore quelques observations sur des dispositions
spéciales du projet, en les rapprochant des anciens Statuts de 1864.

 
