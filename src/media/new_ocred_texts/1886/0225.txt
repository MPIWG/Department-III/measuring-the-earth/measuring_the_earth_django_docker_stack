201

Bei den ersten acht Längenbestimmungen wurden die Passage-Instrumente von
Herbst verwendet, deren Construction die bei uns übliche Anwendung der Döllen’schen Me-
thode der Zeitbestimmung im Verticale des Polarsternes zulässt. Bei den drei Letzten wurden
Verticalkreise von Repsold verwendet und Zeitbestimmungen nach Zingerscher Methode
(Durchgangsbeobachtungen auf gleichen Höhen) ausgeführt. Die Längendifferenzen Batum-
Nikolaiew und Taschkent-Buchara sind aus zwölf, die beiden letzten aus drei, und alle
Uebrigen aus sechs Beobachtungsabenden abgeleitet. Die persönliche Gleichung ist aus den
Resultaten entweder durch den Beobachterwechsel oder durch unmittelbare Bestimmung (10
und 11) eliminirt worden.

b) Zugleich mit Längenbestimmungen sind auch Breitenmessungen mit dem Vertical-
Kreise von Repsold in folgenden Puncten gemacht worden :

 

 

Geogr. Breite. Beobachter. . | Titel d. publication.

 

 

. Tschenstochau. . . . 50.48 51,2 - 014 Polanowaki \ Sapiski W. T. O. Bd. LXI.
Wlozlawsk .. . . 5229256 04% 1881. | Vierteljahresschr. d. A. G.

Nowoalexandria .. .| 5125 0,1 +0,14 IE nel | HAL Eee,

‚Buchka®a. . ....., 3946 32,250 14 Pomeranzeff u.

. Yalutorowsk. > Salesski.
ischm <. Noch nicht definitiv | Ob. Lt. Schmidt.

le cd  . berechnet.

Æ Ww bo

 

I © ©

 

 

c) Chronometer-Expeditionen.

Bei Orts-Bestimmungen zur Orientirung und Feststellung der in unsern asia-
tischen Besitzungen (Sibirien, Turkestan und Transkaspisches Gebiet) gegenwärtig ge-
führten topographischen Aufnahmen sind, wegen der örtlichen Verhältnisse, die Triangu-
lirungs-Arbeiten durch Ghronometer-Expeditionen ersetzt. Nur in manchen mehr bevölkerten
und cultivirten Gegenden werden die Aufnahmen auf Triangulationspuncten begründet. Bei
den Chronometer-Expeditionen gebrauchen wir gewöhnlich einen Repsold’schen Vertical-
‚ Kreis und 4-6 Box-Chronometer. Bei den Expeditionen aber die mit geographischen
Zwecken in noch wenig bekannten Gegenden unternommen werden, begnügen wir uns mit
dem Prismen-Kreise von Pistor und Martens und mit einigen Taschenchronometern. Unter
den in den letzten zwei Berichtsjahren ausgeführten Expeditionen sind folgende zu erwähnen :

1. Zwei Chronometer-Expeditionen von Ob.-Lt. Schmidt (1884 und 1885) im Ge-
biete von Akmolinsk (west. Sibirien), durch welche die geographische Lage von 21 Punkten
bestimmt ist. Publiciert in Bd. XLI, S. W. T. 0.

2. Chronometrische Expedition in dem Transkaspischen Gebiete von Cap. Gedeonoff;
bestimmt 47 Punkte zwischen Kisil-Arwat und Chiwa. Publiciert in S. W. T. 0. Bd. XL.
26

 
