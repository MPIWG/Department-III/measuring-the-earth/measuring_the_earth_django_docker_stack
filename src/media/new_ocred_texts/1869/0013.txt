“1 A TN SP

im

a AT

DENT RER.

=
=
3
=
m
z
S
EI

ce 0

des Central-Büreaus, die Beobachtungen zur Längen-Bestimmung zwischen Bonn und Leiden
mit Mai dieses Jahres anfangen werden und dass sich, unmittelbar nach deren Vollendung,
auch eine Bestimmung der Länge des einfachen Secunden-Pendels an der Sternwarte in Leiden
ausführen lassen wird.

Die an der Sternwarte in Leiden unternommene Bestimmung der Declinationen der
Fixsterne, welche bei der europäischen Gradmessung zu Breiten- Bestimmungen angewandt
sind, ist ihrer Vollendung nahe. In meinem Berichte des vergangenen Jahres habe ich den
Plan bekannt gemacht, welcher, in Uebereinstimmung mit der Sternwarte in Leipzig, bei dieser
Arbeit befolgt ist. Nach diesem Plane waren für die 202 Gradmessungs-Sterne 3232 Meridian-
Durchgänge zu beobachten. Davon kamen 1376 vor unter den Beobachtungen von Fundamen-
tal-Sternen, welche in den Jahren 1864—1868 von den Herren Dr. Kam und Dr. van Henne-
keler angestellt und im ersten Bande der Annalen der Sternwarte in Leiden veröffentlicht
sind. Sobald bei den Beobachtungen für die Fundamental-Sterne nur noch Lücken auszufüllen
waren, nämlich im Sept. 1867, haben die Beobachtungen, welehe ausschliesslich für die Grad-
messungs-Sterne anzustellen waren, begonnen und nachdem die Fundamental-Stern-Beobach-
tungen, im Juni 1868, als geschlossen betrachtet wurden, ist an der Leidner Sternwarte kaum '
etwas anderes als die Beobachtungen für die Gradmessungs-Sterne ausgeführt. Als im Febr.
1869 Herr Dr. Kampf, an Herın Dr. van Hennekeler’s Stelle, an der Leidner Sternwarte
trat, hatten die Herren Dr. Kam und Dr. van Hennekeler 742 der noch fehlenden 1856
Meridian-Durchgänge beöbachtet. Herr Dr. Kampf, der unmittelbar alle Beobachtungen am
Meridiankreise auf sich genommen hatte, hat bei seinem Abgang, am 20. Dec. 1869, 915 von
ihm beobachtete Meridian-Durchgänge hinterlassen, so dass noch 199 Meridian-Durchgänge zur
Beobachtung für Herrn Dr. Valentiner blieben, welcher, nach Herrn Dr. Kam’s Abgang,
am 1. Juli 1869 die Stelle als Observator an der Leidner Sternwarte antrat. Herr Dr. Va-
lentiner hat bis heute 177 der noch fehlenden Meridian-Durchgänge beobachtet, weshalb
nur noch 22 zu beobachten übrig bleiben. Eine schlechte Witterung hat, besonders in den
letzten Monaten, die Fortsetzung der Beobachtungen sehr gestört.

Mit der Reduetion der hier angestellten Beobachtungen fand ich immer so grosse
Schwierigkeiten, dass ich selbst gezwungen war, die Beobachtungen der Fundamental-Sterne
fast gänzlich unreducirt zu veröffentlichen. Herr Dr. Kam wurde, vom Febr. 1369 an, von
allen Beobachtungen am Meridiankreise entbunden, damit er seine, für die Gradmessungs-
Sterne angestellten Beobachtungen würde redueiren können, aber als er am 1. Mai 1869 die
Sternwarte verliess, hatte er damit noch nicht einmal einen Anfang gemacht. Herr Dr. Kampf
reducirte die 742, von den Herren Dr. Kam und Dr. van Hennekeler, und die 915 von
ihm selbst beobachteten Zenithdistanzen auf 0 Jan. 1870. Herr Dr. Valentiner hat die 1576
Zenithdistanzen der Gradmessungs-Sterne, welche zu den früher beobachteten Fundamental-
Sternen gehören, so wie seine eigenen Beobachtungen, auf 0 Jan. 1870 reducirt.

Ich habe schon früher gezeigt, dass es nicht möglich ist, mit dem Leidner Meridian-
kreise die erforderliche Genauigkeit zu erreichen, ohne die Bestimmung der Fehler aller ein-

 
