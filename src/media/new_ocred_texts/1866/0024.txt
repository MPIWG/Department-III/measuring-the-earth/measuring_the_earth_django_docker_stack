 

ai —

und Norden des Beobachtungspunktes liegen beziehungsweise 26,73, 27,20, 27,56 und 26,87
Fuss Oldenb. unter jener Horizontale. (1 Oldenb. Fuss = 0,9427297 Rhein. Fuss.)

Behufs der anzustellenden Beobachtungen wurde ein aus starken Pfählen und Brettern
zusammengesetztes, den Beobachtungspfeiler einschliessendes, 6eckiges, mit den erforderlichen
verschliessbaren Oeffnungen (Luken) versehenes kleines Observatorium errichtet, und um zu
vermeiden, dass beim Beobachten das Fundament des Beobachtungspfeilers unmittelbar berührt
werde, wurden an die 6 eingegrabenen Pfosten des Observatoriums starke Bohlen hochkant
befestigt, und auf dieselben der Fussboden aus doppelten Brettern gelegt.

Bei den späteren Beobachtungen von den Dreieckpunkten Varel und Jever nach
Dangast hin war für solche Fälle, wo Heliotropen keine Anwendung finden konnten, auf
Station Dangast genau über der Mitte des Beobachtungspfeilers ein 13füssiger Flaggenstock
aufgesteckt, respective an dem Observatorium befestigt, welcher ein 23 Fuss breites und 5 Fuss
hohes, nach oben in eine Spitze auslaufendes Brett als Visirobjeet trug. In der Mitte dieses
Brettes war ein 104 Zoll breiter weisser Streifen gemalt, während die beiden Seitenstreifen
oben roth und unten schwarz angestrichen worden waren. Das Brett war drehbar und wurde
selbstredend jedesmal rechtwinkelig gegen die Visirlinie gerichtet.

Betrefts der Lage des Beobachtungspfeilers bei Dangast in Beziehung auf die nächste
Umgegend füge ich noch die anliegende Zeichnung A., und zum bessern Verständniss des
Aufbaues desselben die Zeichnung B. bei.

Für die Ausführung der Beobachtungen, sowohl der astronomischen als der terrestri-
schen, war schon früher durch gefällige Vermittlung Sr. Excellenz des Herrn General-Lieutenant
Baeyer mit gefälliger Zustimmung des Herrn Professor Förster der erste Observator an der
Königlichen Sternwarte zu Berlin, Herr Dr. Tietjen, gewonnen worden, und traf derselbe am
20. April vorigen Jahres mit den erforderlichen Instrumenten u. s. w. in Oldenburg ein.

Nachdem die nöthigen Vorbereitungen beendigt waren, wurden die Sternbeobachtun-
gen und erforderlichen terrestrischen Winkelmessungen auf Station Dangast am 25. April durch
Herrn Dr. Tietjen mit Assistenz des Geometers Herrn Wiedfeldt begonnen und bis zum
24. Mai fortgeführt.

Sodan fanden durch die genannten beiden Herren
vom 25. Mai bis 4. Juni auf dem luther. Thurm zu Varel
und schliesslich
vom 2. bis 16. Juni auf dem Schlossthurm zu Jever
terrestrische Winkelmessungen Statt.

Leider waren fast während der ganzen oben bemerkten Zeit die Witterungsverhält-
nisse und andere Umstände den Beobachtungen wenig günstig, insbesondere den terrestrischen
Winkelmessungen die an der Küste nicht seltenen Nebel und aufsteigenden Dünste, sowie der

im Frühjahre in biesiger Gegend häufig vorkommende Moorrauch sehr hinderlich.

Die zur unmittelbaren Bestimmung der Polhöhe von Dangast von Herrn Dr. Tietjen
mit grosser Sorgfalt angestellten Sternbeobachtungen sind in solchem Umfange und mit

|
|

 

 
