en nn |

i AT

a ie oe =

a

Aus der Vergleichung der einzelnen aus jedem Sterne folgenden Resultate mit den
zugehörigen Mitteln ergiebt sich für dieses Jahr der wahrscheinliche Fehler Einer
Beobachtung

am Passageinstrumente = + 0"3497 aus 291 Beobachtungen
am Universalinstrumente = + 0"5535 - 276 -
woraus das Verhältniss der Gewichte wie 1:0,4 folgen würde. Ich halte es jedoch nicht für
gerechtfertigt, das Gewicht der am Universalinstrumente beobachteten Sterne behufs ihrer
Verbindung mit den im ersten Vertical beobachteten in diesem Verhältnisse zu reduciren, da
hierdurch der Einfluss der ersteren rücksichtlich ihrer Declination allzu sehr zu Gunsten der
letzteren herabgedrückt würde. Die Kleinheit des wahrscheinlichen Fehlers einer Beobachtung
am Starke'schen Passageinstrumente ist die Folge der Schärfe in der Auffassung der Antritte,
welche das vorzügliche optische Bild bei 120maliger Vergrösserung gestattet. Ich habe auch
die mehr als nöthig grosse Anzahl von Beobachtungen nur aus dem Grunde gemacht, um
überhaupt die Leistungsfähigkeit des Instrumentes kennen zu lernen, da mir in Wien selbst,
in Ermangelung einer geeigneten Aufstellung, hierzu die Gelegenheit fehlte. Bei der zufälligen
nahe vollständigen Uebereinstimmung der an beiden Instrumenten erhaltenen Resultate ist
übrigens im vorliegenden Falle die Frage ohne praktische Bedeutung. Ich lasse demnach
die Gewichte unverändert und es ergeben sich sofort aus den einzelnen Sternen folgende
Resultate für die Polhöhe des Dreieckspunktes:
& Aurigae 49° 1’ 17623 Gewicht 97

e Urs. maj. 16"821 - 64
a Cygni 17467 - 130
a Urs. min. 17837 - 60
bp aa 17/029 - 40
B Cephei 16184 - 44
6 Tauri 17244 : 39
æ Coronae 17/7906 - 40
a Aquilae 1.56 - 13
a Orionis 17308 - 24
a Serpentis 18/1007 - 16
567

aus welchen als wahrscheinlichster Werth hervorgeht:
Polhôühe von Wétrnik A = 49° I! 17/358 + 01027.

Azimuth.

Die Richtung, deren Azimuth ich bestimmte, war jene nach dem Dreieckspunkte
Markstein (Entfernung = 23543 W. Klafter). Die Signalisirung geschah mittelst Heliotrop,
der auf Markstein centrisch aufgestellt war. Die Resultate sind folgende:

10 *

 
