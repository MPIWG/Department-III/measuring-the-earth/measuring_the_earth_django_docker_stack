 

RS

Aus den verbesserten astronomischen und geodätischen Bestimmungen hat ferner das
Pariser Observatorium eme neue Bestimmung der Figur der Erde hergeleitet und folgendes
Resultat gefunden:

1

die Abplattung — DFE
Od),c

den Quadranten des Meridians = 10001334”,
die halbe grosse Axe — 6378204”,
Die sehr übereinstimmenden Werthe von Struve und der Ordnance Survey geben im Mittel:
1
294,31 ’
Quadrant = 10001893",
halbe grosse Axe = 6378233".

Das Mittel aus beiden Abplattungen nähert sich sehr dem Verhältniss der Schwungkraft zur

Abplattung =

Schwere unter dem Aequator, und bestitigt die Vermuthung, die Baeyer in „Figur und
Grösse der Erde“ auf Seite 44 ausgesprochen hat.

Durch die vorstehenden Untersuchungen wurde Herr Villarceau auf ein neues Theorem
geführt, welches wir seiner erfolgreichen praktischen Anwendung wegen hier in extenso

mittheilen.

De leffet des Attractions locales sur les longitudes et les azimuts; applications d’un
nouveau Théorème à l'étude de la figure de la Terre;
par M. Yvon Villarceau.

» Quelle que soit la figure du sphéroïde terrestre, par un point M de sa surface menons
une parallèle à l'axe du monde et, par cette parallèle, un plan de direction encore indéterminée
et assujettie seulement à faire un petit angle avec le méridien astronomique du lieu; dans ce
plan, et par le point M, menons une droite de direction indéterminée et assujettie seulement
à faire un petit angle avec la direction du zénith astronomique. Nous nommerons le plan
ainsi défini plan méridien auxiliaire, et la droite zénith auxiliaire. Soit B un signal géodésique
observé du lieu M, Z son azimut par rapport au méridien auxiliaire, et compté du sud à l’ouest.

„Si nous construisons une sphère ayant son centre au point M, les trois plans menés
par le zénith auxiliaire, par le point B et la droite parallèle à l'axe du monde (dont nous
considérons seulement la partie boréale), détermineront un triangle sphérique. Soient A, B,
C les trois angles de ce triangle, répondant respectivement aux trois points où les droites
percent la sphère; a, b, c les trois côtés opposés; ces côtés seront respectivement égaux à la
distance polaire du signal B, à la colatitude du lieu et à la distance zénithale du signal (les
deux derniers se rapportant bien entendu au zénith auxiliaire). On aura, dans le triangle ABC,

cotAsinC + cosbcosC — cotasinb — 0.

» Considérons actuellement la vraie direction du zénith, telle que la déterminent les

attractions locales et autres, et formons un nouveau triangle sphérique au moyen de cette

ä
4
j
3

suis

as mel ble ou, dal a m ian

 

|
|
|

 
