A TNT SP

MUR

iR

ij!

=
x
z
x
S
>

 

ln eu

Seite in den Hintergrund trat und das militairische Bedürfniss der Anfertigung der Karte
vorherrschend wurde. Die Ingenieur-Geographen eilten mit der Berechnung der Azimuthe,
Längen und Breiten der Dreieckspunkte, und so schlichen sich Mängel ein die fast durch-
gängig eine Üorrektion der geographischen Positionen und der Azimuthe in dem Mémorial
du Dépôt de la Guerre nothwendig machen, wenn man sie zu wissenschaftlichen Zwecken ver-
wenden will*). Die bedeutendsten Geodäten des französischen Generalstabes haben wieder-
holt ihr Bedauern darüber ausgesprochen, dass diese Mängel bei so wichtigen Arbeiten nicht
vermieden worden sind, und haben in einzelnen Theilen des Netzes, die Rechnungen vervoll-
ständigt und Fehler verbessert, wie Colonel Hossard in der Kette im Parallel von Paris und
in neuerer Zeit Colonel Levret in der Delambre’schen Meridian-Kette zwischen Paris und Dün-
kirchen, bei Gelegenheit der 1861 ausgeführten Verbindung zwischen den französischen und
englischen Dreiecken.

Ausserdem hatte das Depöt de la Guerre auf zahlreichen Punkten Polhöhen und
Azimuthe bestimmen lassen und zwei Längengradmessungen vermittelst Pulverblitzen, die eine
zwischen Brest, Strasburg und München, die andere unter dem mittleren Parallel zwischen
Marennes (Mündung der Gironde) und Fiume ausführen lassen. Mit der Verwerthung dieser
Materialien zur Bestimmung der Figur der Erde beschäftigten sich Puissant und Colonel
Broussaud. Der erste fand aus einer Combination der Polhöhen und der Azimuthe, dass die
beiden Theile von Frankreich, die durch den Meridian von Paris getrennt werden, zwei ver-
schiedenen Ellipsoiden angehören, von denen das eine an den Polen abgeplattet, das andere
in die Länge gezogen (allongé) sei, oder dass mindestens doch die Figur der Erde in Frank-
reich die grössten Unregelmässigkeiten darbiete. Der andere bearbeitete die Längengrad-
messung unter dem mittleren Parallel und fand, dass die Länge eines Grades an verschiedenen
Stellen desselben sehr beträchtliche Differenzen zeigte, die bis jetzt unerklärt geblieben sind.

Obgleich diese Arbeiten keine befriedigende Resultate geliefert haben und vielmehr
Alles in Frage zu stellen schienen, so haben sie doch das grosse Verdienst die Aufmerksam-
keit auf diesen Gegenstand gelenkt zu haben. Es würde wohl noch lange gedauert haben,
ehe die Widersprüche zwischen den Messungs-Resultaten und der Theorie aufgelöst worden
wären, wenn nicht die herrliche Erfindung der elektrischen Telegraphie die Mittel zu sehr
genauen Längenbestimmungen geliefert hätte. So wie einmal die Sicherheit der astronomischen
Bestimmungen ausser Zweifel gesetzt war, vereinfachte sich die Frage dahin, dass die Ab-
weichungen entweder Fehlern in einzelnen T'heilen des geodätischen Netzes oder Local-

Attractionen zuzuschreiben seien.

*) Dies ist überall der Fall gewesen und ist auch gegenwärtig noch der Fall, wo die Leitung einer
Landesvermessung lediglich in die Hände einer einzigen Behörde gelegt ist. Die Tendenz des Kartenmachens
wird immer vorwalten, weil die wissenschaftliche Seite in der Regel unzureichend vertreten ist. Aus diesem
Grunde ist bei allen in neuerer Zeit und gut organisirten Landesvermessungen wenigstens doch die Haupttriangu-
lation einer wissenschaftlichen Commission übertragen, wie z. B. in Spanien, Italien, der Schweiz und im König-
reich Sachsen.
