 

= mn —

technischen und wissenschaftlichen Behandlung der geodätischen Arbeiten gemacht worden
sind, als auch wegen der Unvollständigkeit der über jene Messung vorliegenden Original-
Documente für die Zwecke der Gradmessung nicht ausreichen, sondern neben den jedenfalls
erforderlichen geographischen Ortsbestimmungen auch eine neue Basismessung und Triangu-
lirung geboten sei, und haben in Betracht der Zeit, welche über der Beschaffung der Instru-
mente voraussichtlich verfliessen dürfte, die Inangriffnahme der Arbeiten auf dem Terrain für
den Sommer 1868 in Aussicht genommen.

Ein definitiver Bescheid ist uns auf unsere Eingabe noch nicht zu Theil geworden,
wohl aber auf die Bitte um eine vorläufige Instruction zum Zwecke der gegenwärtigen
Berichterstattung die Antwort zugekommen, dass die Königliche Regierung an die Regierungen
der benachbarten Staaten Bayern und Baden die Anfrage gerichtet habe, was dort aus Staats-
mitteln für den fraglichen Zweck verwendet werde, da eine genaue Kenntniss hiervon für die
erste Eintschliessung, sowie eventuell für die weitere Behandlung der Sache von Werth zu
sein scheine.

Sobald eine definitive Entscheidung erfolgt, werden wir nicht ermangeln, Ew. ... .
von derselben in Kenntniss zu setzen,

Stuttgart, den 30. März 1867.

Prof. Dr. C. W. Baur. Prof. Dr. Schoder. Prof. Dr. Zech.

 

|
4
i

 
