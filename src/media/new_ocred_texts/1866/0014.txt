 

ae.

Arbeiten für die Gradmessung auf sich genommen hat, hat mich zu dem Berichte ermächtigt,
dass der, bei den Herren Pistor und Martins in Berlin bestellte Theodolid abgeliefert ist und
dass er, im Herbste des vergangenen Jahres, eine Vorbereitungsreise in unserem Lande unter-
nommen hat, mit seinem Gehülfen für die Gradmessung, Herrn Doctor und Artillerielieutenant
A. L. Boeck, welcher sich jetzt an der Leidner Sternwarte in Beobachtungen übt. In Ueber-
einstimmung mit den Ansichten des Central-Bureau’s beabsichtigt Herr Dr. Stamkart eine
doppelte Kette von Dreiecken auszumessen, von Leiden aus, nordwärts bis zu Jever in Oldenburg
und südwärts bis zu Peer in Belgien und dabei, so viel möglich, an Krayenhoff’s Stationen
die Winkelmessungen anzustellen. Bei dieser Reise des vergangenen Jahres bemerkte Herr
Dr. Stamkart aber, dass einige Thürme, wo Krayenhoff gemessen hat, nicht mehr da sind und
dass an mehreren Stationen Krayenhoff’s die früher beobachteten Gegenstände jetzt von Bäumen
verdeckt werden. Besonders wichtig für die Triangulation wäre die Insel Uck im Zuidersee,
aber diese Insel wird sich von der Küste aus nicht beobachten lassen, ohne die Errichtung
eines Signals von ohngefähr 30 Meter Höhe. Herr Dr. Stamkart hofft im nächsten Sommer
die Messungen anfangen zu können und dazu braucht er nur die erforderliche Geldsumme,
deren Aufsetzung wahrscheinlich, aber, in diesem Augenblicke, noch nicht gesichert ist.

Den Entschliissen der Conferenz des Jahres 1864 zufolge soll der Längen-Unterschied
von Leiden mit Bonn, Brüssel und Utrecht bestimmt werden. Die dazu unsererseits erforder-
liche Geldsumme ist schon längst zugesagt und von der Regierung in das Budget des Staates
eingerückt, aber, besonderen Umständen zufolge, ist das Staats-Budget für 1867 bei unserer
Volksvertretung noch nicht in Discussion gekommen und damit ist es noch unsicher, ob uns
die beabsichtigten Arbeiten für die Gradmessung, in diesem Jahre, möglich sein werden. Ich
habe die Herren Argelander, Quetelet und Hoek bereits gebeten mir gütigst mitzutheilen, ob
sie sich in diesem Jahre zu einer Längenbestimmung zwischen ihren Sternwarten und
Leiden verbinden können, falls mir die dazu erforderlichen Geldmittel zu Gebote kommen
werden. Herr Prof. Quetelet hat dessen hochgeschätzte Mitwirkung, mit der verbindlichsten
Gefälligkeit, versprochen. Herr Prof. Argelander konnte sich noch nicht gänzlich verbinden,
aber liess mich keinesweges ohne Hoffnung. Der Längenbestimmung zwischen Leiden und
Utrecht aber setzen sich so grosse Schwierigkeiten entgegen, dass ich befürchte, diese Bestim-
mung gänzlich aufgeben zu müssen.

Das Central-Bureau hat, in Uebereinstimmung mit Herrn Freiherrn P. A. von Schrenck,
Grossherzoglichem Ober-Kammerrath in Oldenburg, den Wunsch ausgesprochen, dass auch
der Längenunterschied zwischen Leiden und Dangast in Oldenburg bestimmt werde, und dass
dazu die Gelegenheit benutzt werde, welche sich, kurz nach Ostern, darbieten wird, bei der
Längenbestimmung zwischen Dangast und Göttingen. Ich will gern an dieser Bestimmung

mitarbeiten, aber, obschon das Central-Bureau eine sich mir darbietende grosse Schwierigkeit

beseitigt hat, bedarf ich die Vermittelung und Hülfsleistung unseres ausgezeichneten Tele-
graphen-Ingenieurs, Herrn E. Wenckebach. Herr Wenckebach kann mir aber keine Hülfe

leisten, ohne die Ermächtigung Seiner Exc. des Ministers des Innern. Ich babe den Herrn

3

ach Ja idl mn

 

un asnanaisnn masse bid

 
