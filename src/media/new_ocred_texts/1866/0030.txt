 

m.

Punkte ist gänzlich verloren gegangen, und wo man für angemessen gehalten hat nachträg-
lich Signale zu errichten, da ist eine Identität mit den Gaussischen Punkten keineswegs ab-
solut zu verbürgen. Das ganze Dreieckssystem der Hannoverschen Landesvermessung schwebt
also gleichsam in der Luft, und steht nur in Punkten dritter und vierter Ordnung, die aus
den Hauptpunkten festgelegt sind, mit der Wirklichkeit im Zusammenhange. Soll demnach
ein exacter Anschluss an nachbarliche Dreiecksketten möglich werden, so ist — selbst schon
um der topographischen Aufnahmen willen — eine neue Triangulirung unumgänglich erfor-
derlich. Glücklicher Weise ist übrigens die also vorzunehmende Arbeit nur von mässigem
Umfange; denn es werden nur bestimmte, dem noch zu entwerfenden Systeme des Gesammt-
gebiets von Norddeutschland entsprechende Dreiecksketten neu zu messen sein, es wird die
Neumessung sich beinahe nur auf Dreiecke erster Ordnung zu beschränken haben, und end-
lich fällt die Vorarbeit der Auswahl der Dreiecke weg, da man völlig den Gaussischen
Dreiecken wird folgen können. Aber die Ausführung fordert Kräfte ersten Ranges, wenn
sie die Gaussischen Dreiecke würdig ersetzen will.

Schliesslich bleibt noch die betrübende Mittheilung zu machen, dass die mitteleuro-
päische Gradmessung den Verlust eines eben so liebenswürdigen wie eifrigen Mitarbeiters zu
beklagen hat. Herr Hauptmann Grumbrecht vom vormaligen Hannoverschen Generalstabe
hat der militärischen Laufbahn entsagt und sich einem neuen Berufe zugewandt, der ihn für

immer unserer Thitigkeit entfremdet.

D. Auszug aus dem Berichte der Herren Dr. Börsch und Kaupert über die im
Jahre 1866 ausgeführten geodätischen Arbeiten für die mitteleuropäische
Gradmessung im vormaligen Kurfürstenthum Hessen.

Das beabsichtigte geometrische Nivellement erster Ordnung in Kurhessen konnte im
Jahre 1866 wegen der eingetretenen politischen Verhältnisse nicht begonnen werden und es
kann daher nur das trigonometrische Nivellement erster Ordnung vorgelegt werden. Dieses
erstreckt sich über das Hauptland von Kurhessen und die von demselben isolirt gelegene
Grafschaft Schaumburg, ist nicht nach einem vorher entworfenen Netze im Zusammenhange
über das ganze Land ausgeführt worden, sondern allmälig von 1841 bis 1853 entstanden, wie
die Triangulation zweiter Ordnung von Nord nach Süd vorschritt und es das jedesmalige
Bedürfniss erheischte, weshalb schliesslich eine Gesammtausgleichung vorgenommen worden
ist. Eine Folge dieses allmäligen Vorschreitens war aber, dass nicht alle Höhendifferenzen
aus gegenseitigen und gleichzeitigen Höhenmessungen bestimmt wurden, sondern dass auch
gegenseitige, ungleichzeitige Messungen in das Höhennetz erster Ordnung aufgenommen
werden mussten, indem diese Messungen schon ausgeführt waren, während erst später die be-
treffenden Punkte in das Hauptnetz aufgenommen wurden, zu einer Ausscheiduug der unter-

geordneten Messungen aber nicht geschritten werden konnte, ohne das nöthige Ineinander-

|
|

 

 
