NT N IV |

ya iu

a A

au, Ab 0

direction et de celles du pöle et du point B. Soient alors A’, B’, C’, a‘, b', c' les angles et
les cétés de ce nouveau triangle. Les deux triangles n’auront de commun que le cotéa= a.
Pour déterminer les différences des quantités homologues dans les deux triangles, il suffira
de différentier l'équation précédente en y supposant a constant. Fffectuant la différentiation

et ayant recours à des relations connues, on trouve
ÊA + (cosb— cotcsinbcos A)OC +cotcsinA db = 0.
Or, le point B étant censé à l'horizon du lieu M, on a c' — 90 degrés et cote! —0; d'où,
en négligeant les quantités du deuxième ordre,
(.) OA -Pcohoe —

Pour nous conformer aux usages géodésiques, nous remplacerons les azimuts par leurs
suppléments, ce qui donnera OA = A!— A =—(Z!—Z). Si nous comptons les longitudes
R et ® du méridien auxiliaire et du méridien astronomique dans le sens de l’est à l’ouest,
nous avons C+8— C'H®, d'où ÊC = C'—C— —(8 —R); enfin, b étant égal au complément
de la latitude L du zénith auxiliaire, nous pouvons prendre cosb = sinL = siml/, en nous
tenant au même degré d’approximation. Moyennant la substitution de ces valeurs, l'équation
(1.) devient

(2) .Z=Z2+Esml(@e®) = 0
relation qui a nécessairement lieu, quels que soient les attractions locales et le plan méridien
auxiliaire considéré, pourvu que l'écart angulaire entre ce plan et le méridien astronomique

reste un petit angle.

Application à la démonstration d'un théorème de Laplace relatif aux sphéroïdes peu
$

différents de la sphère.

Soit une ligne géodésique issue d’un lieu dont la longitude et la latitude sont £, et
L,, et menée suivant la direction australe du méridien de ce lieu: il est clair que si la Terre
est un sphéroïde de révolution autour de son axe de figure, la ligne géodésique sera contenue
tout entière dans le plan méridien passant par le lieu de départ; mais si le sphéroide nest
pas de révolution, la ligne géodésique s'écartera progressivement de ce plan. En un point
de latitude L, la direction de la ligne géodésique ne coïncidera pas non plus avec le méridien
astronomique de ce lieu. Si nous prenons, pour direction du signal B, celle du prolongement
austral de la ligne géodésique, l’azimut astronomique de B sera Z’. Maintenant, considérons
l'ensemble des points de la ligne géodésique compris entre L, et L, et soit, en un point de
cette ligne, & la longitude d'un plan méridien auxiliaire assujetti à être tangent à la ligne

géodésique en ce point. Au point L,, £ se confondra avec %,, et au point L on aura

Lag
R den aL

Lo

Q à z ; i
Or la dérivée . étant supposée développée en séries suivant les puissances de l’accroisse-

General- Bericht f. 1866. 2

 
