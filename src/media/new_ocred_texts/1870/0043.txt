en!

10 PPT

i
i
ë
=
€
8
=
k
ë
E
:
;
?
:
=

Zu
Station Kopenhagen, Nicolai-Kirchthurm.
@ = 55° 40! 49,660.
a ee eee

 

| Entfernungen

Namen der Punkte, Azimuthe. 1 À

Log. in Toisen.
eee perpen ps ep et Mes
Nordpunkt's «.. Gaps 0 0 0,000

Memel:s. teen ae Saha, 4 42.005 54385426 . 3 274500,178
Künigebenes oui aus 98 46 20,452 54219401 . 4 264204,456
rune Narr 102.310. 5,3859048 . 3 243167,110
Berlinahte) and Gis an TL 007) 90454 5,2683275 . 9 183369,708
Store (Mollehdi.°).. 4... 294 15 14,84 43257767 21172,72

Azimuth und Breite von Kopenhagen sind geodätisch von Berlin aus gerechnet; die
Breite von Berlin wurde dabei zu 52° 30' 17",22 angenommen nach den Bestimmungen aus den
Jahren 1853 und 1866. |

Die vier Stationen Berlin, Trunz, Memel, Königsberg sind aus Baeyer’s ‚Messen
auf der sphäroidischen Erdoberfläche” genommen pag. 104.

Die Seite Berlin—Kopenhagen wurde so berechnet: Berlin—Darserort wurde aus
den Dreiecken der preussischen Küstenvermessung gerechnet, nachdem zuvor die Logarithmen
der Seiten derselben um: + 31,6 verbessert wurden (siehe Generalbericht der Europ. Gradm.
für 1863, pag. 28). Darserort— Kopenhagen wurde aus den Dreiecken der Dänischen Grad-
messung (I. Bd. pag. 387, 388) gerechnet; aus beiden Seiten dann Berlin— Kopenhagen.

Die Seite Berlin— Altona wurde aus den Seiten Berlin— Lüneburg und Lüneburg—
Altona erhalten. Erstere wurde gerechnet aus den preuss. Dreiecken von der Seite Eich-
berg—Eichstädt bis zur Seite Hochbök — Ruhnerberg (Jahresb. für 1863, pag.23) und den
Mecklenburgschen Dreiecken (a. a. O. pag. 5). Altona — Lüneburg aus dem Wittstein’schen
Coordinatenverzeichnisse der Hannöverschen Landesvermessung aus den Jahren 1821 bis 1844,

und zwar aus den Gauss’schen Coordinaten auf legale Meter gebracht.

3. Bericht des Herrn Prof. Dr. Wilistein.

Meinem vorjährigen Berichte habe ich die folgende Ergänzung hinzuzufügen.

Um für die geodätischen Linien Göttingen— Dangast und Göttingen — Helgoland
diejenigen Daten zu berechnen, welche erforderlich sind, damit aus ihnen die Gestalt der
Erde bestimmt werden könne, musste die Triangulation von Gauss zu Hülfe genommen
werden, und da die Resultate dieser Triangulationen in der Form von ebenen Coordinaten
vorliegen (s. d. im Jahre 1868 auf Anlass der Grundsteuer-Veranlagung von mir heraus-

gegebene ÜOoordinaten-Verzeichniss der hannoverschen Landesvermessung), so kam es zunächst

 
