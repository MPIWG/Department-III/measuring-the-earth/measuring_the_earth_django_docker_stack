 

 

Ba

wobei & der Winkel ist, den der bewegliche Fühlspiegel mit dem festen Spiegel bildet, und
D den Abstand der beiden Berührungspunkte der Maassstäbe mit dem Spiegel bedeutet. Die
Bestimmung des Winkels & geschieht bekanntlich mit Hülfe des Mikrometers am Beobach-
tungsfernrohr, und zwar mit vollkommen ausreichender Schärfe. So ist bei den von v, Stein-
heil schon früher (Wiener Denkschriften Bd. XXVII. 1867 „Ueber genaue und invariable
Copien ete.“) beschriebenen Comparator der Winkelwerth eines Umganges der Mikrometer-
schraube nahe 5’, also für einen Trommeltheil 3”. Bei den damit ausgeführten Beobachtungen
betrug meist der Abstand D der beiden Berührungspunkte ungefähr 10°, so dass die Ver-
schiebung um einen Trommeltheil am Mikrometer einer Längendifferenz der Maassstäbe von
ungefähr 0"",00015 entsprach. Zur Ermittlung der Grösse D bedient sich Steinheil eines
sogenannten Tiefenmessers. Beträgt nun die Längendifferenz der beiden zu vergleichenden
Maassstäbe nicht mehr als 0”",01, so ist die Genauigkeit der Messung mit dem Tiefenmesser
ganz entsprechend der Bestimmung des Winkels &. Man liesst nämlich an dem Tiefenmesser
direct noch 0””,1 ab, was bei der eben gemachten Annahme für den Längsunterschied der
Maassstäbe, eine Differenz in der Länge von 0,0001 direct erkennen lässt. Wenn jedoch
die Längendifferenz der Maassstäbe grösser, oder, wie es bei dem neuen Comparator ist, die
Bestimmung des Winkels & genauer wird, so ist damit auch eine vermehrte Genauigkeit in
der Bestimmung von D gefordert.

Mechaniker Stollenreuther in München führte nach meiner Angabe den in Fig. 3 und
4 gezeichneten Apparat aus, welcher den Tiefenmesser zu ersetzen hat. (Taf. VL.)

Die Newtonschen Farbenringe geben ein sehr bequemes Mittel, um die Berührungs-
punkte, deren Entfernung ja gemessen werden soll, zu erkennen. Wird der Berührungsspiegel
(S) durch den aus der Zeichnung leicht verständlichen Hebel (H), auf den die Spiralfeder
(F) wirkt, an die Maassstäbe angedrückt, so erblickt man auf.der freien Seite des Spiegels
bei seitlicher Beleuchtung die Newtonschen Farbenringe. Das mit einem Fadenkreuz ver-
sehene Beobachtungsfernrohr (B) kann mit der Mikrometerschraube (M) zuerst auf den Mittel-
punkt der unteren Newtonschen Farbenringe, dann auf den der oberen gestellt, und die Ent-
fernung derselben an der Mikrometertheilung abgelesen werden.

Die Beobachtungen können nach doppelter Methode ausgeführt werden. Man nivellirt
die Maassstäbe, stellt sodann mit Hülfe der kleinen Wasserwage (W) den Mikrometerschlitten
vertical, und misst nun den verticalen Abstand der Berührungspunkte, also die gewünschte
Grösse D direct. Man könnte jedoch, wenn man das Fadenkreuz des Beobachtungsfernrohrs
beleuchtet, die optische Axe dieses gebrochenen Fernrohrs genau parallel gegen die Berüh-
rungsebene des Fühlspiegels einstellen, und so die Hypothenuse des rechtwinklichen Dreiecks
bestimmen, dessen Katheten die Längen dl und D sind. Die erste Methode empfiehlt sich
durch ihre Einfachheit, während die letztere eine grössere Genauigkeit verspricht.

Vorläufig beschränkten sich die vorgenommenen Arbeiten mit dem neuen Apparate
nur auf das Zusammenstellen desselben, die Ermittlung seiner Constanten und auf einige ver-

gleichende Beobachtungen zwischen drei Glasmetern.

 

PRE VEN nd Med re Te

an cou cam es ial bad autant mue

 
