Ve ne ori

Rn |

Pr

BE TE

PTT

arian) Ferrers

POOP TN ORT TRIB

Ce

Für die Arbeit auf den Bahnen, wo die Distanzen so nahe gleich genommen werden
können, dass in einzelnen Fällen selbst erhebliche Instrumentalfehler nur verschwindend kleine
Correctionen am Resultat nöthig machten, gedenken wir künftig die Operationen so weit zu
vereinfachen, dass wir uns nur durch Probeablesungen, welche schon bisher täglich zweimal
gemacht wurden, die Kenntniss des jeweiligen Betrags der Instrumentalfehler sichern.

Das grosse Polygon Nördlingen — Aalen — Heidenheim — Ulm — Friedrichshafen — Lin-
dau — Augsburg— Nördlingen ergab einen Schlussfehler von On 111% aut eine Länge von
495 Kilometern, woraus sich der wahrscheinliche Fehler pro Kilometer zu + 0,0034 berech-
net, was zulässig sein dürfte, wenn man berücksichtigt, dass einzelne Strecken ausserhalb
der Bahn gelegen waren mit ganz ungünstigem Profil, und dass auf der Bahn selbst man-
nichfach sehr starke Steigungen vorkommen. Durch die Strecke Augsburg— Ulm wurde das
grosse Polygon in zwei kleinere zerlegt, Nördlingen — Ulm und Ulm—Lindau, deren Schluss-
fehler erst in neuester Zeit festgestellt wurden, übrigens ein weniger befriedigendes Resultat
liefern, so dass es nöthig sein wird, weitere Polygone zu schliessen, worüber eine vorläufige
Verständigung mit dem bayerischen Commissär Herrn Director Bauernfeind stattgefunden hat.
Es beträgt nämlich der Schlussfehler in dem Polygon

Nördlingen— Ulm . +0",1751

Ulm—Lindau. . . —0",0634

Nördlingen — Lindau —0",1117.
Der wahrscheinliche Fehler in dem ersten Polygon berechnet sich bei einer Länge von
258 Kilometern zu + 0,0073 pro Kilometer, eine Grösse, welche nur in dem aus der
110 Kilom. langen sächsischen und der 201 Kilom. langen bayerischen Linie zusammen-
gesetzten Polygon Franzensbad— Hof übertroffen wird, wo bei einem Schlussfehler von 0”,2046
und unter Voraussetzung gleicher Gewichte sich der wahrscheinliche Fehler pro Kilometer zu
+ 0%,0078 berechnet.

Die in den Jahren 1868 — 1870 in Würtemberg nivellirten Strecken haben eine Ge-
sammtlänge von 829,4 Kilometern. Es wurden ausgeführt:

1868. Das Polygon Stuttgart— Heilbronn—Creilsheim— Aalen—Stuttgart mit 254 Kilometer.
1869. Bruchsal— Stuttgart. . . . mit 78,2 Kilometer

Canstatt— Friedrichshafen. . - 1945 -
Goldshöfe— Nördlingen . . - 32,8 =
Aalen —Heidenheim . . . . - 22,4

Heidenheim—Beimerstellen . - 34,4 -
Ulm - Neu Um... „2. 2,0 -
Friedrichshafen — Nonnenhorn - 16,6 -

 

zusammen 380,9 Kilometer.
1870. Creilsheim—Leudenbach . . mit 44,7 Kilometer
Plochingen —Willmgen . . „72. 145 -

 

zusammen 194,5 Kilometer.

 
