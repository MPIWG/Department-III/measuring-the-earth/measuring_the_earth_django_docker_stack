AE PTIT PTE PTT PP PPT POP

&
rs
=
=
=

Le ON ae

Die Polhöhen und Azimuthbestimmung auf Sibenica, so wie die Beobachtungen auf
den Stazionen Obesenjak, Svilaja, 8. Giorgio, Velki grad, Rogo, S. Andrä, Ostra glavica
und Snieznica begannen Mitte Juli und waren Anfangs Oktober beendet. Ausserdem wur-
den Höhenbestimmungen und einige Richtungsbeobachtungen auf mehreren Punkten zwischen :
Antivari und Skutari in Albanien als Ergänzung zu den vorjährigen Arbeiten nachgetragen.

Die Polhöhe auf Sibenica wurde vom Oberst Ganahl unter Assistenz des Oberlieute-
nant Hartl aus 600 Einstellungen, und zwar aus 200 des Polaris, 100 von nördlichen und
300 von südlichen Sternen, dann aus circa 300 Durchgängen von «& Lyrae, y Cygni, o Andro-
medae und & Aurigae im 1. Vertikal bestimmt. Für das Azimuth sind 28 Sätze und zwar
jeder an einer anderen Stelle des Kreises mit regelmässiger Verstellung desselben beobach-
tet worden.

Jeder Satz besteht aus 4 Einstellungen des terrestrischen Objectes und eben so viel
des Polarsternes, und zwar zur Hälfte in jeder Kreislage.

Die geodätischen Beobachtungen auf oben genannten Stazionen hat mit Ausnahme
des Svilaja, wo ÖOberlieutenant Hartl beobachtete, sämmtlich Lieutenant von Gyurkovich
ausgeführt.

Major Breymann, welcher nach vollendeter Basismessung anderer dringender Arbeiten
wegen nach Wien zurückgekehrt war, hat im Verlaufe des Monates August und September
östlich Wien’s zur Fortsetzung des Netzes im 48. Parallel Recognoscirungen ausgeführt und
auf den Punkten Zobor und Magoshegy die Markirung und Pfeilererrichtung vorgenommen,
dann auf dem Hundsheimerberg die Richtungsbeobachtungen begonnen.

Ebenso wurden bei Gelegenheit der Triangulirung 2. und 3. Ordnung in Sieben-
bürgen vom Hauptmann Hold und in der Banater-Militär-Grenze vom Hauptmann Vergeiner

u. z. im ersteren Lande auf 15, im letzteren auf 13 Stazionen die Richtungen 1. Ordnung
neu beobachtet.

Für die Gradmessung und Triangulirung 2. Ordnung in Siebenbürgen sind im
Jahre 1870 15,000 Fl. verwendet worden, dieselbe Summe ist auch für das Jahr 1871 bereits
dafür bewilligt worden. _
Wien, 24. Februar 1871.
Fligely.

on Ol dee gn ae oe
Schreiben des Herrn von Schrenck an den Generallieut. z. D. Baeyer.

Ew. Excellenz beehre ich mich in Bezugnahme auf Ihre Aufforderung vom 5. v. M.
gehorsamst zu berichten, dass im verflossenen Jahre Seitens Oldenburg Kosten im Interesse
der europäischen Gradmessung nicht aufgewendet sind, abgesehen von der geringen Kosten-
summe von 2 Thlr., welche die Beaufsichtigung des astronomischen Stationspunktes Dangast

jährlich veranlasst.

 
