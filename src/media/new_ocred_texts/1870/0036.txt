 

 

une. le

Seehöhe der Eisenbahnschienen am östlichen Ende der über die sogenannte

Eon Elbe suhvenden: Brücke cr small. hi FI a Gd sO 48,874
Sehwelle der Kirchenthür am nördlichen Domthurme . . .... . . 56,004
Oestlicher Beobachtungspfeiler auf dem nördlichen Domthurme . . . . 138,590
Westlicher à 5 M ji 55 ahead 38,947
(eee) dieses, Phurmes). yanbiuua.ull va ebenen... 2000 196,810

Am 2. Juni verliessen wir Magdeburg und begaben uns nach Burkersrode, einem
1 Meile westlich von Freiburg a. d. Unstrut auf einem Plateau von 150 Toisen mittlerer See-
höhe gelegenen Dorfe. Dreieckspunkt ist der Kirchthum. Bis zur Höhe 8,5 Tois. aus festem
Mauerwerk bestehend, trägt derselbe ein mit Schiefer (gedecktes, sogenanntes Kropfdach.
Der schon früher errichtete und als Leuchtstand benutzte Pfeiler auf der Krone des Ge-
mäuers an der Nord-Ost-Ecke reichte leider für die Beobachtungen nicht aus, weil der Insels-
berg hier nicht sichtbar war, und es musste deshalb an der Nord-West-Ecke ein zweiter
Pfeiler hergestellt und mit einem Beobachtungsgerüst umgeben werden. Auf letzterem waren
zwar alle Punkte sichtbar, aber die Richtung nach Leipzig ging wegen der stark convexen
Gestalt des Daches bei diesem so nahe vorbei, dass die Beobachtungen, namentlich bei Fern-
rohr rechts, unsicher wurden. Aus diesem Grunde musste auch auf dem ersten Pfeiler beob-
tet werden. Die zu beobachtenden Richtungen waren: der Inselsberg, der Brocken, der
Petersberg und. Leipzig. Als Nullpunkt diente eine weisse Tafel mit senkrechtem schwarzen
Striche, welche an das Mauerwerk des Kirchthurms von Crawinkel, einem ‘/, Meilen nach
Nord hin entfernten Dorfe, befestigt worden war. Diese Art der Befestigung der Tafel
scheint mir aus mehreren Gründen vortheilhafter zu sein, als das Anheften an einen Pfahl,

hauptsächlich deswegen, weil man sich dabei höher über den Erdboden erheben kann. Ob-

gleich eine Zerstörung beider Beobachtungspfeiler nicht zu besorgen ist, so ist dennoch zur

Vorsicht ein Festlegungsstein auf dem Kirchhofe in der Nähe der Nord-Ost-Ecke versenkt
und die Lage desselben gegen den Nord-Ost-Pfeiler bestimmt worden. Das Wetter war über-
aus unbeständig und die Entfernungen der zu beobachtenden Objekte zum Theil sehr bedeu-
tend (der Brocken 13 Meilen, der Inselsberg 11 M.). Deshalb gingen die Beobachtungen nur
langsam von Statten, und wir konnten erst nach 6 Wochen (den 16. Juli) Burkersrode ver-
lassen, um uns nach Gotha zu wenden, wo auf dem /, Meile von der Stadt entfernten See-
berge zu beobachten war. Der Gaussische Beobachtungspfeiler, welcher Dreieckspunkt ist,
hat eine Höhe von 8 Fuss und musste mit einem Beobachtungsgerüst umgeben werden. Als
Nullpunkt ist der 1068 Tois. entfernte östliche Schlossthurm von Gotha genommen worden,
dessen Helmstange ein gut einstellbares und immer sichtbares Objekt lieferte. Die zu beob-
achtenden Dreieckspunkte waren: der Inselsberg, der Brocken (13 Meilen) und der Kirchtburm
von Struth (bei Mühlhausen). Der Krieg, welcher bald nach unserer Ankunft ausbrach, hat
unsere Arbeiten nur in sofern gestört, als die zur Einrichtung der Heliotropen erforderlichen
Reisen wegen des beschränkten und eine Zeit lang ganz aufgehobenen Personen-Verkehrs aut

den Eisenbahnen viel Zeit erforderten. Dagegen wurden die Beobachtungen sehr haufig durch

 

8 hier Aa dl Me

akt

iss ch hee ih he a dt ue oe

 
