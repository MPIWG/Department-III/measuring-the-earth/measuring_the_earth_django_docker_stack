Ihre wenn ver ie ore

PA APTE SPP AT ETT |

se EU

9

9°. Draltra via si é tenuto conto delle due equazioni di 2* Classe date da’ triangoli
1, 8, delle due equazioni di 1* Classe offerte da’ giri d’orizzonte intorno a’ punti Gerbini e
Perni, e delle due equazioni laterali, che ban luogo intorno a’ punti medesimi.

Sicché riassumendo si hanno 2* equazioni di 1* Classe, 8 equazioni di 2* Classe, e
4 equazioni di 3* Classe; in tutto 14 equazioni.

I restanti triangoli 9, 10, 11, 12 derivano da’ precedenti.

In ultimo l’error medio de’ valori angolari ottenuti & m= 0",55.

$. 11°. Chiudiamo questa breve relazione coll’ adempiere al debito di chiarire i nomi
di coloro che occupati alle operazioni indicate han satisfatto al loro incarico con intelligenza
e solerzia.

I lavori di Campagna sono stati eseguiti tutti sotto la general direzione del Comm'
de’ Vecchi Colonnello di Stato Maggiore, e membro della Commissione Italiana per l’Arco
meridiano.

La base @ stata misurata da Uffiziali ed Ingegneri Geografi dello Stato Maggiore:
e propriamente il Capit? Marangio il Capit® de Vita e l’Ingegnere d’Atri si occu-
pavano delle Spranghe e della misura de Cunei, il Capit® Osio dava l’allineamento.

La rete circostante alla base @ stata eseguita dal Cap™ de Vita, e dallo In-
seo? d'A pr

I calcoli sono stati eseguiti qui in Napoli nel Gabinetto Geodetico dello Stato Mag-
giore, e ad essi han preso parte principale i Capit! Marangioe de Vita, e gli Ingegneri
geografi d’Atri ed Arabia; e parte ausiliaria PIngeg"® geografo Lucci, ed il Topo-
grafo Nacciarone.

Napoli addi 1° Aprile 1866.

Il Professore di Geodesia

Federigo Schiavoni.

 
