Tin | ea ec

(RY TFET SP RT TY |

un dou

September 23. 51° 51 42”,25 4.0",04
24. 42,24 + 0,06.

Von den noch auszuführenden Längenbestimmungen mit auswärtigen Sternwarten wird
vielleicht in diesem Jahre keine ausgeführt werden können, da den Sternwarten, an welche
ich Leipzig noch anzuschliessen gedenke, theilweise die telegraphische Verbindung, theilweise
die nöthigen Instrumente fehlen. Hier in Sachsen ist es meine Absicht, im Jahre 1866 den
Punkt Kapellenberg bei Brambach in Länge, Breite und Azimuth, den Punkt Fichtelberg
und Kahleberg in Breite und Azimuth und schliesslich die 5 Punkte um Leipzig herum in
Breite und Azimuth, welche im verflossenen Jahre von Herrn Professor Nagel und mir re-
cognoscirt sind und auf denen die Pfeiler nächstens errichtet werden, zu bestimmen.

Leipzig, ım Februar 1866.
C. Bruhns.

1 Sa Cn sen Cob ure Gort hina,
Bericht des Geheimen Regierungsraths Dr. Hansen.

In Uebereinstimmung mit den in der ersten Conferenz der Bevollmächtigten der
mitteleuropäischen Gradmessung gefassten Beschlüssen wurde im Monat April des vorigen
Jahres die telegraphische Bestimmung des Längenunterschiedes zwischen den Sternwarten
von Leipzig und Gotha ausgeführt. Das Resultat derselben, dessen Berechnung von Herrn
Dr. Auwers kürzlich vollendet worden ist, giebt für diesen Längenunterschied

6 43°,485
und bezieht sich auf die Mittelpunkte der Thürme der genannten Sternwarten.

Die bez. Beobachtungen und die Nachweisung der Einzelnheiten dieser Bestimmung

und der Berechnung derselben wird ehestens in einer besonderen Abhandlung dem Druck

übergeben werden.-

1. Schweden. und Neoerwez.u.
1. Bericht über den Fortschritt der Gradmessungsarbeiten in Schweden im Jahre 1865
von Prof. Dr. D. G. Lindhagen.

Im letztvergangenen Jahre, 1865, wurden im Interesse der mitteleuropäischen Grad-
messung folgende Arbeiten Schwedischerseits ausgeführt:

1) Im Laufe einiger Wochen des Januars und Februars wurde der bei unseren Ba-
sismessungen angewandte Basisapparat in Bezug auf die Wärmeausdehnung der vier Mess-
stangen untersucht, wobei ausserdem zwei zum Apparate gehörige Normalstangen einer ähn-
lichen Untersuchung unterworfen wurden. Bei diesen Untersuchungen wandte ich dieselbe
Methode an, welche zur Bestimmung der Ausdehnung der Messstangen des neueren Struve-
schen Basisapparats gedient hatte, und welche in dem Werke Struves: „Are du meridien
entre le Danube et la mer glaciale“ T. I, S. 46—50 näher auseinandergesetz ist.

2) Im Laufe des letztvergangenen Sommers wurden die Längenunterschiede zwischen

 
