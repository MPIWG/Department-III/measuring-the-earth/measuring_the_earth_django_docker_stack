RT NT |

A

Bo

nommene, nach der letzten Berechnung derselben und nach der neuen Berechnung der Hol-
steinischen Basis (siehe: General-Bericht über die mitteleuropäische Gradmessung für das
Jahr 1864) berichtigte Seite Göttingen-Meridianzeichen, welche in Bessel’schen Toisen ausge-
drückt zum Logarithmus hat = 3,4103790. Die in dieser Abtheilung mit * bezeichneten
Stationspunkte und Richtungen sind des Zusammenhangs wegen mit aufgenommen, beziehen
sich aber auf spätere zur geodätischen Bestimmung der Sternwarte von Marburg im Som-
mer 1865 ausgeführte und auf das Dreieck Amöneburg-Dünsberg-Hasserod basirte Messun-
gen und Berechnungen.

Als Grundlage für die Berechnung der Dreiecksseiten der zweiten Abtheilung dienten
die aus der ersten Abtheilung bekannten definitiven Richtungen und Längen der Dreiecks-
seiten Hils-Hohehagen, Hohehagen-Hercules, Hercules-Desenberg, und Desenberg-Hohelohr.
Die Winkelmessungen sind zum grösseren Theile den trigonometrischen Arbeiten des König-
lich Preussischen Steuerrath Vorländer (Geographische Bestimmungen im Königl. Preussischen
Regierungsbezirke Minden etc. Minden 1853) entnommen und nur zum kleineren Theile für
den Zweck einer Verbindung der Grafschaft Schaumburg mit dem Hauptlande Kurhessens
durch Dreiecke erster Ordnung im Jahre 1853 durch Messungen auf den Stationen Köterberg,
Wittekindstein, Süntel und Wilhelmsthurm vervollständigt, jedoch im Zusammenhange neu
ausgeglichen worden.

Von sämmtlichen Dreieckspunkten sind drei nur durch Anschnitte von andern Punk-
ten, nicht durch Messungen auf ihnen selbst, festgelegt, die Visirpunkte sind aber markirt,
nämlich Langeberg durch einen Postamentstein, Weidelsberg durch ein in den Felsen, den
s. g. ,Freudenstein* eingehauenes Kreutz, und Hercules, Kubus unter der Statue.

Die Dreieckspunkte jeder Abtheilung sind alphabetisch geordnet und die erste Rich-
tung auf jeder Station ist immer = 0° gesetzt.

Zur Bestimmung der mittlern und wahrscheinlichen Fehler der einzelnen Richtungen
dienen die nachfolgenden Formeln. Bezeichnet 2, die Anzahl der Bedingungsgleichungen,
[vv] die Summen der Fehlerquadrate, m den mittleren, r den wahrscheinlichen Fehler, so ist

0a lel

m r = 0,674489 .m.

Hiernach hat man:
1) für die 1. Abtheilung ausschliesslich der mit * bezeichneten Richtungen:

m — 0,9460, r = 0,6381;

2) für die mit * bezeichneten Richtungen: |
m 0,9912, r = 0/,6686 ;

3) für die 2, Abtheilung:
m = 1",3665, r — OICOZLT.

Dr. Börsch.

 
