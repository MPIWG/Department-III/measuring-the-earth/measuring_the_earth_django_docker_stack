 

u | ee

gefangen werden, so wird sich doch vieles vorbereiten lassen. Zum Behuf der Längen-Be-
stimmungen bestellte ich, schon am 4. Septbr. 1865, bei Herrn Knoblich in Altona, einen
vollständigen Registrir-Apparat, dessen Ablieferung von Herrn Knoblich innerhalb sechs Mo-
nate versprochen wurde. Als ich aber anfangs mehrere Briefe des Herrn Knoblich erhalten
hatte, welche mich zu den grössten Hoffnungen berechtigten, hörten diese günstigen Vor-
zeichen auf einmal auf, und seit 17. Novbr. 1865 habe ich von Herrn Knoblich durchaus
keine Antwort auf meine Briefe erhalten. Ich erhielt von Herrn Knoblich selbst keine Ant-
wort auf mein Schreiben vom 5. Febr. d. J., worin ich mir kurze Mittheilung über die Fort-
schritte des Apparats erbat, um dieselbe in diese Nachrichten aufnehmen zu können.

Die Herren Repsold arbeiten jetzt an einem schönen und kostspieligen Apparat, zur
Basis-Messung, für die Triangulation der Insel Java. Hoffentlich wird es uns erlaubt sein
diesen Apparat, bevor der Absendung nach Java, zu einer Basis-Messung in unserem Vater-
lande zu benutzen.

Es ist meine Absicht die Längen-Bestimmungen zwischen Leiden und benachbarten
Sternwarten anzufangen, so bald als die Umstände es erlauben werden. Zu einer Azimuth-
Bestimmung bietet die Leidner Sternwarte eine schöne Gelegenheit dar. Meine Mitarbeiter
an der hiesigen Sternwarte bleiben mit mir bereit die Arbeiten auszuführen, welche die Be-
hörden der mitteleuropäischen Gradmessung uns, zu diesem Behuf, aufzutragen wünschen.
Ich bedauere es sehr, dass die Mitwirkung meines Vaterlandes an der mitteleuropäischen
Gradmessung, durch die Umstände, so lange verzögert wurde, obschon ich niemals auf mich
warten liess.

Leiden, 22. Februar 1866.
F. Kaiser.

NOs tail en.

Ausser dem Protokoll der Conferenz vom 8. bis 6. Juni 1865 der Italienischen geo-
dätischen Commission, welches sich bereits in den Händen der Herren Bevollmächtigten be-
findet, ist kein weiterer Bericht eingegangen. Es mag daher hier ein kurzer Auszug aus
diesem Protokoll Platz finden.

Der Präsident Generallieutenant Ricei, Exc., theilt der Commission mit, dass bereits
im Jahr 1865 folgende Arbeiten ausgeführt wurden:

1. Die Messung einer Grundlinie bei Catania und ihre Verbindung mit der Triangulation
von Sicilien.
2. Die Verbindung der 1860 bei Foggia gemessenen Grundlinie mit der Triangulation des
Königreichs Neapel;
und spricht die Hoffnung aus, dass noch im Laufe des Jahres die Verbindung beider Grund-
linien unter sich durch eine Dreieckskette 1ster Ordnung werde hergestellt werden, oder doch

eine Dreieckskette vom Parallel von Cosenza bis zum Cap Passaro.

Unter den Beschlüssen der Conferenz sind als die wichtigsten hervorzuheben:

=
ü
=
=
x

 

PT)

day

à 1 nme ad u, allan dab Aa le iy)

 
