Dh RAA EP ATS TU N: |

I Im on

[LA

ae UN

schon in der allgemeinen Conferenz des Jahres 1867 einen vorläufigen Bericht abgestattet.
Die sehr zahlreichen, von mir selbst abgelesenen Signale zur Uhrvergleichung, die noch zahl-
reicheren, von mir selbst reducirten Beobachtungen zur Bestimmung der persönlichen Gleichungen
und die von meinem Sohne, Dr. P. J. Kaiser und mir angestellten Untersuchungen über die
Trägheit der benutzten Relais, sind von mir in den Monaten Juni und August 1867 Herrn
Prof. Förster, der Mitglied des Central-Büreaus, mitgetheilt. Als die Beobachtungen zur Zeit-
bestimmung schon längst so weit redueirt waren, als dies ohne die vom Central-Büreau ver-
sprochenen Ephemeriden der gemeinschaftlich beobachteten Sterne möglich war, sind dieselben
Herrn Prof. Förster, auf dessen Wunsch, im Monat November 1868 vollständig übersandt.
Die allgemeine Conferenz des Jahres 1864 hat die zu der europäischen Gradmessung
erforderlichen Längenbestimmungen angewiesen und dazu gehörte die Längenbestimmung
zwischen den Sternwarten von Brüssel, Bonn und Leiden. Es hat bei mir an keinen Be-
strebungen gefehlt, um dieselbe zur Ausführung zu bringen, aber es schien, dass mich dabei
durchaus Unglücksfälle treffen müssten. Nachdem im Jahre 1867 meine Bemühungen erfolglos
geblieben waren, hoffte ich, dass im Jahre 1868 eine gleichzeitige Längenbestimmung zwischen
Brüssel, Bonn und Leiden stattfinden würde. Im Winter vorher habe ich grosse Opfer an
Zeit und Mühe gebracht, um zum Behufe dieser Untersuchung zwei Exemplare eines neuen
Apparates zu Stande zu bringen, welcher sich besonders zur absoluten Bestimmung des per-
sönlichen Fehlers bei Registrirbeobachtungen eignet und sehr leicht transportabel ist. Im Monat
Februar 1868 schrieb ich Herrn Director A. Quetelet und Herrn Professor Argelander mit der
Bitte, im nächsten Sommer an einer gleichzeitigen Längenbestimmung zwischen Brüssel, Bonn
und Leiden mitwirken zu wollen und schlug vor, dabei nahezu dieselben Methoden zu befolgen,
die bei der Längenbestimmung zwischen Leipzig, Dangast, Göttingen und Leiden im vorigen
Jahre angewandt waren und sich sehr gut bewährt hatten. Ich fügte meiner Bitte eine aus-
führliche Beschreibung und Abbildung der Einrichtungen hinzu, welche im vorigen Jahre bei
den oben genannten Längenbestimmungen an der Leidener Sternwarte getroffen waren und
bot sogleich für eine unbestimmte Zeit den Gebrauch der obengenannten Apparate an, in der
Hoffnung, dass diese die Bestimmung der persönlichen Fehler würden versichern und erleichtern
können. Von Herrn Prof. Argelander ist keine Antwort bei mir eingegangen. Herr Director
Quetelet versprach gütigst seine Mitwirkung, doch die Arbeiten an der Sternwarte in Brüssel
machten einen Aufschub nothwendig. Am Ende wurde eine abgesonderte Längenbestimmung
zwischen Brüssel und Leiden beschlossen; jedoch machte Herr Direetor Quetelet die Be-
dingung, dass die Beobachtungen zur Zeitbestimmung, mit Auge und Ohr, am grossen Passagen-
instrumente der Sternwarte in Brüssel angestellt werden sollten. Die Leitung der Arbeit hatte
Herr Director Quetelet die Gefälligkeit, mir zu überlassen. Ich würde die Registrirmethode
vorgezogen haben, nicht nur, weil sie ohne Zweifel die genauere ist, sondern auch besonders,
weil sich dabei die persönlichen Gleichungen, welche immer die schwache Seite der Längen-
bestimmungen ausmachen, am wenigsten schwankend zeigen. Ich konnte mich aber in den

Wunsch des Herrn Director Quetelet fügen, da sich auch durch Beobachtungen mit Auge und
2%

 
