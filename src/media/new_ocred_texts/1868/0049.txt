(cP PATTY Tee |

so ae

TR NS So aa

Es ist kein Bericht eingegangen.

15. Sachsen.

I. Bericht über die im Jahre 1868 im Interesse der europäischen Gradmessung von
Seiten des Königreichs Sachsen ausgeführten geodätischen Arbeiten.

Im Anschluss an die Berichte der vorausgegangenen Jahre und an die Mittheilungen
im Bericht über die Verhandlungen der im Jahre 1867. zu Berlin abgehaltenen allgemeinen
Conferenz der europäischen Gradmessung, hat die Sächsische Commission über die Fortschritte
der im Jahre 1868 vollzogenen geodätischen Arbeiten Folgendes zu berichten:

Unter der Leitung des Herrn Professor Nagel sind im Jahre 1868 neue Pfeilerbaue
zur Ausführung gekommen, so dass nun das Sächsische Triangulirungsnetz, bis Ende des
Jahres 1868, durch 73 Pfeiler, wovon 17 Pfeiler dem Dreiecksnetz der ersten Klasse angehören,
fixirt ist. Hierzu sollen im Jahr 1869 die noch übrigen zwei Pfeiler des aus 19 Eckpunkten
bestehenden Hauptnetzes, sowie 16 Pfeiler für Punkte der zweiten Klasse ausgeführt werden.

Der letzte Hauptpfeiler bei Thiemendorf im Altenburgischen soll von Preussen und
Sachsen auf gemeinschaftliche Kosten ausgeführt werden, es hat aber dessen Ort bis jetzt noch
nicht ganz festgestellt werden können. Da, den vorgelegten Bahnprojeeten zufolge, die Cottbus-
Grossenhainer Eisenbahn unsere, bereits durch drei Pfeiler fixirte Basis entweder sehr nahe kommen,
oder gar durchschneiden würde, so ist beschlossen und von der Sächsischen Regierung genehmigt
worden, dass diese Basis verlassen und durch eine andere eirca 500 Ellen weiter nördlich liegende
Grundlinie ersetzt werde. Die hierzu nöthigen Basispfeiler sollen zwar in diesem Jahre ausgeführt
werden, da aber beim Setzen und Austrocknen des Mauerwerks derselben noch kleine Ver-
schiebungen eintreten könnten, hat die Sächsische Commission es für nöthig gefunden, von
der Ausmessung der Basis im Jahre 1869 noch abzusehen. Da uns überdies auch daran

‚liegen muss, für das mit vielen Kosten hergestellte Dreiecksnetz vom Königreich Sachsen eine

genaue Basis zu erhalten, so müssen wir auch wünschen, dieselbe mittels eines neuen, den
Fortschritten der Wissenschaft entsprechenden Apparats ausmessen zu können.

Möchte uns daher der, unter der Leitung des Centralbtireaus anzufertigende Basis-
Apparat recht bald zur Verfügung stehen!

Winkelmessungen konnten im Jahre 1868 vom Herrn Professor a nur auf
dem Valtenberg und zwar: von dem auf demselben stehenden König-Johann-Thurm aus vor-
genommen werden. Es ist dieser Berg die umfänglichste Station im Sächsischen Dreiecks-
netze, da von derselben aus die grösste Anzahl von Visuren nach erster und zweiter Klasse
gemacht werden konnten. Unter den 21 Richtungen, nach welchen von diesem ausgezeich-
neten Punkte aus gemessen wurde, gingen 9 nach Dreieckspunkten erster Klasse, nämlich

nach der Nostizhöhe, dem Jauernick, dem Jeschken, der Lausche, ferner nach dem Hohen-

m

General-Bericht f. 1868. FU

 

 
