2 PRP TP YTS TY |

im

Re

Für das Jahr 1868 liefen drei solcher Berichte ein, nämlich:
am 25. Februar der des Herrn Bauernfeind,
am 1. März der des Herrn von Steinheil, und
am 4. März der des Herrn Seidel.

Herr von Lamont hat keinen Bericht erstattet.

Diesen Berichten entnehmen wir Folgendes:

Professor Bauernfeind berichtet:

„In der Erklärung, welche ich in der sechsten Sitzung der zweiten Conferenz der
europäischen Gradmessung am 5. October 1867 über die Gründe abgegeben habe, aus denen
die Königl. Bayerische Staats-Regierung ein Gutachten jener Conferenz über den Werth der
älteren und von mir in der ‘ersten Sitzung dargestellten Bayerischen Triangulation zu erhalten
wünschte, sprach ich die Hoffnung aus, unser Landtag werde für die Jahre 1868 und 1869
die in das Budget der neunten Finanz-Periode eingestellte Summe von 20,000 fl. für Zwecke
der in Bayern vorzunehmenden geodätischen Arbeiten genehmigen. Diese Hoffnung ging je-
doch (bekanntlich) nicht in Erfüllung, indem nur die Hälfte der verlangten Summe bewilligt
wurde, wovon ein Theil (3000 fl.) im Jahre 1868 und der Rest (7000fl.) im Jahre 1869 zur
Verwendung kommen wird.

Da diese geringeren Mittel noch dus erst mit dem Monat August mee das so
erschien es als das Zweckmässigste, dieselben vorzugsweise auf das Nivellement des Landes
zu verwenden, um nicht am Ende zwei mit ungenügenden Fonds begonnene Arbeiten (Tri-
angulation und Nivellement) gleichzeitig in’s Stocken gerathen zu sehen.

Bis zur Vollendung der hierfür erforderlichen Vorbereitungen, und namentlich bis zur
Herstellung der erforderlichen Nivellir- Instrumente, sollte der mir. seit dem 1. August: beige-
gebene und zunächst mit Uebungsarbeiten an dem geodätischen Institut der Münchener poly-
technischen Schule und mit Zeichnungsarbeiten für die neuen Nivellirapparate beschäftigte
Assistent, Herr August Vogler, von der Erlaubniss des Präsidenten des Central-Büreau’s, Herrn
Generallieutenant Baeyer, Gebrauch machen und zu seiner Instruction die Triangulirungs-
arbeiten der Königl. Preussischen Gradmessungs - Commission, welche zur Zeit auf der Station
Barnitz bei Kemberg stattfanden, beiwohnen.

Dieses geschah auch in der Zeit vom 1. September bis 14. October 1868, und ich
finde mich veranlasst, hier des Herrn Professor Dr. Sadebeck, welcher jene Arbeiten leitete,
und Herrn Vogler’s Zwecke mit ebensoviel Güte als Sachkenntniss förderte, dankend zu

gedenken.

Der Aufenthalt meines Assistenten in Barnitz wird für die bevorstehende Bayer. Tri-
angulation nicht ohne Friichte bleiben. |

Gegen Mitte October wollte ich mich mit dem Sächsischen geodätischen Commissär,
Herrn Bergrath und Professor Dr. Weisbach, über den Anschluss des Bayerischen Nivellements
an das Sächsische besprechen, meine Amtsgeschäfte als Director der eben in’s Leben getretenen

 

 
