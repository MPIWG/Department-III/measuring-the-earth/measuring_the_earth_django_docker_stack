 

 
 

  

  

 

   
 

 

 

 

 

 

 

N Angewandte : EE : ee cn Se a ee ee
Brea. 2 e it. 2 z = Veröffentlicht. Titel der Publication. Bemerkungen.
| ue | poque . 3 3 = Publié. Titre de la publication. Remarques.
Horizontal- B. mss
Distanzen des |
Polarsternes. |
dto. BD |
dto. Be |
Horizontalwinkel |
zwischen Polaris |
und dem irdisch. |
| Objecte. Me
%  Meridianmire. | 1871, Mars .
Digressions de | 1864, Mars—Mai . Pas publiée.
la Polaire.
: : (Be
SB:
Sl P
Passageninstr. > 182? Gerling, Beiträge zur Geographie Kur-
hessens. Cassel 1839, 8. 68.
desgl. 1824—1835 Bessel und Baeyer, Gradmessung in Ost-
preussen. Berlin 1838.
e Den Danske Gradmaaling, andet Bind.
Kopenhagen 1872, p. 489.
Passageninstr. u.| 1832, 1834 Bessel und Baeyer, Gradmessung in Ost-
Univers.-Instr. preussen. Berlin 1838.
desgl. 1352... ss 5 ss 5 5 es
Passageninstr. | 1847—1848 . À see BEN nn ‚Bere Noch nicht. publieme.
Universalinstr. | 1852 x Baeyer, Die Verbindungen der Preussi-
schen und Russischen Dreiecksketten.
Berlin 1857.
desgl. 1857 . | Baeyer, Astronomische Bestimmungen für
| die Europäische Gradmessung aus den
Jahren 1857—1866. Leipzig 1873.
desgl. 1858? . Generalbericht über die Europäische Grad-
messung für das Jahr 1869. Berlin
1870, 5: 2.
desgl. 1859 Baeyer, Astronomische Bestimmungen für
die Europäische Gradmessung aus den
Jahren 1857—1866. Leipzig 1873.
desgl. 1862 ” ’ ” ” D ”
desgl. | 1863 ” 59 ” ” ” 5,
D usé
Universalinstr. 1864 ” ” „ D ” ”
desgl. | 1865 ” „ „ „ „ „
desgl. 1866 | ” D ” 2 ” 29%
Passageninstr. u.| 1869 Publication des geodätischen Instituts:
_ Universalinstr. Astronomisch-geodatische Arbeiten in
den Jahren 1872, 1869 u. 1867. Leipzig
1874.
Universalinstr. 1869 ” ”? ” 29 29 ” ek
desgl. 1869 Generalbericht über dieEuropäischeGrad- | Definitiver Werth:
messung für das Jahr 1869. Berlin 321026'12.28.
1873, 8. 44.
Passageninstr. u.| 1870 Publication des geodätischen Instituts:
_ Universalinstr. | | Astronomisch-geodätische Arbeïten im
| | Jahre 1870. Leipzig 1871.
Universalinstr. | 1870 | Generalberichtüber die Europäische Grad-

 

 

 

 

 

 

messung für das Jahr 1870. Berlin
1871, 8. 33.

 

 
