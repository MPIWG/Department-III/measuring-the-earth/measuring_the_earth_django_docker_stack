 

gene sn ses ea

 

 

66

Gewichts-Wesen den Anstoss gegeben, und es kann ihr daher nicht gleichgiiltig sein,
welchen Erfolg ihre diesem Zwecke wiederholt gewidmeten Bemühungen gehabt haben.
Wir glauben also, dass es unsere Pflicht ist, der permanenten Commission einen kur-
zen Bericht über die, Convention zu erstatten, welche im Frühjahre 1875 zwischen
siebzehn Staaten behufs der Organisation eines internationalen Maass- und Gewichts-
Büreaus zu Paris abgeschlossen worden ist und besonders die Bestimmungen darzulegen,
welche die Geodäsie und die Gradmessung betreffen.

Bekanntlich hatte die permanente Commission von der General-Conferenz in
Dresden den Auftrag erhalten, der französischen Regierung den von der Conferenz

gefassten Beschluss mitzutheilen, zu Gunsten der Zusammenberufung einer diplomati-'

schen Conferenz, welche die internationale Organisation für die Reform des Maass- und
Gewichts-Wesens sicher zu stellen hätte. Die Commission hat daher an die französische
Regierung ein Schreiben gerichtet, welches in den Protokollen von 1874 (Seite 8) ver-
öffentlicht ist und schon den 9. November hat unser Präsident, der Herr General Ibanez
von Seiten Sr. Excellenz des Herrn Herzogs Decazes folgende günstige Antwort erhalten:

Paris, 9 Novembre 1874.

Monsieur le Général, Vous avez bien voulu, en votre qualité de
Président du Comité permanent de l’Association géodésique internationale,
me communiquer, le 28 Septembre dernier, une résolution votée à l’unani-
mité par cette Assemblée et ayant pour objet de provoquer la réunion à
Paris, dans le plus bref délai possible, d’une Conférence diplomatique
appelée à régler, par voie de convention, les différentes questions qui se
rattachent aux travaux de la Commission internationale du Mètre.

J'avais déjà, vers la fin de l’année dernière, porté à la connaissance
des Gouvernements intéressés le voeu émis dans ce sens par le Comité per-
manent de la Commission du Metre. Sur la demande de ce même Comité,
je viens d'adresser à tous les Etats d'Europe et d’Amerique une convocation
officielle a leffet de se faire représenter dans la Conférence diplomatique
dont j'ai proposé de fixer la réunion au 1% Février prochain.

En répondant ainsi au désir exprimé par l'Association géodésique in-
ternationale, je me félicite, Monsieur le Général, de contribuer, autant qu'il
peut dépendre de moi, à la réalisation d’une oeuvre à laquelle le Gouverne-
ment français attache, pour sa part, le plus vif intérêt.

Recevez, Monsieur le Général, les assurances de ma haute consi-
deration.
(signé) Decazes.

Darauf ist am 1. Februar 1875 die diplomatische Conferenz zusammengetreten
und die Mehrzahl von den Staaten, welehe im Jahre 1872 an der internationalen Meter-
Commission Theil genommen hatten, zwanzig an der Zahl, waren durch ihre Minister
oder Gesandten in Paris und durch besondere wissenschaftliche Abgeordnete vertreten.
Diese Abgeordneten, zu einer Special-Commission vereinigt, wurden mit der Ausarbei-

 

 

 

ud

/

TL Le ML

La

thu te ey

1k st hh, lel did ld ely pie 114

2.) ssetihtéenns, mmasstt HA |
