qi |

rer

Tenner ET Era

rentrer

Per terre

t
&
R
5
=
=
E
.
k
gr
=
=
È
Ë
r

i

verticales sont déviées, dans la direction du méridien, aussi bien que dans celle du
parallèle, de façon à les faire converger vers le centre de la montagne; cependant il y
a des irrégularités très frappantes dont il est difficile de se rendre compte par la seule
action des masses visibles; peut-être la richesse de ces montagnes en minerais de fer,
de cuivre et d'argent, et les grandes variations de densité qui en résultent, peuvent
contribuer à expliquer quelques anomalies. On poursuit activement ces recherches.

Mr. le Comm. Perrier demande, au sujet de cette communication, Si, pour les
comparaisons & faire entre les coordonnees astronomiques et geodesiques, il ne serait
pas bon de s’entendre sur une valeur à adopter pour l’aplatissement de la Terre.

Mr. le Général Baeyer répond que pour les recherches qu'il a faites, des dé-
viations de la verticale dans le Harz, recherches qui portent sur un rayon assez restreint,
les différences de valeur dans les dimensions de l’ellipsoïde ne peuvent pas avoir d’in-
fluence sensible.

Mr. le Président remercie Son Exc. le Général Baeyer et donne la parole à
Mr. le Prof. Peters qui mentionne que l’ancien Observatoire d’Altona a été rattaché au
réseau géodésique, que les calculs sont terminés et seront publiés prochainement; pour.
l'Observatoire de Kiel, qui est en construction en ce moment, on fera la jonction l’année
prochaine.

Ensuite MM. Barozzi, le Général de Forsch, et Bruhns, présentent les rapports
sur les travaux faits en Roumanie, Russie et en Saxe. *)

Mr. le Président remercie chacun de ces Messieurs, et prie Mr. Hirsch de ré-
sumer en français une lettre de Mr. Lindhagen qui contient des renseignements sur l’état
des travaux en Suède. **)

Enfin Mr. Hirsch fait le rapport pour la Suisse***), en complétant les données
contenues dans le procès-verbal de la dernière séance de la Commission suisse. Il met
des exemplaires de ce procès-verbal, ainsi que du mémoire sur la longitude Milan-Sim-
plon-Neuchâtel à la disposition de ceux de ses collègues qui ne l’auraient pas encore reçu.

Mr. Faye invite MM. les membres de l'Association à visiter les établissements
scientifiques nouvellement créés par le Bureau des Longitudes sur les terrains concédés
par la ville de Paris dans son parc de Mont Souris.

Mr. le Président remercie au nom de l’Association.

La visite se fera le vendredi 24 septembre, à 37.

Mr. le Président propose d'ouvrir la séance de demain à 17.
L'Ordre du jour est ainsi réglé:
Séance à 1. — Continuation du programme; à 3! — visite à Mont Souris. —
La séance est levée à 4! 30".
*) Voir le rapport général.
*#) Voir le rapport général.
***) Voir le rapport général.

 
