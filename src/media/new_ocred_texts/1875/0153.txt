143 .

I wen mer oe ori

Nous ne nous étendrons pas davantage sur la solution fournie par les développe-
ments trigonométriques. D’une part, ils exigeraient, pour satisfaire à la condition
d’équidistance des points, un nombre de stations bien plus considérable que le nombre
necessaire dans le cas des développements algebriques (ceux-ci n’exigeant le rapproche-
ment des stations que dans les régions accidentées). D’autre part, il nous semble que
la formation de l’équation générale de la surface de niveau n’offrirait, pour la discussion,

aucun avantage sur les équations qui représentent cette surface dans des espaces plus
limités.

 

pa ART

LAS LIL. Z RIT LIT Je

 
