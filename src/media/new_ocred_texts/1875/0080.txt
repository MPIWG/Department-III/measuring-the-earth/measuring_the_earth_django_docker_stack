 

 

 

70

Nach einer Besprechung, welche sich über diesen Antrag zwischen den Herren
Laussedat, Hirsch, Ricci, Perrier und Bruhns entspinnt, wird derselbe .mit einigen
Aenderungen*) in der Fassung angenommen.

Hierauf kommt die Frage über Local-Anziehungen und Loth-Ablenkungen zur
Berathung.

. Herr Faye wiederholt den Wunsch, dass um den Puy de Dome herum Unter-
suchungen über Lothablenkungen angestellt werden möchten. Da die geologische Be-
schaffenheit dieses Berges vollständig bekannt sei, würden die Untersuchungen über die
Local-Attraction an diesem Punkte vortreffliche Beiträge für die Bestimmung der Dich-
tigkeit der Erde und für die Förderung der Lösung mancher geodätischer und geologischer
Probleme liefern. Diese Arbeiten würden eine Fortsetzung der von Maskelyne unter-
nommenen sein.

Die Berathung erfährt eine Unterbrechung, indem der Minister des öffentlichen
Unterrichts, Herr Wallon, das Wort ergreift, um den Dank der französischen Regierung
dafür auszusprechen, dass die Stadt Paris zum diesjährigen Versammlungs-Orte der
permanenten Commission gewählt worden ist. — Der Herr Minister drückt im Namen
des Präsidenten der Republik dessen Bedauern aus, dass er wegen seiner Abwesenheit
von Paris die fremden Gelehrten nicht empfangen könne.

Der Präsident dankt Sr. Excellenz dem Herrn Minister für das Interesse, welches
er der permanenten Commission dadurch bekunde, dass er ihren Berathungen beige-
wohnt habe. —

Herr Perrier theilt mit, dass auf dem Gipfel des Puy de Döme ein geodätischer
Pfeiler stehe, dessen Höhenlage scharf bestimmt ist, und dass auf demselben im Laufe
des nächsten Jahres Winkelmessungen und vollständige astronomische Beobachtungen
ausgeführt werden sollen; die von Herrn Faye verlangte Untersuchung wird ebenfalls,
sobald es die Umstände erlauben werden, in Angriff genommen werden.

Herr Hirsch glaubt, dass die Versammlung den Antrag des Herrn Faye nur
aufs Wärmste unterstützen kann; man wird mit Spannung den Resultaten eines solchen
Unternehmens entgegensehen.

. Herr Faye verallgemeinert seinen Wunsch und bittet, dass ähnliche Unter-
suchungen überall angestellt werden möchten, wo die geologische Beschaffenheit der
Gebirge gut bestimmt ist.

Die Herren v. Oppolzer und Bruhns erinnern daran, dass dieser Wunsch auf
Anresung des Herrn Baeyer schon auf der Dresdener Conferenz laut geworden ist.

Herr Faye erwidert darauf, dass sein Wunsch nicht derselbe sei; er verlange,
dass die Arbeit von Maskelyne in Gegenden, welche eine ähnliche Untersuchung ge-
statten, wieder aufgenommen werde.

Herr Hirsch unterstützt den Wunsch des Herrn Faye.

*) Diese Aenderungen sind bereits in der obigen Fassung berücksichtigt worden. A. Hirsch.

 

x

1

rd a A

VW

UA ml

ro |

À A cu ah te a ha Hain 2:

2. soœttéiins vamesett Vi |

 
