 

5
q
i
N
|
i

SSS ep ES TI RE ET

134

surface de niveau dans toute l’étendue d’une zone de plusieurs degrés de largeur. La
vérification de l'exactitude des résultats reposerait alors sur l’identité des cotes d'altitude
obtenues pour une même station, qui se trouverait faire partie de plusieurs zones
distinctes. Toutefois on doit remarquer que si, au lieu de diriger l’axe des zones
suivant une ligne géodésique, on infléchit l’axe de la première zone parallèlement au
contour de la première circonscription, les deux extrémités de la zone coincideront, ce
qui fournira la vérification la plus directe; en outre, la zone et l’espace intérieur déjà
déterminé auront de nombreux points communs qui fourniront autant de vérifications
distinctes. Dans cette manière de procéder, sil arrive que, dans une région, il soit
nécessaire de faire de nouvelles stations astronomiques, la partie des calculs qu'il
faudra reprendre se trouvera limitée à cette même région. En continuant ainsi, on ne
sera pas exposé à reprendre un ensemble de calculs déjà effectués, et l’on sera en
possession d’une base solide, sur laquelle pourront s'appuyer avec sécurité les déter-

minations relatives aux zones extérieures.

Dans ce travail, nous ferons usage des développements en séries ordonnées
suivant les puissances et produits des différences des coordonnées des points considérés
et du point central. Comme on peut employer les coordonnées angulaires et les coor-
données linéaires, il est clair que les développements pourront s’obtenir sous des formes
différentes.

1° Emploi des coordonnées angulaires. — En conservant les notations de la
Communication du 2 octobre 1871, et négligeant les termes du second ordre par rap-

port aux attractions locales, on réduit l'équation différentielle (18) de la surface du
pP q

niveau à
22

aC
équation dans laquelle on peut remplacer V et acos / par leurs valeurs en fonctions

de L; les équations (13) donnent a cet effet

sin (L' — L) dL — acosi cos L sin(£@ — £) d£,

>

Ba. V2 Se a? cos*L a 67 Sil liga COs ki - cos L.

Soient L; et £, les latitude et longitude géodésiques d’un point pris dans le
voisinage du centre d’un groupe de stations peu éloignées, L et £ désignant les coor-
données de l’une quelconque des autres; nous poserons

Besen be LL.
a? mur
oe m ce Lsn(! —£) = — “ya sin (Li — L);

en conséquence, l’équation différentielle (36) deviendra
Bee DIN == 4 dt Foods
et la condition que cette différentielle soit exacte sera
dr ds
u ue om Re

vr LOT LIL ET TUE L'ILE |

all mil

ur |

 

|
|

 

 
