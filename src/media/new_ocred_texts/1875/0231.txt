Te man we oy

 

a al

[OPA A BL EL fi dun

x

 

Stations dangles. Depuis la Conférence de Dresde, 21 stations du 1% ordre
ont été faites, 17 signaux en maçonnerie ont été érigés et tous les calculs relatifs à
chaque station isolée ont été terminés pour 29 sommets de notre réseau.

Nivellements de précision. La ligne Segovia — Avila— Toledo — Madridejos, qui
forme un grand polygone de 518 kilomètres avec la ligne Madridejos—Madrid—Ségovia,
déjà nivelée, a été complètement terminée avant la fin de l’année 1874, ayant atteint
l'altitude de 1415 mètres. Durant l’année actuelle les lignes A/bacete — Ubeda — Bailen
et San Chidrian — Valladolid — Benavente— Lugo ont été également nivelées a double;
cette dernière à fourni l'altitude de la base de Lugo sur le niveau moyen de la
Méditerrannée à Alicante. Le travail de nivellement double, fait depuis mon dernier
rapport à Dresde, est de 1000 kilomètres avec 1018 repères dont 210 en bronze.

Maréographes. Celui d’Alicante a déjà fourni les courbes diurnes pendant
18 mois et leur relevé, au moyen du planimètre, se fait au jour le jour.

À l'entrée même de la baie de Santander, sur un emplacement très favorable à
un maréographe, mais qui a entraîné de nombreuses difficultés pour les constructions
nécessaires, un appareil de grandes dimensions à été monté tout récemment et il
commencera bientôt à fonctionner, en même temps qu’une station météorologique.

Enfin je ferai commencer immédiatement les travaux pour installer un troisième
appareil près de Cädiz, station exceptionnellement intéressante par sa situation voisine
du détroit de Gibraltar, et dont l’importance augmentera encore, en combinant avec
elle et avec la station d’Alicante, un 4°™¢ maréographe que je me propose d’établir pres
de Rosas 4 Vextrémité nord de notre cote orientale.

En résumant tous les travaux terminés, 2 bases ont été mesurées; les obser-
vations des directions azimutales et des distances zénithales ont été faites sur 362 sommets
du 1° ordre, et tous les calculs sont terminés, en tant que chaque station est considérée
isolément; une ligne qui traverse la Péninsule et plusieurs polygones sont nivelés
à double, ce qui a nécessité un nivellement de précision simple de 5000 kilomètres
de ‘longueur avec plus de 2500 repères; 2 maréographes sont établis, Pun sur la
Méditerranée, l’autre sur l'Océan; on a fait des observations de latitude et d’azimut a
5 sommets du réseau; tous les instruments nécessaires pour la détermination des
différences de longitude et de l'intensité de la pesanteur ont été commandés et la
plupart sont déjà à l’Institut géographique; le 1* volume des Mémoires, que jal
l'honneur de vous offrir, contient, entre -autres, une partie du réseau géodésique, le
commencement des nivellements de précision, des comparaisons de règles et quelques
déterminations de latitudes et d’azimuts.

Telle est l'oeuvre accomplie. Il nous reste à en terminer une partie relative-
ment peu considérable, et à multiplier les travaux astronomiques pour qu’ils soient en
rapport avec l'étendue du réseau géodésique. Mais l’Institut géographique ayant été
tout récemment chargé de cette tâche importante, il est à espérer que, la période de
