IN we verein en

 

IT TER I Im an ont

i

ii

PRE A Bae TRI TIME

11104

À

Les différences de longitude déterminées astronomiquement sont celles des
observatoires de Greenwich et Bruxelles:
172889 en temps,
et des observatoires de Berlin et de Bruxelles:
36655 en temps.

Pour des raisons étrangères à la science, les astronomes du Dépôt de la Guerre
n'ont pu s'établir à l’observatoire et y déterminer un azimut. Ils ont dû prendre leur
station à la tourelle Est de l’église St. Joseph, dont la longitude a été déduite de celle
de lPobservatoire à l’aide d’une petite triangulation particulière. — Longitude orientale
de St. Joseph 7737 en are de la division sexagésimale.

L’almanach séculaire publié en 1854 par le Directeur de l’observatoire royal
de Bruxelles, fixe la latitude de cet établissement & 50°51'10’5, mais, pour des raisons
consignées & la page 712 du compte rendu des observations astronomiques, publié en
1867, l’on à adopté au Dépôt de la Guerre: |

50051. 10.63,
et Von a calculé géodésiquement la latitude de St. Joseph trouvée de:
50050'37:61.

Ces valeurs de la latitude et de la longitude de la tourelle Est de l’église de
St. Joseph ä Bruxelles sont les éléments de départ de toutes les coordonnées géodésiques
des sommets de toute la triangulation qui sert de base à la construction de la carte
du pays. Les longitudes sont comptées de l’observatoire.

En 1855, on a déterminé la latitude astronomique du signal de Lommel,
extrémité Nord de la base calculée dans le Limbourg et dont la direction générale est
à peu près perpendiculaire à la base mesurée (2300257213).

Cette latitude est

| 51010'38.92,
a une demi seconde environ d’approximation.

À l’Ouest du pays, on à pris pour station astronomique la tour des Templiers
a Nieuport. C’est un point de premier ordre et l’un des sommets du quadrilatère
Ostende — Ghistelles — Dixmude — Nieuport qui renferme la base d’Ostende et la base
Zandvoorde.

 

calculée Raverzyde
Des raisons d'installation ont seules motivé ce choix. La latitude observée est:
5107 90/09
avec une erreur moyenne de = 0717.
Trois azimuts ont été déterminés astronomiquement savoir:
1° Azimut du cote Lommel (signal) — Camp (signal) de
20045 2179
compté du Sud vers l'Ouest.
2% Azimut du côté Nieuport — Raverzyde de
52°27 ot
compté du Nord vers l’Est.

 
