 

a

wh (hem mR A) Ame an | u di ul

Ajoutant cette expression avec le développement (43) de 6, il vient

5 — © dé = 6; as ys? + vs + ee
8

d’où

 

 

a d BB 1
fe — [LE a) == 6:8 + en 5 ne es
on a d’ailleurs, en vertu du l’équation (49),
| l ua 0 2 À 1 3
D oc LT. Pt on t+ xst tl et
+ o
ar

1
ye dsl a en =
La somme de ces deux expressions fournit, suivant la formule (42), lintégrale

 

 

cherchée :
| if I, 1 do. x

A ee aes on un Uri Gee
9 E
A7) : E
( ) 1 ns aL a il à 1 1 1 =
eee ate OF en eg. Ei

Od €

Dans les applications numériques, les quantités s et ¢ seront exprimées par
leurs valeurs en nombres abstraits ou en rapports d’arc au rayon.

20 Emploi des coordonnées linéaires. — Pour faire comprendre comment on est
conduit à choisir les coordonnées dont il s’agit, nous ferons subir quelques transfor-
mations à l'équation différentielle (36).

Désignons par & le rayon de courbure de la section méridienne au point (L, £ ),

et ® le rayon du parallèle de latitude L, dont la valeur est y 22+ y?, nous aurons

ae ae
(48) ee ad ohne alae te wa — à, C003 À À,

relations dont la seconde se déduit de la première équation (12).

 

 

 

*) P. 8. Soit x, — y x° + y”, on aura

 

 

 

 

 

hi ( in de?
Su
; ini)
f R= + ——
Eu N
: den?
a
| ou x, et 2, ainsi que leurs différentielles, sont liés par les relations
4 002 2” Lax, edz dx? az? 2 d?z
a + ol, 3 2 = 0, Se 0
| a C a c a € C
| de là on déduit, en ayant égard a l’équation (9),
i ae dz? fhe c‘ x? 4 ( 2 gi a ei
b ay ot 1e — ail ya na} == ya
D dx? a* z 2” \a c be
4 d’z 1 (5 de? 0 ( Cr $
1 — = — - sr LE = — ı;
i ‘ an? N aa) a° 8 | >) ae
| et il vient ;
| 2 22
GC. i
KS = |
|

 

 
