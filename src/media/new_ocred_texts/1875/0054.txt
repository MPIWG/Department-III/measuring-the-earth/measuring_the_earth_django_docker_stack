 

 

4

worden ist, hat sich in ihrer Sitzung vom 28. September constituirt, indem sie den
Herrn General Ibarez zum Präsidenten, den Herrn Professor von Bauernfeind zum
Vice-Präsidenten und die Herren Bruhns und Hirsch zu Schriftführern. erwählt hat.
Nachdem die französischen Commissare im Namen der französischen Regierung die per-
manente Commission eingeladen hatten, sich dieses Jahr in Paris zu versammeln, hat
die Commission diese freundliche Einladung einstimmig angenommen und sie bezeust
durch die vollzählige Gegenwart aller ihrer Mitglieder den hohen Werth, welchen sie
der thätigen und fruchtbringenden Mitwirkung Frankreichs beimisst. Se. Excellenz der
Herr Minister des öffentlichen Unterrichts hat in einem Briefe, welchen er im Monat
April an das Präsidium gerichtet, das Interesse ausgedrückt, welches die französische
Regierung an das wissenschaftliche Unternehmen knüpft, welches unsere Commission
verfolgt, indem sie die Kräfte aller civilisirten Länder vereinigt. Seine volle Unter-
stützung uns versprechend, hat uns der Herr Minister angeboten, unsere Sitzungen in
dem Hotel des Ministeriums der auswärtigen Angelegenheiten abzuhalten. —

Die permanente Commission erfüllt eine traurige Pflicht, indem sie an den herben
Verlust erinnert, welchen unsere Gesellschaft kürzlich durch den Tod zweier ihrer Mit-
glieder, des General Dufour in Genf und des General Folque in Lissabon, erlitten hat,
welche beide durch ihre schönen Arbeiten bekannt und durch die Mitwirkung, welche
sie unserem Werke gewidmet haben, bei uns in bleibendem Andenken stehen.

Die permanente Commission hat ihre Schriftführer beauftragt, die Berichte über
die vierte General- Conferenz auszuarbeiten und sie durch das Central- Büreau drucken
zu lassen. Wir haben uns dieses Auftrages entledigt und die Protokolle, welche zugleich
als General-Bericht. für 1874 dienen, sind in zwei Sprachen veröffentlicht ; sie sind be-
reits seit einiger Zeit in Ihren Händen.

Die Commission hat auf die beiden Briefe Antwort erhalten ‚ welche sie an die
französische Regierung in Betreff der Meter-Frage, und an die preussische wegen Be-
willigung eines für das Central-Büreau bestimmten Gebäudes dem übernommenen Auf-
trage gemäss gerichtet hatte. Die diplomatische Meter-Conferenz hat sich im Frühjahre
versammelt und in Paris ein besonderes Institut für Maass und Gewicht gegründet,
welches bestimmt ist, den Wissenschaften im Allgemeinen und der Geodäsie im Besonderen
grosse Dienste zu leisten. Die preussische Regierung hat kürzlich geantwortet, dass sie
gewillt ist, dem Central-Biireau eine Räumlichkeit für die Maassvergleichungen und für
die Aufstellung der Instrumente zu bewilligen und dass sie bereits die bezüglichen An-
ordnungen getroffen ‘hat.

Zur Ausführung der in Dresden gefassten Beschlüsse in Bezug auf die An-
fertigung von Tabellen über die astronomischen und geodatischen Coordinaten, ferner
eines Registers der in den einzelnen Ländern erschienenen geodätischen Publicationen,
und einer Generalkarte der Dreiecke und der Nivellements, hat das Central-Büreau und
die permanente Commission Circulare an die Commissare erlassen, und letztere haben
theilweise auf die an sie gerichteten Fragen geantwortet: Herr Hirsch wird über diesen
(segenstand einen besonderen Bericht erstatten.

 

=i
<
=
=
=
a
=
a
=
=
x
Y

Lil

[il

ah mh

 

2. nnd wu Sih |

 
