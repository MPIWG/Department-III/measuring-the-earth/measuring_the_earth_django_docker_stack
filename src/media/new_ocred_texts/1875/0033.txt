 

par TANT

ART

rn Tg u

=
=

23

90 [a Commission recommande aux observateurs de rechercher si, dans leur in-
strument, les tranchants des couteaux peuvent être identifiés avec les axes de rotation,
et de déterminer, au besoin, les distances entre les axes et les tranchants des couteaux.

30 Il est désirable que les appareils employés dans les différents pays puissent
être comparés entre eux en les faisant osciller dans la même station, pour laquelle on
propose le point où Bessel a déterminé dans le temps la pesanteur et qui se trouve dans
le Bureau actuel des poids et mesures, x Berlin. :

40 En outre la Commission approuve, dans l'intérêt de la science, le projet de
Mr. Pearce de déterminer à nouveau la pesanteur, au moyen de son pendule à réver-

s les trois stations importantes pour l’histoire de ces recherches, où Bessel

sion, da
Borda et Matthieu en France et Kater en Angleterre ont fait leurs

en Allemagne,
déterminations fondamentales.

Ces quatre propositions sont successivement mises aux voix et adoptées.

Mr. le Général Ricci demande la parole pour rappeler qu’en 1869 la Commis-
Association géodésique avait émis le voeu que l'Italie pût faire

sion permanente de P
le Prof. Govi pour, la mesure de la longueur

l'essai de la méthode proposée par Mr.
du pendule simple. Or, par suite des évènements qui ont eu lieu depuis ce temps - là,
il n’a pas été possible d'exécuter ces expériences. Mr. le Général demande si la Com-
mission est toujours du même avis à l’egard de l'essai à faire de cette méthode.

Mr. le Président répond à Mr. le Général Ricci que le voeu formulé par la
Commission en 1869 demeure toujours le même et que l'Association serait heureuse
de le voir réalise.

Mr. le Commandant Perrier, rapporteur de la Commission spéciale pour l’appa-
reil des bases, lit le rapport suivant:

Dans la 3e séance de la 4e Conférence géodésique internationale, le 25 sep-
tembre 1874, une commission composée de six membres a été adjointe 4 Mr. le Gene-
à meilleure construction à donner à Pappareil des bases et
des propositions motivées à la Commission permanente.
dont tous les membres étaient présents à Paris, à

ral Baeyer pour étudier |
pour soumettre le plus tot possible

Cette commission spéciale,
l'exception du Colonel Ganahl empéché, s’est r&unie les 21 et 23 septembre, sous la
Présidence de Mr. le Général Baeyer.

Elle a admis, à l'unanimité, après une courte discussion, qu'il était nécessaire
n compris de l'Association, de faire construire un appareil
Un appareil de cette nature pourra,
servir à la mesure des bases dans

et opportun, dans l’interet bie
international pour la mesure de bases géodésiques.

en effet, ainsi que l’a dit excellemment Mr. Hirsch,
sont pas pourvus d’un appareil spécial et permettra ainsi de

les petits pays qui ne
sie européenne; il offrira, en outre,

combler plusieurs lacunes importantes dans la Géodé
le précieux avantage de rendre possible la répétition de mesures déjà faites, et, par la
suite, la comparaison des résultats déjà obtenus au moyen des différents appareils
nationaux avec ceux que donnera l’appareil international, d’où résultera, Si Von peut

s'exprimer ainsi, l'équation des bases des différents pays.

 
