 

 

Annexe 2,

CONVENTION DU METRE

signée à Paris le 20 Mai 1875

par les Plénipotentiaires de l'Allemagne, Autriche-Hongrie, Belgique, Brésil, Confédéra-
tion Argentine, Danemark, Espagne, Etats-Unis d'Amérique, France, Italie, Pérou, Por-
tugal, Russie, Suède-Norvége, Suisse, Turquie et Vénézuéla.

ARTICLE PREMIER.

Les Hautes Parties contractantes s’engagent à fonder et entretenir, à frais com-
muns, un Bureau international des poids et mesures, scientifique et permanent, dont le
siége est à Paris.

ART. 2.

Le Gouvernement français prendra les dispositions nécessaires pour faciliter
l'acquisition ou, s’il y à lieu, la construction d’un bâtiment spécialement affecté à cette
destination, dans les conditions déterminées par le Règlement annexé à la présente
Convention.
| ART, 3. A

Le Bureau international fonctionnera sous la direction et la surveillance exclusive
d’un Comité international des poids et mesures, placé lui-même sous l’autorité d’une
Conférence générale des poids et mesures formée de délégués de tous les Gouvernements
contractants.

ART. 4.

La présidence de la Conference générale des poids et mesures est attribuée au

président en exercice de l’Académie des sciences de Paris.

ARE. D.

L'organisation du Bureau ainsi que la composition et les attributions du Comité

international et de la Conférence générale des poids et mesures sont déterminées par

le Reglement annexé a la présente Convention.

e

 

x
=

ULLI. IT]

7 Ian 7}

11 Led

Le mul

Lai do HA

jt mel te a dit a dde ad su py ie 11:

2. seit sais. masses NA |

 
