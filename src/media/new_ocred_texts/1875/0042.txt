 

 

 

SIXIÈME SÉANCE.

Paris, le 27 Septembre 1875.

Présents: MM. les Délégués: Adan, Baeyer, von Bauernfeind, de Barozzi, Bruhns,
Faye, Ferrero, de Forsch, Hirsch, Ibanez, d’Oppolzer, Perrier, Peters, Ricci, Saget,
de Vecchi, Villarceau, — et parmi les invités: MM. Bassot, Bullot, Bouquet de la
Grye, Breguet, Bréton de Champ, Chasles, Govi, Laussedat, Peirce, de la Roche-Poncié.

Presidence de Mr. le Göneral Ibanez.

MM. Bruhns et Hirsch fonctionnent comme secrétaires.

Mr. Hirsch donne lecture du Procès-verbal, qui est mis aux voix et adopté.

Mr. Bouquet de la Grye expose combien il serait utile de mesurer les varia-
tions de la pesanteur; il a pu constater les variations à l’île Campbell, lors de son
expédition pour le passage de Vénus, et il a construit, pour les déterminer, un appa-
reil qui à figuré à l’exposition des sciences géographiques.

La discussion est ouverte sur le § 5 du programme.

Mr. Hirsch dit que tous les ouvrages importants qui ont paru dans le courant
de Vannée ont été distribués soit antérieurement par l’intermédiaire du Bureau Central,
soit pendant la session par les auteurs, et qu'il ne croit pas par conséquent qu'il y
ait lieu de discuter sur ce point.

Sur le $ 6: „presentation du cercle azimutal du Depöt de la Guerre fran-
gaise“, Mr. le Commandant Perrier dit que cet appareil, construit par les freres Brunner
à Paris, a été mis sous les yeux de l’Association, lors de l’excursion à Montsouris: il
veut simplement insister sur les points suivants qui lui paraissent constituer de grands
avantages pour les instruments destinés & mesurer des angles azimutaux:

19 la fixité et la stabilité de l’appareil ;

2° son mode d'éclairage zénithal ;

3° Vemploi de la vis micrométrique à l’oculaire.

Mr. le Général Baeyer reconnaît les grands avantages de l'instrument pour
l'éclairage, et il est décidé à essayer de munir ses instruments de ce système d'éclairage.

Quant au fil mobile de l’oculaire, il est d'avis que, lorsqu'il s’agit de diminuer
l'importance des erreurs de pointé par rapport aux erreurs de lecture, la méthode
paraît bonne. Il veut également essayer ce système. Il fait remarquer Cependant
que, d’après son expérience, l’image héliotropique montre, à côté des oscillations rapides,
un autre mouvement lent d’une période de 15 à 20% et d’une amplitude considérable,
qui s'est élevée quelquefois jusqu’à 10 ou 15 secondes d’arc. — Selon lui, l'influence
beaucoup plus dangereuse de ces déplacements se trouve moins détruite par les pointés
rapides au micromètre que par l’augmentation du nombre des observations.

 

3
=
à
a
=
a
x
=
ü
ü

m

til

LM nike il

to il

 

2. stations wu Sih |

 
