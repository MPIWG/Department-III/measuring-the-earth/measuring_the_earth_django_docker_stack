 

1

=
a
a
=
i
=

10

pee ed 118084011180

Mr. le Président remercie Mr. le Général Bacyer et les Secrétaires de ces rap-
ports. Il propose de consacrer la journée de demain aux séances des deux commissions
spéciales chargées de l'étude des questions de l’appareil de base et du pendule, afın
qu'elles puissent faire des propositions dans une des prochaines séances. Il fixe la
séance de la Commission des appareils de base à Mardi à 10" du matin et celle de la
Commission du pendule à 2*; tous le membres de la réunion qui s'intéressent particu-
lierement à ces questions sont invités à assister à ces délibérations.

La prochaine séance de la Commission permanente est fixée à Mercredi, à 22:
elle sera consacrée à la communication des rapports des délégués sur les travaux ac- :
complis dans les différents pays. ;

La séance est levée à deux heures et demie.

Le outil

SECONDE SÉANCE.

Lune M |

Paris, le 22 septembre 1875.

La séance est ouverte à 2 30m.

Présents: MM. les Commissaires: Adan, Barozzi, de Bauernfeind, Bruhns, Faye,
Ferrero, de Forsch, Hirsch, Ibanez, d’Oppolzer, Perrier, Peters, Ricci, Saget, de Vecchi,
Villarceau. Assistent en outre d’entre les invités: MM. Banderali, Bassot, Billot, Bou-

quet de la Grye, Breton de Champ, Bugnot, Delesse, Laussedat, Mahmoud Bey, de la
Roche- Poncié.

Prösidence de Mr. Ibanez.

MM. Bruhns et Hirsch fonctionnent comme Secrétaires.

Le procès-verbal de la première séance est lu et adopté.

L'ordre du jour porte: — Rapports des délégués des différents Etats.

Mr. le Général Baeyer se fait excuser de ne pouvoir assister à la séance.

Mr. Hirsch distribue, au nom du Général Baeyer, une notice intitulée: „Ueber
Fehlerbestimmung und Ausgleichung eines geometrischen Nivellements.“ (Sur la deter-
mination des erreurs et sur la compensation des nivellements de précision.)

ml Le, M LL th ad mn 1

Ï ii

Sur Pinvitation de Mr. le Président, MM. von Oppolzer, von Bauernfeind et Adan

présentent, l’un après l’autre, les rapports sur les travaux accomplis en Autriche, en
Bavière et en Belgique. *)

*) Voir ces rapports à leur place dans le rapport général.

ass ann, name si |

  
