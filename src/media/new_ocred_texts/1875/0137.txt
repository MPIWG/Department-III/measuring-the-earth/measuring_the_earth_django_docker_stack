a ee

 

IL RT

it

Mi

era BR RT je Twin

1

x

Effectuant les deux intégrations restantes, il vient finalement, d’après la formule (3),

ae ghee Sie hy E
à (A. — Lyo) = % £2 JE sin de oe
; 7

qt

+ 22 ( 1 gin oh; cose {= —= cos 2: L cos EL

: ê a

(26) P Q

+ — + singL sind £ — © cos cL sin ‘£)
4 4

a

2 ie
+ OL+22 sin«L — 2 — cos:L,

°

 

expression dans lagnelle £ et L en dehors des signes sin et cos doivent être exprimés
en nombres abstraits.

=. est la constante de l’intégration, que l’on obtiendra en fixant une condition

telle que serait celle du minimum de la somme 8A? *).

Nous rappellerons que, d’après notre premier théorème sur les attractions
locales, les longitudes peuvent être généralement remplacées par les azimuts. Or,
suivant ce théorème, on a, quelles que soient ces attractions,
QT}. » 2 2.2... 242 Aut ı,
en désignant par Z' et Z les azimuts astronomique et géodésique. On effectuera la
substitution dont il s’agit, en posant (19).

cos À

On observera seulement que cette fonction sera mal déterminée dans les régions

équatoriales; c’est qu’en effet, à l’équateur, l'influence des attractions locales sur les

: LZ’ —Z : aa wa
azımuts etant nulle, le rapport y devient indetermine.

Au reste, l’emploi des azimuts, dans le probléme actuel, ne parait pas offrir
autant de garanties de précision que celui des longitudes, attendu que les erreurs des
azimuts géodésiques doivent croître plus rapidement, avec la longueur des lignes
géodésiques, que celles des longitudes.

La formule (26) contient les termes ¢) £ et Cy L, dont la présence peut
surprendre tout d’abord; il est cependant facile de s’expliquer leur existence: si, par
exemple, les constantes linéaires des calculs géodésiques ne sont pas tout a fait exactes,
les extrémités des lignes géodésiques détermineront des longitudes, latitudes et azimuts
dautant plus erronés que ces lignes seront plus étendues; en un mot, les coordonnées
et azimuts géodésiques seront affectés d’erreurs systématiques, ou croissantes avec
L'et £. Si donc le calcul des coefficients des fonctions F et f conduit à des valeurs

*) L’ordonnée A est identique avec la différence A—Ah’ que nous avons considérée dans notre
Communication du 28 décembre 1868.

 
