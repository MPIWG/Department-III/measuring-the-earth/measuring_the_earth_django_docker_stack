MORE

À PS PATTES mL |

Im aan

TET RT

IR
=
=
=
=
x
=
Re
®
=
=
=

 

131
dans le voisinage des observatoires, surface à laquelle les couches atmosphériques de
méme densité sont paralléles, n’exerce aucune influence sur les réfractions astro-
nomiques *). Sous ce rapport, une étude analogue à celle que M. Schweitzer a faite
aux environs de Moscou, et qui s’étendrait à une surface de 100 à 120 kilomêtres de
rayon autour de Paris, serait d’un certain intérêt pour notre Observatoire.

Il y a plusieurs années, j’ai obtenu, relativement à la région caucasienne, un
résultat que je me suis abstenu de publier, parce qu’il repose sur des données très
incomplètes: aussi le présenterai-je aujourd’hui, non comme l'expression d’une réalité,
mais seulement d’une possibilité.

Le colonel Chodzko aurait constaté, dans le Caucase, des différences entre les
latitudes astronomiques et géodésiques, variant de D4 secondes, dans une amplitude
d'arc de méridien moindre qu’un degré: voilà, certes, un système de données bien
incomplet. Pour en déduire un résultat, il fallait nécessairement le compléter par des
hypothèses. Voici celles que j'ai faites: 10 le point où les latitudes astronomiques et
géodésiques s'accordent est au milieu de larc considéré; 2° le chiffre 54 secondes est
la valeur maximum des attractions locales en cette région; 3° la fonction qui exprime
leur effet sur les latitudes est impaire et assujettie a la condition de s’accorder
sensiblement avec la loi de la raison inverse du carré de la distance, quand les
distances au milieu de lare méridien sont tres grandes. En conséquence j'ai admis la
relation

ii b= 6 u mo.
Ly? oe OL Fee da

ot. Ly désigne la latitude du milieu de l’arc, et C et y deux constantes que les conditions

9

joo

t

N

énoncées déterminent.
À désignant l’altitude d’un point du profil méridien de la surface de niveau, dont

la latitude est L, @ le demi-axe équatorial et e l’excentricité de Vellipse méridienne, on a
a (1 — e?) CL — I)

 

 

es 2 dl;
ne C2 + (L— 1%)?”
d’où, en intégrant,
= A = FO — RE
2 Vv?+(L—-Lo)2

*) Qui sait si les réfractions calculées en ayant égard à la vraie figure de la surface de niveau
n’atténueraient pas sensiblement les discordances systématiques que présentent les meilleurs catalogues
d'étoiles? Il faudrait, pour cela, que la correction qui en résulterait pour les réfractions püût atteindre
1 à 2 secondes. Nous nous réservons d'examiner cette question une autre fois.
**) En posant
L — L,
tang y = ——"
Y

on obtient les formules suivantes, qui sont plus appropriées au calcul numérique:

4 1

és ee
L' — L — a sinwcos?y, A =A, — 2a (1 — e?) -—— sin? 1” sin? $4,
v y

L,, L, L‘ sont supposés exprimés en secondes.
Lis
