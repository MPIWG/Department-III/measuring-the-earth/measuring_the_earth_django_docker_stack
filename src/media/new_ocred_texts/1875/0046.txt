   

14

30

vn ea md A |

une carte générale de triangles: la premiere fois, l’échelle était trop grande, l’autre fois
elle était trop petite. — Il montre les difficultés pour l’exécution d’une telle carte: il
trouve suffisant, pour le moment, que les différents Etats tiennent leur carte de triangles
à jour; — ce n’est que plus tard, quand les travaux seront terminés, que l’on pourra
construire une carte générale, par sections.

Mr. Hirsch appuie la proposition du Général de Vecchi, parce qu'il ne s’agit pas,
pour le moment, d’avoir une carte exacte, mais seulement d’avoir un ensemble approché.

Ce qu'il y à de mieux à faire, c’est de s’en tenir à la proposition déjà adoptée
sur ce sujet.

Cette opinion ayant prévalu, la discussion est close sur ce point. ;

Mr. Villarceau revient sur la proposition de Mr. le Major Adan et demande s’il
ne convient pas de renvoyer à une Commisson spéciale la question du choix d’un point
central, servant de point de départ pour les calculs géodésiques. — Il montre l’im-
portance de cette proposition.

ii

Wey

Mr. le Général Baeyer estime qu'il est impossible de choisir le point central
dans l’état actuel des travaux, les chaînes n’étant pas encore rattachées les unes aux autres
et leur calcul n’étant pas définitif pour la plupart; il ne croit pas le moment venu de
donner une solution à cette question.

La tu

 

ko |

Mr. Villarceaw se contente alors de demander qu’on hate les travaux et les
études pour arriver à la réalisation de son voeu.

Mr. le Comm. Perrier fait connaître que Mr. Glüssener de Bruxelles a envoyé
à Paris un appareil servant à l’enregistrement des passages. Ce chronographe est dé-
posé chez Mr. Rumkorf, où on peut le voir.

Mr. le Président annonce que les Procès-verbaux de cette session seront publiés in
extenso, en même temps que le Rapport général du Bureau Central ; on invitera les délégués
absents à envoyer leurs rapports à la fin de l’année, et on priera les membres présents
de compléter leurs rapports présentés en séance, en ajoutant les travaux qui auront été
accomplis dans le courant des trois mois qui restent.

Pour le choix de la ville où l’on se réunira l'an prochain, Mr. le Président
demande si la Commission permanente veut prendre cette décision tout de suite, ou si
elle veut la renvoyer au printemps prochain, pour être prise par correspondance.

Par 8 voix contre 1 la décision est renvoyée au printemps.

Le programme de la session étant épuisé, Mr. le Président, avant de clore la
séance, prononce, au nom de la Commission, les paroles suivantes:

„Qu’il me soit permis d’exprimer mes remerciements aux membres de cette ré-
union qui, par leurs rapports, leurs communications et leurs propositions, ainsi que par
la part qu'ils ont prise à nos débats scientifiques, ont contribué à l’avancement de
loeuvre que nous poursuivons. Je remercie également tous les membres invités qui
ont honoré nos travaux de leur présence et ont pris part à nos délibérations.

Avant de clore la session, je vous ferai une proposition qui, j'en suis certain,
aura l’assentiment général:

 

1
q
|
|

2. shit mamie Sih |

 
