 

 

 

140

Boe tier temt sept thpm th +...

(2) —btep-+2tm-+ hp? + 2kpm+31m?-+...,

dé à . 11

a ap + 2c’ p* +e pm+...,
(EF) =a b+ Oe —e) p+ 20m + BEN
NY /
+ 2h — 2k) pm + (k — 3) m? +...,
eo DR EH PEW mE BOP + pm pi mt...

Or ces deux derniers développements devant être égaux, en vertu de l’équation de
condition (63), quelles que soient les valeurs des variables p et m#, on en déduit, entre
les trois suites de coefficients, les relations

à D 26 — © =a. ee Ip,

(65) 4 11 1 if 1 € art
sa ho, 2b De De, k -3l=1T...

 

7 1?

Substituant dans le développement de ¢ les valeurs de ¢;, a’, b’, €
que donnent ces relations, on obtient cette expression

9 exes,

I BZ 5 e)p +(e —2f)m
a

ee eh. die a
(66) | aa. h) p? + (h’ — k) pm

+ (kK — 31) m?+....

Les inconnues @, w;, a, b, c,...., a, b, ¢,... s’obtiendront en résolvant
Wane, 3 ‘

simultanément les deux premières équations (64) et l'équation (66), au moyen d’un
nombre suffisant de systèmes de valeurs censées connues de &, u et & Dans ces
équations **), les quantités observées sont engagées sous une forme qui rend immédiate-
ment comparables les erreurs de leurs premiers membres. La possibilité de repré-
senter les données au moyen d’un nombre beaucoup moindre de coefficients offrira
un moyen de contrôle auquel concourront à la fois les longitudes, les latitudes et les
azimuts.
Soit
M, ,r.
d’où
; 1 Sn Ri 1
M = w; + bp + Dm + soe ae er f TE ay EB

+ k p?m + 3lpm?+YV mi +...

V

4 “a } a ARE I a
*) P. $. Pour utiliser les Tables géodésiques, on pourra remplacer ici a par

N°
™) BP. S. Ajoutez : et en y supposant la dernière multipliée par a cos L.

 

td lke

Meee

wit bea

ah nl Il

I Ihe ill

ist dh ki u a nn 11:

2.1 2a, wma Su |

 
