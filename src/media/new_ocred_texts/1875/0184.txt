 

nn

174

4

Le coefficient de la flexion, calculé par l’ensemble des observations est égal
à — 0.96. ?
J’ai adopte pour la latitude d’Alger
00/40/2075 O15:

 

5° Nouvelle détermination de la Méridienne de France.

Les opérations relatives à la nouvelle méridienne de France ont été continuées,
pendant la campagne de 1875, par Mr. le Commandant Perrier, assisté de Mr. le
Capitaine Bassot; elles ont été poussées du Sud vers le Nord, entre les parallèles de
Clermont et de Bourges de l’ancienne triangulation, depuis le côté Puy de Gué— Royère,
jusqu’au côté Humbligny— Ir Charité. Elles comprennent les observations azimutales
faites aux stations de

Sermur, St. Amand,
Toulx Ste. Croix, Beaux Vents,
Arpheuille, Bourges,
Cullan, Humbligny,

Le Vilhain,
ce qui porte à 38 le nombre des stations déjà faites depuis la chaîne des Pyrénées.
En outre, la station astronomique de Saligny le Vif sur le parallèle de Bourges, a été
rattachée à la méridienne.

Les stations de Cullan, le Vilhain, Arpheuille, St. Amand et Saligny le Vif
sont l’oeuvre de Mr. le Capitaine Bassot qui m’a remplacé sur le terrain pendant la
durée du Congrès géographique.

Une erreur de caleul nous empéche de faire connaître aujourd’hui même la
différence obtenue pour Saligny le Vif, entre les coordonnées astronomiques directes
et les coordonnées géodésiques.

Ce qui caractérise plus spécialement la campagne de 1875 en France, c’est
l'introduction , dans les opérations géodésiques, des observations de nuit; je me borne
à indiquer le fait sur lequel j’appellerai la discussion en présentant un mémoire spécial
à lAssociation dans une de nos prochaines séances.

°

Il me reste enfin à faire connaître a l’Association que le Dépôt de la Guerre
vient de faire construire, dans le parc de Montsouris"), un petit observatoire permanent
d’astronomie geodesique, qui sera utilisé pour la mesure des différences de longitude
que l'Etat major français se propose d'entreprendre sur son territoire. — Pendant la
campagne de Vhiver prochain, des observations astronomiques seront faites en Algérie,
par MM. Perrier et Bassot, non loin des frontières de la Tunisie et du Maroc, afin
d'obtenir l’amplitude de l’arc de parallèle algérien et nous espérons que, l’année

1) Sur un emplacement concédé par la Ville de Paris au Bureau des Longitudes, qui y a déjà
fait établir des observatoires spéciaux pour les besoins de la géographie, de l’astronomie et de la
navigation.

 

x
=
=
a
>
=
=
x
x
U

al

hie ne

 

j
|

 
