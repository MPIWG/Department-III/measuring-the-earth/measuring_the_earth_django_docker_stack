 

|
a
ji
a
4

172

de nature identique enfoui sous le sol. En outre les observatoires de Paris et d’Alger
*pouvaient échanger directement leurs signaux, grâce à un relais particulier placé à
l’observatoire de Marseille et disposé de telle sorte que tour à tour fermé sous
l’influence des courants venus de Paris ou d'Alger, il pouvait lancer dans le câble ou
dans le fil aérien le courant d’une pile locale dont l'intensité était réglée par une
boussole et un rhéostat. :

On a employé la méthode de l’enregistrement pour l’observation des passages
et l'échange télégraphique des signaux, suivant des procédés analogues à ceux qui
venaient d’être introduits en France par MM. Loewy et d'Oppolzer.

Voici l’ordre des opérations successives :

a) Paris — Marseille.

1° Equation personnelle entre MM. Stephan et Loewy.

2° Mr. Stephan a Marseille, Mr. Loewy a Paris.

3° Nouvelle équation personnelle.

b) Paris — Alger. c) Marseille — Alger.

1° Equation personnelle, à Alger, entre MM. Loewy et Perrier.

29 Mr. Loewy à Alger, Mr. Stephan à Marseille.

30 Mr. Loewy à Alger, Mr. Perrier à Paris.

4° Equations personnelles entre MM. Loewy-Perrier d’une part, MM. Loewy-
Stephan d’autre part.

_ Les observations et les échanges de signaux ont marché de la manière la plus
satisfaisante. Les calculs de réduction ne sont pas encore terminés; les résultats
seront communiqués à là prochaine Réunion de la Commission permanente.

4° a) Azimut fondamental de l'Algérie.

Pour déterminer l’azimut fondamental de là triangulation algérienne, j'ai fait
placer à 30 kilometres environ de l’observatoire vers le Sud et très près du Méridien,
sur l’un des mamelons de la chaîne de l'Atlas, un miroir solaire. Tous les soirs,
avant le coucher du soleil, MM. Penel et Bassot pointaient au fil mobile du cercle
méridien, dans les deux positions de l’instrument, et un grand nombre de fois, la mire

‚ meridienne placée au Nord et le miroir solaire placé au Sud. L’azimut de la mire
étant donné chaque jour par les observations de passage, on a pu calculer pour chaque
Journée l’azimut du miroir avec une très grande exactitude.

L'intervention de ce miroir fournit le précieux avantage de substituer l’azimut

d’un point très éloigné et invariable à l’azimut de la mire méridienne, dans les

opérations de réduction de l’azimut observé directement à l’azimut sur l'horizon de la

station géodésique voisine.

b) Latitude de l’observatoire d’Alger.

J’ai mesuré la latitude de l’observatoire d’Alger’ au moyen du cercle méridien
de Secretan pareil à ceux qui ont été employés en France et décrits dans les Annales
de l’observatoire.

 

x
=
=
=
=
a
=
3
=
>
=
x

Led

Lak nike

no!

mL ln a hä munis à:

|
|
|

 
