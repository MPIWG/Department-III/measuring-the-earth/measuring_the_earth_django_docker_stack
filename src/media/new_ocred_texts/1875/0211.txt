 

ILE RT À HAE GE li

[LL

ii

LR A Ba | Pa oie

[

x

201

ment les rumbs de tous les points saillants, tant naturels qu/artificiels, parmi lesquels il
faut faire entrer au moins trois signaux géodésiques préalablement déterminés, afin d’étre
fixé sur le lieu de la station. Au moyen de celles-ei, et d’après les innombrables inter-
sections fournies par les rumbs, on marque sur une grande feuille de papier tous les
points saillants du terrain, par rapport à ceux du 1° ordre, qu’on y à projeté d'avance.
On procède ensuite au choix des triangles de 27°, 32°, 4°, 00... ordre et à la
construction des signaux, à moins qu’on ne puisse employer des bâtiments déjà
existants, comme des tours, des dômes, des moulins à vent etc. Les signaux bâtis ad hoc
sont presque toujours de petites pyramides à forme de cône tronqué, en maçonnerie,
ne.

d’une hauteur de 2"5; le cercle de la base étant de 1”, et le cercle supérieur de
0"4 terminant par un hémisphère.

Dans le choix de tous les signaux secondaires on à suivi les préceptes suivants:

19° Que le réseau géodésique soit formé par des triangles en bonnes conditions
angulaires.

29 Que la dérivation des triangles s’effectue en les diminuant successivement,
c’est à dire, que les côtés qui servent de bases soient, autant que possible, plus longs
que les côtés qui en dérivent. :

3° Que la longueur des cötes des derniers triangles soit & peu pres de
2500 metres.

Outre ces conditions, il y a la suivante: chaque papier d’une planchette a
dimensions réguliéres doit contenir, pour le moins, deux points géodésiques réciproque-
ment visibles, ce qui suppose la nécessité d’une carte topographique du pays, à l’Echelle

1
10000
On emploie toujours, dans l'observation des angles azimutaux et verticaux,
des théodolites avec deux lunettes, dont une est de repère; les dernières divisions
des cercles gradués (azimutal et vertical) sont de 10 minutes et les deux cercles sont
munis de deux verniers de 10 secondes. Le limbe vertical n’est pas complet, puisqu'il
ne s'étend pas au delà d’une demi-circonférence.

de

La précision des observations va en diminuant, au fur et à mesure que les
triangles s’approchent des ordres inférieurs, parce que la longueur des côtés diminue
aussi.

On fait toujours, dans tous les points, les observations d’angles azimutaux et
verticaux, sauf quand il y a impossibilité manifeste, ce qui n’arrive pas souvent.

Pour abrèger ces détails qui ne peuvent être que de peu d’intérêt pour la Confé-
rence internationale, nous finirons par dire que les travaux de la petite géodésie, déjà
complets, comprennent une superficie équivalente aux À de la surface totale du pays,

avec 6797 points géodésiques déterminés. Ces points devront subir, il est vrai, une
26

 
