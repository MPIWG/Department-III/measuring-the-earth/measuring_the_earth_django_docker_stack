 

 

 

96

savoir la determination sur place, dans chaque station, de la longueur du pendule, et
surtout de l’&limination de l’influence de l’air soit au point de vue statique, soit au
point de vue dynamique (remous, etc.). Car cette derniere influence s’elimine aussi
bien que la diminution du poids dans l’air, par la difference entre la duree de l’os-
cillation dans les deux positions, a condition que l’amplitude moyenne des oscillations
soit sensiblement la même dans les deux modes de suspension. Or, dès que l’on connaît
approximativement le décroissement de l’amplitude dans les deux positions, il est
toujours facile de satisfaire à cette condition dans des limites largement suffisantes,
ainsi qu'il résulte des expériences faites ad hoc par Mr. Plantamour.

Enfin, on ne saurait admettre que notre appareil de Repsold ne satisfasse pas
aux exigences de la science actuelle. Car dans chaque observation isolée, dans un mode
de suspension, Mr. Plantamour à obtenu l'intervalle de temps de 24005 environ avec
une erreur moyenne de 02012, soit de lLo500903 et avec une moyenne de dix jours
d’observations, il obtient pour la pesanteur une valeur dont l’erreur moyenne est de
00000 au plus, exactitude certainement suffisante.

Mr. Hirsch ajoute aux observations de son collègue encore quelques considéra-
tions sur des points soulevés dans le mémoire du bureau central. D'abord, la crainte
que le trépied de suspension entre également en oscillation, à moins que ce ne soit un
fait constaté par des observations directes, lui semble peu fondée; en effet, il ne peut
pas être question d’oscillations proprement dites d’un corps d’une forme pareille, reposant
sur trois points d'appui; ensuite le mouvement du pendule dont le moment mécanique
est peu considérable à cause de sa faible vitesse, ne pourrait se communiquer au
trépied que par le frottement du couteau sur le plateau; or, ce frottement est insigni-
fiant, comme le montre la lenteur dans le décroissement de l’amplitude, causé presque
totalement par la résistance de l’air.

Quant à la mobilité des couteaux, Mr. Hirsch croit qu’en effet on pourrait se
dispenser de l’échange des couteaux, attendu que l’expérience de notre appareil a dé-

montré que l'artiste parvient à les faire d’une forme suffisamment identique, et qu’on

pourrait les fixer d’une manière solidaire à la tige, parce que le défaut de parallélisme
s’élimine par le retournement horizontal du pendule. La détermination exacte de la
température réelle du pendule semble aussi à Mr. Hirsch constituer une des principales
difficultés de l’opération, difficulté qu'il ne faut cependant pas exagérer. En effet, une
incertitude de 0°2 sur la température ne donne lieu qu'à une incertitude au-dessous
de ‘100000 Sur la longueur d’un pendule en laiton, et si l’on a soin d'opérer dans une
enceinte dans laquelle la température varie peu, on pourra certainement réduire
au-dessous de 0°2 l’incertitude sur la température du pendule à l’aide de thermomètres
placés convenablement. La plus grande difficulté consiste dans la détermination du
coefficient de dilatation qui doit être faite, le pendule étant suspendu, et non dans une
position horizontale; à défaut d'appareils ad hoc, on doit recourir à la comparaison
d'observations faites en hiver et en été. Mais en tout cas, le moyen de déterminer la
température que propose l’auteur de la notice, semble & Mr. Hirsch non seulement

 

lt

ud

/

Modded A hha

wey) iam dc mA

ak ini

kat mil

Ass sutton bah das ban lll nn 1

 
