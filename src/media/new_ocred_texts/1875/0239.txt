 

   

 

 

 

N — ee
ie | S38
: | Sees NEUE, : ue |
Jahr und Monat. else Veröffentlicht. Titel der Publication. Bemerkungen.
Année et Mors. < Br Publie. Titre de la publication. Remar ques
à | | | |
— — = = = = = = = Sa = EEE = en ZZ = —- eee =;
\ | | D [ i i6 a :
pau. | La longitude publiée dans
| le Nautical Almanac ne } |
| | nous mérite pas une
b | | entière confiance.
13 |
Ihe iP. |
de |
TPs

1858, September . | Peters, Bestimmung des Längenunterschiedes zwischen
| Altona und Schwerin. Altona 1861.
Se, 4 : | s. Frankreich.

Be CU D. dus la ee ee Noch nicht berechnet.
1864, April . . . . .|.\.1\.| Bruhns und Forster, Bestimmung der Längendifferenz |
LS zwischen den Sternwarten zu Berlin und Leipzig.
| |: beipzis 1865. |

| et on | Vorläufiges Resultat, vel.

Be 2 ee onen ‚(en Sinn:

eee SS AP Ss |  Generalbericht über die

(So a eee Re | Europaische Gradmes-

BE Ye | sung fiir das Jahr 1871,
Pa | Berlin 1872, S 47—48.

965, April . . . . .|.|.|.| Generalbericht über die mitteleuropäische Gradmessung

| | für 1866. Berlin 1867, S. 81.

ı Bruhns und Auwers, Bestimmung der Längendifferenz
zwischen den Sternwarten zu Leipzig und Gotha.
Leipzig 1866. |

Publication des geodätischen Instituts: Bruhns, Bestim-

| mung der Längendifferenz zwischen Berlin und Wien. |

| Lt herpzis LS
mo @cober, November ||... - . 2 ee -Nogh nicht bercchnet.
tee april, Mai 2. o-oo fiw |. Publication des geodätischen Instituts: Bruhns, Astrono-
| misch-geodätische Arbeiten in den Jahren 1872, 1869 |
| und 1867, Leipzig 1874, sowie Astronomisch-geodätische

el Fi ' Arbeiten in den Jahren 1873 und 1874. Berlin 1875, S. 56.

La | 1867, April, Mai . . . |...) Ebendaselbst.

1865, April

1865, September, October

° .

 

 

1867, August, September |. |. |. Peters, Bestimmung des Längenunterschiedes zwischen den
| | | | S§ternwarten von Altona und Kiel. Kiel 1873.
1868, April, Mai . . .1.1|.|.| Bruhns, Bestimmung der Längendifferenz zwischen Berlin
ei | und Lund. Lund 1870. |
1870, Mai, Juni . . .|.|.|.) Publication des geodätischen Instituts: Bruhns, Astrono- |

| '  misch-geodät. Arbeiten im Jahre 1870. Leipzig 1871. |

 

 

 

1870\ 1: : | \\Publication des geodätischen Instituts: Bruhns, Astrono-
1871 f Juli, Juli—August |. -|- ¢ misch-geodät. Arbeiten im Jahre 1871. Leipzig 1873. |
1871, August, September |.|.|. Ebendaselbst. |
ie Jun, Jul . . z.$| «\-.|Publication des geodatischen Instituts: Bruhns, Astrono- | |
| | misch-geodatische Arbeiten in den Jahren 1872, 1869 | i
| und 1867. Leipzig 1374. |
1374, Juni, Jui . . -. |... |Pablication des geodätischen Instituts: Astronomisch-geo- |
à | dätische Arbeiten in den Jahren 1873 u. 1874. Berlin 1875. | |
1874, Juni, Juli = x be 2 a 2 | 22 22 29 29 22 29 22 DD |
1874, Juni, Juli s > 24 ire ® | 22 29 22 22 22 22 22 29
EP ui Anoust.. . : |. 4... 3 + 5 ~ > > 2 a |
ee. mt canne | Vorlaufige Resultate, Generalbericht für 1871, S. 47—49. | Zur grossen Längengrad-
EP | messung gehörig.
| lara || Von General v. Forsch mit-
| getheilt.

Nochnicht ganzberechnet

 

 

| Struve, Jahresbericht der Nicolai-Hauptsternwarte. Peters- | Von der Red. hinzugefügt.

| |) |, Lure 1603 |

#

 
