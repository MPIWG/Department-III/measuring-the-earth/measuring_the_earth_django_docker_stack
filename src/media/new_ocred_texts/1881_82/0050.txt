 

|
i
P
if
i
I
u
|
i
Le

——— a ——
i

42

 

tamour’s auf der Herreise — um seinem theuren Freunde und ausgezeichneten Col-
legen den gebührenden Nachruf zu weihen; ich werde mich deshalb begnügen, über dem
kaum geschlossenen Grabe mit einigen Worten meiner dankerfüllten Freundschaft Aus-
druck zu geben.

Die Arbeiten Plantamour’s in den exacten Wissenschaften, insbesondere der
Astronomie und physischen Geographie, sind zahlreich und vielfältig; was sie alle aus-
zeichnet und auf die Stufe der Classicität erhebt, ist der Stempel mathematischer Schärfe
und numerischer Genauigkeit. Plantamour hatte sozusagen den Cultus der Genauigkeit
auf sein Banner geschrieben. Die ihm angeborene Vorliebe für exacte Wissenschaften
und angewandte Mathematik war zur Blüthe gebracht in Bessel’s Schule; und wie er
einer der bedeutendsten Schüler dieses grossen Meisters war, so erwies ihm derselbe
eine unverkennbare Zuneigung, für welche Plantamour stets eine gerechte und dankbare
Erinnerung behielt. ;

Plantamour war ein vorzüglicher Beobachter, aber mehr noch ein unermüdlicher

und sicherer Rechner. Mit unbegrenzter Hingebung widmete er sich bis in seine letzten

Lebenstage der ermüdenden Rechnungsarbeit, indem er seine astronomischen, meteorolo-
gischen und geodätischen Arbeiten selbst reducirte und dabei stets bemüht war, die Be-
obachtungen erschöpfend zu verwerthen; es gibt wenige Gelehrte, die von der Methode
der kleinsten Quadrate gleich ausgedehnten und kritischen Gebrauch gemacht haben.

Während 20 Jahren gemeinsamer Arbeit war der Sprecher in der glücklichen
Lage, Plantamour’s ungewöhnliches Rechnungstalent und dessen scrupulöse Genauigkeit
kennen zu lernen.

Es ist hier nicht der Ort, von den zahlreichen astronomischen Arbeiten des
Verstorbenen zu sprechen, dieselben sind von seinen Collegen genügsam gekannt und
gewürdigt; noch auch von seinen Verdiensten um die Meteorologie, die er sich erwarb,
indem er als einer der ersten nach Bessel’s Methode in das Studium der periodischen
Erscheinungen eintrat und, die fundamentale Bedeutung meteorologischer Beobachtungen
auf Höhenpunkten erkennend, die auf dem grossen St. Bernhard errichtete meteorolo-
gische Station, welche lange Zeit die höchstgelegene der Erde blieb und seit mehr als
30 Jahren regelmässige Beobachtungen liefert, reorganisirte und leitete.

Sie alle kennen die zählreichen geodätischen Arbeiten Plantamour’s. Es ziemt
nicht seinem fast ständigen Mitarbeiter auf eine nähere Würdigung derselben einzugehen,
aber es ist Pflicht, den beträchtlichen und überwiegenden Antheil hervorzuheben, den
unser verewigter Freund an der Entwicklung unseres Werkes nicht nur in der Schweiz,
sondern im Allgemeinen genommen hat.

Als General Baeyer die Idee zu unserem Unternehmen fasste, war Plantamour
einer derjenigen, welche dieselbe mit Begeisterung aufnahmen und fortpflanzten. In
seinen damaligen zahlreichen Zusammenkünften mit dem Berichterstatter und später
im Schoosse der schweizerischen Commission entstand die erste Anregung zu
der bedeutungsvollen Erweiterung des Baeyer’schen Programms durch die Auf-
nahme der Präcisionsnivellements und der Pendelbeobachtungen. Die Einführung

Hodis di) Mb mR MMs bes bs tisk

1188 1111188

u ua.

Li

a amd den as dat dad ad aa pme in 1:

os seæmiastiems mare A|

 
