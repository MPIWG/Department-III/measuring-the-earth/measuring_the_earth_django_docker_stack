 

i
|
:
;
j
N
rf
i
i
j
a
i
I,
:
i

i
I
i
|
i
N
|
i

38

In den Niederlanden werden die gedeihlichen und freundschaftlichen
Beziehungen der Völker hoch in Ehren gehalten und erwecken das lebhafteste

‚Interesse. Und wie man daselbst den Austausch der Erzeugnisse des Bodens

und der Arbeit der verschiedenen Länder als wohlthätig betrachtet von einem
anderen und höheren Standpunkte als dem des materiellen Vortheils, so wird
auch der unmittelbare Austausch der unvergänglichen Früchte des menschlichen
Geistes hier stets wahrem und einmüthigem Verständnis begegnen.

In diesem Sinne begrüssen die Niederlande jene Männer, welche aus
den verschiedensten Theilen der Erde herbeigekommen sind, um mit den Er-
gebnissen ihrer hehren wissenschaftlichen Forschungen das Gemeingut der
Menschheit zu bereichern, um sich gegenseitig anzuregen und vereint neue
Fortschritte und Entdeckungen anzubahnen.

Dass Sie, meine Herren, für diesmal den Haag zur Stätte Ihrer Zu-
sammenkunft gewählt haben, dazu beglückwünschen sich die Niederlande und
danken es Ihnen durch meinen Mund! Mir aber gestatten Sie noch, meine
besten Wünsche für das Gelingen Ihrer Arbeiten und die Hoffnung auszu-
drücken, dass Ihr Aufenthalt in unserer Mitte, wie kurz derselbe leider auch
sein mag, nur gute und angenehme Eindrücke bei Ihnen hinterlassen werde.

Der Präsident, Generallieutenant Ibanez, beantwortet die Ansprache des Ministers

mit folgenden Worten*):

 

Herr Minister!
Vor Jahresfrist sollte die permanente Commission der Europäischen

.Gradmessung entsprechend der freundlichen Einladung der Regierung Sr. Ma-

jestät des Königs sich im Haag versammeln, aber unvorhergesehene Hinder-
nisse, zum Theile schmerzlicher Art, haben uns gezwungen, auf die Abhal-
tung der vorjährigen Sitzungen zu verzichten. Die kgl. niederländische Re-
gierung hatte die Freundlichkeit, ihre gütige Einladung aufrecht zu erhalten;
ich bin somit in der glücklichen Lage, die Befriedigung zum Ausdrucke bringen
zu können, welche uns alle darüber erfüllt, dass wir die Geodäten Europa’s
vereinigt sehen in dem Vaterlande der Snellius und Huygens, des Generals
Krayenhoff und unserer einstigen hervorragenden Öollegen: Kaiser und Stamkart.

Der Letztere wurde im laufenden Jahre der Wissenschaft, welcher er
mit so vielem Erfolge diente, und seinen«Collegen, welche die Liebenswür-
digkeit seines Charakters schätzen gelernt, entrissen. Was uns hierüber zu
trösten vermag, ist die Ueberzeugung, dass die geodätischen Arbeiten in
Holland gleichwohl mit demselben Erfolge durch die Gradmessungscommission
werden fortgeführt werden, welche so ausgezeichnete und auf jedem Gebiete
unserer Forschung sachkundige Gelehrte in sich vereinigt.

Wir alle bedauern die Abwesenheit des ehrwürdigen Gründers unseres

*) Uebersetzung des französischen Originaltextes.

a ha bi nt Bit ed

0.

=

 

 
