 

18

DEUXIÈME SÉANCE.
La Haye, Mardi 12 Septembre 18382.

Présidence de M. le Général Ibanez.

La séance est ouverte % 10'/,”.

Sont présents, de la Commission permanente:

MM. v. Bauernfemd, Baulina, Hirsch, v. Oppolzer.

Des délégués: MM. Backhuyzen, Baraquer, Capitaneanu, v. Diesen, Fearnley,
Ferrero, Oudemans, Perrier, Schols, Villarceau.

Le Procès verbal de la première séance est lu en Français par M. Ærsch, et
en Allemand par M. v. Oppolzer; personne n'ayant présenté des observations, il est adopté.

M. v. Bauernfeind prend place au fauteuil de la présidence et donne la parole
à M. le Général Ibañez, pour présenter le rapport sur les travaux accomplis en
Espagne dans les deux années de 1881 et 1882.*)

M. Ibanez fait circuler trois nouvelles feuilles de sa carte d'Espagne au 55650;
qui continuent à réaliser le haut degré de perfection qu’on a pu admirer chez les pre-
mières feuilles.

M. le Colonel Baraquer ajoute une communication spéciale sur les observations
de pendule qu’il a exécutées à Observatoire de Madrid.**)

M. le Colonel Perrier fait l'observation que le fait consigné dans le Rapport
Espagnol au sujet de la différence de niveau, trouvée égale à 0,°6742 entre les ports
de Santander et d’Alicante, est d'autant plus remarquable qu’il confirme de très près
le chiffre que M. Bourdaloue a indiqué dans le temps pour la différence entre l'Océan
et la Mediterrannée, savoir 0,72. La legère différence entre les deux chiffres s'explique
peut-être par le choix que l'Espagne a fait pour l'établissement de son maréographe à
Santander qui, dans l'opinion de M. Perrier, n’est peut-être pas propre à indiquer le
véritable niveau de l’océan libre, étant situé dans un golf plus ou moins fermé.

M. Hirsch est également heureux, de voir cette ancienne question, tant discutée
et débattue, de la différence de niveau des mers, résolue enfin pour la première fois par
les moyens modernes d'observation exacte. — Il ne peut pas partager l'opinion de son
collègue sur la situation défectueuse du maréographe de Santander, puisque le golf de
Biscaye est largement ouvert et que le maréographe s’y trouve installé sur un rocher
avancé, en face de la .mer libre. Il serait à désirer que partout ailleurs on établit,
dans un avenir rapproché, des maréographes dans les mêmes conditions excellentes.

Y

*) Voir au Rapport Général pour 1881/82, Espayne.

” 99 ” 5 2) „ „

bit eam OM Ae Rem Red ok

IL (nihil)

dl

In. nnd An ns hs ha an Han 1

 
