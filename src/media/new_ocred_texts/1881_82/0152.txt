 

144

Nachtrag. Supplement.

Belgien. Belgique.

Rapport sur les travaux exécutés en 1881 et 1882 par la section géodésique de l’In-
stitut cartographique militaire de Belgique.

A l’époque de la dernière réunion de la conférence internationale de Géodésie
les calculs relatifs à la compensation du VIII* groupe de triangles avaient été terminés.
Il était dés lors permis d’unir les deux bases d’Ostende et de Lommel par une chaîne
de triangles partant d’Ostende vers le sudest du pays, pour remonter ensuite vers
Lommel suivant le méridien.

Depuis cette époque, les calculs concernant les IX®, X°, x er XI sroupes
fondamentaux ont été complètement achevés. Ces groupes n'ont toutefois pas l’impor-
tance de ceux qui précèdent, et pourraient plutôt être considérés comme en formant
le complément.

En effet, le IX°, comprenant 5 équations aux angles et 2 équations dites sup-
plémentaires,*) est situé à la frontière Est du royaume; le X° ayant 8 équations aux
angles, et le XI°, comprenant 6 équations aux angles, 1 équation aux côtés et 2 équa-
tions supplémentaires, limitent le réseau vers le Sud. Enfin le XII° groupe, de 9 équa-
tions aux angles, 2 équations aux côtés et 6 équations supplémentaires, est formé des
triangles qui occupent la position centrale du pays, c'est-à-dire des triangles qui ne sont
pas compris dans les deux chaînes qui unissent les bases d'Ostende et de Lommel.

Des modifications ont dû être apportées, pour la compensation des groupes IN,
XI et XII, aux équations de condition primitivement posées. Par suite des détermina-
tions de corrections et de côtés obtenus par la compensation de groupes antérieurs, des
éléments connus ont pu être introduits dans ces trois groupes, ce qui à permis de
diminuer le nombre des équations de condition posées. Il a été possible de calculer
des triangles ayant ainsi les éléments suffisants, et d'introduire, par des équations supplé-
mentaires, des longueurs de côtés obtenus par ces calculs préalables.

*) Nous rappellerons que les équations supplémentaires, dans la compensation du réseau belge in-
troduisent des longueurs de côtés dans les équations de condition.

 

1
i
i
3

seer mate ue ill A lh aa à —

 
