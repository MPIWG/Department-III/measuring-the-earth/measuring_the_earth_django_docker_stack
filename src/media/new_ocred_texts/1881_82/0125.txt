DURE eee Sn rs or ne =>

vu PP

die he PAT

iR!

Ju 1

it

=
=
x
=
x
x
=
=

ln
Das Mittelwasser der Nordsee bei Amsterdam liegt also 93 Millimeter höher
als das Mittelwasser der Ostsee bei Swinemünde und giebt deutlich die Stauung zu er-
kennen, die durch den Anprall der Flutwelle an den Continent herbeigeführt wird.

Baeyer.

II. Bericht des Sections-Chefs Geh. Reg.-Rath Prof. Dr. Sadebeck.

Seit dem letzten Generalberichte sind von dieser Section folgende praktische
Arbeiten ausgeführt worden:

Für den Sommer 1881 war die Vervollständigung resp. Weiterführung der Ver-
bindung des selbstregistrirenden Pegels auf Helgoland mit dem Festlande ins Auge ge-
fasst und es wurde dieser Section, bestehend aus dem Unterzeichneten und den beiden
Assistenten Werner und Dr. Börsch, die Station Neuwerk zugewiesen.

Was die Station selbst anbelangt, so findet sich die Beschreibung im General-
berichte für 1878.

Zum Anschluss an das Festland wurde an der Küste der Punkt Kugelbake aus-
gewählt; da dies kein Dreieckspunkt war, musste zur Ermittlung der Entfernung Neu-
werk—Kugelbake eine Kleintriangulation ausgeführt werden, welche sich auf die Seite
Neuwerk—Cuxhaven Leuchtthurm des Gauss’schen Coordinaten - Verzeichnisses stützt.
Die Einrichtung der Stationen sowie die Winkelmessungen auf den 4 Stationen dieses
Netzes wurden in der Zeit vom 15. Juni bis 9. Juli ausgeführt.

Am 13. Juli begannen die gegenseitigen und gleichzeitigen Beobachtungen
zwischen Neuwerk und Helgoland und wurden bis zum 15. August fortgesetzt.

Nachdem die Beobachter von Station Helgoland nach Station Kugelbake über-
gesiedelt, begannen am 22. August die DES UT zwischen Neuwerk und Fer
bake und dauerten bis zum 7. September.

Nach Beendigung des trigonometrischen Nivellements wurde zwischen Kalb
und Cuxhaven Leuchtthurm ein zweites geometrisches Nivellement ausgeführt, nachdem
das erste Nivellement dieser Strecke bereits im Monat Juni ausgeführt worden war.

Im Sommer 1882 nahm der Assistent Herr Werner Theil an den Beobachtungen
auf Station Kernsdorf, einem Punkte der Thorn—Memeler Kette. Auf dieser Station
wurden Polhöhe und Azimuth bestimmt, sowie sechs Richtungen älterer Ketten nachgemessen. —

An Bureauarbeiten sind von der Section folgende Arbeiten ausgeführt:

Zunächst wurde der Druck des Generalberichtes für 1880 vollendet, welcher
sich bis Ende Juni hinzog.

‘Im Winter 1881/82 wurde ‚das Hessische Dreiecksnetz““ druckreif hergestellt

 
