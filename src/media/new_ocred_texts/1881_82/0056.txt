 

i
|
|

ae

D

Se a a EN ee Ce eee

10.

LL

12.

14.

15.

48

-F. Publikationen des geodätischen Instituts.

. Winkel- und Seitengleichungen. Von Dr. Alfred Westphal.

Ueber die Beziehung der bei der Stations-Ausgleichung gewählten Null-
richtung von Wilhelm Werner. Berlin 1880. Verlag von P. Stankiewicz’ Buch-
druckerei.

. Astronomisch-geodätische Arbeiten in den Jahren 1879 und 1880. Von Pro-

fessor Dr. Albrecht. Berlin 1881. Verlag von P. Stankiewicz’ Buchdruckerei.

. Die Ausdehnungs-Coefficienten der Küstenvermessung. Von Dr. Alfred West-

phal. Berlin 1881. Verlag von P. Stankiewicz’ Buchdruckerei.

. Verhandlungen der vom 13. bis 16. September 1880 in München vereinigten

Allgemeinen Conferenz der Europäischen Gradmessung, redigirt von den Schrift-
führern ©. Bruhns, A. Hirsch. Zugleich mit dem Generalbericht für das Jahr
1880 herausgegeben vom Centralbureau der Europäischen Gradmessung. Berlin
ise), Verlas yon G. Reimer.

. Das Mittelwasser der Ostsee bei Swinemünde Von Dr. Sebt. Berlin 1881.

Verlag von P. Stankiewicz’ Buchdruckerei.

. Literatur der praktischen und theoretischen Gradmessungsarbeiten. Im Auf-

trage der permanenten Commission bearbeitet von M. Sadebeck. Berlin 1881.
Verlag von P. Stankiewicz’ Buchdruckerei.

. Ueber die Nivellements-Arbeiten im Preussischen Staate und die Darstellung

ihrer Resultate in richtigen Meereshöhen. Vom Präsidenten des geodätischen
Instituts. Berlin 1881. (Als Manuskript gedruckt.)

. Präcisions-Nivellement der Elbe. Zweite Mittheilung. Von Dr. Sezbt. Berlin

1881. Verlag von P. Stankiewicz’ Buchdruckerei.

. Das Hessische Dreiecksnetz. Von Professor Dr. Sadebeck. Berlin 1882.

Verlag von P. Stankiewicz’ Buchdruckerei.

Zur Entstehungsgeschichte der Europäischen Gradmessung. Vom Präsidenten
des geodätischen Instituts. Berlin 1882. Verlag von P. Stankiewicz’ Buch-
druckerei.

Das Rheinische Dreiecksnetz. II. Heft. Die Netzausgleichung. Von Pro-
fessor Dr. Fischer. Berlin 1882. Verlag von P. Stankiewicz’ Buchdruckerei.
Der Einfluss der Lateralrefraction auf das Messen von Horizontwinkeln. Von
Professor Dr. Fischer. Berlin 1882. Verlag von P. Stankiewicz’ Buch-
druckerei.

. Astronomisch-geodätische Ortsbestimmungen im Harz. Im Jahre 1881 aus-

seführt von Dr. Moritz Löw. Berlin 1882. Verlag von P. Stankiewicz’
Buchdruckerei.

Gradmessungs-Nivellement zwischen Swinemiinde und Constanz. Von Dr.
Seibt. Berlin 1882. Verlag von P. Stankiewicz’ Buchdruckerei.

Absolute Höhen der Festpunkte des Gradmessungs-Nivellements zwischen

0 1 LE Mb ARERR si shins:

ALAN and man in 11

Lu di them bn a ld

aot anette, mimansaed dik

 
