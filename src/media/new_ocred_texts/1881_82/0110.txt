 

}
hs
i
i
i
4
|
i
4
B
ï

Ta

I mem rm ere

Se m mm m um ze

j
N
|
ii
i

102

 

ads: Die tiefen Schachte des Bergwerkes zu Pribram in Böhmen bieten eine
günstige Oertlichkeit zur Ausführung verschiedenartiger wissenschaftlicher Un-
tersuchungen und die Bergdirection ist bestrebt, durch die bereitwilligste Un-
terstützung der Beobachter solche Arbeiten zu fördern.
| Sofern nun die Resultate solcher Untersuchungen mit der Polhöhe der
Beobachtungs-Station zusammenhängen, erscheint es nothwendig, eine zuver-
lässige directe Bestimmung dieses Elementes zu besitzen.

Diese Erwägung und die Nothwendigkeit für die Bestimmung der De-
viation der Grubencompasse und für die Beobachtungen am magnetischen
Declinatorium ein sicher bestimmtes Azimuth zu haben, veranlasste die k. k.
Instituts-Direction im Einvernehmen mit der Berg-Direction anzuordnen, dass
gelegentlich der astronomischen Gradmessungs-Arbeiten in Böhmen auch in
Pribram ähnliche Beobachtungen auszuführen seien.

Diesem zufolge wurden auf dem Hauptpfeiler des magnetischen Ob-
servatoriums

4 Sätze Zenithdistanzen südlicher Sterne,

4, . nôrdlicher ,, und

4 ,, Azimuth gemessen; letztere mit der am Strachen-Schachte etablirteu
Mire.

Durch diese in der Zeit vom 27. Juni bis 4. Juli ausgeführten Beobach-
tungen ist sowohl die Polhöhe, wie das Azimuth auf die Bogensecunde richtig
bestimmt und durch die Messung der Winkel zwischen der Mire und andern
verlässlichen Objecten dafür gesorgt worden, dass die gewonnenen Resultate
nicht verloren gehen, wenn das Gebäude des Strachen-Schachtes baulichen
Umänderungen unterworfen werden müsste.

Die vom Leiter der Instituts-Sternwarte nunmehrigem Major v. Sternech;,
welcher alle hier aufgezählten astronomischen Beobachtungen ausführte, im
Februar 1882 begonnenen Untersuchungen über die Intensität der Schwere im
Inneren der Erde im tiefsten Schachte des Bergwerkes zu Pribram
wurden im Monate Jänner 1883 fortgesetzt.

B. Trigonometrische Arbeiten.
1. Triangulirung im westlichen Theile der Monarchie.

a. Signalbau.
Die Arbeiten begannen im Monate Juni im Küstenlande mit dem Pyramidenbaue

auf Radica, Mrzavec, Op¢tina und Monte Maglio, welche Punkte zu dem — mit

Italien vereinbarten — Anschlussnetze gehören.

Der Signalbau wurde sodann in Tirol fortgesetzt und sind auf Pasubio, Frate,
foeanelia, Nambino, Peller; Roen, Cima d’Asta, Zangen Schlern, Puez,
Plosen und Kraxentrag Pyramiden errichtet worden.

 

:
i
4
i
3
i
!
i

etna kes bald a nn

 
