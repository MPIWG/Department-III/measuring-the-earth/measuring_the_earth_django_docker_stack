Pye NE |

nn RS EMI

TRI

i

95

 

 

ist gut, und wer weiss, wie bald wieder Nebel und Unwetter jede weitere Beobachtung
unmöglich machen.

Der Sommer 1881 war den Triangulirungs-Arbeiten in der Gletscher- und Fels-
Region besonders ungünstig. Anfangs August gab es heisse wolkenlose Tage mit sehr
schlechter Fernsicht. Während dieser Zeit wurden die Beobachtungen auf dem Reis-
rachkopf (2209™ Seehöhe) auf dem Ziethenkopf (2485%) und auf dem Ankogl
(3263”) ausgeführt.

In der zweiten Hälfte des Monates begannen heftige Stürme, welche mit geringen
Unterbrechungen bis Mitte September andauerten. In dieser Periode sollten die Winkel-
messungen auf dem Grossglockner (3798%) und auf dem Grossvenediger (wo
das trigonometrische Signal steht, 3659%) vorgenommen werden.

Für beide Punkte standen Schutzhütten zur Verfügung, für ersteren die Reo
herzog Johann-Hütte auf der Adlersruhe (in 3463 Meter Seehöhe), für letzteren die
Pragerhütte (2491 Meter).

Von der Adlersruhe bis zum Gipfel des Grossglockners sind allerdings nur mehr
330 Meter Höhenunterschied zu überwinden; doch sind auf dieser Strecke alle jene
Stellen zu passiren, welche die Besteigung dieses Berges schwierig machen. Der steile
Anstieg zu demgänzlich vergletscherten Kleinglockner, dessen höchstgelegene, 45°—50°
seböschte Eisfläche in einer Horizontalen überquert werden muss, das Passiren der
hierauf folgenden „Scharte‘“, einer veränderlichen, oft nur 0%3 breiten Schneebrücke
zwischen Klein- und Gross-Glockner, endlich das Erklimmen der felsigen Spitze des
letzteren sind Leistungen, welche nicht gewöhnliche Anforderungen an die Kraft, Aus-
dauer und völlige Schwindelfreiheit des Besteigers stellen. Ist demnach der Auf- und
Abstieg schon für unbepackte Touristen keine leichte Aufgabe, so wird er zu einer wahren
Bravour-Leistung für die mit den Instrumenten und Requisiten beladenen Träger,
besonders aber für jenen, welcher den Untertheil des Theodoliten (ein Gewicht von
26") am Rücken tragen muss.

Des Morgens und spät am Abend, wenn die das Eis bedeckende Firnschichte
hart gefroren ist und dem mit Steigeisen versehenen Fusse festen Halt bietet, hängt
ein glückliches Gelingen der Besteigung nur von der Routine des Touristen und des
Führers ab; wenn jedoch der Beobachter, welcher des Morgens bei günstiger Witterung
zum Gipfel des Grossglockners aufgestiegen ist, durch herannahende Gewitter gezwungen
wird, die Spitze des Berges zu verlassen, bevor noch der, von der Sonne im Laufe des
Tages stark aufgeweichte Schnee wieder erhärtet und tragfähig geworden ist, dann
wird der Abstieg über die steilen Gletscher des Kleinglockners äusserst gefahrvoll.

Wesentlich anders charakterisirt sich der Grossvenediger, zu dessen Ersteigung
von der Pragerhütte aus noch 1170 Meter Höhenunterschied zu überwinden sind.

Der Grossvenediger stellt eigentlich keine besonderen Anforderungen an seine
Ersteiger: über sanft und gleichmässig ansteigende Schneeflächen führt der Pfad —
wenn man einzelne Gletscherspalten an der Seite eines kundigen Führers zu vermeiden

 

 
