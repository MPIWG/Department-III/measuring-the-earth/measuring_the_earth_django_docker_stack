 

134

fer de trois mètres du bureau fédéral de Berne à laquelle ont été rapportées provisoire-
ment la plupart des mires dont on s’est servi pour les nivellements de précision en

Europe.
Neuchatel, Avril 1883.

Dr. Ad. Hirsch.

Spanien. Espagne.

Rapport de Monsieur le Général Ibañez sur les travaux géodésiques
de 1’ Institut Géographique et Statistique.

Depuis la Conférence de Munich, tenue au mois de Septembre 1880, l'activité
scientifique de l’Institut géographique et statistique d’Espagne, en ce qui concerne la
haute géodésie, a donné les résultats suivants:

Bases.
Le petit réseau qui relie la base d’Olite, dans la Navarre, au côté Higa-
Vigas du grand réseau péninsulaire, — (dont j'ai entretenu la Conférence à Munich en
rapportant que les observations angulaires étaient faites à ses neuf points et que les
35 équations de condition qu'ils fournissent conjointement avec les 29 lignes qui les
joignent, étaient établies,) — est complètement terminé par la résolution de ce groupe
d'équations.

Quadrilatère de jonction des réseaux Espagnol et Algérien.

En ce qui concerne l'Espagne ce travail a fait un pas de plus par le calcul du
côté Mulhacen-Tetica (voir la carte qui vient d’être distribuée), en se servant de
trois bases, savoir de celles de Madridejos, Arcos de la Frontera et Carthagène.

Au moyen des chaînes de triangles qui relient entre elles ces trois bases, et en
prenant pour valeurs angulaires les plus probables à chaque station isolée, une valeur
unique a été déterminée pour le dit côté, en faisant une compensation des erreurs an-
gulaires. Cette valeur du côté Mulhacen-Tetica est de

828272546 Æ 0"115.

Compensation du grand réseau espagnol.

Après avoir terminé la résolution du groupe d'équations relatives à la jonction
de la base d'Olite, le même calcul a été entrepris pour le groupe No. I, celui de la
Galice, des dix qui composent le réseau total.

 

a
i
i
!

ne he de Ha lala da dh ae +

 
