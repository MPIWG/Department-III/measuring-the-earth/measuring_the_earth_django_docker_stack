TR 0 ere =

 

rem

wre

arten

PATTERN

=
=
=
.
Ä

Monsieur Hirsch donne lecture du

Rapport de la Commission permanente pour 1881—82,

Messieurs,

Dans la séance de la Commission permanente du 17 Septembre 1880 & Munich,
le voeu avait été émis que la réunion prochaine eût lieu dans une ville des Pays-Bas.
Donnant suite à cette initiative, le Bureau de la Commission s'était mis en rapport avec
l’un de nos collègues néerlandais, M. le Professeur van der Sande Backhuyzen, qui nous
a fait savoir que le Gouvernement des Pays-Bas était disposé à recevoir la Commission
permanente à La Haye. La Commission, consultée par correspondance, avait accepté
à l'unanimité l’aimable invitation et nous pouvions convoquer par circulaire du 22
Juillet la Commission à La Haye pour le 12 Septembre 1881.

Vous connaissez tous les raisons qui ont obligé la Commission à renoncer à
cette réunion en 1881; le mort inattendue de notre regretté confrère M. Brubhns,
ainsi que la demande de nos collègues italiens, venue au dernier moment, de changer
la date de la réunion pour éviter une fâcheuse coïncidence avec le Congrès de géogra-
phie à Venise, ont en effet déterminé la Commission à décider par vote télégra-
phique, à l’unanimité moins une voix, de renvoyer la conférence à cette année.

Nous avons tous regretté de devoir prendre pareille décision; et bien que ce
ne soit pas la première fois que la Commission ait été obligée de renoncer aux con-
férences réglementaires annuelles, nous croyons cependant qu’il ne faudrait rien changer
aux intervalles des réunions, prévus par le Reglement et qu'il faut observer la règle
établie à cet égard, sauf dans des cas exceptionnels comme celui de l’année dernière.

Nous étions donc heureux d’apprendre que le Gouvernement Royal maintenait
sa gracieuse invitation pour l’année courante, de sorte que, après avoir consulté la
Commission, nous avons pu inviter ses Membres et Messieurs les délégués à la con-
férence annuelle. —

Depuis notre dernière conférence en 1880, la mort à fait de. tristes ravages
dans les rangs de l’Association géodésique internationale, en nous enlevant plusieurs
de nos collègues des plus anciens et des plus distingués.

Ainsi nous fut enlevé le 25 Juillet 1881, dans la fleur de l’âge, un des promoteurs
de notre oeuvre, M. le Dr. Ch. Bruhns qui a fait partie de la Commission permanente
dès la première conférence géodésique de 1864 et qui, en sa qualité de Secrétaire de
la Commission, a rendu les plus grands services à l’entreprise de la mesure des degrés
en Europe.

Bruhns a été le fils de ses oeuvres dans toute l’acception du mot, un „self
made man“, avec toutes les qualités de ces vaillantes natures qui savent sortir des
conditions modestes de leur origine pour s'élever jusque dans les hauteurs de la vie

 
