 

|
;
|
I
|

RN

Be

i
H

— 0 =

Aus den vorläufigen Winkelmessungen hat sich ergeben, dass das Centrum der
Pleissenburg von dem Centrum der Sternwarte
1191,416 Meter
entfernt liegt bei dem Azimuth
RAS STE

Die östliche Entfernung beträgt 1145,555 Meter, die nördliche 327,379 Meter, so-
dass das Centrum der Pleissenburg von dem Centrum der Sternwarte 3°,496 östlich und
10",59 nördlich liegt. Die Polarcoordinaten des Centrums der Pleissenburg sind daraus

Lange = 4™ 45.841 westlich von Berlin,
Breite = + 51° 20/ 16",89.

Wegen der Basismessung konnten sich die tibrigen Triangulirungs-Arbeiten
des mit unterzeichneten Nagel im Sommer 1872 nur auf einige Recognoscirungen, wobei
die Punkte I. Classe Reust und Cuhndorf und einige Punkte Il. Classe ermittelt wurden,
sowie auf die Winkelbeobachtungen auf dem Basiszwischenpunkte Grossenhain beschränken.
Auf letzterem wurden die Richtungen nach den Punkten I. Classe Collm und Strauch, nach
den 6 Basisnetzpunkten Buchberg, Grossdobritz, Baselitz, Weida, Raschütz und Quersa,
sowie nach 2 Punkten Il. Classe festgeleet.

Die Nivellements-Arbeiten sind ebenfalls aufgehalten worden, weil das be-
treffende Personal bei der Basismessung gebraucht wurde; überdiess auch der eine Assistent
noch vor Beendigung der Nivellements eine anderweite Stellung annahm. Durch die
Assistenten Richter und Ueberall sind daher nur die Linien

Dresden — Wahnsdorf
Franzensbad — Elster
Elster — Hof
Gässenreuth — Oelsnitz
Oelsnitz — Falkenstein
Auerbach — Kirchberg — Zwickau
Werdau — Ronneburg
Ronneburg — Altenburg
mit 27,2 deutschen Meilen = 204 Kilometer Gesammtlänge in 118 Tagen incl. der Sonn-
und Regentage doppelt und die Linien
Schindmaass — Gössnitz
Gössnitz — Werdau
mit 4,1 deutschen Meilen = 30,75 Kilometer Gesammtlänge in 7 Tagen einfach nivellirt
worden.

Die Linie Franzensbad — Elster— Hof wurde hauptsächlich nivellirt, um das Polygon,

durch welches früher die Höhen von Franzensbad und Hof bestimmt worden waren, zum

Abschluss zu bringen, da bezüglich des Höhenunterschieds Franzensbad — Elster eine grössere

dh un.

li

 

 
