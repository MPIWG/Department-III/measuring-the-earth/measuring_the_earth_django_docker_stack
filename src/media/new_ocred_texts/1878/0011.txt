ge nl

nn PAT

it
11
LE
iR
+
it
iR
x
È
:f

 

An den Sitzungen nahmen Theil:

Als Vertreter des Senats der Freien Hanse-Stadt Hamburg:

Herr
Herr

Herr
Herr
Herr
Herr
Herr

Herr
Herr
Herr

Herr
Herr
Herr
Herr
Herr
Herr
Herr
Herr
Herr
Herr

Bürgermeister Dr. Kirchenpauer, Magnificenz.
Senator Mayn.

D Die Mitglieder der permanenten Commission:

Generallieutenant Ibanez Excellenz aus Madrid, Präsident.

Professor Dr. von Bauernfeind aus München, Vice-Präsident.

Professor Dr. Bruhms aus Leipzig, Schriftführer.

Professor Dr. Hirsch aus Neuchätel, Schriftführer.

Generallieutenant Dr. Baeyer Excellenz aus Berlin, Präsident des
Central-Büreaus.

Generallieutenant von Forsch Excellenz aus St. Petersburg.

Generalmajor Mayo aus Florenz.

Professor Dr. von Oppolzer aus Wien.

2) Die Bevollmächtigten:

Oberstlieutenant Adam aus Brüssel.

Geheimer Etatsrath Andrae Excellenz aus Kopenhagen.

Oberst Barozzi aus Bucharest.

Oberst Barraquer aus Madrid.

Oberst Ferrero aus Florenz.

Hilgard aus Washington, als Vertreter der U.S. Coast and geodetic Survey.
Commandant Perrier aus Paris.

Professor Dr. Peters aus Kiel.

Professor Dr. Sadebeck aus Berlin.

Akademiker Villarceau aus Paris.

 
