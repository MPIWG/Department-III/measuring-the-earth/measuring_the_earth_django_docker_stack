Le

SP mn EN |

RTT

iL AW

Pr TA AR Bi m

Ont pris part à la réunion:

I. Les Représentants du Gouvernement de Genève:

Monsieur Carteret, Président du Conseil d'Etat, Chef du Département de l’In-
struction publique.
Monsieur Chauvet, Conseiller d'Etat.

If. Les Membres de la Commission permanente:

Son Excellence Monsieur le Général Ibanez de Madrid, Président.
M. le Dr. de Bauernfeind de Munich, Vice-Président.

M. le Dr. Bruhns de Leipzig
M. le Dr. Hirsch de Neuchatel
M. Faye, Membre de l'Institut de Paris.

Son Excellence M. le Général de Forsch de St. Pétersbourg.
M. le Général Mayo de Florence.

M. le Dr. von Oppolzer de Vienne.

Secrétaires.

III. Délégués:

. le Lieutenant-Colonel Adan de Bruxelles.
. le Colonel Ferrero de Florence.

. le Directeur Peters de Kiel.

. le Professeur Plantamour de Genève.

. Respighi de Rome.

. Villarceau, Membre de l’Institut de Paris.

See

IV. Invités:

. Doutiller de Beaumont de Genève.
. le Professeur Cellérier de Genève.
. le Professeur Chaix de Genève.

Sen

1*

 
