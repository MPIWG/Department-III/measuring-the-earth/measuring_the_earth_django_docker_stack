eR m nn

a an a a m m
.

ee

|
|

 

88
che vi ho distribuito. Vedrete la rimarchevole concordanza dei risultati astronomici e
trigonometrici Sopra una estensione abbastanza considerevole, come si rileva dalla
tavola No. 1. Vedrete ancora nella pagina seguente gli elementi dello sferoide terrestre,
calcolati mediante le nostre triangolazioni, ed il loro rimarchevole accordo cogli ele-
menti di Bessel.

In questo punto si eseguiscono le osservazioni angolari sulle Alpi e nella
pianura del Piemonte, eppero noi lavoriamo ora sul terreno della grande misura del
parallelo medio. Le ricerche che potranno farsi nelle regioni alpine presenteranno una
feconda messe pei nostri scienziati.

Qui cade in acconcio una mia confessione. Io non ho indicato nel mio pro-
gramma la successione delle ricerche da farsi per la direzione ed intensitä della gravita.
Lungi dal rifiutarmi ad intraprendere siffatte ricerche, mi © perd sembrato che non
sarebbe possibile fare un programma troppo anticipato del loro cammino. Ho creduto
opportuno di lasciare in cid la più larga indipendenza ai colleghi che dovranno dedicarsi
a tali ricerche, limitandomi a fornir loro gli strumenti ed i mezzi economici che

garanno necessari.
Adesso permettetemi di esporvi in brevi parole i lavori eseguiti nel corrente anno.

A. Lavori trigonometricr.

Una quindicina di stazioni trigonometriche di primo ordine sono state fatte nel
Piemonte, una parte delle quali pel rattacco della base del Ticino con la rete trigono-
metrica principale. Inoltre si e cominciata la riconoscenza e si sono eseguite le osserva-
zioni di primo ordine nell’isola di Sardegna. |

L’operazione pit importante che si eseguisce quest’anno è la misura della base
di Somma presso il Ticino. Questa base fu misurata una volta nel secolo passato e

rimisurata all’epoca dell’operazione del parallelo medio. Voi sapete che i risultati ave-

vano presentato alcune anomalie. Era interessante fare una terza misura della medesima
cogli apparati perfezionati dell’epoca attuale. Io aveva pregato 8. E. il generale Baeyer
di mettere a mia disposizione l’apparato internazionale, ma disgraziatamente non era

stato ancora terminato dal suo costruttore.
La necessitä di misurare una base di dieci chilometri coll’apparato di Bessel ci

ha consigliato di aggiungere ai cavalletti dei congegni che ne facilitano il maneggio ed

abbreviano l’operazione del collocamento delle spranghe.
' La misura sara terminata verso la fine di settembre e sara l’oggetto di una

pubblicazione che non manchera d’interesse.
B. Livellazioni di precisione.

Le livellazioni di precisione incominciate l’anno scorso dal commissario italiano
Oberholtzer, d’accordo con i commissari svizzeri, saranno terminate quest’anno. Nello

 

Si
=
=
3
=
=
=
:
z
x
=
U

M

tl

Wi

Lia)

 

2. ouate names NE |

 
