<1 are nn

7
Ich bin glücklich, constatiren zu Können, dass die permanente Commission
berathungsfähig versammelt ist, da nur eines ihrer Mitglieder abwesend ist;
zugleich freue ich mich, dass eine ansehnliche Anzahl anderer Bevollmächtigten
unserer Einladung folgend an den Versammlungen Theil nimmt. Wir sind
sanz besonders erfreut, wiederum ein Mitglied der U. S. Coast Survey unter
uns zu sehen, welches die amerikanische Geodäsie bei der Europäischen
Gradmessung vertritt. | :
Ich erkläre die Sitzung der permanenten Commission für eröffnet.

Der Präsident stellt Herrn Z/&lgard als Vertreter der U. S. Coast Survey vor,
und Herr Hörsch verliest folgendes von dem Superintendenten derselben, Herrn Patterson,
eingegangenes Beglaubigungs - Schreiben:

Washington, June 21th 1878.
To His Excellency |

President of the International Geodesic Association.
Sir,

The Honorable the Secretary of the Treasury of the United States has vested
me with authority to direct a representative of the United States Coast and Geodetic
Survey to attend the meetings of the International Geodesic Association, over whose
deliberations you preside.

It was our good fortune to have had, at the last meeting of the Association,
the representative of our work received most cordially and assigned a seat at the meetings.

Being encouraged by this cordiality, I have assigned to professor J. H. Hil-
gard, the principal Assistant of the Coast and Geodetic Survey and Inspector of Standard
weights and measures, the responsible duty of representing our Survey at your meetings,
and beg that the same courtesy be extended to him that so greatly gratified our re-
presentative at the meetings at Stuttgart. Professor Hilgard, as the representative of
the United States in the international bureau of weights and measures placed at Paris,
as well as by his scientific reputation, is too well known to you, to require a more
formal introduction than a ‘reference to his name.

I am happy to take advantage of this opportunity to offer my most distin-
guished consideration.

I-have sie: honor tothe ei Your most obedient servant

Carlile P. Patterson
Supt. Coast and Geodetic Survey and Standard
weights and measures of the U. S.

 
