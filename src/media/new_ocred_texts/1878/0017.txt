= en EEE mn

g

Ferner ist den Sitzungsberichten der Generalbericht über die Fortschritte der
Europäischen Gradmessung im Jahre 1877 angefügt und zum Schluss noch eine Ab-
handlung des Herrn Plantamour: ,,Recherches expérimentales sur le mouvement simultané
d’un pendule et de ses supports,“ abgedruckt.

Das befriedigende Resultat dieser letzteren Untersuchungen ist, dass den Pendel-
längen, die mit dem Schweizer Apparat ermittelt sind, die Grösse 0”"1724 + 0""0014 für
Genf und 0”"1302 + 00032 für die übrigen Schweizer Stationen, sowie 0""1357 + 0""0027
für Berlin hinzugefügt werden muss. 5

Die permanente Commission hat durch Circular ftir die diesjährige Sitzung:
Hamburg einstimmig gewählt, nachdem dieselbe von dem Hohen Senate dieser Stadt eine
freundliche Einladung erhalten hatte, für welche die permanente Commission hierdurch
ihren lebhaften Dank sich auszusprechen erlaubt.

Die permanente Commission hat, wie leider fast alljährlich, auch dieses Jahr
den Tod eines Commissars, des Directors der Delfter polytechnischen Schule, des Herrn
Cohen Stuart, welcher besonders durch die Nivellements in Holland die Europäische
Gradmessung gefördert hat, zu beklagen. Bewahren wir demselben ein freundliches
Andenken.

Als neuer Commissar für Spanien ist Herr Oberst Barraquer eingetreten.

Mit Freuden können wir constatiren, wie auch aus dem Generalbericht
hervorgeht, dass in allen betheiligten Ländern die Arbeiten der Europäischen Grad-
messung fortgeschritten sind.

Eine Anzahl neuer Publicationen sind seit der vorigen Conferenz erschienen
z. B. die Arbeiten des geodätischen Instituts in Berlin im Jahre 1877, enthaltend die
Bestimmung des Längenunterschieds zwischen Berlin und Paris, Berlin und Bonn,
Bonn und Paris; ferner ein Präcisionsnivellement der Elbe; ein Werk „Ueber die Figur
der Erde“ von Professor Bruns. Ein erster Theil des dritten Bandes der Triangulation
du royaume de Belgique, Observations astronomiques, partie théorique‘, ist erschienen
von dem Dépôt de la guerre in Brüssel. Ferner: ,,Die geodätischen Hauptpunkte der
dänischen Gradmessung und ihre Coordinaten‘‘; eine Publication der Schweizer Commission,
enthaltend die Bestimmung der Längendifferenz zwischen Zürich—Pfänder —Gäbris; eine
Publication der französischen Commissare: ,,Die Längenbestimmung zwischen Paris und
Algier“, aA.

Von den in diesem Jahre ausgeführten Arbeiten werden wir im Laufe dieser
Sitzungen hören, und ist das Material, welches uns zur schliesslichen Ableitung der
Grösse und Figur der Erde dienen wird, auch im verflossenen Jahre wesentlich vermehrt.

Herr Sadebeck trägt auf Ersuchen des Herrn BDaeyer den Bericht des Central-
Büreaus vor.

 
