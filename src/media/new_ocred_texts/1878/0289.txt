| nr in or

en Inn |

(TY Sapa

OL ne

an AMT

i Fr TORR A BE FL

97

Nor wegen.

Es ist kein Bericht eingegangen.

eer?

Oesterreich.

1. Bericht des K. K. Gradmessungs-Bureaus in Wien.

Die Arbeiten des Bureaus der K. K. Oesterreichischen Gradmessung sind wäh-
rend des Jahres 1879 lediglich Reductionsarbeiten gewesen, und es haben sich namentlich
folgende als definitiv zu betrachtende Längenbestimmungs-Resultate ergeben:

Greéenwich==Wien ce 05 de 0 2 on
Berin Wen. ..: 2... & 1 40060
Leipzie-Wien . . ...... OÙ He
Pulkowa— Wien ee 0 60 aa)
Warschau Wien . . . 0 ©! 13" Ro 0
Pola- Wien . ... 2. 0 m Dana
Miinchen—Bregenz . ....& 7 19736

Unter diesen Resultaten verdient die Linge Greenwich—Wien eine besondere
Beachtung, da der oben angeführte Zahlenwerth eigentlich das Resultat zweier unab-
hängig durchgeführten Längenbestimmungen ist, deren eine auf Beobachtungen im Meri-
dian beruht, während für die zweite Döllen’s Methode der Zeitbestimmung im Verticale
des Polarsterns benützt wurde.

Die Beobachtungen der Herren Nahlick und Kühnert im Meridian ergaben den
Werth 12 5™ 21800, jene der Herren Anton und Schram im Verticale des Polarsternes
den Werth 12 5" 21-01. -

Das Arrangement dieser Längenbestimmung war zudem so getroffen, dass gleich-
zeitig die Längen: Berlin— Greenwich und München— Greenwich ermittelt wurden.

Als Reduction des österreichischen Beobachtungspunktes in Greenwich auf den
Transit cercle daselbst wurde der Werth + 0°20 ermittelt.

Erwähnenswerth sind ausserdem noch die für die Längen Pulkowa— Wien und
Warschau— Wien gewonnenen Resultate. Diese Resultate werden durch die russischer
Seits ermittelte Länge Pulkowa— Warschau auf das vollständigste bestätigt, da der
Schlussfehler des Dreiecks nur 0°02 beträgt. Von besonderem Interesse ist hierbei noch
die Uebereinstimmung der hier gewonnenen Resultate mit dem seiner Zeit durch Chro-

13

 
