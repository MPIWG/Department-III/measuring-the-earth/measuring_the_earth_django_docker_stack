In verein nr

 

un AR

Ji Lu!

ANT hie LL

58

Das Nivellementsnetz ist voriges Jahr um 740 Kilometer vergrössert worden
und ist im Osten Spaniens schon nahezu fertig. Ausser den beiden Mareographen, die
schon seit mehreren Jahren zu Santander und Alicante functioniren, hat er einen dritten
nach dem System von Reitz in Cadix aufstellen lassen. Trotz der enormen Schwierig-
keiten, welche die Gewalt des Meeres der Aufstellung an dieser Station entgegenstellt,
hoffe er doch die nöthigen Einrichtungen fertig zu bringen, so dass der Apparat wahr-
scheinlich von Anfang 1880 an in Thitigkeit sein werde.

Herr Ibahez giebt sodann interessante Details über die schwierige Operation
der Verbindung Algiers mit Spanien, die er im Verein mit seinen französischen Collegen
unternommen hat. Er bedauert, weniger gute Nachrichten mittheilen zu können, als die
in der vorigen Sitzung von Herrn Faye gegebenen waren. Die erste Depesche des Herrn
Commandanten Perrier besagt in der That, dass er von M’Sabiha am 10. September die
elektrischen Lichter von Tetica und Mulhacen gesehen hat, aber er spricht nicht davon,
das Licht von Filhaoussen gesehen zu haben. Seitdem hat er eine Depesche von Tetica
erhalten, die ihm mittheilt, dass das Licht von M’Sabiha gesehen worden ist, aber das
von Filhaoussen ist weder am Tage noch in der Nacht gesehen worden. Endlich hat
Herr Commandant Perrier am 17. telegraphirt, dass man seit dem 10. gar nichts gesehen
habe. Herr Ibanez ist hierher nach Genf direct von Mulhacen gekommen, wo er sich
vier Tage und vier Nächte aufgehalten, ohne dass die Witterung ihm erlaubt hätte,
eine der drei anderen Stationen zu sehen; aber er hat Gelegenheit gehabt, die grosse
Aufopferung schätzen zu lernen, welche man haben muss, um auf diesem ungastlichen
Gipfel auszuhalten. Die spanischen Geodäten werden in jedem Falle, so lange es Men-
schen möglich ist, dort bleiben, und so bedauerlich es auch sein würde, wenn die Jahres-
zeit die Operationen vor ihrer Vollendung abzubrechen nöthigte, so versichere er doch,
dass man alsdann nächstes Jahr die Aufgabe wieder von Neuem in Angriff nehmen
werde

Herr von Bauernfeind dankt Herrn Ibalez und wünscht, dass die schöne und
kühne Unternehmung der französischen und spanischen Collegen, durch welche ein grosser
Fortschritt in unserer Kenntniss der Gestalt der Erde erlangt werden wird, ge-
lingen möge.

Herr Ibanez übernimmt wieder das Präsidium und giebt Herrn Villarceaw das
Wort, um folgende Mittheilung über die Höhen-Messungen vorzutragen:

„Wiederholt hat die Frage der geometrischen Nivellements Gelegenheit zu Con-
troversen gegeben, die vielleicht nicht entstanden wären, wenn die Geodäten über die
Definition der „Höhen“ übereingekommen wären. Der Gegenstand dieser Notiz ist der
Vorschlag einer solchen Definition. ‚Ich wünschte alle zur Vervollständigung der Einsicht
in diesen Gegenstand nothwendigen Entwickelungen vorlegen zu können, aber die Ur-
sache, welche die Veröffentlichung der Abhandlung verzögerte, die ich mir erlaubte, der

*) Siehe Generalbericht für 1879, Spanien.

 
