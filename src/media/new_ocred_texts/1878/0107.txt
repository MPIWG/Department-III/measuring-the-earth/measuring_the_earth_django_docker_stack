Tim ren mem en uni on

SRT ST |

ven GE TM

rrr

2. AL

rire

PETITION TT

Preussen.

Der neue Basisapparat, den das geodätische Institut 1876 in Paris bei den
Herren Gebrüdern Brunner bestellt hatte, ist 1878 fertig geworden und unversehrt hier
angekommen. Da dem Institut noch immer ein Dienstlokal fehlt, so ist derselbe in dem
7 Kilometer von hier entfernten Steglitz in einer Villa mit Garten miethweise vorläufig
untergebracht. — Mit dem Eintritt der besseren Witterung sollen die Constanten des
Apparates bestimmt, und in dem Garten Probe-Messungen vorgenommen werden. —
Von dem Ausfall dieser Untersuchungen wird es abhängen, ob im Laufe des nächsten
Sommers schon die Nachmessung einer Grundlinie wird stattfinden können oder nicht.

Was die Ausführung des Apparates anbetrifft, so haben die Herren Gebrüder
Brunner demselben eine Vollendung gegeben, wie sie nur von solchen Künstlern erwartet
werden kann. — Die grossen Verdienste, die sich Herr St. Claire-Deville um die Her-
stellung des Platin-Iridium-Stabes erworben hat, lassen sich hier nur kurz andeuten.
Wer dieselben genauer kennen lernen will, findet sie in dem Separatabdruck, *) welcher
dem Generalbericht als Anhang beigefügt ist.

Ausserdem hat das Institut ebenfalls aus der Werkstatt der Gebrüder Brunner
einen Theodoliten erhalten, welcher dem des Herrn Commandanten Perrier ähnlich ist.
Derselbe hat 0”42 Durchmesser, ist mit beweglichem Faden versehen und für Nacht-
beobachtungen mit centraler Beleuchtung von oben eingerichtet.

Das geodätische Institut muss als Centralbureau der Europäischen Gradmessung
und als ausführendes Organ der permanenten Commission neben seinen practischen
Arbeiten sich auch in beständiger Uebersicht aller neuen Publicationen auf dem weiten
Gebiete der Gradmessung zu erhalten suchen, d. h. es muss die neu erscheinenden
practischen Gradmessungs-Arbeiten genau kennen lernen, und muss die theoretischen
Forschungen auf dem Gebiete der mathematisch-physikalischen Wissenschaften wenig-
stens so weit verfolgen, um zeitgemässe wirkliche Verbesserungen in Vorschlag bringen
zu können.

Zur Erreichung des ersteren Zweckes finden allwöchentlich an einem Abend in
den Räumen des Instituts unter dem Vorsitz des ältesten Seetions- Chefs Zusammen-
künfte der Mitglieder des Instituts statt. In diesen Zusammenkünften werden allgemein
wissenschaftliche Gegenstände besprochen, Berichte über neue Publicationen erstattet
oder selbständige Vorträge gehalten.

Durch diese Einrichtung wird zwar der eine Zweck: zur Information der Mit-
glieder des Instituts im ganzen Umfange ihrer dienstlichen Thätigkeit, erreicht; allein
es fehlt noch ein anderer wichtiger Factor: Die Erhaltung des Instituts auf
der Hohe der Wissenschaft, d.-h. die Fortentwickelung seiner Mitglieder in den-

*) Derselbe ist den Herren Bevollmächtigten bereits zugesendet worden.

 
