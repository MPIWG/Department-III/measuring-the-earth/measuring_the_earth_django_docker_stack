 

N
|
!

 

m

 

 

6 H. SAINTE-CLAIRE DEVILLE ET E. MASCART.

Soient, en effet,

V, le volume du-tube à zéro;

¢ le volume à zéro du gaz qui en est sorti;

k le coefficient de dilatation du tube entre zéro et 100°;

a le coefficient de dilatation de l’azote sous pression constante;
T la température de l’experience.

En écrivant que le volume du tube à zéro est égal à la somme des
volumes du gaz sorti, lequel est à zéro dans le voluménomètre, et du
gaz qu'il renferme 4 la température T, ce dernier étant ramené à zéro,

on obtient l'équation

1+ 3kT
u. VY, ——
Pe

?

d’où l’on déduit

En prenant la valeur de & sous la dernière forme, on peut remarquer
que le premier terme est très petit et peut être évalué une fois pour
toutes d’une manière approchée, ce qui simplifie beaucoup les calculs.
La valeur de ce terme est d’environ 0,000036 pour les expériences
faites au voisinage de 100°, ce qui donne

I I

uw |

a — 0,000030 +

v

Le poids p du mercure écoulé occuperait, à la température de zero,
le volume ¢.

D’autre part, le tube, comme on l’a vu, renferme, à la température
de zéro, un poids d’eau égal a 1"8,0388125; il renfermerait a la méme
température un poids de mercure égal a

1, 090012) ~<13,00701 — 14,125366.

On a donc
Vo oe LA, 125366

v p

Le poids p était d’environ 3*8, 750.

 

werner her ee een

|
|

 
