 

SS ne

RP EN NET PERS

PR ET EPP EPP

a

N

ee aaEaEaELEEEEL

h
}
hr
rf

i
f
It
|
i
i
|

116

walde—Strauch—Raschiitz in einer Gesammtlinge von 93 Kilometer einfach nivellirt.
Die Strecke Raschiitz—Grossenhain—Lempertswalde erhielt dann im November noch ein
Controlnivellement durch den Assistenten Resch, weil in dieser Linie die Basispunkte
mit. eingeschlossen sind.

Leipzig und Dresden, Ende Februar 1879.

C. Bruhns. A. Nagel.

Schweiz — Suisse.
1. Travaux astronomiques et nivellements de précision.

Ainsi qu'il a été dit dans le dernier rapport, les travaux astronomiques qui ont
figuré sur le programme de la part que la Suisse avait à contribuer pour la grande
entreprise géodésique, sont terminés sur le terrain, et nous n'avons plus qu'à achever
la réduction et à soigner, en commun avec nos collégues des autres pays, la publication
des opérations de longitude exécutées dans ces dernières années. M. Plantamour corrige
dans ce moment les épreuves du mémoire qui rend compte de-la détermination de
longitude entre Genève et Strassbourg et la publication concernant l'opération entre
Munich et Genève paraîtra également encore en 1879.

Il n’a pas été fait de recherches de pendule chez nous dans l'année 1878. A
l'occasion de l’intéressant rapport de M. Peirce, qui se trouve publié dans ce volume,
nous sommes heureux que notre collègue Américain reconnaît maintenant la différence
appréciable entre les déterminations de la constante de flexion par voie statique ou
dynamique. Quant au mémoire de M. Peirce publié dans le No. XX VII des „Proceedings
of the American Society, M. Plantamour demande à constater que, contrairement à
l'opinion que M. Peirce lui attribue par erreur, il n’a jamais prétendu que, dans le
mouvement simultané du pendule et du trépied, il y eût une inégalité dans la période
des deux mouvements; du reste l’observation montre directement le contraire, puisque
le pendule et le trépied arrivent simultanément à l'amplitude extrême de leur oscillation.

M. Plantamour a énoncé seulement l’idée que dans l’oscillation du pendule la
déviation produite par .la flexion pouvait être un peu (mais pas considérablement‘)
plus faible que si une force horizontale égale à la composante horizontale du poids du
pendule était appliquée dans une expérience statique. Or les expériences de M. Peirce
confirment cette idée, puisqu'il trouve que la flexion est de 2 pour cent plus faible dans
l'expérience dynamique que dans l'expérience statique, si le trépied est sur un support
très rigide, et de 6 pour cent plus faible, si le trépied est mis sur un support en bois.
De plus M. Peirce a appliqué dans ses expériences statiques une force horizontale équi-

 

pou el 1 084 A le |

I!

fab iki)

 

20: eut auæs, moe hi |

 
