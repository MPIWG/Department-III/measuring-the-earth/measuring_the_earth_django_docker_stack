 

im

UR a a | RE

47

Angesichts der vorgerückten Zeit verschiebt der Präsident die Fortsetzung der
Berichte auf die nächste Sitzung, doch giebt er noch Herrn Æ Sainte-Claire Deville das
Wort, weil derselbe morgen nach Paris zurückzukehren genöthigt ist.

Herr Deville legt in seinem und Herrn Mascart’s Namen eine zweite Abhand-
lung über die Construktion des geodätischen internationalen Maassstabes vor. Sie ent-
halt die Bestimmung des Ausdehnungscoéfficienten dieses Maassstabes zwischen 0°, 320,
78° und 100° und die empirische Formel, welche die Ausdehnung zwischen diesen
Grenzen darstellt. Die Abhandlung wird den Publikationen des Central-Bureaus beige-
geben werden.

Bei Besprechung der Masse des Platin-Iridiums, welche das Gewicht des in der
Thermometerröhre enthaltenen Wassers darstellt und über die Beständigkeit oder Ver-
änderlichkeit des Metalles, aus welchem der internationale geodätische Maassstab
gearbeitet ist, Auskunft geben soll, geht Redner auf einige die Wägungen von grosser
Genauigkeit betreffende Punkte ein.

Er legt in seinem und seines Mitarbeiters Namen grosses Gewicht auf die zer-
störende und tiefgehende Einwirkung, die der immer kieselhaltige Staub auf die Waagen
und auf die in mit Sammet ausgepolsterten Kapseln eingeschlossenen Gewichte ausübt.
Er räth an, beständig in eine Waage mit doppeltem Gehäuse einen trockenen und
mittelst Baumwolle durchgesiebten Luftstrom von grosser Geschwindigkeit eintreten zu
lassen, ferner die Gewichte nur mit Elfenbeinstützen in Berührung zu bringen und sie
in einem vollkommen luftleeren Raum aufzubewahren.

Alle diese Fragen betreffs der Wägungen von grosser Genauigkeit und der Con-
struktion der Gewichte werden in einer Abhandlung besprochen werden, die nächstes
Jahr der permanenten Commission vorgelegt werden wird.

Nachdem der Präsident Herrn Sainte-Claire Deville gedankt hat, schliesst er
die Sitzung um 4 Uhr 15 Minuten und bestimmt für die nächste Zusammenkunft den
folgenden Tag, Mittwoch den 17. September, 10 Uhr Morgens.

Die Herren Plantamour und Soret laden die Mitglieder der Versammlung: ein,
morgen Nachmittag die Werkstätten der Gesellschaft für Construktion physikalischer
Instrumente, sowie die Sternwarte mit dem daselbst soeben aufgestellten . Aequatorial
zu besuchen.

 
