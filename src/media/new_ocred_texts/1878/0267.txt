ee

 

ii AMT

ML ji

PATER BR

19

 

messungs-Commission im Jahre 1879 einen Stillstand, es kamen nämlich die beabsich-
tigten Bestimmungen der geographischen Breite des Dreieckspunktes Altenburg bei
Bamberg und des Azimuths der Seite Altenburg—Banz nicht zur Ausführung; dagegen
hat der Freund und Mitarbeiter des Verstorbenen, der nunmehrige Herr Oberst C. von
Orff, die ihm so unerwartet bereitete Musse dazu benützt, die schon lange vorbereiteten
Manuscripte über früher unter seiner Mitwirkung von der Sternwarte in Bogenhausen
für die Europäische Gradmessung vorgenommene astronomische Arbeiten zu vollenden
und der Bayerischen Commission zur Genehmigung des Druckes vorzulegen. Auf Grund
dieser Vorlage wird in nächster Zeit eine etwa 19 Bogen umfassende Abhandlung über
die von der Königl. Sternwarte zu Bogenhausen in den Jahren 1873 bis 1875 ausge-
führten Breiten- und Azimuth-Bestimmungen an die Herren Gradmessungs-Commissäre
vertheilt werden. In derselben sind folgende neue Publikationen enthalten:

1. die Bestimmung der geographischen Breite des trigonometrischen Hauptpunktes
in Nürnberg;

2. die Breitenbestimmung zu Mittenwald, in den Alpen, nahe der südlichen
Landesgrenze gegen Tirol;

3. die Breitenbestimmung zu Holzkirchen auf der südbayerischen Hochebene,
in einer Entfernung von 14 Kilometer von den nördlichsten Bergen der
Alpenkette;

4 die Bestimmung der geographischen Breite zu Ingolstadt, zunächst nach
Horrebow’s Methode, dann durch Circummeridian-Höhen südlicher und nörd-
licher Sterne;

5. die Ermittlung der Polhöhe des trigonometrischen Hauptpunktes auf der Veste

Wülzburg (am nordwestlichen Abfalle des Jura) nach den eben genannten
zwei Methoden;

6. die Azimuth-Bestimmung der Hauptdreiecksseite Wülzburg— Eichelberg in
Wülzburg,

während folgende astronomische Arbeiten schon in anderen Schriften gedruckt und den
Herren Commissären mitgetheilt wurden, nämlich:

7. die Bestimmung des Azimuths der Dreiecksseite Aufkirchen—Wendelstein auf
dem westlichen Thurme der Sternwarte Bogenhausen und

8. die Bestimmung des Azimuths der Seite Oberföhring — Aufkirchen von der
Basispyramide bei Oberföhring aus, in dem Werke ‚Die Bayerische Landes-
vermessung in ihrer wissenschaftlichen Grundlage“, München, 1873;

9. die Bestimmung der geographischen Breite der Königl. Sternwarte bei München
nach zwei verschiedenen Methoden, in dem Supplement zum XXI. Bde. der
Annalen dieser Sternwarte und in einem Separat-Abdrucke; endlich

10. die Bestimmung des geographischen Längenunterschiedes zwischen München
und Genf durch die Herren Plantamour und Orff als besondere Druckschrift
10*

 
