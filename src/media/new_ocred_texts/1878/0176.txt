 

|
|
|
|
|
|
|

|
i
|
|
|
|
a

 

46 H. SAINTE-CLAIRE DEVILLE ET E. MASCART.

L’eau, distillée avec un peu de sulfate d’alumine, distillée de nou-
veau avec un peu de permanganate de potasse, est enfin distillée seule
et condensée dans un serpentin en platine. On doit la conserver égale-
ment dans le platine et ne s’en servir qu'après l'avoir fait bouillir
longtemps et lavoir refroidie rapidement. Elle ne doit laisser aucun
résidu par évaporation.

Le mercure doit être distillé dans un courant d’air et dans une
cornue de fer. On le laissera ensuite longtemps en contact avec de
l'acide sulfurique et du sulfate de mercure. En l’&vaporant dans un
moufle à basse température, chassant ensuite le peu d'oxyde de mer-
eure qui reste par l’action d’une température plus élevée, le résidu
devra être nul.

Nous terminerons ce Mémoire par une description succincte du
procédé destiné à fixer d’une manière indépendante de toute gran-
deur variable avec le temps la longueur du millimètre qui nous servira
d'unité.

Dans l’appareil employé pour déterminer le coefficient de dilatation
du platine iridié, le tube le plus étroit (£), qui sert de'témoin tempo-
raire et qui est plongé constamment dans la glace, porte à chaque extré-
mité, du lieu d’un trait unique, un millimetre divisé en dixiemes. Apres
avoir pointé, par exemple, les deux traits du gros tube à une tempéra-
ture quelconque, on amène le tube témoin sous les microscopes et
l'on pointe à chaque extrémité la division du millimètre qui se trouve
la plus rapprochée du croisement des fils. Comme la valeur du tambour
micrométrique en fonction du millimetre divisé est facile a évaluer
par expérience, on connait done en fonction des millimetres traces sur
le tube témoin la dilatation qu'a éprouvée le gros tube entre deux tem-
pératures successives, telles que zero et 100 degrés.

On à supposé implicitement que Îles deux millimètres sont iden-
tiques. Cette identité est très-probable, puisque les millimètres ont été
tracés par la même machine et à la même époque. Toutefois, ıl est fa-
cile d’en vérifier l’exactitude en pointant les microscopes sur deux di-
visions, puis en répétant un nouveau pointé après avoir fait glisser
l'auge d’une petite quantité dans le sens de sa longueur. La mesure de
ce déplacement par les deux microscopes doit être la même.

cote epee de lt bol MBM il té + ds À à 4

sense bh A badd did cl ne

1
|

 

 
