II me Tr nr =

 

Rt

per rir

PP PET VIR DETTE TT

69

Baden.

Die Winkelmessungen zu der Badischen Gradmessungs- Triangulation wurden
schon im Jahre 1877 beendigt. Es wird beabsichtigt, die Ausgleichung des ganzen
Rheinischen Dreiecksnetzes von der Bonner Basis bis zum Anschluss an die
einem Guss auszuführen.

Die Ausgleichungs-Rechnungen haben bereits begonnen und schreiten in er-
wünschter Weise fort.

An Nivellementsarbeiten für die Badische Gradmessung wurden ausgeführt:

1. Das Nivellement von Radolfzell bis Friedrichshafen.

Die fünfte allgemeine Conferenz (1877 in Stuttgart) hatte den Wunsch aus-
gesprochen, baldmöglichst die Lücke ausgefüllt zu sehen, welche zur Zeit in dem den
Bodensee umschliessenden Nivellement noch offen war. Das Central-Büreau übernahm
die Ausfüllung derselben für 1878 und übertrug dem Assistenten Seibt die Ausführung,
deren Resultate in dem nachfolgenden Bericht enthalten sind:

Schweiz in

Nivellement

zwischen der Höhenmarke am Stationsgebäude zu Radolfzell und
der Hohenmarke an der Hafenmauer zu Friedrichshafen.

Erklirung der Zeichen:

= Mittelpunkt der centrischen Bohrung eines horizontal eincementirten Messingbolzens.

Die vor dem Bolzen angebrachte eiserne Schutzplatte trägt die Inschrift: „‚Höhenmarke.«
/\ = Eingemeisseltes Dreieck.
dy, == Ausgemeisselte Dreiecksfliche.

Vorbemerkungen:

1. Das Nivellement ist von dem Unterzeichneten in der Zeit vom 12. Juni bis 25. Juni 1878 nach der
im „Präcisions-Nivellement der Elbe, Publication des Königlichen geodätischen Instituts“ pag. 8 bis 14
und pag. 41 bis 45 beschriebenen Beobachtungsmethode ausgeführt worden, und zwar:

von [H,M] Radolfzell bis AN Stahringen auf dem Eisenbahnkörper,

» /A Stahringen » 2% Maurach auf der Chaussee,

» fr Maurach » Âx Immenstaad auf dem sich unmittelbar am Bodensee hinziehenden
Fusswege,

» A) Immenstaad bis am Stationsgebäude zu Friedrichshafen auf der Chaussee,

„ [HM am Stationsgebäude zu Friedrichshafen bis [H.M] an der Hafenmauer zu Friedrichs-
hafen auf dem Eisenbahnkörper.

2. An sämmtliche Höhenunterschiede und Ordinaten ist die Correction, welche sich für die verwendeten

Nivellirlatten durch deren Vergleichung mit dem Schweizer Meter ergab, angebracht.

 
