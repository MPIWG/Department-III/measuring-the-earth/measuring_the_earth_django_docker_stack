 

 

40

allem, was zur Theilnahme der Niederlande an der europäischen Gradmessung nöthig
ist, zu beschäftigen hat. Sie besteht aus den Herren
F. J. Stamkart, ältestem Professor des Polytechnikums in Delft,
J. A. C. Oudemans, Director der Sternwarte in Utreeht,
_H. G. van de Sande-Bakhuyzen, Director der Sternwarte in Leiden,
J. Bosscha, Director des Polytechnikums in Delft,
G. V. van Diesen, Chef-Ingenieur der Wasserbauten in Middelburg.

Die Commission hat Herrn Stamkart zum Präsidenten, Herrn Oudemans zum
Vice-Präsidenten, Herrn Bosscha zum Secretair ernannt.

Die permanente Commission beauftragte ihre Schriftführer, die Sitzungsberichte
zu redigiren und dem Central-Bureau zum Druck zu übergeben. Selbiges ist geschehen,
und liegen die Sitzungsberichte in deutscher und französischer Sprache vor und sind
denselben zwei Abhandlungen:

1. Vorläufige Vergleichung der Höhenlagen Europäischer Meere,

2. Sur la construction de la règle géodésique internationale par MMrs. Sainte-

Claire Deville et E. Mascart

hinzugefügt. Die permanente Commission ist den Autoren der unter 2 genannten Ab-
handlung zu ganz besonderem Danke verpflichtet. Einige wissenschaftliche Mittheilungen
betreffend die Theorie des Reversionspendels, besonders den Einfluss der cylindrischen
Form der Schneiden und das Rollen derselben, sowie über eine strenge geometrische
Definition der Höhendifferenz zweier Punkte auf der Erdoberfläche, welche Herr Villar-
ceau geben wollte, hat derselbe vertagt und wird in der gegenwärtigen Conferenz dar-
über Mittheilung machen; die erste Frage ist in den Annales de l’Observatoire de Paris
behandelt.

Den Sitzungsberichten der permanenten Commission ist ferner der General-
bericht über die Fortschritte der Europäischen Gradmessung im Jahre 1878 angefügt;
wir haben darin zum ersten Male eine Mittheilung des U. S. Coast Survey zu
registriren und wird Herr Hilgard für den nächsten General-Bericht eine Abhandlung
über den Einfluss der amerikanischen Gradmessung auf die Gestalt der Erde einsenden.

Auch dieses Mal können wir mit Freuden constatiren, wie auch aus dem General-
bericht hervorgeht, dass in dem letzten Jahre die Arbeiten der Gradmessung in allen
betheiligten Ländern fortgeschritten sind. Der von dem Central-Bureau bestellte Basis-
apparat zur Nachmessung von Grundlinien ist fertig geworden und wird bereits zu
einer Nachmessung der schlesischen Grundlinie verwendet. Und wiederum ist eine An-
zahl von Publikationen zu verzeichnen, in welchen verschiedene astronomische, trigono-
metrische und nivellitische Arbeiten enthalten sind. Wir führen hier nur auf: die astrono-
misch-geodätischen Arbeiten des Kgl. Preuss. geodätischen Instituts, welche die Längen-
bestimmung zwischen Berlin—Altona—Helogoland , Altona—Bonn—Wilhelmshaven, Al-
tona—Wilhelmshaven enthalten; die ,,Détermination télégraphique de la différence de
longitude entre Geneve et Strasbourg von Plantamour und Low. In den Annales de
l'Observatoire de Marseille finden wir die Beobachtungen zu den Längendifferenzen
Paris—Marseille und Algier—Marseille.

it ue à Mau ad a a el a lee

 

 
