TI ernennen nn ori ne

PT RY RTFM T

1008 7

im

ne

a ae

ern

27
Der Präsident stellt die Frage über den nächsten Versammlungsort der perma-
nenten Commission zur Discussion. Es wird einstimmig beschlossen:

Die permanente Commission wird durch Circular im nächsten
Frühjahr den Ort näher bestimmen.

Die Herren Repsold laden zum Besuche ihres mechanischen Institutes ein,
welche Einladung dankend angenommen wird.

Der Präsident fordert die Versammlung auf, für die herzliche gastliche Auf-
nahme und für die bereitwillige Unterstützung, welche der Senat der Stadt Ham-

burg der Versammlung gewährt habe, durch Erheben von den Sitzen dem warmen
Danke Ausdruck zu geben.

Alle Anwesenden erheben sich. -
Herrn Senator Hayn wird für die gastfreie Aufnahme, mit welcher derselbe der

Commission die Räume seines Hauses für ihre Sitzungen zur Verfügung gestellt hat, .
der Dank der Versammlung votirt.

Das Büreau wird beauftragt, den Dank der Commission dem Herrn Dr. Kirchen-
pauer und dem Herrn Senator Hayn schriftlich zu übermitteln.

Der Präsident setzt die Vollziehung der Protokolle für die Mitglieder der per-
manenten Commission zu Sonntag an und schliesst die allgemeinen Sitzungen.
Schluss der Sitzung 4 Uhr.

45

 
