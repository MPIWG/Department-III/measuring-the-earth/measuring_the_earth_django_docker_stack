 

100

jenigen Wissenschaften, welche bei der Gradmessung Anwendung finden. Den Praktikern,
die im Sommer mit Beobachtungen, im Winter mit Berechnung derselben vollauf be-
schäftigt sind, bleibt nicht die Zeit, sich durch Selbststudium an der Spitze der Fort-
schritte dieser Wissenschaften zu erhalten.

Um den Interessen des Instituts auch in dieser Beziehung Rechnung zu tragen,
hat auf mein Ersuchen der Professor an der hiesigen Universität und Mitglied des
wissenschaftlichen Beiraths Herr Dr. Bruns seit dem Herbst vorigen Jahres es über-
nommen, in zwei wöchentlichen Doppel- Stunden streng wissenschaftliche Vorträge über
Geodäsie und damit verwandte Wissenschaften ebenfalls während der Abendstunden in
den Räumen des Instituts zu halten.

Die Zuhörer bestehen:

1. aus den Mitgliedern des Instituts nach freier Wahl,

2. aus Dilettanten, die sich fiir die Gradmessung interessiren,

3. aus einigen Studenten, die Herr Professor Bruns für befähigt hält, seinem
Vortrage zu folgen.

Diese Vorträge gewähren erstens den Sections-Chefs ein einfaches Mittel, sich
die Uebersicht über die ihrem Specialfach ferner liegenden Theile der Wissenschaft zu
erhalten; zweitens erleichtern dieselben den Assistenten wesentlich das Selbststudium
und drittens wird aus den übrigen Zuhörern der künftige Ersatz gut vorbereiteter
Assistenten herangebildet werden können.

Für die theoretische Fortbildung und Ausbildung der Assistenten ist auf diese
Weise ausreichend gesorgt. Es fehlt aber noch immer an einem Dienstlokal, in welchem
neu eingetretene Assistenten im Beobachten unterwiesen werden können; doch ist Hoff-
nung vorhanden, dies letzte Desiderium in nächster Zeit erfüllt zu sehen.

Berlin, im Februar 1879.

Der Präsident des Königlichen geodätischen Instituts

Baeyer.

Specialberichte der Sectionen.
A. Bericht der Section des Professor Dr. Sadebeck.

Im Winter 1877—78 sind die Ausgleichungsrechnungen für das märkisch-
thüringische Netz bis zur Auflösung der Endgleichungen fortgeführt worden. Ausserdem
hat der Unterzeichnete den Druck der Verhandlungen der allgemeinen Conferenz in
Stuttgart und des Generalberichts überwacht und für denselben Uebersetzungs- und
Prüfungs-Arbeiten ausgeführt. Ferner hat sich der Assistent Werner an der von dem

Sections- Chef Professor Dr. Fischer in Beziehung auf Kreistheilung und Biegung der

Fernröhre unternommenen Prüfung der Instrumente betheiligt, welche für die Ausführung
der Sommerarbeiten bestimmt waren.

 

a le 1

nr dd

kn nl

 

a semeniannedinans, mom Sih |

 
