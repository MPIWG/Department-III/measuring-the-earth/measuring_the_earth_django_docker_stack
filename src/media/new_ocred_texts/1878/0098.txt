 

f
i
i
i
a
i
Ik

|
|
%

Norwegen.

Die im letzten Berichte angekündigte Neumessung des südlichen Theiles unseres
Dreiecksnetzes ist in den zwei letzten Jahren ausgeführt worden, und zwar wesentlich
von den Herren Prof. Schiötz und Observator Geelmuyden. Betheiligt dabei war auch
Unterzeichneter durch die bezüglichen Richtungsbeobachtungen auf den Stationen Höge-
varde und Dragonkollen, an welchen beiden Punkten ich ausserdem die zur
Bestimmung je eines Azimuths — am letzten Orte auch der Polhöhe — nöthigen
Beobachtungen angestellt habe. Auf diese Polhöhenbestimmung habe ich geglaubt ein
besonderes Gewicht legen zu müssen, weil der schwedische Punkt Dragonkollen
die südlichste astronomische Station des norwegischen Gradmessungsnetzes bildet.

Die Commission wird nächstens dem Centralbureau den jetzt zum Druck über-
gebenen Bericht über die im Jahre 1864 ausgeführten Basismessungen als erste Pu-
blication einreichen können. Daran wird sich bald schliessen die nahezu fertige Aus-
gleichung des ebenfalls im Jahre 1864 von dem damaligen Observator Mohn gemessenen
Dreiecksnetzes, welches die Verbindung der Grundlinie auf dem Egeberg mit der
Sternwarte und mit der Hauptdreiecksseite Toas—Kolsas vermittelt.

Ferner liegt zum Druck fertig eine Arbeit über die Beobachtungen des regi-

‘ strirenden Pegels bei Oscaburg (7 Jahrgänge discutirt).

Registrirende Mareographen werden in den folgenden Hafen in diesem Jahre
eingerichtet werden:

Christiania,

Arendal,

Bergen,

Christiansund,

Kabelvaag (Lofoten),

Tromso,

Vardo.

Die vor zwei Jahren ausgesprochene Hoffnung, die projectirten Nivellements-

- arbeiten bald anfangen zu können, ist bisher nicht in Erfüllung gegangen. Die vor-

jährige Nationalversammlung hat die dazu beantragten Mittel nicht bewilligt, und die
Commission hat es bei der gegenwärtig gespannten finanziellen Lage nicht für angemessen
erachtet, den Antrag zu erneuern. Man wird, was diese in zweiter Linie gestellten
Arbeiten betrifft, bessere Zeiten abwarten müssen.
Christiania, den 25. Februar 1879.
Für die norwegische Commission

C. Fearnley.

eee

 

3
=
=:
=

a
a

©:
zs)
i
x

WI 1

Il

al ml

 

 
