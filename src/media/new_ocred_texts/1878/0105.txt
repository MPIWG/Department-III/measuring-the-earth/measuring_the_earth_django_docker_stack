Wii TR. WNT ITNT

ITP NT Prarie cn

tr

ET 2

&
=
=
=
à
:
ë
=
=
=
=

97

 

à cause de l'invasion intempestive des grandes pluies; et comme, dès qu'il s'agit de
mesures de haute précision, le principe connu doit régner, que la plus grande
certitude des résultats ne s’atteint point autant par un grand nombre
d'opérations moins exactes, que par un petit nombre d'opérations pré-
cises dans tous les éléments, nous recommandons toujours aux observateurs à
nos ordres de profiter seulement des jours, dans lesquels se présente une atmosphère
limpide, et où les objets lointains montrent, du moins pendant quelques heures, des
images tranquilleæ ou seulement agitées d’une très petite ondulation.

A cause de cela, nous le répétons, il n'a pas été possible d’obtenir, dans
l’époque mentionnée, autant de travaux utiles qu’on aurait pu l’espérer, tandis que les
fatigues et les incommodités des observateurs, déjà si grandes, ont augmenté avec les
contrariétés du temps. Néanmoins, nous allons rapporter succinctement toutes les opéra-
tions géodésiques qu’on a pu réaliser en Portugal dans cette année.

Stations d’angles.

On à terminé toutes les observations sur cinq sommets géodésiques de premier
ordre, savoir: S. Miguel (Algarve), Aspa, Cercal, S..Nomedio et Galiñeiro. Ces
deux derniers points sont situés en Espagne (Galice), celui de Galiheiro étant d'une
très grande importance, parce qu’il appartient en commun aux chaînes géodésiques
fondamentales des deux royaumes de la péninsule Ibérique.

Les instruments employés, aussi bien que les méthodes suivies dans Vobseryation
des angles azimutaux et des distances zénithales, ont été les mémes que ceux déjà
indiqués dans les rapports précédents, c’est-à-dire, on a observé tous les angles au
moyen de théodolites de Repsold et de grands altazimuts de Troughton, en employant
les réitérations systématiques. ;

On à toujours mesuré les distances zénithales par des observations croisées
(cercle vertical à gauche et à droite), et en considérant que dans les altazimuts de
Troughton on ne peut pas faire les réitérations au vertical, on a observé le même point
avec tous les fils horizontaux du réticule dans le but de compenser, autant que pos-
sible, ce défaut considérable.

Les héliotropes ont été employés seulement dans les cas de visibilité difficile.

Nivellements spéciaux.

En considération de l'importance de la station du Galineiro, ci-dessus citée,
on à exécuté un nivellement géodésique spécial entre ce point géodésique et les repères
du niveau des eaux moyennes de la mer au port de Camindra.

On a choisi, en outre, tous les points nécessaires à la jonction hypsométrique
du sommet du Galiñeiro avec celui de lOural, dont l'altitude est déjà déterminée par
deux nivellements de précision, qui se rattachent à l'horizon fondamental, qu'on déter-
mine au moyen dune longue série d'observations de marées près de Villa do Conde.

15

 
