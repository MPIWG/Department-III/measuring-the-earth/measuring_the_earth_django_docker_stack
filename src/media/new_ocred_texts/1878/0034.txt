 

dürfen.

un
en

20e |
Die Mannheimer Sternwarte befindet sich in einem hohen, stark massiven Thurm
des Grossherzoglichen Schlosses, derselbe eignet sich wenig zu einer Sternwarte, aber
vortrefflich zu einem Refractions- Observatorium. Die Verlegung der Sternwarte nach
Heidelberg, um dort eine namhafte Lücke in den Universitäts-Studien auszufüllen, wird
auch bereit$ von der Grossherzoglichen Regierung beabsichtigt, es fehlen aber zur Zeit
noch die erforderlichen Geldmittel. Die Gründung und Dotirung eines selbstständigen
Refractions-Observatoriums kann aber der Badischen Regierung nicht zugemuthet wer-
den, sie ist nur von Seiten der Reichsregierung, möglich und auch dann nur ausführbar,
wenn die Badische Regierung den Thurm, in welchem die Sternwarte sich gegenwärtig
befindet, zu diesem Zweck zur Disposition stellt. Es kommt also Alles darauf an, die
Regierung des Deutschen Reiches dafür zu interessiren. —
Zu diesem Zwecke wäre eine Erklärung von Seiten der permanenten Commis-
sion, dass die Organisation eines solchen Observatoriums zeitgemäss und wünschens-

werth wäre, sicherlich von der günstigsten Wirkung.
Baeyer.

Herr Peters theilt mit, dass von der Kieler Sternwarte Refractionsbeobachtungen
regelmässig, und zwar in einer Richtung über den Kieler Hafen, in einer andern Rich-
tung über das Land, angestellt werden.

Nach Erörterungen der Herren von Oppolzer, Bruhns, Hirsch, Ferrero, von
Bauernfeind wird der Antrag des Herrn General Baeyer in folgender Form:

Die permanente Commission erklärt, dass zum Studium der
terrestrischen Refraction besondere Observatorien fir die
Wissenschaft nützlich und wünschenswerth sind,

einstimmig angenommen.
Herr Villarceau bittet um die Erlaubniss, noch zwei Punkte kurz anregen zu
Zunächst wünscht er in Betreff der Theorie des Reversionspendels den wichtigen
Punkt der cylindrischen Form der Schneiden und den Einfluss des Rollens derselben,
womit er sich kürzlich beschäftigt hat, in einer kurzen Mittheilung für den General-
"Bericht behandeln zu dürfen.

Zweitens hält er es für nothwendig, eine strenge geometrische Definition der
Höhendifferenz zweier Punkte auf der Erdoberfläche aufzustellen, um Missverständnissen
bei der Benutzung der Nivellements für die geodätischen Studien vorzubeugen. *)

*) Nach einer später erhaltenen Mittheilung des Herrn Villarceau zieht derselbe vor, diese
Gegenstände in der nächsten Conferenz zu behandeln. 4A. H.

 

ah Am ay

1 11 ed

abet telly |)

kn |

ji wa dn ig All dad ll ey i 1

a semenananinans, mann Sil |

 
