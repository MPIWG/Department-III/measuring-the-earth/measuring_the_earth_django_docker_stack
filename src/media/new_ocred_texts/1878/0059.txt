HR

 

Int

mm mir

FETTE VOT RAT WOT TPT TT

51
sans changer d’une manière sensible son ellipticité. Un nivellement devant s’étendre
sur l’immense espace compris entre les deux océans à été récemment entrepris. Enfin
les observations de pendule ont été continuées par M. Peirce, qui est occupé actuelle-
ment à les réduire et à les publier. *)

M. de Bauernfeind remercie M. Hilgard de ses intéressantes communications.

M. Ibanez reprend la présidence, et propose de renvoyer l'étude des questions
théoriques traitées par M. Adan dans les trois mémoires qu’il a distribués, à une Com-
mission spéciale formée de MM. d’Oppolzer, Peters et Villarceau. Cette proposition est
adoptée à l’unanimité.

M. Hirsch donne, au nom de M. le général Baevyer, lecture d’un mémoire con-
cernant l’organisation d’un observatoire spécial pour l’étude dela réfraction terrestre.

Rapport a la Commission permanente sur la création dun observa-
toire pour l'étude de la réfraction terrestre.

x

é Dans la seconde séance de la dernière conférence à Stuttgart j'ai exprimé
l'espoir qu’il serait peut-être possible d'exécuter à l’observatoire de Mannheim des obser-
vations de réfraction analogues à celles que j'ai faites moi-même déjà en 1849 sur
le Brocken. **)

Cet espoir était fondé sur l’autorisation que Son Excellence M. de Stoesser,
ministre de l’intérieur du Grand-Duché de Baden que j'avais entretenu de ce sujet, .
m'avait. donnée, de me mettre en rapport dans ce but avec le directeur de l’observatoire
de Manheim, M. le Dr. Valentiner. En même temps M. le ministre ajouta qu'avant
l'expiration de la période budgétaire actuelle il ne fallait pas compter sur des crédits
destinés à l'exécution du projet.

En conséquence, je priai M. le Dr. Valentiner, qui s'intéresse vivement pour ce
genre d'observations, d'examiner si et avec quels frais il serait possible d’ériger sur 1a
plate-forme de l’observatoire de Mannheim un pilier et une coupole. Si les moyens de
l'institut suffisaient à la construction de la coupole, je pourrais mettre à sa disposition
un instrument universel de Pistor et Martins, de sorte qu'il pourrait commencer au

\

printemps 1878 à faire des essais et a former un observateur.

*) Voir au rapport général, Etats-Unis d'Amérique.
**) Voir sur la planche quelques-unes des nombreuses courbes observées sur le Brocken.

7*

 
