MRC

2 PMY YP TMS NE |

PART EEE RNIT Re

37
Endlich, meine Herren, fügen Sie zu den Ehren, die dieser Saal erfahren
hat, Ihre Versammlung hinzu.

Was mir an allen diesen Zusammenkünften, die hier nach einander
stattgefunden, auffällt, ist der friedliche und internationale Charakter, den
sie alle aufweisen.

Ehemals stiessen die Gelehrten, und besonders diejenigen, welche wie die
Geodäten genöthigt sind, ihre Arbeiten mittelst gemeinsamer Anstrengungen
zu Stande zu bringen, in ihren Untersuchungen auf tausend Schwierigkeiten,
die in den Entfernungen von einander und den ungenügenden Communi-
cationsmitteln ihren Grund hatten. Heutzutage sind die Entfernungen gering
geworden und die Gelehrten erfreuen sich überall der wohlwollendsten Aufnahme.

Sie arbeiten, meine Herren, in Ihrem Gebiet und mit Ihren Mitteln
ebenfalls an einem Werke des Fortschritts, des Friedens und des guten Ein-
vernehmens zwischen den Nationen.

Ich bewillkommne Sie daher in unserem Lande; ich hoffe, dass Ihre
gegenwärtige Zusammenkunft der Wissenschaft und Ihrem grossen Unter-
nehmen von Nutzen sein wird; endlich wünsche ich, dass Sie von Ihrem Auf-
enthalte unter uns nur angenehme Erinnerungen mitnehmen mögen!

Herr General Ibanez, Präsident der permanenten Commission, erwiderte:

Mir wird die Ehre zu Theil, auf Ihre soeben im Namen der Republik
und des Cantons Genf an uns gerichteten wohlwollenden Bewillkommnungs-
worte zu antworten.

Die internationale geodätische Vereinigung, deren Vertreter wir sind,
hat mit grossem Vergnügen die Einladung empfangen, im schönen Genf
zusammenzutreten, und die Delegirten der an dem grossen Unternehmen
betheiligten Regierungen vereinigen sich besonders gern in so berühmten
Mittelpunkten der Wissenschaft, wie dieser es ist, um die angenehmen Bande,
die sie an die Gelehrten anderer Länder fesseln, enger zu knüpfen. Die
Gelehrten der Schweiz haben der Gradmessung seit ihrer Gründung durch
unsern verehrten General Baeyer bedeutende Dienste geleistet, und obwohl
die Gelegenheit nicht günstig sein mag, kann ich doch nicht umhin, die
Initiative zur Verallgemeinerung der Beobachtungen der Intensität der Schwere
mittelst des Reversionspendels, welche die Schweizer Delegirten ergriffen
haben, sowie ihre Bemühungen hervorzuheben, die Gradmessungs-Arbeiten
auf die Präcisionsnivellements auszudehnen, die jetzt einen sehr bedeutenden
Theil der Oberfläche Europas umfassen, und die bald alle Meere, welche
unseren Erdtheil bespülen, verbinden werden. Die gelehrten. Directoren der
Observatorien zu Genf und Neuchätel sind hier anwesend; aber auch wenn

 
