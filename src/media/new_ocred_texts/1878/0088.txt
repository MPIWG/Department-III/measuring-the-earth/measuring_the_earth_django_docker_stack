 

|
|
it
|

L
.
|

80

 

mer en différents points de la côte, dont le rapport présenté par nous à Stuttgart a
fait voir les relations. En effet, les travaux de la Hollande, de l’Institut géodésique, de
l'Etat-Major prussien et de la Belgique étant reliés, il n’est pas douteux que les con-
séquences soient intéressantes à plus d’un titre.

Les opérations du nivellement de base et de la compensation des altitudes
principales vont être incessamment publiées, le manuscrit est complètement préparé,
mais des raisons administratives n’ont pas permis de le livrer à l’imprimeur. Les cotes
des points principaux et des points intermédiaires paraîtront en entiers séparées par
provinces, afin d’être plus facilement accessibles aux administrations, aux ingénieurs et
à toutes les personnes qui auront à se servir du pivellement officiel.

Publications.

Les résultats des observations astronomiques faites depuis vingt-deux ans, sont
consignés dans le livre III de la triangulation du royaume. Un supplément à ce livre a
paru indispensable, il indique les méthodes d'observation et de calcul mises en pra-
tique par les astronomes du Dépôt de la Guerre. Cet ouvrage à été envoyé en mai aux
divers établissements scientifiques de l’Europe.

La recherche des moyens les plus faciles et les plus exacts d'arriver à la con-
naissance de la figure de la terre a occupé la section géodésique de notre Institut. Il
est résulté de ces études trois mémoires présentés successivement en mars, avril et mai
à la classe des sciences de l’Académie Royale de Bruxelles, qui a bien voulu les exa-
miner et les publier. Vous avez tous, Messieurs, reçu un exemplaire de ces mémoires,
dont je serais heureux de pouvoir développer devant vous les tendances, si ma propo-
sition était agréée par la Commission permanente.

Les résultats de ces recherches scientifiques peuvent se résumer comme suit:
1. Quelles que soient les attractions locales, les coordonnées géodésiques et les coor-
données astronomiques sont liées par certaines relations; 2. Les différences entre ces
coordonnées peuvent servir à améliorer les éléments de Vellipsoide osculateur; 3. La
détermination des éléments de l’ellipsoide osculateur ne doit plus dépendre exclusivement
des chaînes de triangles dirigées dans le sens des méridiens et des parallèles, toute
chaîne entre deux sommets astronomiques peut concourir à cette recherche; 4. En
chaque point du globe les coordonnées astronomiques, affranchies des déviations locales,
doivent s’écarter des coordonnées géodésiques à cause d’une déviation ellipsoidale,
provenant de ce que la normale à la surface terrestre, supposée unie, n’est pas normale
à l’ellipsoïde d’assimilation, quelque bien connues qu’en puissent être les dimensions.

L'importance de ces questions n’échappera à aucun des délégués à l’association
internationale, il serait conséquemment très-désirable que vous voulussiez bien les faire
examiner attentivement par des commissaires nommés & cet effet.

1 Août 1878.
E. Adan.

 

n/a ua

 

|
|

 
