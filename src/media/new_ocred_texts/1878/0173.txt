SUR LA CONSTRUCTION DE LA RÈGLE GÉODÉSIQUE INTERNATIONALE. 43 |
construire; mais, si notre projet paraît réalisable, nous serions très- : | |
heureux que d’autres que nous en fissent la tentative.

Le fléau en acier serait soustrait à l’action du magnétisme terrestre
par son enveloppe en tôle de fer; de cette façon, l'emploi d’une ma-
tiere aussi légère, aussi élastique et aussi rigide que l’acier, ne pré-
senterait plus aucun inconvénient,

Re

Fig. 12.

i ii \ | |

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Une pareille balance débarrasse l’opérateur de toutes les erreurs |
qu'il peut commettre, en observant le thermomètre, le baromètre et |
l’hygromètre, et de tous les calculs nécessaires pour corriger les résul-
tats donnés par les instruments.

Mais il ya un problème dont nous n’avons pas abordé l'étude : c’est
celui qui concerne la déformation du tube témoin et de la règle géodé-

L sique elle-même. !
Un cylindre parfait d’une section circulaire est difficile à niveler, ou
au moins son nivellement est une opération très-délicate. Cependant

IT RT

PT RT BETTE TT RTT

 
