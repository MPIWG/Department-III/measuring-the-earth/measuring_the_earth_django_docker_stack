 

|

 

16 H. SAINTE-CLAIRE DEVILLE ET E. MASCART.

de la pression atmosphérique, il faut nécessairement que ses plateaux
aient le même poids et le même volume, ce qu'il est facile de réaliser.
Mais, ce qui est plus difficile à obtenir, ce sont des fléaux dont la forme
soit exactement symétrique par rapport au centre de gravité. Ce dé-
faut se manifeste surtout dans les fléaux évidés, ceux, par exemple,
qui portent des trous circulaires. Il suffit que les centres de ces trous
ne soient pas, de part et d'autre, à une même distance du couteau cen-
tral, pour que la balance devienne sensible, à la même température,
aux changements de pression accusés par le baromètre.

La seule manière de corriger cette cause d'erreur consiste dans la
multiplicité des pesées. Il faut, de plus, que les poids marqués ou les
tares, présentant rigoureusement le même volume extérieur, soient,
autant que possible, de même nature (").

En répétant les pesées soir ct matin, observant et inscrivant avec les
poids marqués la hauteur du baromètre, la température et le point de
rosée, on obtient une série de nombres que l’on traduit par une courbe

. Ou par une formule empirique.

On prend pour une des variables le poids de la matière, pour l’autre
variable la pression atmosphérique, diminuée des de la force élastique
de la vapeur d’eau hygrométrique et du produit de la variation de
température par 2%%, 78. Ce dernier produit doit être ajouté à la pres-
sion ou en être retranché, suivant que la température est inférieure
ou supérieure à la température de comparaison. La pression ainsi cal-
culée est proportionnelle à la densité de l'air. On obtient ainsi la loi
des variations de la balance avec l’état atmosphérique et la modification
qu’on doit faire subir aux poids pour les ramener à une même densité
de l’air dans deux séries de pesées de deux matières diffférentes.

Ainsi, quand nous avons pesé le tube de platine iridié vide d'air,
ce qui à duré plusieurs mois, nous avons étudié la loi de la variation
du poids P de ce tube avec la densité de l’air exprimée par la formule

 

(') Deux autres causes tres-fréquentes de l’inconstance dans une balance viennent: 1° de
ce que les poids, placés sur des parties différentes d’un méme plateau, ne produisent pas le
même effet sur le fléau. On corrige ce défaut en plaçant sur les plateaux et à la partie infé-
rieure de ceux-ci deux pointes. On change la place des poids sur le plateau jusqu’à ce que
ces deux pointes soient toujours l’une au-dessus de l’autre.

Souvent aussi la balance, mise au repos, puis en mouvement, ne donne pas deux fois de suite
la même déviation de l’aiguille. Nous ne connaissons pas de remède à ce défaut fréquent.

Mb Mh Ak TUT mW En

ME

mr

=

 

g-j-specsisagidinaee, apeissam Sil

 

 
