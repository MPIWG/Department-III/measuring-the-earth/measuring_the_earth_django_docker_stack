[YN wen Ver on oe

APTE I

1 0 77

NIET

Programme pour la réunion de la Commission permanente
a Hambourg en 1878.

1. Rapports annuels de la Commission permanente et du Bureau central.

2. Rapports de MM. Iles délégués sur l'avancement des travaux dans leurs
pays, en 1878.

3. Propositions concernant quelques questions scientifiques.

Local des séances.

Alsterthor No. 22, à l'hôtel de M. le sénateur Hayn.

Ont pris part à la réunion:

I. Les Représentants du Sénat de la Ville Hanséatique de Hambourg:

Sa Magnificence Monsieur le Dr. Kirchenpauer, Bourgmestre régnant.
Monsieur le Sénateur Hayn.

II. Les Membres de la Commission permanente:

Son Excellence Monsieur le Général Ibanez de Madrid, president.
M. le Docteur von Bauernfeind de Munich, vice-président.
M. le Docteur Bruhns de Leipzig \ Re
M. le Docteur Hirsch de Neuchâtel / -
Son Excellence Monsieur le Général Dr. Baeyer de Berlin, président du Bureau
central.
Son Excellence Monsieur le Général de Forsch de Saint-Pétersbourg. :
Monsieur le General Mayo de Florence.
Monsieur le Docteur von Oppolzer de Vienne.

 
