 

vw

ET LT ad

SUR LA CONSTRUCTION DE LA RÈGLE GÉODÉSIQUE INTERNATIONALE. 13
de trois pierres calcaires supportées par un massif de béton de plus
de 2 mètres d’ épaisseur.

La pierre (') qui sert de base a une longueur de 1",65, une largeur
de 0", 60 et une épaisseur de 0", 75.

Rio, >.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Deux autres pierres, celles qui supportent les microscopes, ont pour
longueur 1™, 10, pour largeur o™, 50 et une épaisseur de o™, 45.
© D
Les microscopes portent d’excellents micrometres que MM. Brunner
P

(") MM. Brünner ont eu l’idée de remplacer les pierres par des blocs de fonte. Nous
croyons l’idée excellente, surtout si l’on coule ces blocs creux et qu'on les op con-
stamment de glace en ies entourant de feutre. Les grosses pierres ne peuvent qu’au bout
d’un temps aiden se mettre jusqu'au centre à la température ambiante, et ne doi-
vent jamais être en repos. D'ailleurs, en employant les phénomènes de chaleur latente pour
porter à une température invariable et les instruments el les matières à mesurer, On peut
entretenir dans les salles d'observation une fraîcheur agréable pendant l'été, et pendant
l'hiver une température douce, sans s’astreindre, comme on l’a proposé et appliqué sans
succès, à maintenir une ls salle a une température constante, hiver et été, au grand
détriment de la santé des observateurs,

3

 
