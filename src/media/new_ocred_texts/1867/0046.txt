 

|
I
3
L

|
#
|

if
|
|

42

Projet et Resolutions de la Commission Italienne.
La Commission se conformant aux délibérations de la Conférence de 1864 a
jugé devoir avant tout fixer l’ensemble des travaux quelle aurait à faire.
Réseaux géodésiques.
La Commission a réconnu que les réseaux géodésiques devaient s'étendre dans
la direction des trois méridiens et trois paralléles suivants:

Méridiens.

La première ligne est celle qui s'étend de Cagliari par la Sardaigne, la Corse,
les côtes de la Toscane, Gênes, Milan, se réunit aux réseaux suisses, et se prolonge à tra-
vers l'Allemagne.

La seconde a son point de départ à l’île de Ponza et par Rome, Florence,
Padoue se poursuivra en Allemagne dans la direction de Munich, Leipzig, Berlin, Ko-
penhague.

La troisième partant de Capo Passaro & Vextrémité Sudest de la Sicile, par
Messine, Potenza, Foggia, traverse au moyen des îles de Tremiti la mer adriatique et
rejoint en Dalmatie les réseaux Autrichiens et Italiens et se prolonge dans la direction
de Vienne.

Ares de parallèles.

Les trois chaînes à mésurer sur les parallèles sont:

1. Une chaîne qui des frontières de la Savoie se prolongerait dans la direction
de Milan, Padoue et Venise suivant le parajlèle moyen. Ce travail déjà fait en partie
en 1821—22—23 par une Commission Piémontaise et Autrichienne composée d'officiers
et d’astronomes des deux pays et dont faisaient part les célèbres astronomes Plana et
Carlini, a besoin d’être completé dans la direction de Padoue afin de pouvoir atteindre
la limite d’approximation fixée par la Conférence.

9. Une chaîne sur le parallèle qui part de la Corse (bien entendu d’accord
avec la France) et se prolonge dans la direction de Monte Gargano pour atteindre la

Dalmatie.
3. Une chaîne qui s’étende de Vile de Ponza a Brindisi.

Conditions aux quelles doivent satisfaire les differentes chaines.

La Commission sans vouloir de suite fixer en détail les points que les difie-
rentes lignes géodésiques susénoncées devaient relier a posé les conditions suivantes:

a) que chaque chaîne soit double, c’est-à-dire formée par des polygones contigus,
à tours d'horizons centraux, pour qu'on y puisse appliquer convenablement les méthodes
de compensation,

b) que le long de ces chaînes à la distance de 20 à 25 triangles on mésure
une base pour s’y relier, et d'établir ces bases préférablement aux points d’intersection
des méridiens et des parallèles.

On à indiqué comme points convenables ceux de Trapani, Catania, Tarente,

Foggia, Rome, Rimini, Livourne, Somma, Turin, Cagliari.

    

Ltd 1 0841 1188 al

I

to nl

Hm bn

js he nn adil id tl

seat ann néant sid |

 
