He

 

URL

Ta m

4145

Herr Baur: „Die bei dieser Operation verwendeten Latten sollen nicht nur
auf ihre Theilungsfehler untersucht, sondern es sollen auch entweder ihre
absoluten Correctionen oder wenigstens ihre Gleichungen genau ermittelt
werden. Die Vertical-Stellung der Nivellirlatten und die Unveränderlichkeit
ihres Standes während der Drehung sind durch besondere Vorrichtungen zu
garantiren.‘

Herr Herr: Was ist eigentlich darunter verstanden: ,,die absolute Correction
oder ihre Gleichung,“ nachdem schon früher von der Untersuchung der Theilung die
Rede gewesen ist?

Herr Hirsch: Die absolute Correction der Latten wird ermittelt werden, wenn
man, wie die Schweizer Commissare es in Bern gethan haben, dieselben auf einem Com-
parator mit einem Meter oder sonst geprüften Maassstabe vergleicht, dann bekommt man
die absolute Länge jeder Latte. Wo dies nicht möglich ist, wäre es wünschenswerth, die rela-
tiven Längen oder Gleichungen mehrerer Latten, die gebraucht werden, festzustellen. Dann
wird wenigstens erzielt, dass jedes Land die Resultate seines Nivellements in derselben
Einheit auszudrücken vermag. Demnächst wäre eine solche Nivellir-Latte mit derjenigen
angrenzender Länder zu vergleichen, und die Länder, welche im Stande sind, die absolute
Länge der Latten etwa in Centimetern oder sonst in einem Maasse auszudrücken,
würden dann den Anhalt geben. Das ist auch die Ansicht der Commission.

Herr Herr: Es wäre dann also noch nöthig, dass, wenn innerhalb eines be-
stimmten Landes die Gleichungen der Latten in diesem Sinne, d. h. wenn die relativen
Längen ermittelt sind, noch in irgend einer Weise für die Gleichungen zum Behufe
der Anschlüsse an die benachbarten Länder gesorgt werden muss?

Herr Hirsch: Ja.

Präsident Baeyer: Ich ersuche also die Herren, welche mit der obigen Fas-
sung des Antrags einverstanden sind, sich zu erheben.

Es geschieht; der Antrag 2. ist angenommen.

Herr Baur: Der dritte Antrag lautet:

„Die Controle bei dieser Operation soll durch polygonalen Abschluss der
Stationen, wobei die Polygone nicht zu gross anzunehmen sind, und wo-
möglich auch durch mehrfache Nivellirung derselben Linien erzielt werden.“

Ich füge bei, dass der Commission beide Mittel als geeignet erschienen und
dass es nur nachträglich als wünschenswerth bezeichnet wurde, dass beide Mittel ange-
wendet würden neben einander.

Herr Bauernfeind: Nach der Fassung des Berichts des Herrn Baur scheint
es, dass der polygonale Anschluss wirklich eine Controle sei. Dieser Ansicht bin ich
nicht. Ich bin vielmehr der Ueberzeugung, dass ein Nivellement in einem Polygon gar
keine Garantie gewährt dafür, dass die Zwischenpunkte auch richtig angegeben sind.
Eine Garantie liegt einzig und allein in dem doppelten Nivellement. Ich beantrage
also, dass „die Controle durch polygonalen Abschluss“ gestrichen und gesagt wird:
die Controle kann blos durch doppeltes Nivellement hergestellt werden.

Herr Hirsch: Ich gestehe Herrn Bauernfeind zu, dass der polygonale Abschluss

19

 

 
