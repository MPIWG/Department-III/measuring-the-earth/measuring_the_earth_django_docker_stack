PE me

PERRET

mr

ot

vor der Eröffnung des Central-Bureaus und in die Arbeiten, welche seit der Eröffnung
desselben ausgeführt wurden.

A. Arbeiten, welche vor der Eröffnung des Oentral-Bureaus, vom

l. November 1864 bis zum 1. April 1866 ausgeführt wurden.

Die ganze wissenschattliche und geschäftliche Correspondenz, die Abfassung der
Generalberichte, der Druck derselben u. s. w. musste in diesem Zeitraum von mir selbst
bestritten werden. a .

Meine officiellen Gehülfen für die Triangulation waren der Hauptmann Habel-
mann wad der Hauptmann Stavenhagen. Ausserdem hatte ich privatim den Professor
Dr. Sadebeck ın Breslau engagirt, mich während seiner Ferienzeit von Mitte Juli bis
Mitte August, also auf vier Wochen jährlich, bei astronomischen Bestimmungen zu
unterstützen und die Beobachtungen im Winter in seinen Mussestunden in Gemeinschaft —
mit dem Assistenten der Breslauer Sternwarte Herın Dr. Günther zu berechnen.

Im Winter von 1864/65 berechneten Professor Sadebeck und Dr. Günther die
von dem Ersteren und mir im Sommer 1864 auf der Station Hornburg nahe unter dem
52ten Parallel ausgeführten Winkelmessungen und Bestimmungen der Polhöhe und des
Azimuthes.

Die beiden zur Gradmessung commandirten Officiere Hauptmann Habelmann
und Hauptmann Sfavenhagen waren mit Ausgleichung der Rheinischen Dreiecke und
mit Ausgleichungen der früher in Schlesien beobachteten Winkel beschäftigt.

Am 1. Mai 1865 war das Commando des Hauptmann ZYabelmann abgelaufen ;
er kehrte zu seinem Truppentheil zurück und der Hauptmann Stavenhagen verblieb allein
bei der Gradmessung. Im Sommer 1865 führte er die Errichtung der Pfeiler und die
Einrichtung der Stationen in Thüringen bis zum Inselsberge aus. Bei Breslau wurde
die Station Rosenthal der europäischen Längengrad-Messung unter dem 52ten Parallel
mit der Sternwarte und mit dem Hauptdreiecksnetz durch eine kleine Triangulation in
Verbindung gebracht. Ferner beobachteten Professor Sadebeck und ich wieder im Juli
und August während dessen Ferienzeit auf dem Brocken Horizontalwinkel und Polhöhe
und Azimuth. Diese Beobachtungen wurden ım darauf folgenden Winter vom Professor
Sadebeck und Dr. Günther in Breslau berechnet. Hauptmann Stavenhagen unterstützte
mich bei verschiedenen kritischen Untersuchungen und wissenschaftlichen Arbeiten.

B. Arbeiten, welche seit der Eröffnung des Central-Bureaus vom
1. April 1866 bis jetzt ausgeführt wurden.

Obgleich der Krieg die Feldarbeiten in Preussen verhinderte, so konnte doch
die astronomische Bestimmung der Oldenburgischen Station Dangast - (Generalbericht
pro 1866) auf den Wunsch des Grossherzoglichen Kommissarius Ober-Kammerrath
Herrn ©. Schrenck und auf Kosten der Oldenbureischen Regierung durch den ersten
Observator an der Berliner Sternwarte Herrn Dr. Tietjen mit einem dreizehnzölligen
Universal-Instrumente in den Monaten April und Mai ausgeführt werden.

Ferner wurde zur Prüfung dieses vortrefflichen, von den Herren Pistor und
Martins im Jahre 1852 gebauten Instrumentes die Polhöhe der Berliner Sternwarte durch

9
.)

 
