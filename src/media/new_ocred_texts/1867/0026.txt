 

 

 

 

Herr Hansen: Die Thätigkeit des Präsidenten des Centralbureaus ist eine
grosse, mühsame und zeitraubende. Herr General Baeyer hat unter den gegebenen
Umständen Alles geleistet, was geleistet werden konnte und ich fordere die Versamm-
lung auf, durch Aufstehen von ihren Sitzen ihm ihren Dank auszudrücken.

Die Versammlung erhebt sich.

Präsident Baeyer: Meime Herren, ich werde, was in meinen Kräften steht,
thun, um ferner den Anforderungen, die Sie an mich stellen, zu entsprechen.

Nach §. 3 der Geschäftsordnung ersuche ich nun die Herren Mitglieder der
Conferenz, über den Stand der Arbeiten in den von ihnen vertretenen Staaten Mitthei-
lung zu machen. Es scheint mir am besten die alphabetische Reihenfolge der Staaten
eintreten zu lassen, und ich bitte den Herrn Schriftführer, die Bevollmächtigten der
Staaten in dieser Weise zu nennen.

Herr Bruhns: Nach alphabetischer Ordnung würde zuerst, da der Commissar
für Baden durch Krankheit verhindert ist, zu erscheinen, Herr Dauernfeind für Bayern
seine Mittheilungen zu machen haben.

Herr Bauernfeind hält sofort folgenden Vortrag:

Ueber die behufs der Landesvermessung ausgeführte Triangulation von Bayern
und deren Beziehung zur Mitteleuropäischen Gradmessung.

In dem Generalberichte über die Mitteleuropäische Gradmessung für das Jahr

1863 ist die Bemerkung enthalten, dass nach einer Mittheilung der Königlichen Steuer-

kataster-Commission zu München die Bayerische Triangulation unter günstigen Ergeb-

nissen nicht blos mit der Oesterreichischen in Tyrol und Vorarlberg, sondern auch mit

jener der Schweiz und von Württemberg, Hessen und Hannover verbunden, dadurch

‚definitiv beendiet und jede weitere trigonometrische Operation ausgeschlossen sei. Mit

Ausnahme einiger geographischen Ortsbestimmungen, welche noch gemacht und veröf-
fentlicht werden sollten, würde somit von Seite Bayerns lediglich dessen Hauptdreiecks-
netz an die Mitteleuropäische Gradmessungs-Commission mitzutheilen gewesen sein.
Die der erwähnten Mittheilung zu Grunde liegende, von den Vorständen der
Königlichen Steuerkataster-Commission und der Königlichen Sternwarte zu München
festgestellte Ansicht, dass der geodätische Theil der Bayerischen Triangulation abge-
schlossen und nur der astronomische in einigen Punkten noch zu ergänzen sei, musste
selbstverständlich für das Vorgehen der Königlichen Staatsregierung so lange von Be-
deutung sein, als ihr keine andere von gleichem oder grösserem Gewichte gegenüber-
stand. Und in der That entsprechen auch dieser Ansicht die bisherigen geringen Lei-
stungen Bayerns für das grosse wissenschaftliche Unternehmen, welches uns hier zu-
sammengeführt hat. Denn seit Beginn dieses Unternehmens wurden in unserem Lande
blos mehrere Azimuthal- und Breitenbestimmungen ausgeführt und die Herausgabe eines
Werkes über die Bayerische Landesvermessung in Angriff genommen. Jene astrono-

“mischen Bestimmungen sind theilweise veröffentlicht worden"), der Druck des fraglichen

@
*) In den Sitzungsberichten der Königlich Bayerischen Akademie der Wissenschaften zu München,

Jahrgang 1860, Bd. L, S. 21—66.

nu ld Lm HP

Lay

ju tu M |

pate en a A A i lh tua 4m

sœsaséiemts nass au |

 
  

N
