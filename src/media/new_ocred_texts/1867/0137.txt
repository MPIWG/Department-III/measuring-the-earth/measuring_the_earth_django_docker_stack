 

UL RT

133

Herr Hirsch: Nach dem in allen berathenden Versammlungen gültigen Princip
muss man dem Antrage des Herrn Seidel auf Theilung des Antrages willfahren.

Herr Seidel: Ich méchte nur noch kurz Herrn Bruhns bemerken, dass das
Centralbureau das Organ der Conferenz ist, die Conferenz hat aber nicht die Aufgabe,
ein neues Meter aufzustellen, sondern nur Wünsche auszusprechen.

Herr Bruhns: Ja, wir können nur Wünsche aussprechen, aber ich glaube, wir
können deshalb auch diesen Wunsch aussprechen.

Herr Zlörsch: Herr Förster hat diesen Zusatz gewünscht, er zieht ihn zurück,
nachdem er sich überzeugt hat, dass schon der erste Beschluss, den Sie heute gefasst
haben, seine Absicht erfüllt hat, und ich votire in demselben Sinne, so dass der An-
trag nun lautet:

„Die Herstellung des neuen Normalmeters, sowie die Anfertigung und Ver-
gleichung der für die verschiedenen Länder bestimmten Copien würde am
besten von einer internationalen Commission besorgt werden, in welcher die
betheiligten Staaten vertreten wären.“

Präsident Baeyer: Ich bringe den Antrag in der eben verlesenen Form zur
Abstimmung.

Punkt 3 wird in der Form angenommen, desgleichen Punkt 9 ohne Debatte.

Es folgt die Verlesung des Punkts 10.

Herr v. Forsch: Ich muss erklären, dass ich mein Mandat so verstehe, dass
ich nur über geodätische Fragen meine Meinung abzugeben habe. Wenn es sich darum
handelt, ein Maass zu blos geodätischen Zwecken einzuführen, so kann ich diesen An-
trag annehmen, wenn es sich aber um die Einführung irgend eines allgemeinen Maasses
oder um die Veränderung von Maassen im Verkehr handelt, so erkläre ich, dass ich
dagegen stimmen muss und auch an meine Regierung über diesen Punkt nicht berichten
kann, da ich mein Mandat überschreiten würde.

Herr Bruhns: Es ist hier überhaupt nur von Einführung des Meter zu geoda-
tischen Zwecken die Rede; was die Regierungen für ein Maass für den allgemeinen Ver-
kehr einführen wollen, darüber haben wir nicht abzustimmen. Wir sprechen nur den
Wunsch aus, dass das Metermaass allgemein für die Europäische Gradmessung einge-
führt werde.

Herr Hirsch: Ich würde die Fassung dahin ändern:

„Die Conferenz stellt es den Bevollmächtigten anheim, obige Beschlüsse
ihren hohen Regierungen zur Kenntniss zu bringen, und die permanente
Commission wird beauftragt, deren Ausführung möglichst zu fördern.“
Dann liegt nichts mehr in der Fassung,
daran Anstand nehmen, verhindern könnte, dafür zu stimmen.

was die Herren Bevollmächtigten, die jetzt

Herr Donati wünscht, dass die permanente Commission die Beschlüsse der

Conferenz den Regierungen auch direkt mittheile.
Herr Hansen: Die Art, wie die Regierungen mit diesem Beschlusse bekannt
gemacht werden, wird einfach darin bestehen, dass man in der Vorlage, mit welcher

 

 
