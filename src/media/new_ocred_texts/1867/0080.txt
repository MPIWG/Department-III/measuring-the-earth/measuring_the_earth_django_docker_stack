 

16

dass der ganze Preussische Staat, der ungefähr 4000 Messtischblätter umfassen würde,
in ungefähr 33 Jahren topographisch kartirt sein wird. Die Art und Weise der Kar-
tirung sehen Sie in den vier Blättern, welche ich die Ehre habe, hier herum zu reichen.
Es sind dies Aufnahmen, wie sie in den Jahren 1853 bis 1856 in den Thüring’schen
Ländern Seitens des Generalstabs ausgeführt sind. Die Aequidistanz der Niveaukurven
beträgt 25 Fuss und bei geringerer Neigung 12%, Fuss, resp. sind auch noch Zwischen-
kurven von 5 Fuss vorhanden. Es ist hierbei die Wahrnehmung höchst interessant
gewesen, dass die neuesten Aufnahmen von der Grafschaft Glatz, wo sehr steile Höhen
vorkommen, es noch erlaubt haben, sogar fünffüssige Aequiniveaukurven anzuwenden.

Es fehlt den hier vorgelegten Blättern zwar noch die Eleganz, welche wir an
den Blättern bewundert haben, die der Herr General Simons die Güte hatte, uns vor-
zulegen, wir werden uns aber bestreben, dass sie auch erreicht werde.

Der zweite Gesichtspunkt strebt dahin, durch Reduction dieser Messtischblätter.
auf den vierten Theil, auf den Maassstab von 1 : 100,000, unsere Karten des Preussi-
schen Staates auf eine Kart& von Norddeutschland, die demnach in 525 Blättern in
Kupferstich auszuführen wäre, auszudehnen. Die Art und Weise dieser Ausführung
wird durch die schon publicirten 23 Sectionen, welche sich über Ostpreussen erstrecken,
vertreten. Die ganzen Arbeiten erfordern aber viel Zeit; der Plan ist dahin gerichtet,
dass die Aufnahme über den Raum von 6200 Quadratmeilen in 33 Jahren, also bis
zum Abschluss dieses Jahrhunderts vollendet sein soll. Um diese bedeutende Aufgabe
in der eben erwähnten Zeit und in dem Maassstabe von 1:25,000 lösen zu können,
ist die Arbeit in mehrere Gruppen eingetheilt und wie diese Gruppen durchgenommen
werden, wird dargestellt durch eine Karte, welche ich mir erlaube hier eireuliren zu lassen.

Es ist aus vielfach erwogenen Gründen, die theilweise militairischer Natur und
deshalb unabänderlich sind, der Gang der Arbeiten so festgestellt, dass die Aufnahme
gleichzeitig, wenigstens bis zum Jahre 1890, sowohl östlich als auch westlich vom
Meridian von Berlin erfolgen würde. Während z. B. in den Jahren 1872 bis 1877
einestheils Westpreussen und dernördliche Theil von Posen, (d.h. derjenige, welcher nördlich
des 53sten Breitengrades liegt), betreten würde, so würde andererseits in derselben
Zeit auch die südliche Hannoversche Gruppe zwischen dem Parallel von Hannover und
dem von Kassel und zwischen dem 26. und 29. Längengrade von Ferro aufgenommen
werden. 1877 würde der südlich von 51Y, Grad Breite liegende Theil von Schlesien und
gleichzeitig die Lüneburgische Gruppe zwischen dem Parallel von Hannover und dem
von Hamburg betreten werden.

Wir gehen mit unseren Arbeiten also aus verschiedenen Gründen gleichzeitig
im Westen und im Osten des Staates vor und der Generalstab hat begründete Aus-
sicht, seine Pläne nicht als blosses Project zu betrachten, sondern sie auch wirklich in
Ausführung gesetzt zu schen. Er hat jetzt schon so bedeutende Mittel zur Verfügung,
dass wir im letzten Jahre anstatt, wie früher mit 30, mit 60 Topographen
arbeiteten, und zwar arbeitet nicht nur eine solche Abtheilung in Ostpreussen, sondern
eine andere gleichzeitig unter sehr günstigen Umständen auch im Westen. Herr Ver-

    

a a He

ak mil

ko il

Ku Ic nah Li a dt a 4e

2.4 semeaninandiinwns, mass sid |

 
