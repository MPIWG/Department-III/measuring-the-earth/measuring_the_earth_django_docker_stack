RTT eT TT |

0 PTT

DOME UTTNT

TFT

Erste Sitzung

zweiten Conferenz der Mitteleuropaischen Gradmessung.

Zu der zweiten allgemeinen Conferenz der Mitteleuropäischen Gradmessung

waren erschienen:

10.

11:

1, Bevollmächtigte,

Bayern.

. Herr Dr. Bauernfeind, Baurath, Akademiker und Professor in München.

29

99

99

Dr. Ludwig Seidel, Akademiker und Professor in München.

Belgien.

Simons, General in Brüssel.

Coburg-Gotha.
Dr. Hansen, Geheimer Regierungs-Rath und Director der Sternwarte in Cane.

Hessen-Darmstadt.
Dr. Huegel, Ober-Steuer-Director in Darmstadt.
Italien.

Ricei, Excellenz, Generallieutenant und Chef des Ceneralstabes in Florenz.
de Vecchi, Oberst im Greneralstabe in Florenz.
Dr. Donati, Director der Sternwarte ın Florenz.

Mecklenburg.
Paschen, Geheimer Kanzlei-Rath und Mitglied der Direction der Landesver-
messung in Schwerin.

Niederlande.
Dr. Kaiser, Director der Sternwarte und Professor in Leyden.

Oesterreich.
Oberstlieutenant v. Ganahl fir Herrn v. Fligely, Éxcellenz, Feldmarschäll-
Lieutenant und Director des militair-geographischen Instituts im Wien.
Dr. Herr, -Professor der praktischen Geometrie am Polytechnischen Institut
in Wien.

 
