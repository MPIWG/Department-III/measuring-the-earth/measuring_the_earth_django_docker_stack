 

100

Siebente Sitzung:

zweiten Conferenz der Mitteleuropäischen Gradmessung.

Berlin, Montag, den 7. October 1867.
Anfang der Sitzung 11 Uhr 15 Minuten.
Prasident Herr Baeyer, Schriftführer die Herren Bruhns und Firsch.
Präsident Baeyer: Meine Herren, ich eröffne unsre heutige Sitzung und ersuche
Herrn Bruhns, das Protokoll der letzten Sitzung zu verlesen.
Geschieht.
Herr Wittstein wünscht eine Berichtigung des Protokolles, welche eine von
ihm gegebene Erklärung betrifft. (Siehe pag. 127.)
Herr Bruhns: Darf ich also folgende Fassung jener Erklärung vorschlagen:
„Herr Wittstein erklärt, sich der Theilnahme an der Abstimmung von dem
Punkt 4 an enthalten zu müssen.“
Herr Wittstein stimmt zu.
Präsident Baeyer: Wünscht noch Jemand das Wort?
Herr Dove: Ich wollte mir nur eine Frage erlauben: es ist nach dem Proto-
koll in der Commission beschlossen worden, dass die beiden Comparatoren, einer für
Maassstabe und einer für Messstangen, gesondert werden. Ist das so richtig verstanden’?

Herr Hirsch: Als Referent habe ich zu erwidern, dass in der That der Be-
schluss auf meinen Vorschlag so gefasst: worden ist. Ich habe nur die einfache Bemer-
kung hinzuzufügen, dass es nicht möglich wäre, einen Comparator. für beide Zwecke
dienen zu lassen, da die Messstangen in der Regel eine solche Länge haben, dass es
unmöglich wäre, dazu denselben Comparator zu benutzen, der zur Vergleichung des Meters
oder der Toise dient. Das war der Grund meines Antrages.

Herr Dove: Soviel ich weiss, war es in der Commission nicht ganz so vor-
geschlagen.

Herr Hirsch: Ich habe es so aufgefasst und demgemäss vorgelegt und glaube,
dass wohl kein Zweifel daraus hervorgehen wird.

Präsident Baeyer: Da zum Protokoll weiter nichts bemerkt wird, betrachte ich
es als angenommen.

Unsere heutige Tagesordnung besteht:

1. in einer kurzen Mittheilung über die gestrigen Beschlüsse der permanenten
Commission
und

2. mm der Fortsetzung der Berichte der Commissionen.

ro in ah nn

js mod da u äh man 1

à 1 Alle rémets |

vr LA UNIL EI

 
  

 
