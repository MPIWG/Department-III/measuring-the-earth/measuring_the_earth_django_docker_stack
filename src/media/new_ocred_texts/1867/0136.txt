 

h

DS PEE EE ESN

Oe

SEEN

RES Er

|
I
|
;

132
Anderm von dem Herrn Geheimenrath Zfülsse, der vor drei Jahren selbst Mitglied der
Maass-Commission in Frankfurt war, dass die meisten Mitglieder damals durchaus
nichts von dem Meterfuss wissen wollten, und ich habe aus zuverlässiger Quelle gehört,
dass, wenn von dem Reichstage ein Einheitsmaass angenommen wird, diess nur das
Metermaass sein wird, ohne den 0,3 Meterfuss.

Herr Hirsch: Auch ich unterstütze diesen Antrag, indem ich mich berufe auf
die Erfahrungen in der Schweiz. Leider hat die Schweiz meines Erachtens den Feh-
ler begangen im Jahre 1850, als dort das neue Maass eingeführt wurde, nicht den
Meter selbst, sondern den Meterfuss von 80 Centimeter anzunehmen. Sie findet sich
nun, nach den Erfahrungen von 16—17 Jahren in die Nothwendigkeit versetzt, diesen
Beschluss aufzuheben und den Meter selbst anzunehmen. Es scheint mir ohne Frage,
dass die andern Staaten, Baden u. s. w., die einen gleichen Fehler begangen haben,
darin der Schweiz folgen werden. Es freut mich, dass die Conferenz die Nothwendig-
keit der Einführung des Meter anerkennt.

Präsident Baeyer: Wünscht Jemand das Wort?

Die Proposition 6 wird angenommen, ebenso die Proposition 7.

Nach Verlesung des Antrags 8 spricht:

Herr Seidel: Es ist mir nicht ganz klar, ob der Sinn dieses Antrages der ist,
dass die internationale Commission selbst das Centralbureau sein soll.

Herr Hirsch: Ich erlaube mir dem Herrn Collegen zu erwidern, dass das nicht
der Fall ist, sondern dass nach der Fassung und der Discussion in der Commission
die Feststellung der Principien, wonach das neue Maass construirt werden soll, Sache

der internationalen Commission ad hoc ist, dass aber die Leitung der mechanischen Aus-.

führung, die natürlich nicht von einer Commission von 15 bis 20 Mitgliedern besorgt
werden kann, also die Ausführung der Beschlüsse der internationalen Commission wo
möglich unserem Uentralbureau überwiesen werden möchte.

Herr Seidel: Nach dieser Auskunft wünsche ich mein Votum dahin zu erklären,
dass ich zwar mit dem ersten Theile des Beschlusses übereinstimmen kann, aber nicht
mit dem zweiten. Unser Centralbureau ist nicht für diesen Zweck gewählt, wir können
also nur aussprechen, dass wir eine internationale Commission wünschen, aber nicht
dass wir das Oentralbureau mit der Ausführung beauftragt wünschen. Es sind gewiss
in vielen gelehrten Körperschaften noch andere Personen vorhanden, die durch ihre bis-
herigen Arbeiten vielleicht noch mehr dazu vorbereitet sein möchten, an der Ausführung
gerade solcher Arbeiten Theil zu nehmen, als die Mitglieder des Centralbureaus. Wenn
der Antrag bei der Abstimmung in zwei Fragen getrennt würde, so könnte ich we-
nigstens für den ersten Theil stimmen, käme der Antrag im Ganzen zur Abstinmung,
so müsste ich gegen den ganzen Antrag stimmen.

Herr Bruhns: Es ist in der vorigen Conferenz ausgesprochen, dass das Cen-.

tralbureau die ausführende Behörde der Conferenz sei. Das Centralbureau hat damals
diese Verpflichtung übernommen, und wir dürfen ihm auch wohl diese Aufgabe
zuertheilen und sicher sein, dass sie von dem Centralbureau ausgeführt wird.

  

 

pe tea mM He

ak (nike)

ko bl |

u la ba u ald aid il aly yy si 1

Mai

2: seat Res matt AA |

 
