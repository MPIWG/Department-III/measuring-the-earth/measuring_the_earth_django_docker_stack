 

rer Orr

aa A Ld

rear

nr

43

Partie astronomique,

Pour la partie astronomique la Commission à adopté pour la fixation des points
où doivent avoir lieu les déterminations astronomiques le projet du professeur Schjapa-
relli déjà exposé par lui à la Conférence de 1864, qui consiste à faire les déterminations *

1. aux observatoires existants;

9, à établir un certain nombre d’observatoires temporaires sur des points trigo-
nométriques ou en proximité là où les lignes des parallèles et des méridiens se
rencontrent;

3. dans certains points où l’on a reconnu ou pourront se reconnaître des ano-
malies dans la direction et l'intensité de la pesanteur.

Détermination des latitudes.

La Commission a adopté:

a) Pour la méridienne de Cagliari, de faire des observations de latitude à Ca-
gliari, dans deux autres points de Vile de Sardaigne, en un ou deux points de la Corse
(si l’on obtient le concours du gouvernement Français), un à Pile d'Elbe, à Pise, Gênes»
Tortona, Pavie, Milan et un point à choisir limitrophe à la Suisse.

b) Pour la meridienne de Ponza observations de latitude à Ponza, Rome, Na-
ples, Montefiascone, Perugia, Florence, Rimini, Bologne.

e) Pour la ern de Capo Passaro observations de latitude au Capo Pas-
saro, Catania, Messine, Cosenza, Potenza et Foggia.

d) On a aussi cri devoir adopter Vidée de faire des observations de latitude
le ‘long de la méridienne de Turin vu l’énormité des attractions locales déjà reconnues,
et on a choisi les localités de St. Remo, un point sur le sommet de l’Appenin, Mondovi,
Sanfré, Saluces, Turin, Massé, Andrate.

Détermination des longitudes.

Pour les longitudes la Commission a fixé de procéder aux opérations suivantes :

e) Sur le parcours du parallele moyen, vu que la vallée du Po est le centre
de beaucoup d’altérations locales, on a jugé nécessaire de déterminer par le moyen de
l'électricité quatre principales différences de longitude, savoir: entre Génève et le Mont-
cenis; entre le Montcenis et Turin; entre Turin et Milan; entre Milan et Padoue. On
a encore jugé convenable d'ajouter le plus grand nombre de détermmations entre les
quatre points principaux par le moyen de signaux à feu ou par le moyen d’expeditions
chronométriques.

f) Sur le parcours du parallèle d’Ajaccio — Monte Gargano, trois difterences
de longitudes entre Ajaccio et l’Elbe; entre Elbe et Rome; entre Rome et le Monte
Gargano.

g#) Sur le parallele de Ponza & Brindisi on a proposé de déterminer les diffe-
rences de longitudes entre Ponza et Naples; entre Naples et Potenza; entre Potenza et
Brindisi. —

Cependant on s’est reservé de proceder à d’autres déterminations si Von s’aper-
cevait des attractions locales, comme on en a déjà reconnu dans l'Italie septentrionale.
On a encore reconnu qu'il était important de faire une serie de différences de
6:

 
