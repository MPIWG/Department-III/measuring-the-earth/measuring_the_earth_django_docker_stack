| Inn wen mer de =e

 

UT RT

a

Herr Kaiser: Ich danke für das Vertrauen, dessen mich die Conferenz würdigt,
und ich spreche die Versicherung aus, das ich mich des Vertrauens würdig erweisen
werde, ich nehme die Wahl an, natürlich unter der Bedingung der Zustimmung meiner
Regierung, woran ich durchaus nicht zweifle.

Herr Ricei nimmt ebenfalls dankend die Wahl an.

Herr Lindhagen: Ich habe mir das Wort erbeten, um eine kleine Erklärung
abzugeben, weshalb ich vorhin gegen die Benennung „Europäische Gradmessung“ gestimmt
habe. Mein Grund ist ein anderer gewesen als der, den Herr Wittstein vorgebracht
hat. Wenn man nämlich ganz bestimmt sagt: „die Europäische Gradmessung“, so setzt
das voraus, es fänden keine andern Europäischen Gradmessungen statt. Thatsächlich
existiren aber auch andere, und dies war der Grund, weshalb ich dagegen war. Ich
gehöre der permanenten Commission an und habe mich in derselben nicht gegen die
Vorschläge der Herren Referenten geäussert, weil mir der eben angeführte Grund erst
nachher eingefallen ist. |

Herr Bruhns: Es wurde gestern bereits erwähnt, dass trotz der Vermehrung
der Mitglieder der permanenten Commission, es bei den anderen Bestimmungen der
Statuten bleiben solle, dass nämlich die permanente Commission beschlussfähig sein soll,
wenn bei den jährlichen Versammlungen incl. des Präsidenten vier Mitglieder anwesend
sind. Wenn Jemand etwas dagegen hätte, so würde ich bitten, einen desfallsigen An-
trag einzubringen, damit in der nächsten Generalversammlung darüber verhandelt
werden kann.

Herr Herr: Ich werde mir erlauben, den Antrag zu stellen, dass zur Be-
schlussfähigkeit wenigstens fünf Mitglieder in der permaneuten Commission anwesend
sein müssen.

Herr Hirsch: Ich erlaube mir die Bemerkung, dass dann die Commission doch
sehr oft nicht beschlussfähig sein wird. |

Herr Bruhns: Ich erwähne ferner noch, dass nach dem Statut die Function
jedes Mitgliedes der Commission sechs Jahre dauert. Es sind heute sechs neue Mitglieder
gewählt worden, es würden also die drei Mitglieder, die heute in der Commission ge-
blieben sind, nach drei Jahren austreten müssen und nach sechs Jahren die andern
sechs. Wenn nicht dagegen gesprochen wird, so würde auch der Paragraph des Statuts,
der sich darauf bezieht, nicht zu ändern sein.

Herr Nagel: Ich würde es für zweckmässiger erachten, wenn alle drei Jahre
blos drei ausscheiden.

Herr Bruhns: Es steht in den Statuten: „Die Mitglieder dieser Commission
fungiren von einer ordentlichen Conferenz zur andern. Zur Zeit jeder ordentlichen
Conferenz scheiden alternirend drei, in der folgenden Conferenz vier Mitglieder der
Commission aus. Die Reihe des Ausscheidens bestimmen die Mitglieder der Commission
unter sich selbst durchs Loos.“

Mithin würde vielleicht ein neuer Antrag so zu stellen sein, dass fortan das

eine Mal vier, das andere Mal fünf ausscheiden.

 

 
