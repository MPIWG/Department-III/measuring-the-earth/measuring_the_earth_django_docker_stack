HA Re

 

TDR LME OT TH

w

67

England machte, ein Resultat, das vollkommen übereinstimmte mit demjenigen, welches
im Jahre 1852 mein Vater in Pulkowa gefunden hatte, ein Resultat, das beiläufig um
! Linien von dem unsern abweicht. Dieser Umstand erforderte fernere Maassver-
rleichungen, die denn auch von den Herren Wagner, Döllen und Kortazzi bei uns aus-
refiihrt sind, und die eine Aufklärung dieses auffallenden Unterschiedes gegeben haben.
Es ist jene Normalstange beiläufig um das Jahr 1860 einer absichtlichen Veränderung
unterworfen gewesen, die angerosteten Enden waren wieder von Neuem polirt. Es ist
also eigentlich nicht mehr die alte Stange und es hätte sich streng genommen das
frühere Resultat nicht wieder herausstellen können. Man hätte demzufolge meinen
müssen, das Resultat, welches der Capitain Clarke gefunden, sei ein fehlerhaftes.
Dem ist aber nicht so. Es erklärt sich der Unterschied dadurch, dass die erwähnte Abschleifung
der Endflächen nicht gleichmässig vorgenommen ist, so dass deren Curven nicht mebr
Kugelsegemente sind, deren Mittelpunkte in der Achse der Stange liegen. Capitain Clarke
hatte nicht in der Mitte der Segmente gemessen, sondern an den äussersten Punkten,
an welchen die Tangente senkrecht auf der Achse der Stange steht. Neuerdings haben wir in
Pulkowa die Messung auch für die äAussersten Punkte ausgeführt und mit den gehörigen
Reductionen auf die Mitte die geniigende Uebereinstimmung mit Herrn Clarke erhalten.
Trotzdem haben wir uns veranlasst gesehen, unsere Normalstange noch einmal nach Kne-
land zu senden, damit dort die Messungen auch auf der Mitte ausgeführt werden,
und zu gleicher Zeit ist ein zweiter Maassstab, an dem seit 1852 keine Veränderun-
gen vorgenommen, zur Controle der ersteren Vergleichung mitgesandt, nachdem zuvor
auch wieder die betreffenden Controlvergleichungen in Pulkowa gemacht wareu. In
gleicher Weise haben wir die der Pulkowaer Sternwarte gehörige Copie der Wiener
Klafter auf den Vorschlag des Herrn v. Littrow nach England gesandt, um dort ver-
glichen zu werden, aber auch zuvor wieder mit unseren Grundmaassen sorgfältig ver-
glichen, um uns nach der Rückkehr überzeugen zu können, dass auf dem Hin- und
Rücktransport keine Veränderungen erfolet sind.

a

0g SS

no

Eine zweite Arbeit, auf die ich mir hier aufmerksam zu machen erlaube, sind
die Operationen, welche zuerst durch Herrn v. Forsch in Finnland in’s Werk gesetzt
sind. Es ist dies eine Abweichung von den sonst allgemein gültigen Methoden bei
geodätischen Aufnahmen in Russland. Da in Finnland das waldige Terrain, überhaupt
die Terrain-Beschaffenheit, die geodätischen Triangulationen und deren Benutzung für
topographische Zwecke in grossem Maasse erschwert, so brachte die Pulkowaer Stern-
warte in Vorschlag, dass die genaue Cartographie des Landes auf zahlreiche, durch
genaue astronomische Ortsbestimmungen gewonnene Positionen begründet würde, zwischen
denen aber die einzelnen Wege und Landstrassen geodätisch mit aller Schärfe aufgenommen
würden, Diese Arbeit kann natürlich in den einzelnen Theilen in Bezug ‚guf die linearen
Distanzen nicht mit den allgemeinen geodätischen Operationen concurriren, bietet aber
jedenfalls eine für alle cartographischen Zwecke genügende Genauigkeit. Sie hat
nebenbei in anderer Beziehung einen Vorzug vor andern Operationen, die in Russland

ausgeführt worden sind. Es sind nämlich hierbei zugleich durch ganz Finnland eine
grosse Menge von Höhenpunkten bestimmt, und zwar nicht bloss die Höhen der an
9*

 

 
