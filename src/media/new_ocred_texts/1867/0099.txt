 

TC

Der zweite Antrag lautet:

„Die Zahl der Mitglieder der permanenten Commission ist von sieben aufneun
zu erhöhen.“

Wenn Sie, meine Herren, von der Mitteleuropäischen zur Europäischen Grad-
messung übergehen und zwar durch das Hinzutreten neuer Ländergebiete, so bedarf es
wohl keiner weitläufigen Motivirung, dass auch die Zahl der Mitglieder der permanenten
Commission, die eine internationale sein soll, von sieben auf neun erhöht werde. Und
obwohl die Mitgliedschaft der permanenten Commission, wie in der vorigen Conferenz
ausdrücklich ausgesprochen, nicht an die Staaten gebunden ist, sondern: die Conferenz
diejenigen Personen hineinwählt, welchen sie das grösste Vertrauen schenkt, ist es doch
wünschenswerth, dass des internationalen Charakters wegen der permanenten Com-
mission auch einige Vertreter der neu hinzugekommenen Staaten angehören.

Die permanente Commission hält, wenn die Zahl der Mitglieder derselben von
sieben auf neun erhöht wird, eine Aenderung des Paragraphen der Statuten, welcher
lautet: —

„Ausserdem gehört zur Fassung eines gültigen Beschlusses, dass inclusive
des Präsidenten mindestens vier Mitglieder in der Versammlung erschienen
waren.‘
nicht für nothwendig und zwar deswegen, weil wahrscheinlich die Mitglieder der per-
manenten Commission sehr weit von einander entfernt wohnen werden, so dass es ihnen
nicht möglich ist, alle Jahre zu den Versammlungen der permanenten Commission zu
kommen.

Herr Hirsch überträgt die Anträge in die französische Sprache.

Präsident Baeyer: Ich setze die Wahlen und die Anträge der permanenten
Commission als ersten Gegenstand auf die morgende Tagesordnung.

Herr Bruhns: Herr Fearnley hat seinen Antrag zu den Referenzstationen for-
mulirt und er lautet:

„Oder auch die Methode, wonach zwei Beobachter sich durch eine zwischen
ihnen befindliche unabhängige Signalstation verbinden.“

Präsident Baeyer: Wünscht Jemand das Wort? — Da sich Niemand meldet,
bringe ich den Antrag zur Abstimmung und ersuche diejenigen Herren, welche dagegeu
sind, sich zu erheben. — Es erhebt sich Niemand, der Antrag ist also ange-
nommen.

Herr Förster: Meine Herren, ich bin zunächst der Träger einer Einladung des
Oberst von Chauvin: Selbiger wird sich sehr freuen, die hochverehrten Mitglieder der
Versammlung heute oder morgen Abend auf der Central-Telegraphenstation zu empfangen
und ihnen die neuen Einrichtungen in der Telegraphie zu zeigen. Ausserdem wollte
ich mir erlauben Ihnen mitzutheilen, dass morgen Abend zwischen 5 und 7 Uhr ich mit
meinen beiden Assistenten bereit sein werde. die Herren auf der Sternwarte zu empfangen,

um Ihnen die Einrichtung derselben zu zeigen.
Herr Hirsch: Meine Herren, ich glaube, die Versammlung ist dem Herrn
Oberst von Chauvin von ganzem Herzen dankbar für die Einladung. Es scheint, dass

 

 
