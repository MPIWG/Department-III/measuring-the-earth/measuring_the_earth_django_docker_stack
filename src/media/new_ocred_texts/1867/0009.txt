Te ee ae

D TATRA TNT TTI

wr

PRET TET

adi | Ad

ar

us:

; oF

Die genannten Herren versammelten sich Montag, den 30. September 1867, in
dem Stuck-Saale des Abgeordnetenhauses zu Berlin.

Herr Baeyer als Alterspräsident eröffnete die Sitzung um 12 Uhr 15 Minuten,
worauf Se. Excellenz der Königlich Preussische Staats-Minister Herr Dr. v. Mühler
die Versammlung mit folgenden Worten anredete:

Meine Herren! Mit Jebhafter Freude begrüsse ich die Conferenz, die
aufs Neue hier zusammentritt, um über die Ausführung der grossartigen Aufgabe
einer Mitteleuropäischen Gradmessung zu berathen. Um ein Unternehmen von solchem
Umfange und von so hoher Bedeutung ins Werk zu setzen, bedurfte es der Gesammt-
anstrengung der civilisirten Nationen Europa’s, und wir können uns freuen, sagen zu
dürfen, dass von keiner Seite die Mithülfe hierzu versagt worden ist. Die Regierungen
haben gern das Ihrige gethan, um die Sache zu fördern und haben die Ausführung des
Werkes in die Hände der ausgezeichnesten wissenschaftlichen Kräfte gelegt, die wir in
dieser hochansehnlichen Versammlung in würdigster Weise vertreten finden. Auch die
Preussische Regierung hat bereitwilligst die Hand geboten und hat durch Gründung des
Uentralbureaus für die Gradmessung dazu beigetragen, die Durchführung des Werkes
möglichst zu sichern. Es ist mir eine grosse Freude gewesen, auch meinerseits etwas
dazu haben thun zu können, das Werk zu fördern. Der Erfolg aber, meine Herren,
liegt in Ihrer Hand; möge es, wie jetzt, auch künftig niemals an den grossen geistigen
Kräften fehlen, welche erforderlich sind, ein Unternehmen von solchem Umfange durch-
zuführen und die bedeutenden Probleme zu lösen, die sich an die Durchführung der
Gradmessung knüpfen.

Herr Baeyer: Als Alterspräsident habe ich zunächst die Ehre, Ihnen eine
kurze Uebersicht der Erfolge des abgelaufenen Trienniums zu geben. Wenn wir
uns die Wünsche und Hoffnungen, die wir in der ersten allgemeinen Conferenz
hegten, vergegenwärtigen, dann müssen wir einerseits uns allerdings gestehen,
dass sie nicht sämmtlich in Erfüllung gegangen sind; anderseits sind aber die Fort-
schritte überall sichtbar gewesen, und einzelne Staaten, ich nenne nur die Schweiz und
Sachsen, haben ganz ansehnliche Resultate aufzuweisen. Auch die anderen Staaten
streben eifrig vorwärts, und im Allgemeinen können wir mit den Erfolgen der Mittel-
europäischen Gradmessung, wenn wir die eingetretenen Störungen und Hindernisse be-
rüchsichtigen, zufrieden sein. Ich freue mich, mittheilen zu können, dass die Bethei-
ligung die frühern Grenzen der Mitteleuropäischen Gradmessung bereits überschritten
und sich erweitert hat bis an die äussersten Grenzen Ruropa’s.

Ich wünsche von ganzem Herzen, dass die Arbeiten auch in dem nächsten
Triennium in gleicher Weise fortschreiten mögen und erkläre hiermit die zweite allge-
meine Conferenz der Bevollmächtigten zur Mitteleuropäischsn Gradmessung für eröffnet.
Ich ersuche den Herrn Präsidenten der permanenten Commission, Herrn /lansen, die wei-
tere geschäftliche Behandlung vorzunehmen.

Prasident Hansen: Nach der vorliegenden Geschäftsordnung würden zunächst
die Wahlen der Präsidenten und der Schriftführer vorzunehmen sein, zuvor habe ich

 
