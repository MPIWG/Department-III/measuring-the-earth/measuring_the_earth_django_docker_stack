 

|
|
|

|
if
|
i
i
oe
|
|
|

70
Beobachter: Lindhagen, Beobachter: Capitain Stecksén.
mit Beihülfe des Dr. Rosen. A ee
SE I N
a. Basisverbindung. Hauptdreieckspunkte.
4. Nördlicher Basisendpunkt. 1. Koster.
2. Knösen. 2. Wäderö.
3. Paarp. 3. Amundshatt
4, Wilseharad. 4. Örnekullen.
9. Boarp. 9. Sälö.
b. Hauptdreieckspunkte. 6. Boxwiksadel.
6. Appelviksas. 7. Brandaberget.
7. Elmeberget. 8. Aleklatt.
8. Borstaras. 9. Westerberget.
9. Texelberget.
10. Nidingen.
112 mon.

12. Hvalas.
13. Marstrand.

Die Arbeiten, welche hier zu Lande für die Europäische Gradmessung noch
ausgeführt werden müssen, sind hauptsächlich astronomische Bestimmungen nebst den
dazu gehörigen Operationen um die astronomischen Punkte mit dem vorhandenen Haupt-
dreiecksnetze in Verbindung zu setzen. Ich hoffe im nächsten Sommer einen Anfang
mit diesen rückständigen Arbeiten machen zu können.

Ein wichtiger Theil der Längenbestimmungen ist schon ausgeführt, namentlich
wie schon in dem Jahresberichte für 1865 — wenn ich mich recht erinnere — erwähnt
ist, die Längenbestimmung zwischen Stockholm, Kopenhagen und Christiana, die von
den Professoren Schjellerup, Fearnley und wir ausgeführt ist. Die noch rückstän-
digen astronomischen Arbeiten dürften im günstigsten Falle vielleicht noch zwei Jahre
verlangen, womit dann wohl alle in Schweden begonnenen Arbeiten für die Mitteleuro-
päische Gradmessung beendet sein werden.

Präsident Baeyer: Ich spreche Herrn Lindhagen den Dank der Versamm-
lung aus.

Herr Bruhns: Herr Fearnley hat das Wort.

Herr Fearnley: Leider sind die Verhältnisse in diesem und im vorigen Jahre
nicht so günstig gewesen, dass die Arbeiten in der Ausdehnung hätten fortgesetzt
werden können, wie wir es gehofft hatten. Im vorigen Jahre hat nur ein ganz kleiner
Theil des nördlichen Endes des Dreiecksnetzes von Drontheim an triangulirt werden
können, es scheint aber, das einige Linien weniger gut ausgeführt sind und deshalb die
Arbeit wiederholt werden muss. In diesem Jahre sind die Triangulationen an zwei
Stellen fortgesetzt, und es sei mir erlaubt, in aller Kürze zu bezeichnen, wie weit «ic
Arbeit fortgeschritten ist Die Dreiecksnetze gehen durch fünf Breitengrade, vom 64.
bis zum 59.° Davon ist gegenwärtig eine Strecke von 64"°bis 624° vollständig gemessen.
Vom südlichen Ende hat sich die Triangulation fast bis zum 61.° erstreckt, und ich
hoffe ganz bestimmt, dass die Triangulation vom Norden aus noch in diesem

    

ey hed 1mm) AI

Te

tay

to |

ans man AA |

 
