 

|
|
h
i
|
Ë

154

Antrag hier zu stellen, sondern ich beschränke mich darauf, unter Zustimmung mehrerer
meiner Herren Collegen, an die verehrte permanente Commission die Bitte zu richten,
wo möglich für die nächste Conferenz einen südlicher gelegenen Punkt zu bestimmen,
und dürfte in dieser Beziehung die Wahl über den Platz wohl nicht zweifelhaft sein,
da ich glaube, dass aus verschiedenen nahe gelegenen Gründen Wien der zweckmässigste
Ort wäre. Unter den Gründen hebe ich nur den einen hervor, dass die südlichen, die

Italienischen und die Süddeutschen Mitglieder der Conferenz einigen Anspruch darauf

haben, dass einmal ein mehr südlich gelegener Ort für die Conferenz gewählt werde.
Herr Bruhns: Ich freue mich, dass dies nicht als ein Antrag an die Conferenz
gekommen ist, sondern als Wunsch mehrerer Conferenz-Mitglieder aufgestellt wird.
Einem Antrage hätte ich insofern widersprechen müssen, als man nicht gut drei Jahre im
Voraus bestimmen kann, wo man sich versammeln will. Die permanente Commission
hat ausserdem gestern beschlossen, dass bei wichtigen Fragen nicht die Anzahl von
fünf Mitgliedern die Beschlussfähigheit bedingen soll, sondern dass in solchen Fällen
sämmtliche Mitglieder der Commission gehört werden sollen. Sie sehen also, meine
Herren, die Bestimmung über den Ort der Conferenz wird künfig nicht mehr in der

Hand von drei Mitgliedern liegen, sondern von fünf, da die Majorität von neun fünf’

ist. Ich glaube, dass der Wunsch, den Herr Zügel eben ausgesprochen hat, sicher
bei einigen Mitgliedern der permanenten Commission Anklang gefunden hat. Ich für
meine Person werde ebenfalls nicht unterlassen, sobald die Wahl des Ortes wieder zur
Sprache kommen sollte, wenn nicht wichtige sachliche Rücksichten uns gerade hierher nach
Berlin fordern, für einen anderen Ort zu stimmen. Wir sind ja auch bei den Ver-
sammlungen der permanenten Commission gewandert, wir sind nach der Schweiz, nach
Wien gegangen, um allen Mitgliedern der permanenten Commission gerecht zu werden.
Präsident Daeyer: Hat noch sonst Jemand einen Antrag zu stellen?
(Pause.)

So statte ich denn im Namen der Versammlung simmtlichen Herrn Bericht-
erstatten den Dank der Versammlung ab. Wenn es vorgekommen sein sollte, dass
ich es vielleicht bei dem einen oder dem andern der Herren vergessen hätte, so
hoffe ich, dass die Versammlung mir gestattet, diesen Fehler dadurch zu verbessern,
dass ich es jetzt nachhole. Hieran knüpfe ich noch den Dank, welchen ich besonders
den neuen Herren Mitgliedern, welche zu unserer Conferenz hinzugetreten sind, sage
für die Thätigkeit und Theilnahme, welche sie unserer Sache bewiesen haben. Ich
danke besonders noch den Herren Commissaren für Bayern und Württemberg für ihre
Berichte, weil ich daraus entnommen habe, dass ein alter Wunsch, den ich schon lange
gehegt habe, seiner Realisirung entgegengeht, nämlich der, über ganz Deutschland eine
vollständige Haupt-Dreieckskette zu Stande zu bringen. Die permanente Commission
wird ihre Hand dazu bieten, so viel es möglich ist. Hierbei will ich mich noch aus-
sprechen über die Stellung, welche die permanente Commission in Bezug auf das geo-
dätische Institut, welches vorgeschlagen ist, einnehmen wird. Das neue geodätische

Institut, von dem ich alle Hoffnung habe, dass es zu Stande kommt, wird ausgerüstet

werden mit den besten Instrumenten und Messapparaten , welche der permanenten Com-

    

va a ae

elle

ml

Io WET

a a bone dn nll AOA ald lasing sin 01

ji

etats méme VA |

 
