 

N
i

|
4
id
4
a

EEE Une eer rere

FFE A TT ONO

Rechnung zu tragen, von mir, wie ich hoffen darf, vielleicht mit Zustimmung des
Herrn v. Struve geändert ist: „im Interesse der Wissenschaft“. Es wird dadurch an-
gedeutet, dass wir nur von diesem Standpunkte aus die Wahl des Meter für wünschens-
werth erklären wollen. Es scheint mir also nicht die Gefahr vorzuliegen, dass danach

‚angenommen werden könnte, dass, wenn etwa das Meter nicht allgemein eingeführt

würde, die Wissenschaft kein Interesse mehr an dem Meter hätte. Jene Annahme
ist aber auch rein hypothetisch, denn nachdem England das Metermaass facultativ be-
reits eingeführt hat, da Oesterreich im Begriff ist, dasselbe wirklich einzuführen, und
da nach Allem, was ich darüber gehört habe, auch Preussen im Begriff steht, dessen
Einführung zur Berathung und Beschlussnahmé dem Reichstage des Norddeutschen
Bundes zu unterbreiten, ist die Einführung des metrischen Systems glücklicherweise
heute nicht mehr fraglich, so dass die Gefahr, welche Herr Hansen vorhin andeutete,
schwerlich in Wirklichkeit vorliegt.

Herr Hansen: Das Bedenken, welches ich habe aussprechen wollen, ging nur

‘dahin, dass in der jetzigen Fassung dieses Passus die als wahrscheinlich bevorstehende

allgemeine Annahme des Meter zu ausschliesslich als Motiv für die Annahme des me-
trischen Systems zu wissenschaftlichen Zwecken erscheint.

Herr Hirsch: Der streitige Passus lautet:

„Es spricht sich die Conferenz im Interesse der Wissenschaft für die Wahl
des metrischen Systems aus.“

Die Motive, weshalb wir uns dahin aussprechen, sind: die Wahrscheinlichkeit
der allgemeinen Einführung des Meter und das Interesse der Wissenschaft an diesem
Maasssystem.

Herr Hansen: Ganz recht, und das erste scheint hierbei der Grund zu sein,
weshalb die Wissenschaft sich für das Meter ausspricht.

Herr Hirsch: Wie wünschen Sie also diesen Antrag zu modificiren?

Herr Hansen: Meine Modification würde die F olgende sein.

„Da unter den möglicherweise in Betracht kommenden Maassen das Meter
die grösste Wahrscheinlichkeit der Annahme für sich hat, so spricht sich

die Conferenz dahin aus, dass die Annahme dieses Maasses auch in wissen-

schaftlicher Beziehung am wünschenswerthesten sei.“
Die Commission will sich ja doch nur dahin äussern, dass die Annahme
des Meters im Verkehr auch mit den Interessen der Wissenschaft übereinstimme.
Herr Simons macht in französischer Sprache den Vorschlag zu sagen:
„Da die Einführung des Meter grössere ihrshheinliehköh bietet als die
eines jeden anderen Maasssystems, so en sich die Conferenz für die Ein-
führung des Meter-Systems aus.“
Herr Herr: Da dieser Passus: „im Interesse der Wissenschaft“, welcher hier

‚vorkommt, wie es scheint, in seiner Fassung Schwierigkeiten verursacht, so würde ich

mir erlauben zu beantragen: ihn hier ganz wegzulassen, denn es steht tie in dem
ersten Antrage, dass die Einführung eines biähenlichen Maasses im Interesse der

  

 

OP LOL IL ET

LME nl

Lun tn Ni!

jt aboli he ha mn à:

2: saules nina SA |

 
