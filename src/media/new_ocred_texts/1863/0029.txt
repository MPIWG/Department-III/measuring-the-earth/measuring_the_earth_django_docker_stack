I Ind wenn werner non ers

<1 oP PPR Te TrTT N TT |

Prarie

[LA

a

ns

nahme einiger noch zweifelhafter Punkte entschieden, und die Auswahl der Basis sammt dem
Dreieckssystem, womit die Linie an das Hauptnetz angeschlossen wird, erfolgt ist, so lässt
sich doch schon im Voraus ermessen, dass wegen gleichmässiger und vollkommener Aus-
gleichung der Beobachtungsfehler die Dreieckspunkte, wie z. B. Hohenstein, Bornchen, Hey-
nitz, der Falkenberg u. s. w., welche von Polygonen rund umschlossen werden, mit in das
Hauptnetz hineingezogen werden müssen, weil jedes Polygon ohne weitere Verbindungslinie
ausser den gewöhnlichen Winkelgleichungen noch eine Seitengleichung liefert. Auf diese
Weise erhält man eine grössere Anzahl von Seitengleichungen, welche zur Befestigung des
(Ganzen wesentlich beitragen, wogegen aus bekannten Gründen von denjenigen Seitengleichun-
gen, in welchen die Sinus sehr spitzer Winkel vorkommen, ein beschränkter Gebrauch zu
machen ist.

In dem folgenden Jahre 1864 möchte zunächst das Dreiecksnetz vollständig festge-
setzt, und zur Aufführung der wichtigsten Pfeiler geschritten werden. Der Hr. Prof. Nagel
hat ein Verzeichniss derselben aufgestellt, und die muthmasslichen Kosten, welche die Auf-
führung derselben verursacht, angegeben.

Deshalb geht vor Allem unser gehorsamstes Gesuch dahin,

„dass das Hohe Königliche Ministerium geruhen möge, die Genehmigung zur
„Ausführung dieser Pfeiler auszuprechen und die hierzu nöthigen Gelder an-
„zuweisen,“

Es ist nicht zu erwarten, dass der Bau sämmtlicher Pfeiler in einem Jahre zu Ende
kommt, und es wird daher am Ende des bevorstehenden Jahres das Hohe Ministerium Ein-
sicht über den Fortschritt dieser Baue und der von denselben beanspruchten Kosten nehmen,
sowie hiervon die weiteren Bewilligungen abhängig machen können.

Die eigentlichen geodätischen Arbeiten werden erst im Jahre 1865 beginnen können,
nachdem der grössere Theil der Hauptdreieckspunkte fixirt ist. Dagegen kann der Hr. Prof.
Bruhns seine astronomischen Arbeiten im folgenden Jahre fortsetzen, wenn das Hohe Königl.
Ministerium hierzu Genehmigung ertheilt hat.

Diese Arbeiten würden, soweit es sich jetzt übersehen lässt, bestehen: zunächst in
der astronomischen Verbindung der Leipziger Sternwarte mit dem Jauernick und dem Ca-
pellenberg, sowie mit den Sternwarten zu Breslau und zu Berlin. Der Geldaufwand, welchen
diese Arbeiten verursachen, lässt sich nach den im instehenden Jahre (1863) gemachten Er-
fahrungen beurtheilen, und es giebt auch der Herr Prof. Bruhns in seinem Specialberichte
hierüber die erforderliche Auskunft.

(gez.) Jul. Weisbach.

Anmerkung. In einem Schreiben vom 18. Januar 1864 hat Hr. Dir. Bruhns in Leipzig zur Förde-
rung der gemeinschaftlichen Sache den Wunsch nach einer bald stattfindenden Confe-
renz ausgedrückt.

4*

 
