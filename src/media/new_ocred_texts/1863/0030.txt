 

a, —

16. Sachsen. Gotha.

Der Herzogliche Commissarius, Hr. Geh. Regierungsrath Dr. Hansen, hat die Ver-
bindung der Gothaer Sternwarte durch telegraphische Längenbestimmungen mit den benach-

barten Sternwarten einzuleiten und auszuführen übernommen.

in Schweden un,.d, Non weg en

Herr Prof. Dr. Lindhagen erstattete in einem Schreiben vom 1. Februar 1864 im
Namen der Königl. Commission über die im Jahre 1863 ausgeführten Arbeiten einen so ein-
gehenden Bericht, dass ich den Inhalt nur durch die Aufnahme des Briefes selbst unverkürzt

wieder geben kann:

_... Herr General! Vor etwa einem Jahre berichtete ich Ihnen über die vorberei-
tenden Anordnungen und Arbeiten, welche hier in Schweden getroffen und ausgeführt waren,
um Ihrem trefflichen Vorschlag einer mitteleuropäischen Gradmessung unsererseits entgegen
zu kommen.

Dieses Mal habe ich zunächst Ihnen die Nachricht zu bringen, dass unsere Reichs-
stände die verlangten Mittel zur Ausführung der für den gedachten Zweck hier zu Lande
nöthigen Arbeiten mit der grössten Zuvorkommenheit bewilligt haben, und dass also kein
Hinderniss mehr für den definitiven Beitritt Schwedens an dem Gradmessungs-Verein ob-
waltet.

Ferner habe ich, Ihrem ausgesprochenen Wunsche gemäss, Sie in Kenntniss zu
setzen von den geodätischen Arbeiten, welche für den nämlichen Zweck im letztvergangenen
Sommer unter meiner Leitung und persönlichen Theilnahme, mit Beihülfe des Hrn. Topo-
graphen-Capitains Stecksen, des Hrn. Privatdocenten Dillner und des Hrn. Landmessers
Bergstrand, ausgeführt worden sind. Ich setze voraus, dass Sie im Besitz der Dreieckskarte
des Schwedischen Topographencorps sind, und dass Sie sich also die Oertlichkeiten anschau-
lich machen können. Es wurden nun im vergangenen Sommer drei Grundlinien gemessen,
und zwar eine von ungefähr 1190 Toisen Länge auf dem Exercisefelde Ladugärdsgärdet in
der Nähe von Stockholm, eine zweite von 1357 Toisen Länge auf dem Exercisefelde Axe-
valla in Westgothland, und eine dritte von etwa 3740 Toisen im südlichsten Theile von
Halland, dicht an der Meeresküste. Von diesen Grundlinien wurde nur eine, nämlich die auf
Axevalla, zwei Mal gemessen, welche Messungen nach einer vorläufigen Berechnung ergaben:

1. Messung . . . 1357,03274 Toisen
2. . 1 MSN OSEO : :
Unterschied . 0,00086 Toise = 0,75 Par. Linie.
Wenn man auch zugeben muss, dass diese überraschende Uebereinstimmung zum

Theil dem Zufall zuzuschreiben ist, so muss man doch andererseits zugeben, dass sie ein

bündiges Zeugniss für die Schärfe des angewandten Messapparats abgiebt. Dieser Apparat

AA hui ma

ii 1 steel Jde, ha

 
