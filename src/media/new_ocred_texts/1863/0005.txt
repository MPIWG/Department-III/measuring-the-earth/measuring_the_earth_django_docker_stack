ee

1 PRAY ar TPIT PET |

TTT

"Ud Md Md

ee

Berechner des Neptun, dem Director der Pariser Sternwarte, Herrn Le Verrier, übertragen.
Da die Triangulation von Frankreich bereits beendigt ist, so hat Herr Le Verrier zunächst
sein Augenmerk auf sehr genaue telegraphische Längenbestimmungen unter den verschiedenen
Parallelen gerichtet, und namentlich auf die Französischen Hauptstationer Marennes, Cler-
mont-Ferrand und Mont-Cenis, der grossen Lingengradmessung unter dem mittleren Parallel.
An den Mont-Cenis können sich dann östlich telegraphische Längenbestimmungen von Turin,
Mailand, Padua und Fiume anschliessen, um auch für diesen Theil des Längenbogens neue
und zuverlässigere Bestimmungen zu erhalten. Dadurch wird nicht bloss eine vollständige
Verification des mittleren Parallels zwischen Fiume und Marennes erreicht, sondern es wird
auch die sehr beträchtliche Erweiterung der mitteleuropäischen Gradmessung bis zum An-
schluss an die grosse Französische Breitengradmessung von Formentera nach Dünkirchen
gewonnen, die für Untersuchungen über die europäischen Krümmungsverhältnisse der Erd-

oberfläche von der grössten Wichtigkeit ist.

6. “Han no wen

Ein Bericht ist nicht eingegangen. Der Königliche Commissarius, Herr Prof. Dr.
Riemann, hat zur Wiederherstellung seiner Gesundheit nach Italien gehen müssen. In Folge
dessen ist Herr Prof. Dr. Schering in Göttingen zu seinem Stellvertreter ernannt worden.
Zuverlässigen Nachrichten zufolge ist die höchst wünschenswerthe Herausgabe der Gaussi-
schen Dreiecke in Aussicht gestellt, obgleich bis jetzt darüber noch keine officielle Entschei-

dung getroffen ist.

i. Hessen Gassch

Die Kurfürstliche Regierung hat Anfangs November 1863 ihren Beitritt zu der mit-
teleuropäischen Gradmessung erklärt und den Geh. Hofrath Herrn Prof. Gerling zu Marburg
und den Vorstand des topographischen Büreaus in Cassel, Herm Kaupert, zu Commissarien
ernannt. Leider aber meldeten etwas über zwei Monate später die Zeitungen schon den am
15. Januar 1864 erfolgten Tod Gerlings. Die mitteleuropäische Gradmessung hat in ihm
einen Geodäten ersten Ranges mit reichen Erfahrungen verloren. Er war der einzige noch
lebende Mitarbeiter von Gauss an der Hannöverischen Gradmessung, und mit der Methode
seines grossen Lehrers, der selbst nichts darüber hinterlassen, vollständig vertraut, so dass er
manchen Aufschluss hätte geben können über Fragen, die nun vielleicht für immer in Dunkel

gehüllt bleiben.

8. Hessen - Darmstiad.

Die Grossherzogliche Regierung hat im Januar des vorigen Jahres ihren Beitritt zu,
der mitteleuropäischen Gradmessung bereitwilligst erklärt und den Geheimen Ober-Steuerrath
Herrn Dr. Hügel zu ihrem Commissarius ernannt. Derselbe hat mitgetheilt, dass das von dem

rühmlichst bekannten Geodäten Eckhardt ausgeführte, auf die Darmstädter Grundlinie ge-
Le

 

 
