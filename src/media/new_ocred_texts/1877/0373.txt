 

LM

TR

Ti 1

FO CPT LRU D ||

DUN PENDULE ET DE SES SUPPORTS. 47

La raison pour laquelle il n’a été attribué aux observations du 18
septembre qu’un poids égal au quart de celui des observations du len-
demain à été déjà indiquée; la valeur de k obtenue par les oscillations
du pendule, d'après la moyenne probable des deux jours, est un peu plus
forte, la différence étant toutefois en dedans de la limite des erreurs, que
par l'autre mode d’expérimentation. C’est le seul cas qui donne une dif-
férence dans ce sens, et il serait difficile d’en assigner la raison; peut-
être faut-il lattribuer à la valeur exceptionnellement forte de Q' pour
ces deux jours. Le contre-poids du miroir avait été placé de façon à ce
que la lunette appuyàt sur la pointe avec une force notablement supé-
rieure à celle pour les autres jours, et cette circonstance à pu agir d’une
manière défavorable sur le résultat des expériences. La valeur de k dé-
duite de la moyenne des deux modes d’expérimentation, le trépied du pen-
dule étant posé directement sur le sol de la salle, et le comparateur étant
enlevé, est 3°,214, tandis que l’on trouve 4,474 le pendule étant placé
sur le support en bois; le mouvement communiqué à ce support par les
oscillations du pendule augmente ainsi le mouvement du plan de sus-
pension dans le rapport de 1 : 0,72, lorsque le comparateur est enlevé.
Il n’a pas été fait d'expériences, le pendule étant placé directement sur le
sol de la salle, et le comparateur étant en place. L'expérience statique
donne une valeur plus forte que celle obtenue dans les deux autres mo-
des d’expérimentation dans le rapport de 1 : 0,98; ce rapport se rappro-
che beaucoup plus de l'unité que dans les autres expériences.

5° Expériences faites à Genève, le pendule étant placé sur le grand pilier en mollasse,
el le comparateur étant en place.

A. Par les oscillations du pendule.
D)

ls D pe
13 novembre k — 2,314 erreur moyenne 0,020 0 —0,182 expér. statiquek=—2,661

 

 

14 » 2,389 » 0,047 04117 » 2,796
15 » 2,920 » 0,033 0,188 » 2,834
Moyenne arithm. k a 2,408 moyenne arithmétique k==2,765

Erreur moyenne = 0,060 erreur movenne 0,0515

 
