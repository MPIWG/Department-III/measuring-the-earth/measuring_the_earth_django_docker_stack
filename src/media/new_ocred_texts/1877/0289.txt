REN |

OUT 7

IM a u

Mal UWL

Je!

BL I

PU LP |

bo
TI
=]

Die Fehlergleichungen.

 

 

 

 

 

 

 

 

 

 

 

 

(u) Millimeter & ou
+ 3.8 =d—a 0.226 + 0.859
— 2.2 =d—a 0.113 — 0,249
— 0.2 =a—b 1.41 — 0.282
+ 0.2 =d—e 1.28 + 0.256
— $2 =c —% 0.861 — 2.155
— 51 =g —d 0.212 — 1.081
+ 12 =f—b 0.841 + 1.009
+ 16 =g—f 0.725 + 1.160
— 0.8 =h—f 0.980 — 0.784
— 04 =g —h 0.781 — 0.312
— 10 =h—i 0.861 — 0.861
+ 3.9 =g—m 0.710 + 2.769
+ 0.7 =m—k 4,35 + 3.045
+ 14 =n—hkh 3.29 - 4,522
— 10.6 =m—n 0.725 — 1.685
+ 24 =k —i 1.11 + 2.664
+01=I1—n 3.04 + 0.804
— 40 =—1 —e 0.591 — 2.364
+ 3.9 =r—y 0.861 — 3.358
+ 90 =s —r 0.639 + 5.670
— 7.9 =s—l 0.886 6.999
— 9.2 —é —} 0.421 — 3.873
— 74 =s —t 1.03 — 7.622
+ 289 =t—p 0.201 + 4.804
+ 20 =t —¥¢Y 0.443 -+ 0.386
— 10 =r —q 1.30 — 1.300
+ 1.1 =p —gq 1.34 + 1.474
— 04=p-—y 0.248 — 0,099

Zu den Normalgleichungen gelangt man durch blosse Additionen und Subtractionen der

vorstehend zusammengestellten Zahlen g und gu.

 
