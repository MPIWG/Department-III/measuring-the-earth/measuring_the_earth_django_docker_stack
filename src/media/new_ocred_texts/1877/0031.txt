A a PTT TP |

DEAD TE TETE

HEFTE

Tarot

OPN RT TTR ETI ee

 

19

Mit Dank werden die Einladungen entgegengenommen.

Präsident Zech geht darauf über zum nächsten Gegenstande der Tagesordnung:
zur Berichterstattung der Commissare über die Fortschritte der Gradmessungsarbeiten
in ihren Ländern, und nach der bekannten Reihenfolge wird zunächst mit Baden zu be-
ginnen sein, worüber Herr General Baeyer wohl berichten wird.

Herr General Baeyer berichtet. *)

Präsident Zech dankt und ersucht die Herren Bevollmächtigten Bayone um
Berichterstattung.

Herr von Bauernfeind berichtet. **)

Herr Seidel fügt hinzu, dass er, da mathematische Rechnungen zu einer kurzen
Berichterstattung sich nur wenig eignen, über das, was er in Bezug auf Arbeiten der
Conferenz gethan habe, an der Stelle berichten wolle, wo die Materie in den Verhand-
lungen zur Sprache kommt,

Der Präsident dankt den Bevollmächtigten und bittet den Vertreter Belgiens,
Herrn Adan, um Berichterstattung.

Herr Adan berichtet über die Arbeiten in Belgien. ”**)

Präsident Zech dankt Herrn Major Adan und schlägt vor, bei der vorgeschrittenen
Zeit mit der Berichterstattung für heute abzubrechen. Die nächste Sitzung setzt er auf
Samstag Morgens 10 Uhr fest, und bemerkt, dass zu den Punkten des Programms
noch die Berichterstattung über die Punkte 5a und 5b in dem Programm der Brüsseler
Conferenz hinzuzufügen ist, welche in einer der nächsten Sitzungen stattfinden werde.
Zugleich zeigt er an, dass die Wahlen für die permanente Commission in der Sitzung
am Montag vorzunehmen sind.

Schluss der Sitzung um 4 Uhr 40 Minuten.

Zweite Sitzung.

Stuttgart, Sonnabend den 29. September 1877.

Anfang der Sitzung: 10 Uhr 15 Minuten.

Anwesend: Die Herren Bevollmächtigten Adan, Baeyer, von Bauernfeind, Betocchi,
Bruhns, Fearnley, Faye, Ferrero, Ganahl, Haffner, Herr, Hirsch, Hiigel, Ibanez, Mayo, Nagel,
von Oppolzer, Perrier, Peters, Plantamour, Sadebeck, Schoder, Seidel, Siegfried, Zech;
ferner die Eingeladenen: Herren Peirce, Sainte-Olaire Deville, von Frisch, Gross, Prof. Baur.

Präsident: Herr Zech. Schriftführer: Herr Bruhns und Herr Hirsch.

 

 

*, Siehe Generalbericht, Baden.
**) Siehe Generalbericht, Bayern.
***) Siehe Generalbericht, Belgien.
3*

 

 
