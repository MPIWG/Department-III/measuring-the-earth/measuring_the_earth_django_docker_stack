 

— — a Bun a ET SE ET Een ren
ERENTO SEEN Sa TS, R Y gi a

 

 

 

52 MOUVEMENT SIMULTANE

44841). Shab Aa sa da

lesquels j'ai dû passer. Dans le cas où j'aurais à reprendre ces expé-
riences avec un autre instrument, je me bornerais à déterminer la
quantité & par les oscillations du pendule lui-même, au lieu de répéter
ies expériences dans lesquelles le mouvement oscillatoire est produit par
des poids alternativement soulevés et abaissés. L'on se rapproche beau-
coup, il est vrai, dans ce mode d’expérimentation, de ce qui a lieu dans
les oscillations, cependant il y a une différence qui, dans quelques cas,
peut étre assez considérable, et il me paraitrait préférable de faire plu-
tôt un plus grand nombre d'observations par les oscillations. Toutefois,
et bien que je n’en aie pas fait usage pour la valeur de k, adoptée en défi-
nilive, je ne regrette pas d’avoir fait ces essais, qui ont pu servir de con-
trôle pour les résultats obtenus avec l'autre moyen d’experimentation. :

dna el A:

LA Lulu it

ii Jul ti |

jar hall bila ais al il 404) 96 0

SUN GS Lu RR se 2 ea sn

 
