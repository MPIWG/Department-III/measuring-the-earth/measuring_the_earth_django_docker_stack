 

 

spécial où plusieurs déterminations astronomiques ont déjà été faites, particulièrement
des déterminations de différences de longitude.

M. Zech remercie MM. Perrier et Faye, et donne la parole à M. Hügel pour
présenter son rapport sur les travaux exécutés dans le Grand-Duché de Hesse-
Darmstadt, *)

M. le Président remercie M. Hügel et invite les délégués de I’Italie à faire
leurs communications.

M. Majo présente le rapport d'Italie. **)

M. Zech remercie M. Majo et donne la parole à M. Baeyer.

M. Baeyer remarque à propos du rapport de M. Hügel que la hauteur du Me-
libocus est maintenant, grace au nivellement, parfaitement déterminée. Cela permettra
de réaliser dans un avenir prochain un projet qu’il poursuit depuis longtemps et que
rappelle le rapport du Bureau central, à savoir l’observation de la réfraction terrestre
à de grandes distances. — M. Baeyer a récemment entretenu le Ministre de l’intérieur
du Grand-Duché de Bade des observations à faire dans ce but, et il a reçu pleins
pouvoirs pour s'entendre à ce sujet avec le directeur de l'observatoire de Mannheim. Il
croyait qu'on ne pourrait pas, de longtemps, commencer ce travail, mais à sa grande
satisfaction il se trouve que les nivellements actuellement exécutés permettent de se
mettre a l’œuvre dès maintenant. M. Baeyer croit que la grandeur de la réfraction
terrestre dans différents azimuts peut être déjà déterminée avec succès par l'observatoire
de Mannheim en employant les hauteurs connues des stations du Melibocus, du Donners-
berg et de Hôrnisgründe visibles de cet observatoire.

M. de Bauernfeind fait remarquer que, déjà en 1857, il avait, à propos du
nivellement des voies ferrées de la Bavière, fait dans les montagnes de la Bavière des
observations astronomiques sur la réfraction terrestre. Plus tard, au printemps de cette
année, M. de Bauernfeind a saisi l’occasion de la jonction entre les nivellements bavarois
et saxon, entre les points Reuth und Dobra d’un côté, Ochsenkopf und Kapellenberg de
l'autre, pour faire des observations sur la réfraction, destinées à confirmer sa théorie
de la réfraction et particulièrement à donner des preuves pratiques de la nécessité de
tenir compte de la température dans la valeur de la réfraction. M. de Bauernfeind avait
déjà relevé ce fait hier dans son rapport, mais n’avait pas insisté parce que les coeffi-
cients ne sont pas encore calculés. Il a, dans son travail, attaché une grande impor-
tance à ce que les points extrêmes fussent déterminés aussi rigoureusement que possible,
afin que les angles d’inclinaison déduits des hauteurs mesurées soient exactement con-
nues. Si ces recherches sont poursuivies, le but de M. Baeyer sera atteint.

M. le Président propose à l’Assemblée de passer directement à la discussion de
l’article 5 du programme, parce que le rapporteur, M. Plantamowr, doit partir des ce
soir. Adopté.

*) Voir au rapport général, Hesse-Darmstadt.
**) Voir au rapport général, Italie.

 

Lu ul

geo el, LOMME Mt ll

DM Teil

1 ro

ust pth hn an ah nn à:
