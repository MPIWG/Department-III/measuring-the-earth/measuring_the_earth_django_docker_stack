 

 

aa
a 374"052 au-dessus du niveau moyen de la mer à Marseille, et
a 3737252 au-dessus du niveau moyen de la mer dans 19 ports des côtes
françaises sur l’Océan et sur la Manche.

Les quatre jonctions que la Commission géodésique suisse a opérées entre son
réseau et celui de la France, ont donné pour la hauteur de cette méme pierre de
Niton 3747070 -- 07021 au-dessus du niveau moyen de la mer dans le port de Marseille
(voir pag. 147), valeur qui ne diffère que de 0"018 de celle des Français.

: La jonction à été opérée à l’ouest avec la France, au nord avec la Bavière et
le Grand-Duché de Bade, au sud avec l’Italie. On travaille maintenant à des nivelle-
ments qui permettront la jonction avec l'Autriche et une troisième jonction avec l'Italie.

Le nivellement suisse est relié par la ligne Rorschach—Sainte-Marguerite & celui de la

Baviere.

Les instruments employés étaient construits par Ertel, et possédaient trois fils
et un grossissement de trente-deux fois. Une division du niveau donnait 4.5 secondes
d’arc. On employait deux plaques de terre, dont l’une A 35™ d'épaisseur était placée
sur l’autre.

Dans chaque station, l'installation de l'instrument correspondait toujours à deux
installations de la mire, ce qui permettait de faire deux nivellements a la fois. De
plus, deux instruments travaillaient toujours de conserve, l’un derrière l’autre, et de
telle manière, que l’un mesurait aux stations d'ordre pair et l’autre aux stations d'ordre
impair. Avant et après chaque lecture sur la mire, on notait la position du niveau.
Dans le calcul on à adopté partout la moyenne des lectures du niveau avant et après
l'observation, et la moyenne des lectures aux trois fils.

Le dernier (quatrième) cahier du ,,Bayerisches Präcisions-Nivellement‘ de M. de
Bauernfeind donne, d’après un calcul de compensation approximatif pour l’erreur
moyenne due. la valeur 2.2™., Il est probable, qu’aprés un nouveau nivellement
de quelques lignes du Fichtelgebirge, cette valeur sera ramenée au-dessous de {Ae

Comme on l’a déjà vu, le nivellement bavarois est relié au nivellement suisse
sur le bord du lac de Constance, par la continuation du nivellement à partir de Lindau
le long de la rive autrichienne du lac jusqu'à Rorschach. Ce nivellement permettra en
même temps d'opérer une nouvelle jonctiou avec l'Autriche (outre les jonctions déjà

existantes de Simbach, Salzbourg et Kufstein). Un nivellement de Lindau à Nonnenhorn

à la frontière wurttembergeoise fournit une jonction du nivellement bavarois avec celui
du Wurttemberg, avec lequel il a encore des points communs & Memmingen, Ulm,
Nördlingen et Wurzbourg.

Au nord le nivellement bavarois rejoint le nivellement saxon et le nivellement
entrepris par Institut géodésique, s’étendant de Swinemiinde 4 Constance.

 

x

1) ial OA me

ak niki Wi

LE lu

me a a dl ad pou Han 11:

Ï ii
