 

28 MOUVEMENT SIMULTANE

4° Expériences faites à Genève, le pendule étant placé sur le support en bois
et le m étant enlevé.

 

 

 

 

Espace | Valeur
en y | | Q eh uns de CU nal, LION Le €
S septembre de 7h 1/, a 10h dusoir. a — 2mm,606 D — 8599", grossissement 3299.

Moyenne de trois observalions pour chaque espace, le pendule étant suspendu par le couteau
le plus éloigné du centre de gravité.

N° Le ; p: | p- fe
ee | ee |) tees L 0004 K — 3,673-| 3,635: | — 0,038
Bcd | 043 | 1485| K 06097 k’ 3,548: | 3,548 0
or ie K 081% k’ 3,493 | 3438 | — 0,088
Pe | eo | 1 eG | k — 60,9658 k — 3019, | 3992 | + 0,073
oe Kom KR = 3,067 | 2,998 | — 0,071
Oe | | eos | k§ — 1,680 k’ — 2,444 | 2,634 | -+ 0,190

Moyenne de trois observations pour chaque espace, le pendule étant suspendu par le couteau
le plus rapproché du centre de gravité.

 

 

 

D 0) 971) 0008) k 1018 k — 3,229 | 3,186 — 0,043

0 2518 1280 0108| k —1,261 k’ = 2,989 | 3, 012 aE 028
2 0er 100,005 &k (1,809 bk = 2870 | 2,900 == 0,030
ee | Solo) k 1735 ki — 2,536 | 2,362 | — 9,606
ie 14167 a on x: 1 02 = 9325 | 9,318 | = 0 007
000, 70,2 | 0,435 | k — 2 2 Ko 2156 | 2027 |. — 0,109
équations finales 42 k —15,7585 k’ 549 écart moyen + 0,054
— 15,7585 k 423,741 oo. == % x 2 —+ 0,081

1. / 10

d’où k —4,209 poids 1,840 erreur moyenne 0,065
kl 0,9494 » 3,046 » 0,046 Q’ =0,226
L'expérience statique avait donné k —4u,896 après me minute de Pa application d'un poids de 188

B. Par Vaction d’un poids alternativement soulevé et abaissé.

42 septembre, de 7h 1, à 10h soir. a — 2nn,974 D = 8599™™ orossissement 2891

l'autre de bas en haut.

 

 

 

 

| Moyenne de deux observations pour chaque espace blanc, l'une en parcourant Véchelle de haut en bas,

 

 

 

pt pr PR | R
> 5611 1,518 | k — 0,659 k’ — 3,898 2861 | +00
à doi D as i = 8 sok) 3.735 | = 9.079
4 | 3,805 1,086) k — 0,965 k’ = 3,672 3,516 | — 0,156
B | 2,740 0,846 | k — 1,183 k’ = 3,939 3,980 |. 0,011
11 | 2,375 0,7935) k — 1,260 k’ — 2,993 3,194 | + 0,201
{2 | 1,682 0,616 | k — 1,623 k’ = 2,799 2798 |. 0.069
18° | 1,339 Do ke 1.880 k — 250 G58 | 1005
1 | 1,060 Oe oo ay) | Bios 1 orig
équations finales 8 k —10,594 k' —925,078 écart moyen + 0,088
—10,524 k 15,866 k’=-30,783 zy / 2®=—+ 0,124
Le Le | 6
d'où k —4,570 poids 1,020 erreur moyenne 0,193

in Ri 1,0015 >» 2. ‘022 » AU OT © 0.989
L'expérience statique avait donné k —4ù ‚910 par l'action d'un piods de 18 suspendu pendant 40s

 

 

 

x
=
=
=
a
=
=
=

1881/1188

url:

LM Laub

aa

ju dh dat min à:

a. 4 semeaiamedienns, mets Vid |

 
