Ira we

a DER ua.) Teer |

(TALL TT

IF]

TILL IL

Te eet at Ba à
te i Be 1

Annexe II.

DELLO STATO. BE TUAEG

DELLE

OSSERVAZIONT MAREOGRAFICHE IN ITALIA

E DET RELZAIVIT STVDdı

RELAZIONE DI ALESSANDRO BETOCCHI

Membro della Commissione italiana

in ocensione della Gonferenza generale di Stuttgart nel settembre 1877

ares

Nello scorcio del 1875 in una memoria che ha per titolo ,,Dei mareografi esi-
stenti in Italia lungo i mari Mediterraneo ed Adriatico, e dei risultati dedotti dalle
rispettive rappresentazioni mareografiche“ ho procurato di rispondere, per cid che spetta
all’Italia, ai desideri che nelle Conferenze di Berlino, di Vienna e di Dresda del 1864,
67, 71 e 74 vennero espressi dall’Associazione internazionale per la misura dei
gradi in Europa.

Nella memoria sopraindicata, descritti i meccanismi mareografici di Livorno, di
porto Corsini presso Ravenna e di Venezia, attivati gia da parecchi anni per cura del
regio Ministero dei lavori pubblici, presentavano copia delle rispettive rappresentazioni
mareografiche ed accennavano succintamente le conseguenze che ne erano state gid
tratte. Soggiungevo inoltre nella detta memoria che pit sicuri ed autorevoli risultati
verrebbero dedotti in appresso dopo lo spoglio e la calcolazione della ricca serie di
curve mareografiche gia raccolte, e sopra tutto poi dopo che fossero moltiplicate le sta-.
zioni mareografiche nei principali porti delle coste italiane. |

Quanto alla prima parte, e cioé allo spoglio delle curve mareografiche ed alla
calcolazione delle rispettive altezze medie, da più mesi due diligenti calcolatori (*) se
ne occupano indefessamente. Al metodo ordinario del calcolo dell’altezza media dedotta

*) I signori conte Giuseppe de Courten ingegnere e Giuseppe Hougraj.
25

 
