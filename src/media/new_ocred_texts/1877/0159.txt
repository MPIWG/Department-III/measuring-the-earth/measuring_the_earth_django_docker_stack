 

1 RT PRP |

ram or un] {il

IAE am

RURAL EE | LAURE à: ha À

ae Label à a ah N in
©

Why adi
OT Ie

 

 

Se

Toujours d'après les décisions de la Conférence, la méthode suivie consiste à

niveler depuis le milieu.

Le contrôle est obtenu soit par la clôture des polygones, soit par la répétition
des nivellements des mêmes lignes.

Pour le nivellement suisse, on fait les lectures à trois fils, et on réduit les trois
lectures au fil de milieu en se servant des distances des fils qu’on détermine de nou-
veau pour chaque campagne. Avant et après chaque lecture sur la mire, on note la
position du niveau, amené à très peu près à la position horizontale au moyen de la vis
d’élévation, afin de pouvoir réduire la lecture à l’horizon.

Chaque jour on fait plusieurs fois la détermination des corrections instrumen-
tales. L'égalité des distances est obtenue approximativement par le porte-mire en
comptant les pas: la différence qui reste est calculée par la position des fils extrêmes
sur la mire, et on en tient compte dans la réduction. La distance de visée était en
moyenne de 40», |

Le premier cahier du ,,Nivellement de précision de la Suisse‘ fournit les don-
nées suivantes sur le degré d’exactitude obtenue: les doubles nivellements donnent,
pour erreur moyenne par kilomètre, les valeurs:

0.59% sur un terrain favorable
’ 2 \
ga a tres défavorable.

La clöture des polygones en revanche donne pour cette méme erreur les valeurs:

0,72™" sur un terrain favorable,
2.900 . défavorable.

En tenant compte de l’ensemble des résultats, on trouve pour l’erreur moyenne

par kilometre :
0,66™" sur un terrain favorable,

iT = defavorable.

Dans la derniére (sixieme) livraison renfermant toute une serie de polygones,
on discute aussi les erreurs moyennes. L’erreur moyenne par kilométre est toujours
comprise entre 3 et 4”, et si l’on tient compte de ce que les lignes de nivellement
passent en partie par des cols très élevés des Alpes, ce résultat peut être considéré
comme tout à fait satisfaisant. Pour avoir le plus possible égard à cette dernière diffi-
culte, on a fait des études très minutieuses sur la variation des mires, et lors du calcul
des erreurs moyennes des côtés des polygones, on n’a pas seulement tenu compte de
la longueur de ces côtés, mais aussi des différences de hauteur qu'ils présentent.

Toutes les hauteurs sont rapportées provisoirement à l'horizon de la pierre à
Niton à Genève. Cette pierre est située, d’après le nivellement français (voir pag. 68),

à 374"452 au-dessus du zéro de l’échelle du port de Marseille,
à 374"133 au-dessus du niveau moyen de la mer à Nice, Toulon, Marseille

et Cette,
19*

 
