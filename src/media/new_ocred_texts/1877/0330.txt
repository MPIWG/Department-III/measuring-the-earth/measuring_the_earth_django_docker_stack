   

4 MOUVEMENT SIMULTANE

de suspension, produile par l’application d’une force horizontale d'une
valeur connue p, et qui peut être mesurée par un certain poids. Ce rap-
port # doit être déterminé par des expériences, il peut varier d’un pen-
dule à l'autre, suivant la rigidité plus ou moins grande du trépied en
laiton, et 11 y a également lieu d'examiner s’il ne varie pas, pour le même
pendule, suivant la nature du support, ou du pilier, sur lequel le trépied
est placé. Si en raison de sa flexibilité, dépendant de la matière dont il
est formé, ou de sa masse relativement peu considérable, le support par-
licipe d'une manière appréciable au mouvement oscillatoire du trépied
métallique, et oscille simultanément avec ce dernier et avec le pendule,
le rapport & pourra être différent de celui qui résulterait de la flexion
seule du trépied métallique. Dans le cas, par conséquent, où le même
pendule aurait servi à la détermination de la pesanteur dans différentes
stations, et aurait élé placé sur des supports, ou piliers, d’une construc-
lion ou d’une nature différente, linfluence de cette cause sur la valeur
de # doit être déterminée par des expériences directes.

Ce rapport Æ peut être déterminé directement par une expérience sla-
tique, ainsi que l’a fait M. Peirce; la force horizontale p était donnée par
un poids de 1kg attaché à un fil passant sur la gorge d’une poulie très-
mobile, et fixé au plan de suspension, de telle façon qu’il exerçât une
traction dans une direction horizontale perpendiculaire au milieu du
plan. La déviation «du plan de suspension résultant de cette traction était
mesurée à l’aide d’une échelle en verre fixée au plan, et d’un microscope
muni d’une vis micrométrique, placé sur un support indépendant. L’a-
vantage de ce mode d’expérimentation consiste en ce que la force hori-
zontale p, donnée par un poids d’un kilogramme, est beaucoup plus forte
que la composante horizontale du poids du pendule, que l’on peut ren-
contrer même avec les plus grands arcs d’oscillation; la déviation « est
par conséquent aussi beaucoup plus forte que celle qui se produit dans
les oscillations du pendule. Cette derniére serait 4 peine appréciable, et
dans tous les cas elle ne serait pas mesurable avec le grossissement que
l'on peut obtenir à laide d'un microscope, tandis que la déviation pro-

 

x
=
=
=
=
a
=
=
>
=
x

O1

ah ml

Lt It

à ds a dd hall dal éd saut pus à:

 
