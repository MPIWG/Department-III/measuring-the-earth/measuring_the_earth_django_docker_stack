 

Nu al

PE TEL TITLE LL

179

technologique de Berlin, et a été mise à ma disposition par Vobligeance de M. le pro-
fesseur Paalzow. Les lectures micrométriques furent faites alternativement sans le poids
et avec le poids, en n’exécutant chaque fois qu’une seule lecture, afin d’éviter une
cause d'erreur pouvant tenir au mouvement du Support du micromètre, ce support étant
en bois. Dans les lectures faites alternativement sans le poids et avec le poids, je
finissais par la disposition par laquelle j'avais commencé (11 pour l’une et 10 pour
l'autre), en sorte que l'instant moyen des observations fût le même dans les deux dis-
positions. La valeur d’une révolution de la vis micrométrique a été mesurée séparément.
Voici les résultats des différentes séries de mesures :

24 mai 1876 À. M. : . 0 E == 0mm0840
Temp. 13° C>P2M. Mur 22 = OF 059
0 . 0340

0.0341

25.:mai 1876, Temp. 130... ...0 0%
0... 0836

Moyenne . . © = 0. 0839 + Omm0001

A Hoboken (près de New-York), j'ai obtenu, par la bienveillance de M. le pro-
fesseur Morton, une poulie excellente qui a été exécutée dans Patelier du Stevens
Institute of technology. J'ai toujours fait une lecture sur chacun des deux traits de
l’échelle avant de changer la disposition du poids. Voici les résultats des séries séparées.

(mars 1877. Temp. 159 C. > = 0220349
10 mars 1877. Temp. 12° . 0332
. 0337
. 0343
. 0342
. 0339
. 0334

. 0342 \ Ces deux series ont droit &
un poids double dans

SO ee es

En moyenne . . 2 — 0. 0340 + 0==0001

D eee eee

Dans toutes ces expériences faites dans différentes positions de l'échelle, la
flexion obtenue a été ramenée à celle qui correspondait au milieu du couteau, et c’est
cette dernière qui est désignée par &.

C’est à cette dernière valeur que je donne la préférence.

Il résulte des expériences décrites à la page 11, faites en vue de la détermi-
nation de la position de l’axe de rotation, que l’extrémité antérieure du plan de suspension
est éloignée de cet axe de )/1™355 X 1207 — 120, Et puisque le mouvement de ce
bout avec le poids d’un kilogramme est 2 + 070008 — 0mm0348, la correction + 0.0008

23*

 
