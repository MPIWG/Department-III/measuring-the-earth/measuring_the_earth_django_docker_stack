va were -

 

LL AR

ma dur tutti à A i
TL ORE TRE RRR EN DIT LIPE GITE FINI

Annexe Id.

Zweite Note

von Th. von Oppolzer.

Was die Bestimmung des Antheiles der Beweglichkeit des Stativs anlangt, so
habe ich zur Bestimmung dieses Fehlers das folgende Verfahren erdacht und theilweise
schon eingeschlagen, welches mir die Bestimmung noch anderer Fehlerquellen erlaubt,
zu denen ich insbesondere rechne: Abweichnung der Schneidenkante von einer geraden
Linie, Deformation derselben auf dem Lager durch den Druck, ete., etc., und welchen
Apparat ich schon in der Pariser Conferenz seinem Principe nach kurz erwähnt habe.

Ein kleines Stäbchen von etwa 3. Lange aus Messing wird in die Nute des
Lagers der Schneiden eingeschoben und durch schwache Spiralfedern nahe seiner Mitte
leicht gegen die Schneide an eine Stelle angedrückt, an der die Messung mit Hilfe des
Mikroskopes des Gomparators gemacht wird. Das Stäbchen liegt also dem Wesen nach,
wenn es richtig adjustirt ist, horizontal, und steht vertikal auf der Schneidenkante.
Jede laterale Bewegung der Schneidenkante wird demnach das genannte Stäbchen im
horizontalen Sinne verschieben. Die eine Endfläche des Stäbchens steht nun mit einem
Fühlhebel in Verbindung, das andere mit einer Spiralfeder, um das Gleichgewicht mit
der spannenden Feder des Fühlhebels herzustellen; man wird. jetzt leicht einsehen
können, das der Fühlhebel die Grösse der lateralen Verschiebung der Schneide wird
bestimmen lassen, und hiermit wird die Relation zwischen der Schneidenkante und der
thatsächlichen Drehungsachse ermittelt werden können, eine Relation, die gemeiniglich
bisher übergangen wurde.

Der Drehungsachse des Fühlhebels habe ich eine solche Einrichtung gegeben,
dass ich dieselbe vorerst mit dem Lager der Schneiden in feste Verbindung bringe; ich
bestimme dadurch die Fehler, die von der Form der Schneiden abhängen, die ich mit
c bezeichnen will. Befestige ich aber nun die Drehungsachse am Pfeiler, auf welchem

 
