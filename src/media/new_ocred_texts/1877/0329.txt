I m

TOT 1T |

al am IT Runde

u

Kl!

x
=
=
=
8
=
æ
x
=
=
IE
a>
=

RECHERCHES EXPERIMENTALES

SUR LE

MOUVEMENT SIMULTANÉ D'UN PENDULE

ET

DE SES SUPPORTS

Dans le pendule à réversion, suivant la construction de Repsold, le
plan de suspension est fixé à un plateau assez massif en laiton, qui est
porté par trois tubes creux, en laiton également, deux des pieds étant sur
une ligne parallèle au plan d’oscillation, et le troisième dans une direc-
tion perpendiculaire à cette ligne. Lorsque le pendule oscille, la compo-
sante horizontale de son poids peut produire une flexion alternativement
sur l’un, ou sur l'autre, des deux tubes, s'ils ne sont pas absolument ri-
gides, et donner lieu ainsi à un déplacement du plan de suspension, tan-
tot dans un sens, tantot dans le sens opposé, suivant que le pendule est
d’un côté, ou de l’autre, de la verticale.

MM. Cellérier et Peirce ont étudié, au point de vue théorique, Vin-
fluence que pouvait avoir ce déplacement oscillatoire du plan de suspen-
sion sur le mouvement du pendule lui-même, et ils ont développé les
formules à l’aide desquelles on pouvait éliminer cette cause d'erreur, par
l'application d’une correction devant être apportée à la durée d'une os-
cillation, ou à la longueur du pendule simple faisant une oscillation dans
une seconde. Le calcul de cette correction exige la connaissance d'une
quantité k, par laquelle on désigne le rapport de la déviation « du plan

 
