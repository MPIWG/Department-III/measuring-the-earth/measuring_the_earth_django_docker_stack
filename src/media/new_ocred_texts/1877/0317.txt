m

1111 PP me

MA A

ulm im!

11

CAN LUE. TZ RCI LOT RIT, RAL

Mia]

305

fach (da sie früher schon nivellirt waren) nivellirt worden. Leider war es wegen der
schlechten Herbstwitterung nicht möglich, die Arbeiten ganz zu Ende zu führen, wes-
halb auch die Gesammtausgleichung und Publicirung bis nach Vollendung der beiden
noch rückständigen Nivellementslinien verschoben werden muss.

C. Bruhns.. A, Nagel.

eee

Schweiz — Suisse.

1. Rapport de M. Hirsch sur les déterminations astronomiques, les travaux
de nivellement et les observations de pendule.

La publication de la determination de longitude, destinée à joindre notre réseau
des points astronomiques à celui de l'Autriche, que j'ai annoncée dans le rapport de
l'année dernière, à paru au printemps de 1877 sous le titre: Détermination télégra-
phique de la différence de longitude entre l'observatoire de Zurich et les stations astro-
nomiques du Pfaender et du Gaebris, par E. Plantamour et R. Wolf, Geneve, Bäle,
Lyon, 1877.“ Comme ce mémoire a été distribué, par les soins du Bureau central, à tous
les délégués de l'Association géodésique, il suffit de constater les résultats très satis-
faisants malgré l’imperfection des pendules enregistreurs dont on s’est servi à Zurich
et au Pfaender. Voici les différences entre les trois stations avec leurs erreurs pro-
bables, qui méritent d'autant plus de confiance qu’elles reposent, non seulement sur
l'accord des déterminations de différents jours, mais surtout sur la clôture du triangle
des longitudes :

Pfaender— Zurich - —= 4” 53691 + 0007
Gaebris— Zurich — 3 40.070 + 0.005
Pfaender—Gaebris = 1 13.621 + 0:009.

La jonction avec l'Allemagne, commencée en 1876 par l'opération entre les
observatoires de Geneve et de Strasbourg, dont les calculs de reduction sont presque
termines, a été complétée en 1877 par celle entre Genéve et Munich; elle a été exécutée
du 1% mai au 7 juin, favorisée par le temps et par un état des lignes télégraphiques ;
tres satisfaisant en Suisse aussi bien qu’en Allemagne. La determination de l'équation
personnelle entre MM. Plantamour et v. Orff, faite déjà l’année dernière à Genève, a
été répétée à la fin des opérations à Munich avec l'instrument de monsieur v. Orff. |

Enfin la jonction entre la Suisse et la France, projetée depuis longtemps, a été
également réalisée en 1877 par les deux opérations entre Paris et Neuchâtel d’un côté,

39

 

 
