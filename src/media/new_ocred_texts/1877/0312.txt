Fe nCns

 

 

 

 

300
dene Stellung der von beiden Beobachtern benutzten Schreibstifte des Chronographen
keinen Einfluss darauf haben. Man wird daher annehmen können, dass der Sicherheit
des für die Längendifferenz ermittelten Werthes sehr nahe der dafür gefundene mittlere
Fehler entsprechen wird.
Aus sämmtlichen Beobachtungen folgt:

Länge der neuen Kopenhagener Sterawarte (Steinpfeiler im östlichen
Meridianzimmer) östlich von der Altonaer Sternwarte (Meridiankreis)
— 10” 32: 563 mit dem» mittleren Fehler = 0:012.

Mittelst der bereits erwähnten Triangulirung wurden auch die Polarcoordinaten
des genannten Pfeilers der Kopenhagener Sternwarte gegen den Nicolai-Thurm (Signal-
stange) in Kopenhagen wie folgt bestimmt:

Azimuth — 160° 18’ 35",5; Log. Abstand = 2.7017002 (Toisen).

Im ersten Bande der Dänischen Gradmessung sind für die Coordinaten der ehe-
maligen Universitäts-Sternwarte, des runden Thurmes (Windfahne), von Nicolai ausge-
rechnet, folgende Werthe gegeben:

Azimuth — 131° 49’ 7”, 16; Log. Abstand = 2.3791638 (Toisen).

Aus diesen Coordinaten erhält man, in Betreff der geographischen Lage gegen
den Steinpfeiler im östlichen Meridianzimmer der Kopenhagener Sternwarte:
Nicolai: 1°261 östlich; 29.858 südlich;
Runder Thurm: 0:066 westlich; 19:793 südlich.

Vom Jahre 1820 an ist der runde Thurm für astronomische Beobachtungen
wenig benutzt worden. Der Ramsden’sche Zenithseetor, mit welchem Schumacher in den
Jahren 1820 und 1821 die Polhöhe in Kopenhagen bestimmt hat, war auf Holkens
Bastion aufgestellt, und eine auf derselben Bastion errichtete kleine Sternwarte hat auch
ferner, bis zur Errichtung der neuen Sternwarte, vorzugsweise als Beobachtungslocal ge-
dient. Nach einer von Schumacher ausgeführten Triangulation liegt die Sternwarte auf
Holkens Bastion 0°57 westlich von dem runden Thurm.

Nachstehend sind für die Längen der neuen Sternwarte, des Nicolai-Thurms und
der beiden Observatorien, welche früher in Kopenhagen zu astronomischen Beobachtungen
benutzt worden sind, diejenigen Werthe zusammengestellt, welche aus der Längenbestim-
mung durch galvanische Signale, in Verbindung mit Triangulirungen hervorgehen:

Länge östlich von der

Altonaer Sternwarte
(Meridiankreis).

Kopenhagen, neue Sternwarte, Pfeiler im östlichen

De Gale ke audaddon _ 10m 325563
Runden har ns EL nsten 10™ 32°497
Observatorium auf Holkens Bastion ....... 102. 31293

DAME A Léna GO Ra We N 10% 337824

 

 

~
=
=
a
=
x
a

11111 Vie

Ih mi

beth toit

ns nel dn in Albani, ld ada sre 1:
