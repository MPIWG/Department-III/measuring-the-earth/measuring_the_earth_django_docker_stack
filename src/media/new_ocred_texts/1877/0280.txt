 

 

 

Se NEUERER

263

D'un autre côté l’on a fait vérifier la cote de la tablette du bajoyer de l’écluse
du commerce à Ostende par rapport au buse de léchelle de pilotage, et l'on a obtenu,
par des mesures précises, un nombre un peu inférieur à celui de 1853; il place la
première position de la mire & 602585 au-dessus du zéro adopté par le Dépôt de la
guerre et non à 62835. L'écart de 0"0250 entre ces deux déterminations affecte toutes
les cotes du réseau des nivellements, et vient, par une circonstance heureuse, compenser
à 3 millimètres près la majoration que les cotes auraient dû subir pour la raison énoncée
plus haut. L'on croit donc devoir maintenir les résultats du calcul de la compensation;
ils seront publiés sans retard.

Par un troisième point de raccordement à Terneuzen, omis dans le calcul com-
muniqué lors de la conférence de Bruxelles, le Peil d'Amsterdam serait au même niveau
absolu que la mer moyenne d’Ostende, on a en effet le Peil a 271337 au-dessus du zero
du Dépôt de la guerre et la mer moyenne & 271355 au-dessus de ce méme point de
comparaison.

Mais le Peil, contrairement aux idées en vigueur, est le niveau moyen des
hautes eaux dans l’Y à Amsterdam et non le niveau moyen des eaux. Les ingénieurs
du Waterstaat n’ont aucun doute à cet égard, et ils ajoutent que le Peil correspond
sensiblement au niveau des mers moyennes sur le littoral hollandais.

En définitive, l'amplitude de la marée étant très faible dans l'Y, nous trouvons,
d’après le général Krayenhoff et M. Van Rees, dix centimètres de différence entre le
Peil et le niveau des mers moyennes à Amsterdam.

En partant de cette donnée, le niveau moyen à Swinemünde est à 0.0522

au-dessus de la mer moyenne d'Amsterdam. Le Lieutenant-Général von Morozowicz

obtient 0121 pour la même différence. (Mittheilungen aus Justus Perthes geogra-
phischer Anstalt, Juillet 1877).
J'annexe à ce rapport les cotes probables des mers moyennes en différents

ports, rapportées à la mer moyenne d’Ostende, résultant des observations des marées
depuis le 1° mars 1854 jusqu'au 31 août 1893.

Mille ....... 227130 | Be Havre 2. 2.8 — 0.389
Bayonoe.......- + 0.126 | Dieppe. ...: — 0.071
Bassin d’Arcachon .. — 0.130 un + 0.106
La Rochelle... . eee lus... + 0.023
Sables d'Olonne ...  —0.141 Dankerque <2... + 0.046
Samt Nazaire . 2... 10,017 | Otandeis ie à se 0.000
Ban ee 2.5.5... — 0.006
Brest. wens), oe + 0,202 ° Anısterdam sw 28 = 0.100
Sant Malo bos. 0,215 | Cumhowen ios cine — 0.164
Canoe nk + 0.367 Eckernliöordes as — 0.358
Cie 1... 4. 0.160 KR 0a Ck ea 0.259
Chloe. un 0.165 | Trawemiinde < . sc. = 0223
Darentalı a era. + 0.127 2 Wismar sa. — 0.190

 

a A 1 Bill |: did 14

111111 ed,

VW

LA tube

Lu to |

110 el bb a Haba data A sud pus 1:

à: imaihitéens mémtt VA

 
