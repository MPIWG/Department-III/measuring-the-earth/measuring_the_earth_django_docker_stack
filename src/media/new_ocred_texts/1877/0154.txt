 

 

 

 

142
images héliotropiques se présentent, pendant la plus grande partie de la journée, sous
des apparences et sont affectées d’ondulations telles qu'on ne peut pas les pointer avec
sécurité. L’observateur, pourvu d’une lunette dont le réticule est fixe, est obligé d’at-
tendre les courtes périodes de la journée où les images lui apparaissent petites et suffi-
samment réduites.

Ici encore l’usage du fil mobile présente un avantage incontestable; il permet,
en effet, en multipliant les pointés sur des images même dilatées et affectées d’une
certaine mobilité, d'obtenir le pointé ‘de la position moyenne de l’image avec une pré-
cision qu'on ne saurait attendre d’un pointé unique, et d'agrandir ainsi les périodes de
temps pendant lesquelles les observations sont praticables avec succès.

C’est afin de multiplier les observations dans les conditions atmosphériques les
plus variées que nous avons eu recours aux observations de nuit injustement proscrites,
depuis cinquante années, de la pratique de la géodésie européenne, Il était, du reste,
tout naturel de penser que les belles images se produisant pendant le jour seulement
aux instants d'équilibre des couches de l'atmosphère, il en serait de même pendant la
nuit, et que les périodes d'équilibre seraient plus prolongées que pendant le jour.

Les premiers résultats que nous avons obtenus, ont été communiqués à l’Asso-
ciation à Paris en 1875, et sont insérés dans le ,,Generalbericht‘, pag. 144 et suivantes :
nous nous contenterons de les résumer ici.

Les appareils employés pour la production des signaux lumineux sont des colli-
mateurs optiques, au foyer principal desquels on concentre la lumière émanée d’une
lampe à pétrole; l'objectif illuminé devient visible à de grandes distances, et produit
l'effet dun globe lumineux à contours bien limités, de teinte uniforme, offrant une bis-
section facile et sûre.

Afin que les comparaisons fussent décisives, nous avons fait, en double,
c'est à dire de jour et de nuit, toutes les stations géodésiques de la campagne d'été
de 1875, au nombre de dix, situées en pays de moyenne montagne, entre 200 et
150 mètres daltitude. Chaque série de nuit n’est que la répétition d'une série de jour;
deux séries conjuguées sont identiques, en ce qui concerne l’instrument et l’observateur.
et ne different entr’elles que par la nature des signaux visés et l’heure des observations.

Nous avons ainsi trouvé, avec le capitaine Bassot, que l’erreur moyenne d’une
observation isolée, obtenue par la moyenne de 37 directions observées, est égale :

de jour a a2 1749

oe différence 0”02,
dépit au 1.47

et la moyenne de lerreur probable d’une direction finale résultant de Vobservation de

vingt séries est égale, dans les deux cas, à + 0"28.

Les écarts qui se révèlent pour une direction entre les résultats de jour et de
nuit sont, tantôt dans un sens, tantôt dans l’autre, généralement petits, et, excepté pour
deux directions, toujours inférieurs à 0/3.

Le calcul des triangles a permis de rechercher comment les directions observées

A a pe a a ai an

 

=
=
3
=
:
à
a
=
=
=
2
=
i

Viel

1

‘in |

ah iki

 
