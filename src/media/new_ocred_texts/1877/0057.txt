 

Baer

UIP NNN Tt te

Wrs-ikeienn han AA LA a ah
i i he A

aah

45

 

a ——————————————eeeeeeeeoTlTlee=

Benennung
des
Basis-Apparates.

Apparat von Repsold |
fiir die Triangulation |
der Insel Java con-
struirt.

3 Messstangen, jede von
4 Toisen Länge, aus
Kiefernholz gefertigt
u. dergestalt mit ein-
ander verbunden, dass
die eine davon ihre
srössteAusdehnung in
horizontaler, die an-
derein vertikalerRich-
tung hat, von vorn ge-
sehen von der Form. |
Am vorderen Ende der |
Maassstäbe sind Eisen-
platten, an den hinte- |
ren Enden sind diese
Platten noch mit)
Züngelchen versehen, |
welche durch einen
Nonius in Zehntel-Li-
nien eingetheilt sind. |
Der Mess-Apparat hat |
Aehnlichkeit mit dem
vom (General Roy sei-
ner Zeit gebrauchten.

Apparat des Baron
Wrede, analog dem
Struve’schen Apparat.

Maassstäbe,
welche zur Etalonnirung
gedient haben.

Normal-Meter von Rep-
sold, bei einer Tem-
peratur von 65° Fah-
renheit mit dem Meter
verglichen, welches

| van Swinden aus Paris

mitgebracht hatte.

Eine auf dem hiesigen
Museum befindliche
Toise, welche von Le-
noir in Paris verfer-
tigt und mit der Toise
von Peru durch Bou-
vard verglichen ist.

 

 

Bemerkungen.

Zweimal gemessene Grundlinie, die Differenz der bei-
den Messungen betrigt 5™™0, :
Der Apparat von Repsold ist in den Astronom.
Nachrichten No. 1661 beschrieben.
Repsold’sches Meter bei 65° Fahr. — van Swinden-
sches Meter bei 0° + O0™™175.

Ueber die Messung der Basis ist ein Mémoire in fran-
zösischer Sprache abgefasst, wovon ich eine Ab-
schrift mit allen Zeichnungen und Tabellen auf der
zweiten General-Conferenz der Gradmessung von
1867 (Verhandl. 8. 41) überreicht habe.

Die Ausgleichung eines Dreiecksnetzes zur Ver-
bindung der Observatorien von Göttingen, Seeberg,
Darmstadt, Mannheim, Speier und Strassburg nach
der Methode der kleinsten Quadrate mit den dess-
fallsigen Schleiermacher’schen Formeln i. J. 1834
von dem Unterzeichneten ausgeführt, hat die Darm-
städter Basis ergeben zu 39761158,

daher verglichen mit 3976.087

Differenz = 0.071

1 a
oder „ou; der Länge,

Darmstadt, den 5. December 1877.

Dr. Hügel,
Grossherz. Hessischer Bevollmächtigter
der europäischen Gradmessung.

Nur die Grundlinie von Axevalla ist zwei Mal ge-
messen worden; die Differenz der beiden Messungen
beträgt 0:75.

 
