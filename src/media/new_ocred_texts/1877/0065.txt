 

en

RP ETS FIRMEN TE

ILE

THEN

Aka

t

a
2=
==
se
25
g
1
a

=
iE
5
iR
iR
=

 

ou

| a —

Benennung
des
Basis-Apparates.

Maassstiibe,
welche zur Etalonnirung

| Bemerkungen.

 

Alter Struve’scher Ap-
parat No. 1.

desgl.
Verbesserter Struve’-

scher Apparat m.Fühl-
hebel.

desgl.
Tenner’scher Apparat.

Struve’scher
No: 2:

Apparat

desgl.
desgl.
desgl.
desgl.

Bessel’scher Apparat.

Spanisch. Apparat, con-
struirt von Brunner.

Etalon N, von Pul-
kowa.

desgl.

Etalon P.

| Sashen No. 10 = T.

Etalon N verglichenmit
dem Normalstab N,,.

 

desgl.
| desgl.
desgl.
desgl.

Copie No.9 der Bessel’-
schen Toise.

Borda’sche Messstange
No. 1.

 

Die Grundlinie Oefver Tornéa ist zweimal gemessen.

Ni ==) 1728 !o1246 N

N:

= Ny + 0!00817 bei 13° R.
1728102066

—

 

Die Basis ist zweimal gemessen worden.

Die spanische Messstange ist 4 Meter lang und be-
steht aus Platina. Sie bildet mit einer Messstange
aus Messing ein Metallthermometer; an den äusser-
sten Enden der Platinastange befinden sich ein-
getheilte Zungen, mit deren Hilfe man die Ent-
fernungen zwischen den in die Grundlinie einge-
richteten Axen der Mikroskope misst. Von der
Grundlinie von Madridejos ist nur ein Theil zwei-
mal gemessen worden, alle übrigen Grundlinien ihrer -

 

ganzen Länge nach doppelt.

 
