ee

PRE PTT PP mr |

it Ue

TANT

FOTN TT Te ert

Fer TER HB NN

 

INHALT — TABLE DES MATIERES,

Bericht über die Verhandlungen der fünften allgemeinen Conferenz der Europäischen
Gradmessung, abgehalten in Stuttgart vom 27. September bis 2. October 1877.

Seite
Verzeichniss der zur Conferenz erschienenen
Bevollmächtigten und der eingeladenen
Herren 2.0.2.0. 000000 3 :
Erste -Sitzune 3222) 2.2. _ 4
Eröffnung durch Bertn Ibanez...  ..,... 4
Begrüssung durch Se. Exc. Herrn Minister-
prasidenten Dr. von Mitmacht.. .. =. . 4
Geschäftsordnung für die Conferenz der Euro-
paischen Gradmessune . ...... 5
Constituirung des Bureau... ...  .. nt 6
Programm für die fünfte Generalconferenz im
Jahre 1877 pe ee 0% 7
Bericht der permanenten Commission ..... 8
Bericht des Centralbureau und des geodäti-
schen Instituts in Preussen 6° 2... . 12

Mittheilungen des Präsidenten Herım Zech.. 18
Berichte der Herren Baeyer, von Bauernfeind,

Seidel und Adam. ....... ee, 19
Zweite. Sitzung ...........,. ee 19
Mittheilungen des Präsidenten ,........ 20
Brief des Superintendenten der Coast Survey
der Vereinigten Staaten... .......,, 20
Begrüssung der Herren Peirce und Sainte-
Claire, Deville 5 ee 20
Eingegangene Publicationen ........., 21

 

Seite

Bericht der Herren Perrier, Faye, Hiigel, Mayo 21
Mittheilungen der Herren Baeyer und von

Bauernfeind tiber Refractions-Beobachtungen 21
Bericht des Herrn Plantamour iiber Punkt V

des Programms, Pendelbeobachtungen ... 22
Berathung iiber diesen Gegenstand ...... 23
Beschluss 7. 2 na ee 24

Bericht über Punkt III des Programms, über
astronomische Bestimmungen und Arbeiten

von Herrn v. Oppoke .  .  .n. 24
Berathung und Beschlüsse über diesen Gegen- __
Stand. 27

Bericht über Punkt IV des Programms, über
Sternörter, welche bei den astronomischen
Bestimmungen angewandt worden sind, von

Here Bruhns.. .... 2.2.0. 00 0002000 28
Beschluss über diesen Gegenstand ....... 37
Dritte Size 0022... 07
Antrag des Herrn Hirsch zur Frage der Pendel-
beobachtutigen’ =. 0 03 38
Wahlen in die permanente Commission.... 38

Berichte der Herren Fearnley, Haffner, Ga-
nahl, v. Oppolzer, Peters, Bruhns, Nagel. . 39

Bericht tiber Punkt VI des Programms, iiber
Basismessungen von Herrn Perrier ..... 39

Mittheilungen zu diesem Gegenstande..... 58

 
