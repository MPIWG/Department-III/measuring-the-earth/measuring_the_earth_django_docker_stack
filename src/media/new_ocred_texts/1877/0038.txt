le el

Re D eee se eee

ns

    
   

ESS

=

RE

ss

ee

 

 

D:
|
i
P
i

RE EEE

ei REE ee

ee

=

wichtigen Kontrollbestimmung durch Schliessung der Polygone, die dadurch erhaltenen
Kontrollen scheinen mir sogar vor der Kontrolle durch Wiederholung der Operationen
den Vorzug zu verdienen. Als besonders wichtig müsste in dieser Hinsicht die mehr-
fache Verbindung der Stationen Marennes, Biarritz, Carcassonne unter einander hervor-
gehoben werden, um den spanischen Längenbestimmungen, die vorerst nur auf der
einzigen Bestimmung Paris-Madrid beruhen, in den Orten Biaritz-Carcassonne weitere
sichere, gegen das europäische Längennetz festgestellte Ausgangsmeridiane zu bieten;
dann würden auch die in Spanien und Portugal dringend nöthigen Bestimmungen wohl
bald zur Ausführung gelangen können.

Im Norden des Netzes würde die leicht auszuführende Längenbestimmung Lund-
Kopenhagen ganz wesentlich zur festen Verbindung des vorliegenden Netzes beitragen.
Die unmittelbare Verbindung des nördlichen Russland mit Deutschland erscheint zur
Zeit noch ungenügend; die Längenbestimmungen Berlin-Pulkowa, Berlin - Warschau,
Königsberg-Pulkowa und Königsberg-Warschau würden die nach meiner Ansicht letzte
vorhandene Lücke in dem Längenbestimmungssystem ausfüllen; wobei jedoch jede ander-
weitige zur Ausführung gelangende Längenbestimmung als werthvoller Beitrag anerkannt
werden muss.

Einer Schwierigkeit ist jedoch auch hier zu gedenken, die für das vorliegende
Referat von Bedeutung. Bei Längenbestimmungen tritt nicht selten der Fall ein, dass,
was die Beobachtungen anlanst, völlig gelungene Operationen bei der Reduction sich als
nicht zureichend erweisen; es wäre nicht undenkbar, dass bei einer oder der anderen
hier als ausgeführt bezeichneten Längenbestimmung eine derartige Verwerfung sich
als nöthig erweisen wird; bei den zahlreichen Bestimmungen jedoch, die für diese
Zwecke vorliegen, dürften derartige Ausnahmsfälle wohl kaum eine wesentliche Bedeu-
tung haben.

In der zweiten Karte (siehe Karte II.) habe ich die Breiten- und Azimuth-
bestimmungen aufgenommen und die Stationen, wo Pendelbeobachtungen gemacht sind,
hinzugezogen, was ohne Beeinträchtigung der Uebersicht geschehen konnte. Ausge-
schlossen sind jene Bestimmungen, wo zahlreiche Beobachtungen auf einem verhältniss-
mässig kleinen Gebiete ausgeführt, um die Lokal-Attraction zu ermitteln; wie dies der
Fall ist bei den schönen Arbeiten des Königl. Preussischen geodätischen Instituts in
dem Gebiete des Harzes und des Thüringer Waldes; die Eintragung würde einer
Karte in grösserem Maassstabe bedürfen. Ausserdem fehlen 4 portugiesische Azimuth-
stationen, deren genäherte geographische Coordinaten nicht zu erhalten waren, nämlich:

Montijunto, Serves, Souzà, S. Torcato.

Was die Positionen selbst in der Karte anlangt, so war es wieder oft geboten,
von den thatsächlichen geographischen Verhältnissen etwas abzuweichen, doch wird, wie
schon oben erwähnt, daraus kein wesentlicher Nachtheil für die Uebersicht der Verhält-
nisse erwachsen. Ein Blick auf die Karte zeigt sofort, dass das Material von Seiten
der Astronomen als herbeigeschafft angesehen werden kann; es sind nur geringe und

 

 

Med blak ul

vr TON IT a He Bike hak

Lae iki Wi

ii en

just ph bb a ha nd ua pus
