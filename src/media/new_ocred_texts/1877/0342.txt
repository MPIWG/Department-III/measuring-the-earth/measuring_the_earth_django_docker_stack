 

rt
fl
i
fF
I
;

 

16 MOUVEMENT SIMULTANE

vant 4 Vobservation des oscillations qu’a la distance insuffisante de 2™,5
4 3 métres, et que d’autre part il aurait fallu renoncer à la détermination
du mouvement du support; cette détermination exige en effet Pinstalla-
tion d’une lunette dans le plan d’oscillation, ce qui aurait été absolument
impossible vu la position occupée par le grand comparateur.

Je me suis donc trouvé dans l'alternative, ou bien de renoncer com-
plétement à faire les observations du pendule à Berlin, ou bien, si Je
persistais à les faire malgré une installation de l'instrument reconnue
d'avance comme défectueuse, de déterminer le mouvement du support
dans des conditions identiques à celles dans lesquelles il se présente
lorsque l’on observe la durée des oscillations.

La pierre triangulaire a été posée, de travers, pour ainsi dire, sur le
pilier, le côté du triangle parallèle au plan doscillation n'étant pas paral-
lèle à la face correspondante du pilier, mais faisant avec elle un angle de
25° environ; la perpendiculaire au plan (oscillation, menée du centre
du pendule, passait alors par Pembrasure de la porte de communication
avec la pièce à côté, en sorte que la lunette pouvait être placée, à une
distance de 4%,5 du pendule, sur un trépied reposant sur une dalle en
pierre formant le seuil de la porte et scellée sur le mur mitoyen entre les
deux pièces. D’après cette position oblique, la direction du plan d’oscilla-
lion ne passait plus sur le comparateur, mais à côté, et il était possible
de placer à une distance de 6 mètres du pendule la lunette servant a
l’observalion du mouvement du support; mais d'un autre côté, les trois
points d'appui du trépied se trouvaient dans une posilion trés-peu symé-
(rique relativement aux faces du pilier. Des deux points d'appui qui sont
sur une ligne parallèle au plan @oseillation, Yun débordait beaucoup
plus que Pautre en dehors du pilier, la ligne joignant en projection ho-
rizontale Le centre du pendule, et l'un des points d'appui était presque
perpendiculaire à Ja face correspondante du pilier, ce point se trouvant
de 14m en dehors. L'autre point d'appui était fort peu en dehors du pro-
longement de la face opposée du pilier, dont 11 n’était distant que de 9
centimètres environ.

 

x
=
=
>
=
a
=
=
x
=
x

pe ial

Ihn nik

 

|
|
