 

F

FR TER? DEMO ETUI

nn

Fir

Why ahve ke kik MAAR AY AR
ETRE URE BETTE

PREMIERE SEANCE.

Sturreart, au Kénigsbau, le 27 septembre 1877.

Sont présents: S. E. M. de Mitinacht, President du Conseil des Ministres et
Ministre des Affaires Étrangères, M. le major-général de Wundt, chef du département
de la Guerre; les délégués : MM. Adan, Baeyer, Faye, Fearnley, Ferrero, Haffner, Herr,
Hirsch, Hügel, Ibanez, Mayo, Nagel, d'Oppolzer, Perrier, Peters, Plantamour, Sadebeck,
Schoder, Seidel, Siegfried, Zech; comme invités: MM. de Frisch, membre du Conseil des
études, M. le directeur de Sülcher, M. de Leins, le directeur de Riecke, le professeur
Reusch, le professeur Gross, le professeur Wieland.

La séance est ouverte & 2 heures 10 minutes par M. le Général Ibanez, Pré-
sident de la Commission permanente.

S. E. M. de Mitinacht adresse à l’Assemblée les paroles suivantes:

Sa Majesté le Roi de Württemberg m'a confié l’honorable mission de souhaiter
la bienvenue à la Conférence et d’être son interprète pour exprimer tout l'intérêt qu’elle
porte aux travaux de l'Association géodésique. Le Gouvernement Württembergeoïis,
qui apprécie également l’importance de ces travaux, salue avec plaisir la présence de
la Conference dans la ville de Stuttgart, et lui souhaite le meilleur succès pour ses
délibérations et pour la continuation de son œuvre; il espère que les membres de |
l'Association se plairont pendant leur court séjour dans le pays Suabe, qui les ac-
cueille cordialement.

M. Ibanez remercie le Gouvernement du Roi de l’accueil gracieux qu'il accorde
à la Conférence, et remercie en particulier M. le Ministre des paroles bienveillantes
qu'il vient de prononcer.

Conformément aux statuts, M. Jbanez prie les Secrétaires de donner connais-
sance du Règlement proposé par la Commission permanente. — M. Bruhns en donne
lecture en allemand, M. Hirsch en français. Ce règlement est adopté à l'unanimité.

 
