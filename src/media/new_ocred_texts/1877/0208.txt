 

EEE RS SEE NE

 

Una osservazione potrebbe essere fatta sulla molteplicita dei mareografi presso
Venezia. Conviene perd riflettere che alla questione della determinazione del livello
medio dei mari molte altre si collegano di altissimo interesse idrologico, geologico e
sismico; e che niun luogo pud forse trovarsi più acconcio a consimili studi di quello sia
il Veneto estuario, nel quale la marea si propaga sopra immense superficie e sopra
innumerevoli canali di svariatissime dimensioni, e rivolti in tutti i sensi, in tutte le
direzioni. Nulla quindi più opportuno poteva essere offerto alla scienza di queste mol-
teplici stazioni mareografiche, il cui numero, anziche diminuito, & desiderabile che venga
aumentato, moltiplicandole lungo gli estremi confini interni della laguna e lungo i numerosi
canali che vi fanno capo. Al che, se una difficolta si oppone, altra certamente non e se non
che quello che deriva dalla spesa occorrente per Vacquisto dei meccanismi registratori,
per l’attivazione delle relative stazioni e per la loro custodia, vigilanza ed esercizio.

Ma queste difficoltà a me sembra che siano, se non del tutto rimosse, per lo
meno attenuate di molto da un ingegnoso meccanismo inventato e messo in opera in
pit punti dei canali veneti dal geometra signor Domenico Dal-Pero; meccanismo che ho
avuto aggio di esaminare in occasione della generale perturbazione che teste ho fatta
nella qualita dispettore del regio Genio civile all’estuario veneto, ai canali lagunari, ed
a quelli di Friulana e Trevigiana navigazione.

Credo che non sara fuor di luogo una descrizione sebbene succinta di questo
meccanismo, siccome quello che ha stretta attinenza colla materia di che tratto, e pud
a mio giudizio in molti casi sostituire convenientemente gli ordinari mareografi.

Dird pertanto qualmente il signor Dal-Pero, nella qualita di agente rurale della
Compagnia delle assicurazioni generali di Venezia e Trieste (la quale possiede numerose
ed estese proprietä nella laguna), fin dall’anno 1870 si prefiggesse lo studio della pro-
pagazione del flusso e reflusso delle acque lungo i canali dell’estuario, allo scopo di
vedere se fosse possibile di utilizzare come forza motrice il regurgito prodotto dalle
alte marèe nel fiume Livenza.

Non potendo fare assegnamento sopra osservazioni saltuarie, nè potendo ragio-
nevolmente proporre la spesa di acquisto e di esercizio di mareografi ordinari, si studio
di comporre uno strumento registratore il più semplice possibile, nel quale, escluso
qualunque meccanismo di orologeria, ogni movimento occorrente venisse prodotto dal
galleggiante; meccanismo a mezzo del quale, senza disegnare la intera curva del movi-
mento ascendente e discendente dell’acqua, venisse registrata l’altezza massima e la
minima di ciascuna oscillazione.

Dopo varie prove e modificazioni suggerite dalla esperienza, il congegno adot-
tato definitivamente dal signor Dal-Pero si compone di un galleggiante il quale, col
sistema di trasmissione comune ai mareografi ordinari, fa scorrere una matita che segna
le oscillazioni dell'acqua in proporzione di un decimo del vero sopra una striscia di
carta tesa fra due cilindri. In questa carta @ gia marcata la linea di riferimento, o
meglio ancora ve la traccia contemporaneamente una matita fissa, evitando cosi gli er-
rori che proverrebbero da qualche inesattezza nello svolgersi della carta stessa. Eviden-
temente, se i cilindri restassero sempre fermi, la matita descriverebbe altrettante linee

 

=x

We A

I

til

Lak nl Il

lb tom

mea hd a MAL dan ad put si

 
