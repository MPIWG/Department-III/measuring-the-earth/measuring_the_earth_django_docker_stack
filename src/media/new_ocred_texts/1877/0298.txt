 

‘286

¢) Publications.

Les mémoires de MM. Lœwy, Perrier et Stephan sur la mesure des longitudes
Paris—Marseille —Alger, vont paraître prochainement, ainsi que le mémoire de MM. Perrier
et Bassot sur les longitudes Alger—Bone—Nemours et sur l'amplitude astronomique de

l'arc du parallèle algérien.
Calculs.

Le calcul de la latitude du Puy-de-Dôme observée en 1876 a donné
L — 450 46' 27:5.

En calculant cet élément par la nouvelle chaîne de triangles comprise entre
Carcassonne et le Puy de Dôme, la latitude de Carcassonne étant prise comme coor-
donnée de départ, nous avons trouvé

L=45° 46’ 21.5.

Il y a done un écart de 6:0 d’arc entre la latitude astronomique et la lati-
tude géodésique.

Brousseaud avait trouvé à Opmes, au sud-est et à dix kilomètres environ du
Puy de Dôme, un écart de même signe voisin de 9° — 8797.

Enfin Delambre à Evaux, à 50 kilomètres au nord-ouest du Puy de Dôme, avait
trouvé un écart analogue, toujours de même signe, qui s'élevait à 6:86. La région du
Puy de Dôme, qui appartient au plateau central de la France, semble donc être le lieu
d’une déviation assez forte de la verticale. Pour éclaircir ce point important, nous nous
proposons de vérifier la latitude d'Evaux, qu'on a considérée jusqu'ici comme fautive,
et qu’on a peut-être eu le tort d’exclure de tous les calculs relatifs à la forme de la terre.

Nous terminerons notre rapport en annonçant à l'Association que le Dépôt de
la guerre de France a commandé à MM. Brunner frères un appareil des bases semblable
à celui qui a été déjà commandé par l’Institut géodésique de Berlin; la règle a été
fabriquée chez M. Mathey, sous le contrôle de M. Deville, et est formée d’un métal
identique à celui de la règle prototype du Bureau international.

Dès que cet appareil nous sera livré, nous ferons les préparatifs nécessaires
pour procéder à une nouvelle mesure de la base de Melun, opération dont l'importance

scientifique n’échappera à aucun de nos collègues.
Perrier.

RER EE Ur :
ie as pA ALL Là 1 BA in A a 2 wi nik ann aan +6 D + CCC

 

=

1 et Aaa I A a Ad ek

ak ml

 

ct aa CLE
