 

    

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

34
| Reduction | | | Reduction | |
Declination | Vierteljahrs- | Wahrsch. Declination | Safford | Wahrsch. | |
schritt | | Anzahl. | en | Anzahl.
1875.0 | aut ntiebler. | 1875.0 | auf r.tiehler. |
(Gradmessung. | Gradmessung. |
: 7 ie | | | |
TA 0154. 1 te Ol ro 6 1922 do: ly 0122 #2 0172 2 |
eh) oe + 0.38 ee 0.00 6 E39 201. —0.21.. ae OOF 8
rd : 0.42 sort 5 +43 6 — 0.07 a ©. Lan] 7
48.6 + 0,34 2 0.07 10 + 48 17 -Er0.070. |: 2 0,0 19
+72 52 -- 0,21 oo 8 52 9 + 0.19 OC. Are
Limites 102.29 2 0.08 8 + 57 41 1 0,10. | ec: one | 8
+ 22 13 + 0.37 ae 0.23 6 | |
27 36 + 0.46 10: 10 8 2a à
aaa 28 + 0.21 A 0,08 7 mu rl...
aoe 22 7 © 27 on 9 Reduction |
a ns on as | Declination | Safford Wahrsch. |
Do D 5 od | E otre | 13 Hé au | Anzahl.
52 23 + 0.58 220.08 | 1875.0 Vierteljahrs- kebler: ;|
7 44 + 0.65 se 0.10 12 Sohn
ie 5 + 0:32 =. 0.12 7 | a ee à al |]
+ 66 47 + 0.19 3.0182 2 i | |
5 + 0.55 + 0.08 3 320% 200 = 6106, + 0.06 13 |
+ 76 32 — 0.35 a OL 7 2 + 38 9 0.20 | 2 0.07 19
+ 82 14 + 0.22 — | I + 42 26 car 4510.09 19
+ 88 4 — 0.19 ab 0473 3 +47 13 = 0.27 Æ 0:07 13
| gr 59 | — 0.44 20.06. 8
ARE te) ae gy 27 — 0.23 = Oo 04120
\

Für die weitere Verwerthung dieser Reductionsgrössen erscheint es zweckmässig,
die Gebiete der Declinationen von — 10° bis +- 30°, innerhalb welcher Grenzen nur
Positionen des Vierteljahrsschrifts- und des Gradmessungscataloges vorkommen, und
+ 30° bis + 60°, innerhalb welcher Gränzen Positionen aller drei Cataloge enthalten
sind, von einander getrennt zu behandeln, von einer Verwerthung der Reductionsgrössen
innerhalb des Gebietes der Declinationen von + 60° bis + 90° aber ganz abzusehen,
einerseits weil hierfür ein Bedürfniss nicht vorliegt, andererseits weil die Zahl der in
diesem Gebiete vorhandenen Reductionen zu gering ist.

Was zunächst die Zone von — 10° bis + 309 betrifft, wird man für die Re-
duction des Vierteljahrsschrifts- auf den Gradmessungscatalog unbedenklich das arith-
metische Mittel aus den in der obigen Tabelle enthaltenen Einzelmitteln der Reductionen:
+ 0:38 in Anwendung bringen können, da die übrig bleibenden Abweichungen: — 0704,
0:00, + 0:04, — 0:04, — 0:17, + 0:11, — 0:01 und -+ 0:08 keine Periodieität zeigen
und innerhalb der wahrscheinlichen Fehler liegen.

Hingegen ist in der anderen Zone, welche, dem Bedürfnisse entsprechend und
weil für die Reduction Safford auf Gradmessung innerhalb der Declinationen + 30° bis
+ 35° nur 2 Sterne vorhanden sind, auf die Gränzen der Declination von + 35° bis
+ 60° einzuschränken ist, eine Periodieität der Werthe nicht zu verkennen. Es wurde
daher das Verfahren eingeschlagen, von den Mittelwerthen in den obigen Tabellen aus-
gehend eine graphische Ausgleichung vorzunehmen, welche bezüglich der Reductionen
für die einzelnen Grade der Declination zu folgenden Ergebnissen geführt hat:

vr a a a 2 wid ul

ak Wim

Ph tho ed

{ks nh Aas Illa ddd edd dl aca ye 11:

2. ettiastaues, mama Hd |

 
