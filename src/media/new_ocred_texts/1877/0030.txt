Be ASIEN

a a OE
Y

 

3

18

auf drei Jahre vom 1. November ab zum Mitgliede des Beiraths ernannt und endlich
unter dem 12. November die Ernennung der nachstehend genannten von der Königlichen
Akademie der Wissenschaften in den Beirath gewählten fünf Mitglieder vom 1. November
ab auf drei Jahre vollzogen. Es sind die Herren:

&

1. Professor Dr. Peters in Kiel,

2. i ,, Helmert in Aachen,
3. 5 „. Auwers in Berlin,
4, s „ Kronecker in Berlin,

5. Dr. Werner Siemens in Berlin.

Der Beirath besteht demnach unter dem Vorsitze des Präsidenten des geodäti-
schen Instituts aus den genannten sechs stimmberechtigten Mitgliedern und den vier
Sections-Chefs, denen aber nur berathende Stimme zusteht.

Die Beschlüsse dieses Collegiums werden künftig in allen die preussischen
Gradmessungs-Arbeiten betreffenden Fragen bündig sein. Bei den internationalen Fragen,
über welche die permanente Commission endgiltig zu entscheiden hat, werde ich bei
Formirung meiner Anträge die Ansichten jenes Collegiums einholen und zu Grunde legen.
Dadurch wird zwar meine bisherige Autonomie beschränkt, aber ich werde dagegen auch
auf dem weiten Gebiete der Gradmessungs-Arbeiten, das ich allein nicht mehr (und
vielleicht Niemand) vollständig übersehen kann, gegen Irrthümer sicher gestellt. Die
Verantwortung, die bisher auf mir persönlich lastete, wird jetzt von dem Collegium mit
mir getheilt. Baeyer.

Präsident Zech dankt im Namen der Versammlung dem Centralbureau und ins-
besondere dem Präsidenten desselben, Herrn General Daeyer. Der nächste Gegenstand der
Tagesordnung ist die Berichterstattung der Bevollmächtigten der einzelnen Länder in
alphabetischer Reihenfolge derselben.

Nach einer kurzen Pause theilt Präsident Zech mit, dass verschiedene Schreiben
von Mitgliedern der Conferenz eingegangen sind, worin sich dieselben entschuldigen
lassen. Es sind dies namentlich die Herren von Forsch in St. Petersburg, de Silva in
Lissabon, Liagre in Brüssel und Zinter in Wien.

Zugleich. hat er die Ehre, im Namen der Königlichen Regierung die Herren
Conferenzmitglieder zu einer Excursion am Sonntag den 30. September nach Urach
einzuladen und im Auftrage des Hofmarschalls Karten zur Tribüne des morgenden Volks-
festes in Cannstadt und der damit verbundenen landwirthschaftlichen Ausstellung, und
vom Oberhofmeisteramte für jeden Tag der Dauer der Conferenz eine Anzahl Billets zum
Königlichen Theater zur Verfügung zu stellen. Zum Besuche der Staatssammlungen,
des Naturalienkabinets, der Bibliothek, der Alterthumssammlung und der Kunstgebäude
hat er im höheren Auftrage einzuladen; endlich sind im Namen des Herrn Oberbürger-
meisters die Herren Commissare zum freien Besuche des Stadtgartens, sowie von dem
Vorstand des Museums zum Besuche der Lesezimmer berechtigt.

a TA ai] u vi 1 là

DL He, 1

OC LOT UN il à

IE toit

à tm yA add dl pus pue à:

 
