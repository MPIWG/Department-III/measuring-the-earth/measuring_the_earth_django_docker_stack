 

56

Die Gesammtzahl der in den zur Europäischen Gradmessung gehörenden Staaten
gemessenen Grundlinien beträgt 74; sie dienen als Ausgangs-Elemente oder. zur Veri-
ficirung bei der Berechnung der verschiedenen geodätischen Netze. Zu den ältesten
Grundlinien gehören die französischen, bayerischen und zwei russische von Tenner, welche
Ende des vorigen und Anfang dieses Jahrhunderts gemessen worden sind und sich von
den neueren durch ihre grössere Länge, ausserdem aber auch dadurch unterscheiden,
dass sie nur einmal gemessen worden sind. Die neuen Grundlinien überschreiten nur
in wenigen Fällen die Länge von 4—5 Kilometern; ausgenommen hiervon sind die algeri-
schen, welche eine durchschnittliche Länge von 10 Kilom. haben, und die spanische von
Madridejos, welehe nahezu 15 Kilom. lang ist. Sie sind grösstentheils zweimal gemessen
worden, mit Ausnahme der nur einmal gemessenen algerischen und der meisten russi-
schen. Von den letzteren ist die Basis von Tornda zweimal gemessen worden.

Die Apparate, welche zu den Basismessungen gedient haben, lassen sich auf 4
Grundformen zurückführen: auf die Apparate von Borda, Bessel, Strwe und den spani-
schen (von Bruner).

Was die Länge der Messstangen anbetrifft, so sind verschiedene Einheiten zu
Grunde gelegt: die Toise von Peru (Urmaass), verschiedene Meter, die Toisen von Bessel,
"Strwe und Spano, die Normaltoise von Wien, deren relative Längen nicht einer allge-
meinen Vergleichung nach identischer Methode unterzogen worden sind, woraus hervor-
geht, dass die europäischen Grundlinien nicht scharf mit einander verglichen werden
können. Nur die Herstellung eines internationalen geodätischen Maassstabes, welche in
nächster Zeit bevorsteht, wird diese Lücke auszufüllen vermögen und gestatten, alle
europäischen Längen auf ein und dieselbe Einheit zu beziehen. Das Verfahren, das bei
dem gegenwärtigen Stande der Frage als das zweckmässigste und sicherste erscheint,
um die Seiten zweier benachbarten Triangulationen unter einander zu vergleichen,
besteht darin, in dem Grenzgebiete ein und dieselbe Grundlinie mit den Apparaten und
von den Geodäten der beiden dabei betheiligten Länder unabhängig von einander messen
zu lassen. Diese doppelte, von den beiderseitigen Beobachtern zweimal ausgeführte
Messung wird den relativen Grad der Genauigkeit beider auf demselben Terrain aus-
geführten Messungsweisen erkennen lassen und die Gleichung der Messstangen des einen
Apparates in Bezug auf den anderen liefern, wodurch man im Stande sein wird, die
Längen der gemeinschaftlichen Seiten in identischen Einheiten auszudrücken.

Zwei derartige Vergleichungen sind in den Jahren 1872 und 1874 zwischen den
österreichischen und Bessel’schen Massstangen durch eine theilweise Nachmessung der
Grundlinie von Grossenhain und zwischen den österreichischen und italienischen durch
die vollständige Messung der Basis von Udine ausgeführt worden. Sie haben die zu-
verlässigsten Resultate geliefert und können nicht genug empfohlen werden.

Welches Apparates man sich auch bedient haben mag, so genügt es, einen Blick
auf das vorhergehende Tableau zu werfen, um sich von dem hohen Grade von Genauig-
keit bei sämmtlichen Basismessungen zu überzeugen. Bei den französischen Grundlinien,
welche die ältesten sind, überschreitet der bei einer Grundlinie von 12 Kilom. Länge

Wd tid ul

wt ieee em A A dea hk

al ml il

Lt toit

jh he Ma data td ut pus 11:

2: ssæthttsains mais NA |

 
