 

|
1

:

ML eee

I

11

ih

Le WP

et

re

ET un nn nn nn nn nn nn nn

DESIGNATION
DE L'APPAREIL DES BASES

Appareil de Repsold,
construit pour la trian-
gulation de l’île
Java.

|
| RÈGLES QUI ONT SERVI
|

pour l'étalonnage

Metre normal de Rep-

de |

|
|
|
|
|
|
|

chacune de 4 toises de |

longueur, reliées en-
semble de telle sorte
que vues de face elles
présentent la forme =,
l’une d'elles ayant sa

lus grande extension |
oO

en direction horizon-
tale et l’autre en di-

rection verticale. Les |

 

règles sont munies à

leurs deux extrémités
de plaques en fer et
ont en outre à leur
extrémité postérieure
de petites languettes
divisées par un vernier

en dixièmes de ligne.

Cet appareil offre cer- |
ressemblances |
avec celui qu’aemployé |

taines
le général Roy.
Appareil du baron Wre-

de, analogue a l’appa-
reil de Struve.

 

sold & la température |
Fahrenheit, |

de G54
comparé avec le mètre
rapporté de Paris par
van Swinden.

3 règles en bois de pin, Une toise déposée au

Musée de Darmstadt,
qui à été construite
par Lenoir 4 Paris et
comparée à la toise
du Pérou par Bouvard.

OBSERVATIONS.

|
|
|
Fa

| Base mesurée deux fois; la’ différence des deux me-
sures est de 5™™0, |
L’appareil de Repsold est décrit dans les ,,Astro-
nomische Nachrichten‘ N° 1661.
Metre de Repsold & 65° Fahrenheit — mötre de
Swinden & 0° + O™™175.

J’ai publié sur la mesure de la base un mémoire en
langue frangaise dont j’ai communiqué une copie,
accompagnée de tous les dessins et tabelles, & la
seconde Conférence générale de l'Association géo-
désique en 1867. (Comptes-rendus, pag. 41.)

J'ai aussi exécuté un calcul de compensation

 

du réseau de triangles qui relie les observatoires
de Gôttingue, du Seeberg, de Darmstadt, Mann-
heim, Spire et Strassbourg, en suivant la méthode
des moindres carrés et en me servant des formules
données par Schleiermacher dans le „Jahrbuch“
de 1834.

Ce calcul a donné pour la base de Darmstadt

une longueur de. 3976158
qui comparée avec 3976.087
donne une difference — 0.071
ou En de la longueur.

Darmstadt, 5 décembre 1877.

Dr. Hügel,

Délégué du Grand-Duché de Hesse-Darmstadt
à l'Association géodésique internationale.

La base d’Axevalla seule a été mesurée deux fois;
la difference des deux mesures = 0/75.

 

 
