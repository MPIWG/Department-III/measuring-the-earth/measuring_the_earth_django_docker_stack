M Tree or ern TH |

LM Gi dl Hu

Wi AR

1 ph lui

WTP | ea A ae LL LUF ON)

249

 

Durch den Ausdruck für s,, nämlich:

sin A, em As sora 0.0... sin An
sini. sin’ Bi, Sim. Asch, oes sin Bn

 

MSN

erhält man die strenge zu erfüllende Gleichung :

C= cot A, dA, — cot B, dB, + cot À, dA, — cot B, dB,
a + cot A, dA, — cot B, dB,,

oder einfacher, indem man mit d die Differenz der beiden Logarithmen für GH be-

zeichnet, und statt eotL A,, cot B;, cot A,, cop Ba |. cot A,, cot B, die ent-
sprechenden, schon bei den Ausgleichungen der Gruppen benutzten logarithmischen
Sinus-Diflerénzen @,, 6,, 4,, 0, à. Ins On einführt:

dad u Bad 5 ua (1)

Da sämmtliche Dreiecke schon ausgeglichen sind, müssen die in Secunden aus-
gedrückten Winkeldifferentiale noch folgende Gleichungen erfüllen :

0 — dA, + dB; + d0,
a= GA, ab, al eae. (2)

nee ere ere rest ele tete

Die strenge Lösung nach der Methode der kleinsten Quadrate giebt hier zur
Bestimmung der Winkeldifferentiale folgende, für alle Dreiecke geltenden , all-
gemeinen Formeln:

 

a FEN; B=— Lk; = — 1: h wa à

wo % eine Constante bezeichnet und noch vorausgesetzt wird, dass den Winkeln in dem
Lin, 2... ni Dreiecke,verschjedene, resp. mit 9,,9.,9, .... 2„ angegebenen Gewichte
beigelegt sind. Substituirt man schliesslich die gefundenen Werthe von dA, dB und dC
in (1), erhält man die Constante % durch die Gleichung:

a ee te (4)

Aus den Formeln (3) geht hervor, dass die Veränderungen hauptsächlich nur
bei den Winkeln A und B eintreten, indem die dG für gut geformte Dreiecke nur sehr
klein werden und sogar überall, wo A=B, vollständig verschwinden. Zur Vereinfachung
der Aufgabe kann es daher wohl erlaubt sein festzusetzen, dass die Winkel C bei der
Ausgleichung als unabänderliche betrachtet werden sollen. Man erreicht dadurch den
grossen Vortheil, die oben sub 2 gestellte Forderung vollständig zu erfüllen, indem nun
die Azimuthe für sämmtliche mit s bezeichneten Seiten gar nicht, und für die übrigen

nur ganz unmerklich geändert werden.
32

 

ut

ES

a nu a nn

 
