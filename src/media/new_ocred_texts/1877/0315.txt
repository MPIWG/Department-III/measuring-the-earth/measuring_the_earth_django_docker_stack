200 ONE mn

UT m Li EM EU QU

Nah be JUIL

gu

en

de premier ordre visibles de la station, en employant les héliotropes dans les cas de
visibilité difficile. Les distances zénithales de chaque point sont le résultat de 15 groupes
d'observations croisées.
Lors des observations des distances zénithales on prit note, à chaque station,
des indications du baromètre et du thermomètre. |

Nivellements spéciaux.

Le niveau moyen de l’océan ayant été déterminé avec assez de rigueur en face
de la petite ville de Caminha, on a procédé & un nivellement trigonométrique de pré-
cision depuis cette ville jusqu’au signal de premier ordre S. Pato, dans l’extrême nord
du Portugal, en suivant deux trajects indépendants pour obtenir la vérification néces-
saire. On trouva une concordance trés satisfaisante entre les deux résultats, les deux
trajects ayant donné seulement une différence de 07023 pour l’altitude du dit signal.

On détermina aussi par un nivellement trigonométrique spécial l'altitude du
signal de premier ordre $. Felix relativement au niveau moyen de la mer obtenu par
les marémètres établis sur la plage de Villa do Conde. Le but de ce travail est de con-
firmer les résultats obtenus déjà par des nivellements géométriques au moyen du procédé
trigonométrique des „stations intermédiaires‘, et de déduire les conditions auxquelles
doit satisfaire l'emploi de celui-ci, qui est extrêmement commode dans les pays monta-
gneux comme le nôtre, pour déterminer, avec une grande précision, les altitudes de
quelques points de premier ordre plus importants. Les calculs relatifs à ce travail n’ont
pas encore été faits, parce que les observations manquent encore sur l’un des sommets
de triangulation spéciale.

Marémètres el marégraphes.

Les échelles de marées ou les marémètres fonctionnent actuellement à la barre
de Villa do Conde (dans le nord du pays) et à l'embouchure du Guadiana (dans le sud).
Les diverses observations sont accompagnées des indications météreologiques nécessaires
pour le calcul définitif du niveau moyen. On a fait l’acquisition d’un excellent maré-
graphe automatique de M. Borrel, qui, sous peu, sera placé à l'embouchure du Tage,
tout proche de S. Juliäo da Barra; et deux autres, que nous possédions déjà, seront
aussi mis bientôt à fonctionner.

Travaux divers.
On a fait les études concernant l’effectuation des observations astronomiques

relatives aux travaux de haute géodésie. On a introduit quelques modifications dans
l'instrument employé aux nivellements géométriques en vue d'en rendre le maniement

  

Fe rg ak

ner hie en.

  

Se
