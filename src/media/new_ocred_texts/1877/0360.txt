 

34 MOUVEMENT SIMULTANE

3° Expériences faites à Berlin, le pendule étant placé sur le pilier en mollasse,
surmonté de l’assise triangulaire, le comparateur étant en place.

 

 

 

 

 

 
 

 

 

 

 

oY | a | Q Equations de condition ce € |
28 octobre, de 11» 1/, 42% 1/.. a = 3™™,308 D=11",885 grossissement 3596. |
Moyenne de deux observations pour chaque espace, faîtes l'une en parcourant l'échelle dans un sens,
l’autre dans le sens opposé. |
No Ue Us p. ue D

2 4,672 1,917 k — 0,506 k’ = 2,363 2,376 + 0,013

3 3.931 1,687 k — 0,593 k’ = 2,330 2,216 — 0,054

7 3,270 1,4825| k — 0,6745 k’ = 2,206 2,183 — 0,023

4 3,059 1,403 k - 0,138 k = 2,180 2,139 — 5041

3 2,748 1,315 k — 01005 kK — 2,090 2,085 — 0,005

10 2.1755 (ow! k - 0,866 Kk > 1,884 1,964 — 0,080
11 1,909 1,0585| k — 0,945 k’ = 1,804 1,874 0,090, |
8 1,622 0,944 KR = 1089 k = fide 1,743 + 0,025 |

12 1,3515 0,844 k 1185 -k7—"1,601 1,600 0008
13 1,071 0,742 k 1,318 ko 1,113 1,413 9250 |
14 0,853 0,668 k 413091 K - 1,2716 1,243 — 0,033 |

équations finales 11 k —10,147 k' —20,895 écart moyen + 0,034

/ 2
40,47 k 110,3735 K—-18,1165 +ı/ Le —+ 0,046

u Ë 9
d'où k —2,954 poids 1,074 erreur moyenne = 0,044

lé 148 >» 1,013 » =20046 (0). 0,387.
L'expérience statique avait donné k —32:,257 par l'action d'un poids de 14118 suspendu pendant 408

 

|
29 octobre, de 7 à 10 soir. a —3mn 199 D — 11",885 grossissement 3715 |
Moyenne de deux observations pour chaque espace, faîtes l’une en parcourant l'échelle dans un sens,
l’autre dans le sens opposé. |

 

 

 

 

N u pr Pe be pe |
2 | 4,592 1,896 | k — 0,5275 k’ — 2,383 2,429 20,088 |
3 | 3,8225 1,6155| k — 0,619 k’ == 2,366 | 2,345 | — 0,021 |
7 | 3,166 1,404 | k — 0,714 k’ = 2,260 | 2,256 | — 0,004
4 | 2,961 1,320 | k — 0,1575 k' — 2,243 9,215 "0,098
9 | 2,6605 1,293 | k — 0,818 k’ = 2,175 | 2,159 | — 0,016
10 | 2,106 1,0455| k — 0,9565 k’ = 2,014 2,030 +. 0,016
11 1,848 0,951 k — 1,0515 k’ = 1,9435 1,9415 — 0,002
8 | 1,570 085551 k — 1,169 k’ == 1,888 1,832 — 0,003

12 1,308 0,1725| k — 1,2945 k — 1,6935 1,7155 -L 0,022

13 | 1,036 0.684 | k — 1468 k — 41,502 1,55% — 0,032

14 0,825 0,595 K — 1,681. k — 1,38 1,357 — 0,020

équations finales 11 k —11,0565 k° —21,824 écart moyen + 0,020

—11,0565 k 12,457 k'—-20,687 7 Se —+ 0,026
u 9

u We |
d'où k —92,919 poids 1,186 erreur moyenne + 0,024. |
ae k’ =0,0205 > 1,343 » ae 0022 > =20,318.
L'expérience statique avait domé k == 32,140 par l'action d'un poids de 4118 suspendu pendant 40°.

   

 

x
=
=
&
a
=
ü
=
=
x

111 he 1.

Mk ini

am!

It ut ane hs vs ed aa m 1

PEESEFPEPTBERGEFEREEENN N

 
