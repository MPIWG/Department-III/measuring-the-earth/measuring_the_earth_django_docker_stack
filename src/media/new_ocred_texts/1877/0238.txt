 

 

 

 

226

Si l’on n’admet pas le principe d'introduire, dans la compensation d’un réseau,
une équation de condition destinée à établir l'accord entre les deux valeurs obtenues
pour la base d'arrivée, il conviendra de calculer le réseau deux fois, en partant succes-
sivement de chacune des deux bases mesurées. Dans ce cas, chaque côté du réseau
sera considéré comme commun à deux triangulations, et l’on adoptera la moyenne de
ces deux valeurs en ayant égard à leurs poids. La théorie montre que, pour les deux
triangles extrêmes, il n’y a pas de correction à faire à la longueur de leurs côtés cal-
culés en fonction de la base-adjacente. Quant aux triangles intermédiaires, ils subissent

des corrections graduelles, qui vont en augmentant à mesure que l’on approche de la
zône située à égale distance des deux bases.
Sa

La compensation d’un réseau géodésique est naturellement renfermée entre
certaines limites imposées par la longueur des calculs. Personne ne proposera, par
exemple, de compenser en un bloc tout le réseau européen, ce qui serait d’ailleurs un
abus évident au point de vue théorique. En effet, l’ellipsoïde osculateur y varie en sens
divers. C’est même cette variation qui semble fournir la seule règle théorique à suivre
pour le fractionnement des triangulations. Chaque réseau partiel, destiné à être com-
pensé isolément, devrait s'étendre sur toute la portion de la surface terrestre pour
laquelle on se croit en droit d'adopter un seul et même ellipsoïde osculateur.“‘

Il est vrai que la subdivision de la surface terrestre, d’après ce principe, n’est
pas encore suffisamment connue; mais un calcul provisoire permettrait, dans beaucoup
de cas, de la déterminer approximativement.

Mais il arriverait malheureusement que les réseaux déterminés d’apres ce prin-
cipe seraient trés inégaux; plusieurs dépasseraient la limite au dela de laquelle les
opérations de calcul deviennent pratiquement inexécutables. En outre, lorsque le réseau
à compenser emprunterait le territoire de plusieurs pays, on se heurterait à la difficulté
de déterminer les poids relatifs des résultats. Enfin il est probable que l’on rencon-
trerait des zônes ne renfermant aucune base mesurée directement.

Dans le procédé actuel de compensation par pays, il semble, si l’on s’en rap-
porte à l’objet de la compensation, que le fractionnement du réseau national devrait
satisfaire aux conditions suivantes :

1) L’étendue de chaque groupe doit être la plus grande possible, en restant
dans les limites théoriques et pratiques dont il a été question précédemment.

2) Pour former un groupe, on choisira une chaîne composée des meilleurs
triangles, et dirigée autant que possible suivant un méridien ou un parallèle.

3) Aux extrémités du côté commun à deux groupes on fera des observations
astonomiques de latitude et d’azimut. J. Liagre.

 

=

LA IL UCI CIUIT ENT

Le Tiki

lb Iran

ju ed Len a Mat A tan ad panda puni 5:

 
