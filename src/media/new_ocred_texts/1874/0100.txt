Tr nn ee oe

Dr PT ST SPP IT TTT TY

PRET

Poppers rience

5
R
x
8
ë
k
t
È
=
R
5

Anhang I.

ASTRONOMIE. De la détermination des déclinaisons des étoiles
fondamentales

par M. Yvon Villarceau.

Les grandes discordances des divers catalogues des déclinaisons des étoiles fon-
damentales montrent l'insuffisance des méthodes employées pour leur détermination. Les
progrès de l’astronomie et ceux de nos connaissances sur la figure de la terre exigent
cependant que les déclinaisons des étoiles acquièrent un haut degré de certitude.

Dans une note communiquée au Bureau des Longitudes (séance du 14 février
1872) et plus récemment dans un mémoire présenté le 18 mai de la même année à
la Commission d'inspection de l'Observatoire de Paris, nous avons exprimé lopinion
que la principale source des difficultés du problème réside dans l'application d’une
théorie des réfractions, qui suppose horizontales les couches atmosphériques de même
densité, à des observations faites pour de trop grandes distances zénithales. Une autre
cause d'erreur, qui accompagne toujours la précédente, réside dans la dispersion de
l’image des étoiles, qui ne permet pas de discerner sûrement la région du spectre que lon
devrait pointer. Nous avons cru pouvoir fixer à 30° environ la limite des distances
zénithales qu’on ne saurait dépasser sans les exposer, du côté des réfractions, à des
erreurs dont les effets ne seraient pas suffisamment atténués par l’accroissement du
nombre des observations. (L'Association géodésique internationale a fixé la limite de
20° pour l'observation des latitudes.) La conséquence immédiate de Padoption d’une
pareille limite est la nécessité pour l’observateur de se déplacer à la surface du globe
avec son instrument: nous avons en effet combattu la proposition qui consisterait à
combiner les observations faites dans des observatoires fixes, échelonnés entre les deux
cercles polaires, comme incompatible avec la condition d’une détermination expérimen-
tale des termes du 2™¢ ordre de la flexion, et comme introduisant dans les résultats un
défaut d’homogénéité, source de nouvelles incertitudes. On peut s’assurer que quatre
stations établies, par exemple, dans le voisinage des deux tropiques et des deux cereles

 
