1 TRITT TP

75

j'ai reçu de notre ministre de la guerre la communication que le gouvernement I. BR.
avait décidé de mesurer la base de Radauz et que M. le capitaine d’état-major I. R.
de Sterneck était autorisé à passer la convention pour la jonction de nos réseaux limi-
trophes. D'un commun accord, nous avons choisi pour raccordement le côté Ibanesti-
Arschitza qui sera déterminé par deux triangles. (Comme la base que l’on est en
train de mesurer n’est qu'à dix kilomètres de notre frontière, et comme dans un avenir
prochain il nous serait impossible d’en mesurer une dans notre pays, j'ai décidé de con-
sidérer cette base comme faisant partie intégrante de notre réseau et d’y ajouter les
stations nécessaires à son raccordement. Actuellement, nous faisons les observations
aux deux extrémités, pour la rattacher à nos triangles de premier ordre.

Notre réseau se compose pour cette année de 26 stations dont la position a été
fixée. Les observations n’ont pu être faites qu'en 11 de ces stations, à cause de l’état
de l'atmosphère et du temps employé à la reconnaissance des points, travail très difficile
dans un pays où l’on manque absolument de cartes dignes de confiance, et sur un
terrain très accidenté et très boise. *)

Comme instruments, nous avons deux cercles azimutaux de 0%42 de diamètre,
construits par MM. Brunner frères à Paris.

Nous nous étions proposé de commencer cette année le nivellement de précision,
mais cela nous a été impossible, n’ayant pas encore tous les instruments nécessaires,
J'espère que l’année prochaine nous serons à même de le commencer.

Pour ce qui concerne les travaux astronomiques, nous ne serons pas non plus
en état de les commencer avant l’année prochaine. |

Je regrette, Messieurs, que nous ne soyons pas plus avancés dans nos travaux
géodésiques, mais comme les premières difficultés ont été à peu pres surmontées, j’es-
père que l’année prochaine nous pourrons accuser un résultat plus satisfaisant.

Präs. von Forsch: Da ich selbst über Russland Bericht zu erstatten habe, er-
suche ich Herrn von Bauernfeind das Präsidium zu übernelimen.

Vicepräs. von Bauernfeind: Herr von Forsch hat das Wort.

Herr von Forsch: Seit meinem letzten Berichte an die permanente Commission
im vorigen Jahre sind die Rechnungen der Längengradmessung so weit fortgeschritten,
dass nunmehr die ganze Dreieckskette von Warschau bis Orsk zusammengestellt und
derartig ausgeglichen ist, dass alle durch Polygone, Diagonalen u. s. w. sich darbieten-
den geometrischen Bedingungen streng erfüllt sind; ausserdem sind die complieirten
Verbindungen derselben mit den drei Grundlinien bei Orsk, Busuluk und Wolsk be-
rechnet; an den Verbindungen mit den übrigen Grundlinien wird gegenwärtig noch
gearbeitet.

Seit vorigem Jahre haben wir ausserdem eine Umrechnung unserer alten
Triangulationen unternommen, welche bekanntlich zu sehr verschiedenen Zeiten, je

*) (Ces 26 stations, dont 2 en Russie et 4 en Autriche, occupent une surface comprise entre
les paralleles 47° et 48° 30' (N,) et les meridiens 23° 30‘ et 25° 30° E (de Paris).

10*

 
