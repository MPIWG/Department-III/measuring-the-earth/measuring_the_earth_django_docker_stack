i
i
i

 

See eee

4
i
i
N
N

So eve mn mn INT Sn men Vo armen erg a wege u age

fa
oO

Herr Hügel: Bekanntlich werden: im Grossherzogthum Hessen die für die
Europäische Gradmessung nöthigen geodätischen Arbeiten, also namentlich die Winkel-
messungen auf denjenigen zum rheinischen Netze gehörenden Dreieckspunkten I. Ranges,
welche sich auf grossherzoglich hessischem Territorium befinden, von unserem Central-
bureau ausgeführt, und gerade im laufenden Jahre sind auf den beiden Punkten:
Melicobus und Taufstein die Winkelmessungen von dem Centralbureau vorgenom-
men worden.
| Was aber die nivellitischen Arbeiten im Grossherzogthume Hessen betrifft,
so bin ich zu meinem grossen Bedauern nicht in der Lage, ausser den im General-
berichte von 1873 angegebenen, von weiteren Arbeiten berichten zu können, indem durch
die Erkrankung des einzigen mir zur Disposition stehenden Assistenten in diesem
Jahre keine Feldarbeiten zur Ausführung kommen konnten. Meinen Assistenten befiel
nämlich ein hartnäckiges Augenübel, in dessen Folge derselbe bis vor kurzem
dienstuntauglich war. Abgesehen davon, dass es immer misslich und mit beson-
deren Nachtheilen verbunden ist, mit einem im Beobachtungs- und Rechnungsverfahren
eingeübten Arbeiter zu wechseln, war zu befürchten, dass, wenn dem sehr dienst-
eifrigen Manne die Arbeit plötzlich abgenommen und einem Anderen übertragen worden
wäre, dieses denselben so unangenehm berühren würde, dass eine Verschlimmerung
seines Leidens leicht die Folge hätte sein können. Es wurde daher vorgezogen, in
diesem Jahre von Fortsetzung der nivellitischen Arbeiten ganz abzusehen.

Indessen sind durch die im Grossherzogthume bis jetzt vorgenommenen nivel-
litischen Arbeiten die Anschlüsse an die angrenzenden Staaten vollzogen, und zwar an
Preussen und Baden durch dreifache. Nivellirung der Main- Neckarbahn von Frank-
furt a. M. bis zur badischen Grenze bei Heppenheim, und an Bayern durch Nivel-
lirung der Darmstadt-Aschaffenburger Bahn bis zur bayerischen Grenze ohnweit Aschaf-
fenburg, und sollen im nächsten Jahre unter allen Umständen die Präcisions - Nivelle-

‘ments im Innern des Grossherzogthums mit allem Eifer fortgesetzt werden.

Pris. von Forsch: Ich danke für die Mittheilungen und bitte Herrn de Vecchi

um seinen Bericht.
Herr de Vecchi:
 RELAZIONE

sui lavori eseguiti dalla Commissione italiana.

IP. Lavori geodetici.
a) Rete meridiana dal Capo Passaro al Monte Hum.

E noto alla Associazione internazionale come i layori geodetici nelle provincie
meridionali d’Italia sieno proceduti con alacrita, e come in poco tempo, sieno state
ultimate sul terreno la rete meridiana dal Capo Passaro alla Dalmazia, e la rete
parallela tra l’Albania e l’isola di Ponza.

Della prima di queste reti, tutta osservata e calcolata non rimaneva che da

44088 4

cc ue Mi a us.

 
