Ti nn Né

KT ST |

Tram

wer gern

ser

=
&
ë
5
8
=
R
k
:

si

glaube ich, vollendet, bis auf den Längenanschluss mit Deutschland und Frankreich ;
betreffs des ersteren erwarten wir die Vorschläge unserer deutschen Collegen, welchen
Punkt sie an Zürich anschliessen wollen. Unsern westlichen Nachbarn haben wir die
Doppelverbindung Genf—Lyon und Paris—Neuchätel vorgeschlagen, welche eine vor-
treffliche Controle ermöglichen, da die Differenzen Paris-—Lyon einerseits und Genf—
Neuchätel anderseits schon bestimmt sind. |

Endlich der dritte Theil unserer Arbeiten betrifft das Nivellement. Auch damit
sind wir etwa bis zu ?/; unserer Aufgabe gediehen. Wir haben bis jetzt in der Schweiz
nahezu 2400 Kilometer nivellirt und davon einen grossen Theil doppelt, einen andern
Theil der Linien einfach, insofern dieselben durch den Polygonalabschluss, den wir
stets angestrebt und erreicht haben, controllirt sind. Unter den doppelt nivellirten be-
findet sich das grosse Alpenpolygon, welches den Norden der Schweiz mit Italien durch
zwei hohe Alpenpässe verbindet, nämlich über den Gotthardt und den Simplon,
Nachdem diese beiden Linien doppelt nivellirt und der bei dem ersten Uebergang über
den Simplon auf italienischem Gebiet begangene Fehler von 1 Meter aufgefunden und
corrigirt worden ist, schliesst dieses grosse Polygon, mit einem Umfang von 896 Kilo-
meter und mit einer Niveaudifferenz von 2180 Meter, mit einem Fehler, der ungefähr
11 Centimeter beträgt.

Man sieht also, dass es wirklich möglich ist, das geometrische Nivelle-
ment nicht nur in ebenen Ländern, sondern sogar im Gebirge und Hochgebirge
mit einer überraschenden Genauigkeit zur Anwendung zu bringen. Denn schliess-
lich ist, wenn man den Fehler mit der überwundenen Höhe vergleicht, ein Fehler

von einem Decimeter bei einer Differenz von 2000" nur Die bei Gelegen-

1
20000
heit der Untersuchung dieses grossen Polygons mir zuerst aufgestossene Vermuthung,
dass durch die Lothablenkung ein Fehler veranlasst sein könnte, der sich später
in diesem Falle als ein Operationsfehler erwiesen hat — diese Vermuthung hat
wenigstens den in meinen Augen nicht zu verachtenden Vortheil gehabt, die Aufmerk-
samkeit der Geodäten und Mathematiker auf die Frage zu lenken, inwieweit die Local-

attraction und Lothablenkungen die geometrischen Nivellements beeinflussen können.

Es sind seitdem von mehreren Seiten, wie Sie wissen, bemerkenswerthe Arbeiten
darüber erschienen; auch unser verehrter Präsident, Herr General Baeyer, hat in den
Astronomischen Nachrichten eine Abhandlung darüber veröffentlicht, die Ihnen jeden-
falls bekannt ist. Ich trete hier nicht näher in die Discussion dieser schwierigen Frage
ein, sondern will nur bemerken, dass, wenn auch bisher unsere schweizerischen Nivel-
lements keine Einflüsse der Lothablenkungen ergeben haben, welche die Fehlergrenze,
insoweit sie den Instrumenten und Beobachtungsmethoden zuzuschreiben ist, wesentlich
übersteigen, doch damit in meinen Augen noch nicht erwiesen ist, dass em solcher
Kinfluss nicht existirt; ich glaube vielmehr, derselbe existirt, ist aber vielleicht

numerisch geringer, als ich früher angenommen habe, und namentlich heben sich die
11

 
