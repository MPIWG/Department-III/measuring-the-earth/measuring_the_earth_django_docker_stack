    
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
 

 

14

an diese, der Gradmessung in vielfacher Weise förderlich gewesenen Männer wird von
uns Allen in Ehren gehalten werden.

Endlich ist noch mitzutheilen, dass zu unser aller grossem Bedauern der bis-
herige Präsident der permanenten Commission, Herr von Fligely, aus Gesundheitsrück-
sichten seine Entlassung genommen hat. Er schreibt:

An
die hochgeehrten Herren Mitglieder der permanenten Commission für die europäische
Gradmessung.

Meine gänzlich herabgekommene Gesundheit hat mich genöthigt, meinen
allerhöchsten Kaiser und Herrn um Enthebung von der Stellung eines k. k.
österr. Bevollmächtigten bei der internationalen europäischen Gradmessung zu
bitten, welche Bitte mir vor wenig Tagen huldvollst gewährt wurde.

Die Unterfertigung der Einladungsschreiben zur diesjährigen allgemeinen
Conferenz in Dresden war mein letzter officieller Akt und mit ihm scheide ich
aus dem ehrenvollen Kreise von Fachmännern, welchem bisher anzugehören
mein höchster Stolz war.

Mit tief empfundenem Danke für das mir jahrelang geschenkte Vertrauen
empfehle ich mich Ihnen, meine hochachtbaren und mir lieb gewordenen bis-
herigen Collegen, in Ihre mir unschätzbar werthe Erinnerung, Sie selbst aber
und die grosse Aufgabe, der Sie Ihre Kräfte weihen, dem Schutze der Vorsehung.

Wien, am 7. September 1874.
August von Fligely,
k. k. wirkl. geheimer Rath und Feldmarschall-Lieutenant des Ruhestandes.

Als Commissare sind neu hinzugekommen: Für Deutschland der Chef der astro-
nomischen Section des geodätischen Instituts in Berlin, Herr Dr. Albrecht; ausgeschieden
ist Herr Prof. Jordan in Carlsruhe. Für Frankreich sind ernannt die Herren Faye,
Villarceau, Saget und Perrier, für Italien die Herren Betocchi, Lorenzoni, Oberholzer,
Respighi, Santini, für Oesterreich- Ungarn die Herren: Oberst Ganahl, Dr. Th. von
Oppolzer und Dr. Tinter, für die Schweiz Herr Oberst Sieyfried.

Herr von Bauernfeind: Ich erlaube mir den Anträg zu stellen, dass die hoch-
geehrte Versammlung, ebenso wie die permanente Commission es bereits gethan, Herrn
von EF ligely ihr lebhaftes Bedauern über sein Ausscheiden ausspricht und ihm ihren Dank
für seine erfolgreiche Wirksamkeit durch ein Telegramm ausdrückt.

Der Antrag wird durch Erheben sämmtlicher Herren von ihren Sitzen ange-
nommen,

Herr Bruhns: Noch habe ich der Conferenz mitzutheilen, dass aus der perma-

nenten Commission die Herren Bruhns, de Vecchi und von Forsch ausscheiden und da
ausserdem zwei Stellen vacant sind, hat die Conferenz daher in einer ihrer niichsten

Sitzungen fünf Mitglieder in die permanente Commission zu wählen.

es ach a ah nn

 

 
