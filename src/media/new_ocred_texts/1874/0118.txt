NADINE |

ira RT

TAT

TTT BIT NT

TPPRE TA TOP

r

Wr

219

darauf hingewiesen, dass im Innern des Gotthard die Dichtigkeit des Gesteines nur
wenig variire und daher ein sehr werthvoller Beitrag zur Kenntniss der mittleren
Dichtigkeit der Erde von einer solchen Untersuchung zu erwarten sei.

b. Geodätische Section.

Den ersten Gegenstand bildet Punkt 4 des Programms: über Maassvergleichungen.

Herr Ibanez und Hirsch begründen den von ihnen gestellten im Programme
enthaltenen Antrag durch eine Darstellung des Zustandes, in welchem gegenwärtig die
Meterfrage sich befindet, insbesondere erwähnte Herr Hirsch, dass durch die Meter-
commission die wissenschaftlichen Principien zur allgemeinen Zufriedenheit festgestellt
worden seien, und auch den Bedingungen, welche die Gradmessungsconferenz aufgestellt
habe, entsprochen werde. Dagegen fehle es an der praktischen Ausführung und es sei
angezeigt, dass die Gradmessungsconferenz, welche seiner Zeit die ganze Meterfrage an-
geregt, auch jetzt wieder die Initiative ergreife, um auf die Ausführung der gefassten
Beschlüsse hinzuwirken.

Herr Faye wünscht die Resolution weniger allgemein gefasst, worauf Herr
Hirsch die Fassung des Vorschlages durch die factische Sachlage begründet und Herrn
Faye darauf aufmerksam macht, dass der Beschluss mit einem Begleitschreiben ver-
sehen an die französische Regierung gelangen werde, worin die nähere Motivirung ent-
halten sein werde.

Herr Faye ist durch diese Erklärung des Herrn Hirsch befriedigt und es
wird der Antrag der Herren Ibanez und Hirsch einstimmig zum Beschluss
erhoben.

Im Anschluss an den in der ersten Sitzung der vierten allgemeinen Conferenz
mitgetheilten Bericht des Centralbureaus macht Herr Hirsch auf die Unzulänglichkeit
der Berliner Localitäten aufmerksam, welche zu Maassvergleichungen als unzureichend
sich erwiesen haben. Herr Hirsch beantragt, dass die preussische Regierung durch die
permanente Commission um Anweisung passender Localitäten angegangen werden möchte.

Herr Bruhns betont, dass die permanente Commission sich 1373 bereits an das
preussische Cultusministerium gewendet habe; nachdem das ‚betreffende Gesuch ohne
eeblieben sei, beantragt er, dasselbe beim Staatsministerium zu wiederholen.

Erfolg ¢

Herr Faye bemerkt, dass ein derartiges Gesuch im Widerspruche stehe mit
dem ersten Beschlusse, welcher auf Herstellung eines internationalen Maassbureaus ge-
richtet sei, worauf Herr Ibazez und Herr Hürsch darauf aufmerksam machen, dass es
sich nicht darum handle, das Centralbureau erst zu schaffen, sondern nur darum,
letzteres in den Stand zu setzen, die ihm aufgetragenen Geschäfte auszuführen.

Es wird hierauf einstimmig beschlossen, den Wunsch des Herrn General
Baeyer, betreffend die Herstellung eines besonderen für Maassverglei-
chungen brauchbaren Gebäudes bei der K. preuss. Staatsregierung zu
unterstützen.

 
