49

per la difficolta di vedere la stella col piccolo ed imperfetto cannocchiale dell’ alta-
zimut. Per questa circostanza e per la nebbia che ha dominato per vari i giorni in
quell’ ora il numero delle osservazioni à riescito piuttosto piccolo, non essendosi potuto

convenientemente osservare che in sei passaggi, coi quali i valori medj ottenuti per la
latitudine sono i seguenti:

nn

N° Osservazione

 

: Diretta Riflessa
Luglio 23 m,== 41° 55 92" a4 12 11
Se I3:066 12 te
si are 23. 46 18 14
sr sel 22. 46 Idies: 96
Agosto 1 24, 05 10 10
: À: 4 22. 92 15 14
Medio — 41°55°23"o7 Somma 77 67.

La declinazione usata é quella del Nautical Almanach 1874. | |

Le osservazioni, fatte sopra varie altre stelle poco distanti dal zenit, non sono -
state finora ridotte che in parte; e percid non si possono ancora presentare 1 risultati.

Le sensibili differenze che si riscontrano sia nei risultati delle singole osser-
vazioni, Sia nei medii ottenuti in ciascun passaggio della polare , possono dipendere da
arie cause, e cioé dalla irregolarita di forma dell’ immagine della stella, che puo
portare una qualche differenza nella collimazione, specialmente fra le osservazioni dirette
e quelle per riflessione, dalla difficoltà di lettura dei microscopj nella quale l’errore
probabile & abbastanza forte, dalla forte ecventricit’ del circolo e da qualche inesattezza
nelle divisioni: quantunque si possano queste inesattezze ritenere abbastanza piccole.

In quanto ai valori delle rivoluzioni delle viti dei microscopj, da varie misure
prese in diverse parti del lembo si & trovato, che essi molto prossimamente corrispon-
dono a 60", cosiché le divisioni della testa della viti si sono direttamente tradotte
in secondi. Finalmente durante le osservazioni si’ & verificato l’immobilità del porta-
| microscopj colla opportuna lettura del livello, e nel caso di spostomenti, ne sono
| state applicate le relative correzioni alle letture fatte sul circolo, essendosi in prece-

denza determinato il valore angolare di ogni divisione del livello stesso, che & risul-
I tato di 2" 43.

HE — ~ Ser errr

Tranny

TT ART

T

Prima di chiudere questa Nota credo opportuno di far conoscere, che era mia
intenzione di determinare la latitudine di Monte Mario anche col cannocchiale zenitale
servendomi di un semplice reticolo invece del micrometro: ma disgraziatamente i fili
di questo reticolo erano troppo sensibili all umidità che dominava in quei giorni nei
quali mi era riservato di fare quelle osservazioni, e cioé nella fine del Luglio e nel
principio dell’ Agosto, cosicche deformato sensibilmente il reticolo non mi fu possibile
fare buone osservazioni; e dovetti percid rimettere ad altra circostanza di farne l’esperi-
mento, per vedere quale grado di approssimazione si possa ripronettere da questo modo
di osservazione.

ET TPR BETTIE TT 7 RTT! Or

r

à
5

 
