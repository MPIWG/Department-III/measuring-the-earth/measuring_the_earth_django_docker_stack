 

re Ser

I

Eee rare
I

 

a

Verfahrens stellen möchte, vielmehr nur beabsichtigt habe, die Aufmerksamkeit darauf
hinzulenken.

Herr Hirsch glaubt auf eine Erscheinung, welche insbesondere bei Ausführung
der Längenbestimmung zwischen Neuenburg, Simplon und Mailand zu Tage getreten,
hinweisen zu müssen. Dieselbe besteht darin, dass oftmals bei völlig gleicher Strom-
stärke eine Verschiedenheit in der Schnelligkeit der Anker-Anziehung der Relais oder
der Registrirapparate beobachtet werde, welche bei Versuchen, die bei Gelegenheit der
obigen Längenbestimmungen ausgeführt wurden, Beträge bis zu 0505 erreicht habe.
Nach seiner Meinung ist die Anziehungszeit der Anker nicht allein von der Stromstärke
abhängig, sondern auch noch von anderweitigen Elementen, unter denen besonders die
mehr oder minder vollständige Isolation der Leitung eine bedeutende Rolle spiele. Bei
der gänzlichen Unmöglichkeit, diese von den Witterungsverhältnissen abhängige Grösse
unter ausreichender Controle halten zu können, erkennt er als einziges Mittel zur Eli-
mination dieser Fehlerquelle die Vermehrung der Anzahl der Beobachtungsabende, da
nur bei einer grossen Zahl von Abenden auf eine ausreichende Compensation dieser
Fehlerquelle zu rechnen sei.

Herr Oppolzer hat die gleiche Wahrnehmung gemacht, glaubt jedoch den Grund
dieser Erscheinung darin suchen zu müssen, dass der Strom am Ende der Leitung nur
allmählich zu seiner vollen Intensität anwachse und die Zeitdauer des veränderlichen
Zustandes selbst wieder von der mehr oder minder guten Isolation der Leitung ab-
hängig sei. Auf der Ausgangsstation erreiche der Strom sehr rasch, auf der Endstation
nur langsam diejenige Intensität, welche zum Ansprechen der Receptivapparate erforder-
lich sei. Beim Ausgleich der Stromstärken werde aber auf diesen veränderlichen Zu-
stand nicht Rücksicht genommen und nur eine Ausgleichung der Maximalstärken be-
wirkt. Er glaubt, als Mittel zur Beseitigung dieser Fehlerquelle das Arbeiten mit
starken Strömen und eine möglichst feine Stellung der Relais anempfehlen zu müssen,
insofern bei Beachtung dieser Vorsichtsmassregeln noch am ersten auf eine gleich
rasche Action der Receptivapparate zu rechnen sei. Als eine sehr wesentliche Vor-
sichtsmassregel gelte ferner die Anwendung durchaus gleichartiger Apparate an allen
Stationen, insbesondere weil dann zu erwarten sei, dass die oben genannten Störungen
nur das Resultat der Stromzeit, nicht aber das der Uhrvergleichung beeinflussen würden.

Eine kleine sich hieran anschliessende Discussion, an welcher sich ausser den
Genannten noch die Herren Bruhns und Albrecht betheiligen, führt zu der Ansicht,
dass die früher festgesetzte untere Grenze für die Zahl der Abende, nämlich vier, jeden-
falls zu niedrig gegriffen sei.

In Bezug auf die weitere Frage des Punktes 1, wie weit die Ausführung der

astronomischen Bestimmungen gediehen ist, wünscht Herr Bruhns, dass das Verzeichniss

der fertigen und der in Aussicht genommenen astronomischen Arbeiten, welches im Be-
richte der letzten Generalversammlung gegeben ist, ergänzt und weiter fortgeführt werde.
Es gelangt schliesslich ein Antrag des Herrn Hirsch zur einstimmigen Annahme, dass
die Herren Commissare ersucht werden, eine Statistik der bereits publicirten astrono-

 

|
i
i
