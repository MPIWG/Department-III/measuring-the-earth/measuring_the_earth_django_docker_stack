 

 

en

Dann kommt Transvaal; bis zum 22° etwa ist dort noch nichts geschehen, aber Herr GILL
hat die Messungen organisirt, sie können also demnächst ausgeführt werden. Weiter
nördlich in Rhodesia, zwischen 22° und 16° Breite, hat man die Messungen über 4 Breiten-
grade theilweise beendigt. Im nördlichen Theile war noch ein Polygon und im Süden eine
Kette von 2 Breitengraden zu messen. In dieser Gegend hatte man mit grossen Schwierig-
keiten zu kümpfen, hervorgerufen erstens durch Fieber, welches besonders den Leiter der
Expedition stark angegriffen hat, ferner durch den Wechsel der Transportweise, indem man
vom Ochsentransport zum Trägertransport übergehen musste. Jetzt sind auch die Messungen
vom Zambesi aus bis zum Tanganyikasee organisirt, und man hat diese Arbeit Herrn
Dr. Rusın übertragen, der an den Arbeiten in Spitzbergen theilgenommen hat. Am Ende des
Monats April ist eine Expedition vom Cap per Dampfboot nach Chinde abgereist.

Da es sich bei den Messungen zwischen 16° und 8° bis 9° Breite um einen Bogen
von ungefähr 7° handelt, ungefähr von derselben Grösse wie die Breitengradmessung in
Ecuador, so wird es wohl einige Jahre dauern, ehe diese Messungen zu Ende geführt sind,
sodass man in Deutschland und im Kongostaat wohl die Zeit haben wird, rechtzeitig zu
beginnen die Arbeiten weiter nördlich fortzusetzen.

Herr Helmert fügt noch hinzu, dass, in Übereinstimmung mit den Beschlüssen der
internationalen Erdmessung und der Association der Akademien, der deutschen Regierung
und derjenigen des Kongostaates der Plan unterbreitet worden ist, die Messungen dem Tan-
ganyikasee entlang und noch weiter bis zu 1° südlicher Breite fortzusetzen. Financieller
Schwierigkeiten wegen haben die deutschen Behörden diese Angelegenheit noch nicht zur
Hand nehmen können, aber Herr Helmert hofft, dass auch durch die Mitwirkung der preus-
sischen Akademie der Wissenschaften die Sache im nächsten Jahre in Gang kommen wird. Er
hatte bereits Gelegenheit durch Vermittlung des Herrn Hauptmann’s Herrmann, der Triangu-
lirungsarbeiten bei den Bestimmungen der Grenzen in der nördlichen und südlichen Gegend
des Tanganyikasees geleitet hat, einen Kostenanschlag zu gewinnen und wissenschaftliche und
technische für die Expedition wichtige Einzelheiten zu sammeln. Herr Helmert zweifelt nicht,
dass man in Deutschland die Expedition organisiren wird, und er hat auch gehört, dass der
Kongostaat einem derartigen wissenschaftlichen Unternehmen günstig gegenüber steht.

Was die Messung der Grundlinien betrifft, so werden sie mit den JÄverın-Apparaten
ausgeführt werden. In der Nähe von Salisbury, 2° südlich vom Zambesi, ist im Jahre 1900
eine Grundlinie von 13!/, englische Meile gemessen worden mit Dräthen, welche vor und nach
den Messungen am Kap justirt worden sind. Bei diesen Messungen nördlich von Rhodesia
sind eine Menge von Metaldrähten benutzt worunter zwei Invardrähte.

Der Secretär theilt die Hauptsachen von Herrn Helmert’s Rede in französischer
Sprache mit.

Herr Darwin fügt hinzu, dass Sir Davın Gızr die russische Regierung gebeten hat
ihm ihr Basisapparat zur Benutzung bei der Messung der Grundlinien in Rhodesia während
einiger Zeit zu überlassen, und dass die Regierung dieser Anfrage Folge geleistet hat.

Herr Bakhuyzen fragt Herrn Darwin, ob es Gıuu’s Absicht sei mit diesem Ap-
parat nur die Messungen mit den Jüderindrähten zu controlliren.

 

vl, aha dia Marllakunı du ul

Lak

vn usbahhhh 1 Ms a

 
