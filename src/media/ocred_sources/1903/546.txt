Brevi notizie sui lavori trigonometrici eseguiti in Italia

(An Stelle der Seiten 259—262 der Verhandlungen zu Stuttgart 1898).

Le triangolazioni del Marieni '), del parallelo medio ?) e del Fergola ?), presenta-
vano un grado di precisione sufhiciente per servire di base a qualsiasi riserca di Alta
Geodesia. D’un ordine di precisione alquanto inferiore era la triangolazione dell’ Isola di
Sardegna del Generale La Marmora *), le cui osservazioni andarono perdute in un incendio.

Tuttavia dovendosi nel 1862 iniziare le levate topografiche per la costruzione della
Carta del Regno, parve opportuno di riordinare la rete generale in modo da farla risultare
continua per tutta la superficie d’Italia onde meglio si prestasse all’ applicazione dei metodi
d’osservazione e di calcolo informati ai progressi della scienza.

I nomi dei direttori dei lavori e degli operatori che si succedettero nelle varie
epoche in cui furono eseguite le misure angolari sono riportati nell’ unito specchietto (re-
datto in base alle istruzioni emanate dall’ Associazione Geodetica) nel quale figurano inoltre
le coordinate geografiche dei vertici e gli strumenti adoperati nelle misure angolari.

Con l’eseguimento delle stazioni di Capraia e Montecristo effettuate in occasione
del collegamento geodetico della Sardegna col Continente compiuto con esito felice nell’
estate decorsa, la rete fondamentale & ora completamente osservata, e l’attivita dell’ Istituto
sara d’ora innanzi prineipalemente rivolta a lavori di indole geodetico-astronomica per lo
studio della verticale.

Le coordinate geografiche dei vertiei della rete fondamentale sono calcolate sull’
ellisoide di Bessel e le longitudini contate dal meridiano dell’ Isola del Ferro.

I confronti dei valori che assumono queste coordinate nei punti di confine fra
Italia e la Francia, la Svizzera e l’Austria sono riuniti nello specchio seguente, dal quale
risulta che le differenze riscontrate (del resto di poca entita) presentano un andamento che
depone in favore della precisione dei lavori eseguiti.

1) Trigonometrische Vermessungen im Kirchenstaate und in Toscana. Wien 1846.

2) Operations geodesiques et astronomiques pour la mesure d’un arc du parallele moyen 1821, 22, 23.
3) Annali Civili del Regno delle due Sicilie, maggio e giugno 1838.

4) Notice sur les operations geodesiques faites en Sardaigne, 1839.

Lunmtl uummhn Te HÄHIRLLIL Makildhukı uud

Ial

nn, cha da vn eh
