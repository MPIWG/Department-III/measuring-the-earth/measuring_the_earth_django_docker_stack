Linn |

am

steine Uranienburgs für andere Zwecke verwendet. So völlig war die Zerstörung, dass
schon 50 Jahre nach Tychos Tod sozusagen keine überirdische Spur von seinen Gebäuden
übrig war.
Über dem Eingang von Sternenburg hatte Tycho einen Stein mit der Inschrift
angebracht:
NEC FASCES NEC OPES
SOLA ARTIS
SCEPTRA PERENNANT

oder wie es auf deutsch ungefähr heissen würde: »Nicht Macht, nicht Reichtum, nur die
Herrschaft der Künste dauert ewig fort”.

Schwerlich hat Tycho geahnt, dass diese Worte so prophetisch für ihn und sein
Werk sein würden!

Der Platz der Tychonischen Sternwarten lag nun wesentlich in demselben Zustand
von ca. 1650 bis zur jetzigen Zeit. (Pl. IV). Ab und zu sind die Ruinen von Gelehrten
besucht worden, und diese haben sie in der Hauptsache übereinstimmend beschrieben. So
besuchte der spätere Bischoff und Schriftsteller Huet 1652 Hven und von ihm wissen wir,
dass schon damals fast keine Spur von den Gebäuden mehr zu sehen war.

Ganz dasselbe berichtet der französische Astronom Picard, der im Jahre 1671 von
der Pariser Akademie nach Hven geschickt wurde, um die geographische Lage Uranien-
burgs zu bestimmen; schon damals war der nordöstliche Teil des Walles geschleift.

Endlich vor ungefähr 90 Jahren wurde der südwestliche Teil des Walles durch-
brochen und ein Schulhaus auf dem Platze aufgeführt.

Schon 1823 unternahm man eine Ausgrabung der Ruinen, und dasselbe fand statt
im Jahre 1901, anlässlich der dreihundertjährigen Wiederkehr von Tychos Todestag,

Bei dieser Gelegenheit wurde eine sorgfältige Vermessung der noch vorhandenen
Überreste ausgeführt, so wie wir es an diesen beiden Plänen sehen. (Wa WIN)

Professor CHARLIER in Lund, der die Ausgrabung leitete, hat die Ruinen in. einer
sehr ansprechenden und pietätsvollen Weise beschrieben und hat gütigst die Originalpläne
kopieren lassen, wofür wir ihm vielen Dank schuldig sind.

Man sieht, dass nicht ganz unbedeutende Überreste von der so interessanten Ster-
nenburg existiren, leider sind sie aber wieder zugedeckt worden, so dass wir uns damit
begnügen müssen, nur den Platz sehen zu können. Von Uranienburg ist nicht viel mehr
vorhanden als das, was wir hier sehen.

Obwohl nun der letzte Teil von Tychos Leben für seine Landsleute wenig erfreulich
und das Schicksal seiner Sternwarten ein gar trauriges gewesen ist, so können wir uns
dennoch freuen, dass sein Wirken hier am Platze die schönsten Früchte trug. Zwar wurde

 
