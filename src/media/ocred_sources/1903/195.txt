|
\
|

 

 

 

 

175
graphique, sous les ordres du Commandant Bourgeois chef de cette section, est partie de
France le 26 ayril 1901 et a debarqu& & Guayaquil le 1er juin.

Les premieres semaines ont &t6 employees & transporter tout le materiel geodesique
et astronomique de Guayaquil & Riobamba, centre des travaux; cette operation longue et
difiicile en raison de l’&tat des chemins muletiers de la Cordillere a exige environ 150 mulets,
une centaine de porteurs indiens et a coüte environ 12000 francs.

La mission s’est trouvee r&unie & Riobamba le 13 juillet et a immediatement com-
mence ses travaux.

L’annee 1901 a &te employ&e aux operations suivantes:

1°. Mesure de la base fondamentale de Riobamba, faite avec l’appareil bimetallique
de Brünner;

2°. Mesure de la latitude aux deux extremites de l’arc;

3°. Mesure d’une latitude et d’un azimut fondamental & la station de Riobamba,
qui parait la plus & l’abri des attractions locales ; i

4°. Mesure de la difference de longitude entre Riobamba et Quito, puis entre Quito
et l’extr&mite nord de l’arc;

5°. Mesure de l’intensite de la pesanteur & la station fondamentale de Riobamba;

6°. Mesure de la base de verification du nord, au fil Jäderin;

7°. Reconnaissance et construction des signaux de la portion de la chaine meridienne
entre Riobamba et l’extremite nord de l’arc.

L’annee 1902 et les six premiers mois de 1903 ont &t& consacres aux 'op6rations
geodesiques proprement dites, ainsi qu’aux op6rations astronomiques sevondaires, aux deter-
minations magnetiques, etc....., dans la region comprise entre Riobamba et l’extr&emite nord.

Les observations d’angles sont faites par la methode de mesure directe des angles
dans toutes leurs combinaisons, en suivant les principes poses par M. le Göneral Schreiber.

Ainsi que cela a 6t& deeide & la suite de l’Assemblee generale de l’Association g60-
desique en 1900, on a determine la latitude astronomique & presque toutes les stations
geodesiques, en vue de l’etude des deviations de la verticale. Les determinations ont &te
faites par la methode des observations circummeridiennes, en observant la mäme &toile cerele
a droite et cercle & gauche.

Actuellement, on commence les operations geodesiques et astronomiques entre Rio-
bamba et l’extrömite sud de l’arc; les travaux doivent encore durer environ deux ans.
L’amplitude totale de l’arc mesure sera de 5° 52°,
