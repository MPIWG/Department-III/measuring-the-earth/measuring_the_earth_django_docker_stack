58

 

 

S, Exeellenz der Cabinetschef Minister der auswärtigen Angelegenheiten, Herr Deuntzer
hält folgende Anrede.

Hochgeehrte Herren,

| Ich habe die Ehre, in Namen ‚der Königlichen Regierung, Sie in Dänemark’s
Hauptstadt herzlich willkommen zu heissen.

Meine Herren, Delegirten der internationalen Erdmessung, Ihre Arbeiten sind so
spezieller Natur und erfordern so tiefgehende Kenntnisse der mathematischen und physi-
schen Wissenschaften, dass die grosse Mehrzahl auch der gebildeten Leute, so bald es sich 1
um die Einzelheiten Ihrer Wissenschaft handelt, Ihren Berathungen nicht folgen können.

Aber im grossen und ganzen kennen wir alle den Zweck welchen Sie durch Ihre
Arbeiten zu erreichen suchen. Ihre Wissenschaft bemüht sich die mathematische Gestalt |
unserer Erde, besonders ihrer Figur im Meeresniveau, und die diese Gestalt beherrschende
Kräfte genau in allen Einzelheiten zu erforschen.

Zugleicherzeit — und diese Angelegenheit ist für uns besonders wichtig — verdanken
wir Ihren Abeiten die nothwendigen, vollkommen genauen Daten für die in unserem Leben eine
so hervorragende Rolle spielenden grossen topographischen Karten, sei es um die genaue
Begrenzung der Grundstücke oder die Grenzen der Staaten festzustellen, sei es um die i
Entwieklung der Verkehrswege zu fördern.

M. H. Die kräftige Unterstützung Ihrer Arbeiten von Seiten aller Staatsregierungen |
beweist wie hoch die Arbeiten der Erdmessung geschätzt werden.

Es besteht vermuthlich keine Association die in so hohem Maasse wie die Ihrige
die Bestrebungen aller eivilisirten Völker zur Erreichung eines gemeinsamen Zweckes ver-
| einigt hat. Das ist eine der wichtigsten Seiten Ihrer Thätigkeit.

) Wenn jemals das Ideal eines ewigen Friedens erreicht wird, während dessen alle

Al

 

ua.

 

 

| unsere gemeinschaftlichen Bestrebungen nur dazu dienen die Wahrheit immer mehr zu
| erforschen, so wird dies nur möglich sein, wenn wir den von Ihrer Association uns vorge-
zeigten Weg einschlagen.

Wir Dänen, wir sind glücklich und stolz, in unserem Vaterlande die Conferenz zu
| begrüssen von Delegirten eines die ganze civilisirte Welt umfassenden wissenschaftlichen
Vereins der so viele berühmte Männer in seiner Mitte zählt.

N Meine Herren, ich wiederhole es, Sie sind uns herzlich willkommen, und ich
schliesse mit dem Wunsch dass Ihre Beratbungen während dieser Tage reichen Erfolg für
die Wissenschaft haben mögen und dass Sie ein angenehme Erinnerung mitnehmen werden
an die leider nur zu kurze Zeit, welche Sie unter uns verbleiben.

Herr General von Zacharie, Mitglied der permanenten Commission, Delegirter von
Dänemark ergreift das Wort und bewillkommt die Conferenz mit folgender Rede.

H 3
1

 
