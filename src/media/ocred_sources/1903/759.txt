ein

en re

mann

U]

Hrn. ML lim N

Pr mn

425
so dass also die grosse Achse nahe mit Cuarke’s Wert übereinstimmt, die Abplattung aber
sogar noch kleiner als die Brssew’sche wird.

Von einer Zusammenstellung der in den beiden angeführten Werken bestimmten
Lotabweichungen kann abgesehen werden, da Herr 0. H. Tırrmanx auf der 14. Allgemeinen
Konferenz in Kopenhagen (1903) einen Bericht vorgelegt hat, der bereits eine Übersicht über
sämtliche Lotabweichungs-Bestimmungen in den Vereinigten Staaten enthält }). Da ferner dieser
Bericht auch in dem I. Teil der Kopenhagener Verhandlungen, 8. 182—212, abgedruckt ist,
so genügt es, wenn nur kurz auf seinen hier in Betracht kommenden Inhalt hingewiesen wird.

Da die verschiedenen grösseren Triangulationen in den Vereinigten Staaten jetzt
miteinander in Verbindung gebracht sind, so wurde die Wahl eines einzigen Ausgangspunktes
für die Berechnung der geographischen Breiten, Längen und Azimute als vorteilhaft er-
kannt ?). Hierzu wurde die nahe in der Mitte des Gebietes gelegene Station Meades Ranch
(Kansas) gewählt, die zwar selbst kein astronomischer Punkt ist, deren Breite, länge und
Azimut aber so angenommen wurden, dass die Mittelwerte der hiermit sich ergebenden
Lotabweichungen (soweit sie bis jetzt bestimmbar waren) für jede der drei Arten nahezu
null werden. Dieses United States Standard Geodetic Datum ist hiernach:

Meades Ranch (Kansas): Breite... ..... — 39213, 26,686

Lange... 0.2.0, = 98 32 30,506 westlich von Greenwich
Azimut von Waldo =75 28 14,52 (8. über W.)

Von diesem Standard Datum ausgehend sind bereits 13000 Positionen von Dreiecks-
punkten berechnet, die sich auf ein Gebiet verteilen, das sich vom atlantischen bis zum
grossen Ozean und vom Oberen See bis New Orleans, also über 57 Längen- und 18
Breitengrade erstreckt. Zu Grunde gelegt wurde, im Anschluss an die früheren Rechnungen,
das Crarke’sche Bllipsoid von 1866: a= 6378206,4m; b= 6356583,8 m. Damit war es
schon ermöglicht, ein einheitliches System abzuleiten, das für 246 Stationen Lotabweichungen
in Breite, für 76 solche in Länge und für 152 solche in Azimut gibt, deren Werte bei
späterer definitiver Bearbeitung nur noch geringe Änderungen erfahren dürften.

Auf Grund dieser Daten hat Herr J. F. Havrorn eine vorläufige Bestimmung der
Gestalt des Geoids für die östliche Hälfte der Vereinigten Staaten ausgeführt, die durch
eine Karte, in die Höhenkurven für die physische Erdoberfläche und für das Geoid einge-
zeichnet sind, erläutert wird; in Bezug auf Einzelheiten sei auf den Bericht selbst verwiesen.

Endlich hat man auch begonnen, im Zusammenhang mit diesen Untersuchungen
über das Geoid, Attraktionsberechnungen auszuführen, die sich auf die Berücksichtigung
der Massen bis zur Entfernung von 4126km um jede Station herum erstrecken sollen; bis
Juni 1903 waren für 9 Stationen diese Rechnungen bereits vollendet.

 

1) 0. H. Tırımann. Report on Geodetic Operations in the United States to the fourteenth General Conference
of the International Geodetic Association. Washington 1903. S. 10—28.

2) Vergl. hierzu: Report of the Superintendent of the Coast and Geodetic Survey showing the Progress of the
Work from Juli 1, 1901, to Juni 30, 1902. Washington 1903. Appendix N° 3. Jorn F, Hayrorp. Triangulation in
Kansas. S. 237—238.

 
