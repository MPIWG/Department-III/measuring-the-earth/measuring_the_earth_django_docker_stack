184

Den hier angegebenen Wert des dynamischen Temp.-Koeff. hat jedoch Herr Hanskı
bei der Reduktion seiner Beobachtungen nicht verwendet, er hat vielmehr das Mittel (21.0)
aus diesem und einem früher vom Geodätischen Institut gefundenen Werte benutzt. Da Herr
Hanskı seine Messungen stets über eine volle Temperaturperiode gleichmässig verteilt hat,
so ist die Anwendung des dynamischen Koeffizienten für das Stationsmittel der Schwingungs-
zeiten von verschwindendem Einfluss; sie bewirkt nur eine bessere Übereinstimmung der
einzelnen Schwingungszeiten, die durch starken Temperaturgang systematisch entstellt sind.

Über die Konstanz der Pendel geben die Anschlussmessungen in Potsdam und
Pulkowa, welche die Messungen in Spitzbergen und Stockholm einschliessen, Auskunft.

Anschlussmessungen in Potsdam.

Pend. 49 Pend. 50 Pend, 51 Mittel Sätze
1901 April 0:.506 5781 0.506 4708 0°.506 5627 0°.506 5371 6
November 5768 4716 5634 Dez 10
Dezember 5765 A714 5630 5369 8
1902 Januar 5776 4692 5639 5369 18

Anschlussmessungen in Pulkowa.

Pend. 49 Pend. 50 Pend. 51 Mittel Sätze
1901 Mai 05.506 4168  0°.506 3106 0.506 4027 0°.506 3767 8
Oktober 4159 3096 4006 3754 8
» 4158 3095 4013 3755 16

Für Potsdam sind sämmtliche Reihen auf die Meereshöhe 87 m reduziert; die beiden
ersten bilden die eigentlichen Anschlussmessungen, die beiden letzten haben zur Bestimmung
‚des Luftdichte- und Temperatur-Koeffizienten gedient. In Pulkowa ist die erste und zweite
Reihe im astrophysikalischen Laboratorium in 74m, die dritte im Keller der Sternwarte
in 71m Meereshöhe beobachtet worden. Die Änderungen der Pendel sind hiernach unbe-
deutend; dasselbe gilt auch für die Zwischenstationen, wo aus den Unterschieden ihrer
Schwingungszeiten auf ihre Konstanz geschlossen werden kann.

Als Koinzidenzuhr diente eine Halbsekunden-Pendeluhr von Srmrasser & RoHpk
(N®. 174). Obgleich diese Uhr mit einem Nickelstahl-Pendel versehen war, zeigte sie in
ihrem Gange doch eine beträchtliche Abhängigkeit von der Temperatur. Herr Hauskı be-
stimmte ihren Temperatur-Koeffizienten aus den Pendelmessungen auf Spitzbergen und fand,
dass eine Temperaturänderung von 1° eine Änderung des täglichen Ganges von 0°.24 be-
wirkte. Bei dem durchweg beträchtlichen Temperaturgang, dem die im Zelt vorgenommenen
Pendelbeobachtungen auf Spitzbergen ausgesetzt waren, machte dieser Umstand eine möglichst
gleichmässige und dichte Verteilung der Messungen auf das Intervall zwischen den ein-
schliessenden Zeitbestimmungen notwendig, um den Einfluss der periodischen Gangänderungen
aus dem Stationsmittel der Schwingungszeiten zu eliminieren. Die folgende Zusammenstel-
lung zeigt diese Verteilung und den Umfang der Schwingungsbeobachtungen.

 

&
ü
I

aa un

nahm kl ha
