BEILAGE B II.

BERICHT

über
die Tätigkeit des Centralbureaus der Internationalen Erdmessung
im Jahre 1902
nebst dem Arbeitsplan für 1903. *)

Be en ET TELGTE

A. Wissenschaitliche Tätigkeit.

Dieselbe erstreckte sich auf folgende Gebiete:
1. Fortsetzung der Berechnungen für das europäische Lotabweichungs-
system;

nn TI Tee nn In

2. desgl. für die Krümmung des Geoids in den Meridianen und Parallelen;
3. Ableitung der Bewegung der Erdachse im Erdkörper aus dem Ergebnis
der freiwilligen Kooperation der Sternwarten, in Verbindung mit

4. der Bearbeitung der Ergebnisse des internationalen Polhöhendienstes; '
5. Absolute Pendelmessungen;
3 6. Relative Schweremessungen. j
= in |
Spezialbericht !
f
I

über die systematische Berechnung von Lotabweichungen.

„In Übereinstimmung mit dem im vorjährigen Tätigkeitsbericht ent-
haltenen Arbeitsplan für 1902 wurde die Drucklegung der südlich der
europäischen Längengradmessung in 52° Breite berechneten geodätischen |

9

*) Der Arbeitsplan ist nicht im Zusammenhange dargestellt, sondern es ist immer bei Be-

sprechung der in jedem einzelnen Arbeitsgebiete ausgeführten Arbeiten angegeben, was ferner

geschehen soll.
IS 9

 

 
