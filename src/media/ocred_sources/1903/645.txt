 

 

 

 

 

I’emploi de l’acier-nickel, d’une part, le rapprochement de la nivelle et de l’axe
optique, d’autre part, ayant rendu pratiquement nögligeables les erreurs dues aux variations
de la temperature, on a fait construire, en 1900, un nouvel instrument (fig. 7 et 8) dans lequel
ces dispositions sont definitivement consaerdes. La nivelle y est encastree dans le tube m&me de la
lunette et fait corps avec celle-ci; les deux organes sont entour&s d’une enveloppe calorifuge
presentant, au-dessus de la fiole, une ouverture partiellement protögde par une glace in-
clinee transversalement, de maniere & renvoyer de cöt& et vers le bas l’image de la bulle.
Les extr&mites de cette image sont reprises & leur tour et retourndes dans la direetion de
l’oculaire par deux prismes & reflexion totale disposes dans un tube accol& & la lunette et
jouant le m&me röle que les prismes bi-röfleeteurs du niveau du service du Nivellement
general de la France; un cilleton speeial, plac& A cöt& de l’oculaire et A une distance
de celui-ci egale a l’&cartement des yeux, permet d’observer la bulle avec un eil tandis
que, de l’autre, on examine l’image de la mire.

Les autres caracteristiques de ce niveau sont les suivantes:

Nivelle: — Valeur angulaire d’une diion ...... 22 23
Ouverture de lobjeetit . 2. z.. 2 9

Dunette: X Distance focale de l’objectif. . . ... on 2 Alm
Grossissement de la lunette: 32 ou 43 fois, selon Poaulairs el

Methode: — WL’instrument qui vient d’etre deerit ne se prete pas aux doubles

observations symetriques qui n&cessitent le retournement de la lunette sens dessus dessous
et de la nivelle bout pour bout; on opere necessairement par la methode de l’&quidistance

pour &liminer les erreurs instrumentales.
Chaque visde comporte la leeture des 3 fils horizontaux dquidistants dont le reticule

est pourvu.

MEXIQUE.
Mire. — La mire employ6e jusqu’alors est du systeme Porro.
Niveau. — Le niveau, construit par GURLEY, comporte une nivelle invariablement

fixe & la lunette et divisee du centre vers les extr&mites.

Chaque division de la nivelle & une largeur de 2,5 mm et une valeur angulaire de
18”, ce qui correspond & 29 metres pour le rayon de courbure de la Äole.

La lunette est pourvue d’un reticule comportant deux fils en croix.

Les caracteristiques sont:

Ouvertüre utile de Pobjeetit. . . ... 2. u... .2.290m

Distance focale de l’objedtit . ... 2... m

Grossen oo: i : A .  . 4lfois
Methode. — Deux mires sont Saplaglen simelianeine.

On observe, en prineipe, dans l’horizontale, mais le niveau ne comportant pas de
vis de fin calage, on note l’inelinaison de la lunette toutes les fois que les lectures faites
aux deux extremites de la bulle ne sont pas rigoureusement 6gales et on caleule ensuite la

reduction & l’horizon.
42

 

n
H
I
|
J
4
=

 

nn
