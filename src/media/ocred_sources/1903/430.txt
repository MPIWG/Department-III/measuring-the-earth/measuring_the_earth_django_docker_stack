 

y
|
I
ü
|

Se

sa

 

F

v
u
a
W
|
|
u

 

 

abgeleitet worden. Über die Entstehung dieser Formel hat Herr Prof. Heımerr bereits im
Bericht von 1900 (8. 370, Anmerkung 2) einige Mitteilungen gemacht; ausführlichere Dar-
legungen enthalten die Sitzungsberichte der physikalisch-mathematischen Klasse der Berliner
Akademie der Wissenschaften vom 14. März 1901, 5. 828 u. f. Hier mag zur allgemeinen
Orientierung folgendes daraus hervorgehoben werden.

Zur Ableitung seiner neuen Formel verwendete Herr Prof. Hvuımerr nur Festlands-
und Küstenwerte von g, die er zunächst beide gesondert behandelte und dann in geeigneter
Weise kombinierte. Als Küstenwerte galten solche g, die in der Nähe der steilabfallenden
Tiefseektisten der Kontinente und grossen Inseln beobachtet sind, während die an ausge-
dehnten Flachseeküsten gefundenen g als Festlandswerte angesehen wurden. Schwerebestim-
mungen auf kleinen isolierten Inselpfeilern im tiefen Wasser, sowie auch solche auf hohen
Bersgipfeln, wurden nicht verwertet, da sie erfahrungsgemäss Anomalien aufweisen, die sich
zur Zeit nicht genügend berücksichtigen lassen, um diese Bestimmungen für die Ableitung
einer Schwereformel brauchbar zu machen. Alles verwendete Beobaehtungsmaterial war vorher
auf das im Bericht von 1900 definierte Wiener Schweresystem !) bezogen und, durch An-
bringung der Höhenreduktion, wie in freier Luft aufs Meeresniveau reduziert worden. Beide
Systeme, Festlands- und Küstenstationen, ergaben nahezu dieselbe Abhängigkeit von ® und
unterschieden sich nur in den Äquatorkonstanten ; sie wurden dann, unter Berücksichtigung
dieses Unterschiedes, in einer neuen Formel für Festlandsstationen vereinigt.

Die Abweichung der neuen Formel von der alten besteht hauptsächlich in einer
Vergrösserung des Äquatorwertes von y, um 0.046 em., die von der Einführung des Wiener
Schweresystems herrührt; sonst stimmen beide Formeln gut überein und ergeben für die
zugehörigen Normalsphäroide nahezu dieselbe Abplattung, was um so bemerkenswerter ist,
als der neuen Formel ein weit umfassenderes, homogeneres und räumlich günstiger ver-
teiltes Beobachtungsmaterial zugrunde lag als der alten. Der Koeflizient des hinzugenom-
menen Gliedes mit sin: 2& ist nach Darwın und WiscHert angesetzt worden, da er sich
aus der Hrı.veurr’schen Ausgleichung zu unsicher ergab.

Zur Umrechnung von x,(1884) auf y,(1901) kann das folgende Täfelchen dienen,
dass den Unterschied beider Formeln in Tausendstelcentimetern giebt.

r, (1901) — x, (1884) — — Tafelwert.

 

4°

42

 

 

92

 

|

|
el zo || % i n | 15
|

 

 

 

 

10 |45 | 44 | 44 | 44 | A& | 48 | 43 | & e
30 | 42 | 42 | 4 10 39), 39120
80, 395381 3858 36 | 36 | 30

35 | 40
35 | 50

40 | 36 | 36 | 35

44 | 44 | 40 | 40
s

|
Ss 37 EB
50 | 35 | 35 | 3% | 34
|
|

60 | 35 | 35 | 35 | 35 | 35 | 35 36 | 36 | 36 | 36 | 60
70. | 36 | 37 | 37 | 37 | 37 | 37 E 37 | 37 | 37 | 70
sol as | ss | ss | ss | as I 38 | 38 | 38 | ss | 38 I so

1) Verhandlungen der XIII. Allgemeinen Konferenz ete. S. 378.

 

JE

di

duch

Vento ana -MblaLAU Marl

hc pam bb u ih bh Ra

 
