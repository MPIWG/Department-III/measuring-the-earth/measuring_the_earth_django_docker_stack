 

135

Eine direkte Berechnung von y, (1901) kann mit Hilfe der nachstehenden Tabelle
ausgeführt werden, wobei man die zweiten Differenzen berücksichtigen muss, wenn man
das Resultat auf + 0.5 Einh. der letzten angesetzten Dezimale sicher haben will. i

 

 

 

 

 

 

 

 

® Yo (1901) ® 70 (1901) | 9 | 9 (1901) i

li

em | cm em ’ ]

0° 978.046 , 30° 979.337 60° Su, 1

1 048 , 3 446 5, 61 982.008 ,, |

2 052 , 32 497 9 62 084 u |

3 060 |, 33 579.83 63 8 ı

| Or 34 662.2 64 231 7 |

5 085 35 746 85 65 su,

6 4102 ,, 36 I 66 370 5,

m 193, Sn AL 67 436 95

8 Are 38 980 005 gg 68 501 2 |
9 AU sg | 093 39 69 So3
10 207% 40 482 a 70 6a
a Da AA DE ai 679 „,
| 12 269 35 49 Seen 72 TEA
: 15 07 a0 43 Al 90 73 186.)
r 14 348 4 Ak 5A 74 836 4,
| 15 399 4 45 632 75 883 4
4 16 | 438 4 46 722 9ı 76 Du.
ae AST 53 47 3 9 Mn 968 38
| as | 539 „, 48 903 90 78 983.006 35

19 593 5 49 | 993 59 79 049 ,, )

5 20 650 5 50 981.082 80 07 5 |
i a 109 1 Bu ATA 59 a 104 5,
! 22 770 64 59 260 31 82 a
Da 834 55 53 N 5 83 Ass
94 900 53 54 AZA g6 84 Au
25 968 10 5 920 8 85 10
26 979.038 +, 56 en 86 200,
97 110 57 688 9 87 217,
38 Asa 58 ON 88 225 ,

: 2 260 „, 59 Sl 89 230 5 |

= 30 | 337 60 930 90 22

s Ausser der Einführung von y, (1901) ist noch die einheitliche Berechnung der

- reduzierten Grössen g, und g”, in unsern Übersichtstabellen zu erwähnen. Die Höhenreduk-

= tion von g aufs Meeresniveau, zunächst wie in freier Luft, wird in den meisten Ab-
handlungen über Schweremessungen nach der Formel g,— y—=2gHjR ermittelt; hier ist
sie überall nach dem genaueren Hrrmerv’schen Ausdruck

9% 9 — 1077xX3086H))

berechnet worden, worin H in Metern zu nehmen ist und g,—g in Centimetern erhalten

1) Verhandlungen der XIII. Allgemeinen Konferenz ete. S. 368, und Sitzungsberichte der Berl, Ak. d. W. 1903, S. 651.

 
