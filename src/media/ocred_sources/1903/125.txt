 

 

120

Pendant l’annde suivante, de 1898 a 1899, les maxima et minima des oscillations
diurnes ont ete: & Alicante 0.323 m. et 0.022 m., & Santander 3.888 m. et 1.265 m. et
a Cadix 3.185 m. et 0.845 m.

Par ces observations le niveau moyen de la mer a subi les variations suivantes:
a Alicante 0.6 mm., & Santander 1.2 mm. et a Cadıx 2.0 mm.

Copenhague le 7 Aoüt 1903.

Le Directeur genfral de U Institut geographique
et statistique, delegue d’Espagne

FrAncısco MARTIN SANCHEZ.

 

3
ü
z
S
®
Y

AT

aha hr a ln

 
