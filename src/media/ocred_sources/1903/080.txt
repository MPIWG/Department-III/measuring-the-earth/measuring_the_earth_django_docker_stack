 

|
t
5

55 TerweraTTTORIREEIR een

75

General v. Zacharie dankt in folgender Weise:
Meine Herren,

Ich danke bestens für die grosse Ehre die Sie mir hier erwiesen. Doch habe ich
einige Bedenken diese Würde anzunehmen, da ich den parlementarischen Discussionen fern
stehe und auch kein Redner bin. Ich hoffe jedoch mit Hülfe unseres Präsidenten meiner
Pflicht in unserem Bureau nachkommen zu können. Ich danke Ihnen herzlichst.

Der Herr Präsident schlägt vor noch in dieser Sitzung die Finanzeommission zu er-
nennen, damit diese sofort mit ihren Arbeiten einen Anfang machen kann. Er macht darauf
aufmerksam, dass alle Delegirten die an später zu bezeichnenden Daten stattfindenden Sitzungen
dieser Commission beiwohnen können. Auf seinen Antrag werden in diese Commission
gewählt die Herren Förster, Darwin, Celoria und Poincare.

Der Herr Präsident theilt mit, dass die zweite Sitzung, Mittwoch 5 August, 9 Uhr
30 Min. stattfinden wird.

Die Tagesordnung ist:

1°. Bericht des Herrn Helmert über die Thätigkeit des Centralbüreaus während
der drei Jahre 1901—1903.

2°. Bericht über den internationalen Breitendienst.

3°, Programm für die Thätigkeit der Centralbüreaus in den nächsten Jahren.

4". Specielle Berichte des Gentralbüreaus.

Landesberichte.

Schluss der Sitzung 3 Uhr 55 Minuten.

 

 

 
