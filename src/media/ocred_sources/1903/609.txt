 

 

 

  

BR ee

 

7

295

 

et, pour chaque base, compares entre eux sur un des segments de la base, d’une longueur
de mille metres, qui servira de kilom&tre t6moin.

4°. L’etalonnage sera fait sur le terrain dans des eonditions meteorologiques se
rapprochant le plus possible de celles que l’on rencontre dans les mesures de base.

Ce plan a &t& rigoureusement suivi. Les resultats qui ont &t& obtenus sont lA pour
attester que l’applieation de ces divers prineipes a permis d’obtenir toute la precision desi-
rable, d’operer avec une rapidit &tonnante et avec une &conomie considerable.

Avec une ©quipe de 5 observateurs et 5 aides, les 9 bases, distribuees le long de
l’are & des intervalles d’environ 2 degres, ont &t6 mesurdes dans l’espace de 6 mois, y compris
les etalonnages faits au debut et A la fin des operations,. La base la plus courte a 6 kilo-
mötres, la plus longue pres de 13 kilomötres: la somme des neuf longueurs atteint 69,2
kilometres. L’erreur moyenne maxima est de ml, l’erreur moyenne minima de a

L’histoire de la geodesie n’offre encore aucun autre exemple d’un tel ensemble de
mesures; il est; digne d’eveiller notre attention.

Il semble d’ailleurs que nous entrons dans une voie nouvelle pour la mesure des
bases et que l’emploi des appareils rapides tend & se generaliser de plus en plus. Le Bureau
international des Poids et mesures s’en preoceupe: vous avez pris connaissance des &tudes
de MM. Bexoir et GvitLtaums sur les fils invar et la preeision qu’on peut en obtenir. Leurs
conclusions sont interessantes A tous 6gards et il semble que l’emploi du metal invar et
Yutilisation amelioree de la methode JÄperın pourront fournir une des solutions du probleme.

Gal Bassor.

 
   

 
