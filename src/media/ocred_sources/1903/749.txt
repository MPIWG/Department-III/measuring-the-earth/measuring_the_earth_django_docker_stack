 

 

|
|
E
|

 

 

=

 

 

415

Über besonders wertvolle und interessante Beiträge zu den Lotabweichungs-Be-
stimmungen ist diesmal auch aus aussereuropäischen Gebieten zu berichten, Ich beginne mit
den englischen Kolonien.

INDIEN.

Seit dem Jahre 1898!) sind in Indien 38 Lotabweichungen in Breite und eine in
Azimut (für Sinpitaung, Great Salween Series, im Betrage von — 6”,10) zu den früher
bestimmten hinzugekommen ?); hierbei wurden die Erdelemente von Everest und die auch
fräher schon benutzten Werte der Breite und des Azimuts für den Ausgangspunkt der
geodätischen Koordinaten in Indien: Kalfänpur beibehalten.

Ausserdem hat Herr Major Lnxox-Convneuam im Winter 1898/99 Beobachtungen
zur Feststellung etwaiger lokaler Lotabweichungen für Kaltänpur angestellt, indem er an
diesem Orte und an 8 Punkten seiner Umgebung Breite und Azimut bestimmte ?). Das
Ergebnis war, dass sich für Kalfänpur aus der Gruppe von 8 Punkten eine nördliche Ab-
lenkung der Lotlinie von 0,60 und eine östliche von 0,22 ergab, während aus sämtlichen
Breitenbestimmungen in Indien eine südliche Ablenkung der Lotlinie von 2”, und aus
sämtlichen Azimutbestimmungen eine westliche von 2,65 ermittelt wurde. Aus den Längen-
bestimmungen folst für die letzte Zahl 3”,00 westlich.

Die bisher angenommenen Ausgangswerte in Breite und Azimut für Kaliänpur
würden mit Rücksicht auf die jetzt anzunehmenden, besten beobachteten Werte Verbesse-
rungen von — 0,29 und von -+-1”,19 erfordern.

Herr Major 8. &. Burrarn, der Superintendent der trigonometrischen Vermessungen
in Indien, untersucht ferner unter anderm in seiner bereits unten angeführten, sehr interessanten
Arbeit: »The Attraction of the Himalaya Mountains upon the Plumb-line in India” ®) ein-
gehend, unter Zuhülfenahme ausgedehnten Materials, die Fehler, mit denen die Ausgangs-

1) Vergl.: Verhandlungen in Stuttgart, 1898, S. 273.

2) Verhaudlungen in Paris, 1900, I. Teil. Bericht über die astronomisch-geodätischen Arbeiten in Indien. Von
S. G. Burraro. S. 107—139, — S. G. Burkaro. The Attraction of the Himalaya Mountains upon the Plumb-line in
India. Dehra Dün 1901. (Survey of India Department. Professional Paper N° 5); — Extracts fiom Narrative Reports
of the Survey of India for the Season 1900—1901. Prepared under the Direction of Sr. G. C. Gorz, Surveyor general
of India. Calcutta 1903. S. 11—21; — Endlich wurde noch ein handschriftlich zur Verfügung gestellter os on
the geodetic Work done in India Fon 1900 to 1903” benutzt.

3) Ein Auszug dieser Arbeit findet sich in der Abhandlung: $. G. Burrarn. The Attraction of the Elimellge
Mountains upon the Plumb-line in India. Monthly Notices of the Royal Astronomical Society. Vol. LXII. London 1902.
S. 180—186; mit diesem Gegenstand beschäftigt sich auch eine Abhandlung von O. FisHER. On Deflections of the Plumb-
line in India. Philosophical Magazine for January 1904. S. 14—25.

 
