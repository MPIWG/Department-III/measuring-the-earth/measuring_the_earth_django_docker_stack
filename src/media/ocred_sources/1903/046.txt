 

Fan en TER ns

een

Fremen een

N

4]

M. Heuvelink lit les rapports suivants. Voir Annexe A VIlla et A VIIID.

M. Helmert comme rapporteur sur les triangulations, ayant recu de M. Darwin le
rapport de Sir Davın GiLL coneernant les travaux geodesiques le long du meridien de 30° de
longitude en Afrique, donne le resume suivant de ce rapport.

Les triangulations dans la colonie du Cap et dans le Natal, depuis le parallele de
35° jusqu’au parallele de 28°, pour la jonction du Cap au meridien de 30° sont termindes.
Vient alors le Transvaal; dans cette contr&e jusqu’au parallöle de 22° on n’a pas encore
fait de mesures, mais M. Gırt a organise les operations de sorte qu’on pourra bientöt les
commencer. Plus au nord dans la Rhodesia, entre les parallöles de 22° et de 16°, on a
termind en partie les mesures & peu pres pour 4°, dans la partie boreale il reste encore
& mesurer un polygone, et au sud une chaine de 2° de longueur. Dans cette contree on a
rencontre beaucoup de diffieultes; d’abord par les fiövres dont surtout le chef de l’expedition
a eu beaucoup & souffrir, ensuite par suite du transport des instruments et de tout ce qui
stait necessaire & ’expedition, qui ne pouvait plus se faire au moyen de chariots a boeufs mais
au moyen de porteurs. On vient & present d’organiser les operations depuis le Zambesi
jusqu’au lae de Tanganyika, qui ont öte confiees ü& M. le Dr. Rusın, lequel a pris part &
l’expedition du Spitzberg. A la fin du mois d’Avril l’expedition est partie du Cap pour se
rendre par bateau a Ohinde.

Comme il s’agit des triangulations entre les paralleles de 16° et de 8° a 9°,
une longueur d’environ 7°, & peu pres la m&me &tendue que l’are de meridien mesure &
l’Equateur, il s’6coulera quelques anndes avant que ces op6rations puissent &tre termindes.
On aura done en Allemagne et dans l’Etat libre du Congo le temps necessaire pour preparer
les travaux destinds A continuer les triangulations vers le nord.

M. Helmert fait: connaitre que, conformement aux resolutions de l’Association geo-
desique internationale et de l’Association des acaddmies, on a invit& les gouvernements de

sur

‚’Empire allemand et de l’Etat libre du Congo & continuer les mesures le long du lae de

Tanganyika et encore plus loin jusqu’& la latitude de 1° 8. Jusqu’& present les difficultes finan-
eißres ont empäche les autorites allemandes de s’occuper de cette affaire, mais il espere que, gräce
a V’appui de l’Acad6mie des sciences & Berlin, on la prendra en main dans l’annde prochaine.
Il a eu dejä l’oecasion de se procurer des donndes fort interessantes sur les necessites finan-
ciöres, scientifiques et techniques de l’expedition, gräce AM. le capitaine Hrrmann qui a dirige
des triangulations dans les parties voisines des frontieres au sud et au nord du lac Taganyika.
Il ne doute pas qu’en Allemagne les operations seront organisdes et, d’apres ce qu’il a appris,
\’Etat libre du Congo serait pröt & favoriser une telle entreprise scientifique.

Quant aux mesures des bases elles seront faites au moyen d’appareils JÄnerın. A 2° au
sud du Zambesi pres de Salisbury une base de 13!/, milles anglaises a te mesuree en 1900
au moyen de fils qui avant et apr&s les mesures ont te etalonnes au Cap. M. Helmert ajoute
qu’un grand nombre de fils ont dt& employees dans ces mesures au nord de Rhodesia, entre
autres deux fils d’invar.

M. le Seeretaire donne un resume en francais du rapport de M. Helmert.

M. Darwin ajoute que Sir Davın GıLL a demande au gouvernement russe de lui

= 6

 
