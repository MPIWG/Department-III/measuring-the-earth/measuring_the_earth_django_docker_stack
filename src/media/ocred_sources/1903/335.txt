44
Ile
Determination de la pesanteur sur I’Oedan Atlantique.

Ces determinations ont &t6 ex&eutdes entre Hambourg, Lisbonne et Rio de Janeiro
par M. le Doeteur H2cKer, qui dans la Meteorologische Zeitschrift 1901, p. 424 et suiv. et
dans la Zeitschrift für Instrumentenkunde 1901 p. 133 et suiv. a public un rapport sur les
comparaisons entre les hypsomötres (thermomötres & ebullition) et les barometres a mercure
installöes au laboratoire, tous les instruments etant en repos. Ce travail a ö&te mentionne
dans mon rapport de 1900. D’apres ces publications l’erreur moyenne d’un resultat obtenu
par des observations pendant une journde, reduit en intensite (aceeleration) de la pesanteur,
ou bien l’erreur moyenne de Ag est & 0.019 em. Quoique les mouvements du bateau em-
p@chent d’atteindre & bord une telle preeision, les informations que nous avions prises
nous faisaient esp6rer que, si les mouvements du bateau n’staient pas trop forts, on pourrait
obtenir des r6sultats qui ne seraient pas sans valeur. Cette attente 6tait fondee sur deux
eirconstances favorables: 1° que dans les baromötres eonstruits par FuEss, d’apres les indi-
cations de M. Hucker, les oscillations du mereure causdes par les mouvements du barometre
staient beaucoup diminudes, et qu’on pouvait espörer que les oscillations seraient syme-
trigues par rapport & la position normale, 2° que M. Hscker voulait utiliser non seule-
ment les hauteurs du baromötre lues & l’eil, mais aussi les hauteurs enregistrdes au moyen
de la photographie (voir le rapport suivant de M. Hecker).

M. Hncker choisit pour ces exp6riences un bateau ä vapeur de la ligne Hambourg-
Lisbonne-Rio de Janeiro, puisque, d’apres les indications de la „deutsche Seewarte”, pendant
les mois de Juillet et d’Aott la mer sur ce trajet est fort tranquille, beaucoup plus que
sur d’autres lignes transatlantiques pendant une periode queleonque de l’annee.

Apres avoir acquis la convietion, d’apres toutes les experiences preparatoires, que le
suceds n’6tait pas improbable, et quoique plusieurs personnes me le deconseillassent, je
proposai au bureau de notre association de mettre & ma disposition la somme de 6000 M.,
qui, d’aprös la r&solution prise dans la röunion de la commission permanente a Lausanne,
avait 66 zöservde pour de telles recherches. Cette proposition fut acceptee.

En outre le „Hamburg-Südamerikanische Dampfschifffahrtsgesellschaft”” accorda, &
ma demande, & M. Hscker, pour lui-m6me et pour ses appareils, libre passage aller et
retour. L’Association g6odösique internationale doit tous ses remereiments & la direetion
de cette soci6t6, d’autant plus que M. le docteur Hecker a trouve A bord des bateaux
toutes especes de facilites pour ses operations.

A cöt6 de sa täche prineipale: determiner l’intensit€ de la pesanteur en pleine mer,
M. Hscker a aussi determing Vintensit6 relative de la pesanteur au moyen d’observations
de pendules aux observatoires de Rio de Janeiro, de Lisbonne et de Madrid. Comme nous
Vavons dit, ces döterminations ont reussi.

C’est non seulement aux directeurs de ces observatoires que nous devons exprimer
notre gratitude mais aussi au comte pD’AyıLa & Lisbonne et A M. F. pm P. Arrınaca &
Madrid, qui ont fait leur possible pour aplanir les difficultös que M. HEcKER pourrait ren-
contrer et pour lui pröparer un aceueil favorable.

s
3:
x

 

ran

ik

I tu In

Bern

in, vashmndhh ht x bel
