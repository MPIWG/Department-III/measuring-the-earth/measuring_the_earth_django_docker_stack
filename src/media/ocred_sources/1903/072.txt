|
|

 

 

67
5°. Von Prof. Celoria (4 October 1902) die Mittheilung dass der Minister des
öffentlichen Unterrichts an Stelle des verstorbenen Generals Ferrero ihn zum Mitglied
der permanenten Commission ernannt hat.
6°. Von der königlichen spanischen Botschaft (11 Juni 1903) die Nachricht, dass
Herr Franeois Martin Sanchez, Excellenz, Generaldirector des geographisch-statistischen
Instituts zum ständigen Delegirten bei der Erdmessung bestellt worden ist.
7°. Vom Reichskanzler des deutschen Reiches ein Brief vom 25 Juni 1903 Ab-
schrift enthaltend eines Schreibens der russischen Botschaft in Berlin mit der Nachricht

dass General Artamonoff, Chef der topographischen Section des Generalstabes zum Mitglied.

der permanenten Commission bestellt worden ist.

8°. Vom auswärtigen Amte der französischen Republik (25 Juni 1903) die Mit-
theilung dass der Präsident der Republik Herrn Darboux, Mitglied des Instituts, zum
Delegirten bei der internationalen Erdmessung ernannt hat.

9°. Vom Reichskanzler des deutschen Reiches die Nachricht (2 Juli 1908) dass
zum Delegirten der Generalkonferenz der internationalen Erdmessung bestellt worden sind:

1’ an Stelle des Generals von Oberhoffer, der Chef der königlichen Landesauf-
nahme, Generalleutnant Steinmetz in Berlin.

2° der Direetor der Sternwarte, Professor Dr. Schorr in Hamburg.

Ferner empfing ich durch Vermittelung des deutschen Reichskanzlers (12 Juni 1908)
die Mittheilung, dass Prof. DARWIN als Delegirter von Grossbritannien an der 14. General-
konferenz theilnehmen wird, und von der kaiserlich deutschen Gesandtschaft in den Nieder-
landen ein Schreiben vom 13 Juli 1903 folgendes Verzeichnis der Delegirten des Reichs zur
14. Allgemeinen Konferenz der internationalen Erdmessung enthaltend.

1. Dr. Nagel Dresden. 8. Dr. Freiherr von Richthofen Potsdam.
2. Dr. Förster Berlin. 9. Dr. Max Schmidt München.

3. Generalleutnant Steinmetz Berlin. 10. Dr. Haid Karlsruhe.

4. Oberst Matthias Berlin. 11. Professor Fenner Darmstadt.

5. Dr. Helmert Potsdam. 12. Dr. Schorr Hamburg.

6. Dr. Albrecht Potsdam. 13. Dr. Becker Strassburg i. Elsass.

7. Dr. Börsch Potsdam.

Wir bedauern es, dass wir die Herren General v. Oberhoffer und General v.
Stubendorff nicht mehr in unseren Konferenzen sehen werden. Letzterer hat unter der
neuen Uebereinkunft regen Antheil an unseren Arbeiten genommen und gehörte unstreitig,
sowohl durch seine Stellung als auch durch seine persönlichen Eigenschaften zu den ersten
unter den Delegirten. Mehrere Male hat er unserer Association wichtige Dienste geleistet
u. a. durch seine Bemühungen zu Gunsten der Einrichtung der Breitenstation in Tschardjui.

General v. Stubendorff hat uns angezeigt dass er, zum Mitelied des Kriegsraths
ernannt, als Chef der topografischen Section des Generalstabes ausgetreten sei.

Aus den verschiedenen Mittheilungen geht hervor dass die Stellen der ausgefallenen
Collegen grossentheils durch neue Delegirte eingenommen sind, und wir begrüssen hier
diejenigen welche sich zum ersten Male an unseren Arbeiten betheiligen werden.

 

4
3
4
4
4

engerenmennenganenne immensen mens

 
