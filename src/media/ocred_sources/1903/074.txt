69

 

2° zugleicherzeit ihre Meinung kundzugeben ob auch der Präsident auf dem Wege |

der Korrespondenz zu wählen, oder ob diese Wahl während der nächsten General-konferenz 1
in 1903 vorzunehmen sei. I
Der ständige Secretär : ;

H. G. VAN DE SANDE BAKHUYZEN. |

Die Mitglieder der permanenten Commission von 19 der 21 betheiligten Staaten
(alle mit Ausnahme von Belgien und Serbien) haben das Circularschreiben beantwortet und
ihre Voten eingesandt.

Wie ich den Delegirten durch Cireular von 38 November 1902 mitgetheilt habe, fasste
die permanente Commission mit 15 gegen 4 Stimmen den Beschluss, die Wahl des Präsi-
denten bis zur Generalkonferenz im Jahre 1903 zu vertagen, und ernannte mit 18 Stimmen
gegen 1 für Herm Darwin, zum provisorischen Vicepräsidenten Herrn General Bassot.

Das Resultat der Abstimmung ist den diplomatischen Vertretern der betheiligten
Staaten in Berlin mitgetheilt worden, und General Bassot hat mir geantwortet dass er
nach erhaltener Ermächtigung seiner Regierung das Amt eines provisorischen Vicepräsi-
denten annehme. :

Nach meiner Ernennung zum ständigen Seeretär, versprach Dr. Hirsch mir die
der Association gehörenden Briefe und sonstige Schriftstücke zukommen zu lassen. Da
ich jedoch nichts empfangen hatte, wandte ich mich nach Dr. Hirsch’s Tode an Herrn
Prof. Tripet in Neuchatel der angewiesen war den Nachlass zu ordnen, er hat die Güte
gehabt mir die betreffenden Schriftstücke zu übermitteln, und nach Empfang habe ich die-
selben fast alle zur Aufbewahrung an das Centralbureau in Potsdam geschickt.

In der Pariser Konferenz wurden einige Beschlüsse gefasst deren Ausführung nach
Art. 2 der Uebereinkunft dem Präsidium übertragen war; ich erlaube mir dieselbe hier
kurz zu erwähnen.

In der letzten Sitzung erklärte Herr Geheimrath Helmert, dass die von dem ver-
storbenen Herrn Prof. Börsch zusammengestellte geodätische Bibliographie vervollständigt
werden müsste, aber dass das Centralbureau keine genügende Anzahl wissenschaftlicher
Hülfsarbeiter zur Verfügung hätte. Er habe jedoch gehört dass Prof. Gore aus Washington,
der ebenfalls eine Bibliographie herausgegeben habe, die Absicht hätte diese zu vervoll-
ständigen und eine neue Ausgabe derselben zu besorgen; die United States Coast and
geodetic Survey wäre geneigt diese Veröffentlichung zu bezahlen, wenn die Erdmessung
diese neue Ausgabe wünschen sollte.

Dieser Erklärung gemäss hat die Konferenz diesen Wunsch geäussert, der dem Herrn
Gore mitgetheilt worden ist, und wir hören mit Freude dass Prof. Gore hier die neue
Ausgabe vorlegen wird, die den Geodäten nützlich sein wird.

Nach einer Discussion über die Verwendung eines Jäderin-Apparats mit Nickel-
stahldrähten für Basismessungen, stellte Herr General Bassot den Antrag: „Die inter-
nationale Erdmessung spricht den Wunsch aus, dass das internationale Mass- und Gewichts-
bureau alle nöthigen Versuche zur Vergleichung der Messdrähte vom System Jäderin

TE FRE"

Ferner

Art

Trap ren gti

5

 
