Fan

er

ont rivalise de zele et d’energie dans l’accomplissement des oguvres qu’ils poursuivent.

Dans le nord, e’est la Suede et la Russie, qui ont commence et termine de concert
la mesure d’un are de 4 degres et demi sous la latitude moyenne de 79 degres, latitude
la plus boreale qu’il soit humainement possible d’aborder pour une operation de ce genre.

Sous l’&quateur, c’est la France qui a recommence la mesure de l’are de Quito et
la poursuit en ce moment en lui donnant un developpement de six degres, vous verrez au
prix de quelles difficultes.

Dans l’Afrique meridionale, c’est la Grande Bretagne qui pr&pare le grand are qui
s’etendra du Cap & Alexandrie.

Je devrais encore @numerer les grands arcs qui se mesurent dans les Etats-Unis-

d’Amerique, et dans les Indes anglaises, ainsi que les jonctions si importantes des iles au
continent, comme celles de Malte et celle de la Sardaigne.

Ce sont la, Messieurs, des auvres primordiales, dont les resultats seront du plus haut
intöröt pour notre Association. Nous les saluons avec orgueil et nous felieitons hautement
les &tats, qui ont assum& de telles täches, de l’initiative qu’ils ont prise.

Vous verrez aussi, MM., par le rapport du Bureau central, comment le probleme
des latitudes s’est preeise et reclame de nouvelles &tudes en associant aux travaux des
stations internationales le eoncours que pourront donner les observatoires astronomiques
places sous d’autres latitudes.

MM., cet apercu general de notre activitE ne vous fournit-il pas une preuve nou-
velle du bienfait de notre Association? N’avons-nous pas ä& nous louer de l’Emulation qui
s’empare des &tats associ6s, au grand proft de notre @uyre, et ici aujourd’hui ne pouvons
nous par entrevoir les grandes conquötes scientifigues qui en rösulteront et preeiseront de
plus en plus les dimensions et la forme de notre planete?

Poursuivons done notre täche avec entrain; consacrons lui toute notre ardeur.
L’entreprise sera longue, car, si nous avons dejäa beaucoup fait, il reste encore beaucoup ä
faire. Nous verrons, nous ou nos successeurs, de nouveaux problömes se poser. Qu’importent
les ouyriers, pourvu que l’institution subsiste? Notre Association, depuis 40 ans qu’elle existe,
a deja subi de pertes sensibles, surtout dans les dernieres annedes: d’autres sont venus:
nous disparaitrons & notre tour — d’autres viendront — mais notre ceuyre resistera et c'est
& la rendre permanente que nous devons nous consacrer pour le grand proüt de la science.

MM. je declare ouverte la 14© Conference de l’Association geodesique internationale.

Sur la proposition de M. le Prösident la seance est suspendue pendant un quart d’heure.

A la reouverture de la sdance M. le President indique l’ordre du jour de cette
seance, lequel portera:

1°. Rapport du Secretaire perpetuel.

20. Election du President et du Vice-prösident de l’Association.

M. le President donne la parole au Secr6taire perpetuel, M. van de Sande Bakhuyzen
pour la lesture de son rapport sur l’activite administrative du Bureau.

M. van de Sande Bakhuyzen s’exprime comme suit.

Se en

 
