62 Ir TFT TS een 0 |

 

ee

variation du barometre pendant les observations fut adopte egal & — 0.25. Ces deux
dernieres valeurs s’accordent assez bien avec les rösultats suivants de la vompen. aylon.

La röduction des donndes obtenues pendant le voyage de retour dtant encore im-
possible, faute de temps, il nous semblait utile d’admettre pour le moment que l’anomalie de
la pesanteur avait en moyenne la möme valeur pour les observations faites A une petite pro-
fondeur de la mer dans les h&misphöres bordal et austral et de caleuler d’apres cette hypo-
these la valeur du coöfficient «. On formait alors d’aprös les donndes du Tableau I les
quations pour v, en y ajoutant, pour les observations faites & une grande profondeur de la
mer, un terme -- %,. Les stations oü la profondeur de la mer etait entre 1000 et 1500
metres n’entraient pas en ligne de compte.

La compensation fut faite söpardment pour la moyenne des deux barometres ordi-
naires et Di chacun des barometres enregistreurs, tant pour le groupe I que pour le
groupe II des thermomötres. Le tableau II donne le r&sultat de ces compensations.

Eapleau Ile

Resultat des compensations.

 

 

| Groupe I des thermometres | Groupe II des thermometres | Moyenne |
| 8 || a a one
) Bar. vis. | Bar. enr. I |Bar. enr.Il|| Bar. vis. | Bar. enr. 1 |Bar.enr, II | generale

 

Valeurs des constantes.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

mm mm = | mm || mm mm | mm | mm
hy 0043 oo2 0 u || + 0.012 .— 0.04. — 00%
a 0008 | 1 0003 + 0.008 — 0.008 | — 0000| oo
b 009. 00 0138 | | £ 0.046 | £ 0.018 | + 0.053 || — 0.037
c | — 0188 | + 0280 | — 092 || — 0.274 | — 0.005 Dr 0.048 | — 0.195
| |
ha | + 0080 | + 0.103 | + 0.095 || — 0.042 | + 0.029 | + 0.0441 || + 0.019
| $ 0.022 | + 0.043 | # 0.040. || & 0.085 | # 0.035 | # 0.050 || + 0019
Ecarts, obs.-caleul.
Dr mm mm | mm | mnı mm mm
Juillet 28 I 2 002 — 003 +00 | 0 "co rn
sm, on _ 083 | 00 0 002,.7.000
29 S —_ 0.03 0 ao . u
31 7 0.06 en — 007 | 2009 ı om vo
An Sn 100.2 .007 200 0o.0 a n 2
Ba la. — ‘+ 002 | ..012 | 000
M | +04 | #013: 700 | 200 oo
„Ss = 00% — 006 | — 0.04 || — 005 | — 0.03 | -+ 0.08 |
5m 2000 — 004. |-08 |) 008 00 200
5 + 0.0 00 = | 008 | 001 _— |
6 = .0.03 — 0.03 | —0.02 | + 001 |+ 003 |+ 0.00 ||
7M — 0.03 040 |. 005 | —- 04 +00) 2008
78.1 —006 7004 +00. oo 2090 , 007
gm 2000 0.0 ı 0% | 08 20a od
Ss 0.0.08 — 2.002 |, 120405 an 2.0.03
I !

 

 

 

eu

ne

 

 

 
