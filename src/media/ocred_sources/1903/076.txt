een WETTE Te:

Ä
3
E
E
5
R
F

Or

INTERNATIONALE ÜERDMESSUNG. Parıs—Leiden den 30. März 1903.

Während der Sitzungen der letzten Konferenz, in Paris 1900, sind als Ort der
nächsten Versammlung Kopenhagen, Budapest, Cambridge, Haag und eine der Städte der
Vereinigten Staaten von Amerika genannt worden.

Das Präsidium, dem nach Artikel 2 der Uebereinkunft die Wahl des Zeitpunktes
und des Ortes der Generalkonferenz obliegt, hat nach Rücksprache mit einigen der Herren
Delegirten, und besonders mit Herrn General von Zacharie, als Ort der Konferenz für
dieses Jahr Kopenhagen gewählt.

Die Sitzungen, zu denen die Herren Delegirten einzuladen wir die Ehre haben,
werden stattfinden im provisorischen Reichstagsgebäude, Fredericiagade 24, in dem Ver-
sammlungssaal der ersten Kammer, der durch gütige Vermittelung des Herrn Generals

von Zacharie zu unserer Verfügung gestellt worden ist. Die Eröffnungssitzung soll be-
ginnen:

Dienstag 4. August 2 Uhr Nachmittags.

Obwohl den Herren Delegirten die Tagesordnung noch zugeschickt werden wird,
glauben wir doch schon jetzt mittheilen zu müssen, dass in der Konferenz vor allem die
Wahl eines Präsidenten an Stelle des verstorbenen Herrn Faye und eines Vicepräsidenten
an Stelle des verstorbenen Herrn Generals Ferrero vorzunehmen ist, da, wie im Circular
vom 3. November 1902 ist mitgetheilt, Herr General Bassot nur provisorisch als Vice-
präsident gewählt worden ist.

Der ständige Secretär : Der provisorische Wicepräsident :
H. G. van DE SAnDE BAKHUYZEN. General Bassor.

Später hat das Präsidium folgendes die Tagesordnung enthaltende Cireular den
Delegirten zugeschickt.

INTERNATIONALE ERDMESSUNG, Parıs—Leien 9 Juli 1903.

Hochgeehrter Herr College,

Durch das Circular des Präsidiums der Internationalen Erdmessung vom 80 März
wurden die Herren Delegirten zum 4 August, 2 Uhr Nachmittags, zur Allgemeinen Kon-
ferenz nach Kopenhagen, Fredericiagade 24, provisorisches Reichstagsgebäude, eineeladen.

} oıh 5 y 5 ’ 8

 

 
