248

 

rendered it necessary to aceurately measure the inclination of each bar at the same time
the wedge intervals were measured. For this purpose a sensitive level was used mounted
on a little mechanism, by means of which the level tube could be raised or lowered at
once with a delicate micrometie screw. The inclination of each bar was entered in the
Recorder’s book with the. other data. This method of measurement was more expeditious
than the first, as much work in staging was avoided, and so far as the after triangulation
test showed was equal in accuracy.

The difference between the two measures on the remeasured portion was 0,308
inches, equal to about 0,15 inches per mile.

The numerical data of the base line are as follows:

Measured Base: 26091,82 feet = 4,942 miles.

Remeasured portion: 11174,29 feet.

Elongation of base by triangulation to Green Hill: 29839,83 ft.

Total length: 55931,65 ft. or 10,593 miles. Visible from end to end.

True azimuth of base: 304° 36° 31". (Bearing of N. end as run from 8. end.)

The measuring bars were assumed to be 10 feet long, their actual lengths in terms
of the Melbourne edition of the British 10 feet standard were as follows:

N® 1: 119,99957 inches;

» 2: 119,99318 inches; and

». 3: 119,99997 inches, at a temperature of 62 Fahr.

The expansion of the base to points in the primary triangulation was obtained
without difficulty, as will be seen on the sketch map of the triaugulation station peak and
Mt. Macedon being reached in the second series of expansion triangles. The primary trian-
gulation was first extended Westwards and Northwards, the western boundary stations being
veached by Mr. A. C. Arzan in July, 1864, the extreme western stations near the sea
being Mount Gambier and Mount Schank in $. Australia. Between these two mountains
was an admirable site for a base of verification, and it was intended to measure one there,
but unfortunately the intention was never carried out for want of means, The sketeh map
of the trigonometrical survey gives the extent of the triangulation, and the effective network.
The later operations of the trigonometrical survey were carried out in the N. Eastern distriets
by Mr. A. Brack, through S. and E. Gippsland by Mr. A. ©. Arzan and Mr. W. Turron.
And as the necessity arose in 1870 for making the boundary between N. S. Wales and
Victoria the triangulation was pushed into N. 8. Wales on the East to include The Pilot,
Kosciusko, and Cape Howe, and mountains in the vieinity of the supposed boundary. This
work was completed and in the hands of the computers in 1871, the true positions of the
stations having been ascertained, the azimuth of a straight line starting from The Pilot
to strike a selected point at Cape Howe was calculated, and in April, 1870, Messrs. Brack
and Attam commenced running, clearing, and marking the boundary line and finished in
March, 1872, Mr. Auzan taking the line from Mr. Brack at Bendoc and producing it to
Cape Howe, where it struck the eoast within 16,8 inches of the marked terminal, completing
a piece of survey work which for diffieulties and for the requirement of skill, energy, and

Lust

und. nahme HÄHIRLT Maillähukı

in a hei.
