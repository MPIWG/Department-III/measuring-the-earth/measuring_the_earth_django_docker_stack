iin

1 gm nl

ie

=
=
x
x

 

XIV. MESSUNGEN DURCH DAS GEOGRAPHISCH-STATISTISCHE
INSTITUT IN MADRID.

Die Memorias del Instituto geografico y estadistico, tomo XI (1899) y tomo XII
(1903), enthalten ausführliche Darstellungen der vom Geographischen Institut in den Jahren
1893—96 ausgeführten absoluten Schweremessungen in San Fernando bei Cadix, La Coruna,
Valencia und Barcelona. Die Ergebnisse dieser Arbeiten sind bereits in den Schwerebericht
von 1900, $8. 288, auf Grund handschriftlicher Mitteilungen aufgenommen worden. Bei
Valencia besteht indes ein Widerspruch zwischen dem Schwerebericht und der Publikation ;
wir stellen deshalb die entgiltigen Resultate für alle 4 Stationen hier nochmals zusammen.

® A H I 6 Beobachter Jahr Memorias

cm em
San Fernando 36°97°.7 6°19..6 30 979.947 000% — To Arcos 1894 EXT SPA
Valencia... 39 298.5 —0 4192 6 -980.105= 0.007 2.6 Mifsut, Aparici 1895/96 7. XI, S. 93
Barcelona .. 4 21.8 +2 104 5 980.326 # 0.005 2.6 Cebrian, Los Arcos 1893 1. XI5 S.995
La Corufa. . 43 22.0 —8 24.5 16 980.550 & 0.006 92.6 Cebrian, LosArcos 1893 T.XI, S. 489

Die geographischen Längen sind dem Schwerebericht von 1900, alle übrigen Daten
den betreffenden Publikationen entnommen worden. Bei San Fernando fehlt die Dichte-
angabe für die Schicht zwischen dem Stations- und dem Meereshorizont; die übrigen Dichte-
angaben werden als Näherungen bezeichnet.

Ausser den vorstehenden Arbeiten wurde 1903 eine absolute Sch werebestimmung

in Valladolid durch Cegrıan und La Rıca vorgenommen, deren Resultat noch nicht be-
kannt ist.

27
