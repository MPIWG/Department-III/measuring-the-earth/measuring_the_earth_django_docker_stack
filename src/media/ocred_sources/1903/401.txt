 

BEILAGE B Vilt.

BERICHT

über einen neuen Komparator der Kaiserlichen Normal-Bichungs-
Kommission zu Berlin,

vVoN

W. FOERSTER.

i Prof. Foerster berichtet über einen neuen Komparator der Kaiserlichen Normal-
Eichungs-Konimission zu Berlin, welcher auch zu Maassvergleichungen für die Zwecke der
höheren Geodäsie dienen kann. Derselbe wird, wie der Vortragende zu erklären ermächtigt
ist, auch gern für die entsprechenden Bedürfnisse anderer Länder zur Verfügung gestellt
werden und eventuell dazu helfen können, das internationale Maass- und Gewichts-Institut
in Breteuil von Detail-Arbeit zu entlasten. Der neue Komparator kann zu Vergleichungen
von 1 Meter bis 4 Meter dienen.

Das wesentlich Neue an den Einrichtungen des Komparators — hauptsächlich von
Herrn Prof. Weinstein herrührend, während die sehr präzise Ausführung von Herrn Mecha-
niker Hans Heele in Berlin geleistet worden ist — besteht erstens darin, dass, ausser den
Maassvergleichungen durch mikroskopische Einstellungen mit dem Auge, auch solche Maass-
vergleichungen ermöglicht sind, welche durch alternierende photographische Aufnahmen der
Lage der Endstriche der beiden Maassstäbe auf einer und derselben Platte ausgeführt werden ;
zweitens in einer, zu anderen als Maassvergleichungs-Zwecken schon mehrfach erprobten,
Art der Regulierung der Temperatur der die Maassstäbe umgebenden Medien auf elektri-
schem Wege.

Die Vergleichung mittelst Photographie geschieht derartig, dass zunächst die beiden
Endstriche des einen Maassstabes unter die beiden photographischen Apparate gebracht, und
dass von diesen Endstrichen unter zehnfacher Vergrösserung zu genau gleicher Zeit eine
Aufnahme auf je einer Platte gemacht wird, wobei die Dauer jeder Exposition nur kleine
Bruchteile einer Sekunde beträgt. Sodaun wird der zweite Maassstab unter die Apparate
gefahren und auf jeder derselben Platten wie vorher je eine Aufnahme gemacht. Die als-
dann durch Ausmessung der Lage der Strich-Aufnahmen mittelst gewöhnlicher mikrometrischer
Einrichtungen bei erneuter fünf- bis zehnmaliger Vergrösserung ermittelte Differenz der

mu Le WhmIaLıı) Mallähıhı | oJ ul

mn

Ialı

Jun haha a nn
