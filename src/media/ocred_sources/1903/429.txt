EFT ST garen 0 ı

IRI

BEILAGE B XII.

BERICHT

über die relativen Messungen der Schwerkraft mit Pendelapparaten
für den Zeitraum von 1900 bis 1903.

UNTER MITWIRKUNG VON

F. R. HELMERT
ERSTATTET VON

E. BORRASS,

Im Jahre 1900 legte Herr Prof. Hsımert der XIH. Alloemeinen Konferenz der
Internationalen Erdmessung in Paris einen Bericht über die relativen Schweremessungen
vor, worin er alle bis dahin bekannt gewordenen Arbeiten dieser Art zusammengestellt und
sachlich erläutert hat )).

Der gegenwärtige Bericht bildet die Fortsetzung des eben erwähnten ; er erstreckt
sich über alle relativen Messungen der Schwerkraft, die in dem Zeitraume von 1900 bis
1903 zur Kenntnis des Centralbureaus gelangten, und enthält auch die Arbeiten, die Herr
Prof. Hermarr bereits in einem Nachtrage zu dem Bericht von 1900, 8. 378 bis 393, mehr
oder minder ausführlich erwähnt hat.

Die tabellarische Zusammenstellung der neuen Arbeiten schliesst sich in ihrer all-
gemeinen Bezeichnung und innern Gruppierung eng an den Bericht von 1900 an, so dass
alle hier vorkommenden Tabellen als zeitliche Fortsetzungen der früheren ersöheine Dem-
entsprechend ist auch die Numerierung der neuen Arbeiten innerhalb der einzelnen Gruppen
im Anschluss an die frühere weitergeführt worden.

Zur Kennzeichnung wiederholt bearbeiteter Stationen ist die Stationsnummer in der
ersten Spalte der betreffenden Tabelle mit einem * versehen, und am Schlusse der Tabelle
mit der entsprechenden Nummer des Berichts von 1900 zusammengestellt worden ; Nummern
ohne * bezeichnen demnach neue Stationen.

Eine systematische Änderung gegen früher haben jedoch die Angaben der 3 letzten
Spalten der Tabellen, in denen die theoretische Sehwonckecnln Y, vorkommt, er-
fahren. Während im Bericht von 1900 der Berechnung von Y, noch die alte Hrımerr’sche
Formel von 1884 zugrunde lag, ist diese Grösse hier nach der neuen Formel von 1901:

cm
= 978.046 $1+ 0.005302 sin?® — 0.000007 sin? 20)

1) Verhandlungen der XIII. Allgemeinen Konferenz ete. $. 139 bis 392.

 

 
