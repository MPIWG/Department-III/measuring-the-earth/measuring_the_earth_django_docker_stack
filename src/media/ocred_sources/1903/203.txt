ll. NıvEnLEMENTS DE DRTAIL.

En 1901, 1902 et 1903, on a ex&cute:
| 1°. 10.800 kilometres de nivellements de 3e ordre;
| 2°. 8.400 kilometres de nivellements de 4© ordre.
Au 31 Decembre 1903, la situation generale des travaux du Nivellement general
de la France sera la suivante;

.

Op&rations effectudes avant 1884.

Longueurs
Nävellements. divers....:0..0. 0 22 00 2 5500 km.
Nivellements Bouurdluaue. ei. 12.200

Nivellements effectu&es de 1884 a 1903 inelus.

Longueurs

: en 11 800 km.
i 2e ode rn 14.300

F 8e -ordre a an 23.100.

| 42 ordre 0.0. 3.300

| Einsemble 200. . 58.200 km.
i

Nivellements restant ä effectuer.

Longueurs

3% ‚Ordre a 11.200 km.

de sordre, m 138.600
Bnsenble. 0.2... KSV
Total general... .. 226.000 km.

= Ill. MARkGRAPHES ET MEDIMARFMÜTRES.

Les observations mar&mötriques ont et& r&egulierement poursuivies, de 1900 a 1903,
dans les 15 stations echelonnees sur le littoral francais.

Toutefois, par suite de divers accidents, l’appareil installe & la Pallice a fonctionne
tres irregulierement en 1901 et 1902.

Au 1er Janvier des anndes 1901, 1902 et 1903, les altitudes du niveau moyen de
la Mediterranee, & Marseille, fournies, depuis leur entree en fonetion, respectivement par
le Maregraphe enregistreur-totalisateur et par les deux medimaremetres installes dans le
möme puits, etaient les suivantes:

gr

Peru Tran a Ir

 
