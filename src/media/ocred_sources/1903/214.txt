|
|
|
|
’

 

|
S

 

190

The detailed report upon the triangulation in 1902 has not yet been published.
It is expected that it will appear in one of the appendices of the Coast and Geodetic Survey
Report for 1903.

WO PUBLISHED REPORTS ON GEODETIO ARCS.

At the time of the last report to the Association, in 1900, the special report on
the transcontinental triangulation !), erossing the United States from the Atlantic to the
Pacific in latitude 39°, had just appeared in print.

This was followed in 1901 by the report?) on the eastern oblique are, extending
from Calais, Me., to New Orleans. La. This, the last work completed by Mr. ©. A. Schott,
well known to the members of the Association for many years as the foremost geodesist
in the United States, was published shortly after his death. The eastern oblique arc is 23!],
degrees of a great cirele, or 2600 kilometers, and is liberally supplied with astronomical
determinations. It is the first instance of the utilization on so grand a scale of an are
obligue to the meridian. The constants of the spheroid derived from this are are, equatorial
radius, a, 6 378 157 meters; polar semi-diameter, b, 6 357 210 meters. These values agree
much more closely with those of the Clarke spheroid of 1866 than with the Bessel spheroid,
and therefore sustained the decision made in 1880 by the Coast and Geodetic Survey to
abandon the Bessel spheroid and adopt the Ularke spheroid of 1866 for use in the United

: — : : 1 : N
States. The compression, = derived from this are, namely, 3048 ° is notable as being

very much smaller than either Clarke’s or Bessel’s value.

At the time of the death of Mr. Schott he had brought the computation of the
triangulation along the western oblique are nearly to completion. 'The necessary force to
carry the work to completion was not available and the computation has since been
advanced but little; It is hoped that the results of this arc, more than 8 degrees long,
parallel to the Pacific coast in California, may be presented to the Association before its next
conference. Statisties showing the aceuracy of the triangulation along this arc have been
furnished to the central bureau for incorporation in a special report on triangulation

THE UNITED STATES STANDARD GEODETIC DATUM.

In the ofiee work of the Coast and Geodetic Survey the most important compu-
tation from the point of view of the Geodetie Association which has been in progress since
1900 has been the reduction of all positions determined by continuous triangulation in the
United States to the United States Standard geodetic Datum. All positions in the United

1) Special Publication N°. 4, of the Coast and Geodetie Survey, „The Transcontinental triangulation and the
American arc of the parallel”, C. A. Schott, Assistant.

2) Special Publication N°. 7, of the Coast and Geodetic Survey, „The Eastern oblique arc of the United States
and osculating spheroid”, ©. A. Schott, Assistant.

All

vd, una. 1 ıma Mala T Mala |

Ba

Ihn aashmlli kn Ma

 

 
