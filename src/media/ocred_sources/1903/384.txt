I WETTE ee

TRITT TIEREN BRITEN TRTITTTT

nr

B

il est visse et rive, puis soude; cette piece est, & son tour, vissee et soudee sur la röglette.
Gräce & ces nenn, nous avons pu realiser un mode d’attache sur la seeurit duquel
nos experiences ne paraissent laisser aucun doute.

Comme on le sait, Yinvar &prouve, dans le cours du temps, de faibles variations,
que l’on attenue ben par un etuvage prealable systömatiguement conduit. D’ailleurs,
de quelque metal qu’ils soient, les fils 6crouis se modifient lentement pendant tres Inst
si on ne les a pas fait vieillir artificiellement par une exposition A des temperatures sufi-
samment &levees. Avant leur emploi, nos fils ont done &t& soumis A un traitement consistant
& les laisser s&journer, pendant ung semaine environ, ü& une temperature voisine de 100°,
puis ä les laisser refroidir tres lentement, de maniere & n’atteindre la temperature ambiante
qu’en trois ou quatre semaines. Autant que cela a &t6 possible, les fils 6taient ensuite eon-
serves pendant des mois & une temperature superieure & 25°, de maniere & completer l’eftet
de cet &tuvage,

Il nous parait utile d’ajouter ici que, dans le cours des deux dernieres annees, plu-
sieurs services geodesiques nous ont config la mission de faire eonstruire pour leur usage
des fils conformes aux modeles que nous avons 6tablis. Or, s’il fallait, en toute occasion,
repeter les longues op6erations qui viennent d’&tre deerites, l’achövement de chaque appareil
exigerait de longs mois. Nous avons done pensd agir dans l’interet general en constituant,
au Bureau international, un d&pöt permanent de fil, que nous soumettons d’abord, en assez
grande quantite, & toutes les actions de nature A augmenter leur stabilite, et que nous
gardons en reserve, de maniere & pouvoir r&pondre rapidement aux demandes qui nous sont
adressees. La SoeietE de Commentry-Fourchambault et Decazeville, qui a partieipe avec la
plus grande liberalitE aux &tudes faites au Bureau sur les aciers au nickel, a bien voulu
mettre & notre disposition les quantites de fil n&cessaires pour l’6tablissement de notre depöt.

Etablissernent d’une base murale. — Nous avions pense tout d’abord que, pour obtenir
un t&moin permanent de la longueur de 24 mötres, gendralement adoptee pour les fils dont
nous nous occupons, il serait necessaire d’installer, sur un robuste support constitude par
une poutre treillagee, une regle mıetallique faite d’un petit nombre de pieces convenablement
aboutees et invariablement r&unies ensemble. Nous avions d6jü &tudie les plans relatifs A cet
appareil, complet€ par des microscopes et des tenseurs constituant par leur ensemble un
veritable comparateur de 24 metres de longueur; mais nous avons 6t6 arrötes dans l’ex6-
cution de ce projet par les frais considerables qu’il aurait entrainds; et nous avons adopte
un projet plus modeste, qui &tait destind seulement, dans notre premiere pensee, a nous
permettre de faire une serie d’exp6riences preliminaires propres & nous donner un simple
apergu de la tenue des fils dans certaines conditions determindes,

L’observatoire du Bureau international comprend, dans la partie anterieure du
bätiment, deux murs paralleles tres &pais, de pres de 50 meötres de longueur, reposant, &
une profondeur de 8 meötres environ, sur le roe qui constitue en partie la colline sur

14

EEE

 

 
