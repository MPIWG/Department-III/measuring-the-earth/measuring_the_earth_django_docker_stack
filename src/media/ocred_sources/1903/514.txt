 

|
|
|
|
|
|
|
|
|
|

 

218

In Russland sind Triangulationen I. Ordnung in neuerer Zeit nicht ausgeführt
worden; es wurde damit begonnen, die grossen Triangulationen, welche zwischen den 47'/,ten
und 52ten Breitengraden sich von der westlichen Landesgrenze bis zur Wolga erstrecken,
einer einheitlichen Bearbeitung zu unterziehen. Die Resultate derselben sind zur Publikation
noch nicht fertig gestellt.

Auch in Schweden hat man sich mit der Revision und Berechnung älterer und
neuer Dreiecksketten, besonders im südlichen Teile des Landes, beschäftigt.

Das topographische Institut der Schweiz hat an das Hauptnetz zur Ergänzung 3
Füllnetze angeschlossen.

In Rumänien werden Erwägungen angestellt über die Ausführung von Ketten so-
wohl im Meridian als auch im Parallel.

Von Griechenland, Portugal und Spanien sind keine Einsendungen erfolgt; in allen
3 Staaten sind die Winkelmessungen für die Hauptdreiecke beendigt. Portugal hat auch die
Azoren trianguliert.

Der augenblickliche Stand der Triangulationen in Europa und Nordafrika ist aus
der Übersichtskarte I zu ersehen.

Die Coast and Geodetic Survey der Vereinigten Staaten von Amerika hat die
Ergebnisse zweier ausgedehnten Triangulationen veröffentlicht. Die eine, der Parallelbogen in
39° Breite, durchquert den amerikanischen Kontinent vom Kap May am Atlantischen Ozean
bis zu Point Arena in Kalifornien und erstreckt sich über etwa 48°/, Längengrade. Be-
merkenswert sind die grossen Sichten, die in seinem östlichen Teile beim Überschreiten
der Gebirgszüge der Coast Range, der Sierra Nevada und der Rocky Mountains erreicht
werden und die bis über 160 km gehen. Die andere grosse Dreieckskette »’The eastern
obligue are of the United States” erstreckt sich, von der kanadischen Grenze im Staate
Maine bis nach dem Staate Louisiana am Golf von Mexico, über etwa 15'/, Breiten- und
22°/, Längengrade. Die seine Endpunkte Calais und New-Orleans verbindende geodätische
Linie hat ein mittleres südwestliches Azimut von 57° 34‘.

Von dem grossen Meridianbogen in 98° westl. Länge von Gr., der nach seiner Voll-
endung von der mexikanischen bis zur kanadischen Grenze gehen und etwa 23 Breitengrade
umfassen wird, ist der mittlere Teil, gegen 12 Breitengrade, bereits gemessen ; die Hälfte
davon etwa ist auch schon ausgeglichen und publiziert worden.

Ausserdem wurden noch eine Kette im Staate Carolina, die in südwestlicher Rich-
tung von dem oben erwähnten schiefen Bogen bis zum Atlantischen Ozean geht, und eine
Dreieckskette in Kalifornien an der Grenze von Mexico beobachtet. Mit der trigonometri-
schen Vermessung von Portorico, der hawaiischen Inseln und der Philippinen ist begonnen
worden. Die Übersichtskarte II gibt den Stand der Triangulierungen in den Vereinigten
Staaten von Amerika an.

Die geodätische Kommission in Mexico hat die Absicht, den amerikanischen Meridian-
bogen in 98° Länge durch Mexico fortzusetzen und zu diesem Zwecke auch bereits mit
der Rekognoszierung begonnen. Vorläufig hat sie ihr Hauptobservatorium in Tacubaya mit
Puebla, das sehr nahe diesem Meridian liegt, durch eine Parallelkette in 19° 30’ Breite ver-

 

=
&
=
S
ü

ul

null.

nn mhk ih Au

i
i
|
|

 
