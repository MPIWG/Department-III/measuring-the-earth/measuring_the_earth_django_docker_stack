 

en

TE

a AB mann)

{i

Pr TRIERER N IR mL len pm

d’epaisseur, 5 millimetres de largeur et 52 mm de longueur entre les points d’attache. Sous

une charge d’environ 5 grammes cette lame s’est montr&e parfaitement robuste, malgre un

grand nombre de manipulations varices. La longueur de la lame est parallele & l’axe optique.
Cette lame est encastree a son extrömite fixe entre deux mäÄchoires de laiton dont on peut
regler l’inclinaison par rapport & tout l’appareil de leeture. A l’extremite libre est fixde
une monture d’aluminiun; cette monture contient un parallelepipede de spath, dont la section
principale est verticale, l’axe optique &tant & 50° environ des faces d’entree et de sortie
de la lumiere.

Le poids total, d’environ 5 grammes, produit une fleche de flexion d’environ 11
millimetres, et un angle de flexion d’environ 18°, ou 64800 secondes. Si done on peut
lire & coup sur la seconde d’are, on aura g & «5000 pres, et si on peut lire le tiers de
seconde, on obtiendra la precision de Soon ayee un appareil extrömement ramasse et leger.
Je me suis d’abord oceup& d’obtenir eette preeision de leeture. Avec l’appareil optique bien
regle, et la lame de quartz soutenue et immobile, un seul pointe donne une precision supe-
rieure & celle-Ja. Quand la lame est libre, elle vibre avec une periode d’environ 0',2 que
l’oeil ne peut suivre; mais, par l’emploi de lames de drap intercalees entre les diverses
parties du support, j’ai r&ussi & empöcher les oscillations d’acquerir une trop grande am-
plitude, pourvu que le tr&pied repose direstement sur un sol stable. Un arröt & mouvement
tres doux me permet d’abandonner la lame sans vitesse, et sans aucune secousse, a cette pre-
cision du tiers de seconde d’arc. En fait, il arrive souvent que la repere soit assez tranquille
pour qu’une seule lecture donne la m&me precision que quand la lame est calee. Quand
il y a des oscillations, il est toujours facile, gräce & l’arr&t, de partir d’une amplitude
extrömement petite et de r&peter les leetures tres frequemment, toutes les demi-minutes par
exemple; dans ces conditions un quart d’heure suffit pour faire une trentaine de pointes;
tous ceux qui sont faits avec le repöre tranquille coineident a moins d’une seconde; et
ceux pour lesquels l’amplitude, est assez reduite pour faire une lecture ne s’ecartent pas
de plus de 3 ou 4 secondes. La moyenne des lectures discutee d’apres le degre de tran-
quillit6 de l’image est done certainement exacte a !/,”, soit & 00000 sur g. J’ai reussi tout
recemment & doubler la sensibilit6 sans augmentation de poids, mais je n’ai pas encore de

mesures ä ce sujet.

Deux series de recherches restent ä faire avant de savoir ce qu’on peut attendre
de cet appareil.

1°. Influence de la temperature.

2°, Influence des residus elastiques.

Je n’ai encore que des donndes tres insuffisantes sur ces deux points:

Temperature. — L’inäuence de la temperature sur tout l’ensemble de l’appareil, ne paräit
pas trös grande 6” & 7” par degre centigrade au plus; un thermometre a mercure divise
au dixiöme de degre parait done devoir suffire. C’est une influence qu’il n’est facile d’etudier
isolöment qu’en hiver. La diffieulte, pour des mesures rapides, sera d’avoir un thermometre

qui obeisse aussi vite que la lame de quartz.
60

 

 
