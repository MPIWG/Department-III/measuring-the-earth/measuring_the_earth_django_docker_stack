 

5 AT + N |

 

 

BEILAGE B XIX.

BERICHT

die Tätigkeit des Centralbureaus der Internationalen Erdmessung
im Jahre 1903

nebst dem Arbeitsplan für 1904. *)

A. Wissenschaftliche Tätigkeit.

1. Berechnungen für das europäische Lotabweichungssystem.

2. Krümmung des Geoids in den Meridianen und Parallelen.

®. Freiwillige Kooperation der Sternwarten in Bezug auf die Veränderung der
geographischen Breiten.

4. Internationaler Polhöhendienst.

>. Absolute Pendelmessungen.

6. Relative Pendelmessungen.

7. Programm für die Bestimmung der Schwerkraft auf dem Indischen und dem

Stillen Ozean.
S. Berichte an die Allgemeine Konferenz in Kopenhagen.

il,

Spezialbericht
über die Bereehnungen für das europäische Lotabweichungssystem.
„Diese Arbeiten beschränkten sich auf Vorbereitungen für den Druck
des III. Heftes der Lotabweichungen, das die nördlich der Längengrad-
messung in 52° Breite abgeleiteten geodätischen Linien und die Anschlüsse
an die russisch-skandinavische Breitengradmessung enthalten soll.

*) Der Arbeitsplan ist bei jedem einzelnen Arbeitsgebiet aufgeführt.

ir 56

h
I
ie
t
i4
13
#
£ 3
Me
\

 
