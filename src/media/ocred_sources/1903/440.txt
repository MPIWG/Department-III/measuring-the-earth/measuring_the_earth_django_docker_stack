 

N
|

ee

EEE

Se

ERSTE

De a ee

ME

 

j
|
|

 

 

 

 

gations-Chronometern. Ueber die absolute Genauigkeit dieser Zeit- und Gangbestimmungen
finden sich keine Angaben vor; die relative Uebereinstimmung der abgeleiteten stündlichen
Nardin-Gänge ist fast überall befriedigend.

Unter den bearbeiteten Stationen befinden sich einige, für die schon anderweite
Resultate vorliegen; wir stellen sie hier des Vergleichs wegen zusammen.

® H I Beobachter Jahr

Fort de France 14°36.3 5 978.512  Gassenmayr 1895
» 80.8009 561  Rodler 1898
Dakar 103 az v. Leidenthal 1894

» A022 9 Hill  Roallem 1897
Lissabon Ba 205,97 980.111 Laurin 1898

» An ol 110!) Hecker 1901
New-York A007 520 9302472) Sabıne 1822

» Ba 20 202 Gassenmayr 1895

» 48.9 88 321 Rodler 1898

Die nicht genau identischen Beobachtungsorte kann man leicht mit Hilfe der
Formeln
dg= + 151 sin 28. dD (d® in Bogenminuten)
dg= — 0.31. dH (dH in Metern)
auf einander beziehen; man erhält damit dg in Einh. d. 3. Dez. von 9.

0) Die Arbeiten von Perensr von PurcLas, 1898/1899. Der Beobachter er-
ledigte mit dem Schneider’schen Pendelapparat N°, 21, zu dem die Pendel N°. 63, 28 und
24 und ein Pfeilerstativ gehörten, folgende 5 Stationen: Tokio, Nagasaki, Chemulp-ho,
Zi-ka-wei und Port-Said.

Die Konstanten der Pendel für Temperatur und Luftdichte hat das k. u. k. militär-
geographische Institut in Wien zu 49.26 X 10-7 und 542 X 10-7 Sternzeitsekunden ermittelt.

Auf jeder Station wurden die Schwingungszeiten der 3 Pendel an zwei aufeinander-
folgenden Tagen je einmal bestimmt. Ob auch Versuche über das Mitschwingen des Pendel-
lagers angestellt worden sind, geht aus den beigegebenen Erläuterungen nieht hervor; in
der Tabelle der Schwingungszeiten wird die Korrektion wegen Mitschwingens des Pfeilers
auf allen Stationen zu null angegeben.

Die Temperaturverhältnisse der Beobachtungsräume waren durchweg recht günstig.

Zu den Koinzidenzbeobachtungen diente ein Nardin’scher Sternzeitehronometer,
dessen stündlicher Gang in Pola, Tokio und Zi-ka-wei durch Vergleichung mit den Uhren
der dortigen Observatorien gefunden wurde. Auf den übrigen Stationen ermittelte ihn der

1) Auf das Wiener System reduziert.

 

&
%

smBin.tiae uhdiansın Marilääua)

urn

Ba un

nn pl bh a

 
