 

 

/
Menilnh hi uud

wen

       

18

u

dans son jardin, de fagon ä& permettre de nouveaux etalonnages a une &poque quelconque. Il ya
lien de remereier cet ami de la science qui a tenu & rester fidöle aux traditions de sa famille.

La mission disposait de deux fils, l’un dit A, en metal invar ou acier au nickel
Gurmtaums, l’autre dit B, en laiton; chaque portee &tait mesurde d’abord avec enl A,,
puis avec le fil B, et pendant ce temps on prenait la temperature au thermometre fronde
aux deux extremites et au milieu de la portde. Les mesures obtenues avec l’invar et avec
le laiton &taient separement corrigdes de la temperature.

On caleulait en outre, & titre de contröle, la longueur de chaque portee, en la
deduisant de la comparaison des longueurs mesurdes avec l’invar et avec le laiton et en
faisant le calcul comme avee un appareil bimetallique.

Cette derniöre methode est evidemment beaucoup moins preise & cause de la faible
dilatation de l’acier au nickel; aussi n’a t-on pas retenu les nombres auxquelles elle con-
duisait; ces nombres ne pouvaient servir que pour &viter les erreurs grossieres comme serait
par exemple l’oubli d’une portee. La premiere methode a donne:

N
\
|
|
|
|
’
i

. Be ......, 9880,75589 |
Pie B . 0,..2.20....9980,74142,
f 3
f Les differences avee la mesure & la regle bimetallique ont ete:
| Pour le fil A, = > 9a 200
Ni Pour le fil B, AT Fon on:

Il est &vident que la eoncordance si complete des resultats ne peut-Etre attribuge
qu’a un heureux hasard; car si l’on compare les deux mesures faites dans le sens aller et
h dans le sens retour, tant avec le fil invar qu’avee le fil de laiton, on constate un Ecart
ee : notablement plus grand; c’est seulement la moyenne des deux mesures A,, ou la moyenne
| des deux mesures B, qui se rapprochent d’une fagon aussi extraordinaire de la longueur
obtenue par la regle de Brunner.

Cependant si nous comparons les longueurs du segment sud obtenues par les deux
methodes, nous constatons la m&me concordance. La moyenne des deux mesures A, ne differe
que de guohos de la moyenne des deux ınesures & la rögle et il semble que ce soit bien
la la preeision que permet d’atteindre la möthode JÄnerın avec le fil invar. Avec le laiton,
l’accord est moins bon, quoique encore tres satisfaisant.

En resume, avec la möthode JÄnsrın, on peut compter sur le oo» ou le uoTVT;
I mais on a beaucoup plus de garanties avec l’acier GviLLaumr qu’avec le laiton. Tous ces
2 chiffres ont &te caleulds en se servant exelusivement du 1er ötalonnage fait dans le Jardin

 

   

j)

ı

ı de don Pevro avant les operations sur le terrain.

| On a trouve en effet entre les deux 6talonnages une difference de zu00, environ,
j et diverses raisons portent & penser que l’une des bornes a dü recevoir un choc dans l'inter-
I valle des deux mesures. Cet ineident ne doit pas nous pröoceuper en ce qui concerne la
n base de Riobamba puisque le chiffre definitif adopte sera naturellement celui qu’a donne

I la regle bimetallique.

{
i
|
;
|

 

 

 

 
