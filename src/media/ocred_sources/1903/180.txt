i
I
|

 

On a d’ailleurs les moyennes suivantes pour les derivees:

A E,& lest. E. & l’ouest.
z 10.020 10.024
dä
7, — 022
22 2 0.280 0.224
dA
a5 — 1.040 —+ 1.029

Pendant les observations astronomiques, l’instrument n’tait pas place exactement
au centre de la station, mais & une distance de 25.5 millimötres de ce point; l’azimuth de
la direction du centre de la station au centre de l’instrument &tait 307° 30".

L’influence de cette installation en dehors du centre sur le resultat de la deter-
mination de latitude est negligeable, de sorte que nous pouvons admettre comme latitude
geographique du point Tor Batoe na Goelang, donnde par les observations astronomiques:

a = 17358 49",80 N.

En se basant sur le resultat des determinations de latitude et d’azimuth effectudes
en 1883 a la station Basis West, par laquelle passe le meridien initial et dont la latitude
est 0° 56'42".788., on a caleul& la valeur suivante pour la latitude du point Tor Batoe

na Goelang: ; en
— 1° 88’ 48".02 N.

Le resultat de la determination direete est done plus &lev& de 1”.78. Cette diffe-
rence doit provenir, non seulement des erreurs fortuites des observations astronomiques et
geodesiques et de la deviation de la verticale, mais aussi de la eirconstance que les posi-
tions apparentes des &toiles, employdes dans la reduction des observations de 1883, ont et&
empruntees au Greenwich Nautical Almanac et se rapportent done ä& un autre systeme,

La correction que l’on doit apporter & l’azimuth du signal de nuit, pour le r&duire
au centre, est — 0”.11, l’azimuth au centre est ainsi:

A — 311° 44716730.

Par des mesures d’angles dans la position exeentrique de l’instrument, le signal de
nuit a &t& reli& aux points Tor si Mangambat et Dolok Maroempak du reseau principal ;
chaque ungle a et&€ mesur& 12 fois dans les deux positions de la lunette; un heliotrope
a ete place dans ce but derriere la fente du signal de nuit.

La somme des angles centres etait 89° 49" 34”,29, alors que les resultats de la com-
pensation & la station avaient fourni pour l’angle Tor Si Mangambat-.Dolok Maroempak 1a
valeur 89° 49’ 34”.03 et que l’angle definitif, resultant de la compensation du reseau, est
89° 49 33.29. La difference a &t& repartie &galement sur les deux angles, de sorte que
chacun d’eux a regu la correction — 0”.50. Au moyen des valeurs ainsi obtenues, on trouve
pour l’azimuth du signal de nuit, d’apres les rösultats de la triangulation:

 
