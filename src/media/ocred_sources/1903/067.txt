 

|:
|
\

EEE EEE EEE

 

 

62

Herr van de Sande Bakhuyzen verliest folgenden Bericht:

Meine Herren,

Meinem Berichte über die Thätigkeit des Ausschusses der internationalen Erdmes-
sung seit der Pariser Versammlung im Jahre 1900, muss leider die Erinnerung an die
grossen Verlusten, welche die Erdmessung in diesen drei Jahren erlitten, vorangehen. In
dem Zeitraum eines Monats verloren wir unseren Präsidenten und unseren Vicepräsidenten.

Unser Präsident Herr Faye, der trotz seines hohen Alters in Paris noch als Vor-
sitzender unsere Sitzungen eröffnete, starb am 2 Juli 1902 im 88en Lebensjahre.

Sofort nach Empfang des mir vom Herrn General Bassot zugeschickten, die traurige
Nachricht enthaltenden Drathberichtes, habe ich den Herren Delegirten, diese Nachricht mit-
getheilt und bin zur Theilnahme an der Beerdigung nach Paris gereist. Unser Vicepräsident,
General Ferrero, und Herr Geheimrath Helmert waren leider verhindert nach Paris zu
kommen, mir lag deshalb die ehrenvolle aber traurige Pflicht ob, am Grabe unseres hoch-
verehrten Präsidenten, im Namen der Erdmessung einige Abschiedsworte zu sprechen und
ihm zu danken für alles was er für unsere Association und für die Geodäsie gethan hat.

Faye’s ganzes Leben war fast nur der Wissenschaft gewidmet. Am 1 October 1814
geboren, trat er, 17 Jahre alt, in die Polytechnische Schule ein, und obwohl er, nachdem
er diese verlassen, sich einige Jahre mit Ingenieurarbeiten beschäftigte, konnte er bald,
einem Ruf Arago’s folgend, sich als dleve in der Pariser Sternwarte mit der Astronomie
beschäftigen. Er zeichnete sich dort durch seine schönen wissenschaftlichen Leistungen
in der Weise aus, dass er im Jahre 1845 zum Mitglied der Akademie der Wissenschaften
gewählt und später zum Mitglied des Längenbureaus und zum Professor der Astronomie und

“ Geodäsie an der Polytechnischen Schule ernannt wurde.

In diesen beiden Eigenschaften hatte er grossen Antheil an der Entwickelung der
Geodäsie in Frankreich. Nachdem schon mehrere interessante Mittheilungen Faye’s in den
Sitzungsberichten der Akademie erschienen waren, reichte er im Jahre 1863 als Bericht-
erstatter einer Commission, die ausser ihm aus den Herren Delaunay und Laugier bestand,
einen Bericht über den jetzigen Zustand der Geodäsie und über die von dem Längen-
bureau im Zusammenwirken mit dem Depöt de la Guerre zu unternehmenden Arbeiten
zur Vollendung des astronomischen Theils der französischen Gradmessung ein. In diesem
Bericht zeigt er in grossen Linien die in Frankreich zu unternehmenden Arbeiten, und es
ist hauptsächlich ihm zu verdanken, dass die neue französische Breitengradmessung, erst
unter Leitung des Generals Perrier, später in den letzteren Jahren unter Leitung unseres
Vicepräsidenten Generals Bassot, fortgeführt worden ist. Es ist natürlich dass Faye einer
der ersternannten französischen Delegirten bei der Gradmessung war, und als er in Dresden
zum ersten Male an der Versammlung theilnahm, wurde er zum Vicepräsidenten und zum
Mitglied der permanenten Commission gewählt. Als solcher hat er alle Versammlungen
besucht, und an allen Arbeiten der Gradmessung regen Antheil genommen. Im Jahre 1891,
nach General Ibanez’s Tode, wurde er von seinen Mitgliedern einstimmig zum Vorsitzenden
der permanenten Commission und später im Jahre 1896 bei der neuen Uebereinkunft zum
Vorsitzenden der Erdmessung gewählt.

 

 

ü
i

Lak lud

Ian inn

i
1
!

 
