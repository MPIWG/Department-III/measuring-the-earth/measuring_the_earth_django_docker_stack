 

x

424

VEREINIGTE STAATEN VON AMERIKA.

Zwei sehr bedeutende Beiträge sind während der Berichtsjahre durch die Vereinigten
Staalen von Amerika geliefert worden, nämlich: »The Transcontinental Triangulation and
the American Are of the Parallel. Washington 1900") und »The Eastern Oblique Arc of
the United States and Osculating Spheroid. Washington 1902” ?), beide bearbeitet von
Herrn C. A. Scnorr.

Der 4225 km lange Parallelbogen in 39° Breite enthält 109 Breiten-, 73 Azimut-
und 37 Längenstationen, für die die entsprechenden Lotabweichungen gegen einen passend
gewählten geodätischen Zentralpunkt, sowohl unter Annahme der Crarxm’schen (1866) als
auch der Busser’schen Elemente, abgeleitet sind. Werden von den 37 Längenstationen je
drei in der Umgebung von Washington und von San Francisco zu je einem Punkt zu-
sammengefasst und fünf andere Stationen wegen starker lokaler Lotstörung in Länge (im
Betrage von 18” bis 24”) ausgeschieden, so ergeben die dann verbleibenden 28 Lotabwei-
chungen in Länge, dass die wittlere Krümmung dieses Teils des 39. Parallels zwischen die
Werte, die den Rlementen von Urarke und von Besser entsprechen, fällt, und dass ferner
die Krümmung der östlichen Hälfte des Bogens mit den Crarkp’schen und die der west-
lichen mit den Bxsser’schen Elementen nahe übereinstimmt. Dieser Bogen wird auch dazu
mitbenutzt, um aus den damals bekannten 3 Meridian- und 2 Parallelbogen in Amerika
durch 5 Combinationen zu je zweien von ihnen fünf Systeme vorläufiger Natur für die
Erdelemente abzuleiten. Die grosse Achse erhält hiernach Werte zwischen 6377577 m und
6379822 m und die Abplattung solche zwischen 1:288,6 und 1: 305,5.

Der östliche schiefe Bogen hat eine Ausdehnung von 2612,3 km im südwestlichen
Azimut von 57° 39/7 (oder von 23!/,° im Bogen grössten Kreises) und erstreckt sich über
15°13,75 in Breite und 22° 47’,4 in Länge. Er enthält 71 Stationen mit Breiten-, 17 mit
Längen- und 55 mit Azimutbestimmungen. Für die geodätischen Positionen wurde derselbe
Ausgangspunkt wie für den Parallelbogen in 39° Breite gewählt; die Lotabweichungen sind
ebenfalls für das Ellipsoid von Crarke (1866) abgeleitet. Dieser schiefe Bogen wurde nun
dazu benutzt, um aus ihm allein ein sich ihm am besten anschmiegendes Rotationsellipsoid
zu bestimmen. Zu diesem Zwecke wurden 36 Breiten-, 14 Längen- und 34 Azimutstationen
ausgewählt, die möglichst gleichmässig über den ganzen Bogen verteilt sind. Es wurden 4
Ausgleichungen vorgenommen, je nachdem für die Azimutbestimmungen die Gewichte 1,
!/,, ?/, und !/, angenommen wurden, während man für die Breiten und Längen das Gewicht
1 beibehielt. Die Resultate weichen nicht stark voneinander ab, jedoch wird das dritte
System für das beste gehalten; dieses ergibt

© 6378157 m, x - 1: 304,5,
1) Treasury Department. U. S. Coast and Geodetic Survey. Hunry S. PrirscHert, Superintendent. Special Pu-

blication N° 4. Cuas. A. Scuont. The Transcontinental Triangulation and the American Are of the Parallel. Washington
1900. S. 831—871.

2) Treasury Department. U. S. Coast and Geodetic Survey. O. H. Trrrsann, Superintendent. Special Publication
N° 7. Cuas. A. Scuorr. The Bastern Oblique Arc of the United States and Oseulating Spheroid. Washington 1902,
S. 369— 394.

 

 

3:
ä
Ü
E

ah ıdukic

nn mh u Ahr re n  ı

 
