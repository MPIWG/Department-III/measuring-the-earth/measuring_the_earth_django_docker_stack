 

f

Hi

\

#

N)
M

 

 

 

120

TRIANGULATION.

Dans les projets primitifs, les officiers devaient se partager en deux brigades qui
se seraient d6placdes parallölement, l’une sur la chaine oceidentale, l’autre sur la chaine
orientale. On devait aussi se servir d’heliostats; les Acad&miciens du XVIIIe siecle avaient
eu en effet A souffrir de la destruction des signaux par les indigenes; on craignait de ren-
contrer la m&me diffieulte et l’experience a depuis prouve que cette crainte n’etait que trop
fondde, on voulait done eviter d’avoir & construire des signaux fixes.

Malheureusement les eirconstances n’ont pas permis de suivre ce plan. En premier
lieu les jours de soleil sont trop peu frequents pour que l’emploi des heliostats puisse etre
avantageux. Knsuite le personnel frangais &tait trop peu nombreux pour qu’on puisse con-
stituer, outre les deux brigades principales, une par chaine, quatre sousbrigades (deux par
chaine, une pour la station d’amont l’autre pour la station d’aval) pour la manwuvre des
heliostats.

On se trouva done oblige de revenir au systöme des mires, exposees, comme nous
l’avons vu, & de frequentes destructions, et pour &viter des transports onereux, on fut conduit
d’autre part, & constituer, non pas deux brigades se deplagant parallelement, mais deux
brigades venant ä la rencontre l’une de l’autre et marchant l’une vers le sud, l’autre vers
le nord. O’&tait renoncer A la mesure des distances zenithales r&eciproques et simultandes puis-
que les deux operateurs ne devaient jamais se trouver en vue l’un de l’autre.

Nous avons dit quelles ont &t& les difficultes rencontrees dans Ja triangulation. Ces
diffieultes, qui ont amene tant de retards ne paraissent pas avoir eu d’influence sur la pre-
cision des resultats. Les angles azimutaux mesur&s donnent une compensation tres satis-
faisante. Le 24 mai, il ne restait plus pour achever la geodesie des stations du trongon
nord, qu’& terminer les quatre stations de Culangal et de Pusacocha, Tupisa et Yura-Cruz.

Les observations sont sans doute commencees sur la premiere moitie du troncon
sud entre Riobamba et Cuenca; la on fera operer deux brigades marchant parallölement
du nord au sud; bien entendu il faudra continuer & construire des signaux, l’emploi des
heliostats demeurant impossible; mais les chances de destruction se trouveront diminuees,
puisque chaque mire ne sera utile que pendant moins de temps.

Je dois signaler que par suite de eirconstances diverses, on a Et oblige d’adopter pour
une partie du reseau une solution toute partieuliere. Il a fallu construire a Sincholagua deux
mires & peu de distance l’une de l’autre, parce que chacune de ves mires &tait invisible de
certains points. On a done mesure de quatre stations Corazon, Pichincha, Panecillo et
Pambamarca l’angle sous lequel se voyait la distance de ces deux mires. Gräce & cette
precaution, et aux conditions favorables dans lesquelles ces observations ont pu £tre faites,
on peut &tre assur6 que la compensation de cette partie du reseau sera aussi solide que
celle du reste de la chaine. Les erreurs de fermeture n’y depassent pas 1".7.

Dans la moiti& sud du troncon nord (section Riobamba—Quito) les fermetures de
36 triangles dont les resultats nous sont connus donnent pour valeur du co£flicient de

comparaison, m — =, 2".1=0".7 (cette valeur tomba a 0".6 si on met & part les

 

In

S

 

ak u lud

an ash kn u ehe

 

 
