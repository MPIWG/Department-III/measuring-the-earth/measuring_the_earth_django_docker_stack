 

Den

E57

rum

IPFTTTTTTERT

ur pain

 

TREE PTR BRTTHT

mr

Un
Ableitung der Bewegung des Nordpols im Erdkörper. Dank der Ausdauer der Beobachter
und der Unterstützung der nationalen Behörden ist der Beobachtungsdienst in befriedi-
gender Weise verlaufen. Über die Ergebnisse hat Herr Geheimrat AusrzcuT fortlaufende
und eingehende Berichte geliefert. Sie müssen alle diejenigen, die zur Begründung und
Fortführung des Breitendienstes beigetragen, mit Genugtuung erfüllen.

Mon un Herr Direktor nn auf Grund derselben eine neue, jährliche
Schwankung 2 der Breite nachgewiesen hat, deren Ursache noch völlig in Dunkel gehüllt
ist, so ist ae äusserst wichtige ide sicherlich nur den günstigen mathematischen
Bein ing en zu danken, die die Lage der Stationen bei dem internationalen Breitendienste
bietet.

Da die allgemeine Konferenz in Stuttgart 1898 den internationalen Beobachtungs-
dienst nur auf 5 Jahre bewilligt hat !), so liegt der diesmaligen Konferenz die Enischee
dung über die Weiterführung ob, zunächst bis zum Ende des Jahres 1906, bis zu welchem
Termin in der Konvention für 18971906 die erhöhte Dotation Vorsoschin ist. Ich glaube
nicht zu irren, dass diese Entscheidung in bejahendem Sinne ausfällt.

Auch an der Methode wird Hildhte zu ändern sein. Wohl aber möchte sich eine Er-
weiterung empfehlen, um durch Beobachtungen auf geeigneten neuen Stationen ausserhalb
des Parallels in 39° 8° Br. die Natur der von Herrn Kınura entdeckten Schwankung 2
zu ergründen. Da die Geldmittel der Association aber gerade nur für den jetzigen Betrieb
ausreichen, so eröffnet sich hier ein Feld für die Mitwirkung von Sternwarten, dessen
Bearbeitung einen besonderen Reiz hat.

Von den systematischen Lotabweichungsberechnungen ist im Anschluss an das 1886
herausgegebene Heft I und an die Längengradmessung in 52° Br. ein Heft II erschienen ;
Heft III ist in Vorbereitung. Für die Weiterführung der Berechnungen ist insbesondere
der Parallelbogen in 47° bis 48° Br. ins Auge an der von Brest bis Astrachan reicht
und dessen südrussischer Teil bereits nl, ist. Es andeit sich dabei um die Vollendung
einer Arbeit, die schon 1826 von Frankreich geplant wurde. Die etwas mangelhafte tri-
on Verbindung von Südbaiern und Tirol wird zur Zeit erneuert. Eine Er-
neuerung wird auch für das kleine Stück des Dreiecksnetzes nötig werden, welches durch
den nördlichen Teil von Rumänien (Moldau) bis zum Anschluss an den nu Teil des
Parallelbogens führt.

Ich möchte hier auch abermals den Wunsch aussprechen dass die südschwedischen
Dreiecke publieiert werden möchten, welche nötig sind, um die bereits publieierten Trian-
gulationsarbeiten von Schweden nd Norwegen über Dinar mit Oentraleuropa verbin-
den zu können.

Das Studium der Krümmung des Geoids längs der grossen Breiten- und Längen-
gradmessungen, welches im letzten Jahre wenig gefördert werden konnte, soll zum kom-
menden wa energischer in Angriff genommen und womöglich zum Abschluss gebracht
werden.

Inzwischen konnte ich Herrn NEwcoMB auf seine Anfrage mitteilen, dass unter An-

1) Verhandlungen p. 40—41, bez. p. 135—136.

 

 

 
