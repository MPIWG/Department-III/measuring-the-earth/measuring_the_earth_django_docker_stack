 

N

324

FRANCE.

1. Etudes sur les methodes et les instruments des nivellements de preeision, par Ö.
M. Govıer, Colonel du Genie en retraite, revues, annotdes et accompagn6es d’une Htude
sur les variations de longueur des mires d’apros les ewperiences du Colonel GovuLser, par On.
Larıumann. Paris 1898,

2. Le Nivellement General de la France, par On. Larızmand. Annales des Mines.
Paris, 1899.

3. Le Nivellement General de la France; ses progres de 1890 & 1901. (Comptes
rendus de l’Association francaise pour l’Avancement des Sciences. Congres d’Ajaceio. 1901).

4. Diagrammes jiguratifs des circonstances d’ewecution des nivellements de 1" ordre,
(19 plauches). I mp. Marchadier 1901.

5. Repertoire definissant les emplacements et altitudes des reperes (Schwob, &diteur,
& Nantes).

Reseau de 1er ordre: 104 planches dont 68 r&cditees (publieation termince).

Reseau de 2° ordre: 41 fascicules parus sur 42.

ITALIE.
LDivellazione geometrica di precisione. (Istituto geografico militare. Firenze).
Fascieule I. — Della linea 1 alla linea 15.
» II. — Della linea 16 alla linea 39.

» III. — Della lines 40 alla linea 52.

PORTUGAL. 2

l. Nivelamentos de precisäo em Portugal nas Linhas de Cascaes a Valenca, Mealhada
a Barca d’Alva, Cascaes a Caldas da Rainha, revistos e coordenados por determinacäo do
Director Geral, General de Divisäo Carlos Ernesto de Arbuds Moreira, pelo Chefe da 12 secgäo
Conde d’Avila, Coronel do Corpo do Estado Maior. Lisboa, 1898.

2. Nivelamentos de precisäo em Portugal nas Linhas de Caldas da Rainha a Elvas,
Santarem a Mealhada, Porto a Valenga, revistos e coordenados pelo conde d’Avila, Corönel
do Servico do Estado Maior, Direetor dos Servicos geodesicos. Lisboa, 1900.

RUSSLE.

1. Le niveau frangais et les operations de nivellement exeeutees & l’aide de cet in-
strument entre Poulkovo et Gatchina, en 1898, par M. Ossırov. (Zapiski de la Section Topo-
graphique. Tome LVIII).

2. Les maidriaux pour le supplöment du catalogue des hauteurs du reseau niveld russe
(Zapiski. Tome LIX).

3. Le nivellement de precision entre Kharkhof et le rescau des nivellements de pre-
cision russe, par M. L. Srruve. (Journal du Ministere des Voies de communication. 1902).

 

Z

um ulaarıı) Marldkunı)

m

at}

Baal un

sahne kan

 
