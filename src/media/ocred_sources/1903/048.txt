1 TEE

er

eher

Free rn

Fre

43

M. le President s’associe & la proposition de M. Förster et invite le Seerdtaire A y
donner suite,

, A propos du rapport de MM. Bexoir et GurLLaums. M. Helmert presente les obser-
vations suivantes.

Pour les mesures avec l’appareil JÄnerın on a employ& deux me&thodes. En Europe
on s’est servi de fils acier-nickel de M. GvıLLaumE pour lesquels une determination assez
grossiere de la temperature suffit. En Amerique et aussi au Japon on s’est servi de rubans
de 100 metres d’un autre metal que l’acier-nickel; la preeision des mesures avec les rubans
est excessivement grande mais jignore de quelle maniere on a pu surmonter les difficultes
causdes par le vent.

La variation des fils en invar dependant du temps doit encore ötre &tudiee. A Potsdam
nous avons aussi fait des experiences et, quoiqu’elles aient &te ex&cutees par des gEeodesiens et
des mecaniciens experimentes, nous avons trouve que dans une periode de 4 semaines les
fils ont subi des variations de longueur montant jusqu’& 490007. Dans ces experiences nous
avons suivi scrupuleusement toutes les indications que M. Guillaume nous avait donnees)).

M. Tittmann. Comme M. Helmert se le rappellera, nos mesures avec les rubans ont
etE faites d’abord par un vent assez fort, afın d’obtenir une determination plus exacte de
la temperature, mais le vent faisait flechir le ruban et les mesures en devenaient inexactes,
de sorte qu’a present nous ne les faisons plus quand le vent est fort. Nos premieres mesures
ont &t6 faites avec des fils; nous avons eu soin de rester toujours & la m&me hauteur au-dessus
du sol et la preeision que nous avons pu obtenir a et& fort satisfaisante.

M. Hewvelink demande & M. Tittmann s’il n’a pas fait des mesures pendant la nuit.

M. Tittmann. Dans nos premieres experiences nous avons opere principalement
pendant la nuit et nous avons obtenu de fort bons resultats. Mais la pre&eision depend en
grande partie des circonstances e.a. de la radiation de la chaleur. On peut d’ailleurs me-
surer la temperature des rubans avec une grande exactitude au moyen de l’electricite,
comme on l’a demontre en plusieurs laboratoires, et comme nous l’avons pratique dans les
mesures des neuf bases du 9° meridien.

M. Förster. Je me permets d’ajouter quelques remarques & ce qui aete dit par M. Hel-
mert. L’avantage d’employer des fils dont la longueur est & peu pres independante de la tempe-
rature n’est pas un avantage absolu, car il est &vident que, les changements causes par la
temperature n’etant pas entierement reversibles, la longueur sera aussi une fonction du temps.
On ne peut profiter du faible coöflicient de dilatation qu’en tenant compte de cette fonction.

Au Bureau des poids et mesures ä Breteuil on s’est occupe aussi de la construction
de rubans en invar, mais c’est la reversibilit@ des changements dependant de la temperature
qui cause quelques difhicultes.

M. Helmert fait observer qu’il n’a aucune objeetion contre l’emploi de l’invar, mais
quand on se sert de la methode JÄvurın la question capitale au point de vue des frais est
celle du comparateur necessaire a l’etalonnage des fils.

l) Dans le rapport du Bureau central (voir Annexe B XIX) on pourra trouver plus de details.

\

 
