in

Se Ge wer

 

jamanıı

ar

wii pw!

TERROR I FIRIRTIE

por

“

L’etude de toutes les observations exdcutdes aux stations depuis le mois de Sep-
tembre 1899 jusqw’au 4 Janvier 1902 et la preparation du manuserit d’un mdmoire con-
tenant les resultats acquis, reprösentent un travail considsrable, Commenes au Bureau
central au printemps de l’annde 1902, ce travail fut assez avancs A la fin de lannde pour
permettre l’impression du premier volume des publications du service international des
latitudes au commencement de l’annde 1903. Ce volume eontient aussi une description
detaillee des differentes stations, ainsi qu’une collection considörable de cartes et de vues,
fournies gracieusement par les difforentes stations. C’est surtout au comite geodesique
japonais et & la section topographique de l’etat-major imperial russe que le bureau central
doit ses remerciments pour les cartes topographiques des environs des stations de Mi-
zousawa et de Tschardjoui mises & la disposition du Bureau central en nombre suffisant
pour tous les exemplaires de la publication.

Un extrait de ce travail contenant les resultats les plus interessants a dte public
dans un me&moire „Resultate des internationalen Breitendienstes in der Zeit von 1899.9_
1902.0” (Astronomische Nachrichten, N. 3808 Vol. 159 P. 245). Dans ce memoire je fais
remarquer que le mouvement du pöle ne satisfait pas a la formule usite jusqu’a present:

Adtv=xcosı-ysin‘

mais quil convient d’adopter, d’apr&s la proposition de M. le Prof. Kınura (Astron. Nach-
richten. N®. 3783 Vol. 158 p. 233), la formule:

AP+vV=xc0R2+ysinı-+2

Il faut done ajouter & la seconde partie de la premiere formule un terme annuel
independant de la longitude de la station,
Les latitudes moyennes des six stations internationales sont d’apres ces caleuls:

Mizousawa 39282 3.62
Tschardjoui 10.67
Carloforte 8.93
Gaithersburg 13.20
Cineinnati SL
Ukiah 12.08
TH. ALBRECHT.
5.

Rapport special sur les determinations absolues de Pintensite de la pesanteur,
et sur les comparaisons de diff6rents pendules.

Conformöment au programme des travaux pour l’annde 1902, nous avons repete la
determination de l’intensitö de la pesanteur avec le pendule italien, afın de trouver la cause
de la variation remarquable des r&sultats döpendant de la densit6 de Pair dans la cloche,
A titre de comparaison les valeurs obtenues en 1901 se trouvent dans le tableau sui-
vant & cöte des valeurs obtenues en 1902.

SZ Qu QQoQlum Qu nnnwmg nun nur mnee Hentai nennen
