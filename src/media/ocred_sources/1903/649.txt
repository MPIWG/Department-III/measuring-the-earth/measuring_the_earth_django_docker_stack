ı

 

321

ETATS-UNIS D’AMERIQUE.

Les echelles de marde ont &t6 rattachdes aux nivellements de preeision dans les
points suivants: i
Biloxi (Miss.), Sandy Hook (N. J.), Boston (Mass.), Washington (D. C.), Morehead
City (N. C.), Brunswick (Ga.), Old Point Comfort (Va.), Annapolis (Md.), Cedar Keys and
St. Augustine (Fla.).
*
*

Le nombre des points de jonetion entre les röseaux europdens de nivellenent est
aujourd’hui suffisant pour que l’on puisse, avec une approximation deja grande, fixer les
relations de hauteur qui existent entre les z&ros hypsom6triques des divers pays. Il y aurait
done un grand interdt A ce que les Etats dont les lignes de rattachement ne sont pas
encore calculdes voulussent bien procöder a cette op6ration dans le plus bref delai possible.
Cela nous permettrait de presenter & la prochaine Conference Generale un tableau d’ensemble
donnant, par rapport & une möme origine, les hauteurs actuelles les plus probables des
points de jonetion des difförents röseaux et celles des reperes fixes, places & cöt6 des mard-
graphes ou medimar6mötres, et auxquels sont directement rapportes les niveaux moyens

deduits des indications de ces appareils.

VI. Publications, parues depuis 1895, relatives aux nivellements
de pr&cision.

ALLEMAGNE.

1°. LANDESAUFNAHME,

»Die Nivellements- Ergebnisse der Trigonometrischen Abteilung der Königlich Preussi-
schen Landesaufnahme’.

Rascieules 1, I, II, VI. . Anuee 1806

» IV, v VIER, 1897
» I, 8 xpexmı 1893
» ll... 02.0 1900

2°. MınismiRE Dws Travaux Puguicos.
Bureau des Nivellements de pre&ecision.

A. Publieations &manant du Directeur du Bureau,
Geheimer Regierungsrat Prof. Dr. Skisr.

l. Die hydrostatische Dijferentialwage. (System Seibt-Fuess). Zentralblatt der Bauverwal-
tung. — Berlin, annede 1896, Page 162.

EEE ag

 

1
n
1
3
}
i
1

\
u u u un

 

ie

een

en ne ern

E
i
