 

314

utilisee tant pour contröler les lectures du voyant que pour determiner stadimetriquement
les distances.

Un thermometre est fixe & la mire.

Le talon, en metal, se termine par un ergot cylindrique de 27 mm de rayon.

Le support de la mire est un disque eirculaire en fer, de 0,15 m de diametre,
presentant au centre une cavite cylindrique de 35 mm de rayon pour recevoir l’ergot du talon.

Niveau. — Le niveau est du type ä nivelle independante, avec vis micrometrique
et tambour gradue.

Les caracteristiques de deux de ces instruments sont les suivantes:

Oyenurekderloppei >... 2.2.2 ..202.200.2..2:29mm | 43 mm
Be... nee 085m | 0,41 m
emsesememt de la lunette . 2... 2.2... 00020.2..2.228 fois 24 a 37
Bon delanwele ...-. 2 ...22.2 241-1758 8%4
Grandeur . : . er. gamm 2 mm
Rayon de courbure & a Hole De nr 2 me 280m 2 120m
Valeur d’une division du mierometre (100 on l tour) | 2,6
Angle sous-tendu par les fils stadimetriques. . . 2.2... 1854” | 16'39"
Bere mel... .. 2. nennen sn | ge
Bocwinmweu sul... 2... .2. 0.0.0.0. 0..0.046kg
Bau 1.2. nenne. thäkg

Methode. — En 1895—1896, on employait la methode dite »des deux nivellements
simultanes’”.

A cet effet, dans chaque station, on visait d’abord les 2 mires places, vers l’arriere,
& des distances inegales du niveau; ces deux mires &taient ensuite portees en avant de
celui-ei, et & des distances respectivement egales aux pr&c&dentes, apres quoi on effectuait les
visees d’avant.

De 1896 & 1898, on a employe, tantöt cette methode, tantöt celle des deux nivelle-
ments distincts, effeetues en sens contraires l’un de l’autre.

A chaque visee, on amenait le voyant de la mire a peu pres dans l’horizontale de
la lunette; on bissectait le voyant avec le fil niveleur et l’on notait la lecture faite sur
la mire. On achevait ensuite d’amener la bulle entre ses rep£eres et on lisait, sur le tambour

de la vis mierometrique, le petit deplacement angulaire imprime& de la sorte & la lunette.
On caleulait enfin la eorreetion correspondante & faire subir a la hauteur de mire.

En chaque station, et successivement pour les visees d’arriere et d’avant, les ope-
rations s’exscutaient dans l’ordre indique par le tableau suivant, oü A signifie que la bulle
de la nivelle est amenee entre ses reperes et B, que le fil du reticule de la lunette bissecte
le voyant.

 

 

=:

etc an zu hasse nd un

 
