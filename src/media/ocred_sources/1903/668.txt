 

la

CANADA.

Mr. W. Ber Dawson, Engineer in charge of the Tidal Survey of Canada, has
written a paper on »Tide levels and datum planes in Eastern Canada” for the Canadian
Society of Civil Engineers !). It will appear in the present year 1903, and the following
is extracted from an advanced proof of the paper. Most of the information has been pu-
blished from time to time in the annual reports of the Tidal Survey, but is now condensed
in the paper referred to.

The object of the Tidal Survey is to afford information to mariners, and thus the
determination of levels is collateral to the object in view, »but it is necessary within certain
»limits, because at the principal tidal stations it is essential to have a bench-mark in order
»to maintain a uniform datum level for the reduction of observations. It was also evident
»that a large amount of important information could be secured by taking more complete
»levels, and by establishing bench-marks at all tidal stations at which recording instru-
»ments were placed, even for a few months. The additional work involved was therefore
»undertaken from the outset; and as the Survey has to be carried on with a minimum of
»technical assistance, this has been done by the writer (Mr. Dawson) personally. The endea
»vour has always been made to connect the new levels with any that were already established’.

The several levels with which it is necessary to deal are, (1) Mean sea-level, (2) the
(British) Admiralty datum or »low water of ordinary springtides”, (3) Indian low water
mark, and (4) what we may now call United States low water mark. The level (2) is not
susceptible of exact scientific determination, but when once fixed it sewes as well as any
other datum level. The level (3) is an arbitrary level which must in general coinceide pretty
nearly with (2); it is determined as being below ınean sea-level by the sum of the semi-
amplitudes of the tides M,, S,, K,, O. The level (4) is a modification of (3); it is defined
as being below mean sea-level by

M;+S+K + 0)ini; — Ki — 0),
where M,, S,, K,, O0, are semi-amplitudes, and M,, K,, O, are the angular retardations
of phases (see Tide tables of the U. S. Coast Survey for 1897, p. 17).

From the point of view of Geodesy the determination of the mean sea-level is the
point of interest, but this is in general carried out through the intervention of the others
levels specified. Mean sea-level can be definitely determined at places where continuous
observations by means of a self-registering gauge have been obtained, and were a bench
mark has been established for reference. Approximate mean sea-level could be obtained from
a few months of continuous observation, but were close accuracy is needed a long series
of observations is requisite.

In order to avoid negative entrees the elevation of local bench marks has usually
been assumed in Canada as 100.00 ft. When some general system of levelling in Canada

1) A summary of the work of the Tidal Survey of Canada is contained in the Report of the Council of the
Royal Society of Canada for 1902.

 

=
&
z
S
ü
a

at}

nun pad ha u AA a

:
i
|
!

 
