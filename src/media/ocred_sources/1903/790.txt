 

ANNEXE B XXI

Deseription d’un gravimetre de flexion,
PAR

M. BRILLOUIN (Paris).

I. EnsemeLe. — ApparuıL A LAME DE (QUARZ.

L’appareil dont j’ai entrepris l’etude avec la subvention de l’Association geodesique
est destine A effeetuer des mesures de la pesanteur, de precision moyenne, mais rapides, en
un grand nombre de points du sol. Peut-ötre deviendrait-il utilisable en mer, au moyen d’un
support convenablement etudie, mais cela me parait douteux.

L’elöment essentiel de l’appareil est une lame de quartz mince encastree a une
extremite, et chargee a l’autre extremite qui est libre. Un proc&de de lecture optique qui
sera deerit plus loin permet de lire l’angle de flexion & une fraetion de seconde pres.

Cest la lecture de ces angles de flexion qui fait connaitre la pesanteur. — Le
procede de leeture des angles que jiai appliqu6 & la balance de quartz n’a pas encore te
deerit, bien que je l’aie imaging et realise ıl ya plus de 20 ans, et employ& depuis plu-
sieurs anndes sur un appareil d’Eötvös pour l’ellipticite du geoide. Je n’ai done pas eu &
l’&tudier & nouveau pour cet appareil.

Dame de quartz. — J’avais pense employer comme lame flechie du quartz fondu;
mais j’ai dü y renoncer. La verrerie Scnorr m’avait fait esperer une lame suffisante mais
s’est finalement reeusee. J’ai fait faire au Laboratoire de \’Ecole normale une tige aussi
pure que possible, par les soins de Mr. Durovr, longue de 55 milimetres large de 5 mm;
Mr. Perrıv me l’a fait tailler sous 0,3 mm d’epaisseur. A l’essai cette lame s’est montree
trös fragile; d’innombrables petites bulles, irrögulierement distribudes, que le procede de
soudure au chalumeau des fragments chauffes et fondus produit inevitablement, expliquent
eette fragilite, et font eraindre que la flexion ne soit tres irreguliere.

Malgr6 l’inconvenient d’nn coeffieient de dilatation plus grand, je me suis decide

a essayer le quartz cristallise.
5

Mr. Petın m’a fourni des lames de 75 millim&tres de longueur, 5 millimetres de
largeur, et d’&paisseurs variees, les unes de 0,32 mm les autres de 0,20 mm. Apres d’assez
nombreux essais relatifs & la flexion produite par diverses charges, & la limite de rupture,
au mode d’encastrement de l’extrömite fixe, etc. — j’ai fini par adopter une lame de 0,20 mm

 

al

Asch äh as haha ia en

 

 
