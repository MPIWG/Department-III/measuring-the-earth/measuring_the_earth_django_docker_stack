 

R
@
|

en

et

Bremer 7

La nivelle &tant solidaire de la lunette, on place le niveau & &gales distances des
deux mires pour &liminer les erreurs instrumentales.

La longueur des portees atteint 75 metres, au maximum; elle descend jusqu’a 14
metres dans les terrains tres inclines.

Les observations s’effectuent dans l’ordre suivant:
1°. Operateur
2°, Lecteur
1°. Lecteur
2°. Operateur

Mire arriere

 

1lere operation
Mire avant

Operateur
. Lecteur
1°. Lecteur
2°, Operateur

| Mire avant
2° operation.
Mire arrıere

om
Or

AUSERATLIE  ETAT DE VICTORIA.

Reperes. — Reperes naturels, espaces, en moyenne, d’un demi-mille (0,8 km) et
pris-exceptionnellement sur des constructions metalliques ou sur des ouvrages en magonnerie.
Mire. — Mire parlante divisee en 0,01 pied (3 mm).

Niveau. — Niveau Dumpy & lunette de 14 pouces (0,58 m) de longueur focale, per-
mettant de lire sur la mire jusqu’& une portce de 400 pieds (environ 120 m).
Methode. — Le nivellement prineipal est contröl& par un second nivellement effectu&

dans le möme sens et servant & mettre en &vidence les fautes du premier.
Tous les 40 milles (64 km), operateurs et instruments changent.

IV. Constatations relatives aux variations de longueur des mires.

Les lois de la variation de longueur du bois des mires, sous l’influence de la temp£-
rature et de l’humidite, ont &t& fixdes par le Colonel GouLier, en 1883—1884, A la suite
d’etudes minutieuses et prolongees, dont il a &te rendu compte & plusieurs reprises A l’As-
sociation G&odesique Internationale ').

Les recherches executees en 1895 & Berlin, sur le möme sujet, par le Dr. StapruAcen,
a la demande du Bureau Central de l’Association Geodösique, ont apporte une preuve de
l’exactitude de ces lois en etablissant definitivement que les variations de longueur des
bois sont liees a la tension relative et non & la tension absolue de la vapeur d’eau contenue
dans l’air.

I) Conference generale de Bruxelles, en 1892 (Comptes rendus, pages 664 et suivantes).
Etude sur les variations de longueur des mires de nivellement d’apres les experiences du Colonel Gouster, par
Cu. Lausemann. — Conference Generale de Stuttgart, en 1898. (Comptes rendus, p. 525 et suivantes).

 

 

 

uniumdl, br l me Hadlaıııl Mahlldhukı | ıudeukee

Lak u lud

Lada ben |

u hmlhih M a Al hnh  n

 
