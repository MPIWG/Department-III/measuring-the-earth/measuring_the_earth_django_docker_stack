 

 

 

   

“

 

 

EEE

PETER VEETERURTT ETEENEÄEEHUETTETE EEE TE TEE ER IR

VI — Grande Breraene,

 

REAL BREIAIN AND TRELAND

°reliminary Historical Remarks.

The Primary Triangulation of the Ordnance Survey of Great Britain and Ireland,
an account of which was published in 1858, may be briefly described as the result of
the observations made with two great Theodolites of 36 inches diameter by Ramsden,
a similar smaller Theodolite of 18 inches diameter by the same optician, and a two
feet Theodolite by Troughton and Simms. The leneths of the sides of the triangles
are, ultimately, made to depend on two Bases measured with Colonel Golby’s compen-
sation Bars. The latitudes of the terminal and intermediate points for arcs of meri-
dians were observed in 1802, 1806, 1813, 1817, and that of Dunkirk in 1818 by. Ge-
neral Mudge, Major Colby, and M" Gardner, with Ramsden’s zenith sector —- which was
unfortunately destroyed in the great fire at the Tower of London in October 1841 —
and the observations for latitude from 1842 to 1850 were made with Airy’s new zenith
sector. By observation of polar stars, with the large Theodolites, the direction of the
ıneridian was determined at 61 stations. Upon the goodness of the Instruments and
the skill and care of the several observers depend the general accuracy of the work,
which, embracing 10° 56° of latitude and (by its extension in 1861 through the North
of France into Belgium) 13° 10° in longitude, should have great weight in the a
tical operations of Europe.

The Ordnance Survey may be taken to have commenced in 1791 by the remeasure-
ment of the Base on Hounslow (by Colonel Williams and Captain Mudge), for the
operations in 1784 and 1787 by General Roy, for the connexion of the meridians of Green-
wich and Paris, became obsolete by the connexion effected in 1821 by Captain Kater and
Major Colby : as indeed the latter is also now, through the effect of subsequent ope-
:ations. Major-General Mudge was the Director from 1798 till 1820, and Major-General
Colby, R. E., from this time till March 1847, and was an observer at all important
stations and measurements of Bases from 1805 till 1828-9.

Colonel L. A. Hall, R. E., was Director of the Survey from 1847 till 1854. The
Principal Triangulation was completed in 1852, and the systematic reduction of the
observations was proceeding for publication under the superintendence of Colonel Yol-
land, R. F., and Colonel Cameron, R. E., when in the summer of 1854 the late Colonel
Sir Henry James, R. E., succeeded Colonel Hall as Director. The account of this Trian-
Sulation, drawn up by Captain A. R. Clarke, R. E., was published in 1858; it describes
the Instruments and methods of caleulation, and contains all the previous observations
of any geodetic value. The only subsequent, but very important, operations — all under

 

 
