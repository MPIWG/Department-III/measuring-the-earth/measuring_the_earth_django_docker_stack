 

Herr Hirsch beabsichtigt nicht, die vom Herrn Präsidenten vorgeschlagene Ver-
schiebung zu bekämpfen, denn er verkennt keineswegs, dass der Strom der Zeit in diesem
Augenblick den Fortschritten eher entgegengesetzt ist, welche sich durch Verständigung und
internationale Uebereinkommen auf dem Gebiete der Geodäsie erziehlen lassen. Er kann sich
jedoch nicht enthalten sein Bedauern auszudrücken, dass ein Vorschlag, welchen er vor
einigen zwanzig Jahren in das Programm der Erdmessung eingeführt hat, der seither
immer mehr Beifall gefunden, und welcher ein so bedeutendes Interesse sowohl vom prak-
tischen wie vom wissenschaftlichen Standpunkte aus darbietet, jetzt aufgegeben wird, wo
fast ganz Europa mit Pra&eisionsnivellements bedeckt ist, welche die verschiedenen Meere in
Verbindung setzen, und wo das mitllere Meeresniveau für eine gewisse Anzahl von Punkten
hinreichend genau bestimmt ist. Wenn man doch zum dritten Mal die definitive Lösung
der Frage, welche in vielen Ländern von den Ingenieuren und Gelehrten mil Ungeduld er-
wartet wird, verschiebt, und diesen Aufschub mit Gründen motivirt, welche nicht gerade
lem Bereiche der Wissenschaft angehören, hat es wenig Bedeutung, ob dies für ein oder drei
Jahre geschieht.

Herr v. Kalmdr glaubt, dass das Centralbureau am besten im Stande sei, diese bis
jetzt noch streitige Frage gründlich zu studieren, denn dasselbe verfügt über die Mittel,
Specialisten herbeizuziehen, deren Gutachten es für nützlich erachtet.

Herr Ferrero stimmt diesem Vorschlage bei.

Der Herr Präsident schlägt endlich vor, das Studium dieser Frage in erster Linie dem
Gentralbureau zu übertragen, welches über seine Untersuchungen der Permanenten Gom-
mission Bericht erstatten wird, welch’ letztere alsdann in drei Jahren die Lösung der
nächsten Generalconferenz unterbreiten kann. Uebrigens widersetzt er sich auch einer Be-
sprechung in der nächsten Sitzung der gegenwärtigen Conferenz nicht; er wünscht jedoch,
diesen Vorschlag nicht nur in seinem eigenen, sondern im Namen der Permanenten Gom-
mission vorzulegen.

Die Ermächtigung hierzu wird dem Präsidenten einstimmig ertheilt.

Der Herr Marquis de Mulhacen erinnert weiter daran, dass seit der Aufnahme der
Frage in das Programm der Erdvermessung, er immer mit dem periodischen Berichte über
die Mareographen betraut gewesen ist; er glaubt, dass der Augenblick gekommen sei, diese
Aufgabe an ein anderes Mitglied der Commission abzutreten, und er schlägt vor, Herrn
v. Kalmär mit dieser Arbeit zu beauftragen, welche für einen Seemann ein ganz besonderes
Interesse bielet und welche in naher Verbindung mit den Precisionsnivellements steht, mit
denen Herr v. Kalmär sich ebenfalls befasst.

Nachdem Herr v. Kalmär seine Einwilligung gegeben, dankt die Permanente Gommis-
sion dem Herrn Marquis de Mulhacön für die bedeutende Summe von Arbeit, welche er lange

Jahre hindurch diesem Zweige des Programms gewidmet hat, und beschliesst, seinem Wunsche
Folge zu geben.

Die Sitzung wird um ein Viertel nach ein Uhr geschlossen.

 

ne

 

 

 
  

En a SEREERT
