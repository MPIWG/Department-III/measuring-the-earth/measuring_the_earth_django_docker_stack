   
     
        
       
       
       
        
     
 
 
  
  
  
  
  
  
  
   
 
  
    
   
 
   
     
   
   
    
   
   
        

V.-— Espagne.

 

  

ı|
|

 

INDICATION I DIREOTEURS |

E x \
N° LATITUDE. |LONGITUDE.| HPOQUE. Rune. |
DES POINTS. | ET OBSERVATRURS. |

 

REMARQUES.
|
u

 

Triangulation du Meridien de Salamanea.

 

 

 

 

 

 

 

 

 

 

 

N Plbemus.  un...o. 43° 39" 16” Imy 207 1872 Lieut. col. d’Etat-Major | Pistor n° 2, || „ Publics dans 1es
| Monet || « Memorias del In-
2 Gamönal .... ..... 131343111 3832%9| 187 id. id. a
3 | Trigueiro ......... 48 168063112.10 21 | 1871 id. ! id. || Madrid, 1975.
4 |, Braüa-Caballe...... 43 0:19°.12° 1.45 1871 = id. |
5 | Mampodre......... 43 1 921712 28:93 | 1871 id.
en... 224 2, 12. (057 |, [868 Col. EM. na Repsold A.
eaellarn zer... 4225 20 | 154.53, | 1868 id. Tal.
8 | Matadeön.......... 42 19,53 | 2 12 24 | 1868 id. id.
9 | Casas-Viejas ....... 42 12 58 | 11 44 14 || 1868 id. id.
10 182. Vicente... ...... 42,120 12216 59) 1868 Aid, id”
11 | Campanario........ 41 48 58. 11 38.19 1864 Comm. d’E.-M. Sanchıs. Repsold n° 2.
12 | Fuentes............ A373 1864 id. Bıstor ne >.
13 | Teso-Santo ........ A 14 18 1.11. 46,33 1864 id. Repsold n° 2.
14 | Castillejo.......... 41 10.394 12 11 12 1863 Cap. d’E.-M. Ahumada, id.
Comm. d’E.-M. Sanchis.
5, 00rrAl ..- ......: 4049 0,1232 1860 Comm. d’art. Corcuera, | Ertel n® 1
cap. du genie Barra-
| : quer.
j 16 | Diego K6mez ...... 40 55 37 1.11.36 8 1860 id. ad.
FB 17T Franeia..2...2....2 A050 22 750 1861 id. 7.210.
18 | Calvitere.......... AU R7 31 211 55 55 1861 id. id.
| 19 Santa Bärbara..... 40, 050. 11 36 16 1863 Comm. d’E.-M. Sanchis, Repsold n=2.
BE cap. d’E.-M. Ahumada.
| 20 | Miravete........... 3942 487 5535 1863 id. Id
| 21 | Valdegamas........ 293256 | I 35 22 1863 id. id.
|: 22 | Pedro Gömez ...... 33 23:36 | 1lsarT 16 1863 id. id.
EI 23| Montänchez........ 3 12 53. | 325% 1863 id. id.
I 24 | Magacela......... 27938 53.44.1156 4 1862 Be du genie Ibarreta, | Repsold (sans mar-
u. cap. d’art. Solano. que).
5 0lva 22... ss 1557,11 3] 2% 1863 id. id.
26 | Santa Ines......... sea so 2 > 1863 : id. id.
E 277 Bienvenida.::!..:. 89. 16 2067| 1 3041 1863 a0 id.
= 28 | Hamapega......... 138.23:19 . IE 53 50 1864-65 Comm. d’E.-M. Monet. Ertel n® 2
29, Eentudia........... E38 3.15.) 11.20-3 1864 id. en id. |
30). CepEon. .......... "3139 32,11r12453 1864 Aids id.
Dee ne Es 3249 1] 1650 1864-72 Comm. d’E.-M. Monet, | Ertel n° ‘2, Pistor
| cap. d’art. Hernändez. nes,
e 23 | Bujadillos......... 7 1445| 1 2911 1864 Comm. d’E.-M: Monet. Ertel n° 2.
Fr 33 | Regatero........ = 3110-2 1722.29 1864-72 Comm. d’E.-M. Monet, | Ertel ne 2, Pistor
cap. d’art. Hernändez. ne 9:
34 Gibalbin.. .:....>.. 3049 4,113 % 1863 Cap. du genie Ruiz Mo- | Ertel n° 2.
reno (D.,J.
35 | Esparteros......... 32.519, 12.11 a 1864 Comm. Ge -M. Monet. | id.
86 -binar>..... A E 204556 | 12 5 1863 id. id.
20, Aljibe........:... 30.) Bi 12 8 58 1863 id. id.
38 | 8. Fernando ....... ı 38.27 55 | 11.28 3 1863 Cap. du genie Ruiz Mo- id. |
z reno (D. |
SI Never .......... 236.15 102. 11.:42. 5 1863 Comm. rar. Monet. id. |
A Ema ....... 20 0654 12 7-40 1863 id. id.
Triangulation du Meridien de Madrid.
Ar ehlatias. .......... 4322921742 1825221027 | 1862270 Gap: B. -M. Quiröga, | Ert.n° 2, Pistor n° 2. |)
I N comm. d’E.-M. Monet. | |
= 32: (errede........... 4325 19 14 2339 | 1862 id. Ertel n° 2. |
IE 9 433 Yalnera:........-. 43 345,135926\ 1861-71 id. Ert. n° 2, Pistor n° 2, ||

 

 

 

AA 2... 0 22 12523) Iso 1d. Ertel n° 2
