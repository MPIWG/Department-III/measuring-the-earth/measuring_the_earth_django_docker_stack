zZ mM

I. — Autriche-Hongrie.

 

 

 

INDIOCATTON
DES POINTS.

LATITUDE.

Stirbee mar?
Kukujova
Bavaniste

Funtina faetje
Zichydorf

BOka. 2.1.
Ludwigsdorf
Panesova.

Szurduk
Kalakatsch
Veudvar

Orlosat 2 2:
Beeskerek

Kis Torak

Csurug

Peterwardein

 

Ojezow
Koniusza
Sieborowice
Bukowina
Bistez:

Szyezkow (Chich- |

. kowo)

Hohe Gehren
Wendelstein

Rofan

Watzmann
Rettenstein.........

Reisrachkopf

‚Gross Glockner....
Ziethenkopf.......:
.M..Paralba......:..

-Gölbner Joch

.\ Rödt Spitz
Schwarzenstein .....
Hinterthal Kogl ..:
Kömlö

Vinieni vreh

Jäsz Ladäny
Särhegy

Erdöhegy
Perdy polje

Harterberg
dihegy)

Garabhegy....

Czernieder

Kis Körös.. .......
Petrovoszelo

Deveeser Puszta ...
Kula auf der Hut-

 

44° 35' 34"

44
44
45
45
45
45
44
45
45
45
45
45
45
45

45

LONGITUDE.

|  EPOQUE.

 

32
49 12
0 24
13 50
21 23
IE
52.22
4 17
9 26
IT
15 48
22 52
30 16
28 26

14 30

I
10 57
9 44
22 9
25 56

2.0
45 7
42 16
27 31
33 33
20 2

 

 

39°56' 46"
39 46 57

3834 3 |

38 37 56
38 47 23
38 90:9
38 26 8
sis 1)
81 09.99
37 45 42
37 58
38 15
38 98
38 15
37 44

26
22

37 32

31.29
37 53
37 42
40 20
40 18

40 10
30 10
29 40
29 27
30 35
29 57

30 36

30 21
30 36
30 23
30

1865.
1865

1870
1870

 

42 |
47 \

52.|

1870
1870
1870
1870
1870
1870
1870
1870
1870
1870-80

1870-80

1848
1848
1848
1848
1848

1848
1852
1852
1852
1857
1852-57-80

|
1857-80-81

-1880-81-83

. 1880-81-84

1884

1882

 

 

1880
1880
1880
1880
1880
1881
1880
1880
1880-81

1580
1880
1880
1880
1880

.1880
1880

 

 

 

DIRECTEURS
ET OBSERVATEURS.

INSTRUMENTS.

REMARQUIS,

 

 

 

 

 

 

 

 

 

 

 

 

Kittner.

Vergeiner.

Vergeiner, Rehm.
Vergeiner, Hoffer.

Ioh. Marieni.

id.

| id.
Pechmann.

id.

id.

-Muszynski.

id.
id.
Grüner.
Muszynski, Grüner, Corti.

Grüner, Corti.
Hartl:

Hartl, Ritter.
Ritter.

Rehm.

Venus.
Oorti.
Venus.

Hartl.
Hoffer.
Kalmär.
Venus, Hartl.
Venus.
id.
id.
id.
id.
Corti.

Kalmär.

Reich. n° 2 del2 p.

 

Starke

| Starke
| Starke

ı Starke
| Starke

 

Starke n° 1 de 1Op.

‚id,

Beich. n° 2 de 12.
id.

 

 

id.
Reichenbach n° 2 de
12 p. et Starke n® 1
de 10 p.
Reichenbach n° 2 de
12 p. et Starke n® 5
de 20,p:
Reich. no 1 de 12 p
i

id.

id.

Damm ı

id.
Starke n°.3 de 10p.
id

id.

Starke n° 1 delOp.
Starke.n° 1 et 3 de
10. p. ein 2de&p:
Starke n° 1 delOp.
et n 2de8p.

Starke n® 3 de 10 p.
Id n°2 et 3de 10p.

Starke n® 2 de 10p.
n° 1 delOp.

id.
n° 4 delÖp.
n°.2 de: Sp
n° 4 delOp.
id.
n° 3. del0p.
n° 5 de Op.
ne2d Sm
Id. n3et4del0p.

Starke

Starke

Starke n° 4 delOp.
id.
id.
id.
id.

Starke n° 2 de8 p.
id.

 

 

=

=
2

Zi

zZ

Italie.

. 378, Russie.

id.

id,
id.

Ad.

Baviere

id.

Delle

id,
id.

 
