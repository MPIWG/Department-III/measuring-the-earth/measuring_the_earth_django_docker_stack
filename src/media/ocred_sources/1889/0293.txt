    
 
 

 

 

ee a a

 

WE - Krance et Algerie.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

    

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

i INDICATION Ei DIRECTEURS
Ne LATITUDE. |LONGITUDE. EPOQUE. ; INSTRUMENTS. REMARQUES,
DES POINTS. ET OBSERVATEURS.
Triangulation du Parallöle d’Amiens. (Partie Orientale — Suite.)
91 |; Hebuterne. .......- 50° 7:31”, 20°18' 8”| Mömorial du Döpöt
Be nn. 0292| 4 8 Göndral do Ia Auor-
93 | St. Quentin........ 49 50 55 | 20 57 13 | ee
94 | Premont .........: 50053121 3% |
GB GUBE 2. 49 53 52 | 21 17 16 SE a |
Bone... 49 33 48 | 21 17 19 2% ® |
97 | La Bouteille....... 49 53 14 | 21 38 16 N SH 3
98 | Mainbressy --.....- 49 45 2191 53 51 R a. 2,
99 | Tron du Sable..... 195323|22 6 0 a O3 = |
100 | Noirtrom ........-. 49 42 42 | 22 14 12 = = 3 2 |
101 | La Grande Croix ..| 49 56 20 | 22 26 37 >3 3
102 | Pagnou de Pusse- =.S 8 |
mange n.: 49 47 49 | 22 31 52 > |
0 Stmma. .....e.. 49 33 5 | 22 35 24
104 | St. Valfroy. ......- 49 34 16 | 22 56 16
Triangulation du Parallele ®Amiens. (Partie Oceidentale.)
12 Villers-Breton-) 1a.
DES ........: Dejä mentionnes.
14 | Vignacourt........
1 Bıry. 2... 49° 51' 25” 19050' 32” 5
106 - Hormmoy.....:.:.... 49 50 47 | 19 33 45 s8 5
107 Bay... 50 138 | 19 35 51 Se ®
- 108 | St. Leger aux Bois.) 49 50 2 | 1916 24 S ns 3
109 | Mont de PAigle....| 49 56 49 | 19 8 32 e ©. a
110 | Foret @Hellet..... 49 4720119 8 1 = AR =
1ll1 | Tourville la Cha- 2 58 2
Der... 49 56 38 | 18 55 33 ea S
112 | Les @randes Ventes. | 49 47 11 | 18 53 29 =.o © |
113 | Phare de PAilly...| 49 55 7 | 18 37 20 an =
114 | St. Laurent ....... 49 45 9 | 18 32 33
115 | Ingomyille......... 49 50 19 | 18 20 58
Triangulation du Paraliele de Paris. (Partie Orientale.)
1 | Pantheon Paris.... Memorial du Depöt
3 ' Belle Assise....... Deja mentionnes. len Vet x
26 | Malvoisine......... 3 2
116 | Rampillon......... 48083’ 4”| 20°43' 47” 3
De... 4852 9 | 20 49 51 ri
118 | Monceaux ......... 4841 4| 2161 S
119 | Ehavandon......... 231952.3117 9 E. &
120 | Allement....... ...|.48 45 40 | 21 27 48 = Se 5
DI Benses-. -:... ..: 48 23 25 | 21 44 26 2 ES 5 =
122 | Sompuis........... 48 41 59 | 21 59 25 S Su ®
123 | Chassericourt....... 48 31 20 | 2214 2 & Er 3"
Da... 48 50.16 | 22 19 52 m See E
125 | Longenille.......... 48 44 7 | 22 50 60 Z SR 3
126 | Moutiers .......... 48 32 23 | 22 53 19 = a8 S
127 | Mönil la Horgne... | 48 42 6 | 23 10 43 % o
28. :6rand. .....:..;:. 413994623 6 =
129 .Moneel.........:.. 48 2543| 23 23 5 =
20 Bu: 48 43 47 | 23 30 30 3
131 | Vaudemont......... 48 24 32 234 4 ©
132 | Amance............ 18453] 3575 7

 

 

 
 

 
