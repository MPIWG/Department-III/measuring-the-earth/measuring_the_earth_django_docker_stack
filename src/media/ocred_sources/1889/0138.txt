 

2
diquer les modifieations survenues et les progres r6alises ; la ot il n’y a pas eu de change-
ments, veuillez en faire la remarque formelle.

Vous faciliteriez grandement le travail de coordination, si vous vouliez suivre, dans
votre r&ponse, l’ordre et le num6rotage des questions posdes et lenir compte des observations
faites dans le rapport de 1883. (Comptes Rendus de 1883, Annexe IV.)

s En completant la carte ci-jointe, vous me fourniriez la possibilit& de presenter ä la
Conference une carte d’ensemble des nivellements de pr£cision.

Veuillez agr&er, Monsieur et tr&s honor& collegue, l’assurance de ma parfaile

consid6ration.
VON KALMAR.

QUESTIONNAIRE.

1. Longueur (en kilom6tres) des lignes niveldes jusqu’ä present :
a) Une fois;
b) Nivel&es A double dans le m&me sens;
c) Nivel&es ä double en sens inverse.
Suivent-elles les chemins de fer, les routes ou des canaux ?

9. Combien de reperes des differents ordres a-t-on fixös? Quelle est leur distance
moyenne ? De quelle nature sont-ils? Comment sont-ils fixes? Sont-ils ä trait horizontal, fixes
sur des murs, ou des eylindres verticaux ä surface horizontale, fix&s dans le sol ?

2

3. a) Dans quels points votre röseau est-il raltach& A ceux des pays limitrophes ?
Sur quelle frontiere et dans quels points la jonclion reste-I-elle & faire ?
L) Dans quels points aboutit il ä la mer, et existe-il des mar&ographes dans
ces points?

4. A quel plan de comparaison provisoire avez-vous rapporle les coltes de votre
reseau ? Quel est le repere fondamental qui le determine 7 Et quelle cote provisoire attri-
bue-on & ce repere fondamental ?

9. Description sommaire des appareils employes :

ca) Ouverture et grossissement des lunettes? Nombre et distance des fils du reticule ?

b) Valeur d’une partie de niveau ?

c) Longueur et division des mires; de quelle facon reposent-elles sur le sol? Par quel
moyen assure-t-on leur verticalite ?

Ö. Description sommaire des möthodes d’operation :

a) Est-ce que toutes les lignes sont niveldes A double et en sens»inverse ?
b) Esi-ce qu’on observe exactement dans l’horizontale, ou fait-on la lecture du niveau
ä chaque visee pour tenir compte de l’inclinaison dans la r&duction ?
c) Est-ce que les erreurs instrumentales sont &limindes par une &galit“ rigonreuse des
distances en avant el en arriere, et comment assure-l-on cette &galitd ?

 

 

 

 

 
