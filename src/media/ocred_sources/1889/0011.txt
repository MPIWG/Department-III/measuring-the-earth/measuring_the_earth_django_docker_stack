 

M. le professeur Helmert, Divecteur de Plnstitut georlesiqne royal de Prusse, Directeur du
Bureau central de l’Associalion, membre de la Commission permanente.
M. le gencral-major Schreiber, chef de la « Landesaulnahme ».

M. le lieutenant-colonel Morsbach, chef de la triangulation A la « Landesaulnahme ».
M. le professeur Albrecht, chef de section a Institut geodesique royal.

ROUMANIE.
M.

e gensral Falcoiano, ade de camp de >. M. le Koi de houmanie, chef du grand Etat-
major, a Bucarest.
SERBIE.

M. le professeur Andonowils, Direcleur de ’Institut geodesique serbe.

un

UISSE.

=

e professeur Hirsch, Directeur de P’Observatoire de Neuchätel, Secretarre perpeluel de
U’ Association geodesique internationale.
STATS-UNIS.
M. le professeur @. Davidson, assistant du « Coast and Geodetie Survey » de Washington.
| MEXIQUE.

M. le professeur Mendizabul Tamborrel, ingenieur-göographe, Directeur de l’Observatoire
de Thapultepee.
M. Angel Anguyano, ingenieur civil et Directeur de ’Observaloire de Tacubaya.

G. ASIE.

 

JAPON.

M. Terao, professeur A l’Universite imperiale et Directeur de ’Observaloire de Tokyo.

Assistent en oulre A la seance :
Comme invites de la Commission permanente :!
N M. le Comte Albert Amadei, conseiller au Ministere des allaires &trangeres, A Vienne.
! M. Backlund, membre de ’Academie de Saint-Petersbourg.
M. R. Bischoffsheim, de Paris.
M. le Dr B.-A. Gould, de Cambridge, des Btats-Unis.

Ä

1. Ulysse Grunchi, chef de service a I’Institul g6ographique-militaire de Florence.

=

   
