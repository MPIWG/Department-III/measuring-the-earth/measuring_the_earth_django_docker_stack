\
$

|

ee
I
7
—
=;

Dart

 

 

gl
R
W

we RUE

ana

ID
Or

AZIMUTS.

 

 

8 \ & Sn
"" Methode et instrument employe.

Annee et mois.

 

VW Angle entre la Polaire et le
| signal.

»

t
}

EF-

A. Angle entre la Polaire et
| le signal, determine au
' moyen d’un instrument uni-
, versel de Starke et Kam-
| merer.

B. Angle entre le signal et la
' mire meridienne, dont l’a-
zimut a 6t& determine au
moyen d’un instrument de
‘ passage de PistoretMartins.

i

!

»

 

 

1887 aoüt.

1887 octobre.

1888 juin.

1888 aoüt.
1889 mai.
1889 mai.
1889 juin.

1886 juillet,
aoüt et sep-
tembre.

TITRE DE LA PUBLICATION.

REMARQUES.

 

Pas encore publie.

 

 

I

Les Azimuts dans la Baviere
sont tous comptes du Nord
en passant par l’Est.

RAPPORT $UR LES AZINUTS, ETC. — 4

 

 

 
