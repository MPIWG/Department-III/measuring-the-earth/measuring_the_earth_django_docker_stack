 

ERTL, . sie

a
LET EEE TTE

Annexe B. XXV.

De

BORLIGEL

Resume des travaux executes depuis la Öonference geodesique de 1588,

l 1° On a termine, sauf de tres legeres rectifications, les observations angulaires du
ı röseau geodesique fondamental.

9° Le nivellement de preeision a &t& rattache, vers le Nord, au r&seau espagnol sur
le pont du Minho, pres de Tuy, et dans peu de mois il sera raltache, vers l’Est, au pont du
Cala, pres de Badajoz.

3° On a complete les observations de latitude et d’azimut au chälteau de Saint-Jorge.
Tous les caleuls ont &t& acheves et les r&sultats sont en voie de publication.

4° Le mar6ographe install& a Gascaes fonetionne depuis six ans.
! Je ne rapporte iei que les travaux qui peuvent intöresser directement la geodesie

internationale.

i

Lisbonne, le 16 aoüt 1889.

 

G.-E. pe ARBUES MOREIRA.

 

 

 

 

ANNEXES B XXV-XXVIb — A

# REN

ä
i
4
2
&

 
