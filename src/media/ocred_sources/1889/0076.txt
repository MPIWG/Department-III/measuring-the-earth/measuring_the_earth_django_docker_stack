 

 

70

 

gute kommen? Wer besonders giebt nicht zu, dass das Streben nach Wahrheit, wenn es
einzig und allein um der Wahrheit willen geschieht, die würdigste Mühe und die edelste
Sorge des Menschen ist, und dass man an diesem Zeichen die nobelsten Charaktere und die
höchste Civilisation erkennt ?

«Es war nicht unpassend diese grossen Ideen, welche so oft den Gegenstand Ihrer
Betrachtungen bilden, Ihnen an diesem Tage und an diesem Orte ins Gedächtniss zurückzu-
rufen, auf dem Boden Frankreichs, wo einer unserer grössten Minister, der berühmte Col-
bert, nicht nur um die Wissenschaften selbst zu rdaitn sondern auch um durch deren
Entdeckungen der Industrie und dem Handel zu nutzen, dem König Ludwig XIV. vorschlug,
die Akademie der Wissenschaften zu gründen, welche eigentlich die Mutter und Lehrerin
der geodätischen Wissenschaft gewesen ist; wo heute noch dieses nationale Institut alle
diejenigen in seinen Schoss beihl welche an den Schöpfungen und Erfindungen der Mensch-
heit in irgend welcher Richtung des menschlichen Geistes arbeiten, und welches ihnen die vor-
dersten Plätze in der Gesellschaft einräumt, wie dies sowohl der erlauchte Chef des französi-
schen Staates, Herr Carnot, welcher durch seine Fachstudien in den Stand gesetzt ist Ihren
Discussionen zu folgen und nöthigenfalls dieselben zu bereichern, sowie mein theurer und
berühmter Freund, der Herr Kriegsminister de Freyeinet, Ihr würdiger Kollege der Aka-
demie der Wissenschaften, bezeugen könnten, dieser hervorragende Ingenieur, welcher die
Früchte seiner hochgelehrten technischen Bildung der grossen Verwaltung, welche er leitet,
zu gute kommen lässt.

« Es macht mir Freude, an die Bande zu erinnern, wodurch die internationale Erd-
messung mit unserer Akademie der Wissenschaft verknüpft ist, welch letztere, ich wieder-
hole es, der eigentliche Heerd Ihrer Wissenschaft gewesen ist und deren Autorität in der
ganzen gelehrten Welt so gross ist. Diese Autorität, meine Herren, haben Sie wieder einmal
durch eine jener Eingebungen anerkannt und bestätigt, für welche unser edelmüthiges Land
sehr empfänglich ist. Denn offenbar ist mit Ihrer Zustimmung der Präsident Ihrer Permanenten
Commission, der General Ibanez, Marquis von Mulhacen, dessen ergebene Mitwirkung Ihnen
seit fünfunddreissig Jahren gesichert ist und der dieses Jahr wieder Ihre Arbeiten hätte präsi-
diren sollen, freiwillig zurückgetreten, um diese Ehre dem Herrn Herve Faye, dem Altmeister
der französischen Astronomen zu überlassen, welcher einen so hohen Platz unter Ihnen ein-
nimmt, dass Sie gewohnt sind, ihn als einen Ihrer Meister anzuerkennen, und welcher es
mir gewiss nicht übel aufnehmen wird, wenn ich neben seinem geachteten Namen an einen
andern französischen Gelehrten erinnere, dessen Verlust die französische Demokratie tief
beklagt; ich meine den General Perrier, einer jener Männer, der in Ihrer Vereinigung so
gern die tiefste Wissenschaft verbunden mit der grössten Hingebung anerkannte.

« Es sind das würdige Nachfolger Ihrer grossen Vorgänger, wie Cassini

,‚ Möchaıin,
Delambre, Biot, Arago.

Frankreich hat seinen gerechten und ruhmvollen Antheil an der
Geschichte der Krdmessung. Aber es ist ein Zug unseres Jahrhunderts, das zu verallgemei-
nern was dem menschlichen Geiste nützlich sein kann. Indem die Rosie ungen an solchen

Werken von allgemeinem Interesse arbeiten, welches ihre Grenzen und die der gegenwär-

tigen Zeit überschreiten, mehren sie ihre Ehre und ihren Ruhm. Sie erfüllen eine vielleicht

 

 
