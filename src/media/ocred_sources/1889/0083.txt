 

y

&
&
\
|

r
Ä
f

 

 

77
Arbeiten in Sachsen einen Bericht einsenden zu wollen, welcher bis jetzt dem Bureau noch
nicht zugekommen ist.

«Der Herr Oberstlieutenant Hartl, von Oesterreich, kann an unseren Diseussionen
nicht Theil nehmen, da ihn die Regierung von Griechenland berufen hat, den Erdmessungs-
dienst in diesem Lande zu organisiren.

« Herr Rue unser verehrter Geodät des Nordens, muss wegen seines vorgerück-
ten Alters auf längere Reisen verzichten und der Zustand seiner Gesundheit hat ihm nicht
erlaubt, nach Paris zu kommen.

« Herr von Bauernfeind schreibt uns, dass es weder ihm noch seinem Gollegen,
Herrn Seeliger, dem Director der Sternwarte von München, möglich sei unserer CGonferenz
beizuwohnen. Sie haben bis Ende dieses Jahres die Zusendung eines Berichtes über die in

Bayern ausgeführten Arbeiten versprochen.

« Die beiden Delegirten von Portugal, die Herren Moreira und d’Avila, haben darauf
verzichten müssen, sich nach Paris zu begeben. Herr Moreira hat mir einen kurzen Ueber-
blick über die in Portugal seit der Gonferenz in Salzburg ausgeführten Arbeiten zugesandt.
Dieser Bericht wird mit denjenigen der andern Länder in den Beilagen ahgedruckt werden.

« Wir haben gestern einen Brief von seiner Excellenz dem General Stebnitzki erhal-
ten, welcher es sehr bedauert, aus Gesundheitsrücksichten verhindert zu sein nach Paris
zu kommen. Es ist dies um so mehr zu beklagen, als das grosse Reich des Nordens, wel-
ches: so ausgedehnte und bedeutende geodätische Arbeiten aufzuweisen hat, ohne Vertreter
an der Conferenz der internationalen Erdmessung ist, für deren Entwicklung dasselbe von
Anfang an das lebhafteste Interesse gezeigt hat.

« Herr Oberst Capitaneanu, von Bukarest, telegraphirt soeben an den Marquis von
Mulhacen, dass Familientrauer ihn augenblicklich am Reisen verhindere.

« Dies sind, wenn ich nicht irre, alle Briefe von unsern Collegen, welche ihre Abwe-
senheit entschuldigt haben.

« Dagegen haben andere Gollegen ihre Ankunft angezeigt. Ihre Zahl ist aber leider
nicht sehr gross; diese Ungewissheit in die uns das Stillschweigen der meisten Delegirten
versetzt hat, ist um so mehr zu bedauern, als wir auf die Anfrage des Herrn Ministers nach
der Anzahl as von uns erwarteten Collegen ihm keine sichere Antwort geben konnten. Wir
haben an achtzig Commissäre die Einladung versandt und nur acht haben sich zum voraus
angemeldet. Sie sehen jedoch, dass glücklicherweise die Zahl derjenigen, welche den Weg
nach Paris gefunden haben, bedeutend beträchtlicher ist. Unter den Astronomen, welche uns
geschrieben haben. erwähne ich unter Anderen Herrn Folie, Director der Sternwarte in
Brüssel, welcher uns gleichzeitig eine Mittheilung über den Einfluss der täglichen Nutalion
auf das Meeresniveau angekündigt hat.

« Wir erlauben uns nun auf eine durch Vermittlung der Regierung von Berlin
zwischen derjenigen von Frankreich und der Permanenten Commission staltgefundenen
Correspondenz überzugehen. Der darauf bezügliche Brief den wir von Herrn Direktor Hel-
mert erhalten haben, lautet folgendermassen :

 
