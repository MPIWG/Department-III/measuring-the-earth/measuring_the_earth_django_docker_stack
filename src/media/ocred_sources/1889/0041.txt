 

 

 

3D

M. le President passe au troisieme chapitre du programme, comprenant les Rapports
de MM. les Deleguds sur l’avancement des travaux dans leurs pays respecuils.

En suivant l’ordre alphabetique des pays, il appelle l’Autriche-Hongrie et donne la
parole AM. le D" Tinter, President de la Commission autrichienne pour la mesure des degres,
lequel rend compte en langue allemande des travaux de celte Commission.

Ensuite, les repr6sentants de Institut g&ographique-militaire de Vienne, M. v. Kal-
mär et M. v. Sterneck, communiquent des r&sum6s sur les Lravaux ex6cutös par la seclion
g6od6ösique et astronomique de cet &tablissement.

Il resulte entre autres points du Rapport de M. ». Kalm“r, qu’ä la demande du Gou-
vernement grec M. le colonel Hartl, accompagne de deux autres olficiers, est parti V’öt& der-
nier pour la Gröce, en emportant l’appareil de bases, pour organiser les Iravaux geodesiques
dans ce pays. (Juant aux nivellements de preeision, ils ont &t& augmentes, pendant lannce,
de 100kım environ, et les points de reperes fondamentlaux, au nombre de 7, sont maintenant
tous places.

M. le Lieutenant-Colonel v. Sterneck eonstate qu’en 1889 on a ex6culd une dätermi-
nation de longitude entre la Schneekoppe et Dablie, ainsi que des determinations de lali-
(ude et d’azimut sur six stations du röseau geodesique de la Boh@me. M. le rapporteur fait
entrevoir que, par celte longitude, ainsi que par des mesures Lr&s soigneuses de latitude,
W’azimut et de pesanteur, ex6cutdes dans 40 stations de la Boh@me, on possedera sous peu
tous les &löments pour d&duire la surface geoidale dans le reseau de la Boh@me.

M. v. Sterneck communique &galement, au nom de son Gollegue, M. le colonel Hartl,
retenu en Grece, un Rapport sur les travanx trigonomötriques diriges par ce dernier, qui
comprennent d’abord la jonction trigonometrique de l’Observatoire de l’Universitö de Vienne,
et ensuite la conlinaation du reseau de premier ordre dans la Transylvanie.

(Voir ces difförents Rapports d’Autriche aux Annexes, B. XXIII.)

M. le President, ayant vemercie MM. les Delögues d’Autriche-Hongrie, invite M. le
colonel Hennequin A prösenter le Rapport pour la Belgique.

M. Hennequwin fait savoir que depuis la derniere Conference generale, les travaux de
Institut cartographique militaire, relatifs A la triangulation, se sont born6s essentiellement
aux reconnaissances de points geodesiques de premier ordre.

En caleulant Pexactitude relative de la triangulation belge, d’apres la formule indi-
quee par le general Ferrero, on a trouve m = & 0 89, tandis que la compensalion, par
application de la formule d’Enke, avait donne e = + er

M. Hennequin menlionne en outre de nouvelles observations astronomiques de lali-
tudes et d’azimuts, qui ont en general donn& de tres bons resultats et ne Lrahissent des devia-
tions un peu consid6rables que dans le sens des azimuts.

Les nivellements de pr&eision ont &t& commences en Belgique en 1887 entre les
&cluses de Heyst, oü existe un repere hollandais, et Ostende, olı on a &tabli un repere fonda-
mental. L’annde suivante, Ostende a &i& reli& A la frontiere frangaise, vers Ghyvelde; ces

 
