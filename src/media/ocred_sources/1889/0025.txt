 

 

 

 

 

19

comme point de depart, que si, jusqu’ä cette epoque, la Belgique parvenait a combler cette
lacune.

« Nous ne nous dissimulons pas que l’aecomplissement de ce travail, dans un si court
delai, demandera des efforts exceplionnels; mais nous espörons que l’avantage de posseder
le point de d&part de toutes les altitudes du continent europcen, justifiera, aux yeux des auto-
ritös scienlifiques et administralives de votre pays, les sacrilices necessaires.

« En raison de la grande influence et de la haute situation dont vous Jouissez dans
votre pays, nous estimons que le meilleur moyen d’arriver ä la realisation de ce projel qui
nous lient A caur, est d’invoquer votre puissanl appui et volre bienveillante intervention.

« Nous esperons que vous voudrez bien, aussitöl que possible, nous informer si le
choix que nous dösirons faire d’Östende, Lrouvera en Belgique le concours indispensable
que nous nous sommes permis de vous signaler.

« Nous saisissons, Monsieur le Gen6ral, celte occasion de vous presenter les nouvelles
assurances de notre haute consideralion.

« Le Seeretaire:: Le President :
(signe) Dr A. HIRSCH. (signe) General IBANEZ.»

M. le General Liagre nous a fait savoir, en r&ponse, que les demarches qu’il avalt
entreprises immediatement aupres du Gouvernement el, en particulier, de M. le Ministre de
la guerre, aboutiraient et que les Chambres voteraient le credit de 15000 fr. que lui deman-
Jerait M. le Ministre de la guerre dans le but d’exdcuter des nivellements de precision.

Et, en effet, j’ai eu le grand plaisir d’apprendre, par notre collegue, M. le Colonel
Hennequin, que ces travaux de nivellement sont en cours d’exseution et qu’ils pourront ätre
terminds l’annde prochaine ou, au plus tard, au printemps suivant; de telle sorte que, non
seulement Je mar&ographe d’Ostende sera ainsi reli6 A d’autres points de l’Ocsan, mais qu'il
sera rattach& aux grands reseaux de !’Allemagne, de la France et des Pays-Bas.

Si cette question entre de nouveau dans le cadre de nos discussions, le rattachement
d’Ostende, dans un avenir Ires rapproche, pourra etre envisage comme acquis.

Vous Lrouverez dans le Rapport que M. le professeur Helmert, Directeur du Bureau
central, va vous communiquer, des renseignements sur d’autres questions scientifiques qui
ont fait !’objet de correspondances entre le Bureau central et divers membres de l’Association.

M. le President, suivant Vordre du jour, donne la parole ä M. le Directeur Helmert,
qui lit le rapport suivant du Bureau central :

Rapport dw Bureau central pour 1889.
Sur ma proposition, Ja Commission permanente, dans sa session de Salzbourg, a

ratifi6 le programme suivant pour les travaux scientifiques du Bureau central :

e

 
