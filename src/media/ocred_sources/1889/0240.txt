DM Autriche-Hongrie.

 

INDICATION
DES POINTS.
>
Hrobaei-Loka
Lamanaskala

Wysoki...........-
Lanckorana

 

 

Lubien
Mogielnica
Wielki Salasz

Wioszezyce
Kobelnica

Pissana hola
JAWorZz.....8%. >.
Wale. 2 228.255:

Base de Tarnow...
Terme Nord.
Base de Tarnow...
Terme Sud.
Odporiszow

Beleryt
Krulowa
St. Martin

Kiwoez. 2.0.0.8.
Kamieniee

Niwiska
Krolewski
Wielopolski

Czarnowka
Gwoznica
Malawa

Kamien..
Hussow

 

 

LATITUDE.

49°49' 25°
49 45 52

49 54 58
49 50 59,

50 3 52

49 58 35
49 46 37
49 32 38

49 29 26
49 39 23
49.43 19

46 56 13
49 56 42
ya)

49 28 54
49 34 34
49 539

50 423
0. 09

50.4 1

»0. 910
50 8 54
50 5 54
4959 7

49 48 53

49 57 54

0 8 9
50 12 49
50 10 49
49 57 51

49 50 21
49 50 24
50 ° 005

50 18 10
49 59 16

50 19 29
50 19 48

 

LONGITUDE.

36° 49! 54"
37.346

37 14 50
37 22 22
a1 au 28

37 4 7
37 37 32

37.5. 8
37 42.56
37 44 46
37 58 26
37 54 42
3: 11.21

38.1.9
38 18 58

38.25 37 |

38 95 49
38 34 52
38 34 47

33 30 42
38 34 16

38 35 47
38 34 58

3841 3
38 38 6
33 40 54

>39 1 2
39. 0 22
39.098
39 15 54
39 34 40
39r21 16

39 20 40
9.41 19
39 47 33

98
1

6
30

 

 

 

 

 

 

EPOQUE.

1875
1875-76

1875
1875-76

1848-75-76

1848-75
1848-49-75

1848-75
1848-75
1850-75

1875
1875
1848-75-76

1848
1848
1848-75

1875-76
1875
1848-49-75

1849
1849

1849
1848-49

1849
1849
1848-49-75

1875
1848-76
1848-49

1848

1848
1848-76

1876
1875-76
1848-76

-1848
1848-76

1848
1848

DIRECTEURS
ET OBSERVATEURS.

INSTRUMENTS.

 

 

 

 

 

 

 

 

 

 

 

Horsetzky.
Hoörsetzky, Tuma.

Tuma.
Kalmär. Tuma.

Ioh. Marieni, Kalmär,
Tuma.

Ioh. Marieni, Kalmar.

id.

id.
Nemethy, Tuma.

Rueber, Tuma.

Tuma.
id.
Nemethy, Tuma.

Ioh. Marieni.
id.
Nemethy, Kalmar.

Tuma.
Kalmar.
Ioh. Marieni, Rueber, Kal-
mär.
Rueber.
rd:

id.
loh. Marieni.

id.
Rueber.
Ioh. Marieni, Rueber, Kal-
mar, Tuma.
Kalmär.
Ioh. Marieni,
Ioh. Marieni.
id.

Tuma.

CS
Ioh. Marieni, Tuma.

Tuma.
Kalmär, Tuma.
Pechmann, Tuma.

Pechmann.
Pechmann, Tuma.

Pechmann.
ick

 

Starke n° 2 de8 p.
Starke n2 de8 p.
er 10. p:

Starke n° 2 de 10 p.
Reichenbach n° 2 de
12 p. et Starke n° 2
de 10 p.

Reichenb. n° 1 et 2
de 12 p. et Starke
n°-2 de 10 p.

Reichenbach n° 1 et

2de1l2p

Reichenb. ne 1 et 9:

de 12 p. et Starke
de 10 p.
Reichenb. n° 1 et 2
de 12 p.
Starke astronom. de
8p.et n°2 de 10p.
Reichenbach de 12p.
et Starke n® 2 de
10,D:
Starke Se de 10 p.

Starke astr. de8 p.
et. n° 2 de. JO p:
I 12 P.

Starke astronom. de
Sp.etn°2del0p.
Starke Sn 2 de 10. pı

Reich. n® 1 de12 p. et

Starke n® 2 de IOp
Reichenb. de 12 p.
id.

Reichenb. et Ertel
astron. de 12 p.

‘Starke astr. de 8 p.

et 10 p.
Starke de 10 p.
Reichenb. de 12 p.
Reich. n®1de12p.et
Starke n°2 de 10 p.
Starke n° 2 delOp.
id.

id.
MB de 1% Pr

Reichen, = 12 p. et
Starken’ 2 de 10 p
Starke n° 2 de 108.

id.
Reich.n°2de 12 p. et
Starke n® 2 de 10 p.
Reich. n° 2 de l2p.
Reich. n’2del2p. et
Starken’ 2 de 10 p.
Reich. u de 12 p.

id.

 

N. 374, Russie.

N. 375,

N. 376,

 

 

id,

id.

 
