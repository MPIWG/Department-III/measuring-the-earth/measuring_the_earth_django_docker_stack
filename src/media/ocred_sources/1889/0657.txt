 

 

 

 

—

ERTIERER, &

 

seen ini

I

Jede Sektion bestand aus einem Vermessungs-Dirigenten, einem Assistenten und 10
bis 15 kommandirten Soldaten. Gleichzeitig sind von diesem Personal alle einstellbaren
Thürme je 6 mal angeschnilten worden.

Von den 14 Netzpunkten befinden sich 2 auf Thürmen, I auf einem Steinpfeiler zu
ebener Erde; für die 11 andern Stationen waren Signale von zum Theil so bedeutender Höhe
erforderlich, dass die mittlere Beobachtungshöhe 154 betrug. Signal Bless hat 26 m. Beo-
bachtungshöhe und 316 Leuchthöhe.

Das Thüringische Netz bedeckt ein Gebiet von etwa 9480 Quadratkilometern oder
172 geographischen Quadratmeilen.

Il. Die Rheinisch-Hessische Kette (vergl. Netzbild 2).
Sie enthält:

1. 6 Anschlusspunkte, die zugleich theils der Hannoverschen, theils der Hannoversch-
Sächsischen Kette, theils dem Thüringischen Netz angehören und durch deren Ausgleichung
bestimmt sind ; |

2. 23 Kettenpunkte;

3. 38 Zwischenpunkte erster Ordnung.

Diese sämmtlichen Punkte sind mit Beobachlungsvorrichtungen bereits versehen.

Auf den 6 Anschlusspunkten, sowie auf den 4% Kettenpunkten Stimmberg, Kreuz-
bere, Knüll und Taufstein sind im Sommer 1889 die Beobachtungen beendet worden.

Die Kette wird voraussichtlich 1891 fertig beobachtet sein.

III. Das Bonner Basisnetz (vergl. Netzbild 2).

Die im Zuge der Rheinisch-Hessischen Kette gelegene im Jahre 1847 gemessene Basis
bei Bonn soll in einem der nächsten Jahre von Neuem gemessen werden. Die Endpunkte
fanden sich 1888 unversehrt vor; die Rekognoszirung eines neuen Basisnetzes hat stalt-
gefunden.

IV. Der Niederländische Anschluss bei Aurich.

Nachdem seitens der Königlich Niederländischen Geodätischen Kommission die Er-
oehnisse der Beobachtungen auf den drei Niederländischen Stationen Finsterwolde, Horn-
huizen und Uithuizermeden während des verflossenen Winters übersandt waren, haben (lie
Ausgleichungen stattgefunden. Die Ergebnisse sind gut.

V. Der Niederländische Anschluss zwischen Bentheim und Mwverath.

Die Rekognoszirungen, welche dazu dienen sollen, die Verbindung zwischen der
Triangulation der Königlich Niederländischen Südprovinzen und der Rheinisch-Ilessischen
Kette herzustellen, sind beiderseits so weit gefördert, dass eine Vereinbarung über die Ge-
staltung des Netzes in nächster Zeit wird erfolgen können.

ANNEXES B. XXVIp-XXVII — 2

 
