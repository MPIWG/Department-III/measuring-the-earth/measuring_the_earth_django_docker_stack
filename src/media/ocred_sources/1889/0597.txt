 

ENNERETTTR EUER

mm nn

 

 

 

 

SEEN EDER

 

Annexe B. XVIlla.

RAPPORT
TRANAUN ENECUTE EN FRANGE, EN ALGERIE ET EN TUNISIE

Service Geosraphique de Armee (Section de Geodesie)
(OCTOBRE 1888-00T0BRE 1889)
PAR

M. 1» Genxtrau DERREGAGAIX

OPERATIONS GEODESIQUES
a) Meridienne de France.

RATTACHEMENT DE LA BASE DE CASSEL

Pour servir de contröle aux operations de la nouvelle Meridienne, une base de veri-
ficalion a ete fixee A P’extr&mite Nord du r&seau, dans les environs de Cassel, sur la roule
nationale nume&ro 16. Cette base est orientee Nord-Sud et offre un developpement de sep!
kilometres et demi environ. Le terme Nord est situ& sous le peristyle de la tour de ’Eglise
de Wormhoudt; le terme Sud sur l’accotement Ouest de la route au lieu dit « La Trompe ».

Le rattachement g&odesique de cette base au r&seau de la Meridienne s’appuie sur le
cöt& Gassel-Rosendaöl-les-Dunkerque, il a &t& effectu& au moyen de deux points auxiliaires :
Bollezeele et Hondschoote. Il a exig& en tout six stations.

Les observalions ont &t6& faites, dans les m&mes conditions de pröcision que pour la
Meridienne, par M. le Gommandant Defforges et MM. les Gapitaines Lubanski et Bourgeois,

sous la direction de M. le Lieutenant-Golonel Bassot.
ANNEXE B. XVII — A

 
