 

 

NIVELLEMENTS.

Im Sommer 1889 sind nıvellirt worden :
1. Die Schleife Tondern-Raahede-Baungaard-Foldinebro-

Hadersleben-Apenrade Zmall . . . ... ...115 Kılomeler.
9. Der Anschluss Hadersleben-Taarning (Amal). . . 15 »
>, ber Anschlussher Ein Ama, . ...  . . _ 6 >
A. Der Anschluss Ploen-Marienleuchte Amal) . . . 8 »

Durch die Nivellements 1 und 2 wird an 4 Grenzpunkten (bei Raahede, Baungaarl,
Foldingbro und Taarning) die Verbindung mit dem Königlich Dänischen Präeisions-Nivelle-
ment hergestellt.

Durch das Nivellement 3 ıst eine neue Verbindung (die dritte) mit dem Königlich
Bayrischen Präcisions-Nivellement geschaffen worden.

Das Nivellement 4 setzt den bei Marienleuchte befindlichen Mareographen mit dem
Nivellements-Netze des Staates in Verbindung.

Letzteres ist nunmehr fertig und hat eine Länge von 16267 Kilometern.

Es erübrigt nun noch, die älteren Linien mit besonderen in festen Bauwerken
anzubringenden Höhenmarken und Mauerbolzen zu versehen (vergl. Nivellements der Trigo-
nometrischen Abtheilung der Landesaufnahme, Band VII, Seite 1-2 und Tafel VII). Mit
dieser Verfestigung wurde im Sommer 1889 in der Provinz Schleswig-Holstein der Anfang
gemacht.

VER(KFFENTLICHUNGEN.

In den Jahren 1888 und 1889 hat die Trigonometgische Abtheilung veröffentlicht :

1. Abrisse, Koordinaten und Höhen sämmtlicher von der Trigonometrischen Ab-
theilung der Landesaufnahme bestimmten Punkte. Achter Theil. Regierungsbezirk Breslau.
Berlin, 1888.

2. Nivellements der Trigonometrischen Abtheilung der Landesaufnahme. Siebenter
Band. Berlin, 1889.

Paris, den 11. Oktober 1889.
MORSBACH,

Oberstlieutenant.

 

 

 
