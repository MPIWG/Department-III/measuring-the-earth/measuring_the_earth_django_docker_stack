 

h

pourrait-on en expliquer une partie. Toutefois, avant d’adopter la conelusion de M. koche
sur l’etat interieur de la Terre, je erois utile de faire un resum& rapide des rösultats obtenus
pour l’aplatissement par diverses m&thodes; car si ces valeurs meltent en &vidence des diffe-
rences sensibles, il restera perwmis d’esperer que des determinations nouvelles, plus nom-
hreuses et plus pröcises, parviendront A supprimer le desaccord.

4. — Occupons-nous en premier lieu de la valeur de l’aplatissement, conelue des me-
sures de degres de latitude. Deus mesures semblables suffisent thöoriquement pour Lrouver
les dlöments «a et &, demi-grand axe el aplatissement de Pellipsoide terrestre suppose peu difle-
rent du g&oide;, mais ces dimensions sont fortement affectees par les erreurs commises sur les
latitudes et par les attractions locales. Pour obtenir une pröcision plus 6levee, il &tait nalurel
le eombiner entre elles toutes les mesures obtenues dans les triangulations les plus impor-

4

tantes. C’est ce qu’a fait Airy, qui a trouve & — Zoo a, Par la discussion de 14 arcs de me&-
999,33
1

vidien et de 4 arcs de parallele. Bessel a obtenu quelques anndes plus fard e — 599 15° Ges
’

calculs ont &t& repris par Clarke, qui a profile de ’extension donnee depuis Bessel aux arcs

russe et anglais, mais surtout & lare ındien; ıl’a trouve & = 9995 valeur admise assez
AL ’

gönsralement aujourd’hui, et pour laquelle se presente la contradietion dont nous avons
parl&, tandis qu’elle n’existait pas pour le nombre de Bessel. Je ne crois pas que nos con-
naissances sur ce point soient arretdes d’une maniere definitive, et je voudrais indiquer en
quelques lignes les desiderata de la möthode adoptse par Bessel et par Clarke. Il faut tout
d’abord rappeler brievement le prineipe de la methode.

Soient M, MM’... des points de la surface de la Terre, situes de part et d’autre el
aA de pelites distances d’an möme meridien, 9, ©.9..... leurs latitudes observees; on peut,
par une reduction facıle, supposer lous ces points situ6s dans un m&me plan meridien.
Dösignons par 5.8.8... lesarcs geodesiques qui, sur la surface des mers prolongee ä tra-
vers le continent, r&pondent aux distances MM. MM.....; ces arcs seront connus par la trian-
oulation et les calculs qui 8’y rattachent. Si le geoide pouvait Ötre consider& comme un ellip-
soide de rövolution, et si ces normales coincidaient avec les verticales des points M, MM.
on aurait, par la formule de rectification de l’arc d’ellipse,

(3) s’ = a[A, (0 — 0) 4A, (Sin? 0’ — sin? 9) + A, (sin 4 — sın 4 ®2)-+...].

ou a reprösente le demi-grand axe, et A,, A,, A,... designent des fonctions connues de
l’aplatissement e de l’ellipsoide. Soient a + da, e 1 de les valeurs exactes de ces Elements,
o+2,9 + ®.... les angles que fait le grand axe de V’ellipsoide avec les normales a sa sur-
face aux points oü cette surface est rencontr6e par les verticales MN, M’N.... des points
M,M...; 2, @ ®'... se composent des erreurs des observations, et des pelites inclinaisons
des verticales pr&eedentes sur les normales A l’ellipsoide. On admet que la triangulation est assez
pr&cise pour qu’on puisse nögliger l’erreur commise sur s‘. En remplacant dans (9), ®, ne .

 

 

 

Mini nn
