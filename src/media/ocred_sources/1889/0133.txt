 

 

 

had

II. Conto der kapitalisirten Einnahmen für 18S8.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

EINNAHMEN. Mark. |Pf.| Mark. |pt.| Fr. |c.| Fr. |c.
Reservirter Kapitalbestand am 31/12 1887. | 14779 |80 18474 |75
Kopitalisirte Binzahlunsen . . . . . ...382040 2550 | —
Zinserträge. 545 [10 681 37

ja go eos IR
AUSGABEN.
Aus den Kapitalisirungen gezahlte Jahres-
bestrase Ghlle tur IS. .:..2.35 20 | — 300 | —
Desgleichen 6 Staaten 188 . . . . 2... | 2160| — 2700 | —
Norwegen 1887 bereits gez. Beitrg. . . . au 300 | —
Einkauf von 12000 Marks 3 !/ %/, preuss.
Osella 368 190| 83008 190 461 |12| 3761 |12
Reservirter Kapitalbestand am 31/12 1888 . | ae, 117945 \—
17364 |90 21706 |12

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Da der Bericht bewiesen hat, dass die Buchhaltung des Gentralbureaus in voll-
ständiger Ordnung geführt ist und alle Auslagen durch Belege begründet sind, so ertheilt die
Commission dem Herrn Professor Helmert, Direktor des Centralbureaus, volle Entlastung für
das Jahr 1888.

Der Bericht schlägt ausserdem vor, dem Gentralbureau einen neuen Kredit von
1600 M. =2000 Fr. zur Fortsetzung der Beobachtungen über die Schwankungen der Erd-
achse zu gewähren.

Dieser Antrag wird ebenfalls einstimmig angenommen.

Herr Helmert liest folgendes Programm für die wissenschaftlichen Arbeiten des
Gentralbureaus für das Jahr 1890 vor:

1° Fortsetzung der Vorstudien über die Schwankungen der Erdachse.

2° Fortsetzung der Berechnung des geodätischen Theiles der Vermessung des Längen-
grad-Bogens im 52sten Parallel.

5° Fortsetzung der Berechnung der Lothabweichungen.

4° Vorstudien zur Fixirung des Normalnullpunktes für die absoluten Höhen Europas,
gemäss dem Beschluss der Generalconferenz.

 
