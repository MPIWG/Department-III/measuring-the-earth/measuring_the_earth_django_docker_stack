 

4

Die Werthe in Columne 12 und 43 sind mit 0,993549 vergleichbar.

Bemerkenswerth ist, dass sowohl aus den Messungen auf Maui, wie aus denen in den
Seealpen bei Nizza eine Aenderung der Schwerkraft mit der Höhe hervorgeht, welche nicht
derjenigen in freier Luft entspricht, sondern um die volle Anziehung der Gebirgsmassen
modificirt ist. Auch eine generelle, Compensation dieser letzteren durch unterirdische Defecte

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Geo-

s NAME Geo- graphische | Meeres- | Beobachtungs-
E LAND. der graphische ro oe werth

z ITPAIPLON. Breite. Greenwich | Metern. in Metern.

ab.

1 2 3 4 9 6 7

1 Italien Padua 15 3% 3 MN 52 19 0,993 5477

I Spanien Madrid (Instituto geogräfico) | 40 24 52 - - 662 0,992 9659
3 » Madrid (Observ.astronomieo) | 40 24 30 — 3 Al 657 0,992 9634
h Russland Nowaja Semlja 12, 22.806 52 36 an — |
5 » Archangel 6% 39 20 10 31 6 = _ |
6 > Moskau 55 45.20 | 37 3% 142 | 0,9945520 |

> Geltuchin 53 48 20 39 48 16% 0,99% 2802
8 » Gross-Scheremetewka 51 37 51 4 49 152 0,994 1558 |
9 | Nord-Amerika | Washington . er 10 | [0,992 990]
10 » San-Franeisco 37 7 1.1993 96 115 0,992 845 |
u > Lick Observatory . 37.:20° \|—121.39.11 [14982 ..|..0,992 837 |
12 Hawaii | Honolulu 91 18 145759 3 0,991 835 |
13 » Haiku 20 56 —156 20 117 0,991 774
14 » Pakaoao . 20 43 156.15 3001 0.991 132
15 » Lahaina;;; 0 32 HR a 3 0,991 724
16 Polynesien Caroline Island 200 —150 14 | 2 0,991 229
|
|

=

ag u armen

eumzgprases

 

1]
|
|
|

 
