 

 

 

 

 

 

 

 

 

 

 

BTALONS | EQUATION DES REGLES
DE DE L’APPAREIL,

CGOMPARAISON. COEFFICIENTS DE DILATATION.

RAITS MERMDBANT DETRE RELATES.

 

 

Etalons A et A. Les deux &quations

| Pour les regles: 50 s6- | moyennes de la portee
ries de comparaisons | pour les deux mesures
avant la 1re mesure: de la base sont:
er one Is JATBICH-DFETFH

   

deux mesures; 66 S&- anne J :

: = — 60,002376 —— — I R:
riesapresla deuxieme 60,0 dba
ms are 1"° mesure et

Pour les microscopes !:
13 series de comparai- | 60,0023982
sonspendantla 1"° me-
sure; 5 series pendant
la 2° mesure.

f
I

— 48dRa

 

2° mesure.

Etalons A et A. Equation moyenne

BF Pour les regles: 79 s&- de la portee:

I vies . om anaisons | 2, BC DPFEFH

A avantlamesure;6l1se- | zn nnozaar HUe/T

Ei} riesapres la mesure. | og la

a Pour les microscopes :
| 11 series de comparai-

sons au cours de la

mesure.

 

 

Etalons A et A. Equation moyenne
4 Pour les rögles: 57 s£- de la poriee:

2 x . BD) Y E U ®
Ba es de comparaisons | ATBLCHD+HEFH

 

 

 

 

N avant la mesure et in nhegaaT= > R }
R —h 32937 ug R;
| autant apres la me- IR, BE
ji sure.

, ; Pour les microscopes :

 

I ne

| / series de comparai-
a ij sons au cours de la
ij mesure,

|

ri

A

 

Du fait mentionne A la
colonne precedente
(nouveau groupe de
comparaisons intro-
du au milieu (de 1a
mesure)ilresultel’em-
plor. de deux "equa-
tions distinctes de la

I Ktalons A et A.

| Pour les regles : 53 se-
|| res de comparaisons
. ı avantlamesure: 60 s6-
ı ); res au milieu de la
| ı Mesure et 80 series
Par apres la mesure. Ce
/ nouveau groupe de

 

 

re

 

 

 

La base fut divisee en 6 troncons par des reperes de fer soli-
dement enfonces dans le sol. Ghaque troncon fut mesure
exactement, de telle sorte que les 6 troncons furent tous
reellement mesures individuellement deux fois, aller et
retour.

Une petite triangulation accessoire a permis de calculer les
valeurs des Ironcons et de les comparer aux valeurs mesu-
res. La longueur totale de la base par la moyenne des deux

. p
mesures — 39184 ,0321. La longueur deduite de la triangu-

pP
lation accessoire donne : 39183,9572.

Pas de triangulation de verification.
Cette base avait ete mesurde en 1825 par le capitaine Everest
avec une chaine d’acier.

Le lieutenant A.-S. Wangh, charge de la reconnaissance,
s’etait altache a retrouver les bases d’une Lriangulation
mesuree a la chaine en 1825 par le colonel Lambton. Cette
recherche fut vaine; une nouvelle ligne fut donc choisie.

La base fut partagee en 3 troncons avec Lriangulation acces-
soire de verification :

I
Valeur mesurcee — 4157815475
Valeur triangulee = 41578 ‚4687

Base divisde en 4 troncons avec v£erification par triangulalion
accessoire !

pP
Valeur mesurde = 36685,7946
Valeur triangulee = 36685.3275

 

 

 

 

 

 
