 

 

 

Annexe A. VlIa.

BERICHT

ÜBER

DIE LOTHABWEICHUNGEN

Unsere Kenntniss des Verlaufes der Lothstörungen in einzelnen Gebieten hat eine
sehr wesentliche Bereicherung erfahren durch die grosse russische Arbeit über Bulgarien,
im XLIII. Band der Sapiski der topographischen Ahtheilung des Grossen russischen Generäl-
stabes, unter Redaction von General-Lieutenant Stebnitzki bezw. General-Major Lebedeff.

In den Jahren 1877-1879 hat der russische Generalstab und das topographische Corps
unter Lebedefls Leitung eine umfassende Triangulation von Bulgarien, gestützt auf 6 Grund-
linien, ausgeführt. Sie sendet ihre Ausläufer bis Konstantinopel und Novibazar und schliesst
in 11 Punkten an die österreichischen Ketten in Rumänien von 1855 und in 2 Punkten an
das russische Hauptdreiecksnetz an. 46 Punkte sind astronomisch in Breite und Länge (meist
telegraphisch) bestimmt. Für die Breiten dient Köstendsche am schwarzen Meer als Ausgang.
Dagegen ist die Summe der Lothabweichungen in Länge auf Null gebrächt.

Zunächst ist mit Walbecks Dimensionen des Erdellipsoids gerechnet, sodann aber
auf Glarke 1880 reducirt.

Im allgemeinen entsprechen die resultirenden Lothabweichungen, soweit sich das
durch den Anblick der Karte allein beurtheilen lässt, den Erhebungen des Festlandes. Doch
sind Andeutungen über Irregularitäten der Massenlagerung vorhanden. Z. B. zeigen 5 Punkte
an der Donau von Widdin bis Silistria, gegen Köstendsche, welches nahezu in gleicher
Breite lregt, Lothabweichungen in südlicher Richtung von 2 bis 10”, im Mittel 7”, was wohl
nicht aus der Wirkung der Gebirge (des Balkans) allein zu erklären ist. Am Balkan selbst
steigen die Abweichungen bis 21”, entsprechend der Maximalhöhe des Gebirges von 2374.
Adrianopel hat fast null, Konstantinopel dagegen 13” in südöstlicher Richtung; hier dürfte
wohl der Gegensatz von Meer und Land im N. und S. zum Ausdruck gelangen.

Diese grosse Arbeit wird noch an Bedeutung gewinnen, wenn ihr Anschluss an das
Lothabweichungssystem von Üentraleuropa vermittelst der österreichischen und russischen
Anschlusspunkte rechnerisch ausgenütlzt sein wird.

‘Eine Liste Berichtigungen zu dem Bericht von 1887 (Verhandlungen von Nizza) ist am Schlusse
beigegeben.
ANNEXES A, vra-vıl — A

 

 
