  

 

2

ation de la Commission permanente a 616 adıninistre comme prece-
le President l’ötat des comptes pour l’an-

Le Fonds de dot
demment. J’aurai ’honneur de remettre A M. Ei

 

nee 1888 et un apercu de la situation actuelle. ee, 1"
HELMERT.

— Ce Rapport est accompagne de la liste suivante des expedilions faites pendant Vexer-
cice 1888/89.

Resum& des expeditions de publications de l’Association geodesique faites par le j
Bureau central.

Depuis la Conförenee de Salzbourg, en 1888, jusqu’ä la fin de seplembre 1889, le
yıreau central a recu de MM. les delögues de l’Association g6odesique internationale, pour
ötre distribudes, les publications suivantes :

1. De M. le gensral Schreiber, a Berlin : Abrisse, Koordinaten und Ilöhen sämmt-
licher von ıler Landes-Aufnahme bestimmten Punkte. Ser Theil. 79 exemplaires.

9. Du meme : Nivellement der Trigonometrischen Abtheilung der Landesaufnahme.

 

 

 
 
 

Bez

Tier Band, 58 exemplaires.

3. De la Commission geodesique suisse : Le Röseau de Triangulation suisse. Vol. II.
Mensuration des bases suisses par MM. le Dr Hirsch et le colonel Dumur.

4. De Ia meme : Das Schweizerische Dreiecksnetz. Band IV. 95 exemplaires.

5. De M. le colonel de Stefanis, ä Florence : Relazione sulle esperienze istitule nel
BR. Osservatorio Astronomico di Padova in Agosto 1885 e Febbraio 1886. 100 exemplaires.

6. Du m&me : Triangolazione di 1° ordine dell’ isola Sardegna. Vol. 1. 100 exem-

 
 
 
 
 
 
  

plaires.
7. De VInstitut militaire-gdographique, ä Vienne : Mittheilungen. VIII Band.

  
   

S0 exemplaires.

8. De M. le conseiller intime, professeur Nagel A Dresde : Das Trigonometrische Netz
| Ordnung, I. Heft. 97 exemplaires.

9.De M le general marquis de Mulhacen : Memorias del Instituto Geographico y
Estadistico. Tomo VII. 67 exemplaires.

10. La Commission permanente a publie les ouvrages suivants !

 

TE

 
 
 
 
 

a) Bibliographie g6eodesique 875 exemplaires dont il a ete distribu6 en mai et Juin:
mis : z ER 4 ae B . . S
72 exemplaires aux Gouvernementis des Etats faisant partie de l’Associalion inter-

 
 

nationale,
369 exemplaires aux delegues, autorites, inslituts, savants, socieles, etc.
liste de distribution &tablie par la Commission,
300 exemplaires ont 6&t& deposes en commission & la librairie G. Reimer, ä Berlin et
134 exemplaires sont restes en depöt au Bureau central.

 

suivant la

er

 
 

   
  
