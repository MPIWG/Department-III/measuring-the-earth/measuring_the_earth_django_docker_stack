EB.

Y.— Espagne,

 

!
ENDLOATTON 3 DIRECTEURS
LATITUDE. LONGITUDE. EPOQUE, _
DES POINTS. |

|
I

INSTRUMENTS. R =
ET OBSERVATEURS. TMARQUng,

 

 

va
=

 

 

=

Triangulation du Parallele de Badajoz. (Suite.)

Aitana | 38° 38'58”| 17024237 1865 Comm, du genie Ibarreta. a (sans mar- |
gue). |
Repsold n° 1, Rep-
sold C. |
Repsold (sans mar- |
que). |

| Monduber 17 24 22 | 1867-69.

Comm. du genie lbarreta,
| |  comm. d’art. Solano.

1748 7 | 1867 id.
|

38 48 12

Triangulation du Parallöle de Madrid.

10° 29' 31”) Comm. d’art. Corcuera, | Ertel n° 1.
cap. du genie Barra-

quer.

S. Cornelio 1861

10, 82224 1861
1860
1861
1860 ;
1860 id.

‘1859 Comm, d’art. Corcuera.
1859 1
1861

. Jarmello
Guinaldo 17.0, 5
Marofa . 2.2... 10 40 52

2,1129 31

12235 38

2 85 54

132.9:.49 id.

13 . 2 54 Comm. d’art. Corcuera,-)\

cap. du genie Barra-

quer. 9
Lieut. col. d’E.-M. Ruiz
Moreno (D. M.), cap.
aE.-M. Monet.
1860 id. .
1863 Cap. du eenie Barraquer.
1863 : e id. 2
1863 j
1863 : :
1863 : a ae
1863-75 Cap. d’E.-M. Puigcerver, | Repsold n° 1, Rep- |
cap. du genie Barra- | sold C. |
quer.
Cap. du genie Barraquer. | Repsold n° 1.
Cap. du gönie Barraquer | Repsold n° 1, Rep-
et comm. d’art. Cabello. | sold A.
Repsold n°

14 24 46 | 1860

Penas Gordas....... 14
Altomira 14 50.51
Berninchez 1512
Bienvenida 15 19 20
Losares 15 1
15 48 47
Collado Bajo 15

2229

1863
1863-73

Sierra-Alta
Javalön

Palomera
ı Javalambre

1863 Cap. du genie Barraquer.
r j d.
Benartoya.:........

1864 gr
1863 id. id
1863-68 Cap. du genie Barraquer, Repsold n° 1, Rep-
cap. d’art. Solano. sold ©. |
Cap. du genie Barraquer. | Repsold n° 1.

 

 

 

Penagolosa

1864
Espadän

1864-68

Desierto...........

1864-76

Cap. du genie Barraquer,
cap. d’art. Solano.
Cap. du genie Barraquer,

 

Repsold n° 1, Rep- |

sold ©.
Repsold n° 1, Rep-

 

cap. d’art. Hernändez. sold A.

 

 

 

 

 

 

 

Triangulation de la Cöte Nord.

S. Sebastian
Tremuzo
Cedeira

AQ°A2! AR
42 49 35
43 10 10
43 516
43 33 16

90 19 32”
843 1] |
9:8
945
925

Cap. d’art. Hernändez.
id.
id.
id.
id.

FPRistor nes:
| id.
id.
id.

id.

I 21870
48
38
56

| 1870

1870
1871

1870

 
