 

61

 

 

   

 

ETALONS
DE
COMPARAISON.

Regle de 10 pieds
du Cap A.
(Standard Bar.)

 

nn

 

 

RT

EQUATION DES REGLES
DE L’APPAREIL,

COEFFICIENTS DE DILATATION.

FAITS MERITANT D’ETRE RELATES.

 

 

La somme des 5 regles
est egale ä
bo
+ 0,165616 (£- 22) ,
-+ 0,000105 (2-22)
Pourlareduction en pieds
on se sert de la relation:

1" = 3,28086933 pieds.

 

conclure que /erreur relative probable de toute mesure
executde au moyen de l’appareil de Colby peut s’exprimer
par [— 2 u +2.6 u ] (2) pour une seule mesure, y. repre-
sentant le millionieme de la longueur mesurde. De cette
formule on a conclu l’importante remarque suivante: que
les erreurs apportees par l’Etalon &tant plus grandes que
celles ä craindre de l’appareil m&me et de la möthode, il ya
bien peu d’avantage a repeter plusieurs fois la mesure d’une
meme base. .

La formule (2) est done consideree comme generale pour
toutes les bases indiennes mesurdes, comme on l’a vu, dans
des conditions d’uniformite toutes particulieres; aussi, toutes
ces bases ont-elles &t& affectees de poids egaux dans le
calcul du reseau indien.

La base de Natal se compose de 3 parties, celle de Port-
Elisabeth de 8 parties.

Cette derniere est triplde par un enchainement qui la porte &
17058,515 pieds. La regle &talon A a ete etalonnee A Bre-
teuil; Ja comparaison avec les 5 regles a eu lieu a ’Obser-
vatoire royal du Cap de Bonne-Esperance.

Les deux bases sont reliees par une chaine de 50 triangles. En
partant de la base de Natal et calculant la base de Port-
Elisabeth, le rösultat differe seulement de 1,94 pouce (4m.93)
de la mesure directe.

La distance des deux bases est de 430 milles anglais (692km.).

 

 

 
