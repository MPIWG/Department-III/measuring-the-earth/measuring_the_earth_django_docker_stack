 

 

  

 

Beh

I. — Autriche-Hongrie.

  

 

 

io

 

 

 

  

 

 

 

   

 

 

 

 

 

INDICATION ä DIRECTEURS
N° LATITUDR. |LONGITUDR.| fiPOQUE. INSTRUMENTS. REMARQUES.
DES POINTS. ET OBSERVATERURS.
76 Kremsmünster ..... SO a Ra RT 1574 Cenna, Rehm. Starke n° 3 delOp.
Observatoire.
2.  Kötzwork........:-:. ‚48 11 26 | 51 52 57 || 1871-72-74 | Sterneck, Trojan, Rehm. | Starke Un.delOp. et
s \ Starke n’ 4 delO p.
- 78 | Pöstlingberg ...... 48 19.29-|\31 55: 25 1371-72 Sterneck, Trojan. id.
79 | Pfennigberg....... 48.18 36 | 32 1 24 | 1871-72-74 | Sterneck, Rehm. Starke n® 9 delOp.
80 | Gottschalling...... 48 13:49 D 81 59 49 1871272 Ganahl, Sterneck. id.
80a| Base de Kleinmün-
Ben... 48 14 58°| 31 56 43 1871 | Ganahl. id.
Terme Quest.
80b| Base de Kieinmün-
EHER nn. 48152 | 3059 2 1871 id. Starke Un.del3p. et
Terme Est. Starke n° 4 de 1Op.
81. Haunsherg.......-: 47 54 57 | 30 39 45 1857-74 "Grüner, Rehm. Starke Un. de 13 p. |N.49, Baviere, prös.
et ne I de IVO. p.
82 | Schafberg .......... 47 46 39, 31. 5 59 1857-74 id. Starke n° 1 et 2 de ||n.9, id.
10 et 8 p.
83 | Grosswand......... 41.90.45 |\-31, 10.45 1857-850 | Langner, Rehm. Starke ne 1 et 4 de
1020
ES Ankosl. .........n: AED | Se 1881 Rehm. . id.
85 | Hochgolling ....... 4716.92 81 25 36 1880 id. id.
8& | Zirbitzkogl........ 47 352 32.34 0) 1846-80 | Hartl. Starke n° 3 delOp.
ST KZinken. 2... E20 32 DR NT 1876 id. id.
88, Bösestein. ......... 47 26 39.) 32 4 10 1876 Sterneck. Starke Univ. de8p.
Sr Hohe Trett>:...... 46 31.12 3159214 1576 id. > 1d,;
892 Kjetzen: - -... 8... 47 3411 | 3154-9 1376 id. id.
8Ib, Grimming......... 47 31 18 | 31 40 59 | 1874-76 | Horsetzky, Sterneck. an det 2 de
e
90 | Grosser Priel......| 47 43:4 | 31 43 45 1874-76 | Cenna, Sterneck. Starke n° i de 10 p.
et Univ. de8 p.
91 | Traunstein........ 47 52.27 | 31 30 21.) 1857-58-76 | Lanener, Breymann, Ster- idee
; neck.
92 | Hochbuchberg ....., 4755 4 | 31 57 41 || 1874-76 | Cenna, Sterneck. id. n 3 de 10 p. et
: : Starke Un. de 8’p.
33 | Hohe Bürgas ...... 413915332 348 1874-76 id. Starken° 4 delOp. et
Starke Un. de Sp.
4 Tusauer.-.>....... 233 2 239 93598 1876° Randhartinger. Starke n° 1 de 10 p.
95 4 WVoralpen..........: 47 44 51 | 3224 7 1873-74 | Randhartinger, Cenna. Id. n°3et4de1l0p.
96 |, Spindeleben ....... 47 55 42 | 32 21 42 || 1873-74-76 | Randhartin., Cenna, Rehm.| Id. n°4 et5 de 10 p.
| Oelscher. .- ........- 47:51 46 | 32 52.7 1876-83 | Randhartinger, Rehm. Starke n° 1 delOp.
98 | Hochsehwab ....... A730 3.924831 1876 Randhartinger. id.
99 | Gleinalpe.......... 47 13 41 | 32 42 53 1876 Hartl, Rehm. Id. nm I et 3de10p.
100° Sehäkl -. ...... .. 47.11 57 | 337 54 1876-77” | Rehm, Molnaär. Id. nO I et de 1Op.
101 | Geschriebenstein... 472113 | 34 6 0 1877.78, Hartl. Starke n° 3 de 10 p.
102 Wechsel... ..... 47 31 53 | 33 34 48 || 1875-76-83 | Gyurkovich, Hartl, Ritter. nn n° 1 = : de
10 p. et 2 de :
103 | Schneeberg........ 47 46 27 | 33 28 34 1874-83 Hartl, Rehm. Id. 1 et 3 de on
104 | Hohe Wand..:..... 47 51 5 | 33 44 6 || 1874-75-76 | Hartl, Cenna. IL a 2 et 9.desp.
et De
105 | Rosalia Kapelle....| 47 41 54 | 33 58 23 | 1874-75-78 | Horsetzky, Cenna, Hartl. | Starke 2de8p.
5 et n°3et5delOp.
105a| Base de Wiener
Neustadt......:.. 47 47 30 | 33 52 37 1875 Hartl. Starke n?° 3 de 10 p.
Terme Nord.
105b; Base de Wiener
Neustadt. _...... 47 44 291 | 33 46 38 1875 Gyurkovich. Starke n® 1 delOp.
Terme Sud.
106 | Hochstradenkogl..., 46 50 47 | 33 35 53 1877 Hartl, Molnär. Id. n° 3et5de1l0p.
107 | Gurgohegy......... 46 30 29 | 34 29 36 1877 Podstawski. Starke n° 4 de 10 p.
Bacher  . .. ....... 46 29 43 | 33, 9 12 | 1877-79-80 | Hartl. Starke n° 3 delOp.
109 | Koraipe........... 46 47. 16 | 32 88 17 1876-77 id. id.

    
