 

|

Ta pe de

a er

 

 

eh

 

 

 

13

Bezeichnen wir entsprechend den an das Nivellement anzubringenden Gorrectionen
die Richtung desselben gegen Süd’ mit +, so ergeben sich in der ganzen Schleife die meridio-

nalen Profillächen

kın?
von Nord gegen Süd = + 85,824
von Süd gegen Nord = — 74,88i

und indem wir dieselben mit dem Factor 0,8309 multiplieiren, erhalten wir als Einfluss der
sphäroidischen Correction auf den Schlussfehler der Nivellementsschleife

mm

e = + 69,65 — 69,4 = + 7,44

Es ist dies selbstverständlich nicht ganz genau die sphäroidische Gorreetion, da bei
Berechnung der meridionalen Flächen nicht die wahren Profile der Nivellementslinie, son-
dern die geradlinige Verbindung der Beobachtungsstationen als obere Begrenzung angenom-
men wurde.

Den Einfluss y der Schwerstörungen auf den Schlussfehler finden wir in analoger Weise
wie im vergangenen Jahre:

mm

y=+ 21,47 — 4,28 +0,19

also geradezu verschwindend.

Wegen des symmetrischen Verlaufes der Nivellements-Trac& in den beiden Nord-Süd
verlaufenden, daher am meisten massgebenden Theilen dieser Schleife, heben sich die Wir-
kungen sowohl der sphäroidischen Correction c als auch jener wegen der Schwerestörungen
fast vollständig auf. Beide Theile der Nivellementslinie verlaufen in tiefen Thälern und über-
schreiten nahezu gleiche Höhen von etwa 1400", den Brenner im Osten, die Reschen-Wasser-
scheide im Westen.

Der Einfluss der sphäroidischen Correction auf den Schlussfehler einer Nivellements-
schleife wäre am grössten, wenn der eine der beiden von Nord nach Süd verlaufenden Theile
des Nivellements sehr hoch, der andere sehr tief gelegen wäre; der Einfluss der Schwerestö-
rungen hingegen dann sehr gross, wenn der eine Theil in freier Gegend oder Ebene, der
andere hingegen von Bergen eingeschlossen wäre, demnach in Thälern oder durch Tunnels
verlaufen würde.

Sind auch die eben gefundenen Einflüsse auf den Schlussfehler der Nivellementsschleife
nur höchst unbedeutend, so beeinflussen sie dennoch die durch das Nivellement erhaltenen

1 Bei dieser Gelegenheit will ich eines Druckfehlers erwähnen, der in meinem Berichte für die Ver-
handlungen der Permanenten Commission in Salzburg 1888, Annex Nr. IVb, pag. 45 unten vorkommt und auf
den Herr Lallemand aufmerksam zu machen die Güte hatte. Es soll dort selbstverständlich heissen, dass die Höhe
in Innsbruck verkleinert werden muss, wenn Bozen als Ausgangspunkt des Nivellements angesehen wird.

(Siehe auch Mitth. des geogr. Inst., VII. Band, pag. 136).

 

 

 
