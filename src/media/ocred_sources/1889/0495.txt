OEIETERE 100,0, ULB

   

 

 

 

 

.)
eo)
Astron.-geod. Gebirgsanziehung

Breite. Zenit westl. Breite. Zenit westl.
" " H
Ondefiazno axtigalrel ııhy& 4.255 +>4i6,7 -E 05%
Giubihseor mamsila=t 22,0 al; 6 — 20,5 + 83,5
Tialidssa?. mi.uodl Iaka\dyf 249,7 129 Aal, 95
Mognonenns1.il. 01 22,8 — 6,9 — 97,1 — 10,3

Hiernach macht sich in dem ganzen Gebiet der vier Punkte eine gemeinsame Attrak-
tion unbekannter Massen geltend, in dem Sinne, dass ein grosser Theil der Attraktion der
nördlich gelegenen Alpen im Betrage von ca. 18” kompensirt ist. Ich muss übrigens gestehen,
dass ich mir diese neueren Ergebnisse noch nicht völlig zusammenpassen kann mit den von
mir nach Denzler in meinem Lothabweichungsbericht, p. 25 (Nizzaer Verhandlungen) ge-
oebenen Werthen.

Das königlich preussische Geodätische Institut hat neuerdings mehrere Werke publi-
eirt, in denen Lothabweichungen in einer grösseren Anzahl von Punkten Norddeutschlunds
abeeleitet sind. Von diesen Bestimmungen ist bei Aufstellung der Karte der Lothabweichun-
sen in Breite Gebrauch gemacht, welche den Verhandlungen in Salzburg beigegeben ist.

An das von mir 1887 in den Verhandlungen von Nizza aufgestellte System von Loth-
abweichungen im West- und Centraleuropa hal auf meinen Wunsch Herr Professor Börsch
die grosse russisch-skandinavische Breitengradmessung angeschlossen. (Siehe Anlage A. XI.)
Die Rechnung ist von zwei Punkten in Ostpreussen, Königsberg und Goldap aus.auf ver-
schiedenen Wegen geführt. Beide Ergebnisse stimmen auf 05 überein und zeigen, dass
bezogen auf das System der Lothabweichungen des englisch-französischen Bogens in Glarke’s
Rechnung von 1880 alle Lothabweichungen des russisch-skandinavischen Bogens um 4” zu
vergrössern sind, dergestatt, dass sie sämmtlich positiv werden.

Dieses Vorherrschen der positiven Beträge der Lothabweichung in Breite tritt übri-
sens ganz allmählich auf, wenn man von West nach Ost geht, denn für Gentraleuropa ist es
auch schon etwas merkbar. Nach meinen Rechnungen von 1887 müssen nämlich zu allen
einem Clarke’schen Ellipsoid möglichst angepassten Lothabweichungen des Bogens von
Dänemark bis Carthago 2” addirt werden, um sie auf das Clarke’sche Ellipsoid zu reduziren,
welches dem englisch-französischen bogen oskulırt.

Es ist der Versuch gemacht worden, durch Aenderung der Elemente des Rotations-
Ellipsoids das System der Lothabweichungen in dem System beider grosser Breitengrad-
messungen zu verbessern, doch ohne wesentlichen Erfolg. Ein dreiaxiges Ellipsoid würde
vielleicht sich besser anpassen lassen, doch haben dergleichen Berechnungen, da die Vollen-
dung der Europäischen Längengradmessung auf dem 52. Breitengrade vor der Thür steht,
jetzt wenig Werth. Ich bin überhaupt der Ansicht, dass es den besten Einblick in die Ano-
malien der Erdgestalt gewährt, wenn für alle Lothabweichungsuntersuchungen auf der
ganzen Erde gegenwärtig immer das Clarke’sche Ellipsoid von 1880 angewandt wird, so lange
nicht ein allen Messungen besser anschliessendes bestimmt ist.

Nach einer gefälligen Mittheilung des Directors der Sternwarte am Gap der guten

 
