 

 

 

 

DRITTE SITZUNG

Mittwoch den 9. October 1889.

Präsident Ilerr Faye.
Es sind anwesend :

Die Herren Delegirten : v. Kalmar, v. Sterneck, Dr. Tinter, Hennequin, Zachariae,
General Marquis von Mulhacen, General Derrecagaiw, Tisserand, Bouquel de la Grye, Bassot,
Defforges, Lallemand, Carusso, Rümker, Nell, General Ferrero, de Stefanis, Betocchi, Celoria,
van de Sande Bakhuyzen, van Diesen, Oudemans, Schols, Forster, Helmert, General Schrei-
ber, Morsbach, Albrecht, Hirsch, Dawidson, Anguyano, Mendizabal, Terao.

Die von der Permanenten Commission eingeladenen Ilerren: Stas, Perrotin.

Die von der französischen Commission eingeladenen Herren : Eginitis, Gogou,
Cruls, Beuf, Wada, General Billot, d’ Abbadie, Cornu, Daubree, Mascart, de Saint Aroman,
Benotl, Caspari, Germain, Halt, Trepied, Teisserenc de Bort, Callandreau, Charmes, Brul-
lard, Bourgeois.

Die Sitzung wird um 2 Uhr 10 Minuten eröffnet.

Der Herr Präsident theilt der Versammlung mit, dass das Protokoll der zweiten
Sitzung zugleich mit dem der dritten Sitzung wird verlesen werden. Er giebt hierauf dem
Ilerrn General Marquis von Mulhacen- das Wort und bittet ihn, den Bericht über den Stand
der zur Bestimmung der mittleren Meereshöhen gemachten Arbeiten zu verlesen.

Dieser Bericht, der unter den Beilagen der Verhandlungen der Conferenz vollständig
erscheinen wird (siehe Beilage A. II), constatirt unter Anderm dass die Zahl der an den
Küsten des continentalen Europas angebrachten Mareographen gegenwärtig auf 94 gestiegen
ist, während der diesen Gegenstand behandelnde Bericht von 1883 nur 58 aufwiess.

Obgleich die Zahl der gegenwärtigen Mareographen eine recht beträchtliche ist, so
dass dieselben, mit den ebenfalls sich ausdehnenden Präeisionsnivellements verbunden, genü-
gen würden, um die Unterschiede der Meeresniveaus, sowie ihre Unveränderlichkeit festzu-
stellen, so hat die Permanente Commission doch den Zeitpunkt noch nicht als günstig erachtet,
um die Frage der Fundamentalebene für die Höhen zu entscheiden. Darum macht der
Berichterstatter im Namen der Permanenten Commission folgenden Vertagungsvorschlag :

« Die Wahl des für ganz Europa gemeinsamen Hlöhennullpunktes wird auf die nächste
PROCRS-VERBAUX — 13
