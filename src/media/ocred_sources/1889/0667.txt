 

 

 

Bun

N N

 

 

 

 

Annexe B. XXIX.

SUISSE.

Gomme les triangulations de premier ordre, la mensuration des bases, ainsi que les
nivellemients de pr&cision sont terminös en Suisse depuis quelques anndes, il reste surtout ä
faire des recherches sur les döviations de la verticale, des mesures de la pesanteur au moyen
du pendule ä reversion, et le rattachement des prinecipales lignes de chemin de fer et des
votes fluviales au reseau du nivellement de precision.

Pour les observations astronomiques et geodesiques proprement dites, on ulilise
un grand instrument universel de Repsold, commnad& il y a deux ans, et dont les conslantes
ont ete determinees.

L’azimut de ’Observatoire de Neuchätel, mesur& A Chaumont avec cet instrument,

a donne un resultal peu satisfaisant, savoir: A, — 180° 8,32 + 0.90, ce qui differe de
Pazimut reciproque que j’ai communique l’annde derniere (A. 646 + 016) de

presque 2”, tandis que les incertitudes des deux valeurs justifient A peine 1” de difference,
et bien que les deux stations soient situ&es presque exactement sur le m&me meridien et que
leur distance depasse ä peine 2 kilometres et demi. S’agirait-il lA d’un phenomene connexe
avec le mouvement azimutal periodique, que j’ai reconnu depuis 28 ans ä la colline sur
laquelle est situ& notre Observatoire ? Cette question fera l’objet de recherches ulterieures.

Pour les hauteurs polaires, les rösultats sont sensiblement plus salisfaisants; ainsiä.
Ghaumont, on a trouve:

 

Parslesalistaneds»zenithales: 1,4 Hund enıaleesee ige 931° 20/96
Par les-passages au ide verlical. 0... 402 091474 19,42 &0,48
Moyenne . gr Oi le Del

Tandı qua l’Öbseryatomwe on a . ,-. .. 2.0.5.6 59 50,61, = 0 1

A = BONS

 

Or, comme les deux stations, siluees A peu pres dans le ıname meridien, sont £loi-
gn6es de 2508”, le calcul geodesique donne pour la difförence de latitude N = 12; par
consequent la difference entre les deux valeurs, astronomique el g6odesique, savoir 2,45
represente la deviation de la verticale A Neuchätel dans le sens indiqu& par l’attraction du
massif des Alpes. Gette deviation, bien plus faible qu’on ne l’aurait cru a priori, est @videm-
ment le r&sultat de la difference des altractions exerc6es par les masses des Alpes et «du Jura.

La Commission suisse a reconnu qu’ily aurait un int6röt particulier ä ötudier de plus

 

 
