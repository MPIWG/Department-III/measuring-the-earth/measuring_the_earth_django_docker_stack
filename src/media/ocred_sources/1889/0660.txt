 

 

La difförence de longitude Varsovie-Kiew, indiquee ci-dessus, est la moyenne des
deux determinalions ex@cutces d’abord par MM. Rylke et Pomerantzew, ct ensuite par
MM. Khandrikow et Fabricius.

Le proced6 telegraphique est aussi appliqud A la determination des differences de
longitude dans la Russie d’Asie; dans les anndes que comprend ce rapporl, on ya efleetue
les döterminations suivantes :!

Turkestan ä l’Ouest de Tasch- , ; MM. Pomevantzew et Salesski,
ken... 2023. 04. 410 \ 18806.

Tukalinsk & !’Ouest d’Omsk. — 0 4 48,94 = 0,02 , MM. Miroschnitschenka et Schmidt,

Thara,a V’ Est d’Omsk. . . + 21 23,02 72210,0% 1888.

Il est a remarquer que les longitudes de Taschkent et d’Oinsk ont El döterminces A
l’aide du telegraphe par MM. Scharnhorst et Koulberg, en 1875-1876 (Sapiski de la Section
Topographique, vol. XXXVI).

b) Latriudes.

Kowel, par MM. Polanowski et Miontchinski, 1888,

Theodosia (pilier Bertrand), par M. Koulberg, 1888.

lin 1888, M. Koulberg a aussi observe en Grimmde les latitudes de six points, disposös
Lois A trois au Sud et au Nord des montagnes Tauriques. Ges delerminations sont deslindes
a Petude de la deviation de la verticale.

c) Azimuls.

La mesure de l’azımut a te elfectuee par M. le Colonel Koulberg, A Theodosia
(pilier Bertran) pour l’orientation de la triangulation de la Crimce.

9. OPERATIONS GEODESIQUES

a) Triangulations de premier ordre.

La triangulation du Gouvernement de Saint-Petersbourg, commencie en 1887, a dte
continuce en 1888. Geite triangulation s’appuie sur deux bases : celle de Poulkowa, mesur&e
a plusieurs reprises avec l’appareil de Struve dans les anndes precedentes, et celle de Molos-
kowitzy, mesurde en 1888 avec l’appareil de Jädärin, construit & Stockholm.

Dans le courant de ’annee passee (1888), nous avons proced& A la triangulation de
la Urimee. La base, pres de la ville de Theodosia, est mesur6e deux foisä P’aide de ’appareil
a languette, qui avait servi jadis pour la mesure des bases au Caucase. La preeision de cette
mensuralion est Ir&s salisfaisanle, Ä en juger par l’accord des resultats :

 

 

 
