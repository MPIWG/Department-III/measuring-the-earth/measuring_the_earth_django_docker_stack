 

 

 

 

 

 

 

 

Annexe A. X.

REFLEXIONS

au sujet de la eonstitution de la Terre supposde fluide dans son interieur,
et remarques sur les diverses methodes
employtes pour determiner Yaplatissement superficiel,

PAR

M. FE. TISSERAND

1. — Clairaut suppose la Terre form&e de couches d’egale densit, separees par
des ellipsoides de r&volution, la densit@ augmenlant constamment de la surface au centre
La loi des densites est inconnue;, elle doit salisfaire seulement a un certain nombre de condi-
tions, Soit «a le demi-petit axe d’une couche queleonque, 1 la valeur de a pour la surface,
la densit& A la surface, A la densil& moyenne. On doit avoır

©
\

1

de
da

U,

oT
o, &tant egal A 5A, ou un peu plus pelit. Il faut en outre que la valeur de l’aplatissement
: 2

superficiel eoincide avec celle qui est fournie par la e&odesie ou par le pendule, ou par les
observations de la Lune. Enfin, la ih&orie du mouvement de rotation de la Terre donne la

condition

eu ja
Wr el - — ,00397%
Er

 

 

(Voir le Wömoire de M. Serret, Annales de l’Observatoire de Paris, t. V, p. 324.)
La loi des densitös considörde par Laplace et Legendre,

sin na
Daarkunnıeg 3

na
ANNEXEA u 4

 
