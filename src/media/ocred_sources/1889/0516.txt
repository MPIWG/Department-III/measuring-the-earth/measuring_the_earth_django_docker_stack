 

2

 

Si dg est la variation apparente de la pesanteur produite par la perte de poids :

2 ge h—M' S
+! dg Mh—M =

an

La longueur du pendule synchrone devient de ce fait,

k? MS

Cette correction fut faite pour la premiere fois par Bouguer, apres lui par Borda,
Biot et Kater. Bessel d&montra qu’elle est insuffisante. Les experiences de Du Buat avaient
mis hors de doute, des 1786, que, lorsqu’un corps se meut dans un fluide, il entraine avec
lui une partie du fluide qui adhere a sa surface et communique aux couches environnantes
les vitesses variables, qui vont en s’eleignant a mesure qu’on s’eloigne du corps en Mouve-
ment. @est ainsi qu’un navire soul&ve et entraine une sorle de proue liquide qui s’ecoule le
long de ses flancs avec üne vilesse relative de signe contraire, mais införieure & la vitesse
propre du navire. (Vest ainsi qu’il se forme & l’arriere du navire un remou et un sillage
dans lesquels les moleeules liquides se melent avec des vitesses tres variables et des frotte-
ments intörieurs relativement consid&rables. |

Les mömes phenomenes se produisenl pendant le mouvement d’un pendule dans
air. Une certaine masse m’ du fluide ambiant est animee de vitesses variables aux differents
points de la masse, vitesses que nous pouVvons toujours supposer proportionnelles & la vitesse
du pendule, d’ailleurs tr&s faible. La force vive du systeme se trouve des lors acerue d’une

quantile !
en
J dm en. ==. la? (ir)

ot le eoeffieient e est difförent pour chaque mol6cule. ‚L’&qualion des forces vives devienl
des lors :

 

I £
: 2 m. [a
Constante=M (h?+k? + — (2 - \?.— 24Mh cos h
M al
La longueur du pendule synehrone est alors :

12 m’ C?

BR a ml?
Be 4 Am) ( Hu)

IN®

’ A . . Re; m ir
L’effet est le m&me que si ’on augmentait de la quanlil& —

A

la constante K? du moment

 

M

(’inertie, et que Bessel exprimait en disant que le pendule se meut dans Pair comme il se

 

 

 

 
