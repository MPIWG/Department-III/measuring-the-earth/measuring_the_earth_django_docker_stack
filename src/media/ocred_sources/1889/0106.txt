 

 

 

 

Herr Hennequin führt ausserdem neue astronomische Breiten- und Azimutbeobach-
tungen an, welche im Allgemeinen recht schöne Resultate geliefert haben und nur ım Sinne
der Azimute etwas bedeutende Abweichungen aufweisen.

Die Präcisionsnivellirungen haben in Belgien im Jahre 1887 zwischen den Schleusen
von Heyst, wo sich ein holländischer Fixpunkt befindet, und Ostende, wo man einen Funda-
mental-Pixpunkt erstellt hat, begonnen. Im folgenden Jahr wurde Ostende, in der Nähe von
Ghyvelde, mit der französischen Grenze verbunden; diese Operationen sind mit einem Kilo-
ineterfehler von 0""'8 — 0""9 ausgeführt worden. Ein besonders bewilligter Kredit wird es

möglich machen, diese Arbeiten längs den Grenzen im Jahre 1891 zu vollenden. (Siehe
Beilage B. XIV.)

Nachdem der Herr Präsident lerrn Hennequin gedankt, bittet er den Herrn Oberst
Zachariae seinen Bericht zu verlesen.

Dieser theilt mit, dass man in Dänemark die Präeisionsnivellements fortgesetzt hat.
Die Methoden sind die nämlichen wie die deren Beschreibung sich in den Gonferenzver-
handlungen von Nizza und Salzburg findet.

Seit Anfang dieses Jahres sind beinahe alle Fixpunkte, sogar die zweiter Ordnung,
unterirdisch, wie dies in den früheren Mittheilungen angegeben wurde, festgelegt worden.
Man hat die rechtwinkligen Latten durch solche mit dreieckigem Schnitt ersetzt, welche sta-
biler sind und sich weniger leicht verwerfen. (Siehe Beilage B. XV.)

Hierauf trägt der Herr General Marquis von Mulhacen einen kurzen Bericht über die
Erdmessungsarbeiten erster Ordnung des geographischen und statistischen Instituts von Spa-
nien’vor. Es folgt aus demselben, dass die Ausgleichung der mit V: Cäceres bezeichneten

Gruppe des grossen spanischen Netzes, mit S0 Bedingungsgleichungen, wovon 91 sich auf

die Winkel und 29 auf die Seiten beziehen, als wahrscheinlichen Fehler einer nicht ausge-
elichenen Richtung 0.35 ergiebt.

Die Präeisionsnivellements werden in Spanien lebhaft gefördert. So hal man seit dem
Jahre 1886 zuerst die Linie von Puerto-Lapiche bis Cordoba mit 309 Kilometer und 98 Bron-
zereperen, hierauf die von Baylen bis Malaga mit 265 Kilometer und 57 Fixpunkten, und
endlich die von Cuesta de l’Espino bis Malaga mit 174 Kilometer und 33 Fixpunkten ausge-
führt, was im Ganzen, seit 1886, 1948 nivellirte Kilometer mit 272 Fixpunkten ausmacht,
wodurch das spanische Präcisionsnivellement im Ganzen die Länge von 10 809 Kilometer
doppelt nivellirter Linien mit 2509 Bronzerepören erreicht.

In Bezug auf die astronomischen Arbeiten, vertheilt Herr von Mulhacen zunächst
Exemplare der soeben in der Nationalbuchdruckerei von Paris erschienenen Schrift über die
Längendifferenz zwischen Paris und Madrid. Dieselbe beträgt 24W5’998 + 0,009.

Endlich überreicht Herr von Mulhacen der Conferenz den Band VII der Memoiren
des geographischen und statistischen Institutes von Spanien, welcher unter andern interes-
santen Arbeiten die vom Herrn Oberst Barraquer ausgeführte Bestimmung der Schwere in
Madrid enthält (Siehe Beilage B. XV1.)

 

 

 

}
}
|
|
&
;

 

 

 
