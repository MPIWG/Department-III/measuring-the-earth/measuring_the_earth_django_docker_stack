ae

XIII. — Prusse, B.

 

 

INDICATION
DES POINTS.

Esens
Aurich

Pilsum

| Borkum
Norderney. .......-
Finsterwolde
Uithuizermeden....
Hornhuizen

Basis (südlich)
Basis (nördlich). ....
Rufach ..:
Sulz
| Sausheim
Belchen
Glaserberg ....
Ballon
Kaiserstuhl. .......
BrEESS0E 2.2...
Strassburg (Ihurm).

Wintersberg
Kelschberg

Delme 2... .-
Kewelsberg
Bevingen

 

 

 

LATITUDE.

| 59038, 07.

53 28 Il
53 13 49
53 29 4
53 35 22
53 42 39
55.11 55
53 24 35
53 23.22

41°54 3°
47 57 46
47 57 50
4 93 18
47 47 30
49 24
27 9
54 7
74
11 26
34 58
30 49
58 47
10.19
54 47
28 4
22 47

LONGITUDE.

 

|
|

 

EPOQUE.

Niederländischer Anschluss.

25°16' 47”
25 848
25 502
24 45 4%

94 a

94 53 49
2446 8,
94 99 37

oo
|

Elsass-Lothringische Dreieckskette.

2398, 3
25 4.6
24.56 44
24 53 13
23... 18
25 30 2
24.59. 8
24 45 57
25 21 23
24 49 3
AG)
24 49 56
25.16.55
24 35.29
24. 142
24 10 3
23 44:57

 

 

©)

 

DIRECTEURS

ET OBSERVATEURS.

INSTRUMENTS,

 

Schreiber.

Schreiber:

 

 

Noch nicht publi-

|| eirt.

Noch nicht puhli-

eirt.

Geodätisches m-

stitut N. 39.

* 1 B ? g £ AR Da 3 ni S . TR) . Y. SI s ; aan
(*) Die Beobachtungen auf diesen drei Punkten sind im Sommer 1885 von der Königlich Niederländischen Geodätischen
Kommission im Anschluss an die diesseitigen Messungen ausgeführt worden.

 
