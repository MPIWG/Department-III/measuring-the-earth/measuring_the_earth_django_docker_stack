st

XII. — Prusse, B.

 

Instrumente. — 1. 15 zölliger Theodolit von Ertel.
Das Instrument ist im Jahre 1832 von Ertel in München gebaut. Es wurde zunächst mit
4 Nonien abgelesen ; Ansaho: 9 Sekunden. Im Winter 18 3/4 wurden die „Nonien durch
Mikroskop-Mikrometer ersetzt; Angabe: Yıo Sekunde. Theilungsdurchmesser — 3. Ver-
erösserung des en Brennweite des Objektivs = 514””. Oeffnung des Objek-
tivs — 47em, Der Kreis wurde im Winter 18 36/37 von Pistor, im Winter 18%. von Pistor und

Martins und 1876 von Repsold neu getheilt. 2
zölliger Repetitionstheodolit von Pistor und Schiek.

Das Instrument giebt durch 4 Nonien unmittelbar 5 Sekunden an.

zölliges Universal-Instrument von Pistor und Martins.

Das Instrument ist im Jahre 1851 von Pistor und Martins in Berlin gebaut. Ablesung
durch Mikroskope; Angabe: 1 Sekunde. Oefinung des Objektivs = 47",

10 zölliges Universal-Instrument N° I von Pistor und Martins.

Das Instrument ist im Jahre 1865 von Pistor und Martins in Berlin gebaut. Ablesung
durch Mikroskope ; Angabe: Yo Sekunde. Theilungsdurchmesser — 265””. Vergrösserung des
Fernrohrs = 40. Brennweite des Objektivs = 506””. Oeffnung des Objektivs = 48””.

. 10 zölliges Universal-Instrument N° II von Pistor und Martins.

Das Instrument ist im Jahre 1868 von Pistor und Martins in Berlin gebaut, und ist dem
vorhergehenden Instrument gleich.

8 zölliges Universal-Instrument von Pistor und Martins.

Das Instrument ist in seiner Bauart den vorstehenden 10 zölligen Universal-Instrumenten
oleich, nur die Dimensionen sind entsprechend kleiner. Ablesung durch un. Angabe:
1 Sekunde.

10 zölliger Theodolit N° I von Pistor und Martins.

Das Instrument ist im Jahre 1872 von Pistor und Martins in Berlin a Der Kreis ist
im Winter 18 72/73 von Repsold neu getheilt. Ablesung durch Mikroskope ; Angabe: Y/ıo Sekunde.
Theilungsdurchmesser — 965mm, Vergrösserung des Fernrohrs — 32. Brennweite des Objek-
tivs = 500”®, Oeffnung des Objektivs =

10 zölliger Theodolit N° II von Pistor und Martins.

Das Instrument ist mit dem vorhergehenden zugleich gebaut und ist ihm völlig gleich. Der
Kreis ist im Winter 18 2/73 von Repsold neu getheilt.

Publikationen. — ı Gradmessung in Ostpreussen und ihre Verbindung mit Preus-

sischen und Russischen Dreiecksketten. Ausgeführt von F. W. Bessel und Baeyer.
Berlin 1838

Die Küstenvermessung und ihre Verbindung mit der Berliner Grundlinie. Ausgeführt
von der Trigonometrischen Abtheilung des Generalstabes. Herausgegeben von J. J.
Baeyer. Berlin 1849.

 
