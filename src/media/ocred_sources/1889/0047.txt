 

x
0
‘
v
Y

Dame

 

nee

Depuis 1884, les discordances par niveldes sont groupees, pour chaque section, d’apres
leur nombre et leur grandeur en un diagramme que l’on compare ensuite au diagramme
thöorique donn& par la loi des probabilites, et röpondant & la meme erreur accidentelle
moyenne. La concordance a toujours ete remarquable.

On obtient, d’antre part, la portion systematique — toujours tres faible — de ces
discordances, en annulant celles-ci progressivement depuis Forigine de la section el en pre-
nant le coefficient moyen d’inclinaison sur l’axe des abscisses, de la courbe reprösentalive de
ces discordances cumul£es.

Un album donnant les resultats de ces deux genres de recherches est actuellement
en cours d’impression el sera prösente A l’Association dans "une de ses prochaines r&unions.

M. Ferrero est heureux de voir qu’on s’occupe de la question des erreurs; mais A
mesure que les donndes augmentent, il devient plus nöcessaire qu’on les Etudie dans chaque
pays m&me ol elles sont recueillies, et qu’on lui fasse connaitre seulement les resultats.

M. le President, avant de passer A Vordre du jour, communique des excuses qu'ila
recues de plusieurs invites, qui n’ont pas pu prendre part aux secances, tels que M. le Minis-
tre de Belgique, le prince de Monaco, M. Goulier et M. Tcheng Ki Tong.

Il donne ensuite la parole A M. v. Sterneck pour complöter son rapport, lu dans la
derniere seance, par une nole sur les d&terminations de la pesanteur, qu’il a execuldes
en 1889. (Voir Annexe B. XXTIlI°.)

M. Helmert se fölicite des nombreuses experiences qui ont et faites en Autriche
et en France sur les variations de la pesanteur.

M. le President donne ensuite la parole A M. Carusso, qui lit le rapport de la
Gre&ce, (Voir Annexe B. XIX.)

M. Rümker donne quelques informations sur le nivellement du terrain de la ville de
Hambourg, et M. Nell parle egalement des nivellements ex&cules dans la Hesse.

(Voir Rapport de. la Hesse, Annexe B. XX.)

M. le General Ferrero lit le rapport sur les travaux geodesiques Italiens. La Lriangu-
lation, qui s’est acerue pendant la derniere annde de 14 stations de premier ordre, sera com-
pletement terminde dans deux ou troisans; le reseau contientsept bases mesurdes, auxquelles
il faudra en ajouter encore une dans les maremmes de Toscane, dans le voisinage de Grosseto.
Les caleuls de compensation sont poussös vigoureusement; on les a divises en 18 groupes
de 45 A 50 @quations de condition chacun, dont 11 sont deja termines.

Les nivellements de precision progressent egalement d’une maniere continue; le rat-
tachement A la frontiere frangaise a &16 ex&cute surquatre points. Sur ’un d’entre eux, ä Vinti-
mille, les cotes absolues provisoires, dont les francaises sont basdces sur Marseille et les
ilaliennes sur Genes, se sont rencontr6es avec une difförence seulement de cing centimelres.

Quant aux mardographes, 12 sont en fonction et leurs courbes en voie de depouille-
ment; avant la fin de l’annde, il y en aura un nouveau d’installe a Ancona el probablement un

autre A Porto-Mauricio.
PROCES-VERBAUX — 6

 
