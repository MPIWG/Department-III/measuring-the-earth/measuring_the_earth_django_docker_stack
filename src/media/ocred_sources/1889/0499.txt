ne nn

nn

ea en

ae seien when

see

 

 

VER

3

Si l’on introduit pour Berne les d&vialions de la verticale qui correspondent A un
ellipsoide de Clarke de 1880, s’adaptant le plus possible & ’Europe oceidentale et centrale
(voir mon Rapport de 1887 sur les deviations de la verticale, Gomptes Rendus de Nice), et
qu’on altribue en consequence A Rauenberg (Berlin) une deviation de + o” en latitude et de
45 en longitude Est, et ä Berne + 4.0 en latitude et + 92 en longitude Est, ou bien une
deviation zenithale vers l’Ouest de — 1 8 on peut en conclure : !

een Aa l’Ouest. El SE DER Old
Gadenaz2o.. rg wg a ap
Giubiasco ee) + 76 ee) + 39
INDIEN BDARLABR BIS Ar u) 20 275
Mognone. ; ZIWINR 2 6) a 2 HD

D’apres ce qui pr&cede, il se produit dans toute la region des qualre points ci-dessus
une altraclion commune de masses inconnues, dans ce sens qu’une grande partie de Pattrac-
tion des Alpes, situges au Nord, et s’elevant & 18” environ, se lrouve compensee. Toulefois,
ces nouveaux resultats s’&cartent notablement de ceux de Denzler, (voir les Comptes Rendus
de Nice, Rapport sur les d6viations de la verticale, p. 25).

L’Institut royal g6odösique de Prusse a recemment public plusieurs ouvrages dans
lesquels on a dötermin& des deviations de la verlicale pour un assez grand nombre de peinis
de l’Allemagne du Nord. On a utilis6 ces döterminations en dressant la carte des deviations
de la verticale en latitude, jointe aux Comptes Rendus de Salzbourg.

Sur mon desir, M. le Professeur Boersch a raltach6 la grande mesure russo-scandinave
des degr6s de lalitude au systeme des deviations de la verticale dans l’ Europe occidentale el
cenlrale, que j’ai expose en 1887 dans la session de Nice '. Le caleul a &t& entrepris dans
differentes directions ä parlir de deux points situes dans la Prusse erientale, Koenigsberg et
Goldap. Les deux resultats s’accordent & 0'5 prös et montrent que, rapport&s au systeme des
döviations de l’arc anglo-francais, dans les calculs de Clarke de 1880, toutes les deviations de
Parc russo-scandinave doivent &tre augmentees de 4”, de telle sorte qu’elles deviennent loutes
positives.

Du reste, cette pr&dominance des valeurs positives des d&vialions en latitude se pre-
sente peu ä peu en allant de l’Ouest ä !’Est, car pour /’Europe centrale elle se fait deja un
peu remarquer. (est que, d’apres mes caleuls de 1887, il faut, sur Parc qui s’ötend du Dane-
mark A Carthage, ajouter 9” A toutes les d6viations adaptees le plus possible a un ellipsoide
de Clarke, afın de pouvoir les reduire & V’ellipsoide de Clarke qui est osculant ä l’arc anglo-
francais.

On a essay& d’am&liorer, par le changement des el&ments de l’ellipsoide de rolation,

! On a corrige les fautes d’impression du Rapport annuel suisse, en partie d’apres « Das Schweize-
rische Dreiecksnetz, 1889. IV. 211.
ı Voir Annexe A, XI.

 
