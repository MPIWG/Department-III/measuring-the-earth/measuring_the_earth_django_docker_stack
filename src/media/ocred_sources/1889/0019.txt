 

 

 

 

13

nesse, gest interesse aussi A la geodösie el qui, jouissant A juste titre d’une grande inluence
sur les hommes de seience et m&me sur les hommes politiques dans son pays, s’est elforce de
faire adherer les Rtats-Unis ä notre Association. Cette adhösion s’est accomplie le 91 fevrier
1889, date A laquelle le Senat a accepte la proposition que le Gouvernement lui avail faite
dans ce but.

« Nous avons aujourd’hui le plaisir de voir parmi nous, pour la premiere fois, |e
savant astronome que jai nomme tout A Vheure el le reprösenlant officiel des Ktals-
Unis, M le professeur Georges Davidson, du U. S. Coast and Geodetie Survey, qui a bien
voulu passer l’Oe&an pour venir prendre part A nos scances.

« Dans le meme continent, je puis mentionner encore la Republique Argentine qui,
il ya peu de lemps, s’est associee A nous. Nous avons recu, A ce sujet, une leltre qui nous
est venue de Berlin.

« Vous vous rappelez, Messieurs, que le Gouvernement prussien a eu la bont& de vou-
loir bien se servir, au debut de la r&organisation de notre Association, des moyens diplomatiques
pour Pappuyer, el qu’ä sa demande tous les autres Gouvernements faisanl partie de P’Associa-
tion ont eonsenti A correspondre, sur les affaires de l’Association, par ’intermödiaire de
leurs reprösentants diplomatiques ä Berlin, avec la Commission permanente et son Bureau.
La Röpublique Argentine a r&pondu qu’elle ‚avait deeide d’adherer A l’Association geodesique,
et que le Prösident de la Röpublique a designe M. Agustin Gonzalez, ingenieur, chef du
Bureau argentin des renseignements a Londres, pour la representer. J'ignore si nous aurons
le plaisir de compter M. Gonzalez parmı nous.

« Ensuite, Messieurs, A l’autre exirdmitö du monde, le grand empire progressiste
du Japon est entre ögalement dans l’Association g&odösique internationale, el nous saluons
parmi nous, comme son delegue, M. Terao, professeur A l’Universit© imperiale, el
direeeteur de l’Observatoire astronomique de Tokio.

Nous avons de meme le plaisir de comprendre parmi nos nouveaux adherents
la Grece, P’heritiere de l’ancien centre le plus illustre et le plus lumineux de notre eivili-
sation. La nouvelle Gröce, Messieurs, s’est converlie A la G&oddsie et a envoye M. Garusso
comme son delegue A notre Conference. Ge fait est d’autant plus rejouissant que dest
l’ancienne Grece qui a 6tabli les premieres bases scientifiques de la Geodösie.

« Je vous conduis un peu vite sur la surface Lerresire , car maintenant jarrive au
3rösil. Quelques-uns d’entre vous, Messieurs, se souviennent que, deja a la reunion que
nous avions, il ya deux ans, au magnifique Observatoire de Nice, Sa Majest& Don Pedro nous
avait fait esperer que ce grand empire s’associerait egalement aux etudes geodesiques que
nous poursuivons. J’ai eu P’honneur de recevoir des lors plusieurs lettres de M. le baron
de Teffe, dont il resulte qu’il a fait tous ses efforts pour avancer l’accession de son pays, el
que $S. M. Pempereur du Brösil est Ires dispose a entrer dans notre Association.

« Nous pouvons done affırmer avec Monsieur le Ministre des Affaires Etrangeres el
avec notre President, M. Fave, que l’Association göodesique s’etend sur presque toutes les
parties du globe terrestre.

« A propos de la r&union de la Gonference, notre Bureau a regu un certain nombre

 
