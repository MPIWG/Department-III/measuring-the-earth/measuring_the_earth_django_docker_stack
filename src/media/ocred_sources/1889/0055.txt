 
  

DEE BITTEREERTER ER

 

 

49
pendant laquelle la Conference generale est r&unie. A V’egard de la cooptalion, M. Defiorges
retient argument deja &mis ne vis-A-vis d’un ar fait par la Commission permanente,
la Conference ne serait plus libre dans sa ratificalion. Done, si l!’on ne veutspas admettre
que les vacances restent sans elre comblees jusqu’a la r&union suivante de la Gonference,
ıl Ba mon que l’election se fit par correspondance et par Ktat.

. Foerster rappelle que l’experience du passe a monir& que sı le nombre des Mem-
bres de . mission permanente n’est pas assez &leve, il devient tres difficile de reunir le
quorum. Il ne saurait pas non plus comprendre l’utilit@ de consigner dans le reglement la
souverainet& de la Conference generale. Bien que les delögues qui forment la Conference de-
pendent ä certains &gards des Gouvernements qui les ont designes, personne ne songe A
contester la libert& scientifique de la Gonlerence.

M. le General Ferrero est d’accord avec M. Feerster qu'il est superflu de faire pro-
clamer par un reglement la souverainete d’une Conference. Il s’oppose & la solution des elec-
tions par correspondance, parce quil est diffieile de s’entendre entre Washington, Madrid
et Berlin, par exemple, sur des choix ä faire. En tout cas, puisque le droit de cooplation par
la Commission se trouve mis en question, il faut que la Conference prenne une decision for-
melle, _ -

Oudemans, dans l’alternative posee par M. Defforges, prefere que les Eleclions
aient toujours lieu dans les röunions des Conferences gön6rales ; il reconnait P’inconvenient
des choix par correspondance.

M. Bakhuyzen ne croit pas & Vefficacite du droit de ralification reserv& A la Confe-
rence, puisque dans un cas de ce genre la Conference a te tellement d’accord qu’elle n’a
m&me pas expressöment ratifi& le choix de la Commission. Il appuie ensuite vivement la mo-
tion de M. Defforges ä propos de lV’article premier. Il la formule dans ces termes : « La di-
rection scientifique appartient, en dehors des r&unions gen6rales, A la Commission perma-
nente. »

M. Hirsch, qui n’attache pas grande importance aux questions de reglement dans
les organisations scientifiques dont le sucees depend surtout du devouement et du travail des
savants qui en font partie, ne veut pas prolonger outre mesure cette discussion. Toutefois,
au sujet de la cooptation, il tient A ajouter a argument tire par M. Forster du. passe de
l’Association g6odesique, ’exemple d’une organisation scientifique internationale tout & fait
analogue, celle du Comite des Poids et Mesures, qui possede ce droit de cooptalion de epuis
quatorze ans et en a use A la satisfaction generale et pour le plus grand bien de la marche
des affaires. La r&alit& du droit de ratification va @tre dömontree tout A V’'heure ; car dans
cette s6ance m&me, la Conference sera appelde A voter, au scrutin secret, sur le choix du
successeur de feu M. v. Oppolzer. Le secret du vote garantit &videmment la liberte d’une
facon absolue. M. Hirsch croit la discussion Epuisee.

M. le President met aux voix d’abord l’amendement de M. Bakhuyzen, ayant pour

objet d’intercaler dans l’artiele 1er les mots «en dehors des reunions generales. »
PROGES-VERBAUX — 7

 
