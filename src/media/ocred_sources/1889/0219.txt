WAngle entre la polaire et le

 

51

AZIMUTS

 

 

Methode et instrument employe.

Annee et mois.

TITRE DE LA PUBLICATION.

REMARUUES.

 

d’un instrument universel
de Pistor et Martins.

[ signal, mesur&e au moyen

mire meridienne dont Y’a-
zimut a ete determine par
un instrument de passage
de Cook.

Ki: entre le sienal et la

N
E
r
{

t

 

 

   

naar @quations personnelles.
—_ 41
m janvier. | Determination
Bl directe.
pie Kir
„, at!
1eBtt |
1876 juillet. »
ab f

de I |

1876 aoüt.

geodes.

tembre 1886.

 

LONGITUDES

 

Le r6sultat est publie en
resume dans le proces-
verbal de la commission

italienne, r&unie

a Milan du 27 au 28 sep-

L’azimut est compte du
Nord vers l’Est.

 

 

 

 

F. Elimination des
innee et mois.

TITRE DE LA PUBLICATION.

REMARQUES.

 

 

 

MY
al y}

 

 

Fergola e Secchi. Sulla diffe-
renza di longitudine fra Napoli
e Roma, ete. Napoli 1871 (Ac-
tes de l’Acad&mie des sciences
de Naples. Tome V).

Pas encore publiee.

 

Le centre de l’Observatoire de
Naples (Capodimonte) est de
0'033 A VEst du cercle märi-
dien.

Les resultats se trouvent indi-
ques dans le proces-verbal de
la commission geodesique ita-
lienne, reunie & Milan du 27

au 28 Septembre 1886.

 

 

 

 
