 

een

 

« Berlin, den 3. April 1889.

« Hochgeehrter Herr Gollege !

« Der französische Botschafter in Berlin hat im Auftrage seiner Regierung bei der

nreussischen Regierung angefragt, ob thatsächlich, wie dieselbe vernommen, einzelne Staaten
lei der internationalen Erdmessung ständige Bevollmächtigte ernannt hätten und welches
der Vorgang bei der Akkreditirung derselben (insbesondere für Oesterreich-Ungarn, Russ-
land unıl Italien), gewesen sei. Der Passus des betreffenden Schreibens lautet :

« Seither (seit dem Beitritt Frankreichs) haben französische Gelehrte als zeitweilige
Delegirte unserer betreffenden Ministerien an den verschiedenen Gradmessungs-Versamm-
lungen theilgenommen.

« Es scheint dass heine ihrer Collegen im Gegentheil als ständige Vertreter ihres
Landes mit dem Titel von Bevollmächtigten ernannt worden sind.

« Ich würde Ihrer Excellenz zu Dank verpflichtet sein, wenn dieselbe mich wissen
lassen wollte, ob diese Angabe begründet ist, und welche Formalitäten, namentlich von
Seiten Russlands, Italiens und Oesterreich-Ungarns angewandt sind, um ihre Gommissare
bei der internationalen Erdmessung zu akkrediliren.

« Hierauf (fährt Herr Helmert fort), kann ich nach unseren Akten nichts erwiedern,
was Russland und Italien anbetrifft. Jedoch ist in Bezug auf Oesterreich-Ungarn, unter dem
10. Januar 1888 mir von meinem vorgesetzten Herrn Minister die Mittheilung zugegangen,
dass die österreichisch-ungarische Botschaft zu Berlin dem Minister der auswärtigen Ange-
legenheiten am 31. Dezember 1887 mitgetheilt habe, dass an Stelle des verstorbenen Hof-
rathes von Oppolzer Herr Direktor Weiss zum Gommissar bei der internationalen Erdmessung
ernannt worden sei.

« Hiervon habe ich dann das Präsidium der internationalen Erdmessung in Kennt-
niss gesetzt.

« Mittheilungen gleicher Art sind diesseits empfangen worden in Bezug auf Serbiens
Delegirten, Herrn Andonowits, und den mexikanischen Delegirten, Herrn Diaz Covarrubias.
Im letzteren Falle ist die Bemerkung beigefügt, dass dem Herrn Präsidenten Ibanez direkte
Mittheilung seitens der mexikanischen Regierung zugegangen sei.

« Aus dem Vorstehenden dürfte der normale Vorgang für die Akkreditirung ständiger
Bevollmächtigter deutlich ersichtlich sein: Mitiheilung an den Minister der

Angelegenheiten zu Berlin und direkte Mitiheilung an das
Eirdmessung.

auswärligen
Präsidium der internationalen

« In Bezug auf Letzteres, direkte Mittheilung an das Präsidium, wäre es mir sehr
erwünscht, von Ihnen baldmöglichst die wichtigen vorliegenden Prien zu erfahren,
da dieselben von Bedeutung für die allgemeine Einführ ung solcher direkter } Mittheilungen,

die im Interesse raschen Geschäftsganges sehr erwünscht sind, sein würden. So weit ich

weiss, ernennen mehrere Staaten (Frankreich, Rumänien) die Delssinten nur von Üonferenz

 

 
