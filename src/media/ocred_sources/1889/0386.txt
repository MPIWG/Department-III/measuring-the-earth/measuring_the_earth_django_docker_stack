ze

XIV. — Roumanie.

 

INDICATION
DES POINTS.

LATITUDE. |LONGITUDE.

 

 

EPOQUE.

DIRECTEURS
INSTRUMENTS.
ET OBSERYATEURS.

 

 

 

 

 

 

 

Ester
Insiratele

Jurilooka
Kalaigi

Kara-harman
Kara-Omer
Kara-Orman
Kasap-Kioi
Kälinova

Kioseler

Käsla-Vadani

Kopadin

Kogealak

Kumarovya

Kopukei

Koslugea

Kuiugiuk

Lunkavita x... »..:....

Märleanu...
unmabersen
Mosorali

Murfat-Kiueiuk

Oltina 2... 2.22.52

Pieineaga
Periprava
Popina
Portita

Sımlellols.........2..%
Sapata-bazä

Seimeni-mari

Stipok

Sonda

Turbencea

Tusla III®

Tusla I*

Yäeäreni

Visterna

Faro Insula Serpilor

Faru Constanta......... R

Bis. Sf.tu Nieolae Ful-
Bea

Giamia Mangalia

Vizir-tepe Ysaccea

Sf.tu Gheorghe Braila....

Triangulation de la Dobroudja. (Suite.)

44 36 15
44 56 9
44 47 5
44 312
44 16 39
44 718
44 28 2
43 49 22
45 323
44 36 40
44 57 37
45 21 33
44 157
014

3 23

33 46

52 49

50 59
849

1 30
1818

17: 37

13 43

13 33

45 16 19
4 9 8
44 40 31
44 11 13
45 11 13
44 616
44 59 51
4524 1

44.39 18
44 53 30
14 44

39 0
42 56
11.599
23 50

5 16.18
13 20
16
34 35

16

37

21

al

55

53

al

28

10 32

10 47
43 48 46
16 8
45 16 46

 

 

44° 96' 25"

44 58 25 |

 

262 9.30.
25 55: 32
26x12 2
26 32 33
25 25 25
26 11 28
25 31 30
26 21 6
25 53 39
21. 5 30
26 19 58
217-191
26 58 4
25 44 8
27 19 14
25 54.15
26 9583
26 13 45
26 5 26
25 13 16
29,8 20
25 55 22
26 34 20
2928 2
21 82)
21.11. 52
25 59 45
25 46 22
21 48

46 8

2 36

49 37

7 91

33 53
sshL

5 51 25
52 27

6 10
1%

39 31

44 45

6 45 52
54 25

26 1232
5 43 13
29 34

52 20

0 40

14 50

19 1

50 51

25 52

27 52 24
20,20 1.

26 28 20

 

26 15 11,
9 8
25 38 9]

 

 

SG,

Colonel ©. Barozzi
Major ©. Capitaneanu, Capitaines Bälan et Sägärceanu, Lieutenant Palada.
4 Theodolites de Starke de 12 p.

 

 

 

 
