 

weichungen der einzelnen Tagesmittel vom Endergebniss der Polhöhe für Berlin und
Potsdam zu bezw. ==0'og und =E0:08. Diese Zahlwerthe sind aber aus dem Grunde nur
als obere Grenzwerthe der wirklichen Unsicherheit der Tagesmittel zu betrachten, weil
in denselben die Anschlussfehler der Sterngruppen unter einander mit enthalten sind,
über deren Grösse ein zuverlässiges Urtheil erst nach Wiederbeobachtung der Gruppe I
zu erhalten ist. Schwankungen der Polhöhe im Betrage selbst nur einer halben Zehntel-
sekunde werden sich daher bei Anwendung der Methode Horrebow-Taleott durch länger
fortgesetzte Beobachtungsreihen mit Sicherheit feststellen lassen.

Trennt man die Resultate des ersten und zweiten Vierteljahres, so erhält man
für die beiden Stationen die folgenden Mittelwerthe:

Berlin. ‘ _ Potsdam.
1839; Ap Sternpaare. Ag Sternpaare.
Januar— März: — 0.03 156 — 0:04 163
April— Juni: +0.01 395 +0.01 556

welche zwar einer Aenderung der Polhöhe in gleichem Sinne entsprechen, ohne dass
diese Zunahme aber zur Zeit als erwiesen zu betrachten ist. Eine sichere Schluss-
folgerung in Betreff der Konstanz bezw. der Veränderlichkeit der Polhöhe wird aus diesen
Reihen erst nach Ermittlung der Zuverlässigkeit der Anschlüsse der einzelnen Stern-
gruppen gezogen werden können.

Andeutungen nennenswerther systematischer Tagesfehler sind in der Berliner
Beobachtungsreihe nur in den Tagen vom 4.—9. Januar und ı.—a. April in negativem
und vom ı2.—27. Juni in positivem Sinne enthalten, doch zeigt ein Vergleich mit den
Ergebnissen der Beobachtungen in Potsdam, dass nur die letztere Abweichung auch in
der Potsdamer Reihe zu erkennen ist.

Wie aber auch das Resultat der gegenwärtigen Untersuchung sich gestalten
möge: ob die Unveränderlichkeit der Polhöhe für die Dauer der Beobachtungsperiode
aus diesen Beobachtungsreihen hervorgehen wird oder ob Schwankungen der Polhöhe
nachzuweisen sein werden, so ist doch schon aus diesem beschränkten Zahlenmaterial
mit Sicherheit zu ersehen, dass diese Untersuchung ein reichhaltiges und schätzenswerthes
Material in Betreff des Studiums der Methode und der Instrumente sowie des Einflusses
der meteorologischen Vorgänge auf das Resultat der Polhöhenbestimmung liefern wird

Ta. ALBRECHT.

N nn —

TRENNEN

 

 

 

 

 
