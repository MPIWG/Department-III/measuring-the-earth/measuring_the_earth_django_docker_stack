 

EI.

z

TABLE DES MATIERES — INHALTSVERZEICHNISS

Proces-verbaux des seances de la Neuritme Conference generale de l’Association geodesique

 

 

 

 

 

 

 

1 internationale, tenue A Paris, du 3 au 42 octobre 1889 Pag. 4-5%
= a3.
Pag. Pa
rremisre söance,S.gclohre, 1889. Rapport de M. le General Ferrero sur les >
Liste des delegußs, .. 1 ....2 .2..2.- 3-5 triangulations (voir Annexe A. IV) . .. 27-28
Liste des invites. . . ie 5-6 Discussion sur la distribution des erreurs
Discours d’ouverture de 5. E. Monsieur aceidentelles . . . 98-99
Spuller, Ministre des Affaires etrangeres Rapport de M. le Colonel a sur on
derDrancp, syn a ae 6-8 mesurations des bases (voir Annexe A. V) 29-30
Reponse de M. le General an de Invitation aux Etats. d’envoyer les regles
! Mulhacen . . 9-10 geodesiques au Bureau international des
4 Nomination de M. Bag comme nd poids et mesures . . : 9
| et de MM. Bakhuyzen et Forster comme Rapport de M. Helmert sur les De
vice-presidenis.  . . Mash 1 |_ ‚ nations des pesanteur par le pendule
| Discours presidentiel de M. Fun ar 0-12 (voir ce rapport en langues allemande
| Rapport de M. Hirsch, Secretaire perp6- el frangaise, dans les Annexes A. VIla et
a\ tuel, sur la gestion de la Commission vi). 2 3
4 permanente. .. . 12 .19 Rapport de M. N eviärond
j Rapport du Bureau a, nen 1889, Par... n la verticale (voir Annexes A. Vla et
| M.le Directeur Helmert.. . 19-22 AN 2 3A
| Rapport de M. v. Kalmär sur les Nivelle- Bonpant de M. Albröhni sur 188 Unger
| ments de preeision (voir Annexe A. 1). 23 tions de latitude, dirigees par le Bureau
Explication deM. Henneguin sur le nivel- central, pour etudier les mouvements de
| lement en Belgique . ...0.0.0.23-24 l’axe terrestre (voir Annexe A.IX) . . 32
| Deuxieme seance, 7 octobre 1889. Troisieme seance, 9 octobre 1889.
| Lecture et adoption du proces-verbal de la Rapport de MN. le Marquis de Mulhacen
! BED same... °,.2.°. 05 25 sur les Mar&ographes (voir Annexe A. II) 33
| MM. Derrecagaiz, Ferrero et Forster Discussion et adoption de la proposilion
| sont designes comme membres de la de renvoyer le choix du zero commun
‚ Commission des finances . . 25 d’altitudes a la prochaine Conference . 34
} Les conclusions du rapport sur les ned Le rapport sur les mar&ographes sera confie
1 ments sont adoptees . . 25-26 a l’avenir au m&me rapporteur que celui
‚| Rapport de M. van de Sande- ae sur les nivellements de precision. . . 3%
sur les determinations astronomiques des Rapports des delegues autrichiens, MM.
longitudes, latitudes et azimuts (voir An- ' Tinter, v. Kalmar, v. Sterneck, Sur
Dexerse np“ 7 nu KBaN Eu E09, 27 les iravaux executes par la Commission
\
|

 

 

   
