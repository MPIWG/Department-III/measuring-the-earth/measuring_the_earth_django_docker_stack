  

 

 

 

ne

nn

3

zugleich vorläufiges Material zur Entscheidung der Frage gewonnen wird, ob die Polhöhe
innerhalb kürzerer Fristen Schwankungen unterworfen ist, hat das ÜOentralbureau_ seit
Anfang d. J. fortlaufende Breitenbestimmungen in Berlin und Potsdam ausgeführt, welche
gegenwärtig noch im Gange sind und zunächst bis zum Frühjahr 1890 fortgesetzt werden
sollen. Gerade diese beiden Stationen, welche nahe bei einander liegen -—- die gegen-
seitige Entfernung beider beträgt 26 Kilometer, — aber hinsichtlich ihrer Umgebungen
durchaus verschiedenen Charakter besitzen — die Station in Berlin befindet sich mitter
in der Stadt, diejenige in Potsdam ist allseitig von Waldung umgeben, — liessen
interessante Schlussfolgerungen erwarten, inwieweit das Resultat der Breitenbestimmung
von äusseren Einflüssen abhängig sei. Da ferner beide Stationen mit verschiedenartig
gebauten Instrumenten ausgerüstet sind — in Berlin beobachtet Herr Dr. Marcuse an
demselben Universal- Transit, an welchem Herr Dr. Küstner die oben erwähnte
Beobachtungsreihe ausgeführt hat, während in Potsdam Herr Schnauder an einem von
Wanschaff angefertigten Zenitteleskop von 68 Millimeter Oefinung beobachtet, — so
wird auch der Einfluss der verschiedenartigen Konstruktion der Instrumente aus den
Resultaten dieser Beobachtungsreihen zu ersehen sein.

Eine wesentliche Förderung erfuhr diese Untersuchung. dadurch, dass die Direk-
toren der Sternwarten in Strassburg und Prag: die Herren Prof. Dr. Becker und Prof.
Dr. Weinek bereitwilligst ihre Kooperation zu diesem Unternehmen zusagten und dem-
gemäss unter Anwendung der gleichen Beobachtungsmethode und nahezu gleichwerthiger
Instrumente bezw. vom Oktober ı888 und vom. Februar 1889 ab gleichfalls fortgesetzte
Breitenbestimmungen ausführten. Diese Erweiterung des ursprünglichen Planes wird es
voraussichtlich ermöglichen, die örtlichen Einflüsse in vollständigerer Weise von den
senerellen Erscheinungen trennen zu können.

Als Beobachtungsmethode wird auf allen vier Stationen diejenige von Horrebow-
Talcott angewendet. Entscheidend für die Wahl dieser Methode war die Erwägung, dass
dieselbe die einzige ist, bei welcher nach den bisherigen Erfahrungen keinerlei syste-
matische Tagesfehler zu befürchten sind. Zudem ist der Genauigkeitsgrad der Resultate
dieser Methode ein so hoher — der mittlere Fehler des Resultates eines Sternpaares in
einer Nacht ergab sich aus den Beobachtungen des Herm Dr. Küstner am Universal-
Transit der Berliner Sternwarte zu #o'20, während derselbe bei meinen Beobachtungen
auf der Schneekoppe am Zenitteleskop von Wanschaff nur =o'15 betrug, — dass bei
Anwendung einer der anderen Bestimmungsweisen ein gleich günstiges Resultat nicht zu
erwarten war. Das ganze Beobachtungsverfahren zeichnet sich ferner durch grosse
Durchsichtigkeit aus und gewährt zugleich den Vortheil, dass die Rechnungsarbeit auf
ein Minimum reducirt ist.

Gegenüber den zahlreichen Breitenbestimmungen, welche bereits nach der Methode
von Horrebow-Talcott ausgeführt worden sind, ist bei den im Gange befindlichen Beo-
bachtungsreihen auf Vorschlag des Herm Prof. Foerster eine wesentliche Verbesserung
dadurch herbeigeführt worden, dass anstatt eines Niveaus deren zwei zur Anwendung
kamen. Diese Vorsichtsmaassregel erschien gegenwärtig um so mehr angezeigt, als in

 

 
