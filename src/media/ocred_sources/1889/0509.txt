ee ee
ERS ESN eh  eET

Se

De

ee ee ER

 

 

 

 

ae

  

 

 

ER

Annexe A. VII!.

RAPPORT

SUR LES

MESURES DE PENDULE

PAR „IR VEREB MER RAR!

A la Gonference de la Commission permanente, tenue & Nice en 1887, jai eu dejä
’honneur de presenter un tableau statistique contenant les rösultats des mesures de pendule
les plus r&centes !.

Ce tableau se trouve entre les mains de MM. les Delegues. Des lors, des mesures
importantes ont &i& ex&culdes dans plusieurs pays. Malgre& cela, il n’est pas encore possible
de complöter nolablement ceite statistique, altendu qu’il n’a &t& publid pour la plupart des cas
que des donnees insuffisantes sur les resultats des nouvelles mesures. Je dois done me borner
en general A sıgnaler en quelques mots les stations dans lesquelles on a fait recemment des
experiences.

Par contre, il est & remarquer que des rapports detailles ont paru sur quelques
anciennes mesures de l’intensite de la pesanteur.

En France, on a continue les mesures de pendule commenc£es en 1883. Apres avoir
dötermine en 1886 et 1887 la pesanteur pour les stations algeriennes d’Alger et de Laghouat,
M. le Lieutenant-Colonel Bassot et M. le Commandant Defforges ont fait, pendant les mois
d’octobre et novembre 1887, dans les’ Alpes-Maritimes, pres de Nice, des recherches minu-
lieuses sur les variations de la pesanteur avec la hauteur, en observant dans quatre stalions
situces a 20m, 366m, 830m et 1420m au-dessus de la mer. Les rösultats de ces imporlantes

‘ Corrections & apporter au « Rapport sur les mesures de pendule, ete., par F. R. Helmert. »
Page 2, ligne 14, au lieu de 1887, lisez , 1877.

» 3, ligne 4, au lieu de : entre 1860 et 1870, Iısez: enire 1870 et 1880.

» 3, ligne 7, ajoutez apres le mot effectue : en 1879 et 1880.

» 4, ligne 41, au lieu de : 0,9939489, lisez : 0,9939498.

» 17, ligne 3, ajoutez : 1883 devant l’indication : p. 484.
Tableau IV, colonne 12, ligne de Königsberg, au lieu de : 574, lisez : 5

ANNEXE

EN
A.

vIrl-A. VII — 3

 
