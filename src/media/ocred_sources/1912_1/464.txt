BETT?

SWETRWF

 

371

 

messen, wobei die Hohen Tauern einerseits über dem Tauerntunnel, anderseits über dem
Sonnblick überquert werden.

Bei dieser Arbeit kommen 44 Stationen zur Beobachtung, wovon in den Jahren 1910
und 1911 auf 26 Stationen die Beobachtungen vorgenommen wurden.

Bei den Messungen kamen Srernzor’sche Pendelapparate (Wandstative), seit 1911
neuer, verbesserter Konstruktion, welche auch die Bestimmung des Mitschwingens der Unter-
lage nach Scaumann’schem Verfahren gestattet, ferner auch Nickelstahlpendel zur Verwen-
dung. Bemerkt kann werden, dass das Mitschwingen bei den Wandstativen ein äusserst
geringes ist.

1910 wurden die Beobachtungen im und zunächst des Tauerntunnels ausgeführt und
beobachteten hiebei 2 Beobachter auf 2 Stationen die Pendel paarweise gleichzeitig, wobei
1 Beobachter stets auf der Zentralstation Böckstein (Anlauftal), woselbst auch die Pendeluhr
installiert war, beobachtete.

Letztere Uhr übertrug mittels Relais die Stromschlüsse auf sämmtliche auswärtige
Stationen. Entlang des Tales und im Tauerntunnel konnte die Staatsleitung benützt werden,
indess für die Stationen im Gebirge eine eigene Leitung gelegt wurde.

Durch den entsprechenden Wechsel der Beobachter und der zugehörigen Pendel war
man in der Lage die Differenzen der Schwingungszeiten bezw. die Schwingungszeit der
mittleren Pendel frei von der Kenntnis des Uhrganges zu erlangen. Ein Vorgang den STERNECK
in früherer Zeit wiederholt zur Anwendung brachte. Selbstredend wurden aber dessen unge-
achtet in Böckstein doch Zeitbestimmungen vorgenommen.

Im folgenden Jahre war der Arbeitsvorgang der, dass von einer Referenzstation, in
welcher auch die Zeitbestimmungen ausgeführt wurden, die Uhrzeiten mittels Telephon,
wobei sich die Telephonapparate unmittelbar in den Beobachtungsräumen befanden, durch
Beobachtung der Koinzidenz zwischen Sternzeit und mittlerer Zeit auf die auswärtigen
Stationen übertragen wurden,

Die Zeitbestimmungen wurden mit Hilfe eines Passagerohres aus Meridiandurchgängen,
wobei das Fernrohr bei jeder Sternpassage umgelegt wurde, vorgenommen.

® 46

 

|
\
|
|
\

 
