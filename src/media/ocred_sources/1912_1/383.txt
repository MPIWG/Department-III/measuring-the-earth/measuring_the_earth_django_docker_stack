 

Re FIR HATTE gwnTIIn(n

oo

mw"

 

Institut geod6sique de l’Universit& de Padoue.

M. le Prof. E. Soner, vu les conditions topographiques sp&ciales des alentours de Padoue,
une plaine au milieu de laquelle surgit le massif isol& des „Colli Huganei”, eut l’idde
W’etudier les variations de la pesanteur en se rapprochant de ce massif, et il s’est pourvu
d’une balance de Eörvös avec laquelle il se propose d’explorer la ligne Abano-Mestre,
L’examen minutieux de la balance, fait dans l’Institut g&odesiqgue de l’Universite, a montr&
la concordance et la stabilit€ de ses indications.

Institut de geod&sie de ’Universit& de Turin.

Le Prof. G@. Armonkrrı, assistant pour l’enseignement de la g&odesie, a apport6 une
modification & la console murale de l’appareil de Srerneck, dans le but d’y faire osciller
un deuxieme pendule, et avec le nouvel appareil il a fait des observations ä& deux stations
gravimetriques & Turin et & Gönes. (Atti della R. Acc. delle Scienze di Torino. 1911).

Institut de g&odeösie de ’Universit& de Naples.

/

Les determinations de gravit& commencees en 1908 par le Prof. Ciccoxkrri, ont &t&
suspendues en attendant les modifications ä& introduire dans l’appareil de Srernzck. Le
Prof. Ciccoxerrı a determine, en 1910, la latitude et un azimut ä l’observatoire du Vesuve,
et a determine, en 1911, l’azimut de l’observatoire du Vesuve sur l’horizon de l’observatoire
de Capodimonte.

»

TRAVAUX EFFECTUES PAR L’INSTITUT GEOGRAPHIQUE MILITAIRE.

l. Travaux astronomiques. — Pour ötudier la forme du geoide dans la
partie moyenne de la vallee de l’Arno on a determine, en 1909 et 1911, 1a latitude et
’azimut astronomiques aux deux stations de 8. Romolo et Vallombrosa. Au moyen d’un
nivellement g&ometrigue ces deux stations ont dejä e&te liees & celles de M. Senario et
S. Giusto oü ces determinations ont &te faites pr&c&demment.

En 1910 on a repet& au signal trigonometrigue de Castanda la dötermination de
latitude et la mesure de l’azimut de la direction Oastanda-Milazzo. Les nouvelles valeurs,
reduites & la position moyenne du pöle, serviront au calcul des coordonndes g&ographiques
des stations de la nouvelle triangulation de la Sicile.

Au commencement de 1909, le Oapitaine d’Etat major Ecıpı et le Capitaine d’ar-
tillerie Gurgo partirent pour l’Eritree, afin d’y ex6cuter un nivellement astronomique le
long de la ligne geodesique Zagher-Tacarai. Les determinations de latitude et d’azimut
effectu&es aux deux extrömites de la ligne ont parues dans la publication „Lavori geodetici
e topografici compiuti nella. Colonia Eritrea”. 1911.
