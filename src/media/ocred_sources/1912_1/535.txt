 

ange

j
|
1
i:
|
1

 

Fe gen

TRIRTI HET

mr

BEILAGE A, XXXVIa,

BERICHT

über die geodätischen Arbeiten der Schwedischen Landesaufnahme.
(Rikets allmänna kartverk).

Seit dem letzten Bericht (1910) sind für die neue Triangulation erster Ordnung, im
südlichen Teil des Landes, Ketten bis 58°,5 Polhöhe recognoseirt worden. Die Winkelmes-
sungen sind jetzt im Gange und werden nach der Scureiger’schen Methode (Gewicht 18)
ausgeführt. Neue Universalinstrumente mit centesimaler Teilung und Horizontalkreisen von
0,325 m Durchmesser werden dabei benutzt. Als Signale dienen Heliotrop und Acetylenlampe,
in einen Apparat zusammengebracht. Das Acetylenlicht kommt sowohl nachts als bei bedecktem
Himmel vor dem Sonnenuntergang zur Verwendung. Für gleichförmige Bilder wird durch
vor dem Objektive der Universalinstrumente angesetzte Irisblenden gesorgt. Nachts wird die
Beleuchtung durch elektrische Lampen erzeugt. Der ganze Beleuchtungsapparat, mit 2 Lampen
für die Mikroskope, einer Feldlampe, regulierbar durch einen Widerstand, 2 Trockenelemente,
schnell austauschbar, und einem Stromschalter, wird leicht an die Vertikalachse befestigt, wodurch
die sonst üblichen unbequemen langen Leitungsdrähte soweit wie möglich vermieden sind.

Bei den jetzt stattfindenden machinalen Rechnungen, sowohl mit der centesimalen als
mit der sexagesimalen Winkelteilung, wie auch bei den logarithmischen Arbeiten mit der
neuen Teilung werden geeignete trigonometrische Tabellen sehr vermisst, besonders 6- und
7-stellige trigonometrische Tabellen, und zwar numerische für beide Winkelteilungen und
logarithmische für die neue Teilung.

Stockholm, September 1912. Kırn D. P. Rosen,

Professor im Generalstabe.
