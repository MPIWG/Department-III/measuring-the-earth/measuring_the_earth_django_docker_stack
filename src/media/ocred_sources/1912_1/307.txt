 

NE gen TITAN STE R "1

{ninRt

em

FITATTORG TIFIBIRTN ART TITERTT

269

de leurs longueurs mesurdes et de leurs longueurs caleuldes en partant de la base fonda-

mentale, sont — fractions qui correspondent & des differences (longueur

ji a
5000 +
caleulee moins longueur mesuree) egales A — 0n,07358 et 0m,02565.

A titre d’indication, on a re&alise l’accord des bases pour un enchainement simple de
triangles extraits du reseau d&jä compense sans accord des bases. En raison möme des faibles
valeurs des fermetures sur les bases de v£6rification, les corrections relatives qu’apporte cet

accord aux longueurs des cötes ne depassent point:

n- pour la section Nord de la triangulation,
1

39700 pour la section Sud de la triangulation.

„Tout en se reservant d’introduire plus tard, s’il y a lieu, les conditions de l’accord
des bases dans une nouvelle compensation generale du r&eseau au cours de sa revision, apres
obtention des deviations de la verticale, il est donc permis de n’attribuer & cet accord qu’une
importance secondaire dans la comparaison finale de la surface de niveau avec l’ellipsoide
de reference et dans le calcul des arcs, total ou partiel, & utiliser pour la determination

de l’ellipsoide terrestre general”.

3° Tome II, fascieule 7: Latitudes observees aus theodolites & microscopes, 2° et
3° parties par le Capitaine Prrrıer, 464 pages, 1911.

Le reseau du nouvel arc &quatorial comprend 60 stations de latitude realisant ainsi
le veu maintes fois exprime au debut de la Mission, tant & l’Association Ge&odösique inter-
nationale qu’ä l’Acad&mie des Sciences de l’Institut de France, de voir effectuer en un nombre
de stations aussi grand que possible des determinations astronomiques et tout specialement
des determinations de latitude qui acquierent une importance particuliere dans une mesure
d’are meridien destinee & fournir des donnees au calcul d’un ellipsoide terrestre general. Les
theodolites & microscopes ont fourni 45 latitudes par distances zenithales circumme£ridiennes.
Le fascicule 7 du tome Ill est consacr& & ces latitudes. Il se divise en trois parties: la
premiere, qui paraitra prochainement, renferme des considerations generales (voir plus loin);
les deux derniöres parties, r&unies en un volume dejä paru, comprenant les Zableaun nume-
riques des observalions et les conclusions.

Sa mise au point a necessit& pres de deux annees de caleuls.

La discussion de toutes les causes d’erreur conduit aux conclusions suivantes: sur
les 45 latitudes au tl&odolite, 7 d’entre elles, obtenues par observation du soleil, presentent
des erreurs moyennes variant de + 0",95 & + 57,48 (sexagesimales) et, tout en fournissant
des indications precieuses pour l’etude des deviations de la verticale, ne sauraient, en fait,
intervenir dans les calculs. Les 38 autres ont &t& obtenues par l’observation des &toiles, avec
toutes les pr&cautions voulues pour affranchir les moyennes des valeurs observees de toutes
les erreurs systömatiques, notamment de la ftexion de la lunette, si dangereuse quand les
6toiles ne sont pas convenablement reparties par rapport au zenith,
