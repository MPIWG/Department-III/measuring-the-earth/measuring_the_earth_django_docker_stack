 

364
umfassenden Revision unterzogen, und dabei nach einer anderen dem Karakter der Beob-
achtungen besser entsprechenden Methode reduciert. Diese Arbeit ist ebenfalls schon so weit
fortgeschritten, dass sie bereits im Anfange des nächsten Jahres dürfte zu Ende geführt
werden können.

Endlich werden auf Anregung und mit Unterstützung der kaiserl. Akademie der
Wissenschaften seit 3 Jahren von den Offisieren des k. uk. militär-geographischen Institutes
im Gehiete der hohen Tauern Schweremessungen ausgeführt, und zwar am Eingange, der
Mitte und dem Einde des Haupttunnels, dann in verschiedenen Thälern, und auf einer Reihe
von Höhenpunkten. Die höchste Station auf weiche die Messungen ausgeführt wurden
befindet sich auf der Spitze des Sonnblicks in einer Höhe von 3000 m. fast genau über
der Axe des Haupttunnels. Diese Arbeiten werden der Hauptsache nach in diesem Jahre
zum Abschlusse gelangen, und die Reduktionen im k. u. k. militär-geographischen Institute
durchgeführt werden.

 
