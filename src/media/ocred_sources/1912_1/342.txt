 

302

The defleetions in Baluchistan are notably smaller than we should expect from the
great mass of hills to the north-west. Gravity observations at Quetta and Mach* have,
however, shown that the Baluchistan hills are probably almost completely compensated
and the latitude results would seem to bear this out.

Topographical corrections have now been computed for about 100 latitude stations
in India, but much yet remains to be done.

III. PENDULUM ÖOBSERVATIONS.

(1) The following is a list of the Pendulum stations at which observations have
been made since the last report to the Geodetic Association was submitted in 1909. A
schedule of results has been sent to Professor Helmert and will appear in his report.

Saugor Amgaon

Damoh. Seoni These stations are in Central India, in and
Katni Jubbulpur around the Vindhya and Satpura hills.
Umaria Maihar ) The values of (90. — y,) varied from — 0'026
Pendra Allahabad to + 0:009 dynes.

Bilaspur Sultanpur Captain Cowie was the observer.

Raipur

Rangoon Meiktila \ The last two stations are on the high ground
Prome Mandalay east of the Irrawaddy valley in Burma.
Henzada Myingyan The remainder are in or near the same
Bassein Maymyo k valley. The values of (9% —Y) are nega-
Toungoo Mogok | tive for the two hill stations and positive
Pyinmana / for the remainder.

Captain Cowie was the observer.

Japla Sasaram T'he last 9 stations are in the Gangetic plain,
Daltonganj Moghalsarai the first two near the narrow Sone valley,
Ranchi Buxar and Ranchi is on the plateau to the south.
Gaya Muzaffarpur The values of (9, —y.) for stations in the
Monghyr Majhauli Raj | plains are negative and for the remainder
Arrah Gorakhpur positive but small.

Captain Couchman was the observer.

(2) An investigation into the correetion due- to the topography of the whole earth
based on the assumption that compensation is complete at a depth of 70 miles below
the surface has been commenced and some of the results are given by Colonel Burrard
in Part VI of this report. The Bouguer correetion for mass is at present used for all
stations.

* Vide page 7 of Report for International Geodetic Conference, 1909.

essen sen aus anheeiaihanensnn

}
!
{

 

i

 
