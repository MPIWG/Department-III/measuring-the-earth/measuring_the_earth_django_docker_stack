 

 

227

moyenne d’un angle calculde par la formule internationale et les fermetures des 23 triangles
dejä caleules. On n’a pas pris en consideration le triangle Encanada—Sierra Moreno—
Chug-Chug, dont l’erreur de fermeture est de 4,2 et on a cru nöcessaire de faire une
revision & Sierra Moreno.

Mesure des bases. — La Section de G&odesie a mesur& en 1910 & Pintados, dans le
dösert nommö „Pampa del Tamarugal’”, une base de 8,3 km, correspondant au trongen II
de la triangulation du nord et, en 1911, une autre au sud de 7 km ä Chacayal, du trongon I
de la triangulation australe.

Prochainement on fera la ‚mesure d’une autre base au nord, celle de Pacieneia, pour
laquelle les piliers qui fixent les extrömites ont &t6 dejä construits et le röseau de develop-
pement en cours de mesure.

L’Ofieina de Mensura de Tierras possede, pour la mesure des bases, un jeu de fils
en invar de 20 et 25m et une regle Etalon de quatre metres, pourvue de six microscopes
qu’on installe sur des piliers robustes en pierre ou en b£ton, & l’endroit m&me ou pres des
bases (Planche II), en permettant ainsi la comparaison directe et independante avec l’&talon
de quatre mötres de chaque fil de vingt metres, ou bien des fils de vingt cing me£tres en
prenant pour intermediaires d’autres fils de vingt mötres sur une base auxiliaire de cent me£tres.

La regle est en section en H, d’acier-nickel 4 42°), construite par la Societe Genevoise
et etudiee soigneusement & Breteuil.

On n’a pas fait usage des barres de projection aux extr&mites du comparateur, on
y pointait directement sur les traits des reglettes avec les fils doubles des mieromötres des
microscopes, au dessous desquelles on presentait les fils d’invar, dans la m&me position
qu’aux mesures des bases, et on faisait usage des tröpieds tenseurs et des poids de 10 kg.
On a fait les pointages en faisant glisser les fils dans les deux sens opposes sur une piece
avec repere ou sur tete de trepied.

Les fils employes pour la mesure des bases & Pintados et Ohacayal sont les num&ros
104, 105 et 323, tous trois etudi6s & Breteuil. Le transport s’est toujours fait avec enroulage
sans bobine, mais les fils 104 et 105 ont &t& longtemps enroules sur des bobines en bois,
depuis 1907 jusqu’a 1910.

Les comparaisons journalieres des deux fils en invar employes & la mesure d’une
base avec un autre, hors d’usage et gard& au bivouac, ont &t& faites au moyen de quarante
lectures & chaque extremite, en employant des trepieds fortement charges et avec les jambes
enfonces dans le sol.

Aux bases on a employ& deux fils en möme temps en faisant six fois la lecture des
echelles, qu’on a fait glisser trois fois en avant et trois fois en arriere, Les observateurs
ont change leurs places chaque fois apres dix positions des fils.

Pendant la mesure d’une base, les trepieds sont installes dans la verticale des piquets
enfonces dans le sol, dont un double nivellement de precision a donn& les cotes referdes
& la töte d’un clou qu’ils portent & la surface superieure. Tous les tr&pieds ont toujours
öt6 charges d’un poids de lO kg, suspendu par une corde passant au dessus des bras qui
portent les vis de calage, pour assurer ainsi la stabilit& des rep&res des trepieds.
