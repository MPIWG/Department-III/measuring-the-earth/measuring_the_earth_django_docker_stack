 

118

Erdrinde ist für die letzteren äusserer, für die Oberfläche innerer Raum. Wenn die Defor-
mation, wie hier, durch Kugelfunktionen dargestellt wird, so müssen in dem Ausdruck für
die Störung des Erdpotentials auch Kugelfunktionen negativen (Grades auftreten, also
hAWa + h,W_.„ Die zweite Konstante ist in der Dallemandschen Theorie ausser Acht
gelassen.

Herr Lallemand, der die Wiechertsche Hypothese des Erdkerns für unwahrscheinlich
hält, will nun seine Theorie bei kontinuierlicher Zunahme der Dichte angewendet wissen.
Aber auch hier muss in dem Ansatz für die Störung des Potentials durch die Deformation
das spezielle Dichtegesetz berücksichtigt werden. '

Herr Dallemand erkennt die Richtigkeit der Rechnungen des Herrn Sehweydar, aber
er fügt hinzu, dass sie sich nicht auf den von ihm behandelten Fall beziehen.

Herr Haid teilt dann die Resultate mit, welche er für die Gezeiten und die Festig-
keit der Erde aus den in Freiburg und Durlach mit den Horizontalpendeln angestellten
Registrierbeobachtungen aus den Jahren 1907 und 1908 abgeleitet hat (Beilage A. XD).

Der Präsident dankt den verschiedenen Delegierten für ihre wichtigen und gelehrten
Berichte und teilt einige Bemerkungen mit über die Mittwoch, abends, im Hotel Atlantic
stattfindende Zusammenkunft.

Die Sitzung wird 11 Uhr 20 Min. auf eine Viertelstunde unterbrochen.

Nach der Wiederaufnahme der Sitzung, erteilt der Präsident Herrn Titimann das Wort,
der eine Übersicht des sehr interessanten Berichts über die in den Vereinigten Staaten aus-
geführten Arbeiten gibt (Beilage A. XLD.

Der Präsident dankt Herrn Tittmann für seinen Bericht und erteilt Herrn Jeanne
das Wort für seinen Bericht über die in Belgien ausgeführten Arbeiten.

Herr Jeanne entschuldigt sich, kein für die Erdmessung wertvolles Material darbieten
zu können. Während der letzten drei Jahre war die geodätische Abteilung fortwährend
mit Arbeiten für das Kriegsministerium in Belgien beschäftigt, und Herr Jeanne glaubt
nicht, dass diese Arbeiten einiges Interesse für diese Konferenz haben würden. Er teilt jedoch
mit, dass die geodätische Abteilung im Jahre 1911 aus den während 10 Abenden mit dem
Prismen-Astrolabium ausgeführten Beobachtungen die Breite von Oul des Sarts bestimmt
hat. Obwohl die Rechnungen noch nicht ganz abgeschlossen sind, zeigen doch die erhaltenen
Resultate eine Lotabweichung von ungefähr zwei Sekunden nach Süden, welche wahrscheinlich
auf die Anziehung der Gebirgsmassen von Rocroi zurückzuführen ist.

Herr Oberst Deinert und Herr Greve legen der Konferenz ihre Berichte über die
Arbeiten in Chile vor (Beilage A. III und A. IV).

Der Präsident dankt den beiden Herren und erteilt Herrn Madsen das Wort für
seinen Bericht über die dänischen Arbeiten (Beilage A. V), und ferner den Herren Zaunhardt,
Helmert, Haid, Schmidt, Klasing und Bauschinger für ihre Berichte über die in den ver-
schiedenen Teilen Deutschlands ausgeführten Arbeiten (Beilagen A. VIII, A. IX, A. Xa,
A. XI, A. XU und A. XIII). Herr Helmert betont in seinem Bericht besonders die syste-
matische Lateralrefraktion, welche Herr Förster hat anzeigen können in Gegenden, wo die

 

ei

sm, vhmadlih br aa

 
