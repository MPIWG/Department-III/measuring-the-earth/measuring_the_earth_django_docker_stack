 

 

28

internationale M. Ernesto GreVE, Chef de la section geodösique de la Oficina de Mensura
de tierras, auparavant professeur de g&odösie et d’astronomie ä l’universit& de l’Etat.

Le 3 Mars 1911, M. le Ministre de Belgique m’a annonc& que M. le Colonel
adjoint d’Etat-major, Gıuues, membre de notre Commission permanente, a 616 remplacd
dans ses fonctions de Directeur general de l’Institut cartographique militaire par le Lieutenant-
colonel d’Etat-major Jrannn,

Le 22 Mai 1911, l’Ambassadeur de Russie a porte & ma connaissance que, d’ordre
de Sa Majeste l’Empereur, le Lieutenant-Göneral POMERANTZEFF, docteur en astronomie,
professeur de geodösie, avait &t& nomme delegue russe aupres de notre Association, et que le
General d’infanterie ARTAMoNOFF, ci-devant delegu6, venait d’&tre nomm6 membre du Conseil
de guerre. Par lettre du 12 Juin, le General H. PoMERANTZEFF m’a communiqus que Sa
Majeste l’Empereur lui avait confi6 la direetion de la Section topographique de l’Etat-major.

Apres la mort de M. Bosscna et la retraite de MM. v. Dissen et Kwistuour,
membres de la Commission geodesique nöerlandaise, le Gouvernement des Pays Bas a
nommöd membres de cette commission: M. le Dr. J. P. Kuzxen, professeur de physique
& l’universite de Leyde, en outre un Ingenieur en chef, directeur des ponts et chaussdes,
le Direeteur du service des levers topographiques militaires, le Ohef du service hydro-
graphique de la marine et un Ingenieur verificateur du cadastre.

Le Ministre de France & la Haye m’a fait savoir, par lettre du 6 Janvier 1912,
que par decret du 21 Decembre 1911, le President de la Republique avait nomm& le chef
d’escadrons hors cadre, LALLEMAXD, du Service geographique de l’armee, delegue permanent
aupres de l’Association internationale geodesigue en remplacement du General BErTHAUT.

Le 18 Mars 1912, M. le General-major Per Nissen m’a communique, qu’ayant
atteint la limite d’äge, il etait remplac& comme delegue de la Norvege aupr&s de l’Association
par M. le Colonel Knorr, Direeteur de l’Institut geographique de la Norvege & Christiania.

Le 14 Mai 1912, M. le Ministre de France & la Haye a porte & ma connaissance que
M. BaıwLAup, membre de l’Institut, Directeur de l’observatoire de Paris, avait &te nomm6&, en
remplacement de M. Bowquar DE LA GRYe, decede, membre de la del&gation chargee de
reprösenter la France, & titre permanent, auprös de l’Association g&odesique internationale.

Le 22 Mai 1912, M. le Lieutenant-colonel du Genie E. Mxssaua, Chef du Service
cartographique de l’armde & Athenes, m’a annonce, qu’apres la mort du General Lenrı, le
Gouvernement royal de la Grece l’avait nomme& delegu6 & cette Conference.

MM. l’Ambassadeur de la Grande Bretagne, le Ministre du Chili et le Ministre de
Roumanie & Berlin m’ont fait savoir que M. le Prof. Sir Gzorez H. Darwıs, comme delegue
de la Grande Bretagne, M. le Colonel Assımın& Frrıx Demerr, Chef du Departement
charg& de la confection de la Carte militaire de I’Etat-major general, comme delegue du
Chili et M. le G&n&ral Jannescv, Directeur du Service g&ographique de l’armee roumaine,
ainsi que M. le Colonel Vasırz Jonzscov, Directeur technique au m&me service, comme
delegues de la Roumanie, assisteront & nos seances.

Je viens de recevoir une lettre de M. le President de la Commission geodesique
autrichienne, m’annongant qu’ont &t6 nomme&s membres de cette Commission: M, le Capitaine

 

3

umursad sah. me hdiarıı Mau lads a.

Lak mit

i
i
I
|
