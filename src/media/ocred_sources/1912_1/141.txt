ee uassaae

TR CTETSETT

i
i
f
F
E
!
&
F

 

pen
oo
wo

einen Plan für die Rechnungsmethode aufzustellen und mit seiner Verwirklichung zu
beginnen.

Mit der Frage der Bearbeitung des europäischen Lotabweichungssystems hatte
ich mich schon 1880 im 12. Kapitel des I. Bandes meiner Theorien der höheren Geo-
däsie beschäftigt und die dazu nötigen strengen Differentialformeln aufgestellt. Ich
hatte damals auch die große Bedeutung der von mir nach Larzacz benannten, außer in Breite
sowohl in Azimut als auch in astronomischer Länge bestimmten Punkte für die Erhöhung
der Genauigkeit der Bestimmung der mathematischen Erdgestalt (also der Lotabweichungen
in Bezug auf ein Referenzellipsoid) bei großen Gebieten erkannt. Demgemäß wurde als
erster Versuch im Zentralbureau ein wesentlich aus Larraczschen Punkten und deren
direkten Dreiecksverbindungen gebildetes astronomisch-geodätisches Netz im norddeutschen
Gebiet behandelt und mit den erforderlichen Formeln und Hilfstafeln als Heft I einer
Serie weiterer Publikationen unter dem Namen ZLotabweichungen der Berliner All-
gemeinen Konferenz von 1886 vorgelegt und gewidmet.

Von den älteren Methoden der Bearbeitung von Gradmessungen unterscheidet
sich das vorgeschlagene Verfahren dadurch, daß erstens die Larraczschen Gleichungen
zur Auseleichung des Netzes zugezogen werden, und daß zweitens nicht alles auf
Meridian- und Parallelbogen zurückgeführt wird, sondern auch schiefe Bogen, wie sie sich
bei der Bildung des astronomisch-geodätischen Netzes zwischen Nachbarpunkten direkt
ergeben, unmittelbar zur Bildung von Lotabweichungsgleichungen herangezogen werden.
Die Methode entspricht der flächenförmigen Ausbreitung des Gradmessungsgebietes und
man kann für sie den Namen „Flächenmethode* einführen; sie unterscheidet sich von
der 20 Jahre später von Joun Havrorv als „Area-Methode“ in den Vereinigten Staaten
von Amerika benutzten Methode nicht wesentlich, sondern nur im praktischen Vor-
gang der Ausnutzung der Larzaczschen Punkte. Abgesehen von letzteren ist aber schon
im Hauptwerke über die Vermessung von Großbritannien und Irland, vor einem halben
Jahrhundert (1858), eine Art Flächenmethode angewandt worden.

Dem Heft I folsten bis zur Gegenwart noch die Hefte II, III und IV, in denen
115 geodätische Linien mit 61 Larraczschen Punkten bearbeitet sind in einem Gebiet von
Teehloi (Dänemark) und Dorpat im Norden bis Nizza und Castanea (Sizilien) im Süden,
Feaghmain (Irland) und Brest im Westen und Bobruisk (Rußland) und Astrachan im
Osten. In dieses Netz lassen sich nun die zahlreichen Zwischenpunkte leicht einschalten,
die nur in Breite oder in Breite und Länge oder Azimut bestimmt sind. Dies gilt z. B.
für das Spezialsystem des Harzes, das jetzt vorliegt (A. Garrr).

Eine eingehende Bearbeitung hat die Längengradmessung in 52° Br. gefunden,
die jetzt einen Teil jenes Netzes bildet. Einen ersten Schritt unternahm schon A. Fiscner
1883, indem er von Bonn bis Greenwich rechnete. 1886 stellten auf Wunsch von
O. Srruyvz die Herren A. Börscn und Th. Wrrrran die auf deutschem Gebiet gelegenen
Dreiecke mit den Anschlüssen an Belgien und Rußland zusammen. Eine eingehendere
Behandlung des Dreiecksnetzes wurde 1893 als Heft I veröffentlicht; 1896 folgte sodann

5
