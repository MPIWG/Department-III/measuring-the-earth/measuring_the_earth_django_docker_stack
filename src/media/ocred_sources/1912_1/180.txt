 

172

Je me suis surtout appliqgue A complöter les jonctions de ces systemes nationaux
entre eux et j’ai aid6 et encourag& entre autres les expöditions de M. Purnam, destindes
& obtenir la jonetion entre Washington, Paris, Potsdam et Londres (3 stations), ainsi que
les expeditions de M. Ham, destindes A rattacher entre elles les stations de Potsdam, Vienne
(2 Stations), Padoue, Karlsruhe, Munich, Strasbourg, Leyde et Paris. Toutes ces expeditions
ont &t& entreprises au moyen de subventions fournies par les dotations des fonds internationaux.

Il faut mentionner aussi l’esxpödition de la marine allemande & l’ouest de l’Afri-
que, oü le lieutenant Losson a dötermine Yintensit6 de la pesanteur en 20 stations
(1898). Cette expedition, ainsi que l’expedition allemande vers les regions antarctiques de
M. von Daveasskı et M. Fırckxer, Vexpedition de la Societ6 des sciences de Göttingue
vers les colonies allemandes dans l’Afrique orientale de M. Kontschürnter et GLAunınG
et V’expedition antarstique anglaise sous la direction de M. le Capitaine Roserr F. Scorr
ont et& aidees d’une maniöre eflicace par le Bureau central. Cette derniöre expedition a
möme regu les appareils de I’Institut g&odesiqgue. L’astronome russe Hanskı, malheureuse-
ment dec&de jeune encore, s’&tait pr&pare, en 1901, au Bureau central pour les observations
de pendule au Spitsberg et, en 1902, le Major S. G. Burrarn et le Major LEnox-CoNYNGHAMm

ont passe quelque temps au Bureau central pour se pröparer A de nouvelles observations

de pendule, qu’on avait l’intention d’excuter dans un grand nombre de stations aux Indes.
La pendule & demi-secondes destinee aux Indes a &t6 examinde A fond par M. le Prof.
HAASEMANN, au moyen d’observations de pendule.

Messieurs MODDERMAN, ANASTASıU, OLTAY, NIETHANMER, KEHLER et d’autres ont recu
au Bureau central des instructions d6taillees concernant les procedes des mesures relatives
de la pesanteur au moyen de pendules.

Des appareils de pendule appartenant ä des delegues &trangers ont &t& examinds
plusieurs fois au Bureau central, en partie par ces del&gues eux-mömes, en partie aussi par
des personnes attachees ä l’Institut geodösique (en general par M. Haasemann), notamment
pour la Suede, le Danemark, le Finlande, le Japon, 1’Espagne, la Röpublique argentine, le
Brösil, le Mexique, la Suisse, l’Italie (Rome), les Pays-Bas, la Baviere, le Wurttemberg,
l’Alsace-Lorraine, la Russie (Kasan),

Quelques-uns de ces Messieurs ainsi que MM. v. STERNEcK, P. G.. Rostn, SAvAnDER,
NaGaoRA, BLuMBACH, Auussıo, NIRTHAMMER, OLraY, Zapp et FAGErHoLM ont sejourne quelque
temps au Bureau central, dans le but de faire des observations de rattachement avec Potsdam U):

Le bätiment de l’Institut offrait pour tous ces travaux les locaux et les appareils
dont on avait besoin; en particulier des horloges exactes et un service horaire fort soigne
que M. le Prof. Wanacı, gräce & ses &tudes approfondies, a considerablement perfectionne.

Plusieurs fois l’atelier du mecanicien a pu rendre de böns services, on y a möme
construit pour d’autres Etats quelques appareils de pendule, ce qui pourtant n’est possible
que dans quelques cas exceptionels.

L’incertitude qui, nonobstant plusieurs excellentes recherches, existait encore, il ya

1) Voir la liste des hötes & l’annexe.

 

’

urarsumd sahne dia. Manildhs hi

Il

il

iu ınmah habs na MhaMuÄRA Äh a

 
