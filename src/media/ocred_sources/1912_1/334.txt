 

ANNEXR A. XVIIL,

ERHECE,

Rapport sur les travaux ox&cutös par le Service cartographigue
de l’Armöe 1909-1912.

Le Service cartographique de l’Armöde n’a pas eu l’occasion de s’occuper des travaux
de triangulation de ler ordre, car ce röseau est dejä acheve.
La base d’Eleusis a &t& definitivement calculde et sa longueur est:

L = 4924,654 933 m
log L = 3.6 923 758,3 m.

Une nouvelle loi a r&organis6 le Service cartographique, auquel a &t@ ajout& une
section speciale pour l’ex6cution du cadastre d’apres les instructions et le concours bien-
veillant du professeur M. Hai.

La section de triangulation s’est oceup6 pendant les dernieres anndes du röseau de
dme et 4me ordre selon les necessitös des travaux topographiques et cadastrales; nsanmoins
nous avons etudie definitivement la question du nivellement de pröeision et de l’installation
des maregraphes et m&dimardmötres.

Depuis l’annee derniere il existe pres de l’Observatoire d’Athönes une station radio-
telegraphique, de sorte que les travaux pour la dötermination exacte de la longitude d’Athönes
seront facilites au point de vue du contröle. Le Gouvernement Royal de Gräce est dispose
de depenser les sommes necessaires pour cette determination et espere obtenir le concours
bienveillant des membres de l’Association G&odesique Internationale.

Le Chef du Service cartographique
Athenes—Hambourg de l’Armde

Septembre 1912. E. MussALa.
Lieut.-Colonel.

 

ü
3
3
3
ä
3
1
1

 

 

nsunasämmadh abs bh a aan

 
