7
M. le Prof. Dr. Z. Marks, President du Senat des professeurs des instituts scienti-
fiques & Hambourg,
M. le Prof. Dr. Rathgen, President du conseil des professeurs de l’Institut colonial,
M. le Dr. J, Zepsold,
M. O0. Repsold,
M. le Prof. Dr. Schwassmann, Astronome & l’Observatoire,
M. le Prof. Dr. Stechert, Chef de section au „Deutsche Seewarte”.

Autres invites:

M. le Comte A. de la Baume Pluvinel, Paris,

M. le Prof. J. Boccardi, Direeteur de l’Öbservatoire de Turin,

M. le Prof. E. Jäderin, Professeur & l’Eeole polytechnique & Stockholm,
M. le Prof. Kovatcheff, Sofa.

M. le Gön6ral Bassot ouvre la seance, et declare que cette seance solennelle sera
uniquement consacree & la comm&moration du cinquantenaire de notre Association fondde par
M. le General Baryer. Il donne d’abord la parole & M. le Senateur von MELLE qui au
nom de la ville de Hambourg souhaite la bienvenue aux delegues, et prononce le discours suivant:

Messieurs,

En saluant au nom du Senat la 17e Conference generale de l’Association geodösique,
je souhaite la bienvenue ä Hambourg aux delegues de tous les Ritats eivilises, et en parti-
culier aux representants de l’Empire allemand et du Ministere de l’Instruction publique
de Prusse

La grande entreprise de l’Association g6odesique internationale ne pourra atteindre
son but, qui est de döterminer les dimensions et la figure de la terre ainsi que la disposition
des masses dans notre globe terrestre, qu’aprös les travaux assidus de plusieurs generations;
des & present, pourtant, cette Association a obtenu d’importants r6sultats seientifiques qui
parfois sont aussi d’un grand interöt pour la pratique; je n’ai qu’& nommer le percement
du tunnel du Simplon. Les travaux de l’Association se continuent reguliörement et sans
reläche d’annee en annee, et toujours de nouveaux problemes surgissent; tous les trois
ans la Conference generale se r&unit tantöt dans un pays tantöt dans un autre pour entendre
les rapports sur les travaux accomplis dans les differents pays, pour etablir le programme
des travaux pour les annees suivantes, et pour discuter les questions genörales concernant
les möthodes & suivre dans vos recherches,

La Oonference actuelle a encore un autre caractere, un caractere historique, car il
y a juste 50 ans que fut fondee, en 1862, & Berlin, sur l’instigation du General Baryer,
l’Association pour la mesure des degres dans l’Europe centrale, Association qui, s’&tendant
en dehors des limites qu’on lui avait assigndes d’abord, prit en 1867 le nom d’Association
