 

254

7. Ein Teilkreis von Hrvoe in Dresden und ein solcher von HırperranD in Freiburg

wurden auf dem Teilkreisuntersucher geprüft.

8. Auf dem geodätischen Turm wurden Horizontalwinkelmessungen nach Fernobjekten

an 22 Tagen vorgenommen.

Auch hat Herr Dr. Förster, der diese Messungen ausführt, eine theoretische Arbeit

verfasst: „Beitrag zur Theorie der Seitenrefraktion”, die in Gertanns Beiträgen zur Geophysik,
XI, 5. 414-—469, erschienen ist, und die die Möglichkeit eröffnet manche grosse Widersprüche
von Larzaczschen Gleichungen durch Refraktion zu erklären.

9. Herr Prof. Dr. Krüger hat die im folgenden unter 10 genannte mathematische

Arbeit verfasst und veröffentlicht.

zone

10.
de

12.

Eingehenderes bieten die Jahrsberichte des Direktors.

Veröffentlichungen.

Bestimmung: der Intensität der Schwerkraft auf 42 Stationen im nördlichen Teile von
Hannover und im Saaletale von Jena bis zur Elbe. Bearbeitet von L. Haasemann.
(Neue Folge. Nr. 41).

Seismometrische Beobachtungen in Potsdam in der Zeit vom 1. Januar bis 31. Dezember
1908. Von O. Hecker. (N. F. Nr. 42).

Astronomisch-Geodätische Arbeiten I. Ordnung. Bestimmung der Polhöhe und des
Azimutes in Memel im Jahre 1907. Telegraphische Längenbestimmungen Potsdam —
Jena, Jena— Gotha und Gotha— Göttingen im Jahre 1909. Von Ta. Ausrecar. (N. F,. Nr. 43).
Katalog der Bibliothek. Von W. SchweynaAr. (N. F. Nr. 44),

Jahresberichte des Direktors für 1909/1910, 1910/1911, 1911/1912. (N. F. Nr. 45,51 u. 56).
Tafel der Werte ad/(a + 5). Von B. Wanacn. (N. F. Nr. 46).

Seismometrische Beobachtungen in Potsdam in der Zeit vom 1. Januar bis 31. Dezember
1909. Desgl. vom 1. Jan. bis 31. Dzbr 1910, und bis dahin 1911. Von Orro Mrissner.
(N. FE. Nr. 47, Nr. 50 und 55).

Polhöhenbestimmungen in den Jahren 1902, 1903, 1908 und 1909. Von M. ScunAUDER.
(N. E. Nr. 48).

. Beobachtungen an Horizontalpendeln über die Deformation des Erdkörpers unter dem

Einfluss von Sonne und Mond. U. Heft. Von O. Hazcker, unter Mitwirkung von
OÖ. Meissner. (N. F. Nr. 49).

Konforme Abbildung des Erdellipsoids in der Ebene. Von L. Krügzr. (N. F. Nr. 52).
Astronomisch-Geodätische Arbeiten im Jahre 1911: Bestimmung der Längendifferenzen
Gotha—Knüll, Knüll—Erndtebrück, Erndtebrück—Bonn und Bonn—-Düsseldorf, und
der Polhöhe auf dem Dreieckspunkte Erndiebrück. Von Tu. Ausrecnr. (N. F. Nr. 53).
Untersuchungen über die Gezeiten der festen Erde und die hypothetische Magmaschicht.
Von Dr. W. SchweyDar. (N. F. Nr. 54).

HELMERT,

sah un

 

i
1
{
i
i
i

 
