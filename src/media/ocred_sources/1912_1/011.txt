 

6

XIV. RUSSIE. :

49. M. H. Pomerantzeff, Lieut.-General, Chef de la Section topographique de l’Rtat-major
& St.-Pötersbourg.

50.
öl.

52.

88.

54.

M.

M.

XV. SUEDE.

le Dr. P. G. Rosen, Prof. honoraire de l’Rtat-major & Stockholm,

le Dr. C. @. Fineman, Directeur du Bureau nautique meteorologique & Stockholm.

XVl. SUISSE.

. R. Gautier, Professeur et Directeur de l’Observatoire de Genöve.

XVII ESPAGNE.

. B. Mier y Muira, Lieut.-Colonel du Genie, Ingenieur g&ographe & Madrid.

XVII. HONGRIE.

le Baron R. von Hötvös, Professeur & l’Universite de Budapest.

XIX. ETATS-UNIS DE L’AMERIQUE.

0. H. Tittmann, Chef du „Coast and Geodetic Survey’ & Washington,

. W. Bowie. Inspeeteur de g6odesie au „Coast and Geodetic Survey” ä Washington.

Invites de Hambourg:

BB

BE

EEEREEEEE

. Behm, Capitaine de fregate, Directeur du „Deutsche Seewarte”,
. le Prof. Dr. Bolte, Directeur de l’Eeole de Navigation,

. Dengel, Inspecteur du cadastre,

. le Dr. Dolberg, Astronome & l’Observatoire,

le Dr. Z. Friederichsen, Seeretaire-göneral de la Societe geographique,
le Dr. Graf, Astronome ä l’Observatoire,

Grotrian, Directeur honoraire du cadastre,

le Prof. Dr. Gürich, Directeur de l’Institut mindralogique et geologique,

. Gurkitt, Ingenieur du cadastre,
. Klasing, Inspecteur en chef du cadastre,
. le Prof. Dr. Köppen, Chef de section au „Deutsche Seewarte”’,

‚le Dr HH. Kuss,

 

=
ü
=

a Te ai

As cs undh hen aa u

 
