 

264

Absteckmasse vorgenommen und die Grundstücksgrenzen unabhängig von allen in Natur
vorkommenden Veränderungen mit Sicherheit festgehalten werden.

Diese Sicherheit wird auch von den Grundeigentümern anerkannt, sodass Grenz-
streitigkeiten zu den grössten Seltenheiten gehören.

Was nun die Höhenbestimmungen betrifft, so wurde das erste umfassende Nivellement
in den Jahren 1869—1871 ausgeführt. Diesem folgte das jetzige, im Jahre 1884 begonnene
Präzisionsnivellement, welches nach der von Herrn Geheimrat Prof. Dr. Szisr ausgebauten
Methode des Bureaus für die Hauptnivellements im preussischen Ministerium der öffentlichen
Arbeiten ausgeführt und im System der Königl Preussischen Landesaufnahme ausgeglichen ist.

Die bei späteren Nivellements gefundenen Höhenabweichungen der einzelnen Höhen-
bolzen werden auf Millimeterpapier graphisch aufgetragen, sodass man ein anschauliches
Bild über die Sicherheit der einzelnen Höhenpunkte erhält.

Die Senkungen waren besonders in der Marsch recht bedeutend, so dass ich die
Aufmerksamkeit auf die jetzigen Hamburgischen Rohrfestpunkten lenken möchte, welche
aus eisernen, durch Muffen mit einander verbundenen Röhren bestehen und bis in den
festen Boden hinuntergetrieben werden.

Die Rohrfestpunkte sind auf Wunsch des Vermessungsbureaus in dankenswerter Weise
von der Sektion für Strom- und Hafenbau hergestellt und besonders auch zu den jährlich
vom Vermessungsbureau ausgeführten Revisionen der selbstschreibenden Flutmesser benutzt
worden.

Ob auch diese Rohrfestpunkte im Laufe der Zeit Senkungen ausgesetzt sind, kann
erst durch längere Beobachtung festgestellt werden, jedenfalls bedeuten sie einen wesentlichen
Fortschritt gegenüber den früheren unsicheren, an Gebäuden und anderen Gegenständen
angebrachten Höhenmarken.

Die Stromkarten der Elbe, die Peilungen u. s. w. werden nicht vom Vermessungs-
bureau, sondern vom Elbvermessungsbureau der Sektion für Strom- und Hafenbau ausgeführt.
Vervielfältigungen der Stromkarten werden in den Massstäben 1: 3000, 1:6000, 1: 25000
und 1:100000 herausgegeben.

Es sei noch gestattet, auf einige dem Vermessungsbureau gestellte Sonderaufgaben
und von ihm gemachte Beobachtungen hinzuweisen.

Zunächst wurde die Beobachtung des Nullpunktes aufgenommen, denn es ist immer
recht unangenehm, wenn dieser Punkt anfängt zu wandern.

Für Hamburg bildet die Spitze des Michaelisturmes diesen Nullpunkt, desshalb wurde
ihre Lage wiederholt geprüft und leider festgestellt, dass sich die Turmspitze, hauptsächlich
wohl durch den Druck der vorherschenden westlichen Winde, immer mehr in östlicher
Richtung: verschoben hatte,

Weitere Nachforschungen ergaben, dass der Erbauer Sonsın die Spitze der aus Holz
bestehenden Turmkonstruktion gegen die Mitte des unteren Turmmauerwerks absichtlich
um 36 em in nordöstlicher Richtung verschoben hatte, obgleich ein verschieben nach West
Nord West dem Einflusse der westlichen Winde besser entgegengewirkt hätte.

Als die Turmspitze für die Triangulation benutzt wurde, war sie bereits um 24 cm

 

 

4
4
ü
1
I
3
‘
i
1
i

nu a sh a an

|
!
!
!

 
