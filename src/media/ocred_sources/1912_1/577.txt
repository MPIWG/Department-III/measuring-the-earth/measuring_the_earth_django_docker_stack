I TREE ETHERNET 9° Te

451

 

D, and ©, are the mean visual and photographie latitudes respectively. The agree-
ment is such that we can assert that the two instruments give identical results,

Simultaneous observations of this character give valuable data for the study of the
phenomenon of abnormal refraction. If there is abnormal refraetion present at times near
the zenith, it should appear from a comparison of the results of the two instruments. If
E, and E, are ihe probable errors of an observation with the visual and photographie in-
struments, respectively, which include the possible refraction errors as well as instrumental
and personal errors, and if Eı is the probable error of the difference of latitude given by
the two instruments on a single night, we have the relation

a nn

provided there is no common error, such as anomalous refraction, which affects both insiru-
ments alike. From the table above we have mean Eı= + 0.12. For the winter and spring
ınonths at Gaithersburg, E, and E, are larger than their average values. They can be placed
at 0”.11 and 0.09 respectively without serious error. But since HE, is considerably smaller
than its value given by the equation above, we conclude that the latitudes are sensibly
influenced by refraction anomalies. The probable error of the anomaly is found from the
equation to be + 0.05, or for a single star +0”.07. These values are of course only
preliminary. More extensive data will eventually be secured for the study of refraetion
anomalies.

In elose relation to this subject is a striking phenomenon which has appeared in
the course of the work, and which seems to be established beyond the possibility of a doubt.
It was early noticed that the three images were not always in line with the measuring
thread, the eurvature correction aside. The displacement sometimes amounted to more than
a second of arc (0.03 millimeters), which is entirely too large to be aceounted for by any
looseness of the carriage. There is strong reason to regard it as a result of anomalous
refraction. In the first place trails of stars obtained with a fixed plate have shown a long
period waviness of the same order of magnitude in addition to the short waves commonly
observed. A further proof is in the fact that the phenomenon oceurs more frequently at
those times when the weather conditions are poor. In the fall during the good observing
weather the anomalies were seldom present. We must conclude that refraction causes at
times a swaying of the star image, in an irregular period of the order of a minute, perhaps
sometimes much longer, and of an amplitude which may amount to more than a second
of are.

These anomalies were, I think, first discovered and measured by Fr. Nusz and Joskpn
Jan Frıc some five years ago (Bulletin International de l’Acad&mie des Seiences de Boh&me),.

It would be interesting to know if such a phenomenon were present at {hose obser-
vatories which are located in a more suitable elimate than Gaithersburg. The weather at
Gaithersburg is subject to rapid changes throughout the year, so that we should expect the
atmospheric conditions to be especially bad for astronomical purposes. At Washington,
near by, a similar phenomenon has been observed visually.

56

he

 
