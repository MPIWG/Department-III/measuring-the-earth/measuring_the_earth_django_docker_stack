BEI AEEHITTVEN

|
i
!
F
!
i

änderung. Der Direktor des Zentralbureaus berichtet, dass diese Beobachtungen im Jahr
1909 angefangen haben.

Dem argentinischen Gesandten habe ich berichtet, dass die Konferenz mit Freude
die Nachricht des Ankaufs der Breitenstation in Oncativo von der argentinischen Regierung
erfahren, und den Wunsch geäussert habe, dass die dort angestellte Beobachtungsreihe zur
Breitenbestimmung weiter fortgesetzt werden möchte. Der Gesandte hat mir geantwortet,
dass er seiner Regierung den Inhalt meines Briefes mitgeteilt habe; ich habe jedoch nicht
erfahren ob diese unserem Wunsche Folge geleistet hat.

In demselben Brief habe ich dem Gesandten mitgeteilt, dass es der Konferenz angenehm
gewesen sei, von Herrn Prof. Porro pn Somenzı den Beitritt Argentiniens zu der inter-
nationalen Erdmessung zu vernehmen, und ich fügte die Bitte hinzu, dass die argentinische
Regierung Schwerebeobachtungen ausführen lassen möchte, besonders in den südlichen Teilen
des Staats, da diese Beobachtungen für die Bestimmung der Figur der Erde in der südlichen
Halbkugel von grossem Interesse sind.

In ihrer letzten Sitzung in Cambridge hatte die Konferenz den Beschluss gefasst, dass
es, in Anbetracht des grossen Wertes der Breitengradmessung in 30° Grad östlicher Länge
für die Bestimmung der Figur und der Grösse der Erde, von grosser Wichtigkeit war die
geodätischen Messungen dort so kräftig wie möglich fortzusetzen. In Übereinstimmung
mit dem Wunsch der Konferenz habe ich diesen Beschluss dem Herrn Reichskanzler des
Deutschen Reiches, dem Gesandten von Belgien und dem Botschafter von Gross-Brittanien
in Berlin mitgeteilt.

Dem russischen Botschafter habe ich, dem Beschluss der Konferenz zufolge, den
besten Dank der Konferenz dargebracht, für die grossen Dienste, welche die russische
Regierung der Erdmessung erwiesen hatte, indem sie Herrn Professor HECKER ein Kriegs-
schiff auf dem schwarzen Meere zur Disposition stellte, um dort den Einfluss der Schiffs-
bewegungen auf seinen Schwereapparat zu untersuchen.

Die Konferenz hatte den Wunsch geäussert, dass zur Erhaltung guter Anschlüsse
der Nivellementsnetze benachbarter Länder, die Terrainarbeiten und die Berechnungen so
viel wie möglich beschleunigt werden möchten für die Anschlüsse:

a von Spanien und Frankreich bei Port Bou,

b. von Österreich einerseits, und Russland und Italien andererseits.

Den Vertretern dieser fünf Staaten in Berlin ist dieser Wunsch mitgeteilt worden.

Der französische Botschafter hat mir geantwortet dass, "was Frankreich betrifft, die
Terrainarbeiten und die Berechnungen für den Anschluss in Port Bou schon seit 1896
fertig seien, und dass es deshalb nur die Sache Spaniens sei, die noch nötigen Arbeiten
auszuführen.

General Artamoxorr hat mir am 4. Oktober 1910 geantwortet, dass die militär-
topographische Abteilung mit dem geographischen Institut in Wien über diese Angelegen-
heit verhandele.

Der spanische Botschafter hat mir mitgeteilt, dass er den Inhalt meines Briefes zur
Kenntnis seiner Regierung gebracht habe.

 
