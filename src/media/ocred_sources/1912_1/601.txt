 

464

Zweite Sitzung, Mittwoch, den 18. September .

Verlesung des Protokolls

Geschäftliche Mitteilungen.

Beschluss, Telegramme zu schicken an a even Da a Back

Geschäftlicher Bench des ständigen Secretärs über die Jahren 1909—1912

Dank des Präsidenten an den Delegierten für die Beileidsbezeugungen bei dem Tode des mn Poinoanl £.

Ernennung der Finanzkommission

Bericht des Herrn Helmert über die Tätigkeit ie ninalbareaug er der 16. Gonstälkonferenz

Bericht des Herrn Albrecht über den internationalen Breitendienst.

Ernennung einer Breitenkommission .

Aeechäfchehe Mitteilungen. :

Bericht des Herrn Galle über die Dein

Bericht des Herrn Bowrgeois über die Herausgabe der Arbeiten Her Bröllene se in Hennder.

Bericht des Herrn Zallemand über die Nivellements. !

Ernennung einer Kommission, zur Beratung eines Vorschlags eine neue Kataborie von N N elle
einzuführen. ;

Geschäftliche Moe.

Dritte Sitzung, Samstag, den 21. September

Verlesung des Protokolls der zweiten Sitzung .

Geschäftliche Mitteilungen .

Bericht des Herrn Bourgeois über alte en dlinien

Bemerkungen der Herren Borrass, Gautier, Foerster und man

Bericht des Herrn Albrecht über Längen-, Breiten- und N ukbestinongen N über den a
wurf einer Dängenbestimmung zwischen Potsdam und Cambridge (Amerika) . .

Bemerkungen der Herren Helmert, v. Hötvös, Schweydar und Tittmann über diesen Entwurf .

Bericht des Herren Helmert über die Lotabweichungen i

Bericht des Herrn Turner im Namen des Herrn Darwin über die lee Mo in Indien:
Australien und Canada

Bericht des Herrn Wade über die Mn in Me eomeen.

Vierte Sitzung, Montag, den 23. September

Verlesung des Protokolls der dritten Sitzung

Geschäftliche Mitteilungen . ;

Bericht des Herrn Borrass über die relativen oe emessungen :

Berichte des Herrn Madsen über einen photographischen Registrierapparat ai Schworemessungen
und über einen statischen Schweremessungsapparat

Bemerkung des Herın Helmert über diese Berichte .

Bericht des Herrn Hecker über die mit dem Horizontalpendel tele a ech

Bemerkung des Herrn Helmert .

Bericht des Herrn Sehweydar über die Deleahion der Erde later do ee von ons und Mond

Bemerkungen des Herrn OR. Lallemand in Bezug auf diesen Bericht

Erwiderung des Herrn Schweydar

Bericht des Herrn Haid über die Beobachtungen ah Bez in bins and Durlach

Geschäftliche Mitteilung

Bericht des Herrn Ziftmann aan die A in deh Wereintelen Ben

Bericht des Herrn Jeanne über die geodätischen Arbeiten in Belgien

Berichte der Herren Deinert und Greve über die Arbeiten in Chile.

Page, Seite

86—108

86
86—87
87
87—101
102
102
102—104
104—106
106
106—107
107
107
107

107
107—108

109—112

109
109
110
110

am
aa
111—112

112
112

113—119

113
113
113--116

116
116
116
116
116—-117
IR.
117—118
118
118
118
118
118

 
