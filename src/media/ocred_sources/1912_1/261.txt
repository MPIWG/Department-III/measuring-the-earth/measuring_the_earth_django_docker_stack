 

ebenso die Unterschiede der Längen der geodätischen Linien und der ebenen Hilfslinien im
conformen Coordinaten-Sistem bekannt waren, habe ich als Bedingungsgleichungen die Formeln:

asin® +bsin(a +B— 180) +esin@a+ß+r—360)=0,
ac08s @ + b.cos (& +8 — 180) + cecos(« +8 -+-r — 360) -d=Ö

 

angewandt und durch deren Diffenzirung die betreffenden Fehlergleichungen aufgestellt.

Der mittlere Fehler einer Richtung betrug + 0",9.

In Bezug auf diese Ausgleichung hat der frühere Director der Sternwarte von Santiago,
Prof. A. OsrecHr, jetzt Consultor technico des Generalstabes eine interessante Arbeit eingereicht.
Unter Weglassung einiger Richtungen, die auf sein Verfahren störend eingewirkt hätten, stimmte
er zunächst alle Dreiecke auf 180° plus dem entsprechenden sphärischen Excess ab. Mit den
nun erhaltenen Werten stellte er eine Azimutgleichung und eine Seitengleichung durch die
ganze Kette auf und berechnete die Coordinaten = und y eines bestimmten Punktes von
zwei Seiten her. Auf diese Weise reduzierte er die ganze Rechnung auf nur 4 Bedingungs-
gleichungen.

Die Einzelheiten dieses Verfahrens sind aus der Arbeit selbst zu ersehen.

Schliesslich ist die Messung des Basisnetzes Chinigue—El Monte noch einmal erfolgt.

Ich hatte die Ergebnisse der ersten Messungen dem Geodätischen Institute zur Begutachtung
eingereicht. Herr Professor Hrrmerr machte mich darauf aufmerksam, dass alle Dreiecke

1 in dem Basisnetz einen negativen Schlussfehler ergaben und dass in Folge dessen ein siste-
f matischer Fehler vorliegen müsse.

Nach den Untersuchungen des wissenschaftlichen Mitarbeiters des Geodätischen Instituts,
Herrn Professor Förster, wäre wahrscheinlicher Weise dieser sistematische Fehler in der
Seitenrefraction zu suchen, welche gerade in Chile durch eine besondre Verteilung der
Wärmeschichten der Atmosphäre, die durch die gleichzeitige Nähe der hohen Üordillere und
des Oceans bedingt sei, verursacht worden wäre. Hierauf gab er an, in welcher Weise er
die Messungen ausführen würde.

Diese Messungen sind nun nach den Ratschlägen des Herrn Prof. Dr. HnıLmerr aus-
& geführt worden. Leider sind die Daten erst in den letzten Momenten meiner Abreise ein-
i gegangen, dennoch hoffe ich sie dem diesjährigen Bericht beifügen zu können.

Klee HE

FeLix Deinerr.
Oberst und Chef der militärischen
Landesaufnahme in Chili.

Te RENTE TTTRENmgIT BETT gem

 

 
