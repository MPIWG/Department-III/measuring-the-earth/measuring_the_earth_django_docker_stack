 

32

 

V, ÜOBSERVATIONS DE LA PESANTEUR.

L’accölöration de la pesanteur fut determinde, au cours de la p£riode considerde dans
ce rapport, au Caucase et dans le bassin du Volga, & Vaide de l’appareil StERNECK; en outre,
on effeetua la liaison de Tachkent A Tiflis, de Tiflis & Poulkowo et de Kazan & Poulkowo.
Les opörations auxquelles se livra le Colonel Konzoung au Caucase portörent, en 1909, sur
11 points, en 1910, sur 13 points et, en 1911, sur 9 points.

Dans le bassin du Volga, 9 points furent observes en 1909 par les astronomes de
1’Observatoire astronomique imp6rial de l’Universit€ de Kazan.

Le Colonel ZarzsskY effectua la liaison de Tachkent ä Tiflis en 1909 et celle de
Tachkent & Poulkowo en 1911. Celle de Kazan & Poulkowo a 6t& effectu&e en 1911 par
M. Baranorr, Astronome-adjoint & l’Observatoire de Kazan (pas encore calculee).

Les rösultats des travaux de 1909 ont 6t6 publies dans l’ouvrage de M. Borrass ainsi
que dans le Tome LXVI des Memoires de la Section Topographique de l’Etat-Major General.

Quant aux rösultats des travaux de 1910 et 1911, ils sont exposes dans le tableau
pae. 891.

Au cours de la periode consideree, on continua ä se servir, pour les observations
du pendule, de chronomötres & interrupteur, qui donnörent de magnifiques rösultats. L’emploi
de ces chronomötres, dans les localit6s incultes et peu accessibles, oflre des avantages con-
sid6rables, 6tant donndes les diffieult6s que prösente le transport des horloges en ces lieux.

V]J, NIVELLEMENTS DE PRECISION.

Au cours de la p6riode considörde dans ce rapport furent effectues les nivellements
suivants:

1) En Russie d’Europe, le long des lignes de chemin de fer situ6es & l’interieur
des grands polygones de la chaine anterieure de nivellement.

2) Au Caucase, on termina la seconde liaison du niveau des Mers Noire et Caspienne
entre Poti et Batoum, sur la Mer Noire, et Bakou, sur la Mer Caspienne.

3) Dans le Turkestan fut achevee la liaison des nivellements de la Russie d’Europe
et de ceux du Turkestan, le long du chemin de fer Orenbourg—Tachkent.

4) En Siberie fut continus le nivellement longeant la ligne du chemin de fer
Transsiberien.

5) Conformöment au dösir exprime & la XVIe Conförence de l’Association Geodesique
Internationale, on a effectu& la liaison des nivellements russe et autrichienne, & la station
de Nowosielica.

Au cours de la p6riode consideree dans ce rapport furent effectues en Russie d’Europe
et en Russie d’Asie 2332 kilomötres de nivellements doubles (dans les 2 sens) et 4460
kilomötres de nivellements dans un seul sens; en outre, durant cette möme periode, on
proceda & l’6tablissement de 311 nouveaux reperes.

i
|
|
|
1

 

 
