 

186

 

Vebersicht der Arbeiten des Königl. Geodätischen Institutes unter Generalleutnant 2. D. Dr. Baeyer,
nebst einem allgemeinen Arbeitsplan des Institules für das nächste Dezennium. Berlin 1886.
49 pages in 4% et avec 2 planches. (Extrait des „Verhandlungen des wissenschaftlichen
Beirats’ de 1886; par M. le Prof. Dr. Helmert).

b) Publications de I’Institut geodesique royal de Prusse.

Astronomisch-Geodätische Arbeiten I. Ordnung. Telegraphische Längenbestimmurgen in den
Jahren 1885 und 1886. Berlin (P. Stankiewiez) 1887. (Von Prof. Dr. Albrecht).
216 pages in 4°.

Astronomisch-Geodätische Arbeiten I. Ordnung. Telegraphische Längenbestimmungen im Jahre
1887. Bestimmung der Polhöhe und des Azimutes auf den Stationen Rauenberg und
Kiel in den Jahren 1886 und 1887. Berlin (P. Stankiewicz) 1889. (Von Prof. Dr.
Albrecht). 269 pages in 4°.

Astronomisch-Geodätische Arbeiten I. Ordnung. Delegraphische Längenbestimmungen in den Jahren
1888 und 1889. Bestimmung der Polhöhe und des Azimutes auf der Schneekoppe im
Jahre 1888. Bestimmung des Azimutes auf Station Trockenberg im Jahre 1889. Berlin
(P. Stankiewicz) 1890. (Von Prof. Dr. Albrecht). 273 pages in 4° et 3 planches.

Astronomisch-Geodätische Arbeiten I. Ordnung. Telegraphische Längenbestimmungen in den
Jahren 1890, 1891 und 1893. Berlin (P. Stankiewicz) 1895. (Von Prof. Dr. Albrecht).
241 pages in 4°.

Polhöhenbestimmungen aus dem Jahre 1886 für zwanzig Stationen nahe dem Meridian des Brockens
vom Harz bis zur dänischen Grenze. Gelegentlich ausgeführte Polhöhen- und Azimutbestim-
mungen aus den Jahren 18378—1884..Berlin (P. Stankiewicz) 1889. (Von Prof. Dr. Löw).
152 pages in 4°,

Lotabweichungen in der Umgebung von Berlin. Berlin (P. Stankiewiez) 1889. (Von Prof.
Dr. Fischer). 155 pages in 4° et 6 planches.

Polhöhenbestimmungen im Harzgebiet. Ausgeführt in den Jahren 1887 bis 1891. Berlin (P.
Stankiewiez) 1894. (Von Prof. Dr. Löw). 75 pages in 4°.

Bestimmung der Polhöhe und ‘der Intensität der Schwerkraft auf zwei und zwanzig Stationen
von der Ostsee bei Kolberg bis zur Schneekoppe. Berlin (P. Stankiewiez) 1896. 288 pages
in 8° et 4 planches.

Die Polhöhe von Potsdam. 1. Heft. Berlin (P. Stankiewiez) 1898. 140 pages in 4° et 3 planches
lithographiees.

Bestimmungen von Azimuten im Harzgebiete ausgeführt in den Jahren 1887 bis 1891. Bestim-
mung der Längendifferenz Jeraheim—Kniel mittels optischer Signale. Berlin (P. Stankiewicz)

1898. 87 pages in 4% et 1 planche.

Bestimmung der Intensität der Schwerkraft auf fünf und fünfzig Stationen von Hadersleben bis
Koburg und in der Umgebung von Göttingen. Bearbeitet von L. Haasemann, Ständigem
Mitarbeiter im Königl. Preussischen Geodätischen Institut. Berlin (P, Stankiewiez) 1889.
96 pages in 8° et 3 planches.

hun AbManı ahlähru

Pr

ran

Ah nn

ass anmahbh aba an.

 

 
