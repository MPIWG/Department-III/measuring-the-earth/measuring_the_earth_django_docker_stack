 

 

FÜNFTE SITZUNG
Mittwoch, den 25. September.

Saal im Vorlesungsgebäude.
Präsident Herr General Bassot.
Die Sitzung wird eröffnet 9 Uhr 15 Min.
Sind anwesend:

die Delegierten, Jeanne, Deinert, Greve, Madsen, Petersen, Foerster, Albrecht, Bauschinger,
v. Bertrab, Borrass, Finsterwalder, Galle, Haid, Helmert, v. Knauer, Kohlschütter, Launhardt,
Schmidt, Schorr, Sehweydar, v. Wegerer, Weidner, v. Zylinicki, Bassot, Baillaud, Bourgeoss,
Hanusse, A. Dallemand, Ch. Lallemand, Messala, Turner, Wade, Oeloria, Guarducei, keina,
Hirayama, Sugiyama, Deyva, Bakhuyzen, Hewelink, Muller, Knofj, Weiss, Schumann, Pomerantzejj,
Rosen, Fineman, Gautier, Mier, v. Bötvös, Titimann, Bowie ;

die Eingeladenen, Kovatchef, Wild, Gurlitt, Dr. I. keepsold, Sehwassmann.
Das Protokoll der vierten Sitzung wird gelesen und genehmigt.

Der Präsident erteilt Herrn Helmert das Wort, der den Empfang eines von einem
Mitglied der argentinischen geodätischen Kommission gesandten Briefes mitteilt, der enthält
dass Oberst Garcia Aparicvo zum Delegierten der argentinischen Republik bei der Erdmessung
ernannt worden ist, und dass ein bedeutender Bericht über die ausgeführten geodätischen
Arbeiten an den Sekretär abgesandt worden ist (Beilage XLII). Herr Helmert freut sich über
diese Mitteilung, da durch diese Arbeiten die Messung des südamerikanischen Meridianbogens
durch Argentinien bis Kap Horn fortgesetzt werden konnte.

Der Präsident erteilt, der Tagesordnung gemäss, den französischen Delegierten das Wort.

Heır A. Dallemand gibt eine Übersicht über die Arbeiten der geodätischen Abteilung
des militärgeographischen Dienstes und äussert den Wunsch, dass die Regierungen von Italien,
der Schweiz und Frankreich sich mit einander verständigen mögen, den Anschluss ihrer

 

men Lammanin cn

3:
i
3
=
&
3#
4
“u
I

u

|
|

 
