 

iel

Name NL FRIRN 0m Luna pri

E

»

 

Triangles Erreur de clöture Exees spherque
Gringo, Artecitas, Dijerina 2.20 0. ae 0.66 0.28
ss 2 Reunowe. u me 0.39
= Tijerina 5 SEE ee ee ee ya 0.47
Rerugio eArleemas ljerina .. .. 0.0000. 2 mod 0.36
Artscıtase@olommres, Menactas »  ....... Eag9 0.22
e ne Njerinar 2000. 2er 0.36
= Tenaecitas. 5 ee ran 0.33
Mjerma, Colombrestemen oo 2 se 0.19
Sommer des ecanıs en mon 0.0.2 7.790449
» 5: en „plusss. 2 22 5 21,05

Les ecarts nögatifs sont l&gerement sup6rieurs.

NIVELLEMENT DE PRECISION.

Ces travaux se sont poursuivis, quoique lentement, et, afin de les faire avancer, on
a fait construire trois niveaux de plus du type „Coast and Geodetie Survey”, pour augmenter
les sections de nivellement dans le but de rattacher plus vite nos bases & la mer.

Depuis & peu pres un an un mar&mötre a 6t& installed & Veracruz, en face du quai
de la Sante; il y fonctionne bien.

En ce moment nous sommes en train d’en installer un autre & Tampico, dans le
io Pänuco, & environ 500 mötres de son embouchure. L’idee a 6t& de faire- l’installation
dans la riviere et d’obtenir la hauteur de l’eau de la mer au moyen d’un siphon dans le
tube en beton arm& oü se trouve le maremetre.

Le resultat des travaux de nivellement a &t& assez bon, car la tolerance qui etait
au commencement de + 5umK a 6t6& reduite & + 3umK,K etant la distance en kilomötres.
Je ne rentre pas dans plus de details, une brochure speciale ayant ete publiee sur ce sujet
(Nivellement de precision de Mexico & Irolo). On a fait connaitre en temps opportun &
M. Onartes LALLEMAnD les progres du travail qui est & present avanc& jusqu’ä Perote.

ASTRONOMIE.

Nous avons pu faire les stations suivantes.

Lanuyü (extr&mite sud de l’are) ot l’on a observe& la latitude et l’azimut.

Jamiltepee: point auxiliaire oü l’on a observ& la latitude et la longitude, le rattachant
& la triangulation meridienne par la methode de SnELLIUS.

Aux points de triangulation: Sartenejas, extremite nord de la base de Rio Verde,
extremitses ouest des bases de la Cruz et de Öolombres, extremite nord de l’arc de meridien,
on a observ& la longitude, la latitude et l’azimut; et aux stations de la triangulation
San Felipe, Escorpiön et Frontön de Palmas, on a observ& seulement la latitude et l’azimut.

 
