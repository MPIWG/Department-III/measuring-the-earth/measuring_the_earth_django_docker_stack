 

256

Die Epoche für den Anfang der 24 Werte einer Periode ist 1907 October 31,
12h Greenw. mittl. Z. Die Auswertung der einzelnen Tiden geschah nach den von Prof.
Dr. Börezn in „Die harmonische Analyse der Gezeitenbeobachtungen” gegebenen Regeln (An-
nalen d. Hydrographie XII. Jahrg. 1884). In der zunächst folgenden Übersicht ist die halb-
tägige Mondtide M, für die in Nord—Süd und in Ost— West stehenden Pendel der Stationen
Freiburg und Durlach angegeben, wobei die Phase auf Ortszeit bezogen ist. In den zuge-
hörigen weiter unten folgenden grafischen Darstellungen ist die ganztägige Mondtide M,
zu M, mit hinzugenommen worden; es ergibt sich daher dort ein doppelter Umlauf. In
diesen figürlichen Darstellungen ist die Phase auf Greenwicher Zeit bezogen. Zu bemerken
ist, dass die N—S Pendel die Komponenten der Lotbewegungen in B—W geben, und
analog die E—W Pendel für die Komponenten der Lotbewegung in N—S gelten. Abgesehen
davon ist in den Tabellen die von ScHwsYDAR angenommene Bezeichnungsweise beibehalten.
H bzgl. 5 bedeuten die beobachtete bezgl. die für eine starre Erde theoretische Amplitude
des Lotes; « und x’ hängen von astronomischen Grössen ab, die durch die Epoche der
Beobachtungen bedingt werden; A ist von der Aufstellung der Horizontalpendel und der
Lage der Station abhängig. Die Grössen ©, K, v und » beziehen sich auf die aus den
Registrierungen schliesslich erhaltenen Resultaten, und es bedeutet = das Verhältniss der
beobachteten zur theoretischen Amplitude des Lotes; K hängt ab von der Differenz der
radialen Deformation in der physischen Oberfläche und in der Niveaufläche der Erde; y und
x bestimmen v® und unter » ist eine Phasenverschiebung verstanden. Bei positivem » ist
die Deformation der physischen Erdoberfläche mit innerer Reibung verbunden, wird v negativ
so rührt die Phasenverschiebung nicht von einem Reibungseinfluss her; schliesslich bedeutet
n den Starrheitskoeffizienten der Erde oder ihre Konstante der Formelastieität. Die Werte
dieser Grössen sind nach Scnweypar’s Vorgang aus M, und O ermittelt und in den beiden
folgenden Tabellen zusammengestellt worden. Wie aus den grafischen Darstellungen von
M, u. M, sowie von ©, welche im gleichen Maassstab gezeichnet sind, deutlich hervorgeht,
ist die O Tide bedeutend kleiner als M,, und ist insbesondere die Meridiankomponente von
O in unseren Breiten wegen des Factors cos 2® ohnehin sehr klein. Letztere ist aus den
E—W Pendeln 3 u. 4 sehr. unsicher bestimmt und kaum nachweisbar, während die Ost-
West Komponente auch in der O Tide gut zu erkennen ist, Die Resultate der E—W Pendel
in der Tabelle der O Tide sind daher ohne Bedeutung.

 

neuestes

Bahn

mattstahemndh hin aa

 
