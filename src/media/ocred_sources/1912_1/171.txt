 

mente were

Verhandlungen der Internationalen Erdmessung,
veröffentlicht von ihrem ständigen Sekretär A. Hirsch.

Verhandlungen der vom 27. Oktober bis zum 1. November 1886 in Berlin abgehaltenen
Achten Allgemeinen Konferenz der Internationalen Erdmessung und deren
Permanenten Kommission. Mit 8 lithographischen Tafeln. Berlin (Reimer) 1887.
XVIIL-+ 248 Seiten in &°.

Verhandlungen der vom 21. bis zum 29. Oktober 1887 auf der Sternwarte zu Nizza
abgehaltenen Konferenz der Permanenten Kommission. Mit 11 lithographischen
Tafeln. Berlin (Reimer) 1888. 673 Seiten in 4°.

Verhandlungen der vom 17. bis 23. September 1888 in Salzburg abgehaltenen Konferenz
der Permanenten Kommission. Mit 5 lithographischen Tafeln. Berlin (Reimer)
1889. 193 Seiten in #°.

Verhandlungen der vom 3. bis 12. Oktober 1889 in Paris abgehaltenen Neunten
Allgemeinen Konferenz der Internationalen Erdmessung und deren Permanenten
Kommission. Mit 14 lithographischen Tafeln. Berlin (Reimer) 1890. 633 Seiten in 4°.

Verhandlungen der vom 15. bis 21. September 1890 zu Freiburg i. B. abgehaltenen
Konferenz der Permanenten Kommission. Mit 9 lithographischen Tafeln. Berlin
(Reimer) 1891. 194 Seiten in #°.

Verhandlungen der vom 8. bis 17. Oktober 1891 zu Florenz abgehaltenen Konferenz
der Permamenten Kommission. Mit 4 lithosraphischen Tafeln. Berlin (Reimer)
1892. 234 Seiten in &'.

Verhandlungen der vom 27. September bis 7. Oktober 1892 in Brüssel abgehaltenen
Zehnten Allgemeinen Konferenz der Internationalen Erdmessung und deren
Permanenten Kommission. Mit 14 lithographischen Tafeln und Karten. Berlin
(Reimer) 1893. 797 Seiten in 4° (besonders angefügt: Rapport sur les Triangulations.
320 Seiten und 3 Karten).

Verhandlungen der vom 12. bis 18. September 1893 in Genf abgehaltenen Konferenz
der Permamenten Kommission. Mit 21 lithographischen Tafeln und Karten.
Berlin (Reimer) 1894. 194 Seiten in #°.

Verhandlungen der vom 5. bis 12. September 1894 in Innsbruck abgehaltenen
Konferenz der Permanenten Kommission. Mit 7 lithographischen Tafeln und
Karten. Berlin (Reimer) 1895. 255 Seiten in #°.

Verhandlungen der vom 25. September bis 12. Oktober 1895 in Berlin abgehaltenen
Elften Allgemeinen Konferenz der Internationalen Erdmessung und deren
Permanenten Kommission.

I. Teil: .Sitzungsberichte. Berlin (Reimer) 1896. 309 Seiten in 4°.

II. Teil: Spezialberichte über die Fortschritte der Erdmessung und Landes-
berichte über die Arbeiten in den einzelnen Staaten. Berlin (Reimer)
1896. 315 Seiten in #° und 16 Tafeln und Karten (besonders an-
sefüst: Rapport sur les Triangulations. 850 Seiten).

35
