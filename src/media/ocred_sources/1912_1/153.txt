u

Übersicht

der Veröffentlichungen des Königl. Preuszischen Geodätischen Institutes
und Zentralbureaus der Internationalen Erdmessung

nebst einem Anhang
über die Verhandlungen der Internationalen Erdmessung.

a) Publikationen des Königl. Preuszischen Geodätischen Institutes.

Bestimmung der Lüngendifferenz zwischen Berlin und Lund, auf telegraphischem Wege
ausgeführt von dem Zentralbureau der Europäischen Gradmessung und der Stern-
warte in Lund im Jahre ‚1868. Von ©. Bruhns, Direktor der Sternwarte und
Professor der Astronomie in „Leipzig, d. 7. Cher der Astron. Sektion des Zentral-
bureaus. Ur Lunds Univ. Arsskrift. Tom. VII. knmgl Sal, SL Seien mm 20
(Diese Publikation ist der Vollständigkeit wegen hier einger eiht.)

Bestimmung der Längendiffereng zwischen Berlin und Wien, auf telegraphischem Wege
ausgeführt von den Herren Prof. Foerster und Prof. Weiss. Von Dr. C. Bruhns,
Direktor der Sternwarte in Leipzig, d. Z. Chef der Astronomischen Abteilung des
Geodätischen Institutes. Leipzig (Engelmann) 1871. 47 Seiten in 4°.

Astronomische Bestimmungen für die Europäische Gradmessung aus den Jahren 1857
bis 1866. Von Dr. J. J. Baeyer, Präsident des Königl. Preußischen Geodätischen
Institutes. Leipzig (Engelmann) 1873. 125 Seiten in 4°.

Beobachtungen mit dem Bessel’schen Pendel-Apparate in a Sn Güldenstein,
ausgeführt im Auftrage des Geodätischen Institutes von Dr. ©. F. W. Beters,
Observator der Sternwarte in Kiel. Mit einem Grundrisse von Güldenstein.
Hamburg (Mauke Söhne) 1874, 151 Seiten in 4°.

Bestimmung des Längenunterschiedes zwischen den Sternwarten von Göttingen und
Altona. Von Prof. Dr. C. A. F. Peters, Direktor der Königl. Sternwarte in Kiel.
Kiel 1880. 103 Seiten in 4°.

Astronomisch-Geodätische Arbeiten im Jahre 1870. Bestimmung der Längendifferenz
zwischen Bonn und Leiden. — Bestimmung der Polhöhe und eines Azimutes in

17
18
