Spenge NNTn 'y |

71775

BEILAGE A. XI.

BERICHT

über diein den Jahren 1910 bis 1912 in Bayern ausgeführten
Erdmessungsarbeiten.

VON

Dr. S. FINSTERWALDER und Dr. M. SCHMIDT.

A. ASTRONOMISCHE ARBEITEN UND PENDELMESSUNGEN.

Über den Stand der astronomischen Arbeiten der Kgl. bayr. Kommission für inter-
nationale Erdmessung ist Folgendes zu berichten:

Das im laufenden Jahre 1912 erschienene Heft 7 der astronomisch-geodätischen
Arbeiten der genannten Kommission enthält die relativen Schweremessungen von 23 Stationen
des Königreichs, von welchen 13 im Jahre 1902 von Professor Anpıne und 15 im Jahre
1907 von Herrn Dr. Grossmann erledigt wurden. Seither hat Dr. Zarp 31 neue über das

ganze rechtsrheinische Bayern verteilte Stationen hinzugefügt, deren Berechnung nahezu

fertiggestellt ist. Die von Herrn Professor Anpıng ausgeführte doppelte Breitenbestimmung
in Asten sowie die Messung des Azimuts Asten-Hochgern sind fertig bearbeitet und werden
demnächst veröffentlicht werden. Ferner ist die Berechnung von 9 älteren Breitenbestim-
mungen (Pfaffenhofen, Eichstädt, Pleinfeld, Roth a. S., Lichtenfels, Öttingen, Nördlingen,
Donauwörth und Mühldorf) der Vollendung: nahe. Das laufende Jahr war ausserdem mit
den Vorbereitungen für die Dängenbestimmungen München—Kirchheim und München—
Asten, welche im Laufe von August und September ausgeführt werden, ausgefüllt.

B. GRoDÄTISCHR UND NIVELLEMENTS- ARBEITEN.

Die geodätischen Arbeiten beschränkten sich auf die Ausführung der trigonometrischen
Seiten- und Koordinatenberechnung der bereits im Jahre 1903 längs des 48. Breitenparallels
in Südbayern gemessenen Hauptdreieckskette und die Untersuchung des Genauigkeitsgrades
der aus den Jahren 1801 und 1807 herrührenden beiden Grundlinienmessungen in Ober-

 
