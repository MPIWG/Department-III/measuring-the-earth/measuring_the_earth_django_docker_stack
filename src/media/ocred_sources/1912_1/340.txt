 

300

The length of the completed triangulation is 112 miles and the area contained
within the triangulation is 256924 sq. miles. The mean error of an observed angle
of triangles computed from the formula

ed?
m=+ 3n
amounts to + 0”'358.

The average height of the 15 stations is 2348 feet. Some of the hills on which the
stations are situated are flat topped and nearly all are thickly wooded, thus necessitating
heavy clearing work before observations could be started.

The instrument used was a three-mieroscope 12-inch micrometer theodolite by Messrs
Troughton and Simms. The observers were Mr ©. H. Tresham and, during his absence
on sick leave for 14 months, the late Lieut. H. G. Bell.

One astronomical azimuth has been observed on this series up to the present time,
namely:

Station Stars A-G
|

Bhusur > A Urss® Minoris, | Sn eh Ban

B.A.C. 2320

‚Since the last Geodetie Conference in 1909 the Indian Survey have obtained a
new three-microscope 12-inch micrometer theodolite by Messrs Troughton and Simms.
Up to 1909 all recent work had been executed by the two-microscope 12-inch micro-
meter theodolitee The new instrument has given entire satisfaction, it reduces the actual
amount of observing work at each station while giving an equally accurate result.

The Invar wires for measuring bases were received in India in December 1908.
The new standard bars were expected to arrive in September 1911, but up to the present
date neither they nor the new comparator have been received.

Burma has been explored with the idea of selecting suitable base lines and two lines
have actually been prepared, but their measurement must awalt the arrıval of our new
standards.

The extension of the Indian triangulation to the west, north and east, has brought
it within measurable distance of connection with the triangulation of other countries.
But the Indian triangulation is fitted on to too small a spheroid; hence at the extremes
appreciable errors are generated.

In order to dissipate these errors the whole triangulation will have to be readjusted
so that it may fit a spheroid of the dimensions now adopted by the International
Geodetic Association as most nearly approaching the true value.

A proposed method of carrying out this adjustment is given in “Note on a change
of the axes of the terrestrial spheroid in relation to the triangulation of the GT.
Survey of India, by J. de Graaff Hunter (Dehra Dun, 1912).

 

i

 

 

cs aaa ab a EL an

{
ı
!
|

 
