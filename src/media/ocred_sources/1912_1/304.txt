 

BEILAGE A. XII.

BBSRICHT

über die Arbeiten in Elsass-Lothringen.

Die in den Jahren 1900—1905 ausgeführten Schweremessungen sind definitiv bear-
beitet worden. Die Resultate sind in der im August 1912 zur Versendung gelangten Ver-
öffentlichung: niedergelegt:

„Relative Bestimmungen der Intensität der Schwerkraft auf 45 Stationen von
Elsass-Lothringen, nach den Beobachtungen von EB. Becker, J. Büncın, L. ÜARNERA,
P. Gast, E. Jost, H. Kosonp, K. ScHiuzer, bearbeitet von E. Becker,

Karlsruhe, Druck der Braunschen Hofbuchdruckerei, 1912.

VI und 150 Seiten 8° mit Karte”,

Im Sommer 1912 wurde vom Unterzeichneten eine Erkundungsexpedition ausgeführt,
um geeignete Stationen für die jetzt in Angriff zu nehmenden Breitenbestimmungen zu er-
mitteln. Es haben sich 22 Punkte als geeignet erwiesen. In ihnen — hauptsächlich Punkten
1. und 2. Ordnung — werden in den nächsten Jahren genaue astronomische Breitenbe-
stimmungen nach der HorrzBow-TALoorr-Methode ausgeführt werden.

J. BAUSCHINGER.

Ah u

j
|

 

4
j
‘
3
3
3
’
i
i

 
