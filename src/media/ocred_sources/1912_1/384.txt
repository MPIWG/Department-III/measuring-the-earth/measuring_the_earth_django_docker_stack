 

336

2. Travaux ge&odäsiques. — En 1909, on a continue les operations pour la
revision de la triangulation au sud du parallele de Rome et l’on a observ& ä& 33 stations
formant 52 triangles.

En 1910, les operations pour relier l’ile de Stromboli au continent ont et& continu6es.

3. Nivellement de pr&cision. — Üonformement au veu, exprimö par la Com-
mission sismologique nommee par le Gouvernement aprös le tremblement de terre de la
Calabre du 28 Döcembre 1908, on a ex&cut& de nouveau le nivellement de precision des lignes
Messina-Oastanda-Gesso en Sieile et Gioia Tauro-Melito di Porte-Salvo en Oalabre.

Dans la plaine de Padoue on a procede ä la determination altimetrigue de nombreuses
lignes nouvelles qui 6taient n&cessaires au „Magistrato alle Acque’”’ pour ses Etudes de bonifi-
cation et de navigation interieure.

En 1910, & la suite d’une proposition de l’Institut g&ographique de Vienne, on a ex&cut&
des nivellements le long: des lignes Toblach-Cortina d’Ampezzo-Belluno, e Trento-Tezze-Bassano
sur un parcours de 230 km. pour obtenir des liaisons nouvelles avec les lignes autrichiennes.
Une conclusion & cet &gard ne peut encore &tre formulee, parce que les donnees de l’Institut
autrichien ne sont pas encore mis ä& notre disposition.

Pour expliquer le d&saccord signal& au Oongr&s de Londres par I’Ing. LAuviemann entre
les rösultats de notre nivellement et ceux du nivellement frangais au „Oolle dell’ Argentera’’
(„Col de Largentiöre”), la direction de Y’Institut confia ä IIng. Lorerrino la revision de
toutes les donndes et des caleuls. On ne peut cependant pas encore arriver ä une conclusion
definitive, parce que les lignes frangaises ne possödent pas le caractöre de lignes fondamen-
tales, comme les lignes italiennes.

Les nouvelles lignes de nivellement, niveldes dans les {rois annees, couvrent un
parcours d’environ 1500 km.

4. Service mar&graphique. — Les marögraphes d’Ancone, de Livourne,
de Messine, de Venise et de Gönes ont cess& leur service ä& cause de differentes circonstances,
mais on a d6jä pris les dispositions pour qu’ils puissent bientöt fonctionner de nouveau.

Un nouveau marögraphe (type Rıcmarn) est etabli sous la direction de l’Institut
g6ographique militaire & Mazzara en Sicile.

5, Relövement astronomiqueg&odesique des cötes Lybiennes entre
Tripoli et Derna. — Une mission composee de l’Ing. LioPerrino, le topographe
ALESSANDRINI et le Lieutenant d’artillerie Gıansı füt envoyde par l’institut en Lybie au
mois de janvier 1912. Dans la pöriode de six mois elle y a eflectu6 les trayaux suivants:

a) determinations de latitude et d’azimut & Tripoli, Lebdah, Bengasi, Derna;

b) quatre mesures de base avec l’appareil de JÄpenın et les fils d’invar dans les quatre
endroits;

c) dans les ports de ces villes on a install& des Echelles marömetriques et par l’etude

 

 
