 

bayern und Franken, wobei die Beziehungen der diesen Messungen zu Grunde liegenden
Maasseinheiten zur internationalen Meterlänge festgestellt wurden. Eine Neuberechnung des
Anschlusses dieser Dreieckskette an die österreichische Gradmessungstriangulierung bei Salz-
burg und in Nordtirol, welche durch die zur Ausführung gekommenen ergänzenden Winkel-
messungen in den österreichischen Anschlussdreiecken bedingt war, veranlasste eine nähere
Untersuchung der aus diesen Berechnungen hervorgegangenen scheinbaren Verschiebung der
Hauptdreieckspunkte Watzmann und Grossrettenstein.

Die Ergebnisse dieser Untersuchungen und Berechnungen sind in den Sitzungsberichten
der Kgl. Bayer. Akademie der Wissenschaften, mathem. physik. Klasse, Jahrg. 1910 und
1912 veröffentlicht und wurden als Material für die Berechnung der Längengradmessung
auf dem 48. Breitenparallel dem Oentralbureau in Potsdam zur Verfügung gestellt.

Was die ausgeführten Nivellementsarbeiten betrifft, so ist das im Landesbericht vom
Jahre 1909 als druckfertix erwähnte neue Höhenverzeichnis mit dem Titel „Das Bayerische
Landesnivellement. Höhen über Normal Null der in Bayern rechts des Rheins und in der
Pfalz durch Marken festgelegten Nivellementspunkte” im Jahre 1910 im Verlag der K. B.
Erdmessungskommission in München erschienen.

Es enthält die Beschreibung der örtlichen Lage und die Meereshöhen von 4320
Höhenfestpunkten nach dem Bestande vom Ende des Jahres 1909, sowie eine Zusammen-
stellung der über die bayerischen Nivellementsarbeiten bisher erschienenen Veröffentlichungen.

Die Gesamtlänge der in diesem Höhenverzeichnis aufgeführten und in einer Über-
sichtskarte dargestellten Nivellementslinien beträgt 6128 km.

Die im letzten Bericht erwähnten Ergänzungsmessungen des bayerischen Landes-
nivellements sind fortgesetzt worden durch in beiden Richtungen ausgeführtes Doppelnivelle-
ment der neuen Linien:

Mühldere- Kosenheimaı 2.7.2. 262’km
Mühldorf— Freilassing. . . . . 66km
Traunstein— Garching. . . . . 34km

und durch wiederholtes Doppelnivellement der bereits früher in einer Richtung nivel-
lierten Linien:
Manltl Mühldorf, ». ... 14% 26km
Rosenheim— Freilassing . . . . 81km
Freilassing—Reichenhall. . . . 15km.
Die endgiltige Ausgleichung und Berechnung dieser Linien ist in Ausführung begriffen.
Auf den in Vorland der Bayerischen Alpen verlaufenden Linien kommen mehrfach
starke Steigungen und Gefälle von 1:100 und darüber vor, bei welchen die Ziellinien
einerseits nur wenige Dezimeter, andererseits aber mehr als zwei Meter hoch über der
Bodenfläche die Latten treffen, wobei sich insbesondere bei warmen, sonnigen Wetter in
Bodennähe Refraktionswirkungen ergeben, welche die starke Unsymmetrie der Lichtkurve
für Vor- und Rückblick bedingen.
Zum Studium der hieraus entstehenden Nivellementsfehler hat der bei diesen Arbeiten
beteiligte Assistent des geodätischen Instituts Dr. KonnmüLter auf umfangreiche Versuchs-

 

ea euere

uns, süshmnh ab eh nn

 
