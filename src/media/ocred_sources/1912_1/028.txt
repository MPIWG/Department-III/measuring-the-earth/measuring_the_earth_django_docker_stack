TEEN

DEUXIEME SEANCHE

Mereredi, 19 Septembre.

Salle du »Vorlesungsgebäude”.

Prösidence de M. le Gön6ral Bassot,

La seance est ouverte ä& 9 heures 15 min.

Sont presents:

les delegu6s: Jeanne, Deinert, Greve, Madsen, Johansen, Petersen, Sand, Foerster, Albrecht,
Bauschinger, Borrass, Finsterwalder, Galle, Haid, Helmert, v. Knauer, Kohlschütter, Launhardt,
Schmidt, Schorr, Schweydar, v. Wegerer, Weidner, Bassot, Baillaud, Bourgeois, Hanusse, 4.
allemand, Ch. Lallemand, Messala, Turner, Celoria, Guardueei, Hirayama, Sugiyama, Leyva,
Dakhuyzen, Heuvelink, Muller, Knoff, Weiss, Schumann, Pomerantzef, Rosen, Fineman, Gautier,
Mier, v. Eötvos, Tittmann, Bowie;

les invites: de la Baume Pluvinel, Jäderin, Kovatcheff, Wild, Dengel, Dolberg, Gurlitt,
Klasing, Stechert.

Le procös-verbal de la premiöre seance est lu et adopte.

M. le President fait savoir qu’il se propose de faire, quelqgues minutes avant la sus-
pension de la seance, l’appel de tous ceux qui assistent & la sdance et prie chacun de se
lever, & l’appel de son nom, afin que les nouveaux apprennent ä connaitre les anciens et
que les anciens aient le plaisir de saluer les nouveaux venus,

M. le Scerdtaire donne connaissance de quelques lettres et depeches qu’il a regues:

1°. Une lettre du Bourgmestre de Hambourg O’Swarn qui, empöche de venir &

nos seauces, exprime le grand inter&t qu’il porte ä nos travaux;

2°. Une lettre de M. Darwın, qui &erit que l’6tat de sa sante ne lni permet pas

de venir & Hambourg; il envoie aux delöguds ses salutations et exprime ses
meilleurs veux pour le succös de la Conförence;;

3°. Une depöche de M. BackLunD, annongant qu’il est emp6che de venir, puis une

lettre du m&me collögue &erite d’une autre main que la sienne mais pourtant

 
