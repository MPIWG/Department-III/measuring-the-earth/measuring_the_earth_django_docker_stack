 

350 :
absolue fut deduite d’observations barom6triques & l’observatoire meteorologique de Oolombres.
propriete de la Compagnie agricole.

La mesure eut lieu & l’&poque oü. soufflent les vents du nord, avec des temperatures,
variant pour cette raison entre 10° et 28° dans des journdes de calme ou de grand vent,
et avec des alternatives de temps couvert.

Comme il fallait s’y attendre, les meilleurs rösultats furent obtenus pendant les journees
de temps calme et couvert, et les moins favorables pendant les journdes de vent et de
temps d&couvert; les r6sultats n’en sont pas moins excellents et le travail fut rapide et &cono-
mique, car la mesure de la base dura seulement une semaine en employant deux ingönieurs
et un aide.

TRIANGULATION.

La triangulation projete entre les stations de Resbaladero et Tanquecillos, jusqu’a
celles de Colombres et Tenacitas, dernieres stations de la chaine meridienne, fut effeetude.

Pour faire les observations, il 6tait indispensable d’employer des tours de 10 metres
de hauteur, et on a suivi la möthode de Besser, except6 pour les stations de Tanqueeillos,
Refugio et Tenacitas oü l’on choisit la m&thode de Soureizen, ayant remarqu& que les tours
etaient un peu mobiles.

La möthode de Scnhremer a dt suivie ä d’autres points ot l’on a repete les obser-
vations d’aprös la methode de Bsser, afin de nous former une opinion sur la valeur des
deux methodes, et l’on est arriv& & la conclusion que, lorsque les visees sont longues et
traversent des zones de temp6ratures tr&s difförentes, la möthode de BrsseL donne de meilleurs
rösultats, car l’erreur probable des observations ainsi que la clöture des triangles sont plus
petites qu’en employant la methode de SCHREIBER; en revanche lorsqu’on fait usage de tours,
la methode de ScureiBer parait bien meilleure.

Les resultats furent les suivants:!

 

Triangles Erreur de elöture Excös sphörique

Resbaladero, Tanquecillos, Buena Vita . . . x... — 1.02 0.8
5 n DomarbBlaneae.ı 22 09 690

s Chapul 5 ee le 1.43

” Buena 2, 20 0.90
Chapul, Nopal, mm... SAN nnaı = 1.61 0.41
” a Deseinar... 2 200. ee) 0.74

„ Gavilan „ . nn... 0.70
ons Blanca, Nopı Cohn 00 0 00. ic 05 08%
Nopal, deinso, Bemusio 0 0.040 weise Se 07 0.37
n n Be Een. eben 0.29

E Reustoy a N 0.30

Gayılan, Gringo None ee 003 0.38

bass ulsansihsisaihhsnnunn

 
