 

 

308

i
ij
i
j

(2) Region (i). Himalaya Mountains.
TABLE I.

Deflections of the Plumb-line.

 

 

 

 

 

 

| Computed deflection in
| the meridian
\ ; Observed
Sresen In une Latitude Longitude Height r defleetion
Himalayas Topographie a
Bose On Hayford’s meridian
witbout } hast
compensation an
a a = =
Birond Et 2a or | 19 45% 2110 metres | =) R- un 7380
Dehra Dun ... 30.197 ma 6) (BO 59 | = 93.9 - 17) 31"
Kurseong ... 20050 8 naar — 10" — 927 — 46”
Lambatach ... 3a le. 70 or Blood =, | 98 ga — 27"
Mussooree ... oo is lol: „2 ent - 30"

Lambatach is in the interior of the Himalayas and 67 kilometres north of Mussooree ;
the other four stations are on the southern border of the range. The deflecetion Dr
decreased between Mussooree and Lambatach—

by. 16. according to topographie caleulations,
by 8” according to Hayford’s hypothesis,
by 8 according to observation.

The following table shows the ratios of computed to observed defleetions:

 

 

 

 

{
DABIEID. 20K |
Ratios of Deflections. j
4
i _ |
Ratio of Ratio of |
Station ‘topographie’ to ‘Hayford’ to
‘observed’ ‘observed’
Birond 11 | 0-4
Dehra Dun le 0°6
Kurseong 1:5 0:5
Lambatach 14 MB
Mussooree 1:8 0:6
Mean BR 1:5 05

 

 

'T'he observed deflections are twice as great as the Hayfordian values.

 
