sea

Sn mern

anti

Treten en meer pen

199
Comptes rendus des scances de la Commission permanente de U’ Association geodesigue internationale
röumie & Geneve du 12 au 18 Septembre 1898. Avec vingt et une cartes et planches.
Berlin (Reimer) 1894. 194 pages in 4°.

Comptes rendus des seunces de la Commission permanente de l’ Association geodesique internationale
reunie & Innsbruck du 5 au 12 Septembre 1894. Avec sept cartes et planches. Berlin
(Reimer) 1895. 255 pages in 4°.

Comptes rendus des seances de la onzieme Conference generale de I’ Association geodesique inter-
nationale et de la Commission permanente reumies a Berlin du 25 Septembre au 12 Octobre 1895.
Ic: Volume: Proces-verbaux. Berlin (Reimer) 1896. 309 pages in 4°.
Ile Volume: Rapports sp6eiaux sur les progres de la Mesure de la terre et rapports
des delegues sur les travaux geodesiques accomplis dans leurs pays.
Berlin (Reimer) 1896. 315 pages in 4° et seize cartes et planches
(Annexe: Rapport sur les Triangulations. 350 pages in 4°).

Comptes rendus des seances de la Commission permamente de U Association geodesigue internationale
reunie & Lausanne du 15 au 21 Octobre 1896. Avec treize cartes et planches. Berlin
(Reimer) 1897. 318 pages in 4°.

Comptes rendus des seances de la douzieme Conference generale de U Association geodesique inter-
nationale reunie & Stuttgart du 3 au 12 Octobre 1898. Avec trente-huit cartes et planches.
Berlin (Reimer) 1899. 582 pages in 4° (Aunexe: Rapport sur les triangulations. Avec
ceing cartes). 454 pages in 4°.

Comptes rendus de l’Association geodesique internationale publies par
le Secretaire perpetuel H. G. van de Sande Bakhuyzen.

Comptes rendus des scances de la treiziöme Conference generale de Association geodesique inter-
nationale reunie & Paris du 25 Septembre au 6 Octobre 1900.
Ier Volume: Procös-verbaux et rapports des delegues sur les travaux geodäsiques
accomplis dans leurs pays. Avec 41 cartes et planches. Berlin (Reimer)
1901. 298 pages in 4°.
Ile Volume: Rapports speciaux et m&moires scientifiques. Avee 5 cartes et planches.
Berlin (Reimer) 1901. 444 pages in 4

Comptes rendus des scances de la quatorzieme Conference generale de V’ Association geodesique
internationale reunie & Copenhague du 4 au 18 Aoüt 1908.
Ier Volume: Proces-verbaux et rapports des delegues sur les travaux geodäsiques
accomplis dans leurs pays. Avec 10 cartes et planches. Berlin (Reimer)
1904. 258 pages in 4%.
