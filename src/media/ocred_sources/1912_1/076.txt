TE TEN

Yl

empfohlen werden und ferner von den nicht hier untergebrachten Instituten, neben der bereits
erwähnten Sternwarte, das Physikalische Staatslaboratorium mit der Hauptstation für Erd-
bebenforschung;, die ebenfalls das Geschenk eines unserer Mitbürger ist, und das Mineralogisch-
Geologische Institut. Auch darf in diesem Zusammenhange die hier als an dem dafür gegebenen
Platze vom Reich errichtete Deutsche Seewarte nicht unerwähnt bleiben. a

Meine Herren! Ich habe Ihnen von Hamburg, seinen wirtschaftlichen und geistigen
Bestrebungen gesprochen. Sie werden gehört haben, dass wir vor wenigen Tagen den Mann
zur letzten Ruhestätte geleitet haben, der als langjähriger Präsident des Senats dem modernen
Hamburg auf so manchen Wegen ein Führer und Schirmer gewesen ist. Tage der Trauer
sind über Hamburg gekommen, und der Senat ist daher zu seinem lebhaften Bedauern jetzt
nicht in der Lage, die Konferenz der Internationalen Erdmessung, wie er es gehofft hatte,
festlich im Rathause zu empfangen. Die Wünsche des Senats aber, die Ihnen unser zu früh
dahingegangener Bürgermeister BURCHARD dort im Rathause ausgesprochen haben würde,
möchte ich Sie bitten nunmehr von mir an dieser Stelle entgegenzunehmen. Der Seuat,
meine Herren, beglückwünscht Sie zu den reichen Ergebnissen Ihrer fünfzigjährigen uner-
müdlichen Tätigkeit und wünscht Ihnen auch für Ihre ferneren Arbeiten und zunächst für
Ihre hiesigen Beratungen den besten Erfolg.

Der Präsident dankt und ersucht nun den Vertreter des Reichskanzlers, Oberregie-
rungsrat Freiherr von Stein aus dem Reichsambt des Innern das Wort zu nehmen. Dieser
sagte foloendes:

Hochverehrter Herr Präsident, meine hochverehrten Herren,

Im Namen und Auftrag des Herrn Reichskanzlers habe ich die Ehre, Sie bei Beginn
Ihrer Tagung zu begrüssen und besonders die so zahlreich erschienenen Herren Vertreter
auswärtiger Regierungen in deutschen Landen willkommen zu heissen.

Die ernste Arbeit, die Sie zu der 17. Allgemeinen Konferenz der Internationalen
Erdmessung zusammenführt, ist von festlichem Glanze umflossen. Fünfzig Jahre sind ver-
gangen, seit in sehr viel kleinerem Kreise und mit zunächst eng: beschränktem Ziel Ihre Ver-
einigung ins Leben getreten ist, die in diesem ersten halben Jahrhundert kräftig um sich
greifend das Ziel weiter und weiter gesteckt hat und jetzt, entsprechend ihrer im eigent-
lichsten Sinne weltumspannenden Aufgabe, die Kulturstaaten der ganzen Welt umfasst.

Eine kurze Spanne Zeit diese fünfzig Jahre für die Erscheinungen, deren Beobachtung
Ihre Arbeit gewidmet ist, aber der in Ihrem Kreise vereinigten Intelligenz aller Nationen
hat sie genügt, Erfolge zu erringen, auf die Sie mit gerechtem Stolze blicken.

Dass Sie diesen Tag des Gedenkens, froh in der Betrachtung des Erreichten, froher
doch, meine ich, im Ausblick auf künftiges Schaffen auf deutscher Erde begehen, dafür
wissen wir Deutsche Ihnen Dank. Dürfen wir doch, die Worte Ihres verehrten Herrn Präsi-
denten haben es bestätigt, darin die feinsinnigste Huldigung für den Mann sehen, dessen
weiten Blick und zähem Wollen Ihre Vereinigung Ihr Entstehen verdankt.
