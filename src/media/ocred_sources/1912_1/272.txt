San

en

tet

iR
1
F
R
£
5
e
F

 

BEILAGE A. VII.

STATISCHER SCHWEREMESSUNGSAPPARAT.

Nebst den mit so grossem Erfolg ausgeführten zahlreichen Schweremessungen mit
sogenannten unveränderlichen Pendeln, haben verschiedene Forscher den Versuch gemacht
für die Bestimmung der Schwere ein anderes Princip anzuwenden, dadurch dass das mit
der Schwere veränderliche Gewicht eines Körpers, gewöhnlich Quecksilber, gemessen wird
durch ein federndes System, dessen Widerstand gegen Formänderungen von der Schwere
unabhängig ist. Wie bekannt hat Sırmens zu diesem Zweck eine federnde Stahlplatte zur
Anwendung. gebracht, während andere Forscher als MAscArT, v. STERNECK, BoUQUET DE LA
GRrYE eine eingeschlossene Luftmasse als Feder benutzten.

Die Vortheile, welche zu erreichen sind, wenn zur Controlle bei den Schweremes-
sungen die Oonstruction eines Apparates nach diesen unabhängigen Prineip gelingen konnte,
hat mich veranlasst zu versuchen diese Aufgabe z zu lösen, und meine diesbezüglichen Be-
trachtungen werde ich kurz mitteilen.

Denkt man sich ein gewöhnliches Quecksilberbarometer so abgeändert, dass das offene
Ende des Barometers abgeschlossen wird, wodurch man eine gewisse Luftmenge über das
Quecksilber absperrt, so wird diese Luftmenge unabhängig von dem Druck der Atmosphäre
mehr oder weniger zusammengedruckt werden, je nachdem die auf den verschiedenen Stellen
der Erde veränderliche Schwere das Gewicht des Quecksilbers vergrössert oder verringert. Die
veränderliche Druckhöhe des Quecksilbers kann demnach dazu dienen die Acceleration der
Schwere eines Ortes zu bestimmen im Verhältniss zu der Schwereacceleration des Ausgangsortes.

Obgleich dieses Prineip so einfach ist, wird doch seine Anwendung mit bedeutenden
Schwierigkeiten verbunden sein. Es kommt ja hier darauf an ganz minimale Grössen zu
messen, wenn man die Schwereverhältnisse auf verschiedenen Orten mit einander vergleichen
will. Dazu kommt der Einfluss der Temperaturänderungen nicht nur des Quecksilbers aber
besonders der eingesperrten Luft. So würde eine Änderung der Schwerebeschleunigung von
0,1 mm, d.h. eine relative Änderung von etwa "/ooooo eine Änderung in der Temperatur
von nur 0,00273° Q. entsprechen. Da nun die Acceleration der Schwere nach den Pendel-
messungen gewöhnlich in Hunderdtel von Millimetern angegeben wird, würde der gedachte
Apparat die Temperaturbestimmung viel schärfer erheischen, als man sie mit den gewöhn-
lichen T'hermometern erreichen kann, wenn man auf diesem Wege Resultate erhalten wollte,
welche mit den Resultaten der Pendelmessungen vergleichbar sind.

 

 
