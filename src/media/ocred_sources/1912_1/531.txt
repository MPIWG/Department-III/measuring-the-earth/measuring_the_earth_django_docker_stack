x mr

oa

TERN RT

Er RRITHURg 1

 

Anzahl
Entfernung | der ver-

| | Anschlussdifferenzen
Log. Vagnarberg— |

.
|
fi Stelle in mm | :
in | . S
Dragonkullen ul | [Er nakm bindenden
|
l

Grundlinien

| | des Log. | für I km | | Dreiecke

 

| |
ae | 4,072 3985 | 0 mn | 30
Amager ( Kopenhagen) | 3376 | — 1 | ei 2l | 400 4.3
Göteborg. 2.2... | 3207 | #28 [ea a 1... | 780
Wänernsaa Heise | 3247 a es 160 2
Egeberg (ne 3845 a N RE
LOTABWEICHUNGEN,

Länge Breite | Azimut | Richtung Lotabweich.

 

Geod.

 

|

Astr. | Geod. | Astr. | Geod. | Astr. Ss Ten

 

]

| ° 1; u | “u °© ’ u
Kopenhagen. . | 12 34 57,68 | 57,68 55 40 43,26

 

43,26 | 106 33'33/39

 

 

       

    

 

 

nn |

33,389 | Malmö .. | 0,00 E —

Himmelskullen |12 39 33,45 | | 56 53 25,40 | 25,97 18, | ak oncheien | + 1,90 | 13°
| AT. | | | |

Göteborg. ..... | 1157 49,44 | 49.77 Ior 42 32,01 | 33,93 | 280 3447,11 | 54,98 | | | Hisingeberg . | + 5,39 | 69
\ ’ | | |

Marstrand. ... 11134 26,52 | | 57 5318,33 | 19,17 | 349 56 10,97 | 16,40 | Boxviks Sadel | 3 + 3,51 | 76

|

Dragonkullen . |1118 14,47 | 159 5:58,76 |55,59 |231 2017,14 |29,10 | Vagnarberg N | -H 7,82 | 114

Kopenhagen (Nicolai III) bildet der Aufangspunkt für die Berechnung der geodätischen
Koordinaten. Man hat gerade diesen Punkt gewählt, weil dadurch eine direkte wichtige
Verbindung mit den Arbeiten der internationalen Erdmessung für die Ableitung von Lot-
abweichungen gewonnen wird. Nach „A. Börsch, Lotabweichungen Heft III” sind für den
astronomischen Punkt Kopenhagen vorläufige Werte von Breite und Länge relativ gegen
den Lapzaozschen Punkt Rauenberg bei Berlin abgeleitet nämlich:

Breite 55 40.43.26
Länge 12 34 57,68 von Greenwich.

Eine direkte astronomische Azimutbestimmung ist nicht vorhanden aber nach Börsch

hat man das Azimut für die Richtung Kopenhagen—Falsterbo . . . = 155° 34' 25,77
und

nach „Anpras, Danske Gradmaaling” den Winkel Malmö — Kopenhagen —

Balsterno ua en ne Da

also das Azimut für die Richtung Kopenhagen— Malmö. . . .

49 052 ‚38
106 33 33 ‚39.

|

|

Die zwei verschiedenen Zahlen für die Länge von Göteborg sind aus Angaben in
* 51
