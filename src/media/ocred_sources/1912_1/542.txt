 

ANNEXE A, XXXIX,

ESPAGNE.

Rapport pr6sente a la 17° Conförence g&eodösique internationale

PAR

M E. MIER.

Avee une carte.

I. TRAVAUX ASTRONOMIQUES.
a. Diff&rences de longitude.

La station sismologique centrale d’Espagne ayant 6t& installee & Toledo, on a cru
que pour ce motif il &tait dösirable de determiner avec pröcision ses coordonnees geogra-
phiques et dans ce but les Ingönieurs G&ographes MM. Mirsur et GALBIs ont procede aux
travaux nöcessaires & la dötermination de la difference de longitude entre cette ville et
Madrid. Les caleuls correspondants ne sont pas encore termines.

Le caleul de la difference de longitude entre Madrid (Observatoirc) et Barcelona (Pare),
dsterminde par MM. les Ingenieurs G&ographes Borrfis et EsreBan, a donne le resultat suivant:

23m 258. 456 + 08.010.

db. Latitudes.

Les latitudes des stations du röseau geodesique de premier ordre observees par
M. l’Ingönieur Azerazu, et calculdes depuis la derniere Oonference geodösique internationale
sont indiquses ci-apres.

 

H
i
i
E
i
1
i

 
