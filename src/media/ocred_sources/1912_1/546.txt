 

426
Les reperes mötalliques qui ont &t& &tablis sont au nombre de 29 sur la ligne de
Medina del Campo & Zamora; 62 sur la ligne de Palencia ä& Santander; 19 sur la ligne
de Castellön & Tarragona et 62 sur la ligne de Tarragona & Barcelona.
Ces travaux sont, pour la plupart, des r&petitions de lignes anterieurement nivel6es,
et par consöquent le r&seau espagnol des nivellements de pr&eision a eu seulement une

augmentation totale de 434 Km. ce qui lui donne ä present une longueur de 13.960 Km.
avec 15.418 reperes, dont 1949 sont en bronze.

 
