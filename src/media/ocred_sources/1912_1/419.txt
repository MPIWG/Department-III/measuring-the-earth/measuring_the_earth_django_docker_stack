 

mn nt

ee

al ael

we pen

LTR II TERRA

ACCELERATION DE LA PESANTEUR.

Je suis heureux d’annoncer qu’apres avoir vaincu les dificultes qui se prösentörent
dans ce genre de trayaux, nous venons de faire avec un succes complet les observations
des pendules americains A,, A,, A, & Tacubaya, pendules prealablement observes par moi-
meme & Washington et que je dois observer de nouveau afın de fermer dans cetie m&me ville
le eireuit Washington-Tacubaya. Je dois dire iei que les trayaux de la Oommission geodesique
mexicaine avec les pendules americains employes & Tacubaya ont &t& facilitös par M, Tırımann,
ä& qui nous sommes tr&s reconnaissants; & Tacubaya on a fait 15 determinations de la valeur
de la duree d’une oscillation avec chaque pendule, et cette valeur comparee & celle obtenue
ä& Washington en acceptant pour ce dernier lieu la valeur suivante de y

g = 9.801128,

donne pour Tacubaya
Gm Bihsa0=a VE:

Nous avons le plaisir d’annoncer 1° qu’afin de connaitre y avec la plus grande pre-
eision possible nous ferons prochainement encore d’autres observations, en employant les
pendules Srückrara dont les constantes ont &te de nouveau determindes & Washington par
moi-möme, et 2° que nous avons adopte pour les trayaux de campagme le systeme de pendule
type „U. 8. Coast and Geodetie Survey”, appareil qui nous sera remis prochainement par
la maison Berger and Son; de nouvelles observations de l’acceleration seront faites avec les
nouveaux pendules ä Washington et ä& Tacubaya et nous domnerons ainsi un grand poids &
la valeur trouvee pour Tacubaya; nous pourrons par consequent considerer notre point de
depart comme station de premier ordre.

A, Lkyva.
