 

366
entsprochen und sind in der Berichtsperiode nachstehende Längenunterschiede gemessen

worden:
Wien (Universitätssternwarte)—Sträzahalom,

Sträzahalom—Ozernowitz,
Wien—Zavinohradi,
Zavinohradi—Sträzahalom,
Sträzahalom—Szatmär N&meti (Domkirche),
Szatmär Nömeti—Üzernowitz.

Sowohl das für die Zeitbestimmung als für den Signalwechsel verwendete Instrumen-
tarium war neuester Konstruktion, sowie auch beim Beobachtungsvorgang alle vom Geheimrat
Dr. Atsrechr in seinen verschiedenen Publikationen angegebenen Direktiven eingehalten
wurden.

Jede der genannten Linien ergab für sich befriedigende Resultate und bleibt der
mittlere Fehler jemals unter 05.02. Ungeachtet dessen ergeben die beiden Dreiecke, welche
aus den genannten Längenunterschiedmessungen gebildet werden einen grösseren Widerspruch
als die Summe der mittleren Fehler der einzelnen Linien.

Aus dieser Ursache konnte zur Veröffentlichung dieser Messungen nicht geschritten
werden und wurde 1912 noch ‘die Linie Wien—Üzernowitz neu gemessen, wodurch sich
eine weitere Kontrolle ergibt und werden im Jahre 1913 die Messungen jener Linien wieder-
holt, bei welchem sich etwa erkennen lässt, dass selbe in das genannte Längenunterschieds-
netz nicht passen.

Erwähnt sei, dass die angeführten Längenunterschiedmessungen insoferne mit Schwierig-
keiten zu kämpfen hatten, als die Stationen einzelner Linien in verschiedenen Wetterbezirken
installiert waren und die Messungen unerwünschterweise längere Zeit in Anspruch nahmen,
anderseits die oft beträchtlich langen telegraphischen Verbindungen bedeutende Gebirge und
Waldgebiete durchquerten und sich hiedurch häufiger Leitungsstörungen ergaben.

B. TIRIGONOMETRISCHE ARBEITEN.

Eigentliche Triangulierungen für die Zwecke der Gradmessung wurden nicht aus-
geführt, sondern nur die bestandene Lücke im Netze 1. Ordnung in Südsteiermark ausgefüllt
und bei diesem Anlasse ausserdem ein Teil des Netzes 1. Ordnung in Kroatien, welches eine
etwas ungünstige Form aufwies, durch Einlegung neuer Punkte umgestaltet.

Über Basismessarbeiten wäre an dem in der 16. allgemeinen Konferenz vorgelegten
Berichte anknüpfend noch mitzuteilen, dass, anlässlich der im Jahre 1908 vorgenommenen
Kontrollmessung des südlichen Basisdritiels, der im Jahre 1857 bei Wiener Neustadt ge-
messenen Grundlinie, auf Grund der nunmehr vorliegenden definitiven Reduktion eine vollends
befriedigende Übereinstimmung der Messungen aus den genannten beiden Jahren sich ergibt
und demnach der Schluss gezogen werden kann, dass sowohl die Stangen des Basisapparates
keinerlei Veränderung und ebenso die Endpunkte der gemessenen Strecke keine Verschiebung

erfuhren.

 

3
i
i
!
i

 
