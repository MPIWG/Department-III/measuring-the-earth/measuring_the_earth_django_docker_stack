 

274
des chronomeötres ‚ötaient dötermines de jour, par l’observation du soleil au theodolite; de
nuit, & l’aide des observations d’stoiles faites A l’astrolabe & prisme.

Les resultats obtenus sont r&sumes dans le tableau ‚suivant:

 

 

LATITUDE AZIMUT
ö DT m mn | mn Zara
STATIONS | La-L;  |(Aa-A,)Cotgo
ASTRONOMIQUE G EODESIQUE ASTRONOMIQUE GEODESIQUE
————l 11000
; G u G u Bee ER 1 u
Coivrel". . . . . | 55.0585,41 | 55,0589,56 | 150,4333,4 | 150,43199 | — 415 4941 45
Lihons,’ 2,°. 2.0 55,8690,59° | 55,3684,56 51,2674,0% | 51.168582 7 ı 008 ze
Nur. ......... | 55,5608,80 | 55,5597,38 | 230,2121,1 | 230,21949 | + 11,48 as.19
Chasseriooutt . . . | 53,9135,49 | 53,9132,66 | 330,8307,9 | 330,83080 | + 2,83 0.09
Cötes-Noires, . . . | 54,0250,51 | 54,0252,91 | 112,4980,7 | 112,498,0 | + 6,0 u
Longeville . . . . | 54,1507,48 | 54,1495,85 46,1907,8 | 46,1858,1 11.63 + 43,59

 

 

B) Differences de longitude.

1° Brest-Dakar par le cäble sous-marin. — Pour donner une base seientifigue aux
travaux geographiques importants entrepris dans l’Afrique Oceidentale frangaise, il &tait
necessaire de fixer d’une fagon döfinitive les eoordonnees des points de d&part en determinant
une difference de longitude entre une station astronomique fondamentale de France et un
point convenablement choisi de l’Afrique Occidentale francaise qui deviendrait ainsi la station
astronomique fondamentale de la colonie. .

Le programme &tudi& comportait une difförence de longitude entre Brest et Dakar
par le cäble sous-marin et une difförence de longitude entre Paris et Dakar par la telegraphie
sans fil; mais les degäts occasionnds par les inondations de 1910 rendirent inutilisable pour
plusieurs mois la station radiotelögraphique de la Tour Eiffel, et seule put ätre entreprise
& Y’aide du cäble, la difference de longitude entre Brest et Dakar.

Le ÜOhef de la Section de G6odösie, qui avait la direction des op6rations, fit faire,
tant & Paris qu’ä Brest, des exp6riences qui montrörent, ainsi qu’on le prövoyait du reste,
l’impossibilit6 de se servir du chronographe ordinaire, en raison surtout de la faible intensite
du courant transmis par le cäAble. On deeida d’employer comme r6cepteur aux deux stations
le siphon-recorder de Sir W. Tmompsox. L’adoption de cet appareil et les ötudes faites en
collaboration avec M. l’Ingenieur en Chef des t6lögraphes Dsvaux-CHARBONNEL, qui voulut
bien nous faire beneficier de sa grande experience, conduisirent, pour les changes de signaux
de comparaison, & l’emploi d’une methode speciale qui a fait l’objet d’une communication
ä l’Acadömie des Sciences (s6ance du 26 septembre 1910, t. 151, n® 13, p- 583—587).

A Brest, la station astronomique comprenait: 1° une horloge astronomique fonda-
mentale de temps sideral installde dans une vieille tour du Chäteau; 2° une horloge astro-
nomique auxiliaire de temps sid6ral placde dans la salle du siphon-recorder au Bureau central
des Postes et Telegraphes et compar&e & l’horloge fondamentale avant et apr&s les Echanges

 

ENT TEE

u tssnänammeh an au hrs hen nn

|
!
|

 
