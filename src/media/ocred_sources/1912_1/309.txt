 

 

IT gRT

 

=
x
B

ar

minations de latitudes secondaires eflectuses en Equateur et au P6rou, les details relatifs
aux theodolites, montres et instruments möt6orologiques employ£s, enfin l’expos& des möthodes
dobservation et de caleul ayant servi dans la d&termination des latitudes au thöodolite, par
observations eircummeridiennes d’etoiles ou du soleil.

4° Tome VI: Zihnographie aneienne, par MM. Vernkau et Rıvar.

Ce fascieule, pret & paraitre, renferme une importante contribution & l’&tude des
populations anciennes de l’Equateur et est illustr6 d’une quarantaine de planches en photo-
typie du plus haut inter&t au point de vue de leur ethnographie.

On voit, par les quelques details qui precädent, que toute l’ampleur possible sera
donnee & cette publication, que les discussions de toute nature feront l’objet de d&veloppe-
ments etendus, de maniere ä degager nettement les enseignements & retirer des trayaux de

la Mission et & donner une idee exacte de la portee de leurs rösultats.

Colonel R. Bourgsois,
Chef de la Mission de V’Equateur.
