 

454

(fEODETIC PUBLICATIONS BY THE UNITED STATES COAST AND GEODETIC SURVEY SINCE
Janvary 1, 1909.

1. Preeise Leveling in the United States, 1903—1907, with a Readjustment of the
Level Net and Resulting Elevations, by Jonn F. Hayrorn, and L. Pırz; 1909.

2. The Figure of the Earth and Isostasy from Measurements in the United States,
by Jonn F. Hayrorn; 1909. &

3. Geodetic Operations in the United States 1906—1909. A report to the Sixteenth
General Conference of the International Geodetic Association, by O. H. Tırımann and JoHn
F, Hayrorn; 1909.

4. Supplementary Investigation in 1909 of the Figure of the Earth and Isostasy,
by Jons F. Hayrorp; 1910.

5. Revised Rdition of the Tables for a Polyconie Projection of Maps; Special Publi-
eatton N. 9. 1.90,

6. Primary Base Lines at Stanton, Tex., and Deming, N. Mex., by Wırzıam Bowir;
Appendix 4, Report for 1910.

7. Triangulation in California, Part Il, by Omas. R, Duvanı and A. L. Banpwin;
Appendix 5, Report for 1910.

8, The Measurement of the Flexure of Pendulum Supports with the Interferometer,
by W. H. Burger; Appendix 6, Report for 1910.

9. Enlarged Edition of the Formul® and Tables for the Oomputation of Geodetie
Positions (with a preface by Caas. R. Duvanı); Special Publication N®. 8; 1911.

10. The Effect of Topography and Isostatic Compensation upon the Intensity of
Gravity, by Joaw F. Hayrorn and Wıruıam Bowir; Special Publication, IN IOERILO 2.

11. Triangulation along the Ninety-eigth Meridian, Nebraska to Canada, and Oon-
nection with the Great Lakes, by Wıruram Bowiz; Appendix 4, Report for 1911.

12. Triangulation along the Ninety-eighth Meridian, Seguin to Point Isabel, Tex.,
by A. L. Baupwin; Appendix 5, Report for 1911.

13. Triangulation along the East Coast of Florida and on the Florida Keys, by
Hvucu O©. Mıronert; Appendix 6, Report for 1911.

14. The Texas-Öalifornia Arc of Primary Triangulation, by Wınzıam Bowiz; Special
Pablteation, N E19,

15. The Hffect of Topography and Isostatice Compensation upon the Intensity of
gravity, Second Paper, by Wırnıam Bowır; Special Publication N°®. 12; 1912.

Hereafter the Reports of the Superintendent of the United States Coast and Geodetic
Survey will be purely administrative in character. The results of the work of the survey
and scientifie papers will be published as separates.

O. H. Tırrmann.

 

 
