m lustiger

{Et 7

TmrwrT

PETER TTTRBTRIT TTT

BEILAGE A, IX.

PREUSSEN.

KÖNIGLICHES GEODÄTISCHES INSTITUT IN POTSDAM.

Bericht über die Arbeiten 1910-12.

Das Geodätische Institut führte ausser den in dem Tätigkeitsberichte des Zentral-
bureaus der Internationalen Erdmessung genannten internationalen Arbeiten noch die nach-
stehend verzeichneten praktischen Arbeiten im Landesgebiete und wissenschaftlichen Unter-
suchungen aus.

1. Um den Gang der Lotabweichungen im Gebiete der Längenbestimmung in 52° Br.
weiter zu verfolgen, wurden die 4 Längenbestimmungen Gotha— Knüll— Erndtebrück—Bonn—
Düsseldorf, sowie die Breitenbestimmung von Erndtebrück ausgeführt.

2. Die rechnerische Bestimmung des Geoids im Harzgebiete wurde fortgesetzt und
nahezu zu Ende geführt.

3. Das Netz der Pendelstationen wurde durch 26 Stationen erweitert. Davon liegen
sechs in einer magnetisch stark gestörten Gegend in West- u. Östpreussen; ein Zusammen-
hang der magnetischen und der Schweren-Störungen ist nicht zu erkennen. Die anderen
20 Stationen ergänzen die Reihe der Pendelmessungen in der Nähe des Meridians von
9° östl. Gr., die nun (das Ausland inbegriffen) von Jütland bis Genua ununterbrochen reicht,

4. Der Pegeldienst an den 9 Ostseestationen und einer Nordseestation wurde fort-
gesetzt. Bei der jährlichen Revision wurden an einigen Pegeln die Kontroll-Nivellements
etwas tiefer ins Innere des Landes ausgedehnt.

5. Über die Deformation des Erdkörpers durch die Anziehung von Mond und Sonne
auf Grund der Beobachtungen in der 25 m tiefgelegenen Brunnenkammer zu Potsdam hat
Herr Geh. Reg. Rat Prof. Dr. Hzcker die nachstehend unter Nr. 9 genannte Veröffentlichung
erscheinen lassen.

Vergl. auch die Veröffentlichung von W. ScHWEYDAR unter Nr. 12.

In Freiburg in Sachsen hat Herr Hscker im Mai 1910 ein neues Horizontal-
pendel nach ZöLunerschen Prinzip für das Institut aufgestellt woran seitdem fortlaufend
beobachtet wird.

.6. Der seismische Dienst wurde fortgesetzt.

 
