AATTTTRRREEETRTE TEURER N

Sn HF

ir

gear

ae

TTRTRRTTARR TI FIRERTN

 

318

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

|
* Stations situated on the east coast

 

of India.

TABLE X.
Northern portion of the Peninsula.
| | Computed deflection in
| | meridian Observed
Station Latitude | Longitude | Height oe week detlechion
| in meridian
| Topographie ‘ Hayford ’
Bithnok 25 2 235 metres + 6" +1" + 8°
Chendwar DD 852 297 345 „ — 11” 0 Tl
Daiadhari 243 D608 + 2" | 0 + 5
Gurwani SA 89590, Dan ol +1" el
Hurilaong el Sr nee _ 4" ol 15%
Kankra Rn 25 38 8 10 ı 498 =. —ıl +4"
Khankaria ... SA 3T, 00567 1108, 5% 0 + 6"
Kesri DA 0A 446 „, + 2" +1” + 10”
Pahargarh 24° 56’ UT 24% AD a +1” + 4”
Rewat 26 54° Az) a0 05 + 4" 0 + 6"
Saugor 232507 187 492 Ole + 4" +1" 4"
Thob 963; 795 Do 0 1 3”
TABLE XI.
Belt of negative deflections across the central portion of the Peninsula.
Computed deflection in
meridian Observed
Station Latitude Longitude Height IE |. deflection
| in -meridian
Topographie  ‘Hayford’
Badgaon 20° 242 0% 338 metres ee 0 5.
Bolarum 90) 18 34% I - 6" 1 -— 5"
Chaniana DA 239, 230 - 4" | 3% 8
Colaba 18° 54° 12 Sl Da - 4" 0 — 9"
Quttack 200297 85° 54 40: „ 93% Bu ke
Deesa DAS 4 DA, ls - 6" ie —ıG
Kem IE NSS OE Dr - 8 0 _ 2"
Khanpisura... 18° 46° 74° 4% O2 5% u 7"
Ladi 23° 9! 11 45 563° „ + 1” 1% ey
Valvadi 20° 44 75° 14 9080, + 8° % — 4"
Lingmara DAS, SOSE DO - 6° ir on
Voi IE a Du 432 2° IE a
Mal 18° 47° 84° 33° 146. an = 67 — 9"
Mandvi 18, 38 122853 Da6 5 I mM
Nialamari le2. 79° 46° 343, = 10% | — |” Sl
Nitali Re mr on 0 0 DD
Parampudi ... if 18 81.15 297. 0: , — 16" H - 5"
Rawal 187322 83,86. 260. — 24% =9 a
Sanjıb ar al 82244 | 643 — 34'* 194 _ 9%
Vanakonda ... 1790, MI 297 499: ,; gen lH ou
Vizagapatam Kell 8310% DI eo he Di
Waltair | 17° 43° 83722 | 60, 30 u - 8

 
