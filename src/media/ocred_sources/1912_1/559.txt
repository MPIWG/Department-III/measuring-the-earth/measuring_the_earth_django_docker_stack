 

436

ihrer Längsrichtung parallel und normal verlaufen, und entsprachen auch bezüglich der
Grösse der Krümmungsstörungen den Forderungen theoretischer Berechnung.

Zur näheren Erklärung dieser Verhältnisse müssen wir die Grösse: Z zu Rate ziehen.
Bezeichnen wir die Koordinaten eines Punktes längs und normal der Talrichtung mit Z und z,
die ebenso gerichteten Hauptkrümmungsradien mit p, und p,, so wird:

r eu 20 ( 1 1 )
(| ae
\ aan An

wo für g der Wert der Beschleunigung in der Breite von 46° 37’ und einer Höhe von

1500 Metern zu setzen ist, also:

0 @0.3.

Der grösste Wert von Z, den wir am Südrande des Tales, dicht am Lago bianco fanden,
war nun: A = 1487.10? 0.G.8., der kleinste in seinem mittleren Teile: 2 — 734.10
0.G, 5. Dementsprechend erhalten wir am Südrande des Tales:

1 1
EI
pı pt
und in der Talmitte:
1 eds
— — — —.0,7487.10-?
pı pt

Nehmen wir nun, wie oben, an, dass der Krümmungsradius a in der Längsrichtung
des Tales wenig von seinem normalen Werte verschieden sei und setzen dem Orte und
Azimute entsprechend:

1

2,910
pı

so ergibt sich am Südrande des Tales:
— —=0,0495.107 also = 206685.10°e

und in der Talmitte:

—= 0,8177.10- also, — 112262.10%e
t

Dieser Krümmungsradius quer zum Tale erreicht somit am Rande den dreissigfachen
und in der Mitte noch nahezu den zweifachen Wert seiner normalen Grösse.

Weitere Rigentümlichkeiten dieses so stark verbogenen Stückes der Niveaufläche sollen
gelegentlich der vollständigen Veröffentlichung der Beobachtungen besprochen werden.

a

ikhsnsuairnnsha

 
