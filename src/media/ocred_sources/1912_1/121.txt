 

116

Mit den bis 1909 geleisteten Arbeiten zusammen, verfügt die Internationale Erd-
messung im Jahre 1912 über ein Beobachtungsmaterial von rund 3120 Schwerkraftswerten,
die sich auf etwa 2780 getrennte Punkte der Erdoberfläche verteilen, freilich noch in sehr
ungleichmässiger Dichte. Der jährliche Zuwachs an neuen Stationen beträgt in der letzten
Berichtsperiode etwa 130 Stationen und ist etwas grösser als in den beiden vorangehenden
Perioden.

Die mitgeteilten Zahlen bekunden am besten das rege Interesse, das den Schwere-
messungen andauernd entgegengebracht wird.

Nicht minder erfreulich als der quantitative Fortschritt der Arbeiten ist auch der
qualitative. Die Genauigkeit der Messungen ist durch Erfahrung und Verbesserung der
Apparate allmählig so gesteigert worden, dass man heute im Stande ist, den Schwereunter-
schied zweier Stationen auf nahezu 0,001 cm/sek* genau zu ermitteln.

Meine Herren, ein Blick auf die dem Berichte 1909 beigegebene Übersichtskarte
über die Verteilung der Schwerestationen auf der Erdoberfläche überzeugt uns, dass auf dem
Gebiete der Schweremessung noch ein gewaltiges Arbeitsquantum vor uns liegt; aber es
sind andrerseits auch viele ausgezeichnete Kräfte tätig, das von Sierneek so glücklich begonnene
Werk fortzusetzen und unsre Kenntnisse vom Aufbau der Erdrinde immer mehr zu erweitern
und zu vertiefen.

Der Präsident dankt Herrn Borrass, und erteilt Herrn General Madsen das Wort, der
erst einen Bericht über einen photographischen Registrierapparat für Schweremessungen
(Beilage A. VI) und nachher einen zweiten Bericht über einen statischen Schweremessungs-
apparat mittels einer die Spannung der Luft messenden Quecksilbersäule (Beilage A. VII) vorlegt.

Herr Helmert macht einige Bemerkungen und äussert die Meinung, dass es jetzt
schwer ist sich über den Wert dieser Instrumente auszusprechen, da man erst nach einigen
Versuchsreihen bestimmen kann, in welcher Weise sie zu benutzen sind.

Der Präsident erteilt Herrn Hecker das Wort, der einige Mitteilungen über die
durch die Assoziation für Hrdbebenkunde mit dem Horizontalpendel in 4 Stationen anzu-
stellenden Beobachtungen macht; eine dieser Stationen wird in Nord-Amerika, eine zweite
im Innern des russischen Reiches, eine dritte in Paris und eine vierte in der südlichen
Hemisphäre errichtet werden (Beilage B. XI).

Herr Helmert bemerkt, dass in der letzten Konferenz ein Betrag von 6000 Mark für
Untersuchungen über die durch die Sonne und den Mond hervorgerufenen Deformationen
bewilligt ist, wovon bis jetzt nur 3000 Mark verwendet sind; es bleibt also noch der
nämliche Betrag, der in der nächsten Periode zu demselben Zweck verwendet werden kann.

Der Präsident erteilt Herrn Schweydar das Wort, der seinen Bericht über die Defor-
mation der Erde unter dem Einfluss von Sonne und Mond vorlegt (Beilage B. XII).

Er teilt daraus folgendes mit:

Dr. Schweydar berichtet über die Resultate, die in seinen sol über die
Gezeiten der festen Erde und die hypothetische Magmaschicht’”’ ausführlich begründet sind.

Er zeigt u. a,, dass das Horizontalpendel dem Monde vorauslaufen muss, wenn die
Deformation gegen den Mond zurückbleibt, und folgert aus den beobachteten Phasen, dass

 

 

 

ua

1 mh nu bias hbelsu nusan
