 

140

der geographischen Breiten für einen gewissen Normalzustand oder mittleren Zustand
der Erde niemals möglich sein wird. Hier wie in den anderen Zweigen der Erdmessung
hat man eben mit zeitlichen Veränderungen des Erdkörpers zu rechnen, die mit der
Zunahme der Genauigkeit der Beobachtungskunst immer mehr hervortreten.

Zu den drei großen Hauptgruppen der wissenschaftlichen Betätigung des Zentral-
hureaus: Lotabweichungen, Schweremessungen und Breitendienst, gesellen sich noch zahl-
reiche verschiedene Arbeiten kleineren Umfanges. Als vor etwa 25 Jahren Rersoıo
sein unpersönliches Mikrometer konstruierte, hat das Zentralbureau sofort damit Versuche
angestellt (1891) und dasselbe als eine wichtige Errungenschaft erkannt. Seitdem wurden
zahlreiche astronomische Längenbestimmungen in verschiedenen Staaten damit ausgeführt.
Das Geodätische Institut und Zentralbureau ermittelte unter anderem die internationalen
Längenunterschiede Potsdam — Greenwich, Potsdam — Pulkowo, Potsdam — Bukarest auf
diese Art.

Wurde so der persönliche Fehler bei Längenbestimmungen fast ganz ausge-
schaltet, so wurde andererseits erkannt, daß auch Breiten- und Azimutbestimmungen
mit Aug- und Öhrbeobachtungen oftmals durch gewisse persönliche Einflüsse entstellt
werden (Löw).

Eine Ausgleichung des astronomischen Längennetzes für einen großen Teil von
Europa bewirkte Arseeenr im Jahre 1904, um die neuen Bestimmungen mit dem unper-
sönlichen Mikrometer auszunutzen.

An der Lösung der Frage nach der Wahl eines Höhennullpunktes für Zentral-

europa beteiligte sich das Zentralbureau hervorragend durch die Ausgleichung eines
Nivellementsnetzes zwischen Nordsee und Mittelmeer, wobei die Einflüsse der Schwer-
kraft nach Möglichkeit berücksichtigt wurden (Kömsex und A. Börsen).
Eine dem Zentralbureau übertragene Untersuchung über den Einfluß der Feuch-
tigkeit der Luft auf hölzerne Nivellierlatten konnte nach Inangriffnahme bald wieder
abgebrochen werden, nachdem Herr Ci. Laruemanv die umfangreichen Arbeiten des
Obersten Govuusr über diese Materie publiziert hatte; er zeigte aber auch, daß die kleine
neue Beobachtungsreihe mit den Govnierschen Ergebnissen in Einklang stand.

Nicht gering ist die Tätigkeit des Zentralbureaus in der Prüfung ihm übersandter
Apparate. Außer den erwähnten Pendelapparaten sind verschiedene astronomische
Apparate und Uhren eingehend untersucht worden.

Um einem zu erwartenden Wunsche zu genügen, hat das Geodätische Institut
und Zentralbureau seine 240m lange Hilfsbasis mehrmals mit seinem Bruxssee-Apparat
gemessen und auch ihre Länge durch eine große Anzahl Invardrähte bestimmt. Es hat
auch die Dänische Gradmessungskommission in letzter Zeit die Hilfsbasis behufs Draht-
eichung nachgemessen.

Das Geodätische Institut und Zentralbureau ist im Laufe der Jahre, seit es ein
Dienstgebäude auf dem Telegraphenberge bei Potsdam bezogen hat, wiederholt von Fach-

12

 

ä
=
®
i

in!

nun bb na hand

1
i
|
1

 
