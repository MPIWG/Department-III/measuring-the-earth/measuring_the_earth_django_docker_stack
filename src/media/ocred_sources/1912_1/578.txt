 

452

Recognition of an anomalous refraction of this kind leads to a rule which might be
advantageously adopted by meridian eircle and latitude observers. For declination measures
several bisections of the star should be made, separated by as great intervals of time as
practicable; for right ascensions, the thread system should bs widely spaced.

Previous experience with zenith instruments for measuring small zenith distances
has disclosed anomalies of scale value corresponding to abrupt and unexplained changes in
focal length. In order to test the stability of the scale value of the photographie zenith
tube at Gaithersburg, three groups of stars have been selected, each group containing about
18 stars, scattered over several hours in right ascension. When a complete group has been
observed, the scale value is determinable for that night by a least square solution. Up to
the present time a complete series of observations on only one group has been obtained.
The results are given below; » is the arc value of one revolution of the comparator screw:

 

 

 

Date. Temperature. n v
|
1912 = i
nl 93 19.9553 + .0005
ee —.0.6 9529 + .0039
NE ER + 3.5 9563 — .0005
DR ae — 4.3 | 9577 — .0019
Nelpe di soooees — 5.4 9557 + .0001
bene. — 5.2 9568 — .0010
Means. ... — 29 Ip ee.

 

 

The declinalions of the stars, which for the determination of » should be known
with preeision, were determined by Prof. R. H. Tucker with the meridian circle of the
Lick Observatory, through the courtesy of Direetor W. W. ÖAMPBELL.

The meteorological conditions on the nights when the scale value has been determined
were unusually bad. Notwithstanding this, the agreement from night to night is seen to
be remarkably close and far better than corresponding determinations with visual instru-
ments. We can safely conelude that there are no abnormal changes of scale value apparent.

From a comparison of these results with some values secured during the previous
summer there does not seem to be any appreciable variation of scale value with temperature.
In order to obtain the temperature effect independently, samples of the focusing rod and
plotographic plates were forwarded to the United States Bureau of Standards for determi-
nation of their thermal coeflicients. The temperature coeflicient of the EıızaBerH THomrsoN
Comparator has been obtained in the office of the United States Coast and Geodetic Survey
by measuring plates in rooms at temperatures of 0° and 40°, respectively.

There is a elass of systematie error usually present in instruments for observing

 

 
