 

208

Zustände der Atmosphäre haben sich bei dieser Untersuchung gezeigt. Auf 52 Kimmtiefen
musste ich, um die Formel abzuleiten, 5 abweichende Unterschiede B— R ausschliessen,
um die Anzahl der positiven und negaliven Unterschiede nahe gleich zu machen. Bei dem
Ableiten der mittleren Fehler darf aber dieses Ausschliessen nicht statt haben, und so finde
ich, wenn ich die sämmtlichen Höhenbestimmungen aus Kimmtiefen in 4 Gruppen ver-

theile :

Für eine Höhe von 75 Meter einen m. FE.

nn > 50 »
oo) alle
Dal) » » 600 »

also überhaupt 1,5 bis 2 Procent.

Die abweichenden Unterschiede B— R befanden sich ausschliesslich in der dritten

»

»

»

»

D)

»

und vierten Gruppe schliesst man sie aus, so hat man:

Für eine Höhe von 300 Meter —
» » » » 600

Hat man nur eine einzige Kimmtiefe beobachtet, so wäre es mehr angezeigt den
grösseren Werth für den zu befürchtenden mittleren Fehler anzunehmen; hat man aber
mehrere beobachtet, und stimmen die abgeleiteten Höhen gut überein, so könnte man an-
nehmen, dass keine ganz anormalen Zustände stattgefunden haben, und also den kleineren

Werth annehmen.

Utrecht, im October 1891.

»

a I

H

 

Toy 4
1 3 M. oder ERS;
58
2% 1
9,4 » » u)
Ai
AU DD. —,
iO
- al
sm INT,

a

 

 

(6) NW a

ode m,
1

4,52 2 Z-

J. A. C. OUDEMANS.

dl uk

ru 1 a TUT TE MA |

lkınndauaul

 

 

 
