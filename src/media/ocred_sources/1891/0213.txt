 

MR

i

ill)

I FE RRIRRR DL AMINAI ALLE Pill Lin

im

 

 

Höhennetzes gestaltete sich demnach zu einer umfangreichen Arbeit, welche Herr Oberst
M. L. J. Van Asperen, nach Rücksprache mit mir, mit grosser Ausdauer und Tact vol-
lendet hat.

Wiederholt war auf Java zwischen den Ingenieuren und mir die Zweckmässigkeit
einer absichtlichen Bestimmung des für Java an verschiedenen Stunden des Tages geltenden
Refractions-Factors! (k oder —k), mittels gleichzeitiger gegenseitiger Zenithwinkel bespro-
chen worden; wegen des dringenden Bedürfnisses von Resultaten für die Aufnahme wurde
diese Bestimmung aber immer auf später verschoben. Eine einzelne Bestimmung zwischen
zwei Punkten des Basisnetzes in West-Java und eine andere zwischen zwei Punkten in Mittel-
Java wurden in diesem Sinne angestellt; mehr systematisch sind aber im Jahre 1877 und
1878 vom Ingenieur Soeters Beobachtungen zwischen fünf Paar Punkten des Basis-Netzes
von Tangsil veranstaltet. Diese Beobachtungen wurden längere Zeit von 7 Uhr Vm. stündlich
bis zu 1 Uhr Nm. fortgesetzt; Barometer und Thermometer wurden dahei regelmässig
notirt. Die Berechnung derselben zeigte zwar eine regelmässige Abnahme des gesuchten
Factors mit der Tageszeit, merkwürdiger Weise gab aber jede Gombination der Stationen
einen anderen Werth für den Factor selbst zu einer bestimmten Stunde; die constanten
Unterschiede betrugen nahe ein Drittel des Werthes selbst, und so verlor diese Untersuchung
in so weit für uns ihren Werth, als sie uns im Betreff des mittleren Refractions-Factors in
der Unsicherheit liess.

Theils sind diese constanten Unterschiede vielleicht dem für jedes Terrain specilisch
eigenen Gesetze der Abnahme der Dichtigkeit der Luft, theils aber auch zweifellos localen
Anziehungen zuzuschreiben, welche die Zenithwinkel fälschen.

Es wurde sodann versucht aus sämmtlichen vorhandenen Paaren von gegenseilig,
wiewohl nicht gleichzeitig beobachteten Zenithwinkeln den mittleren Werth des Refractions-
factors abzuleiten. Um zu verhindern dass die locale Anziehung einen zu grossen Einfluss
auf das Resultat ausübte, wurden nur Paare benutzt, wo die Entfernung der beiden Stand-
dunkte 20000 Meter übertraf. Ich bemerke hierbei, dass die Zenithwinkel überhaupt um
etwa dieselbe Tagesstunde, nämlich gegen acht Uhr des Morgens genommen wurden.

Nichtsdestoweniger war es überraschend, wie auf Java, wo Barometer und Thermo-
meter einen so regelmässigen Gang haben, der gefundene Werth von k dennoch so aus-
serordentlich verschieden war?. Und auch mit zunehmenden Höhen der Standpunkte nimmt
der Refraetions-Factor nicht regelmässig ab; grosse wie kleine Werthe kommen sowohl bei

a

ı Es ist meines. Erachtens nicht ganz richtig, diese sehr variable Zahl eine (onstante zu nennen,
wie oft geschieht.

2 Vielleicht könnte hierfür die folgende Erklärung abgegeben werden. Nach einer heiteren Nacht, wo
die unteren Luftschichten sich abgekühlt und also eine grössere Dichtigkeit bekommen haben, muss die
Refraction früh ansehnlich sein. Die Morgensonne befördert aber die Ausgleichung der Dichtigkeit der Schich-
ten der Atmosphäre und. macht also die Refraction abnehmen. Eine heitere Nacht, von einer in den Cirrhus-
Gegenden bezogenen Atmosphäre gefolgt, muss also einen Maximalwerth der Refraction geben ; eine bezogene
Nacht, von einem sonnigen Morgen gefolgt, einen Minimalwerth. Es wäre zu wünschen, dass bei geodälischen
Höhenbestimmungen die atmosphärischen Zustände genau beobachtet und notirt würden.

 
