 

9

In diesem Berichte erwähnt Herr Hirsch, dass er gegenwärtig den Schluss seines
Werkes « Präcisionsnivellement in der Schweiz » veröffentlicht, welches in der zehnten
Lieferung den katalog der Schweizer Höhen mit einigen Tausend ausgeglichenen Höhenan-
gaben enthält. | Ä

Er erklärt, dass, da die Frage betreff eines allgemeinen Normalhorizontes noch
nicht gelöst ist, die schweizerische geodätische Commission zu ihrem Bedauern genöthigt
gewesen sei, ihre Höhen auf den Nullpunkt des Landes, das heisst auf den Fixpunkt der
« Pierre du Niton » (Geneve), zu beziehen.

Bei diesem Anlass bittet Herr Hirsch um die Erlaubniss, den in der letzten Sitzung
verlesenen Bericht des Herrn Helnert, zu besprechen. Er bedauert lebhalt, dass Irotz der
gelehrten Besprechung der Resultate der Ausgleichungen einer gewissen Zahl von Polygonen
durch den Herrn Direktor des Gentralbureau's, die Schlussfolgerungen seines Berichtes
vorerst vollständig negativ sind.

Ohne vorläufig untersuchen zu wollen, ob Herr IHelmert die Thatsache, dass die
Niveauunterschiede der verschiedenen Meere die Ungewissheit, mit der man sie feststellen
kann, nicht übersteigen (was man offenbar bestreiten kann, sobald diese Unterschiede ihre
mittlern Fehler zwei bis drei Mal übersteigen), wissenschaftlich festgestellt habe, glaubt
Herr Hirsch, dass dieser spezielle, von Herrn Helmert ausschliesslich behandelte Gesichts-
punkt, obgleich er keineswegs uninteressant ist, doch nicht die eigentliche lauptfrage
darstellt, um welche es sich handelt. Vor Allem scheint es nothwendig auf diesem Gebiete,
auf dem noch grosse Verschiedenheit, um nicht zu sagen Verworrenheit, herrscht, Ueber-
einstimmung für den Ausgangspunkt der Höhenbestiimmungen einzuführen, um zu ver-
meiden, dass sobald man die Landes-Grenzen überschreitet, die sogenannten absoluten
Höhenzahlen um einen oder mehrere Meter sich ändern, was nicht blos für die Eisenbahn-
und Kanal-Ingenieure, sondern auch für die wissenschaftlichen hypsometrischen Unter-
suchungen sehr unbequem ist.

Was Herr Hirsch vom Gentralbureau besonders gerne ermittelt gesehen hätte und
was er noch vor der nächsten Generalconferenz von Herrn Helmert untersucht zu sehen
hofft, das sind unter Anderm folgende Punkte :

4. Welches sind die Höhen der Normalfixpunkte, welche gegenwärtig in den ver-
schiedenen Ländern gebräuchlich sind, und mit welcher Genauigkeit kann man sie auf das
nächste mittlere Meeresniveau beziehen, sei es indem man den direkten Nivellementslinien
folgt, oder indem man sie aus der GCompensation der bezüglichen Netze ableitet.

9. Eine vergleichende Liste der mittleren Meereshöhen, welche durch die in den
verschiedenen Häfen aufgestellten Mareographen geliefert worden sind, aufzustellen, wobei
für jedes einzelne Instrument seine Thätigkeitsdauer und die Genauigkeit seiner Resultate
anzugeben sind; so viel wie möglich, sollten für die an denselben Küsten befindlichen be-
nachbarten Mareographen ihre durch direktes Nivellement gefundenen Niveaudifferenzen
beigefügt werden.

„)

3. Nach den erhaltenen Angaben die Frage zu erörtern, welches Meer und welche

syn ah fa DT IHRE MN 1. al ul

 

LIE

kon tnikil!

and hl

 

 
