 

Annexe A. Il.

LE ZERO DES ALTITUDES

Mirror pm M 12 Pron Meınseer, DIRECTEUR, Du BormAU CENTRAL.

@est un vieil usage d’indiquer Paltitude d’un lieu de la surface du globe d’apres
la surface de la mer. Au point de vue de la geodesie, ce proc&de est absolument indispen-
sable, parce que l’ocdan est le seul moyen connu de rapporter ä un horizon commun les tra-
vaux g6odesiques des iles et continents divers söpards par les mers. A P’int6rieur de surfaces
terrestres, il est vrai, on ne peut en general obtenir Vallitude r&ciproque de divers points
que par des nivellements; mais le rösultat de ces mesures n’arrive A une importance generale
et scienlifigue qu’en se rattachant au niveau de l’ocdan. Malheureusement une cerlaine inde-
eision est inherente A la surface de l’ocdan par suite de son agitalion constante; on läche
d’y remödier le plus possible par l’observation de ce qu’on appelle le niveau moyen de la
mer. Cependant, il est certain que les niveaux moyens ne röpondent pas exaetement & une
surface de niveau. En th&orie, cela est parfaitement clair; toutefois la voie pour caleuler par
la theorie les denivellations provenant de difförentes causes est encore assez peu explorce.
La encore on en est r&duit & l’observation.

C’est un des plus grands merites de ’Association g6od6sique internalionale d’avoir
commence des d&marches convenables sous ce rapport Löt apres sa fondation. On decida de
d&terminer le niveau moyen de la mer au plus grand nombre possible d’endroits conve-
nables et d’&tablir un reseau de nivellements de preeision comprenant aussi ces points des
cöles.

Gräce A ces deeisions, et aussi au fait que de tels travaux ont une grande valeur
pratique pour les Etats civilis6s modernes, on a ex6culd dans les vingt-ceing dernieres anndes
un grand nombre de nivellements minutieux el determind une serie de niveaux moyens de la
mer. Le reseau des lignes de nivellement est devenu particulierement serr& ces dernieres
anndes dans P’ouest et le centre de l’Europe, et l’on peut se demander si ’on ne pourrait
d6ja en döduire des rösultats gendraux, quoique les travaux soient encore en cours d’exe-
cution dans bien des cas.

 

dl ul _ |

|

sauna. a1 RL Mal

alkıaduukulı

 

 
