 

Annexe B. VI.

GRIECHENLAND

Bericht über die Triangulirungs-Arbeiten in Griechenland im Jahre 1891.

Im verflossenen Winter wurden für alle im Jahre 1890 beobachteten Stationen
1. Ordnung die wahrscheinlichsten Richtungen berechnet und die Dreiecke provisorisch zu-
sammengestellt.

Die zum Theile von griechischen Officieren (Anfängern im geodälischen F ache) durch-
geführten Messungen erwiesen sich hiebei als sehr gut, da nur ein einziger Dreiecksfehler
die zulässige Fehlergrenze überschritt.

Mitte Juni 1891 begab ich mich wieder nach Griechenland, um die Einleitungen für
die Fortsetzung der Triangulirung zu treffen, die Feldarbeiten konnten jedoch erst Ende Juni
beginnen, und es wurde das Polygonnetz gegen Norden fortgesetzt. Das beiliegende Skelet
zeigt den Stand der Arbeiten mit Ende December ; durch Nachmessungen auf Taygetos wurde
der vorhin erwähnte grössere Dreiecksfehler een verringert und beträgt jetzt nur
mehr 1'3.

Der Vorrath an Präcisions-Instrumenten wurde abermals vermehrt und zwar um
einen Theodoliten von Starke und Kammerer in Wien!, so dass jetzt fünf Instrumente für
Winkelmessungen 1. Ordnung zur Verfügung stehen.

Die bei den geodätischen Arbeiten zugetheilten königl. griechischen Offieiere machen
in ihrer Ausbildung erfreuliche Fortschritte, und es ist die Zahl der einheimischen Officiere,
denen man selbständige Winkelmessungen anvertrauen kann, in steter Zunahme begriffen.
Infolge dessen konnte ich Ende August wieder nach Wien urucklehr in. die Leitung deı
Arbeiten während meiner Abwesenheit dem Herrn k. und k. Major Franz Lehr überlassend,
und es konnte auch das dritte Mitglied der österr.-ungar. Mission, der Herr k. und k. Linien-
schiffs-Lieutenant Julius Lohr, nach zweijähriger erfolgreicher Thätigkeit in Griechenland,
von dort abberufen werden.

Im Laufe des Jahres 1892 dürfte das Dreiecksnetz 1. Ordnung in dem festländischen
Theile des Königreiches Griechenland fertig werden.

Wien, im Jänner 189.

HARTL,

k. und k. Oberstlieutenant.

ı Vergl. hierüber meinen Aufsatz: « Die Landesvermessung in Griechenland » in den Mittheilungen
des k,. und k. milit.-geogr. Inst., Band X, Seite 200 ff.

1 Var

nun 11T TFT JE MT ER 1

Walk

ih

I. lud

1, yrrmalb pn sn

Austin

 

 
