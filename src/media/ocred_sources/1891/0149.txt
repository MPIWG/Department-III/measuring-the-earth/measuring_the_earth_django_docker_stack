 

1! Au T im

RR TI ARTEGIT TFT PART IT

Bi

et

 

 

141

Dans la premiere comme dans la seconde serie, une marche systemalique est @vi-
dente; la difförence entre les resultats obtenus dans les deux positions des contacts ex-
cäde 9°. Les deux series sont d’ailleurs concordantes, en ce sens qu’elles indiquent que la
Toise, dans sa position normale (inseription en haut), parait sensiblement plus courte lorsque
le point de contact se rapproche de l’observateur qui lit cette inseription. I] y aurait done, de
Pavant aA l’arriere, une obliquite sensible des faces terminales, au moins pres de leur partie
centrale, par rapport äl’axe de la Toise.

Les mömes essais ont &te faits, exactement de la m&öme maniere, sur la Toise n° 9;
ils ont conduit aux re6sultats suivants :

Toise no 9. (Combinaison des abouts I-III). Premiere position de la Toise (inseription
en dessus).

 

Mr NT \
B 48a)
nr 47.36

Be 41.86
N 48.12

B 47.85
N. 48.99

Br 41.76
N. 46.78

Rn ale)
N = 13)

| Moyı. 41,69 a Ad!

La Toise est relournee face pour face (combinaison des abouts II-IV).

H
+ 40.69

ne r
Be en 15
Rn 41.82
Br. 41.43
a 42.20
Bi: 40.98
Kor: 41.88
Bi 43.42
Ä. 44.35
De 010.83
A alu Al

Moy. + 49.08 + 42.16

 
