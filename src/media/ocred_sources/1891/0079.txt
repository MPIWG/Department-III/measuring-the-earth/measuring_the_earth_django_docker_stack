 

elle

NT

able

=
=
m
=
=
=
=
im
>
u
33
iR

 

 

2

tionen deutlich zu unterscheiden. Herr Ferster schlägt demnach vor, in dieser (onferenz
eine Spezial-Commission aus mehreren besonders mit dieser Frage vertrauten Mitgliedern zu
ernennen, welche dem Bureau der Permanenten Commission beigegeben würde, um der
nächsten General-Conferenz einen vollständigen Entwurf für regelmässig organisirte Breiten-
beobachtungen vorzulegen.

Herr General Ferrero hat von den im Berichte des Herrn Helmert enthaltenen Resul-
taten mit grossem Interesse Kenntniss genommen, er theilt die Ansichten des Herrn Foerster
über die Bedeutung des Breitenproblems und über den Nutzen, dasselbe durch die Erdmes-
sungs-Vereinigung weiter verfolgen zu lassen. Jedoch glaubt er, um praktisch zu verfahren,
wäre es vortheilhaft, nur eine wenig zahlreiche Commission zu ernennen, schon mit Hin-
sicht auf die Schwierigkeit eine solche Commission häufig genug zu versammeln. Es scheint
ihm, dieselbe sollte drei Mitglieder nicht überschreiten, worunter natürlich in erster Linie
Herr Helmert und italiänischer Seits vielleicht Herr Lorenzoni vorzuschlagen wären.

Ferner hält Herr Ferrero dafür, dass bei diesen Beobachtungen die seculären Ver-
änderungen von den periodischen vollständig zu trennen seien, da das Studium der ersteren
offenbar zu viel Zeit und zu bedeutende finanzielle Opfer verlangen würde.

Herr van de Sande-Bakhuyzen glaubt nicht, dass man beim gegenwärtigen Stande
der Frage die sekulären Veränderungen von den periodischen schon trennen könnte; wäre

‚die Erde vollständig fest, so würde die Aufgabe bedeutend einfacher; die festen Schichten

haben aber wahrscheinlich nur eine verhältnissmässig geringe Dicke. Es ist daher mög-
lich, wenn nicht sehr wahrscheinlich, dass von den Jahreszeiten abhängende Erscheinungen,
wie die Meeresströmungen, Regenmengen, Bildung des Polareises u. s. w. einen gewissen
Einfluss ausüben. Desshalb muss die Erscheinung in ihrer Gesammtheit durch eine Reihe
systematischer Beobachtungen studirt werden.

Herr Tisserand theilt dieselbe Ansicht; er erachtel es für unmöglich und gefährlich,
zum Voraus schon den sekularen Theil der Erscheinung ausscheiden zu wollen, den man
erst zu erkennen im Stande sein wird, nachdem man während einer genügend langen Reihe
von Jahren genaue Beobachtungen angestellt haben wird.

Herr Hirsch ist derselben Ansicht; zugleich theilt er die Meinung des Herrn General
Ferrero über die Erfolglosigkeit der Arbeiten zu zahlreicher (ommissionen, — den Beweis
dafür hat die Association selbst schon geliefert. Zudem darf nicht ausser Acht gelassen wer-
den, dass den Statuten der Associalion gemäss die Permanente Commission beauftragt. ist,
besondere Studien dieser Art zu leiten.

Der Herr Präsident glaubt den verschiedenen Ansichten am besten Rechnung zu
tragen, indem er vorschlägt, für diese besondern Untersuchungen der Breitenänderungen
dem Bureau der Permanenten Commission folgende drei Mitglieder beizugeben: die Herren
Foerster, Bakhuyzen und Tisserand.

Herr Fersier erklärt sich damit einverstanden, wenn der Commission das Recht
