 

20

Qutre les travaux el affaires qui incomberent au Bureau central du fait des recher-
ches sur les variations de latitude, il s’est occupe aussi, comme dans les dernieres annees,
des calculs pour la mesure des degr&s de longitude de Struve.

Tous les materiaux de trianeulation, depuis la frontiere prusso-russe Jusqu’en
Angleterre, forment maintenant un seul tout. Pour le reseau anglo-Irangais de la Manche
seulement, les mesures francaises et angdaises des angles n’ont pas &l& combinees entre
elles, mais simplement les longues lignes geodösiques caleuldes d’apres ces deux groupes
de mesures; l’accord est suffisant.

Les lignes geodösiques entre les stations astronomiques sont presque toutes calculees.

Nous avons aussi mainlenant une communication de M. le directeur Benoit sur la
comparaison de la toise de Bessel avec le prototype du metre international a Breteuil. D’apres
cette communication, le facteur de reduction de la toise au metre doit etre augmente de
1/75000, comme cela a eu lieu & peu pres aussi pour le pied anglais, d’apres les nouvelles
indications am6ricaines. En consöquence, les differences de rattachement des cötes de triangle
qui existent, d’apres une nolice de M. le general Derrecagaix publice dans les Gormmptes-
Rendus de l’Acad&mie des sciences de Paris, entre la nouvelle chaine de la meridienne
francaise, le reseau anglais de la Manche et le röseau belge, seront considerablement reduites.

J’ai eommence la rödaetion du travail du Bureau central sur la mesure des degres
de longitude de Struve en Allemagne, en Belgique et sur la Manche, et je pense proceder
d’abord A ’impression des materiaux de Iriangulation.

r r

La Conference generale de Paris .avait charge le Bureau central d’Etudier la question
du choix d’un z&ro des altitudes pour l’Europe, ei de presenter un travail sur celte question
a la Commission permanente. D’accord avec les opinions de quelques delegues, qui avalent
döja &te exprimdes partiellement ä Paris, j'ai eu d’abord en vue la reponse ä& cette question :

L’exactitude actuelle des nivellements de preeision permet-elle l’acceptation d’un
zero des allitudes commun pour toute ’Europe, ou seulement pour l’Europe oceidentale et
centrale, de telle facon que les Etats plus &loignes puissent s’y rapporier avec une sürele
suffisante ?

Le resultat est malheureusement en faveur de Ja negative. Pourtant, le travail
immense du reseau de nivellements de preeision en Europe, ex&cute depuis un quart de
siccle, offre un r&sultat positif important, puisqu’on sait ä prösent que le niveau moyen des
mers dans l’Europe centrale et oceidentale est le merme ä un ou deux decimetres pres. ke
fait d’avoir contribus A la connaissance de ce resultat doit ätre une cause de salisfaction
pour tous ceux qui ont partieip& & ce Lravail giganlesque.

Jaimerais ä ne presenter le rappori detaille sur la question du zero des altitudes
qw’ä l’une des prochaines sdances de la Commission permanente.

L’activite administrative du Bureau central a consist& surtout dans l’expedition des
Comptes-Rendus de la Cominission permanente, session de Fribourg, et de nombreuses publi-

 

 

sun dd AD VAN NA U 1. dl ul

Ilm

Lkr ul) 1

wind

ba bb a N

 
