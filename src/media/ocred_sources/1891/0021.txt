 

1:
5
=
ie
=
um
-
in

 

IM LIEGE

IR

LITT

 

l’empechent de se rendre ä la Conference de la Commission permanente dont ıl fait partie.
M. le general Arbser ajoute dans sa lettre que les rapports sur les iravaux geodesiques,
exdeutes en Autriche pendant l’annde derniere, seront envoyes A temps pour Elre commu-
niques & la Gonference.

M. Hirsch fait remarquer qu’aucun autre delegue de l’Autriche-Hongrie n’a annonce
son arrivde, de sorle que, pour la premiere fois depuis l’existence de P’Association, !’empire
d’Autriche-Hongrie n’est pas represente dans la Conference.

Enfin, le Seeretaire annonce que M. le general Stebnitzki s’est excuse d’eire empeche
de venir A Florence, et qu’il lui a envoy@ son rapport qui sera lu dans une des seances
ulterieures.

La Commission permanente procede ensuite ä l’election du President. Comme M. von
Kalmär a transmis par lettre son droit de vote a M. le general Ferrero, neuf bulletins sont
delivres et rentres; leur d&pouillement par le bureau donne le r&sultat suivant !

M. Faye obtient 8 voix.
M. van de Sande Bakhuyzen 1 voix.

En consequence, M. Faye est nomme president de lu Commission permemente.

Il prend place au fauteuil presidentiel et prononce les paroles suivantes !

« Messieurs et chers collegues,

« Je suis infiniment honor6 de la prösidence que vous avez bien voulu me conlerer.
Je ferai tous mes efforts pour me rendre digne de votre cenfiance, et pour ne pas resier au-
dessous de mes prödöcesseurs parmi lesquels je dois citer Villustre general Baeyer, le londa-
teur de notre Associalion, et M. le general Ibanez. De ce dernier on ne peut oublier. qu'il a
donn& un grand essor A la g6odesie espagnole et qu’il a contribud, avec les officiers francais
et le general Perrier, & la jonction de P’Afrique A /’Espagne au moyen des plus grands
triangles qui aient Jamais &t& inesures.

« Profitant du privilöge qui m’est confer6 par nos reglements, de designer le vice-
prösident, je prie M. le general Ferrero d’aecepter cette honorable fonetion. Je me fais un
grand plaisir de rendre hommage A ’homme qui a presid@ avec lant de succes A la geodesie
italienne et qui a ferme immense polygone geodösique qui entoure le bassin oceidental de
la Mediterrande, en jetant des trianeles italiens par dessus la mer depuis la Sieile jusqu’ä
la Tunisie. »

Le bureau ayant di charg& de nommer la Commission des comptes et des finances,
il la compose de MM. Ferster, van de Sande Bakhuyzen ei von Zachariae. M. le President
prie la Commission de prösenter son rapport aussitöl que possible.

I] fixe la prochaine reunion au samedi 10 oclobre ä& 2 heures.

La seance est levee A D heures.
