 

IN AR DIN

FORT RAR UL ARINN DT pr

He

 

 

Annexe B.X.

RÜSSIE

Rapport sur les travaux eeodesiques exeeutes en Russie en 1890.

1. OPERATIONS ASTRONOMIQUES
«ı) Determinations telegraphiques de differences de longitude.

Les rösultats des observations de MM. Kortazzi, Polanowsky et Miontehinsky effec-
iuces en 199 sont:

1. Sarepta (sur la Volga), Eglise.

Bostow (sur le Don), eathödtale  . 936 2: 2.04909:073
2. Rostow (sur le Don), cathödrale.
Alesandrowek, cathedrale. 98 9
Alexandrowsk, cathedrale.
Nieolaew, centre de !’Observaionıe . : 0905 385

ae
w-

Toutes ces longitudes sont deduites d’observations de six soirdes. Au milieu des
observations, les observateurs ont changc de slalion.

b) Latiludes.

M. le colonel Rylke a determine les latitudes suivantes :

dssapeptal celise). ,.. ., Zoo oe
2. Alexandrowsk, cathedrale. . 47 48 40.37 = 0.90

Ges observalions ont &t& faites d’aprös la methode de Taleott. Sur chacune des sta-
lions, on a observ& 20 paires d’ötoiles tirdes pour la plupart du catalogue de Poulkowa :
« Positions moyennes de 3549 dtoiles », edition 1880.

ASSOC. GEOD. INTERN. — 98

 
