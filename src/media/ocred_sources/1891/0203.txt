 

Na AR UM]

ll

Tal

rm ln HERRIN RINGEN A LREL pw

 

 

Annexe B. VIII’.

NOTIZ UBER DIE TRIANGULATION VON JAVA

Von J.-A.-0. OUDEMANS

Nachdem der geographische Ingenieur S. H. de Lange! die Polhöhe, und mittels
Mondbeobachtungen die Länge von Batavia und Menado, und überdies noch die Lage ver-
schiedener Punkte, sowohl in der Residentschaft Menado, als auf der Hin- und Rückreise im
0. I. Archipel bestimmt hatte, wurde im Jahre 1854 die Triangulation von Java in der
Residentschaft Cheribon von ihm begonnen. Der Zweck dieser Triangulation war die Lie-
ferung fester Punkte für die seit kurzem angefangene Aufnahme behufs Anfertigung einer
« militärischen und topographischen Karte ». Die grösste Genauigkeit wurde dabei nicht ge-
fordert; auch war de Lange nur mit dürftigen Hülfsmitteln ausgerüstet, da mit seiner
Ernennung im Jahre 1849 mehr bestimmt die astronomische Ortsbestimmung im Öst-Indi-
schen Archipel bezweckt worden war.

Im Jahre 1851 war ihm ein Assistent in der Person seines Bruders G. A. de Lange?
beigegeben worden, so dass die Triangulation von den beiden Brüdern angefangen wurde.
Eine vorläufige Grundlinie wurde, unweit Cheribon, dem Strande entlang, mit einer Mess-
kette gemessen und aus dieser mittels ein Paar Dreiecke die Entfernung zweier Gipfel be-
stimmt. Sehr bald aber erkrankte der Ingenieur S. H. de Lange und musste mit Urlaub nach
Europa zurückkehren; er erreichte aber die Heimath nicht mehr und starb an Bord des
Schiffes bald nach seiner Abfahrt.

So setzte Herr G. A. de Lange, der seinem Bruder bald als Ingenieur nachlolgte,
in den Jahren 1855 bis 1857 die Triangulation über die Residentschaften Banjumas, Bagelen
und Kadu fort, nachdem erst ein Anschluss an Batavia durch ein Azimut, von Pangerango
aus, nach der Hafenwarte zu Batavia, und den Breitenunterschied dieser Orte angestrebt
worden war. Als Assistent wurde ihm zunächst temporär der Controlenr bei der « Verwal-

ı Vor seiner Ernennung Lieutenant zur See I. Classe, z. D.; früher mit dem Unterricht der Mathe-
matik und Nautik an der Marine-Akademie zu Medemblik beauftragt.
2 Damals Lieutenant zur See, 2. Glasse.

 
