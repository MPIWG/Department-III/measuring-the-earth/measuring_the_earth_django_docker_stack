 

161

So wie vor zwei Jahren, und auch im Vorjahre, war Oberstlieutenant Hartl heuer
ebenfalls durch mehrere Monate in Griechenland mit der Leitung der dortigen Triangulirung
beschäftigt.

| a. PRÄCISIONS-NIVELLEMENT.

Die im Vorjahre, auf Ansuchen des Herrn Directors des Central-Bureau’s der inter-
nationalen Erdmessung, angeordnete Zusammenstellung der internationalen Anschluss- und
Durchgangs-Nivellement-Linien wurde im Frühjahre beendet und an das Central-Bureau
abgeschickt.

Wie im Vorjahre wurden auch heuer meist zweite Messungen in Galizien und Nord-
Ungarn vorgenommen, weshalb mit Ende der Sommerarbeiten auch heuer die Gesammtlänge
der Nivellement-Linien nicht erheblich vermehrt wird.

Von den Nivellement-Anschlüssen an Russland wurde heuer der bei Radziwilow
dadurch fertig gestellt, dass an die hart an der Grenze errichtete russische Marke direct
angeschlossen wurde.

Bei Tomaszow war an der Grenze keine russische Marke zu finden, weshalb unser
Nivellement bei einer am österreichischen Zollhause (1 km. von der Grenze) eingemauerten
metallenen Höhenmarke abgeschlossen wurde.

Auf diese Weise ist nunmehr unser Nivellement mit dem russischen an zwei Stellen
verbunden, in Szczakowa und Radziwilow, und an drei weileren Stellen ein Anschluss vor-
bereitet, bei Tomaszöw, Podwoloczyska (Aufnahmsgebäude, österreichische Bahnseite) und
bei Nowostelica (österreichisches Aufnahmsgebäude an der russischen Grenze).

Nach Mittheilung unseres Herrn Oollegen Excellenz Stebnitzky wird von Seite der
russischen Nivelleure im Sommer 1892 auch bei Tomaszöw eine Nivellement-Marke in tinmit-
telbarer Nähe unserer Grenze angebracht werden.

Sobald dies geschehen ist, wird dieselbe auch von unserer Seite in das Nivellement
einbezogen, wodurch ein dritter Anschluss mit Russland hergestellt ist.

Mit Ende des Jahres 1891 ist die Länge unseres Nivellement-Netzes 16985 km., in
welchen 2862 « Höhenmarken » errichtet und einbezogen sind.

Im verflossenen Sommer wurden gemacht :

«ı) Doppelmessung der Linien :

Zablotce- Brody-Reschsgrenze (Bahn).

Rawaruskua-Belzec (Strasse).

b) Zweite Messungen auf den Linien :

Przemysl-Jaroslau-Pilzno (Strasse).

 

IN I AMT

un

Jaslo-Pilzno »
= Pelzno-Bochnia »
: Krakau-Oswiecim »
Banreve-Poprad-Neu-Sandec »
- Lopatyn-Brody »

ASSOC. GEOD. INTERN. — 2

FIR ERIIARR. ILL ARINAIN UML ULKERE AL

eh

 
