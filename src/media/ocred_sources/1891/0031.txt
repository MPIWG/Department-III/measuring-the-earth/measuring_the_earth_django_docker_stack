FE amnae pause

ankam RT TIEREN erh

ANA

I AR

I

il ii pain

    

um
=
im

 

M. le President remercie M. Helmert de son interessant Rapport, et donne la parole
aM. Ferster, qui desire ajouter quelques mots au sujet de la variation des latitudes.

M. Forster constate que, malgr& le caraclere provisoire que possedent les premiers
rösultats parvenus de Honolulu, ils suffisent, par leur marche completement identique et de
signe contraire avec celle observ6e en Europe, pour demontrer qu’il s’agit, en eflet, d’un
phenomene general de V’axe terrestre, et pour justifier Association de continuer d’une
maniere syst&matique et avec perseverance l’ötude de cet important phenomene. Il interesse,
il est vrai, l’astronomie autant que la gdodesie; mais l’astronomie ne possede pas pour un
pareil travail d’organe aussi approprie que l’est notre Commission permanente, dotee d’un
petit budget accorde par les Gouvernements des Etats de la Convention geodesique, landis
que la Societe astronomique internationale ne possede que de faihles ressources provenanl
des souscriptions de ses membres, destindes uniquement ä& P’ötude des positions des £toiles
fixes de notre hömisphere.

M. Feerster estime qu’il faudrait organiser tout un service d’observations regulieres
et conlinues de latitude dans un certain nombre d’Observatoires distribuds convenablement
sur la Terre; il ne sait pas si trois points situes dans les latitudes moyennes, a 120° de
difference de longitude environ, par exemple en Italie, aux Etats-Unis d’Am6rique et dans
le Japon suffiront A ce but. On en jugera lorsque l’expedition de Honolulu sera terminee.

En tout cas, il semble A M. Feerster que les azimuls absolus devraient intervenir
egalement dans cette &tude, surtout dans des Observatoires qui possedent des mires lointaines,
comme par exemple ceux de Neuchätel, de Nice et sous peu celui de Potsdam ; car les azimuts
montreront cerlainement l’influence des variations periodiques de latitude d’une maniere tres
distincte et assez differente, dans leurs caracteres, des changements provenanl d’autres causes
meteorologiques, opliques, elc.

M. Forster propose donc de nommer dans cette r&union une Commission speciale
de quelques membres particuli&rement comp6tents, qu’on adjoindrait au bureau de la lom-
mission permanente, et qui serait chargee de proposer ä la prochaine Conference generale
un projet complet d’un service regulier de latitude.

M. le General Ferrero a entendu avec beaucoup d’interät les rösultats contenus dans
le Rapport de M. Helmert, et il approuve les idees de M. Feerster sur l’importance du pro-
blerne des latitudes et sur l’utilit& de les poursuivre par l’Association g&odesique. Toutefois,
il croit que pour etre pratique, il faudrait nommer une Commission peu nombreuse, a cause
des difficultes qu’on &prouvera & la reunir; il lui semble qu’elle ne devrait pas depasser
trois membres, parmi lesquels naturellement M. Helmert et, du cöt& des asironomes italiens,
peul-etre M. Lörenzoni.

D’autre part, M. Ferrero estime qu’il faudrait separer dans ces &tudes la question
des variations s&eulaires d’avec les variations periodiques; les premieres demanderaient
evidemment trop de ivmps et exigeraient des moyens financiers trop considerables.

M. van de Sande Bakhuyzen ne croit pas qu’on puisse, dans l’&tat actuel Je la

 
