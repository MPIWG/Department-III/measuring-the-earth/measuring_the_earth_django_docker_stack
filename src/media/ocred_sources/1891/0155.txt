EFT GRILARERE IULMRIAIL ALL ke pl!

 

III ART

Pen

 

 

4147

L’&quation admise jusqu’ä present pour la Toise de Bessel, par rapport ä la Toise du Perou,
eanı x, ilen r6sulterait pour la longueur de la Toise de Bessel ä 16, 35

1,9490348.

Telle est, par cons&quent, la valeur qu’on Iui a attribude Jusqu’ä l’epoque actuelle.
Or les &tudes faites au Bureau international ont conduit, pour cetie Toise, ä la valeur

1,949061

quieest tr&s probablement exacte A un ou deux microns pres, et qui difiere de la pr&cedente de
96°9. La Toise de Bessel a donc en realit€ une longueur plus grande de

26”2 1
1949000  7A000

environ que celle qui lui a öt& attribude. Torutes les longueurs “ı en parlant de cette
Toise et exprimees en melres, doivent done elre augmentces de .. de leur valeur.

Le 13 avril 1891, le General Derrecagaix, Directeur du Service geographique de
’arımnde franeaise, en prösentant ä l’Acad&mie des sciences (Gomptes Rendus, GXI, p. 770)
les resultats d’une mesure recente d’une nouvelle base aux environs de Paris, an remar-
quer que, si l’on caleule, en partant de celte base, les cötes de jonction de la nouvelle meri-
dienne de France avec les triangulations anglaise, belge, italienne et espagnole, on irouve
une discordance systematique; tous les cötes caleules par les triangulations na nere sont
plus courts d’une quantite relative qui est presque constante, et qui est de —, m environ.
Comme la Toise de Bessel a servi d’etalon fondamental, pour le plus grand nombre e au moins
des däterminations dont il est ici question, on voit que, si P’on retablit le veritable rapport
des unitös employ6es de part et d’autre, cette discordance systematique disparait d’une facon
presque complete. Si ce resultat se trouve ulterieurement confirme par d’autres verifications,
il est &videmment d’une haute importance. Il demontrerait une fois de plus, d’une facon
inattendue, la grande valeur des op6rations geodesiques faites dans les differents pays, en
meme temps que l’invariabilite des anciens etalons. Ge seul resultat suffirait pour justitier le
travail considörable qui, ainsi qu’on vient de le voir, a &t6 execut& au Bureau international
des Poids et Mesures pour fixer le rapport des anciennes mesures aux nouvelles avec tout le
degr& de preeision qu'il etait possible d’atteindre.

Octobre 1891.
Dr Rene BENOIT.

 
