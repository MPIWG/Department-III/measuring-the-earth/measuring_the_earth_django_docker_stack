TEE ger

im y

TE er

a

MR ANA

iii,

ll

 

WEIT TI IE RRTARRG ALLARINN,

 

 

Annexe B. VIII«e.

Rapport sur les travaux de Sumatra.

Ranvort sueeinct des operations faites jusau’a la fin de 1890 sur Vile de Sumatra
/

par la section Irigonomelrique.

La section trigonometrique (Lriangulatie brigade) du Service topographique ressor-
tant A l’ötat-major de Parmde des Indes orientales, est charg6e de la triangulation de File de
Sumatra. Cette triangulalion comprendra en premier lieu les parties de cette ile designees
sous les dönominations : Gouvernement van Sumatra’s Westkust et Residentie Oostkust van
Sumalra.

Qutre la triangulalion primaire, la section Irigonomelrique ex&cute aussi les trian-
ogulations secondaires qui doivent servir A la determination des points de reperes pour le
lev6 topographique et le dessin des cartes, op6rations faites par une section spöctale (opne-
mings brigade).

Le personnel de la section trigonomötrique est compose de : un capitaine ou major
comme chef, deux capitaines, deux lieutenants et douze sous-officiers. En outre, la section
einploie environ soixante-dix Malais pour desservir les heliotropes et pour d’aulres services.

Les op6rations ont &i& commencdes au mois de mars 1883 dans les environs de
Padang, la capitale du « Gouvernement van Sumatra’s Westkust », par la mesure d’une base
et la determination d’une latitude et d’un azimut. La triangulation a te commencee l’annee
suivante et continuce des lors sans interruptlion.

Les instruments pour la mesure des angles proviennent en partie du Service geo-
graphique, qui a fait dans le temps la triangulation de Vile de Java, et en partie d’acquisi-
tions faites en 1883. (es instruments proviennent des ateliers de Pistor ei Martins et de
Wegner ä Berlin. Ge sont des altazimuts appropries aux ope@rations astronomiques, avec des
cercles de 10, 8 et 6 pouces de diametre. Comme signaux, on emploie presque exelusive-
ment des heliotropes.

A la fin de 1890, la mesure des angles &tail terminde sur 48 stalions de premier
ordre, 52 de deuxieme ordre et un certain nombre de stations de troisißme ordre.

Pour les stations de premier ordre, on emploie la methode developpee par le chef
actuel de la Landesaufnahme de Prusse, M. le lientenant-general Schreiber (voir Zeitschrift

ASSOC. GEOD. INTERN. — 21

 
