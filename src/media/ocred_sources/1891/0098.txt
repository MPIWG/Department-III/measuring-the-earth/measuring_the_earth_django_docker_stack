 

Da es sich hiebei gleichzeitig um eine metrologische und geodätische Frage handle,
sollle man wohl überlegen, ob eine Neumessung einer der spanischen Grundlinien ange-
zeigt sei.

Herr Hirsch ist derselben Ansicht; er glaubt sogar dass es sich einzig um eine
metrologische Frage handle, welche vielmehr durch Rechnung und nöthigenlalls durch Ver-
gleichungen in Breteuil, als durch neue Operation auf dem Terrain gelöst werden sollte. Da
der eiserne Stab des Generals Ibanez auf dem internationalen Bureau für Maasse und
Gewichte ganz genau bestimmt worden, so genügt es in der That bei der Reduktion der
Grundlinien, welche mit diesem Stabe vermessen sind, seine (sleichung in Anwendung zu
bringen um dieselben in genauen metrischen Einheiten auszudrücken. Da der bimetallische
Stab, der bei der Messung der Gentralbasis von Madridejos gedient hat, nur nach dem alten
Stabe von Borda bestimmt worden ist, so kann man höchstens den Wunsch aussprechen,
dass dieser Stab ebenfalls dem internationalen Bureau zugeschickt werde, um dort mil dem
neuen metrischen Prototype verglichen zu werden. Sollte Herr d’Arrillaga ihn zu diesem
Zwecke nach Breteuil senden können, so wird er, sobald die dringendsten Arbeiten es
erlauben, aufs Genaueste bestimmt werden.

Nach diesen Erläuterungen erklärt sich Herr Oberst Bassot befriedigt und zieht
seinen ersten Antrag zurück.

Was den zweiten Wunsch anbetrifft, alle gemessenen Grundlinien der verschiedenen
Länder mit den Anschlussseiten der benachbarten Netze verbunden und diese Seiten in den
verschiedenen angewandten Einheiten ausgedrückt zu sehen, erklärt Herr Hirsch, dass dies
offenbar eine wesentliche Bedingung ist, um eine der Hauptaufgaben der Association zu
erreichen, nämlich alle geodätischen Netze Europas zum Zwecke der Erforschung dieses
Theiles der Erdoberfläche zu verwerthen. Er fügt hinzu, dass dieser Wunsch schon grössten-
theils erfüllt ist; denn in fast allen Ländern, wo Grundlinien gemessen worden, sind dieselben
mit den Grenzseiten durch Triangulationen erster Ordnung verbunden; in den meisten
Fällen sind auch die Elemente dieser Arbeit bereits veröffentlicht. In den Fällen, wo diese
Anschlüsse noch nicht gemacht worden sind, sollte man die nöthigen Elemente durch Cor-
respondenz der geodätischen Commissionen und Institute der betreffenden Länder zu ge-
winnen suchen. Ohne Zweifel ist es eine der Hauptaufgaben des Centralbureau’s, alle diese
Angaben zu sammeln und in synoptischen Tabellen zusammenzustellen ; wenn Herr Helmert
bis zur nächsten allgemeinen Conferenz diese Arbeit ausführen könnte, würde er sicherlich
einen wesentlichen Schritt zur Erreichung des Endzieles der Gradmessung beitragen.

Nachdem Herr Prof. Helmert erklärt hat, dass er bereit sei sich unverzüglich mit
dieser wichtigen Arbeit zu beschäftigen, spricht die Permanente Commission den Wunsch
aus, die Länder, welche die nöthigen Elemente noch nicht veröffentlicht hätten, möchten
dieselben sobald als möglich dem Centralbureau mittheilen, damit dasselbe die synoptischen
Tabellen betreffend den Anschluss der geodätischen Netze Europas der nächsten General-
Conferenz in möglichst vollständiger Form unterbreiten könne.

 

 

ulllı

Jul

PIMBraT = TIERTTIOUE TRITT TERIG VTTHICTETEEN

Luk la)

 
