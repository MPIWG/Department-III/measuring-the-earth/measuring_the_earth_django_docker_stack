 

He

Se er

Nun Mm

in

dia] jr

a
B
=
=

==
Be
r

m

s=

E

äh

ERSTE SITZUNG

Donnerstag den 8. Oktober 1891.

Die Sitzung wird um 2 Uhr 15 Minuten im « Saale der Zweihundert » im « Palazzo

Vecchio » eröffnet.

— ot

© Sı

ot

Anwesend sind :

I. Die Mitglieder der Permanenten Commission :

Herr Prof. R. Helmert, Divektor des Köniel. Preuss. Geodätischen Institutes und des
Gentralburean’s der Internationalen Erdmessung, in Berlin.

Herr Prof. Dr. Ad. Hirsch, Direktor der Sternwarte, in Neuenburg, ständiger Sekretär.

Herr #. @. van de Sande-Bakhuyzen, Direktor der Sternwarte in Leyden.

Herr H. Faye, Mitglied der Akademie der Wissenschaften und Präsident des Längen-
bureau’s, in Paris.

Herr General-Lieutenant A. Ferrero, Direktor des militär-geographischen Instituts, in

Florenz.
Herr Prof. Dr. W. Feerster, Direktor der Sternwarte, in Berlin.
Herr Oberst Hennequein, Direktor des Militär-Kartographischen Instituts, in Brüssel.
Herr Oberst von Zachariae, Direktor der Geodätischen Arbeiten, in Aarhus (Dänemark).

ll. Die Abgeordneten :

Herr F. de P. Arrillaga, Generaldirektor des Geographischen und statistischen Instituts,
in Madrid. |

Herr Oberst-Lieutenant Bassol, Chef der geodälischen Sektion im geographischen Amt
der Armee, und Gorrespondirendes Mitglieder des Längenbureau’s, in Paris.

Herr Prof. A. Betocchi, Sektions-Präsident des Generalraths im Ministerium der öffent-
lichen Arbeiten, in Rom.

Herr Bouquet de la Grye, Mitglied der Akademie der Wissenschaften und des Längen-
bureau’s und Chefingenieur-IHydrograph der Marine, in Paris.

Herr Constantin Carusso, aus Athen.

ASsoe. GEOD, INTERN. -- 7

 
