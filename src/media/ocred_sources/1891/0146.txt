 

158

Si T.,, represente la longueur ä 0° comprise entre les deux traits de la Toise,
E „la longueur A 0° de la Regle de comparaison, « et «’ les coefficients de dilatation respec-
tifs des deux regles, chaque comparaison conduit A une relation de la forme

rm rm rN ER,
I Us el

ou, avec une approximation plus que suffisante :

in, 8x 1903000 (x — a) t=n.

D’apres l’expression (1) ci-dessus (page 112), on a
a — 0.000017588.

En r6solvant les syst&mes d’öquations fournis par les observations respectivemen!
faites sur les deux Toises, on trouve :
Pour la Toise de Bessel:

« — a — — 0.000005896,
d’oü
| “© — + 0.00001169.
Pour la loise n® 9:
a a = — 0.000006838,
d’ou

== + 0.000011050.

La substitulion de ces valeurs dans les äquations de condition conduit aux erreurs
rösiduelles inscrites dans le tableau ci-dessus A cöl& des observations. L’erreur probable
d’une observation est, pour les deux series, & 07.

Si on compare les coefficients de dilalalion ainsi trouves A ceux qui ont &t& deduits
des series de comparaisons des Toises entieres, on voit qu/il y a concordance presque par-
faite pour la Toise ne 9. Pour la Toise de Bessel, la discordance est un peu plus conside-
rable, sans ötre importante. Il est diffieile de se rendre exactement compte de ceux auxquels
il convient de donner la pröförence, et le caleul rigoureux des erreurs probables n’appren-
drait pas grand’chose A cet 6gard. Si les dernieres op6rations ont &L& faites par une methode
plus simple et ind&pendante des causes d’erreurs toules speciales que comportent les me-
sures des regles A bouts, et en outre dans un intervalle de temperature plus etendu, par
contre les premieres valeurs reposent sur un nombre d’observalions beaucoup plus conside-
rable. En admettant finalement, comme valeurs les plus probables, les moyennes des resul-

dl 8

sun or 1 TRIAL BE 1

Lak mil

nn!

 

 
