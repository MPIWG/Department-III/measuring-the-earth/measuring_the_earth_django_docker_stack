 

176

La moyenne de ces differences est 0"289, quantit& dont le zäro d’Ostende se trouve-
rait au-dessous de ’Amsterdamsche Peil, soit 0'140 au-dessous du niveau moyen de la mer
a Amsterdam.

Raccordement avec l’Allemagne.

 

 

 

 

 

Matrieule Altitude us
prus- Designation du repere. DPUS- a Difference Observations.
sIenne. Senne >
ceorrection
N.N. =
s ı m m m L’altitude belge est
38 Eupen. Borne frontiere SsolERr3E .u30E530 0,295 | brute et caleulee di-
N rectement.

 

 

 

 

 

 

 

 

Le zero d’Östende serait ainsi de 0'296 au-dessous du Normal-Null, quantit@ qui
concorde tres bien avec la precedente (0289), pour autant que le Normal-Null coineide avec
l’Amsterdamsche Peil ou n’en soit que peu different. (Voir le rapport de M. von Kalmär dans
les Gomptes rendus de lu Gonference generale de Paris en 1889, p. 12, 6° alinea.)

Marcographes. — Gontrairement a ce qu/il y avait lieu d’esperer, administration
des Ponis et Ühaussees n’a pu proceder encore A la nouvelle installation du mar&ographe
d’Östende, travail qui, selon toute apparence, ne sera plus guere differe.

Un medimaremetre a 6te etabli, A la fin du mois de Juin dernier, pres de l’emplace-
ment qu’occupera ce mar&ographe, et les observations paraissent s’effeetuer actuellement
dans de bonnes conllitions.

Les telömaregraphes de Malines et d’Anvers ont et relies au röseau des nivelle-
ments.

Le colonel d’etut- major,

directeur de U Institut cartogruphique miltlaire,

B. HENNEQUIN.

(dl ul —_

ana RE MAN A|

wkuuuk

Nu taMdehl

Jsnnunmlbl aa a babe ua

 

 
