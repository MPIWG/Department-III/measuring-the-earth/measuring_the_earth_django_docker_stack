RS 10155 URN N N

 

Im

IN Am

FETTE RR TER Prem

IT

a3

 

 

39

sont contenus dans deux memoires publies en 1875 et 1879 ei dans un troisieme qui vient
de paraitre et que les honores collegues ont recu probablement ces derniers jours.

(ie troisieme memoire contient entre autres la discussion des deux dernieres mesures
de base et la comparaison exacte du metre normal avec le metre des Archives.

Au reste, la reduction de toutes les observations astronomiques est lerminde, de
meme que la compensalion du reseau. 1] aurait fallu trop de temps pour venir A bout d’une
compensation complete de tout le reseau avec ses trois bases, ce qui aurait conduit ä environ
2459 equations normales. M. Oudemans a prefer& employer une compensalion partielle et la
preuve qu’il a reussi ressort de la faible valeur des correetions qu'il a fallu ajouter aux lon-
gueurs mesurees des trois bases afın d’arriver a un accord complet. Ges corrections sont de
9, 13 et 4 unites de la septieme d&cimale dans les logarıthmes des longueurs.

Afın de donner une idee de l’exactitude de la mesure des angles, M. Oudemans a
calcule, conformement ä la proposition faite par M. le general Ferrero, l’erreur moyenne
d’une direction d’apres les &carts qui existent dans la fermeture des triangles. Il a trouve
pour cette erreur moyenne + 0,81.

Afın de se procurer un raccordement avec la triangulation & faire ulterieurement
sur !’ile de Sumatra, on arattache deux points de Sumatra au canevas de l’ile de Java.

La triangulation de Sumatra a et commencee en 1883 et poursuivie regulierement
avec un personnel compose de cing ofliciers, douze sous-officiers et un certain nombre de
malais comme aides et heliotropistes. La Lriangulation entreprise dans le m&me but que
celle de Java comprend, outre la triangulation primaire, aussi des triangulations secondaires.
Pour la triangulation primaire, on a ernploy& des l’origine des instruments et des methodes
d’observation propres A donner l’exactitude necessaire pour la mesure des degres, de sorte
que dans la suite on aura, a cöte des degres mesures dans le P&rou, une seconde triangula-
tion ex&cutde sous l’Eequateur.

Pour le moment, on donne une courte notice sur l’organisation de cette triangu-
lation, qu’on pourra joindre aux Comptes-Rendus de cette Conference; & l’avenir on espere
pouvoir donner chaque annde un apercu des progres de cette entreprise.

(Voir Travaux de triangulation de Java, Annexe B. VIII et Travaux executes sur
Pile de Sumatra, Annexe B. VIIIe.)

sur Pinvitalion de M. le President, M. le Prof. Helmert lit le Rapport sur les travaux
executes l’annee derniere par I’Institut geodesique prussien, qu'il demande la permission de
completer jusqu’ä la fin de l’annde pour la publication des CGomptes-Rendus. Son Rapport
constate entre autres l’achevement des recherches sur les deviations de la verticale dans la
region du Harz, qui permetient maintenant d’essayer la construction de cette partie du
gcoide.

I! rend compte en outre de quelques observalions destindes aux d&terminations de
la difference de longitude entre Berlin et Potsdam, oü la methode d’observer les passages
des &toiles au moyen du micrometre de Repsold a donn& les meilleurs resultats, en faisant
disparaitre presque entierement les equations personnelles. (Voir Annexe B. IXa.)
