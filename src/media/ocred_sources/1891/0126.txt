 

118

Les surfaces terminales de la Toise ont 616 ensuite examinees, en premier lieu celle
de droite, linscriplion etant tournde du cöl& de l’observateur dans le sens de la lecture;
cette surface correspond A la fin de la division. Cette surface R presente plusieurs taches
brun-fone&; en outre, en a au-dessous de l’intersection des diagonales, une partie endom-
magee de plusieurs millimötres carres, el en d quelques sillons.

arele durıset Inserip ton.

 

 

Fig. %

L’autre surface terminale L, A V’origine de la division, presente en c, au-dessus de
l’intersection des diagonales, un sillon profond, de plusieurs millimetres de longueur et d’une
fraction de millimötre de largeur (0"""5 environ); en outre, deux trous de la fonte en d et e
(de Amm de longueur sur '/, de millimetre de largeur environ); en f de nombreuses et fines
granulations; en y une d&pression plate peu profonde ; enfin de nombreuses taches noirätres.

+,

 

 

 

N SIEH? ef fıon ars diriser
m . a Br
ä a. 3% & |
N won,
Bra >.

Quoique la surface L ait produit sur les observateurs une impression moins favo-
rable que la surface R, mieux conservde, elle parait preösenter encore dans la region mediane
des parties suffisamment bonnes pour servir ä la mesure de la longueur.

Apres cet examen, la Toise a &t& remise dans sa boite. Elle y repose sur huit
supports et sept appuis intermediaires. Le couvercle presse sur ces quinze points. Les bouts
de la Toise ont &t& de nouveau recouverts de papier d’etain, en laissant le milieu ä decou-
vert. Aux deux extrömites, on a plac6 deux cales de bois recouvertes de papier d’etain, qui
separent la Toise de la boite;, et des ressorts plats exercent une pression mod£r6e sur les
faces terminales de la Toise.

La boite en bois a && ensuite ferm6e, et la elef remise par M. Peters au Professeur

Helmert soussigne.
Signe : G. M. PETERS.

F. R. HELMERT.
Dh. Atbrpesm

Pour les comparaisons, la Toise a &i& placde dans le comparateur universel, sur
le banc n° 1, qui est le plus rapproch& de la Regle-etalon. Elle reposait sur un support de
fonte, plan, de 4 metre de longueur, place sous son milieu, et prolonge a chaque bout
par deux fortes cales de chene, tres soigneusement ajustdes a la meme hauteur, et munies

 

ul.

ara Ta I EMI

1

un Cu

to hu!

IE

mh u aba u

u

 
