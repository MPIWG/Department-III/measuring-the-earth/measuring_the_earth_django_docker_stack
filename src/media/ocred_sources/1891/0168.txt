 

Annexe B. Ia.

ÖSTERREICH-UNGARN

BERICHT

iiber die Gradmessungs-Arbeiten der astronomisch-geodätischen Gruppe des
k. u. k. militär-seoeraphischen Institutes im Jahre 1891.

Infolge des Antrages der Direction des obigen Institutes hat das k. und k. Reiclis-
Kriegs-Ministerium, mit Erlass Abth. 5, Nr. 649, vom 10. März d. )]., angeordnel:

Die im Jahre 1891 durchzuführenden astronomischen, dann Triangulirungs- und
Nivellements-Arbeiten haben zu umlassen:

A. Die Fortsetzung des Baues der Feld-Observatorien und der Beobachtungen auf
den in Mähren, Ober- und Nieder-Oesterreich, dann im Centrum von Siebenbürgen noch
nothwendigen astronomischen Stationen Il. Ordnung.

B. Die Höhenmessungen im Anschlusse an das Präcisions-Nivellement in Ost-Galizien
und Nord-Ungarn, um für die Reambulirung die richtigen Höhen auf die Gatasterpunkte
übertragen zu können.

C. Die für Gradmessungs-Zwecke dienenden Ergänzungs-Beobachtungen im Netze
I. Ordnung in dem diesjährigen Rayon, dann auf 6 Punkten des Triangulirungs-Rayons vom
Jahre 1888 und auf 5 Punkten der Rayons 1889 und 1890; überdies, nach Zulass der Zeit,
auch die Neubeobachtung solcher Punkte nördlich und westlich von Szatmar.

D. Die Fortsetzung des Präcisions-Nivellements in Galizien und Nord-Ungarn zur
Herstellung doppelt nivellirter Schleifenschlüsse und Einbeziehung von an den nivellirten
Linien liegenden Triangulirungs-Punkten, meteorologischen Stationen und Flusspegeln.

E. Die Fortsetzung der Schwerebestimmungen in Tirol und Erweiterung dieser

Untersuchungen über die Landesgrenzen nach Bayern und Italien, worüber mit dem Erlasse
Abth. 5, Nr. 536, vom 28. Februar I. J. das Nöthige bereits angeordnet wurde.

ea a IRA EM uud ul

ar

al inkl

il.

Ja yrahmmadl kb as bad ta

 

 
