 

Na AR

LI TAT

SORT TRTRRRG LULIRINGIN EL Em dl

im

 

 

169

In Mähren auf den Punkten: Blaskov, Spilzberg, Hora, Ambrozug, und Maydenberg,

In Böhmen auf dem Punkte Markstein an der mährischen Grenze;

In Nieder-Oesterreich auf Spitlelmais und Predigstuhl, und endlich

In Ober-Oesterreich auf dem Punkte Viehbery.

Durch diese Stationen ist, mit Einschluss der schon in früheren Jahren beobachteten
Stationen Rapotitz, Jauerling und Buschberg, das astronomische Netz von der Nordgrenze
Böhmens bis an das linke Donauufer erweitert worden, und überspannt gegenwärtig die
Fläche von 3 Meridiangraden in einer beiläufigen Breite von 4 Längengraden.

Der Vorgang bei diesen Beobachtungen war ganz conform jenem der Vorjahre. Auf
den Stationen Markstein und Predigstuhl konnte wegen allzu ausgedehnter Waldungen das
Azimut nicht beobachtet werden.

b) Bureauarbeiten.

Es wurden nachstehende Arbeiten ausgeführt :

1. Berechnung der Zeitbestimmungen von 30 im Vorjahre und heuer beobachteten
astronomischen Stationen.

2. Vollständige Berechnung der Breite von 21 im Jahre 1590 beobachteten Stationen.

5. Berechnung des Azimutes von 4 Stationen.

R. von STERNECK m. p.

k. und k. Oberstlieutenant.

y. TRIGONOMETRISCHE ARBEITEN

wurden vorgenommen in der Polygonskette, welche von Lemberg gegen Süden geführt ist,
die Karpathen überschreitet und an das Netz im ehemaligen Grossfürstenihum Siebenbürgen
anschliesst.

Es sind new gebaut worden die Pyramiden auf: Hoszany, Einsiedel, Kobylicka gora,
Wiszenka, Neuka, Lysa göra, Ciuchow dzial, Opary, Bakoczyn und Tigla Moruti; ausgebes-
sert : 17 Pyramiden,

Es wurden beobachtet die Stationen : Lysa göra, Kat (Kont), Gorgan ılemski und
Läpos Üseretetö,

Nachtragsmessungen vorgenommen auf 9 Stalionen.

Im Laufe des Jahres 1892 wird der V. Band der « Astronomisch-geodätischen Arbei-
ten des k. und k. militär-geographischen Institutes » publieirt werden, enthaltend die Beob-
achtungen der Horizontalwinkel auf den Dreieckspunkte I. Ordnung in Böhmen, dann die
Polygonskette im Wiener Meridian, von der Schneekoppe bis Dalmatien.

HARTL m. p.

k. und k. Oberstlieutenant.

 
