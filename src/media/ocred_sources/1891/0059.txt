 

TR Wr

al im! TTRTTH TEL U

jun)

II

j

E
=
=
ii
=
3
=
Im
ar
FE
E
“3
m

 

36.
a.
38.
29.

2

OU.
Se
32.
30.
34,
35.
36.
Sl.
38.
AM.
4.
4.
Ad.
Ad.
43.
Ab.

41.
48.
49.
0.

rn

ro

Io.
Od.
90.
96.
37
98.
99.
60.
61.
62.
69.

>
5

-

51

Herr Hauptmann Quaglia, am milit.-geograph. Institut.

Herr Lieutenant Poggi, am milit.-geograph. Institut.

Herr Ritter Giovanni Ferrero, Chef der Ill. Abth. am milit.-geograph. Institut.

Herr Ritter Francesco Derchi, Ingenieur am milit.-geograph. Institut.

Herr Ritter Giovanni Cloza, Ingenieur am milit.-geograph. Institut.

Herr Kederigo Guarducei, Ingenieur am milit.-geograph. Institut.

Herr Arnaldo Ginevri, Ingenieur am milit.-geograph. Institut.

Herr Getulio Mariani, Ingenieur am milit.-geograph. Institut.

Herr Raffaello Grechi, Ingenieur am milit.-geograph. Institut.

Herr Lazzaro Vitale, Ingenieur am milit.-geograph. Institut.

Herr Antonio Loperfido, Ingenieur am milit.-geograph. Institut.

Herr Italo Busoni, Ingenieur am milit.-geograph. Institut.

Herr Ritter Giovanni Macarı, am milit.-geograph. Institut.

Herr Ritter Pompilio Trombetti, am milit.-geograph. Institut.

Herr Ubaldo Bettazzi, am milit.-geograph. Institut.

Herr Oberst Pietro Valle, am milit.-geograph. Institut.

Herr Umberto Valle, am milit.-geograph. Institut.

Herr Marquis Andrea Corsint.

Herr Commandeur Emanuele Artom.

Herr Commandeur Nicola D’Atri, Departements-Direktor des Gadasters.

Herr Prof. Pittei, Direktor der meteorologischen Station am Institut der höheren
Studien. |

Herr Rev. P. Giovannozzi, Direktor des Ximenischen Observatoriums.

Herr F. Meucci, Direktor am Galilei-Museum.

Herr Prof. Pietro Marchi, Präsident am Königl. technischen Institut.

Herr Artimini, Stadtrath.

Herr Burresi, Stadtrath.

Herr Dainelli, Vice-Bürgermeister von Florenz.

Herr Farallı, Stadtrath.

Herr Franchetti, Assessor im Stadtrath.

Herr Marquis Gerini, Stadtrath.

Herr Giachetti, Assessor ım Stadtrath.

Herr Levi, Stadtrath.

Herr Marchettinti, Stadtrath.

Herr Marchi, Assessor im Stadtrath.

Herr Paoli, Stadtrath.

Herr Spighi, Stadtrath.

Herr Vitta, Stadtrath.

Herr R. Bischoffsheim, Mitglied der Akademie der Wissenschaften, in Paris, eingeladen
von der Permanenten Commission.
