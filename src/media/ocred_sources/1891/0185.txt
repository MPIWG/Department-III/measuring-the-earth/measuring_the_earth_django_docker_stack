 

IE

=
iE
m
fi
3
=
3
im

 

 

Annexe B. III.

DANEMARK

Rapport sur les travaux geodesiques de lV’annee 1891.

1. STATIONS TRIGONOMETRIQUES

La revision des stations trigonomötriques mentionnde dans mon rapport de l’annee
passee a öt6 continue aux stations de Lerbjerg, Bögebjerg, Dyrebanke et Dyret. Pour le mo-
ment, on s’oceupe de la conservation de la station astronomique A Skagen en remmplagant le
pieu en ch@ne qui porte le repere, par un pilier en granit entoure de quatre reperes acces-
soires.

9. TRAVAUX ASTRONOMIQUES

Le caleul provisoire des observations de ’an dernier ä Teglhöi a donne

Latiiude de Teehör = 7 > 1 ı 5 „WU

Ce resultat, base sur l’observation de vingt couples d’etoiles d’apres la methode
d’Horrebow, ne peut pas @tre consider comme definitif, parce qu’on attend le remesurage
des döclinaisons de plusieurs des &toiles employees et la determination definitive de la divi-
sion du mieromötre. Cependant l’erreur ä craindre sur la valeur obtenue ne depassera guere
deux dixiemes de seconde.

Cette annde, on a termine les observations sur les deux stations de Lysnet et Dyre-
banke et les latitudes en seront caleul&es dans le courant de l’annde prochaine.

3. NIVELLEMENT DE PRECISION

La longueur des lignes nivel&es pendant l’annee 1891 est de 120 kılometres, dont
90 niveläös A double et en sens inverse, et 30 une fois. La somme du travail equivaut ä& 210
kilometres de nivellement simple, mais a deux lectures doubles des deux divisions de la mire
triangulaire.
ASSOC. GEOD. INTERN. — 23

 
