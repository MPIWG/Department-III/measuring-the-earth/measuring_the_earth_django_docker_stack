 

Il

Dali hm Ina

ul

Tl

=

is
=
am

=
Em
er
a
in

 

 

69

9. Von der Trigonometrischen Abtheilung der Königlichen Landesaufnahme in
jerlin: Theil IX der Abrisse, Coordinalen und Höhen. 82 Exemplare.

10. Von der Norwegischen Erdmessungs-Commission in Christiania : Geodätische
Arbeiten. Hefi VI. 125 Exemplare.

11. Von derselben : Geodätische Arbeiten. Heft VII. 125 Exemplare.

12. Von der Bayerischen Erdmessungs-Commission in München : Das Bayerische
Präcisions- Nivellement. Ste Mittherlung. 100 Exemplare.

13. Von der Italienischen Erdmessungs-Commission : Confronti e verificazioni d’A-
zimnut assolult in Milano. 100 Exemplare.

14. Von der schweizerischen geodätischen Commission : Das schweizerische Dreiecks-
nelz. V. Band. 94 Exemplare.

1% wo derselben : Proces-verbal de la 34° seance de lu Commission geodesigue
suisse. 80 Exemplare.

16. Von dem k. und k. Gradmessungs-Bureau in Wien : Astronomische Arbeiten.
II. Band. Längenbestimmungen. 119 Exemplare.

17. Von dem k. und k. Militär-geographischen Institut in Wien : Mittheilungen.
X. Band. 80 Exemplare.

18. Von der Königl. Bayerischen Erdmessungs-Commission : Nachtrag zu den Mit-
therlungen II und Ill über die Ergebnisse aus Beobachtungen der terrestrischen Refraction.
100 Exemplare.

19. Von Herrn Gomte d’Avila ın Lissabon : Memoria sobre a determinacao das coor-
denadas geographicas... em Lisboa. 50 Exemplare.

20. Von demselben : Trrangulacao fundamental. 1. 30 Exemplare.

21. Von Herrn Professor Dr. Lindhagen in Stockholm : ZLängenbestimmungen
zwischen Stockholm, Kopenhagen und Christiania. 80 Exemplare. |

22,..\on len trigonomelrischen Abtheilung der Königlichen Landesaufnahme in
Berlin : Hauptdrevecke. IV. Theil. 77 Exemplare.

23. Von Seiten der Permanenten Commission : Verhandlungen der 1889 in Paris
ubgehaltenen 9. Allgemeinen Konferenz der Internationalen Erdmessung. Gedruckt sind 62;
Exemplare, davon gelangten im Juni und Juli 1890 zur Vertheilung

130 Exemplare an die Regierungen,

>84 Exemplare an die Delegirten, Behörden, Institute, Gelehrte, Gesellschaften, etc.

100 Exemplare sind bei G. Reimer in Commission und

11 Exemplare mithin im Bestande verblieben.

Ausserdem sind für das geodätische Institut auf dessen Kosten noch ferner 195 Ab-
züge angefertigt worden.

Von derselben : Verhandlungen der 1890 in Freiburg i. B. abgehalienen Kon-

ferenz der Permunenien Commission. 625 Exemplare sind gedruckt worden ; davon gelangten

im April und Mai 1891 zur Vertheilung :

135 Exemplare an die E

378 Exemplare an die Delegirten, ac a Gelehrte, (sesellschaften, ete
