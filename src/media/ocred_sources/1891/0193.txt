 

Im ART

LITT

RITA TI ARIHGIE BRETTEN FALLE

luikıl
IT gıT

 

 

185

Nivellement de precision,

La brigade de nivellement avait ex&cute, dans les anndes precedentes, la ligne de
Tunis A Böne par Soukharras et Duvivier, ainsi que la liene de Duvivier a Kroubs pres Cons-
tantine. Celte annee, les operations sont parties de Kroubs et ont &t& dirigees sur Philippe-
ville en suivant la voie ferrde, sur un parcours de 102 kilometres, puis de Philippeville &
Böne, le long de la route nationale, sur une longueur de 98 kilometres. Elles ont ferme le
polygone Böne, Duvivier, Kroubs, Philippeville, le point de depart et d’arrivee A Böne £lant
le mödimaremetre etabli dans le port. Ce polygone a 407 kilometres de developpement et
l’erreur de fermeture est de + 105mm,

L’erreur kilometrique probable sur le nivellement effectue cette ann&e a &l& trouvee
de + 0""8 sur la voie ferrde et de + 0""7 sur la route. Mais dans cette seconde partie, les
discordances enire l’aller et le retour ont affeete une allure systematique tres nelte, quoique
de faible valeur; en fin d’operations, la somme des discordances positives — 243” 8 et la
somme des discordances nögatives — 222””6;, tandis que, sur la voie ferree, ces deux som-
mes sont respectivement + 267"”2 et — 2627.

On a installe dans le voisinage de Böne, sur un rocher voisin de la mer, un repere
fondamental de nivellement desting ä contröler dans l’avenir les variations qui peuvent se
produire dans le niveau moyen de la mer par rapport au sol ferme. Le repere est scell@ dans
le roc m&me : il a te rattach& au medimar&metre, puis recouvert d’un monolithe.

Intensile de la pesanteur.

M. le capitaine Bourgeois a mesur& la pesanteur relative, avec le pendule reversible
inversable du commandant Defforges, en six stations d’une ligne qui coupe presque perpen-

. dieulairement le haut plateau de l’Algerie, entre Philippeville et Biskra. Ges six stations sont:

Philippeville (altitude 60"), Gonstantine (670m), Ain-Yagout (850m), Batna (1040m), El Ran-
tara (540m) et Biskra (125%).

Ges Lravaux se rattachent, avec ceux qui viennent d’etre poursuivis dans les Pyrenees
Orientales, A un programme regulier d’operations ayant pour objet l’Etude du degr& de com-
pensation realise dans l’&corce terrestre autour du bassın de la Mediterranee.

3. PUBLICATIONS

Le Service göographique a fait paraitre, au commencement de V’annee, de grandes
tables de logarithmes ä huit d&cimales, contenant les logarithmes des nombres entiers de 1 &
120000, et ceux des sinus et tangentes de 10 en 10 secondes d’arc, dans le syst&me de la

division centösimale du quadrant.
ASSOC. GEOD. INTERN. — 24

 
