 

Kt

1 ca Te ITIFATA 73 Ser

AM Al

IN ERT

ll )

La tal

BL LLRENEL PRBILLLÄNIL

   

=
me
m
m
=
im

)

perdu le grand geodösien qui a fonde notre «euvre, le general Baeyer. Puis ont disparu
successivement Hansen, Peters, Oppolzer, Villarceaux, Perrier, Liagre, Secchi, Ibanez,
Respighi et d’autres, tous savanis de premier ordre, qui ont jet& un grand lustre sur notre
Association. Mais les institutions fecondes de la science survivent ä leurs fondaleurs et notre
douleur est attönude par les sucees de l’Assoeiation, qui est devenue toujours plus puissante
et plus vaste par l’adhesion de presque tous les Etats eiviliscs du globe.

«A l’epoque de notre premiere röunion dans cette ville, Florence etait la capitale
du royaume et le gouvernement tout entier avait participe A la röception des savants @lran-

oenie italien, Florence est restee un centre
puissant d’atlraction pour les hommes de la pensee.

« En 1869, M. Ubaldino Peruzzi, dont les cendres sont encore chaudes, &tait & la tete
de la Munieipalit& de Florence. Cet homme de pensde et d’aclion peul Eire mis au meme
rang que le gensral Baeyer pour l’importance de Foeuvre qu/il a laissce derriere luı.

«Florentin d’adoption, je devrais peut-etre m’abstenir de glorifier notre ville, mais
Piämontais de naissance, je puis relever ses merites sans &tre aceusd d’esprit de clocher.
Florence, compardeä (’autres grandes cits, presente cet avanlage que la ville moderne n’est
pas indigne de Vaart de ses anettres. lcı, tout ce qu’il y a de nouveau ne constilue pas une
offense aux modeles laiss6s par les Arnolfo, les Brunellesco, les Giotto, les Alberti, les
Michelangelo et tant d’autres. Florence s’est agrandıe en restant toujours un modele de bon

gers; mais, favorisee par la nature et par le

gott et de finesse arlistique.
«(est lä un grand merite des habitants de cette ville, mais ce resultat est dü surtout

aux efforts persöveranis de Peruzzi qui, au prix de mille diffieultes, avalt rendu Florence
digne d’ötre la capitale du pays le plus artistique du monde.

« Ainsi, de meme que les succes de notre Association nous rappellent le nom du
oöneral Baeyer, chaque pierre de la nouvelle Florence nous parle d’Ubaldino Peruzzi. Voilä
certes une pensöe rejouissante qui atlönue en nous l’amertume de sa mort. »

M. Hirsch repond A ces @loquents discours par ces paroles :

« Monsieur le Ministre, Messieurs,

« Qest la troisieme fois, depuis son origine, que l’Associalion geodesique Interna-
tionale a ’honneur de sieger en ltalie.

« La premiere fois, en 1869, lorsque son celebre fondateur, le general Baeyer vivail
encore, la Commission permanente s’est rönnie dans cette splendide ville des fleurs et des arts,
qui se trouvalt alors sous la premiere magistrature de Villustre Peruzzi, auquel nous aurons
demain la douleur de rendre les derniers devoirs. Fils d’une des plus anciennes familles de
Florenee, &leve comme ingönieur des « Ponts et Ühaussces » de Paris, ce noble patriote,
qui a eu tous les honneurs de son pays et qui a cependant toujours dedaigne les avanlages
materiels du pouvoir, est acquis, & cöte de tanl d’autres titres de gloire, celui d’avoir erde
la nouvelle Florence, sans compromeltre en rien le caractere sı admirablement pittoresque

ASSOC. GEOD. INTERN. — 2

 
