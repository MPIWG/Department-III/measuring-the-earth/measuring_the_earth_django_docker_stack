 

5

capitale, vous y trouvez encore le centre «des dtudes geodesiques d’Italie dans Institut
göographique militaire, qui a rendu d’importanis services a votre Association, A cöt& des
travaux qu’il a accomplis pour la cartographie militaire et scientifique. L’impulsion donnee A
cet Institut par son &minent directeur, M.le general Annibal Ferrero, est sı remarquable que,
appel& dejä par la confiance des delegues italiens ä la tete de la Commission geodesique, le
göncral Ferrero a presidö, en cette qualite, la Conference generale de Rome en 1885, et
aujourd’hui il a prösid& ä Porganisation de la Conference actuelle dans ce palais historique
de la Seigneurie de Florence, dans lequel j’ai ’honneur de vous offrir, au nom du Gonseil
municipal, l’usage de cette salle pour vos scances.

« Je suis heureux que le Gouvernement royal aussi soil inlervenu ponr saluer votre
arrivse parıni nous, en prouvant ainsi qu'il fait le plus grand cas des savantes recherches de
l’Associalion g&odesique el en se faisant l’interprete de la pensee de notre auguste Souverain,
protecteur empresse des hautes &tudes el de tous les progräs de la civilisation.

«Je me röjouis ögalement que la parole du Gouvernement royal vous soil apporlee
par Son Excellence le Ministre de lInstruction publique, M. le Sönateur Villari, historien
cölebre de Machiavel, le grand Seertaire de la Republique de Florence, et de Savonarole, le
saint moine qui, sur la place m&me situde au-dessous de nous, a paye de sa vie sur le bücher
la fougue de sa parole ardente pr&chant la liberts, dans une &poque douloureuse (’asservis-
sement, A un peuple qui, aujourd’hui, redevenu libre, a &lev& un monument & la memoire
imperissable du martyr, dans la salle m&me oü il proposait de tenir les seances Ju Grand
Gonseil.

« (est ainsi que deux hommes illustres, chers a Florence qui les considere comme
des eitoyens d’adoption, s’unissent A moi pour vous recevoir, hötes honores et sympalhiques,
dans cette ville que votre Association universelle honore pour la seconde fois en Iui envoyant
P’ölite de ses delöguss, accourus de toutes les parties de ’Europe sur les bords de ’Arno.

« Que votre söjour au milieu de nous vous soit agreable el que volre @uvre s’acheve
4 votre satisfaction, avec tous les resultats utiles que vous desirez pour la science et pour le
monde. Vous ajouterez ainsi de nouveaux fleurons A votre couronne et vous laisserez un
autre souvenir glorieux A ajouter A tous cenx qui se raltachent ä ce palaıs historique. »

M. le general Ferrero s’exprime ensuile dans ces termes :

« Apres les öloquents discours de Son Excellence le Ministre de ’Instruetion publique
et de Pillustre reprösentant de la ville de Florence, permettez-moi d’ajouter auelques mols,
en ma qualite de president de la Commission geodesique italienne.

« Comme vous l’avez entendu par les orateurs qui viennent de prendre la parole,
c’est en 1869 que votre Commission permanente s’est r&unie & Florence pour la premiere
fois. J’ai pens& qu’il y aurait un certain intöret A comparer sur quelques points ces deux
Conferences geodssiques. A la premiere r&union assistaient les hommes illustres qui ont eree
notre Association. En 1869, l’astronome Donati inaugurait en votre presence P’Observatoire
d’Arcetri; quatre ans plus tard, le celebre savant Alorentin n’dtait plus. Des lors, nous avons

=
=:
a
Pe
®

=:
«

ET

 

i

Int

abnımlyı u

ur

its prall ben a bl

iu

 
