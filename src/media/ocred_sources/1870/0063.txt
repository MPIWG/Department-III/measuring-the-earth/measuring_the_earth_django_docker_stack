 

TR Ten ETT ITT

vr

ze A oe

und Sorgfalt ausgeführt und später für die Veröffentlichung bestimmt hatte, in das Grab
gesunken.

Den Gradmessungsarbeiten hat er sich vom Beginn derselben an und zwar seit dem
Frühjahr 1862 mit grosser Liebe, Aufopferung und Hingebung gewidmet und selbst nur wenige
Tage vor seinem nicht geahnten Hinscheiden hat er noch den Wunsch ausgesprochen, dass
ihm wenigstens noch ein paar Jahre das Leben erhalten bleiben möge, um die sächsischen
Gradmessungsarbeiten mit vollenden helfen zu können. Leider sollte ihm, dem Vortrefflichen,
nicht vergönnt sein, das Ende dieser mit so viel Lust und Liebe gepflegten Arbeiten zu er-
leben und die Früchte seines Wirkens zu geniessen.

In der Königlich Sächsischen Commission für die Europäische Gradmessung war er
ein treuer Mitarbeiter, ein lieber, biederer College und der Unterzeichnete beklagt mit seinem
Hingange den Verlust eines aufrichtigen, väterlichen Freundes und wohlwollenden Berathers.

Die unter der speciellen Leitung des Verstorbenen durch die Assistenten Richter,
Brause und Hausse ausgeführten Nivellirungsarbeiten wurden im Jahre 1870 soviel als
möglich in der Weise ausgeführt, wie sie von ihm nach seinem vorjährigen Bericht disponirt
worden sind. Nur ist auch hier der Krieg nicht ohne Nachwirkung geblieben, indem nur ein
Theil der aufgestellten Linien hat vollendet werden können. Diese Linien betreffen den süd-
lich von der Linie Grossenhain— Radeburg — Camenz — Bautzen — Löbau— Reichenbach lie-
genden Theil von Sachsen bis zur Elbe respective bis zur Landesgrenze mit Böhmen, und

es sind in das Nivellementsnetz die Gradmessungsstationen Lausche und Valtenberg, sowie die

Basispunkte bei Grossenhain mit eingebunden worden.

Für das laufende Jahr bleibt demnach noch zu vollenden übrig der nördlich von ge-
nannten Städten liegende Theil bis zur Landesgrenze mit Preussen, sowie einige Ergänzungs-
linien zwischen Grossenhain und Strehla, bei Wurzen, bei Leipzig und im Voigtlande und
man hofft, diese Arbeiten im gegenwärtigen Jahre soweit zum Abschluss zu bringen, dass
alsdann die Ausgleichung des ganzen Netzes vorgenommen werden kann.

Von den beiden Commissaren, dem Prof. Dr. Bruhns und dem Unterzeichneten, ist
dem Königlichen Sächsischen Ministerium der Finanzen vorgeschlagen worden, die vom Ober-
bergrath Prof. Dr. Weisbach übernommenen Arbeiten, nämlich die Nivellirungen und die
Basismessung in der Weise unter beide Commissare zu vertheilen, dass die Nivellementsar-
beiten in der bisherigen Weise, also im Sinne des Herrn Oberbergraths, von den. beiden
Assistenten Richter und Brause unter der Leitung des Unterzeichneten vollendet und ausge-
glichen, von beiden Commissaren aber im Namen Weisbachs publieirt werden, dass dagegen
die Vorbereitungen zur Basismessung der Prof. Dr. Bruhns übernimmt, während über die
Ausführung der Basismessung selbst beide Commissare sich später näher verabreden.

Was die von dem Unterzeichneten auszuführenden trigonometrischen Arbeiten
anlangt, so sind dieselben im verflossenen Jahre keineswegs so vorgeschritten, als eigentlich zu
wünschen gewesen wäre. Die Fortschritte der Arbeiten sind gehemmt worden durch den Mangel

der nöthigen Assistenz, durch die eingetretenen Kriegsereignisse und durch üble Witterung.

8 *

 
