Er Tree Tee |

TTmorm NT

mu art

II

Tey TIT

&
=
a
43
;
iR
2>
Re
®
A
43
oe
22
ar

 

Annexe VIII.

 

RAPPORT

SUR
L'ÉTAT DES TRAVAUX POUR LA DÉTERMINATION DU NIVEAU MOYEN DES MERS
PAR

M. IBANEZ.

Poa obtenir les données nécessaires à la rédaction du rapport, j'ai adressé à M. M.
les Délégués des différentes Nations maritimes un questionnaire sous forme de tableau.
Ce questionnaire, divisé en quatre sections, comprend les renseignements suivants:
Situation de Vappareil: mer et localité dans lesquelles l’appareil enregistreur
est situé; nature et forme de la côte à l'endroit, afin de savoir jusqu'à quel point
le niveau déterminé par l'appareil peut être considéré comme le niveau au large.
Description de lappareil: Système employé, dimensions du papier sur lequel
se tracent les courbes; l'échelle de la courbe et les chiffres ou lectures que donne
l'appareil dans le cas où il est en même temps compteur, toutes ces données étant d’une
grande importance pour déterminer la précision qu'on pourrait attendre des résultats.
Installation. Dans cette section deux questions distinctes étaient comprises:

l'installation par rapport au terrain même et la communication de ce mar

éographe
avec les eaux.

Pour la première question j'ai démandé les dimensions du puits; l'espèce, la forme
et les dimensions du canal qui le met en communication avec la mer; la position du
canal par rapport aux eaux, en indiquant son inclinaison dans le sens vertical et la distance
de sa bouche au fond de la mer, ainsi que l’exposition de la même bouche par .rapport
aux Courants dominants, et l'intensité de ces courants. J'avais également demandé les
causes ordinaires d’obstruction que la nature du fond ou des matières en suspens dans
Veau pourraient occasionner en altérant le niveau donné par l'appareil.

Pour la seconde question, soit la Jonction des diverses maréographes entre-eux, il
était demandé quel est le plan de comparaison de l'appareil, l'altitude de ce zéro
spécial et sa surface de repère, en désignant la méthode suivie pour sa détermination.

Résultats. Cette section se divise en deux parties: la première, intitulée résultats
généraux, comprend la durée de la période des observations, et comme résultat obtenu les
hauteurs de l’eau maxima et minima observées pendant la dite période, ainsi que la hau-
teur moyenne résultant de toutes les observations faites. Sous l'alinéa résultats par

 
