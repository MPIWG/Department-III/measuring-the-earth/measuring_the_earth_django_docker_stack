 

dans une certaine mesure, en diminuant l’influence des erreurs de division des mires
et des erreurs fortuites d’observation; mais l’erreur systématique du tassement, agissant
toujours dans le méme sens et s’accumulant dans toutes les stations, reste dans la
moyenne de pareilles opérations. Il serait donc peut-être indiqué de compléter les
anciennes règles, en recommandant aux directeurs de ces opérations de les faire répéter
désormais dans le sens inverse, où cela n’est pas fait encore.

Quant au nombre, à la distance et à la nature des repères sur lequels on s’est
abstenu, avec raison, de fixer des règles, il règne une assez grande diversité dans les
différents pays. Presque partout on a cependant employé deux catégories de repères,
ceux de premier ordre en métal, fixés surtout à des monuments et constructions solides
et les autres de second ordre, scellés ou même seulement peints sur des pierres ou des
rochers là où les besoins de l'opération l’exigeaient. Les repères en métal sont ordi-
nairement des cylindres en bronze, en laiton on en fer; mais il y en a deux espèces,
les uns sont des cylindres métalliques scellés horizontalement et offrant un trait hori-
zontal.à la visée directe de la lunette de l’opérateur, comme en Hesse, dans le Wurtem-
berg, en Prusse, en France, dans les Pays-Bas, en Italie et en Autriche; les autres
sont des cylindres scellés verticalement dans des édifices ou des rochers et offrant une
surface polie à fleur du sol pour y poser la mire, comme en Espagne et en Suisse. Les
deux systèmes ont leurs avantages; les cylindres horizontaux dans les murs des édifices
sont plus en an contre les voleurs et les degradations de la stupide gaminerie et
moins exposés 4 l’usure par les influences atmospheriques; par contre, ils sont sujets
aux tassements et variations de niveau qui peuvent se produire dans les murs méme des
bâtiments solides, dans une plus forte mesure que les cylindres scellés dans le rocher
naturel ou dans les moellons des soubassements, des seuils etc. des monuments.

Quant aux repères de second ordre qu’on établit naturellement aux points dans
lesquels on interrompt journellement les opérations, il est à regretter que dans quelques
pays on n’ait rien fait pour les conserver au delà du jour suivant, ou qu’on se soit con-
tenté de les peindre simplement à l’huile qui naturellement s ’efface après quelques mois
ou années. Il est si facile de les marquer d’une manière plus durable par des entailles
faites au ciseau qui laissent intacte le point même où la mire a reposé, et qu'on peut
faire exécuter par les aides pendant les jours de mauvais temps.

Voici le nombre des repères par pays, auxquels je joins la distance moyenne:

1% ordre. 2™ ordre. Total. Distance moyenne.
i, Bavière... ». 17 1520 1597 1228
2. Hesse ye or 64 ? 64 9.4
>. Prüsser. ...., 1965 ? 1965 3.4
D Saxe no à 4 1120 1120 2
5. Wurtemberg . . 60 700 760 2.4
6. Auttiche 43... 1329 2 1329 5.4

 

 

ats to 3495 3340 6835

 

i
a
i
3
;

tit

Je eirmutrns

vec ah A ae M eh se à

 
