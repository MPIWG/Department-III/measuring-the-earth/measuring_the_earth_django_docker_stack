 

 

 

26

 

 

 

 

 

 

 

 

 

Grossbritannien.
(EP |
Ne | Indication des ue. Longi- | Époque. | Directeur Instrument. | Remarques.
Points. tude. | | et Observateurs. |
\ Ba len a
128 | Horton’s Gazebo. . 50°31’36”| 15°42'22”| | |
129 | Howth Hill . . . .. 53 29 24 | 11 35 49 | 1829—44 Génér. Portlock, Gordon,  |Ramsden de3 p.|
I Théodol. de 7”.|
130 | Hungry Hill . . . .|51 41 13 | 7 52 18 | 1832—48 Génér. Portlock. * (Ramsden de p.
131 | Ingleborough . . . .154 9 59 |15 16 0 | 1807 | |
132 |Inkpen Beacon .. 51 21 9 |16 11 57 | 1844 Caporals Stewart, Cosgrove. id. |
133 | Jara North Pap ..155 54 8 |11 39 37 | 1847 Serg. Donelan. | id. |
134 | Karnbonellis . . . .| 50 10 57 |12 26 7 1796 Génér. Mudge. | id. |
135 |Karn Galver.... 50 955/12 3 41 1850 Caporal Wotherspoon. Ramsdende18”.
136 | Karnminnis . . . .. 50-1141 | 12 7 49 | 1845 Serg. Donelan. Ramsden de3p.
137 | Keeper ..- - -.. B24 (5 | 9 24 10 | 1830 Génér. Portlock. id.
138 | Kellie Law ..... HG ILES ı 14 53 3 | 1847 Serg. Winzer. id. |
139 | Keysoe Spire 62115 0 17 14 9 | 1843 Serg. Steel. Ramsdende18”.
140 | King’s Arbour . . . 51 28 47 | 17 12 50 | 1799 Génér. Mudge. Ramsden de3p.|
141 Kinpure... . 0. 33.10.41 11 19 56 | 1829 Gordon. | id.
122 Knock... ..... 57 35 3 |14 53 20 | 1814 Génér. Colby, Gardner. id. |
143 | Knockalongy . . . . 54 11 39 | 8 54 15 | 1828 Lieut. Murphy. Ramsdendel8”.
144 |Knockanaffrin . . . 52 17 20 |10 4 56 | 1829 Génér. Portlock. |Ramsden de3p.
145) Knocklayd ..... 55 9 48 | 11 24 48 | 1827 id. | id. |
146 | Knockmealdown . . | 52 13 40 | 9 44 54 | | |
147 | Knocknadober . Di 59 85, 7 20.91 | 1840 Serg. Donelan. Hhéod de, 121
148 | Knocknagante . 51 53 37 | 7 48 48 | 1840 Beale. id |
149 Knocknaskeagh . . 52 6 26 | 9 13 50 | 1831 Génér. Portlock. Ramsden de3p |
150 | Lawshall . . . . .. 02 018 8 2217| 1843 Serg. Donelan. id.
151) Laxfield. . . . : sale 7 119 | 60 1845 Serg. Bay. | Troughton de |
| 2 pieds.
152 | Layton Hill. . . .. of 16 49 [5 38 17 | 1817 Gardner. Ramsden de 3p
153 | Leith Hill. . . . .. 51 10 34 | 17 17 35 | 1822 -44 Génér. Colby, Capits. Kater, id. |
ME Gardner, Sere. Donelan. |
154|Lincoln....... 09 14 317 Al 1842 Lieut, da Costa. Troughton de.
| 2 pieds
1152 | Little Sterling. . .|57 27 32 |15 51 9 | 1814, 50 Gardner, Serg. Steel. Ramsden de3p.
| Théodol. de 7”.
88b | Littledown Down Sue 59 116 27 Al 1846 Sere. Steel. Ramsden del18".| Nahe bei 88.
155|Llanelian...... 03 192] 15 55 59 1805 Génér, Colby. Ramsden de 3p.
156} Longmount..... be 32 40 | 14 47 2 | 1843 Lieut. Lunyken, Caporal id |
Stewart. |
157 | Basede Lough Fogle |55 9 6 |10 43 55 1828 Capit. Henderson, Lieuts. Troughton de
Terme Nord | Murphy, Mould. | (2 pieds.
158 | Base de Lough Fogle | 55 2 34 |10 39 24 | 1829 Génér. Pringle, Lieuts. Mur- id
Terme Sud phy, Henderson et Mould. | |
159 | Lumsden . . . ... 60154197 | 15 2729 1846 Serg. Winzer. Ramsden de3p.
-160| Lundy Island... ./51 10 2 |12 59 29 1845 Caporal Stewart. id. |
16 Iym ......:.. 0245 417 1543| 1843 Serg. Bay. Troughton de
2pieds.. Li]
162 | Lyons Hill . . . .. Boy 25 \11 7 43 1829 Génér. Portlock, ‘Ramsden de3p.|
163\Maker. ......-. 50 20 50 |13 28 44 1846 Serg. Donelan. id.
164 | Malvern. . . . . .. »2 6 17 1519 31 1844 Caporals Cosgrove, Stewart. id.
165 | Mamsuil. . . . . .. a1 16 18 | 12 32 37 1848 Serg. Winzer. | id.
166 | Mendip . .. .... wis 7,15 710 1844 Oaporal Stewart. id.
167 |\Merrick....... bd) 821.13 11 46 1852 Serg. Jenkins. | id. |
168 | Merrington . . . .. 54 40 89 116 4 10 1854 Cahill. |Theod. de 12”.
169 | Mickfield . . . . .. = 0 AA, 134, 15 1845 Bere, Steel. |Ramsdendel8”.
1701 Milk Hill... ... 51 29 34 15 48 48 1850 Serg. Donelan. ‘Ramsden de3 p.
171| Base de Misterton | 53 31 46 |16 45 31 1801 Woolcot, id. |
Carr, Terme Nord
172 | Base de Misterton | 53 27 29 | 16 44 23 1801 id. id.
Carr, Terme Sud. |
173 | Moelfre Issa. . 08 14149 | 14 5 32 1806 Génér. Colby, Woolcot. id.
174 | Monach . . : . . .. 68121122 | 11 21 16 1839 Col. Robinson, Capit. Hornby. | id.
175 | Mordington . . . .. 55 48 928 | 15 35 36 | 1846 Serg. Winzer. | id.
\ |

 

 

eerste ak ue 2e MA a a a

 
