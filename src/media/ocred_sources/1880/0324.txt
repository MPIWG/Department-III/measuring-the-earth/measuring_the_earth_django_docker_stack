 

Tan re as re

Seer Tee rotes Re ea

NS EST UP PP SET a SAA RAS SO ST SRE RSR RE EPP TT SEN EMTS ETSY ESE

ar
a
ii
a
if

16

Airy, (Sir) George Biddell. On a problem of geodesy. (Phil. Mag. XXXVI, 1850,
pp. 96—99.)

— On the determination of the longitude of the observatory of Cambridge by
means of galvanical signals. (Astronom. Soc. Month. Not. XIII, 1852—53,
pp. 248—252.)

— Sur la différence de longitude des observatoires de Bruxelles et de Greenwich
déterminée par des signaux galvan. Traduit de l'Anglais. Bruxelles, 1857.

— On the difference of longitude between the observatories of Brussels and
Greenwich, as determined by galvanical signals. . (Astron. Soc. Month.
Nor XV, 1253-54, pp. 246 49; Astron Soc: Mem. XXIV, 1856,
pp. 1—28.)

— Note respecting the recent Pendulum experiments in the Harton colliery.
(Astron. Soc. Month. Not. XV, 185455, p. 46. Phil. Trans. 1856, pp.
297—356.)

— Lecture on the pendulum experiments at Harton pit. London 1855.

— On the Pendulum experiments lately made inthe Harton colliery for ascer-
taining the mean density of the earth. (Royal Inst. Proc. IT, 1854—58,
pp. 17 22; Anmnal. de Chimie XLII, 1855, pp. 381—383.)

— On the computation of the effect of the Attraction of mountain Masses, as
disturbing the apparent astronomical latitude of stations. in Geodetic Sur-
veys. (Phil. Trans. 1855, pp. 101—104; Astron. Soc. Month. Not. XVI,
1855—56, pp. 42—43.)

— Remarks upon certain cases of pers. equations (Month. Not., XVI, 1856.)

_ Account of Pendulum Experiments undertaken in the Harton Colliery for the

purpose of determining the mean density of the earth. (Suppl. etc. Phil.
Trans. 1856, pp. 297—356. Poggend. Annal. XCVII, 1856, pp. 599—605.)
— Supplement to the Account of the Experiments in the Harton Colliery.
Experim. at Greenw. Obs. for Temperature constant. (Phil. Trans. 1856,
pp. 343 — 55.)
_ Determination of the longitude of Valentia in Ireland by Galvanic Signals
in the Summer of 1862. (Append. to the Greenw. astron. observ. 1862.)
— On the algebraical and numerical theory of errors of observations and the
combination of observations. 2. edit. revised. London 1875.
Airy et Leverrier. Nouvelle Détermination de longitude entre les observatoires de
Paris et Greenwich. Paris 1854. (C. R. XXXIX, pp. 552—66.)
Baily, F. Report on the Pendulum Experim. made by the late Capt. H. Foster ete.
(Astr. Soc. Mem. VIJ, pp. 1—378).
Short Account of two invariable Pendulums, the one of Iron and the other
Or Copper, (Asim. coc, Month: Not, 1, 1827—30, pp. 78—30.)

 

j
4
i
i

pry vy

 
