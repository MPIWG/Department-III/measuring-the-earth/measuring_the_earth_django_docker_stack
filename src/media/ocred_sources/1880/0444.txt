 

PTE CRE EN DO PP RER RE AO AE ES
) x « he

a menge

i
i
N
}
Hy
fk
|
|

PENS SSS ARORA SEE ASSP ELEN ORGS SEEN TE 5

28

In den folgenden Jahren sind noch nachstehende Anschlusspunkte in Aussicht
genommen, die aber noch zum Theile der Vereinbarung mit den Nachbarstaaten bedürfen:

a. für Russland: Brody, Podwoloczyska, Nowoselica,

D. |, Preussen: Zittau,

€:  pachsen: Zittau, Bodenbach, Annaberg, Franzensbad,
d. „ Bayern: Fürth, Mittenwald, Bregenz,

e. ,, die Schweiz: Martinsbruck,

1. „Italien: Borghetto, Cormons,

& ., Serbien: Semlin, Basias, Orsova,

h‘,, Rumänien: Verciorova, Predeal, Suczawa.

Unser adriatisches Meer wurde bis nun durch die Nivellements in 3 Punkten
erreicht, d. i. in Triest, Pola und Fiume.
In Triest und Pola sind Mareographen in Thätigkeit; der in Fiume früher in

Thätigkeit gewesene Mareograph kann erst nach Vollendung des Hafenbaves wieder auf-

gestellt werden.

Anfänglich wurde die Höhenmarke im Raume des selbstregistrirenden Fluth-
messers im Finanz-Wachtgebäude am Molo Sartorio in Triest als Ausgangshöhenmarke
betrachtet.

Die Seehöhe dieser Ausgangshöhenmarke ist nach Ermittlung (aus 14jährigen
Aufzeichnungen des genannten Mareographen) und Angabe des Herrn Professors Dr. Tarolfi
an der nautischen Akademie in Triest:

+ 373520 (über dem Mittelwasser),

siehe einer „bräcisions-Nivellement in und um Wien, ausgeführt in den Jahren 1876
und 1877 von der Triangulirungs-Calcul-Abtheilung des Le k. militär-geographischen In-
stituts, Zeitschrift des österreichischen Ingenieur- und Architecten-Vereins, Heft 6 und 7,
ee 1878.“

Es wurde diesem nach unser provisorischer Vergleichshorizont 3° 352 unter der
genannten Höhenmarke in Triest gewählt.

Auf diesen sind sämmtliche Coten bezogen, und als vorläufige Seehöhen be-
zeichnet worden. -

Nach Schaffung des früher erwähnten Hauptfixpunktes an der Drauthalbahn
zwischen Maria Rast und Taal erscheint unser Vergleichshorizont verlässlicher durch
diesen fixirt und zwar kommt dem Hauptfixpunkte in Bezug auf unseren provisorischen
Vergleichshorizont die Cote (Seehöhe) 295”56 vorläufig zu.

Anmerkung.

Unser Messing-Normalmeter Me (Eigenthum der Triangulirungs-Calcul-Abtheilung),
welcher in der im Berichte für das Jahr 1878 angeführten Weise zu den periodischen

j
3
i
i
i
j

 

dus à haha A ch a

 
