Te en Te

4 RT PPT. Y PTT mY |

Im rin) Kl

pa mt

Par Uae a ya

A oleh

55
Dans ce calcul on a pris pour base de départ la longueur du côté Tetica —
Mulhacen, obtenu en partant des deux bases de Madridejos et d’Arcos. On trouve ainsi
pour la longueur du côté: |
M’Sabiha—Rilhaoussen: . irn. da va as an, 105179™11
En partant de la base d’Alger, par la chaine algérienne on obtient
pour la longueur de ce cöte

M’Sabiha—Filhaoussen 3, 20517994

différence . . 0.80
: Jn80
soit — au

105180 de la longueur totale.

1
230000
Ce résultat pourra étre modifié par la compensation ultérieure des deux réseaux,
mais nous n’hésitons pas A le signaler & l’attention de l’Association.
La jonction géodésique de l'Espagne avec l’Algerie est ainsi réalisée de la ma-
nière la plus heureuse et l'Association est désormais en possession vers l’ouest de
l'Europe d’un arc de méridien de 289,

Barraquer. Perrier,

Jonction Astronomique de PAlgérie avec l’Espagne.

Lorsque la jonction de l'Espagne avec l'Algérie fut décidée, nous fûmes frappés
de l'intérêt qu’il ÿ aurait à compléter l'opération purement géodésique en reliant entre
eux les réseaux astronomiques des deux pays. Nous avions déjà un grand polygone de
longitudes comprenant Alger- Marseille -Paris-Madrid, pourquoi ne tenterions nous pas
de le fermer en lui faisant passer la mer, ainsi qu’à nos triangles? Il n’y a pas de
câble sous-marin entre l'Algérie et l'Espagne; maïs nous pourrions peut-être y suppléer
par des signaux lumineux que nous avions déjà tout organisés pour nos triangles.

Malgré l'importance extrême de la première opération, nous avons tenu presque
autant à la partie astronomique et nous n’avons pas hésité à prolonger nos travaux de
plusieurs semaines, malgré les difficultés dont on pourra se faire une idée en se rap-
pelant les catastrophes atmosphériques qui sont venues fondre sure l'Espagne pendant
l'automne de 1879.

M. Merino occupait la station de Tetica en Espagne; M. Perrier celle de M’Sa-
biha en Algérie. Les deux stations géodésiques avaient été, en quelques jours, trans-
formées en observatoires astronomiques temporaires: les deux observateurs étaient pour-
vus de cercles méridiens et d'appareils identiques pour l'enregistrement électrique des
observations d'étoiles et des signaux lumineux.

Pour comparer entre elles les pendules de Tetica et de M’Sabiha, nous avons
eu recours, Comme nous venons de l'indiquer à l’échange réciproque, par dessus la Mé-

 
