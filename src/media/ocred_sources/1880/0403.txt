— pa

1 GRANT

AVANTE

ren gm tine

Dr

DIT PE)

LS

95

 

Stebnitzky, J. Ueber die Ablenkung der Lothlinie durch die Anziehung der kaukasi-
schen Berge. (Bull. de l’Acad. de St. Petersb. 1871, pp. 232—44.)
— Ueber die geographische Lage und die absolute Höhe der Stadt Teheran.
(A. N. 2113.) 2 1870, |
— Neue Bestimmung der Länge und Breite von Constantinopel. Aus dem rus-
sischen Manuscript übersetzt v. Block. (A. N. 2349.)

CTeÖöHRrNkin. Onpexbrenie pasHOCTI KoAToTE Koncranranonozs-Orecca. Mopcroï
Uôopauxr. Toms 1880. C.-Tlerepôyprs.

(Stebnitzky.) (Längen-Differenz-Bestimmung zwischen Konstantinopel und Odessa. Journal
für Marine. Juli 1880. St. Petersb.).

— HaGxrronenis Haye rauaniamr MOBOPOTHBIXS MAATHUKOBS, HPOHSBeNeHHLIA
BB Tnoruch. Upuaom. xp XXXVI tomy Ban. Wun. Axag. Hays.
N° 1. Iino. Ie0 5

(Beobachtungen der Pendel-Schwingungen in Tiflis. Suppl. zu dem XXXVIII
Bande der Mem. der k. Akad. der Wissens. Nr. 1. St. Petersb. 1880).
Struve, F. G. W. Dissertatio inauguralis de geographica positione speculae astron.
Dorpatensis. Mitaviae, 1813.

—- Nachricht von der trigonometrischen Vermessung Livlands. 1817. (Lindenau
u. Bohnenberger’s Zeitschrift für Astronomie 4 und Astr. Jahrb. v. Bode,
1819 u. 20)

— Nachricht von der russischen Gradmess. 1822. (ANG 5.73200, 33.)

— Sur la mesure des degrés du méridien de Dorpat. (Rejection de la mesure
des angles par la répétition. Réfutation de M. Amaici sur l’inexactitude
des lectures sur les cercles méridiens à l'aide des verniers. 1824. (Zach,
corresp. astron. 11.)

— Vorläufiger Bericht von der russischen Gradmess. etc. während d. J. 1821
bis 27 in den Ostseeprovinzen des Reichs ausgeführt. Dorpat, 1827.

— Resultate der Gradmess. in den Ostseeprovinzen Russlands. 1829. (A. N.
164.

— a der beiden in den Ostseeprovinzen und in Lithauen bearbeiteten
Bogen der russ. Gradmess. 1832. (Mém. de l’Acad. de St. Petersb. und
A. Ne 1833.)

— Beschreibung der 1821—31 ausgeführten Breitengradmess. in den Ostsee-
provinzen Russlands, 2 Bde. Dorpat, 1831.

— Ueber die neuesten geoditischen Arbeiten in Russland etc. Dorpat. Jahr-
buch 1833.

— Anwendung des Durchgangs-Instrumentes für die geograph. Ortsbestimmungen.
St. Petersb. 1833. (Französisch ebenda, 1838.)

— Bericht über die Fortsetzung der russ. Gradmess. nach Norden. St. Petersh.
1836.

 
