 

70

parties. Voici les différences entre les deux mesures des sections comme elles ont été
trouvées provisoirement :

1) + 071326
2) 1 450
3) — 0. 152
4) — 0.265
5) — 0.020
6) + 0.684
7) — 9.065
8) — 0. 463
9) + 0.064
10) — 0. 431
d’ou résulte erreur moyenne = = 0™™891

|

et l’erreur probable.. 20.60]

c'est-à-dire =. de toute la longueur.

En 1880 la base de Berlin a été mesurée de nouveau avec l’appareil de Brunner.
Elle est située sur la chaussée de Berlin à Zossen, entre les villages Mariendorf et
Lichtenrade et elle a une longueur de 2336.3 m. La ligne n'étant éloignée que de deux
mètres du bord de la chaussée, celle-ci n’avait pas assez d’espace pour l'appareil nou-
veau, d'autant plus que l’espace était diminuée par de grands arbres, qui se sont dé-
veloppés depuis 1846, où la première mesure a eu lieu. Il était donc nécessaire de
faire une mesure parallèle. Pour rester dans la chaussée, on aurait été forcé de
mettre l'alignement au milieu et de faire barrer la chaussée pour plusieurs semaines.
A cause du grand mouvement, on n’en pouvait pas obtenir la permission, de sorte qu’on
était forcé d'exécuter la mesure dans les champs voisins. La nouvelle ligne est
située à l’ouest, à une distance de 34 m de la première. La mesure à duré du
7 août au 3 septembre, c’est-à-dire pendant 28 jours. Elle a été éxécutée par six aides
de l'institut géodésique sons la direction du chef de section, M. le professeur Dr. Fischer.
Cette fois aussi on a fait la mesure en dix parties, en avant et en arrière. L'erreur
probable est trouvée provisoirement presque de la même quantité que l’année passée.

C. Travaux astronomiques.

Dans l'été 1878, on a éxécuté les observations pour la détermination des difé-
rences de longitude entre l'observatoire de Berlin, l’ancien observatoire d’Altona et
le: point trigonométrique, situé dans l’île de Helgoland; et entre l’ancien observatoire
d’Altona et les observatoires de Bonne et de Wilhelmshaven. Après avoir terminé
ces observations, on en a fait la réduction en hiver 1878/79. On trouve les résultats
dans les publications de l'institut géodésique: Astronomisch-geodätische Arbeiten im
Jahre 1878, Berlin 1879. Dans l'été 1879, les travaux astronomiques ne pouvaient

   

pet dut de à 1 oh Mi A a

ju 4 ma bé a hä ar an

 
