1 1 RP RT pm |

regt

LT UT gm Ti rt

Per

France et Algérie.

Les travaux exécutés en France et Algérie comprennent dans l’ordre où ils ont
été exécutés: 7

1° La mesure de la différence de longitude en Algérie, entre les stations d’Alger
et de M’Sabiha, Cette opération a été exécutée pendant l’automne de 1879, par M. M. le
Colonel Perrier et le Capitaine Bassot, M. Bassot occupant la station d'Alger et
M. Perrier celle de M’Sabiba; ces deux stations étant reliées par un fil aérien. Elle
est contemporaine de celle qui avait pour objet la mesure de la différence de longitude
entre M’Sabiha en Algérie et Tetica en Espagne, et qui s’exécutait par l'échange et
l’enrégistrement réciproque de signaux lumineux rythmés. — La station de Tetica est
ainsi reliée avec l'observatoire de Paris par M’Sabiha, Alger et Marseille: la différence
Paris—Madrid est déjà connue, il ne reste plus pour fermer le polygone des Longitudes
Paris, Marseille, Alger, M’Sabiha, Tetica, Madrid, Paris, qu'à mesurer la différence entre
Madrid et Tetica.

Les calculs sont terminés et nous ont donné:

différence de Longitude entre Alger (est) et M’Sabiha (ouest) = Où 15% 355068 + 0501.
2° La mesure de la latitude de la station de M’Sabiha, obtenue par le Capitaine
Defforges; par l'observation de quatre séries de quarante distances zénithales méridiennes

d'étoiles bien connues, les séries étant conjuguées deux à deux cercle à l’est, cercle à
l’ouest, pour deux origines communes équidistantes sur le limbe.

Tableau des valeurs obtenues pour la latitude de MSabiha.

 

Calage au Nadir. 2190 2600 Moyennes.
Moyennes des séries cercle à l’est 35° 39° 36750 | 35739 35.80 30 39 Sols |

Moyennes des séries-cercle à l’ouest | 35 39 37.03 | 35 39 37.51 59 01

 

 

|

Moyennes des séries conjuguées + 85 39:36.76 3529 00 50 56 01

 

 

 

Latitude. adoptée 35° 59 50627 20.1.

3° Liaison géodésique du côté M'Sabiha—Filhaoussen avec la triangulation
algérienne.
Cette liaison a été effectuée à l’aide d’un petit réseau comprenant les cing sta-
tions de M’Sabiha, Filhaoussen, Nador, Tessala, Tafaraoui dont les quatre dernières

Gen.-Ber. 2

 
