er en OPEN rpm ı

een)

AE PET

ARTE

wann

ea JE I

Cassini, J. (Sohn des Vorigen). De la figure de la terre. (Ib. 1713.)

— De la perpendiculaire à la méridienne de Paris prolongée vers lorient.
(Ib. 1734.)

— Méthode pour la détermination de la figure de la terre. (Ib. 1733 et 1759.)

— Des opérations géométriques que l’on emploie pour déterminer les distances
sur la terre etc. :(1b.,1786.)

— Moyens de construire un pendule qui ne puisse s’allonger par la chaleur etc.
(Ib. 1741.)

— Abhandlung von der Figur und Grösse der Erden, übersetzt von Klimmen.
Arnstadt u. Leipzig, 1741.

= Sur les opérations géométriques en France 1737, 1738. (Mém. de l’Acad. de
Paris. 1139)

Cassini de Thury, €. F. (Sohn des Vorigen). Avertissement ou Introduction à la carte
générale et particulière de la France. Paris, 1744. (An dieser Karte,
seinem Hauptwerke, arbeitete er bis 1756 auf Kosten der Regierung, später
durch Unterstützung einer Privatgesellschaft.)

— (mit Maraldi). Cartes de triangles de la France. Ib. 1744.

— La méridienne de l’observatoire roy. de Paris prolongée vers le Nord. (Mém.
de l’Acad. de Paris. 1740.)

— La méridienne de l'observatoire roy. de Paris, verifiée dans toute l’etendu
du royaume par des nouvelles observations; pour en deduire la vraye
grandeur des degrés de la terre, tant en longitude qu’en latitude, et pour
y assujettir toutes les opérations géometriques faites par ordre du Roi, .
pour lever une Carte générale de la France. Paris, 1744.

relations de deux voyages faites en Allemagne qui comprennent les opérations
relatives a la figure de la terre, à la géographie et à l’astronomie. Ib.
170». 22 eat 1719.

— Déscription géométrique de la terre. Ib. 1775.

= Description geometrique de la France. Ib. 1784.

—_ Méthode directe pour déterminer les réfractions ete. (Ib. 1773 et Mém.
de Bertin. 1773.)

= Jonction de la méridienne de Paris avec celle de Snellius. (Mem. de l’Acad.
de Paris, 1748.)

— Prolongation de la perpendiculaire jusqu’à Vienne. (Mem. de l’Acad. de
Paris, Iba.

— Relation d’un voyage en Allemagne. Paris, 1775.

Cassini, J. D., Graf v. Thury. (Sohn des Vorigen.) Exposé des opérations faites en
France en 1787 pour la jonction des méridiennes de Paris et de Green-
wich. (Ib. 1792. Annexe: Description et usage du cercle répétiteur de Borda.)

De la jonction des Observatoires de Paris et de Greenwich. (Mém. de l’Acad.
des scienc. de Paris 1788.)

Anh. IX. Gradm iit, 5

 
