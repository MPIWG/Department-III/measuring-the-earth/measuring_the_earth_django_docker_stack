 

|
i
H

8

 

 

 

 

 

 

 

 

 

 

 

Danemark.
eaaeeneee eee eee rere eee errr
ro Indication des ti Longi- Ko Directeur ned RU
N Enke. Latitude. ele | poque. At Ones. | Instrument. | Remarques.
21 | Storskoven..... br 1229" 97°54! 85") 1869 Capit. Mehldahl. |
DI MDISB N. ...... 57 17 2227 38 52 | 1869 id. 2 dr)
2 Megihol. _ .. . . 57 29 22 |97 48 12 | 1869 id. eo |
A rale........ 57 25 21 |28 7 57 | 1869 id. ee
25 | Skagen ......- 57 43 46 |28 15 18 | 1869 n id. |
26 | Knivsbjerg . . . .. 55: 8 6 127 6 24 |  1845—46 Nyegaard. |
ee... 55 7 30 |27 55 58 | 1845 id. | Theod. 2
28 | Lysabbel ..... - DAA te 2740 9) 1818 46 Caroc, Nyegaard. |
29 |Fakkebjerg. . . - - 54 44 23 28 21 53 | 1818—41—44 id. |
80 | Sprenge .....- na 27 14 107 16 9 | 1817—18 Schumacher, Caroc. |
81 | Hohenhorst. . . .. DA 15 1 127 50 53 | 1817 —18 id. |
32 | Bungsberg . . . .. 54 12 39 | 28 22 19 | 1817—18—41 | Schumacher, Caroc, Nyegaard. |
33 | Segeberg . . . . .. 53 56 7 |27 58 54 | 1817—18 | Schumacher, Caroc. Théodolite |
94 Lüheck ... . . . . 55 52 4128 22 58; 1817 Schumacher. de 12"... :|
85 | Eichede . . ..... Big 2 188 4 17 | 1819 Caroc. |
36 Base de Braak . . 53 38 27 | 27 52 30 | 1822 Schumacher. |
Terme Nord | |
37 | Base de Braak . . 53 35 44 |27 54 15 | 1822 | Schumacher, Caroc. |
Terme Sud | | |
BOIGIOK. . .<....- - 53 38 2 | 27 57 44 1818 —22 Caroc, Nehus. Théodolite de |
| Ss eu 10 al
88a | Bornbeck. . . . .. ne 56 11128 0 17 1822 Schumacher, Caroc. Theod. de 12".
39 | Hohenhorn . . . .. 53 28 32 |28 1 53 | 1818—24—925 | Caroc. | Théodolite de |
8” et 12”.
40 | Hamburg ..... 53 32 53 | 27 38 29 | 1817-18—22—23) Schumacher, Caroc, Zahrt-
24—25 mann, Nehus. | |
41 | Rônneburg . . . .. 53 25 38 | 27 39 56 | 1823 Caroc. | ; 1
42 urn, ce 53 14 57 |28 356 | 1818 Schumacher. | Théod. de 12”.
43 | Lauenburg . . . .. 53 92 49 |98 13 28 1818 id. | |
44 Store Müllehoi. . .|55 19 9 130 5 12 | 1837-38-40 | Schumacher, Peterson, Nye- | |
gaard, Nehus. | |
45 Malmo ....... 55 36 25 3040 5 | : 1839 Peterson. \
46 | Falsterbo. . . . .. 55 23 2 | 30 28 54 | 1839 id. || Théod. Ertel |
47 | Kulsbjerg . . . . . Bo) 0130 | 30 39 17 1842 Nyegaard. i |
48 | Hiddenso . . . . .. 54 35 51 | 30 46 56 | || Preussen Nr. 179.
49|Darserort ..... 54 28 35 | 30 10 22 | Preussen Nr. 181.
50 | Vigerlose . . . . .. 54 49 42 | 99 34 13 | 1839—40—41 | Peterson, Nehus, Nyegaard. | Théod. Ertel.
51 Dietrichshagen 1541 660 29 25 44 | 1841 Nehus. IThéod.Reichen- Preussen Nr. 182.
| | bach de 12”. |
52|Konigsbjerg ... .|54 57 55 | 80 10 24 1839—40 Peterson, Nehus, Nyegaard. | Théod. Ertel. |
Bug ........ 54 26 9 28 51 39 | |
54 | Hohen Schonberg .| 53 58 47 | 28 45 35 | | || Preussen Nr. 183.
ii | |
Frankreich und Algier.
Méridienne de Dunkerque, |  Meridienne de Bayeux.
a de Fontainebleau. Triangulation Mediterraneenne.
Parallele d’Amiens. = Vérification.
a de Paris. Jonction de l’Angleterre.
+ de Bourges. Triangulatiou avec l’lle de Corse.
5 de Moyen.
Jonction de la Base de Bordeaux. Ari
Supplement au parallele Moyen. Algerie.
Parallele de Rodez. Triangulation Primordiale.
Chaine des Pyrenées. Liaison de la base de Bone.
Meridienne de Strasbourg. os 5 d’Oran.
» de Sedan. > > de Blidah.

 

 

4
4
i

 
