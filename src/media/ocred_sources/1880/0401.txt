 

le M RP

APR Pt ep

AN

ER TIERE PTT

MEN

Nyren, M. Die Polhöhe von Pulkowa. (M&m. de l'acad. imp. des sc. de St. Petersb.
XIX, 10.) St. Petersb. 1873.

— Déclinaisons moyennes corrigées des étoiles principales. St. Petersb. 1875.

Pansner, J. H. L. von. Nachricht über geodätische Arbeiten in Russland 1812. (Zach
monatl. Corresp. 26).

— Nachrichten von den Vermess. im Petersb. Gouvernement. (Astr. Jahrb. von
Bode, 1814).
— Hôhen der Oerter über der Meeresfläche im europ. und asiat. Russland.
Berlin, 1836.
Parrot, J.J. F.W. Ueber d. Schneegrenze am Monte Rosa. (Schweigg. J ournal, XIX, 1817).
— Messungen der Höhenunterschiede zw. d. Kasp. und Schwarz. Meere. (Pogg.
Ann. XXI, 1894).

Paucker, M. C. von. Die Gestalt der Erde. (St. Petersb. Ac. Bull. XII, 1854, col.
97—128; XIII, 1855, col. 49-89, 225 —49).

Mepesomrankoss, A. Ucropuyeckoe o6osp$uie pabors gaa usmbpenia Mepu-
mana mexzxy 70° 40’ w 45° 20’. (1852).

(Perewoschtschikow, D.) Historische Uebersicht über die Veränderung der Meridiane
zwischen 70° 40° u. 45° 20’. (1852).

— Die Figur der Erde nach den Meridianen von Paris und Ostindien und nach
Pendelbeob. (Der Petersburger Academie eingesendet.)

Prazmowski. Rapport fait à M. le directeur de l’observatoire central sur les travaux
de l'expédition de Bessarabie, entreprise en 1852, pour terminer les opé-
rations de la mesure de l’arc du méridien. St. Petersb. 1853. (Bull. Acad.)

Peñnere, M. Onucanie chBepnaro Gepera Poccin. I, II, Cn6. 1843, 1850 r.
Kapta b£1aro mopa.

(Reineke, M.) (Beschreibung der nördlichen Gestade von Russland. St. Petersb. 1843,
1850. Mit einer Karte des weissen Meeres).

Reissig, C. von. Der Apparat zur Basismessung. St. Petersb. 1823.

Rumovski, S. Experimenta circa longitudinem penduli simplicis minuta secunda oscil-
lantis in urbe Selenginsk instituta. (St. Petersb. Acad. Sc. Nov. Comm.
1765, XI, pp. 468 — 80.)

— Meditatio de figura telluris exactius cognoscenda. (St. Petersb. Acad. Sc.
Nova Acta, XIII, 1802, pp. 407—17.)

= Experimenta circa longitudinem penduli simplicis minuta secunda Kolae et
Archangelopoli oscillantis. (St. Petersb. Ac. Sc. Novi Comm. 1771, XVI,
pp. 567—85.)

Sabler, & Beobachtungen über die irdische Strahlenbrechung etc. Dorpat, 1839.

— Anleitung zu astronom. Beobachtungen, um die Breite und Länge der Orte
zu bestimmen. St. Petersburg, 1803.

— Ueber das Problem vermittelst ausser dem Meridian gemessener Höhen die
Polhöhe zu finden. (Bode’s Jahrb. f. 1790.)

I

 
