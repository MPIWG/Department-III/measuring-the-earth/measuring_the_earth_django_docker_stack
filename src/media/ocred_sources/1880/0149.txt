rn

nn IRB eT PTO mmr ene TREN

ba dE dda
reper AU T

 

MITTE

Te

5
i
1}
à
i
ë
5
=
E
Zs
|

 

3

Portugal. M. Arbués Moreiro.

Les déterminations ne sont pas encore commencées, mais elles le seront pro-
chainement, à l’aide du pendule à réversion, dans les stations de Lisbonne et de Coimbre,
auxquelles on ajoutera probablement plus tard d’autres stations.

Prusse. M. le Prof. Albrecht.

Les stations dans lesquelles la détermination de la pesanteur a été effectuée
avec le pendule à réversion de l’Institut géodésique de Prusse sont: Gotha, Seeberg,
Inselsberg, Berlin, Leipzig, Dresde, Bonn, Leyde, Mannheim et Freiberg; toutes ces
stations sont reliées au réseau géodésique des pays, et les coordonnées astronomiques
déterminées pour toutes, sauf pour Freiberg. La méthode employée à été l’enregistre-
ment chronographique des passages pour les observations faites en 1869, ¢. a d. pour
les 5 premiéres stations indiquées, et celle des coincidences pour les observations faites
dans les 5 dernières en 1870 et 1871. La réduction définitive des observations faites
à Leipzig et à Freiberg n’est pas encore terminée, pour les autres stations les résultats
sont donnés dans les publications de l'Institut géodésique de Prusse, I n’a pas été
fait encore d'expériences directes sur le mouvement des supports, et sur la correction
à apporter à la longueur du pendule simple à seconde pour tenir compte de ce mouve-
ment; seulement une valeur approchée de cette correction a été obtenue par la compa-
raison de la longueur du pendule trouvée par Bessel en 1835 avec celle que donnent
les observations avec le pendule à réversion. M. Albrecht estime que cette correction
de + 0%*182, déterminée ainsi pour Berlin, peut s'appliquer également aux autres stations.

Russie. M. le Général von Forsch.

Le rapport de M. le Général von Forsch résume d’abord les observations faites
par l’amiral Liüfke dans sa campagne de circumnavigation, puis il donne les résultats
obtenus ces dernières années pour 13 stations!) russes par M. M. Sawitsch, Leng et
Smyslow. Ces savants se sont servis de deux pendules à réversion de Repsold (le petit
module, durée oscillat. 3°) et ils ont employé la méthode des coïncidences. Dans aucune
des stations, il n'a été tenu compte du mouvement du trépied et des supports. Plus
tard, en 1874, M. le Colonel Zinger fit avec les deux appareils une nouvelle détermination
a Pulkowa, en 1876, 1877 à 1878 le Général Stebnitzki et le Colonel Koulberg & Tiflis.

Saxe. M. le Prof. Bruhns.

Stations: Leipzig, Dresde et Freiberg, dont il a été deja fait mention dans le
rapport de M. le Prof. Albrecht.

') Tornea, Nicolaistadt, St. Petersburg, Pulkowa, Réval, Dorpat, Jakobstadt, Wilna, Belin, Kré-
ménetz, Kamnitz-Podolza, Kichinev, Jsmail.

 
