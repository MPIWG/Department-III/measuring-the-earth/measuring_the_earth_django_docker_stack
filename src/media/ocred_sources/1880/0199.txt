 

 

 

 

 

      

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Belgien.
Een EEE
; Indication des | à Longi- | f Directeur
Ne | Latitude. a Epoque. ?
| Points. | tude. 4 pod et Observateurs. se Amon:
| oz! st Hubert o 9) 50° 1'54"| 23° 5'34"|
| Weris SI 150.19 8 123198 8
Ë 64 | Se eS 49 59 14 | 23 29 22
65 | Novitle:. a0. 50 225 | 2S 26 20
66 | Arlon ee > 149 89847 193 97 15
67 | Montquintin . . ..|49 32 44 193 8 22
68 | Bouillon... cis | 49 46 12 | 22 43 19
69 | Hamepré . ..... 49 50,, 1.23 8 19
70 | Bellas. 62 0.0. | 80) 12384 | 23 40 11
= Wannetis 0... |50 19 53 | 23 39 41
| Cul des Sarts . . .149 58 3 [22 9 1
73 s Auller + 2... |49 49 9 |93 99 2
74) Willerzie . . .. . .|49 57 5 | 22 30 37 |
75. ÉSalhay: 2. ‘er |50 31 18 23 48 42 |
(6 | Nierset.. . .. .../50 99 1 2357249
@esBande. . 2... 50 722 2353|
18  Malempré. 5. | 50 15 6 |23 23 45 |
| Dänemark.
i |
Ne | oo dee | Tasinie nen Époque. eue Instrument. Remarques,
| Points. | le tudes à et Observateurs.
| | |
1 1 Copenhague .. . .| 55°40'43”| 30°14’48") 1837—38—40 | Nyegaard, Peterson, Nehus. Publiés dans le ,Den
| Bet. de 8. Nicolas | Ban mi
| | | lumes 2, 3 et A
la | Base Gi 1554900 17 PS0 12 13) 1838 Nyegaard, Peterson. |
Terme Nord
ib) Base, 2.22.56. 55 a7 Al 9801450 1838 id.
| Terme Sud al
= 1c/|Frederiksholm . 55 38 36 | 30 11 59 1838 id. 1
+ 1d|Copenhague . . . .|55 40 48 |30 14 9) 1837 id. u
| | Egl. de S. Pierre | N |
4 te | | Frydenhoi ale. 55.57 871.30. 7/50 1838 id. | Théodolite
= er Valhoh : Le 55 40 30 | 30 7 22 | 1838 id. { de Ertel.
+ 19 |Kongelmuden....)55 33 40 | 30 13 58. 1838—39 id, | J
2 Snodelev . ..... . 155 34216 129 AZ Ag \ 1838 id.
3 | Julianehoi. . . ... 55 46 54 | 29 27 18 1839 Peterson.
: 4] Morkemosebjerg. .|55 88 7 | 29 20 52 | 1842-43 Nyegaard. ;
127 5|Veirhot . ......:...| 55,47 36 29 3740, 1842—43 id.
6: Kloveshoi... .. .. 55 34 11 |29 0 55 | 1843 id.
7.Refsnaes..... . |55 44 10 | 28 34 38 | 1843 id.
+ 8/|Bogebjerg ..... ap 32 40 128 21 33 | 1844 id.
! 9)Dyrebanke..... 5b 22 5 |27 3: | 1844—45 id.
* 10 | Skamlingsbanke . . 55 25 8 271349 1846 id. |
11 | Trodelmosebanke . 55 43 46 |27 36 9 | 1846—67 Nyegaard, Capit. Mehldahl. |
NO Dyret. se ne 55 50 35 | 28 13 41 | 1847— 67 Thalbitzen, Capit. Mehldahl.
13 | Eiersbavnehoi . . .|55 58 38 |27 29 43 | 1867 Capit. Mehldahl.
14 | Agri Bavnehoi . . . 56 13 48 |28 12 5 1868 id.
15.-Eysnet 00 D6 22a eae se | 1868 id. Théod. Ertel
16 | Hegedal Bavnehoi . 56 30 19 |28 13 49 | 1868 id. Gage 157.
We HOVNGL 3... icc 56 38 41 | 27 39 56 | 1868 id.
18 | Rold Bavnehoi. . .|56 46 3 | 27 29 15 | 1868 id.
19 | Muldbjerg. . . ... 56 54 31 |27 55 30 1870 id. |
20 lägerdalchoi . . . .|56 55 88/27 28 49° 1869 id, | |

 

 

 

 

 

 

 

à
i
=
“È
i
A
BS
x
3
a
+

 
