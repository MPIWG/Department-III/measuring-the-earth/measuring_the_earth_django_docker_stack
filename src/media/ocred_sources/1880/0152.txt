 

|

É
D
|
4
|
4
|
f

EEE EEE RE

re

à secondes, la déviation qui à été mesurée dans les mêmes circonstances, c’est à dire celle
qui est produite par les oscillations du pendule. Il nous est impossible de partager
l'opinion énoncée à plusieurs reprises par M. Peirce, et en particulier dans l’important
mémoire publié récemment: „Measurements of gravity at initial stations in America and
Europe, Appendix Nr. 15‘. M. Peirce admet bien que, lorsque le trépied de Repsold re-
pose sur un support flexible, comme le support en bois de l'Observatoire de Genève, sur
lequel il a fait osciller son pendule et qui m’avait également servi pour mes expériences,
la constante du balancement peut être augmentée; c’est ainsi qu’il à calculé, dans la
réduction des observations qu'il a faites à Genève, l’augmentation de la déviation résul-
tant de la flexibilité du support en bois, en réduisant pour son pendule l'augmentation
que j'avais trouvée pour celui de la commission suisse. Mais pour les autres stations
européennes dans lesquelles il a fait ses expériences, à Paris, à Berlin et à Kew, il a
appliqué la constante du balancement résultant de ses expériences à Hoboken, où le
trépied de Repsold était placé probablement sur un pilier très massif et très solidement
fondé. Et cependant, à Paris, il avait trouvé une déviation plus forte qu'à Hoboken
dans le rapport de 1,09 :1, et il reconnaît lui même qu’il aurait peut-être mieux fait
de l’employer. M. Peirce n’a pas fait, & ce qu’il parait, des expériences sur la déviation
à Berlin et à Kew, mais il ne pense pas qu'elle ait pu s’écarter sensiblement de celle
trouvée & Hoboken, ce dont il compte s’assurer directement à la première occasion.
Il est possible qu’a Kew la déviation fût très peu différente de ce qu'elle était à Ho-
boken, si le trépied de Repsold était posé sur un pilier aussi massif;!) mais cela n’était
certainement pas le cas à Berlin, où M. Peirce a fait ses expériences sur le même pilier
qui m'a servi plus tard, et dont la disposition défectueuse a été signalée dans le mé-
moire ,Mouvement simultané d’un pendule et de ses supports, pages 15 et suivantes“.

M. Peirce s’est servi de la même assise triangulaire simplement posée sur le
pilier, très-solidement fondé il est vrai, mais dont les dimensions étaient trop petites pour la
distance entre les pieds du trépied de Repsold; ces pieds portaient ainsi à faux en
dehors du pilier et, vu le poids relativement très peu considérabie de cette assise trian-
gulaire, l’on avait certainement à craindre un mouvement de balancement du support, plus
considérable dans l'expérience statique que dans l’expérience dynamique. Par les ob-
servations du pendule lui même j'ai trouvé une déviation plus forte à Berlin que sur
un pilier très-massif, tel que ceux dont je m'étais servi dans les autres stations suisses,

dans le rapport de 1,042 : 1, tandis que par l’expérience statique la déviation était plus

forte dans le rapport de 1,19: 1.

L'erreur sur la longueur du pendule simple à secondes, provenant du mouvement
des supports, peut certainement être éliminée d’une manière très simple par la détermi-
nation expérimentale de la constante du balancement du trépied métallique posé sur un

1) Tant que les piliers sur lesquels le trépied métallique est posé dans différentes stations, sont
pour ainsi dire identiques quant à leurs dimensions, leur masse, et la solidité des fondations, on peut sans
inconvénieht appliquer la même constante de balancement pour toutes.

 

!

ss sun mh dh ke an an

 
