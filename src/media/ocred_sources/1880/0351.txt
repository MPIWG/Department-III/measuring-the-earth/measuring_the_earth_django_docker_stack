TEN NW!

mern

Hanne

DL

TR Ren DRITT wenn

Villarceau, Y. Détermination des longitudes, latitudes et azimuts terrestres, au moyen
des observations faites au Cercle méridien de Rigaud, en 1864, Section I —
Déscription de linstrument et mode suivi dans les réductions des obser-

vations. Section II — Station de Brest; Section III — Station de Rodez;
Section IV — Station de Carcassonne. (Annales de l’observatoire impérial

de Paris, Mémoires, Tome IX.)

— Détermination des longitudes, latitudes et azimuts terrestres, au moyen des
observations faites au Cercle méridien No. IT de Rigaud, 1865. Section V
— Station de Saligny-le-vif (Cher); Section VI — Station de Lyon (Four-
vieres); Section VIL — Paris (Station du jardin) 1866; Section VIII —
Station de Saint-Martin-du-Tertre. (Annales de l’observatoire impérial de
Paris, Mémoires, Tome IX.)

— De l'effet des attractions locales sur les longitudes et les azimuts; applications
d'un nouveau théorème à l'étude de la figure de la Terre. (C. R. 1866.)

— Comparaisons des determinations astronomiques faites pour l’observatoire im-
perial de Paris, avec les positions et azimuts geodesiques publies par le
Dépot de. la Guerre. (Ch, 1860)

— Nouvelle détermination d’un azimut fondamental, par l'orientation générale
de la Carte de France. (C. R. 1866 et General-Bericht, 1866, p. 8.)

= De Veffet des attractions locales sur les longitudes et azimuts; applications
d’un nouveau théorème à l'étude de la figure de la Terre. (Journal de
Mathématiques pures et appliquées; 2. série, Tome XII, 1867.)

a Nouveau théorème sur les attractions locales. (C. R. 1868.)

= Nouvelle détermination de la vraie figure de la Terre ou de la surface de
niveau, n’exigeant pas Vemploi des nivellements proprement dits. (C. R.
EXRI]I. 318715

— Nouveau mode d'application du troisième théorème sur les attractions locales
au contrôle des réseaux géodésiques et à la détermination de la vraie
figure de la Terre. (CE R RUE 79705)

— Nouveaux théorèmes sur les attractions locales et applications à la détermi-
mination de la vraie figure de la Terre. (Journal de mathématiques pures
et appliquées, Tome VIIL 2. série, Novembre 1873.)

— Détermination astronomique des longitudes, latitudes et azimuts effectués par
l'observatoire de Paris. Recherches sur les attractions locales. (Procès-
verbaux des séances de la commission permanente de l'association géo-
désique tenues à Vienne en 1873 — Appendice VI.)

= De la détermination des déclinaisons des étoiles fondamentales. (Comptes
rendus des séances de la 4. Conférence géodésique internationale appen-
dice Let p. 11%:)

6*

 
