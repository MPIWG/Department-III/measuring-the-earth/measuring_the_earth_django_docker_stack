 

|
|
|

ee

ee

BSS

SE

BEE Eger

|
i
|
i
|
i
5
i
|
L.

13

Challis, J. Theory of the Correction to be applied to a Ball Pendulum for the Re-
duction to a Vacuum. (Phil. Mag. III, 1833, pp. 185—7.) (See also 1832
pp. 40—5.)

— On the motion of a small Sphere vibrating in a resisting Medium. (Phil.
Mag. XVII, 1840, pp. 462—67.) (Followed in 1841 by two replies to
Mr. Airy’s “Remarks“.)

Chisholm, H. W. An account of Comparisons between two Russian Pendulums and
Repsold’s Scale and between Repsold’s Scale and the standard sub-divided
imperial Yard. (App. V to Eigth Ann. Rep. of the Warden of the Standards
for 1873—74, 26 pp. Corrections to the same, pp. 39—43.)

Clarke, A. R. On the measurements of azimuths on a spheroid. (Astron. Soc. Mem. XX,
1851, pp. 131—136. Astron. Soc. Month. Not. XI, 1850—51, pp. 147—148.)

— On the deflection of the plumb line at Arthurs seat and the mean density
of the earth. (Phil. Transact. 1856, p. 591.)

— On the figure and dimensions of the earth etc. (Phil. trans. 1856, p. 607.)

— Note on Archdeacon Pratts Paper “On the effect of local attraction in the
English arc“. (Phil. Trans. 1858, pp. 787—790.)

— Ordnance trigonometrical Survey of Great Britain and Ireland. London, 1858.

— Note on the figure of the Earth. (Astron. Soc. Month. Not. XIX, 1859,
pp. 36—38.) |

— On the figure of the Earth. (Astron. Soc. Month. Not. XXIX, 1861, pp. 25— 44.)

— Comparisons of the standards of length of England, France, Belgium, Prussia,
Russia, India, Australia made at the ordnance survey office Southampton.

London, 1866.
— Determination of the Great European Arc of Parallel. London 1867.

— On the course of geodetic lines on the earth’s surface. (Phil. Mag. 1870, May.)

— Results of the comparisons of the standards of length of England, Austria etc.
(Phil. Trans. 1873, p. 445.)

— On a correction to observed latitudes. (The London etc. philosoph. magazine
Vol WV, 1677, pp. 502505.)

— On the potential of an Ellipsoid at an external point. (The London etc.
philos. magaz. Vol. IV, 1877, pp. 458—461.)

— On the Figure of the Earth. (Phil. Mag. 1878 Aug.)

— On the determinations of the meridian with the Diagonal Transit Instrument.
(Astron. Soc. Mem. XXXVII, p. 57.)

— The Figure of the Earth. (Enc. Brit. 9% Edit. vol. VII, pp. 97—608.)

— Geodesy. (Oxford, 1880.)

Clyrton, W.E., and Perry, J. Determination of the acceleration of gravity for Tokio,

Japan. (Phil Mag. Ser. 5, Vol: 9, pp: 292—301.)

 

|

 
