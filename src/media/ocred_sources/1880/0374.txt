Se ee

 

|

ll
Ri
i
u
Hit
i
a
i
i
|
Bi
|

EEE TORE REP n

RE

Für 1878. Verhandlungen der vom 4. bis 8. Septbr. in Hamburg vereinigten perm. Comm.
der europ. Gradm., redigirt von den Schriftführern C. Bruhns u. A. Hirsch.
Zugleich mit dem Gen.-Ber. für d. J. 1878. Mit 2 lithograph. Karten.
Berlin, 1879. (Deutsch und französisch.)
Die Verhandlungen enthalten als Anhang: Vorläufige Vergleichung der
Höhenlagen europäischer Meere,
Der Generalbericht enthält als Anhang: Sur la construction de la regle
géodésique internationale par M. M. Sainte-Claire Deville et E. Mascart.
Parıs, 1879.
Für 1879. Verhandlungen der vom 16. bis 20. Septbr. in Genf vereinigten perm. Comm.
der europ. Gradm., redigirt von den Schriftführern C. Bruhns ime: Ay Hirsch.
Zugleich mit dem Gen.-Ber. für d. J. 1879. Berlin, 1880. (Deutsch und
französisch.)
Die Verhandlungen enthalten als Anhang: Note sur le pendule a re-
version par Yvon Villarceau.
7 Der Gen.-Ber. ebenfalls als Anhang: Relationen zw. dem Fundamental-
V catalog fir d. Zonenbeobachtungen etc. (Von Albrecht.) Ueberdies ist eine
zweite Abhandlung von Sainte-Claire Deville et E. Mascart beigegeben: Sur
la construction de la regle internationale.

9. Publicationen des geodätischen Institutes.

Astronomisch- geodätische Arbeiten im Jahre 1870. Inhalt: J. Langendifferenz zwischen
of Bonn und Leiden. — II. Polhöhe und Azimuth in Mannheim. — HI. Lange
des Secundenpendels in Bonn, Leiden und Mannheim. Herausgegeben von

©, Brulins, Leipzig, 1371.
‘Astronomisch-geoditische Arbeiten im Jahre 1871. Inhalt: I. Längendifferenz zwischen
/ Leipzig und Mannheim, Bonn und Mannheim. — I. Polhohe und Azimuth in
Durlach. Herausgegeben von C. Bruhns. Leipzig, 1873.
Astronomisch-geodätische Arbeiten in den Jahren 1872, 1869 und 1867. Inhalt: I. Längen-
differenzen zwischen Berlin und dem Rugard auf Rügen, zwischen Leipzig,

/ Göttingen, Dangast und Leiden. — IE Polhôhe auf dem Rugard, Seeberge und
Inselsberge. — II. Länge des Secundenpendels in Gotha, auf dem Seeberge,
dem Inselsberge und in Berlin. Herausgegeben von C. Bruhns. Leipzig,
1874.

Astronomisch-geodätische Arbeiten in den Jahren 1575 u. 1874. Inhalt: I. Längendifferenz
zwischen Brocken und Göttingen, Brocken und Leipzig, Berlin und Göt-

\ /

/ tingen. — U. Polhöhen auf den Stationen Mühlhausen, Me ttenhorn, Hohegeis,
Ilseburg, Asse, Löwenburg, Kuhberg, Bornstedter Warte, Gegenstein und
Regenstein. (Von Albrecht.) Berlin, 1875.

 

i
q
i

ee al A dd aa ll a

 
