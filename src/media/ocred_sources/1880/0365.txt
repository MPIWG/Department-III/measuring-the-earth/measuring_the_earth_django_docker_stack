 

re

 

a RTT

Tg] ia

PA POPE RMR EH PETITE

rer

Hansteen, Ch. Geographiske Bestemmelser i Norge. Christiania, 1823.

Möller, D. M. A. Om Lunds Observatorii longitud. Lund, 1853.

Norwegische Commission der europäischen Gradmessung.  Geodätische Arbeiten.
Heft II. Die Verbindung der Basis bei Christiania mit der Hauptdreiecks-
Seite Toaas—Kolsaas. Mit einer Dreieckskarte. Christiania, 1880,

14. Oesterreich.

Alle, M. Zur Theorie des Gauss’schen Krümmungsmaasses. (Sitz.-Ber. der Akad. der
Wiss. in Wien LXXIV, 9.)
Augustin. Ueber die trigonometrische Vermessung der österr. Monarchie. 1813. (Zach,
monatl. Corr. 27.) |
Beck, D. Nonii theoria et usus. Memming, 1780.
Binnenthal. Die trigonometrische Vermessung Oesterreichs. 1812. (Zach, monatl.
Gorm 2:)
Gzuber. Bemerkungen über die mathematische Behandlung von Beobachtungsergebnissen.
(Techn. Blatter, VIII, p. 131.)
Genauigkeit der geodätischen Punktbestimmung durch zwei und mehrere
Gerade. (Ibid. 1878, p. 65 ff.)
David, A. Trigonometrische Vermessung zur Verbindung der Prager Sternwarte mit
dem Lorenzberge und zur Bestimmung der Breite des Ortes auf dem
Hradschin, wo Tycho beobachtet hat. (Abhandl. der K. böhmischen
Gesellsch. der Wiss. 1805.)
— Trigon. Vermess. — Astronomische Ortsbestimmung des Egerlandes. Prag, 1824.
Fallon, L. A. von. Levée du Tyrol, et sur les positions géographiques de quelques
villes de VItalie et sur les différences que Von y a remarquées entre les
determinations astr. et trigonom. 1820. (Zach, Corr. astronomique 5.)
— Archiv der astronom.-trigonom. Vermessung der k. k. österr. Staaten, Band I.
Wien, 1822 und 1824.
= Lingenunterschiede durch Blickfeuer zwischen Padua und Fiume. (A. NonQoy -
— Höhenmessung von Oesterreich aus trigonom. Nivellirungen. Herausgegeben
von F. Freisauff von Neudegg. Wien, 1831.
Gerlach, F. W. A. Die Bestimmung der Gestalt und Grösse der Erde u. s. w. Wien, 1782.
Gruber, L. Ueber eine allgemeine Refractions-Formel. (Bull. de l’Acad. de.St. Peters-
bourg. T. XII.) St. Petersburg, 1867;
— Ueber einen Apparat zu Coincedenzbeobachtungen bei Schwerebestimmungen
mit Hilfe des Reversionspendels. (Sitzungs-Ber. der Akad. der Wiss. in
Wien LXXXL) Wien, 1875.
Hann, J. Ueber gewisse beträchtliche Unregelmässigkeiten des Meeres-Niveaus. (Wien,
geogr. Gesellsch, Mitth. 1875, 15 pp.)

Anh. X. Gradm.-Lit. 8

 
