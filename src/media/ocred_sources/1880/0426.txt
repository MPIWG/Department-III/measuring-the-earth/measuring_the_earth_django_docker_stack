 

 

i
\
fi
i
N
j
N
N

mem om mes

|
j
N
i
|;
if
h
;

 

appartiennent au réseau primordial de lAlgérie. Les anciens centres de repère ayant
été retrouvés intacts, la triangulation de l’Algérie est ainsi définitivement reliée avec
celle de l'Espagne.

Les observations astronomiques ayant donné à M’Sabiha l’azimut d’une mire
méridienne, il à suffi de mesurer, en cette station, l'angle de cette mire avec la direc-
tion de Tessala, pour obtenir l’azimut d’un côté géodésique.

A.la station de M’Sabiha, reliée & la triangulation algérienne, nous avons donc
mesuré: les différences de longitude avec Alger et avec Tetica, la latitude et un azimut.

40 Latitude de la station centrale de l'observatoire astronomique de Lyon,
mesurée par le Capitaine Bassot. Cette opération n'avait pas pu être exécutée en 1877.
Naleun adopiee i= 45 21 39:9 #01 (six series).

5° Nouvelle mesure de la latitude du Puy de dôme par le Capitaine Defforges.

 

Valeur obtenue en 1876 par le colonel Perrier . . . 45° 46° 27°98 (2 séries).
Valeur obtenue en 1880 par M. le Capitaine Defforges 45° 46° 28:08 (6 séries.)
Valeurs definitivement adoptée . . 45 46 28.0 Æ O:1.
69 Reconnaissance des abords de la base de Melun. — Construction des signaux

en charpente nécessaires au rattachement de cette base avec la triangulation. L’un de
ces Signaux à 30 mètres de hauteur.

M. M. Brunner frères ont livré au dépôt de la Guerre un appareil des bases
semblable à l’appareil acquis récemment par l’Institut Géodésique de Berlin; la nouvelle
mesure de la base de Melun sera entreprise lorsqu'il aura été possible de comparer
cette règle avec le mètre international ou la règle géodésique internationale.

Nivellement général de la France. — Les opérations relatives au nivellement des
lignes primordiales de la France ne tarderont pas à être entreprises; je me borne en
attendant de pouvoir fournir à l'association des indications précises sur le personnel,
les instruments et les méthodes d'observation, à déposer sur le bureau une carte indi-

quant les positions des lignes nouvelles à niveler. Ces lignes forment un polygone et
comprennent un développement de kilomètres.

Calculs: Calcul de la longueur de l’arc du parallèle algérien.

La simple inspection de la carte de nos triangles algériens, moatre qu’ils
s’ecartent peu du parallèle de 36°. Si en outre on prend la moyenne des latidudes
des quatre-vingt huit sommets de la chaîne, on obtient une valeur peu différente de
36°. Il était tout naturel de choisir cet arc de parallèle pour en calculer la longueur.

La méthode que nous avons adoptée consiste à considérer chacun des éléments
de la courbe cherchée comme un petit arc de parallèle d’une sphère dont la surface
s’approche le plus qu'il est possible, de celle du sphéroide terrestre, dans toute l'étendue
de la ligne à rectifier, et à calculer successivement les portions de parallèle interceptées
entre les méridiens des extrémités des côtés qui relient entre elles, par une ligne brisée

 

i
i
i
;
|
i
