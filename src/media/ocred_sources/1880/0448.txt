 

5
if
h
i
hi
%

32

auszuführen. Trotzdem ist es den beiden Assistenten Werner und Börsch möglich ge-
worden, die Ausgleichung des hessischen Netzes zu vollenden, während ich die mir von
der permanenten Commission übertragene Vervollständigung des Literatur-Verzeichnisses

ausgeführt habe.
Sadebeck.

II. Section. des Professor Dr. Börsch.

Ueber die von dem geodätischen Institute im Grossherzogthum Baden ausgeführten nivellitischen
Arbeiten siehe 8. 3. i

Im Sommer 1880 wurde vom Assistenten Herrn Sebi das zweite Nivellement
der Linie Berlin-Bitterfeld zugleich mit den erforderlichen Revisionsmessungen und
ebenso das zweite Nivellement der Linie Frankfurt-Heppenheim ausgeführt, auch zwischen
Eisenach und Frankfurt mehrfache Revisionsnivellements vorgenommen. Um für alle
einzelnen Nivellements der Linien Swinemünde-Constanz-Friedrichsfeld durchweg gleiche
Genauigkeit zu erlangen, werden im Sommer auch noch die zwischen den revidirten
Strecken liegenden älteren Nivellementsbeobachtungen durch neue ersetzt werden. Da
auch das Mittelwasser der Ostsee bei Swinemünde aus sämmtlichem zuverlässigen Be-
obachtungsmaterial an den verschiedenen Pegeln aus den Jahren 1811 bis incl. 1879
bereits ermittelt ist — die Publication dieser von dem Assistenten Herrn Seibt aus-
geführten Arbeit steht in Kürze bevor —, so werden die definitiven Seehöhen aller Fix-
punkte der Linie Swinemünde-Berlin- Basel-Constanz-Friedrichsfeld im kommenden Winter

berechnet werden können.
Börsch.

III. Section des Professor Dr. Albrecht.

Da im Sommer 1880 die Mitwirkung der Assistenten der astronomischen Section

‘des Institutes bei der Nachmessung der alten Berliner Grundlinie mit dem neuen inter-

nationalen Basismessapparat erforderlich war, konnten umfassendere astronomische Ar-
beiten im vergangenen Jahne nicht ausgeführt werden. Indess war es nach Beendigung
der Basismessung doch noch möglich, behufs Ergänzung der Untersuchung über den
Verlauf der Lothablenkung im Harze die fernerweite Bestimmung der Polhöhen auf den

Stationen Neinstedt, Vietorshöhe und Josephshöhe, sowie die Bestimmung des Azimuthes

auf der Station Neinstedt vorzunehmen. Die Beobachtungen auf Station Neinstedt wurden

 

:
:
i
i
:
j
$
i

ee alla hd

 
