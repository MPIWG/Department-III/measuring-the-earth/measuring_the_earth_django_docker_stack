  
 
  

ee m,

REINE EN TE Ve SA 5
sega fo aS GEHE EEE EEE ee ERE PS EP Te ae TT :

RE

 

12
Bogenhausen, Nürnberg, Mittenwald, Holzkirchen, Ingolstadt, Wülzburg und ein Azimut
in Wülzburg. Die Pendellänge in München ist bestimmt. Eine 5. Lieferung des
bayerischen Präcisionsnivellements ist, nachdem dasselbe noch in verschiedenen Rich-
tungen ergänzt war, erschienen; auch ist eine Publikation über die sämmtlichen Nivelle-
ments in populärer Darstellung von Herrn von Bauernfeind herausgegeben.

In Württemberg ist mit dem Nivellement fortgefahren und sind die verschie-
denen Polygone, welche im Ganzen 2454 km umfassen, ausgeglichen. Die Polhöhe des
Bussen ist berechnet und angegeben.

In Baden harren die ausgeführten Messungen der Ausgleichung, welche an
dem k. preussischen geodätischen Institut gerechnet wird. Die unsern Zwecken dienenden
Nivellements sind vollendet; die Ausführung eines weiteren Nivellementsnetzes ist unter
die Direction der grossh. badischen Verkehrsanstalten gestellt.

In Hessen hat der neu ernannte Commissar Professor Nell noch weitere Nivelle-
ments theils schon ausgeführt, theils in Aussicht gestellt.

In Belgien ist mit allen Arbeiten weiter fortgefahren. Eine ‘grosse Anzahl
von Dreiecksnetzen ist bereits ausgeglichen und sind in den Anschlüssen sehr befrie-
digende Uebereinstimmungen erreicht. Das Nivellement ist vollendet und sind in dem
letzten Jahre drei Publikationen über die Triangulation und die Nivellements erschienen.

In den Niederlanden sind die Triangulationen in der Provinz Groningen fort-
gesetzt und sollen an die in Ostfriesland, Westphalen und Hannover angeschlossen werden.
Ein Azimut von Utrecht aus nach Ammerfort ist gemessen. Mit der Fortsetzung der Nivelle-
ments beschäftigen sich die Herren Professor Bakhuyzen und van Diesen, und sind ver-
schiedene Polygone zum Abschluss gebracht.

In Oesterreich ist noch eine Grundlinie bei Dubica gemessen und für verschie-
dene Triangulationen sind Signalbauten und Winkelmessungen ausgeführt. Die Resultate
mehrerer Längendifferenzbestimmungen, welche sich auf fundamentale Punkte beziehen,
sind, als nahezu definitiv, veröffentlicht und an den übrigen wird gearbeitet. Polhöhen
und Azimute an den Basisendpunkten und verschiedenen andern Punkten erster Ord-
nung hat die trigonometrische Abtheilung vollendet und zu der grossen Menge der Ni-
vellements sind mehrere Polygone hinzugekommen. Die aus den Aufzeichnungen der
Mareographen erhaltenen Resultate über die Meereshöhe sind srösstentheils veröffentlicht.

In der Schweiz sind eine”Anzahl trigonometrischer Punkte von Neuem gemes-
sen, die Ausgleichung der Stationen ist vollendet und mit der Ausgleichung des Netzes
begonnen. Durch die bereitwillige Darleihung des spanischen Basisapparates ist schon
eine Grundlinie bei Aarburg gemessen und die Messung zweier anderen für nächstes
Jahr ist in Aussicht genommen. Die Nivellements haben sich um verschiedene Linien
über die Alpen nach Italien vermehrt und ist die 7. Lieferung des schweizerischen
Präeisionsnivellements erschienen. Die Längenbestimmungen Genf— Strassburg und
Genf—München sind in den letzten Jahren veröffentlicht.

 

=
i
3
I
3
3
3
ä
:

43 14 140 À

‘hats, pil ns, du ad debat a at am à

 
