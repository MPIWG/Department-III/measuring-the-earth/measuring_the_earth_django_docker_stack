45

Zweite Sitzung.

München, den 15. September 1880.

Anwesend die Herren: von Bauernfeind, Bruhns, Faye, von Forsch, Hirsch, I banez,
Mayo, von Oppolzer. Herr General Baeyer ist durch Unwohlsein verhindert.
Anfang der Sitzung 10 Uhr.

Herr von Bauernfeind fordert die Herren auf, sich zu constituiren, und bei der
Abstimmung erhalten:

Herr Ibanez ist daher zum Präsidenten, die Herren Bruhns und Hirsch zu Secre-
tären gewählt und dieselben nehmen die Wahl dankend an.

Herr Ibanez ernennt Herrn von Bauernfeind zum Vicepräsidenten.
Schluss der Sitzung 10 Uhr 15 Minuten.

| Ibanez . . . . 7 Stimmen als Präsident
Hayes 0 sea À : hs .s
| Broins +0 1 > als Secretär
is Hirsch + ue . os .
| von Oppolzer . . 2 : ,

 
