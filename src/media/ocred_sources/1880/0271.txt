IT TO TR

HULL AR

TR TTT

FO TN OR TAT DR 1

Désignation
de

Appareil
du systeme
Bessel

Appareil
Autrichien

| AppareilPrussien
de
Brunner freres

id.

 

Appareil du
General Ibanez
construit par
M. M.
Brunner fréres

id.

Appareil du
Général Ibanez

l'appareil des bases.

| Regles
qui ont servi pour
l’etalonnage.

| Double toise
| étalon de Spano
| rapportée à la
| toise de Bessel

 

id.

Toise normale de
| Vienne comparée
| en 1850 avec la
toise de Pulkowa

Règle Géodésique
en platine com-
parée à la règle
de Borda No. 1

id.

 

 

Observations.

La base du Tésin a été mesurée deux fois; l'écart entre les
deux mesures est de 0199.
C'est l’ancienne base Lombarde du Parallèle moyen
(mais nous ne savons pas encore si les deux extrémités
sont identiques).

Mesurée deux fois; l'écart entre les deux mesures est de
01320.

 

Mesurée deux fois. — La première mesure à été interrompue
en 1878 par les inondations. La différence entre les
deux mesures est de 8 millimètres environ.

Mesurée deux fois. — La règle en platine de l'appareil
employé n’a encore été l’objet d'aucune comparaison avec
un étalon normal; cette comparaison sera effectuée plus
tard au Bureau international des poids et mesures.

C’est la répétition de la mesure faite en 1854 par
le Général Baeyer, avec l'appareil de Bessel.

| Mesurée deux fois.
C’est la répétition de la mesure faite en 1846, égale-
ment par le Général Baeyer, avec l'appareil de Bessel.

Mesurée deux fois.

Mesurée deux fois.

Mesurée trois fois; deux fois par les Espagnols, une fois par

 

les Suisses.

 
