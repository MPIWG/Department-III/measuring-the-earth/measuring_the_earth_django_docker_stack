sh pe TN mn

 

Te

wae

RE
EEE TREE BT gm 1

PRÉSENTÉ À L'ASSOCIATION GÉODÉSIQUE PAR C. CELLERIER, 21

 

 

 

 

1ere 9de
disposition disposition

Dans la premiere disposition, | D | | D’ |

l'appareil, outre les disques qui lui ln |

sont fixés, en porte deux autres, D vide,

et D’ plein, centrés sur l'axe de la m. A

tige, et symetriques de forme et de

position; C, C’ sont les couteaux: D, a)

D’ les centres de gravité des disques, -G -@

G celui du pendule tout entier; la er a

lettre 8 est le rapport du poids de D or AR

au poids total, les disques compris; | | F

e le même rapport pour D’; on pose D’ | | D |

|

GC=h GC=h De: ma

On aura la seconde disposition en enlevant les disques, et retournant chacun d’eux
pour le mettre à la place de l’autre; soient alors G’ le centre de gravité total, et en outre

QVO=j}h', GCA kh” Bu Deo

Theorie. Cherchons la hauteur GG’ = w dont le centre de gravité s’est élevé ;
pour cela comptons les abscisses sur l’axe de la tige, de bas en haut, à partir du
point G&; soient +, celle du centre de gravité du pendule sauf les disques: et # la

masse totale, disques compris. D’après les propriétés générales du centre de gravité
on aura, pour les deux dispositions

0 = (m — om — fm) x, + om(h + a) em" + A),
mu = (m — am — om) a, + 6m (h + D) — om ( +b);
on en tire en soustrayant
(I) w= CO —66, c—-hthloa.s, da u... a! tn Bi.

Les momens d’inertie par rapport & G dans la premiére disposition, par rapport a G
dans la seconde, pour l'appareil entier, peuvent être mis sous la forme

m (hh +8),  m(h'n" Le);

on suppose dans le premier & très petit de sorte que la longueur réduite différe peu
de À; il y a a démontrer que 6 est très petit du même ordre, de sorte que "échange
des disques déplace notablement le centre de gravité sans altérer la durée.

 
