 

i
i
4

dés

ereeee-sdailhde geet bob id old

13. France.
a) Instructions pour M. M. les opérateurs, par Bourdaloue“, à Bourges, chez
Mad. veuve Jolles Loucas.
b) „Nivellement general de la France. Notes diverses par Bourdaloue, a Bourges,
| 1 vol., imprimerie Æ. Pigelet, 1864.
| c) Nivellement general de la France. Résultat des opérations exécutées pour
| l'établissement du réseau des lignes de base. A Bourges, 3 vol. Imprimerie

 

E. Pigelet 1864.
d) Commission centrale du nivellement de la France, premiere session 1878—1879.

Publication du Ministére des Travaux publics. Imprimerie nationale 1879.
e) Bulletin du Ministère des Travaux publics, première année 1880. Imprimerie

nationale.
f) Mémoires manuscrits de la Sous-Commission du nivellnment général de la

France.
14. Pays-Bas.

| „Uitkomsten van de in 1875 en 1876 uitgevoerde nauwkeurigheits-
waterpassing.‘
| „Uitkomsten van de in 1877 mitgevoerde nauwkeurigheids-waterpassing.“
, Description succincte des instruments et méthodes par le Professeur
Cohen Stuart, dans le Tydochrift van het Koninklyk Instituut voor [Ingénieurs
1876—1877, Procés-verbaux page 97.“
| „Uitkomsten van de in 1879 uitgevoerde nauwkeurigheids-waterpassing.“
Qu'il soit permis au rapporteur d'exprimer le vœu que non seulement
à l'avenir l'échange des publications entre tous les pays associés soit orga-
nisé d’une manière complète par l’intermédiaire du Bureau Central, mais
autant que faire se peut encore, les publications antérieures soient distribuées
par les pays, qui ne l’ont pas encore fait, aux autres associés.

rc sh he 2e Du ha doi A amas

eee

En terminant ce rapport détaillé, et tout en félicitant Association géodésique
des progrès que l’hypsométrie a faits presque partout, nous croyons devoir formuler en
quelques propositions, soumises au vote de la Conférence générale, les points qu'il impor-
terait de réaliser dans l'intérêt de cette branche de notre tâche:

1. Pour éliminer l'effet du tassement, on recommande de faire les doubles opera-
tions toujours en Sens inverse.

2. Dans les calculs de compensation il importe de tenir compte non seulement
des longueurs des lignes parcourues, mais aussi des differences de hauteur
qu'on à rencontrées.

 
