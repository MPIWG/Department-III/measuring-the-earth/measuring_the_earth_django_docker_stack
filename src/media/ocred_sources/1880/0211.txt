19

*

Triangulation de la Méridienne de Bayeux. (Partie Sud.)
TT

 

 

 

 

 

 

 

 

 

 

 

 

 

 

| | |
Ne Indication des | Latitude. Longi- | Epoque. Direetem Instrument. | Remarques.
Points. | tude. et Observateurs. |
252 Chadenac...... ||
209 Enarıme ......|
| 2Ol | SoubFan... . .. | &
E 59 | i =
| + | ae = : | Déjà mentionnés, 5 a | Z
265 St. André de Cubrac | | : 2a 3
269 Blanquefort .... | = Sie =
266 Montagné...... | | So =,
419 Bouliac....... 44°48'52"| 17° 9'30"| a BA
De rerson. u. 44 46 : 30 | 117 15 51 | æ a 2
221 Sannay......... 44 43 19 |17 40 a A .2 D
eo Pyros og. | 44 35 46 | 17 33 ao a |
423 |Escassefort ....|44 32 58 |17 54 0 | = |
= | Romestaing . . ... [44 25 1.17 39 54 | 5 | |
SEL Monelar:.. . .…. 5s fo | |
oe | en | | Déjà mentionnés. | | |
| | | |
| Triangulation Méditerranéenne.
291 | Montagné . . . . .. | | | |
293 | Houpies . . . . . . : | Déjà mentionnés. | : | |
iE 396 | Tabouret . .. ... | | Ss |
=4p 425 | Saintes Maries . .|48 27 7 |22 5 27 | 3.2 eal
2E 1426 | Bagnet . . .. ... 48 44 5192 5 49 | | Su | gi |
Æ 427 | Aigues Mortes . . . 43 34 7 |21 50 50 | a | a8 | © |
4 428 | Pic St. Loup. . . . 43 46 45 |21 28 27 | | À » | = |
129 |Cette..... ...,48 24.10. 21 20 51 | = os | = |
+ 430) Cabrieres ..... ı48 85 53 |21 1 11 | | S= | = | .
| 431 | Bezièrs . . . . . .. | 48 20 31 | 20 52 23 | 3 oo | 2
68 Saint Pons..... |43 31 34 |20 23 40 | 2 Ar | = |
2 Alarıe . ..:... 43 8 54 |20 17 26 || Su | & |
432 | Narbonne. . . .. .| 43 11 8 |20 40 O | SE | |
| Puen a | Déjà mentionnés | oe | |
2 Bugaragh... : : | J a | |
Ë Triangulation de Verification.
if 130 | Pithiviers . .... | | | [Memorial du Dépôt de
Ë a | Eyes oF Croix . | 3 han ithe
a4 onnerville..... | | en | Paris 1865.
= 34/Orléans....... | =
42|Bourges ...... Déja mentionnés. | oes Cee |
36 |Chammont ..... | 5 © En |
37 | Souesmes..... . | 35 ae
: A1 Méry-és-Bois. . .. | a A.
196 | Menétrévol . . . .. | a As Sn
"4383 |Monnerville ... .|48 20 51 |19 42 24 | | Eye oe I
1434 lenville....... 48 12 5 |19 32 55 | = EB wos |
0435 |Newville .......|48 4 8 |19 41 12 | © ae oo.
Bec Patay...... | 48 92 53 | 19 20 48. an | 2o
"437 | Cuzoner le Marche 47 54 41 19 11 30 5 32
488 | Beaugency . . . .. 47 44 38 | 19 17 44 Se DE
À 439 | Mulsans ...... KAT AL 47 119 2 56 || oe 7
| 440 | Cour Cheverney. .| 47 8034 19 7 7 8
za Mur 5.028. 0, A7 24048 | 19 16 19 | 2
2 Thelllay ...... ı47 18 50 |19 42 11 | 5

 

:
a

&
B3
==
at
i
a5
“=
1
=
+e
&
4È

 
