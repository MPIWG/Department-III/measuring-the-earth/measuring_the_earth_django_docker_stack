  
 

TS
à =

SR RS PE

 

|
|
|
|
|
|
|
j

28

Tennant, J. F. An Examination of the figure of the Indian Meridian. as deduced by
Archdeacon Pratt. (Astr. Soc. Month. Not. 1856—57, pp. 56— —63.)
— On the effect of local attraction in modifying the apparent form of the earth.
(Astr. Soc. Month. Not. XCII, 1856—57, pp. 236—41.)
Tiarks, J. L. Dalby’s method of finding the difference of longitude between two points
of a geodetical line on a spheroid etc. (Phil. Mag. Ser. Ii, Vol. IV, 1828.)
— On the longitude of the trigonometr. survey of England. (Ib. V, 1829.)
Todhunter, J. On the Arc of Meridian measured in Lapland. (Camb. Phil. Trans.
: NI 1841)
— A history of the mathemat. theories of attraction and the figure of the earth,
from the time of Newton to that of Laplace. 2 Vol. London 1873.
Troughton, E. A comparison of the repeating circle of Borda with the altitude and
azimuth circle, (Mem. Astron. Soc. 1, 1822.)
— Mehrere Artikel, z. B. Circle, Graduation, in Brewster’s Edinb. Encyclopaedia.
Er verfertigte eine grosse Anzahl geodät. und astronom. Instrumente
von vollendeter Arbeit und zum Theil neuer Erfindung. Dahin gehören
die grossen Theodolithen für die amerikan. Küstenaufnahme (1815) und die
Vermessungen in Irland (1822) und Ostindien (1830), sowie die Apparate
zu den in letztgenannten Ländern von Colby (1827) und Everest (1829)
ausgeführten Basismessungen etc.
Topping, M. Measurement of a base line on the coast of Coromandel. (Phil. Trans.
1792.)
Wales, W. The method of finding the longitude by time-keepers. London, 1794.

Walker, G On the doctrine of the sphere; with an appendix containing the solution
of a problem for ascertaining the latitude and longitude of a place, to-
gether with the apparent time. London, 1775 et 77.
Walker, J. T. Account of the operations of the great trigonom. survey of India.
Vor tsi) vol il, (879. Vol. Lil, 1873. Vol. IV, .1876. Dehra
Doon.
— Administr. report, great trigonom. survey of India. 1864—5.
— Letter to the president (on the Indian Pendulum observations.)
a Preleminary notice of results of Pendulum experiments made in India. (Roy.
Soc. Proc. XV, pp, 318—9.)
— Administration report, great trigonom. survey of India. 1865—6. (Basevi,
Appendix pp. XXVI—XXVIII, Dehra, 1866.)
The like for 1866—7, pp. 18—22. (Basevi, Append. pp. XXXIX—XLI.)
The like for 1867—8, pp. 22—3. (Basevi, Append. pp. LVI—LX.)
The like for 1868—9, p. 4, pp. 22—3. (Basevi, Append. pp.
XX VITI—XXX.)
The like for 1869—70, pp. 15—6. (Basevi, Append. pp. XXVI—XXXI.)

 

}
}

alu ee à dd he su

 
