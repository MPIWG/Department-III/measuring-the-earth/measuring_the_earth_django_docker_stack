wots it PTT Tee er Wet

pear ut

Te UNP ENT

ee

80.
Coraboeuf, J. B. Mémoire sur les opérations géodésiques des Pyrénées et la compa-
raison du niveau des deux mers. Paris, 1831.
— Exposé des opérations faites en 1825 aux deux extrémités de la base de
Perpignan. (Conn. d. temps, 1832.)
— Notice sur les opérations géodésiques que les ingénieurs géographes français
exécutèrent à Rome en 1809 et 1810. Paris 1853.
Cornu, A., et Baille, J. Détermination nouvelle de la constante de l’attraction et de
la densité moyenne de la terre. (C. R. LXXVI, pp. 954—58; 1873.)
— Sur la mesure de la densité moyenne de la terre. (C2 :R7 KOME pp.
699—702; 1878.)
Dangos(d’Angos). Observations sur la réfraction terrestre. (Mém. Inst. Sav. étrang. I, 1805.)
Delambre, J. B. J. Base du système métrique décimal. Paris, 1806—10.
— Analyse de l’ouvrage de Svanberg sur la mesure de Laponie. (Conn. d.
temps, 1808.)
= Extrait d’un mémoire de Rodriguez sur la mesure de 3 degrés en Angleterre.
(Conn. d. temps, 1816.)
— Histoire de astronomie au XVIIT® Siècle, Publiée par M. Mathieu. Paris, 1827.
= Histoire de la mesure de la terre. Ib.
~~ Opérations trigonométriques pour la a de l’Angleterre. (Conn. d,
temps, 1818.) |
— Nouvelle déscription de la France formant les tomes VI, VII et IX des Mém.
du dépôt, de la guèrre ete. Paris, 1852 40 52.
Delaunay, C. E. Sur la géodésie française etc. Paris, 1863. (C.R)
— Sur l’état actuel de la geodesie. Paris, 1864.
Delisle de la Croyere, Louis. Observatio longitudinis penduli simplieis facta Archangelo-
poli referente Jos. Nic. de lIsle. (St. Petersb. Acad. Sc. Comm. 1829,
pp. 322—28.)
Dépôt général de la Guerre. Mémorial du dépôt etc. Tome VI, publié par le com-
mandant Perrier: :
Détermination des longitudes, latitudes et azimuts terrestres en Algérie.
Paris, 1877—80.
1° fascicule. Station d’Alger.
2° fasc. Stations de Böne et de Nemours.
3° fasc. Stations astronom. de Biskra et Laghouat (1877), Géryville
et Carthage (Tunisie). (1878.)
Deville, Sainte-Claire. Sur la construction de la régle géodésique internationale. (Gen.-
Ber. f. 1878.) . Deuxième Mem. (Gen.-Ber. f. 1879.)
Dumas, J. L. Verknüpfte 1803 im Auftrage der Regierung die Vermessungen von
Frankreich und Belgien durch eine Triangulation,
Duperrey, L. J. Observations du pendule invariable. (Conn. de temps, 1830, pp. 83 — 99.)

5*

 
