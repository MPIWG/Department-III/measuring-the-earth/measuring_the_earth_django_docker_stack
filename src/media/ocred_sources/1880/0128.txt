 

i
3

sen Re eee eer

SE EES SO

L

 

10 RAPPORT SUR LA QUESTION DU PENDULE,

Si Yon supposait numériquement B = A, on devrait prendre
BA ;
HP) arc lang I tang u i uw —u" = vu,
nn 1 :
de sorte que Ww restat compris entre + 9 7 on aurait en même temps:

oa u) Eco — à + d) = Eacos (mi + ff’ + W),

a ayant la valeur (11), et l’on en conclurait comme précédemment que la durée moyenne

I

. TE 3 À , .
serait -7, pourvu que Vangle m’t +f’ + w ou w’ —w”, fit toujours croissant: c’est

ce qui aura lieu en général, « et «” variant beaucoup plus lentement que w’. Il pourrait
2

‘en être autrement si A et B différaient très peu; en même temps le minimum de a

serait presque o; c’est un cas anormal, étranger aux applications du pendule, et dont
la discussion serait inutile. En le laissant de côté on voit que la durée moyenne d’os-

. . TT T «TL
cillation sera, ou 7 Dour les deux pendules, ou -r pour tous deux, ou > bon Yun

Tr
et -— pour l’autre.

Choix de la disposition du pendule double.

L'appareil peut s’employer de deux manières:

1° La méthode directe consiste à observer une série d’oscillations d'un des pen-
dules en cherchant à atténuer ou à mesurer l'erreur due à l'influence de l’autre.

2° La méthode des passages consiste à noter les passages simultanés par la

: po P TT
verticale pour en déduire la durée moyenne — où —.

- En tout cas il faut qu’en supposant le mouvement simple on puisse en tirer
la valeur de g.

La disposition employée par M. Pierce se prêterait mal au premier procédé, car

x

les pendules marchant en sens contraire tendent & produire un balancement de torsion

que nous avons vu n'être pas négligeable. Quant à déduire y de ae ai: c’est à dire de

 

 

I

e en
g

cela exige une mesure directe de x, y, ou au moins, en supposant 6 trés petit, de 2 —y;
nous avons vu que cette quantité n’était pas nulle, et les variations continuelles de la

valeur de % ne permettent guères de supposer qu’une mesure suffirait une fois pour

toutes, la rotation comme le balancement pouvant être influencée par le sol ou les piliers.

 

3
i
3
q
i

ns ich a i, idl LA ald ali dd lh Aide ts
