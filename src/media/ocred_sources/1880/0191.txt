voy id AA Mi,
DI: LE

 

in

i
LR LILI AE

RUE

it
&
B:
i
z=
it
28
4
A
==
:
iR

 

5

Instrumenten und auf Kosten der K. Bayerischen Commission der Europäischen Grad-
messung. Von den Instrumenten, die hier um so weniger beschrieben zu werden
brauchen, als sie von den geehrten Herren Theilnehmern der Generalversammlung be-
reits vorgestern im geodätischen Institute der technischen Hochschule in Augenschein
genommen wurden, sind vorzüglich zu nennen: erstens, ein Héhenkreis von Ertel & Sohn
in München von 22 Centimeter Durchmesser, welcher jedoch im Jahre 1878 durch
Lingke & Co in Freiberg eine Umgestaltung und namentlich eine neue Theilung erhielt,
bei welcher jeder Gradstrich mit einer im Mikroskop zu lesenden Zahl versehen wurde;
zweitens sind hier die beiden Nivellirinstrumente (No. 1252 und No. 1253) von Lingke
anzuführen, welche ich für den vorliegenden Zweck mit Positionsmikrometern zur ge-
nauesten Messung kleiner Verticalwinkel versehen liess, und drittens dürfen auch unter
den Signalisirungsapparaten, welche hauptsächlich aus Bertram’schen Heliotropen be-
standen, die Petroleumlampen mit einfachen Rundbrennern von 22mm und mit Messing-
reflectoren von 34 cm Durchmesser nicht vergessen werden. Diese Lampen, von der
Fabrik Kolb in Nürnberg geliefert, bewährten sich vorzüglich; sie besassen eine voll-
ständige Sturmsicherheit und konnten auf 60 Kilometer Entfernung scharf anvisirt und
bei guter Luft selbst noch mit freiem Auge erkannt werden.

Indem ich mich in Hinsicht auf die Bestimmung der Constanten der Instrumente
und die Anordnung der Kinzelnbeobachtungen auf meine Eingangs erwähnte Abhandlung
berufe, welche alle hier in Betracht kommenden Einzelnheiten enthält, erlaube ich mir
sofort Einiges von den vorläufig erlangten Resultaten mitzutheilen.

In Bezug auf die Lateralrefraetion sind die Ergebnisse negativer Natur, in so
ferne aus unsern Beobachtungen geschlossen werden muss, dass es zwischen Döbra und
Kapellenberg eine merkliche Seitenrefraction nicht giebt. Es ist dieses auch wohl be-
greiflich, wenn man weiss, dass die Luftlinie Döbra—Kapellenberg durchschnittlich
200 Meter über dem Boden und an keiner Stelle in der Nähe einer hohen und weit
ausgedehnten Bergfläche hinzieht, welche vorzugsweise Seitenrefractionen zu veranlassen
geeignet ist. (Nach einer sehr geschätzten Zuschrift, welche ich vor Abgabe des Ma-
nuscripts dieses Vortrags an das Centralbureau der Europäischen Gradmessung, nämlich
am 29. November 1880 erhielt, scheint Herr Etatsrath Andrae in Kopenhagen aus unse-
ren Beobachtungen nicht bloss das Bestehen einer Lateralrefraction überhaupt, sondern
auch eine mit der Entfernung zunehmende Grösse derselben nachweisen zu können,
wenn eine bestimmte von ihm in Bezug auf die Stellung der Mikrometerschraube ge-
stellte Frage mit Ja beantwortet würde. Diese Antwort habe ich unter einer gewissen
Beschränkung geben können. Es darf daher wohl erwartet werden, dass der berühmte
dänische Forscher seinen Gedanken weiter verfolgen und das Ergebniss, zu dem es
führt, durch den Druck veröffentlichen wird.)

Was die Verticalrefraction betrifft, so habe ich die Beobachtungsergebnisse in
einer Wandtafel graphisch dargestellt, welche in verjüngtem Maassstabe meiner Abhand-
lung beigegeben werden wird. Aus dieser Tafel ist zunächst Folgendes zu entnehmen:
