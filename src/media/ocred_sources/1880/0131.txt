Ira wer

|
IE
|
|
(

 

   

MMPS A OTR MUTE

rennet

et

it
ih
2
a
25
3

2

A
a
a=
2È
AS
as

PRESENTE A L’ASSOCIATION GEODESIQUE PAR C. CELLERIER. 13

Ak : AUDE
I

nn Rdn

 

A+B

on aura par la formule (9),
v = arc tang (c’ tang w) — are tang (c tang w) ;

on peut regarder # comme le même dans les deux termes à cause de sa lente variation ;
il en résulte
3 (U — c) sin 2 wu

tang y == 34 :
8 cos 24 + ce’ sin 2u ?

 

ce et c’ différant peu de + 1, on aura à peu près

v= 4 (ce — c) sin 2 u;
ainsi la difference de phase est maxima au milieu du cycle, quand sin 2u= tt I: ce
maximum est

 

 

 

A—B A’ — B’
te -9=— 45) 44 Try)
ou d’après les formules (14),
1 rm 1, 20 D
= n —n mus DT a y

Cette valeur peut-être sensible; elle est toutefois assez petite pour indiquer que les deux
pendules resteront à peu près en opposition.

Durées. Posons

T= +4),

m

T etant Vintervalle entre deux passages successifs du premier pendule par la verticale
?

et w un petit nombre variable qui s’appellera l'erreur d'influence. On aura aux deux
passages

mi+f—v=Üt+drn, MÉLT +f—v— Av (+97:

vû la lenteur de variation de W, on peut remplacer son accroissement /\w par LE. . n ,

d’où résulte

il DAL gee Me ET x aw

 

“m. de 0m du m 20 Do?
ou d’apres la formule (10)
D B
(15) sn B (A cos 2 wu + B)

TT AIT BEA De.

 
