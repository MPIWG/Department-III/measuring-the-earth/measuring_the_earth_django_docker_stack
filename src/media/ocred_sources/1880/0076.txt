 

64

M. le Président donne lecture de différentes lettres d’excuses, entre autres de

M. M. Tinter à Vienne, de Arbuas- Moreira à Lisbonne, von Darozzi &
Bucarest, [Zilgard et Peirce & Washington.

Ces messieurs regrettent vivement que d’autres occupations urgentes ou la
maladie les empéchent d’assister a la conférence. M. le Président ajoute, qu’en outre,
il est fort à regretter que les représentants du Coast-Survey des Etats-Unis aient
été empèchés, par les affaires de famille, de se rencontrer à cette réunion; mais
qu’il à le plaisir, de saluer dans l'assemblée un ancien membre du Coast-Survey, le
Directeur de l’Observatoire de la République Argentine, M. le Dr. Gould de Cördoba.

Il donne ensuite lecture d’une lettre de M. Hügel. de Darmstadt, par laquelle il
regrette de devoir, par suite de son âge avancé, renoncer à son Commissariat, et se
recommande au bienveillant souvenir des membres de la Conférence.

Le Président prie ensuite les secrétaires de bien vouloir communiquer à l’assem-
blée le Rapport de la Commission permanente, communication qui est faite par M.
Bruhns en langue allemande et par M. Hirsch en langue française.

Rapport de la commission permanente.

D'après le règlement la commission permanente doit présenter à la conférence
générale un rapport sur son activité depuis la 5"° conférence et sur les progres de la
mesure des degres en Europe.

La commission permanente s’est réunie quatre fois depuis la clöture de la der-
niere conférence générale. Immédiatement aprés la conférence de Stuttgart, elle tint
séance le 3 octobre 1877, après s'être constituée dès le 1" octobre, par la nomination
de.M. Ibanez comme president, de M. de Bauernfeind comme vice-président, et de M. M.
Bruhns et Hirsch comme secrétaires. Elle vérifia les procès-verbaux de la dernière con-
férence, chargea les secrétaires de la publication des comptes-rendus et expédia quelques
affaires pendantes.

En suite d’une aimable invitation du Sénat de la ville libre de Hambourg, une
circulaire, lancée au printemps de 1878, convoqua la réunion suivante de la commission
permanente dans cette ville, où les séances eurent lieu du 4 au 8 septembre.

En 1879, la commission, répondant à une gracieuse invitation du conseil d'Etat
du canton de Genève, se réunit la troisième fois, du 15 au 20 septembre, à Genève, où,
une fois les questions scientifiques vidées, le nom de Munich fut mis en avant comme

   

po de 4 a 44311 ha Aa il,

li

jé mAh bé Did LL ul ds ily nn 2

+1 ppéassttiétéée-cnests nd |

 
