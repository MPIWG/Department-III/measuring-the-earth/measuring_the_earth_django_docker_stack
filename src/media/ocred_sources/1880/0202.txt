10

   

Triangulation de la Meridienne de Dunkerque. (Partie Sud.)

 

 

 

 

 

 

 

eal
à nüleabion des Latitude. | Eee | Epoque. Purecuet Instrument. Remarques.
D à Points. I bude *) et Observateurs. |
| | | |
DBart......... 1452947 0”\,20° 7.14r|
LL HONIMEIMAC : : : . . . . 145. 34 14 119 47 10 |
| 57 | Anbassin . . . . .. PAS 4 19:48 20"
| 98) Violan.-......- (457 50 20 15 7. |
1 59 | La Bastide . . ... 44 50 8 |19 46 54 | | |
) 60|Montsalvy .....) 44 42 53 |20 9 58 | |
61|Rienpeyroux..../44 19 2 19 53 38 | i |
l 62 Rodez. . . . . . . 44 21 5 |20 14 15 @
| #65) a Gaste . . . . . . | 44 7 57 | 20 18 10 2 ®
W 64 | St. Georges . . . . 44 0 42 19 56 16 ; = | H
I 65|Cambatjon..... |43 51 55 | 20 13 23 83 a =
i 66|Mont-Redon .... 43 43 1 |19 58 16 a 2 i |
| 67 |Montalet ....../48 41 0 | 20 24 19 = m = |
| 68 | Saint-Pons . . . .. |43 31 34 | 20 28 40 2 3 = |
i CONNOPE .-.....: |43 25 29 |20 7 31 © 2 D |
| wo Alanlo......-- 143, 8 54 120 17 26 au = D |
q 71 | Carcassonne . . . . 43 12 55 |20 0 46 Fe E a
| 72 Bugarach.....- As 51 20, 229 = © | =
Bidanch......:. 42 54 38 | 20 20 31 m =
j 74 Forceral ...... 42 43 38 | 20 21 44 D
1 RS lESspira .. ..... 412 49 55 2029 47 a
| 6 Vernet ....... 42 43 17 |20 32 55
4 Terme austral.
i na wernet »...... 42 49 28 |20 34 46
Terme boréal. |
78 | Puy de la Estella. | 42 30 54 20 12 57
79 | Puy Camellas . . . | 42 26 27 | 20 29 26

 

 

 

 

 

 

 

Triangulation de la Méridienne de Fontainebleau à Bourges.

31 | Bois Commun . .

crie be 2e M an db à asus à

PAS PET ANT ET ER NE nn

 

 

42 | Bourges. . ..... Déjà mentionnés. | Ê |
29 Chapelle fa Reine . | 38 |
80 | Haut du Turc . . .| 47 49 40 | 20 10 59 = 2
SIıGen. ........ A7 41 9 |20 17 40 : SE | 2
82 Assigny....... 47 25 56 20 25 51 es ce | =
83 Humbligny .....|47 16 8 2018 4 og | 2
84 | Montargis ..... 47 59 59 | 20 28 27 2 2 2
85 Les Bezards . . . .| 47 48 19 20 24 50 do ae a
86 | Montifaux. . . . .. 47 36 34 | 20 35 45 os =
GT ROUY  ..:..-: 47 29 11 | 20 49 48 LE | 5
88 | La Charité . .... 47 10 41 | 20 40 48 fe 7
| 89 |Saligny le Vif...|47 2 44 20 25 50 | 2a |

 

 

 

 

 

Triangulation du Parallele d’Amiens. (Partie Orientale.)

NT

 

 

 

 

 

12 | Villers-Bretonneiux | | |
10| Sourdon ...... Déjà mentionnés. | = |
15 Beauquene ..... if | 2
SO) Lihons ....... |49 49 35 |20 25 41 | Se zi |
91|Hébuterne ..... 0 0a 2018 3 2 © |
a ee 50 022 2041 8 ai As = |
| 93 | Saint-Quentin . . . 49 50 55 |20 57 18 | =e 2
94 | Prémont . ..... 60 053 21 8 25 = o& =
Op Guise ........ 49 53 52 |21 17 16 @ “3 © %
am -......- 49 33 48 |21 17 19 Se | S
97 | La Bouteille . . . . | 49 53 14 |21 38 16 ae | 5
| 98] Mainbressy . ...|49 43 2 |91 53 51 = | |
i 99 | Trou du Sable. ...|49 53 23 |22 6 0 = |
| |
|

 
