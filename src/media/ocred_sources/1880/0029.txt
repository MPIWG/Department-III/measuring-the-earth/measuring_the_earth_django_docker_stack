or

1

r

en

Rn

TRS

al ay

A EP QT TOP EP Heme

a

selben vorgenommen; die Resultate sind in der Publikation des geodätischen Insti-
tutes: Astronomisch-geodätische Arbeiten im Jahre 1878, Berlin 1879, niedergelegt.

Im Sommer 1879 konnten astronomische Arbeiten aus dem Grunde nicht aus-
seführt werden, weil die Mitwirkung aller disponiblen Kräfte des Instituts zur Nach-
messung der Schlesischen Grundlinie mit dem neuen internationalen Basisapparat
erforderlich war.

Im Winter 1879/80 wurden äls eine vorbereitende Arbeit für die Ableitung der
Erddimensionen für alle diejenigen Punkte, deren Polhöhen und Längenunterschiede
durch direete astronomische Beobachtungen ermittelt worden sind, von den Bessel’schen
Dimensionen des Erdsphäroids ausgehend die gegenseitigen Azimuthe und die Längen
der geodätischen Linien berechnet, sowie die numerischen Differentialgleichungen auf-
sestellt, welche zwischen kleinen Aenderungen der gegebenen und der gesuchten Grössen
bestehen. Im Ganzen sind 73 Linien dieser Art bearbeitet worden.

Da die Reduction der früheren Polhöhenbestimmungen zum Theil unter Anwen-
dung der älteren Präcessions-, Nutations- und Aberrationsconstanten erfolgt war, es aber
wünschenswerth erscheinen muss, alle Polhöhenbestimmungen auf ein und dasselbe
System zu beziehen, so ist @ine Umrechnung aller inländischen Polhöhenbestimmungen
auf die inzwischen von den Ephemeridensammlungen adoptirten neueren Constanten von
Struve und Peters in Angriff genommen worden. Als Hülfsmittel für die Berechnung
der Reduction vom mittleren auf den scheinbaren Ort der Sterne haben die in der
Schrift: Struve, Tabulae quantitatum Besselianarum pro 1840 ad 1879, Petropoli
1861—1871, gedient.

Bei dieser Umrechnung wurde auch behufs Erzielung einer möglichsten Gleich-
formigkeit und Zuverlässigkeit der Polhöhenbestimmungen Veranlassung genommen, die
Sternpositionen selbst einer Prüfung zu unterziehen und, soweit dies angängig, die bei
der Reduction der Beobachtungen in Anwendung gebrachten Werthe durch inzwischen
erlangte zuverlässigere Positionen zu ersetzen. Als Grundlage wurde in dieser Bezie-
hung in erster Linie der Fundamental-Catalog für die Zonenbeobachtungen am nörd-
lichen Himmel von Aumers, Leipzig 1879, aus dem Grunde gewählt, weil die Positionen
dieses Cataloges die zuverlässigsten Werthe darstellen, welche auf Grund des gegen-
wärtig vorliegenden Beobachtungsmaterials sich ableiten lassen. In denjenigen Fällen,
wo der betreffende Stern nicht in dem genannten Cataloge enthalten war, wurden mög-
lichst sichere Positionen aus dem Gradmessungscatalog oder anderen neueren Üatalogen
unter Reduction derselben auf das Declinationssystem des Fundamental-Cataloges abgeleitet.

Diese Umrechnung der inländischen Polhöhenbestimmungen ist gegenwärtig noch
im Gange, wird aber im Laufe des nächsten Winters wohl sicher zum Abschluss gelan-
gen können.

Umfassendere praktisch astronomische Arbeiten konnten im Sommer 1889 aus
dem Grunde nicht ausgeführt werden, weil die Mitwirkung der Assistenten der astrono-
mischen Section an der Nachmessung der alten Berliner Grundlinie mit dem neuen inter-

©
3

 
