1 PY PA PPP PUPPY YT Tey!

DE RNE EURE

IPP TURE

mr

RETTET

rw

Nobile, A. Memoria sulle stelle cadenti. Napoli, 1838.

Nota. Gia nel 1838 lAstronomo Antonio Nobile mediante tre osser-
vazioni di stelle cadenti fatte contemporaneamente a Napoli e Palermo
aveva trovato per differenza di longitudine fra i due Osservatorii 32 3658.
Nel Nautical Almanac secondo un trasporto di cronometri fatto non si
sa quando, nè da chi si trovava invece registrata questa differenza
3™ 34569.

Nobile e Tacchini. Determinazione telegrafica fra gli Osservatorii di Napoli e Palermo.
Napoli, 1874.

Nota. Si ricerca un più esatto valore della differenza di longitudine
fra Napoli e Palermo e si trova fra il centro dell’Osservatorio di Napoli
e la sala meridiana di quello di Palermo 42 = 3 355823 + 05013 essendo
Palermo pit occidentale.

Opérations géodésiques et astronomiques pour la mesure d’un arc du parallèle moyen.
Milan, 1825 et 1827. (2 Vol. avec des planches.)

Nota. Misura di un arco del parallelo medio da Bordeaux a Fiume.
(1821—1823.) La porzione centrale fra la Savoja ed il Ticino venne affi-
data ad una Commissione di Ufficiali ed Astronomi Piemontesi ed Austriachi.
Detto tratto si attaco da una parte al lato Colombier —Granier della
triangolazione francese, dall’altra al lato Vigevano—Busto proveniente della
base di Somma, cosiché la rete Lombarda gia collegata coll’Austriaca, ‘si
riuni anche traverso il Piemonte colla rete francese.

Oriani, B. Formole per calcolare la latitudine e la longitudine sullo sferoide elittico.
Milano, 1821.
~ Posizione geografica di alcuni monti visibili da Milano. (Effem. astr. di
Milano, 1823.)
= Posizione geografica di alcuni monti e citta della Lombardia. (Effem. astr.
di Milano, 1824 e 1825.)
— Misura del arco del meridiano compreso fra Milano e Genova. (Effem. astr.
di Milano, 1827.)
Piazzi, G. Differenza di longitudine Palermo—Parigi. (Conn. des temps 1835.)
Nota. Mediante otto occultazioni osservate a Palermo da Piazzi (dal
1794 al 1817). Daussy ha trovato 44” 4°] (medio di 4 valori.)
Piola, Don & Sul moto d’un pendolo etc. (Effem. di Milano 1831. App. 37—75.)
mon Sulla teoria del pendolo. (Ibid. 1832, 75—93.)
Plana, G. Note sur la figure de la terre et la loi de la pesanteur à sa surface d’après
l'hypothèse publiée en 1690. (A. N. XXXV, 1853, col. 371--78)
Sur le pendule composé dans un milieu résistant. (Bruxelles, Acad. Sc. Bull.
1832—34, pp. 190—3.)

7%

 
