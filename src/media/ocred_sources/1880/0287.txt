ur Fin Semen PN emer errr MT |

TT Ir! TIME Vel

THOT

mney

veel LUEUR À LL Ga a tia veer
DOTE TOR CPU DIT EPP GET CPR TOUTE

à
am

 

le niveau moyen dans un de ses ports et préparer sur son territoire la jonction de
ce point par des lignes qui remonteront le Danube et traverseront la Hongrie, à l’un
des ports Autrichiens de l’Adriatique.

Suivant le questionnaire de la circulaire, nous arrivons aux appareils et métho-
des employés.

Sur ce point l'Association s’est abstenue, très sagement suivant nous, de vouloir
trop règlementer. Et cependant il existe une uniformité suffisante pour les points essen-
tiels. Les lunettes employées ont des ouvertures variant de 17 mm (la Hesse) jusqu’à
45 mm (Bavière), la grande majorité étant comprise entre 30 mm et 40 mm; la distance
focale varie de 33°5 (Belgique) jusqu'à 48 em (France); le grossissement est compris
entre 14 (Russie) et 42 (Suisse). Le réticule consiste presque partout en 3 fils hori-
zontaux et 1 fil vertical; dans quelques pays cependant on se contente. d’un simple
croisé de fils, abandonnant ainsi l’avantage d’une triple lecture sur des traits différents
de la mire, et d’un moyen très simple de déterminer la distance de la mire.

La sensibilité des miveaux employés est par contre assez différente; car une
partie, qui a ordinairement { ligne de longueur, correspond à une valeur angulaire
variant de 1:9 (dans un des instruments Suisses en 1877) jusqu'à 20” en Belgique;
pour la grande majorité des instruments la valeur d’une partie du niveau est comprise
GIE SN ea 3.

Quant aux mires qui ont presque partout 3m ou 4m de longueur, elles sont
presque toutes divisées en centimétres ou de 2 en 2 centimétres comme en France, quelques
fois avec indication des demi-centimètres, sauf dans le grand Duché de Hesse où la
division va de 2 mm en 2 mm; en Belgique on a employé soit des mires parlantes dont
les centimètres sont subdivisés en 3mm, 4fhm et 3 mm, soit avec tringle et voyant
donnant le millimètre.

Quant à léquation des mires, la plupart des pays ont donné suite à l'invi-
tation qui leur a été faite, dans le temps, de les comparer à l’étalon de fer du bureau
des poids et mesures à Berne; il serait à désirer que les quelques pays qui n’en ont
pas profité encore (Pays-Bas, Belgique, Saxe, Russie, Portugal), le fassent, parce
qu'il suffira alors de comparer l’étalon de Berne au nouveau prototype international du
metre ce qui pourra se faire sous peu, pour exprimer toutes les altitudes de l’Europe
dans la nouvelle unité métrique normale. Dans la plupart des pays les mires ont été
controlées en outre au moyen de différents étalons du mètre ou de la toise, dont les
équations ne sont pas définitivement connues. Il est à regretter qu'on n'ait pas con-
trôlé la variabilité des mires avec le temps partout avec les mêmes soins; car tandis
qu'en Saxe par exemple, on est allé jusqu'à comparer la mire tous les jours de l’opéra-
tion à un double mètre en acier fondu étalonné à Berlin, et aux Pays-Bas où l’on les
examine également tous les jours, en Bavière on s’est contenté de les comparer une
fois pour toutes & un metre en laiton de Breithaupt, et en France on ne s’en est pas
inquièté du tout. Dans la plupart des pays cependant ou les a comparé chaque année,
et même souvent au commencement et à la fin de chaque compagne. — En Belgique

2*

 
