rn we

m

Eu aT a rR RRP | a reger en HH

tere qe

à

À
if
À
4
i

Anhang III.

Bericht

über

astronomische Ortsbestimmungen, Längen-, Breiten- und Azimutbestimmungen
von

Dr -@: Bruh»ns

(Nebst 2 Karten.)

A: der Conferenz der permanenten Commission in Genf wurde der Bericht über die
zum Zwecke der Europäischen Gradmessung ausgeführten astronomischen Ortsbestim-
mungen Herrn von Oppolzer und mir übertragen; doch im Laufe der Arbeit stellte es
sich heraus, dass es besser sei, das ganze Referat in eine Hand zu vereinigen, und da
Herr von Oppolzer im Jahre 1877 den Bericht übernommen gehabt hatte, ersuchte der-
selbe mich, für die gegenwärtige Conferenz über diesen Gegenstand zu referiren.

Das erste Verzeichniss der ausgeführten astronomischen Bestimmungen wurde
von mir auf der dritten Allgemeinen Conferenz im Jahre 1871 gegeben und zugleich
eine Karte hinzugefügt. Auf der vierten Allgemeinen Conferenz in Dresden wurde der
Antrag angenommen, eine neue Zusammenstellung anzufertigen, nebst Angabe der Orte,
wo Pendelbeobachtungen angestellt waren, und zugleich sollten die Titel der Publica-
tionen, in welchen die Resultate enthalten, namhaft gemacht werden. Diesem Antrage
wurde durch die von den Secretären der permanenten Commission, Herrn Hirsch und
mir, gemachte Uebersicht in unsern Verhandlungen für das Jahr 1875 Folge gegeben.
1876 gaben die italienischen Herren Commissare eine Karte mit den ausgeführten Be-
stimmungen der Längendifferenzen und endlich berichtete auf der fünften Allgemeinen
Conferenz in Stuttgart im Jahre 1877 Herr von Oppolzer und legte zwei Karten vor,
eine mit den Längenbestimmungen, die andere mit den Breiten- und Azimutbestimmungen,
welche in den Verhandlungen dieser Conferenz gedruckt sind.

Da seit dem Jahre 1875 kein vollständiges Verzeichniss gegeben und seit jener
Zeit eine Anzahl Ortsbestimmungen definitiv reducirt und publicirt ist, schien es mir
erforderlich, eine vollständigere Zusammenstellung als im Jahre 1875 zu geben und ich
sandte daher im October 1879 an sämmtliche Herren Commissare ein Circular mit einer
Anzahl gestellter Fragen, um deren Antwort ich bat.

Aus den eingegangenen Antworten ist nun zunächst das folgsnde Verzeichniss
der Längendifferenzbestimmungen und der Breiten- und Azimutbestimmungen hervor-
gegangen. Das Verzeichniss der Längenbestimmungen ist, da dasselbe oft benutzt wird,
ganz vollständig wiederholt, während für die Breiten- und Azimutbestimmungen die Er-- °
gänzung des 1875 gegebenen Verzeichnisses zu genügen schien. Die Anordnung von
1875 ist beibehalten und es sind die Verzeichnisse die folgenden:

 
