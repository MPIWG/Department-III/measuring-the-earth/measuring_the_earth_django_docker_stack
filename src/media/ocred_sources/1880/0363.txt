BE ge IT Te

BE Hi PR

ITP TT

w tier

55

Cohen Stuart. Mededeeling omtrent de in 1875 en 1876 uitgevoerde Naauwkeurigheids
Waterpassing.. (Over gedrukt uit de Notulen der Vergadering van het
Koningliik Instituut van Ingenieurs van 13 Februarii 1877.)
= Uitkomsten van de in 1875 en 1876 uitgevoerde Naauwkeurigheids Water-
passing. Lithographirt.
— Ids pro, £877:
Hennert, J. F. Ondezoekning omtrent de waare gedaante der Aarde. Middelburg 1773.
(Verhandl. Genootsch. Vliessing. III et IV.)
Huyghens, C. Traité de la lumière. Avec un discours de la cause de la pesanteur.
Leyden, 1690.
Kaiser, F. De volledige bepaling van persoonlijke fouten. (Verslagen en Mededeelingen
der kon. Akad. XV, 1863.)
— Eenige Opmerkingen omtrent de Periodieke Fouten van Mikrometer Schroeven.
Naar Anleiding van de jongste Ondezoekningen ann de Sterrewacht te
Leiden. Amsterdam 1866. (Mededeelingen der koningl. Akad. van Weten-
schappen. Afdeeling Naturkunde, 24 Reeks, Deel I.)
— Ueber einen neuen Apparat zur absoluten Bestimmung von persônlichen Fehlern.
Amsterdam 1867. (Ebenda 2% Reeks. Deel II.) (Verslagen en Mede-
deelingen der kon. Akad. 1868.)
_ Sur la détermination absolue de l’erreur personelle dans les observations
astronomiques. 1863. (Ebenda T. 15 p. 173—220.)
— Annalen der Sternwarte in Leiden. Band I, Harlem 1868. Band II, Haag
1870. Band III, Haag 1875. Band IV, fortgesetzt von van de Sande
Bakhuyzen. Haag 1877.
Kaiser und Cohen Stuart. De eischen der medewerking aan de ontworpen graadmeting
in midden Europa. Amsterdam 1864. —

Krayenhoff. Die Landesvermessung der Batavischen Republik. (Zach, monatl. Corresp.
1305, 1804,83)
— Précis historique des opérations géodésiques et astronomiques faites en Hol-
lande pour servir de base à la topographie de cet état. La Haye 1815.
— Recueil des observations hydrographiques et topographiques faites en Hol-
lande. La Haye, 1835.
Lulolfs, J. Inleidinge tot eene natur en wiskundige beschouwing des ardkloots tot dienst
der Landgenoten geschreven. Leyden 1750. (Deutsch von A. G. Kastner.
Göttingen u. Leipzig, 1755.)
Muschenbroek, P. van. Dissertationes physicae ete. Leyden, 1729.
Nieuwland, P. Douwe’s Methode, aus zwei ausser dem Meridian liegenden Sonnenhöhen
die Breite eines Ortes zu finden. (Bode’s Jahrb. Suppl. I, 1793.)
Oudemans, J. A. Ch. Die Triangulation von Java. Erste Abtheilung, Vergleichung des
Repsoldschen Basismess-Apparates mit dem Normal-Meter, Batavia, 1875.
Zweite Abtheilung, Die Basis von Simplak. Im Haag, 1878.

 
