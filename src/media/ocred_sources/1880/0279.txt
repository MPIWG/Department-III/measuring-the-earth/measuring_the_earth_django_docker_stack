nun ee Se |

1

rm

RRR A RR à à em dun

i
i
i
43
3
3
3
3
2:
2
ë
a
aS
==
2»

8. LHaactitude des résultats:
Quelle est @errew probable par kilomètre, déduite soit des doubles
nivellements, soit de la clôture des polygones ?

9. Indication exacte des titres et des dates des publications faites sur les nivelle-
ments de precision dans votre pays.

Dans l'espoir que vous voudrez bien contribuer à rendre possible le résumé
général de l’hypsométrie actuelle de l’Europe, en me transmettant en temps utile et 52
possible, jusqu'au 15 Juillet, des données succintes et précises sur les points indiqués,
j'ai l'honneur, Monsieur et très-honoré collègue, de vous présenter l’assurance de ma

parfaite considération.

Dr. Ad. Hirsch.

Parmi les 18 pays auxquels j'ai adressé cette circulaire, 14 ont répondu aux
questions posées, tandis que 4 (le Danemark, la Suède, la Norwège et la Roumanie) ne
m'ont point envoyé de renseignements.

Avant de résumer dans un tableau général les données qui ont été fournies, il
sera utile de rappeler brièvement les décisions qui ont été prises par les deux pre-
mières Conférences générales au sujet des nivellements et qui ont posé les règles à
suivre dans ces opérations.

Déjà dans la première Conférence générale tenue à Berlin en 1864, on a pris
sur la proposition de votre rapporteur, les résolutions suivantes:

1. Il est à désirer que dans les pays qui participent à la mesure d'arc de
l'Europe centrale, on exécute à côté des déterminations trigonométriques
de hauteur, des .nivellements de premier ordre, en employant la méthode
du nivellement depuis le milieu et en ménageant le contrôle nécessaire par
la combinaison polygonale des stations. On suivra dans ces nivellements
de préférence les lignes de chemin de fer, les canaux, les routes etc.

2. Les hauteurs de chaque pays seront rapportées à un seul point zéro,
solidement établi; tous ces points de départ seront reliés entre eux par
un nivellement de précision.

3, Le niveau moyen des différentes mers doit être déterminé dans le plus
grand nombre possible de ports et de préférence au moyen d'appareils
enregistreurs. Les points zéro des échelles de ports doivent être compris
dans le nivellement de premier ordre.

4. Selon les résultats de toutes ces mesures on choisira plus tard le plan
général de comparaison pour toutes les hauteurs de l’Europe.

Comme en Suisse et dans quelques autres pays, on s'était immédiatement mis

à l’œuvre et qu’on avait obtenu des résultats très-satisfaisants, la seconde Conférence,

réunie en 1867 à Berlin, a pris dans la séance du 7 Octobre 1867, les résolutions sui-
1*

 

 
