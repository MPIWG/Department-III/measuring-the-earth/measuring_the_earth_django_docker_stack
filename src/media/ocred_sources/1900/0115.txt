5 Te WTeRTeeTeAge Feng Tin oT

Ani

ir

1 ln

i

&
ı»
iE
12

 

 

109

(c.) The Indian observed Azimuths.

Astronomical azimuths have been observed on all the series of the Indian trian-
gulation. These observations probably owed their origin to the belief held by geodesists in
the early days of the century that azimuths were required to control the-direction of trian-
gulation just as bases were required to control its length. The lesson was gradually learnt
that the errors generated in triangulation are smaller than those caused by local attraction
in astronomical azimuths, and the idea of foreing triangulation into accordance with observed
azimuths has long since been abandoned. But the observation of azimuths has been con-
tinued, partly with the objeet of discovering the local attraction in the Prime Vertical,
partly to bring evidence to bear on the figure of the earth, partly to gauge tlıe correetness
of the fundamental azimuth and orientation of the Indian Survey and perhaps partly as a
check against the entrance of any gross mistake into the triangulation.

Azimuths being mere angles, and not being convertible, as are latitudes and longitudes
into linear distances on the spheroid, afford no evidence as to the dimensions of the Earth,
but they can be used to determine the elliptieity. Colonel Clarke on page 291 of his Geodesy
states that the observation of the difference of longitudes gives us no information that is
not also given by the observation of azimuth. Though this may be theoretically correct,
in practice azimuths deserve less weight than either observed latitudes or longitudes. Azimuth
observations are taken with reference to only two stars, and are consequently dependent on
the correctness of the places of two stars. They are dependent on the accuracy of the trian-
gulation to a greater extent than either latitudes or longitudes, because in the deduction
of the deflection of the plumb-line from azimuthal observations, the difference between the
astronomical and geodetic azimuths, has to be multiplied by the cotangent of the latitude.
This factor is larger than 6 in southern India and accordingly the original errors of obser-
vation of the star’s place, and of the triangulation are sextupled.

The details of the Indian observed azimuths have been published from time to
time in the several volumes of the Great Trigonometrical Survey of India. They are
collected together for the first time in the following table.

S. @G. BuRraRD.
