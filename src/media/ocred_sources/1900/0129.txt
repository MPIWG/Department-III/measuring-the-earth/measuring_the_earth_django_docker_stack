ET En rTTeRgTT RER

La iRl

ww

Fi SRTTETA N LE

121

 

 

 

Snconns or Seconpsor | (9 ya 0)
CorrEcrion | SUM OF | COMPUTED 7
Branıon Onsmvad | "zo nnan | 1Asr nwo | ononumo |PERIBONON op
POLE COLUMNS AZIMUTH

5 En PRIME VERT.

X TAN. LAT.
Daiadhari 3037 39823553 + 0".15 52.68 50”.41 + 2,27
Bhaorasa 95°.412/.39".86 2.0711 39",47 38".08 + 1".39
N. E. End of Base 80, 46 33”.96 +- 0".08 34.04 81”.61 -.— DRS
Surantal 10.272.48.97 >: 0.02 43”,39 40”.46 5.2.98
Losalli 305° 52’ 55.80 =0",07 95.79 I S0 19%
Salot 175° 58’ 10”.16 u 1016 10.89 Ds
Kamkhera 154° 45:.36',67 — 0”,05 36.62 35.31 + 1”.31
Ahmadpur 1852: 10:.06°,27 — 0.08 56”.19 53”.91 + 2".28

 

 

 

 

 

 

 

The geodetic azimuths have been computed on the assumption that the azimuth
of Surantal at Kalianpur is 190° 27’ 5”.10.

In the following table the value of the fundamental azimuth at Kalianpur is
deduced from the observed azimuth at each station of the group by applying the geodetic

difference of azimuth derived from the triangulation.

 

 

 

 

 

 

 

 

 

 

Eh Obenkyan zen GEODETIG RESULTING AZIMUTH

DIFFERENCE AT KALIANPUR.

Daiadhari 303° 32'.52°,68 119°.5°45431 190927,.7.87
Bhaorasa 95° 19.397,47 95° 14.27.02 6",49
N. E. End of Base 80° 46’ 34.04 109° 40’ 33”.49 7.2.09
Surantal 10 27.48.39 179° 59’ 24.64 8.03
Losalli 305° 52’ 55.73 119 25.82.20 8.58
Salot 175° 58’ 10”.16 14998 54.31 4,87
Kamkhera 154° 45’ 36.62 852.47 29”.79 6.41
Ahmadpur 185° 10° 56.19 re 1119 77.38
Mean 190° 27’ 6.39

 

We have, then, the three following values of the azimuth at Kalianpur.

Value adopted in computations of the triangulation 190° 27° 5”.10
6".29
6”.39.

On the assumption that the yalue derived from the group is freed from the

Mean and

Value derived from the group

best. observed value. .. 2...

 

 
