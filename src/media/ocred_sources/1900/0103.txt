aaa 4

NE Fame WTTTATEERTg "nn

vera N TR

Es ist deshalb für die Erdmessung von der grössten Bedeutung, dass das Präsidium
die Ansichten der Herren Delegirten über die Verwendung der Dotation kenne, und Herr
Bakhuyzen bittet die Herren Delegirten, ihre Meinungen über die Finanzverwaltung entweder
während dieser Öonferenz, oder nachher, dem Präsidium mitzutheilen.

Herr Ferster übernimmt das Präsidium und ertheilt Herrn Helmert das Wort für seinen
Bericht über den Arbeitsplan des Öentralbureaus für die nächsten Jahre (Siehe Beilage B. III).
Herr Celoria macht die Bemerkung ‘dass in diesem Plan die Längenbestimmung
Paris-Greenwich nicht erwähnt ist, welche nicht nur die Sternwarten dieser beiden Städte,
sondern ganz besonders die internationale Erdmessung interessirt; nach der Meinung des

Herrn Celoria ist es angezeigt, dass unser ÜOentralbureau und auch die Erdmessung an

dieser Arbeit, welche eine allgemeine Bedeutung hat, theilnehme, und er schlägt vor die
Conferenz möge den Wunsch aussprechen, dass die Directoren der beiden grossen Sternwarten
in Paris und Greenwich sich bei der bevorstehenden Längenbestimmung zwischen beiden
Sternwarten mit dem Oentralbureau in Verbindung setzen.

Herr Hirsch unterstützt den Antrag des Herrn Celoria, und macht die Bemerkung,
dass diese neue Bestimmung auf Anregung der Erdmessung unternommen wird. Es ist also
wünschenswerth dass dieses grosse Unternehmen unter Mitwirkung der Erdmessung zu
Stande komme.

Herr Darwin wird Herrn Christie bitten sich mit dem Centralbureau in Verbindung
zu setzen, aber er meint, es sei viel besser dass Herrn Celoria’s Antrag von der Üonferenz
genehmigt werde. Er fragt, ob einer der französischen Astronomen der Pariser Sternwarte
der Conferenz einige Mittheilungen in Betreff des geäusserten Wunsches machen kann.

Der Präsident erklärt, er sei überzeugt, Herr Zewy, Director der Pariser Sternwarte,
werde gar keine Einwände gegen den geäusserten Wunsch erheben. Er bringt den Antrag
des Herrn Celoria zur Abstimmung.

Dieser Antrag wird genehmigt.

Herr Zachariae entwickelt einen Antrag in Bezug auf Art. 4 des Arbeitsplanes des
Oentralbureaus, in der Absicht, dass die Erdmessung ihr früheres Vorhaben, eine centrale
Pendelstation in Breteuil zu errichten, aufgebe.

Dieser Antrag lautet wie folgt:

In Anbetracht, dass in den letzten Jahren die nationalen Hauptpendelstationen durch
zahlreiche relative Pendelbestimmungen mit einander verbunden worden sind, hat die Gründung
einer Centralstation zur Vergleichung der verschiedenen absoluten Bestimmungen der Pendel-
längen nicht mehr dasselbe Interesse wie zu der Zeit als, in Folge eines Beschlusses der
Generalconferenz der Erdmessung, das internationale Mass- und Gewichtscomit& gebeten
wurde sich mit der Gründung und Organisation einer ÜÖentralstation in Breteuil zu be-
fassen ;

in Anbetracht weiter, dass in dem internationalen Mass- und Gewichtsbureau, die
genaue Zeitbestimmung vielleicht auf grosse Schwierigkeiten stossen werde, und dass
dieses internationale Bureau schon mit sehr wichtigen metrologischen Arbeiten im Interesse
der Erdmessung beschäftigt ist;

13

 
