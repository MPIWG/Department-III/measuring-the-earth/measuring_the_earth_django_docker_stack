un, u ib a we ua 01 © © |
SE ar WETTER gen

ML.

Sir

ir

je

han c orraandhahidnhbihih: kön:
ET TITTEN T

26.
27.

29.

50.
31.

IR.
38.

54.

35.

36.

Herr
Herr

. Herr

Herr

Herr
Herr

Herr

Herr

Herr

Herr

XL MEXICO.

Anguiano, Ingenieur, Director der geodätischen Commission in Mexico, in Taeubaya.
Valle, Director der nationalen Mexicanischen Sternwarte in Tacubaya.

XU. NORWEGEN.

Mohn, Professor an der Universität in Christiania, Director des Norwegischen
meteorologischen Instituts.

XII. NIEDERLANDE.
van de Sande Bakhuyzen, Director der Sternwarte in Leiden, ständiger Secretär der
internationalen Erdmessung.
Hewvelink, Professor an der Polytechnischen Schule in Delft.

Oudemans, Ober-Ingenieur und Ühef des geographischen Dienstes in Ost-Indien
a. D., früher Professor der Astronomie und Director der Sternwarte in Utrecht.

XIV. RUMÄNIEN.

General Dratiano, Director des geographischen Instituts des Generalstabs in Bucharest.
Commandant ZAimniceano, vom Generalstab in Bucharest.

XV. RUSSLAND.

General von Stubendorff, Chef der militär-topographischen Abtheilung des General-
stabs in St. Petersburg. ,

XVI. SCHWEDEN,

Dr. Rosen, Professor und Mitglied der Akademie der Wissenschaften in Stockholm.

XVII SCHWEIZ. ,

Herr Dr. Hirsch, Director der Sternwarte in Neuchätel.

0. Die Herren Eingeladenen: Backlund, Benoit, Bischofsheim, Prinz Roland Bonaparte,
Bouillet, Callandreau, Caspari, Ohappuis, Darboux, Deslandres, Frandin, Gautier,
Gill, Gore,, Guillaume, Guyou, Hatt, Janssen, Laurent, Liard, Mascart, Noirel,
Perrier, Stephan.

 
