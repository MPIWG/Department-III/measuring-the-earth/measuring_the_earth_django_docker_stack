2 A EA aha nar=ri
SIE TAT "STATT ir

han
ERETIEIT T

ET
RI

nern
rn

hin
ann

Aires

nk

ln aah haih Ankammihah ’
Da Le TE I Eu LITT 7, BITZ UT IE U DIEIII LEE ELLI

SIXIRME SRANOE

Samedi, 6 Octobre 1900.

Presidence de M. Ferster, Vice-president.
La seance est ouverte & 9 heures ?],.
Sont prösents:

I. Les delegues: MM. Alörecht, Bakhuyzen, Barraquer, Bassot, Börsch, Bouquet de la
Grye, Bourgeois, Bratiano, Oeloria, Darwin, Guarducei, Haid, Hewvelink, Hirsch, Lallemand,
Matthias, Mohn, Nagaoka, Oudemans, Poincare, Bimniceano, Rosen, Schmidt, v. Stubendorff,
Valle, Vigano, Winston et Zachariae.

II. Les invit6ss: MM. Benoit, Bowillet, Caspari, Chappuis, Guillaume, Heraud, d’Ocagne.

M. le President donne la parole & M. le Seeretaire pour la lecture du proces-verbal
de la derniere seance.

Le proces-verbal de la cinquiöme seance est lu et adopte.

M. le Seeretaire annonce que le Bureau a recu une lettre de M. Becker, directeur
de l’Observatoire de Strassbourg, döclarant qu’ä son grand regret il est empäche d’assister
& la Conförence generale, & cause d’observations de pendule dans l’Alsace-Lorraine dont il
est occupe en ce moment.

M. Bouquet de la Grye remplace M. Ferster au fauteuil de la presidence.

M. Ferster, en presentant le rapport de la Öommission des finances, composee de
MM. Zachariae, president, Tinter et Ferster, rapporteur, (Voir Annexe B. IV) rappelle que
dans la derniöre seance le general Bassot a exprim6 le veu de prendre des mesures pour ne
pas s’occuper de ce rapport au dernier moment. M. Ferster fait remarquer que dans presque
toutes les conferences on ne s’est occupe de ia question des finances que dans la derniere
ou avant-derniere seance, et que cet ajournement peut &tre expliqu& en partie par le fait
que, dans le cours des discussions, des r6solutions sont prises qui peuvent avoir une influ-
ence sur la redaction definitive du rapport. Dans cette Conference la presentation du rapport
a ei& encore retardee parce que l’on avait exprim& le vaeu que le rapport füt imprimd

>

 
