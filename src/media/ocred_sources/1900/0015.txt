 

 

=
33

ih
par

bat
an

win

4
ET

 

Tan

temikelt

kasheiiaiahh d &
ET

|
l
!

y

Je regrette aussi que Monsieur de Arillaga soit rappel& en Espagne & l’expiration
de la session du Comite international des poids et mesures et qu’il ne puisse des lors
assister ä notre r&union.

Messieurs, en terminant, je remercie, au nom des delegues de la France, nos
$minents invit6s qui ont bien voulu venir iei, ä& notre priere, temoigner & nos collegues de
l’Association g6od6sique linteröt qu’ils portent & ses travaux, et montrer qu'ils en apprecient
l’importance pour la conquöte definitive du domaine qui est offert & ’humanite, a la con-
dition qu’elle saura l’occuper.

Sur la proposition de M. le president la seance est suspendue pendant un quart d’heure.

A la reouverture de la s6ance M. le prösident donne la parole au secrötaire provi-
soire de l’Association, M. van de Sande Bakhuyzen, pour la lecture de son rapport sur
V’activit& administrative du Bureau.

M. van de Sande Bakhuyzen s’exprime comme suit:

Messieurs,

En vous prösentant mon rapport sur les actes du Bureau de l’Association, je dois
vous faire observer que ce n’est que depuis trois mois que je remplis les fonctions de
secretaire, de sorte que je suis ötranger & presque tout ce qui a öt& fait par le Bureau.

Ce n’est qu’en simple- del&gu& que j’ai suivi l’histoire de notre Association, depuis
notre derniöre r&union, & Stuttgart, jusqu’au mois de juillet de cette annde, et plusieurs
details ne sont venus & ma connaissance que depuis peu de jours. Il m’aurait done 6t&
impossible de composer ce rapport, sans les indications que je dois & l’ancien secretaire
perpetuel, M. le Dr. Hirsch, et surtout au direeteur du Bureau central, M. le prof. Heimert,
qui a eu la bienveillance de m’envoyer les copies des lettres oflicielles qu’il avait regues
depuis notre derniere conference.

Si, malgre cela, j’ai dans ce rapport omis quelques details qui devraient y en,
je vous prie de m’excuser et surtout de me les indiquer, afin que je puisse les inserer
dans le rapport imprime. r

Le nombre des Etats qui ont donne leur adhesion & la Convention actuelle est
rest6 tel quil #tait & l’epoque de notre session de Stuttgart. J’ai seulement & mentionner
une lettre de M. le Ministre de l’instruction publique et des cultes de la Prusse, du 18
novembre 1899, nous notifiant que la Gröce s’est fait inserire de nouveau parmi les Etats
adhörents. A ce quil parait elle avait done retire son adhesion.

Dans le personnel des delegu6ss il y a plusieurs changements & signaler. Le Bureau
a d’abord regu la lettre suivante de M. le colonel von Schmidt:

„Par decret imp6rial d’aujourd’hui, j’ai &t6 nomm& commandant du regiment d’in-

2

 

 
