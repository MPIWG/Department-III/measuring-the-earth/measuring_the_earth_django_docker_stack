 

BEREITET

Een TE

REES HE

72

Meine Herren,

Seit der Allgemeinen Konferenz in Stuttgart, im Oktober 1898, sind 2 Jahresbe-
richte über die Thätigkeit des Oentralbureaus erschienen. Da nun konventionsgemäss auch
am Schlusse des laufenden Jahres ein ausführlicher Bericht erscheinen wird, so kann ich
mich heute auf eine kurze, zusammenfassende Schilderung der Thätigkeit des Centralbureaus
beschränken.

Im Vordergrunde standen die Vorbereitung und die Organisation des internationalen
Polhöhendienstes. Nachdem die Stuttgarter Konferenz sich für die Anwendung des visuellen
Beobachtungsverfahrens ausgesprochen hatte, mussten vor allem die neuen Zenitteleskope
für vier der 6 Stationen, für welche keine älteren geeigneten zur Verfügung standen, fertig
gestellt werden, was wegen Erkrankung: des Mechanikers Wanschaf nur dadurch ermöglicht
werden konnte, dass der Mechaniker Fechner des Geodätischen Instituts die Leitung der
Arbeiten sowie die Zusammenstellung und Justierung der Instrumente übernahm. Es gelang
so, die Zenitteleskope, wie auch die anderen erforderlichen Apparate, rechtzeitig den Stationen
zur Verfügung zu stellen. Für die Einrichtung der Stationen ist die Internationale Erd-
messung den Landesbehörden zu grösstem Dank verpflichtet. Infolge der überall energisch
betrievenen Vorbereitungen konnten alle 6 Stationen noch vor Ende des Jahres 1899 ihre
Thätigkeit beginnen. Durch eine von Herrn Albrecht verfasste „Anleitung”, die auch den
Herren Delegirten übersandt wurde, ist die Gleichmässigkeit der Arbeiten auf den sechs
Stationen nach Möglichkeit gesichert.

Die Berechnung der Beobachtungen erfolgt im Centralbureau unter Leitung von
Herrn Albrecht durch Herrn Wanach und zwei Hülfsrechner. Herr Aldrecht wird demnächst
die Ehre haben, der Versammlung einige vorläufige Ergebnisse mitzutheilen.

Ich darf noch bemerken, dass sich auf der diesjährigen Weltausstellung sowohl ein
visuelles wie ein photographisches Zenitteleskop von Wanschaff befinden; ersteres ist das in
Honolulu und Potsdam benutzte, letzteres das von Herrn Marcuse angegebene.

Ferner möchte ich darauf hinweisen, dass Herr Albrecht die Bahn des Nordpols der
Erdaxe im Erdkörper auf Grund der freiwilligen Cooperation der Sternwarten wiederum
weiter verfolgt hat, und darüber bei Beginn der Jahre 1899 und 1900 Publikationen verfasste.

Die systematischen Lothabweichungsberechnungen im Anschluss an die europäische
Längengradmessung in 52° Breite sind von den Herren Börsch und Krüger unter Assistenz
von Herrn Schendel weiter gefördert worden. Es liegen jetzt Linien vor von Bonn bis Brest
und bis Genua und Nizza, ferner der Wiener Meridian von der Schneekoppe bis zur Insel
Sieilien. Neuerdings wurde die russisch-skandinavische Breitengradmessung mit der Längen-
gradmessung verbunden und ein Netz von Linien nördlich der Linie Bonn-Warschau bear-
beitet, welches namentlich Kopenhagen mit dem ganzen System verbindet. Durch diese
neuen Rechnungen ist die schon früher bemerkte Thatsache bestätigt worden, dass die
kleinen Axen möglichst anschliessender Ellipsen des franz.-engl. Meridianbogens und des
russ.-skand. Meridianbogens einen Winkel von 4.5 miteinander bilden.

Naturgemäss schreiten die systematischen Rechnungen nur langsam vorwärts und

 

ur

vu

=

=
a
zi

=
=
&
Eu

Lakı 1 tulkı REN
Nana een

In ia
mat

3A cam br abheben

 
