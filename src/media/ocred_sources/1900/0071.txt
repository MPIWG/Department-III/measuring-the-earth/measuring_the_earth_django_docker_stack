mm m EEE a A ER |
SE Tr Tee RT FETT

TINO IF

eı

a
TIWTAT

E
ii
ib

'E

I

ARE NEMELTELLT.
I EL Zu UI 0 2

65

merksam machen zu müssen, dass durch die Pariser Oonferenz, an der Stelle des jetzigen
Secretärs, der aus Gesundheitsrücksichten sich am 31 December des vorigen Jahres ge-
nöthigt sah gegen den len Juli 1900 seine Entlassung zu nehmen, ein neuer ständiger
Secretär gewählt werden muss.

Dem 11e Artikel der Uebereinkunft gemäss, müssen bei dieser Wahl die Stimmen in
der Conferenz nach Staaten abgegeben werden, und wir zweifeln nicht dass die Delegirten in
Paris die nöthigen Instruktionen über diesen Punkt von ihren Regierungen erhalten haben werden.

Der Secretär Der Präsident
Dr. An. Hirsca. H. Farr.

Am 8en August folgte ein zweites Circular welches die Tagesordnung nach Art. 2
unserer Uebereinkunft enthielt:

Hochgeehrter Herr und College,

Durch das Circular des Präsidiums der internationalen Erdmessung vom 12en März,
Paris und Neuchätel, wurden die Herren Delegirten eingeladen zur Generalconferenz in
Paris, Palais de la Nouvelle Sorbonne, am 25 September 2 Uhr nachmittags.

Wie haben die Ehre Ihnen, nach Art. 2 der Uebereinkunft von 1896, folgende
Tagesordnung für diese Conferenz mitzutheilen.

1. Eröffnung der Generalconferenz durch den Herrn Minister des öffentlichen
Unterrichts und der schönen Künste.

. Bericht des provisorischen Secretärs.

..Wahl des ständigen Secretärs.

‚ Bericht des Directors des Centralbureaus.

. Bericht über die Breitenbeobachtungen auf den 6 internationalen Stationen.

. Discussion über 4 und 5.

. Programm der Arbeiten für die nächsten Jahre.

. Bericht der Finanz-Commission.

. Provisorisches Budget für die nächsten Jahre.

. Specialberichte über:

SO oa DD dd

m
&)

. die Dreiecksnetze von Herrn General Ferrero,

die Grundlinien von Herrn General Bassot,

die Präcisions-nivellements von Herrn Vice-Admiral von Kalmar,

. die Mareographen von Herrn Bouquet de la Grye,

die astronomischen Messungen vom ÜÖentralbureau (Herrn Prof. Albrecht),

die Lothabweichungen vom Centralbureau (Herrn Director Helmert),

die Schwerebestimmungen vom Öentralbureau (Herrn Director Helmert).
9

Ss mare, ee

 
