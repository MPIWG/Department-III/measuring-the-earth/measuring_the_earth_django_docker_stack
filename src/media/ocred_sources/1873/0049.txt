nn

Pr PV

un 247

fiir 1870.0, e den Betrag der Eigenbewegung, n die Anzahl der Jahre von der Epoche
der Beobachtung bis 1870.0, endlich p das Gewicht dieser Gleichung bezeichnet, so war
die Aufgabe lediglich darauf zurückgeführt, aus dem System dieser Gleichungen die
wahrscheinlichsten Werthe der Grössen x und e zu ermitteln. Nach der Methode der
kleinsten Quadrate lässt sich ein System von Gleichungen der obigen Form auf die
beiden Endgleichungen:
(pa) = (p)e— (pn)e
— (pan) = — (pn)x + (pna}e

redueiren, durch deren Auflösung man unmittelbar zur Kenntniss der Grössen + und €
gelangt. Es setzt dieses Verfahren allerdings voraus, dass man die Eigenbewegung
in Rectascension und Declination als der Zeit proportional ansehen könne, welche An-
nahme in dem Maasse um so mehr von der Wahrheit abweichen wird, je grösser die
Declination der Sterne ist. Da jedoch im vorliegenden Falle die Grenze der Deelination
870 14’ nicht überschritten worden ist und fast ausnahmslos nur kleine Higen-
bewegungen in Betracht kommen, so ist die obige Annahme als zulässig erachtet worden.

Die folgende Tabelle enthält die Ergebnisse dieses Ausgleichungsverfahrens:
die definitiven Positionen für 1870.0 und die Eigenbewegungen, ferner in Columne 3
die Grössen der Sterne nach Drittel-Grössenklassen fortschreitend (beispielsweise be-
deutet 4.5:44, 5.4:42 Grösse), in Columne 5 den wahrscheinlichen Fehler der in Co-
lumne 4 enthaltenen definitiven Position, in Columne 6 die Anzahl der Cataloge, aus
denen die definitiven Positionen hervorgegangen sind und in Columne 7 die Ausdrticke

für die Präcession (unter Anwendung der Bessel’schen Constanten) bis incl. der von

der dritten Potenz der Zeit abhängigen Glieder.

 
