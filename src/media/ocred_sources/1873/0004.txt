 

 

aan » pe

diese Büreauarbeiten schlossen sich als Feldarbeiten an: erstens eine Fortsetzung der
Versuche zur praktischen Durchführung des vom Berichterstatter vorgeschlagenen rein
geodätischen Verfahrens zur Bestimmung der Erdkrümmung und Lothabweichung, und
zweitens die Weiterführung des Präeisionsnivellements auf den Strecken Regensburg —
Schwandorf — Weiden und Regensburg— Neumarkt— Nürnberg. Mit den ersteren Arbeiten
war der Ingenieur-Assistent J. H. Franke und mit den letzteren der Ingenieur-Assistent
A. Rieppel betraut. Ueber diese Arbeiten wird in besonderen Schriften öffentlich
berichtet werden. Die „dritte Mittheilung“ über „das Bayerische Präcisionsnivellement“
ist von der mathematisch-physikalischen Klasse der hiesigen Königl. Akademie der
Wissenschaften bereits zum Drucke begutachtet, und es steht zu hoffen, dass dieselbe
in einigen Monaten in den Händen der Herren Kommissäre der Europäischen Grad-
messung sein wird.

Durch das Nivellement der oben erwähnten Linien kamen folgende Schleifen
zum Abschluss: erstens Rosenheim — Holzkirchen — München — Grafing — Rosenheim in
einer “Gesammtlänge von 138,9 Kilometer mit einem wahrscheinlichen Fehler von
+ 013 Millimeter pro Kilometer; zweitens Weiden— Bayreuth—Bamberg—Niirnberg—
Augsburg—Miinchen—Regensburg— Weiden in einer Gesammtlange von 689,3 Kilometer
mit einem wahrscheinlichen Fehler von + 0,54 Millimeter pro Kilometer. Dieses letzte
erosse Polygon wurde durch die 100.2 Kilometer lange Linie Regensburg —Neumarkt—
Nürnberg getheilt und es ergab sich in Nürnberg eine Schlussdifferenz von 0”,0379
und ein wahrscheinlicher Fehler von +1,72 Millimeter pro Kilometer.

Das Bayerische Präeisionsnivellement, welches zur Zeit eine Länge von 1850,4
Kilometer oder 249,28 geographischen Meilen umfasst, wird in dem laufenden Jahre
gänzlich vollendet werden.

An astronomischen Arbeiten kamen zur Ausführung: die Bestimmung
der Längendifferenzen München —Leipzig und München— Nürnberg, dann die Bestimmung
der geographischen Breiten für Nürnberg, Mittenwald und Holzkirchen. Die Leitung
der Arbeiten, aus denen sich der Längenunterschied zwischen Leipzig und München
ergab, besorgten Herr Direktor Bruhns und der Berichterstatter, während alle übrigen
astronomischen Operationen vom Herrn Direktor v. Lamont ausgingen. Die Ergebnisse
dieser Arbeiten werden ebenfalls in kurzer Zeit veröffentlicht und den Herren Kom-
missären der Europäischen Gradmessung mitgetheilt werden.

Die Triangulation von Bayern anbelangend, so ist dieselbe endlich gegen
den Schluss des Jahres 1873 in dem umfassenden Werke: „Die Bayerische Landes-
vermessung in ihrer wissenschaftlichen Grundlage“, München 1875, so dargestellt worden,
dass sich daraus ein Schluss auf ihre Genauigkeit und folglich auch auf ihre Brauchbar-
keit für die Europäische Gradmessung ziehen lässt. Nachdem die frühere Messung in
mehreren Stücken verbessert und das Hauptdreiecksnetz neuerlich nach einer wissen-
schaftlichen Methode, gegen die sich bei dem dermaligen Stande der Ausgleichungs-

 

|

 
