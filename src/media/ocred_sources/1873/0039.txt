i
ib
à
=
È

A

ern OTT

 

 

In Dieseltneatıon,
) = 81° 820 830 840 850 86° 87°
Bradley . 00} 600! 400] 000} 600! 6000! 0,00
Groombridge !) | 0,77) +068) + 0,61) + 0,55) + 0,50) -- 0,467" -- 0,44
Schwerd i aN 0,00 | 0:60 | 490,00.) 0,00." 0,00! " 0,0017 7000
Greenwich 12 Y. 1840 . | 40,15) +024) 4097| 4028| + 0,29 | + 0,30) + 0,32
Greenwich 12 Y. 1845 . — 0,09 | — 0,03) + 0,01| + 0,05 | + 0,09 | + 0,13) + 0,17
Radcliffe I.2) . 1098| +0,97 | -.0,97| + 0,97) + 0,97 + 0,97) + 0,96
Greenwich 6 Y. /— 0,19} — 013) —0,07) — 004| 0,00, +0,03, + 0,07
Carrington . 000! 0,00) 0900) 000 0,00 0,00 0,00
Greenwich 7 Y. 0,24, —018| — 0,16 — 0,15 | —0,14| — 0,14) — 028
Radeliffe II.) +051| + 057) 4059| + 0,60! + 0,60) + 0,50 | + 0,60
Washington 0.00| 0,001" »0,004 1000.” 000000111060
Greenwich New 7 Y. 024) — 0,18) — 0,16) — 0,151 — 0,14 — 044) 10,18
Berhn | ae 0,00} 0,00! 0,00 | 000 0,00! 0,00! 000
Radeliffe 1862—70 . 0,00; 000) 0,00) 000) 000 000 00
Greenwich 1868-—70 | 9.94] 0181 — 016! —0,15| —0,14| —014| _ 0,13

 

 

 

 

 

1) 4 0" 582 cos (a „ab It).

2) — 0,307 sin (2 — 23° 38’),

3) 1 Ad’ (Astron. Vierteljahrsschrift, Bd. V, pag. 309).

Bezüglich dieser Relationen und zwar zunächst der für die Reetascension gül-
tigen Werthe ist zu erwähnen, dass diejenigen für Groombridge und Struve 1815
den Untersuchungen von Mädler, die der Greenwich Cataloge denen von Auwers
im IV. Bande der astronomischen Vierteljahrsschrift unter Berücksichtigung der Ver-
besserung des Aequinoctiums des 7 Year Cataloges, die des Il. Radeliffe Cataloges
den Untersuchungen im V. Bande der astronomischen Vierteljahrsschrift unter alleiniger
Benutzung des von der Declination abhängigen Gliedes entlehnt sind, während für die
neueren Radeliffe Beobachtungen die Reductionsgrössen durch eine angenäherte Rech-
nung ermittelt wurden. Für die anderen Cataloge sind dieselben zu Null angenommen,
da angenäherte Vergleichungen die Zulässigkeit dieser Annahme rechtfertigten. Die
Correctionen für die Declination konnten fast durchgängig dem Aufsatze von Auwers
in No. 1532 1536 der astronomischen Nachrichten entnommen werden, diejenigen für
Schwerd und Carrington sind nach Argelander zu Null angenommen, ebenso wie
die der anderen Cataloge mit Ausnahme des II. Radcliffe Cataloges, für welchen die
Reductionsgrössen wiederum dem V. Bande der astronomischen Vierteljahrsschrift ent-
lehnt wurden.
Ferner erschien es nothwendig, den einzelnen Positionen entsprechende Gewichte

zu geben; dieselben wurden nach dem Vorgange von Argelander in: „Untersuchungen
über die Eigenbewegungen von 250 Sternen“ im VI. Bande der Bonner Beobachtungen

der Anzahl der einzelnen Beobachtungen entsprechend gewählt, indessen in viel gerin-

 
