 

|
D
a

SR

Terrainhinderniss. Die nördlichen °/, sind vollkommen eben; im südlichen Viertel tritt
eine sanfte Steigung bis zum südlichen Endpunkte ein; auf diesen Endpunkten sind zur
Sichtbarmachung für die Winkelmessung, sowie zu dieser selbst nur niedrige Bauten
erforderlich. Auf den zunächst liegenden Anschlusspunkten: Nördliche Gleiche und
Gieseberg sind Parterre-Pfeiler ausreichend; dasselbe gilt von dem ältern Punkt Hohe-
hagen; auf diesem befindet sich auf der höchsten Kuppe ein Basaltsteinbruch, und liegt
der hierdurch zu Schaden gekommene Stein von Gauss umgestürzt zur Seite; der neue
Punkt kann unweit des älteren genommen werden.

Auf Flinsberg ist der nach Gieseberg gehenden Richtung wegen eine Erhöhung
des Standes um etwa 10 Fuss, auf Weeper wegen der Richtung Hils um etwa
20 Fuss nöthig.

Auf Meissner, Inselsberg, Brocken sind nur Parterre-Stände anzubringen, hin-
gegen befindet sich auf dem alten Punkte Hils niedriger, höchstens 15 Fuss hoher Wald,
bei etwa 12 Fuss Beobachtungshöhe und mit Hülfe Abkappens einer noch etwa hin-
dernden Baumspitze sind alle Richtungen zu erreichen. Auf Hils befindet sich übrigens
ein starker steinerner Pfeiler, der etwa 4 Fuss aus dem Boden herausragt, zur Bezeich-
nung des ältern Punktes; leider ist, dieser Stein nur wenig in den Boden eingelassen,
so dass er trotz seiner Grösse und Schwere wackelt.

Von Flinsberg den Thurm von Struth auf dem Eichsfelde zu erreichen, würde
einer dazwischen liegenden Waldhöhe wegen nur durch einen bedeutenden Hochbau zu
bewirken sein. (Hierzu Tafel IV.) |

Berlin, October 1373. (gez) von Morozowiez.

3. Bericht des Herrn Professor Dr. Sadebeck.

Durch beschränkte Geldmittel und durch Untersuchungen am Comparator zuriick-
gehalten, habe ich im Jahre 1873 erst am 3. Juli zu den Sommer-Arbeiten von Berlin
abgehen können. Begleitet von meinem neuen Assistenten Werner, welcher an die Stelle
des an die Sternwarte von Strassburg berufenen Dr. Schur getreten war, begab ich
mich zuerst nach dem Inselsberge, um einen Heliotropenstand herzustellen, und von da
nach dem Meissner. Bei den früheren von Gerling dort angestellten Winkelmessungen
hatte die Richtung nach dem Brocken nicht beobachtet werden können, indem der auf
demselben befindliche Gaussische Punkt, welcher auf dem ersten Brockenthurme gewesen
war, bei einem Brande des Brockenhauses zerstört worden war. Weil daher in dem
Dreiecke: Brocken, Inselsberg, Meissner der Winkel auf letzterem fehlte, so sollte von
uns die Richtung von dem Meissner nach dem Brocken festgelegt werden. Ausserdem
sollten auch die Richtungen nach einigen anderen hessischen Dreieckspunkten beobachtet
und Polhöhe nebst Azimuth gemessen werden.

Die Vergleichung unserer Messungen mit denen des hessischen Netzes (Gen.-Ber.

v. 1865, S. 51) ergiebt sich aus der nachfolgenden Zusammenstellung.

 

;
i
i
i
3
i
1
j

Ad eus vu

corne ilui Lu Le

 
