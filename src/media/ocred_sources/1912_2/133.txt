ANNEXE B. VII,

RAPPORT

triennal sur les bases (Pöriode de 1909 a 1911.

Dans les trois anndes qui se sont &coulees depuis le dernier rapport triennal sur les
bases (A. G.1., Comptes-rendus de la XVI conference, 2 partie, annexe B. VI], pages 105
a 125), il a &t& mesure, comme dans la periode triennale precedente, un assez grand nombre
de bases.

Nous relevons, en eflet, dans les tableaux annexds au present rapport, & l’actif des
Etats adherents, en y comprenant quelques bases anterieures & 1909 et qui ne sont pas
encore mentionnees dans les actes de l’Assoeciation:

En Allemagne, deux bases, dont une dans le Sud-Ouest africain ;

Au Chili, trois bases, dont une datant de 1903;

Au Danemark, une base;

Aux Üitats-Unis d’Amerique, deux bases;

En France, quatre bases, dont trois en Alg&rie-Tunisie;

Ein Grande-Bretagne, deux bases, dont une dans l’Ouganda;

En Gröce, une base, mesuree en 1889;

Dans les possessions italiennes d’Afrique, plusieurs bases destindes simplement ä des
levös topographiques, deux d’entre elles &tant particulisrement precises ;

Au Japon, deux bases, et une autre antsrieure au rapport de 1909;

Au Mexique, une base;

En Norvege, une base;

En Mozambique (colonie portugaise), deux bases;

Enfin, en Russie, eing bases, dont une sur laquelle nous n’avons encore regu aucun
renseignement;

Soit, au total, vingt-sept bases fondamentales nouvelles.

A l’exception des Etats-Unis, du Chili, de la France, de la Grande-Bretagne et du
Japon, qui ont envoy6 ou publi6 sur ces mesures r&centes des rapports detailles, nous ne

 
