 

307

Tabelle VIII. Französische Messungen.
Gruppe 2. Arbeiten mit neueren Pendelapparaten.

0. Messungen mit Reversionspendeln des Service g&ographique.

(Ref. Stat. Paris.)

Herr Colonel Bourgeois, Direktor des Service g&ographique de l’Armde, teilte uns
brieflich (3 Sept. 1912) die Ergebnisse von 10 relativen Schwerebestimmungen mit, die
der Astronom am Observatoire de Bordeaux, Herr Prof. Dr. Esctangon, von 1909 bis 1911
im Anschluss an Paris ausgeführt hat. Die Ergebnisse von 1909 für 6 Stationen im süd-
lichen Frankreich sind bereits in unsern Ber. 1909, S. 190, nach der daselbst auf 8. 182
angegebenen Quelle aufgenommen worden. Bei den wiederholten Beobachtungen in Floirac
(1909, 10 u. 11) und in Paris (1909 u. 11) stellte sich indes eine zwar regelmässige, aber
ziemlich beträchtliche Aenderung der Schwingungsdauer des benutzten Pendels heraus, die
Herrn Prof. Escovanaon zu einer Neureduktion seiner Messungen von 1909 veranlasste, wo-
nach die früheren Ergebnisse derselben zumteil beträchtliche Aenderungen erfuhren. Nach
der Art der Bearbeitung müssen sämtliche Stationen von 1909-1911 als eine Beobach-
tungsreihe aufgefasst werden; ich habe deshalb die gesamten, nunmehr in definitiver Form
vorliegenden Resultate in den gegenwärtigen Bericht aufgenommen, womit die entsprechenden
Angaben im Ber. 1909, S. 190, ausser Kraft treten. Auf meinem Wunsch hat mir Herr
Prof. Escraneon brieflich (5. Juni 1913) einige Mitteilungen über die Ausführung seiner
Messungen zugehen lassen, wovon ich die wichtigsten hier in Kürze wiedergebe.

Die Messungen wurden mit einem Derrorezs’schen Pendel (pendule reversible et
inversable) von 50cm Schneidenabstand ausgeführt. Das Pendel schwang überall im luft-
verdünnten Raume bei 10 mm innerem Druck, der sich während der Messungen fast absolut
konstant hielt. Zwecks Elimination der täglichen Schwankungen des Uhrganges fanden die
Beobachtungen ohne Unterbrechung von einer Zeitbestimmung (Uhrvergleichung) bis zur
andern statt. Der Gang der Koinzidenzuhr auf den Aussenstationen wurde durch telephonische
Vergleichung mit den Uhren in Floirac (Observatoire de Bordeaux) täglich 2 mal ermittelt,
wovon nur die die Pendelbeobachtungen einschliessenden abendlichen Vergleichungen Verwer-
tung fanden, während die Vergleichungen am Morgen zur Kontrolle des Ganges der Koinzidenzuhr
in der Zwischenzeit dienten. Die Gänge der Vergleichsuhren wurden durch genaue Zeitbe-
stimmungen (m. F. + 0°.02) fortlaufend kontrolliert. Die Zahl der vollständigen
Bestimmungen der Schwingungsdauer (schweres Gewicht unten, oben, kombiniert mit Vorder-
seite, Jückseite des Pendels) betrug in Paris 48 (16 i. J. 1909, 32 i. J. 1911), in Floirae 72
(je 24 i. d. d. 1909, 1910 u. 1911), in Areachon 24 (8 i. J. 1909, 16 i. J. 1910), in

 
