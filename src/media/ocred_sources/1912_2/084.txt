 

 

76

worden. Mit den Widersprüchen zwischen der neuen österreichischen Vermessung in Salzburg
und den südbayerischen Dreiecken beschäftigt sich eine Abhandlung von M. Scamipr in den
Sitzungsberichten der Münchener Akademie 1912. Die Unterschiede von 70 -Einheiten in
der 7. Stelle des Logarithmus sind zwar durch die Entfernung von der Josephstädter Grund-
linie erklärlich, auf der die österreichischen Rechnungen beruhen, aber 2 in nahe entgegen-
gesetzten Azimuten vom Punkte Watzmann ausgehende Seiten erhalten gerade die grösste
und kleinste Verbesserung, so dass an eine Verrückung der Festlegung oder an Einflüsse
der Lateral-Refraktion gedacht werden kann, während ein Zentrierungsfehler als ausgeschlossen
gelten darf. Bei Bearbeitung der Längengradmessung in 48 Grad Breite wird in Bezug auf
diese Anschlussdifferenzen Aufklärung: geschaffen werden müssen.

In Frankreich ist die Triangulation des Meridians von Lyon in derselben Weise wie
bisher bis zum Mittelmeer fortgesetzt worden. Bei Gelegenheit der Aufnahme für die Karte
im Massstabe 1/50000 in den Gebirgsgegenden der Seealpen, des Jura’s und des Oisans-
Massivs wurde eine Verbindungskette rekognosziert, welche den von der Internationalen
Erdmessung gewünschten Anschluss an Italien und die Schweiz herzustellen geeignet ist.

In Italien ist die Erneuerung der Triangulation im Süden des Parallels von Rom
fortgesetzt worden, 52 Dreiecke mit 33 Stationen sind vollendet. Ferner wurden die Arbeiten
zur Verbindung der Insel Stromboli mit dem Kontinent gefördert. Das hydrographische
Institut hat in der Kolonie Erythräa am Roten Meere und an der Benadir-Küste Triangu-
lationen ausgeführt.

In der Schweiz sind 13 Dreiecke I. Ordnung im Nordosten nachgemessen worden, der
mittlere Dreiecksschlussfehler wurde bei Seitenlängen zwischen 23 und 75 km zu + I”
gefunden.

In Preussen ist die Verbindungskette zwischen der Berliner und Schubiner Grundlinie
fertig gemessen worden; der mittlere Fehler aus den Dreiecksschlussfehlern dieser Kette ist
noch nicht vollendet. Es wird beabsichtigt, auch die Netze in Schlesien, der Mark Branden-
burg und der Provinz Posen neu zu messen und Südwestafrika und alle deutschen Kolonien
mit einer Triangulation zu versehen.

In Russland ist die Messung eines Meridianbogens von Petersburg bis zum Schwarzen
Meere begonnen worden, der durch Parallelbögen mit der alten russisch-skandinavischen Breiten-
gradmessung verbunden werden soll. Bisher sind 13 Dreiecke zwischen 59° 40° und 58° 0’
Breite in den Gouvernements von St. Petersburg, Novgorod und Pskow gemessen, wobei
hohe Signalbauten nötig wurden; der mittlere Dreiecksschlussfehler betrug + 0,76. Ferner
wurden Dreiecksmessungen 1. Ordnung im Kaukasus zur Ergänzung der früheren Arbeiten
unternommen, die 25 neue Punkte umfassen. Eine Kette, die sich von Omsk in südwest-
licher Richtung längs des Irtych-Flusses erstreckt, und die 47 Dreiecke enthält, wurde
rekognosziert. Die Vermessung in Transbaikalien wurde von der Station Mandchurei längs
der Eisenbahn nach dem Baikalsee hin fortgesetzt. Der mittlere Dreiecksschlussfehler betrug
bei dieser Messung + 1”,00. Sodann ist eine Triangulation im äussersten Osten in der
Gegend des Oussouri zwischen Vladivostock und Khabarovsk im Gange.

Die von der Internationalen Erdmessung angeregte Verbindung von Turkestan mit

 
