159

o. Dase de Mwon 1911.

Cette base est la plus recente, pour la France. Elle a &t& mesuree & la rögle invar
aller et retour et avec deux fils invar @galement aller et. retour. Toutes les operations ont
ete particulierement soignees.

La mesure ä la rögle a dur& 13 seances pour l’aller et 12 s6ances pour le retour.
Sauf au debut, la vitesse atteinte a &t& de 33 & 40 portees ä l’heure, soit de 140 & 160 m;
et par jour, de 700 & 800 m. On est arriv& une fois ä faire 42 portees & l’heure et, & deux
reprises differentes, on a mesur& 816 m dans une m&me seance. On sait que cette grande
rapidite est düe & l’application de la methode des coincidences, qui supprime toute lecture
de niveau ou de tambour.

La base, longue de 8482 m, a &t& segmentee en 12 sections, qui donnent les &carts
suivants (regle invar):

 

 

 

 

 

mm

section nd. 1 longueur 368 m ecart aller et retour — 2,59
nee 1182 m 0,42
mr 784m — 2,08
md: 688m — 11,18

Dr 432 m N
mio 672m — 0,85
ne 7 i20 m 3]
DS 736m — 0,83
ng 816m — 1,52
DO 672 m — 0,41
no 864 m +0,05
nl 546 m : ee
Bagetoialer 0: .... 82489 m — 11,60

1.9 59 0 49 2 08
Ma E x = ll Ka
aa m | a
24.38
ae — x
en 2.03

“ a 1
er Vor a anmmnn

L’erreur sur la base totale est:

E„= 1,01V 8,48 — + 2mm,94

qui correspond & une erreur probable E,—= + 1mm,96.

 

 
