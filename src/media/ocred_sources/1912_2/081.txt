TEE ET ETTEUTTT UT TENT U UETHUTn

TREE

 

73

2. INTERPOLIERBARKEIT NACH GEOGRAPHISCHER LÄNGE.

Soll über die blosse Fortsetzung dieser „Polbahn’ hinaus ein Fortschritt geschehen,
so ist es am aussichtsreichsten, da anzusetzen, wo bisher die grössten Abweichungen auf-
getreten sind. Die Beobachtungen in T'schardjui und Gaithersburg verraten das Bestehen
besonderer Erscheinungen in stärkerem Masse. Da T'schardjui isoliert liegt, in der Nähe von
Gaithersburg aber bereits 3 Stationen: Oincinnati, Philadelphia und Naval Observatory liegen,
auf denen laufende Polhöhenbeobachtungen gemacht werden, so ist esam einfachsten, mehrere
Jahre eine oder mehrere Stationen einzuschalten, deren Lage je nach Ausfall in zweck-
mässiger Weise zu variieren wäre. Hieraus würde hervorgehen, wie diese Erscheinungen in
bezug auf geographische Länge variieren. (2. Vorschlag).

3. INTERPOLIERBARKEIT IN BEZUG AUF ZEIT.

Es sind sichere Merkmale vorhanden dafür, dass die Polhöhenmessung von kurz-
periodischen Erscheinungen beeinflusst wird; hiervon werden die #y2 wesentlich betroffen.
Es erscheint mir nötig, eine oder mehrere Beobacktungsreihen zu veranlassen, die sich nach
Möglichkeit über alle Tagesstunden erstrecken, so dass eine Verquickung von Rektascension,
Tageszeit, Jahreszeit, Sonnenlänge künftig wegfällt. (3. Vorschlag). Dass es möglich ist,
derartige Reihen mit Erfolg durchzuführen, erkennt man:

1) daraus, dass es möglich war, aus der klassischen Aberrationsreihe Srruve’s aus dem
Jahre 1841/42 eine Tageskurvenschar zu konstruieren; siehe Ergänzungsheft Nr. 11 der
Astronomischen Nachrichten, 1906. Aus dieser Kurvenschar liessen sich Schlussfehler und andere
Widersprüche ableiten, wie sie in den Beobachtungen zur Breitenvariation aufgetreten sind;

2) zu einem Teile schon aus der bekannten, von den Herren Kımuraı und NARANo
ausgeführten 4-Gruppenreihe.

4. INTERPOLIERBARKEIT IN BEZUG AUF BREITE,

Die sveben erwähnte Srruvssche Reihe ist in Pulkowa angestellt worden; da Pulkowa
die Polhöhe 60° hat, so ist zu erwarten, dass man in höherer Breite wegen der längeren
Nacht noch mehr Erfolg haben werde. Eine Station in höherer geographischer Breite ist
auch deshalb wichtig, weil man dadurch dem Pole, dessen Bahn bestimmt werden soll,
näher kommt; zur Zeit wird vom Parallel -- 39° 8 auf den Pol extrapoliert, auch die
Beobachtungen vorher erstreckten sich nur auf einen Streifen zwischen @ = + 60° bis
P9=+ 30°, bestenfalls bis ® = — 32°. (4. Vorschlag).

Von diesen 4 Vorschlägen ist der dritte der wichtigste nach meiner Meinung.

5. GESICHTSPUNKTE FÜR DIE BEARBEITUNG DER VORGESCHLAGENEN BHoBACHTUNGEN.

Zur Aufklärung der aus den Beobachtungen zur Breitenvariation folgenden Erschei-
nungen sind mathematisch-physikalische Deduktionen erwünscht; sie würden ermöglichen:
