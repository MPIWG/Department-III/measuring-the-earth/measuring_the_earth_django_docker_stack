 

 

267

Für die Dichtekonstanten fand Prof. Hassmmann 1911 folgende Werte in
10-7 sek Sternzeit:

Pendel 76 696,6 + 8,8
TUT 693,12 5,2
TB 705,5 + 9,4
Bd 680,7 # 7.0

Auch für diese Konstanten liegen ältere Werte vor (Ber. 1909, S. 84), die jedoch
auf einem andern Stativ (Dreipendelstativ) erhalten wurden, und deshalb mit den vorstehenden
Ergebnissen nicht streng vergleichbar sind. Angewendet wurde das einfache Mittel aus den
vorstehenden Angaben.

Gruppe 5. Messungen der Königl. bayerischen Gradmessungs-Kommission.
(Ref. Stat. München).

Nach einer brieflichen Mitteilung des Observators der Kommission, Herrn Dr. Zapr,
vom 22. April 1912 sind in Bayern in dem Zeitraum von 1909—1912 relative Schwere-
messungen auf 31 über das ganze Land verteilten Stationen durch Dr. Zapp ausgeführt
worden. Die Ergebnisse dieser Arbeiten, zu denen noch eine neue Bestimmung des Schwere-
unterschiedes München— Potsdam durch Dr. Zarp aus dem Jahre 1909 hinzukommt (Ber.
1909, S, 86), liegen zur Zeit noch nicht vor.

Inzwischen sind zwei ältere Beobachtungsreihen aus den Jahren 1902 und 1907,
auf die wir in unsern g-Berichten (1903, 8. 164; 1906, 8. 180; 1909, 8. 86) bereits hin-
gewiesen haben, unter dem Titel: Astronomisch-geodätische Arbeiten, Heft 7; Relative Schwere-
Messungen in Bayern in den Jahren 1902—1907, 2. Reihe 1902 und 3. Reihe 1907; München
1912, veröffentlicht und ihre definitiven Ergebnisse in den vorliegenden Bericht aufgenommen
worden.

Die Reihe von 1902 (13 Aussenstationen) wurde von dem früheren Observator der
Münchener Sternwarte, Herrn Prof. Dr. Auprsg, die von 1907 (15 Aussenstationen) von dem
Observator Herrn Prof. Dr. Grossmann bearbeitet. Apparate und Beobachtungsmethode
waren im wesentlichen dieselben wie in den Jahren 1896—1900 (Ber. 1906, 8. 177 u. £.).
In beiden Fällen kamen die Sterneck-Schneiper’schen Pendel N°. 89, 90 und 91 aus-
schliesslich auf dem Wandstativ in Anwendung. Die Stabilität des Wandstativs wurde
auf allen Stationen mit einem Stossdynamometer geprüft, mit dem im Sekunden- oder
Doppelsekundentakt je 10 Stösse von grosser Intensität sowohl auf die Konsole als auch
auf benachbarte Stellen der Wand ausgeübt wurden. Die dadurch erzeugten Pendelausschläge
waren so gering, dass von einer Berücksichtigung des Mitschwingens meist abgesehen
werden konnte; nur auf 4 Stationen der Reihe 1902 erreichte dasselbe den Betrag von
0.001 cm sek? !),

 

 

1) Den von mir im Ber. 1906, S. 178, erhobenen Einwand, dass zwischen der Konsole und der das Pendellager
tragenden Deckplatte möglicherweise ein relatives Mitschwingen von veränderlichem Betrage stattfinden könnte, sucht
Herr Anpıne durch die folgenden Ausführungen (l.c. p. 3) zu entkräften:

 
