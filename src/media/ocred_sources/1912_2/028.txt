 

 

20

Il faut encore mentionner qu’en 1910, pendant l’ete, le „Kgl. Preussische Landes-
aufnahme” a mesur6 avec l’appareil de Brsser et avec plusieurs fils d’invar la base auxi-
liaire de 240 mötres de 1’Institut g&odesigue. Cette base avait 6t6 mesurde aussi & trois
reprises par l’Institut au moyen de l’appareil Brunner et de plusieurs fils d’invar.

En outre il faut signaler que, pendant l’annse 1910, un comparateur pour la mesure
des regles de 4 mötres a 6t& installe & ’Institut par M. O0. Torprer et fils & Potsdam.
Au moyen d’un appareil auxiliaire il peut servir aussi & la comparaison de regles de d metres.

Enfin je dois communiquer qu’au moyen de l’appareil pour examiner les divisions
eirculaires on a examin& deux cereles göodesiques, l’un de la fabrique de M. G. Hryos
ä Dresde et l’autre de la fabrique de M. M. Hıwpınrann & Fribourg en Saxe. M. le Dr.
Förster publiera sous peu les rösultats de ces recherches.

B. Gestion administrative.
15

Le fonds des dotations a 6t& g6rö& comme d’habitude. En nous röservant le
depöt conventionel des comptes exacts et des depenses, rous donnons ci-dessous un apergu
du mouvement des fonds pendant l’annde 1911.

Recettes.

Bolde nett der fonda a lasfinide 19102270... 022 .2...M 568731,67
Contributions pour les anndes preeednts . 2 222000, 799,60
Contribubions pour: lannes Ol m! 2: wa u ei, 2063 055,15
Vente des publications. . . ss 182,93

Interdts: du „Kur- und Ne ee Bubtenschtaftliehe, Danlehins-
kasser a Berlin... 5 550,10

B du „Königliche Beehshäinng Beönseische Biantsbanle
älberlines aa u Se unuonleng en 737,10

Total:  M. 129056,55

Depenses.
Indemnite du secrdtaire perpötuel.. . . . =... M2.9000,00
Pour le service des latitudes (au nord de efunfenm) Na 62 444,14

ee 5 5 (au sudde lequateun? 2. 6 533,95
Pour les caleuls relatifs aux döterminations de la pesanteur, des

deviations de la verticale, et de la figure de la terre. . „ 2 692,00
Pour la station de Pribram . . . 3, 980,23
Frais d’impression (Öomptes rendus, Vol. ir 11 etIIT, reulsines ee, ) 5 15 588,80
Frais de transport, port de lettres, frais d’expediion . . . ,„ 22127408

Total: M. 95366,20

Par consequent & la fin de 1911 le solde actif ötait . . . . M. 33 690,35

 
