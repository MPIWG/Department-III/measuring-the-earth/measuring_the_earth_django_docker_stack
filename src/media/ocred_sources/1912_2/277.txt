 

|

&
Ui

 

 

ANNEXE B. VIl®,

RESOLUTION

concernant les nivellements de pr6cision, adopt6e
dans la s6eance du 25 Septembre 1912 de la 17° me Conference gönerale
de P’Association g&odesique internationale.

La 17°me Conference generale de l’Association g&odesique internationale,

Vu les progrös importants realises dans l’art des nivellements, depuis l’annde 1867,
ol, pour la premiere fois, des limites ont &t& fixdes aux erreurs admissihles dans les nivelle-
ments de preecision;

Vu lintert qu’au point de vue des besoins sup6rieurs de la g&odesie, il y aurait
a creer une nouvelle categorie de nivellements, plus preeis, c’est-A-dire avec des tolerances
plus etroites et dont les erreurs, accidentelle et syst6matique, seraient caleuldes d’apres des
regles uniformes;

Tout en maintenant sans modifieation les toldrances de 1867 pour les nivelle-
ments de pre&cision;

Decide de ranger desormais dans une nouvelle catögorie dite des nivellements
de-haute pr&cision, toute ligne, tout groupe de lignes, ou reseau, nivelös deux fois,
en sens opposes, & des dates autant que possibles differentes, et dont les erreurs, accidentelle
et systematique, uniform&ment caleuldes au moyen des formules ci-apres ne depasseront pas:

+ Imm par km pour l’erreur accidentelle probable,

 

ou + Imnm,5 or id. id. moyenne,
+ Omm,2 id. id.  systematique probable,
ou + 0mm,3 id. id. id. moyenne,

Si l’on designe par

L, la longueur d’une ligne isol&e, ou d’un cöt6 de maille polygonale dans le cas
d’un reseau;
ZL, le developpement total du groupe de lignes ou du reseau consideres;
A, la discordance des resultats des deux nivellements, relev6e entre deux reperes
consecutifs ;
