 

 

 

 

|
|
|

Mais, les secondes formules &tant plus simples, il est doublement preförable de les
employer, du moins apres les avoir complötees pour en expurger l’influence des erreurs
systematiques.

Voyons comment on peut &valuer ces derniöres et en tenir compte dans le calcul
de l’erreur accidentelle probable.

0. Evaluation directe des erreurs systömatiques,

Le coeffieient d’erreur systematique d’une ligne de nivellement se deduit de la
quantit6 par kilometre dont s’aceroit, en moyenne, la discordance progressive relev&e entre
les resultats des deux op6rations executees, de pröfsrence en sens inverses, sur la ligne
consideree.

S, mesurant la partie systömatique de la discordance totale relevee entre les extre-
mites de cette ligne, de longueur L, l’erreur systematique probable kilometrique ox de la
moyenne des deux nivellements aura pour expression !).

I 8

4), . . . . Ku 5

Pour un ensemble de lignes, la moyenne quadratique correspondante so, s’obtiendra
en appliquant prealablement au carr& de l’erreur syst6matique ox, relative A chacune des
lignes, un poids proportionnel ä la longueur L de cette ligne.

On aura des lors:

1

X 82
5) . . . . o a: == 9SL S a

Ainsi, dans le reseau fondamental du Nivellement &önsral de la France pour lequel:

on
5 1, — 1330,
on a:
%, — Omm,117.
D. 3° phase. — Oaleul de Perreur aceidentelle probable, expurge6e

de linfluence des erreurs systematiques.

Le coeflieient probable, x ou c, suivant le cas, de l’erreur systematique ayant 6t&
determine comme on vient de le voir, il est maintenant facile de completer les formules
(Ibis) et (2bis) pour avoir le coeflicient, y’« ou „'r, de l’erreur accidentelle, expurge de
l’influence des erreurs syst&matiques.

1) Pour la demonstration de cette formule, voir „Note sur le röle des erreurs systematiques dans les nivellements”
©. R. Confee de Berlin, 1895, Op. eit. $ IT et „Nivellement de haute pr£eision’ op. eit. Supplements, Note II, $ 10%,

»

 
