 

222

Europe.

1/. Allemagne-Prusse (Institut geodesique).

 

Reperes de contröle
Noms des

 

Noms localites ou sont 3
eg Altitudes
situes des e ee

des mers ; Emplacement Zabhol tees au
maregraphes zero normal
| prussien
!
| | | m
Mer du Nord | Bremerhaven , Ursprünglich am Leucht- (5,931)
turm, später an der kleinen 4.700
| Kaiserschleuse : ü
Travemünde Am Lotsenamt 4,518
Marienleuchte Am Maschinenhause 1,124
Wismar Am Baumhause 3,658
Warnemünde An der Vogtei 4,335
Arkona Am Pegelhause 1.976
Swinemünde Gebäude auf dem Bauhofe 2,123
|
Stolpmünde Am Lotsenhause 4,428
' Pillau Am Leuchtturme 2,855
Memel ‚ An den Kaimauer 1,967

lg. Allemagne-Prusse (Landesaufnahme).

 

 

Altitudes orthometriques
rapportces au

 

Noms des RR ; nero nor Ange
‘siong sro normal prussien
Noms focalites otı sont Dun Emplacement des _- en nn
| situes les des ie ee | Niveau Observations
des mers £ ls appareils Reperes | Zeros des | i
appareils apparelis N i moyen de
de contröle appareils BE)
1 2 3 t | 5 | 6 | Bi 8
| & N er nı m Su
Baltique | Stolpmünde | Echelle de Beim Nerzre’schen -} 0,853 - 0,099 l) Niveau moyen
|  marcee Grundstücke “ calcule par Institut
Kolbergermünde id. 3eim Hafenbureau - 0,991 — 1,609 — 0,119 en ie ae
: i : : ; ; = o et deduit d’observa-
Sw : a 1. — 1 —. 0,023 : RAR
Swinemtinde id Im Bauhafen 2 1,074 N) 23 | tions mardmötriques
Wieck id. Im Hafen -+ 1,582 — 1,418 oa nenne a dor,
Warnemünde id. Bei der Vogtei -+ 0,730 — 15210 — 0.139
Marienleuchte id. An der Ländungs- — — —
brücke
Kiel Maregraphe An der Kaiser- + 2,721 — 0,279 0,286
, eleetrique lichen Werft
Flensburg Echelle de An der Landungs- — 0,298 — 2.298 —

|  maree brücke

 

ass
