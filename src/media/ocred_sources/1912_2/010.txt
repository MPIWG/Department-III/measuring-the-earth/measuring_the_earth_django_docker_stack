TEE ST

Es möge hier noch auf die Bestimmung des Geoids im Harzgebiete hingewiesen
werden, welche seit längerer Zeit in den Arbeitsplan des Kgl. Preußischen Geodätischen
Instituts aufgenommen ist und sich gegenwärtig ihrem Abschluß nähert. Diese Arbeit
steht unter Leitung von Herrn Prof. Dr. A. Garzz, und es wird dabei die Krümmung
der Lotlinien nach dem von mir angegebenen Verfahren berücksichtigt.

8
Der Internationale Breitendienst.

Der Internationale Breitendienst auf dem Nordparallel in + 39° 8’ Breite hat
auch im Jahre 1911 ohne Unterbrechung funktioniert.

Im ganzen sind im Laufe des Berichtsjahres

in Mizusawa 2157 Sternpaare
„ Tschardjui 1841 4
„ Carloforte 2425 :
„ Gaithersburg 1510 5
„ Cineinnati 1141 ie
„ Ukiah 1200 5

beobachtet worden.

Als Beobachter waren während des Jahres 1911 die Herren tätig:

in Mizusawa: Prof. Dr. H. Kınura und Dr. M. Haskmoro;

„ Ischardjui: . Oberstleutnant Krenwarow;
„ Earlotorte: Dr. G. A. Favaro und vom Mai ab Dr. G. Bemrorap;
„ Gaithersburg: Dr. Fran« E. Ross und Dr. Warrer N. Ross;

„ Cineinnati: Prof. Dr. J. G. Porser und Dr. ER. J. Yowenn;

„ Ukiah: Dr. Jaues D. Manokıtr.

Die laufende Reduktion der Beobachtungen wurde gleichwie in den Vorjahren
unmittelbar nach Eingang der Original-Beobachtungsbücher von dem Obseryator im
Geodätischen Institut: Herrn Prof. Wanach, unter Mithilfe der Rechner: Lehrer
A. Wısanows&1, OÖ. ScHönreLp und Frau Hesse ausgeführt.

Im Jahre 1911 hat sich auch zum ersten Male seit Beginn des Internationalen
Breitendienstes der Fall ereignet, daß bei Gelegenheit der Übersendung der Beobachtungs-
bücher von den Stationen nach Potsdam ein solches Buch (das Septemberbuch von
Gaithersburg) auf dem Posttransport verloren gegangen ist. Da aber verabredungsgemäß
Abschriften der Beobachtungsbücher auf den Stationen zurückbehalten werden, ist aus
diesem Vorkommnis kein größerer Schaden erwachsen.

Das Sternprogramm hat am Beginn des Jahres 1912 aus Anlaß der Veränderungen,
welche die Präzession in den Deklinationen der Sterne bedingt, wiederum eine teilweise
Umgestaltung erfahren müssen. Indes brauchten von den 96 Sternpaaren nur 16 durch

 
