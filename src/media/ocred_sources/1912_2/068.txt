 

m
retrograden Umlauf des $% so nahe zusammen, als die Beobachtungsgenauigkeit zulässt.
Es besteht die Gleichung für mittlere Tage:

l 1 > 1 :
6798, 3355 = S%) T 3931,16 = p) 2100, 3816’
beobachtet sind 2184, 2187 und 2185 + 7 mittlere Tage.
2. Die aus den genannten Beobachtungen folgende Dauer der OHanpuerschen (14'/,
Monats-) Periode fällt mit der aus der Formel:
1 1 1 1 366, 2422
305, 2422 — 6816,9487 E Su) | 3240, 3187 Cp) ! OimsPeriode * 365, 2428

folgenden Dauer, nämlich 438, 0961 mittlere Tage, sehr nahe zusammen; diese Formel folgt
auf zwei verschiedenen Wegen aus gewissen Interferenzbetrachtungen. Hiernach wäre die
Cuannuersche Periode eine a priori gegebene Konstante des Systems Sonne-Erde-Mond.

3. Die Reihe von 22 Jahresmitteln des Radiusvektors der „Polbahn” oder V =? -+ y?
zeigt drei übereinandergelagerte Perioden mit den Dauern:

19.46 9.40 6.36 Jahre;

 

 

gegen die bekannten Perioden von
18.6 8.85 6.0 Jahren

ergibt sich hieraus eine durchschnittliche Verlängerung von '/,,; die genannte Reihe von
Jahresmitteln lässt sich durch die um '/,, verkleinerten Argumentdifferenzen $-$d, und p-p,
darstellen mit einem mittleren Fehler eines Jahresmittels von + 0".023, wo Sb, und p, für die
Epoche 1900.0 gelten. Die grösste der 22 Abweichungen bleibt unter 0”.05. Die Formel ist:

+ 0”.161 + 0”.040..sin $d + 0".004.cos SV
+ 0",072. sin ($% — pP) + 0.030 . cos (SB —p')
— 0",001.. sin p’ + 0".023. cos p';

Sb und p’ sind abhängig von S%,, 7, und den verkleinerten Argumentdifferenzen.
Der mittlere Fehler der Konstanten ist + 0.005, eines der sechs Koeffizienten + 0.007;
die Darstellung lässt sich noch bessern. Die übrigbleibenden Fehler sind:

890%, , "or 1900.45 07.00
id . 004 5 00
a, 2,0 56 um
Be: 00 nn
4.45 0.00 4.45: >°.20%08
5.45 0.00 455
645 6.4 1 on
7.45 0.00 ee
8.45 0.00 a)
9.45 0.01 3. 00

0 2
1845: 220109,

 

u a

 
