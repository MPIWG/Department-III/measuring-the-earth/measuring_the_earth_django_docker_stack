 

liegenden Teile des Geoids dienen, indem in Bezug auf sie in bekannter Weise durch
astronomisch-geodätische Messungen mittelst Breite und Azimut oder Länge relative Lot-
abweichungen bestimmt werden. ;

Um nun ein einheitliches System von Lotabweichungen für ein und dasselbe Ellipsoid
aufstellen zu können, ist es erforderlich, nicht nur die Zahlenwerte der Lotabweichungen
für ein beliebiges Ellipsoid abzuleiten, sondern auch noch die Differentialausdrücke für
Änderungen der Elemente des Ellipsoids. Ist z.B. Greenwich (0) Ausgang und Warschau (19)
ein darauf bezogener LapuAackscher Punkt, so hat man nach S. 193 der genannten Längen-
gradmessung:

&,—= + 77,29 + 0,9590&, — 0,1775, — 4111 42 _ 4533da
A = — 8,47 — 0,4622 &, + 0,9774 2, + 75904 ©* + 46461 da,

; : : = : da x
warin & u. A Lotabw. in Breite u. Länge bezeichnen, sowie — u. dz Änderungen der

Bzsseuschen Elemente a und Abplattung «.

Hat man nun in der Gegend von Warschau einen Punkt, den wir Nr. 20 nennen
wollen, durch ähnliche Ausdrücke auf Nr. 19 bezogen, so kann man dann leicht auch Nr. 20
auf den Ausgang Nr. 0 beziehen, indem man die Ausdrücke &,, u. A,, in die Ausdrücke
fun oy, U), einseuzu

Um dem Zentralbureau die Zusammenfassung der Ergebnisse der Landesarbeiten zu
erleichtern, ist es erwünscht, dass die in den einzelnen Ländern abgeleiteten Lotabweichungen
durch 5 gliedrige Ausdrücke der angegebenen Art ausgedrückt werden unter Angabe der
Elemente des benutzten Ellipsoids.

Sehr förderlich würde es auch sein, wenn die einzelnen Länder die in ihr Bereich
fallenden Larvaozschen Gleichungen aufstellen und ausgleichen wollten.

Nur durch Zusammenwirken der Landesorgane mit dem. Zentralbureau kann diesem
seine Aufgabe so erleichtert werden, dass man in Europa zu einem ähnlichen umfassenden
Ergebnis gelangt wie die Coast and Geodetic Survey es für die Vereinigten Staaten von
Amerika erreicht hat.

 

i
i
i
i
j

 
