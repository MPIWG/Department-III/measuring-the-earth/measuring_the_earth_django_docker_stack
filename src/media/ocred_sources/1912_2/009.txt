 

4
Erledigung dieser Linien fehlten allerdings noch zum Teil die astronomischen Angaben für
Strazsahalom und Sz&chenyi hegy. Da auch weiter nach Westen hin das Material noch
unvollständig war, indem in Bayern im Gebiet der Längengradmessung zwei neue
astronomische Stationen eingefügt werden sollen, und da zugleich Herr Geheimrat
Börscn erkrankte und am 1. Juli in den Ruhestand trat, so sind vorläufig die
Berechnungsarbeiten für die Längengradmessung abgebrochen worden.

Es ist sehr bedauerlich, daß das Zentralbureau der I. E. die höchst schätzbare
Kraft des Herrn Börsch verloren hat. Als genauer Kenner der bezüglichen mathe-
matischen Methoden und der einschlägigen Literatur, sowie im Besitze guter Sprach-
kenntnisse, hat er der Internationalen Erdmessung durch mehr als drei Dezennien
hindurch die besten Dienste geleistet.

Über die zum größten Teile von ihm bearbeiteten geodätischen Linien gibt
die dem Bericht angehängte Tafel Aufschluß. Dieses Liniensystem wird noch in sich
soweit nötig verarbeitet werden und die Grundlage für eine einheitliche Darstellung
der in Europa bestimmten Lotabweichungen gegen einen Zentralpunkt bilden. Herr
Prof. Dr. Krüser wird diese Rechenarbeiten leiten, welche von den Herren Dr. Försrer
und Dr. Borrz bewirkt werden sollen. Einige Vorstudien sind schon erfolst.

2.
Untersuchung der Krümmung des Geoids in den Meridianen und Parallelen.

Für die beiden westlichen Stationen der Europäischen Längengradmessung in
52° Br. wurde die isostatische Reduktion des Lotes durch den Kgl. Landmesser
Herrn Hıroxer berechnet. Dieselbe beträgt im Sinne einer Störung des Zenits in der
Richtung nach Osten für

Feaghmain zz.
Killorglin —21;

davon sind etwa 1.5 auf die allgemeine kontinentale Erhebung zu rechnen. Die
Prüfung dieser Zahlenwerte steht noch aus.

Durch Herrn E. Gurermans wurde auch die Wirkung der in Zentraleuropa in
dem Viereck zwischen 7° und 21° Länge und 43° bis 56° Breite zufolge der Schwere-
bestimmungen liegenden Störungsmassen auf Feaghmain und Greenwich ermittelt. Die
östliche Lotstörung beträgt bezw. — 0:21 und —0'69. Sie ist zu der isostatischen
Störung hinzuzufügen. Auch diese Zahlenwerte sind noch zu prüfen sowie durch
Benutzung weiterer Schwerewerte außerhalb des betrachteten Gebiets zu ergänzen.
Nach der letzten Ausgleichung der Längengradmessung durch Herrn Prof. Dr. Scaumans
(Verhandlungen 1909, I, S. 167) ist die östliche Lotstörung in Feaghmain — 3'6, in
Killorglin + 2:4. Erstere Zahl paßt gut zur isostatischen Rechnung, letztere nicht.
Dies bedarf noch der Untersuchung.

 

 
