 

230

ASIE.
18. Empire DES InDes.

Depuis la Conference de 1909, les rösultats des nivellements de preeision ex&cut6s
dans l’Inde, de 1858 & 1909, ont &te publies dans „G. T. Survey”’ (volumes XIX, XIXa et XIX 2).

Le volume XIX contient l’historigue du travail, la deseription des procedes et la
discussion des r&sultats obtenus. Les volumes XIXa et XIX2, relatifs, le premier & la partie
septentrionale et, le second, ä la partie. m&ridionale de l’Inde, contiennent la deseription
des emplacements et les altitudes des rep£res.

AMERIQUE DU NORD.

21. Erars-Unis.

Preeise Leveling in the United States 1903—07.

AMERIQUE DU SUD.
22. Mexiqun.

Nivelacion. de preeision de la lines Mexico-Irolo-Buchure.

25. RAPUBLIVUE ARGENTINE.

Memoria sobre nivelaciön de precisiön presentada al XIe Congreso internacional de
navegaciön (Sesiön de San Petersburgo; mayo de 1908). — Buenos Aires, 1903. — Ministerio
de obras püblicas; direceiön general de obras hidräulicas.

VII. Calcul des erreurs et definition des nivellements de pr&cision.

Dans sa seance du 7 octobre 1867, la deuxiöme Conference de l’Association geode-
sique de l’Europe centrale a deeid& qu’on doit classer comme nivellement de preeision tout
nivellement oü „Zerreur probable de la difference de niveau de deux points distanis d’un kilo-
metre ne depasse pas 3 millimetres en moyenne et 5 millimetres au maximum”.

Mais ä& la suite des ameliorations realisdes, depuis un demi-siöcle, dans la construction
des instruments et dans les möthodes d’observation, la precision des nivellements s’est no-
tablement acerue. Aussi, dans le questionnaire habituel joint & ma cireulaire d’avril 1912,
adressee aux delögues competents des divers Nitats assoei6s, ai-je eru devoir provoquer leurs
observations au sujet d’une nouvelle definition &ventuelle des nivellements de preeision, qui
reduirait, par exemple, au tiers de leur valeur, les limites d’erreurs fixdes en 1867.

TOTEN TUE

 
