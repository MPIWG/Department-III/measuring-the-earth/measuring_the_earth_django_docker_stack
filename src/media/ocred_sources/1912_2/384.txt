 

or

existe entre la deviation effective de la verticale, sous l’action de la Lune ou du Soleil, et
la deviation que l’on observerait si Ja Terre &tait d’une absolue rigidite.

D’autre part, comme Euler l’a montre, les pöles terrestres se deplacent.l&gerement
& la surface du globe. Si ce dernier &tait rigoureusement indeformable, le mouvement dont
il s’agit aurait une periode r, de 305 jours; mais Nrzwcomp a fait voir que la periode
effective r, doit &tre plus longue si le globe est Elastique.

Supposant, comme je l’ai fait moi-möme, le globe simplement forme& de couches
concentriques de möme densit6, A. E. Love!) a d&montr& qu’entre les divers el&ments du
probleme, il existe necessairement la relation suivante:

aw@”

 

dans laquelle
r designe la periode effective r, du mouvement eul£rien ;
&, l’aplatissement effectif &, du globe;
a, le rayon &quatorial;
w, la vitesse angulaire de rotation de la Terre sur elle-m&me;
9, Yacceleration de la gravite ä l’&quateur;

a+9=-, =...068

le rapport entre les amplitudes de la maree effective du geoide et de la mar6e correspondante,
caleulee dans l’hypothöse de la rigidit@ absolue du globe

4@

et a

1
Be
ne
Vaplatissement theorique dü & la force centrifuge.
Larmor ?), en outre, a fait voir que la relation (7), comme celle de ÖrAıRaur pour

la pesanteur, est ind&pendante de la loi de variation de la densite avec la profondeur.
Substituant mes propres notations & celles de Lovs, j’ai pu Eerire ainsi sa formule:

ee)

Mais, des relations (1), (2), (8) et (5), on peut tirer aussi:

ee =}
& & &,

1) The yielding of the Barth to disturbing Forces, n°. 9, formule (15) (Proc. of the Royal Society, 1909).
2) The relation of the Barth’s free precessional nutation ete.... (Proc. of the Roy. Soc. 1909).

 

rerbichansulksisansshsisaäunnnen

 
