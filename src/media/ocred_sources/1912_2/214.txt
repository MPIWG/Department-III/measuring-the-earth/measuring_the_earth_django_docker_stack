 

200

TABLEAU I. — GEEST. TABLEAU II. — MARSCH.

 

 

 

| Nombre des reperes Nombre des reperes

 

 

 

 

 

er des en ich ah Be ln
l’affaissement | | | V’affaissement | en
constat& 2 N Ip | H | Il constate | ia | m TE vgl

! ! | |
mm | | | mm | re
) 70 aut. 6 A 0 10023 7 14 25
A 27 ne - 2 I a Mi 4
2 30 A Be ol 16 2)
3 23 Ye I sr 36 | 2927 1 3
4 Se, a a ea en 7
5 ee 10—20 Era 10 16 8
6 2 ge i 20-30 62 5 6 3
7 a s 30—40 no 2 3 a
8 a , : oe E 3
9 | | » | 1 | » 50—60 al 2 » ”
10) 3 20 | | il | 1 60-70 ee oe
20 a 30 N me 70-80 Be Sl,
30 a 40 Ar, | | 80-90 8 | - ln
ii we Se Se a x h
Totaux | 301 Ra | 3 ee :
140 1 N » „
210 I 0,
Totaux 0 oe 55

 

 

M. Gurumr a tire notamment de son &tude cette conclusion que le scellement de
reperes sur les ouvrages d’art (categories II et III) est nettement preferable au scellement
sur bätiments, möme fondes sur pilotis (categorie Ib).

En trois endroits, on a observ& des relövements de rep£eres, vraisembablement dus:
pour l’un, ä la poussee des terres sur un mur de soutönement; pour le second, & l’exhaus-
sement d’une voüte de maconnerie, sous l’effet des variations de la temperature; pour la
troisiöme, scelle ä 6m de la rive de l’Elbe, dans le bätiment d’une Echelle de maree fond&
sur terrain argileux et &lastique, aux mouvements alternatifs d’exhaussement et d’affaissement
que ce bätiment subit avec le flux et le reflux de la maree, ä raison de Imm de deplace-
ment vertical pour 1 mötre de variation du niveau de l’eau.

d. Hxsse.

Dans le but de deceler des mouvements &ventuels du sol, on a reitere, sur 72 km,
le nivellement des Chemins de fer de Darmstadt & Hetzbach et de Wiebelsbach & Babenhausen.

D’autre part l’existenee d’un centre sismique ayant &t6 constate ä& Grosz-Gerau,
ancien nivellement de Darmstadt & Mayence sera reitere deux fois en 1913, apr&s que
la ligne entiere aura &t& largement dotee de repöres (rivets scell&s dans des murs, sur
des seuils d’ouvrages, ou dans le sol).

 

{

 
