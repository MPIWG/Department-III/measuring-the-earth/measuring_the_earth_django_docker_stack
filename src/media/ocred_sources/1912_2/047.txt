a

 

39

[eUy2

Gaithersburg: M. le Dr. Frank E, Ross et, pendant la seconde moitie de
l’annde, M. le Dr. ©. W. FRrEDzRIicK;

& Cincinnati: M. le Prof. Dr. J. G@. Porter et M. le Dr. HR. J. YoweıL;

a Ukiah: M. le Dr. Jamzs D. MAppkızu jusqu’au mois d’Aoüt et M. le

Dr. W. F. Msyer depuis le mois d’Aoüt.

De meöme que dans les annees precedentes, les reductions ordinaires ont et& faites,
immediatement apres la reception des cahiers originaux des observations, par M. le Prof.
WanacH, observateur & l’Institut geodesique, avec le concours des caleulateurs: M. A.
Wısanowskı, M. O. SchönreLp et Mwe HREks.

Pour les declinaisons moyennes des 80 couples d’etoiles, qui appartiennent tant au
programme actuel qu’au programme qu’on a introduit le 5 Janvier 1912, on a utilis& les
corrections des valeurs adoptces des declinaisons, deduites des reductions des observations
des annees 1906, 1907 et 1908 (Voir Vol. IV des „Resultate des internationalen Breiten-
dienstes’’ pag. 161). Pour les 16 couples nouvellement introduits dans le programme, on
a determine seulement les declinaisons moyennes d’apr&s les positions empruntees aux cata-
logues d’etoiles.

La plus grande partie des quantitös necessaires ä la reduction des positions moyennes
aux positions apparentes a &t& calculde par M. OÖ. ScHönreLn et Mme Hresr. Afın de mettre
les observateurs en etat de reduire eux mömes leurs observations et de se rendre compte
de la preeision de leurs rösultats, les releves des &phömerides des declinaisons apparentes,
du 7 Decembre 1912 au 7 Decembre 1913, interpoles pour les temps de culmination &
Greenwich, ont &t& envoy6ös le 4 Decembre 1912 & toutes les stations.

En me servant des corrections des valeurs adoptees des deelinaisons moyennes des
couples d’etoiles, publiges dans le Vol. IV des „Resultate des Internationalen Breitendienstes”,
jai deduit pour la periode 1911,0 & 1912,0, de la möme maniere que pour les anndes
precedentes, une orbite provisoire du pöle, dont les r&sultats ont &t6 publi6s dans le N 4588
des „Astronomische Nachrichten’. Ils permettent, des maintenant, de reduire & la position
moyenne du pöle les observations astronomiques et les determinations de longitude, de
latitude et d’azimut.

Des stations de latitude situees sous le parallöle de 31°55’, au sud de l’öquateur,
on n’a plus de resultats d’observations, puisque ä& Bayswater les observations ont cess& ä
la fin du mois de Juillet 1908 et & Oncativo ä la fin du mois de Juillet 1911. Les rösultats
des observations de Bayswater ont &t& completement publies dans le Vol. IV des „Resultate
et, tandis que pour les observations, faites & Oncativo, on a publi6 dans le möme volume
seulement les rösultats jusqu’au 5 Janvier 1909. Depuis cette &poque il nous restait encore
pour Oncativo des r&sultats plus ou moins complets jusqu’au 28 Juillet 1911, qui ont 6t6
reduits d’une maniere detaillde (Voir les Comptes rendus de la Conference gönörale tenue A
Hambourg, Vol. I, Annexe A. Ila) et qui nous ont fait voir aussi que la continuation des
observations & Oncativo a confirm& la conclusion, que les resultats deduits des observations
faites aux stations du parallele boral peuvent &tre appliques aux stations du parallele austral.

 
