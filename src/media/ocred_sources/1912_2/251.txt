I TEEN ET TUT

 

B. — FORMULES EN USAGE DANS QUELQUES PAYS POUR LE CALOUL DES ERREURS
PROBABLES DES NIVELLEMENTS (Suite).

a) Erreur accidentelle probable kilomötrique (suite).
| | Pays ou la formule
| est employe&e

 

 

Mö6thode de caleul. | Formule.

ne er OR WE RE une
ß) Caleul effectud en partant des dcarts de fermeture des ‚polygones :

 

( Pour un ( s u tr Suisse,
polygone: e OP

1°. sans tenir compte des

u mm,

s [
=” om So |

poids des &carts de fermeture. : Allemagne (Lan-
Pour un 4 ir desaufnahme),
reseau: Autriche-Hongrie,
Danemark.

France, Russie.

|
|
2°. en affectant de poids | Pour un |
les 6carts de fermeture. | |

) Erreur systömatique probable kilome6trique de la
moyenne de deux nivellements.

 

 

Pays oü la formule

est employee.
—m— m N

Methode de calecul. | Formule,

 

1°. Oaleul effectu& en partant des (Z u ı m | France, Belgique,
discordances relev6es entre les rösul- ) sL e N Roumanie.
tats des deux nivellements. en al Sy & a!

2 SE |

\ sue 68 S 9 9

2°. Caleul effeetu6 pour un röseau [.: ai = ee }
en comparant les erreurs acciden- | > |
li en des discordances de 2 Et \ France.
repere & repere, d’une part, et les \ SH
ecarts de fermeture des polygones, a 1 Be ano |
d’autre part. Sl I SL Gebr Tn)

 

Les conclusions qui m’ont paru pouvoir &tre degag6es des renseignements ci-dessus
font l’objet d’une note distinete, imprimde & la suite du present Rapport.

++ j 32

 
