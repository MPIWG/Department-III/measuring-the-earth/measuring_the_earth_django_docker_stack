 

 

 

289

nur innerhalb ihrer mittleren Unsicherheit von einander abweichen, so habe ich die Resultate
des Herrn Dr. Baranow, der als Beobachter am besten über seine Messungen orientiert
sein muss,. in unsre Tabelle aufgenommen. Damit tritt der im: Bericht 1909, 8. 148, für
die Engelhardt-Stw. vorläufig mitgeteilte y-Wert ausser Kraft.

In der im Vorangehenden besprochenen Publikation haben die Herren Dvzrago und
Baranow schliesslich noch auf verschiedene Weise versucht, die Schwerkraft für Kasan aus
sämtlichen früheren Verbindungen dieser Station mit Hauptpunkten des europäischen Schwere-
netzes abzuleiten. Da diese Arbeit für den vorliegenden Bericht kein wesentliches Interesse
bietet, indem der einwandfreieste Teil jener älteren Verbindungen zur Ableitung des y-Wertes
für Kasan im Ber. 1909 berücksichtigt worden ist, so beschränke ich mich hier auf die
Angabe der Endresultate. Herr Dr. Baranow erhielt aus 7 in der Zeit von 1896-1909
ausgeführten Verbindungen den Wert

9(Kasan im Potsd. Syst.) = 981.566 + 0.004 em sck,

während Herr Prof. Duzı1go aus dem vorhandenen Material 5 unabhängige Verbindungen
auswählte, aus denen er den Wert

g9(Kasan im Potsd. Syst.) = 981.571 + 0.004cm sek

fand. Das Dusıaco’sche Resultat stimmt mit dem Ergebnis der Netzausgleichung im Ber.
1909, das auf den damals vorliegenden älteren Verbindungen beruht, nahezu überein.

Die eingangs unter 4) erwähnte Publikation enthält zunächst die Ergebnisse der
gravimetrischen Verbindung von Kasan mit Pulkowo und St. Petersburg (Haupt-Eichamt),
die im Jahre 1911 von den Herren Dr. Baranow und Dr. BanacHawirsch gemeinsam aus-
geführt wurde. Es kam dabei nur das Pfeilerstativ in Anwendung, da eine in Kasan vor-
angegangene Untersuchung über den Einfluss der Bodenerschütterungen durch den Tages-
verkehr auf die Schwingungsdauer der Pendel für die beiden Stative folgende systematische
Einflüsse ergeben hatte:

Wandstativ Pfeilerstativ
Nacht — Tag - + 98.107 sek + 2,10” sek
Morgen — Abend +7 » —5 »

Auf die Bestimmung der Schwingungsdauer und ihrer wesentlichen Reduktions-
elemente (Pendeltemperatur, Uhrgang, Mitschwingen) verwendeten die Beobachter grosse
Sorgfalt. Die Messungen fanden auf allen 3 Stationen in Kellern statt; in Pulkowo war
der Beobachtungspfeiler nicht identisch mit dem von Hanskı und BorrAss 1901 benutzten;
in Petersburg war die Bodenunruhe ziemlich gross. Als Koinzidenzuhr diente in Kasan die
Pendeluhr Tırpr No. 268, in Pulkowo anfangs die Pendeluhr Honwö No. 28, deren Gang
jedoch so veränderlich war, dass später die mit einem Rızruer’schen Nickelstahlpendel ver-
sehene Uhr Hawesk No. 36 genommen wurde. Dieselbe Uhr fand auch in Petersburg

 
