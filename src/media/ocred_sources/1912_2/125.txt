17

West AUSTRALIA.

In May 1909 the ‘De Grey’ and ‘Marble Bar’ triangulations were connected by a
triangulation, the average error of close being 5”.4 per triangle (See triangles marked red
on tracing N°. 1 herewith).

In September 1910, the old coastal triangulation near Fremantle was connected with
the trigonometrical station at Mt. Bakewell near York, the average error of elose being
2",7 per angle (See triangles marked red on tracing N°. 2 herewith).

TASMANIA.

Although it is very desirable that the triangulation of Tasmania should be completed,
and though its conical heights greatly facilitate its triangulation, it has not been possible
to extend the Survey during the last three years. Its initial base of 20181.6 feet is in the
Country of Monmonth near the estuary of the Derwent, and the verification base of 25746
feet is near the town of Longford. The importance of its extension is being urged.

G. H. Knızps.
(Commonwealth Statistieian).
Past President and Honorary Member of the
Institution of Surveyors of N. S. Wales.

 
