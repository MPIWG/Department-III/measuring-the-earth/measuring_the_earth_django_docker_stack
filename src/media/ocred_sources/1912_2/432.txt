 

404.

avec un appareil tripendulaire de l’Institut g6odesique, afin de mettre l’exp6dition antaretique,
de 1910 du Capitaine Scort, en etat de faire dans la region antaretigue des d6terminations
relatives de la pesanteur par rapport & Potsdam. Le physicien O. 8. Wrisur a alors emport6
cet appareil avec une pendule de l’Institut geodesique. Il a pu faire les d&terminations & Ross
Island, et il est retournd, au mois d’Octobre, & Potsdam afın d’y renouveler les observations
avec les m&mes pendules et de nous rendre les instruments. Je suis heureux de pouvoir
constater que les trois pendules n’ont pas subi de changements appreciables, de sorte que
l’on peut s’attendre & de bons resultats.

Au commencement de l’annee, M. le Professeur HAaAsEMANN a fait & nouveau des
determinations fort exactes du coeflicient thermometrique pour l’appareil & quatre pendules
de la Commission geodesique danoise, Ensuite il a determine & nouveau le coeflicient thermo-
metrique pour l’appareil & quatre pendules de la Commission g&odesique de la Hongrie (voir le
rapport annuel du directeur de l’Institut geodösique royal 1912/13 pag. 25/26). Dans les deux
cas les nouvelles determinations se sont accordees fort bien avec les rösultats obtenus auparavant.

Pendant les mois de Novembre et de Decembre, l’ing&nieur de Ja Commission g&ode-
sique neerlandaise M. Venıne Meınasz a execute dans l’Institut geodesiqgue des observations
avec deux appareils de pendule, l’un d’apres Derrorgzs, l’autre d’apres STERNECK, construit
par SwÜückramH. Ü’etait son intention, entre autres, d’utiliser les installations de l’Institut
pour determiner le coeflicient thermomötrique du pendule francais.

Enfin au mois de Decembre, M. le Dr. FagerHuoLm de Stockholm a execute dans
Institut geodesique, avec un nouvel appareil ä& 4 pendules de FEcnner, des determinations
relatives pour obtenir la jonction avec Potsdam; comme le temps pressait, M. le Prof.
FIAASEMANN a coopere fort activement & ces travaux.

4, RäDUCTION ISOSTATIQUE DE L/INTENSITE DE LA PESANTEUR.

Le Bureau central a charge M. le Dr. Hüsser de caleuler les perturbations iso-
statiques de la pesanteur & 13 stations sur la cöte d’Afrique, en partant de ’hypothöse PrArr-
Hayrorp. Dans le tableau suivant on trouve les perturbations observees et calcul£es.

: { Perturbation de la pesanteur
Latitude Longitude

 

. observee calculee
m le Em yseCzz Gin Se Cgz

Tanger +35 46 5 490 — 0,040 + 0,011
Dakar +14 40 17 25» + 0,116 + 0,072
Bathurst a 19 16 34 » + 0,122 + 0,036
Freetown 029 13 1%» + 0,085 + 0,033
Monrovia a ol 10 49 » + 0,086 + 0,055
Lagos +63 3 26E + 0,059 + 0,035
Libreville + 02 9 927» — 0,030 +.0,016
Cap Lopez — 042 8 48» + 0,051 + 0,043
Banana — 6141 123) + 0,040 +0,016
Lobitobucht —12 20 43 35» + 0,059 + 0,002
Gr. Fischbucht — 16 24 11 43» + 0,049 + 0,034
Lüderitzbucht — 26 39 15 10» + 0,036 + 0,002
Mocambique —15 2 38 25» + 0,078 + 0,051

 

 

abs ansaane

 

 
