5 27

Die laufende Reduktion der Beobachtungen wurde gleichwie in den Vorjahren
unmittelbar nach Eingang der Original-Beobachtungsbücher von dem Observator im
Geodätischen Institut: Herrn Prof. Wanach, unter Mithilfe der Rechner: Lehrer
A. Wısaxowskt, O. SchöxreLp und Frau Heess ausgeführt.

Bei den mittleren Örtern der Sternpaare wurde für diejenigen 80 Sternpaare,
welche sowohl dem bisherigen, als auch dem am 5. Januar 1912 eingeführten neuen
Beobachtungsprogramm angehören, den Verbesserungen der angenommenen Deklinationen
der Sternpaare, welche aus der Bearbeitung der Jahrgänge 1906—1908 (vergl. Band IV
der „Reswitate des Internationalen Breitendienstes“ Seite 161) hervorgegangen sind,
Rechnung getragen. Nur für die 16 neu hinzugetretenen Sternpaare ist die Ableitung
von Aussangswerten für die Deklinationen lediglich auf Grund der in den Sternkatalogen
enthaltenen Positionen erfolgt.

Die Reduktionen der mittleren Deklinationen der Sternpaare auf den scheinbaren
Ort sind im wesentlichen von Herrn O. ScaöxreLo und Frau Hrvs» berechnet und die
Verzeichnisse der scheinbaren Deklinationen vom 7. Dezember 1912 bis 7. Dezember 1913,
für die Zeiten der Greenwicher Kulmination interpoliert, unter dem 4. Dezember 1912
den Stationen zugesandt worden, um den Beobachtern die Möglichkeit zu bieten, sich
über den Ausfall ihrer Beobachtungen durch Reduktion derselben selbst Rechenschaft
geben zu können.

Gleichwie in den Vorjahren habe ich auch in diesem Jahre eine provisorische
Ableitung der Bahn des Poles für das Zeitintervall von 1911.0—1912.0 auf Grundlage
der in Band IV der „Resultate etc.“ abgeleiteten Verbesserungen der angenommenen
mittleren Deklinationen der Sternpaare ausgeführt und deren Resultate in Nr. 4588 der
Astronomischen Nachrichten publiziert. Dadurch ist die Möglichkeit gegeben, die im
i Jahre 1911 ausgeführten astronomischen Beobachtungen und astronomisch-geographischen
Ortsbestimmungen schon jetzt vom Einfluß der Breitenvariation befreien und auf eine

mittlere Lage des Poles reduzieren zu können.

Tr

TEE"

Vom Südparallel —- 31°55’ lag aus dem Berichtsjahr kein weiteres Beobachtungs-
material vor, da die Beobachtungen in Bayswater schon Ende Juli 1908 und diejenigen
in Oncativo Ende Juli i911 ihren Abschluß erhalten hatten. Die Ergebnisse der
Beobachtungen in Bayswater sind bereits vollständig in Band IV der „Resultate ete.“
enthalten; von den Beobachtungen in Öneativo enthält Band IV aber nur die Resultate
bis zum 5. Januar 1909. Von Oncativo lag über diesen Termin hinaus noch ein mehr
oder minder vollständiges Beobachtungsmaterial bis zum 28. Juli 1911 vor, welches einer
eingehenden Bearbeitung unterzogen worden ist (vergl. den I. Teil der „Verhandlungen
der Hambarger Konferenz“ Beilage A. IIa.) und zu dem Resultat geführt hat, daß
die Zulässigkeit der Übertragung (der Resultate des Nordparallels auf die Südhalbkugel
auch durch die Fortsetzung der Beobachtungen in Oneativo Bestätigung findet.

Einen Ersatz für den Ausfall der Stationen Bayswater und Oncativo auf der
Südhalbkugel bietet die Beobachtungsreihe in Johannesburg (— 26° 11’ Breite), welche

i 6

 
