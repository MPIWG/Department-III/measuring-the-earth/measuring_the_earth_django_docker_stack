 

!
Ei
£
Ei

= ben
Fu

 

en

146

2

n? (4m.8” sin 1” ne = ns

Chaque segment donne une &quation de cette nature, d’oü un systeme que l’on peut
traiter par la methode des moindres carres.

Quand un nivellement de preeision relie aussi les divers rep£äres, on a interet &

evaluer A par rapport aux altitudes de ce nivellement suppose tr&ös exact et le nombre des
equations est double.

A Lyon, le nivellement de la base par la rögle donne entre l’aller et le retour une
discordance de 0m,803. Cet Ecart est en grande partie dü ä la collimation du niveau, erreur
attenue & la suite d’une rectification faite & Breteuil, comme nous l’avons dejä dit.

Pour 2.120 portees, si l’on admet que l’Ecart est dü uniquement ä cette cause d’erreur,
on a:

i 1 803mm ü
INN — nr 0,00005
£ vaut donc & peu pres 50°, mais la pente du terrain de la base est trop faible pour que
cette correetion puisse modifier les longueurs mesur6es, car la pente n’atteint 0,017 que sur
les 8 premiers hectometres, et demeure ensuite presque nulle sur tout le reste de la base.

L’erreur aceidentelle sur 8 n’est pas trös grande, vu que la moyenne des differences
de niveau aller et retour par la rögle est de 22m,251, trös voisine des valeurs 220,377
fournie par les distances zenithales geodesiques et 22m,496 donnee par la moyenne des
nivellements au moyen des fils.

L’influence de l’erreur de lecture accidentelle du niveau d’ailleurs est insignifiante
sur le nivellement.

Exemple: & Lyon, » = 2.120 portees. On peut supposer i toujours connu & 40” pres.

L.di. Vn = 40“ X 0,0000016X 4m X V 2,120 = 12 mm.

Das le cas des fils, on sait que si la pente est sup6erieure ä& 1/100, il faut se mefier
des lectures faites & la lunette nivelatrice qui fait partie de l’apparel UArPENnTIER, d cause
des defauts de l’&chelle mieromötrique. C’est le cas de la base de Blida qui, dans son en-
semble, a une pente de 0,018. Or preeisöment, le nivellement doit &tre alors plus exact;
pour ces deux raisons, on aura recours dans un pareil cas ä un nivellement geometrique direct,
avec mire et niveau.

A Lyon, la diffsrenee de niveau (en moyenne) trouv6e avec l’appareil ÜARPENTIER est
de 0m,120 par rapport & la difference d’altitude admise entre les 2 reperes. L’erreur acci-
dentelle (dz) en parties du rayon serait done:

24m. da. V 353 portees = 0,120 ou dx = 0,0003

et, comme il y a une visee directe et une visee inverse, l’erreur moyenne sur chaque visee
serait 0,0005, nombre trös petit, qui est d’accord avec la valeur que l’on aurait pu fixer
ä priori.

 

i
i
