mn

Tr

 

55

(1250 Fr.) ont 6t& depens6s pour ces recherches, on doit inscrire encore 8000 M. (10 000 Fr.)
pour le m&me but.

Pour les recherches concernant les deformations de la terre sous l’influence de la
lune et du soleil on a vot& dans la derniere Oonference 6000 M. (7500 Fr.) dont on n’a
depense jusqu’& prösent que 3000 M. (3750 Fr.). Il faut done voter le reste de 3000 M.
(3750 Fr.) pour le prochain triennat.

Pour les recherches sur les deviations de la verticale et pour les recherches relatives
ä& la gravit& on avait accord& dans la derniöre Conference une somme de 13 000. M. (16 250 Fr.)
dont on a dejä depense 11000 M. (13 750 Fr.). Il parait indiqu& de continuer cette sub-
vention avec la somme de 12000 M. (15 000 Fr.).

Enfin il est vivement recommand& d’aider l’observatoire de Turin par une subvention
de 3000 M. (3 750 Fr.) pour l’observation suivie d’un groupe d’etoiles zenithales, tout
exceptionnellement favorable ä la determination de la latitude.

On a done ce tableau triennal:

1) Subvention de la Station Johannesburg . . A 4000 5.000
2) » de l’Observatoire de Santiago (Chili) ale 3.000 3750
3) » desl.Obseryatozer de urn az 7 3.000 3750
4) Pour les recherches theoriques et numeriques du mouve-

ment de l’axe terrestre . . 8000 10000
5) Pour les recherches concernant ee: Al mins ds ie

terre sous l’influence de la lune et du soleil. . . 3.000 3750
6) Pour les recherches sur les deviations de la verticale et

relatives a larorayite - >... .,2 2.2 22022 .120002 19000

oral 27290005 749250

Ces depenses deduites de la somme de 67000 M. (83 750 Fr.) disponible pour les
depenses extraordinaires du prochain triennat laisseraient une reserve de 34 000 M. (42 500 Fr.).
La Commission est d’avis que cette reserve pourra &tre regardee comme suffisante
pour la sürete du service, pourvu qu’on ait soin que les sommes pr&vues ne soient pas depassees.

Mapsrx, FOERSTER,
President. Rapporteur,
