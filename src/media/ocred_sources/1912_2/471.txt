 

— 18 —-
On deduit du tableau precedent que l’erreur probable de fermeture d’un triangle est
== 11.34
D’ou il resulte, pour l’erreur probable d’un angle, la valeurs
nn 0.

Le sommet Renca a et& choisi, pour la compensation, comme origine du reseau et le cöte
Renca—Lagunas comme premier cöte diagonal. La base amplifide est le cötE Calera—Cementerio
et on a adopte, pour le vogarithme de sa longueur, la valeur

logs — 4,601.776 3 (Calera — Cementerio)

Le calcul des cötes successifs a dte ex&cute comme ila &te indiqu6 plus hautetona obtenu,
de deux manieres differentes, la distance Renca — Lagunas, en partant de la base amplifice, dans
un sens et dans l’autre. Les deux valeurs sont

logs (Ikenca — Lagunas)

 

 

A) 4,427.1758
(ID 4,457.1865
DM) m
On deduit de la
z — 0,04750 d (logs) = — 5,"08

D’autre part le calcul des orientations des cötes successifs et celui des coordonnees des

sommets ont donne les resultats suivants, pour la diagonale Renca— Lagunas et le sommet Renca,

Q(Renca— Lagunas) X (Renca) Y (Renca)

N a ei met met
(I) 292. 45. 38,42 — 5.989,83 — 2.034,28
(II) 292. 45. 34,81 — 5.986,10 —+ 2.634,20

 

 

 

 

 

Da 0 a +0,08

 

|
i

 
