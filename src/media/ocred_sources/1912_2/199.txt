 

 

 

 

 

185

Enfin, lorsque, suivant une pratique observee par le m&me service depuis 1886 '), il
a &t6 en outre preserit de placer la troisitme branche alternativement & droite et ä& gauche
du cheminement, dans deux stations cons6cutives, l’erreur en question est tombee & quelques
dixitmes de millimötres par kilometre.

Rep£sre fondamental anglais.
(Echelle: 75%).

mm | m}

en 4 ed

 

 

 

 

 

 

 

   

 

 

     

  
 
    

 

 

  

 

 

 

1 |
Fig. 3. — Plan Fig. 3bis,. — Plan
pour une profondeur n’excedant pas 1m,60. pour une profondeur excedant 11,80.
72
I 72

I: 2 DL
REGEN, RER RG 3. g:

! 1 ! 7 TÜR} EEG TE TÜR,

i D ee:

 

rn

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Fig. 4. — Coupe Fig. 4bis, — Coupe
pour un repere etabli dans la marne ou dans la pour un repere etabli dans le roc ferme.
craie dure.
LEGENDE.
M. Massif en ciment, r,r. Rivets metalliques scelles au eiment,
B. Borne en pierre, s.  Bloe de silex,

p. Plaque indicatrice,
L. Cavite a parois cimentees, fermee par une
dalle D, en calcaire d’York.

Couvercles et cadres en fer,
Tuyau d’assainissement en fer,
Poteaux d’angle de la balustrade.

SEID

1) Voir: Rapport sur les travaux du Service du Nivellement General de la France presente A la 13me Conference
Generale de l‘Association geodesique Iuternationale, tenue & Paris en 1900,

 
