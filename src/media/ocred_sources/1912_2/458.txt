Re) u

Ecuations de condition

La compensation a pour objet de corriger les angles des triangles de telle sorte que
l’erreur de fermeture du polygone disparaisse.

On adopte conme inceonnues les eorrections des deux angles de chaque triangle, adjacents
aux cötes lateraux; ces angles ont dejäa dte corriges deux fois: d’abord pour appliquer le theor&me
de Legendre et ensuite pour tenir compte .de l’excös spherique. Ce sont done les correccions de
ces angles, dejä corriges deux fois, qui sont les inconnues du probleme de la compensation.

Soit » le nombre des triangles du rescau; les cötes diagonaux se designent par les nu-
meros pairs’ successifs, depuis zero, qui represente la diagonale  origine du’reseau, jusqu’a In
qui represente de nouveau cette möme diagonale. Les indices croissent lorsqu’on pareourt le po: '
Iygone de gauche ä droite.

Dans chaque triangle les correetions inconnues des deux angles se designent por la lettre
x, affectee du signe + ou —, etd’un indice choisi comme l'indique la figure ci contre. —fig ()—.

Figura 1

 
  

 

‚Polıgono estersor
NITEE

c.
7

Le signe de x est + lorsque le cöte lateral du triangle est situd sur le polygone interieur
du reseau et — dans l’autre cas.
Soit d2 la variation de l’orientation du eöt« diagonal d’indice 2k; on deduit de la figure

2k
a2 ©, + x + x
2k 3k2 2k1 2k
de = a) a nn
2k-+2 2% KL 2K+2

 
