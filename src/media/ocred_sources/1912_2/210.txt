 

   

 

196

Methode d’operations. — Avee. le niveau du Coast and Geodetie Survey on utilise
les trois fils du rötieule.

Avec le niveau de la Landesaufnahme, on pointe le fil niveleur successivemönt sur deux
traits consscutifs de la division de la mire et, chaque fois, on lit la position des extr&mites
de la bulle.

M. Ernest Greve a fait construire un „interpolateur” special, permettant & l’obser-
vateur de caleuler rapidement la lecture de mire qui aurait &{6 faite si la bulle eüt ete
mise exactement entre’ ses repöres. Üet appareil qui dispense l’observateur de connaitre la
valeur angulaire d’une division de la fiole, a et& trouv& d’un emploi trös commode en
campagne.

Le niveau de la Landesaufnahme et la methode correspondante de pointages ont
paru pröferables, bien, qu’en fait, on n’ait pas eu l’occasion d’essayer, dans des conditions
identiques, les deux instruments et les deux methodes,

25. Rürusuıqus Argentıns. (Ministöre des Travaux Publics).

Reperes. — On emploie trois types de repöres, savoir: un modele & console (fie. 21,
22 et 23), et un modele eylindrique (fig. 24 et 25) rappelant les modeles frangais corres-

Fig. 21, 22 et 23. — Reperes ä console de la Republique Argentine.

 

DELA

IV,

4

 

 

 

 

 

 

 

 

Elevation Vue de profil.

: ER, : : 5 Serie
pondants, et aussi un goujon cylindrique (ig. 26) de plus petite dimension. Ües reperes
sont scellds sur des bätiments ou, & defaut, sur des piliers en magonnerie construits ü

cet eflet.

 

 

|

 
