151

car lerreur d’etalonnage et lerreur de dilatation se traduisent dans leur effet, par des erreurs
systematiques, proportionnelles aux longueurs, et disparaissent en trös grande partie dans
les discordances.

Quant & la formule & appliquer dans le calcul de l’erreur apparente, il me parait
indiqu& pour le moment de donner la preference ä& l’expression habituelle:

1 Ve ao
BE Br n, (n,-1) a Gen...

les discordances »,, v, — etant caleulees pour chaque mesure aller ou retour par rapport

ä la moyenne des mesures par section, B &tant la longueur de la base en kilomötres.
L’erreur de nivellement, je l’ai montre, se decompose en deux parties, l’une qui prend

la forme aceidentelle et l’autre l’allure systömatique. J’admets que l’effet de la lüre ’est

normalement fait sentir dans les discordances et qu’il n’y a plus & en tenir compte.
L’erreur totale est done de la forme:

 

„.B-- e.V B, pour une base de B kilomötres.

La rapportant au kilomötre, je distinguerai l’erreur accidentelle e,; et l’erreur totale
(9, + &). Au moyen de ces deux donnees, il est aise d’obtenir l’erreur systematique (kilo-
metrique) yx, et de calculer une quelconque des 3 erreurs pour toute la longueur de la base.

Aussi les tableaux que j’ai donne dans mon ler Rapport (Rapport triennal), mention-
nent-ils les coeflicients e; et 4, + e;, suflisants pour permettre toutes les comparaisons et
tous les caleuls.

NOUYEAU QUESTIONNAIRE.

Comme consequence de l’&tude qui pr&cede, je propose une nouvelle forme de ques-
tionnaire relative aux bases, qui tient compte des id6es developp6es plus haut.

1°. Details sur le transport & pied d’euvre des appareils, depuis le d&pöt d’oü ils sont
partis, jusqu’au terrain de la mesure.

2°. En quelle saison la mesure a-t-elle &t& effectu&e? — Circonstances atmospheriques. —
A quel moment de la journee a-t-on oper6?

3°. Details descriptifs et numeriques sur chaque appareil employe.

4°. Sectionnement de la base.

5°. Nombre de mesures de chaque segment. Les appareils sont-ils abrites du soleil et

comment?

6°. L’elimination de la difference d’&quation personnelle est-elle envisagee ?

7°. Tableau des mesures individuelles de chaque segment.

8°. Nivellement de l’appareil de mesure.

9°. Mesure des appoints, centrage sur les termes et les repöres de fin de journee, pr6-
cision obtenue.

10°. Etalonnage des appareils, proced& employ6? — Hiquation des appareils.

 
