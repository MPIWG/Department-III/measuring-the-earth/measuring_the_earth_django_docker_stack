ji
LE

 

ANNEXE B, VIII,

BESCHLUSS

den Präcisionsnivellement betreffend, gefasst
in der Sitzung vom 25°r September 1912 der 17. allgemeinen Konferenz
der Internationalen Erdmessung.

In Anbetracht der grossen Fortschritte in der Genauigkeit der Nivellements seit
1867, dem Jahre, in welchem zum ersten Male der Begriff „Präzisions-Nivellement” durch
Fehlergrenzen festgesetzt wurde und in Hinblick auf die heutigen, erhöhten Forderungen
der Geodäsie, die eine neue Kategorie von Nivellements mit engeren Fehlergrenzen und
eine einheitliche Fehlerrechnung bedingen,

beschliesst die 17. Konferenz:

neben den unverändert bestehenden Präzisions-nivellements sollen in Zukunft genannt
werden

Nivellements von hoher Präzision

alle diejenigen Gebilde — Linien, Schleifen, Netze —, die folgende Bedingungen erfüllen:

1) zweimalige Nivellierung,
2) entgegengesetzte Richtung beider Operationen,
3) an verschiedenen Tagen, soweit dies die besonderen Umstände zulassen,
4) die Fehler, nach unterstehenden Formeln gerechnet, dürfen folgende Grenzen nicht

überschreiten:

+ 1,0 mm in Bezug auf die wahrscheinlichen, zufälligen Fehler,
oder: ı+ Las., 5 »  » mittleren, zufälligen Fehler,

0,0 5 »  „ Wwahrscheinlichen, systematischen Fehler,
oder 203. E »  „» mittleren, systematischen Fehler,

 
