 

ra WETTER

CHI ll ıkı

1. _ Geodatische Arbeiten.

Y

Die Triangulierung des sogenannten Uentr alnetzes (Red Central) ist beendigt
worden. Dasselbe erstreckt sich zwischen de Paralellkreisen 32° 22’ und 33° 42’ und zwis.
-hen den Meridianen 70° 26° und 71° 33’ westlicher Länge von Greenwich ei reitet sich
u, Santiagd, Valparaiso und Aconcagua von Central-Chile aus.
Die Winkelmessung a nach der Schrei berschen Methode ausgeführt mit dem
Unterschiede, dass in jeder Stellung de ns der zu messende Winkel entweder im
Hinsgang oder im Rückgang der Alidade beob yachtet wurde, so dass nur die Hälfte der
Aufschreibungen gemacht wurden, die bei der regelrechten Durchführung der Methode
nöthig gewesen wäre. Dieser be deutenden Arbeitsverminde rung stand indess eine kleine
Vermehrung gegenüber, die darin bestand, dass bei jeder E instellung des Fernrohrs auch
die Enden der Blase des Reiternivells abgelesen wurden, um später nach der Formel

 

über die

  

Eh ousnltoh

den Einfluss der Schiefe der Vertikalachse auf die Horizontalwinkelmessung zu corri-
SIeren.
Nun ist » sen I die Componente die der Aufstellungsfehler auf die
chse im rechten nn zur Zielrichtung ©) dieser Werth wird aber gerade
durch die Reiterlibelle gemessen, so dass man für die vorstehende Formel:

I—I, = (S—60). _ tg h setzen kann.

Die Vorzeichenregel wurde am Instrument ausprobiert, indem man bei positiven

oder negativen h einen st ken Vertikalachsenf la absichtlich herbeiführte und dann
eine Beobachtung bei nicht berichtigten und berichtigten Iustrument machte. Der \nters-

chied der Ablesungen muss dann elei eh der berechneten Correetion sein.

 

*) Siehe Handbuch der Vermessungskunde, IE. Band, Jordan—Reinhertz-Eggert 1908. Seite 252.

 
