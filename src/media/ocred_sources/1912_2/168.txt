 

 

y59m,743.88
744.39
744.72

moyenne — 959m,744.31
— Omm,d8

v® ( — 0mm,08

+ Omm,41

ee van 0,40 — 0nm,26
3309

= Omm 27,

puis ce segment-&talon a servi & etalonner les 6 fils invar employes pour le reste de la
base; ä cet eflet, on l’a mesur& deux fois aller et deux fois retour avec chacun de ces 6 fils,
d’abord avant, ensuite apr&s les op6rations, et, adoptant pour la longueur du segment-etalon
la moyenne des mesures A la rögle, on en a deduit pour chaque fil son &quation (& 15°),
en prenant la moyenne des 4 nombres obtenus avec ce fil. On n’a pas tenu compte, dans
la reduction, des r6sultats donnds par le Bureau de Breteuil un peu auparavant, qui en
differaient respectivement de Omm,14, Qmm,]7, Omm,10, Owm,16, Omm,16 et Omm,12).

On peut admettre ä priori que la pr&cision d’une mesure au fil est 1/200,000, dans
les conditions les plus döfeetueuses. Toutefois, ä& titre d’exp6rience, on a consery6 ici les
mesures au fil aller et retour, bien que les 6carts correspondent ü des fractions plus grandes.

Par definition, la moyenne des 24 mesures au fil sur le segment-&talon est, natu-
rellement la m&me que la moyenne des mesures ä la rögle. Ces mesures donnent 24 6carts,
qu’il n’y a pas lieu de faire partieiper au caleul de l’erreur apparente. Quoi qu’il en soit,
dans le cas partieulier, il se trouve qu’en raison du grand nombre de mesures, la preeision
des 24 mesures au fil est assez grande. Ein effet:

 

v v2

a

7,08 50,13

“el 1,00 1,00

il m. 4,28 18,32

| 3,80 14,44

7,60 57,76

| 3,08 9,49

‚14| 612 37,45

| #48 20,07

| 6,40 40,96

1,80 3,24

20: 5,0 25,00

| 3,20 10,24

 

 
