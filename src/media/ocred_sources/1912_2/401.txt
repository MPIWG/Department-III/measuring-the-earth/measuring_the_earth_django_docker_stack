i

 

373

1898. Verh. A.K. S. 569—571. Annexe O. II. A. TanakADATE: A proposal for a mercurial
level, to be used in Zenittelescopes.
S. 48, 137. Diskussion über die Ermittelung von Lotschwankungen auf den
Breitenstationen durch Nivellements, bezw. Horizontalpendel.

1900. Verh. A. K. II. 8. 100—111. Beilage B. V. Tu. Ausrecar: Bericht über die Breiten-
beobachtungen auf den 6 internationalen Stationen.
Abbildung des Zenitteleskops.
Vom Jahre 1900 ab sind die Berichte über die Breitenvariation teils in den all-
jährlich erscheinenden „Berichten über die Tätigkeit des Zentralbureaus’” enthalten,
teils sind die ausführlichen Resultate speziell des Breitendienstes in der Publikation
„Resultate des Internationalen Breitendienstes’’ (Band I Berlin 1903, Band II
Berlin 1906, Band III Berlin 1909, Band IV Berlin 1911) niedergelegt.

1912. Verh. A. K. S. 41, 104. Bericht von Ta. Ansrecart mit Beilagen, A. Ile. S, 201—
211 und A. IB 8.212 — 222, über Oncativa und Johannesburg.

LITERATUR.

1880. Verh, A. K. Anhang. IX. M. Sıpzseck: Literatur der praktischen und theoretischen
Gradmessungsarbeiten. 108 S.

1883. Verh. A. K. Annexe VIII. O. Börsce: Literatur der praktischen und theoretischen
Gradmessungsarbeiten, zweite Mitteilung. 82 8.

1889. O. BörscHh. Geodätische Literatur, auf Wunsch der Permanenten Kommission zu-
sammengestellt. (Berlin, G. Reımer).

1887. J. H. Gore. A Bibliography of Geodesy. (U. St. Coast and Geodetic Survey. Report
for 1887, App. 16).

1902. J. H. Gore. A Bibliography of Geodesy. Second edition. (U. St. Coast and Geodetic
Survey. Report for 1902, App. 8).

 
