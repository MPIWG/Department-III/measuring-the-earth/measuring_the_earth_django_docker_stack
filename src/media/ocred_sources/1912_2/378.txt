 

N

 

tungen beziehen, das Hauptmondglied M, abgeleitet. Der Apparat war I km von der Küste
entfernt aufgestellt. Ich fand:

Richtung W308 0.0027  theor. Wert: 0.0105
a N. 0.0 nn 000R
ae 0.0005 „er 0.0030,

Das Verhältnis der Beobachtung zur Theorie ergibt sich aus jeder Richtung verschieden und
viel zu klein. Der Einfluss der Gezeiten des Meeres ist demnach bedeutend.

Ich habe ferner alle bisherigen Beobachtungen mit Horizontalpendeln, die sich aus-
schliesslich auf die halbtägige Mondwelle beziehen, diskutiert und gefunden, dass die neueren
Ergebnisse die älteren im wesentlichen bestätigen und für die oben definierte Grösse X den
Wert von 0.33 bis 0.4 liefern. Hieraus folgt für den Starrheitskoeffizienten der Erde 5 bis
6.10!! (egs), während die CHanpLer’sche Periode der Polbewegung 11.7.10!! ergibt und
die Erdbebenbeobachtungen noch grössere Werte fordern.

Um diesen Unterschied näher zu untersuchen, habe ich zunächst die Meeresgezeiten
stalisch in Rechnung gezogen; dies ist notwendig, wenn der Einfluss der Polflut auf die
Unanpter’sche Periode festgestellt werden soll, da erstere den statischen Gesetzen folgt.
Hierbei ist angenommen, dass die ganze Erde vom Ozean bedeckt wird.

Die statischen Meeresgezeiten verkleinern die elastischen Fluten und vergrössern
das Verhältnis der Beobachtung zur Theorie. Wäre die Erde so starr wie Stahl, so würden
die elastischen Fluten um '/,, verkleinert und jenes Verhältnis um '/, vergrössert. Berück-
sichtigt man von diesem statischen Gesichtspunkt aus den Einfluss des Meeres und berechnet
den Starrheitskoeflizienten aus den Pendelbeobachtungen, so ergibt sich, wie zu erwarten
ist, ein noch kleinerer Wert, nämlich 3.6.10!'!; der Unterschied gegen das Resultat aus der
Polbewegung wird demnach noch grösser.

Der Grund hierfür liegt aber darin, dass die halbtägige Meeresflut, die bei dem
Horizontalpendel als störender Faktor in Frage kommt, durchaus nicht nach den Gesetzen
der Statik berechnet werden darf. Die dynamische Theorie der Meeresgezeiten lehrt, dass
die nahezu halbtägigen Tiden, je nach dem zu Grunde gelegten Tiefengesetz auf der ganzen
Erde oder teilweise umgekehrt sind, d.h. dass Hochwasser eintritt, wo die statische Theorie
Niedrigwasser fordert und umgekehrt. Da nun die elastischen Gezeiten den stati-
tischen Gesetzen folgen, so wird der periodische Druck des Meeres die Deformation der
festen Oberfläche vergrössern, da das Wasser gerade abfliesst, wenn das Land steigt. Wir
beobachten also im Vergleich zu der Flutkraft zu grosse elastische Fluten und berechnen daher
einen zu kleinen Starrheitskoeflizienten gegenüber den Resultaten aus der Polbewegung.

Eine strenge Berücksichtigung der Meeresfluten vom dynamischen Standpunkte ist
vorläufig kaum möglich, da die Theorie noch zu unvollkommen ist. Um wenigstens eine
Schätzung und ein allgemeines Bild von der Wirkungsweise der dynamischen Gezeiten auf
die elastische Ebbe und Flut zu geben, habe ich die Rechnung für das spezielle Tiefengesetz

Mel sin 20,

 
