 

f
W

252

alles bezogen auf 1 km als Einheit.
Bezeichnet man mit:

L, die Länge einer einzelnen Strecke oder, bei Netzen, die Länge einer Polygon-Seite ;
ZL, die Gesamtumfang des Netzes;
A, die Widerspruch in Hin- und Rückmessung von Festpunkt zu Festpunkt;
r, die Entfernung zwischen diesen zwei Festpunkten ;
S, den Widerspruch zwischen beiden Einzelmessungen ganzer Linien, oder bei Netzen,
von Knotenpunkt zu Knotenpunkt;
f, den: Schlussfehler der Schleife eines Netzes nach Anbringung der orthometrischen
Korrektion;
2f?, die Summe der Quadrate vorstehender Widersprüche, einschliesslich denjenigen
(Zf) des umschliessenden Vielecks;

=

so ergeben sich für die Fehlerrechnung folgende Formeln:

1°. Der wahrscheinliche, zufällige Fehler für Gruppen von Linien, mögen
diese eine geschlossene Figur bilden oder nicht:

; A ra
( ee MO, SL. en -L n

2°. Der wahrscheinliche, systematische Fehler,

a. Bei Gruppen von Linien, die kein Netz bilden:

1 u
(U) ° . . . . F={95] So,

b. Bei Netzen, die aus mindestens 10 Schleifen gebildet werden:

1 2

Die mittleren zufälligen und systematischen Fehler ergeben sich, wenn man in

9 ja: M
den vorstehenden Formeln die rechte Seite mit 7 multipliziert.

 

u.

i
|
i
|
i

 
