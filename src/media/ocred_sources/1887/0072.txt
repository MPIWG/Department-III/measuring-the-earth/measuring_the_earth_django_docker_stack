 

 

 

66

. . . ” a : . . r r Go
A MM. les delöguds qu’ils veuillent bien, jusqu’ä la session suivante, reflechin

permanente el | : a
sur la meilleure utilisation d’une partie du total de ces moyens dısponibles.

D’apres notre avis, il se prösentera un assez grand nombre de travaux ou de

publications scientifiques d’un inter&t commun A nous tous, pour lesquels il sera tres utile

voir disponi | 3 d ‚enti s croyons
«’avoir disponibles, au moment propice, quelques moyens de subvention, et nous croy

qu’ä cet egard Vavenir nous ouyrira encore des horizons plus etendus.

4» Nous devons enfin laisser ä la session suivante le soin du contröle et de la
decharge de la gestion financiere pour l’exereice de 1887, qui n’est pas encore termine. N. le
Directeur du Bureau central soumeltra alors les comptes de 1887 et les pieces juslificatives
A l’examen de la Commission permanente.

/ Le Rupporteur,
F@ERSTER.

Le rapport constate, entre autres, que le solde disponible est actuellement de 6750
francs, et que la Commission propose de porler ce solde ä compte nouveau.

Cetle proposition, ainsı que les aulres, comprises dans le rapport, sont adoplees.

M. Hirsch est d’accord que les finances de la Commission ne se trouventpas dans un etat
trop mauvais. Toutefois il croit utile de constater qu’ä la date du 1er octobre, quatre des
Etats associes, parmi lesquels trois grands pays, n’ont pas encore versd leur contribution
pour l’annde courante. Q’est une preuve nouvelle ä l’appui de la n&cessite, mentionnee dans
le rapport du Bureau de la Commission, lu dans la premiere seance, de proposer & la pro-
chaine Gonförence generale de demander aux puissances signataires d’ajouter A la Conven-
tion une disposition qui permette au Bureau de la Commission de s’adresser aux Gou-
vernements de ces Etats, ainsi que cela est prevu, par exemple, dans la Convention du
metlre.

M. von Kalmär lit une communication de son gouvernement, par laquelle il est auto-
ris6 a annoncer le paiement immediat de la contribution autrichienne.

M. le President remercie la Sous-Commission et son rapporteur, M. Foerster.

M. le President, passant au dernier sujet A l’ordre du jour, donne la parole A
M. Helmert, qui lit le projet de programme pour les travaux du Bureau central. Le voici:

Programme de travail pour le Bureau central en 1 887/88.

I. (Conformement & la proposition de M. Bakhuysen) : Etude de la distribution
gcographique des points astronomiques, afın de pr&parer, d’accord avec les

ns { delegues des
differents Etats, de

nouvelles döterminalions astronomiques de coordonndes g6ographiques.
2. Etude et catalogue des declinaisons d’&toiles qui rösultent des

nd. ee determinations de
alitudes faites jusqu’ä prösent par des observations au 1er vertical.

|

   

 

   
    
 
  
  

 
