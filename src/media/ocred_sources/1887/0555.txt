 

 

 

 

 

Annexe N» X.

RAPPORT

SUR L

IHAVAUX EN SUISSE

SS
a

7

I. La triangulation prineipale de premier ordre de la Suisse est terminde depuis
plusieurs annees et les rösultats publies par la Commission geodösique. Comme je Vai
explique dejä dans le dernier rapport & Berlin, les trois röseaux de jonction de nos bases
au reseau principal ont offert des difficultes speeiales, qui sont dues soit aux attractions
locales, soit aux r&fractions laterales exceptionnelles; de nouvelles mesures angulaires de
contröle ont confirme la Commission dans ’opinion que, nalgr& la conformation en quelques
parties peu favorable de certains triangles de ces röseaux, les erreurs d’observation tout &
fait normales sont hors de cause. Du reste, pour le r&seau de la base de Bellinzone, dans
lequel nous avons determine les deviations de la verticale dans plusieurs stations, en y me-
surant la latitude et ’azimut, les contradictions ont presque entierement disparu. La Com-
mission va proc£der au m&me moyen au moins pour un des deux autres röseaux. Les mesures
trigonometriques se bornent maintenant en Suisse essentiellement A la jonction des stations
astronomiques au reseau general.

2. Les travaux asironomiques et les mesures de la pesanteur vont prendre en Suisse
plus d’extension, d’apres la decision de notre Commission g&odesique et conform&ment aux
recommandations de la Conference de Berlin. Dans ce but, nous avons fait Pacquisition d’un
nouvel instrument universel de Repsold, qui va &tre livr& au printemps 1888; nous avons
demande au c&lebre artiste quelques modifications de detail, qui concernent essentiellement
un meilleur Eclairage du champ par plusieurs prismes, un röseau de fils plus complet,
y compris un fil mobile vertical, et des plaques de calage, munies chacune d’une [ente, pour

eviter des tensions du Ir&pied par suite des dilatations.
RAPPORT SUISSE — I

 
