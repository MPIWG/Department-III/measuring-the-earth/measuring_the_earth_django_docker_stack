 

 

 

 

 

3

Die Unternehmungen im Harze hatten sich fortdauernd der Unterstützung der Her-
zoglich Braunschweig-Lüueburgischen Kammer und der ihr unterstellten Forstbeamten zu
erfreuen.

Es ist noch zu erwähnen, dass auch in diesem Jahre, im Oktober, eine Revision des
dem Geodätischen Institut unterstellten Mareographen in Travemünde, sowie mehrerer Pegel
an der Ostseeküste behufs Sicherung der Höhenlage stattgefunden hat. Der Swinemünder
Mareograph ist leider im Sommer bei einem Brande des ihn bergenden, noch anderen
Zwecken dienenden Gebäudes verbrannt. Bis zur Wiederherstellung müssen die täglich ein
Mal an einem Pegel daselbst stattfindenden Ablesungen zur Ausfüllung der Lücken dienen,
was erfahrungsmässig im vorliegenden Falle für die Bildung des Mittelwasserstandes aus-
reicht.

G. BUREAUARBEITEN.

Die Reduction der sämmtlichen astronomischen Beobachtungen sowohl des vorigen
Jahres wie der früheren ist im Wesentlichen beendet. \
Die Ergebnisse der Längenbestimmungen des Jahres 1886 sind folgende :

: Rugard, T. P., östlich von Kiel, Meridiankreis :

1311,59 —£ 0,013 mitt. F., 15 Abende, Gewicht 14,06.
Berlin, Centr. der Sternw., östlich von Kiel, Meridiankreis:

1259'941 + 0,95 mittl. F., 15 Abende, Gewicht 12,69.

Berlin, Centr. der Sternw., östlich von Rauenberg, T. P.:
m Ss Ss
0 6,393 + 0,007 mitt. F., 21 Abende, Gewicht 19,38.

Hierbei sind die Gewichte auf einen vollen Abend als Einheit bezogen und die mittle-
ren Fehler aus den Abweichungen der Ergebnisse verschiedener Abende vom Mittel unter
Annahme eines konstanten Werthes der persönlichen Gleichung abgeleitet.

Der Einfluss der Instrumente bildete den Gegenstand besonderer Untersuchung
bei der dritten der obigen Bestimmungen, indem ausser mehrmaligem Wechsel der Be-
obachter auch ein Wechsel der Instrumente stattfand. Der Einfluss fand sich glich 0.008, ist
mithin nicht mit Sicherheit zu konstatiren.

Die Längenbestimmungen Kiel-Rugard und Kiel-Berlin sind mit den anderen Bestim-
mungen dieser Art von Seiten des Geodätischen Instituts in genügender Uebereinstimmung,
wie aus nachstehenden Kontrollaufstellungen hervorgeht :

Kerl. un 1886 49 59,944
Berlin-Swinemünde . . 1883 3928909
oma... Bo. oa

 

Schlussfehler : + 0007

 
