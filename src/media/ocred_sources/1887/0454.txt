 

40 Sur la mesure de l’intensit& absolue de la pesanteur.

Les premiers observaleurs qui ont employ& le pendule A reversion de Repsold ne se
pr&occuperent pas du support, le croyant assez ferme pour ne pas &tre Ebranl& par le mou-
vement du pendule.

Le general Baeyer et M. Peirce soupconnerent les premiers Pentrainement du sup-
port par le pendule oscillant et l’accroissement que subit, par le fait de cet entrainement, la
durde d’oscillation propre du pendule.

M. Peirce mit experimentalement le fait en Evidence. O’est & lui et & M. Gellerier
qu’est due la premiere theorie mathematique du phenomene. Ges deux savants arrivent, par
des moyens un peu differents, A la formule de correcetion connue :

non Deh,
nl IE)

Leurs analyses reposent d’ailleurs sur le m&me principe, qui est l’axiome fondamen-
tal de la theorie de l’elasticite, savoir que les pelits deplacements des molecules du support,
considere comme un solide elastique, sont proportionnels aux forces qui les sollicitent.

La constante e contenue dans la formule est ce qu’on nomme le coefficient d’elasti-

 

 

cilö du support, c’est le rapport du deplacement A V’effort m

M. Plantamour, ä Geneve et a Berlin, M. Peirce, & Hoboken, ont &tudie experimen-
talement et presque simultandment, dans les plus grands details et A l’aide de methodes de
mesure differentes, l’influence du mouvement du support sur la durde d’oscillation du pen-
dule. Nous rösumons en quelques mots les conclusions assez differentes des deux observa-
teurs. |

M. Plantamour distingue deux coefficients d’elasticite, le coeffieient staligue, obtenu
par Pexperience staligue, en mesurant le döplacement trös petit produit par un eflort connu
applique horizontalement au support au point de suspension du pendule, et le coefficient
dymamique, donne par experience dymamigque, laquelle consiste A mesurer les deplacements
du support pendant les oseillations möme, sous V’effort du pendule en mouvement, effort

IT, est la durde d’oseillation corrigee ;

T,, la durde observee;

pP, le poids du pendule;

h, la distance du centre de gravite A l’axe de Suspension ;
!, la longueur du pendule synchrone.

  
 
 
 
 
 
 
 
    
   
   
   
   
  
    
    
