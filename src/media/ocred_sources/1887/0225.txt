sen

ml

 

 

 

 

 

 

Anzahl. Mittel.

S s »
Von 56 — 58 we N Dänemark und

» 54 — 56 1 Norddeutschland,

Da 8: 12 Tnersu eben.

v5 33 — 0,% Centraldeutschland.

» 48 — 0 I6 — 3,8 Bayern, Baden

oA >20 + 0,6 Nördlicher und centraler Theil der Alpen
y NA == 46 117 2210.65 °Oberilahen:

ty Kalllalsserrn\

5 in ” 15 b it 32 | Centralitalien.

» 36 — 38 1 = 5,3 Garlhago.

10 Gruppen 117 — 35,9

Bringt man entsprechend an alle mittleren Werthe der Lothabweichung die Reduk-
tionsgrösse + 3,9 an, so ergiebt sich

Yon 506 58 + 1,9
>» 54 56 + 0,4
» DIE 54 + 2,0
» 90 — 92 + 3,5
» 438 — 50 +0,
» 46 — 48 + 45
» Ab — Ab oT
» 492 — 44 202
» AU — 49 — 4,9
» 36 — 38 — 1,4.

Der Anschluss kann als ein recht günstiger bezeichnet werden. Es liegt auch in ihm
ein wenn auch wenig kräftiger Beweis zu Gunsten des Qlarke’schen Ellipsoids, denn für das
Bessel’sche Ellipsoid ist der Anschluss weniger gut. Man hat nämlich nach Tabelle III fol-
gende mittlere Lothabweichungen :

Anzahl. Bessel. Clarke.
So 8 DD Al 3 91
so mM 13 59

 

Unterschied : + 3,5 — 1,5

Die Abweichungsdifferenzen + 3,5 und — 1,5 liegen nach entgegengesetzten Seiten,
so dass das beste Ellipsoid ein zu dem Bessel’schen und Glarke’schen mittleres sein würde.

 
