Hannoversch-Sächsische Dreieckskette.

 

INDICATION

DES POINTS.

LATITUDE.

LONGITUDE.

 

 

KPOQUE.

DIRECTEURS.

 

INSTRUMENTS.

 

 

REMARQUES,

 

 

 

 

Wurzelberg
Leipzig
Petersberg

Wilsdort.......:.

Kyffhäuser
Ettersberg
Ohmberg

Inselsberg

Meissner
Ahlsburg
Hohehagen
Gleichen

Basis (südlich) ....
Basis (nördlich) .

Brocken
Sauberg
Wohlenberg

Falkenberg
Lüss
Wilsede

Rauheberg
.. Kaiserberg

Nindorf-Meldorf...
Friedrichskoog ..

Silberberg
Neuwerk
Sievern
Langwarden
Wangeroog

Altenoythe
Cloppenburg
Windberg

Basis (östlich)

Basis (westlich)...

Berssen
Hengstberg
Queekenberg
Bentheim

Dörenberg
Soester Warte..
Winnefeld
Billstein
Moosberg
Ahlsburg
Meissner

 

 

51040’ 21”

51 20 16
5L 35 52
51 10 21
Bl 24 47
al 055
HL 27. 90:
50, 31.9

51 13 46
51 44 45
51 28.31
al 28 18
51.28 27
51.81 15
5148 2
>22 99l
5221.93
52 30 41
52 50 52
»230.9
53.10 9

54° 11’ 54”

53 55 53
53 36 12
os 917
54.155
53 43 52
Da 55. I
83 39 26
53 36 20
53 47 32
53 38 57
83 34 25
53 28 11
53.23 56
53 15 32
53 13 49
53 1 50
52 50 39
52 52 51
52 37 3
52 48 17
52 45 36
52 45 32
52 49 42
52 32 27

18 15

DI 2

10 37

44 9

53 0

23 2

45 15

44 45

13

30.21. 22
30 2 27
29 37 18
29 24 15
28 44 41
28 55 2
28 415
23 8 4A

21 31 32
27.29 26
27 25 55
27 42 28
27 36 24
27 36 40
28112
27 42 30
28 4 30
27 35 58
27 32: 4
28.037
27 36 27

 

Hannoversche Dreieckskette.

27 12 55
27 8:39
26 46 21
26 30 21
26 43 28

26 17 28
25 58 12
25315
25 16 47
25 34 13
23 843
25 48 15
25 35 44
a1

25 33

25 42
2a
2

25

25

25

25

25

 

 

 

262.55 „82

26 9.49,

 

 

 

 

Schreiber.

Schreiber.

 

 

|stitut-N. 46. °

Geodätisches I
Istitut N. 48.

 

|| Geodätisches I
|stitut N. 47.

 

|| Noch nicht publi-

| eirt. 5

||
i
|

 

 

 

| Geodätisches In-

 
