 

 

 

 

 

 

 

 

Annexe No Ile.
BERICHT

aber die ın den letzten Jahrem ausseftührten

PENDELMESSUNGEN

VON

FE. R. HEEMERT

Il. ALLGEMEINER THEIL

Mit dem Bericht über die ausgeführten Pendelmessungen hatte die letzte Allgemeine
Gonferenz Herrn von Öppolzer betraut; nach dem jähen Tode dieses ausgezeichneten Mitgliedes
unserer Geodätischen Vereinigung wurde mir von Seiten der Permanenten Commission die

. Berichterstattung überwiesen. Soweit ich es habe aus Publicationen und durch einige brief-
liche Mittheilungen in Erfahrung bringen können, ist darnach der gegenwärtige Stand der
neueren Pendelmessungen folgender :

In Frankreich hat CGapitän Defforges 1883 mit einem neuen Repsold’schen Rever-
sionspendelapparate (kleinerer Art) die Pendellänge zu Paris beobachtet; vergl. die Angaben
in den Verhandlungen 1883 in Rom, p. 232. Sein Resultat stimmt bis auf Hundertelmilli-
meler sowohl mit dem wenige Jahre früher von Peirce erzielten, als mit den neu reducirien,
alten Messungen von Borda und Biot überein. In den Jahren 1884/86 hat Capitän Defforges
mit einem neuen Pendelapparat eigener Construction Beobachtungen auf dem Pie du Midi,
in Lyon und Dunkerque angestellt, sowie in Paris Versuche mit Villarceau’s Apparat aus-
geführt (vergl. Verhandlungen 1886 in Berlin, p. 199).

In Spanien hat Golonel Barraquer in Madrid und an einigen anderen Orten mit
einem neuen Repsold’schen Reversionspendelapparate beobachtet. Gegenwärtig ist derselbe
mit der Vorbereitung einer bezüglichen Publikation beschäftigt. Ueber die Madrider Mes-
sungen ist kurz in den Verhandlungen 1883 zu Rom, p. 288, berichtet.

PENDELMESSUNGEN. — 1

 
