 

TER EEE ELLE

 

 

 

NOTE ANNEXE
Sur un nouvel appareil de coincidences.

Vogel et apres lui Bruhns et Oppolzer ont employ&, pour l’observation des coinci-
dences, deux fentes minces, l’une fixe, l’autre mobile et port&e par le balancier de l’horloge
de comparaison. La persistance des impressions lumineuses sur la retine fait que, pendant
le temps tres court du passage de la fente mobile devant la fente fixe, l’observateur voit le
pendule immobile en un point de son oscillation et peut mesurer avec une grande pr&cision
’intervalle qui s’ecoule entre deux apparitions de la pointe du pendule sur le zero d’une
£chelle divis6e convenablement disposee.

L’ineonvenient du procede est de donner des images d&formees par la diffraction et
fortement irisdes. Il faut en outre concentrer sur la pointe du pendule et l’öchelle divisde une
quantit& de Jumiere considerable.

Nous avions modifi& profondement la methode. Nous avons conserve& la fente mobile
du balancier et supprime la fente fixe voisine. Un collimateur place entre le pendule et le
balancier donne, dans le plan de la fente mobile, une image nette et tr&s petite de la pointe
du pendule. Un microscope grossissant fortement est pointe, A travers un prisme A röflexion
totale, sur le revers de la fente mobile, qui lui d&couvre le pendule pendant le temps tres
court de son passage devant la petite image formee par le collimateur.

Le passage du pendule par la verticale est defini & l’aide d’une fente fixe placee der-
riere la pointe du pendule et tellement reglee que lorsque le pendule est vertical, sa pointe
parait dans le microscope, ä chaque &clair donne par la fente mobile, exactement encadr6e
dans la fente. O’est l’intervalle compris entre deux apparitions successives du pendule dans
cette position qui est mesur& par l’observateur a l’aide de l’horloge de comparaison. Pendant
cet intervalle de temps, le pendule a fait deux oscillations simples de plus ou de moins que
le balancier.

Dans la r£alite, le phenomene est moins simple; ce n’est pas le moment oü la
pointe du pendule parait encadr£e dans la fente fixe qui est observe. Ge moment est deduit

de l’observation de l'instant de l’apparition et de la disparition des deux filets lumineux qui

se forment entre la pointe etles deux bords de la fente. Une &tude detaillee des phenomenes
optiques produits montre que la moyenne des deux apparitions ou disparitions extremes doit
etre identique & la moyenne des deux apparitions ou disparitions moyennes. Le procede d’ob-
servation porte donc avec lui une v£rilication et un critörium de l’erreur commise. Aux
amplitudes moyennes, les deux moyennes ne different jamais de plus d’une seconde. Aux
amplitudes tres pelites (on peut se servir de l’appareil pour observer des oscillations de 2’

 
