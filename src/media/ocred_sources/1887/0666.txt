2

France et Algerie.

 

 

INDICATION

DES POINTS.

 

LATITUDE.

LONGITUDE.

 

 

EPOQUE.

 

DIRECTEURS

ET OBSERVATEURS,

INSTRUMENTS,.

 

 

 

 

Triangulation Primordiale Alg

Dar Chouachi

Hachem Daroug...

Sidi Medjahed

| Sidi Bou Ziri

Tafaraoui
Tour Combes

Nador de Tlemeen.

Filhaoussen
Ras Ashfour

 

302 2 33
35 59 28
35 41 16
36 47 33
35 30 33

35 45 29

35 26 24
35 38 6
35 16 56
35 28 40
35 949
35 5 46
34 47 43
> 051
34 33 53
3 150

 

180. 2,282
17 48 24
17 53 36
17 35 38
17 37 49

17 9 46

17. 9 40
16 57 33
16 52 14
60 1
Io 17
16 4 5
16 20 58
15 58 31
15 52 35
15 38 12

 

 

1865
1865
1865
1865
- 1867

1867

1867
1867
1868
1868
1868
1868
1868
1868
1868

Capitaine Bondivenne.

- Perkier. en azimut. n°2. ||

er
| Capitaine Berrier.

rienne. (Partie Occidentale — Suite.)

Cerele röpetiteur de

Gambey n° 3.

Cerele azimutal de |

Brunner.
| Theod.
na:

Triangulation de Liaison de la Base de Böne avec la chaine primordiale.

Aouara

Belelita

La Mafrag
Böne (Phare) .
Edough

Base de Böne.....

Terme Nord
Base de Böne
Terme Sud

36°32' 53”
36 41 0
3649 7
36 50 41
36 54 31
36 53 6
36 51 19

2514 37
25 41 42
25 20 20
25 37 26
25 26 22
>s1ls
29

25 27 55

1867

1867 _

1867
1867
1867

1867

1868
1868

Command. Versigny.

Cercle repetiteur n. 4°

Triangulation de Liaison de la Base d’Oran avec la chaine primordiale.

Tafaraoui
Tour Combes
Mangin
Santa Cruz

Terme Etoile
Base d’Oran
Terme Azercg

Melab el Kora ...

La Boudjarcah..
a Maison Carröe.
Alger (Phare)

35°45' 29”

35 26 24
35 38 6

36°39' 57”
36 83 7
36 48 33
36 47 38
36 43 22
86 47 16

17. 3.46%

17 9 40
16 57 33.
Il
16 59 39
i7 59

17.0.8

2037 122

20 54 6
20 54 45
20 41 11
20 48 29
20 44 1

1867

1867
1867
1867
1867
1867

1867

Triangulation de Liaison

1860
1859
1860
1860
1860
1860

Capitaine Boudivenne.

Capitaine Perrier.

Theodol.
bey ns.

de Gambey ||

| Memorial ü De.

pöt General de
Guss TomeX Ih

 

de Gam- |

Cercle azimutal de ||

| Brunner.
|

de la Base de Blidah avec la mer.

Capitaine

Versigny.

Theodolite n® 13.
id.
Cerecle n® 4.

Thöodolite n% 13
Cercle repet. n° 4,

 
