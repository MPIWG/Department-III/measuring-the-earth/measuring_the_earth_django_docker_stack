 

 

 

 

9

Locus perennis

diligentissime cum libella librationis, quae est in Austria et Hungaria confeeta, cum mensura
graduum meridionalium et parallelorum, quam Europeam vocanl ereetum.

MICOG.. ...

zu deutsch :
Hauptfiepunkt
des
Präcisions-Nivellement in Oesterreich-Ungarn
ausgeführt
in Verbindung mit der Europäischen Gradmessung
errichtet MDGCC.......

Sieht man von der Inschrift ab, so ist dies jene Art der Herstellung eines Haupt/fix-
punktes, wie sie am Bacher-Gebirge zwischen Maria-Rast und Faal, bereits stattfand.

Schliesslich ist noch zu bemerken, dass nach dem von der k. k. geologischen
Reichsanstalt eingeholten Gutachten, vorerst eine eingehende Recognoscirung des Gebirgs-
stockes bis auf den Kamm hinauf stattfinden muss, um die Versicherung zu gewinnen, dass
der Gebirgstheil, auf welchem die Marke errichtet werden soll, nicht etwa durch Gesteins-
klüfte etc. von dem Hauptstocke des Gebirges abgetrennt ist.

Nebst der oben beschriebenen Marke ist in der durch das Absprengen der Stufe
entstandenen Vertikal-Fläche des Urgesteines eine gewöhnliche Höhenmarke mit Messing
konus zu errichten und deren Reductions-Element auf die Urmarke genauestens zu erheben.

Der eine dieser Hauptfixpunkte sollte in der Marmaros östlich von Marmaros-Sziget,
der andere im Rothenthurm-Passe, südlich von Hermannstadt liegen.

Nachdem eine in den letzten Tagen des Monates Juni bei Marmaros Sziget vorge-

nommene Recognoscirung sofort erkennen liess, dass nur das rechte Theisz-Ufer für eine
solche Anlage in Betracht kommen kann, wenn nicht nur der vorangeführten Instruction,
sondern auch den weiteren Bedingungen entsprochen werden soll, dass der Punkt durch das
Präeisions-Nivellement leicht erreichbar sei, dass der Transport und die Aufstellung des Monu-
mentes sowie die Entfernung des abgetragenen Gesteines und Erdreiches nicht mit grossen
Schwierigkeiten verbunden sei, und dass die Baukosten nicht zu gross sein dürften; so wurden
an der Strasse von Marmaros-Sziget nach Kolomea, Trebusa 8 verschiedene Stellen aus-
gesucht und Gesteinsproben der k. k. geologischen Reichsanstalt vorgelegt, welche einen
dieser Punkte als den günstigsten bezeichnete.
In gleicher Weise wurde die Gegend des Rothenthurm Passes Anfangs August recognoseirt
und sind auch von dort Gesteinsproben an die k. k. geologische Reichsanstali eingesendet
worden. Mach Angabe dieser Anstalt wurde auch hier ein Punkt hart an der Strasse nach
Rumänien, nördlich der österreichischen Haupt Contumaz, gewählt.

An beiden Orten wurde vom Urgesteine so viel abgesprengt, dass ein genügendes
Planum für das Monument hergestellt werden konnte.

 
