 

EM

ber

nes
er;
der
l-
1en

 

 

 

29

  

ZWEITE SITZUNG

92. October 1887.

Die Sitzung wird um 2 Uhr 20 Minuten eröffnet.
Präsident: Herr General Ibanez.
Es sind anwesend:

1. Die Mitglieder der permanenten Commission. Die Ilerren : Van de Sande Bak-
huyzen, Faye, Ferrero, Foerster, Helmert, Hirsch, Zachari.

2. Die Delegirten. Die Herren : d’Avila, Bassot, Beltochi, Gornu, Defforges, v. Kal-
mär, Lorenzoni, Perrier, de Stefanis.

3. Als Eingeladene. Die Herren Bischoffsheim, Brunner, Gahours, de Goatpont, Lalle-
mand, Navarrete, Stephan.

Der Schriftführer verliest das Protokoll der ersten Sitzung ; dasselbe wird ohne Be-
merkungen angenommen. Herr Hirsch entschuldigt sich, dass er nicht die nothwendige
Zeit gehabt habe, um gleichzeitig das Protokoll auch auf deutsch zu redigiren. Indessen ist
es selbstverständlich, dass in der Publikation sämmtliche Protokolle in beiden Sprachen ge-
geben werden.

Der Präsident giebt das Wort dem Schriftführer behufs Berichterstattung über die
eingegangene Correspondenz. Herr Hirsch verliest ein Schreiben des Oberstlieutenant Henne-
quin von Brüssel, datirt vom 19. Oktober, worin derselbe seine Abwesenheit entschuldigt
und anzeigt, dass er für das Archiv der permanenten Commission zwei Exemplare des
1. Theiles des 6. Bandes der Beleischen Triangulation einsenden wird.

Ferner hat der Schriftführer von Herrn Max v. Bauernfeind in München den nach-
stehenden Brief für die permanente Commission erhalten, worin er bedauert, in München
Izurückgehalten zu sein, und die Gründe angiebt, die ihn verhindert haben, die 3. Mit-
heilung über die terrestrische Refraction, die zur Zeit vollendet ist, einzusenden.

  
