49

lichen Arbeiten, den Regierungen, Eisenbahnen, Ingenieuren, etc. mit Ungeduld erwartet
und gerade der Umstand, dass die mareographischen Mittel noch nicht genügend her
kannt sind, um das Meer wählen zu können, auf welches die absoluten oe unrbezacliin
sind, verhindert, dem allgemeinen Verlangen jetzt schon gerecht zu werden. Will nah nun
noch die verwickelten Correetionen des Herrn Bouquet de la Grye in die Rechnung einführen
so wird man jedenfalls in diesem Jahrhundert zu keinen Resultaten gelangen. Und doch ist

1: > > e : :
es von Wichtigkeit, die Geduld der Regierungen in diesem Punkte auf keine allzu harte
Probe zu stellen.

ep Hk Yan a :
en Herı Backhuysen macht noch einige Bemerkungen in Bezug auf das Protokoll. Er
wird sich mit dem Schriftführer verständigen, damit seine Ausführungen genau so wieder-
gegeben werden, wie er sie gedacht hat.

Der Schriftführer äussert sein Bedauern, Herrn Backhuysen unrichtig verstanden zu
haben, und erklärt sich bereit, alle Verbesserungen aufzunehmen, welche sein College ihm
bezeichnen wird.

Nach diesen verschiedenen Bemerkungen und Diskussionen wird das Protokoll an-
genommen.

Der Vorsitzende schlägt vor, mit dem Verlesen der Berichte der einzelnen Staaten
fortzufahren, und da Herr Ferrero im Falle ist, vielleicht schon nächstens nach Hause

zurückreisen zu müssen, ertheilt er, unter Einverständniss des Herrn Perrier, dem italieni-
schen Delegirten das Wort.

Herr Ferrero liest seinen Bericht über die im letzten Jahre in Italien ausgeführten
geodätischen Arbeiten. (Siehe Annex N° VI.)

Auf die Frage des Vorsitzenden, ob Jemand eine Bemerkung zu machen oder um
eine Auskunft zu ersuchen habe, bedauert Herr Ferrero, die Triangulationskarten zu früh
verschickt zu haben, so dass die Delegirten dieselben jetzt bei sich zu llause und nicht hier
zu ihrer Verfügung haben.

Herr Lorenzoni hätte ebenfalls eine Mittheilung zu machen. Da er jetzt abwesend ist
und nächstens nach Hause zurückkehren muss, bittet Herr Ferrero um die Erlaubniss, die-
selbe dem ständigen Sekretär zusenden zu dürfen, damit sie den Verhandlungen einver-
leıbt werde.

Der Vorsitzende dankt Herrn Ferrero und erklärt, dass seinem Wunsche nichts ent-
gegenstehe'. Er ertheilt das Wort Herrn General Perrier.

Herr Perrier liest seinen Bericht über die in Frankreich ausgeführten Arbeiten vor.
(Siehe Annex Va.)

Herr Bassot liest einen Spezialbericht über die Bestimmung der französischen Basis-
apparate. (Siehe Annex \®.)

1 Die Note des Herrn Lorenzoni ist in der folgenden Sitzung verlesen (s. S. 53).
VERHANDLUNGEN. — 7

 
