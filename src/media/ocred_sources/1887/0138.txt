 

 

N
H
l
j
I
l

 

64

rührt. Er bemerkt, dass die Verzögerung der Phasen dem Cosinus der Amplitude propor-
tional ist.

Herr Perrier sagt, dass die von Herrn Lallemand berührte Frage der Correctionen
nicht neu ist. Schon im Jahre 1873 ist sie von Wittstein und später von Helmert behandelt
worden. Herr Perrier hat eine Abhandlung des Obersten Gouill&, vor Augen, von der ein
Auszug in den « Comptes Rendus » der Akademie der Wissenschaften enthalten ist. Diese
fünfzehn Seiten starke Abhandlung beschäftigt sich mit dynamischen, orthometrischen und
barymetrischen Correetionen, namentlich vom praktischen Gesichtspunkte aus betrachtet.
Herr Perrier würde diese Note mit Vergnügen in den Verhandlungen der Erdmessung ab-
gedrukt sehen.

Herr Hirsch unterstützt das Begehren des Herrn Perrier, unter der Voraussetzung,
dass die in Rede stehende Abhandlung nicht schon anderweitig gedrukt ist.

Auf die verneinende Antwort des Herrn Perrier bringt der Vorsitzende den Antrag
zur Abstimmung. Derselbe wird von der Permanenten Commission angenommen z

Herr Ferrero hat den Eindruk bekommen, dass Herr Lallemand sich theoretisch von
der Wahrheit nicht weit entfernt haben kann; auf dem praktischen Gebiete kann nur die Er-
fahrung über das Verdienst seines neuen Apparates entscheiden und darum erlaubt er sich,
an Herrn Lallemand die Bitte zu richten, ihm einen oder mehrere Exemplare seines Medi-
maremeters zu verschaffen, damit er denselben an den Küsten Italien’s experimentell prüfen
kann.

Nachdem sich auch Herr Betocchi diesem Wunsche angeschlossen, erwiedert Herr
Lallemand, dass er denselben mit Vergnügen erfüllen werde. Der Preis dieser Instrumente
beträgt gegenwärtig 80 bis 85 Franken, wird sich aber später durch die Fabrikation im
Grosen auf 50 oder 60 Franken redueiren lassen.

Herr Bakhuysen ist froh, Herrn Lallemand von dem Anschlusse Frankreich’s an
Belgien sprechen zu hören, denn das lässt hoffen, dass die Nordsee mit dem Mittelländischen
Meere verbunden wird, vorausgesetzt, dass auch das belgische Netz an das holländische an-
geschlossen wird, denn es ist lraurig zu sagen, dass bis jetzt der Hafen von Amsterdam
noch nicht an denjenigen von Östende hat angeschlossen werden können. Er macht darum
den Vorschlag, dass durch die Vermittelung des Gentralbureau’s Herr Major Hennequin er-
sucht werden möge, das belgische Nivellementsnetz mit demjenigen der Niederlande und von
Frankreich zu verbinden.

Ilerr Lallemand unterstützt diesen Vorschlag und die Commission beschliesst, dem-
selben Folge zu geben.

Da die Diskussion über die verschiedenen Punkte des Berichtes des Herrn Lallemand
zu Ende ist, bittet Herr /firsch den Vorsitzenden um die Erlaubniss, die Mitglieder daran

a In Folge des traurigen Todes unseres Collegen Perrier, hat der Secretär die Abhandlung des Oberst
Gouille nicht erhalten. N

 

 

 

 
