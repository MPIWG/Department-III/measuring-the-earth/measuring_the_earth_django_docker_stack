 

 

 

Redukt.
auf 450

Breite.

niveau für

Länge des Sekundenpen-
dels im Meeres-

450 Breite nach

 

 

0,993 500

Faye. Bouguer.
12 13

0,993 492

Beobachter.

=>

Jahr der Beobachtung.

BEMERKUNGEN.

 

14

Albrecht

 

 

 

 

 

eo

Dichtiekeit der Erdschicht = 2, überall, nur bei Freiberg 2,69.
Beobachtet wurde auf einem 25m hohen 'Thurme, Erdschicht daher
100m dick.

ne Beobachtungen wurden im Erdgeschoss der Sternwarte ange-
stellt.

Beobachtet wurde auf einer parabolischen Bergspitze. deren Höhe
h=585m über der ebenen Terrainschicht, und deren Basisradius a=A45%
angenommen wurde.

Der Beobachtungsort befand sich in unmittelbarer Nähe des Abra-
hamschachtes süber Tage. »

Beobachtet wurde im Keller der früheren Sternwarte, welche auf
einer parabolischen Bergspitze von 48m Höhe über der ebenen Terrain-
schicht gelegen war. Der Basisradius wurde =1415 h angenommen.

Der Apparat war im Meridianzimmer der Sternwarte in der. Etage
aufgestellt.

Der Apparat befand sich im Hauptsaal des Mathematischen Salons.
Der Apparat war im Südzimmer der Sternwarte aufgestellt.

Beobachtet wurde in einem Parterrelokal der Sternwarte.

Die Beobachtungen fanden im früheren Magnetischen Haus, genau
an der von Bessel im Jahre 1835 benutzten Stelle, statt.

 

Die Diehtigkeit der Erdschicht (er 2 angenommen.

Bessel’s Platz. 1835; 0—2,

Der Apparat stand in einem Parterrezimmer der Sternwarte, in wel-
chem auch Sabine 1828 beobachtet hatte; 0=1,8.

Bessel’s Platz 1826/27. 0 =2 angenommen.

 

 

 

 

 

 

 

 

 

 

 

5 550 545 » 1870
— 535 617 510 » 1869
54 587 539 » 1871
— 549 553 524 » 1869
548 169 A » 1869
559 546 535 » 1870
—_ 578 600 590 » 1869
— 652 556 556 » 1870
—_ 683 |f0,993 552) [0,993 549] » 1869
0.993 605 | 0,993 603 Neumayer 1863
[0,933 5521| [0.993 549] C. F. W. Peters | 1869
576 574 » 1869
— 875 577 575 » 1870
(+ 460 || 0,993 555 | 0,993 541 Farquhar 1880
+ 416 643 567 Peirce 1879
It a15 555 514 1879
390 582 581 s 1877
259 588 582 » 1876
— 589 518 57% » 1876
— 683 553 549 » 1876
Ei 420 || 0,933 579 | 0,993 502 Barraquer 1883
+25 612 635|| Pisati u. Pueei | 1882
I — 288 571 523 . Orft 1877
| 352 574 568 Defforges 1883
— 965 602 585 Bredichin

 

Bei Nr. 51-53 wurde 0 = 2,8; bei Nr. 54-57 = 2 angenommen.

Oestlich von den Alleghany Mts., ziemlich eben. Im Keller der Fak-
torei von Mr. Farquhar, Duke street.

In den Allesh. Mountains, Cambria County. Im Keller des Hauses
von Mrs. Frances S. Me Donald, Centre street.

Wenige Meter entfernt steiler Abfall in’s Ohio-Thal, ein Erosions-
thal; genaue Berücksichtigung vorläufig unmöglich.

Im Keller von Stevens Institute of Technology.
Im Observatoire, Meridianhalle, unweit der Stellen früherer Mes-

sungen.
Im Observatory.

Im grossen Maassvergleichungsraum des Gebäudes der Normalaich-

 

ungskommission, W enige ] Meter von dem Platze älterer Messungen.

Die Dichtigkeit 0 der Erdschicht ist im Allgemeinen gleich 2,8 ange-
nommen, halb so gross als die mittlere Erddichte 5,6; bei München ist

indessen mit v. Orff = 245 gesetzt und bei Paris Ö —2 angenommen.

 

 

 

 
