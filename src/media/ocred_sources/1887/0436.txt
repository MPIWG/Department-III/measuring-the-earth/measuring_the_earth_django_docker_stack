 

 

 

6

Ebenso hat der «Service geographique frangais » seine zwei Basisapparate nach
breteuil geschickt, nämlich :

1. Den Brunner’schen bimetallischen Platinmaassstab.

2. Den monometallischen Eisenmaassstab von Brunner.

Herr Commandant Bassot wird in seinem Berichte über die in Frankreich ausge-
führten Arbeiten einzelne Thatsachen besprechen, welche die Versammlung interessiren
werden. In diesem Berichte beschränke ich mich darauf, zu erwähnen, dass die neuen fran-
zösischen Messstangen ınit dem internationalen Maassstab verglichen worden sind und dass
man in Breteuil auch ihre Ausdehnungscoefficienten nach den vervollkommneten Methoden
bestimmt hat, welche in unserem internationalen Institute im Gebrauch sind.

Es ist wünschenswerth, dass dieses Beispiel von allen Nationen, welche der Erd-
messung angehören, befolgt werde und dass die Resultate dieser neuen Bestimmungen publi-
cirt werden, damit wir sie in dem nächsten dreijährigen Bericht der Erdmessung vorlegen
und die Resultate, zu denen sie führen werden, anführen können.

Gez.: (@l PERRIER.

 

 

 

 

 
