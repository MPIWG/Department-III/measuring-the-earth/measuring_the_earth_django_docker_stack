 

S

 

 

25

3. De l’Institut g&ographique militaire imp6rial royal de Vienne : Communications,
vol. VI. 73 exemplaires.

A. De M. le general Ferrero ä Florence : Processo verbale della sedute della
Uommissione geodetica Italiana tenute in Milano, Settembre 1886. (Procs-verbal de la ses-
sion de la Commission g&odösique italienne tenue A Milan en Septembre 1886.) 100 exem-
plaires.

5. De M. le lieutenant-colonel Haffner, ä& Christiania : Travaux geodesiques, V°,
fascieule. 125 exemplaires.

6. Du meme : Vandstandsobservationer, IV fascicule. 125 exemplaires.

7. Du secretaire perpetuel de la Commission permanente, M. le Dr Hirsch, de Neu-
chätel : Comptes rendus de la huitieme Conference generale tenue A Berlin en 1886. 554
exemplaires.

8. De M. le general Ibanez, de Madrid : Memorias del Instituto Geografico y Esta-
destico, tomo VI. 68 exemplaires.

9. De M. le general Perrier, de Paris : M&morial du Depöt de la guerre, tome XII,
premiere partie. 75 exemplaires.

10. Du meme : Jonetion geodesique et astronomique de l’Algerie avec l’Espagne.
104 exemplaires.

11. De M. le Dr Schram, de Vienne : Necrologie de Th. von Öppolzer. 70 exemplaires.

12. De M. le göneral Ferrero, de Florence : Determinazione della differenza di longi-

tudine fra Napoli et Roma. (De la difference de longitude entre Florence et Naples.) 100 exem-
plaires.

PROCES-VERBAUX — 4

 
