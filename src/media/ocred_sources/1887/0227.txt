 

 

für

 

‘
®
&
|
:

 

ee

= +62,

wobei der Einfluss der Glieder mit%X% und‘ vernachlässigt ist. Derselbe dürfte aber kaum
G B
1/," erreichen (vergl. $ 5).
Dieser Betrag der Lothabweichung stimmt recht gut mit dem vorher erhaltenen
+ 379 überein, was sehr interessant ist und als ein Zeichen dafür aufgefasst werden kann,
dass das Geoid in den westlichen und den mittleren Meridianen Europa’s im Grossen und
Ganzen demselben Rotationsellipsoid sich anpasst.

Ich habe zur Bestimmung der Lothabweichung von Rauenberg das Mittel der Werthe
3.9 und 6,2 benutzt und also

£ u
S

—ı td

Rauenbereg

geselzt. Mit diesem Werthe ist in Tabelle II eine vierte Spalte von Lothabweichungen be-
rechnet und zwar mit ausreichender Genaniekeit einfach durch Addition von 5” zu den Zah-
len der dritten.

Die Tabelle III ist, soweit die 4. Spalte der Lothabweichungen in Betracht kommt,
hiermit im Wesentlichen auf das System der für’s Glarke’sche Ellipsoid von 1880 im franzö-
sisch-englischen Meridianbogen berechneten Lothabweichungen reducirt.

Betrachtet man nun diese Lothabweichungen in Breite (4. Spalte) im Grossen und
Ganzen, so zeigen sich zunächst in Dänemark und an der deutschen Ost- und Nordseeküste
(Nr. 1 bis 20 der Tabelle III) fast überall Werthe, die nicht erheblich von null verschieden
sind, entsprechend dem ebenen Charakter dieser Landstriche; nur an einigen wenigen Orten
kommen Werthe bis zu 4” vor, die auf unterirdische Unregelmässigkeiten der Massen-
lagerung der Erdkruste lokalen Charakters hinweisen (vergl. in dieser Hinsicht besonders
Nr. 1, Skagen, Nr. 3, Lysabbel, Nr. 11, Königsberg, und Nr. 12, Trunz). Dagegen zeigt sich
ein allgemeines Vorherrschen des positiven Vorzeichens in den Lothabweichungen in Breite
für Ostdeutschland und überhaupt in den Gegenden Deutschlands von mittlerer Breite, vergl.
Nr. 21-31, 35-41, 42-56, 57-61 und 81. Zur Erklärung dieser Thatsache können höchstens
vereinzelt die Formen der Erdoberfläche mit Erfolg herangezogen werden; u. a. sind jeden-
falls auf der Nordseite des Harzes die Lothabweichungen grösser als eine Attraktionsberech-
nung auf Grund der Geländeformen sie ergeben kann. Wir haben es also hier mit einer ge-
nerellen Anomalie des Geoids in Deutschland zu thun, die auf anormale unterirdische Massen-
lagerungsverhältnisse generellen Charakters hinweist.

 
