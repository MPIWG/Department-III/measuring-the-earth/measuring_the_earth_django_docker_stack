Triangulation de Pologne. Jonetion a Tarnowitz. |

— 108 —

 

INDICATION

DES POINTS.

LATITUDE.

LONGITUDE.

EPOQUE.

DIRECTEURS

ET OBSERVATEURS.

INSTRUMENTS,

 

 

 

 

 

Poremba-Gurna ...
Malechitze

Roclaki
Podlessitze

Markowitze

Lubschau
Trockenberg

-

Spectal et Razio-

Sosnowitze........
Boretschno
Gromblin
Donitschewo

Bogumilow
Wincentow

- Jaworzno
-Medzno..
‚Jatzinska
Wrentschitze
Rodostzow .
. Grabowka

Triangulation de Pologne. Entre Varsovie et le cöt6 Zbendowitze-Abramow.
Dejä mentionne.

Milosna
Tschlekowka
Gura.n.....
Stara-Kuta

Stanislawowitze...

Braeziny..........

Zawada .

Baranow ..

Zbendowitze et A-
bramow.........

50°.21' 23°
50 21 49
50.3257
50 24 31
50 34 25
021.9
50 43 50
|-50 33. 36
50 41 26
50 36 46
50 24 42

 

3750'19"
87 27 12
37 28 26
37 12.42
37 11 50
36.45 6
36 57 5
36 49 54
36 44 22
36 39 17
36 32 32

 

} Dejä mentionnes.

59 37! 97"
52 28 15
52 28 13
52 17 38
52 16 46

59: 7 44
52 419
58.16
53.43

45 19

Al 11

32 39

33 1

39 98

24 37
20 13

13 49
9.43

20 12

9 48
118

50 57 51
50 51 53
50 50 24
50 55 28
50 49 53
50.48 35

 

590 g' g"
ol 29 15
51 55 56
51 48 33
51 45 38
51 34 8
ol 39 10
51 27 34
5l 32 12

| D6j& mentionnes.

 

36° 11’ 44"
36 32 36
36.9 56
36: 17, 59
36 0 44

35 52 24
36 812
35 54 11
36 614
35 59° 3
36 16 15
36 5 7
30.25... 6
36 42 8
36 48 48
36 29 45
36 43 24
‚36 27 16
36 15 24
36 10 1
36 17 48
36 37 40
36 23 28
36 37 38
36 49 40
86 43 7
3651 4

 

 

 

39, 3.10.
38 52 52
39 10 12
38 57 0
a9 17 19
84 9.50
39 28.6
39 22 18
39 47 25

 

 

 

1845-53.

1845-53.

 

 

 

l

r

enera

Lieut.-G

Lieut.-General de Tenner.

Lieut.-General
de Tenner.

 

 

 

Triangulation de Pologne. Entre Thorn et Tarnowitz.

Zn.

sodolites de 1

2. Th

2 Theodolites de 12 p.

2 Theodozites de 12 p.

ı Prusse.

I Onchion avec

id.

 

 

 

 

 

 

 
