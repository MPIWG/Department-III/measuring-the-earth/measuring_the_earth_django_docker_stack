 

Hi:

     
   

FB

O.DIC

Diesen neuen Basismessungen müssen die endgültigen Berechnungen der vor 1880
gemessenen Basen hinzugefügt werden. Unser holländischer College Herr Oudemans hat von

0.9028
ATZE

DD

ul neuem die Länge der Harlemer Basis berechnet, welche in den Jahren 1868 und 69 von den
TAN { & )
N Herren Stamkart und Yvan Hees gemessen wurde. Durch sehr gut geleitete Vergleichsopera-

tionen zwischen den zwei Messstangen seines Repsold’schen Basisapparates und dem Meter
X N» 27 aus Platin Iridium, welcher selbst direkt von den Herren Stamkart, Boscha und
Oudemans mit dem Meter des Archives verglichen war, hat Oudemans für die Länge dieser
Basis erhalten

5971,740 + 0.004

welcher Werth um 0088 vom früher angenommenen Werthe abweicht.

Ebenso sind die schweizerischen Basen einer neuen Rechnung unterworfen worden.
Die Bestimmung der spanischen eisernen Messstange, welche zu diesen Messungen gedient
halte, ist von neuem in Breteuil vorgenommen worden mittelst einer Vergleichung mit dem
Normalmaassstabe N° 2 des geodätischen Comparators, sowie mit dem provisorischen
Meter — Prototyp I, aus Platin — Iridium, welcher vorher mit dem Meter der Archive
Frankreichs verglichen worden war. Man hat in Breteuil ebenfalls den Ausdehnungscoeffi-
cienten dieser spanischen Messstange bestimmt und gefunden, dass derselbe sich erheblich
verändert hatte infolge des Transportes des Apparates per Eilzug von Madrid nach Aarberg
im Jahre 1880.

Man erhielt für die spanische Messstange die Werthe :

I, — lo (1 + 0,000011426 1 + 0,00000000815 £)
+ 0,000000015 ++ 0,00000000040

m u u
ou >30

Die Herren Hirsch und Dumur, welche die Messungen und Rechnungen geleitet ha-
ben, erhielten für die drei schweizerischen Basen die folgenden Zahlen :

 

Basis von Aarberg 200,211 = 0,9
» Weinfelden 2540,335 = 1,3
» Bellinzone 3200,408 = 1,3

welche Zahlen mit den vorläufig mit Hülfe der von General Ibanez gegebenen Grössen für
die Länge und den Ausdehnungscoefficienten der Messstange berechneten die nachstehenden
Differenzen ergeben :

 

 

 
