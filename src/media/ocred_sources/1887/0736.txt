— 102 —

Russie.

 

INDICATION £ DIRECTEURS
LATITUDE. |LONGITUDE.| HEPOQUE. INSTRUMENTS.
DES POINTS. ET OBSERVATEURS,.

 

 

 

 

Triangulation de Bessarabie.

Staro-Nekrassowka | 45°20' 2”| 46° 35' 37” || Publiesdans eL’are
Ismadl.. 45 20 24 | 46 28 40 da Meridien entre,
-.| An 99.90 46 25 39 Ke Danuhe a5 fu Mare
Saphianowka 45 24 36 | 46 32 25 || vr. Sr
Kairaklia 45 28 47 | 46 26 32  ||tersbonrg, 1860. _
Katlabueh-Suchoi. .| 45 29 37 34 49 iR
Taschbunar Il.....| 45 33 59 ; 30 I
Katlabuch 45 36 47 | 46 35 |
Karakurt 45 31. 56 283 : |
 Pandanlia 45 47 0 | 46 34 45 |
45 47 42 18 59 |
Taraklia An 55 21,46 25 29
Kamboli 15845 A
Banrtschi 16 7 614022
Malojaroslawetz-
Kaja 46 38
Kulmskaja 46 41
' Baschkalia : 46 28
Nesselrode 46 46
Nikolaiewska A A
Mowo-Kanschani... A
Dschamana 46 52
£ 46 32
46 1923:
46 44 12
47 18 41
Peressctschino..... 46 27 23
Zigancschti 46 12 48
Bologan 46 29
Sagaikani 46 11
Rospopeni 46 18
T'schutuleschti..... 45 59
410, 9
45 30
21 52,
A 23 Ur
As A al
Au 1822
48 19 An 32 al
48 14 4459 5
48 22 26 | 45 10 46
Gwosdantzi 4825 A844 5843
Britschani 48 20 42 | 44 47 10

> ee nn

rtel de 12.

u

Theöodolite terrestre d’F

Lieut.-General de Tenner.

Instrument Universel d’Ertel de 13 p.

 

 

 

 

 

 

 

 

 

 

 

Triangulation de Volhinie et de Podolie.

u danın! | || Publiesdansel‘

Britschani Dejä mentionnes. | da Moral
Woltschenetz ae 48028 921 44235. 02) Ä
Sagorjane......... 48 44 29 | ALAT 6 |
Ssupruhkowzi..... 2845 93 AU DOT AT
Karatschkowzi..... 48 53 47 | 44 19 46 |
Hanowka 48 55 43 | 44 31 28 |
Tschernowody 2535 4175
Baranowka Ay 8 53 | 44 38 95
Jeltschin . 49 19 46 | 44 20 50
49929 4A |44 8598

%
de

3
>
z

| Struve. \

4,3P.
.Arepe

titeur de
jet.de lOp.

pe
Vr6

5
<

Lieut.-General
de Tenner
od.astron

7
&
>

d’Ertel de 8 p.
Inst. Univ. d’Ertel

Troughton de 1
13.2.
Theod.

The

 

 

Cercle ri

 

 
