 

 

8

 

zwar jeder 6mal, gemessen. In gleicher Weise ist die Richtung nach dem Zwischenpunkt
Wüstenwolde an zwei Netzrichtungen angeschlossen worden.

Jede der Rangklassen a), b), c) ist für sich auf der Station ausgeglichen.

Eine derartige Anordnung gewährt gegenüber dem Streben, alle auf einer Station
vorhandenen Richtungen möglichst zusammenhängend zu beobachten, den Vortheil, dass die
Beobachtungen niederen Ranges in den Zwischenzeiten, wo die Luftbeschaffenheit Beobach-
tungen höheren Ranges zu machen ohnehin verbietet, ausgeführt werden können, indem sie
wegen der kleineren Entfernungen nicht nur leichter gelingen, sondern auch bei etwas weni-
ger günstigen Umständen angestellt werden dürfen ; denn es liegt auf der Hand, dass Netz-
beobachtungen, deren Fehler auf das Gebiet des Netzes beschränkt bleiben, nicht so genau
zu sein brauchen, wie Kettenbeobachtungen, deren Fehler sich über das ganze Dreiecks-
system, soweit es noch nicht endgültig feststeht, fortpflanzen, und dass ein ähnliches Ver-
hältniss zwischen den Netz- und Zwischenpunktsbeobachtungen besteht.

Bei unserer Anordnung hängt daher die zur Erledigung einer Station erforderliche
Zeit allein von der daselbst vorhandenen höchsten Rangklasse ab, dergestalt, dass die Beob-
achtungen niederen Ranges dabei überhaupt nicht mitsprechen. Wollte man dagegen alle
Richtungen zusammenhängend beobachten, so würde man genöthigt sein, alle Beobachtungen
mit der für die höchste Rangklasse erforderlichen Genauigkeit auszuführen. Während somit
eine zweckmässige Gliederung der Beobachtungen Arbeitsersparung bedeutet, nöthigt der
Mangel einer solchen zur Arbeitsvergeudung.

Noch wichtiger für die Oekonomie der Arbeit ist die Auswahl der zu beobachtenden
Richtungen in einem Netz unter allen vorhandenen. Abgesehen davon, dass diagonale und
transversale Richtungen gegenüber denjenigen, welche den besten Rechnungsweg von Drei-
eck zu Dreieck vermitteln, einen geringen Einfluss auf die Punktbestimmung, und oft sogar
nur den Werth einer rohen Kontrolle haben, kommt in Betracht, dass ihre Beobachtung
schwerer gelingt, weil sie die längeren sind. Der Beobachter wird also, falls sie nicht ausge-
schlossen oder für sich beobachtet werden, genöthigt, gerade auf diejenigen Beobachtungen,
auf die es am wenigsten ankommt, die meiste Zeit — und oft eine kaum erschwingliche —
zu verwenden, oder sich bei ihnen mit einer geringeren Genauigkeit zu begnügen ; im letz-
terem Falle muss er sich aber gefallen lassen, dass sie die übrigen Beobachtungen verderben,
da in der Ausgleichung die einen von den anderen sich nicht trennen lassen und die Zuthei-
lung verschiedener Gewichte erhebliche Bedenken hat. Selbst der Einwand, dass die nöthige
Zeit vorhanden war, schützt den Beobachter nicht vor dem Vorwurf, dass er in der gleichen
Zeit Besseres hätte leisten können.

Wenn somit die Sucht, möglichst viele von den in einem Netz vorhandenen Sichten
zusammenhängend zu beobachten, schon die Messung unverhältnissmässig erschwert, so
steigert sie die Schwierigkeiten der Berechnung nicht minder, unter Umständen sogar bis
zur Undurchführbarkeit. Aber selbst wenn eine solche mühevolle Arbeit glücklich durch-
geführt ist, so erscheint das fertig vorliegende Werk, lediglich als solches ohne Rücksicht
auf die darauf verwendete Arbeit beurtheilt, durch die Schwierigkeiten seiner rechnerischen
Handhabung in seinem Werthe ausserordentlich beeinträchtigt ; es ist ein noli me tangere,

 

 
