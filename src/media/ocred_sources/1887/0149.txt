 

 

 

Annexe No TI“,
BAPPORT

SUR LES

DRWTATIONS DE LA VERTICALE

Pr ı DIEEMERT

TABLE DES MATIERES

Introduction.
I. Recherches sur de vastes territoires pour lesquels la forme de l’ellipsoide de compa-
raison a une certaine influence.

Io

1. L’ellipsoide terrestre general.
2. Deviations de la verticale, suivant la latitude, dans la Grande-Bretagne et
UIrlande,

ro

Io

3. Deviations de la verticale, suivant la latitude, en France et en Belgique.

8

4. Deviations de la verticale, suivant la latitude, dans le Danemark, l’Allemagne,
la Suisse et I’Italie.

on

5. Quelques deviations de la verticale, dans le sens de la longitude, pour l’Eu-
rope centrale et occidentale.

due

6. Deviations de la verticale au Caucase et dans la Crimee.

9

x 7. Deviations de la verticale dans les Btats-Unis de !’Amerique du Nord.

Il. Recherches sur lesquelles la forme de lellipsorde de comparaison est sans influence.
l. La contree de Moscou (Schweizer).

AD

oO

2. Deux petits territoires des Alpes (Pechmann).
3. Le 49° parallele dans ’Amerique du Nord (Greene).
%. La contree de Leipzig (Bruhns-Nagel).

5. Le Harz (Boeyer-Andrxe).

a TO OD

(Avec 3 cartes.)

RAPPORT HELM. — I

 
