Tabelle 1.

 

 

 

 

Nummer.

NAME
der

STATION.

 

Geogr.

Breite.

| Geogr. Länge

von
Greenwich

ab.

Meeres-

höhe

in

Metern.

 

 

 

|
|
|

 

Dicke der Erdschicht
über der
Station in Metern.

Beobachtete
Schwingungs-

zeit

Daraus
berechnete

Länge

 

des Sekundenpendels

 

in Sekunden.

in Metern.

 

 

 

7

 

 

 

8

=> | Warhrscheinlicher Fehler

!

 

 

 

 

 

Zwinger.
Schlossberg.

Kapellenberg.

Mundloch.
Oben Mitte.
Oben Ende.

Pulverthurm.

Krusnä hora.

Mundloch.
Stollen-Mitte.
Stollen-Ende.

 

Uj major.
Abtei-Keller.
Szabo-Keller.

Plateau-Mitte.

Plateau-Rand.

Wien. Türkenschanze.

Wien, geogr, Institut.
Schöeckl. bei Graz.

Dorf Pfelders in Pyrol.

Hochpunkt Sandbüchel,
in Tyrol.

7

9 98 AA

39 10
38 10

46 /

H

° I "
25 35 54
35.59

35 A

Horizontaldi-
stanz vom
Mundloch :

590 3

180
1300
17.00

(vom Pulver-
thurm 200m)

Horizontaldi-
stanz vom
Mundloch:

390
780

 

 

Die Stationen
liegen ange-
nähert von Nord
nach Süd grup-
pirt.

 

It

 

36

 

 

100

 

 

 

| 0,500 2650

633

235

u

 

. Saghegy in Ungarn,
ı 0,500

9787
824
923

353

867
0,500 9673

9745
0642

1239

I

1996

 

 

I. Kronstadt in Sieha
[0,993 4282]
4350

3559

Kruäinä hora i
10,993 9049)

Kruäna hora in Böhm
0,993 9049]

8932
8872

 

Mai 1834 (in

[0,993 5655]
5509
5120

4995

FO
5339

 

V.
10,993

Relative
7616]

I IDDIT
3 1332
DIE

Ss

2 8402

 

 
