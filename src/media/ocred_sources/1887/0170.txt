   
  
 
 
 
 
 
 
 
 
 
 
  
  
   
   
       
 
 

 

92

 

pas Pellipsoide employ& dans le tableau III ä la seconde place et qui correspond A peu de
chose pres au systeme 4 du $ 1, dont la difference de deviation dans le sens negalif est encore
de 1” environ superieure que pour l’ellipsoide de Clarke.

Quant A la quantit& de reduction -}- 3’9 calculde plus haut, elle represente essen-
tiellement la valeur de la deviation de la verticale en latitude pour Rauenberg, exigee par le
systöme entier de la 3° rubrique des deviations de la verticale dans le tableau IF. Je neme
suis cependant pas arret& ä ce rösultat, mais j’ai constate en outre Ja m&me deviation de la
verticale par rapport au systöme de l’arc möridien franco-anglais. A la suite des caleuls faits
par P’Institut g&odesique et joints au fasc. I. mentionne & plusieurs reprises, il s’ensuivait par
le rattachement de Rauenberg avec Greenwich et Paris les deux syst&mes d’equations suivanls,
dans lesquels & designe la deviation du veritable zenith par rapport au zenith ellipsoidal vers

le Nord, A la deviation en longitude vers l’Est (de sorte que & — latitude astron. — ge&odes.
*% — longitude Est astron. — geodes.)

 

 

 

 

ee a. i
G R R =
hier oe re = 995.
G R R Z
et
Er rg ee er rang 2 |
P R R a

ee a ee ar Fe ag
P Br R Ü

Dans ce qui precede, les indices G, P et R se rapportent ä Greenwich, Paris et
Rauenberg.

    
      
   

an Hu da
En supprimant les termes de ae da, on trouve

une pang & = 06.9564. 0,988 8 „= 01132,
R G G

      
  

       
 
 

d’autre part » — #4 14,56 4.059892 — 0,099.%

R RB

 
 

Si l’on introduit iei, d’apres le$ I, tableau I® et conformement A la latitude employse
dans le caleul pour Paris,

     
