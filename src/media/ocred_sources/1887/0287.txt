     

 

 

 

 

 

 

 

3

Einige Ergebnisse, welche sich bei Betrachtung der Tabellen darbieten, sind fol-

   

gende:

   

Lokale Anomalien der Länge L des einfachen Sekundenpendels sind von erösserem
Umfange (sowohl nach Faye’s wie Bouguer’s Reduktionsweise) angedeutet an nachstehenden
Orten.

    
   
   
   
    
    
    
     
     

Höhe

Nr. in Metern.

5 Tokio ,ın Japan. 2%: 6  Lzu klein um ca. 90 Mikron
30 Duschett im Kaukasus . su DD... » Ile »
35 Baku am Kaspischen Meer . ı > No ,
37 Mannheim in Deutschland. 125 » » » 30 _»
49 Gotha in Deutschland ab > » N 0
99 Rom 5) Eaucenos )» 0

        
 

wobei ich die Abweichungen für Faye’s Reduktionsmethode angesetzt habe.

In einigen Fällen lässt die Reduktion nach Bouguer Anomalien übrig, die bei den-
jenigen nach Faye verschwinden — auch bestehen einige umgekehrte Fälle, bei denen aber
zum Theil die Anomalie für die Faye’sche Reduktion wieder verschwindet, wenn man die
Anziehung der betreffenden Bergkuppen unterhalb der Station in Abzug bringt (was sowohl
der Ansicht von Faye und anderen über die Constitution der Erdkruste, wonach nur die ge-
nerellen Erhebungen derselben durch Dichtigkeitsdefekte kompensirt sind, wie auch meiner
Kondensationsmethode ! annähernd entspricht). Diese Fälle habe ich in der folgenden Ueber-
sicht des Verhaltens der beiden Reduktionsmethoden hervorgehoben.

Kapellenberg in Siebenbürgen (Tabelle I, Nr. 1-3) zeigt gegen den Schlossberg da-
selbst nach Faye’s Reduktion ein L um 29 Mikrons zu gross, welche nach Abzug der An-
ziehung der Bergkuppen bis auf 3* verschwinden, da alsdann die Pendellängen annähernd
0,993693 und 626 werden. Die verbleibende Differenz mit der benachbarten Station Zwinger
ist bei einem Betrage von noch nicht 20* zwar klein, aber doch grösser als bei Bouguer’s
Reduktion. Immerhin ist ihr wegen der Unsicherheit der Schätzung der Dichtigkeit unter-
halb Station Zwinger (die ganze Schicht von 573m bis zum Meeresniveau dürfte wohl kaum
aus Löss bestehen) wenig Bedeutung beizumessen.

Entschieden vortheilhaft zeigt sich die Faye’sche Reduktionsmethode, namentlich
nach weiterer Verbesserung im Sinne der Kondensation, bei den Stationen der Lokalunter-
suchung in Krusnä hora in Böhmen und am Saghegy in Ungarn (Nr. 4-8 und 12-16) sowie
bei den Stationen der Reihe V v. Sterheck’s (Nr. 17-21). Ein bemerkenswerther Kondensa-
lionseinfluss dürfte bei Nr. 19, Schöckl, und bei Nr. 20, Pfelders, vorhanden sein, beide im
Sinne einer Vermehrung der Uebereinstimmung, ersterer gleich — 33”, letzterer gleich
einem nicht ohne Weiteres angebbaren, positiven Werthe.

Bei den Stationen der Determinations with the Katers Pendulum, Tabelle Il, wo
nur geringe Höhen auftreten, ist auch kein bemerkenswerther Unterschied im Erfolg der

    
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   

rleimert, I, p. 129, 1727 ete.
