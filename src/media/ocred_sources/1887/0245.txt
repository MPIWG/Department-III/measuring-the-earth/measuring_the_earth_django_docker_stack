 

VE. Lothabweichungen in Breite für die östliche Küstenregion.

 

 

 

     

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

5 | | = Lothabweich.
Ss | Beobachtete & = Ne
= | z mE A.-Ur.
ZN | Name der Station. Ice eng ® 5 Bemerkungen.
| ä | | Breite. 3 = Clarke
| | = | 21806.
Be Sin ea
| ar ; Minus, j ‘ Tonne
| | I Calais. 15 11 9.41.67 17 + 2,04 3,7|\
ı | 3 | Cooper. 14 59 12,68|67 28 |— 2,5. — 0,91]
j | 8 | Humpback. 14 51 A 68 7 1— 32 SE 3,6 In der Nähe der Küste.
u | 1 | Bangor. 14 48 12,86!68 47 |— 5,0|— 3,8
u | 5 Harris. 14 3) 54,84169 9 |— 1,7) 0,0
| = 6 | Farmington. , |44 40 19,55|70 9 |— 4,9|— 3,2| _Oestlich der Blue Mts.
| ad Howard. 144 37 48,67[67 24 +0,54 2,1] an der Küste.
! 3 8 Mount Desert. |A4 21 4.62 68 14 I— al RT 0,6 Inselstation, kleiner Bere.
! | = 9| Ragged. 2) 1335) 69 9 I— 3,9 = a An der Küste.
F = 10 Sebattis. AR ES, 37,60 u Ba 1. SER 0,2 Wahrscheinlich kleiner Berg.
| E 11 | Mount Pleasant. |14 1 36,48|70 49 |— 1,7)— 0,2 Kleiner Berg südöstl. der White Mts.
| =, 12 Cape Small. |48 46 43,48169 51 |— 1,1|-F 0,3] Küste.
@ ı18 Mount
Independence. 1a 2534.23 0 19 > 0,7 + 0% Kleiner Bere an der Küste.
j 14 Gunstock. a al 31 1 222 — 0,5 + 0,8 Im Innern.
h 15 Agamenticus. oe 24.98 70 42 |— 0,8 + 0,4 An der Rüste, wahrscheinlich Hügel.
16 Unkonoonue. "2 58 59.355171 35 — 1.6 — 05 Im Innern.
eo 17 | Isles of Shoals. |'9 59 12,88|70 37 |— 3,1 — 2,0 Kleine Insel.
18 T['hompson. 23 38.2870 AA —g 4,6 — 3.6 Hügel an der Küste.
ö 19 Wachusett. 1 29 17,08 Al De 4,6 Z = Berg von 900m Höhe, im Innern.
| & | 20 | Havard Observ.,22 22 48,05[71 8 | 6,3 — 5,4
5 Cambridge. / An der Küste.
ı & | 21 | Cloverden Obs., |12 22 40,97|71 7 |— 6.2\— 5,3|)
Ra Cambridge.
| = 22 Mount Tom. 2 39 |— 3,1 — 2 Kleiner Berg im Innern.
j | 23 Manomet. A 58) 3.3 70 35 |\— A — 3,% Kleiner Hügel an der Küste.
| 24 Nantucket. Ze 3E7 14.06 70 6 | — Sl — 2 Auf der Nordseite einer Insel.
IE 2 Sandfort. 41 27 40,08|72 57 \— 3.3 — 2,1 Im Innern, hügelig.
| = |2%6 | West Hills. |40 48 49,96173 5 | 5,6— 5.2] Auf Long Islands, eben.
| = | 2% | Rutherfurd’s
| es Öle non von. a0 Ba 18a
| 5 Bern Wan anıtıloia. —
ms 29 Mount Rose. AU >» 5,44 Ti AS —+ RS + 20 Im Innern, in der Nähe des Delaware.
3 30 ı Gummere's Obs. | 40.5.44551,6 + 4,814 5.1 Die Länge war nicht zu ermitteln.
| | +? 31 Yard 139 58 29,39|75 23 + 4.5 + 4,6 Im ebenem Terrain.
| =, | 32 [old High Sehool| _
i 18 Obs., Philadelphia. |39 57 7,5 |75 10 |4+ 1,4 1,6| Stadt Philadelphia.
I

LOTHABWEICHUNGEN — 6

 

 

 
