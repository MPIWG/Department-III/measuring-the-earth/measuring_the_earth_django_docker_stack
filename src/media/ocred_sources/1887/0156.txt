T’. Mesures de degres de latitude franco-anglaise et russe.

 

-i

 

Noms des stations.

 

 

 

. Ecosse.

Angleterre.

Saxavord
North Rona
Great Stirling .
Kellie Law
Durham
CGlifton .
Arbury
Greenwich.

Dunkerque
Dunnose

Pantheon

Carcassonne

Barcelone .
Montjouy

Formentera

Latitude
geographique
observee.

O3 al,
60 49 37,21
59, :2.45.19
57 27.4942
56 14 53.60
446 6.20
327: 29.30
3 13 26,59
1 28 38.30
8.41 )
(8,9) \

37 654

5
5
5

SB

Jo

50
19 31.30)
= (53,3 |
22 47,90
21 44,96

39

(53.99)

Obsersation
ler
Clarke 1880

| Obs.-caleul.

(QJualrieme
systeme

d’elements.

 

17,98 °
(17,09) |

53,17 \

+ E53 \
(+ 2.0) \
12.0

rg

—0,8
— 4,1

 

 

Fuglenaes .
Stuor-oivi .
Tornea.
Kilpi-maki.
Hogland
Dorpat.
Jacobstadt..
Nemesch

Belin
Kremenetz. .
Ssuprunkowzi
Wodolui

Staro Nekrassowka

 

40 11,23
4) 58,40
) 49 41,57
38.825
5.9.84
23 4S0
4,97
4,16
274216
49,95
3,0%
95,98
9,9%

 

— 1.7

+0,

— 4,7

 

1,0
es
I
10
hr
4.05
sba,h
u
08

+ 3,3

 

(

Gr 2,0)\ |
+ 1,2 ) |
(+0,2)\ |

—8.1) |
Zi

 

 

 

 

 

 

 

 
