 

16

Commission gehörigen Messingstab, mit H bezeichnet, und der mir durch den Director der-
selben, Herrn Ministerialrath Friedrich Arzberger zu diesem Zwecke geliehen wurde, bei der
Temperatur des schmelzenden Schnees verglichen. Aus sieben Vergleichs-Resultaten hat sich
die Länge des Stabes M, bei 0°G mit 100,002135 mm = 0,00249 mm ergeben. Bedenkt
ınan, dass der dem Vereleichsnormale H anhaftende wahrscheinliche Fehler = 0,00174 mm
ist, so kann ınan das für M. aus den Vergleichungen mit P erhaltene Resultat ganz befriedi-
gend nennen.

Bestimmung der Ausdehnung des Stabes.

Wenngleich die mir zu Gebote stehenden Hilfsmittel eine directe Bestimmung der
Ausdehnung (des Stabes M. nicht möglich machten, so glaube ich doch, dass das auf indirek-
tem Wege erhaltene Resultat der Ausdehnung ein sicheres sein wird.

Aus 17 Vergleichungen von M. mit P zwischen den Temperaturen 6.61 C und
19.85 GC ist die Ausdehnung von M, für 1° C mit 0,018691 und aus 26 Vereleichungen mit
dem Stabe H zwischen 10.34 C und 1985 C mit 0,018861 gefunden worden ; hieraus folgt
im Mittel mit Rücksicht auf die Zahl der Vergleichungen die Ausdehnung des Stabes M. für
Ee& >

— 0,018794 = 0,000031

Länge des Stabes M, bei der Temperatur i° C:

M. = 1000,01895 + 0,018794 t.
Bestimmung der Theilungsfehler des Stabes M..

Anfänglich war nur die Bestimmung der Theilungsfehler der Decimeterstriche in
Aussicht genommen ; ich dehnte aber später diese Untersuchung auf die sämmtlichen Centi-
melerstriche aus.

Bei dieser Untersuchung befolgte ich das Prineip von Hansen, hielt mich aber an
den für die praktische Ausführung geeigneteren Weg des D' 0. J. Broch, wie er denselben
für das « Bureau international des poids et mesures » vorgezeichnet hat Das Ergebniss dieser
Untersuchungen ist in nachfolgender Tabelle T enthalten, wobei ich bemerke, dass die
Werte für die Theilungsfehler in Einheiten der dritten Stelle des Millimeters angesetzt

-worden sind.

 

 

nn un ns

 

 

 

 
