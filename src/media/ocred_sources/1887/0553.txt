 

 

 

 

3

toute attente et qu'il a r&ussi a obtenir une serie complete d’observations. M. Wilkitzki a ob-
serve au meme lieu ou avait te erig& V’observaloire meteorologique de M. Andreief, lors de
l’expedition met6orologique polaire internationale. (Petit Karmakul, baie de Moller, 72°23
N lat er 599° 225 B loneit.).

9. TRAVAUX DE CALCUL.
Les caleuls concernant le 52W° parallele sont poursuivis.

6. PUBLICATIONS.

Tome XLI des « Zapiski » (Memoires de la Section Topographique militaire) contient,
outre le Rapport general sur les travaux executes par le Corps des Topographes en 1886,
plusieurs monographies de nos g&odesiens.

Sous presse : Description de la triangulation de la principaute de Bulgarie, par le
general-major Lebedef (en langue russe), renferme beaucoup de faits interessants, qui
prouvent d’une maniere incontestable l’influence des massifs du Balkan et du Ryllo sur la
direction de la verticale.

 
