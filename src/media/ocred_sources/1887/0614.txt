 

x x

 

Lug

   

Autriche-Hongrie.

 

 

 

 

 

 

 

 

  

 

 

 
  

INDICATION DIRECTEURS
N°® LATITUDE. |LONGITUDE. EPOQUE. INSTRUMENTS.
DES POINTS. ET OBSERVATEURS.
1.0 wBegen. ........... 46°30' 44”| 32224 58”|| 1861-77 Vergeiner, Hartl. Starke n® 3 et 4 de
z 10 5
D:
111 | Saualpe........... 46 51 16 | 32219 2 1877 Hartl. Starke n°3 de 10p.
112. Si. beter.....::. „746 37.30 31.5929 |. 181617 id. id.
113 , Golie@...... -. > 46 29 32 | 31 43 14 | 1861-77 | Breymann, Rehm. Id.nelet5de1lOp.
114 | Gerlitzen ......... 46 41 44 | 31 34 49 | 1877-79 | Rehm, Hartl. Id.ne]et3de10p.
115 ! Eisenhut.......... 46 57 10 ı 31 35°39 | 1879-80 | Hartl. Starke n°3de 10 p.
116 | Staffberg. ...-: .... 46 44 12 | 31 6 14 1879 id. Id.
117 | Thorkofl.......... 46 42 9 | 30 44 32 1879 id. id.
118 | Kosuta............ 46 26 17 | 32 0 36 1877 Rehm. Starke n° 1 de 10p
119 | Plegas............ 46 954 | 31 46 46 1861 Breymann. Starke n°5 de 10p.
20. Radiea......:...:. 46 13 42 | 31 31 52 || 1861-84 | Merkl, Fiala. Starke n° 1 de 1Op.
; etn°1lde8p.
1a Vdine.. .>8...... Ab 8 35. | 20.54.10 1861 Breymann. Starke n°5 e 10p.
199  MrZavec...: ..:.... 45 58 44 | 31 28 18 || 1861-84 | Breymann, Lehrl. ld. ne 5 de 10p. &
n°1de8 n
23 | UVrasiea.....-..... 46 826 | 32 10 Al || 1861-77 | Vergeiner, Breymann,| Starke n° n 4 et5
as Rehm. de 10p.
124 | Grinteue.......... 46 21 28:| 3212 5 || 1861-77 |. Vergeiner, Rehm. -Id.ne1 et4 de 10
125 | Gurivreh.......... 46 15.29 | 32 28 57 | 1861 Vergeiner. Starke n° 4 de 10 Be
126° Donati..........-. 46 15 48 | 33 24 31 || 1877-80 | Rehm, Corti. Starke n° 1 de 8p.
t 10p.
126a Wurmberg........ 46 28 29 | 33 28 46 1877 Hartl st k
1 ri. Star °.3 de 10
126b| Buchberg......:.... A6 26.6 | 33162 es P
1266 Base de Kranichs- N n “
ee 46 28 15, 38.091. 4 1877 Ganahl, Rehm. StarkeUniv.de1l3p.
126d| Base de Kranichs-
feld. ...2.22... 5 7
. Terme Sud 46 25 45 | 33 23 37 1877 Hartl. Starke n°3 de lOp.
VANCIeR 2.0... 46 10 35 7 , 2 z
128 | VisZ.....2......... 45 54 7 3 kr 36 18 Dodereelı Molnsr stes- ne 2 1 * 3
129 | Petrivrh.......... 3 7 > ne
sersoht 45 85 10 | 34 58 44 1878 Sterneck. Starke n° 4 de 1Öp.
rzevopolje....... 45236355 05 1878 Randhartinge tarke n® 2
I a 152022 | 3246 2 | 1878.79 | Randharinger. Rehm. | Ik me2et4de1d p
au 45 12 20 | 34 5 7 ek : no :
132, Base de Dubiea.... 4914.49 34 30 10 1879 a San en ie 108
Terme Nord = ern
132b| Base de Dubica.... 7 :
2 en 45 13 14 | 34 30 10 |ı 1878-79 id. Id.n°2et4del0p.
AO 22... 5] 9
nr 2 5 5 a a a In Lehr!. Starke n° 2 de 10p
1095| Hunka .c......,.. 65 r x Kalmar. Starke n° 2 de 10p.
136 | Ceneli | 45 86 53 | 34 25 12 | 1877-79 | Podstawski, Rehm Starken’4d 10D
epelin.n.o nu, 15 93 48 | 33 56 14 | 1877-79 | Podstaweki- £ nn en
137 | Priseka.......... 9 r odstawski, Kalmar. Id.n°2et4del0p.
138 nn 49 12316 | 39 57 51 1879 Rehm se n® s
Plesiviea.......... 45 44 16 | 33 90 4 7 Sc Starke n° 4 de 10p.
 evorae En. 5. 2 a Podstawski. Starke n° 4 de 10p.
140 | Ivanich, (Öbserv.).\ 45 4491 24 3 9 77-79 | Podstawski, Schwarz. Id.n®let4de1lOp.
141 | Oklinak........... 45 43 96 | 32 56 31 1878 1.
142 Privis a 45 98 16 39 58 50 1877. = Nahlik. Starke n°® 4 de 10 P.
wel nn 2 . 3 a 778 Kalmar. Starke n°2 de 1Op.
 icla-läsien la. 2 877-7879 Kalmar, Schwarz. Id.nelet2de1l0p.
2 ne Pal, Schwarz, Nah- | Starken°2,4et5de
45 | Treskovae......... i o s 5 15 10 p. |
146 | Tuchovie.......... 15 0 “ 3 18 58 1874-78 | Hartl, Kalmär. Id.n°2et3de10p.
a ee eo = o. n 1861-78 Breymann, Kalmär. Id.n°2etö5de 1l0p.
148 | Annaberg......... 45 46 35 | 39 21 50 1861-78 | Vergeiner, Kalmaär. Id.n°2 et 4de 10p.
oe. 2 32 24 50 1861-78 | Breymann, Kalmär. Id.n2et5d
( 45 59 34 | 39 34 18 | 1861-7 i 2eL 0 A® OP.
150 | Krimberg......... 45 55 45 | 392 81a) jeel Wr Merkl, Rehm. Starke n° 1 de 10p.
151 | Schneeberg........ 13 35 21 | 30 649 a a Rehm. Id.n®let5de1l0p.
g \

Starke n° 4 de 10p.

m

REMARQUrs,

 

Te

Italie.

 

 
