Dans la 2° partie de l’annde 1879, la triangulation algerienne a et reliee avec ]a
triangulation de l’Espagne par une operation considerable dont les resultats sont con-
signds dans un memoire special redig6 en commun par MM. le general Ibanez pour
l’Espagne et le colonel Perrier pour la France (voir aussi Tome XII du Memorial).

Comme, d’un autre cöte, cette triangulation aboutit vers V’Est aux triangles Getee
par les officiers italiens entre la Sicile et la Tunisie, on voit que le bassin occidental
de la Mediterrange peut &tre consider6 comme entoure d’une chaine continue de

triangles.

Disons, en terminant, que l’arc de meridien anglo-franco-hispano-algerien s’&tend
maintenant sans interruption depuis les iles Shetland, au nord, jusqu’ä Laghouat au sud
par une amplitude superieure & 28 degres et quil pourra un jour &tre prolonge vers
le sud, encore d’un degre jusqu’au territoire des M’zabites, ainsi qu’il resulte d’une
reconnaissance röcemment executee par le capitaine Defforges.

 
