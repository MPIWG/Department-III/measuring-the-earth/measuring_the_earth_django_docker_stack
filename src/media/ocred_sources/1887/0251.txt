 

 

 

 

 

 

II. Untersuchungen, bei denen die Form des Referenzellipsoids
ohne Einfluss ist.

$ 1. Lokale Lothabweichungen bei Moskau.

Die von Schweizer bewirkte Untersuchung des Ganges der Lothabweichung bei
Moskau ist das erste grössere derartige Unternehmen; es wurde durch 0. v. Struve’s Be-
merkung (1848) veranlasst, dass die astronomische Polhöhe der Moskauer Sternwarte mit
der geodätisch übertragenen einen starken Unterschied zeige. Die Publikation weist ent-
sprechend zuerst nach, dass der Centralpunkt der moskauischen Triangulation, der Glocken-
Ihurm Iwan Weliki des Kreml, sowohl gegen sechs, 70-150 km entfernte Punkte, dieser
Triangulation, wie gegen noch weiter entfernte Punkte (Twer, Smolensk, Petersburg) eine
Abweichung in Polhöhe von — 8 bis — 14” (astr.-geod.) besitzt.

1848 bestimmte Schweizer zunächst 6 Punkte bei Moskau und da dies wenig Auf-
schluss gab, 1853 weitere 14, jedoch waren die angewandten Instrumente von geringer
Kraft. 1858 und 59 wurde die Arbeit mit einem Repsold’schen Höhenkreis fortgesetzt und
zwar durch Bestimmung von Punkten auf Umkreisen um Iwan Weliki mit ca. 5 zu km
wachsenden Radien. In noch weitere Umgebung wurde die Arbeit 1861 geführt!. Die Karte
der Publikation enthält 96 Punkte, von denen über 80 astronomisch und geodätisch (letzteres
zum Theil doppelt) im Laufe der Untersuchungen bestimmt sind.

Das untersuchte Gebiet erstreckt sich 15 in Breite nach N, 30’ nach S, 40’ in Länge
nach E und 70’ nach W. Seizt:man nach Maassgabe der Verbindung mit entfernten Punkten
die Lothabweichung in Breite für Iwan Weliki = — 7.5, so ergiebt sich für die Nähe des
Meridians dieses Punktes folgende Reihe von Abweichungen :

 

 

 

 

 

 

 

 

 

 

| Abstand
Mittel der Punkte. Polhöhe.; | vn Lothabwei-

‚ der Null- | chung A.-G.

72,z0ne.
Kuno Iroizkoje, Puschkino ,. . .ı :7..: a9 5959. 1,4218 — 0.2
Archangelsk, Bolschoı Mytyschiy &..,. . .,. Ba 10 zı
Leonowo I, Ostankino . . 55 49,95: | 11,9 23 5,08
Iwan Weliki. Kloster Andr oniew. - Univers. Observ. ae eh uw — 71,59
Kolosenckoje, Woronzowo . . va... Sa Alla, 1.99 — 15
Nessjädy, Jassenewo . a. Do hd + 2,9
Suchanowo, Dydyldino, Ostafiewo. we, Joe! | 0 — 81
Boniesaı Gleba Podolsk aa on Jen, gen 599612 | 1159 + 5.25
120ledino, Maiwäjewskoje.i 3 nasınısiandantl sah. 59 2142 a1, »116.9 —+ 1.25
Bioleds, -charapowO 2 eu, ya aka Kemer 55 15,4 | 22,1 4- 1,85

1G. Schweizer. Untersuchungen über die in der Nähe von Moskau stattfindende Lokalattrak-
tion. Moskau, 1863 und 1864.

 
