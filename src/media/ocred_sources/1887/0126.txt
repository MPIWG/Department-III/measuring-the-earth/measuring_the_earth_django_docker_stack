  

MT TG

 

 

92

hat zu Theil werden lassen. Seine Vereinfachungen und Erweiterungen der Beobachtungs-

methoden bilden einen neuen Abschnitt in der Bestimmung der Schwere mittels des Pendels.

Herr Förster theilt die Ansicht des Herrn Defforges über die invariablen Pendel,
welche so manigfaltigen Fehlerquellen ausgesetzt sind, unter denen eine der hauptsächlich-
sten augenscheinlich im Widerstande der umgebenden Luft besteht. Dieser Widerstand va-
Pirt mit der Form und den Dimensionen des Lokals, wie das jüngst auf expermimentellem
Wege mittelst Anwendung von Lycopodium-Staub von einen deutschen Gelehrten nachge-
wiesen worden ist.

Herr Feerster giebt alsdann seiner Bewunderung Ausdruck über die jüngsten von
Herrn Cornu erdachten Synchronisationsvorrichlungen, welche in Frankreich ausgezeichnete
Resultate ergeben haben.

Herr Bussot bestätigt, dass er in der That jüngst sehr bemerkenswerthe Resultate
für die Bestimmung der relativen Schwere zwischen der Sternwarte von Nizza und Peira
Cava erhalten hat, wo ein vollständig synchronischer Gang mittelst einer Telegraphenlinie
erreicht worden ist, die sich in einem ziemlich mangelhaften Zustande befand. Herr Bassot
hat die Absicht, die Experimente in dieser Gegend noch weiter auszudehnen und wird seiner
Zeit nicht ermangeln, darüber Bericht zu erstatten.

Herr Perrier führt aus, dass es praktisch eine Unmöglichkeit wäre, in einer bedeu-
tenden Anzahl von Stationen eines grossen Landes die Bestimmung der absoluten Schwere
nach der vervollkommneten Methode durchzuführen, wie dies in Paris und in Nizza geschehen
ist, und dass man sich auf die Anwendung derselben in einer bestimmten Anzahl von Fun-
damentalpunkten beschränken muss. — Für alle übrigen sollte man sich mit Differential-
Messungen begnügen.

Ilerr von Kalmär bemerkt dass der von Herrn Perrier geschilderte Vorgang gerade
derjenige ist, den sein College, Major von Sterneck, in Oesterreich seit mehreren Jahren ver-
folgt.

Herr Hirsch ist vollkommen von der Nützlichkeit dieses Vorganges überzeugt; in-
dessen muss man sich hüten, für die Differential-Messungen einen der vielen letzthin erfun-
denen, namentlich mit Federn versehenen Apparate zu verwenden, die von ihren Erfindern
über Gebühr gerühmt werden. Es scheint Herrn Hirsch, dass man darnach trachten muss,
den relativen Bestimmungen denselben Genauigkeitsgrad wie den absoluten Schweremessun-
gen zu geben, wie das der Fall ist, wenn man für die ersteren ein gut construirtes invariables
Pendel verwendet.

Herr Hetmert kommt auf den Widerstand der umgebenden Luft zurück und erinnert
daran, dass verschiedene Beobachter, besonders in England und in Indien, ihre unveränder-
lichen Pendel in Kästen haben schwingen lassen, um so die Veränderlichkeit dieses Einflusses
zu eliminiren.

Herr von Kalmär erwähnt dass das nämliche Verfahren ebenfalls von Herrn von
Sterneck befolgt wird.

 

 

 

 

   
 

   
