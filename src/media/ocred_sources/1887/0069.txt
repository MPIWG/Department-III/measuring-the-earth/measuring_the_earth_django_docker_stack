 

Nuni-

bser-
Sions
Pıe-
jercie
dans

ITON-
ich.

opost
cie de
» solu-

gonsr
[oil

 

63

 

Dans l’interet de Pastronomie et de la geodesie, il serait A desirer
mes commencassent A ajoute
de l’influence des trois termes suivanls, de forme eonnue mais de valeur numerique incon-
nue, ä l’aide desquels l’ensemble d’un grand nombre d’observations lunaires faites en diffe-
renis lieux contribuerait dans l’avenir
combinees seulement avec les obsery
laisser.

que les astrono-
"a toutes leurs observations fondamentales de la Lune le caleul

& limiter les incertitudes que les mesures s6odesiques
alıons astronomiques d’ötoiles fixes pourraient encore

1° Un terme d r qui desiene la correetion hypothetique de
la distance au centre de gravite, qu’on a adoptee d
pour le lieu d’observation.

la valeur numerique de
ans la reduction des observations lunaires

2° Un terme d f qui designe la correction hypothetique de la valeur num&
la declinaison du zenith geocentrique, adoptee de m@me dans la r&
de la Lune au centre de gravite de la Terre.

rique de
duction des observations
3° Un terme d ! qui designe la correetion hypothetique de la valeur numerique de
Faseension droite du zenith geocentrique, qu’on a adoptee au moment de l’observation lunaire
pour sa reduction au centre de gravit& de la Terre.

(Quant ä la realisation d’un tel voau, je me permets d’ajouter que je me suis dejä
mis en communication avec quelques astronomes specialement comp&tents dans la branche
des observations lunaires, et que j’ai rencontr& chez eux un assentiment general.

Mais, dans les discussions relatives A ces propositions, on a &tabli en m&me temps la
necessil& de perfectionner les observations lunaires, car dans tat actuel de ces observa tions,
qui sont encore loujours faites aux bords ires aceidentes de la Lune, on ne pourrait presque
rien esperer de leur application aux problemes geodesiques.

En effet, chaque erreur angulaire commise dans l’adoption de la position du rayon
vecleur d’un lieu d’observation se fait voir dans la reduction des observations lunaires A peu
pres avec sa soixantiöme partie, de sorte que V’exactitude d’une seule determination de la
position de la Lune qui, d’apres les proced6s usites, doit ötre &valude A une erreur probable
de plus d’une seconde, correspond ä une incertitude de la position du rayon vecteur du lieu
’observation qui doit depasser 60 secondes. Et certainement des incertitudes de cette ImMpor-
tance semblent &tre tres rares.

Il sera done nöcessaire, pour atteindre le but que nous nous proposons, de com-
mencer enfin & eliminer les bords de la Lune de toutes les observations lunaires fondamen-
tales et de baser ces dernieres sur un ensemble d’assez nombreux points distribues sur
toute la surface lunaire, d’apres un plan qui embrasse la determination de la rotalion et de
la position du centre de gravit& de la Lune, en m&me temps que l’etude de la figure de la

surface lunaire.

Ges points doivent @tre choisis de telle sorte que la phase de l’illumination n’ait
aucune influence sur leurs positions qui ne soit calculable, et de facon qu’ils puissent &tre
mesures aussi dans ’illumination produite par la Terre.

 

 
