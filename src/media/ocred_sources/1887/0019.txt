 

 

 

1 “)

dans un pays ee pendant sa participation A l’Assoeiation, seraient jusqu’ä un cer-
tain point soumises au contröle de l’Association, ou aux suggestions de la Conförence gene-
rale.

« Ayant declare dans mon rapport que « du moment qu’une Association internationale
ne se bornerait pas ä une surface conlinentale, mais s’occuperait de la planete entiere
comme comprise dans la sphere de ses mesures, de ses experiences et observalions, notre
Gouvernement ne saurait sans inconvenient cine la participation a une entreprise d’une
telle importance generale pour le monde scientifique », j’ai constate que « les considera-
tions mentionnees (entre autres le fait d’une limitation territoriale et d’un caraectere auto-
ritaire) conduisent a la conclusion que, tout en ne permettant rien de ce qui pourraitl inter-
rompre ou affaiblir l’utile intimitE de nos relations avec les hommes de science, ou P’ardeur
de notre interät pour les affaires scientiliques de l’Europe, il ne conviendrait pas au Gou-
vernement des Etats-Unis d’adhörer A la Convention d’octobre 1886, concernant l’Associa-
tion g&odesique internationale pour les mesures de la Terre, conforme A l’invitation courtoise
qui nous a etE adressde par le ministre d’Allemagne. »

« Je suis maintenant inform& par notre professeur distingug, M. B. A. Gould, de
Cambridge, qu’il ui etait parvenu plusieurs lettres de la part d’öminents geodesistes euro-
peens, invoquant instamment son intervention aupres de notre Gouvernement en vue de son
adh6sion & la Convention. M. Gould exprime en möme temps sa profonde conviction — basde
sur sa connaissance personnelle des savants qui sont ä la tetede l’organisation — que l’Asso-
ciation, dans ses projets d’operations g&odesiques, n’a nullement l’intention de les limiter A la
surface de ’Europe, ou de les restreindre uniquement aux mesures des degres en Europe,
mais qu’elle se propose au contraire de prendre connaissance de toutes les op6rations g&od6-
siques sans aucune. limitation locale. Il affirme en outre que, d’apres son appreciation,
l’Association se propose seulement d’exercer une influence consultative et nullement une
influence auloritaire et obligatoire sur les operations geodesiques de ses membres. Enfin,
il nous demontre en substance qu’il s’agit d’une Association internationale, destinee A
donner un caractere cosmopolite ä la geodesie, afın d’arriver A une unilormite substantielle de
methodes et de publications, mais uniquement par des discussions, des consultations et des
eonseils r&ciproques.

« Si les appr£eiations de M. le Dr Gould sont correctes, les objeetions principales que
Jai mentionndes dans mon rapport contre l’adhesion de notre gouvernement tombent, et il
est tres probable que les geodesiens de notre survey auraient ei& d’accord de recommander
a notre gouvernement l’adhesion A la Convention.

« Comme cette affaire sera probablement de nouveau reprise, oserais-je faire appel
a votre complaisance, en vous priant de bien vouloir m’informer le plus töt possible, si
les vues de M. le Dr Gould, indiquees ci-dessus, touchant les intentions de l’Association,
sont exacles?

« L’Association g&odesique internationale se propose-t-elle de prendre connaissance
des op£rations geodesiques sans limites locales ?

« Cette Association, ou bien son Bureau central ou ses Gonförences gänerales se

 
