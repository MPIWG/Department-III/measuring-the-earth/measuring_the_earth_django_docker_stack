 

sei,

‚Ts

sur le

nl, el
N du
arell

re

59

M. Betocchi trouve le systöme de cet instrument tres ingönieux, mais il ne peut pas
s’empecher d’avoir quelques doutes sur ses resultats. La complication prineipale que
M. Lallemand lui semble introduire, en remplagant les mareographes &prouvös, par son
medimardmetre, provient de Pencrassement du vase poreux, difförant suivant les endroits
des cötes ol P’instrument est place, et progressant avec le temps. Il faut done &videmment
les changer plus ou moins souvent, et ä chaque changement du vase poreux, on influencera
nöcessairement la hauleur indiqude par V’appareil.

M. Lallemand vepond que, d’apres V’experience quwil a faite A Marseille avec un
vase poreux de ce genre, qu’il a laisse s’encrasser pendant deux ans et demi, il ne s’est pro-
duit aucun changement dans le fonetionnement de l’appareil

M. Hirsch w’a pas l’intention d’entrer dans les nombreux details interessants du
memoire tres circonstancie de M. Lallemand; il s’abstiendra surtout de diseuter les theories
des corrections orthomeötriques ou dynamiques developpees par M. Lallemand. M. Hirsch se
borne, ä ce sujet, & soulenir que, ni (höoriquement, ni praliquement, la sup6riorite de
l’altimetrie dynamique n’est suffisamment demontree pour qu’on puisse en parler comme
d’une verite classique de la science, et moins encore pour qu’on puisse faire entendre que
les nivellements de pr&cision, ex@eutös dans la plupart des pays, devraient &lre renouvelös
d’apres la methode des corrections dynamiques.

M. Hirsch a &t& surtout Srappe par une remarque un peu hasardee que M. Lalle-
mand.a faite dans son discours, d’apres laquelle il n’existerait point de niveau constant
moyen de Ja mer. M. Hirsch ne peut pas admettre que M. Lallemand, inventeur d’un nouvel
appareil destine preeisöment a determiner le niveau moyen de la mer, puisse croire lui-m&eme
serieusement A une these pareille; car, &videmment, ce serait peu serieux de chercher avec
beaucoup de peine el & grands frais quelque chose qu’on declare d’avance ne pas exister.
M. Hirsch serait le dernier & se plaindre qu’on täche de perfectionner toujours davantage les
theories et les observations dans les sciences exactes; mais il ne peut pas admettre que des
observations isoldes et restreintes suffisent pour renverser ou faire abandonner les Iheories,
envisagees comme classiques, des grands geometres du sieele dernier et du commencement
du nötre, qui ont dömontr& la stabilit@ de l’equilibre, et par consöquent la constance du niveau
moyen de I’Ocean, lequel ne saurait changer que par suite de la diminulion ou de l’augmen-
tation de la quantite totale d’eau qui se trouve A la surface de la terre, sous forme liquide
ou sous forme de vapeur, supposition pour laquelle la physique du globe n’offre, jusqu’ä
present, aucune preuve, et qui, au contraire, est envisagee, par la plupart des physiciens et
des met£orologistes, comme impossible.

Sans doute M. Lallemand a raison, qu’il faut fixer le niveau fondamental par des
reperes materiels places le long des cöles, pres de la mer. Mais ces reperes ne doivent &!re
envisag6s que comme des moyens de faciliter aux ingenieurs et geodesiens la possibilit6 de
recourir au niveau moyen de la mer. Ce serait bien malheureux si la Commission perma-
nente de l’Association g&odösique devait quitter Nice avec la convietion qu’il n’existe pas de
niveau moyen de la mer, et qu'il faut le remplacer par ’horizon d’un repere fondamental

 

 
