  
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
   
  
   
    
   
  
   
  
  

/

pourra etre prolong6 encore plus au sud d’environ un degr& et demi. Il r&sulte, en effet,
d’une reconnaissance ex&cutce par MM. les capitaines Deflorges et de Mussy, & la suite du
travail precedent, que la triangulalion peut &tre poursuivie jusqu’aux environs de Gardaia,
dans le pays des M’Zabites, ä la latitude de 32°20°. La latitude de Saxavord, station extreme
du nord de l’Ecosse, etant de 6050, la grande meridienne de Paris alteindra ainsi un deve-
loppement total de 28 degres et demi.

7° Difference de latitude entre Alger et Laghouat.

Afin d’obtenir ’amplitude astronomique de la porlion d’arc meridien comprise
entre Alger et Laghouat, des observations de latitude ont &t& faites simultanäment aux stations
astronomiques d’Alger et de Laghouat par les distances zenithales meridiennes d’etoiles
apparlenant ä un catalogue commun aux deux stations, ensorte que la difference de latitude
de ces deux stations est ind&pendante des erreurs du catalogue et de l’influence de la saison.

Ges observalions ont &i& faites A Laghouat par M. Defforges, etä Alger par M. de
Magnin.

CALGULS

Les caleuls relatifs a la mesure des differences de longitude entre

Paris-Leyde,
Paris-Madrid,
Paris-Dunkerque,

sont compleiement termines. Nous avons ainsi trouv6, par la combinaison des observations
conjugudes et d’accord avec nos collögues hollandais ei espagnol :

Observations de MM. Bassot et Backhuysen : Paris-Leyde A— 8"35°992
» Bassot et Esteban : Paris-Madrid 94. 0,000

» Bassot et Defforges : Paris-Dunkerque — 0.17,995

Les observations ex&cutdes, en 1863, par MM. Leverrier et Aguilar, entre Paris-
Biarritz et Biarritz-Madrid, avaient donn&

 

"
ji
MH

Ri

Paris-Biarvitz . 1534.48
Biarritz-Madrid 8.831,60 ;
d’oü Paris-Madrid . 24. 6,08
