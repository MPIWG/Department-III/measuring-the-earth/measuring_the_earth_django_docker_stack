Ak

Mehrere Mitglieder, unter anderem die Herren Ferrero, Perrier, Bakhuysen, schla-
sen Zusätze vor, um den Antrag des Herrn von Kalmär zu mildern. Da Herr von Kalmar sich
mit jeder Milderung seines Vorschlages einverstanden erklärt, schlägt der Vorsitzende vor,
dass sich der Antragsteller mit dem Schriftführer über die Redaktion seines Antrages in bei-
den Sprachen einigen möge, in dem Sinne, dass der Diskussion Rechnung getragen wird,
und dass der neue Antrag in einer der nächsten Sitzungen der Commission vorgelegt wird.

Dieser Vorschlag wird angenommen.

Herr Bouquet de la Grye legt der Gonferenz die Resultate der Triangulation vor,
welche von den Hydrographen-Ingenieuren längst der Küste von Tunis, von der Grenze Algiers
bis Tripolis ausgeführt worden ist. Die Kette enthält ungefähr 1000 Stationen, und zwischen
Tunis und der südlichen Grenze des neuen Netzes schliessen die Dreiecke mit einem mittleren
Fehler von ungefähr 3”.

Es sind nur einige Verifikationsbasen gemessen worden, da die Triangulation in
Wirklichkeit sich auf eine Seite stützt, welche vom « Geographischen Bureau der Armee »
gegeben ist und die vom Netze von Algier abhängt.

Herr Hauune, Ingenieur-Hydrograph, war seit drei Jahren mit dieser Triangulation,
die eine Länge von nahezu 800 Kilometer hat, beschäftigt und hat eine südliche Endstation
astronomisch bestimmt. — Die Vergleichung der Resultate mit den Angaben des Herrn Per-
‚rier für Carthago ergiebt in Länge eine Differenz von 5’ und in Breite eine solche von 3”.
Diese Unterschiede können als Ursachen Lokalattraktionen haben.

Herr Bouquet de la Grye theilt alsdann der Conferenz die Resultate mit, welche er
für das mittlere Meeresniveau in Brest, Cherbourg und Havre erhalten hat.

Für diese drei Stationen wurde das mittlere Niveau für alle meteorologischen Ein-
flüsse corrigirt, deren Beobachtung zu Bedingungsgleichungen führte, mittelst deren die
Stärke und die Richtung des Windes, der Luftdruck und der Einfluss des Regens eliminirt
wurden; ausserdem wurde der kleinen Mondwelle mit langer Periode Rechnung ge-
tragen.

Um ferner die Untersuchung über das mittlere Niveau in den angegebenen Häfen zu
erleichtern, hat man ein für allemal die Differenz zwischen der Integration der Mareographen-

Curven während einer langen Periode, und zwischen dem Mittel von Ebbe und Fluth be-
stimmt.

Die Resultate dieser Rechnungen, d. h. die corrigirten mittleren Meeres-Niveaus
geben, sobald sie durch ein Präcisionsnivellement verbunden sind, ein Maass für den Ueber-
schuss an lebendiger Kraft, die in der Ebbe und Fluth entwickelt wird, und für den Einfluss
des an unseren Küsten veränderlichen Salzgehalts.

Herr Forster wünscht, dass die zweite Specialcommission, die in der letzten Sitzung
vom Vorsitzenden ernannt worden ist, beauftragt werde, sich von neuem mit den ralionnel-
sten Mitteln zu befassen, mit denen das Centralbureau die Specialberichterstatter in ihren
synoplischen Arbeiten unterstützen und denselben die Benützung der in den verschiedenen

 
