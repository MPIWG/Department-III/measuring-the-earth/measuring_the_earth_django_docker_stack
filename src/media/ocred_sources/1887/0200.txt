En EEE TERIEEHENTE Eee

ERLEBEN

v
+

a

 

 

2

tent inexpliqu6s par Vattraclion de masses de lerrains voisins. Greene convient qu’une
extension plus grande de la peripherie dans les caleuls d’attraction pourrait encore amelio-
rer un peu le raccordement; cependant il s’en reföre aussi A l’influence des differences de
lensit6 des diverses formations geologiques dans le territoire considere. En n’admetlant
meme pour cela que 0,1 de la densite de l’eau, il y aura ndanmoins, par Vintermediaire de
cette influence, possibilit& d’expliquer des difförences qui restent encore pour un bon nom-
bre de cas, pourvu qu’on suppose seulement que ces differences de densit& existent jusqu’&a
la profondeur de plusieurs kilometres.

x 4. Deviations de la verticale dans les environs de Leipzig.

Nagel a rapport6 sur ces deviations dans les Comptes rendus de la Conference de
1886, annexe 13, p. 292, ete., döviations au sujet desquelles Bruhns a fait entrepren-
re des observations en 1868. Les döviations relatives £ en latitude, aussi bien que celles A
en longitude ont pu &tre interpoldes avec une exactitude suffisante au moyen d’une simple
formule. Pour un point avec les coordonndes rectangulaires «km vers le Nord el ykm vers
’Est, rapport6 ä la Pleissenbourg comme point zero, ona:

E—= — 0.128 + 0,0067.2 + 0,0670.
x = — 0,276 + 0,1652.2 — 0,2459.y

La premiere de ces formules d’interpolation, valable pour le voisinage immediat de
Leipzig (dans un eireuit d’environ 10 kilometres), peut ire employde A titre d’essai, en vue
d’esaminer si on peut !’ötendre ulterieurement aux points de Petersberg et Pfarrsberg dans
le quadrant nord-ouest des environs plus &loignds de Leipzig. Pour ces points, les deviations
relatives de la verticale par rapport ä Leipzig sont contenues dans le tableau III. Mais on a:

& Yy E observ. &calc. Obs.-Calc.
Petersberg : +30 km — 99km — 0.8 —17 +09
Pfarrsberg: + 2 — 33 — dh ed, IN

d’olı ’on peut reconnaitre que la deviation de la verticale en latitude dans le quadrant nord-
ouest suit son cours tres regulierement. Les differences observation-caleul sont faibles par
vapport A la preeision des valeurs d’observation aussi bien que des valeurs calculees. Les coef-
ficients des formules pour & et % ont des erreurs moyennes de +0. 015 & + 0,017, ce qui
oceasionne dansles valeurs caleuldes des incerlitudes moyennes döpassant + 0,5.

Au reste, ces formules permettent encore un examen partiel r&ciproqne. G’est-a-
dire que si, par rapport ä& leur forme, on pose pour l’elevation relative z du geoide au-
dessus de Pellipsoide l’equation !

 

 

 

 

 
