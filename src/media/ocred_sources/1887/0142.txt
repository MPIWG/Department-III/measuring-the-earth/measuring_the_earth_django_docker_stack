 

 

68

Herr Fiersier liest im Namen der Specialeommmission den folgenden Bericht über

die Finanzen :

« Bericht der Specialcommission über die Finanzen
von Herrn Ferster.

« Die Specialcommission hat mich beauftragt, der Permanenten Commission die fol-
eenden Bemerkungen und Vorschläge zu unterbreiten :

«1. Laut dem Bericht des Centralbureau’s betragen bis jetzt die Beiträge der Slaaten
9700 Franken, und es stehen noch für dasselbe Verwaltungsjahr von vier Staaten die Bei-
träge im Betrage von 7050 Franken aus, so dass die Gesammtsumme der Jahresbeiträge
für 1887 16,750 Franken beträgt.

«Es ist somit durch die derzeitigen Beitritte die obere Grenze für das Budget,
welche von der Convention auf 20,000 Franken angesetzt ist, noch nicht erreicht, indessen
wird man derselben bereits im nächsten Jahre sehr nahe kommen, denn der Beitritt der
Vereinigten Staaten und Serbiens, welcher nach massgebenden Mittheilungen sehr wahrschein-
lich ist, wird die Einnahmen unseres Budgets für 1888 auf die Summe von 19,300 Franken
erhöhen.

«Wir schlagen desshalb der Permanenten Commission vor, anerkennen zu wollen,
dass die Absichten der Convention betreff der Dotation der Erdmessung zur Zeit als genügend
verwirklicht erscheinen.

«2. Bis jetzt haben fünf der Vertragsstaaten von der durch $ 8 der Convention vor-
gesehenen Bestimmung Gebrauch gemacht, indem sie ihre Beiträge für zehn Jahre kapitali-
sirt eingezahlt haben. Die Gesammtsumme dieser Einzahlungen beträgt, nach Abzug des Bei-
tages für 1887, 18,000 Franken. Diese Summe, zu welcher noch die Zinsen kommen, bleibt
für die künftigen Geschäftsjahre zur Verfügung.

« Was die Gleichwerthigkeit dieser kapitalisirten Einzahlungen wit den jährlichen
Beiträgen für ein zehnjähriges Intervall betrifft, so hat sich die Specialeommission überzeugt,
dass in dieser Beziehung die Voraussetzungen, die im Artikel 8 der Convention, betreff der
Bestimmung des Zinsfusses gemacht worden sind, nicht strenge durchgeführt werden kön-
nnen, dass indessen, wenn alle näheren Umstände in Betracht gezogen werden, die in
Frage stehende Acquivalenz genügend erreicht zu sein scheint, besonders wenn man bedenkt,
dass die Kapitalisirung gegenüber den jährlichen Einzahlungen für die finanzielle Verwaltung
der Permanenten Commission erhebliche Vortheile bietet. Es ist übrigens zu erwarten, dass
der beständige Zinsertrag aller uns zur Verfügung stehenden Mittel, selbst zu einem Zinsfuss,
der 2°/o nicht übersteigt, gestatten wird, die Einnahmen des vollständigen Budgets bis zu dem
durch die Convention bestimmten Zeitpunkte von zehn Jahren nahezu sicher stellen wird.

«Sobald sämmtliche Einzahlungen, kapitalisirte oder in jährlichen Beiträgen, statt-
gefunden haben, wird die nutzbringende Anlage der Dotation, welche bis jetzt nur vorläufig
organisirt ist, bei der deutschen Reichsbank geschehen.

 

 

 

 
