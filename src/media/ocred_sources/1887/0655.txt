 

France et Algerie.

 

N®

IN BIOAIIKON

DES POINTS.
|

|
|

LATITUDE, | LONGITUDE.

 

 

EPOQUE.

|

ET OBSERVATEURS.

DIRECTEURS

 

INSTRUMENTS.

 

 

 

REMARQUES.

 

Triangulation du Parallele d’Amiens. (Partie Orientale — Suite.)

91 |) Hebuterne

921

3
94
95
96
97

 

Nurlu

St. Quentin
Bremont...:.....:
Guisen 2... 8:
Laon

La Bouteille
Mainbressy

Trou du Sable ....

La Grande Croix..
Pagnou de Pusse-

Villers-Breton-

Vignacourt........

St. Leger aux Bois.
Mont de P’Aigle...
Foret d’Hellet....
Tourville la Cha-

Les Grandes Ventes.
Phare de PAilly...
St. Laurent
Ingouville

 

Pantheon
Belle Assise
Malvoisine
Rampillon

Monceaux
Chavaudon
Allement

Longeville
Montiers..
Menil la Horgne..

 

30° 7 817] 20°18. 37)
280 2022 | 2041 8
49 50 55 | 20 57 13
50° 0532| 213 25
49 53 52 | 21 16
49 3348 | 21 19
49 53 14 | 31 16
4943 2121 51 ||
49 53 23 | 22 6 ||

49 42 42 22 1412 |
49 56 20 | 22 26 37 |

49 | 22 31 52
5 | 22 35 24
16 | 22 56 16

 

 

 

Major Corabeeuf,
lieutenant Testu.

 

Cercles repetiteurs.

Triangulation du Parallele d’Amiens. (Partie Occidentale.)

Deja mentionnes.

49°51' 25”) 19° 50’ 32
49 50 47 | 19 33 45
50-7 38 19,25 91
49 50 2 | 19 16 24
49 56 49 | 19 8 32
AuAT7 20: 193 1

49 56 38 | 18 55 33
49 47 11 | 18 53 29
2939, 61231. 20
49 45 9 .18 32 33
49 50 19 | 18.20 58

 

 

 

 

 

v

Major Delahaye,
lieutenant Peytier.

 

Cerecles repetiteurs.

Triangulation du Parallöle de Paris. (Partie Orientale.)

Deja mentionnes.

48°33' 4”| 20043! 47°
48 52 9 | 20 49 51
48 41 44 61
48 19 2 79
48 45 40 | 21 97 48
48 23 3 44 26
48 41 59 59 5
48 31 20 149
48 50 16 19 52
48 44 7 50 60
48.32 23 53 19
48 42 6 | 23 10 43
48 22 46 6 52
48 25 43 3 5
48 42 47 30 30
48-24 32 44 4
4845 31 |23857 5

 

 

 

1818-19-20-21.

 

Colonel Henry,
lieutenants Levillain, Martner, Faulte
et Delavarande.

 

Cereles repetiteurs.

 

 

 

 

 

Memorial du Depöt
General de la Guer-
re, Tome VI.

Memorial du Depöt
General de la Guer-
re, Tomes VI et IX.

 
