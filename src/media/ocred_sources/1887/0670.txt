Colonel L. A. Hall, R.E., was Director of the Survey from 1847 till 1854, The -
Principal Triangulation was completed in 1852, and the systematic reduction of the
observations was proceeding for publication under the superintendence of Colonel Yol-
land; R.E., and Colonel Cameron, R.E., when in the summer of 1854 the late Cobat
Sir Henry James, R. E., succeeded en Hall as Director. The account of this Trian-
gulation, drawn up by Captain A. R. Clarke, R. E., was published in 1858: it deseribes:
the Instruments and methods of calculation, and contains all .the previous observations
of any geodetic value. The only subsequent, but very important, operations — all under ’
the direction of Sir Henry James — were the extension of the Triangulation through
the North of France into Belgium in 1861; the comparison of the various national stan-
dards of length; the positions of Feaghmain and Haverfordwest Longitude stations on
the oreat arc of Parallel, accounts of which, drawn up by Colonel Clarke, were published
in 1863, 1866, and 1867 respectively. These complete the contributions of the Ördnance
'Trigonometrical Survey of Great Britain to the geodesy of Europe, and towards 12
determination of the figure and dimensions of the Earth.

‚In further explanation it may be stated that the Triangulation of England and
Wales and extension into Scotland was at first carried on for the purpose of supplying
Bases for the construction of the series of maps on the scale of one inch to a mile,
In 1825, however, the Government having ordered that county maps of Ireland should
be made on a scale of 6 inches to a mile, operations in England and Scotland were.
partially suspended till the completion of the Irish Survey in 1841. And as it was
‚then decided that the maps of the North of England and Scotland must be on the same
scale as the Irish maps, it was found necessary to revisit most of the Trigonometrical
Stations. The exigencies of progress and economy induced Colonel Colby to instruct,
under the superintendence of his then executive officer, Captain Yolland, non-commis-
sioned Officers to make observations with the large Instruments. During the years 1842,
1843 and 1844 commissioned Officers observed with Airy’s zenith sector. The obser-
vations from 1845 till 1850 were made by Serjeants J. Steel and W. Jenkins, Royal
Sappers and Miners. Up to 1842 the chord method of calculating the angles and sides.

of triangles was in use: since then the more simple, well known, method of Legendre
has been adopted.

Southampton, 24h June 1887.

T..p. Worms
' Colonel R.E. for D.@.0.8.

 
