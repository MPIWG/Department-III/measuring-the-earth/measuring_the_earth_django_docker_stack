 

 

 

 

DZ

 

 

Fr

7

N
=
BJ
Fi
in
ze

 

 

 

19

On voit, sur ce diagramme, que dans le nord de la France, la difference entre les
deux syst&mes d’altitudes depasse rarement 10 centimetres ; au contraire, dans les Pyrenees,
elle peut atteindre jusqu’ä 1M,60, mais seulement pour des hauteurs de 3000 metres et plus.

IV. — Rasume.

11. D’aprös la theorie actuelle du nivellement geometrique, le relief du sol, rapport&
a la surface de niveau zero, se trouverait determind par ses interseclions avec une serie de
surfaces auatliaires paralleles et de niveau, ayant chacune pour caracleristique d’elre affectee
d’une cole unique dans toute son ölendue.

Or, nous l’avons demontre, le parallelisme et la conservalion du niveau sont deux
proprietes incompatibles. Il faut opter pour l’une ou pour l’autre.

La theorie orthometrique, caleulant la distance (en metres, par exemple) de chaque
point du relief ä la surface de niveau zero, sacrifie la consid6ration du niveau pour conserver
le parullelisme des surfaces auxiliaires.

La theorie dynamique, au contraire, determine (en kilogrammetres) le travail A
depenser, en raison, de la pesanteur pour s’elever du repäre ou de la surface de niveau zero
a tous les autres rep£res. Elle conserve ainsi les surfaces de niveau, en se resignant A perdre
le benefice de l’&quidistance geometrique.

Les deux systemes ont chacun leurs avanlages et leurs inconvenients.

12. Ainsi, avec la theorie orthometrique, la definition habituelle de V’altitude n’est pas
modifiee; les corrections A apporter aux resultats bruts du nivellement sont relativement
faibles et rögulieres d’allure.

Des lors, il suffit de les introduire dans le calcul des altitudes du Reseau fondamental
des nivellements, sans avoir A s’en pr&occuper pour les operations de dätail qui viendront
plus tard s’y appuyer.

En revanche, on peut faire a cette methode les critiques suivantes :

1° Les ultitudes orthometriques des points nivelös ne sont pas rigoureusement les
distances de ces points ä la surface de comparaison, puisque ces altitudes sont mesurdes sur
les trajectoires orthogonales des surfaces de niveau, et qua ces trajectoires sont des lignes
courbes.

2° Sı l’on voulait rapporter le nivellement a une autre surface de niveau, celle-cj
n’etant pas parallele & la premiere, au lieu d’ajouter ou de retrancher simplement une
constante, comme on le fait d’habitude, il faudrait, en toute rigueur, appliquer & chacune
des altitudes une correction differente.

3° Les surfaces de niveau n’etant pas paralleles, les points d’une möme surface de
niveau ont des altitudes orthomelriques differentes, el deux points de m&me altitude ne son
pas forcemen! de niveau. Des lors, la difference de niveau cessant d’avoir pour mesure la deffe-
rence des altitudes perd, dans cette th&orie, toute espece de sens.

 
