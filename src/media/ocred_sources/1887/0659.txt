France et Algerie.

 

 

INDICATION

DES POINTS.

LATITUDE.

 

 

LONGITUDE.

 

|

EPOQUE. |

ET

DIRECTEURS

OBSERVATEURS.

INSTRUMENTS.

 

 

 

REMARQUES.

 

 

 

Triangulation de Jonction de la Base de Bordeaux au Parallele Moyen.

| 45019 30”, 17024 58”

| Chantillae
Nonaville
Chadenac

Soubran

Si. Savin

Le Gibault

La Pouiade

St. Andre de Cub-

Montagne

Bordeaux. ........
Blanquefort
Lengean

Captieux

Bellachat

Frene

Encombres

Mont Jouvet
Roche Chevriere...
Mont Tabor

Chaberton
Albergian
Rochemelon

Puech de Monsei-

‚Cougouille

Puech d’Aluech ...
L’Hort de Dieu...
Roc de Malpertus .
Le Guidon de Bou-

Dent de Rez
Montmou

Grand Montagne ..
Mont Ventoux

Les Houpies

Le Leberon

Mourr& de Chenier.
Les Monges
‚Le Grand Coyer...

Deja mentionnes.

AD 7
45 8 34

44 59 57
44 55 53
44 46 11
44 50 19
44 54 38
44 58 839
44 51 38
45 41 34
45 32 30
45 21 9
45 17 51
29 42

17 37

6 51
9:25
57 54

0 27

12 13

 

45 10 54
5 529 |

 

I od
17 12 53
17 32 4
17 22 45

17 12 36
ld 32 9
17.21
1725

1

53

48

5l
24 4
23 51
24 6
24 18
24 23
24 13 39
24 32 53
24 24 48
24 39 23
24 44 28

 

 

 

Colonel Brousseaud,
lieutenants Fessard, Levret et Dupuy.

 

Cereles röpetiteurs.

Triangulation du Parallöle de Rodez. (Partie Orientale.)

43 57 51
44 20 9
4 719
A 5

4 659
44 25 46
44 13 24
43 58 34
44 10 27
43 42 44
43 48 56
4 723
43 Al 27
43 50 30
44 15 46
a6
44 26 57
43 44 51
43 48 53

 

 

410192582 202352332
Deja mentionnes.

20 47 21
21 4 58
21 14 40
21 30 33

21 56 56
22° 951
22 23 57
22 25 46
22 56 31
22 38 45
23 150
23 27 58
23 34 18
24 052
23 51 28
24 21 12
24 19 25
24 19 31

24 37 59

 

 

 

Capitaine Durand,
lieutenants Rozet et Levret.

 

Cereles repetiteurs.

 

 

 

 

Memorial du Depöt
General de la Guer-
re, Tome VI.

 
