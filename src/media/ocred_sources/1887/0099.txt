ze

=

E

Don
den
me

gatol
niit

   

 

 

 

95

 

B. Copieen von vorstehenden Toisen.

j BP: ann’ce (ana NoG u IE or z ; 2
Su. baumann’s Gopie N° 9 von |, 1852 von Baeyer, 1878 von Schreiber und Foerster
mit | verglichen.
vl. un: Gopie N° 10 von 1, 1852 von Beayer, 1878 von Schreiber und
R N 5 : / G , Senabani 5 T. es n ; S : S
Foerstei mit I, 1872 von Sadebeck mit VII verglichen. VI und VIII befinden sich in der
tigonometrischen Abtheilung des grossen Generalstabes (Oberst Schreiber) in Berlin
x Ir > Ni = AN A! ei / 6 r 5 x “ © E , r
IN. Baumann’s Copie N° 11 von I, 1852 von Baeyer mit 1 verglichen. Depöt de la
guerre in Brüssel.
- T IR anauy 9 le a,
X. Copie H. von V, 1828 von Struve mit V verglichen. Sternwarte zu Pulkowa.
XI. Berliner Toise von Lenoir, 1852 von Baeyer und 1878 von Schreiber und
s AST9 y I le IE, ar ;
Foerster mit I, und 1872 von Sadebeck mit VII verglichen. Kaiserliche Normal-Aichungs-
commission in Berlin.

C. Weitere Toisenmaassstäbe.

XII. Italienische Toise von Spano, 1865 in Berlin mit VIII verglichen.

XIII. Italienische Toise von Ertel, 1865 und 1869 in Italien mit XII verglichen.
Beide Maassstäbe befinden sich im Istituto topografico militare in Florenz. |

XIV. Russische Doppeltoise N, verglichen mit V, X und VI.) Sternwarte zu

XV. Russische Doppeltoise N‘ verglichen mit XIV. \ Pulkowa.

XV. Englische Ordnance Toise, 1863 bis 1865 von Glarke mit VII, IX und XV
verglichen; befindet sich bei der Ordnance Survey Office in Southampton.

Zusammengestellt von Dr. A. Börsch.

Anlage I.

Seit der letzten Allgemeinen Conferenz im Jahre 1886 sind dem Gentralbureau aus
der Internationalen Vereinigung folgende Publikationen zugegangen :

1. Von Herrn Major d’Avila in Lissabon : Ligacäo do Observatorio Astronomico de
Lisboa com a Triangulacäo fundamental. 37 Exemplare.

9. Von demselben : Relatorio dos Trabalhos geodesicos, 1884-1885. 36 Exemplare.

3. Von dem K. K. Militär-geographischen Institat in Wien : Mittheilungen, Band
VI. 73 Exemplare.

4. Von Herrn General Ferrero in Florenz : Processo verbale della sedute della Gom-
missione geodetiea Italiana tenute in Milano, Settemhre 1886. 100 Exemplare.

5. Von Herrn Oberstlieutenant Haffner in Christiana : Geodätische Arbeiten, Heft V.

125 Exemplare.
VERHANDLUNGEN. — %

  
