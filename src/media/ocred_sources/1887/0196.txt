|
|

en

 

A8

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

u, .
| Distance le
2 A de eviation
Moyenne des points. Hauteur du pole Iaole Wo:
| Zero.
Be ° ’ „H
Kioweo, Neomkoje Puschkhmo, _.: .. 2% 59 59,9 21,8 ze
Archangelsk. Bolscheı Myschy . 2. — sa , .ı
Leonowo I, Ostankino sy: ; 55 19.35 | 11,9 — 5,05
IwanWeliki. CouventAndroniew, Univers. Observ. > a u ee
olomenskoje, Woronzowo .. .  .:... ol ı 5 iD
Nessjädy, Jassenewo . ‚SI MOBEINSD. | 55 DU/EI DE SH + 2.35
Suchanowo, Dydyldino, Ostafi iewo. in: ayfiem-nn. 55 31,1 | A) el
Boruscaı Gleba, Bodolsk, eye: SASS mn ine as 99.206,72 | 11,9 17.99
Koledino Mapuapow .kofer ... . 2.2. . 59 21,2 ı 16.9 a,
Nolody, Donarapomor rar. en sn. Bee + 1,8

Ce tableau, emprunt& A la p. 85 du M&moire en question et complöt£ partiellement
au moyen des indicalions de la carte, montre que la deviation de la verticale dans le meri-
dien d’Iwan Weliki suit une marche assez &gale vers le Nord et le Sud & partir d’un point
z&r0 situe A D5°38°. On peut admeltre, comme moyenne approximalive, les donnees suivantes
(comp ». 86, Ol er 11):

Distance Deviation
182 — 3,4 km 9
a 78
RR Da, 5,15 (A)
noeıı ) 9,4
99,1 Al 0

Si l’on imagine le profil du m£ridien d’Iwan Weliki tourne de 79° en Azimut vers
l’Ouest, on a ä peu pres une idee du cours de la deviation de la verticale d’un cöle et
de P’autre du meridien en question dans les limites du terriloire examine. La ligne zero
moyenne est assez exactement droite, tandis qu’en gen6ral, sous 79° d’Azimut, les courbes
de meme devialion possedent des &carts irreguliers par rapport aux lignes droiles.

Schweizer s’occupe {res minutieusement ä chercher des explications convenables
aux devialions observees de la verticale, par l’attraction des masses souterraines, parce que
les inegalites du sol ne suffisent pas ä les expliquer & cause de leur insignifiance. Un vide
de forme spherique, est de prime abord exclu comme motif d’explicalion, ä cause de la
grande etendue des deviations de ’Est A l’Ouest (ce fait doit ätre signal& en presence de
diverses communicalions errondes qui ont &iE produites sur ces investigations); d’apres
Schweizer m&öme, un vide de forme allongee ou eylindrique ne suffit pas, aussi peu qu’un
prisme allonge dont la coupe serait un reclangle, un triangle ou un trapeze. Par contre

 

—
