 

|
l
1
|
|

 

 

I. Untersuchungen für ausgedehnte Gebiete, wobei die Form des
Referenzellipsoids von Einfluss ist.

s 1. Das allgemeine Erdellipsoid.

Von den verschiedenen Angaben für das Erd-Rotationsellipsoid hebe ich hier drei
hervor.

Erstens das Ellipsoid von Bessel aus dem Jahre 1841, zweitens das Glarke’sche Rl-
lipsoid von 1866 und drittens das Ellipsoid von Glarke aus dem Jahre 1880. Für diese Aus-
wahl sprechen folgende Gründe :

Das Bessel’sche Ellipsoid wird noch jetzt vielfach bei Berechnung geographischer
Positionen aus geodätischen Messungen benutzt; es kann in der That als ein in manchen
Gegenden genügender Repräsentant der Geoidform angesehen werden. Das Glarke’sche Ellip-
soid von 1866 ist ausgezeichnet durch die bekannte genaue englische Vergleichung der bei
den verschiedenen Gradmessungen angewandten Maasseinheiten, welche hier zum ersten
Male Eingang findet. Ausserdem ist es seit 1880 bei den ausgedehnten Vermessungen in den
Vereinigten Staaten von Nordamerika eingeführt an Stelle des bis dahin angewandten
Bessel’schen Ellipsoids. Wie in $ 7 näher mitgetheilt werden wird, hatte dieser Wechsel
eine erhebliche Verminderung der Lothabweichungen zur Folge. Das Clarke’sche Ellipsoid
von 1880 endlich ist als jüngstes Rechnungsergebniss des berühmten Geodäten in Betracht
zu ziehen. Es unterscheidet sich nur wenig von dein Ellipsoid von 1866.

Die Originalangaben für diese drei Ellipsoide sind die folgenden, unter a und 5b die
Halbaxen der Meridianellipse verstanden.

Maasseinheit.
Besel oil! joa - 6, s14 8935. 3931
ob — 0 al, 509)

>39 | Toise du Perou.
Clarke 1866 ? a 20 926 0062 )
)

„951 Englische Fuss.

20 926 202
20 854 895

Clarke 1880 3
Englische Fuss.

Il

1 Astronomische Nachrichten, 1842. XIX, N 438, p. 97; R. Engelmann’s Ausgabe der « Abhand-
lungen von F. W. Bessel. » II, p. 62.

?Comparisons of the Standards of Length of England, France, Belgium, Prussia, Russia, India,
Australia, made at the Ordnance Survey Office, Southampton, by Captain A. R. Glarke, under the direction
of Colonel Sir Henry James. London, 1866, p. 280-287.

3 Geodesy. By Colonel A. R. Clarke, C. B., etc. Oxford, at the Clarendon Press. 1880, p. 319.

 
