 

 

11

unsere Anstrengungen in demselben Gebiete für die allgemeinen Resultate der Gradmessung
sehr förderlich sein werden.
« Genehmigen Sie, Herr Präsident, die Versicherung meiner ausgezeichneten lloch-
achtung.
«(ge2.) Rich. H)'Shurkean,
« Col RI Ener.
« Direetor Gen! of Ordnance Survey.

« A Monsieur le President de l’Association g&odesique internationale. »

England hat sich auf der Generalconferenz in Berlin im Jahre 1886 nieht vertreten
lassen, « weil es in diesem Augenblicke keinen Delegirten zur Verfügung hatte, » trotzdem hat
es uns unterm 27. December des Jahres 1886 durch eine Depesche des englischen Ministers in
Bern, Sir Francis Adams, den zum General-Direktor der « Ordnance Survey » ernannten Sir
@. W. Wilson als Delegirten Englands zur Internationalen Erdmessung angezeigt. Auf die
letztere Note habe ich durch folgendes Schreiben geantwortet, welches in der Uebersetzung
lautet:

« Neuchätel, den 9. Januar 1887.

«Ich habe die Ehre, den Empfang des Schreibens vom 27. December 1886 anzu-
zeigen, durch welches Ihre Excellenz auf Befehl Ihrer Majestät der Königin mir anzuzeigen die
Güte hatte, dass der Oberst Sir C. W. Wilson zum General-Direktor der « Ordnance Sur-
vey » und gleichzeitig zum Delegirten des vereinigten Königreichs bei der Internationalen
Erdmessung ernannt worden ist.

« Ich bin ganz besonders erfreut, diese Nachricht den Mitgliedern der Erdmessung
mittheilen zu können, nicht allein, weil ich die wissenschaftlichen Verdienste Sir Wilson’s
sehr hoch anschlage, sondern auch, weil diese Mittheilung den Beweis in sich schliesst, dass
die Abwesenheit Grosshritaniens an der letzten General-Conferenz der Erdmessung in Berlin,
die wir alle sehr bedauert haben, nur vorübergehend war, und dass England, welches vor eini°
gen Jahren dieser wissenschaftlichen internationalen Organisation beigetreten war, in der es
berufen ist, eine wichtige Rolle zu spielen, entschlossen ist, darin zu verbleiben, indem es
der Convention (vom October 1886), die Internationale Erdmessung betreffend, beipflichtet,
die nach dem Tode des Generals Baeyer, des Gründers der alten Gradmessung, zum Zwecke
einer Reorganisation dieser letzteren von der preussischen Regierung vorgeschlagen und von
allen Staaten angenommen wurde, welche vordem zu diesem wissenschaftlichen Werke sich
vereinigt halten.

« Genehmigen Sie, Herr Minister, die Versicherung meiner grossen llochachtung.

« Der ständige Sekretär der Internationalen Erdmessung.

« (gez.) Dr. Ad. Hırscu. »

 
