 

2
&

 

In solution A the absolute terms are simply the observed apparent deflections. This
is the usual solution in which no relation is postulated between the deflections of the
vertical and the topography. This is equivalent to the assumption that there is complete
isostatie compensation at depth zero; that there exists immediately below every elevation
(either mountain or continent) the full compensating defect of density, and that at the
very surface ‚of the ocean floor there lies material of the excessive density necessary to
compensate for the depression of this floor. Under no other condition can it be true that
the observed deflections of the vertical are independent of the known topography.

The particular depths of compensation assumed in solutions E, H, and G depend
mainly upon extensive preliminary investigations made largely for the purpose of obtaining
an approximate idea of the most probable depth of compensation.

The normal equations, with the five sets of absolute terms, are given here.

Normal equations.
Absolute terms.

 

Solution Solution Solution Solution

 

 

 

 

Solution
B: E. H. G: A,

a a

+ 286.69 (6) — 3.32(1)+ 17 63 (z,) — 18.06 (eu) — 77,98 a 71.75 (10000e?) —1 738.89 or — 43 14 or — 24.85 or— 22.950r— S.59=0
a

3.32 (6) 446.75 (a) + 0.25(&,) + 0.97 (2x) + 12.083 m) 2 15.25(2000062)-- 142.8] + Sa Aa nee 078074581 —=0

17.63(@) + 0.28 (r) + 137.29 (&.) — 88.48 (er (10 0006?)-+3 962.61 + 23.95 22.63 29.28 168.10= 0

18.06 (+ 0.97 (A) 101.61 (e,) + 64.13 = rn 73.25 (10000e?)—4292.95 + 385.74 519.94 -+543.63 4964.19 = 0

3 (6) + 12.08 (2) — 88.48 (@,) + 5418 (a1) + 188.15( 70) + 197.82.00000°) 8598.18 + 93.85 +286.97 +319.99

 

 

1a RQ a 2 2 3 S m a cp on
5(4) + 15.25 (9) — 127.78 (2,) + 73.25 (@,) + 187.82 ne I )-+ 9861.02 -+ 274.01 +461.86 +492.79

The fact that there are two unknowns («,) and z,) in these normal equations,
referring to the correetion to the initial azimuth, will be explained fully later under the
heading »Coineident longitude and azimuth observations”.

The values of the unknowns derived from the five solutions are given below. The

solntions gave directly the values of (1) and (10000 e2), but for convenience of reference

there are given in these tables (a) and (e?), the corrections to the Clarke equatorial radius

and square of the eccentricity.

nennen Dimneoannnnnnnn

 

 

A) | (&e) | (ev) | (a) | (e)
uinktuse ton a a.
| u | u | u | Meters |
SolutioniBi., 2.2.02 | + 21.04 -- 0.000659
Solihen sh. nr. Bela De nor ‚0 Ale 468 | 1.292 | — .000064
Solution Ha 22.02.22 ii 0 Er ol N) + 98 — .000066
Sontion@ Zi 0) ae 2 ) 2,16 — ‚000065

See oe 6 2780 _ 961.1» 1000085

+879.34 = 0

+998.15 = 0

 

}
1
|
1
!
1
i
!
j
