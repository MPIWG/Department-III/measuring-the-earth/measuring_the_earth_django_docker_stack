 

294

Il)

Jahre

10

B. Geschäftliche Tätigkeit.

b:

Der Dotationsionds wurde wie bisher verwaltet.

Seine

Bewegung im

1907 stellt sich, vorbehaltlich der konventionsmäßigen genauen Nachweisung
der Einnahmen und Ausgaben, wie folgt:

Einnahmen.

Bestand des Fonds Ende 1906
Beiträge für 1906 .
Beiträge für 1907 a u IE BO
Zinsen: Von der Kur- und Neumärkischen Ritterschaftlichen
Darlehnskasse in Berlin . ;
„ : Von der Königlichen Seehandlung @kenkische Staats-
bank) in Berlin

Summa:

Ausgaben.

Indemnität des ständigen Sekretärs

Für den Internationalen Breitendienst (Koripäralle)

ER, % 5 (Südparallel)

Für andere wissenschaftliche Arbeiten (betr. Schwerkraft und
Erdgestalt) . 5

Für Beschaffung bezw. Beheiae von Triserniienke

Für Druckkosten

Fracht, Porto, a nllıhecadeten

Summa:
Demnach war der Bestand Ende 1907 gleich

Hiervon befanden sich:
bei der Kur- und Neumärkischen Ritterschaftlichen Dar-
lehnskasse in Berlin Ei
bei der Königlichen Seehandlung reunläche Staatsbank)
in Berlin a
und zum Betriebe in ae bfdsst es Fee
DSumma:

An Beiträgen sind für 1901/07 rückständig 13200: M.*)

*) Die Gesamtsumme der disponiblen Fonds stellt sich Ende 1907 rechnungsmäßig auf rund
68754 M., wenn 12 900M. Vorausbezahlung an Betriebskosten für Gaithersburg, Ukiah, Bayswater und
Oncativo at 1908 zu dem Kassenbestand von 55 854 M. addiert werden.

”

52 759,74

10 600,00
59 900,82

583,10

1171,40

M. 124 965,66

M.

”

in.
M.

M.

M.

5.000,00
44 339,08
17 114,75

1 211,00
185,42
265,25
996,00

69 111,50

55 854,16

15 262,00

39 092,16
1500,00
55 854,16

 

 
