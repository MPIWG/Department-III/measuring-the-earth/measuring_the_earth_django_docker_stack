EEE

\
H
j:
i

365

6. VERÄNDERUNGEN DER SCHWERKRAFTSBESCHLEUNIGUNG IN EINER NIVEAUFLÄCHR,

Wenn g die Schwerkraftsbeschleunigung bedeutet, so sind ihre Gradienten:

 

98 u 98 RU
= su ma...
deren Resultante, der totale Gradient, nach Grösse und Richtung durch gerade Linien
grafisch darstellbar ist.

Für das Arader Gebiet ist diese Darstellung durch die Karte (Fig. 8, S. 366) verwirklicht
und zwar bezieht sich diese auf die subterranen Störungswerte, welche von den topogra-
fischen Werten nur in der nächsten Nähe des Gebirges merklich abweichen. (Siehe Tabelle I).

Wären die Werte der Gradienten in allen Punkten der Niveaufläche bekannt, so
würden sich auch endliche Werte der Veränderungen Ag durch Integration berechnen
lassen. Annähernd, doch mit befriedigender Annäherung, lässt sich eine solche Rechnung
dann ausführen, wenn die Beobachtungsstationen nahe genug zu einander liegen, um den
Gradienten zwischen zwei benachbarten Stationen als lineare Function der Ortskoordinaten
betrachten zu können. Die Zulässigkeit einer derartigen Berechnung lässt sich durch ihre
Ausführung längs einer geschlossenen Linie erproben. Es soll ja für eine solche:

 

[IE as=0
Jade

So wurde beispielsweise die Rechnung für das Dreieck ABC unserer Karte ausge-
führt, Die Summe der schrittweise für je zwei benachbarte Stationen berechneten Werte
von Ag um das ganze geschlossene Dreieck gebildet, übersteigt dabei nicht den Wert von
1,8. 10°, ist also geringer als zwei milliontel Teile des ganzen g. Die ganze 34 Kilometer
lange Strecke enthält 21 Stationen. Im Falle noch diehter gelegener Stationen, besonders
in jenem Teile des Gebietes, wo die Störungen grösser sind, würde das Resultat voraus-
sichtlich noch günstiger ausfallen. Ähnliche Proben, welche ich für die Beobachtungen bei
Verseez und im Fruska Gora-Gebiete anstellte, führten zu ebenso guten Ergebnissen.

Gestützt hierauf kann die Berechnung der endlichen Differenzen Ag in Angriff ge-
nommen werden.

So wurden in dem als Beispiel behandelten Falle die Differenzen der Beschleunigung
g—g, zuerst für das Dreieck ABC bestimmt, und durch solche korrigierte Werte ersetzt,
welche sich aus der gleichmässigen Verteilung des fehlerhaften Überschusses ergeben. Von
diesen Dreiecken ausgehend konnten dann diese Differenzen für sämmtliche Stationen des
untersuchten Gebietes berechnet werden. Eine systematische Ausgleichung der Beobachtungen
wurde bisher nicht ausgeführt, da ich dieselben noch nicht für ganz abgeschlossen erachte.

Die bis heute erzielten Resultate sind in der Tabelle II für —g, zusammengestellt
und auch in Form von Linien gleicher Beschleunigung (Isogammen) in die Karte (Fig. 8)
eingetragen. Der Wertabstand zweier benachbarter Isogammen ist dabei gleich zwei Ein-

7

 

I
’
4

 
