 

264

auf dem Telegraphenberge bei Potsdam gewählten Punkt erhalten, auf welchen die Werte
von Raueberg übertragen werden sollen. Zu dem Zweck ist um Berlin ein 5-seitiges Polygon
erkundet, welches in sich ausgeglichen und in welchem der Raueberg und der Punkt auf
dem Telegraphenberg sowohl in ihrer Lage gegeneinander als zu den Polygonpunkten mit
besonderer Schärfe festgelegt werden sollen. Die Verbindung des Polygons mit dem West-
preussischen Dreiecksnetz erfolgt durch eine ebenfalls bereits erkundete breite Dreieckskette.
Als Ausgangspunkt für die Längen der Seiten des Polygons ist eine Basis bei Berlin in
Aussicht genommen, welche voraussichtlich im Jahre 1908 gemessen werden wird.

In Südwestafiika wurde eine doppelte Dreieckskette von Swakopmund über Windhuk
bis in die Gegend von Gobabis, wo sie an die deutsch-englische Grenzvermessung anschliesst,
gemessen. Als Ausgangswert für die Längen der Dreiecksseiten wurde bei Windhuk mit
Invardrähten eine Basis gemessen und deren Länge durch ein Vergrösserungsnetz auf eine
Dreiecksseite übertragen. Zur Orientierung der Kette wurden in Windhuk geographische
Breite und Azimut astronomisch bestimmt; die Längendifferenz gegen Kapstadt wurde tele-
graphisch ermittelt. Die Ergebnisse befinden sich soweit sich bis jetzt übersehen lässt, in
recht guter Übereinstimmung mit den englischerseits ermittelten Werten.

B. NIVELLEMENTS.

Das im letzten Bericht erwähnte lediglich für wissenschaftliche Zwecke ausgeführte
Küstennivellement ist über Königsberg bis Pillau fortgesetzt und durch die Linie Konitz-
Dirschau erneut an den N. H. in Berlin angeschlossen worden. Die Fortführung dieses
Nivellements bis Memel ist für das nächste Jahr in Aussicht genommen.

- 0. BisonDERE ARBEITEN.

Auf Veranlassung der magnetischen Landesanstalt werden in diesem Jahre gelegent-
lich der Beobachtungen I11.0 auf etwa 2], aller beobachteten Stationen Messungen zur
Feststellung der magnetischen Deklination ausgeführt. Die Messungen sollen.in den nächsten

Jahren fortgesetzt werden.

D. VERÖFFENTLICHUNGEN.

Seit dem Bericht vom Jahre 1906 ist im Druck erschienen der XV. Teil des Werkes
»Abrisse, Koordinaten und Höhen” mit den gesamten Messungsergebnissen in dem Regierungs-

bezirk Merseburg und dem Herzogtum Anhalt.
Der XVIL. Teil desselben Werkes enthaltend die Messungsergebnisse der Resierungs-
bezirke Hannover und Hildesheim ist druckfertig, der XX. Teil für die Provinz Westfalen

ist in der Bearbeitung.

v. BERTRAB.

VERTRITT

 

 
