 

306 |

B. Gestion administrative.
1»

Le fonds des dotations a etE ger& comme d’habitude. En nous reservant le
depöt conventionnel des comptes exacts des recettes et des depenses, nous donnons ci-
dessous un apergu du mouvement des fonds pendant l’annee 1907.

Recettes.

Solde actif des fonds 4 la inde 1906... .. 2 0... 4.792 (09,02
Contributions pour 1906... 00.2 nee ne 10 600,00
Contributions pour 1907. . . . » ..,>72:59900,82

Interets: du Kur- und Neumärkische hie Darlchne
kasse a Berlin, 2 en 533,70
» du Königliche dung siehe be a» 1 171,40
Berlin Total:  M. 124 965,66

Depenses.

Indemnite au seeretaire perpetuel . . -» : sine. Nee 0 000,00
Pour le service international des latitudes (a oa ge u daualene) » 44 339,08
Da» » > » (au sud de l’&quateur) 2 AT i
Pour d’autres travaux seientifiques (determinations de la pesanteur ;
et determination de la figure de la terre) . . co... 1 211,00 i
Pour l’achat et la reparation des instruments. . . 0. 9», 185,42
Pour frais d’impression . . \ ie ee 265,25
Frais de transport, port de en a lee pedttion Be 996,00
| Total: 2 1V102069211,50
Par consequent & la fin de 1907 le solde acht cat >... NM 99294,16

Sur cette somme est depose:
aupres du »Kur- und Seuns 2. Ritter-

schaftliche Darlehnskasse” & Berlin. . . M. 15 262,00
aupres du »Königliche lag (Preus-
| sische Staatsbank)”’ & Berlin . . . - » 39.092,16
N dans la caisse du Bureau central pour les Iran
de Yadministrauon . „x. 07, 1900,00

Total: M. 55 854,16

 
