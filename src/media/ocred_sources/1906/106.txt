TINTEN DEE

 

zer

stabe zurückgetreten und als solcher durch seinen Sohn Dr. Karl Rosen ersetzt ist, verliest
seinen Bericht über die geodätischen Arbeiten mit denen man sich während der drei letzten
Jahre in Schweden beschäftigt hat. (Siehe A. XVII).

Der Präsident dankt Herrn Rosen bestens für den von ihm verlesenen interessanten
Bericht, und ertheilt Herrn Guillaume das Wort für seine Mittheilung über die Benutzung
der Invardrähte für Basismessungen.

Herr Guillaume erinnert, dass Herr General Bassot, und später die Erdmessung,
Herrn Benoit und ihn aufgefordert haben sich mit Untersuchungen über die Anwendung
der Invardrähte für Basismessungen nach der Jäderin-methode zu beschäftigen, und er theilt
die im Institute in Breteuil erhaltenen Resultate die beste Construction der Drähte und
Hülfsapparate betreffend mit. Dabei zeigt er, wie die Trommeln zur Aufwickelung der
Drähte construirt sind-und in welcher Weise man dem nachtheiligen Einfluss grosser Tempe-
raturunterschiede auf die aufgewickelten Drähte vorgebeugt hat.

Herr Böurgeois fragt, ob es nicht besser wäre hölzerne Trommeln anzuwenden.
Herr Guillaume erwidert, dass die Dimensionen der hölzernen Trommeln sich durch die Luft-
feuchtigkeit ändern, und also nachtheilig auf die Längen der Drähte einwirken würden.

Der Präsident schlägt eine Unterbrechung der Sitzung während einer Viertelstunde
vor; die Sitzung wird um 11 Uhr 50 Min. wieder aufgenommen.

In Erwiderung einer Frage des Herrn Eötvös theilt Herr Guillaume mit, wie die
Drahtenden mit den vertheilten Stäbchen verbunden sind. Auf eine Frage des Herrn Tittmann,
ob es bei Anwendung von längeren Drähten, z.B. von 72 Meter, nicht besser wäre den
Draht an zwei Stellen zwischen den Endpunkten zu unterstützen, antwortet Herr Guillaume,
dass man in verschiedenen Fällen das sicher thun könnte, aber dass es auch Fälle giebt
in welchen man, z.B. durch die sehr grossen Höhen der Stützen, diese nicht anwenden
kann; er hat deshalb auch die Methode der Messung mit langen freien Drähten studirt.

Herr Guillaume theilt nachher noch einige zur Erreichung guter Resultate praktisch
wichtigen Bemerkungen mit.

Im Anschluss an die Bemerkungen des Herrn Bourgeois über die grosse Länge-
änderungen der Invardrähte in Ecuador, erwähnt Herr Guillaume, dass diese vielleicht theil-
weise durch die hohen Temperaturen der Drähte während der Reise von Frankreich nach
Ecuador zu erklären sind. Wenn Invardrähte längere Zeit höheren Temperaturen ausgesetzt
sind, dauert es Monate bis sie wieder ihre ursprüngliche Länge erhalten haben. Dieser
Einfluss ist jedoch zu gering um die grossen beobachteten Aenderungen zu erklären.

Herr Guillaume erwähnt einen anderen Umstand, der auch vielleicht einen nach-
theiligen Einfluss ausgeübt hat. Bevor die Drähte verglichen werden, werden sie stark er-
schüttert, indem man sie wiederholt mit Kraft an den Boden schlägt. Jetzt ist in der
Regel die Zahl der Schläge für jeden Draht ungefähr 300; dagegen war diese Zahl für
die nach Ecuador versandten Drähte nur 100.

Nachdem Herr Guillaume seine Rede beendet hatte, spricht der Präsident ihm seinen
Dank aus, dass er die Güte gehabt zur Mittheilung der Resultate seiner Untersuchungen
nach Budapest zu kommen, und dankt ihm ferner im Namen der Erdmessung für seine

 
