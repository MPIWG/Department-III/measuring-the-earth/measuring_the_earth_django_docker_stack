 

302

Une variation de 1” en &, produit en a une variation de — 24 m. A la valeur &, — — 6”
correspond une longueur du demi grand axe de 6378016 m., & la valeur &, = — #",5 cor-
respond done un demi grand axe de 6377980, valeur qu’on devrait substituer provisoirement
a celle qui a &tE publide dans le rapport de 1906. Cette substitution n’a pas d’influence no-
table sur la valeur moyenne de a pour l’Europe, qui se trouve dans ce rapport,

Il convient de mentionner encore que M. le Prof. Börsch a elabor& son rapport
sur les deviations de la verticale, presente & Budapest, et qu’au mois d’Aoüt de 1907 il a envoye
au seer6taire perp6tuel le manuscrit pour la publication dans le 2° vol. des Comptes rendus de
Budapest.

3, RaAPPoRT SPECIAL SUR LE SERVICE INTERNATIONAL DES LATITUDES.

Le service international des latitudes a de nouveau bien fonetionne pendant l’annee
1907, tant sous le parallele de 39° 8 au nord de l’Equateur que sous le parallele de — 31° 55
au sud de l’Equateur.

Le nombre total des couples d’stoiles observdes pendant cette annee a öte,

au nord de l’equateur au sud de l’equateur
ä Mizousawa. . 1843
a Tschardjoui . 1981 ä Bayswater. . 1350
a @irlotorte, . 2002 a Oncatıwo > 2.2050
& Gaithersbourg 1942
a Cineinnati. . 1216
alkah. ... 2295

Les observateurs pendant l’annde 1907 etaient:

& Mizousawa: M. le Prof. H. Kımurı depuis le mois de Mars jusqu’au
mois de Decembre, M. le Dr. T. NaXAnNo jusqu’au mois de
Juillet, et M. le Dr. M. Hasırmoro depuis le mois de Juillet;

&a Tschardjoui: M. le Lieut.-Col. Dawypow jusqu’au mois de Juin et M. le
Capt. A. Ausan depuis le mois de Juin;

a Carloforte: M. le Dr. L. Vornı et M. le Dr. G.5m25

&% Gaithersbourg: M. le Dr. Frank E. Ross;

& Cineinnati: M. le Dr. Ds Lisue STEWART;

a Ukiah: M. le Dr. S. D. Townızr jusqu’au mois d’Aoüt et M. le
Dr. J. D. Mapprizı depuis le mois de Juin;

a Bayswater: M. le Dr. C. Hassen;

& Oncativo: M. le Prof. Dr. L. CARNERA.

Ainsi que dans les anndes pr&c&dentes les röductions ordinaires des observations ont
&t& faites, immediatement aprös la reception des cahiers originaux des observations, par
Vobservateur de l’Institut geodesique, M. le Prof, WanacH, avec V’assistance des calculateurs
M. W. Hanse, M. F. Jasronskı, M. A. Wisanowskı et M. V. Voszer. La plus grande

 
