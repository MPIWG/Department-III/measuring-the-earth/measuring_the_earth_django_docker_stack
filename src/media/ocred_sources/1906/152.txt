d I

In the northern territory of South Australia a trigonometrical survey of about
19000 square miles has been commenced in the neighbourhood of the Victoria river, but
the results of the survey have not yet been received.

In Vietoria two series of observations for the determination of gravity by means
of the pendulum were made at the Melbourne Observatory, one in May 1904 by Dr. Hucker,
and another in November 1905 by Lieutenant Auzssıo of the Royal Italian Navy.

In Western Australia extensive triangulations have been made. I learn fronı
Mr. Harry Jonnston, Surveyor General, and from Mr. J. S. Brockman, that the angles have
been measured with a six-inch theodolite, and the bases with steel bands. Although the
triangulation cannot be regarded as possessing geodetic aceuracy, yet a close agreement has
been found between the various bases measured in widely separated portions of the work,
and the survey has proved to be very useful in affording topographical information with
regard to the country. About 150 miles of triangulation of this order was carried out in
1905 along the Lennard River and the King Leopold range near Collier Bay.

Lieutenant Auzssıo also swung his pendulum at Perth.
G. H. Darwin.

kRGyPpYT)
by: Captain H. G, Eyoss, m. EB, FR. ’R. >

At the present time the production of maps for administrative purposes is of primary
importance, and although cadastral maps for the whole of the cultivated area have been
completed, those on smaller scales have been published for a small area only. It is therefore
necessary to complete the second order triangulation over the western half of the Delta
before any considerable progress can be made with the first or geodetie order.

In the meantime, however, a certain amount of preparatory work has been done;
the four-metre Brunner base bar has been compared with the international standards at Breteuil,
comparator houses were built last year at Helwan near Cairo, and a complete set of ap-
paratus for measuring bases with Invar wires of the JÄpERIN pattern was purchased.

During the present year comparisons between the Brunner bar and Invar four-metre
bar have been commenced, a Repsold 40 cm. theodolite has been purchased and some
preliminary investigations have been undertaken.

The completion of the second order triangulation over the western half of the Delta
will necessarily prevent much being done in geodetic survey during the coming winter, except
in the branch of preeise levelling, with which considerable progress has already been made.
But as soon as the second order triangulation is complete, more attention will be paid to
work of the highest order of preeision.

1) The Survey of Egypt being under the direction of a British Officer, it appears practically convenient to add
this note to the British Report. @.H..D.

18

 
