au

283

Au contraire, pour des mires sortant d’un local relativement humide et employees
en ete, sur le terrain, on coustaterait un raccoureissement notable durant les premieres semaines
de la campagne.

Ainsi s’expliqueraient tout naturellement les anomalies rappelees au debut de ce chapitre.

Les considerations qui precedent auraient d’ailleurs pu se deduire tout naturellement
des lois du Colonel GouLier sur la variation de longueur des mires avec la temperature et
P’humidite }).

On peut aussi une fois de plus, pour les nivellements de preeision en pays de mon-
tagnes, constater l’absolue necessite d’une determination de l’erreur metrique des mires pendant
le cours m&me des operations et le danger qu'il y aurait & se contenter d’une interpolation
proportionnelle au temps, faite pour chacune entre les deux etalonnages du debut et de la
fin de la campagne.

©. — RECHERCHES SUR LES ERREURS INSTRUMENTALES DES NIVELLEMENTS.

En vue de mieux connaitre la part des erreurs instrumentales, dans la preeision des
nivellements, nous avons commence, sur un itineraire jalonne par des reperes stables espaces
en moyenne de 120 metres les uns des autres, une serie d’experiences comparatives avec
notre niveau & prismes et nos mires & compensation, d’une part, et avec les instruments du
Coast and Geodetic Survey des Hitats-Unis de l’Amerique du Nord, d’autre part.

Les resultats detaill&s de ces experiences seront ulterieurement publies. Je puis dire
seulement que, jJusqu’ici, nous ne sommes pas encore parvenus & obtenir avec les instruments
americains la preeision r&alisee avec les appareils francais. Peut-ötre cela tient-il & ce que
nos operateurs n’ont pas encore une experience suffisante du niveau am6ricain. En tous cas,
il serait tres desirable, qu’& titre de contröle, des experiences comparatives analogues fussent
ex6cutees aux Htats-Unis. Quoiqu’il en soit d’apres les observations deja faites les erreurs
a craindre dans les nivellements de preeision, paraissent &tre d’ordre meteorologique bien
plutöt que d’ordre instrumental. Pour en mesurer et en restreindre l’influence, le procede
le moins aleatoire serait alors la reiteration des operations dans des eirconstances aussi
variees et aussi differentes que possible.

II. — NIVELLEMENTS DE DETAIL,

En 1904, 1905 et 1906, on a exeeute:

1%. — 6.400 kilometres de nivellements de 3° ordre, dans les Alpes et les Pyröndes;

2°. — 3.800 kilometres de nivellements de 4e ordre, dont 2.000 environ dans les
me&mes regions.

7. a2 ‚2 + . 2
1) Voir: Conference Generale de Bruxelles, en 1892 (Comptes-rendus, pages 664 et suivantes, et mon „Etude sur
les variations de longueur des mires de nivellement d’aprös les experienees du Colonel Gowurer”, Conference Generale
de Stuttgart, en 1898 (Comptes-rendus p. 525 et suivantes).

 
