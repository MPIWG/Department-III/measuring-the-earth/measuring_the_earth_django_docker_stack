ia

 

|
|
.s
j
k
5
“E
FE

IR
jusqu’au ler Novembre 1905, interpolees pour le temps de eulminaison de Greenwich, a ete
autographie et envoy& le 16 Octobre 1904 aux stations, afıin de mettre les observateurs en
tat de reduire leurs observations et de se rendre compte de la precision de leurs resultats.

La redaction du IIe volume des Resultats du service international des latitudes a
&t6 differde jusqu’a l’annde 1905 parce qu’il y avait avantage de röunir, cette fois, les r&-
sultats obtenus pendant une periode de trois anndes, d&puis le 5 Janvier 1902 jusqu’au 4 Janvier
1905, et d’entreprendre une discussion de tous les resultats obtenus pendant les eing premieres
anndes du service international des latitudes, en se servant de la reduction definitive de ces
observations. La redaction de ce II®e volume sera commencee sous peu.

Afın d’obtenir, avant la publication de ce volume, un resultat approximatif pour le
mouvement du pöle terrestre, j’ai determine d’une maniere provisoire l’orbite du pöle pour
la periode de 1903,0 a 1904,0 en me servant des corrections des deelinaisons des couples
d’stoiles determindes anterieurement. Les r&sultats de cette &tude, publies dans le N°. 3945
des Astronomische Nachrichten, permettent de r&duire les observations astronomiques et les
determinations de longitude, de latitude et d’azimut executees dans le courant de V’annee
1903 & la position moyenne du pöle. Une determination analogue des resultats provisoires

pour la periode 1904,0 & 1905,0 sera enterprise au printemps de 1905.
Tu. ALBRECHT.

4.

RAPPORT SPECIAL CONCERNANT LES MESURES PREPARATOIRES PRISES EN VUE
D’UNE EXTENSION DU SERVICE INTERNATIONAL DES LATITUDES.

Les resultats obtenus jusqu’ä present par le service international des latitudes ont
demontre la necessit6 d’6tendre ce service, du moins temporairement, & l’hemisphere austral.
C’est seulement par cette extension qu’il sera possible d’obtenir une base certaine pour la
solution de toutes les questions qui se presentent dans l’6tude du mouvement du pöle.

Le Bureau de l’Association g&odesique internationale a done soumis & l’approbation
des membres de la Commission permanente une proposition tendant & instituer dans l’he-
misphere austral un service limite de latitude, tel que le montant disponible du solde actif
le permettait. En attendant les resolutions & prendre par la Commission permanente, le
Bureau central a pris les mesures pr&paratoires suivantes,

Quant au choix des stations situges le plus favorablement pour r&soudre ces questions,
on avait d’abord pense & la combinaison: Sydney, Cap de bonne Esperance et Santiago dont
on s’&tait oceupe & plusieurs reprises. Dans les Comptes rendus de la conference a Lausanne,
page 153, on a deja indique que cette combinaison, dans un sens mathematique, n’offrait
pas des conditions avantageuses pour la determination aussi exacte que possible des coor-
donndes du pöle. Il y a en outre l’inconvenient que la position de l’observatoire au Cap
n’exelut pas entierement le danger des anomalies de la r&fraction, du moins temporaires.

Il parait done plus favorable de choisir, d’apres la proposition de M. Heımerr, deux
stations sur le m&me parallöle avec une difference de longitude de 180°. Ces stations se
