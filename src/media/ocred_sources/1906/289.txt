 

turen

N

 

 

 

259

 

sie ergeben fi u
& —= — 0.084 + 0.753
ul — 1 8,822 + 2.430

Die Gewichte sind dieselben wie bei II; die v unterscheiden sich von den vorigen
um höchstens 0.09, [vv] = 704.7, [nn.2] — 704.6, mittlerer Fehler einer Gleichung

+4”,42. Aus u folgt:
dan = + 562.6 m SE 155.0 m.

Nebenbei sei hervorgehoben, dass die Darstellung der älteren Verbindung zwischen
Greenwich und dem Pantheon sich denen der Ausgleichungen II und III im allgemeinen
gut anschliesst (vergleiche S. 247), und dass bei den genannten beiden Ausgleichungen,
die vor I den Vorzug verdienen, der Referenzstation Panth6on verschwindend kleine Lotab-
weichungen zukommen.

IV. Bei genügend dichter Besetzung mit astronomischen Stationen ergeben auch
kürzere Bögen, als der hier behandelte, für da Resultate, deren Unterschiede sich vergleichen
lassen; aus diesem Grunde sind auch beim neuen westeuropäischen Meridianbogen Teilbögen
untersucht worden.

Der Teilbogen Saxavord-Pantheon enthält 16 Stationen und umspannt 12°.0, die
Normalgleichungen lauten:

+ 15.9698 &, -+ 3.3525 u — 0.3155 w — 36.589 — 0
7 3.3925 — 0.9783 — 0.0638 — 11.074 —=0
— 0.3155 — 0.0638 + 0.0084 + el

Da es für den kürzeren Bogen um so weniger Sinn hat, eine Abplattung zu be-
rechnen, so wird w=( zu setzen sein und man erhält:

5 = — 0304  +1552, Gewicht 4.481
„no So » 0.2745.

Dabei ist [vv] = 151.12, [n n. 2] — 151.14, mittlerer Fehler einer Gleichung + 3”.29.

Aus u folst:
dary rn - 788.2 m oe: 399.9 m,

V. Als Endstationen eines südlichen Teilbogens wurden genommen Rosenda&l und
Laghouat, die Anzahl der Stationen ist 25, die Ausdehnung ist 17°.2; die Normalgleichungen
lauten :

 

—+ 25.0642 &p — 6.8135 u —+ 3.9780 w 6006432 0
— 6.8135 —+ 2.6806 — 1.6148 — 19.949 = 0
+ 839780 — 1.6148 —+ 0.9857 + 11.596 0.
Die Lösungen für w=0 sind:
& — — 2.0380 +1.702, Gewicht 7.746
u = + 2.282 = 5.203 » 0.8284,

 

—

u

nn nn num nn meer me ui u EEE |

A tt
