 

2

Die Reduktionen der mittleren Deklinationen der Sternpaäre auf
den scheinbaren Ort sind im wesentlichen von den Herren  Rechnungsrat
E. Menperson und Ingenieur P. ScHurze berechnet worden.

Das Verzeichnis der mittleren Deklinationen und Eigenbewegungen
der Sterne, sowie dasjenige der scheinbaren Deklinationen der Sternpaare
für das Zeitintervall ‘vom 6. ‚Januar 1906 bis 5. Januar "1908 ist den
‘Stationen unter dem 10.‘ Dezember 1906 zugesendet worden.

Die Reduktion der Beobachtungen, von Herrn Prof: Wanach unter
Mitwirkung der Herren Hexsr, Jasroxskı und "Wisanowskt "ausgeführt, ist
gegenwärtig in vollem Gange, indes "haben wegen des ‘bisher noch aus-
stehenden Kettenschlusses keine Resultate abgeleitet werden können.“

Ta. ALBREcHT.

Kooperationen zum Internationalen Breitendienste sind im Gange auf den Stern-
warten in Leiden, Pulkowa und Tokyo. Vom 1. April 1907 ab wird auch die unter
Leitung von Herrn Direktor Inses stehende Sternwarte in Johannesburg in Südafrika
an den fortlaufenden Breitenbeohachtungen teilnehmen.

Das Heft 13 der „Mitteilungen der Nikolai- Hauptsternwarte zu Pulkowo“
sibt einen wertvollen Bericht über den Gang der geogr. Br. daselbst während der
2 Jahre 1904.7—1906.7 nach zwei verschiedenen Methoden: der Kettenmethode und
dem Verfahren der fortlaufenden Beobachtung der Kulminationen von d Cassiopejae, an
demselben Instrument.

O.
Die absoluten Schwerebestimmungen in Potsdam haben ihren Abschluß
durch Herausgabe des Werkes:
„Destimmung der absoluten Größe der Schwerkraft zw Potsdam mit
Reversionspendeln, von Prof. Dr. F. Küunen und Prof. Dr. Pu. FurrwänsLer“
sefunden. Das Ergebnis für die Beschleunigung der Schwerkraft im Pendel-
saale des Geodätischen Instituts ist:
981.274 cm - sek”?.

Die mittlere Unsicherheit dieses Wertes ist =# 0.008 em. Sechs andere Be-
stimmungen konnten sehr genau dureh vorhandene relative Bestimmungen auf Potsdam
übertragen werden. Sie ergaben:

981.309 nach Barraquer, Madrid; neu reduziert: 981.270
‘981.332 „ Derrorees, Paris; = 5 981.282
981.278 „ vw. OpPpöuzer, Wien; S 3 981.273
981.212 „ Hiörenzont, Padua; e h 981.263

981.253 „ "Besser, Schumacuer und Prreas, in
Königsbere, Güldenstem und Berlin; unverändert: “981.253
„ Pe und Pucer, Rom, 5 981.274 ,

981.274

 

i
i
1
4
d
d
i

 
