1. nur
TEE TTV

TIRIRTI am Lira pen m

x
®

ae

PRUSSE,

1%. Landesaufnahme.

A. — Altitudes des reperes de jonetion avec les nivellements des pays limitrophes.

 

Noms des

 

Reperes de rattachement

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

"Altitudes Aa Definition du zero
pays Noms des localites Numeros Dösignation nn ae auquel sont rapportdes
limitrophes Br A de l’emplacement des reperes dessus du les altitudes
sont situes cules , zero-normal
prussien (?)
TE | m
Niederlande, Nieuwe Schans 1856 |Nebender Brücke, gegenüber dem) _ 2,674 (1) Zero 6tabli en 1879
Zollamt x l’Öbservatoire de Berlin,
Frensdorferhaar 5013 |Neben der Brücke an der Grenze 24.031 Hour ar oblkieatoirement
Bentheim 5021 300 m südl. des Bahnhofes 58,009 ne a
n \ Se g 2
Elten 5870 An der Grenze 15,104 tous les reperes du royaume
Dammerbruch 5873 en Z 20,953 de Prusse
Belgien Maldingen 8690 5m östl. der Grenze 514,893
Eupen 5881 An der Grenze 301,234
Frankreich | Novcant 8661 An der Grenze 181,626
Avricourt 8652 |Am Garten des Zollamts 278,753
Be , 8705 |An der östlichen Tunnelöffnung) 711,358
a \.m.B. Neben B. 8705 113,235
Altmünsterol 8647 \llm östl. vom Landesgrenzsten] 340,053
Schweiz Kiffis 6533 Nahe der Grenze, beim Nummer-| 503,315
stein 19,5
Basel 6534 An der Grenze, beim Nummer-| 258,785
stein 48,1
Württemberg Bretten 6620 Neue Strasse nach Knittlingen,| 182,941
an der Grenze
Alexanderschanze | 6649 Auf dem Kniebis an der Grenze 968,254
Bayern Kahl 6565 | An der Grenze 112,184
Elm Bolzen | Tunnel, nördl. Portal 312,840
| Obersieman 6950 200 m östl. Obersieman 315,914
Mosbach 8761 | An der Grenze 754,100
Henneberg 8765 | Dem | 489,940
Eicha 8767 | N " 313,933
Hellingen 8770 | N n 376,264
Neuhaus 8772 mn " 356,637
Probstzella 877: mm „ 361,412
Kögelmütsle 8781 75m von der Brücke an der | 495,449

 

Grenze

 

 
