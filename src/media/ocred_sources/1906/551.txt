 

75

Il ne parait pas non plus absolument d&montre que les mouvements en question soient beaucoup
moins frequents ni moins importants dans les zönes stables en apparence que dans les
regions a sismes nombreux.

Le probleme pose par le Congres geologique international et par l’Association des
Acad6mies est done l’un des plus interessants, mais, en möme temps, l’un des plus delieats
que la geodesie puisse avoir & resoudre.

Dans les quelques pages qui vont suivre, apres avoir bri&vement resume l’historique
de la question, je me bornerai & en montrer les difficultes et & suggerer quelques desiderata,
au sujet des nivellements speciaux & ex&cuter pour l’objet en vue.

I. ApuRrou HISTORIQUR.

Des 1867, dans sa deuxiöme Üonference generale, sur l’initiative des professeurs Dovz
et Sarrorıus von WALTERSHAUSEN, notre Association, qui portait alors le nom d’Association
g6odesique europeenne, signalait la possibilit de constater, au moyen de nivellements repetes
a de longs intervalles, les mouvements verticaux du sol. En m&me temps et dans le m&me
but, le Professeur von WALTERSHAUSEN proposait l’Erection de reperes fondamentaux (Ur-
marken), &tablis sur la roche ferme, dans des conditions exceptionnelles de duree et de
stabilite ').

En 1881, le troisiöme Congres international de geographie, tenu A Venise, exprimait
le vceu que l’Assoeiation geodesique europeenne voulüt bien provoquer partout la reiteration
periodique des nivellements de preeision, de maniere & faire ressortir les lentes variations
d’altitudes des reperes principaux.

En 1885-86, & la suite du grand tremblement de terre d’Agram (1880), le gourer-
nement austro-hongrois faisait reiterer, sous la direction du Lt. Colonel Franz Leuet, les
nivellements de preeision ex&eutes en 1878—79, dans la möme region, par l’Institut geogra-
phique militaire de Vienne. La comparaison des r&sultats des deux op£rations fut faite avec
un soin partieulier, mais elle ne revela que des discordances rentrant, pour la plupart, dans
l’ordre de grandeur des erreurs possibles,

En France, la comparaison du nouveau nivellement general avec le nivellement de
BOURDALOUE, ex6cute vers 1860, soit 30 anndes auparavant, semblait devoir fournir de
precieuses indications sur les mouvements Eventuels du sol, dans l’intervalle; une discordance
progressive, croissant du Sud au Nord et atteignant plus d’un metre ü Brest, avait te

 

lente del suolo, 1883), en Croatie (Dr. Pırar, Grundzüge der Abyssodynamik, Agram, 1881), en Andalousie, au Japon
(Proceedings of the sismological society, 1884);

L’apparition ou la disparition d’objets fixes lointains, derriere l’ecran forme par des collines plus proches, pheno-
mene signal: en Boheme (KorısrkA, Congres des Sciences geographique, 1875); en Espagne, dans la province de Zamora
(Botello, Les Mondes, 1870); dans le Jura francais (GrrRArDoT et Romreux, Memoires de la Societe d’Emulation du Jura,
1887); en Suisse (Jahresbericht der geographischen Gesellschaft von Bern, XII, 1894); en Thuringe, & Potsdam, en
Pomeranie et dans le Mecklenbourg (Geographische Gesellschaft zu Jena, 1893) ete. etc.

1) L’Autriche-Hongrie, comme on sait, possöde actuellement huit de ces reperes fondamentaux, dont les relations
reciproques de hauteur ont &t& determindes avec le plus grand soin.

 
