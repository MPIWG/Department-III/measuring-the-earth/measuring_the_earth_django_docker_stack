 

Setzen wir dann: AU

so bedeutet h die Erhebung der Niveaufläche über dem Normalellipsoid E, welches durch
den Anfangspunkt 21083 gelegt ist und die Zahl, welche AU in Einheiten der Ordnung
103 ausdrückt, misst auch diese Erhebungen in Centimetern mit einer Vernachlässigung
von ungefähr zwei Procenten.

Bemerken will ich noch, dass, da die Beobachtungen selbst noch zu ergänzen sind,
auch ihre Darstellung keine endgültige sein kann, so dass derselben mehr der Wert einer
übersichtlichen Skizze als einer genau ausgeführten Zeichnung beizulegen ist.

III. ANWENDUNG AUF UNTERSUCHUNGEN
ÜBER DIE MASSENVERTEILUNG IN DER ERDKRUSTE.

Die grosse Empfindlichkeit, mit welcher die Drehwage alle Abweichungen von der
konzentrischen Kugelgestalt der Erdoberfläche und auch der im Inneren der Erde gelegenen
Trennungsflächen verschieden dichter Materialen anzeigt, macht dieses Instrument besonders
dazu geeignet, Aufklärungen über Massenverteilungen zu geben, welche auch für den
Geologen von Interesse sind.

Schon das Bild, welches die Karte Fig. 8 zeigt, kann als Beleg für diese Behauptung
dienen. Man sieht dort, wie sich das von Osten herziehende Gebirg unter die alluvialen
Gebilde vertieft, ja es ist möglich auch die Grösse dieser Vertiefung annähernd anzugeben.
Wird nämlich angenommen, dass die durchschnittliche Dichte der Gesteme — 2,7 die der
daraufliegenden Alluvialgebilde — 2,0 ist, dann entspricht(nach der Formel Ag—=2#G(e—o)h)
einer Verringerung Ag= 0,001 0.6.8. annähernd eine Vertiefung des dichten Gesteines
um 38 Metern. Die um 0,002 C.G.$8. abstehenden Isoganımen erhalten so die Bedeutung von
Schichtenlinien, welche der Höhendifferenz von 76 Metern entsprechend auf einander folgen.

Ein solcher Schluss ist natürlich nur insofern zulässig, als die Annahme als berechtigt
erscheint, dass die Schwerestörungen allein durch diese eine Dichtedifferenz hervorgerufen
werden. Tatsächlich scheinen aber schon in diesem angeführten Beispiele auch tiefer ge-
legene Massen mitzuwirken, da das regelmässige Anwachsen der Beschleunigung von der
nördlichen Station 2215 in südlicher Richtung bis zur Station 2238 die Vermutung zulässt,
dass die Faltung des Gebirges auch östlich von Arad weiter zieht, und sich noch in grosser
Entfernung unter der Tiefebene fortsetzt, ja vielleicht an dem westlichen Rande des Alföld

wieder zu Tage tritt.

Es wäre aber sehr verfrüht, auf Grund des bisher zur Verfügung stehenden mangel-
haften Beobachtungsmaterials derartigen Vermutungen den Wert strenger Schlüsse beizulegen.
Mich diesmal auf eine Darstellung der Empfindlichkeit des Instrumentes beschränkend,
will ich hervorheben, dass diese je nach der Lage der anziehenden Massen bald in dem

 

 

|

basic bähsuliansbshslussenssun
