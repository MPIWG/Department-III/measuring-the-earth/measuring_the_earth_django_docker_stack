 

rn

ES ERS

ee ee

 

168

Vers le mois de juin 1904 tout &tait pröt pour commencer les observations, mais
la saison des pluies ayant öt6 cette annde d’une durde et d’une persistance extraordinaires,
on ne put rien faire jusqu’au mois de Decembre. Depuis cette &poque jusqu’en Mai 1906
on a pu obtenir 1882 observations de latitude.

La liste des 6toiles a &t6 formde en utilisant pour la plus grands partie les positions
des catalogues de Safford, de Greenwich (1890), du Coast and Geodetic Survey et de Neweomb
(1900), et elle contient douze groupes de huit paires chacun; dans le choix des couples
d’stoiles et dans la methode d’observation, nous avons toujours suiyi, autant que possible, les
instructions de l’Assoeiation geodesique internationale. Les niveaux ont &t& &tudies tres
conseieneieusement au moyen de l’appareil de M. G. Sınamörser de la maison Fauru & Cie et
on a commence & construire les courbes respectives selon le systeme de M. Srem. J’aurai
bientöt le plaisir d’envoyer a M. BAkHuyzen les premiers resultats de nos observations que

‘oe n’ai pu envoyer plus töt.
J p y
PUBLICATIONS.

Le premier volume des »Annales de la Commission G&odesique Mexicaine” que jai eu
le plaisir d’envoyer a mes honorables collägues, ne contient qu’une tres faible partie de ce
que nous avons fait reellement; car malgre mon plus ardent desir de publier regulierement

tous les resultats de nos travaux, il y a eu des diffieultes que je n’ai pas pu vaincre.
Le manuserit du deuxiöme volume est deja acheve et pröt pour la presse. D contient seulement

des travaux astronomiques.

Tacubaya, Aoüt 1906. ANGEL ANGUIANO.

ar hhnhulsnshesiunihhanu

ä
4
1
i

 

 
