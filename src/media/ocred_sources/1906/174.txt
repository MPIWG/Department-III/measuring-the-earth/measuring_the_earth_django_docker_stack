u

SET TREE TETTRTEERTENETTTN ""

 

repete les mesures angulaires aux stations du reseau d’agrandissement de la Base de Foggia.
D’apres les nouvelles donnees cette base, mesuree avec Ja toise de Dessel en 1859-60 sous
la direction de M. le Professeur Früp&rıc SCHIAVONI, s’accorde encore mieux qu’auparavant
avec la longueur de la base de Sin) mesurde par les Autrichiens. En effet, avant la revision
des mesures angulaires, on avait entre les deux bases une difference de 112 unites du sep-
tieme deeimale du logarithme !), tandis qu’a present cette difference n’est que de trois de
ces unites ?).

Ce resultat a fait voir qu'il etait interessant de repeter les mesures a toutes les
stations des rescaux de l’Italie meridionale; on a pu alors &liminer un defaut d’origine,
car, dans les anciennes triangulations les angles furent mesures avec des instruments r&pe-
titeurs d’apres des methodes qui ne permettaient pas d’obtenir une preeision rendue necessaire
par les progres de la Geodesie moderne.

En möme temps on pourra determiner plus exactement les differences qui existent
entre la Base de Crati et la Base de Catane, & laquelle est rattachee la triangulation de
Vile de Sicile mesur&e de nouveau pendant les annees 1895-97.

La revision des mesures angulaires du reseau de la Base de Foggia a aussi donne
l’oecasion de calculer les deviations de la verticale & Termoli (poiut de Laplace) et & l’ile
de Lissa (oü il y a une station rattachaut la triangulation italienne a la triangulation
autrichienne) par rapport & Genes.

On obtient pour Termoli:

a os
„= — 8,48
a) 14 ‚95
Ms 22
et pour Lissa:
8 = = 07,67
„— — 12 ‚34
ee 12 ‚36
„20880

On voit done que la deviation en latitude que nous avons supposee nulle & Gönes,
ce qui n’est pas en contradiction avec les recherches de M. Hrımenr ?), semble augmenter jusqu’ä
Termoli et diminuer brusquement a l'ile de Zissa. Ces caleuls prouvent que l’ellipsoide de
Bessel s’accorde bien avec les positions astronomiques de G£nes et de Lissa, et indiquent
en m&me temps qu’il existe probablement une cause souterraine d’aetion locale entre le
promontoire du Gargano et les iles Curzolaires.

xx

*
1) Cfr. Pubblieazione dell’ Istituto Topografico Militare. Parte I, Geodetica, faseicolo 3°. Napoli 1877-78.
2) Nuove misure angolari della rete di sviluppo della Base Geodetica di Foggia. Firenze 1904.

3) Cfr. Rapport sur les deviations de la verticale par M. HeLmerT. Comptes-rendus des sdances de la Commission
permanente etc., reunie & Nice 1887.

I 20

 
