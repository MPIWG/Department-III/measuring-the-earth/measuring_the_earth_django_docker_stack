 

226

Die erste Reihe umfasst 234 einzelne Pendelbeobachtungen, die sich in 78 Sätzen
zu je 3 Pendeln anordnen; die zweite und dritte Reihe enthalten je 48 Pendel in 16
Sätzen. Zwischen der zweiten und dritten Reihe liegen die Messungen in Genua (Aug. 1904,
51 Pendel in 17 Sätzen) und Venedig (Nov. 1904, 48 Pendel in 16 Sätzen). Bei der Ab-
leitung des Schwereunterschiedes dieser Stationen gegen Padua ist nur die Reihe Padua I
berücksichtigt worden, während die Reihen II und III gewissermassen nur als Kontrolle
für die Unveränderlichkeit der Pendel gedient haben. Die Vereinigung dieser beiden Reihen
unter Berücksiehtigung der Gewichte giebt nahezu denselben Mittelwert wie die erste
Reihe.

Das Mitschwingen ist in der Hauptschwingungsebene nach der Zweipendelmethode
bestimmt worden, und für die nur ein Lager enthaltende Schwingungsebene dadurch,
dass die Schwingungsdauer eines jeden Pendels wiederholt auf jedem der 3 Lager ermit-
telt wurde.

Die Zeitbestimmungen in Padua besorgte die Sternwarte; auf den beiden andern
Stationen führte sie Dr. Anzssıo selbst aus, und zwar in Genua auf der Sternwarte des
Königl. Hydrographischen Instituts, in Venedig auf der Sternwarte des Königl. Marine-
Arsenals. Die Hauptuhr in Venedig wurde ausserdem während der Schweremessungen mit
der Hauptuhr in Padua täglich telegraphisch verglichen. Als Koinzidenzuhren dienten in
Padua und Venedig der Chronometer Narvın N®. 20, in Genua die Sekundenpendeluhr
Fropsmam N®. 1337, die nach mittlerer Zeit reguliert war.

Alle für die relative Messung wesentlichen Elemente (Temperatur, Uhrgang, Mit-
schwingen, Veränderlichkeit der Pendel) wurden mit grosser Sorgfalt bestimmt, und die
Ergebnisse einer eingehenden Genauigkeitsuntersuchung unterworfen. Es ergab sich dabei
der m, F. einer Ag-Bestimmung für beide Stationen übereinstimmend zu + 0,003 em/sek?.

Für die Erdbodendichte in Genua und Venedig fehlen die Angaben, weshalb wir
von der Aufnahme der Grössen g”, und g’,—r, in unsre Tabelle absehen mussten.

Die Stationen Padua und Venedig sind ausserdem von Herrn General Dr. von
Sperneck und Herrn Marineleutnant vox Trivunzı besucht worden (Schwerebericht für 1900,
S. 142, 154 und 181). Wir stellen die Resultate aller Beobachter hier zusammen.

od A H I Beobachter Jahr
Padua 45° 24/1 119 5% 19 980,671 v. Sterneck 1891
24,1 52,2 19 685 v. Triulzi 1894
24,0 52,9 19 659 Alessio 1904
Venedig 45° 26,6 122 192 4 980,669 v. Sterneck 1891
25,8 20,9 2 664 v. Triulzi 1894
26,2 Da 3 638 Alessio 1904

Die g-Werte von Annssıo beziehen sich auf ein anderes System; streng vergleichbar
aber sind die Schwereunterschiede beider Stationen für alle 3 Beobachter. Mit Vernachläs-
sigung der kleinen Positionsverschiedenheiten in Venedig sind diese Unterschiede:

 

asourmnbihsuullnnsLabslissiitennnen

 
