335

IV. Die GRADMESSUNG AUF SPITZBERGEN.

Die Feldarbeiten der schwedischen Expedition wurden, wie bekannt, im Jahre 1902
beendet.

Die Publicationen der Resultate sind in Ermangelung der dazu nöthigen Arbeits-
kräfte verzögert; doch sind folgende geodätische und fysikalische Publicationen erschienen :

Tome I. Sect. II. Be Mesure des angles horizontaux et verticaux, redige par P. @. Roskx.

Tome I. Seect. V. Mare&graphe, nivellement, redige par V. CARLHRIM-GYLLENSKÖLD,

Tome I. Seet. VII. A. Leve magnetique du Spitsberg, par E. SoLanper.

Tome II. Seet. VIII. A. Observations regulieres A la station d’hivernage, par J. Wusrman.

Tome II. Seet. VIII.B. Radiation solaire, par J. Wesıman.

Tome II. Sect. VIII. Bi. Etats des glaces et de la neige, par J. Wastman.

Tome I. Sect. VIII. Br. Forme et grandeur des cristaux de neige, par J. Wusrman.

Tome II. Sect. VIII. Bur. Observations meteorologiques faites a la station de montagne, par |
J. Westmman.

Tome II. Sect. VIII. Biv. Observations meteorologiques et hydrographiques faites en mer
1899, par J. Wustman.

Tome II. Sect. VIH. BY. Observations meteorologiques en mer 1901.

Tome II. Sect. VIII.C. Aurores boreales, par J. Wusrman.

ee

urn een a a haar
FESTER TERN

Die Ausgleichung des schwedischen Dreiecksnetzes ist druckfertig.
Die übrigen fehlenden Publicationen werden erfolgen insoweit erforderliche Kräfte
dazu vorhanden sind.

P. G. Rosi.

 

 

 

 
