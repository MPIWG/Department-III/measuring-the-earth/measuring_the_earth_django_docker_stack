 

i
i
F
a
SE
i
&

15

Erreurs de clöture des polygones.

    

 

 

 

 

 

 

 

 

= Be 1
| | era de doniee Erreurs de elöture
|) 3 Ce en introduisant les r&sultats
| || sans tenir compte d’autres male
Ne.| Polygones a, parallele de 52°:
| | z a =
|
| | ; | en dans les : en | dans les
| | u longitude | angles I a longitude angles
| |
Bonn— Übagsberg— N ottuln— 3 | y = L 5 =
Brocken 2 sau ee. | — 0.02 | - 001 |+ 1.52 || — 0.01 + 0.02 |+ 0.19
2 | Brocken—Nottuln—Wilhelmsha- || |
|  ven—Kaiserberg—Lüss . . . . | 0.00 | + 0.12 |— 2.82 — — —_
3 | Brocken—Züss—Rauenberg— |)
bepnear 22. 3.0 2... | — 0.02 | + 0.04) + 114 + 0.01 + 0.09 | — 0.31
4 | Rauenberg— Lüss—Kaiserberg— || |
| Kiel—Dietrichshagen— Rugard— | |
Vogelsang 3 | 0.00 1722000212867 — r_ _
A. Börsch.

Depuis le ler Decembre, M. ingenieur diplome Fr. Köntsr de Prague et M. le
Dr. A. SemerAn de Vienne ont pris part & ces calculs, dans le but de s’instruire.

A cause d’autres travaux M. le Prof. Dr. L. Krücır n’a pu s’occuper que pendant peu
de temps des caleuls necessaires & la construction d’un systeme continu de deviations de la
verticale pour l’Europe et l’Afrique septentrionale d’apres l’ellipsoide indique dans le rapport
preeedent '). On a determine les deviations des stations de l’arc de parallele de 52° depuis
Greenwich jusqu’a Varsovie, de m&me que celles des stations indiquees dans le 1er cahier
des deviations de la verticale (Lotabweichungen, Heft I) par rapport a celles de Rauenberg
pres de Berlin.

M. le Prof. Dr. A. Garız a termine les caleuls des deviations de la verticale dans
une region autour du Brocken de 2° en latitude et 4° en longitude. A present on s’occupe
de calculs de contröle. M. le Prof. GALLE a aussi commence& les calculs d’attraction.

DETERMINATION DE LA COURBURE DU GEOIDE LE LONG DES MERIDIENS
ET DES PARALLELES.

M. le Prof. Dr. R. Scuumann a Aix-la-Chapelle ne pouvait faire avancer cette
determination que par des calculs relatifs a la courbure des meridiens dans l’Inde. Il y alieu
d’esperer que pendant l’annde 1905 les travaux pourront &tre continues avec plus d’Energie.

1) Ellipsoide de Besser dont la grande axe a a Et& augmente de „545,4: en adoptant pour la deviation locale

ä Rauenberg pres de Berlin &= 5" en latitude (vers le Nord) et A= +4” en longitude (vers /’Est).
