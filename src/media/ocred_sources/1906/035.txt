 

 

DEUXIEME SEANCE

Vendredi, le 21 Septembre.

Presidence de M. le General Bassot.
La seance est ouverte & 9 heures 45 m.
Sont presents!

les delegues, Bassot, Bakhuyzen, Darwin, Guarducei, Crema, Bouquet de la Grye,
Darboux, Poincare, Anguiano, Galarzu, Mier, Hanusse, Bourgeois, Lallemand, Cantea, Gillis,
Schiötz, Muller, Kalmdr, Heuvelink, Schorr, v. Bertrab, Schmidt, Weiss, Zachariae, Borrass,
Becker, Hecker, Eötvös, Helmert, Albrecht, Rosen, Haid, Backlund, Artamonof, Foerster,
Gautier, Börsch, Tittmann, Hayford, Porro, Tasaka, Kimura, Lehrl, Tinter, Valle;

les invites, Andres, Driencourt, Farkas, Shinjo, Antalffy, Dobrovics.
M. le Seerdtaire lit le proees-verbal de la 1! seance qui est adopte.

Sur la proposition de M. le President, M. Backlund, qui n’assistait pas a la pre-
miere seance, est nomme membre de la commission des latitudes.

M. le President propose ensuite que la commission des finances et la commission des
latitudes se mettent en rapport l’une avec l’autre, parce que les propositions de la seconde
commission pourront modifier les propositions de la commission des finances.

D’apres l’ordre du jour M. le President donne la parole & M. Helmert pour lire le
rapport sur les triangulations.

M. Helmert deelare quil a present6 un rapport complet sur les triangulations, il
ya trois ans, et que pendant la derniere periode triennale le nomıbre des triangulations ne
s’est pas beaucoup aceru; il serait donc superflu de presenter dans cette conference un
nouveau rapport, qu’il se röserve pour la prochaine conference.

 

|
i
Ä
