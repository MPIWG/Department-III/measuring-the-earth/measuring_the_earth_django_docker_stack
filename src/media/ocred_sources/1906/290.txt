 

N.

Es ist [vv] = 518.81, [nn. 2]—= 515.83; der mittlere Fehler einer Gleichung wird
+ 4".74. Aus u folgt:
dav = + 145.5 m 2397.89 008
Bevor der Meridian von Laghouat zugezogen werden konnte, war noch ein Teilbogen
zwischen Rosendaöl und Nemours mit 21 Stationen bei 15°.9 Ausdehnung ausgeglichen
worden; es ergaben sich die Normalgleichungen:

+ 21.0454 &, — 4,9023 u -- 2.7434 w + 60.229 = 0
— 4.9023 + 1.7635 — 1.0190 — 16.882 — 0
2.1434 1.0196 + 0.5984 + 9.574 =0;

sie lieferen für w—0 die Lösungen:

|
4

a 1 Co 7.418
u = +45% = 6.003 » 0.6216.

Dabei ist [vv] = 423.63, [nn.2] — 425.64, der mittlere Fehler einer Gleichung:

+4",73. Aus u folst:
da = + 292.7 m er 282.0 m,

welcher Wert die Verminderung zwischen dam und day bestätigt.

Ueber die Bedeutung dieser Achsenverbesserungen da; bis day und ihrer Unterschiede,
namentlich über die Schätzung ihrer wirklichen Unsicherheit gegenüber der hier gegebenen
rechnerischen, giebt die mehrfach genannte Hrımeersche Abhandlung »die Grösse der
Erde” auf 8. 525—583 Aufschluss.

Aachen, Sommer 1906. R. SCHUMANN.

 

ua

 

i
}
