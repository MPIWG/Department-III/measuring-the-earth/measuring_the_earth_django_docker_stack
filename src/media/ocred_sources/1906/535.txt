Transport: M. 1000,00
Honoraires pour des caleuls differents & MM. le Prof. dr. Schumann,
Hönsınnkets Von sen. a ee ee 1 100,00

mMotalı MM. 2100,00

RESUME DES ENVOIS DE PUBLICATIONS DE L’ÄSSOCIATION GEODESIQUE FAITS N
PAR LE BUREAU CENTRAL.

1. Ausgleichung des zentraleuropäischen Längennetzes. Von Professor Tu. ALBRECHT. N
Astronomische Nachrichten N? 3993[4 . . . . : =. 0.0129 De j
2. Bericht über die Tätigkeit des Centralbureaus der en Being
im Jahre 1904 nebst dem Arbeitsplan für 1905 (Neue Folge N® 11). . . 299 »
3. Rapport sur les travauw du Bureau Central de l’ Association geodesique inter-
nationale en 1904 et programme des travaus pour l’ewercice de 1905. . . 134 »

4. Provisorische Resultate des Internationalen Breitendienstes in der Zeit von
| 1904.0—1905.0. Von Professor Tu. Ausrscht. Astronomische Nachrichten
ir NOT. een, ee. 1205
9. Die Ergebnisse der eBerungen de er 1. g Mihitär- Geographischen
Instituts. I. Band. Triangulierung I. Ordnung im westlichen Teile der Mo-
narchie und den südlich anschliessenden Gebieten. Herausgegeben vom K.u.K.
Militär-Geographischen Institute. Mit 7 Tafeln . . . 80 »
6. Die Ergebnisse der Triangulierungen des K. u. K. a Coral
Instituts. II. Band. Triangulierung I. Ordnung im östlichen Teile der Mo-
narchie. Herausgegeben vom K. u. K. 0 Institute. Mit
a Taten. 80 »
1a n Ergebnisse 2. inguliane “ u. SE U Gengndshischen
Instituts. III. Band. Triangulierung Il. u. III. Ordnung in Ungarn. Heraus-
gegeben vom K. u. K. Militär-Geographischen Institute. Mit 5 Tafeln . . 80 >»
8. Mitteilungen des K. u. K. Militär-Geograplischen Instituts in Wien. Heraus-
gegeben auf Befehl des K. u. K. Reichs-Kriegsministeriums. XXIV. Band. 80 »
9. Les nouveaux appareils pour la mesure rapide des bases geodesiques; par
MM. J. Rent Benxoir, Direeteur du Bureau international des poids et mesures,

et CH. En. Gussrumm, Directeur-Adomt. 0 0 .2....0.. Jt>
d’apres le rappoıt sur les travaux du Bureau Central de 1903 p. 13... . . 2.2... M. 1045,12
Ben nn 2 » wapeaı 222.2 %„ Jene
d’apres la speeification donnee ci-dessus . ER N 1000,00
: Total: M. 21922,73
& deduire provenu de la vente dinstruments . 2 917,00 S

“ Total des frais: M. 21 005,73

 

 
