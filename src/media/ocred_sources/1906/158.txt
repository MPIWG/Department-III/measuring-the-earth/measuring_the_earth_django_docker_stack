;

F
ie
iE

 

 

ANNEXE A. IL®,

SIEISSE

Quelques donnöes sur la mesure de la base göodösique
du tunnel du Simplon en Mars 1906, communiquees a la quinzieme
conference de l’Association g6odösique internationale a Budapest.

Avec trois planches.

Les figures de la Pl. I sont extraites de »Les nowweaux appareils pour la Mesure des
bases geodesiques par MM. J. Rust Banorr, Direeteur du Bureau international des
Poids et Mesures et Cu. Ev. Gurwraume, Direeteur-adjoint. — Paris, Gauthier
Villars, 1069.

Les figures de la Pl. II paraitront dans le volume en preparation des Publications de
la Commission geodesique suisse sur la Mesure de la base du Tunnel du Simplon,
ainsi que dans la nouvelle Edition du travail de MM. Banorr et GurLLaume.

La figure de la Pl. III represente les entrees nord et sud du Tunnel du Simplon.

I. — LE TUNNEL DU SIMPLON.

La figure 11 (Pl. III) represente les entrees nord et sud du tunnel du Simplon &
l’echelle du "/oooo et domne les prineipales dimensions du tunnel telles qu’elles resultent
de la triangulation execeutee par M. Rosswmunn pour determiner la direction, la longueur,
etc., du tunnel.

Les reperes fives qui ont servi & la determination de l’axe du tunnel sont:

1°. Le signal du point nord de l’axe, au sud de la route de Brigue ä la Furka,
sur la rive droite du Rhöne ü& environ 318 metres de l’entree de la galerie de direction nord.

2°. Un repere en bronze fix& sur un socle de beton enterre et place sur la rive
gauche du Rhöne & 27m,6 de cette möme entree,

 
