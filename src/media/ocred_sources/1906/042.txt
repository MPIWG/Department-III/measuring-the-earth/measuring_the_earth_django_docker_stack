\
I
}
R
E

37

de la base du Simplon par la commission geodesique suisse, ex&eutee avec la collaboration
et sous la direction de M. Guillaume, & qui on est en grande partie redevable du succ&s
de ce travail (voir le rapport A. 11).

M. le rapporteur raconte que, par une chute d’un des operateurs portant le fil,
il s’est produit un aceroe dans le fil qui, d’apres les mesures faites immediatement avant
et apr&s cet accident, avait produit dans la longueur du fil une variation d’environ 0,5 mm.,
beaueoup moins grande que la variation indiqude par M. Bourgeois. Apres cet acceident le
fil a dtE immeliatement remplac& par un autre.

Sur une demande de M. Bourgeois, M. Gautier repond que, pendant la mesure de
la base, le fil n’a &t6 enroule et deroul& qu’une fois, tandis que pendant les operations en
Russie et dans l"Equateur le fil a &t6 enrould chaque jour. M. Gautier ajoute que M. Guillaume,
qui viendra dans peu de jours, pourra, mieux que lui, repondre aux differentes remarques
presentees par M. Bourgeois.

M. le President exprime ses remerciements a M. Gautier pour les deux rapports tres
interessants, et il annonce que la commission des finances se r&unira aujourd’hui & deux
heures et demie.

Personne ne demandant la parole la seance est levee a midi et demi.

Pendant la suspension et apres la fin de la seance, M. Hecker explique les differents
instruments dont il s’est servi pour les mesures de la pesanteur en mer et qui sont installes
dans une des salles de l’Academie.

 
