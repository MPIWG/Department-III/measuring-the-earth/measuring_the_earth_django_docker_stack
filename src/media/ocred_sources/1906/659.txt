 

168
Das Mitschwingen des Pendellagers ist mit Rücksicht auf die Anwendung der Wand-
konsole nicht bestimmt worden.
Auf die Bestimmung der Pendeltemperatur wurde grosse Sorgfalt verwendet; ge-
wöhnlich wurden nur 2 Pendel im Laufe eines Tages beobachtet, die bereits mehrere Stunden
vor ihrer Beobachtung im Arbeitskasten hingen.
Für die reduzierten Schwingungsdauern der Pendel finden sich in der dritten Mit-

 

teilung des Herrn Runzxı folgende Angaben:

Pendel 80 Pendel 81 Pendel 82
Krakau, Juni 80—Juli 5, 1903: 03.507 7586 03.507 9354* 035.507 6623
7582 9359 6629
7582 9362 6627
7593* 9348 6628
0.507 7585 0.507 9356 0.507 6627

Kiew, Aug. 10—13, 1904:

03.507 7525

05,507 9305

03,507 6565

7581 9304 6567

7524 9300 6568

0.507 7527 0.5079808 0.507 6567

Krakau, Dez. 11—16, 1904: 05.507 7544* 0.507 9855 05.507 6615
7585 9347 6621

7578 (*) 9839 6630

0.507 7574 0.5079347 0.507 6622

Die mit * versehenen Werte hat Herr Rupzkı verworfen, da der interpolierte Uhr-
gang an diesen Stellen etwas unsicherer ist. Wir halten, nach Einsicht in die uns mitge-
teilten Rechnungen, den vollständigen Ausschluss dieser Werte nicht für gerechtfertigt und
haben sie deshalb mit halbem Gewicht, den mit (*) bezeichneten Wert aber mit vollem
Gewicht zur Mittelbildung herangezogen, weil in diesem Fall der Uhrgang durchaus normal
verläuft.

Die erste Reihe in Krakau (1903) hat bereits zur Ableitung des Schwereunterschiedes
Krakau—Wien M.G.I. gedient und ist in unserm Schwerebericht für 1903, 5. 150, mit-
geteilt. Die dort angegebenen Stationsmittel der Schwingungsdauer der Pendel 80, 81 und
82 weichen von den obigen Werten um —3, —3 und —2 Einh. der 7. Dez. ab, was
seinen Grund in der nachträglichen strengeren Berechnung der Uhrgänge hat. Der im
Bericht 1903 für Krakau abgeleitete g-Wert (981,071 cm/sek?) würde demnach unter Ver-
wendung der obigen Daten eine Korrektion 39 = — 0,001 cm/sek? erhalten müssen.

Wir benutzen die erste Reihe in Krakau (1903) nur zur Ableitung der zeitlichen
Änderungen der Pendel und reduzieren mit diesen die zweite Reihe von 1904 auf die

 
