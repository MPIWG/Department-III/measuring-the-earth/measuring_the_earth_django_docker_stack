Sm EETTTERETETT Te

13 37
beitung der Beobachtungen auf der Reise Sydney—San Francisco ist begonnen.
Über die Genauigkeit der mikrometrischen Messungen lassen sich noch Keine
bestimmten Angaben machen, da noch verschiedene kleinere Instrumental-
korrektionen ermittelt werden müssen. Zu diesen gehört auch der Einfluß
der Schiffsbewegung. Um diese genauer studieren zu können, ließ ich während
der Reise durch registrierende Pendel die durch das Schlingern und Stampfen
les Schiffes hervorgerufenen Bewegungen des Barometerapparates gegen die
Vertikale aufzeichnen. Die Bewegung der Barometer in der Vertikalen ergibt
sich aus der Größe des „‚Pumpens“. Kin besonderer, nach seismometrischen
Prinzipien gebauter Apparat für Messungen letzterer Art zerbrach bereits bei
der Ausreise in der groben See des Biskaya.

Stereophotogrammetrische Aufnahmen der Wellen zur Bestimmung der
Wellenhöhe, die ich ausgeführt habe, eignen sich nicht zur Bestimmung der
Schiffsbewegung, da sie nur ein momentanes Bild der stets wechselnden Meeres-
oberfläche geben. Außerdem hängt die Bewegung des Schiffes nicht nur von
der Wellenhöhe, sondern auch noch von verschiedenen anderen Faktoren ab.

Was die Pendelbeobachtungen anlangt, so ist die definitive Reduktion
bereits erfolgt. Für diese Beobachtungen kam ein Dreipendelapparat von
Srückraru in Friedenau, und für die Zeitbestimmungen ein kleines Passagen-
instrument von Bansere in Friedenau zur Anwendung. Als Differenzen der
Schwingungsdauer ergaben sich bei den Anschlußbeobachtungen in Potsdam
vor der Ausreise und nach der Rückkehr für die benutzten 6 Pendel die

folgenden Werte in 10°’ Sekunden als Einheit:
Pendel Nr.: 16 21 D T 8 6
Potsdam I-IE: = 1 10 2 12 72 2218

Die zuerst aufgeführten Pendel haben sich also als sehr konstant
erwiesen. Pendel Nr. 6 ist wegen zu großer Veränderlichkeit ausge-
schlossen worden.

In der folgenden Übersicht sind die Ergebnisse für die einzelnen

Stationen aufgeführt.
Melbourne. Souterrain des Observatoriums.
go=—37°49 59, A— 145855 h— 26.9 m.
Beobachtet wurden 386 Pendel an 4 Tagen. Von seiten des Observatoriums

wurden an 6 Tagen Zeitbestimmungen an dem großen Meridiankreise ange-
stellt, in denen 54 Zeitsterne beobachtet wurden.

4 = 9.80002 m.

Sydney. Souterrain des Observatoriums.

= — 81’ 51 AM ERS IE WARE H- 43m

 
