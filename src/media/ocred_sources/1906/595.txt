 

i
1

|
F

 

 

 

Noms des
pays

limitrophes

 

BELGIQUE,

Altitudes des reperes de jonetion avee les nivellements des pays limitrophes.

Noms des
localites ou ils

sont situes

 

Numeros

matricules

 

Designation de

V’emplacement des reperes

 

Reperes de rattachement

 

 

Altitudes
orthometriques

|rapportdes au

zero normal
des nivelle-

ments belges(')

m

 

Altitudes
offieielles des
reperes dans
le pays voisin

 

Definition du zero
auquel sont rapportees

les altitudes

 

m

  

 

 

 

 

 

 

 

 

Eupen 5881 (a) Borne fronticre 301,574 301,234 (1) Le zero normal des nivellements belzes
Baelen L 6-1 | Bureau de la douane belge | 294,950 294,612 ee ee ni
N id. L P-19-11 |Rivet ü eöte de la borne 184| 302,645 302,304 Are DD erfode de 187 ä 1885.
Welkenraedt RG.17% Eglise, cöte gauche du por- 255,506 255,164 Il est defini par la cote 3m,658 assignde au
tail busc de l’&eluse du Bassin du Commerce,
Berbesthal = Ferme „Gute Weide”; rep.| 266,978 266,636 par rapport & ce zEro.
4 | prineipal X Sans D’aprös la moyenne gEn6rale des 27 anndes
id. | PER Meme bätiment; boulon 266,388 266,046 @observations du m@me mardgraphe (1578—
Allemagne Welkenraedt L P-17-VI Maison Teller 266,208 265,366 1905 inclus), le niveau moyen de la mer &
Herbesthal N Maison n°. 99 269,466 269,124 Ostende se trouverait & 0m,006 au-dessus
Welkenraedt LP-18 Chapelle 282,437 982,095 du zero normal.
Herbesthal 5 Maison, angle du chemin de 284,848 284,509
Baelen \ {
id. | DPABTIE | Morun fontire 1eG | 288060 | 283517 |, tere Lemenke me 16 iii =
Da 32a) F = 1 I dessus du niveau moyen de la merä Ostende.
id. B rontiere, Aqueduc 51/5,0« 514,6
id % Frontiere, borne 87 521,105 520,769
id. L 6 36-13 Borne kilt 20 509,567 509,231 (a) Reptre de la Landesaufnahne.
id. L 6-36", Ecole communale 500,514 500,182
Buche nen Cabaret „Au Potenn” | 3m348 3m.212 (6) N
» DE Eelusette 1 ‚454 1 ,322(c) | D’apres V’ensemble des 12 altitudes
Baisieux B ab-® (2) Maison de Garde n°, 18 30 ‚491 30 ‚328 (c) jci-contre, le niveau moyen de la Me-
id. B ab-14(6) Passage ü niveau n°. 17. 81 ‚431 31 ‚269 (ec) |diterrande & Marseille serait & Om,151
Blanc-Misseron B be-® (D) Gare 31 ‚176 31 ‚009 (ce) lau-dessus du niveau moyen de la mer
id. B be &-L(b) Pont 29 ‚124 29 ‚558 (c) du Nord & Ostende.
nt id. ' Bbe ®-IL(d) Pont de l’Aunelle 29 ‚075 28 ‚910 (ce) i Ir
zanes Heer-Agimont | CLN Maison du sieur Gillis 102 ‚916 | 102 ‚737 (ce) | (6) Repere du nivellement de preei-
id. I" OL49-Ts Aqueduc du ruisscau du | 101 ‚089 100 ‚910 (ec) |sion de la France.
Bas-Pre
Mont St. Martin AB® (b) Passage & niveau n° 9 262 ‚138 262 ‚016(c) | (ec) Altitude orthometrique.
id. A B-®-L (2) Aqueduc 262 ‚304 262 ‚183 (ec)
id. A B-®-11 (b) Poneeau sur le ruisseau 262 ‚072 261 ‚951 (e)
Al ah | d’Aubange |
| m m “ RN
Heyst 117550/(@) Eglise 8,338 5,051 D’apres l’ensemble des 36 altitudes
vl. 55l (A) id. (seuil) 3,857 3,569  jci-contre ’Amsterdamsche-peil serait &
id. 552 (d) Canal Leopold 3,969 3,642 0m,320 au-dessus du niveau moyen de
id. 553 (d) id. 6,120 DI la mer & Ostende.
id. 554 (d) id. 6,565 6,243 }
PER id. | II 399 (d) Canal de Selzaete 5,073 4,152 (d) Repere du nivellement des Pays-
ee id. | 400 (d) ‚Canal de derivation de la Lys 2,572 2,246 Bas.
Ramscapelle I 546 (d) | Eglise 3,935 3,611
id. 548 (d) Canal 5,461 5,150
id. 549 (d) Maison pres de la borne Bo 0,767
Kllıa
Westcapelle I 541(d) |Pont sur le „Vuyle Vaart” 1,382 1,058
id. 542 (d) Schaepenbrug 1,891 1,573
