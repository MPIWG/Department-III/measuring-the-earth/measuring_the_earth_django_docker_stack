 

BEILAGE B. X,

BERICHT

über die relativen Messungen der Schwerkraft mit Pendelapparaten
für den Zeitraum von 1903 bis 1906.

ERSTATTET VON

E. BORRASS.

Der vorliegende Bericht erstreckt sich über alle relativen Messungen der Schwerkraft,
die in dem Zeitraum von 1903 bis 1906 zur Kenntnis des Zentralbureaus gelangten; er
umfasst also auch die Messungen, die zeitlich noch in die vorangehende Berichtsperiode
(1900 bis 1903) fallen, aber, wegen verspäteter Mitteilung, im Bericht für 1903 nicht mehr
berücksichtigt werden konnten. Ausserdem wurden einige ältere Arbeiten, deren vorläufige
Ergebnisse in früheren Berichten mitgeteilt sind, von neuem aufgenommen, nachdem
inzwischen ihre definitiven Resultate veröffentlicht worden sind. Diese notwendigen
Neuaufnahmen älterer Arbeiten sind sowohl im Text als auch in den Tabellen des vorliegenden
Berichts gesondert aufgeführt und durch die Bezeichnung: »Korrektur zum Bericht von
(1900 oder 1903)« kenntlich gemacht.

Die tabellarische Zusammenstellung der neuen Arbeiten schliesst sich in ihrer all-
gemeinen Bezeichnung und innern Gruppierung eng an die Berichte von 1900 und 1903
an, so dass alle hier vorkommenden Tabellen als zeitliche Fortsetzungen der früheren er-
scheinen. Dementsprechend ist auch die Numerierung der neuen Arbeiten innerhalb der
einzelnen Gruppen im Anschluss an die frühere weiter geführt worden.

Zur Kennzeichnung wiederholt bearbeiteter Stationen ist die Stationsnummer in der
ersten Spalte der betreffenden Tabelle mit einem * versehen worden; Nummern ohne *
bezeichnen demnach neue Stationen.

Die Berechnung der Schwerestörungen g,—y, und g’,—r, gegen das Hernerr’sche
Normalsphäroid von 1901 ist, wie im Bericht von 1903, nach den Formeln

Yo —= 978.046 $1 + 0.005302 sin? @ — 0.000007 sin? 29%
%9=9I E- 10-7 X 3086 H

a 30
ia WU =d

 

 
