ua anne a wi hut var=

irl

 

EN

 

1896 Pendel 89 Pendel 90 Pendel 91
Wien, M.@.I. Mai 5 035,507 8185 05,507 7487 05,507 7005
> 8 8169 7448 6971
ST 8200 7476 7001
3 8196 7501 7035
3 818l 7458 7001
» 10 8185 7480 7001
Mittel 0,507 8186 0,507 7470 0.507 7002
München, Stw. Juni 11 03,507 8508 03,507 7810 03,507 7206
se ı 8494 7779 7204
Juli A 8495 daad 7224
» 4 8483 7764 71234
Mittel: 0,5078495 0,507 7782 0,507 7217
1896, München — Wien: + 309 = 912 + 215.

Die Messungen mit den Pendel 91 mussten verworfen werden, weil sich dieses Pendel
zwischen Wien und München sprungweise um rund ‚100 Einh. der 7. Dez. geändert hatte;
aber auch die beiden andern Pendel zeigten bei ihrer weiteren Beobachtung in München,
die Prof. Axpıne in den Monaten August, September und Oktober 1896 fortsetzte, ziemlich
starke Änderungen. Prof. Anvıng fand jedoch, dass sich diese durch lineare Zeitglieder nahezu
darstellen liessen; er leitete deshalb aus allen Münchener Beobachtungen des Jahres 1896
Interpolationsformeln für die Pendel 89 und 90 ab, aus denen er die Münchener Schwin-
gungsdauern dieser Pendel für die Epoche der Wiener Messungen extrapolierte. Danach
erhalten die obigen Stationsunterschiede + 309 und + 312 noch Korrektionen wegen Ver-
änderlichkeit der Pendel von + 21 und + 22 Einh. der 7. Dez., so dass sich als End-
resultat ergiebt

1896, München— Wien = + 332 x 10-7 sek = — 0,128 cm/sek?.

Genau denselben Wert hat Prof. Anpıne bei der zweiten, vollwertigen Verbindung der beiden
Stationen im Jahre 1899 erhalten.

Nach 1896 sind die Veränderungen (Verkürzungen) der 3 Pendel erheblich geringer
und werden überall durch eng einschliessende Referenzbeobachtungen in München für die
Aussenstationen unschädlich gemacht. Wir geben hier nur einen Überblick über die Zeit
und den Umfang der Messungen auf den Hauptstationen Wien, München und Potsdam,
sowie auf den Doppelstationen Innsbruck und Koburg, und stellen zum Schluss die Anpıng-
schen Ergebnisse für diese Stationen mit den Resultaten anderer Beobachter zusammen.

 
