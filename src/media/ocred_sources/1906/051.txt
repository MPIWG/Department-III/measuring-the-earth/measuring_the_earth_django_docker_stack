46

M. le President remercie vivement M. Rosen de son interessant rapport et donne la
parole a M. Guillaume pour sa communication sur Yemploi des fils d’invar pour la mesure
des bases.

M. Guillaume rappelle que c’est M. le General Bassot, puis ’Association geodesique,
qui ont engage M. Benoit et lui & entreprendre des recherches sur l’emploi des fils d’invar
pour la mesure des bases d’apres la methode Jäderin, et il indique les resultats auxquels
on est parvenu & l’Institut de Breteuil, par rapport & la meilleure construction des fils et
des appareils auxiliaires. Il fait connaitre la construction des tambours pour l’enroulage des
fils, et il indique le moyen qui sert & &liminer l’influence nuisible d’une grande variation de
temperature sur la longueur du fil enroule.

M. Bourgeois pose la question de savoir s’il ne serait pas preferable d’employer des
tambours en bois. M. Guillaume repond que les dimensions des tambours en bois change-
raient par les variations de l’6tat hygromätrique de l’air, ce qui aurait une influence fächeuse.

N. le Prösident propose de suspendre la s&ance pendant un quart d’heure.

La seance est reprise & 11 h. 50 min.

M. Guillaume, en reponse & une demande de M. Eötrös, donne des renseignements
sur la liaison du fil aux röglettes. A M. Titimann, qui lui avait demande s’il n’etait pas pre-
ferable, en employant des fils d’une grande longueur p. e. de 72 mötres, de supporter le
fil en deux points entre les deux reperes, M. Guillaume fait remarquer que certainement
on pourrait le faire en plusieurs cas, mais qu’il y a aussi des cas oüı les appuis ne sauraient
ötre employes, p. e. & cause de leur grande hauteur; il a done &tudie aussi la meilleure
möthode pour employer les fils longs sans appui.

M. Guillaume donne ensuite quelques indieations pratiques, dont il faut tenir compte
quand on veut obtenir de bons resultats.

Rappelant les variations assez grandes que, d’apres la communication de M. Bourgeo:s,
les longueurs des fils avaient subies & \’Equateur, M. Guillaume fait remarquer qu’une partie
de ces variations peut ötre expliqude par l’influence de la haute temperature & laquelle les
fils avaient 6t6 exposes pendant leur transport de la France & l’Equateur. Quand les fils
d’invar ont dt6 exposes pendant un certain temps A une temperature elevee, il faut quelques
mois pour que les fils reprennent leurs longueurs normales. Oette influence est pourtant trop
faible pour pouvoir expliquer les grandes varlations qu’on a observees.

Il y a, d’apres M. Guillaume, une autre eirconstance qui a eu peut-ätre une influence
fächeuse. Avant que les fils soient compares, on les expose ü des secousses Energiques, par
un grand nombre de battages contre le plancher. A present le nombre des battages est
d’environ 300, tandis que les fils destines & l’Equateur n’ont subi que 100 choc».

M. Guillaume ayant termine son discours M. le President le remercie d’abord d’etre
venu & Budapest pour faire connaitre les resultats de ses recherches, et lui presente ensuite
au nom de l’Association, ses chaleureux remerciments pour son brillant discours et pour les
grands services qu’il a rendus & la göodesie par ses recherches. Selon Vavis du president il
ressort de ces recherches que l’emploi des fils d’invar est d’une grande importance pour la
mesure des bases, & condition qu’on emploie plusieurs fils pour la mesure de chaque base.
