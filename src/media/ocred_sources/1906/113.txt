 

 

standen um unsere Arbeiten zu erleichtern und uns und die Personen, welche uns be-
gleiten, wo möglich zu Dienste zu sein”.

Die Delegirten erheben sich. Sehr lebhafter Beifall.

»Da Niemand mehr das Wort verlangt, werde ich sofort unsere Sitzung schliessen,
aber ich möchte doch hinweisen auf die Wichtigkeit der abgehaltenen Konferenz. Niemals
vorher hatten wir von so viele Arbeiten Kenntnis zu nehmen, und es ist sicher, dass die
internationale Erdmessung durch die Cooperation aller betheiligten Staaten eine grosse
Ausbreitung erhalten hat. Unsere Organisation die, wie der Herr Sekretär bemerkte,
seit 1896 democratischer ist als früher, hat sich als eine gute Grundlage für unsere Ar-
beiten bewährt, und auch in finanzieller Hinsicht können wir mit Vertrauen der Zukunft
entgegengehen”.

Herr Weiss bittet ums Wort um dem Präsidium zu danken, erst Herrn General Bassot,
der als Präsident in vortrefllicher Weise unsere Berathungen geleitet hat, und ferner dem
ständigen Sekretär, der stets unermüdlich im Interesse unserer Association beschäftigt war.
Beifall.

Der Präsident dankt Herrn Weiss für seine liebenswürdigen Worte.

Der Sekretär bittet das Präsidium zu ermächtigen, das Protokoll dieser letzten Sitzung
festzustellen.

Da Niemand das Wort verlangt, erklärt der Präsident die 15. Generalkonferenz der
internationalen Erdmessung für geschlossen.

Die Sitzung wird 12 Uhr aufgehoben.

 

3

 
