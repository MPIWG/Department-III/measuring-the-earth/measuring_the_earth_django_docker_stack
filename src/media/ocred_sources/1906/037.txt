 

 

32

M. le President remereie M. Lallemand et donne la parole a M. Darwin pour lire
son rapport sur les maregraphes. M. Darwin dit qwil n’aurait & ajouter que fort peu a son
rapport prösente a Copenhague, de sorte qu’il n’a pas jug& necessaire de faire un nouveau
rapport pour cette conference.

M. Albrecht qui avait et6 invite par M. le President & lire son rapport sur les
determinations de longitude, de latitude et d’azimut, fait une deelaration analogue.

M. le President donne la parole a M. Börsch pour la lecture de son rapport sur les
deviations de la verticale.

M. Börsch dit que depuis les trois dernieres anndes on a determine un assez grand
nombre de deviations de la verticale dans presque tous les pays, de sorte qu’il peut presenter
1 cette conference un nouveau rapport (voir B.X). Il y indique les nouvelles publications
qui ont paru pendant cette periode, et rend compte des rösultats acquis. M. le rapporteur
donne ensuite un apercu un peu plus detaill& du Sme eahier des deviations de la verticale
(Illes Heft der Lotabweichungen) qu'il vient de publier, et dans lequel il expose les r&sultats
deduits du ealcul d’un reseau astronomique et geodesique dans la partie septentrionale de
l’Allemagne et dans le Danemark.

M. le President vemercie M. Börsch de son interessant rapport et donne la parole
a M. Borrass pour son rapport sur les determinations relatives de la pesanteur.

M. Borrass presente son rapport (voir B. XD. Il y indique que, pendant Ja derniere
periode triennale, la valeur relative de la pesanteur a &t& determinee dans environ 300 stations;
on y a employe presque exelusivement la methode introduite par M. le general von Sterneck
qu’il remercie du grand service que, par cette methode, il a rendu & la geodesie. Gräce aux
differents perfectionnements apportes aux instruments, surtout par l’introduction de l’appareil &
quatre pendules, on peut atteindre dans la determination relative de la pesanteur une pre-
eision allant jusqu’& Ygcoono, meme jusqu’ä "000 000- M. Borrass signale les deviations extra-
ordinaires observees au Pamir qui permettent de tirer des conelusions fort interessantes par
rapport a la constitution de notre globe.

M. le President remereie M. Borrass de son interessant rapport et donne la parole &
M. Helmert. :

. M. Helmert ajoute au rapport de M. Borrass quelques remarques sur l’intensite ab-
solue de la pesanteur & Potsdam, d&terminde au moyen d’observations faites avee 5 pendules,
1 Potsdam möme, et au moyen d’observations faites dans d’autres stations qui ont ete rattachees
& Potsdam par des determinations relatives.

Dans la reduction de ces observations M. Helmert a tenu compte des erreurs provenant
des mouvements du couteau indiques par M. Deforges ct aussi de l’erreur döpendant de la flexion,
dont M. Lorenzoni avait donne une theorie qui n’stait pas entierement exacte. En traitant cette
question d’une maniere plus rigoureuse, comme celle que M. Helmert indique dans sa publication
sur le pendule & reversion, il a obtenu des rösultats assez exacts en se servant de l’integration
par quadratures. En apportant toutes ces corrections aux resultats obtenus dans les autres
stations, il obtient des valeurs fort concordantes pour l’intensite de la pesanteur & Potsdam.
Il en ressort que l’on conuait & present cette valeur avec une preeision d’au moins !/,00000

 

i

re
