ET ET TRETEN N

 

97

Der Präsident dankt Herrn Galarza und fragt, ob einer der Delegirten einen Bericht über
die geodätischen Arbeiten in Griechenland vorlegen kann ; da sich Niemand meldet, fordert der
Präsident Herrn Bodola auf seinen Bericht über die geodätischen Arbeiten in Ungarn zu verlesen.

Herr Bodola erklärt, dass er in Bezug auf die in Ungarn ausgeführten geodätischen
Arbeiten dem Bericht des Herrn Lehrl über die Arbeiten des militär-geographischen
Instituts und dem Bericht des Herrn Baron Eötvös über seine eigenen Arbeiten nichts bin-
zuzufügen habe.

Der Präsident ertheilt dem italienischen Delegirten Herrn Crema das Wort, der einen
Bericht über die Arbeiten des italienischen militär-geographischen Instituts in Florenz und
einen Bericht über die Arbeiten der italienischen geodätischen Commission vorlegt. (Siehe
A.IVa und A.IVD).

Der Präsident dankt Herrn Crema für die beiden interessanten Berichte und ertheilt
Herrn Darboux das Wort.

Auf Grund des grossen Interesses einer Verbindung der französischen und italienischen
Dreiecksnetze über die Insel Corsika, und der Dreiecksnetze in Algerien und Italien über
Tunesien, spricht Herr Darboux den Wunsch aus, dass die französischen und italienischen Re-
gierungen sich zu der Ausführung der dazu benöthigten Arbeiten einigen möchten.

Der Präsident bemerkt, dass schon vor vielen Jahren in einer Generalkonferenz der
Wunsch einer Verbindung der französischen und italienischen Dreiecksnetze über Corsica
geäussert ist; er fordert Herrn Darbour auf seinen Wunsch in der letzten Sitzung vorzu-
bringen. Die Verbindung der Dreiecksmessungen in Italien und Algerien durch Tunesien
ist schon'zu Stande gebracht.

Der Präsident ertheilt Herrn General Tasaka das Wort für seinen Bericht über die
geodätischen Arbeiten in Japan. ;

Herr Tusaka theilt mit, dass in den letzten drei Jahren diese Arbeiten nur in be-
schränktem Maasstabe ausgeführt sind, da während längerer Zeit, Officiere und andere
Beambte des geodätischen Dienstes mit anderen Arbeiten beschäftigt waren. Es sind jedoch
astronomische Beobachtungen, Präeisionsnivellements, Schwerebestimmungen und Dreiecks-
messungen ausgeführt worden. (Siehe A. V).

Der Präsident dankt Herrn Tasaka für seinen interessanten Bericht und ertheilt
Herrn Anguiano das Wort.

Herr Angwiano verliest seinen Berieht über die in Mexico angestellten Arbeiten, Basis-
messungen, Triangulationen, astronomische Beobachtungen, Präcisionsnivellements, Schwere-
bestimmungen, Bestimmungen der Breitenvariation. (Siehe A. V]).

Der Präsident dankt Herrn Anguiano für seinen so inhaltsreichen Bericht, woraus
man ersieht mit welcher Energie die Arbeiten in Mexico fortgesetzt werden, und ertheilt
dem Herrn Delegirten aus Norwegen das Wort.

Herr Schiötz verliest den Bericht über die in seinem Lande ausgeführten Arbeiten.
(Siehe A. VII).

Nachdem der Präsident Herrn Schiötz gedankt hat, ertheilt er den Herren Delegirten
der Niederlande das Wort.

I 13

 
