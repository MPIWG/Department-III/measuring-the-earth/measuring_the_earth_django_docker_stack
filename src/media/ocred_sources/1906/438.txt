 

 

380

 

Dann schreiten wir weiter, und indem in den Indexwerten a=2, b=3 und c=4
gesetzt wird, berechnen wir aus den Gleichungen 16.) die Werte &,, und n,,, daunna—= 3,
b=4,c—5 gesetzt die Werte von &,, und ,, und so weiter.

Diese Art der Berechnung wurde für die Beobachtungen des Arader Gebiets tat-
sächlich ausgeführt und zwar für eine 91 Kilometer lange Zikzaklinie, welche mit 40 Sta-
tionen ein nahezu gleichschenkeliges Dreieck umschliesst. Dieses ganze geschlossene Dreiecks-
netz ist auf der Übersichtskarte (Fig. 7) sichtbar gemacht.

Die Rechnung geschah mit jenem Systeme von Werten, welches wir topografische

2 U 0 U\m 92
Störungswerte nannten, also ausgehend von den Werten u =) (5; L
y° 08 9E0

Dementsprechend erhalten wir als Ergebniss der Rechnung auch solche Störungs-

werte für
OU \m U \u
er) 5 (Bel

a \
Rn Node

Wenn also mit A und Ar die Lotablenkungen in einem Punkte bezeichnet werden,

so sind diese:

und

 

i 1 ci
Au=-| <> 9
& FawJ:
und
nd nn
a

Die Rechnung führt also zur Bestimmung von:

i DU \u ii en
nn in i

1/2 U wm
Ba -.( m 5

17.)

 

 

aım

d. i. zur Bestimmung der relativen Lotablenkungen.

Die Rechnungsresultate sind in der beifolgenden Tabelle III derart zusammengestellt,
dass in der zweiten und dritten Columne die Wertdifferenzen für je zwei zu einem Linien-
stück gehörende Nachbarpunkte angegeben sind, während die fünfte und sechste Columne
die Differenzen der einzelnen Stationswerte von gemeinsamen Anfangswerten enthält. Als

9° oU

ala ; U un
solche Anfangswerte wurden gewählt für (5 der Stationswert hm für en

Sen rrohhssslhalknnshahsisaihhssnnnn

 

i
!
F

 
