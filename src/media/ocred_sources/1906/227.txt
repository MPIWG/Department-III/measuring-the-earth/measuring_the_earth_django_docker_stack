 

5
N
%
\
$
Fi
i
r
Ei
&

Eng e

en

200
(8) For the United States and adjacent areas, if the isostatic compensation is uni-
formly distributed with respect to depth, the most probable value of the limiting depth
is 71 miles (114 kilometers), and it is practically certain that the limiting depth is not
less than 50 miles (80 kilometers) nor more than 100 miles (160 kilometers).
(4) For the United States and adjacent areas the average departure from perfect
isostatie compensation is less than one-tenth, as measured by the magnitude of the average

unexplained deflections of the vertical.

DATA USED IN THE INVESTIGATION.

The triangulation used in this investigation includes the transcontinental triangulation
from New Jersey to California, the western oblique are covering three-fourths of the length
of California, the eastern oblique are from Maine to Louisiana, the triangulation by the
Lake Survey in the vieinity of the Great Lakes and comprised mainly within the States
of New York, Ohio, Indiana, Illinois, Wisconsin, and Michigan, and triangulation not included
in tbe preceding items, but extending over various portions of New England, southern
Maryland, eastern Virginia, North Carolina, and Tennessee.

All this triangulation has been reduced to the United States Standard Datum; that
is, the latitudes, longitudes, and azimuths have been computed continuously through it on
the basis of the assumption that the latitude of the triangulation station Meades Ranch
(in Kansas) is 89° 13° 26.686, its longitude is 98° 32° 30”,506, and the azimuth of the
line, Meades Ranch to Waldo, is 75° 28’ 14”.52. The computations are all based upon the
Clarke spheroid of 1866.

In the investigation 507 astronomie determinations have been used — 265 of latitude,
79 of longitude, and 163 of azimuth. Eleven of the determinations of longitude were made
at stations praetically coincident with stations at which determinations of auimuth were
made. Henee the 507 astronomie determinations furnish that component of the deflection
of the vertical which lies in the meridian at 265 stalions, and the prime vertical component
at 231 stations.

These astrononie determinations are distributed, though not unifornly, over the
whole area covered by the triangulation referred to above. They are scattered over 33
States. The extremes in latitude are 48° 47’ at St. Ignace, on the northern shore of Lake
Superior, and 99° 57’ at New Orleans, Louisiana. "The extremes in longitude are 67. 16%
at-Calais, Maine, and 124° 24 at Cape Mendoeino, California.

For nearly all the astronomie stations the geodetic positions and the results of the
astronomie observations were printed in the report rendered to the Association at Copenhagen ').
T'he astronomie observations indicated by the following lists, which are in tbe same form
as those referred to above, were added during the progress of the investigation:

Vol. 1, pp. 193 — 206.

1) See Report of the Fourteenth General Conference of the International Geodetic Association,

ah ana

 
