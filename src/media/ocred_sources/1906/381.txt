IRRE

ie
E

 

325

d’obtenir des points fondamentaux pour la determination des points geographiques & l’aide
de chronometres; ces derniers points serviront de points de depart pour les levers & petite
Echelle.

Vu l’immensite et la faible eulture du territoire, un lever ä l’Echelle 1: 84000 sufit
pleinement aux besoins actuels; les exigences de pareille Echelle sont sufflsamment satisfaites
par des determinations astronomiques a l’aide de chronometres de precision, ces travaux
exigent un personnel peu nombreux et peuvent se faire a peu de frais.

Les determinations de l’heure ont ete faites exclusivement d’apres la methode de
M. le professeur N. Zıxger, publiee en 1874 dans l’Appendice au tome XXV des M&moires
de l’Academie Imperiale des sciences sous le titre » Determination de l’heure d’apres les
hauteurs correspondantes de diverses etoiles’’ et traduite en 1877 en allemand par Hxınkıca
KELCHNER !).

L’emploi de cette methode a &t€ rendu plus commode par les professeurs Korrazzı?)
et Wırrram°) qui ont compose des tables auxiliaires, et par le lieutenant-colonel Curtcautkin ?)
qui a compose d’apres les tables de Wırrram des ephemerides d’etoiles tellement completes
que pour la zone 39°—61° de latitude Nord les determinations de l’heure peuvent se faire
immediatement sans aucun caleul prealable.

Les latitudes ont &t& observdes le plus souvent d’apres la methode des hauteurs
correspondantes d’etoiles, proposee par le general Pıryısow. Cette methode a &t& pour la
premiere fois publiee en 1877 dans les M&moires de la Societe Imperiale russe de geogra-
phie°). En 1899 M. Pırvrsow a publie un second m&moire avec un catalogue des couples
d’etoiles pour la zone 35°—65° lat. N °). Cette methode, Eprouvee aux observatoires de
Kharkow et de Varsovie, a donn& des resultats excellents.

Depuis 1893 on a commenc& & se servir du petit cercle vertical de Rersonn (diametre
du limbe 15em.2, ouverture de l’objeetif 3em.38, division du niveau du limbe vertical 1”.5).
En pratique cet instrument donne des r&sultats d’une exactitude tr&s peu inferieure A ceux
obtenus avec le cercle vertical de RzrsorLn grand type; en möme temps, gräce au petit
volume et au faible poids ') de instrument, le deplacement des observateurs est beaucoup
facılite dans les contrees montagneuses et forestieres de la Siberie et du Turkestan.

1) Die Zeitbestimmung aus correspondirenden Höhen ‚verschiedener Sterne von N. ZiNGEr. Aus dem russischen
übersetzt von Heınr. KELcHNer. Leipzig, 1877.

2) J. Korrazzı. Hülfstafeln zur Berechnung oertlicher Ephemeriden für die Zeitbestimmungen der Zinenr’schen
Methode. 1891.

3) Dr. Wırrram. Tables auxiliaires pour la determination de l’heure par des hauteurs correspondantes de diffe-
rentes etoiles. 1892.

4 N. Curcunreın. Ephemerides d’etoiles pour la determination de l’heure d’apres la methode du professeur
N. Zıinger. 1902 (en russe).

5) M. Pıevısow. Sur la determination de la latitude geographique d’apıes les hauteurs correspondantes de deux
etoiles. Me&moires de la Soc. Imp. russe de geogr. 1877 (en russe).

6) M. Pınvısow. Determination de la latitude d’apres les hauteurs correspondantes de deux &toiles. St. Peters-
bourg. 1899 (en russe).

7) Dimensions de la caisse: 36 X 30 X 35 em.; poids de l’instrument: sans caisse, environ 8 kgr.; avec caisse,
environ 18 kgr.

 
