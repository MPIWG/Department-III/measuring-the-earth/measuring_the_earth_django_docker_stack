   

i
’
i
i
}

288 i

EG ER EEN !

Les cotes du niveau moyen, exelusivement deduites des observations des medimare-

i mötres, et rapportees au niveau moyen fourni par le mödimardmötre N® 1 de l’observatoire
marögraphique de Marseille, sont donnees dans le tableau ei-apres. Ces cotes ont et& cal-
} euldes au 1er Janvier 1906, d’apres l’altitude rationnelle !) du zero de chaque appareil.

 
 

u CR. Mi 7 Fl Anm SER
Al Cote du niveau

ebeniree
moyen, par rapport Date d’entree
M£dimaremetres Ah celui de en fonetion

Marseille, au ler
Janvier 1906

Mm m nn

de Pappareil

 

Manche Centimetres

| habe de. in... 10 1891

hi Ocean

j Canaria ae — 3 1890 >
Quiberon Wa man... 2058 1889

N nes SablesadkOlonnerr 2.0. — 4 1892

\ arPalleem une. LO) 18912)

| Biarritz 0 2. en. + 17 18893)

I! Str Jeandeluuzenen ne .n: EM 1890

Mediterrane

 

 

ı Port Menduesee a... — |] 1888
| een a 1888
Pontäde Bouere nn rn 3 1594
Marlseues 0 a. a... — ] 1894
{ Marseille (Port Vieux)........- +1 1890 |
! Marseille (Anse Calvo) i
Appareil N® I.............- 0 1885 i
j Appareil N® 2.............. ll 1890 |
aa @iobat eh es ee. er +5 18953 E
Niere 0 or reeeen. — 6 1888

Paris le 31 decembre 1906.

1) Les altitudes rationnelles resultent de la compensation simultande des reseaux de ler et de 2© ordres du
Nivellement general de la France. (Voir Comptes-rendus de la Conference de l’Association G&odesique internationale, tenue
* Paris en 1900, ler volume, pag. 184).

2) Interruptions pendant les anndes 1904 et 1905.

3) Fonetionnement defeetueux en 1905.

 
