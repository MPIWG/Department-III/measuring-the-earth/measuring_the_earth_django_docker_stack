 

|

 

112
of the U. $. Coast Survey through the then newly laid cable, but the stations were not
at that time direetly conneeted with Greenwich '). Subsequently a cable was laid from Brest
to England, and in 1872 the U. 8. Coast Survey determined the difference of longitude
between St. Pierre Island, Newfoundland, and Greenwich, by way of Brest 2), The results
of these three operations, together with a provisional result of the determination of the
longitude of Montreal (4h 5m 18.67) given in this volume, were used by the late Mr. C. A.
Scnort of the U. $. Coast Survey Department to form a network ?) stretehing from Green-
wich to San Franeisco, from which the adjusted difference of longitude between Greenwich
and Montreal is found to be 4h 54m 18:.634, agreeing almost preeisely with the results,

which we now give:

Longitude of Montreal Transit Pier west of the Greenwich Transit Circle:
1892 determination. Ah 54m 185,62 + °.024

Longitude of Canso Transit Pier west of the Greenwich Transit Circle:
1892 determination . .» - = Ah Am 75.27 + 8.033

Longitude of Waterville Transit Pier west of the Greenwich Transit Circle:
1892 determination. 40m 415.30 + 5.021.

3. Longitude of Killorglin.

For reasons given in the volume, it was thought desirable that, as a supplement
to the determinations of the longitudes of Valentia and Waterville, the longitude of a third
station in the neighbourhood of the western end of the great European arc of longitude
should be determined as a check on possible local attraction at those places. The principal
station of the Ordnance Survey at the western end of the European are is at Feaghmain
in the north-west of the island of Valentia. The astronomical longitude of this place was
found in 1844 by transmission of chronometers to be 41m 23°.23 *) west of Greenwich
(Bradley’s meridian). The longitude of Knightstown in the north-east of Valentia islaud
was determined by telegraphic communication in 1862 and found to be 41m 95.81 °). The
astronomical longitude of a third station, Foilhomurrum, in the south of the island, was
found in the course of the work of the trans-atlantie longitude determination made in
1866 by ofücers of the U. 8. Coast Survey, to be 41m 335.346) west of the Greenwich
transit circle.

The differences of geodetie longitude between these th
computed on the supposition that the network of the British and

ree stations and Greenwich,
Irish triangulation as a whole

 

1) U. 8. Coest Survey Report, 1874, p. 163, and Memoirs of the American Academy, vol. IX, p- 487.

2) U. S. Coast Survey, Report, 1872, Appendix N°. 13.
3) U. S. Coast Survey Report, 1897, Part. II, Appendix N°. 2, and Astronomical
4) Determination of the longitude of Valentia by transmission of chronometers,

Journal, N°. 412.
Appendix to Greenwich Obser-

vations, 1845.
5) Longitude of Valentia by galvanie signals, Appendix III, to Green
6) This is the corrected result, U. S. Coast Survey Report, 1872, p-

by Mr. Scnorr in his investigations (v. supra) is 41m 333.409.

wich Observations, 1862.
934, The resulting value of this are found

 

eremsnssnhde enahsn hai ih isst eihikchneheen

vonmhhpanaae

 
