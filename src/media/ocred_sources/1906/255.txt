 

 

 

   

 

 

 

 

 

 

 

 

228
a T = essen teens nee - nn - =
Solution B. | Solution E. | Solution H. | Solution G. | Solution A.
Maximum resihuaen 2 ne | + 43.34 | 164 + 15.74 - 15.94 | 0
Percentage of residuals greater than 5”,00\ 66 | 18 18 18 | 29
Percentage of residnals less than 2.00 15 41 4:3 43 34
Mean residual, without regard to sign. 8.86 3.06 3.04 3.04 | 3.92

 

These comparisons confirm the statements already made, based on the sums of the
squares of the residuals.

The results of the five solutions make it elear that if the assumed depth of com-
pensation is made to vary from infinity to zero the sum of the squares of the residuals
desreases from 65434 for the assumed depth infinity to a minimum value of about 8010
for some assumed depth not differing greatly from 114 kilometers, and then increases again
to 13922 as the assumed depth is decreased to zero.

The depth for which the sum of the squares of the residuals would be a minimum
is the ideal most probable depth of compensation.

To ascertain this ideal most probable depth with great accuracy is not important,
for two reasons: (a): It is evident from a comparison of the sums of the squares of the
residuals for solutions E, H, and @ that the ideal minimum sum, when ascertained, would
be found to be very little less than 8013, the sum corresponding to solution G, and that
therefore the corresponding solution would be a very slight improvement on solution G.
(b) It is evident from a comparison of the values of the equatorial radius, flattening, and
polar semidiameter, as derived from solutions & and H, that: a change in the depth of com-
pensation adopted as most probable introduces but little change into the corresponding
most probable values for the equatorial radius, flattening, and polar semidiameter.

From the results of solutions E, H, and 6, using an approximate process which is
easy of application but of which the explanation is too long to be incorporated in this
report, the conelusion was reached that the most probable depth of compensation is 112.9
kilometers. This depth agrees so closely with that used in solution G, 113.7 kilometers,
that it is not certain that solution G can be improved upon. :

For these reasons solution G is adopted as the most probable solution. This fixes
upon the following values as the most probable which can be derived at present from

observations in the United States.

Tatitude of Mendes Roncn ou nun. u. eek: 39. 13 26.47 + 0.171)
Bornsitude ones Ru nn 98 32 30.64 + 0.40
Azimuth of line Meades Ranch to Waldo, to be used in computations extending

eastward from. Meadesaranch nr ar eu 15 23 1465 =#+ 0.32
Azimuth of line Meades Ranch to Waldo, to be used in computations extending

westward from Meades Ranch. . Be 220.5

Equatorial radius of the earth, meters, 6.378 283 u 34.
Reciprocal of flattening, 297.8 + 0.9.
Polar semidiameter, meters, 6 356 868.

 

1) These probable errors are sliehtly too small, as they are based upon the assumption that ihe residuals are
all aceidental in character.

 

i
|
3
‘
!
i

habe

cum eh bee a at nnn

 
