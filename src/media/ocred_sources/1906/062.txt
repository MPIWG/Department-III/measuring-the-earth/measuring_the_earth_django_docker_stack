i
F
E
i

Sue

E
i
i

I

ana

 

ERÖFFNUNGS-SITZUNG

Donnerstag den 20. September 1906.

Präsident Herr General Bassot.

Die Generalkonferenz wird um 10'/, Uhr in dem Sitzungssaal der Akademie
der Wissenschaften eröffnet.

Anwesend sind:
Seine Excellenz Graf Albert Apponyi, Unterrichtsminister.
Seine Excellenz Herr Albert Berzeviczy, Präsident der Akademie der Wissenschaften.
Herr Victor Molndär, Staatsseeretär am Unterrichtsministerium,
Herr Dr. Gustaf Heinrich, Secretär der Akademie der Wissenschaften.
die Delegirten :

J. ARGENTINIEN,

Herr Dr. F. Porro de Somenzi, Professor und Director der Universitätssternwarte,
la Plata.

II. BELGIEN.

Herr L. @:illis, Major im Generalstabe, Direetor des militär-cartographischen Instituts,
Brussel.
III. DÄNEMARK.

Herr General von Zachariae, Klampenborg bei Kopenhagen.
IV. DEUTSCHLAND,

Herr Geheimrath Dr, W. Foerster, Professor an der Universität, Berlin.

Herr Hofrath Dr. M. Haid, Professor an der Technischen Hochschule, Karlsruhe.
Herr Dr. M. Schmidt, Professor au der Technischen Hochschule, München.

Herr Dr. R. Schorr, Professor, Director der Sternwarte, Hamburg.

Herr Geheimrath Dr. 7%. Albrecht, Professor, Abtheilungsvorsteher in Königl. Geodät.
Institut, Potsdam.

 
