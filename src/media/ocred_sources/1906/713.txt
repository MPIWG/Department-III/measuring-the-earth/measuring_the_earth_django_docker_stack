 

XV, MESSUNGEN DURCH ITALIENISCHE BEOBACHTER.
XVd. Maxssungen DURCH Herrn Pror. Armoxemmi.

Herr Professor Amonsrm hat im Jahre 1904 relative Schweremessungen auf 17
Stationen in Oberitalien (Piemont) im Anschluss an Turin ausgeführt und die Ergebnisse
unter dem Titel; » Determinazioni di gravitä relativa nel Piemonte eseguite nell’estate dell’anno
1904 coll’ apparaio pendolare di Sterneck” in den Atti della Rt. Accademia delle scienze di
Torino, Vol. XL, Disp. r4a, 1904—1905 veröffentlicht.

Zu den Messungen diente wieder der Schneiver’sche Pendelapparat der Universität
Turin mit den Pendeln N®, 41, 42, 45 u. 46 und einem Wandstativ (siehe Bericht für
1903, 8. 199), dessen Stabilität überall mit einem Dynamometer geprüft wurde. Bei An-
wendung einer Stosskraft von 5 kg im Sekundentakt liessen sich keine messbaren Bewe-
gungen des Wandstativs nachweisen.

Die Zeitbestimmungen wurden von der Turiner Sternwarte ausgeführt und von dort
telegraphisch nach den Aussenstationen übertragen, ausgenommen die Station Caluso, wo
sie der Beobachter selbst besorgte. Die Übertragung war jedoch keine direkte, sie geschah
vielmehr in der folgenden Weise. Auf der Pendelstation befanden sich die Ohronometer
Praskerr N®, 5190 (MZ) und Fronsnam N®. 3576 (SZ). Der erste diente als Standard and
wurde während der Messungen nicht berührt, der zweite war mit elektrischem Kontakt
versehen und diente als Koinzidenzuhr. Unmittelbar vor der Zeitübertragung, die allabend-
lich stattfand, verglich der Beobachter beide Chronometer durch Koinzidenzen nach Aug-
und Ohr und begab sich darauf mit Fropswam nach dem der Station zunächst gelegenen
Telegraphenamt. Hier war ein Chronograph aufgestellt, dessen einer Anker mit der Stern-
warte Turin durch die Leitung verbunden war, während sich der andere mit dem SZ-
Chronometer Fropsnan in einem lokalen Stromkreise befand. Turin schaltete dann einen
MZ-Chronometer (Narpın 17/7360), der unmittelbar vorher mit der Hauptuhr verglichen
war, in die Linie ein, so dass sich die Sekundenskalen von Narvın und Fropsuam neben-
einander auf dem Chronographen verzeichneten. Nach der Vergleichung wurde FRoDsHAMm
nach der Pendelstation zurückgebracht und von neuem mit PLAskkrr verglichen. Die Zeit-
bestimmungen in Caluso führte Herr Prof. Aımoxerm mit einem grössern Theodolit im
Vertikal von « Ursae min. aus, wobei er an 3 Abenden je 6 bis 8 Zeitsterne beobachtete.

Was den Umfang der Pendelbeobachtungen betrifft, so wurden auf jeder Station 2
bis 4 Sätze zu je 4 Pendeln beobachtet, die sich auf 1 bis 2 Tage verteilen. Die Aenderung
der Pendel ergab sich aus den Turiner Ausgangs- und Schlussbeobachtungen zu:

 

eseronbhhscusulkansLabuin sites

j
i
i
|
i
i
j

 
