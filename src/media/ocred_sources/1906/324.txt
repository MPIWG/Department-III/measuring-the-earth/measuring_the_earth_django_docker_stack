285

Au 1er Janvier 1895, l’altitude du niveau moyen de la Mediterranee, d’apres le
marögraphe totalisateur, etait de + 2 millimötres seulement. Depuis 1895, cette cote, toujours
caleul€ depuis l’origine, n’a pas cesse d’augmenter; elle atteignait + 21 millimetres au
le: Janvier 1906. Dans l’intervalle, sauf de petites variations accidentelles, d’une anne A
l’autre, liees aux circonstances meteorologiques, les niveaux moyens annuels ont reguliere-
ment deeru, tout d’abord, de 1885 (origine des observations) & 1890; puis ils se sont
releves de 1890 a 1900; depuis lors, ils s’abaissent de nouveau (fig. 1 page 286).

D’apres les resultats fournis par les medimaremötres installes dans les divers ports
du littoral, ce phenomene serait general.

En rapprochant de ces chiffres les cotes autrefois ealeuldes par M. Bovguer ou La
Grye'), d’apres les indications du mar&graphe de Brest et, d’autre part, les renseignements
analogues fournis depuis, par le service hydrographique de la Marine ainsi que les resultats
du medimaremetre install& & Camaret, sur la rive sud de la rade de Brest, on constate,
(ig. 2) que, depuis un demi-sieele tout au moins, le niveau moyen annuel de la mer sur
les eötes de France a oseill& entre deux niveaux limites, distants de 6 & 8 centimetres l’un
de P’autre, les maxima et minima successifs ayant eu lieu & peu pres aux &poques suivantes:

Vers 1852. , . 2 Nosmum
Vers 1870, |.,..°,.... Nınmenum
Wers, 1886. 5. . 2... Maximum
Vers 1896... , . . Moun

Formuler des maintenant une hypothöse, quant & la cause de ces lentes varlations,
serait sans doute pr&mature, ;

1) Voir les Comptes-rendus de la Conference tenue A Rome, en 1883, par P’Association g6odesique internationale;
page 246 et les Comptes-rendus do l’Acaddmie des Seiences (Seance du 19 novembre 1888).

 
