 

 

 

294
tout proche de Guayaquil. On sconomiserait ainsi deux stations, ce qui serait fort important
stant donne le retard des op6rations, et ’on ne perdrait pas beaucoup en pröcision. Il .reste
% savoir si cela est possible; les cotes du triangle auraient de 100 a 120 km.; M. MauraAın
estime que cette distance pourrait tre franchie; e’est ce qu’une reconnaissance ulterieure

nous pourra seule apprendre.

Divers. — Les observations magnetiques ont 6te poursuivies.
M. le Dr. Rıver est rentr6 en France en rapportant pour le Museum de nombreuses
caisses de collections; ces collections interessent toutes les parties de /’Histoire naturelle,

mais principalement l’Anthropologie.

Programme et resume — Nous avons dit plus haut qu’on pouyait prevoir
l’achevement des op6rations du trongon 8. pour la fin de 1904. La latitude de Cuenca est
actuellement mesurde. Il est probable pue l’on aura terming & la möme Epoque:

1°. les differences de longitude Quenca-Quito et Payta-Quito;;

2%, Je nivellement de preeision;

il resterait done pour 1908:

1°. la base de Payta;

90, Je rattachement de Guayaquil et la difference de longitude Guayaquil-Quito ;

30, es observations pendulaires.

Il y a lieu une fois de plus de fölieiter nos offieiers des r&sultats qu’ils ont obtenus
et dont la valeur seientifigue est tres grande, de rendre hommage ä leur zele et & leur
constance dans les eirconstances diffieiles oü ils ont opere depuis trois ans.

Nous devons remercier ögalement les ofliciers öquatoriens dont le concours nous a
ts trös utile, et surtout le Gouvernement &quatorien qui n’a cesse de nous venir en aide,
non seulement par ses subsides, mais par son intervention constante aupres des populations.

1904.

La Commission chargee du contröle de V’expedition de V’Equateur s’est, comme les
anndes pröeedentes, r&unie pour entendre le rapport de M. le commandant BourGEols sur
les op6rations de Vannde 1904. Elle a eu le regret de constater que les conditions elima-
teriques ne se sont pas ameliordes et que le retard qui s’6tait produit dans les annees
precdentes s’est encore accentue. Il y a 2 ans, nous pouvions esperer qu’on pousserait
jusqu’a Cuenca avant la fin de 1903; il y a 1 an, nous comptions encore qu’on atteindrait
ce point vers le milieu de 1904. En realite, e’est seulement en novembre que les stations

qui entourent cette ville ont pu &tre terminees. Depuis, on n’a pas pu marcher plus rapı-
stait encore & Tinajillas et Narihuima, a 50 km

dement, de sorte qu’au mois de janvier on
et SO km au sud de Üuenca.

babe sllanlkslusäbann

 

i
’
|
!
!
|
!
j

 
