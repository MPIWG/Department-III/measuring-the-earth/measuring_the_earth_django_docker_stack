 

f
F

 

 

147

qui a une dilatation un peu plus forte que celle de l’invar de premiere qualite. Leur for-
mule de dilatation est

& — (+ 0.793 -/- 0.000 16 8) 10-*.

Les autres fils de la Commission geodesique suisse, Nos A 31, A 32 et A 33, sont
issus d’une operation metallurgique partieuliörement re&ussie, mais leur construction, faite
par M. Dimiche & Paris, est un peu moins pratique, Ils ont servi pour le contröle des
autres fils de 24m, avant et apreös les mesures, soit & Sövres, soit A Brigue.

Le travail avait &t6 combine de facon & ce que les deux fils 98 et 99 fussent em-
ployes, chacun pour la moitie de la longueur totale, mais en les &changeant de sorte qu’ils
se contrölent l’un par l’autre pour un tiers de la longueur et se correspondent chacun &
lui-m&me pour les deux autres tiers (fig. 12).

Fig. 12,

 

 

Aller | |
Bri Iselle
En Bam — | |

e—— 98 99 So

 

Malheureusement, l’emploi du fil N 98 a öt& arröt& & la 368me portde, En trans-
portant ce fil ü la 369me portee, un des observateurs a fait une chute, le fil a &t& legere-
ment avarie et remplac® par le fil 99 qui a servi de la portee 369 jusqu’& la portee 826
(repere V) & l’aller et pour toute la mesure de retour, Le möme fil 99 a aussi servi avec
le fil de 72m, & la mesure, aller et retour, de la section exterieure entre les reperes I et II.

VI. RESULTATS DE LA MESURR.
A. Reduction provisoire.

Des que la mesure de la base du tunnel a &t& terminee, les groupes de mesure des
trois equipes se sont constitues en bureaux de calcul, & Brigue, et ont fait une premiere
verification des resultats. Cette verification ne pouvait naturellement pas tenir compte de
tous les el&ments de r&duetion, mais elle a permis de s’assurer que les mesures aller et
retour des diverses sections de la base concordaient d’une fason satisfaisante. La plus forte
difference, pour une section de 2400 metres, etait de 7mm,9 et les valeurs de la longueur
totale concordaient & quelques millimetres pres.

B. Reduction a peu prös definitive.

Cette reduction a &te conficee a M. M. Knapp, ingenieur de la Commission, sous la
direction de MM. les professeurs Rosenmunn et Rıcerysgach. Elle a donne, au commence-

 
