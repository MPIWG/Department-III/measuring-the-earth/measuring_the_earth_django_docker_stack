 

F
;
|

9

Cette confiance qu’on nous accorde, nous ne la tromperons pas. Nous en avons pour
garants les travaux que nous suggerons, ceux que nous conseillons, ceux aussi que nous
entreprenons eb poursuivons nous m&mes avec les ressources dont nous disposons.

L’histoire des dix dernieres annees ne met-elle pas en relief notre f&conditE? Ce sont
les ares du Spitzberg, de l’Equateur, du Sud de l’Afrique, et d’autres aux Etats-Unis d’Amerique,
qui ont ete mesure pendant cette periode; ce sont aussi les etudes sur la gravite, sur les
deviations de la verticale, sur les nivellements de preeision, que nous poursuivons sans reläche.
C'est enfin le delieat problöme de la variation des latitudes dont nous avons entrepris la
solution avec t&nacite, et que nous esperons bien resoudre sous peu d’annees.

Nous avons le droit d’&tre fiers de l’oeuyre accomplie, et nous n’avons qu’ä suivre
la voie deja tracee pour bien meriter de la science.

Messieurs, la session qui va s’ouvrir nous apporte des questions du plus haut interet:
des trayaux delicats et nouveaux sont soumis A notre examen: d’autre part, l’Association
internationale des Academies nous consulte sur des probl&mes que suscite le seisme. Notre
programme est done charge: mettons nous r&esolument & l’euvre.

Je declare ouverte la quinzieme Conference generale de 1 Association geodesique inter-
nationale.

Sur la proposition de M. le president la s&ance est suspendue pendant un quart d’heure.

A la reouverture de la seance M. le president indique l’ordre du jour de la seance
d’aujourd’hui, lequel portera:

1°. Communication de M. BovorAa.

2°. Rapport du Seeretaire perpetuel.

3°. Rapport du Direeteur du Bureau central.

4°. Rapport de M. Arsrechr sur la variation des latitudes.

M. le president croit qu’il est preferable qu’on fasse la lecture de ces deux derniers
vapports dans la seance d’aujourd’hui, afin qu’on puisse nonnıer deux commissions, la com-
mission des finances et la commission des latitudes, qui pourront examiner les propositions
qui se trouvent dans ces deux rapports. Il invite ensuite les delegues et les invites de
vouloir bien 6erire sur une feuille de papier leurs noms et ceux des personnes qui les ac-
compagnent, avec l'indication de l’hötel oü ils sont descendus, afın qu’on puisse dresser
une liste de ceux qui assistent aux sdances.

M. Bodola donne quelques renseignements concernant les invitations adressees aux
delegues.

M. le president donne la parole au secretaire pour la lecture de son rapport.

M. le secrdtaire perpetuel lit le rapport suivant sur l’activit6 administrative du Bureau.

Messıkurs,

D’aprös l’artiele 2 de la nouvelle Convention geodesique internationale, l’organe su-
perieur de l’Association geodesique est la Conference generale des del&gues des Gouvernements
interesses. Dans l’intervalle des sessions l’exdeution des deeisions de la Conference generale

2

 
