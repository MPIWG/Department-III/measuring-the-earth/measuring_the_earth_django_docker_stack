 

134

seit dem auf der Kopenhagener 14. Allgemeinen Konferenz erstatteten Bericht!) geben die
»Tätigkeitsberichte des Zentralbureaus in den Jahren 1903—1906” ?) und die »Jahresbe-
richte des Direktors des Königl. Geodätischen Instituts für die Zeit von April 1903 bis
April 1907?) fortlaufend Aufschluss. Da die Tätigkeitsberichte des Zentralbureaus auch in den
Verh. der I. E, erscheinen — der für 1903 findet sich bereits in den » Verhandlungen in Kopen-
hagen, 1903, II. Teil, Beilage B. XIX”, 8.427 —452 —,so genügt im allgemeinen dieser Hinweis.

Besonders erwähnt sei u. a., dass der Tätigkeitsbericht für 1905 als Ergebnis einer
ersten Bearbeitung des neuen französischen Meridianbogens und seiner Fortsetzung durch
Spanien und Algier für 21 Punkte von Rosendaöl bei Dünkirchen bis Nemours in Alsier
Lotabweichungen in Breite (£) enthält, bezogen auf ein Ellipsoid, das die Abplattung des
Besser’schen Ellipsoids hat, dessen Äquatorialhalbachse aber um nahe 300 mı grösser ist,
was diesem Bogen am besten entspricht. Diese E werden weiter unten neben den ver-
schiedenen andern Systemen von & für den westeuropäisch-afrikanischen Meridianstreifen
aufgeführt werden. Die neue Bearbeitung dieses Meridianstreifens durch Herrn Brot. R.
Schumans in Aachen, auf die in dem Tätigkeitsbericht für 1906 hingewiesen wird, ist
vollständig in die vorliegenden Verhandlungen (I. Teil, S. 244/261) aufgenommen worden.
Ausserdem gibt Herr Geheimrat Hermerr auf Grund seiner, in der später besonders be-
sprochenen Abhandlung über » die Grösse der Erde” enthaltenen Untersuchungen in diesem
Bericht einen Wert für die Äquatorialhalbachse des Erdellipsoids in internationalen Metern,
wie er im Mittel aus dem russisch-skandinavischen und dem westeuropäisch-afrikanischen
Meridianbogen, sowie aus den Längengradmessungen in 52° und in 47'|,° Breite folgt
(unter Annahme der Besser’schen Abplattung: a=1: 299,15), nämlich:

a —= 6378150 m.

Dieser Wert würde sich bei Annahme von a—1: 298,3, wie sich der Abplattungs-
wert aus den Schweremessungen ergibt, nur um rund 10 m verkleinern. Für die Vermessung
von Äsypten wurde ferner von Herrn Hrımert infolge einer Anfrage mit weiterer Rück-
sicht auf die neuesten rechnerischen Ergebnisse in den Vereinigten Staaten von Amerika
ein Rotationsellipsoid mit

a — 6378200 m, a=1:298,3
empfohlen.

Von den systematischen Berechnungen für das europäische Lotabweichungssystem
ist ein weiterer Teil als Heft III der » Lotabweichungen” erschienen *). Diese Veröffentlichung

1) Verhandlungen der I. E. in Kopenhagen, 1903, Il. Teil, Beilage B. XVIH, S. 399-426. A. BörscH, Bericht
über Lotabweichungen (1903).

2) Bericht über die Tätigkeit des Zentralbureaus der I. E. im Jahre 1903, Berlin 1904, S. 3—4; Dasselbe für
1904, Berlin 1905, S. 3—5; Dasselbe für 1905, Berlin 1906, S. 3—6; Dasselbe für 1906, Berlin 1907, S. 8—5.

3) Jahresbericht des Direktors des Königlichen Geodätischen Instituts für die Zeit von April 1903 bis April 1904,
Potsdam 1904, S. 14—15; Dasselbe für 1904/05, Potsdam 1905, 8. 14—15; Dasselbe für 1905/06, Potsdam 1906,
S. 14—16; Dasselbe für 1906/07, Potsdam 1907, S. 12—13

4) A. Börsen, Lotabweichungen. Heft III. Astronomisch-geodätisches Netz I. Ordnung nördlich der europäischen
Längengradmessung in 52° Breite. Mit einer lithographierten Tafel. Berlin 1906. (Veröffentlichung des Königl. Preuss.

Geod. Insts. Neue Folge N? 28).

 

 
