BEILAGE B. XV. i |

BERICHT
über
die Tätigkeit des Zentralbureaus der Internationalen Erdmessung
im Jahre 1907

nebst dem Arbeitsplan für 1908. *)
A. Wissenschaftliche Tätigkeit. ;
1. Berechnungen für das europäische Lotabweichungssystem.
E 2. Krümmung des Geoids in den Meridianen und Parallelen.
ä 3. Internationaler Breitendienst.
4. Relative Pendelmessungen. |
5. Berechnung der Schwerkraftsbestimmungen auf dem Indischen und dem
f Stillen Ozean und an deren Küsten.
; 6. Verschiedenes. |
I,
Spezialbericht

über die Berechnungen für das europäische Lotabweichungssystem.

E „Im Berichtsjahre wurde mit der zusammenfassenden Bearbeitung
der astronomisch-geodätischen Verbindungen der mitteleuropäischen Dreiecks-
netze mit der russisch-skandinavischen Breitengradmessung, die den Inhalt
des Heftes IV der „Lotabweichungen“ bilden sollen, begonnen. $ Für mehrere
geodätische Linien lagen bereits in früheren Jahren ausgeführte Rechnungen
vor. Ein genaueres Studium der älteren russischen Veröffentlichungen über
die Triangulationen in dieser Gegend verursachte jedoch mancherlei Änderungen
und Neurechnungen, die freilich an den früheren Ergebnissen in der Haupt-
E sache nur wenig änderten. Dazu kam noch, daß nach dem inzwischen
erfolgten Erscheinen des russischen Anteils der europäischen Längen-
gradmessung in 52° Breite der Larraczsche Punkt Grodno eingeschaltet
werden konnte, wodurch die auf einem großen Umwege erhaltene geodätische
Linie Goldapper Berg—Nemesch in angemessener Weise geteilt wurde.
Endlich gelang es, in den „Memoiren (Sapiski) des kriegs-topographischen |
Depöts“ des russischen großen Generalstabes (Band VIII, St. Petersburg 18435)

”) Der Arbeitsplan ist bei jedem einzelnen Arbeitsgebiet ersichtlich,

AREA TTTTRNAIT BaTTTTwETT Fe
u ei

 

 
