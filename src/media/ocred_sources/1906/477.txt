BEILAGE B. 1.

BERICHT

über

die Tätigkeit des Centralbureaus der Internationalen Erdmessung
im Jahre 1904

nebst dem Arbeitsplan für 1905.*)

A. Wissenschaftliche Tätigkeit.

ee or

i 1. Berechnungen für da seuropäische Lotabweichungssystem.
| 2. Krümmung des Geoids in den Meridianen und Parallelen.
3. Internationaler Polhöhendienst in + 39° 8° Breite.
ij 4. Vorbereitungen für die Ausdehnung des Internationalen Polhöhendienstes
auf die Südhalbkugel.
5. Absolute Pendelmessungen.
6. Relative Pendelmessungen.
7. Bestimmung der Schwerkraft auf dem Indischen und dem Stillen Ozean und

an deren Küsten.
8. Verschiedenes.
i“

Spezialbericht:
über die Berechnungen für das europäische Lotabweichungssystem.

„Die Anfertigung der Druckhandschrift für das III. Heft der ZLotab-
weichungen ist ziemlich weit vorgeschritten. Es liegen die Linien, die durch
das Polygon Bonn — Ubagsberg—Nottuln— Wilhelmshaven—Kaiserberg— Kiel—
Dietrichshagen — Rugard — Vogelsang — Rauenberg— Leipzig — Brocken — Bonn
gebildet und von ihm eingeschlossen werden, druckfertig vor. Ausgedehnte
Rechnungen waren hierbei noch auszuführen, die teils zur Kontrolle, teils zur
Ergänzung des bereits vorhandenen Materials dienen sollten. So wurden
z. B. die Linien Wilhelmshaven—Helgoland und Wilhelmshaven— Borkum neu
berechnet, die zweite besonders deshalb, weil für Borkum, das dem Meridian
von Bonn sehr nahe liest, im Jahre 1904 die Breite und die Länge bestimmt

 

*) Der Arbeitsplan ist bei jedem einzelnen Arbeitsgebiet ersichtlich.

 

ı*

 
