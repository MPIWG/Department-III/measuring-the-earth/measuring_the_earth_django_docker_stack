 

 

ANNEXE A, IV®.

/

ITALIE.

Rapport sur les travaux ex6cutes par l’Institut geographique militaire
italien dans les ann6es 1903-1906.

TRIANGULATION.

Pendant le mois de Juin 1903, on a rattache la station internationale de Carloforte
au r&seau fondamental de l’ile de Sardaigne par des mesures angulaires aux stations de
Punta Sobera et Isola del Toro. En möme temps on 4 dstermind de nouveau la distance
entre la mire meridienne et l’axe du telescope zenital, et on a deduit l’azimut astronomique
de Punta Sobera en fonction de celui de la mire susdite. On a ainsi obtenu tous les Elements

pour le caleul de la deviation de la verticale; en effet, en retenant pour la latitude
astronomique de Carloforte la valeur:
39° 8’ 8",980 ')

et l’azimut astronomique de Punta Sibera, yu du centre de la station etant:

102° 19' 48",385 ?)

on obtient:
&E — — 0,309 (deviation meridienne)
av. 72141 (devasıon orientale)
car A. a 5oell, ercn por que l’ellipsoide de Bessel coupe normalement la

verticale au niveau de la mer de G£nes.

z

La distance entre le zenith astronomique et le zenith ellipsoidique sera done &

Carloforte:
& = .1.,148

et son azımut:

y— 267° 31
“x
*
Dans la m&me annee, et preeisement du mois d’Aoüt au mois de Septembre, on a

1) Cfr. Resultate des Internationalen Breitendienstes (Band I von Ti. ALBRECHT).
ella Sardegna al Continente. Firenze 1904,

 

2, Oft. Appendice al collegamento geodetico d

ad anna

 

 
