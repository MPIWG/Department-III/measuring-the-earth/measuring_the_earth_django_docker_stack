+

 

 

aa 35

Spezialberieht über die absoluten Schweremessungen und über die Vergleichung
verschiedener Pendel.

„Der erste Teil der Arbeit über die Bestimmung der absoluten Größe
der Schwerkraft in Potsdam: Die Pendel schwingen mit ‚Schneiden auf
ebener Unterlage, und der zweite Teil: Die Pendel schwingen mit ebenen
Flächen auf einer feststehenden Schneide, sind im Laufe des verflossenen
Jahres gedruckt worden; für den zweiten Teil war das Druckmanuskript noch
anzufertigen gewesen. Der dritte Teil: Genauigkeitsuntersuchungen und
Ableitung des Schlußresultates, soll in nächster Zeit gedruckt werden.

Können.
6.

Relative Pendelmessungen. Herr Professor Hassemanw untersuchte zwei
Vierpendelapparate, die Mechaniker Srückrarz in Friedenau für die Niederlande (Grad-
messungskommission) und für Italien (Prof. Parazzo) konstruiert hatte. Für die 4 Pendel
jedes der beiden Apparate wurde sowohl die Dichte- als auch die Temperaturkonstante
bestimmt. Während der Beobachtungen für die Temperaturkonstanten hatten sich zwei
der niederländischen Pendel so stark geändert, daß eine zweite Beobachtungsreihe für
nötig erachtet wurde. Auch die für Italien bestimmten Pendel haben sich während
der Temperaturkonstantenbestimmung nicht ganz unveränderlich gehalten, doch liegt
diese Änderung innerhalb der zulässigen Fehlergrenze und kommt nur in der Größe
der mittleren Fehler der Ergebnisse zur Erscheinung. In der nachfolgenden Übersicht
werden auch die Ergebnisse der Untersuchung der mexikanischen Pendel vom vorigen
Jahre wiederholt, da im Bericht für 1904 versehentlich für die Temperaturkonstanten
z. T. nur vorläufige Werte gegeben worden waren.

Ergebnisse der Bestimmungen

der Dichtekonstanten der Temperaturkonstanten
1. Mexikanische Pendel: Nr. 84 669.4=#3.3 46.07 30.08
85 659.2 2.8 46.07 0.08
86 660.8&3.8 46.23 30.08
sn SIT ent A60 AS 03
9. Niederländische Pendel: Nr. 88 665.9=E95.7 47.03 30.09
sg 673.223.0 46.39 30.05
90. 0608.12 989 46.94 0.11
91 683.9=EA.5 46.98 30.05
3. Italienische Pendel: Nr. 92. 677.9=2456 47.02 0.16
93 682.5 4.4 AO SEO IT
94 658.8#5.6 Asa
9 664.2 85.4 4711 =E.020

Die Konstanten gelten für 10" Sekunden als Einheit.
