min

N meer wer

ü
5
E
&
E
E
R
B

VII. MESSUNGEN DER DÄNISCHEN GRADMESSUNGS-KOMMISSION.

Die Kommission hat in den Jahren 1903, 1904 und 1905 relative Schweremessungen
auf 34 dänischen Stationen im Anschluss an Kopenhagen ausgeführt. Als Beobachter wirkten
die Herren Kapitän N. M. Prrersen und Oberleutnant N. P. Jomansen. Die Angaben
unsrer Tabelle, sowie die folgenden Bemerkungen über die Art und Genauigkeit der Mes-
sungen verdanken wir einer schriftlichen Mitteilung des Herrn Generalleutnants von ZACHARIZ.

Im Jahre 1903 wurden die Messungen mit dem vom Mechaniker Fronner (Potsdam)
verbesserten STERNECK-ScCHNEIDER’schen Pendelapparat N°. 14, zu dem 3 Pendel gehören,
ausgeführt; in den folgenden Jahren 1904 und 1905 kam der neue Fronnur’sche 4-Pendel-
apparat der dänischen Gradmessungs-Kommission in Anwendung, wobei die Messungen im
luftverdünnten Raum unter 32 bis 40 mm Druck stattfanden,

Der m.F. einer relativen Bestimmung g—g; zwischen einer Aussenstation und
Kopenhagen stellt sich in den Jahren 1903, 1904 und 1905 ungefähr auf + 0,003, + 0,002
und + 0,0015 cm/sek?.

Ausser der Anschlussstation Kopenhagen, wo sowohl vor als nach jeder Kampagne
wenigstens 2 Pendelreihen ausgeführt wurden, wurde in allen 3 Jahren noch eine lokale
Zentralstation verwendet. Auf dieser verblieb die Hauptuhr während der ganzen
Kampagne und wurde von hier aus successive mit den Beobachtungsstationen telegraphisch
verbunden. Die Zeitbestimmungen auf der Zentralstation wurden mit einem Bamsere’schen
Passageninstrumente ausgeführt, und zwar, wenn das Wetter es erlaubte, täglich.

Zwischen den Messungen auf den Beobachtungsstationen wurden oft auch Messungen
auf der Zentralstation angestellt, wodurch es möglich war, die Konstanz der Pendel während
der ganzen Kampagne in verhältnismässig kurzen Zeitintervallen zu kontrollieren. Es zeigte
sich, dass die Pendel recht invariabel blieben; bei den verbesserten Scuneiver’schen Pendeln
kam eine kleine Kontraktion zum Ausdruck, während die sehr begrenzte Veränderliehkeit
der Frouner’schen Pendel nicht auf Kontraktion deutete, sondern während beider Kampagnen
(1904 und 1905) einen völlig zufälligen Charakter hatte.

Auf der Station Vissenbjerg (N°®. 111 der folg. Tab.) ist schon 1897 beobachtet
worden (Bericht von 1900, S. 218, N°. 17); der damals gefundene g-Wert weicht, wenn
man die Höhenreduktion in Kopenhagen berücksichtigt, um -- 0,005 cm/sek? von dem
gegenwärtigen Werte ab. Die Erdbodendichte ist für diese Station früher zu 2,1, jetzt zu
1,9 angenommen worden.

 

 
