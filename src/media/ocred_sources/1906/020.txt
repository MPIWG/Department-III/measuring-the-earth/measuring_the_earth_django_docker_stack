Ze

 

|
!
!
|
E

 

15

latitudes (eontinuation jusqu’&a la fin de 1906 de ce service tel qu'il fonctionne actuellement,
et le vu de le continuer aussi apres cette date) ont &t€ communiquees & M. le Ministre
du Japon, avee la priere que le gouvernement Japonais fasse continuer les observations de
latitude & Mizousawa conform@ment A ces resolutions. Le 23 Novembre 1904, M. le Ministre
du Japon nous fit parvenir une röponse entierement favorable.

Une des resolutions de la Conference de Copenhague fut la suivante: le Bureau de
l’Association est pri& de pourvoir & une extension des observations de latitude.
Conformement aux deeisions du Bureau la eireulaire suivante fut envoyee aux delegues.

Amen enaseniane Nicz—Leyde, 1 Dee. 1904.
Internationale.

Cireulaire ä MM. les membres de la commission permanente de l’Association
g6odesique internationale.

MoNSIEUR ET TRES HONORE COLLEGUE!

Dans la 5e seance (13 Aoüt 1903) de la 14e Conference generale de l’Assoeiation
o6odesique internationale A Copenhague, on a adopte, sur l’avis de la Commission des
Finances, la proposition suivante: »Ze Bureau de ÜAssociation est prid de pourvoir & une
extension des observations de latitudes”.

Le but de cette proposition &tait de faire ex&cuter des observations dans l’hemisphere
austral, afin de contröler les r&sultats obtenus dans l’hemisphere boreal par les observations
faites dans les 6 stations du service international des latitudes; surtout afin de chercher
Vexplieation et, si possible, de determiner la cause de la variation annuelle, independante
de la longitude des stations, qui a &t& indiqude par M, Kımura.

D’apres une etude de cette question faite par MM. Hermert et Anprecnt, le Bureau
croit qu’avec les ressources financieres de l’Association on pourra faire ex&cuter, pendant
deux anndes, des observations avec le telescope zenital en deux stations situees sur un
möme parallele dans l’hemisphere austral, l’une probablement dans la Republique Argentine,
pres de Cordoba, l’autre probablement dans l’Australie, pres de Perth. D’apres un devis
provisoire les frais de ces observations pendant deux annees seraient a peu pres 80000 Mk.
(100.000 fr.).

Selon l’avis du Bureau cette somme pourrait &tre prelevee sur l’actif disponible et
les recettes ordinaires de l’Association.

Cet avis se justifie ainsi qu’il suit:

A la fin de 1903, l’aetif disponible etait 105000 Mk. (131250. fr.). D’apres la reso-
lution de la Conference generale de Copenhague, une somme de 20000 Mk. (25000 fr.) a
ete mise A la disposition du Bureau central pour l’expedition de M. Hzcker (determination

.
