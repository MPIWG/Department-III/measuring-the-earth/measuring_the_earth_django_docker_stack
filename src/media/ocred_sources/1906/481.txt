7 8

Die Bearbeitung eines II. Bandes der Resultate des Internationalen
Breitendienstes wurde auf die Dauer eines Jahres vertagt, da es sich als zweck-
mäßig herausstellte, diesmal einen dreijährigen Zeitraum: vom 5. Januar 1902
bis 4. Januar 1905, zusammenzufassen und im Anschluß an die definitive
Reduktion dieses Beobachtungsmateriales eine Diskussion der Gesamtergebnisse
der ersten 5 Jahre des Internationalen Breitendienstes vorzunehmen. Die Be-
arbeitung dieses II. Bandes wird daher nunmehr in Angriff genommen werden.

Um aber schon vor dem Erscheinen dieses Bandes einen vorläufigen
Aufschluß über den weiteren Verlauf der Polhöhenbewegung zu erhalten, habe
ich auf Grund der abgeleiteten Verbesserungen der angenommenen mittleren
Deklinationen der Sternpaare für das Zeitintervall von 1903.0—1904.0 eine
provisorische Ableitung der Bahn des Poles ausgeführt, deren Resultate in
No. 38945 der Astronomischen Nachrichten veröffentlicht worden sind. Hier-
durch ist es ermöglicht, die im Verlauf des Jahres 1903 ausgeführten astro-
nomischen Beobachtungen und astronomisch-geographischen Ortsbestimmungen

i schon jetzt auf eine mittlere Lage des Poles reduzieren zu können. Eine
“ analoge Ableitung provisorischer Resultate für die Zeit von 1904.0—1905.0
ist für das Frühjahr 1905 in Aussicht genommen.

i Te. Augrechr.

4

Ni

Spezialbericht über die Vorbereitungen
zur Ausdehnung des Internationalen Breitendienstes auf die Südhalbkugel.

„Die bisherigen Resultate des Internationalen Breitendienstes haben
die Notwendigkeit ergeben, den Breitendienst wenigstens zeitweilig auch auf
die Südhalbkugel auszudehnen. Nur dann wird es möglich sein, sichere Grund-
lagen für die Deutung aller bei der Breitenvariation auftretenden Erscheinungen
E zu erlangen.

Das Präsidium d. I. E. hat dementsprechend der Permanenten
Kommission den Vorschlag gemacht, einen Breitendienst in beschränktem
Umfange nach Maßgabe der zur Verfügung stehenden Gelder auch auf der
Südhalbkugel einzurichten. Von Seiten des Oentralbureaus wurden in Erwartung
der Beschlüsse der P. K. einstweilen folgende Vorbereitungen getroffen.

In betreff einer möglichst geeigneten Wahl der Beobachtungsstationen
‚war in erster Linie an die schon wiederholt in Vorschlag gebrachte Kombination
Sydney, Kapstadt, Santiago gedacht worden. Aber schon in den Zausanner
Verhandlungen ist Seite 153 darauf hingewiesen worden, daß diese Kom-
bination in mathematischer Beziehung keine günstigen Vorbedingungen zu mög-
lichst scharfer Bestimmung der Polkoordinaten bietet. Dazu tritt ferner noch
der Umstand, daß die Lage des Observatoriums in Kapstadt ein zeitweiliges
Auftreten von Refraktionsanomalien nicht völlig ausschließt.

Es erscheint daher zweckmäßiger, nach dem Vorschlage von Herrn
Geheimrat Herxerr, zwei Stationen auf gleichem Parällel, aber mit einem

er

u

i

i
|
ab
ze
|
E
|
E
F

 
