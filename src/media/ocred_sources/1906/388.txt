 

 

332

Es wäre vielleicht von Interesse die Aufmerksamkeit auf diejenigen Punkte zu lenken,
welche für diese Messungen karakteristisch sind.

Die Arbeiten haben eine Strecke von mehr als 6500 Kilometer umfasst und waren
durch die Beschaffenheit des Terrains, und die ungünstigen klimatischen Verhältnisse sehr
erschwert. Dazu kommen noch, besonders in den nördlichen Gegenden mit ihrer dünnen
Bevölkerung, die persönlichen Unannehmlichkeiten, welche besonders aus Mangel an Quartier
entstanden.

1. Plan des Nivellements.

Es lag in dem Hauptplan der schwedischen Präcisionsnivellements alle solche an der
Küste gelegenen Punkte zu verbinden, an denen Messungen der Wasserhöhe in der eine
oder andere Weise angestellt waren. Um dies zu realisieren, wurden die Nivellementslinien
in zwei Gruppen verteilt, nämlich Hauptlinien und Nebenlinien. Die ersteren bilden eine zu-
sammenhängende Kette von 12 Polygonen und wurden in Zusammenhang ausgeglichen; die
zweiten gehen von verschiedenen Punkten der Hauptlinien aus, und verknüpfen diese mit
den Pegeln und solchen Wasserhöhenmarken, welche zugänglich waren.

2. Die Marken sind folgende:

a) Der Normalhöhenpunkt ist auf den Riddarholmen in Stockholm angebracht und
in Granitgneisfelsen fest eingemauert.

b) Gut geschützte Marken von Messing, welche mit Höhenangaben versehen sind.
Dieselben wurden Hufvudmärken (Hauptmarken) benannt und finden sich bei Riksgränsen,
Kiruna, Gellivare, Storlien und Helsingborg in der Türme Kärnan.

c) Die übrigen Marken bestehen in der Regel aus Bolzen von Messing oder Eisen, welche
in Felsen oder grossen erdfesten Steinen lotrecht angebracht sind. Sie haben eine Länge von
circa 10 Centimeter und ragen nur so wenig über das umgebende Niveau hervor, dass die
Latte frei gedreht werden kann.

Die Anzahl dieser Marken beträgt 2440 und deren Mittelabstand 2,7 km.

d) Endlich kommen dazu ältere topografische Punkte, welche mit dem Präeisions-
nivellement verbunden wurden, und andere zufällige Marken durch in Felsen eingehauene
Bohrlöcher bei den Übergang von Sunden und Wässern.

3, Methode der Beobachtungen, Instrument.

Da die wichtigen Punkte der Wasserstandsbestimmungen theilweise auf Inseln, Scheren
oder anderswo gelegen sind, wodurch die Nivellementslinien sich oft über Sunde und ver-
hältnissmässig breite Wasserflächen erstreckten, wurden auch andere geodätischen Opera-
tionen als die gewöhnliche geometrische Nivellirungsmethode angewandt. Es wurden also drei
Methoden benutzt, nämlich 1° die gewönliche bei Abständen von höchstens 60 meter, 2° hori-

anerrmmnbhhssulhulinnsbabsiursiisernener

 

|
|
i

 
