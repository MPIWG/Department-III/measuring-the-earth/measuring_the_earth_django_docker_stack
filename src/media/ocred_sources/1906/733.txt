 

BEILAGE B. XI.

BERICHT

über die Schwerkraftsmessungen auf dem Meere.

Von den Schwerkraftsmessungen, die ich im Auftrage der Internationalen Erdmes-
sung auf dem Indischen und Stillen Ozean ausgeführt habe, sind zunächst die auf den Reisen
Sydney—San Franeisco -und San Francisco— Yokohama ausgeführten bearbeitet.

Wie bereits in früheren Berichten mitgeteilt ist, habe ich 5 photographisch regis-
trierende Quecksilberbarometer und 6 Siedethermometer bei meinen Beobachtungen benutzt.
Die Barometer waren, wie bei meiner ersten Reise, durchgängig stark gedämpft. Denn in-
folge der Schiffsbewegung ist die Beschleunigung, die das Quecksilberbarometer an Bord
erfährt, sehr veränderlich und das Barometer pumpt daher stark, wenn die Dämpfung nicht
genügend ist. Wie gross die Schwankung in der resultierenden Beschleunigung ist, zeigt
folgende Betrachtung. Nehmen wir eine gar nicht ungewöhnliche Bewegung des Schiffes in
den Vertikalen von 2 m bei einer Wellenperiode von 7 Sekunden an, so schwankt das
resultierende, auf die Quecksilberbarometer einwirkende g je nachdem das Schiff sich ab-
wärts oder aufwärts bewegt, rund zwischen 9 und 11m.

Im Mittel aus vielen Wellen hebt sich aber der durch die Schiffsbewegung verur-
sachte negative oder positive Zuwachs der Beschleunigung annähernd auf; in wie weit das
stattfindet, soll später noch eingehend untersucht werden.

Die Angaben eines der von mir benutzten 5 Barometer auf der Reise Sydney—
San Franeisco habe ich von der Bearbeitung ausgeschlossen, da die Beobachtungen an diesem
.in sich widersprechend waren.

Das Glasrohr dieses Barometers weist übrigens einen Fehler in der Herstellung auf.
Die beiden Enden der Kapillare erweitern sich nämlich nicht gleichmässig.

Für die Beobachtungen an den Quecksilberbarometern ist es sehr wichtig, dass sie
so aufgehängt sind, dass die Vibrationen des Schiffes, die durch das Arbeiten der Maschine
entstehen, nicht auf sie einwirken. Vibrationen sind stets vorhanden, selbst wenn die be-
wegten Massen der Maschine auf das sorgfältigste ausbalanziert sind. Bei den beschränkten
Raumverhältnissen an Bord ist es nicht immer leicht, in einfacher Weise eine gute Auf-

hängung zu erreichen.

 

d
3

 
