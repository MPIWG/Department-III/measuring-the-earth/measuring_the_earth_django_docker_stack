 

 

Reduction factors IF’, corresponding to various depths of compensation in kilometers.

[Depths in kilometers].

 

 

 

 

 

 

 

 

 

INS. of
329.8 231.3 162.2 120.9 113.7 79.76 55.92

ring.

| —  —

Oo esse ee ee ee 0.997
OB a ee ec ee nee ae 0.997 .996
ONE SEELE | AS E RS er 0.997 0.997 .996 .995
IE le et 0.997 .996 .996 .995 .993
Da RE 0.997 .996 .995 .995 .992 .988
24 0.997 .996 995 993 | .992 .988 .984
23 .996 .995 .992 .989 .988 .984 IT
2 995 .992 388 | .985 984 | 974 .965
21 .992 .988 9854 IT .974 .965 .952
20 .988 .984 IE | .967 963 .952 .930
19 .984 974 .965 .955 952 930 .897
18 .974 965 .952 .935 .930 .897 .358
37 .965 .952 .930 .903 .897 .858 .800
16 .952 .930 .897 .866 .858 .800 122
15 930, | .897 .858 al .800 .722 .618
14 .897 .858 .800 UN 122 .618 493
15 .858 .800 22 .638 | ‚618 493 | .358
12 .s00 .122 .618 ol 493 1008 234
ul .122 .618 .493 .382 ‚358 234 239
10 ‚618 493 || 958 234 .139 .077
9 493 .358 234 153 2189 .077 .040
8 Bose | 23% .189 .086 .077 .040 .020
m 234 .139 .077 .045 .040 .020 .010
6 4189 AU .040 .022 .020 .010 .005
5 .077 .040 020 ON 010° 005 .005
4 .040 .020 .010 .006 .005 .003 .HOL
3 .020 .010 009 | .003 .003 .001 ‚001
2 .010 .005 .003 .001 .001 ‚001 .000
u .005 .003 ‚001 .001 .001 .000 .000

It may be noticed that, with the exception of the column headed 120.9, the figures
are the same in the various columns; that the columns differ from each other simply in
having the figures displaced vertically. This arises from the fact that the successive assumed
depths of compensation, with the one exception stated, are the same as the outer radii
of the successive rings. An inspection of the formula shows that the relation stated is true
when such a selection has been made. This selection of depths saved considerable time in
computing the factors F.

The following nine selected cases will serve to indieate how widely the computed
defleetions, after isostatic compensation is considered, depart from the computed topographic
deflections:

rk aha

 

 
