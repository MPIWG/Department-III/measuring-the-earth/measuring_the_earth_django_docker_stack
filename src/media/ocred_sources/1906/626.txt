Gun genen: ie

Talı apl

FI REER IL TEmINE ALL LE pm al

Fr |

n'

330 0

 

enthält ein astronomisch-geodätisches Netz I. Ordnung nördlich der europäischen Längen-
oradmessung in 52° Breite, das sich über Norddeutschland und Dänemark erstreckt. Das
Netz besteht aus 9 dureh geodätische Linien gebildeten Polygonen und aus 4 davon aus-
gehenden Einzellinien; es umfasst 28 Punkte, von denen 19 Laprace'sche Punkte sind,
Auf 2 weiteren Punkten ist die Breite und die geogr. Länge, auf 7 Punkten die Breite
und das Azimut und auf 1 Punkt die Breite allein bestimmt worden, während 3 Punkte
astronomisch überhaupt nicht festgelegt sind und nur als Polygoneckpunkte auftreten. Für
die astronomischen Punkte, zu denen noch Berlin und Göttingen hinzugefügt wurden, sind
schon vorläufige, aber bereits ziemlich scharfe Werte der Lotabweichungen berechnet worden,
und zwar für 26 Punkte die Lotabweichungen in Breite und in Länge und für 1 Punkt
nur die in Breite. Es wurden zwei Systeme (& A, & x) abgeleitet, beide für Rauenberg
bei Berlin als Ausgangspunkt, jedoch das eine mit Bussev’s Elementen des Erdellipsoids,
und mit &=0" und A=0" für Rauenberg, und das andre mit

a — Aussen (1 — onao)) 0 = (Isrssen )

sowie mit #—=--5” und a=-+ 4 für Rauenberg. Die zweiten Elemente beruhen auf all-
gemeineren Untersuchungen von Herrn Prof. Hnıauurr über den Verlauf der Lotabweichungen
in Zentral- und Westeuropa und werden in neuerer Zeit bei Arbeiten über die Geoidgestalt
im Zentralbureau der I. E. bevorzugt. Bei diesem zweiten System der Lotabweichungen
verläuft die Kurve für ®=U nahe der Nord- und Ostseeküste entlang, während die für
®—-5” ihr im allgemeinen parallel ist und sich längs der südlichen Begrenzung des
astronomisch-geodätischen Netzes hinzieht. Hierin spricht sich eine regionale Erhebung des
Geoids innerhalb Norddeutschlands aus, die man etwa mit einer Schwelle vergleichen kann.
Der Mittelwert der A im zweiten System ist nur ein Bruchteil der Sekunde, so dass die
neueren Elemente hier schon für das immerhin beschränkte Gebiet eine Bestätigung finden.

In der Tabelle auf 8. 136 sind die ermittelten Lotabweichungen in derselben Weise
wie in den früheren Berichten zusammengestellt '), wobei aber die aus den Azimutbestim-
mungen abgeleiteten A und A’ mit kursiven Ziffern gedruckt sind.

Herr Geheimrat Harmerr leitet in seiner Abhandlung über »die Grösse der Eirde
aus den einzelnen grösseren Gradmessungen, und zwar zunächst aus den 4 grossen euro-
päischen Bogen, Werte für die halbe grosse Achse des Erdellipsoids ab, unter Annahme
des Besse1’schen Abplattungswertes: a—=1: 399,15, der, da er mit dem aus den Schwere-
messungen folgenden Wert: a=1: 298,3 nahe übereinstimmt, aus Zweckmässigkeitsgründen
vorerst beibehalten wurde. Es sollen nur die Hauptergebnisse dieser Arbeit, soweit sie hier in
Betracht kommen, angeführt werden; im übrigen aber sei auf die Abhandlung selbst verwiesen.

1. Die russisch-skandinavische Breitengradmessung ergibt gegen die Busser’sche

halbe grosse Achse:

”2)

= Valras)r si

1) Eine vereinfachte Übersicht über diese Lotabweichungen findet sich auch schon in dem „Jahresbericht u. s. w.

für 1905/06. Potsdam 1906”. Ss. 14—16.
2) F. R. Henuert, Die Grösse der Erde. Erste Mitteilung. Sitz.-Ber. der Königl. Preuss. Akad. der Wiss. Berlin

1906. S. 525—537.

 

 
