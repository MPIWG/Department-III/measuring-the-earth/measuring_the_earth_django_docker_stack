Tr ie

 

Tee rw ww ern pn

 

1
un zele de jeune homme, il prit alors en main les determinations de la pesanteur au moyen
de pendules, dont il a publi6 les resultats dans quelques memoires; le dernier en fut publie
en 1904. L’annde suivante il mourut, honore et aime de tous ceux qui ont eu le bonheur
de le connaitre,

Une fort grande perte pour la science fut la mort du Baron Frrpınann von Rıcht-
HOFEN survenue le 6 Octobre 1905. Ce n’est pas ici le lieu de signaler les grands merites
que cet homme &minent a eus pour la geologie et la geographie. Il sufit de constater que
parmi les representants de ces sciences il a &t& place, omnium consensu, au premier rang,
tant par ses publications, que par son enseignement comme professeur a Bonn, & Leipzig,
et & Berlin. Dans cette derniere ville il fat nomme plus tard, il y a environ 4 ans, directeur
da nouvel institut geographique, dont la creation etait düe surtout & son Energique initiative.

Ce fut une heureuse idee, que justifiait le lien intime entre la geologie et la geodesie,
de nommer, en 1898, M. le Baron vox Rıchruoren delesue de l’empire allemand aupres de
notre association. Depuis cette annde il assistait & nos conferences et il aurait jou& certaine-
ment un röle preponderant dans la discussion sur la proposition faite par l’Association des
academies, dont nous devons nous occuper un de ces jours.

Nous deplorons sa perte, mais le souvenir de l’homme de genie, de l’homme de cur
nous reste,

Le 21 Mars 1906 est decede apres de longues souffrances M. le Dr. Franz MicHuarı
KARLINSKI, professeur et directeur de l’observatoire & Öracovie, qui depuis de longues anndes
etait un membre fort honor& de la commission geodesique autrichienne. Il n’a publie que
peu de travaux geodesiques et c’est surtout dans l’astronomie et la meteorologie qu’il a eu
de granıs merites.

Nous recevions, il y a peu de mois, de M. Trrrmann la triste nouvelle de la mort
de l’assistant du Coast and Geodetice Survey, Erasmus Darwın Preston, a läge de 55 ans,
survenue le 2 Mai de cette annee.

Preston etait un savant de grand talent, qui par d’heureuses &tudes, d’abord en
Amerique, puis a Paris et & Vienne, s’e&tait acquis de vastes connaissances dans les domaines
de l’astronomie, de la geodesie et du magnetisme.

Depuis l’annde 1879 jusqu’& sa mort il &tait attache au Coast and geodetic Survey,
mais pendant cette periode il a fait partie de plusieurs expeditions scientifiques hors de
l’Amerique. Il a pris une part active a l’observation des passages de V&nus et de Mercure
sur le soleil et ä l’observation de deux &elipses totales, et, & l’invitation de notre Asso-
ciation geodesique, il a determine, en 1891 et 1892, la variation de.la latitude a Waikiki,
pendant la m&me periode que M. le Dr. Marcuse faisait des observations analogues &
Honolulu & peu de distance de Waikiki.

Eu 1898 il assistait comme delegue des Htats-Unis & la conference generale A Stutt-
gart, oü il s’occupait beaucoup des travaux de deux commissions nomme&es pour presenter
des rapports sur la mesure de l’arc du meridien dans la R&publique de l’Equateur et sur
le service international des latitudes.

Preston a publi6 les resultats de ses recherches dans un grand nombre de me-

 
