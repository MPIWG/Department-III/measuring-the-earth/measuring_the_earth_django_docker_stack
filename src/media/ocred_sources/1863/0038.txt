 

ge

dadurch, dass die Schwingungen viel langsamer sind, so dass man häufig die einzelnen Bilder
‘n einer Anzahl bestimmter Lichtpunkte, welche in Kreisform um einen Mittelpunkt zu hüpfen
scheinen, noch deutlich erkennen kann. Später am Abend beschleunigen sich die Schwin-
gungen, werden aber selten so schnell, dass sie, wie am Mittage, in einen grösseren Licht-
schein übergehen.

Die Intensität des Heliotropenlichtes ist sehr stark, auf Entfernungen von etwa drei
Meilen muss die 9 bis 10 Quadratzoll grosse Spiegelfläche um wenigstens die Hälfte verklei-
nert werden, weil sonst das Licht so scharf und stechend ist, dass es nicht mit Sicherheit
beobachtet werden kann. Bei noch kleineren Entfernungen kann es durch ein vorgescho-
benes farbiges Glas gedämpft werden, wozu sich grün am besten eignet. Der volle Spiegel
reicht auf 10 bis 15 Meilen aus, wenn die Luft nicht zu dunstig und räucherig ist.

Die Dauer der ruhigen Bilder ist sehr verschieden. Es giebt Tage, aber selten, wo
man selbst in den Mittagstunden noch beobachten kann; dagegen giebt es aber auch welche,
wo das Zittern nicht aufhört und so heftig ist, dass man gar nicht beobachten kann.

Besondere Witterungsverhältnisse haben einen entschiedenen Einfluss. Bei Höhen-
rauch (trockenem Nebel) bleibt das Licht, so lange es durchdringt, den ganzen Tag über
ruhig, und eignet sich vortrefflich zum Beobachten, nur muss es nach Azimuth und Zenith-
distanze aufgesucht werden, wie ein Stern bei Tage.

- An windigen Tagen, besonders bei trockenen N.O.-Winden, tritt ebenfalls selten Zit-
tern ein. Dagegen ist das Zittern nach starken Regengüssen immer so heftig, dass das
Beobachten unmöglich ist. — Auch Spiegelungen kommen vor, ich habe aber die Spiegel-
bilder nie anders, als genau in einer Verticalebene geschen.

Diese meine Beobachtungen sind hauptsächlich bei Gelegenheit der Gradmessung und
Küstenvermessung angestellt, sie beziehen sich also auf eine bestimmte Localität, den Preussi-
schen Küstenstrich an der Ostsee, und auf Entfernungen von 4 bis 8 deutschen Meilen. Es
wäre daher wünschenswerth, die Beobachtungen unter verschiedenen anderen Verhältnissen
fortzusetzen. Hieran schliessen sich nun noch verschiedene andere Fragen an, z. B.:

1. Stehen die ruhigen Bilder in irgend einem Zusammenhang mit der Wärmeabnahme
zwischen Standpunkt und Object, oder mit der irdischen Strahlenbrechung?

3. Das Heliotropenlicht zeigt zuweilen Farben, besonders Roth, Blau oder Grün.
Welches sind die Umstände, unter denen sie sich zeigen, und stehen diese Farben
mit der Wärmeabnahme in Verbindung ?

3. In den Mittagstunden, wenn das Heliotropenlicht gross und flackernd erscheint,
bemerkt man zuweilen, dass der intensivere Lichtkern langsame, Minuten dauernde
horizontale Schwankungen nach links und rechts macht. Ein solches langsames
Hin- und Herschwanken des Heliotropenlichtes habe ich auch an einem warmen
und windstillen Sommertage gegen Abend, kurz vor dem Eintreten der zweiten

Periode des Zitterns und Hüpfens der Bilder, bemerkt, wo das Licht sehr ruhig

und klein wie ein Sternchen erschien. Das Schwanken nach jeder Seite hin

ak nl

In: 106 ph ds va hs äh an mann 1

 
