Kt TFA Ye ng ee Ta TTT |

IRT

ya

a a a

a BE

werden könne. Hr. Dr. Hirsch übernahm es, das Chronometer unter seiner Leitung anfer-
tigen zu lassen.

Hr. Denzler erstattete demnächst Bericht über seine fehlgeschlagenen Bemühungen,
eine Dreieckskette nach der Lombardei, über die Alpen, so zu führen, dass alle Dreiecks-
punkte leicht zugänglich seien und sich unter der Schneegrenze befänden. Er legte ein neues
Dreiecksproject zu einem Uebergang über die Alpen vor, welches die Punkte Hundstock,
Titlis, Hangendhorn, Sixmadun (Badus), Pizzo Basodine, Pizzo Costa und Limidario ent-
hält, die sämmtlich zugänglich sind, so dass in allen Dreiecken die drei Winkel gemessen
werden könnten. Wenn man aber auf das Messen der 3 Winkel verzichten wolle, so könne
ein anderes Netz mit Finsteraarhorn, dem Scopi und dem Sonnenhorn gebildet werden, mit
dem man von der Seite Chasseral-Röthifluh ebenfalls bis Mailand mit nur 10 Dreiecken ge-
langen könne, während das erste System mit dem Titlis 14 Dreiecke enthalte. Die Com-
mission entschied sich für das Titlis-System.

Hierauf wurde über die Frage verhandelt, ob man mit den neuen Winkelmessungen
von .den Hauptdreiecken der westlichen Schweiz ausgehen solle, oder ob es vorzuziehen sei,
das ganze Dreiecksnetz von neuem zu messen. Die Commission entschied sich für das Letz-
tere und übertrug Hrn. Denzler die obere Leitung der neuen Haupttriangulation mit dem
Wunsche, dass wo möglich noch im Laufe des Jahres die Verbindungen mit den Dreiecks-
ketten der benachbarten deutschen Staaten eingeleitet werden möchten.

Schliesslich wurden noch die Pendelbeobachtungen und die Einwirkung der Berg-
massen auf die Abweichung der Lothlinie besprochen.

Die Commission entschied, dass zunächst auf den Sternwarten die Einwirkung be-
nachbarter Berge auf die Abweichung der Lothlinie in so weit zu untersuchen sei, dass im
Meridian und im ersten Vertical der Sternwarten zu Beobachtungen passende Punkte aus-
gewählt würden, und ersuchte die Herren Directoren, diese Untersuchung, jeder für seine
Sternwarte, zu übernehmen. Ausserdem würde Hr. Denzler noch beauftragt, für das Haupt-

dreiecksnetz Materialien zum Studium über die Anziehung der Bergmassen zu sammeln.

II. Auszug aus dem Circularschreiben des Präsidenten Hrn. Professor Wolf an die
Mitglieder der geodätischen Commission. Zürich, den 23. Februar 1864.
(Eingegangen am 8. März.)

Der Präsident zeigt an, dass das Universal-Instrument von Ertel und der Pendel-
Apparat von Repsold eingetroffen sind. Ueber die Anfertigung des Chronometers werde Hr.
Dr. Hirsch in der nächsten Sitzung (Anfangs April) Bericht erstatten.

Durch ein Schreiben des Vorstehers des Eidgenössischen Departements des Innern,
Hrn. Bundesratli Schenk, wird die Commission aufgefordert, die Fixirung des Schweizerischen
Höhennetzes, Behufs Regulirung der Pegelbeobachtungen, in den Bereich ihrer Untersuchun-
gen zu ziehen.

 
