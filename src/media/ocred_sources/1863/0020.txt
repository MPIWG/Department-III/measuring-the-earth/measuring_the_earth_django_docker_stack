 

ir
i
K
IP

0

stimmten Wohnraum in zwei Stuben, deren eine der Mannschaft zugewiesen wurde, und
konnte so schon am 3. September nach Leipzig telegraphiren, dass alles wieder in Ordnung
und zur Beobachtung bereit sei. An demselben Tage rückte der Telegraphist ein, der für
die ganze Dauer der Längenbestimmungen hier zu verbleiben hatte. Am 5. und 11. Sep-
tember gelangen die ersten vollständigen Bestimmungen; die übrige Zeit war zum Theile
durch einseitig trübes Wetter oder Störungen in der Leitung verloren gegangen. Am 12.
wechselten die Beobachter, um persönliche Gleichungen zu eliminiren, und brachten jeder
seinen Taster und sein Relai mit. Da Dr. Weiss sich in Leipzig unter Anleitung von Dir.
Bruhns mit den Leipziger Einrichtungen bekannt machen musste, so traf der letztere erst
am 15. in Dahlitz ein. Es gelangen hierauf am 18., 23. September, 3. und 4. October vier
vollständige Reihen mit gewechselten Beobachtern, die deshalb am 5. October in ihre Hei-
math zurückkehrten. Obschon Dr. Weiss an diesem Tage erst um 7 Uhr Abends in Prag
eintreffen konnte, von wo er eine Stunde nach Dablitz zu fahren, hier die mitgenommenen
Apparate auszupacken und sich auf den in einer halben Stunde kaum zu erreichenden Berg

zu begeben hatte, so war doch schon um 9 Uhr die Beobachtung in vollem Gange, was ich

‘als einen Massstab der Leistungshöhe anführen zu müssen glaube, welche durch die heutigen

Communications- und Correspondenzmittel ermöglicht ist. Nachdem übrigens am 6. October
noch eine unvollständige Reihe geglückt war, gelang am 7. wieder eine vollkommene, mit
welcher die Längenbestimmung abgeschlossen wurde.

Da um diese Zeit das zuletzt für Ende August sicher zugesagte Wiener Universale
noch immer nicht eingetroffen war, wie es denn in der T’hat auch heute noch nicht abgelie-
fert ist, so drohte mir die unerfreuliche Aussicht, halbverrichteter Sache von Dablitz abgehen
zu müssen; denn schon die Breite hätte nur allenfalls im Ersten Verticale bestimmt werden
können, zu welchem Zwecke ich vorsichtshalber ein zweites tragbares Mittagsrohr von Stein-
heil mitgebracht hatte, auf die Azimuthmessung hätte ich einfach zu verzichten, und dafür
im künftigen Jahre die Expedition zu wiederholen gehabt. Aus dieser Verlegenheit halt mir
nun glücklicherweise das zufällig verfügbare, treffliche, dreizebnzöllige Universalinstrument
von Pistor und Martins, dessen Director Bruhns sich heuer in Leipzig und Freiberg für die-
selben Zwecke, die hier noch zu erreichen waren, bedient hatte. Das Instrument langte am
6. October in Dablitz an und wurde unverzüglich in Thätigkeit gesetzt. Als ich am 10. Oc-
tober anderer Geschäfte wegen nach Wien zurückkehren musste, waren am Tage vorher
Hrn. Dr. E. Weiss bereits mehrere vollständige Breitenbestimmungen mit Circummeridian-
höhen mit Beobachtungen des Polarsternes ausser dem Meridiane, sowie eine weniger voll-

ständige Azimuthmessung gelungen, und ich verliess Dablitz mit der seitdem erfüllten Hofl-

nung, dass trotz der vorgerückten Jahreszeit noch alles zu Stande kommen werde, was man

billigerweise zu fordern berechtigt war. Was die Azimuthe betraf, so hatte die Erfahrung
gelehrt, dass die dortige, zwischen Moldau und Elbe gelegene Gegend im Herbste eines so
reinen Horizontes, wie man dessen bedürfte, um die gegen 18. d. M. entfernte Schneekoppe

deutlich zu sehen, sich nur äusserst selten erfreut. In den zwei Monaten, die ich beinahe

u Bl

m

ll rd

al

wi

an cha ds u ah an Han

 
