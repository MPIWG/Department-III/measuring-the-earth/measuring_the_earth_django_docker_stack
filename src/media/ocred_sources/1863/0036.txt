 

Morschlıce

1. Nachdem jetzt in dem ganzen Bereich der mitteleuropäischen Gradmessung
sämmtliche Staaten ihren Beitritt erklärt haben, hat sich das Bedürfniss nach einer allgemei-
nen Conferenz fühlbar gemacht, und es sind auch bereits im General-Bericht dahin gerichtete
Wünsche von verschiedenen Seiten her ausgesprochen worden. Ich glaube mich daher der
Zustimmung der sämmtlichen Herren Bevollmächtigten versichert halten zu dürfen, wenn ich
sie hiermit zu einer in Berlin abzuhaltenden General-Oonferenz einlade, und als Tag der
Versammlung den 15, October festsetze. Der Stoff, welcher sich für die Verhandlungen dar-
bietet, ist reichhaltig.

Die zunächst wichtigsten Punkte, welche zu besprechen sein werden, dürften etwa
folgende sein:

1. Die Regulirung der Maasseinheiten, wo directe Vergleichungen nicht mehr mög-

lich sind.
2. Die Fehlervertheilung bei den Anschlussseiten der Dreiecksketten und bei dem
Transport der Azimuthe.

3. Die Feststellung der Fehlergrenze für Polhöhen-Azimuthal- und telegraphische
Längen-Bestimmungen.

4. Gruppirung der Punkte, deren astronomische Bestimmung wünschenswerth er-

scheint.

S

Intensitätsbestimmungen der Schwere.

@P)

Astronomisch - geodätische Polar-Coordinaten.

I

Vervollständigung der Nivellements und Regulirung der absoluten Höhenver-
hältnisse.

8. Allgemeinere Fragen, als Redactions-Angelegenheiten, theoretische Untersuchun-

an u. w usw.

Nächst der Erledigung der wissenschaftlichen und geschäftlichen Fragen ist aber aucl
die persönliche Bekanntschaft der Bevollmächtigten unter einander nicht ganz gering anzu-
schlagen. Bei gemeinsamen Besprechungen tritt, durch die Vielseitigkeit der Auffassungen
und Anschauungen, das Ziel für jeden Einzelnen viel klarer und bestimmter hervor; der
gegenseitige Austausch der Ansichten und Ideen giebt Anregungen zu neuen Ansichten und
Ideen, steigert die Intensität der Kräfte der Einzelnen und bringt neues und frisches Leben
in die gemeinsame T'hätigkeit Aller.

Ueberzeugt von der Wichtigkeit dieser Gründe erlaube ich mir, den betheiligten

hohen Staatsregierungen diese Wünsche vieler Bevollmächtigten zu unterbreiten, und ver-

binde damit die ganz gehorsamste Bitte, mir gestatten zu wollen, die Beschickung der

IUMe Mb 1 A

vo)

id

Lak tn

il

hu add as 5 bh hd Ak ans mn

 
