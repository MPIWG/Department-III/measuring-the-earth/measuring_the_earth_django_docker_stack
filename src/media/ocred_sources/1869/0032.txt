 

fi
q
i
a
Ri
F
f
f
u
}
U

LD

Der w. F. ist aus dem Mittel der 6 Stationen

für eine Schwerpunkts- für
bestimmung
in Lage 1. +0.030
in Lage IT. 10.028

 

die 16 Schwerpunktsbestimmungen
auf einer Station

mn

+-0 .007
+-0.007,

welche letzte Grösse „s!,, der einen und „7155 der andern Länge ist. Da die grösste Cor-

rection wegen der Schwerpunktslage und der Verschiedenheit der Schwingungszeiten in beiden

mim

Lagen auf keiner Station mehr als 0.217 beträgt, so ist
genügend.

die erreichte Genauigkeit mehr als

Die Längenmessungen haben für die Entfernungen der Sehneiden von einander er-

geben:
Dunkle Sehneide.

Länge w. F. Länge

mm mm

Helle Sehneide. Mittel.
w. F. Länge w. F.

mnt min min

Leipzig, Serie I.  999.86725 0.00060 : 999.86771 40.000985 999.86748 =0.00057

Gotha 86631 0.00049 86739 0.00063 86685 0.00040
Seeberg 86922 0.00048 87063 0.000583 86993 0.00036
Inselsberg 86741 0.00055 86771 0.00057 86756 0.00040
Berlin 86939 0.00035 87000 0.00037 86970 0.00025
Leipzig, Serie II. 87014 -+0.00060 87092 40.000834 87053 +0.00034

Daraus folgt im Mittel der w. F. einer Längenmessung:

nin

bei dunkler Schneide = +0.00199 = 302507 der Länge,

= keller - = +0.00215 =
Der w. F. daher aus 16 Längenbestimmungen:

nin

bei dunkler Schneide = +-0.00050 = 35%
bei heller Schneide = =-0.00054 = +9:
und der w. F. des Mittels beider

mm

= 0.000537 = a7
Die Differenz der Längenmessungen zwischen heller und

dunkel — hell: Leipzig, Serie I.
Gotha
Seeberg
Inselsberg
Berlin
Leipzig, Serie I.

Mittel

3000 = =
5000

i
i
4
i
3
4

der Lange,

Re
0000
1
500

000

der anse.

dunkler Schneide ist (Mittelwerthe)

my

—0.0004.6
=O VOLO.
—0.0014.1
—0 .0003.0
- 0.0006.1

>. 0008

mm

= —(.0007.7.

Die Differenz ist um so kleiner, je günstiger sich die Beleuchtung gestaltet hat.

 
