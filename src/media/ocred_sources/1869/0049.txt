Te

Äh FRA TOTRBTT* A nA Ten |

TR

Ua

ü
&
R
z
%
m
®
S
S
: N

a

ein Object, wenn es bei zweiter Lage des Fernrohrs nicht mehr sichtbar sein sollte, später
durch besondere Verbindung in dieser Lage und Stellung des Kreises nachgeholt werden.
Dieselbe Operation wurde demnächst auf fünf andern Ständen des Kreises wiederholt, in
welchen der Nullpunkt bei 30, 60, 90, 120 und 150 Grad eingestellt wurde, so dass jedes
Object auf 12 um 30 Grad verschiedenen Stellen des Kreises eingestellt ist. Und da jedesmal
der Theilstrich links und rechts abgelesen ist, so beträgt die Zahl der mikroskopischen Ein-
stellungen für jedes Object 72.

So vortheilhaft die Anwendung des Nullpunktes für die Winkelmessung ist, so haben
doch die Erfahrungen des letzten Sommers ergeben, dass mancherlei Uebelstände damit ver-
bunden sind, wenn nicht die grösste Vorsicht angewendet wird. Entweder ist die als Null-
punkt dienende Thurmspitze zu weit entfernt, oder zu nahe, um ein klares Bild im Focus des
Oeulars zu geben. Im ersteren Falle ist auch Phase zu befürchten. Dann steht selten die
Helmstange, welche entweder oberhalb oder unterhalb des Knopfes genommen werden muss,
genau senkrecht. Oft sogar, wenn der Thurm seiner Nähe wegen zwar sehr gut einstellbar
ist, wie es in Cöln und Roermond der Fall war, wo ein in derselben Stadt gelegener Thurm
als Nullpunkt diente, drängt sich die Befürchtung auf, dass der Thurm bei seitlicher Erwär-
mung durch die Sonne eine Biegung erhält und so der Nullpunkt variabel ist. Die Beobach-
tungen des letzten Sommers zeigen in der That auf einigen Stationen Unterschiede, welche
auf die Unsicherheit des Nullpunktes zurückgeführt werden müssen. Um diesen Uebelständen
abzuhelfen, soll für die Folge ein künstlicher Nullpunkt, eine Tafel mit schwarzer Marke auf
weissem Grunde, welche je nach der Lokalität in der Entfernung von ', bis 1 Meile aufzu-
stellen ist, angewendet werden. Es liegt dieses eigentlich so nahe, dass man sich wundern

' muss, nicht längst darauf verfallen zu sein, aber man muss erst durch die Erfahrung be-

lehrt werden.

Das Instrument, welches in den Jahren 1868 und 69 zur Winkelmessung benutzt ist,
ist das dem Geodätischen Institut gehörige Universalinstrument No. I von Pistor & Martins.
Die Kreise sind zehnzöllig und in Zwölftelgrade getheilt. Der Schraubenkopf der Mikroskope
ist. in 120 Theile getheilt und erlaubt die Ablesung der Zehntel-Secunde, da 2, Umdrehungen
der Schraube auf ein Intervall des Theilkreises gehen, die unmittelbaren Theilstriche des
Schraubenkopfs also der Secunde entsprechen. Das Instrument hat sich vortrefflich bewährt,
des vielfachen Transports per Axe und Eisenbahn ungeachtet ist nie ein eigentlicher Fehler
entstanden. Die Veränderung des Collimationsfehlers, der Niveaus und des Nullpunkts des
Höhenkreises, welche nieht zu vermeiden ist, kann hier nicht in Anschlag kommen, da solche
Berichtigungen auf jeder Station zuerst vorzunehmen sind. Die Bewegung des Instruments .
ist stets eine sanfte, obgleich entschieden feste gewesen und die Fäden des Fernrohrs sowohl
als die der Mikroskope waren stets schön und unverbesserlich. Der Theilungsfehler wird durch
die Combination der Beobachtungen eliminirt, indem jeder Winkel durch den ganzen Kreis
herum getragen wird, so dass das Mittel aus allen Beobachtungen als frei von Theilungsfehler

angesehen werden kann. Gegen dieses Mittel zeigten indess einzelne Stände kleine Abweichun-
General-Bericht f. 1869. 7

 
