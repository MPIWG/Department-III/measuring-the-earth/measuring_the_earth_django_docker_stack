| rn

nn |

I AA

Inn

Amt

ee iR

vations sont terminees. On se sert de theodolites de Pistor et Martins dont le cereles azimutaux

m

ont 0,28 de diametre avee deux mieroscopes mierometriques qui donnent les deux secondes;

ın m

les Iunettes, de 0,047 d’ouverture et 0,515 de distance focale, ont un grossissement de 34 fois.

est sur les cötes des grands triangles des chaines prineipales ainsi que sur ceux des
quadrilateres, que les triangulations secondaires s’appuient. Ces travaux sont destines exelu-
sivement ä servir de base & la topographie du territoire espagnol sur la Peninsule.

Dans les iles Baleares j’ai fait des triangulations speeiales qui s’appuient chacune sur
une base mesurde. Afin d’eviter les deplacements et les transports par mer de V’appareil em-
ploy& A la mesure de la base centrale, et en möme temps dans lintention d’essayer un systeme
que, tout en donnant lexactitude suffisante, permit d’obtenir une vitesse beaueoup plus grande
dans l’operation et plus de simplieit6 dans les caleuls, je fis construire un nouvel appareil
dans lequel j’ai supprim& les mieroscopes mierometriques en me contentant de faire, au moyen
de vis de rappel, les coineidences des traits graves aux extremitss de la r6gle avec d’autres
traits portes par des pieces qui marquent les intervalles successifs que la regle doit parcourir.
Cette regle, pourvue de quatre thermometres de mercure, est en fer. J’ai trouve pour son
coeffieient de dilatation lineaire 0,000010798 + 0,000000002 pour chaque degr& centigrade;
et pour la longueur de la m&me regle, & 21,98 entre les traits extr&ömes, 4,000587 -+ 0,000002.

Voiei les resultats que Jai obtenus, avec cet appareil, dans la mesure des petites
bases de Mahon et d’Ivice:

Base de Mahon

Liere mesure 2ieme mesure Differences
Liere jourmnde 400,0075 400,0072 + 0,0003
Zieme  - 400,0699 400,0697 + 0,0002
Sieme  - 400,0670 400,0671 0,0001
dieme - 400,0639 400,0632 + 0,0007
Hieme 400,0690 400,0695 — 0,0005
Gjeme  - 359,0271 359,0271 0,0000
2359,3044 2359,3038 + 0,0006
Base d’Ivice
liere mesure 2ieme mesure. Differences
Jiere journde 400,0564 400,0567 — 0,0003
Zieme  - 400,0141 400,0142 0,0001
Ziime - 399,9102 399,9106 — 0,0004
4ieme  - 464,5393 464,5390 + 0,0003
1664,5200 1664,5205 = 0,0008
La vitesse de mesure a ete, en moyenne, de 120 metres par heure.
Madrid le 11 fevrier 1870. 0. Ibanez.
General-Bericht f. 1869. I

 

 
