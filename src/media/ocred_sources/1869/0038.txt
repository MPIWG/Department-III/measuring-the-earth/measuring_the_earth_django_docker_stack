 

38

Die Beobachtungen der Sterne im ersten Vertikal sind vollständig redueirt. Die Stern-
positionen sind aus verschiedenen Catalogen genommen und auf die Tab. Reduct. zurückge-

führt.
für 1869.0.

B. A. C. No. 6252

« 1869.0

hevma Ss

ö 1869.0

! I

18 17 50.16 -+49 39 45.22

Für die auf dem Seeberge beobachteten Sterne finden sich so die mittlern Oerter

Autorität

(Radcliffe Catalogue)

 

a re re

0255 18 18 11.19 49 3 22.40 (Greenwich Seven-years-Cat., Greenw. 1840.
1845; Radel.; Armagh Cat.)
Ba SAAL .0. Io 19311 1,.10 (Greenw. 7 y., Greenw. 1864. 65.; Radel.)
6603 19 11 54.30 29,50 26.28 (Badel.)
6734 19 32 55.65 4955 6.57 (Astron. Vierteljahrsschrift)
6895 1957 39.20 2942 28.80 (Greenw. T y.; Gr. 1845.; Radel. Arm.)
6985 20 11 54.68 49 49 47.90 (Radel.)
Mit diesen Deelinationen finden sich die Breiten auf dem Seeberg aus den Sternen
B. A. C. No. 6252 50'586 5.69 Gewicht 3
6255 au) 3
6421 6.19 2
6603 5.88 2%
6734 9) 3
6895 6.28 2%
6985 4.48 2

 

li

Mittel y = 50 56 5.34 Gewicht 18.
Die Gewichte sind den Beobachtungstagen entsprechend, die halben Gewichte rühren daher,
dass der Stern an weniger Fäden und bei schlechter Luft beobachtet ist. Der wahrscheinliche
Fehler einer Breitenbestimmung aus einem Stern in einer Nacht oder vom Gewicht 1 ist

Are seh ach ara

+0.20 und es müsste danach der wahrscheinliche Fehler des Endresultats mit dem Gewicht

IM}
18, wenn die Declinationen absolut richtig wären, +0.05 sein.
Die Resultate der Breite sind schliesslich:

aus Beobachtungen von Zenithdistanzen, südlich und nördlich vom Zenith 50 56 6.38

im ersten Vertikal, - . - - - 5,34

I}

Mittel 50 56 5.86.

Nach der Thüringischen Gradmessung ist angenommen:

0 el,
50 56 5.20
za

op ehemaliges Passagen-Instrument
Reduction auf den jetzigen Punkt

!

50.56, 3e

 
