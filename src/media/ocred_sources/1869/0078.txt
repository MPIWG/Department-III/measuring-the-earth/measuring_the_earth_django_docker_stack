\
1A
R n

 

u

fehlen darf, um mit der Neigung des Spiegels noch messbar zu sein, so ist die Einrichtung
getroffen, dass der Abstand der Berührungsplatten der Glaseylinder an dem einen Cylinder
mit Schrauben verstellbar ist. f

Ueber die beiden Pfeiler kömmt ein aus 4 Holzwänden gebildeter Rahmen a,b, c,d,
der durch eine aufgelegte Platte von Gusseisen e,e,e,e zum Tisch umgestaltet wird. Natürlich
sind in der Platte 2 Löcher für die Glaseylinder etwas weiter als die Cylinder ausgearbeitet,
so dass letztere frei durchgehen. Der innere Raum dieses Tisches oder Kastens ist mit schlech-
ten Wärmeleitern (Sägespähne oder Baumwolle ete.) ausgefüllt.

Auf die gusseisene nivellirte Platte kommt, nachdem die Kautschukplatten über die
Glaseylinder gesteckt und ihre Scheiben gg’ auf dem Tische ausgebreitet sind, der Glaskasten
ffıff, zu stehen. Auch dessen Boden hat 2 Löcher, durch welche die Glaseylinder frei hin-
durch gehn. Der Glaskasten sitzt also nur auf den 2 Kautschuk-Scheiben gg, auf und be-
wirkt so den wasserdichten Schluss durch seine Schwere.

An dem einen Ende des Tisches vor dem’ Glaskasten steht der Fernrohrträger 9, der

so hoch ist, dass das Objektiv über den Glastrog hinwegsehen kann. In den Glastrog kommt

am Ende des Troges das feststehende Parallelglas h festgekittet‘) auf den Glaseylinder D'.
Gegenüber am Fernrohrende des Troges steht der Schuberschlitten ö, welcher den verstellbaren
Spiegel k trägt. Endlich ist auf den Boden des Glasgefässes eine ebenfalls aus Glasplatten ge-
bildete Brücke oder ein Schemel I gesetzt, der den Maassstäben als Unterlage dient. Damit
der Maassstab sich frei und unabhängig vom Schemel ausdehnen kann, sind 4 Rolleylinder
von Glas quer über die Brücke gelegt. Die Cylinder m, m, m,m, liegen also senkrecht zur
Längenaxe des Maassstabes. Es sei der Maassstab eine Toise n. Nun kommen die "Träger
oder Ständer mit den Abschiebeeylindern zwischen die Spiegel und die Enden der Toise.
Sie sind durch ihren Fuss am Glaskasten orientirt und werden nur in der Höhe so gestellt,
dass sie auf die Mitte der Stabdicke treffen. Die Toise wird dann seitlich nach den Cylindern
gerichtet so, dass dieselbe Vertikalebene durch die Axe der Toise führt.

Wie wir den Apparat bis jetzt zusammengestellt haben, dient er, nachdem der Stab
unter Flüssigkeit gesetzt ist ete., um die absolute Ausdehnung der aufgelegten Toise zu be-
stimmen. Soll aber die Toise mit 2 Metern verglichen werden, so kommen auf die Toise
wieder Rolleylinder 0,0,o, o, und auf diese die 2 Meter p und p'. Man hat jetzt nur den
untersten Abschiebeeylinder, der bei B gegen den Berührungspunkt des Pfeilers drückt, her-
auszunehmen und den Spiegel k in der Höhe so zu verstellen, dass seine Drehungsaxe in der
Mitte zwischen der Axe der Toise und der Meter liegt.

. “N . . . . Si
Es ist von selbst einleuchtend, wie Toisen unter einander verglichen werden. Sollen
\

1) Wenn die Planfläche am Glaseylinder D’ nicht $anz genau und im hohen Grade plan geschliffen
ist, so sitzt der nur am Rande umgekittete Spiegel nicht fest. In diesem Falle, der wohl immer eintreten
wird, ist es nöthig das Planglas erwärmt auf Pechtropfen aufzusetzen, so wie Fraunhofer seine Objektivlinsen
zum Poliren aufsetzte. Das Glas ist dann in so vielen Punkten unterstützt, dass keine Durchbiegung statt-
findet und die Pechtropfen gleichen die Gestaltfehler der Unterlage aus.

srl dach ara

 

i
1
i
i
i

 
