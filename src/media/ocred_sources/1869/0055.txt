en nn

TE YET NT NT |

am nun

a

u

A

9) Marke auf der Gesimsplatte am südwestlichen Ende der Main-
brücke der Main-Neckar-Eisenbahn, Anschlusspunkt an das
Grossherzoglich Hessische Nivellement erster Ordnung = 51,0804.

Börsch.

>. Bericht über die Fortsetzung des Nivellements nördlich der Elbe über Berlin
nach Stettin und Stralsund.

Die Absicht, im Sommer 1869 in Swinemünde einen registrirenden Pegel aufzustellen,
ist durch unvorhergesehene Hindernisse vereitelt worden, welche ihren Grund darin hatten,
dass der Boden daselbst fast überall aus Triebsand besteht und nicht erlaubte innerhalb eines
vorhandenen Gebäudes einen Brunnen zu graben und zur Communikation mit dem freien
Wasser im Hafen eine Röhrenleitung unter den Fundamenten hindurch zu führen. Es blieb
daher nichts anderes übrig, als an einer passenden Stelle ein neues Häuschen für den Pegel
zu bauen, nachdem ein Brunnen abgeteuft und die Röhrenleitung gelest war. Dieser Bau
wurde aber erst im Spätherbst fertig und musste den Winter über austrocknen, so dass der
Pegel erst mit Eintritt der guten Witterung im Jahr 1870 aufgestellt werden kann. Die Ver-
bindung mit demselben wird durch ein Nivellement von Anclam nach Swinemünde bewerk-
stelligt werden.

Das doppelt ausgeführte Hauptnivellement, welches 1868 von Röderau und Leipzig
über Berlin bis Neustadt-Eberswalde vorgeschritten war, wurde 1869 von dem Markscheider
Harnisch, dem Markscheider Festner und dem Geometer Francke bis Angermünde fortgesetzt
und von da aus, einerseits über Stettin und Pölitz nach dem Engen-Oderkruge, am Einfluss
der Oder in das Haf, andererseits über Pasewalk und Greifswald nach Stralsund weitergeführt,
und an beiden Endpunkten mit den daselbst befindlichen Pegeln in Verbindung gebracht. Die
ganze nivellirte Strecke beträgt etwa 43 Meilen, auf welcher 28 Fixpunkte hergerichtet wur-
den. — Definitive Angaben der Höhen über der Ostsee können aber für jetzt noch nicht
gegeben werden, weil das mittlere Niveau der Ostsee erst aus vieljährigen Pegelbeobachtungen
ermittelt werden muss, und weil ausserdem die polygonalen Abschlüsse der Nivellements noch
nicht so weit beendigt sind, dass eine definitive Ausgleichung stattfinden kann.

Durch den Anschluss des geometrischen Nivellements in Berlin und am Engen-Oder-
kruge an das trigonometrische Nivellement, welches ich im Jahr 1835 zwischen Swinemünde
und Berlin ausgeführt habe, und das in Commission bei F. Dümmler 1840 in Berlin erschienen
ist, ergiebt sich die Differenz auf dieser Strecke zwischen dem geometrischen und trigonome-
trisehen Nivellement, wie folgt: Nach dem Generalbericht pro 1868 Seite 68 liegt die Höhen-
marke am Anhalter Bahnhof in Berlin, aus dem trigonometrischen Nivellement hergeleitet,

m

37,256 über dem mittleren Niveau der Ostsee.

 
