 

Ye

Gutachten übergeben worden, in welchem der Stand der über die Triangulirung vorhandenen
Dokumente dargelegt und der Versuch gemacht worden ist, aus denselben zwei Dreiecksketten
herzustellen, welche das badische Netz mit dem bairischen in Verbindung bringen sollen, im
Norden von den Punkten Steinsberg und Katzenbuckel nach Nipf, Neresheim und
Haselberg, im Süden von Plättenberg, Trinitatis und Hohenlaub nach Roggenbursg,
Kornburg und Anger, unter Anschluss an die Basis Solitude-Ludwigsburg und das
Observatorium Tübingen.

Es hat sich dabei herausgestellt, dass die über die Winkelmessung vorhandenen Auf-
zeichnungen wohl erlauben würden, diesen Messungen einen solchen Grad der Zuverlässigkeit
zuzuschreiben, vermöge dessen sie sich unter Anwendung eines zweckmässigen Ausgleichungs-
verfahrens für die Gradmessung gebrauchen liessen, — dass diese Aufzeichnungen aber nur
noch höchst unvollständig vorhanden sind und die Werthe, mit welchen Bohnenberger in seinen
Rechnungen die beobachteten Winkel anführt, in denjenigen Fällen, wo eine Vergleichung
mit den Originalaufzeichnungen unmöglich ist, einen einigermaassen ausreichenden Maassstab
für die Zuverlässigkeit solcher Winkelangaben besässe. Ueberdies erhält in Folge des Um-
standes, dass die Bohnenberger’sche Triangulirung nicht nach einem einheitlichen Plane, son-
dern stückweise nach dem jedesmaligen Bedürfniss der Katastervermessung ausgeführt wurde,
die nördliche Kette eine ungünstige Gestalt; untergeordnete Punkte, welche in dieselbe auf-
genommen werden müssen, bringen Dreiecke mit grossen Fehlersummen in’s Spiel; über einige
Dreiecke der südlichen Kette fehlen selbst alle Angaben beobachteter Winkel. Auf was für
neue Arbeiten, falls die in dem Gutachten entwickelte Ansicht Zustimmung findet, bei uns hinzu-
wirken wäre, wird von dem Plan abhängen, nach welchem neben der bereits projectirten neuen
badischen Triangulirung überhaupt im südwestlichen Deutschland trigonometrische Verbin-
dungen herzustellen sind. Nach unserer Ansicht werden dabei vorzugsweise zwei Ketten,
welche in den oben angegebenen Richtungen anzulegen wären, ins Auge zu fassen sein.

Eine aus Veranlassung der bevorstehenden Einführung des metrischen Maasses und
auf Anregung der Centralstelle für Handel und Gewerbe von uns vorgenommene Nachmessung
der Bohnenberger’schen Basismessstangen hat folgende Ergebnisse geliefert.

Die Stangen wurden auf einem der Königl. Münze gehörigen Comparator mit dem
neuen vom Conservatoire des arts et metiers bezogenen messingenen mötre &talon, ferner mit
einem im Besitz der polytechnischen Schule befindlichen Metermaassstab von Stahl und end-
lich mit der 1820 von Fortin in Paris gelieferten Copie der Toise von Peru verglichen, nach
welcher Bohnenberger seine fünf genau zu zwei Toisen in Rechnung gebrachten Stangen ab-
gleichen liess. Auf die betreffenden Normaltemperaturen redueirt ergab sich:

Stahlmeter der polyt. Schule = 0,999965 + 0,0000051 metre etalon
(der metre etalon wird von Paris aus angegeben — 0,999996 Normalmeter).
Toise = 1,949035 meötre etalon.
Messstange No. 1 = 1728,255 Par. Lin.
„ ande a8, 104,

 

:
i
|

 
