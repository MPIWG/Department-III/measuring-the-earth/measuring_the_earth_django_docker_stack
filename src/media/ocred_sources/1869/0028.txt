 

h
if
f
f
f
i
{i

ee
Daraus ist das Endresultat mit Berücksichtigung der Gewichte:
Lund von Berlin westlich = 0 49.813 a 0.021,
wozu noch die Reduction der Instrumente auf die Pfeiler
in Lund mit 0.028,

in Berlin mit 1.0.045
kömmt, nach deren Anbringung das oben gegebene Resultat erscheint.
Die persönliche Gleichung fand sich an den verschiedenen Abenden zwischen den
Herren Valentiner und Bäcklund aus direeten Beobachtungen:

Kreis West Kreis Ost
V—B Zahl der Sterne V—B Zahl der Sterne Mittel
Juni 26. 0.018 6 10.002 6 0.008
I 97, 0.015 12 229.001 12 1.0.007
= 28. —0.005 12 —0.033 12 — (0.044
- 30. 0.033 12 —(.002 2 --0.015

Die Sterne zur Bestimmung der persönlichen Gleichung wurden mit nahe denselben Declina-
tionen gewählt, wie die Sterne zur Bestimmung der Längendifferenz, und eine Aenderung der
persönlichen Gleichung nach den Declinationen zeigt sich mit Bestimmtheit nicht, denn es
findet sich:

Mittl. Deel. Kr. West Mitt]. Decl. Ku. Ost
V—B V—B
0 ! s 0 ! Ss
+ 3 29 —0.049 +35 0.011
+15 22 +0.039 +14 44 +0.015
+32 35 —0.007 +25 25 —0.056.

Auch mit Hilfe des Apparates zur Bestimmung der persönlichen Gleichung wurde die absolute
persönliche Gleichung gefunden und zwar:

Kr. West Kr. Ost
VB VB Mittel
1. 0.004 0.035 0.015
do. 0.023 0.022 0.023
= 96. 0.027 0.018 0.022
a. + 0.084 0.006 10.039
30, 0.003 0.025 29
eh 4, 0.010 10.028 0.009.

Leitet man die Resultate hieraus ab, so ergiebt sich die persönliche Gleichung aus:

 

i
i

 
