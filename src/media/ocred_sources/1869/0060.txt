 

Ba:

Besondere Controlen hat der schon seit einigen Jahren fertige Theil des Haupt-
nivellirungsnetzes durch die Nivellirung von mehreren Zwischenlinien erlangt. Dieselben sind
von einem dritten Assistenten vollzogen worden, und haben durchgängig auf gute, zum Theil
auf eine überraschend genaue Uebereinstimmung mit den früher erlangten Ergebnissen des
Hauptnivellements geführt.

Der Geldaufwand, welchen die Gradmessungsarbeiten von Seiten des Königreichs
Sachsen bis jetzt erfordert haben, ist bis jetzt im Durchschnitt jährlich 6000 bis 7000 Thaler
gewesen. Detaillirte Angaben lassen sich, so lange noch alle Arbeiten im Gange sind, nicht
mit Sicherheit machen.

Freiberg, den 4. März 1870. .
Julius Weisbach.

3. Bericht über die im Jahre 1869 im Königreich Sachsen ausgeführten
astronomischen Arbeiten.

Theils des ungünstigen’ Wetters wegen, theils weil meine früheren Gradmessungs-
Assistenten, die Herren DDr. Helmert und Valentiner, ersterer die Observatorstelle in Hamburg,
letzterer eine gleiche in Leiden erhalten haben, konnten die projectirten Arbeiten für das Jahr
1869 nicht in der Weise ausgeführt werden, wie ich beabsichtigt hatte. Da die noch fehlenden
astronomischen Arbeiten im Königreich Sachsen überhaupt nur noch einen oder zwei Sommer
erfordern, schien, weil die geodätischen Arbeiten noch über eine beträchtlich längere Zeit sich
erstrecken werden, ein Eilen mit den astronomischen Arbeiten nicht so sehr nöthig und ich
habe mich daher eingerichtet, zunächst mit den Reductionen sämmtlicher Beobachtungen vor-
wärts zu kommen, um die definitiven Resultate ableiten zu können. Bis jetzt ist nun die
Reduction der Längenbestimmungen Wien-Leipzig vollendet, nächstdem wurde an einer Re-

duetion der bestimmten astronomischen Punkte bei Leipzig gearbeitet (der geodätische Theil -

ist nahe fertig) und die Verification einiger Winkel vorgenommen.

Ausserdem wurde, wie in dem Berichte für Preussen (Centralbüreau) schon gesagt ist,
die Länge des Secundenpendels in Leipzig bestimmt, dessen Resultat an dem erwähnten Orte
angegeben ist.

Die Bestimmung der sogenannten Gradmessungssterne (s. Gradmessungsbericht 1365
pag. 69) ist nahe vollendet. Mein Observator Herr Dr. Engelmann, der diese Bestimmungen
am hiesigen achtfüssigen Meridiankreis ausführt, theilt mir mit, dass bis jetzt 195 Sterne voll-
ständig, d.h. in jeder der beiden Kreislagen Ost und West, jeder Stern mindestens viermal
beobachtet ist. ‘Es restiren noch 11 Beobachtungen in der Westlage für 6 Sterne, 4 Beobach-
tungen in der Ostlage für 1 Stern, und da diese fehlenden Sterne im Frühjahr eulminiren,
werden diese Beobachtungen bald vollendet sein. Der grösste Theil der Reduetionen ist auch

schon gemacht, so dass sehr bald die Resultate publieirt werden können.

dh aa

des sah dh

 

i
1
|
i

 
