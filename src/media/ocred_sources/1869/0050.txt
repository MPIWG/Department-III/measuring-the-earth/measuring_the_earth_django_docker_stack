 

'
|
|

a:

gen in demselben Sinne, 2. B. ergab der zweite Stand, wo der eine Schenkel des Winkels bei
30 Grad eingestellt wird, durchgehends ein kleineres Resultat. Da diese und ähnliche Ab-
weichungen nicht wohl anders als dureh Theilungsfehler erklärt werden konnten, so wurde
der Versuch gemacht, den Theilungsfehler aus den Beobachtungen abzuleiten. Die dahin
führenden Betrachtungen sind folgende.

Da bei jeder Einstellung die gegenüber liegenden Mikroskope abgelesen sind, woraus
das Mittel genommen ist, so ist der durch das Verstellen des Kreises und das Anziehen der
Klemmsehraube jedesmal entstehende Excentrieitätsfehler eliminirt. Ebenso ist der Einfluss des
Collimationsfehlers beseitigt, wenn das Mittel aus beiden Lagen genommen wird. Der in dem
so erhaltenen Winkel zurück bleibende Theilungsfehler ist für jeden Schenkel des Winkels der
aus den gegenüber liegenden Punkten des Theilkreises combinirte, und daher für gegenüber

liegende Punkte derselbe. Der Theilungsfehler ist daher nur von 0 bis 180 Grad zu ermitteln.

Man geht nun davon aus, dass das Mittel aus allen Ständen frei von Theilungsfehler ist,
welches, wenn auch nicht in aller Strenge, doch nahe zutreffen wird, und bildet die Differen-
zen jedes Standes gegen dieses Mittel. Bezeichnet man mit p(m) die Correetion wegen Thei-
lungsfehler für die Stelle m des Theilkreises, also mit 9(o) für die Stelle o, so ist p(m)
— (m + 180), und jeder auf dem ersten Stande gemessene Winkel giebt eine Gleichung von

der Form

m— 0 + gm) — p(0)=M,
wo m der auf dem ersten Stande gemessene Winkel und M das Mittel aus allen Ständen be-
deutet. Sind nun viele Winkel auf dem ersten Stande gemessen, m, m), mi...) und

werden die zugehörigen Mittel mit 7, |’, M"... M%) bezeichnet, so erhält man folgende

Gleichungen

pm) — 90) =M —m
oa) go) N m

() pm) — g(0) = M" — m’

am) — 9(0) = MM — m”.

Die Summe von n Gleichungen dieser Art ergiebt
(2) Zp(m)—n.p(o)=S(M—m).

Sind die Beobachtungen so zahlreich, dass die Ablesungen m über den ganzen Theilkreis sich
so kommen in denselben auch alle Theilungsfehler vor, und man kann deren Summe
— o setzen. Denkt man sich den Theilungsfehler als Curve, wo (m) die Ordinate für die
Abseisse m ist, so bestimmt die Gleichung > (m) = o die Constante, da man den Theilungs-
fehler ebenso gut = k-+ p(m) setzen kann, so dass also k = o oder die Summe aller positiven
Theilungsfehler der Summe aller negativen gleich gesetzt ist. Hiernach erhält man aus der

Gleichung (2)

 

vertheilen,

90) = „2 m—M)

als die Correetion des Punktes o des Theilkreises.

 

1
!

Ans ah he a

 
