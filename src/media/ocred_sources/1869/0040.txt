 

ne

ZT,

Die Breitendifferenz findet sich daher aus den geodätischen Beobachtungen 04 57.47
aus den astronomischen und zwar:

aus dem Mittel aller Beobachtungen 4 54.52
aus den Zenithdistanzen allein 54.18
aus allen Sternen im ersten Vertikal 54.87
endlich aus den auf beiden Stationen gemeinsam im ersten Vertikal beobachte-

ten Sternen B. A. C. No. 6734, 6895, 6985 54.69.

Es muss noch erwähnt werden, dass die Declinationen der Fundamentalsterne, wenn

I
man sie aus dem Nautical-Almanac entnimmt, die Breite für den Seeberg 0.46, für den Insels-
I e N
berg 0.30 geringer ergeben würden. Eine noch um etwa 0.3 geringere Breite würde resultiren,

wenn man die Deelinationen auf den Katalog der Astronomischen Vierteljahrsschrift Band IV
reducirte.

Nach diesen Erörterungen ist es wohl keinem Zweifel unterworfen, dass auf dem
Inselsberg eine Lokalabweichung in Breite von etwa 3” vorhanden ist.

Die Bestimmung des Azimuths

ist auch redueirt. Auf dem Seeberge ist das Azimuth sowohl mit dem Universal-Instrument
als auch mit dem Passagen-Instrument mit gebrochenem Fernrohr, wie schon früher erwähnt,

bestimmt. Es findet sich mit dem Universal-Instrument das Azimuth des Inselsberges
/ 1

auf Stand I aus 8 Beob. = 243 39 24.80

- ie tz 22.12
- - 1 - -. > 29.094
- MM. ..- 26.99
- ee 20.94

 

° ! 1}
im Mittel = 243 39 24.16.
Die Differenzen bei den verschiedenen Ständen sind, da auf dem Inselsberge sich dieselben
Differenzen zeigen, nieht zufällige, sondern den periodischen Theilungsfehlern zuzuschreiben.

Hl
Es findet sich nämlich der w. F. eines Standmittels +0.25 und daraus der des Endresultats

2}
+0.11 und bei so geringen w. F. ist es gar nicht möglich, dass ohne periodische Theilungs-

2

fehler auf den verschiedenen Ständen sich eine Differenz bis zu 9.20 zeigen kann.
Die Beobachtungen am Passagen-Instrument geben aus 3 oberen und 3 unteren Culmi-

a

nationen des Polarsterns das Azimuth der Marke = 10.37 + 0.15 und es war der Winkel

zwischen Marke und Inselsberg auf

 

i
!
i

re bh aeceanua

 
