IN |

LEW ul

ITU AR

a. a

2. Bericht über die im Jahre 1868 im Königreich Sachsen ausgeführten astronomischen
Arbeiten.

Im Jahre 1868 wurde in der Bestimmung der astronomischen Punkte fortgefahren und
zwar zunächst fünf Punkte um Leipzig herum (Deukstein bei Wachau, Grenzhübel bei Knaut-
naundorf, Wachberg bei Rückmarsdorf, Markstein bei Göbschelwitz, Schwarzer Berg bei Taucha)
und die Pleissenburg in Leipzig selbst bestimmt. Die Arbeiten auf diesen Punkten bestanden:

1) in Messung von Richtungen nach den andern Punkten und nach der Pleissen-
burg in Leipzig,

2) in Polhöhenbestimmungen,

3) in Azimuthbestimmungen.

Zu den Messungen der Richtungen wurde ein Universalinstrument von Pistor u. Martins
mit 10zölligen Kreisen mit mikroskopischer Ablesung verwandt und jede Richtung an zwölf
Stellen des Kreises, an jeder Stelle achtmal eingestellt, wobei das Fernrohr mehrfach um-
gelegt wurde, so dass ebensoviel Richtungen in Lage Fernrohr rechts, als in Lage Fernrohr
links gemessen sind. Die Messungen wurden ausserdem so angeordnet, dass, wenn eine der
Zeit proportionale Drehung der Pfeiler stattfand, dieselbe sich eliminirte.

Obwohl die Pfeiler (es sind auf allen Punkten Steinpfeiler und zwar an vier Punkten
Granitpfeiler zu ebener Erde, auf dem fünften Punkte, Denkstein bei Wachau, ist auf einen
grossen Granitpfeiler, ein Backsteinpfeiler aufgesetzt und auf der Pleissenburg sind zwei Granit-
pfeiler auf der 9 Fuss dicken Umfassungsmauer in einer Höhe von 120 Fuss über dem Erd-
boden aufgesetzt) nur 1—2 Meilen von einander entfernt sind, konnten sie doch nur bei
günstiger Beleuchtung gesehen werden und war dies der Fall, so wurden sie oft unmittelbar
eingestellt. Auf den Pfeilern war noch ein viereckiges weisses Brett so aufgestellt, dass es
senkrecht auf die zu messende Richtung stand, und endlich wenn dies auch nicht sichtbar,
wurde von den Pfeilern Heliotropenlicht nach dem Punkte geworfen, von wo aus die Messung
geschah. Auf der Pleissenburg war eine Pyramide angebracht, die von den meisten der
Punkte eingestellt werden konnte und nur von einigen Punkten wurde noch ein auf dem
Pfeiler befestigtes Brett als Riehtungsobjeet genommen. Nächst dem Heliotropenlicht hat sich
als bestes Einstellungsobject das auf dem Pfeiler befestigte weisse Brett, welches ein Quadrat
von 2 Fuss Seite war, einstellen lassen.

Dass bei den verschiedenen Einstellungsobjeeten alle nöthigen Elemente zur genauen
Centrirung ermittelt wurden, braucht wohl nicht erst erwähnt zu werden.

Die Breitenbestimmungen

wurden mit zwei verschiedenen Instrumenten nach verschiedenen Methoden ausgeführt. Mit
demselben Instrument, mit welchem die Richtungen gemessen, wurden 48 Zenithdistanzen des
Polarsterns an sechs verschiedenen Stellen des Kreises und ebenso 48 Einstellungen an 4 süd-

lichen Sternen zwischen —3° und -+13° Deelination ausgeführt, so dass die südliche Zenith-
7
4

 
