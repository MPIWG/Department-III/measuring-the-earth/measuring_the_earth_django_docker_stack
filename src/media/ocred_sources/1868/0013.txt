[YIN ii

en in |

ei m AR

sr ae

| Qt = Ry % GR On+ı
One nn Oa 0,
(4.) a AR Nea
la =n.0.-0,
| Oh 0,
Die letzte dieser Gleichungen giebt eine scharfe Controle für die richtige Berechnung sämmt-
licher Coefficienten Z und Q, indem man stets 0, = Lu; erhalten soll. |
Das Gewicht P für den gefundenen Werth des Winkels giebt die Gleichung :

6) P=355- 1[a]0.—Tag].

Bei der wirklichen Ausführung der Rechnungen braucht man gar nicht die Gleichungen (3.) und
(4.) niederzuschreiben. Placirt man zuerst die n Constanten i in einer horizontalen Linie, dann
können sämmtliche Coeffieienten Z und Q in zwei darunter angebrachten horizontalen Linien sogleich
hingeschrieben werden, indem man in der ersten von links nach rechts, in der zweiten aber von
rechts nach links rechnet, und dabei überall den eben gefundenen Coefficienten mit dem darüber
stehenden Werth von k multiplicirt und von dem Producte den nächst vorhergehenden Coef-
ficienten abzieht. Die schematische Form der ganzen Rechnung wird dann die folgende:

 

k | DIR ES A
WE a,
Q Q, 0, Q, 0, ne 1 7 Er Laws .

 

Um mit kleineren Zahlen zu rechnen wird man nieht durch unmittelbare Anwendung der
Formel (1.) den ganzen Winkel X direct ‚bestimmen, sondern es stets vorziehen, von einem
genäherten Werthe X, ausgehend und indem man X = X,+ @ setzt, nur die Correction x zu
berechnen. Wenn man vorläufig alle Theilungen der Reihe ignorirt, wird der mittelst einer
Anzahl von [a] Wiederholungen gemessene Winkel durch den Bogen [M] bestimmt. Von
dem entsprechenden und sehr angenäherten Werthe:
| Ri m

ausgehend, und indem man:

M,—a,X, = m,,> M,—a,X, =m,, Moo, X, =m, 4 Ma, X =",
setzt, giebt dann die Formel (1.) zur Bestimmung der Correction:
| sm Job, >

[a10,— [40]

Die Anwendung der entwickelten Formeln werde ich noch durch die Berechnung des

folgenden, mittelst einer Reihe von 12 Wiederholungen gemessenen Winkels näher erläutern:

(i.) i=

 

 

Anz. der Wiederb. | Ablesungen des Kreises.
0 OE ror
2 GE"; 74 44/66
6 183° 211 38",7
8

‚2449. 98! 55,9
| 366° 43! 21", 6.

ei
bo

 

 
