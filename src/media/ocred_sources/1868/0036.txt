 

tn

und ist vor einigen Jahren abgebrochen. Nahezu an derselben Stelle ist der jetzige Thurm
wieder aufgebauet. :

Das dem Anscheine nach sehr solide Mauerwerk dieses Thurms hat eine Höhe von
ungefähr 80 Fuss. Etwa 20 Fuss höher werde ich an dem Gebälk der auf dem Mauerwerk
ruhenden Pyramide eine solide Unterlage für das Stativ des Instruments und an einem andern
Theile des Gebälks einen Fussboden für den Beobachter befestigen lassen. Von dem Punkte
des Stativs, der sieh unter der Mitte des Instruments befinden wird, kann man ein Loth bis
zum Fussboden des Thurms herablassen. Wird demnach in diesem Fussboden ein Granit-
block mit eingegossener Marke festgelegt, so lässt sich jederzeit die Stellung des Instruments
sowohl, als eines im Thurme aufgestellten Heliotrops auf die Marke beziehen, so dass die
Lage des Thurmknopfs durchaus nicht in Betracht kommt. Auf solche Weise wird man den
Thurm mit Sicherheit als Gradmessungs-Station benutzen können. In Lauenburg ist das
Sehumacher’sche Stations- Centrum jetzt auch nicht mehr vorhanden. An der Stelle dieses
Dreieckspunkts wurde, ein Paar Fuss unter der Erdoberfläche, nur noch ein viereckter eichener
Pfahl vorgefunden, dessen Kopf ganz verfault war und keine Marke enthielt. Letztere ist
wahrscheinlich beim Wegräumen von Paschen’s Gerüst verloren gegangen. Herr Geheime-
Rath Paschen hat nämlich im Jahre 1854 die Marke noch vorgefunden und nahe bei derselben
drei mit Bohrlöchern und Bleimarken versehene Granitsteine circa 2 Fuss tief in die Erde ver-
senkt, und die Lage der ersten Marke gegen die letztern durch gemessene Distanzen und
Richtungen scharf bestimmt. Nach diesen Angaben liess ich das Schumacher’sche Stations-
Centrum wieder herstellen.

Granitpfeiler habe ich bereits in Lauenburg, Varendorf, Niendorf, Rahlstedt und
Hohenhorst aufstellen lassen. An jedem dieser Punkte geschah die Aufstellung in folgender
Weise: Auf einem Fundament von Ziegelsteinen wurde eine Steinplatte, deren obere Fläche
nahezu zwei Fuss unter der Erdoberfläche sich befindet, mit Cement befestigt. In der Mitte
dieser Platte wurde ein messingener Cylinder von 18 bis 24 Linien Höhe und 10 bis 12 Linien
Durchmesser mit Blei so eingegossen, dass seine obere kreisförmige Fläche mit der obern
Fläche der Steinplatte nahezu in einer Ebene ist. In der Mitte der obern Fläche des Cylinders
befindet sich ein Loch von 10 Millimeter Tiefe und einem Millimeter im Durchmesser, in dessen
lothrecht gerichteter Längenachse das Stations-Centrum liegt. Auf diese Steinplatte wurde der
Granitpfeiler gestellt, und in dessen obere Fläche ein zweiter Messingseylinder von derselben
Beschaffenheit, wie der im Fundament befindliche so versenkt, dass seine durch ein Loch be-
zeichnete Längenachse mit derjenigen des untern Cylinders in derselben Vertieallinie liegt.
Neben jedem der Pfeiler von Varendorf, Niendorf, Rahlstedt und Hohenhorst, habe ich noch
zwei mit Messingwerken versehene Pfeiler von Ziegelsteinen aufmauern lassen, deren obere
Fläche etwa 2 Fuss unter der Erdoberfläche liegt. Die Lage der Marken dieser Nebenpteiler
gegen die zugehörigen Stations-Centra ist bei der Vornahme der Winkelmessungen noch zu

bestimmen.
Auf den Stationen Lauenburg, Varendorf und Hohenhorst, fallen die jetzigen Stations-

3

ar vorm

 

8 ame hab a Had ahd ih sao

 
