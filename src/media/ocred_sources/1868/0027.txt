ee oe

ern in |

mie bu RP T

mu eT

Zusammenstellung der berechneten Entfernungen der Dreieckspunkte von der Seite

Jever- Langwarden bis zur Seite Helgoland- Wangeroge. (Taf. II.)

Station Dangast.

1 m Log. Entf, in Toisen

Jever, Schlossthurm 0 0 40,130  4,0125278.6
Langwarden, Kirchth. 82 46 17,633 4,0295711.6
Varel, Kirchth. 221 6) 90,535 3,4871532 .8

Station Varel, Kirchthurm.

0.0.0029 4,1060767 .0
32.0. 22,390 3,4871532.8

Jever, Schlossth.
Dangast, T.P.

Station Langwarden, Kirchthurm.
0.0. 40,568. 4,0295711.6

47 20 20,682  4,1425542.0
125 10: 35,742 . 3,8180015.4

Dangast, T.P.
Jever, Schlossth.
Bremer Leuchtth.

Station Jever, Schlossthurm.

Wangeroge, Leuchtth. 0: 0° —- 0,303 4,0924679.6
Bremer Leuchtth. 50 41. 55,179 4,1478643.9
Langwarden, Kirehth. 82 54 59,048 4,1425542.0
Dangast, T.P. 132 48 22,527 4,0125278.6
Varel, Kirchth. 11 52 3005 4,1060767.0

Station Bremer Leuehtthurm.

Wangeroge, Kirchth. 0 O +0,030

Wangeroge, Leuchtth. 1 56 40,465 4,0946622 .0
Neuwerk, Leuchtth. t07 32 55.009 4,1581340.5
Langwarden, Kirchth. 231 43 22,649 3,8180015.4

Jever, Schlossth. 306 40 2,528 4,1478643.9

Station Wangeroge, Leuchtthurm.

0 0 0,058
71 28 21,862
111 50 56,872
128 36 6,016
180 52 26,774

Helgoland T.P.
Neuwerk, Leuchtth.
Bremer Leuchtth.
Langwarden, Kirchth.
Jever, Schlossth.

4,3304170.0
4,0946622 .0
4,2410898.6
4,0924679 .6

4,3496946.3 —

4*

IE NN
10292,666
10704,618

3070,105

12766,642
3070,105

10704,618
13885,266
6576,602

12372,799
14056,085
13885,266
10292,666
12766,642

12435,470
14392,427

6576,602
14056,085

2237 1,476
21400,159
12435,470
17421,673
12372,799

 
