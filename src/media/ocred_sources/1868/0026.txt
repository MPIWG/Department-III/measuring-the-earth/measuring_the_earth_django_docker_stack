 

a

servator an der Königl. Sternwarte in Berlin, ausgeführt worden und zwar von letzterem die
auf den Stationen Dangast, Varel, Jever und Langwarden.

Die Winkelmessungen in Dangast wurden mit dem gebrochenen 13zölligen Universal-
instrumente von Pistor und Martins ausgeführt, welches Herr General Dr. Baeyer mehrfach an-
gewandt und in seinem Werke „Verbindungen der Russischen und Preussischen Dreiecks-
ketten ete.“ auf Seite 69 näher beschrieben hat.

Die 13zölligen Kreise dieses Instrumentes haben je zwei diametral gegenüberstehende
Microscope, welche die Ablesung bis auf eine Secunde gestatten. Die übrigen Winkelmessungen
in Varel, Jever und Langwarden sind mit einem 8zölligen, excentrischen Universalinstrumente
angestellt, welches von denselben Künstlern verfertigt ist und eine ähnliche Microscop-Einrichtung
hat, nur dass die direete Ablesung hier zwei Secunden giebt.

Ueber die Station Dangast ist das Nähere bereits in dem Generalbericht für 1866 mit-
getheilt. Bei den Beobachtungen daselbst wurde kein Heliotropenlicht angewandt, sondern es
wurden die Thurmspitzen von Varel, Jever und Langwarden direct eingestellt.

Der Stand des Instruments in Varel war in einem Schallloche auf der sehr dieken
und soliden Mauer des lutherischen Thurmes. Wegen der sehr ungünstigen Witterungsverhält-
nisse, des häufigen Nebels an der Seeküste und des oft sehr dieken, für Heliotropenlicht un-
durchdringlichen Moorrauches, konnte Langwarden nie gesehen werden, es wurden daher nur die
Riehtungen nach Jever und Dangast, meist unter Anwendung von Heliotropenlicht, eingestellt.

In Jever wurden die Beobachtungen auf dem Schlossthurm angestellt. Derselbe hat
bis gegen 60 Fuss Höhe, eine sehr dicke Mauer aus Backsteinen, im übrigen Theile besteht
er aber aus Holz. Aus verschiedenen Gründen konnte im Jahre 1866 das Instrument nicht
auf dem Mauerwerke aufgestellt werden, es wurde deshalb in einer grösseren Höhe von dem
scheinbar soliden hölzernen Oberbau aus gemessen. Das Holzgefüge zeigte sich jedoch so
schwankend, dass im nächsten Jahre eine Wiederholung der Beobachtungen erwünscht war.
Diese wurden dann von einem auf der Mauer errichteten steinernen Pfeiler aus angestellt.

Zu Langwarden wurde oben in dem sehr engen steinernen Thurm beobachtet, in dessen
Mitte ein steinerner Pfeiler errichtet war. Wegen des fortwährenden Seenebels konnte Wangeroge
von hier aus fast nie gesehen werden, ebenso liess sich Varel nur einige Male beobachten.

Zwischen den auf diese Weise ausgeführten Messungen und den von Gauss ange-
stellten zeigen sich nicht unerhebliche Unterschiede. Erwägt man, dass Gauss seine Beob-
achtungen unter viel günstigeren Umständen und mit einem grösseren Instrumente ausgeführt
hat, so dürfte es gerechtfertigt erscheinen, dass dessen Resultate als Grundlage der Aus-
gleichungen angenommen wurden. Besonders entscheidend trat dabei auch der Umstand auf,
dass in dem Dreiecksnetz, welches zur Verbindung von Dangast und Helgoland dient, mehrere
trigonometrische Punkte seit der Messung von Gauss eine Veränderung erlitten haben können,
indem namentlich der Thurm zu Langwarden umgebaut worden ist und sehr wahrscheinlich
die Spitzen der Kirchthürme zu Wangeroge und Bremerlehe eine etwas veränderte Lage an-
genommen haben.

 

|
j

|
i
|

 
