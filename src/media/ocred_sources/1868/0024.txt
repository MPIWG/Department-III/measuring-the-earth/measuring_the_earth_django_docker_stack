 

a,

weisen mir erlaubte. Auch alle übrigen, in dem Zeitraum vom 1. bis 11. September 1868
angestellten Beobachtungen und besonders diejenigen, welche unmittelbar vor und nach der
Umlegung stattgefunden haben, zeigen, dass das Instrument äusserst solide ist und von einer
Umlegung nichts zu leiden hat. Hatte Herr Kam Ursache, diese Umlegung für eine beträcht-
liche Fehlerquelle zu halten, so ist es desto sonderbarer, dass er, anstatt jeden Tag einen
Blick auf seine Beobachtungen zu werfen, dieselben erst drei Monate nachher angesehen hat.

Herr Dr. Kampf hat sieh erstrebt, aus den Tagebüchern und den mündlichen Mitthei-
lungen des Herrn Kam, sowie aus dem Instrumente selbst die Quelle der Fehler abzuleiten,
doch es ist ihm ebensowenig als mir gelungen.

Da die Beobachtungen des Herrn Kam mit allen gleichzeitig angestellten Beobachtungen
am Meridiankreise unvereinbar sind, mussten sie selbst die Data ihrer Reduction liefern. Der
Collimationsfehler wurde dazu jedesmal aus den, an zwei unmittelbar auf einander folgenden
Tagen angestellten Beobachtungen des Polaris abgeleitet und das Azimuth an jedem Abend
aus der Beobachtung des Polaris und der beiden Fundamentalsterne. Beide, Collimationsfehler
und Azimuth, kamen weit grösser und schwankender heraus, als die übrigen Beobachtungen
dieselben gaben. Ich schätze die Unsicherheit der Zeitbestimmung des Herrn Kam auf etwa
0°,3 und falls man noch etwas aus der Längenbestimmung zwischen Brüssel und Leiden
machen will, so fragt es sich, ob es nicht besser wäre, anstatt der verunglückten Zeitbestimmung
des Herın Kam die Zeitbestimmung des Herrn van Hennekeler anzuwenden; jedoch beruht
dieselbe auf den Beobachtungen von nur ein Paar, in Brüssel nicht beobachteter Sterne und
ist zwölf Stunden nach der Uhrvergleichung angestellt.

Wäre die Längenbestimmung zwischen Brüssel und Leiden eine Sache, welche nur
mich persönlich anginge, so würde ich alle darauf verwandte Mühe und Kosten als verloren
betrachten, über dieselbe gänzlich das Stillschweigen bewahren und mich beeifern, dieselbe,
sobald wie möglich, unter einer besseren Mitwirkung zu wiederholen. Diese Untersuchung ist
aber eine öffentliche Angelegenheit, welche schon öffentlich bekannt geworden ist, wozu die
Regierungen von Belgien und den Niederlanden mit der grössten Bereitwilligkeit die erforder-
liche Hülfe gewährt haben und wofür Herr Director A. Quetelet sich eine Unterbrechung der
Arbeiten an seiner Sternwarte hat gefallen lassen. Nachdem die Arbeiten an der Leidener
Sternwarte, welehe ich, während zwei Observatoren dabei angestellt waren, doch nicht alle
selbst ausführen konnte, mir seit mehreren Jahren soviel Verdruss verursacht haben, war es
mir unbeschreiblich peinlich, öffentlich gestehen zu müssen, dass auch die Längenbestimmung
zwischen Brüssel und Leiden verfehlt ist. Ich hätte vielleicht die wissenschaftliche Welt be-
trügen und die in Leiden angestellten Zeitbestimmungen so einkleiden können, dass deren
Unriehtigkeit sich vorläufig nicht entdecken liesse; und obschon Einige behaupten werden, dass
‚dies meine Pflicht wäre und ich mich dadurch von sehr grosser Unannehmlichkeit hätte be-
freien können, habe ich doch dieser Betrügerei durchaus mich nicht hingeben wollen und so

blieb mir nichts Anderes übrig, als die Sache bekannt zu machen, so wie sie beschaffen ist.
Leiden, 10. März 1809. F. Kaiser.

 

ju ds home LA dab, dh äh tua mn sn

{
i
i
|

 
