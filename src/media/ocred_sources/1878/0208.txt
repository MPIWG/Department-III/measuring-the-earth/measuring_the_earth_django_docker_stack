 

be
i
a
i
Ha
IE

ee

RER EEE TERN TEL ETERNEEE

Sn

SEES

en

ESTER

ESS

ee

RETTET EERE

ERSTE

ES TE ET

N

 

DEUXIÈME SÉANCE.

Genève, mercredi, 17 septembre 1879.

La séance commence à 10 heures 30 minutes sous la Présidence de M. le

General Ibanez.

Sont presents, de la Commission permanente:

MMrs. von Bauernfeind, Faye, von Forsch, Mayo, von Oppolzer.

Des délégués: MMrs. Adan, Ferrero, Peters, Plantamour, Respighi, Villarceau.

Des invités: MMrs. Poutillier de Beaumont, Professeur Cellérier, Professeur Chaz,
David, Colonel Gauthier, Sainte-Olaire Deville, Maggia, Maunoir, Raoul Pictet, Rümker,
Professeur Lowis Soret, Charles Soret, L. Tessier, Professeur Thury, Veynassat, Ingénieur.

MMrs. Bruhns et Hirsch fonctionnent comme secrétaires. '

Le procés-verbal de la précédente séance est lu en francais par M. Hirsch et
adopté. M. Hirsch explique que le temps ayant manqué pour faire aussi la rédaction
en allemand, les comptes-rendus publieront cependant comme d'habitude le procès-verbal
dans les deux langues.

Le Président donne la parole à M. Faye, délégué de la France.

M. Faye regrette que son collègue, M. je Commandant Perrier soit empêché
de rendre compte lui-même des travaux qu'il à dirigés, attendu qu'il est occupé dans
ce moment même en Algérie au grand travail de jonction entre les deux continents.
Toutefois M. Perrier lui a remis la rédaction de son rapport qu’il à l'honneur de pré-
senter à l’assemblée.*)

M. Faye ajoute à ce rapport d’interessantes explications de detail sur l’organi-
sation des travaux géodésiques en Algérie.

M. le Président remercie M. Faye et félicite la France d’avoir décidé l'exécution
d’un nouveau nivellement de précision; il fait ressortir les avantages qui résulteront
pour l'Espagne de pouvoir ainsi se rattacher au réseau européen par les lignes françaises
qui se joindront sans doute aux repères que l'Espagne a placés dans ce but à la
frontière.

*) Voir au Rapport général pour 1879, France.

 

4
j
!
j

 
