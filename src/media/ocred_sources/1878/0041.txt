Te en ut

PSP TP |

NL RU

rent R

I RET 7 BRET! 0177

PREMIERE SEANCE
de la Commission permanente

tenue à Hamroure le mercredi 4 septembre 1878.

Sont presents:

Monsieur le bourgmestre Dr. Körchenpauer et Monsieur le sénateur Hayn,
representants du Senat de Hambourg.

Les membres de la Commission permanente:

MM. Baeyer, de Bauernfeind, Bruhns, de Forsch, Hirsch, I banez , Mayo
et d’Oppolzer.

MM. les délégués: Adan, Borraquer, Ferrero, Perrier, Peters, Sadebeck, Villarceau. :

M. Hilgard, représentant du Coast and geodetic Survey américain.

Invites: MM. le directeur Riimker, Friedrichsen et le Dr. Fels.

La séance est ouverte & 2 heures 20 minutes.

Présidence de M. Ibanez; MM. Bruhns et Hirsch fonctionnent comme secrétaires.

M. Ibanez ouvre la séance et Monsieur le bourgmestre Kirchenpauer prend
la parole pour souhaiter la bienvenue & la Commission géodésique. Il s’exprime
dans les termes suivants:

Messieurs !

Le Sénat de cette ville, qui est en même temps le gouvernement de l'Etat
de Hambourg, m'a confié la mission honorable de souhaiter en son nom la
bienvenue à la Commission permanente de l'Association géodésique inter-
nationale. Nous remercions le Bureau et les membres de l'Association
d'avoir bien voulu accepter notre invitation; nous sommes très-sensibles à
l'honneur que la Commission permanente fait à notre ville en se réunissant
chez nous; car nous apprécions parfaitement la portée de la grande entre-
prise qui réunit les gouvernements du continent pour une oeuvre scientifique
commune, et nous connaissons le mérite des savants auxquels l'exécution
de cette oeuvre est confiée.

S'il est naturel que notre petite république ne soit pas appelée à participer
directement à la mesure des degrés en Europe, il a été toutefois donné à

5

 
