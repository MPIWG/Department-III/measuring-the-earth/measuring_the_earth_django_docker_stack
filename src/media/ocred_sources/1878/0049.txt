MRC

BA ge |

BA AA |

PONT TRE CNT

NTI TANT

Dee Dee eme

Voici le résultat pour la longueur de la base de Braake:

 

 

 

 

 

 

HO A RD
1. D'après la mesure de Schumacher, révisée
par Peters ét Andrae. 07 3014:451 0
Nr . . m 1
2. Déduite de la base de Berlin . . . . . . 3014.457 503000
21 ou D
3. Déduite de la base de Kônigsberg. . . . 3014.460 538000

 

Cet accord est tellement satisfaisant, que l'Institut géodésique n'hésite pas de
conserver pour la. mesure des degrés la valeur de Schumacher, savoir 3014°45115;
log. 3,4792083.

D. Publications.

1. Comptes-Rendus de la cinquième conférence générale de U Association géodésique
internationale pour la mesure des degrés en Europe, réunie à Stuttgart du 27 septembre
au 2 octobre 1877, rédigés par les secrétaires C. Bruhns et A. Hirsch, publiés pour
servir de rapport général pour l’année 1877, par le Bureau Central de l’Association
géodésique internationale.

2. La figure terrestre, mémoire destiné à la mesure des degrés en Europe, par
M. le Dr. Henri Bruns. Berlin 1878.

3. Le Réseau de triangles des provinces Rhénanes, deuxième fascicule, Les ob-
servations de directions.

A, Travaux astronomico-géodésiques, exécutés dans l’année 1877. Berlin 1878.

5. Nivellement de précision de lElbe, exécuté par Wilhelm Seibt sur la demande
des administrations de Prusse, Mecklenbourg et Anhalt, préposées aux travaux de cor-
rection de l’Elbe. Berlin 1878.

 
