| ve re er

 

a m

m la

lie!

Fr PATER ii

87

Rs

 

 

 

 

 

 

 

 

 

 

 

 

 

Bi À = 60 an À — 450
glieder. | Max.-Corr. Entspr. Azim. Max.-Corr. Entspr. Azim,
X da 35 cos’2 — 1/5, 13 2] cz U.

AU’ da 63 sin?2 = 1, 24 sin?e = ].

A” da 52 sin?®?® — ], m sin?z =].

B da 59 co? — 1. 86 cos? UT,

B' da 3 sin? 2 = 0,2592. 8 sin?z = 0,4999.
B" da 33 cos 2— — 0,8153. 109 COs2— + 0,7238,
© di | 30 eos 2 == Il, 32 COS 22 — ],

Sr daR 18 sin2?z = 1]. 15 sin?z —=].

@” dr 15 sin?2 = ], 11 sin?z — ],

D de | 7 sin?z = 1. 11 sin2e 1.

D' de 18 cos22 — 0,5946, 18 cos2z = ].

DS" dz 10 cos2z2 = |, al COS 22 — 1,

 

Da die wirklich vorkommenden Werthe der Differentiale da, da, ax und dz wohl
öfters viel kleiner als die hier angewendeten sind, da die Maximalwerthe bei verschie-
denen Azimuthen eintreten, und da auch die mit verschiedenen Vorzeichen behafteten
Hauptglieder sich gegenseitig mehr oder minder aufheben, kann man gewiss behaupten,
dass sämmtliche Totalcorrectionen bei der hier behandelten Aufgabe vollständig ignorirt
werden können.

Andrae.

Frankreich.

Rapport sur les travaux exécutés pendant l’année 1879, par M. F. Perrier.
Les travaux exécutés en France, pendant l’année 1879, comprennent, dans l’ordre
chronologique:

a) La reconnaissance des sommets de la chaîne géodésique qui doit prolonger
le parallèle algérien à travers la Tunisie, jusqu'aux rivages qui font face à la
Sicile et à Maite.

b) La continuation des travaux relatifs à la nouvelle méridienne de France.

c) La préparation et l’exécution, sur le terrain, des opérations destinées à relier
l'Algérie avec l'Espagne, par dessus la Méditerranée.

d) Les études préparatoires ayant pour objet la recherche des meilleures méthodes
à adopter pour le nouveau nivellement général de la France.

 
