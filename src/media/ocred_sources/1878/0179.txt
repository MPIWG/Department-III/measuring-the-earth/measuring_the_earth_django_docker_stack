Me

“(RPP PAP YP rer

til

ma RT Im oo

u

Lu,

Par IPN eae

SUR LA CONSTRUCTION DE LA REGLE GEODESIQUE INTERNATIONALE. 4g

nation précise, directe et indépendante de l'emploi de tout instrument
ayant une valeur individuelle. Rien ne peut donc être considéré, en mi-
crométrie, comme évident.

Cependant nous admettrons comme axiome que la température de la
glace est invariable avec le temps. D’après Les travaux de sir William
Thomson, il n’en pourrait être autrement que si la pression atmosphé-
rique ou l'accélération due à la pesanteur changeait de telle façon que
les conditions de la vie humaine cessassent d’exister sur la terre. Nous
avons prouvé, par un procédé dont l'exactitude dépasse tous ceux qui
ont été employés jusqu'ici, que la température de la glace fondante
produite par la glace longtemps conservée dans une glacière est rigou-
reusement invariable.

Nous admettrons comme axiome que la densité del’eau est invariable
avec le temps, et cela pour les mémes raisons et sous les mémes condi-
tions que la fusion de la glace.

Nous admettrons que la densité du mercure à zéro ne varie pas avec
le temps, et que ce métal peut servir indéfiniment à mesurer les pres-
sions des gaz qui servent de matière thermométrique. En employant
le mercure seulement pour constater l'identité des pressions de deux
gaz dont l’un est confiné dans un espace invariable, nous nous affran-
chissons des mesures du barometre, et, par suite, des corrections qu il
faut apporter à la mesure des hauteurs pour annuler l'influence de la
température, de la latitude et des variations de la pesanteur.

Nous admettrons qu’une masse de platine iridié ne perd aucune
partie de sa substance par volatilisation à la température ordinaire, À
la rigueur, ce principe, qui peut paraître évident, devrait être démon-
tré; mais il est clair qu'aucun procédé de mesure ne pourrait aujour-
d’hui être appliqué à la détermination de la tension de vapeur du
platine iridié au-dessous de 100 degrés. Cependant nous croyons utile,
mais aussi pour d’autres raisons, de renfermer dans une enveloppe de
verre vide d’air et fermé à la lampe la masse de platine iridié qui doit
servir de témoin pour constater la variabilité ou la permanence des pro-
priétés physiques de la règle géodésique.

Enfin nous admettrons que la longueur d'onde du rayon rouge de la
lithine ou vert du thallium est invariable avec le temps, de sorte qu’au
moyen du millimètre dont la longueur aura été exprimée par le nombre

 
