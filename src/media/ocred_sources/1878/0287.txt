Een |

COUT 7

LH On

Want

a N gi

Tp) | aaa

95

zu astronomischen Beobachtungen bestimmter Ort in unserm Lande b
sich die Vermessungsarbeiten auf Nivellements.

Zunächst wurde in der Provinz Rheinhessen ein Controll-

efindet, so beschränken

& —— —>—— Divellement in der Richtung von Mainz nach Armsheim auf dem
\ ve Eisenbahndamme ausgeführt. Diese Linie bildet die Diagonale
\ : / | eines Vierecks, dessen Seiten: Mainz — Gaulsheim (bei Bingen),
4\ De >| Mainz — Worms, Worms — Armsheim und Armsheim — Gaulsheim
\ a “| schon in früheren Jahren nivellirt wurden. Wir wollen die Aus-
na | gleichung der Messungsresultate vornehmen, wobei wir den mitt-
a | leren Fehler des Nivellements der Quadratwurzel der Länge der

| nivellirten Strecke proportional setzen, indem die Zielweiten con-

 

stant, oder doch beinahe constant waren.
die Pfeile die Richtung des Steigens an.

Die unmittelbaren Nivellirungsresultate sind:

In der Figur zeigen

Entfernung Gewicht

MG =h; = 3,19392 m 29 m Pi = 0.036
MW = ha - 121050, 1, = 442 Di == 0,023
WA = hs = A2 65101 5 80 ps = 0,026
AG = hs = 585023 , 1, = 226 | Da — 0014
MA =hs = 54,74663 , ls — 358 ,, Ps = 0,028.

Nach der Methode der bedingten Beobachtungen erhält man hier 2 Bedingungs-
gleichungen, welche zu den folgenden ausgeglichenen Werthen der Höhendifferenzen
führen:

hı = 3,80400 m
ho = 1100
hs = 42,65000 „,
he — 58,9531 |
hs = 5415411...

Der mittlere Fehler einer Nivellirungsstrecke von 1 km ergiebt sich daraus
+ 1,3 mm.

Bei dem Nivelliren selbst wurde in folgender Weise verfahren:

Das Nivellirinstrument wurde stets in der Mitte zwischen den beiden Punkten
aufgestellt, die Libelle zum scharfen Einspielen gebracht und die Latten abgelesen. So-
dann wurde eine zweite Beobachtung ausgeführt, nachdem man das Fernrohr eine halbe
Umdrehung in seinen Lagern hatte machen lassen. Darauf wurden die beiden Nivellir-
latten mit einander vertauscht, und noch einmal 4 Ablesungen in derselben Weise wie
vorher gemacht. -

Ein anderes Nivellement wurde in der Provinz Starkenburg von Biblis nach
Lampertheim und Mannheim auf der neuen Bahnlinie ausgeführt, konnte indess, nament-
lich in Folge ungünstiger Witterung nicht zum Abschluss gebracht werden.

Darmstadt, den 12. April 1880.

zu

Dr. Nell,

Professor der Geodäsie an der Technischen Hochschule.

 
