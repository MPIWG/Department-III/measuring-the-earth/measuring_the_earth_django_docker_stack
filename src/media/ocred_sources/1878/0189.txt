He

“lth TFA Te |

man HU

man at

Till

ra ee LU

Wp?

INHALT — TABLE DES MATIERES,

Procès-verbaux des séances de la Commission permanente tenues
à Genève du 16 à 20 Septembre 1879.

Liste des membres de la Commission per-
mañente, des délégués et des invités .
Programme de la session

SERRE Te ele) Pire:

Première séance.

Ouverture de la séance. Discours de M.
Carteret, Président du Conseil d'Etat
de Geneve, *

Discours de M. le Général Ibañez . . ..

Dépouillement de la correspondance

Rapport de la Commission permanente pour
1879 0:2... 00000200025

Rapport du Bureau central et de l'Institut
geodesique Prusien 2.

Rapport de M. v. Bauernfeind sur les tra-
vaux en bayame 0...

Discussion sur l'influence des attractions
locales sur les nivellements . .....

Rapport de M. Adan sur les travaux Belges

Communication de M. Sainte-Claire Deville
sur le coefficient de dilatation de la
règle géodésique 2

Deuxième séance.

Rapport de M. Faye sur les travaux en
Frances ci 000. 0.0
Discussion sur le nouveau nivellement de
précision projeté en France ..,...

page

14
14

15

16

17

 

Discussion à l’occasion d'une communica-
tion de M. Villarceau sur le roulement
des couteaux de pendule à reversion .

Rapport de M. Mayo sur les travaux en

Tate ....2.2.0 000020 2 0.2
Rapport de M. v. Oppolzer sur les travaux
en Autsiche....
Rapport de M. y. Forsch sur les travaux
en Russie) 0. 2.

Troisième séance.

Discussion sur le pendule double
Proposition de M. Hirsch adoptée; MM.
Plantamour et Cellérier designés comme
rapporteurs pour la Conférence générale
Rapport de M. Hirsch sur les travaux en
SUISSE. 00.02 ne
Communication de M. Plantamour sur les
projets de mesures de base en Suisse
Rapport du Général Ibanez sur les travaux
en Espagne
Communication du même sur les travaux
actuellement poursuivis pour la jonc-
tion de l'Espagne et de l'Algérie . ..
Note de M. Villarceau sur la définition des
Altitudes. un
Communication de M. Respighi sur son ca-
talogue de déclinaisons 2 2...
Rapport de la Commission spéciale sur les
mémoires théoriques de M. Adan, pre-
senté par M. Peters . .... se

page

1%

18

18

18

19et20

20

21

21

21

22—24

24

 
