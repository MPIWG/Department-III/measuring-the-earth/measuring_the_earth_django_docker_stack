 

h
IK
f
N
Ky
R
iy
i

5
ff
N
ii
i
i
:
i
Mi
ti

 

48

Zweite Sitzung

der permanenten Commission.

Verhandelt Genf, Mittwoch den 17. September 1879.

Anfang der Sitzung 10 Uhr 30 Minuten.
Anwesend: von der permanenten Commission die Herren: von Bauernfeind,

Bruhns, Faye, von Forsch, Hirsch, Ibanez, Mayo, von Oppolzer; von den Bevollmächtigten

die Herren: Adan, Ferrero, Peters, Plantamour, Respighi, Villarceau; als Eingeladene
die Herren: Boutillier de Beaumont, Ceilérier, Chaiz, David, Sainte-Claire Deville, Emil
Gauthier, Maggia, Mounoir, Raoul Pictet, Riimker, L. Soret, Ch. Soret, Tessier, Thury,
Veynassat.

Prasident: Herr General Ibanez.

Secretire: die Herren Bruhns und Hirsch.

Das Protokoll der vorigen Sitzung wird in französischer Sprache von Herrn
Hirsch verlesen und angenommen. Herr Hirsch bemerkt, dass die Zeit zur Ueber-
setzung ins Deutsche gefehlt habe, doch würden wie bisher die Protokolle in beiden
Sprachen veröffentlicht werden. |

Der Präsident giebt Herrn Faye als Bevollmächtigtem für Frankreich das Wort:

Herr Faye beklagt, dass sein College, der Herr Commandant Perrier, verhindert
sei, selbst über die von ihm geleiteten Arbeiten Bericht zu erstatten, weil er gegenwärtig
sich in Algier befinde, um die Verbindung zwischen Europa und Afrika durch ein Drei-
ecksnetz und Längendifferenzen herzustellen. Herr Perrier habe ihm aber seinen Bericht”)
übersandt, welchen er sich beehre der Versammlung vorzulegen. Herr Faye giebt zu
diesem Berichte interessante Erklärungen über die Organisation und Ausführung der geo-
dätischen Arbeiten in Algier.

*) Siehe General-Bericht für 1879, Frankreich.

srl dach a

 

 
