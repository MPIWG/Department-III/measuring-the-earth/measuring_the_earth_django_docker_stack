 

B
|

SERIE

Se

i F
a
i

 

28

Jusqu’à quel point ces travaux ont-ils été complétés depuis la 5° conférence ?

Quelles lacunes sont encore à signaler?

Quelles sont les déterminations de longitudes qui n’ont pas encore été
publiées ?

Quelles sont les longitudes qui peuvent donner lieu à des équations ?

Peut-on déjà calculer, en se basant sur le sphéroïde de Bessel, des lignes
géodésiques comprises entre des points isolés, dans le but de déterminer les
différences qui existent dans les coordonnées astronomiques et les coordonnées
géodésiques ?

Tl est à désirer qu'une nouvelle comparaison soit établie entre toutes
les déterminations astronomiques de coordonnées publiées jusqu’à ce jour,
ayant rapport à la mésure européenne du méridien. (Voir conférence générale
de 1874, Dresde.)

IV. De la réfraction.
M. von Bauernfeind présentera un travail sur ce sujet.
V. Des triangulations.
Rapporteur M. le Colonel Ferrero.
VI. De la compensation des réseaux trigonométriques.
De l'état actuel des travaux concernant la compensation des réseaux

dans les différents états.
De la jonction des réseaux des différents états entre eux.

Rapporteur M. Peters.

VII. Des appareils pour la meswre des bases et de la remesuration des bases.

Les désirata qui ont été formulés 4 la 5™° conférence sont-ils déja en
partie remplis ?

Signale-t-on encore des lacunes dans le réseau des bases d'Europe?

Rapporteur M. Perrier.

VII. Des nivellements de précision.

Jusqu'à quel point ont-ils progressé ?

Est-il à souhaiter qu'après un certain nombre d'années écoulées, on
reprenne le nivellement de certaines lignes de premier ordre afin de pouvoir
déterminer des soulèvements ou des abaissements relatifs dans le cas où il
s’en produirait?

Rapporteur M. Hirsch.

IX. Des maréographes.
Quels sont parmi les maréographes proposés ceux qui sont plus spéciale-

ment à recommander ?
Quels sont les résultats donnés par les maréographes en activité ?

Rapporteur M. Ibanez.
X. De la determination de l'intensité de la pesanteur.
Des meilleurs moyens à employer pour corriger et utiliser les obser-
vations du pendule à reversion qui ont été faites sans qu’on ait tenu compte

de Voscillation des trépieds et des piliers?

 

 

i ill ld all dh aa

all LL)
