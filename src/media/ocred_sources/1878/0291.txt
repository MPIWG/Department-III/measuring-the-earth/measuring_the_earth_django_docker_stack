MRC

 

Ian AR

il

D PO DR a

99

Polygonskette, welche die Triangulirung von Steyermark und Kärnten mit jener von Tirol
verbinden soll, wurden die Beobachtungen (in Folge anderweitiger dringender Arbeiten
jedoch blos mit einem Instrumente) fortgesetzt. In der kurzen Zeit, während welcher
der Aufenthalt und die Ausführung von Messungen auf diesen Hochpunkten überhaupt
möglich ist, wurden die Richtungsbeobachtungen auf Gerlitzen, Eisenhut, Staffberg und
Thorkofl ausgeführt und nahm überdies der Beobachter persönlich eine Recognos-
cirung des Ankogl (3253%) und Grossglockner (37972) vor, um zu constatiren, ob sich
in der Nähe dieser Firnspitzen ein — den bescheidenen Anforderungen unserer Triangu-
lirungs-Offiziere entsprechender — Bivouacplatz werde finden lassen, von welchem aus
der trigonometrische Punkt in etwa 1 bis 2 Stunden zu erreichen wäre. *

Das Ergebniss dieser Recognoscirung war, dass auf der Südseite des Ankogl,
unmittelbar am Fusse des Gletschers, die Möglichkeit geboten ist ein Zelt aufzuschlagen,
und zwar an einer Stelle, welche im August und Anfangs September schneefrei sein

dürfte und von der aus man auf den trigonometrischen Punkt in etwa 1'/, bis 2 Stunden
gelangen kann.

Etwas günstiger als am Ankogl werden sich die Unterkunfts-Verhältnisse für den
Beobachter am Grossglockner gestalten, wenn mittlerweile die Schutzhütte, welche der
Alpenclub „Oesterreich“ auf der Adlersruhe (3463%) erbauen lässt, fertig sein wird.
Dagegen ist der Aufstieg von da über den Kleinglockner und die Scharte etwas
beschwerlich und überdies ein längerer Aufenthalt auf dem Grossglockner an einem
sonnigen Tage nur dann rathsam, wenn auch der neue Glocknerweg (dessen Eisenstangen
und Drahtseile jetzt unverlässlich sein sollen) wieder gangbar gemacht sein, und der
Beobachter somit die Möglichkeit haben wird, auf dem Felskamme zwischen Kôdmtz- und
Teischnitzgletscher zur Stüdlhütte (2800) herabzusteigen, falls der Abstieg über den
sehr steilen Gletscher unter dem Kleinglockner des lockeren Schnee’s halber nicht mehr
ausführbar sein sollte.

c. Das Präcisions-Nivellement wurde auf folgenden Linien ausgeführt:
1. Pilsen—Prag doppelt.

2. Pilsen— Eger doppelt, in Eger erfolgte der Anschluss an das bayerische Nivelle-
ment und wurde auch die bei Eger gelegene Grundlinie in das Nivellement
einbezogen.

3. Zell am See— Wörgl— Kufstein doppelt mit Anschluss an das bayerische
Nivellement.

4. Zweite Messung der Linie: Hauptfixpunkt (westlich von Marburg bei Faal)—
Bleiburg—Klagenfurt— Villach mit Einbeziehung des Drau-Pegels in Villach
und der astronomischen Station S. Peter bei Klagenfurt.

5. Doppelmessung der Linie Gross-Kanizsa—Si6ö Fök—Stuhlweissenburg mit Ein-
beziehung des beim Landungsplatze in Siö Fök befindlichen (Plattensee) Pegels.

6. Einfache Messung der Linie Agram — Karlstadt — Ogulin— Sensko, zum An-
schlusse an die einfache Linie Sensko—Fiume.

137

 
