ET ST en Tr!

1m

/weite Sitzung

der permanenten Commission.

Verhandelt Hamburg, Donnerstag den 5. Sept. 1878.

Anfang der Sitzung 10 Uhr 20 Minuten.

Präsident: Herr Ibanez.

Schriftführer: die Herren Bruhns und Hirsch.

Anwesend von der permanenten Commission die Herren: von Pauernfeind,
Bruhns, von Forsch, Hirsch, Ibanez, Mayo, von Oppolzer; von den Bevollmächtigten die
Herren: Adan, Barozzi, Barraquer, Ferrero, Hilgard, Perrier, Peters, Sadebeck,
Villarceau; ausserdem die Herren: Sainte-Claire Deville, Professor Bruns aus Berlin,
Riimker, Friedrichsen, Fels, Plath, J. Repsold, Reitz und J. Herz; später Herr Bürger-
meister Dr. Kérchenpauer und Senator Hayn.

Das Protokoll der vorigen Sitzung wird in deutscher und französischer Sprache
verlesen und genehmigt.

Vorgelegt werden: Zwei Karten von Hamburg und Umgebung, ein ,, Rapport de
la Belgique“, eine graphische Uebersicht von Refractions-Beobachtungen ausgeführt vom
31. August bis 8. September 1849 mit drei Universal-Instrumenten auf dem Brocken,
Derenburg und Kupferkuhle bei Kroppenstädt von Baeyer, Bertram und von Hesse.*)

Der Präsident begrüsst als neu anwesend Herrn Oberst Barozzi, Herrn Samte-
Olaire Deville, Dr. Bruns aus Berlin, ferner die eingeladenen Herren Dr. Plath, Repsold
und Reitz aus Hamburg.

Der Tagesordnung gemäss, wonach die Berichterstattung der Herren Commissare
vorzunehmen ist, theilt Herr /banez mit, dass über die Badischen Arbeiten Herr Baeyer
berichten werde, sobald dessen leider angegriffene Gesundheit seine Anwesenheit
wieder gestatte.

Es erhält das Wort der Commissar für Bayern Herr von Bäuernfeind, der über
noch vorzunehmende Bestimmungen von Azimuthen, Polhöhen und Lothablenkungen,
über Refractionsmessungen und eine kleine Basismessung Mittheilung macht. **) :

Zu der projectirten Basismessung ersucht Herr von Bauernfeind für 1879 oder
1880 die Benutzung des neuen vom geodätischen Institut in Berlin bei Brunner in

*) Siehe die lithographinte Tafel.
**) Siehe im Generalbericht, Bayern.

 
