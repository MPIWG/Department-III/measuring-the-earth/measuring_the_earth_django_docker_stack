 

|

a
ae
i

ey
a
|

u

12 Hl. SAINTE-CLAIRE DEVILLE ET E. MASCART.

MM. Brunner ont tracé sur ces deux cylindres deux traits équidi-
stants, séparés par une longueur tres-voisine du metre des archives.

L’un d’eux est destiné à servir de témoin temporaire pour la compa-
raison. Il est maintenu, sur des supports à roulettes, dans une auge
à double paroi montée sur le chariot d’un comparateur (fig. aly

Une seconde auge semblable, placee sur le méme chariot, recoit le
plus gros (T) des deux tubes, celui sur lequel doivent porter les expe-
riences. Le chariot permet de faire passer successivement sous deux
microscopes fixes les traits tracés à égale distance sur les deux tubes
cylindriques.

Les microscopes, montés sur des piliers solides, permettent de dé-
terminer dans chaque expérience les longueurs comprises entre les
deux traits de chaque tube, d’où l'on peut déduire le coefficient de
dilatation de la matière.

Ce comparateur a été construit avec le plus grand soin par
MM. Brunner frères.

Le problème que nous nous sommes posé n’est pas simple. C'est
pour cela, et à cause de la difficulté du sujet, du long temps qui s’é-
coulera encore avant que le travail soit terminé, que nous allons décrire
non-seulement nos appareils construits, les résultats déjà obtenus, mais
aussi nos appareils en construction et nos projets mêmes, afin que nous
puissions recevoir les critiques et les conseils de tous les membres de
l'Association et même de tous les savants qui, en dehors d'elle, s’oc-
cupent de métrologie.

Notre intention est d’abord de déterminer le coefficient de dilatation
du platine iridié de la règle géodésique avec une grande précision et
ensuite de transformer en témoin permanent l’un des tubes (le tube T)
construits avec la matière de cette règle. Un pareil tube témoin pourra
servir en tout temps pour constater si cette matière n’a pas subi, par
suite d’une action moléeulaire, une modification qui impliquerait un
changement de longueur de la règle géodésique, et pour déterminer
alors la variation de cette longueur.

Nous nous oceuperons d’abord du coefficient de dilatation de la règle
géodésique, et nous examinerons successivement les différentes parties
de l’opération.

1° Détermination des longueurs. — Notre comparateur est compose

Adi

zu usa: MB A LLL bh u él su

Lulu

ui

Lab Lulu

 

0.) rasée mme si |

 

 
