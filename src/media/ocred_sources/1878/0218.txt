   

a m eo se nn

t
N
fF
1

 

mr

26

la différence entre les déterminations géodésiques et astronomiques de la latitude, de la
longitude et de l’azimut.

M. Yvon Villarceau a traité les mêmes questions dans les mémoires qui sont
publiés dans les volumes 62, 67, 73 et 76 des ,,Comptes rendus de l’Académie des sciences“
et au Journal de mathématiques pures et appliquées‘ (2% Série Tome XIT), mémoires

que M. Adan cite dans ses écrits. L’exposé théorique de M. Villarceau diffère essen-

tiellement de celui de M. Adan et il est plus complet en ce sens qu’il permet de déduire
de la comparaison des valeurs astronomiques et géodésiques des trois coordonnées non
seulement les éléments les plus probables du sphéroïde, mais en même temps la forme
de la surface de niveau à laquelle toutes les verticales sont normales. Ce dernier pro-
blème n'a pas été traité par M. Adan qui s’en réfère à l'application du 32° théorème
de M. Villarceau, *) en faisant remarquer que les altitudes des stations astronomiques,
par rapport à la surface du sphéroïde probable, peuvent être déduites par des obser-
vations de pendule.

Les formules données par M. Adan dans le mémoire Correction des éléments
de Vellipsoide osculateur‘ peuvent sans doute être employées utilement dans les recherches
sur les déviations de la verticale: il fait suite aux travaux antérieurs par lesquels l’auteur
a si bien mérité de la géodésie.

Certaines hypothèses sur le mouvement du pendule qu'on rencontre dans ce
dernier mémoire, n’ont pas de rapport direct avec l’objet principal de cette étude, de
sorte que nous pouvons nous dispenser de les discuter ici. —

M. Adan remercie la commission de sa bienveillante appréciation des travaux
qu’il a présentés.

Toutefois, il se réserve de discuter dans une nouvelle note quelques points
qui lui semblent mériter un nouvel examen. Il se borne à relever dans ce moment le
fait que les formules qu’il a données dans son mémoire étaient destinées plutôt à servir
de préparation qu’à être substituées aux formules données antérieurement par M.
Villarceau.

M. le Président après avoir remercié M. le Professeur Peters, met en discussion
le point 4 du programme, savoir le choix du lieu et de l’époque de la VI™ conférence
générale.

M. Bruhns propose Munich qui offre beaucoup d’avantages déja par sa position
centrale.
M. von Bauernfeind dit n’avoir aucun doute que si la Commission est unanime
pour désigner Munich, son gouvernement recevra avec plaisir l’Association géodésique.
Toutefois il n’est point autorisé par son gouvernement à donner à cet égard une assu-
rance formelle.

M. Hirsch est d'avis que dans cette situation, la Commission, pour respecter

la liberté de la décision du gouvernement bavarois, devrait se borner à émettre le vœu

*) Voir Adan, „Deviation ellipsoidale‘ page 15.

 

a da sua Lab à à là nanas

ii es

 
