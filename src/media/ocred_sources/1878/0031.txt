PRE Re TN

1 RPE

mt

ya nun

aparı

iR
se
36
a
23
2

iR
BS
à
=3
5
=

Länge bildet, erfolgt, 5 Grundlinien sind gemessen, und 50 Polhöhen, 80 Azimuthe
und 6 Längendifferenzen bestimmt.

Der Einfluss dieser Beobachtungen auf die Elemente der Figur der Erde wird
sein, dass die von Clarke gefundenen Dimensionen nur wenig vergrössert werden, ohne
die Elliptieität merklich zu ändern. Ein geometrisches Nivellement von der Ostküste
bis an die Westküste ist angefangen worden; die Pendelversuche sind fortgesetzt und
Herr Peirce ist mit der Reduction der bisher ausgeführten Beobachtungen beschäftigt. *)

Der Präsident Herr von Bauernfeind spricht den Dank der Versammlung aus.

Herr Ibanez übernimmt wieder das Präsidium.

Herr Adan hat einige wissenschaftliche Fragen über die Zugrundlegung ver-
schiedener Abplattungen bei der Berechnung der Dreiecksnetze in drei Broschüren vor-
gelegt, und zum Studium dieser Fragen schlägt der Präsident die Uebertragung derselben
an eine Commission, bestehend aus den Herren Peters, Villarceau und von Oppolzer, vor.
Der Vorschlag wird einstimmig angenommen.

Herr Hirsch trägt im Auftrage des Herrn Baeyer folgendes Schreiben, welches
die Errichtung von Observatorien fiir die Erforschung der terrestrischen Refraction
motivirt, vor:

Bericht an die permanente Commission über die Gründung eines
Observatoriums zur Erforschung der terrestrischen Refraction.

In der 2. Sitzung der vorjährigen allgemeinen Conferenz in Stuttgart habe ich
die Hoffnung ausgesprochen, dass es vielleicht gelingen werde, auf der Mannheimer
Sternwarte ähnliche Refractions - Beobachtungen ausführen zu lassen, wie ich sie im
Jahre 1849 auf dem Brocken **) ausgeführt habe. Diese Hoffnung gründete sich darauf,
dass Se. Exxcellenz der Badische Minister des Innern, Herr von Stösser, auf einen
ihm darüber gehaltenen Vortrag mich ermächtigt hatte, in dieser Angelegenheit mit,
dem Director der Mannheimer Sternwarte, Herrn Dr. Valentiner, in Verbindung zu tre-
ten. Gleichzeitig fügte er aber auch die Bemerkung hinzu, dass vor Ablauf der jetzigen
Finanzperiode auf eine Bewilligung von Geldmitteln nicht zu rechnen sei. — In Folge
dessen ersuchte ich den Herrn Dr. Valentiner, der sich lebhaft für die Refraetionsbeob-
achtungen interessirte, sich darüber auszusprechen, Ob und Wie und mit welchen
Kosten auf der Plateform der Mannheimer Sternwarte ein Pfeiler und ein Öbser-
vationshäuschen zu errichten sei? — Wenn die Mittel des Instituts zur Herstellung
des Häuschens ausreichten, so könne ich ein 8zölliges Universal-Instrument von Pistor

*) Siehe Generalbericht, Vereinigte Staaten.
**) Auf dem lithographirten Blatt, welches in der zweiten Sitzung vorgelegt (S. pag, 15).
sind einige der zahlreichen beobachteten Curven graphisch dargestellt. .

 
