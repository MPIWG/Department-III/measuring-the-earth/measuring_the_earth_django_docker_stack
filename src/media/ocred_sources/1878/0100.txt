 

92

Bericht

über die von der Triangulirungs-Caleul- Abtheilung des K. Königl. militair-
geographischen Instituts im Jahre 1878 für Zwecke der Europäischen
| Gradmessung ausgeführten Arbeiten.

A. Astronomische Messungen.

I. Bestimmung von Breite und Azimuth auf dem trigonometrischen Punkte Dubica
Kula an der Unna.

1. Die Breite wurde nach zwei Methoden, nämlich aus Zenithdistanzen nörd-
licher und südlicher Sterne, dann aus Sternpassagen durch den 1. Vertical
bestimmt.

a) Zur Polhöhen-Bestimmung nach der ersten Methode wurden gemessen:
48 Sätze (zu 6 Pointirungen, von denen 3 bei Kreis links, 3 bei Kreis
rechts) des Polsternes und die Kreisverstellung derart geregelt, dass
die Zenithpunkte auf 45°, 135°, 180° und 270° fielen, dann die Beob-
achtungszeit auf die Morgen- und Abendstunden möglichst gleichförmig
vertheilt; ferner

Circum-Meridian-Zenithdistanzen solcher Südsterne, welche im Mittel
nahezu dieselbe Meridian-Zenithdistanz haben, wie der Polstern, nämlich
a Orionis, y Orionis, « Canis minoris, 8 Ophiuchi, = Orionis, 7 Serpentis,
© Ophiuchi, 6 Librae, im Ganzen 46 Sätze. Weitere 28 Sätze von 8
Ursae minoris und 24 Sätze der entsprechenden südlichen Sterne,
nämlich a Bootis, y Geminorum, a Ophiuchi und a Leonis, — endlich

16 Sätze von & Draconis mit 12 Sätzen der Südsterne: a Coronae
borealis und 6 Herculis.

Im Ganzen sind demnach 522 Doppel-Beobachtungen (bestehend aus
je einer Pointirung bei Kreis rechts und einer bei Kreis links) gemacht
worden und ergab sich hieraus die Polhéhe des trigonometrischen
Punktes Dubica Kula

fo Ay e220

b) Im 1. Vertical wurde

0 Cie... an. Abenden
à Cveni à
a Ne

E (yon... A >

© Andromedae

5 und
ı Andromedae „,

Go Oo

39

 

ANTON IL ELEC Ree 1 ai:

Ti

itl

il

OC LT

 

{

 
