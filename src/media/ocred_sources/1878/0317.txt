D SE RT A

COTES

WL AT

li!

1000 N a

HW! ip?

10. Mergentheim—Wirzburg . . . . . 191601 00020 > Jo 7am)
11. Würzburg—Weikersheim . . . . . 414355 — 00016 — 41) 1003
12. Nordlingen—Augsburg—Ulm. . . . 48.1451 — 0.0906 — 48 1945
13. Ulm—Süssen . . . . . . YL 9669 2000 = on
14. Süssen— Gaildorf -. . . 2.2... a7 io en er
15. Süssen—Plochingen. . . ,„ © 0 7112907 "oo == file
16, Plochingen—Stutigart - 0 3.8858 + 0.0005 = 3.8863
17. Bietigheim—Stuttgart - . «4 . 1.2, 99.9693 + 0.0037 == 2040040
18. Stuttgart—Herrenberg . .. . . . 178.6519 = 60048 = 1764567
19. Herrenberg—Horb . . . . : i), 869592 —— 0.0062 =. 02
20. Horb=-Mühlackr 2 7 . 150.7391. + 0.0360 — 150. 7751
21. Plochingen—Tibingen. . . . .. . 684788 = ©0110 = 07
22. Tibingen—Herrenberg. . . . + … 106.3108 — 0.0077 = 100-020
23. Ulm—Aulendorf... ..; ..:. ... (4499 = 0025, = nr
24. Aulendorf— Tübingen . . : . … . 2251900 = 0.0219 — 9950010
25. Aulendorf—Friedriehshafen ; . . . 143.3915 1 00109 — 143 4017
26. Friedrichshafen—Radolfszell . . . . 6.0121 4 0021 - co»
21. Badelfiszei-Hord - 6.4255 + 0.0226 = 6.4481
28. Ulm—Nonnenhorn . . ... . «+ 29 on, 5 0707
29. Nonnenhorn— Friedrichshafen . . . 14.7179 — 0.0010 = 14.7169
30. Crailsheim—Goldshöfe . . . . .... 59.5582 — 0.0040 = 59.554

31. Goldshöfe—Nördlingen . . . =. . 40.4488 1 0.0045 = 104
Für die Herleitung absoluter Höhen dienen nun die Anschlusspunkte an das
bayerische Präcisionsnivellement, deren Höhen Herr Director von Bauernfeind in seiner
neuesten (fünften) Mittheilung über das bayerische Präcisionsnivellement veröffentlicht hat.
Diese Anschlusspunkte sind:
Höhenmarke am Ulmer Münster . . (V. Mittheilg. S. 84 No. LX.) 478.5434
Württemb.Höhenmarke bei Nonnenhorn (I. 5 „110 „ 608) 20272
Höhenmarke am Bahnhnofv.Nördlingen (I. ; 5» OL „ 230 203031
Es wird somit der Höhenunterschied:

Ulm— Nördlingen im bayr. Niv. 48.1500, im württ. Niv. 48.1245 Differ. — 0.0255
Nôrdlingen—Nonnenhorn*) „ „ 104002, à 5 „10.1558 , 0007,
Nonnenhorn — Ulm oe 5 DO2OO2, 2 », 98.2004 — 1000
Würde man die Höhe von Ulm zu Grunde legen, so fände sich:
bayrisch württemb. Diff.

Ulm 478.5434 478.5434 0

Nonnenhorn 420.2772 420.2830 — 0.0058

Nordlingen 430.3934 430.4189 — 0.0255

Um diese Differenzen zu vermitteln, sind die württembergischen Höhen noch
um 0.0104 zu vermindern, so dass man hat:

*) Die abweichende Angabe. von Bauernfeinds in seiner V. Mittheilung $ 15 beruht auf einer
Mittheilung früherer Ausgleichungsresultate,

 
