a

 

FT

DFI

OTT TT TPIT PTT ae

103

Dr. Stephan zum ersten Male eine unterirdische Landleitung zur Verfügung gestellt
worden war. Dieser Umstand bot zugleich den Anlass, an solchen Abenden, an denen
infolge bewölkten Himmels astronomische Beobachtungen nicht ausführbar waren, ver-
gleichende Versuche über die Fortpflanzungsfähigkeit des electrischen Stromes in ober-
und unterirdischen Landleitungen anzustellen; ein V orhaben, welches seitens des Herrn
General - Postmeisters Dr. Stephan bereitwilligste Unterstützung fand. Derselbe geneh-
migte, dass an solchen für die Längenbestimmung nicht auszunutzenden Abenden behufs
Ermöglichung einer unmittelbaren Vergleichung des Verhaltens der oberirdischen und
unterirdischen Leitung an Stelle der unterirdischen Leitung zwischen Berlin und Altona
eine oberirdische zwischen denselben Stationen eingeschaltet werde, und ferner, um über
die Abhängigkeit der bezüglichen Erscheinungen von der Leitungslänge ein Urtheil zu
gewinnen, dass während der Längenbestimmung Altona—Bonn-- Wilhelmshaven in gleicher
Weise die oberirdische Leitung Altona—Bonn nach Bedürfniss durch eine unterirdische
Leitung von Altona über Hamburg, Berlin, Frankfurt a. M. nach Mainz in Verbindung
mit einer oberirdischen Leitung von Mainz nach Bonn ersetzt werde.

Diese Versuche liegen gegenwärtig abgeschlossen vor und ist deren Reduction
bereits in Angriff genommen. Obwohl dieselbe bis jetzt noch nicht hat zu Ende geführt
werden können, lassen die bisherigen Ergebnisse doch schon mit Sicherheit erkennen,
dass aus diesen Versuchen sehr interessante Resultate über die Fortpflanzungsfähigkeit
des electrischen Stromes in ober- und unterirdischen Leitungen, sowie insbesondere auch

über die Form der Curve der Stromintensität auf der Endstation einer längeren Leitung
hervorgehen werden.

Nachschrift: Nach Beendigung der Reduction. ist eine vorläufige Publi-
cation der Resultate dieser Versuche in No. 2225 der
Astronomischen Nachrichten erfolgt.
Albrecht.

D. Bericht der Section des Professor Dr. Fischer.

Nach Beendigung der praktischen Arbeiten im Sommer 1877 , in welchem die
Winkelmessungen für das rheinische Netz vollendet wurden, führten die beiden Mitglie-
der der Section, Dr. Fischer und Dr. Westphal, nachstehende Bureauarbeiten aus:

1. Die Stationsausgleichungen der im Sommer ausgeführten Richtungs-
beobachtungen.
Den Druck des zweiten Heftes des rheinischen Dreiecksnetzes: „Die
tichtungsbeobachtungen“.
3. Die Aufstellung der Bedingungsgleichungen für das rheinische Hauptnetz
und die Auflösung derselben. Die Ausgleichung ist bis zur Prüfung der
Gleichungen in Bezug auf die Correlaten vorgeschritten.

bo

 
