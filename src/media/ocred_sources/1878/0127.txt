FH RER |

IM Or ir

ir

NT FBI

ETES LFP PAT a

119

below the lower knife-edge. At the end of this tongue was soldered a horizontal table
of brass carrying a glass micrometer scale in a horizontal plane. This was looked at
with a high power microscope directed vertically downwards and carrying an eye-piece
micrometer scale. The experiments consisted first in drawing the pendulum to one side
by means of a string passing through the hole just above the lower knife-edge, this
string being a long one and carefully kept perpendicular to the axis of the pendulum.
The effect for the small angles of deflection used (less than 3°) is sensibly the same
as if the force were applied at the knife-edge. The pendulum being thus deflected, the
are of deflection and the displacement of the micrometer scale were both carefully
observed. To get the dynamical deflection, the pendulum was simply set Swinging by
applying the finger close to the lower knife-edge, and readings of the amplitude of its
oscillation and that of the seale attached to the stand were alternately made. In this
way, I found in three sets of experiments as follows:

‘

 

 

Statical Dynamical
flexure. flexure.
2068 tee 259 2
260 1 2 208 4 4
203.0 2 200 à |
Mean: 267 27 201 1

The near equality of the two was further shown by the fact that the ratio of
the dynamical flexure to the arc of oscillation remained constant from the first starting

of the pendulum; for the statical flexure is nothing but the initial value of the dyna-
mical flexure.

Lest it might be objected that the long tube which I attached to the pendulum
stand could in some mysterious way produce an error in my result, I have made other
experiments, in which the micrometer scale was attached directly to the upper part of
the Repsold tripod in a vertical plane parallel to that of the oscillation of the pendulum
and have looked at it with the microscope direeted horizontally. These experiments are
naturally less accurate than the others, but they give the same result, the ratio of
dynamical to statical flexure being according to six sets of experiments:

0.996
0.973
0.979
0.992
0.963
0.986

 
