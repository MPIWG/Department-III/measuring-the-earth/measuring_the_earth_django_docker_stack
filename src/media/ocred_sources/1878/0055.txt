À US memes mess do +

 

IE ART

rr Or TNT T TIT DTT TT eer eerie

47

opérations géodésiques, suivie de quelques développements théoriques. Le calcul de
compensation de ses séries d’observations, exécuté suivant la méthode de Bessel, l’a
conduit à reconnaître l’inexactitude des résultats obtenus, lorsqu'on traite d’une manière
identique les erreurs de division et les autres sources d'erreur. D’autres erreurs encore
proviennent du fait que les directions prises sont introduites dans le calcul de compen-
sation du réseau avec des poids différents. Il croit qu'au lieu des observations de
directions en girus, il est préférable de retourner à l’ancienne méthode de Gauss, de
mesurer les angles.

M. Andrae attire ensuite l'attention de la réunion sur la question de savoir
si et dans quelle mesure on peut compter sur l’immobilité .des points soi-disant fixes.
En eflet, en remesurant un triangle mesuré autrefois par Schumacher, M. Andrae a
trouvé des différences de plusieurs secondes, qui, pour deux angles, ont méme atteint la
valeur de 7 secondes.

Il croit que ces différences sont produites par des changements ou des déplace-
ments de la surface terrestre, et que ce nouveau point de vue rend la tâche de la
géodésie plus compliquée et de diffieile.

M. d’Oppolzer demande, à l’occasion de l’&eart de 7”, si Yon a vérifié aussi les
triangles voisins, parce que, là aussi, des écarts analogues auraient dû se produire.
M. Andrae répond que malheureusement on n’a pas pu le faire.

M. Villarceau remarque que lui aussi s'est occupé de cette question, et pense
que la constance de la verticale dans un lieu ne peut être contrôlée que par des ob-
servations astronomiques. Dans ce but il faudrait que les astronomes renonçassent
au cercle vicieux dans lequel ilstournent à à présent, en déterminant ensemble, et les uns
par les autres, les ascensions droites des étoiles et la marche de la pendule. Selon lui, il
faudrait déterminer la marche de la pendule indépendamment du ciel, ce qu’on pourrait
obtenir en établissant deux pendules dans un lieu à température et pression constantes.

M. Perrier croit également que les changements des angles avec le temps con-
statés par M. Andrae doivent être attribués à des mouvements du terrain, Il est
davis qu'on arrivera peut-être un jour à constater des variations périodiques de la
forme de la surface terrestre, provenant des mouvements de la masse intérieure du
globe, semblables aux mouvements des marées océaniques. Les phénomènes dont il est
question dans ce moment pourraient conduire à remplacer la géodésie statique actuelle
par une géodésie dynamique de l'avenir.

M. Hirsch a suivi les importantes communications de M. Andrae avee un in-
térét tout particulier. La premiére question soulevée par M. Andrae a une importance
speciale pour la Suisse, ou, par suite de circonstances particuliéres, on a fait en partie
des observations d’angles et plus tard des observations de directions qu'il s’agit de
combiner dans une seule compensation. Dernièrement on a fait exécuter en Suisse,
par M. le docteur Koppe, la compensation d’un polygone par les deux méthodes de
Besse ar celle d’Andrae, et on a pu se convaincre que, non seulement
les valeurs définitives, mais aussi leurs erreurs sont presque identiques dans les deux cas.

Le second point mentionné par M. Andrae, concernant les mouvements du

           

 
