228 een

Im ul

ren

ALAM MLA, MM

SUR LA CONSTRUCTION DE LA REGLE GEODESIQUE INTERNATIONALE. A

près a la dimension requise, 5 millimètres d’épaisseur sur 21 milli-
mètres de largeur (4",r0 de longueur). Enfin la forme parfaitement
rectangulaire fut donnée à la règle en la faisant passer plusieurs fois
à travers le même trou d’une filière en acier.

» À chaque passe on a recuit la règle à la plus haute température, au
moyen d’un très-grand chalumeau à gaz alimenté par de l'oxygène, et
on l’a fait passer plusieurs fois par le même trou de la filière, en la
chauffant chaque fois, jusqu’à ce qu’elle n’y subit plus de pression et
qu’elle n’acquit plus d’allongement sensible. Après chaque passe, soit
sous le marteau-pilon, soit sous le laminoir, soit au travers de la
filiere, la matiere, chauffee soit dans un grand moufle en platine, soit
au chalumeau, était décapée par du borax fondu et de l’acide chlor-
hydrique concentre. »

Le platine iridié de la regle géodésique préparé par M. Matthey a
été pour moi le sujet d’une étude attentive dont je donnerai les princi-
paux résultats :

1° Un petit lingot fondu, pris dans la masse fondue, pesait

. Dans Paire, 0% 2 Pere 3108,89
Dans l’eau à 18 degrés. . 296%, 424

Sa densité a zero est donc 21,508.
2° Une masse parallélépipédique pesait

Dans Pair ui Spies
Dans l’eau à 4 degrés. -.. Bug

Sa densité était done 21, 516.

Il en résulte que la matière, recuite a tres-haute temperature, a re-
pris sensiblement la densité du métal fondu.

3° Son analyse, faite sur deux échantillons pris aux deux extré-
mités de la règle, a donné :

I. U.
Platine ire, res 89,40 89,42
Imndiam. 2... 0.0.5 =, 20410 10,39
Rhodiamiss 8. 0,18 0,16
Ruthenium. nr. 2. 0,20 0,10
Ders. oe. ana d08 0,06

9990 99,96

 
