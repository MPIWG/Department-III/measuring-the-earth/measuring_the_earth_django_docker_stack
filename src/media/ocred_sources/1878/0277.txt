 

=

a gen iu!

2 FL a ae

u
u

OSS Ge da à

= U + di’) + y see W + aa’),
2" = (2° + dze')— n tang (N + dx’)
oder mit vollkommen hinreichender Genauigkeit:

N ae |
— (7 — l') cos A cos al’
n—= @ — 2") cotane N I COLane 7) dz |

S

(1)

wo wir mit ¢ und n die Abweichungen der Lothlinie von der Normale des Erdsphäroids,
respective in der Richtung gegen Süden und gegen Westen bezeichnen.

Die Bedingungsgleichungen werden in bekannter Weise aus den Gleichungen (1)
abgeleitet, indem man für dN, dl’ und de’ ihre Ausdrücke mittelst der durch die Aus-
gleichungen zu bestimmenden Elemente substituirt. Da man bei Uebertragung des Drei-
ecksnetzes auf das Erdsphäroid nicht nur der grossen Halbachse des Normalsphäroids a
und der Abplattung a die Zuwächse da und de, sondern auch der Breite X und dem

Azimuthe z die Zuwiichse da und dz beilegen muss, erhalten die erwähnten Ausdrücke
die Form:

dh == da +8 da ice 0 9 4G |
di. = MN .da + 8 da = © a a

(2)
de = MW da + DB da vo.

und die behandelte Aufgabe ist folglich auf die Bestimmung der 12 Coefficienten: Aa,
1”, 8,8, BEC CE... 2mrichec ut,

Eine sehr einfache Lüsung dieser Aufgabe geben nun die für die unmittelbare
Substitution in (1) geeigneten Formeln:

UN (e— 4). +R@A+(ce—3A)sin An)da- COS 4. dÀ + Sin cos À. de |

 

 

 

da : ; 2
COS = cos) = + @ sin? A cos A.do — sin gsinX.dA + (A —a) dz (3).
da é COS À sind COS À
; 9 4 2! — 5 ae ın 3 San 7 Ny 1 5 7 l2.
+ cotang X'dz/—9 cos x. = + 0 sin N de .ŒX +C080 m 0 |

Da hier:

 

A ed bo emo, a ;
so werden simmtliche Coefficienten mittelst einer auf ein Minimum reducirten Rechnung,
wobei vierzifirige Logarithmen vollkommen genügen, durch die unmittelbar gegebenen
Positionen von A und B bestimmt. Betrachtet man die Entfernung vom Centralpunkte,
AB = K, als eine Grésse erster Ordnung, so sind die Coefficienten überall, mit einziger
Ausnahme eines sehr kleinen Gliedes im Ausdrucke für B”, bis auf Grössen dritter Ord-
nung excl. entwickelt, und die Formeln (3) geben daher für Entfernungen von 50 bis

 
