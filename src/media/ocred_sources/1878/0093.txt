ze

À LA La

iR!

Emil

Ten)

Perlen IHR BL ET

85

proquement, afın de vérifier la visibilité des sommets. Ils étaient munis de théodolites
légers, permettant d’obtenir, d’une maniere approchée, les angles du réseau, et de mesurer
les hauteurs des sommets au-dessus de l’horizon.

Les conditions, dans lesquelles étaient placés les observateurs, étaient particulière-
ment difficiles. En Algérie la fréquence des brumes pendant les mois d'été empéchait
de voir les côtes d’Espagne, et les: premières pluies de septembre pouvaient seules
dissiper ces brumes. En Espagne, au contraire, à cause de l'altitude des sommets, les
observateurs ne pouvaient y stationner que pendant la saison chaude.

C’est dans la période comprise entre la fin du mois d’août et le 15 octobre
que les groupes placés en Espagne, sous la direction du colonel de Monet, et, en Al-
gérie, sous les ordres des capitaines Derrien et Koszutski, ont pu réciproquement, mais

non sans peine, apercevoir et viser, en azimut et en hauteur, les miroirs des sta-

tions opposées.

Le problème de la liaison de lAlgérie avec l'Espagne peut être considéré
comme résolu.

En augmentant la surface des miroirs solaires pour les observations de jour, et
employant la lumière électrique pour les observations de nuit, la visibilité réciproque
des diverses stations sera plus fréquente et on peut espérer de terminer les observations
définitives pendant la campagne de 1879.

L’arc de méridien anglo-franco-espagnol s’étendra alors, sans interruption, depuis
les îles Shetland jusqu’au désert du Sahara par ane amplitude de 28 degrés.

Publications.

Le premier fascicule du tome XI du Mémorial du Dépôt de la Guerre sera
prochainement envoyé aux membres de l’Association. Il contient tout ce qui concerne
la station astronomique d’Alger (longitude, latitude et azimut fondamental de la
carte d'Algérie).

Perrier.

Hessen - Darmstadt.

Im Jahre 1878 sind keine nivellitischen Arbeiten weiter ausgeführt worden.

Herr Hügel hat wegen Kränklichkeit leider das Mandat als Bevollmächtigter
niedergelegt.

Les

 
