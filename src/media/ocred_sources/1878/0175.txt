I en vr 8

 

paar

a

mire

DETENTE NBA

SUR LA CONSTRUCTION DE LA RÈGLE GÉODÉSIQUE INTERNATIONALE, 45

pour fléchir sous leur propre poids, finissent par contracter une cour-
bure permanente quand elles sont appuyées obliquement,

Savart, en faisant vibrer dans le sens de sa longueur un fil métallique
chargé d’un poids, dit l’avoir vu s’allonger de - de sa longueur pri-
mitive tant que duraient les vibrations et conserver ensuite un allon-
gement permanent que n'avait pu produire la charge.

Il résulte encore des expériences de M. Wiedemann que, si l’on tend
une règle par des poids ou même par son propre poids, il reste le plus
souvent un peu de flexion permanente, que la méthode tres-précise du
miroir permet de constater.

Notre opinion est que le tube cylindrique témoin de la règle géo-
désique doit être couché sur du velours fait avec du coton écru ou
avec du fil de platine extrêmement fin. En tout cas, nous ne conseille-
rons jamais de le suspendre sur deux rouleaux. On pourrait craindre
qu'au bout d’un plus ou moins grand nombre d’années il ne se pro-
duisit une courbure dont la flèche, d’abord insensible, augmenterait
lentement avec le temps.

Un cylindre creux est le corps qui, sous le poids le plus petit, sup-
porte, sans se déformer, le plus grand effort : c’est la forme des os.
Nous croyons qu’une règle de cette forme est plus capable que toute
autre de résister à son propre poids et à des pressions extérieures.
Néanmoins nous croyons aussi qu'il sera prudent de la supporter dans
toute son étendue.

On pourrait encore la suspendre verticalement, en compensant Île
poids des différentes parties par des contre-poids placés à de petites
distances les uns des autres et maintenus à l’aide d’une légère pression.
Ce serait la meilleure disposition à adopter pour étudier journellement,
au moyen de microscopes horizontaux, les variations de sa longueur

avec la température. |
Mais c’est là un projet de comparateur à mouvement vertical que

nous ne faisons qu'indiquer, sauf à le développer plus tard, s’il est
praticable et s’il mérite de fixer l'attention des constructeurs.

Ajoutons quelques remarques sur la purification de l’eau distillée et
du mercure que nous avons employés.

x]

 
