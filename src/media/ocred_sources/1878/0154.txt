 

en oe

id
i
|
l
i

24 H. SAINTE-CLAIRE DEVILLE ET E. MASCART.

de la température, il vaut mieux encore entourer le tube thermomé-
trique d’une enveloppe peu conductrice, d'après la méthode indiquée
par M. le baron Wrede.

Ce sont là les motifs qui nous ont déterminés à faire un tube avec le
platine iridié de la règle géodésique et à le remplir d'azote comme
matière thermométrique.

L’azote a été préparé avec de l'air que l’on a fait passer au travers
de longs tubes de verre remplis de cuivre et chauffés au rouge: on a
purifié ensuite le gaz au moyen de potasse monohydratée et sèche.

On déterminera la température du tube par augmentation ou la
diminution du yolume de cette masse confinée d’azote, en méme temps
que seront faites les mesures de l’espace compris entre les deux lignes
tracées sur sa surface.

Voluménométre à pression et a température constantes.

Pour mesurer l'azote sorti de notre tube de platine iridié, nous avons
fait construire, par M. Alvergniat, un voluménomètre entièrement en
verre, fondé sur ce principe que la masse de gaz qu'ilcontient peut tou-
jours être ramenée à la pression d'une autre masse d'azote enfermé dans
un espace absolument imperméable et parfaitement clos; cet azote, le
vase qui le contient et le mercure qui est en contact avec lui, tout l’ap-
pareil enfin est toujours entouré de glace et par conséquent maintenu
a une température constante.

Le voluménometre se cempose d’un tube en U (fig. 5), dont la por-
tion M est remplie de gaz azote sec. Ce tube est surmonté d’un systeme
de robinets qui servent à introduire le mercure et l'azote, et que l’on
supprime après le remplissage; le tube M est alors fermé à la lampe
dans sa partie effilée, au-dessous du robinet R’.

La partie inférieure de ce tube est remplie de mercure dont le ni-
veau P est assujetti à toucher une pointe d’émail blanc faisant fonction
de la pointe d'ivoire du baromètre Fortin. Ce tube doit être aussi
fermé à la lampe au-dessus du robinet R”, qui sert, comme le robi-
net R’, au remplissage de l’appareil.

Le tube M’ communique lui-même par un tube horizontal avec un
réservoir V dont la partie supérieure O est soudée à un tube fin de

 

3
3
3
:
3
a
3
a
3
>
3
q

Pre 7]

an. ame u a a run nn

|
|

 
