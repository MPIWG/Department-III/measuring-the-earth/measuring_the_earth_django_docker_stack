a ee

 

a RT

RR RA a ni

FU]

 

a

Les azimuts ont été mesurés par les distances zenithales, par les angles horaires,
par les hauteurs correspondantes et par les passages méridiens des étoiles fondamentales.

Le réseau des triangles de 1° ordre fut fait d’un jet, sans séparation des
observations spéciales aux triangles qui doivent former les chaînes dans le sens d’un
méridien et dans le sens d’un parallèle, de façon que jusqu’à ce jour, le travail de la
compensation à été retardé. Pour opérer en une fois, il faudrait résoudre 258 équations
de condition dont les inconnues sont liées entre elles par plus de 500 équations de
poids. Afin d'éviter un travail aussi pénible, on a cherché le moyen d'opérer par
groupes de 30 équations au plus, tout en obtenant au résultat final l’exactitude qu'il
exige. Il consiste à poser des équations supplémentaires qui établissent les relations
d'identité entre les côtés trouvés par les groupes précédents; à éliminer des équations
de poids les corrections qui devraient s’appliquer deux fois avec des valeurs différentes
aux directions communes à des groupes voisins et à considérer comme auxiliaires les
corrections à faire aux directions liées par les équations des directions probables à
celles du groupe considéré, mais pour lesquelles les équations de condition géométriques
ne seront posées que dans la compensation des groupes adjacents. Ce travail est
commencé, il sera terminé pour la chaîne de liaison des bases dans le sens du parallèle
de 51° latitude boréale, vers le milieu de l’année prochaine.

Les côtés des triangles de 1° ordre ont été calculés de proche en proche, en
adoptant successivement pour côtés connus dans les triangles, les moyennes des valeurs
obtenues précédemment. C’est la même marche qui à été suivie dans la determination
des latitudes, des longitudes et des azimuts géodésiques, de façon que nous avons à
Lommel (signal):

latitude D6%8546.09 valeur astronomique,

1 5648539.76 ,, géodésique (provisoire),
azimut  23°0621.9 » astronomique,
u 2360579.5 »  géodésique (provisoire),

et a Nieuport:
latitude 36%8117.56 valeur astronomique,
8 56%8125.38 ,, géodésique (provisoire),
azimut 243° 7565.1 » astronomique,
a 2435 1522.9 » géodésique (provisoire).
Les différences varieraient si l’on avait fait le calcul par chaînes de triangles
et le résultat sera définitif lorsque la compensation du réseau sera complète.
Les longitudes géodésiques sont:
à. Lommel : (sig.).::..,04+ oe 1e 16081699;
3 Nisuport 57.0000: as Ba 0094
comptees de l’observatoire de Bruxelles.
Si nous comparons la latitude de Dunkerque donnée par Delambre à la latitude
géodésique déduite de Nieuport par le triangle Nieuport, Cassel, Dunkerque, nous

avons:
