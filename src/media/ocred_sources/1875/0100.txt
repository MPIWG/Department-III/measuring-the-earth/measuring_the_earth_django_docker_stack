 

ONE NO

RAPPORT DE LA COMMISSION DU PENDULE.

Circulaire de Mr. le Dr. Baeyer, president.

Le Bureau central de l’Association géodésique avait fait construire par MM.
Repsold à Hambourg un pendule à réversion destiné aux déterminations de l’intensité
de la pesanteur. Cet appareil a été décrit dans la publication de l’Institut géodésique
royal prussien intitulée ,,Astronomisch-geodätische Arbeiten im Jahre 1870% publiée
par Mr. Bruhns, Leipzig 1871 (pages 110—116).

Des observations ont été faites avec cet appareil: a Gotha, sur le Seeberg, sur
l’Inselsberg, à Berlin, Leipzig, Dresde, Freiberg, Bonn, Leyde, Mannheim et sur le
Rugard (Ile de Rügen). Mais dans les nombreux transports auxquels ce pendule a été
soumis, il a subi quelques modifications de forme et quelques altérations. De plus
l’expérience a prouvé que sa construction laissait à désirer à certains égards, de sorte
que le Bureau central a l'intention de faire construire un nouvel appareil pour lequel on
tiendra compte de toutes les expériences faites jusqu'ici et dans lequel on éliminera
toutes les causes d’erreurs connues, afin qu’il réponde aux exigences de la science
actuelle. Pour atteindre ce but, le Bureau central a fait un résumé

1° Des doutes qu’il a sur l’exactitude de son appareil ;

2° Des conditions qu'il est à désirer que le nouvel appareil remplisse;

30 Des procédés par lesquels on pourrait, d’après lui, satisfaire à ces conditions.

19 Doutes qu'inspire l'exactitude du pendule de Repsold dont le Bureau central
est en possession.

L’expérience nous a prouvé que les avantages que promettait un pendule d’un
metre de longueur étaient contrebalancés par d’assez graves inconvénients, et tout
d’abord qu'un appareil aussi long était peu maniable et difficile à transporter.

La nécessité de suspendre le pendule au moyen d’un trépied est aussi une

 

x

Head) Bimer |

D et 1101180

LA min

uno m I

 

|

2 them mameté À |
