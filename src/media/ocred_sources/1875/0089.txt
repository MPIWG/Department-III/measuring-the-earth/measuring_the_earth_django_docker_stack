|
|

 

PTT

Ir

=
x
à
=
=
È
=
>

Ferner ist das Aufhängen des Pendels an einem Stativ eine Quelle von: Fehlern,
da ein an einem Stativ schwingendes Pendel dasselbe ebenfalls in Schwingungen ver-
setzt und hierdurch der Gang des Pendels influirt wird. Wenn dies der Fall ist, so
kann zwar durch Verkürzung des Pendels und Verkleinerung des Stativs dieser Ein-
fluss vermindert werden, ob er ganz unschädlich ‘gemacht werden kann, bleibt fraglich.

Ferner wird der Genauigkeitsgrad des Endresultats wesentlich beeinflusst durch
die Beweglichkeit der Schneiden, insofern diese als Ursache von Veränderungen en
nichfacher Art zu betrachten ist.

Schliesslich verändert das Metallthermometer ax den Transport seinen Null-
punkt und ist daher in seinen Angaben nicht zuverlässig. Es scheint, dass die Ursache
hiervon in Erschütterungen bei dem Transport auf der Eisenbahn zw suchen ist,

2):Anforderungen, welche bei einem neuen Pendelapparat
wünschenswerth erscheinen,

Die Intensitätsbestimmungen der Schwere werden zur Ergründung der Loth-
ablenkungen künftig viel zahlreicher stattfinden: müssen, als dies bisher geschehen.

Wenn eine Lothablenkung durch seitlich lagernde Schichten von grösserer
Dichtigkeit hervorgebracht wird, so vermehrt sie die Intensität der Schwere; wird sie
durch Schichten geringer Dichtigkeit hervorgerufen, so vermindert sie die. ‘Schwere.
Es ist also möglich , bei Vorhandensein von Lothablenkungen durch Pendelbeobach-
tungen die Frage zu entscheiden , welcher von obigen Fällen vorliegt: Da es sich
dabei aber um sehr kleine Grdssen handelt, miissen alle Hilfsmittel , welche Wissen-
schaft und Technik gewähren , in Anspruch genommen werden. Namentlich wird der
neue Apparat folgende Erfordernisse haben müssen:.

1) Er muss möglichst einfach und solid sein, so dass er durch den PTO
und den Gebrauch nicht leidet.

2) Die Aufstellung darf nicht zu schwierig und die Beobachtung mit demselben
muss leicht sein und nicht zu viel Zeit erfordern.

3) Er muss derart construirt sein, dass der Genauigkeitsgrad, welchen die bis-
herigen Apparate dieser Art gewähren, thunlichst überboten wird.

3) Unmassgebliche Ansichten über die Erfüllung der aufgestellten
Anforderungen.

Die Vortheile, welche ein Reversionspendelapparat gewährt, sind. allgemein
anerkannt; andererseits steht aber fest, dass die Messungen der Abstände der Schnei-
den und der Lage des Schwerpunktes sehr subtile Operationen sind, und dass die dazu
erforderlichen Apparate zu den feinsten gehören ‚.die zu. Messoperationen verwendet
werden. Man wird daher nicht leugnen können, dass man auf passageren Stationen
diesen Operationen nicht dieselbe Sorgfalt wie auf festen Sternwarten, widmen kann,
und dass überdies nach verschiedenen und langen Transporten die Unveränderlichkeit
der Apparate selbst nicht verbürgt werden kann.

 
