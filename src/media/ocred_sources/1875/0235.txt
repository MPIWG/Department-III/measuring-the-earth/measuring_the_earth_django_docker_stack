 

ie Gradmessung ausgeführten Ortsbestimmungen.

IG

an

‚ determinees par la mesure des degrés en Europe.

en, Différences des longitudes.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

—_ — & =
Jahr und Monat. ie = Veröffentlieht. Titel der Publication. | Bemerkungen.
Année et Mois. = = =| Publié. Titre de la publication. | Remarques.
SS a ee: == er m = ee Fe = — = ee Pe E = "
1871, August, September | | Bruhns, Astron.-geodät. Arbeiten im Jahre 1871, Leipzig 1873. |\s. Preussen.
1870, Juli, 1871, Juli, Aug. | | Ebendaselbst. (Von der Red. hinzugefügt.
1873, Juli . ee | Bauernfeind u. Bruhns, Längenunterschied zwischen Leipzig
| | und München. Abhandlungen der k. bayer. Akademie |
: < | | der Wissenschaften U. Cl. XI. Bd. 11. Abth. | |
Br om, Beben ee . s. Oesterreich. |
1875, Mai, August, Sept. |: HR) | |
BB Me nen. | AR OR. me. à 0e. longe (Nana Italien. |
1875, August, September | . | . |. | x |
D ue Jul a ee en» Bauen \s. Oesterreich.
1874, Juli, August. DE ER en ee fVon der Red. hinzugefügt. |
1853, Novbr., December . | . |.|. | Airy, Memoirs of the Royal Society. | Gehören zu den ersten
1857, April, Mai, October |. |. ., Encke, Abhandl. der Academie, Berlin 1858; A. Quetelet, , Längenbestimmungen auf |
| | | | Annales de l’Observ. de Bruxelles. _ telegraphischem Wee, |
RE sel | Von der Red. hinzugefügt. i
1868, September . . . |: |... Kaiser, Annalen der Sternwarte in Leiden, 2. Bd 5 222 |
ee | Jr, |
1863 s | ee eee ee ne | Mr. Peters s’est chargé
| - | | du calcul des observations.
1865, Juli, September ee Voir,,Generalbericht 1865.
| | | Schweden“. |
1863 ead Annales de l’Observatoire de Paris. Mém. Tom. VUI. i
een un | = ==] ” ” ” ” ” ” ” ” |
1863 . . . ® . . | | . Sl 39 3° 39 ” se CE) „ |
NEO SE is. 5... e | a Fr 33 , : „ ” ” u ” |
iA Mai, Juni... (Cw. fe oak Monthly Notices, Vol. XV pag. 124.
ee oe ©. Heke | «eAomales de l'Observatoire de Paris. Mém. Tom. VIII. |
rae i Oe, DE | > 2 :, ” ” ” ” > |
1856 | feo 95 = 5 5 35
1864 on 9 é : 5 Ue de) „ 33 9 9 99 9 O°) ” |
Bus, Se us ae. | 59 5 er) 9 ” ” ”
1865 | | | 39 5 , . 5
| 4
(1863 D | a
1866 es : Ne Bes | Longitude géodésique.
1863 | | 5 „ 5 ’ A]
D a eB OR |
1873, August, October . |. | N te | Voir l’Autriche. f
1873, September, October |. |. |. | ee + |
|
|
il
| |
i
|

 
