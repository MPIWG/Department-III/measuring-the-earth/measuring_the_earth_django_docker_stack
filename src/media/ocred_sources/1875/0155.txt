 

IM un!

IUT AR

LEZ LIL LT REZ TT METIER LR

VAR I

145

théorème sur les attractions locales était satisfaite, à un très petit nombre de secondes
près, à l'inverse de ce qui se produisait pour les autres chaînes. du réseau français.
Plus tard enfin, ayant à pointer, à Saint-Martin-du-Tertre, un réverbère placé sur le
Panthéon, à 30 kilomètres de distance, Mr. Villarceau obtint, par les moyennes d’un
grand nombre de pointés faits avec un fil mobile, des résultats d’une extrême con-
cordance, pour l’azimut de ce réverbere.

De l’ensemble de ces faits, appuyés par des considérations théoriques,
Mr. Villarceau avait conclu que les observations de nuit devaient être l’objet de
nouvelles recherches, et il inclinait même à croire qu’elles devaient être préférées à
celles de jour, surtout dans les pays de plaine.

C'est à ce moment que nous entreprenions la mesure nouvelle de la méridienne
de France, en commençant par la région australe du côté de Perpignan et: remontant
vers le nord.

La partie de la méridienne comprise entre les Pyrénées et les montagnes de
"Auvergne ne se prétait guère aux observations de nuit, que Mr. Villarceau désirait
introduire dans la pratique de la Géodésie; les points de stations sont, en effet, situés
à des altitudes considérables, sur des sommets d’un accès pénible; les nuits y-sont
très froides, même en été, et les observations auraient offert des difficultés et des
dangers capables de rebuter les observateurs les plus intrépides.

Cependant, en 1874, Mr. Elie de Beaumont, dans un Rapport sur les premières
opérations de la Méridienne, recommandait instamment l'essai des observations de nuit
dans les régions où elles seraient possibles et que nous étions près d'atteindre; peu
apres, Mr. le colonel Laussedat faisait connaître à l’Académie que les signaux de nuit,
obtenus par des collimateurs optiques, donnaient presque toujours des images tranquilles,
uniformes et convenablement réduites.

L'étude des observations de nuit s’imposait à nous, et nous nous proposions
de faire, en 1874, des expériences comparatives sur des signaux de nuit et de jour;
mais, pour des motifs de service qu'il est inutile de rapporter ici, ce n’est qu’en 1875
que nous avons pu les entreprendre et les mener à bonne fin.

Les anciens observateurs, pourvus de cercles répétiteurs ou de théodolites,
avaient signalé en première ligne, dans la pratique des observations de nuit, la difficulté
d'obtenir un éclairage facile et régulier des divisions du limbe, aussi bien que du
champ de la lunette. Cet inconvénient n’existe pas pour nous; l'Association a pu voir
qu'avec notre instrument une seule lampe peut fournir un éclairage complet, et nous
avons même remarqué, dans les nombreuses observations faites en divers points, que
les traits de la graduation se détachent ainsi plus nettement que pendant le jour.

Les réverbères, disait-on, étant visibles des localités voisines, effrayent les
populations et, par contre-coup, inquiètent les observateurs eux- mêmes ; leur entretien
et leur surveillance exigent des soins particuliers qui en rendent l’emploi coûteux; ils
ne peuvent pas toujours être dirigés sûrement et commodément vers les stations d’où

ils doivent être observés. Enfin, pour les apercevoir à de grandes distances, il faut
19

 
