 

 

 

 

 

 

42

Herr Breton de Champ, Ober-Ingenieur des Brücken- und Wegebaues;
,, Jaquemin, Ober-Ingenieur des Briicken- und Wegebaues, Director der

Ostbahn ; .

Delesse, Ober-Ingenieur fiir den Bergbau, Präsident der Jury für die
geographische Ausstellung;

Baron Reille, General-Commissar des internationalen geograph. Congresses;

Maunoir, General-Secretair der geographischen Gesellschaft in Paris;

Mahmoud Bey, Director der Sternwarte in Cairo;

Govi, Mitglied des internationalen Comites für Maass und Gewicht;

Arrillaga, Abtheilungs-Chef im geographischen und statistischen Institut
in Spanien. |

29

99
29
29
29

29

Der Herr Präsident erklärt die Sitzung für eröffnet.

Herr Jourdain, Mitglied des Instituts, General-Secretair des Ministeriums des
öffentlichen Unterrichts u. s. w., nimmt das Wort im Namen des Herrn Ministers, welcher
durch einen bedauerlichen Unfall verhindert ist, der Sitzung, wie er beabsichtigt hatte,
beizuwohnen, um der geodätischen Commission zu danken, dass sie der Einladung der
französischen Regierung nachgekommen ist, und um derselben zu bekunden, mit welchem
Interesse man in Frankreich das von der Commission für die Europäische Gradmessung
unternommene wissenschaftliche Werk begrüsst. Frankreich, sagt der Herr General-
Secretair, bleibt seinen alten Traditionen getreu, indem es an Forschungen Theil nimmt,
welche es seiner Zeit hervorgerufen hat, und indem es sich an einem Unternehmen be-
theiligt, welches durch die Pflege der Wissenschaft und durch das Gefühl europäischer
Zusammengehörigkeit erweckt worden ist.

Der Herr Präsident antwortet, indem er der französischen Regierung für die
sympathischen Gesinnungen dankt, welche sie soeben der geodätischen Commission aus-
gesprochen hat; die Commission schätzt sich glücklich, in der Hauptstadt des Landes
zu tagen, welches man als die Wiege der modernen Geodäsie bezeichnen kann. — Bei
der Unterstützung, welche die Regierungen den Bestrebungen der Commission ohne Unter-
lass gewähren und bei der vortrefflichen Hilfe der Geodäten und Astronomen der ver-
schiedenen Länder kann man hoffen, dass die Geodäsie noch vor dem Ende unsers
Jahrhunderts eine Entwicklung vollständig erlangt haben werde, welche eben so wichtig
und noch bedeutender sein dürfte, als diejenige, welche der glänzenden Epoche des
18. Jahrhunderts zum Ruhme gereicht.

Herr General Baeyer fügt einige Worte hinzu, um an die unsterblichen Arbeiten
zu erinnern, welche Frankreich zur Bestimmung der Gestalt und Grösse der Erde aus-
geführt hat, und welche mehreren Generationen als Muster gedient haben. Herr Baeyer
schätzt sich glücklich, den Zeitpunkt erlebt zu haben, wo die wissenschaftliche Association,
welche er hervorgerufen hat, thatsächlich ganz Europa umfasst und wo man mit Zu-
versicht hoffen darf, dass das hohe Ziel unseres gemeinsamen Werkes erreicht werden wird.

 

F
=x
ä
=
=
=
a
=
i
=
=
x

LL

ak ini

 

2. esttaunes maniseté Si |
