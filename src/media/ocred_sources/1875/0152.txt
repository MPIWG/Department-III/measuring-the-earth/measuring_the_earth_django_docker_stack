 

 

 

Remarques concernant l'emploi des séries trigonométriques dans la représentation
des effets des attractions, et l'intégration de l'équation différentielle
des surfaces de niveau.

Les avantages des développements algébriques qui ont été proposés dans le
précédent Mémoire nous semblent assez évidents pour nous dispenser d’y revenir;
aussi ne le ferons-nous pas: nous voulons seulement examiner ici comment on arriverait
à vaincre certaines difficultés qui paraîtraient s’opposer à l’emploi des séries trigono-

métriques.

©

On a vu, dans l’avant-dernier Mémoire, comment le problème se simplifie
lorsque, au lieu de rechercher l’équation des surfaces de niveau, on se propose tout
d’abord de former l’equation d’un profil de ces surfaces; la détermination des divers
profils équivaut en effet à celle de la surface entière dont ils font partie. Nous avons
fait remarquer que, les stations astronomiques n’étant pas généralement échelonnées
le long du profil considéré, il serait nécessaire de déduire, par voie d’interpolation,
les valeurs des perturbations produites par les attractions locales, aux points du profil
que l’on se propose de substituer aux stations astronomiques. Nous n’avons pas indiqué
à cette occasion le mode d’interpolation auquel il conviendrait de recourir; or il est
clair que cette lacune se trouve comblée par les développements contenus dans le
dernier Mémoire. Supposons maintenant qu’on effectue les interpolations dont il s’agit
et que, de plus, les points substitués aux stations astronomiques soient équidistants,
tant dans le sens des méridiens que dans le sens des parallèles, et suffisamment
rapprochés pour, que toutes les inflexions de la surface de niveau puissent être
convenablement représentées; il n’est pas difficile de reconnaître que, dans le cas d’un
espace limité par des méridiens et des parallèles, les divers coefficients des développe-
ments trigonométriques pourront alors être obtenus, sans qu'il soit nécessaire de
résoudre l’ensemble des équations de condition. Dans ce cas, en effet, chacun de ces
divers coefficients s’obtiendra au moyen d’une intégrale définie double, qui se calculera
par des formules de quadrature convenablement appropriées au sujet.

+

Si, comme en réalité, l’espace considéré est limité par un contour irrégulier,
tel que celui des continents ou des frontières, la solution ne pourra être ramenée à la
précédente qu’en substituant aux valeurs inconnues des perturbations dans les régions
comprises entre les limites de la surface et les méridiens et parallèles circonscrits,
des fonctions arbitraires assujetties d’ailleurs à l'équation de condition relative à
lintégrabilité: les résultats obtenus ne s’appliqueraient évidemment qu'aux points
compris dans l’espace primitivement considéré. 7

 

ud dh

I

a Ad

ill

il

1

Lahn m

 

2.4 2a, menu ih |

 
