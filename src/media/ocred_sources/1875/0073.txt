|
|

 

pere tronr

&
=
=
&
S
he
=
=
=
B
5
è
&

63

nationalen Apparates die beste Lösung des Problems der Zukunft, welches darin besteht,
in die europäischen Netze Einheit zu bringen und alle Längen in identischen Einheiten
auszudrücken.

Wenn dies zugegeben wird, so ergiebt sich als natürliche Folge der Herstellung
eines internationalen metrischen Urmaasses, dass die Vergleichung des internationalen
geodätischen Maassstabes in dem internationalen Büreau für Gewichte und Maasse unter
der Verantwortlichkeit der Beobachter der verschiedenen Länder, welche die Grund-
linien zu messen haben, sowie unter Mitwirkung des internationalen Büreaus, ausgeführt
werden muss.

In Betreff der Wahl des Apparates hat Herr General Baeyer vorgeschlagen,
eine ähnliche Construction anzunehmen, wie bei demjenigen, welchen Herr Brunner für
die spanische Regierung gebaut hat, indem er hinzufügt, dass dieser Apparat allen
jetzigen Anforderungen der Wissenschaft genüge, da er aus einem aus zwei Metallen
zusammengesetzten Maassstabe und aus Mikroskopen mit Mikrometern bestehe, und bei
der Ausführung von Messungen Resultate von ausserordentlicher Schärfe geliefert habe.
Mehrere Mitglieder glaubten, obgleich sie diesen Vorschlag billigten, es sei vorzuziehen,
heute noch keine endgültige Entscheidung zu treffen. Um neuen Systemen Zeit zu
lassen sich zu bewähren, namentlich auch demjenigen, welches Herr Fiaye ausgedacht hat,
beschränkt sich die Commission auf Vorschlag des Herrn General Forsch darauf, zu
beantragen, dass dem Central-Büreau die Sorge überlassen werde, das System zu
wählen, welches den höchsten Grad von Genauigkeit liefert.

Der Kostenpunkt war in der Dresdener General-Conferenz noch nicht vorgesehen
worden; aus den Protokollen geht in der That hervor, dass es sich einfach darum handelte,
dass die Special-Commission für den Basis-Apparat das beste System zur Annahme aus-
suchte. Die Commission hat geglaubt, dass sie ihre Befugniss billiger Weise dahin ausdeh-
nen könne, den Vorschlag zu machen, dass dieser internationale Apparat auf gemeinschaft-
liche Kosten angeschafft werde, mit dem Vorbehalt, dass jeder Commissar der an der Grad-
messung betheiligten Staaten die Genehmigung seiner Regierung einholt. Der nächsten
General-Conferenz bleibt dann die Sorge überlassen, den Geldpunkt und andere Detail-
fragen zu ordnen, so auch die Bestimmung, wo der Apparat aufbewahrt werden soll.

Um einer Verzögerung von zwei Jahren vorzubeugen, erbietet sich Herr General
Baeyer im Namen des Central-Büreaus, die für baldige Erwerbung des internationalen
Apparates erforderlichen Geldmittel vorzuschiessen, ohne dass durch diese Erwerbung
den Repräsentanten der einzelnen Staaten für die Zukunft eine finanzielle Verantwort-
lichkeit auferlegt wird.

Kurz zusammengefasst schlägt die ernannte Special- Commission folgende Be-
schlüsse vor:

1) Es liegt im Interesse des gemeinsamen Werkes, dass für die europäische
Gradmessung ein Basis-Apparat auf gemeinschaftliche Kosten angeschafft werde.

2) Der Maassstab dieses Apparats muss mit dem internationalen Meter ver-
glichen werden.

 
