 

114

sa densit6*). Les membres de ladite Commission se sont montrés favorables à l'emploi
de théorèmes généraux sur les effets des attractions locales, théorèmes annoncés par
Yun deux, Mr. Schering, mais non encore publiés **). Constatons, en passant, que
nous avions nous-méme donné la préférence A ce mode d’investigation, en présentant
à l’Académie, en 1866, un théorème dont le théorème de Laplace est un cas parti-
culier. L’un de ceux que Mr. Schering a découverts est également une généralisation
du théorème de Laplace, et son savant auteur nous fait espérer une prochaine publi-
cation, dans laquelle il fera connaître en quoi son résultat diffère du mien **#). J'aurais
pu, dès 1866, indiquer un nouveau théorème, et je ne Vai pas fait, parce que la
possibilité de son application me paraissait encore éloignée. L'Académie voudra bien

 

#) P. S. La Commission s’est occupée tout particulièrement de la question: s’il conviendrait
d'entreprendre dans quelques points astronomiques principaux, ou même dans tous, des recherches sem-
blables à celles qui ont été entreprises dans plusieurs points astronomiques de l’arc mesuré en Angleterre,
où l’on a tenu compte des déviations locales a priori, en se fondant sur le relief et la densité du sol.
La Commission ne ‘croit pas pouvoir recommander ce procédé.... (Comptes rendus de la Conférence
géodésique internationale ..., réunie à Berlin du 30 septembre au 7 octobre 1867, p. 85 et 86.)

**) P. §. Nous extrayons du méme Recueil, pag. 86, les lignes suivantes:

Mr. Schering croit pouvoir insister particulièrement sur ces théorèmes (concernant les attractions
locales), parce qu'ils lui semblent ajouter de la force aux autres raisons qui militent pour l'importance
de la détermination, au même endroit, de toutes les trois coordonnées astronomiques, azimuts, latitudes
et longitudes, ainsi que pour l'utilité d’une distribution uniforme des stations astronomiques sur tout le
terrain mesuré trigonométriquement.*

„La Commission à accueilli avec reconnaissance ces Communications de Mr. Schering et elle
croit y voir un nouveau motif pour le principe établi déjà il y a trois ans, de faire les trois détermi-
nations astronomiques dans le plus grand nombre possible de points, uniformément distribués; mais la
Commission à dû se refuser à discuter ce sujet de plus près, parce que les théorèmes dont Mr. Schering
a parlé n’ont pas encore été publiés. Cependant la Commission a cru, dans l'intérêt de la cause, devoir
prier Mr. Schering de hâter cette publication autant que possible, et elle espère que la Conférence se
foindra à cette résolution de la Commission.“

wet) P. §. L’Association géodésique internationale m’a fait l’honneur d’insérer, dans son
General-Bericht pour 1866, plusieurs de mes Mémoires sur la Géodésie et, entre autres, le premier
théorème sur les attractions locales; maïs ce théorème n’a pas été mentionné dans la réunion de la
Commission chargée, en 1867, d’examiner la question des attractions locales. Cependant il y avait quelque
intérêt à comparer deux théorèmes que leurs auteurs considèrent comme des extensions du théorème de
Laplace. Ce desideratum a été signalé au général Baeyer, président de la Conférence, et j'ai reçu à
cette occasion les explications suivantes de Mr. Schering:

,-..Je m’empresse de vous assurer que ce n’est pas par oubli ou par ignorance que, dans
mon annonce de deux théorèmes géodésiques, je n’ai pas mentionné vos ......... travaux sur cette pro-
fonde théorie: c’est seulement à cause de limpossibilité où je me trouvais de marquer, sans entrer en
matière, la différence qui existe entre l’étendue de votre théorème et le mien, qui sont tous deux des
généralisations du théorème de Laplace ....

» - .. J'espère trouver bientôt le temps pour la publication de mes autres recherches géodésiques,
et je ne doute pas que vous ne soyez bien satisfait de la manière dont je témoignerai de ma vive estime
pour les grands progrès tout récents et bien connus dont cette science vous est redevable. ...

,... L'intérêt commun pour le progrès des sciences vous fera pardonner si mon peu d'usage
de m’exprimer en francais m’empéche de vous dire aussi bien que je le désire, combien j'estime votre
sagacité bien nécessaire pour appliquer tous les secours que la science peut offrir à la détermination de
la surface géodésique, si irrégulière, et combien je me sens obligé par la bienveillance avec laquelle
vous traitez la discussion d’un cas concernant l’histoire de la science.‘

 

=

pe LU LL EL CITE LILI

Mm

ttl

ll

A mt

 

oiahttin te, man AA |

 
