 

|
Ë
}
5

6

Lisbonne; on a acquis tous les instruments nécessaires, et on a enfin adopté les
méthodes modernes d’observation et de calcul.

Les chaines choisies, étant formées, autant que possible, par des quadrilatéres
contigus avec deux diagonales, sont au nombre de trois: l’une, se rattachant dans la
Galice et dans l’Algarve à la chaîne espagnole du littoral, suit aussi, à peu pres,
le littoral portugais; elle a de plus lavantage de s'étendre presqu’en totalité dans la
direction du méridien de Coimbra; les deux autres peuvent être considérées comme la
continuation de celles des parallèles de Madrid et de Ciudad-Real. Les espaces
compris par les chaînes sont aussi couverts de triangles du 1% ordre, dont les angles
peuvent être mesurés avec un peu moins de précision, en profitant dans beaucoup de
cas d'anciens travaux; mais quant à la mesure des angles des 3 chaînes fondamentales,
on agit avec toute la rigueur qu’exigent les progrès de la science.

On a envoyé à Mr. Hirsch, secrétaire de la Commission permanente, une copie
de ces projections ainsi qu'une autre relative aux nivellements de précision, desquels
nous rendrons compte toute a Vheure.

Mesure des bases. Nous ne parlerons pas des anciennes mesures exécutées par
le Docteur Ciera, parceque, dans le rapport de 1868, se trouvent les indications
necessaires sur ces travaux, ainsi que les causes qui ont amené leur abandon.

Notre base d’opérations est encore celle qui a été mesurée par le Général
Filippe Folque entre Batel et Montijo avec un ancien appareil de règles en bois du
Brésil, imaginé par le Docteur I Monteiro da Rocha, #ilustre astronome. L'unité
fondamentale adoptée dans cette opération fut la brasse nommée ,,braça de Ciera‘ qui se
trouve représentée sur une barre de fer forgé, sa longueur y étant indiquée par la

‘distance entre les centres de deux petits cercles en or, incrustés dans la brasse. Cet

étalon existe parfaitement conservé, et sa relation avec le mètre est:

1 brasse de Ciera — 2,1980 mètres, à la température de 25° C.;
mais ce chiffre ne peut servir que pour donner une relation approximative entre notre
étalon et le mètre français. La brasse de Ciera a été employée dans tous les calculs de
haute géodésie, comme c'était naturel.

La description des règles et de Pétalon de la brasse a été publiée dans les
Mémoires du Général Filippe Folque sur les travaux géodésiques du Portugal. On y
voit que la base Batel— —Montijo, étant mesurée dans les deux sens, le second mesurage
accuse la différence de O 02077, la longueur définitive étant de 4787? 9412.

Quoique cette différence soit très petite en raison des moyens employés
(ce qui prouve l’habileté et le grand soin des observateurs), elle n’est pas d'accord,
et ne pouvait pas l'être, avec les surprenants résultats qu’on obtient au moyen
des appareils modernes; et c’est pour cela que la valeur de la base telle qu’elle
est déterminée, ne peut s’employer dans les grandes opérations géodésiques actuelles.

Eu égard à ces circonstances, le gouvernement portugais à acquis dans les
teliers de Repsold & SGhne, de Hambourg, un appareil pour mesurer les bases.

 

x

=
=
=

Wadia | Mite

cai) ie

1 tet

al

 

2. 2emmmieaninenn, masse DA |

 
