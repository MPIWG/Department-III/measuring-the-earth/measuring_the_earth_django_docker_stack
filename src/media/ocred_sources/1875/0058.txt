 

 

Der Präsident dankt Herr General Baeyer und den Schriftführern für diese
Berichte. Er schlägt vor, auf morgen die Sitzungen der beiden Speecial- Commissionen
anzuberaumen, welche mit der Behandlung der Fragen über die Basis- und Pendel-
Apparate betraut worden sind, damit sie in einer der nächsten Sitzungen V orschläge
machen können. Er setzt die Sitzung der Commission für die Basis-Apparate auf
Dienstag 10 Uhr Vormittags und die der Commission für das Pendel auf 2 Uhr N
mittags an. Alle Mitglieder der Versammlung, welche sich für diese Fı
werden eingeladen, diesen Berathungen beizuwohnen.

Die nächste Sitzung der permanenten Commission wird auf Mittwoch 2 U
festgesetzt; sie soll der Mittheilung der Berichte der Commissare über die in den ver-
schiedenen Ländern ausgeführten Arbeiten gewidmet sein.

Schluss der Sitzung 2'/, Uhr.

ach-
agen interessiren,

hr

| - Zweite Sitzung,

Paris, den 22. September 1875.

Eröffnung der Sitzung um 2 Uhr 30 Minuten.
Gegenwärtig sind von den Commissaren: die Herren Adan, Baro
feind, Druhns, Faye, Ferrero, von Forsch, Hirsch, Ibanez, von Oppolzer, Perrier, Peters,
Ricci, Saget, de Vecchi, Villarceau, und von den Gästen: die Herren Bassot, Banderali,

Billot, Bouquet de la Grye, Breton de Champ, Bugnot, Delesse, Laussedat, Mahmoud
Bey, de la Roche-Poncié.

Präsident: Herr Ibanez.

Schriftführer: die Herren Bruhns und Hirsch.

Das Protokoll der ersten Sitzung wird vorgelesen und genehmigt.

Tagesordnung : Berichte der Commissare der verschiedenen St

Herr General Baeyer lässt sich entschuldigen, dass er der
wohnen kann.

Herr Hirsch vertheilt im Namen des Herrn Baeyer eine Abhandlung: ,,Ueber
Fehlerbestimmung und Ausgleichung eines geometrischen Nivellements.“

Auf Ersuchen des Herrn Präsidenten erstatten nach einander die Herren
v. Oppolzer, v. Bauernfeind und Adan die Berichte über
Belgien ausgeführten Arbeiten. ”)

Nachdem der Herr Präsident den Commissaren für il
hat, tritt er den Vorsitz an Herrn ». Bauernfeind ab.

Der Präsident bittet Herrn General Iba

221, von Bauern-

aaten.
Sitzung nicht bei-

die in Oesterreich, Bayern und
ire Mittheilungen gedankt

fez den Bericht tiber Spanien vorzutragen.

*) Vergl. den Generalbericht, in welchen diese Berichte aufgenommen sind.

    

vey) iam dm A a Rm Dated: 11

I a

 
