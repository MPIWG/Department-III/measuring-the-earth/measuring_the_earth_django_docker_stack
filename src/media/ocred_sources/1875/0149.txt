a ee

 

mu ART

pit

UTR I Da we

[

MT pit

139

Ce résultat nous a conduit à remplacer les latitudes et longitudes par les arcs
de méridien et de parallèle.

Soient: m l'arc de méridien compris entre les latitudes L et L,, p Vare de
paralléle a la latitude L, compris entre les méridiens de longitudes feb £5 nou
aurons : :

(HO) SO tn f RAL), pe E(k = £5)
d’où
(60) <5 dt Re ip Cdl (ee)

et, en ayant égard aux relations (53) et (59),

(OL) sn ae ee a de sinL.RaL:

IS

Au moyen de ces relations, jointes à la seconde équation (57), on éliminera
aisément ® d£ et RdL de l’équation (50), et l’on aura
(82)... | = si = (prod ho
L’öquation de condition relative à l'intégralité de celle-ci peut s’écrire

du a ( des

(63) jae ee ae | a == .6 1p : ).

dp
Pour intégrer actuellement léquation (62), nous allons former les expressions
de u, & et €, suivant les puissances et produits de m et de p.
Posons en conséquence
B—®,; +ap—Lbm+cp? +epm+fm?+gp + hp?m
+ kom? + ms + ...,
Cae u = au; fa pth m+cp?+e pm+i m+ p> +h pm
+k pm@+ lm +...,
(= 109i epee

nous aurons successivement

*) P. §. La valeur de cette intégrale se déduira des Tables géodésiques qui donnent les arcs
de méridien comptés de l’équateur, au moyen d’une simple soustraction.

#*) P. §. On peut, en suivant une autre voie, établir la relation (62): par exemple en
; : da da aa os ee
exprimant les dérivées (52) et apt on des dérivées de A relatives à L et £, et vérifier que

l'équation de condition (63) s'accorde avec l'équation (58). C’est en vue de présenter ce mode de
démonstration que nous avons compris sous une parenthèse la dérivée de & par rapport à m; toutefois

nous avons jugé superflu d’insister davantage sur ce point.
18*

 
