 

152

Sterne, ferner aus Durchgangsbeobachtungen von 6 Sternen durch den ersten Vertikal
bestimmt. Azimuthe wurden 15 Sätze (zu 4 Einstellungen) mit der Richtung Lisa hora
und 6 Sätze mit der Richtung Hurky gemessen.

B. Sanct Martin bei Tarnow in Galizien. Hier wurde die Polhöhe aus
27 Sätzen Zenithdistanzen nördlicher und 18 Sätzen Zenithdistanzen südlicher Sterne,
dann aus Durchgangsbeobachtungen von 6 Sternen durch den ersten Vertikal bestimmt
und 15 Sätze Azimuth mit Wal, 6 Sätze mit Liwocz gemessen.

Ü. Zmieszenie bei Przemysl in Galizien. Die Breite dieses Punktes wurde aus
30 Sätzen Zenithdistanzen nördlicher, 32 Sätzen Zenithdistanzen südlicher Sterne, dann
aus Durchgangsbeobachtungen von 6 Sternen durch den ersten Vertikal bestimmt und
12 Satze Azimuth mit Kopistanska, 12 Sätze mit Radimno gemessen.

D. Grzimalow mogila bei Grzimalow in Galizien. ‚ Auf diesem Punkte wurde
die Breite durch Messung von 46 Sätzen Zenithdistanzen nördlicher, 45 Sätzen Zenith-
distanzen südlicher Sterne, ferner durch Beobachtung von 8 Sternen im ersten Vertikal
bestimmt und 18. Sätze Azimuth der Richtung Kobilovloky gemessen.

E. Nördlicher Basis- Endpunkt bei Kranichsfeld in Süd-Steiermark. Die
Messungen auf diesem Punkte konnten in Folge der anhaltend schlechten . Witterung
des vorjährigen Spätherbstes nicht ganz zu Ende geführt werden und sind noch einige
Sätze Zenithdistanzen und Azimuthe nachzutragen; dagegen sind die Beobachtungen
im ersten. Vertikal vollendet.

2) Triangulirungen.

A. In der Bukowina und in Ost-Galizien. — Hier wurde eine Polygonkette,
von der im Jahre 1874 gemessenen Radautzer Grundlinie ausgehend, bis nach Lemberg
geführt und dort an das in den Jahren 1848 und 1849 gemessene, westlich von Lemberg
gelegene Netz angeschlossen. Zugleich wurden die bei Lemberg und Czernowitz gele-
genen astronomischen Stationen erster Ordnung mit dem Hauptdreiecksnetze verbunden.

B. In West-Galizien wurde eine Polygonkette von dem eben erwähnten in den
Jahren 1848 und 1849 gemessenen Netze nach Westen bis an die schlesische Grenze
und eine zweite Kette nach Süden zum Anschlusse an die Triangulirung des königlich
ungarischen Katasters geführt.

C. Schlesien, Mähren, Niederösterreich und Steiermark. — Dieses Polygon-
netz beginnt an der schlesisch-galizischen Grenze, wo es an die sub B. erwähnte Arbeit
anschliesst, und zieht sich längs der Karpathen über Wien nach Wiener Neustadt
(wo sich eine im Jahre 1857 gemessene Grundlinie befindet) und von da nach Ober-
Steiermark. Auf einigen der in Niederösterreich und Mähren liegenden Punkten dieses
Polygonnetzes ist schon in früheren Jahren beobachtet worden.

3) Basis-Messung.
Die bei Kranichsfeld (unweit Marburg in Siid-Steiermark) im Jahre 1860
gemessene Grundlinie enthielt in ihrer südlichen Hälfte einen grösseren Fehler; es war
nämlich damals gefunden worden:

 

x

=
=
=
=
a
ü
x

IM

Im

al

 

2.1 2nmmamantcinnnn mise NA |

 
