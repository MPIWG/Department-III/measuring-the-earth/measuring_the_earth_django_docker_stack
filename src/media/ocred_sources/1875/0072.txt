 

 

62

Diese vier Anträge wurden nach einander zur Abstimmung gebracht und ange-
nommen.

Herr General Ricei bittet um das Wort, um daran zu erinnern, dass im Jahre
1869 die permanente Commission den Wunsch ausgesprochen hat, dass die vom Herrn
Prof. Govi für die Messung der Länge des einfachen Pendels vorgeschlagene Methode
in Italien versucht werde, was in Folge der Ereignisse seit jener Zeit bisher nicht
möglich gewesen ist. Der General frägt an, ob die Commission noch immer derselben
Ansicht ist, dass diese Methode durch einen Versuch geprüft werde?

Der Präsident erwidert darauf dem Herrn Ricci, dass der im Jahre 1869 von
der permanenten Commission ausgesprochene Wunsch noch immer derselbe sei, und
dass dieselbe sich freuen würde, ihn verwirklicht zu sehen.

Herr Perrier, Berichterstatter der Special-Commission für die Basis-Apparate,
liest folgenden Bericht vor:

In der dritten Sitzung der vierten allgemeinen Conferenz, am 25. September
1874 ist eine aus sechs Mitgliedern bestehende Commission dem Herrn General Baeyer
beigegeben worden, um zu untersuchen, welches die beste Construction für den Basis-
Apparat sein dürfte, und möglichst bald der permanenten Commission motivirte Vor-
schläge zu unterbreiten.

Diese Special-Commission, deren Mitglieder, mit Ausnahme des am Erscheinen
verhinderten Oberstem Ganahl , in Paris gegenwärtig waren, hat sich am 21. und
23. September unter Vorsitz des Herrn General Baeyer versammelt.

Nach kurzer Besprechung ist einstimmig anerkannt worden, dass es nützlich
und im wahren Interesse der Gradmessung zweckmässig wäre, wenn für die Messung
der geodätischen Grundlinien ein internationaler Apparat angeschafft würde. Ein solcher
Apparat wird in der That, wie Herr Hirsch auseinander gesetzt hat, zur Messung der
Grundlinien in kleineren Ländern, welche nicht mit einem besonderen Apparat versorgt
sind, dienen können und so gestatten, dass mehrere erhebliche Lücken in den euro-
päischen Dreiecksnetzen ausgefüllt werden. Er wird überdies den grossen Vortheil
darbieten, dass Grundlinien , welche bereits gemessen worden, nachgemessen werden
können, und folglich die Vergleichung der bereits mit den Apparaten verschiedener
Länder gewonnenen Resultate mit denjenigen ermöglichen , welche der internationale
Apparat liefern wird, woraus, wenn man sich so ausdrücken darf, die Gleichung der
Grundlinien der verschiedenen Länder hervorgeht.

Es handelt sich nicht darum eine neue Construction aufzunöthigen, welche
dazu bestimmt wäre, die schon vorhandenen zu verdrängen. Es geben ja, wie die
Herren de Vecchi, Faye und Perrier bemerken, alle heutigen Apparate thatsächlich
Resultate von einer hohen Genauigkeit; aber wenn es bei dem gegenwärtigen Stande
der geodätischen Arbeiten in Europa zur Vergleichung zweier benachbarten Dreiecks-
netze genügt, dass eine gemeinschaftliche Grundlinie mit den Maassstäben der beiden
Länder. gemessen werde, wie es neuerdings die italienischen und österreichischen
Offiziere bei der Basis von Udine gethan haben, so gewährt die Existenz eines inter-

 

x

a TE TE Teen

ak nie

to wi

Wi

ch a ald ai ddl sual ei

2. saussttuns memes DEA |

 
