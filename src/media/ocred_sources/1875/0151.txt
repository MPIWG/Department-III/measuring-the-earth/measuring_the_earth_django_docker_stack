Ihe we em er

 

IT RT

m

FE N a

ne

Péquation (62) deviendra

CS oe —adA=Mdm-+Gdp.
On verifiera aisement que la condition a = (à est satisfaite.
dp am?

?

En appliquant à l'intégration de l’équation (68) le mode de calcul qui a été
suivi à l’égard de l’équation (40), on obtient finalement *)

(Qi A) =aiptum+ ar topmt Pim + I cp

, 1
(Oo). aE erm tipm® + tm tt gp

et DD] et

1 1
+ heim + kp tt pm $I u

Ce développement, poussé jusqu'aux termes du quatrième ordre, sera sans
doute suffisant, lorsque les distances en longitude et latitude des stations au point
central ne dépasseront pas 1}, à 2 degrés. Dans tous les cas, la résolution des

équations (64) et (66) permettra toujours de limiter les termes des développements au
nom bre convenable.

Les formules (47) ou (69) nous paraissent résoudre le problème des surfaces
de niveau de la manière qui se prête le mieux aux exigences de la pratique.

Dans une autre Communication, nous examinerons l’influence que pourrait
avoir sur les résultats la correction des éléments du calcul des positions géodésiques.

*) P. S. La formule d’intégration de l’expression précédente est, en désignant par A, la

constante,
a / dM
— — & == æ — ——d d .
a (A —A,) fans f| af? m )dp

Voici le détail des calculs :
: aM :
dp ~ Pep atin + hp* + 2kpm + 31m —...,

‘dM
[ij ma dmb epmb in? +h pmb k pm tint.

J dp
’dM

@ — |, (Ger er en
e p

; “dM 1 1 1

f G = f i dm) Op OP io WP Ea CR ee CP

3 1 1 watt ay
[man um+ bpm+ > mem Tran rt m?

x le 1
+ 3 kp°m + 9 KD°m° + 1 pm° + 4 lime + ...;
on en déduit la formule (69) du texte.

 
