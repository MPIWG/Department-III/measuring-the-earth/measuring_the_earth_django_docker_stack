TRES

 

ini AW

ara VB RE a!

x

 

175

Pendant 12 soirées, du 25 Décembre 1874 au 11 Janvier 1875, j’ai observé les
distances zénithales méridiennes de 46 étoiles, comprises en ascension droite entre
gp de Pégase et o de Persée, culminant en nombre égal au Nord et au Sud du zénith
et à moins de 25 degrés de distance. Les positions apparentes ont été prises dans le

catalogue publié chaque année par la Rédaction des Ephémerides astronomiques de

Berlin; la plupart des astres observés sont d’un ordre inférieur au 3ème; trois d’entre eux
seulement sont de l’ordre 2 ou 2.3.

Parmi les 12 séries, six ont été faites dans la position directe du cercle et les
six autres dans la position inverse, les deux séries de chaque couple correspondant à
un même calage au Nadir, les six calages successifs étant équidistants sur le limbe et
l’observateur ayant eu soin de prendre deux positions différentes, face au Sud et face
au Nord, dans l’observation du Nadir pour les deux séries d’un même couple.
J’ai ainsi obtenu 419 valeurs de la latitude pour l’observatoire d'Alger.
La moyenne générale donne: . £ — 36452775
La moyenné des 12 séries: . . £ -86 27
Les séries faites cercle à l'Est f £ — 3645 5.11 |
cp.» à cercle à l’Öuet le 86 45 2 4

Difference 10,01
Ce résultat prouve combien il est important d'observer dans les deux positions
du cercle.
Si on ne considère que les 12 étoiles observées dans les 12 positions du cercle,

elles fournissent la valeur en 36049 2778.

Six de ces étoiles culminent au Nord et six au Sud du zénith; les premières
donnent, pour la latitude 276 et les autres 2780.

Ces 12 étoiles permettent de calculer l’erreur moyenne de la latitude fournie
par l’une d’elles; on trouve ainsi E — + 0/51 et pour l’erreur moyenne de la moyenne
Ei ==. Ope

L’erreur moyenne d’une observation isolée de la latitude est égale à

pe e
ENVI = à Lot
En considérant comme une observation complète la moyenne des deux séries
de chaque couple, on obtient six résultats d’une concordance remarquable.
36045'2.15
2.65
2.85
2.66
2.76
2:85
