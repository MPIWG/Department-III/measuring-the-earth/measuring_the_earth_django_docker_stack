ee

 

an LH

Pr Beh RL ji

a

187

wurden probeweise vorläufige Stichproben durchgeführt, die überall befriedigende
Resultate gaben, so dass ein vollständiges Gelingen aller Operationen mit einem hohen
Grade von Wahrscheinlichkeit schon jetzt erwartet werden darf.

Wien, 28. December 1875.

Berieht über die von Prof. Dr. W. Tinter im Jahre 1875 ausgeführten
Gradmessungsarbeiten.

Die mir für die praktischen Arbeiten zum Zwecke der Gradmessung zur
Verfügung stehende Ferialzeit habe ich zur Bestimmung der Polhöhe und des Azimuthes
einer Richtung auf der Station Krakau benützt. Der eigentliche Punkt, auf welchen
sämmtliche Beobachtungen dieser Station zu beziehen sein werden, ist der Meridiankreis
der Sternwarte in Krakau. Das schon im Jahre 1873 zum Zwecke der Längenmessungen
aufgebaute Observatorium in dem die Sternwarte umgebenden botanischen Garten
wurde zu den Beobachtungen für die Polhöhe benützt; auf dem nördlichen Pfeiler
wurde das Universalinstrument, auf dem südlichen Pfeiler zum Zwecke der Beobachtung
von Sterndurchgängen im ersten Vertikal das Passageninstrument aufgestellt; beide
Instrumente sind der k. k. Gradmessung gehörig und von G. Starke gebaut.

Von Uhren benützte ich eine Pendeluhr von Kessels und einen Chronometer
von Weichert: erstere gehört der Sternwarte, letzterer dem k. k. militair-geographischen
Institute. Das Azimuth sollte von der Richtung nach dem Wandahügel gemessen
werden; es konnte dieses nur von einem hoch gelegenen Punkte der Sternwarte
geschehen. Herr Direktor F. Karlinski gestattete mir die Aufstellung des der Stern-
warte gehörigen achtzölligen Universalinstrumentes zum Zwecke dieser Beobachtungen
unter der Kuppel des Aequatoriales. Es erforderte dieses einen noch höheren Aufbau
des Pfeilers, um über den Rand der Kuppel nach dem Punkte auf dem Wandahügel

sehen zu können.
Polhöhenbestimmung.

Die Polhöhe wurde nach drei Methoden bestimmt:

1) Durch die Messung von Zenithdistanzen des Polarsternes;

2). durch die Messung von Circummeridianzenithdistanzen nérdlicher und
südlicher Sterne;

3) durch die Beobachtung von Sterndurchgängen im ersten Vertikal.

Ad 1) Der Polarstern wurde in 30 Sätzen, von denen 15 auf die obere und
15 auf die untere Culmination zu reduciren kommen, beobachtet. Je 12 Einstellungen,
und zwar 6 in jeder Kreislage, bilden einen Satz. Die Verstellung des Kreises erfolgte
regelmässig von 12 zu 12 Grad. Der Polarstern wurde daher bei 360 Einstellungen

beobachtet.

24*

 
