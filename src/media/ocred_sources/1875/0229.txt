ee

 

u AR

OR mn 10

{

x

 

219

lac, faites sur ces 6 limnimètres, à un seul et même horizon. En faisant ce calcul
pour les indications des limnimetres pendant tous les jours de plusieurs années,
Mr. Plantamour a trouvé par ces moyennes que, tandis que trois de ces échelles
donnent pour le niveau du lac une pente presque insensible de l'extrémité supérieure
jusqu'à 11}, kilomètres en amont de la ville de Geneve, les lectures s’accordant entre
elles dans les limites de l’incertitude, deux des limnimetres, placés dans la partie inter-
médiaire, indiquent une dénivellation de la surface de l’eau sensible et constante
pendant toute l’année. Le niveau du lac est de 2em trop élevé à Genthod et de
2m trop bas à Ouchy, quantités qui dépassent considérablement, de 3 à 4 fois, leur
incertitude. Je m’abstiens pour le moment de toute tentative d'expliquer un fait aussi
étrange, avant d’avoir établi, par de nouvelles opérations de contrôle, sa réalité d’une
manière absolue.

Pour revenir à nos nivellements, nous avons, dans le courant de la campagne
actuelle, achevé d’abord le double nivellement du polygone oriental, pour lequel le
calcul de réduction a donné une clôture satisfaisante, de sorte que notre jonction avec
les nivellements allemands autour du lac de Constance peut être envisagée comme
réalisée. Ensuite nous avons fait niveler la ligne du Rhin entre Steckborn—Schaffhouse
et Stein, le long de laquelle nous avons rattaché également notre nivellement à plusieurs
repères allemands. A cette occasion je crois devoir mentionner que, notre ingénieur
ayant dû dans ces opérations passer sur territoire badois pour quelques kilomètres, il
a fallu recourir à l’intervention diplomatique, pour lui procurer la permission nécessaire;
il serait à désirer que les autorités des Etats associés pour la mesure des degrés en
Europe accordassent plus de facilités aux ingénieurs de leurs voisins dans des occasions
pareilles. — De son côté, l'ingénieur du Bureau Central, Mr. le docteur Bôrsch, est
venu cet été exécuter la jonction avec nos repères suisses à Bâle et à Constance. À
cette occasion Mr. Börsch s’est rendu à Berne, pourvoir comparer les mires allemandes
avec notre étalon de 3 mètres qui a servi à la comparaison de toutes les autres mires,
par exemple aussi de celles qui vont être employées en Roumanie.

A. Hirsch.

 

Spanien.

Rapport sur l'état des travaux géodésiques poursuivis par l'Institut géographique et
statistique d'Espagne.

Pour donner une idée précise de l’état d'avancement de nos travaux géodésiques
du 1% ordre, je commencerai par mentionner ceux qui ont été accomplis pendant la
dernière année, depuis le rapport que j'ai eu l’honneur de faire à la Conférence de

Dresde, et puis je ferai un bref résumé de tous les travaux déjà terminés.
287
