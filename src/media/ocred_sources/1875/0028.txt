 

 

 

18

3° En ce qui concerne la question des cartes, certains pays ont envoyé leurs
cartes de triangles et de nivellement; — mais ces cartes, outre qu’elles ne sont pas au
complet, ne sont pas comparables.

Mr. Hirsch propose qu’on adopte un type et qu’on prie les Etats de se con-

former à ce type. — En tous cas la publication de cette carte présente de grandes
difficultés: si l’échelle est trop petite, on ne verra rien; — si elle est assez grande,

la carte, qui embrasse tout Europe, aura des dimensions peu pratiques: il faudra pro-
bablement en arriver à publier cette carte d'ensemble par sections.

Mr. Hirsch demande que cette 3e question soit également renvoyée au Bureau
Central, pour qu’il complète d’abord les données, en se procurant les cartes de la part
des pays qui n’en ont pas encore envoyé.

La discussion s'ouvre sur les formulaires proposés par Mr. Hirsch. — Mr.
dOppolzer propose d'ajouter au formulaire des latitudes une colonne spéciale pour in-
diquer les étoiles employées pour la détermination des latitudes: Mr. Perrier demande
que ces étoiles de latitude soient désignées, non seulement par leur N°. de catalogue,
mais aussi par leur déclinaison moyenne au commencement de l’année courante.

Ces deux propositions sont adoptées.

Mr. Perrier demande que, dans le tableau, on mentionne, à côté des coordon-
nées astronomiques, les coordonnées géodésiques.

Il demande également que le catalogue bibliographique se borne à mentionner
les ouvrages ayant trait directement à la mesure des degrés.

Mr. Villarceau croit inutile de mentionner l'erreur probable dans les déter-
minations des coordonnées astronomiques; il demande que les astronomes français ne
soient pas astreints à remplir cette colonne du formulaire.

Mr. Hirsch répond à Mr. Villarceau que la décision de la Commission n’obli-
gerait pas à remplir le formulaire en entier, et que si, en France, on ne calcule pas
l'erreur probable, ce qui, dans son opinion, serait bien regrettable, on ne l’indiquera
pas pour ce pays, tandis qu’on le fera pour tous les autres.

Mr. Hirsch. répond à Mr. Perrier que les coordonnées astronomiques et les
coordonnées géodésiques seront publiées dans des tableaux séparés, dont la comparaison
sera facile. — Quant au catalogue, il ne voit pas d’inconvénient à ce qu'il mentionne
quelques ouvrages anciens qui seraient en dehors de la lettre du programme: il vaut
mieux pécher par excès que par défaut.

Mr. Perrier demande que, dans le formulaire des latitudes, on indique le mois
ou la saison où l’observation a été faite.

Mr. d’Oppolzer ne croit pas à l'utilité de cette mention.

Mr. Perrier maintient sa demande; Mr. Villarceau Vappuie: — elle est adoptée.

Mr. Faye véclame contre Vopinion qu’on semble avoir, que le calcul des pro-
babilités est négligé en France: il n’a qu'à mentionner les beaux travaux des Géomètres
français sur ce point: quant à lui personnellement, il préconise et professe la théorie
des probabilités.

 

à

/

MoM ied |) Me a

1 1884111184

vo a

m

11

1

LM tube

 

2. s20méitéttnnt, masitts vu |

 
