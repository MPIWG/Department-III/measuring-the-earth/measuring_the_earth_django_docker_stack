 

 

a

B. Astronomische Arbeiten.

Die Conferenz, welche im vergangenen Jahre in Dresden versammelt war, hat
den Arbeiten über die Loth- Ablenkung, welche wir im Harze ausgeführt haben, ein
solches Interesse geschenkt, dass ich geglaubt habe, einen allgemeinen Wunsch zu er-
füllen, wenn ich in diesem Jahre alle zu Gebote stehenden astronomischen Kräfte zur
Erforschung dieser Ablenkungen verwendete. Das Central-Büreau besitzt zur Polhöhen-
Messung nur zwei Instrumente, ein 13zölliges Universal-Instrument von Pistor und
Martins und ein Passagen-Instrument. Da indessen Herr Professor Bruhns ein anderes
eben solches Instrument, welches seiner Sternwarte gehört, zu meiner Verfügung gestellt
hat, so konnte ich zu jenen Untersuchungen drei Instrumente verwenden. Nachdem Pro-
fessor Albrecht seine beiden Assistenten Dr. Löw und Richter installirt hatte, hat er
selbst mit dem Passagen-Instrumente beobachtet. Anfang September hatten diese Herren
auf 14 Stationen die Breite gemessen. Um das Programm dieses Jahres zu vervoll-
ständigen, bleibt nur noch die Station Hercules bei Cassel übrig, wo Herr Albrecht
Polhöhe und Azimuth misst, und die beiden Stationen Dolmar und Heldburg im Siiden
des Thüringer Waldes zu erledigen. Die vorliegende Karte giebt die Lage aller dieser
Stationen und die vorläufig berechneten Werthe der Loth-Ablenkungen an.

©. Nivellitische Arbeiten.

Das Nivellement, welches im vergangenen Jahre von Hannover über Rheine bis
Salzbergen an der Niederländischen Grenze geführt worden war, ist in diesem Jahre
von Hannover über Düsseldorf bis Venloo fortgesetzt worden, wo es eine zweite Ver-
bindung mit dem von Amsterdam ausgehenden niederländischen Nivellement finden wird.

Von Düsseldorf aus ist auf dem linken Rhein-Ufer über Cöln und Coblenz bis
Mainz und von da einerseits nach Frankfurt, andernseits nach Ludwigshafen und Mann-
heim nivellirt worden. Auf diese Art erhält man zwei grosse Polygone.

1) Das erste von Frankfurt über Cassel, Hannover, Düsseldorf, Cöln, Mainz

zurück nach Frankfurt hat einen Umfang von 884m.

2) Das zweite von Frankfurt über Mainz, Ludwigshafen, Friedrichsfelde zurück

nach Frankfurt hat einen Umfang von 192 in,

Durch die Fortsetzung des Nivellements von Ludwigshafen über Landau nach
Basel, wo die Verbindung mit dem Schweizer Netze erhalten worden ist, hat man ein
drittes grosses Polygon gebildet: Heidelberg, Mannheim, Landau, Basel, Carlsruhe,
Heidelberg, in welchem mehrere Diagonalen nivellirt sind.

Der neue registrirende Pegel, welcher seit zwei Jahren nach den Entwürfen
und unter Leitung des Herrn Ingenieur Reitz in Hamburg gebaut wird, und welcher
nicht blos die Höhen-Curve aufzeichnen, sondern zugleich auch die Mittel bilden wird,
ist nahezu fertig.

Mitte Juli bin ich mit Herrn Major Ferrero, Chef der trigonometrischen Ab-
theilung des militär-geographischen Instituts in Florenz, nach Hamburg gereist, um

 

x
=
=
=
à
a
>
a
=
=

Tin a

ak nk

“rt mnt

jt al Mn na Hdi A thai tnt nn 1
