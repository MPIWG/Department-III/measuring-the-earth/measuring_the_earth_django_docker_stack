per

t
}
&
8
k
t

TP NN "TOT NT nn

51
verknüpft, wenn man nicht das Mittel der Pegelangaben, sondern das Mittel der Meeres-
höhe erhalten will, muss man da nicht die erhaltenen Resultate wegen der verschiedenen,
sehr merklichen Störungs-Ursachen verbessern ?

Wenn der Westwind an einem Punkte das Meer um 30°" und an einem anderen
benachbarten nur um 10™ erhebt, muss man da nicht bei der Vergleichung diese
Ziffern auf ein und dieselbe Einheit zurückführen, um sich gegen die wahrscheinlichen
Schwankungen des meteorologischen Zustandes zu schützen ?

Wenn einer der Punkte in der Nähe eines Flusses gelegen ist, welcher dem
Pegel seine süssen Gewässer zusendet, wird es da nicht in dieser Beziehung eine
andere schr bemerkenswerthe Störung geben? sie kann in der That bis auf 3% für ein
Meter steigen.

Endlich ändert sich mit der Zusammensetzung des Meerwassers oft die Temperatur
und die Ausdehnung, Ursachen, welche, wenn man nur Wasserschichten von geringer
Dicke betrachtet, als unbedeutend zu betrachten sind, welche aber eine merkliche Wirkung
ausüben können, wenn man die Verbindungen zwischen tiefen Meeren untersucht.

Mit einem Worte, die Frage wird studirt, die gesammelten Materialien sind
zahlreich und man kann hoffen, dass dieses Studium das nächste Jahr beendet werden
wird, was bereits von dem Studium über die Aenderung der Flutheurve an der West-
kiiste von Frankreich gilt.

Herr Villarceau fügt einige Worte hinzu und stellt mehrere Fragen über ver-
schiedene Punkte des Berichtes von Herrn Perrier; unter andern bestätigt er das, was
Herr Perrier über die Vortrefflichkeit der Arbeiten des Capitän Roudaire gesagt hat.

Der Präsident dankt den französischen Commissaren für ihre Mittheilungen.

Die Fortsetzung der Berichterstattungen wird auf den folgenden Tag, Donners-
tag den 23. September 2 Uhr anberaumt.

Die Special-Commission der Basis-Apparate wird am folgenden Tage um 10 Uhr
im Hôtel Voltaire bei Herrn General Baeyer tagen.

Schluss der Sitzung um 4'} Uhr.

Dritte Sitzung.

Paris, den 23. September 1875

Anfang der Sitzung 2 Uhr 30 Minuten

Anwesend von den Commissaren die Herren: Adan, Baeyer, Barozzi, v. Bauernfeind,
Bruhns, Faye, Ferrero, v. Forsch, Hirsch, Ibanez, v. Oppolzer, Perrier, Peters, Ricci, Saget,
de Vecchi, Villarceau; von den Eingeladenen die Herren: Bassot , Billot, Bouquet

de la Grye, Breton de Champ, Govi, Mahmud Bey, Peirce, de la Roche- Poncié.
7*

 
