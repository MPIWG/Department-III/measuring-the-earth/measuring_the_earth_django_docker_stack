 

 

 

110

En conséquence, les Délégués de tous les Gouvernements qui étaient représentés
à la Commission internationale de 1872, ainsi que les Membres de la section française,
feront de droit partie de cette première réunion pour concourir à la sanction des pro-
totypes.
ART 18:
Le Comité international mentionné à l’article 3 de la Convention, et composé

comme il est dit à l’article 8 du Règlement, est chargé de recevoir et de comparer

entre eux les nouveaux prototypes, d’après les décisions scientifiques de la Commission
internationale de 1872 et de son Comité permanent, sous réserve des modifications que
l'expérience pourrait suggérer dans l’avenir.

ART. 4.

La section française de la Commission internationale de 1872 reste chargée
des travaux qui lui ont été confiés pour la construction des nouveaux prototypes, avec
le concours du Comité international.

ART. 5.
Les frais de fabrication des &talons mötriques construits par la section francaise

seront rembourses par les Gouvernements interesses, d’apres le prix de revient par

unite qui sera determine par ladite section.

Aal, 0,
Le Comité international est autorisé à se constituer immédiatement et à faire
toutes les études préparatoires nécessaires pour la mise à exécution de la Convention,
sans engager aucune dépense avant l'échange des ratifications de ladite Convention.

 

pt eet OB A hak

Lak nike

to |

js mo ts ha hd nn à

 
