 

 

Le tracé se fait au moyen d’un erayon sur une feuille fixée A demeure sur un
cylindre horizontal de 1" de longueur et de 0®46 environ de diamètre; les heures
ont une longueur de 6 centimètres. Chaque feuille enregistre trente marées; en chan-
geant la couleur du crayon on pourrait avoir toute une lunaison.

Le dépouillement des feuilles est fait par chaque observateur, et envoyé régu-
lierement au dépôt. Ce dépouillement a eu en vue la recherche des coéfficients de la
formule des marées plutôt que celle du niveau moyen, c’est pourquoi on n’a pas songé
à user du procédé rapide du planimètre, mais les feuilles calculées permettent d'obtenir
rapidement ce niveau moyen.

Les résultats obtenus au moyen des maréographes ont permis d'améliorer la
publication commencée en 1839 par Mr. Chazallon, le véritable promoteur, en France,
de l’étude pratique des marées.

L'annuaire des marées, très répandu dans les ports, est aujourd’hui calculé
par mon collègue, Mr. Gaussin, qui y a apporté d’utiles améliorations.

Indépendamment des maréographes fixes, les ingénieurs utilisent aussi deux
systèmes de maréographes volants qui donnent de bons résultats. Celui du Coast Sur-
D 1 2 Ditique. facilement installable, :l-n’a d’autre inconvénient que de
donner un tracé très réduit en hauteur, si l’amplitude de la marée est considérable.
Le tracé s'effectue sur une feuille de papier sans fin.

Mr. l’Amiral Paris a essayé de concentrer un appareil sur une tige creuse

servant de puits, pour avoir les marées du large en fixant la tige sur des bancs de

CG

“sable; les résultats ont été favorables.

Mr. Hirsch propose de faire faire un pas à la question par l’adoption des pro-
positions suivantes :

1° Tous les pays ayant des côtes, sont priés d’envoyer au Bureau Central la
description des maréographes dont ils se servent, avec dessins, tableaux &e.

29 Ces mêmes pays sont priés d'indiquer en quels points de la côte sont in-
stallés les maréographes, et quels sont les points où l’on a l'intention d’en établir.

3° Le Bureau Central est chargé de publier un résumé de ces documents.

Mr. Bouquet de la Grye pense que, dans cette question, il ne faut pas seule-
ment avoir en vue la détermination du niveau moyen de la mer; un maréographe don-

nant des indications plus complètes doit être préféré parce que, tout en répondant au :

premier but, il permet d’étudicr les marées et fournit des données pour prédire les coups
de vent.

Mr. Faye et Mr. le Général Baeyer appuient cette opinion.

Les 3 propositions de Mr. Hirsch sont successivement mises aux voix et
adoptées.

En passant au paragraphe 3 du programme, Mr. le President donne la parole
a Mr. Hirsch qui fait la communication suiyante sur l’établissement international

des
poids et mesures:

 

 

ar
=
=
=
a
=
=
=
=
x
ü

a nl
