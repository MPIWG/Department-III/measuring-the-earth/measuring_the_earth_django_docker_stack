 

Ta AR

Jin

CI LAIT. LE

®
à
=
>

Annexe 2

CONTRAT AVEC BRUNNER A PARIS.

Nous soussignes, Brunner, constructeur d’instruments de précision à Paris, rue
de Vaugirard, 159, nous engageons à construire pour le Gouvernement Prussien l’ap-
pareil ci-après dénommé : :

Savoir:

Arr. 1. Un appareil & mesurer les bases, composé de:
Un thermomètre métallique formé de deux règles de quatre mètres de lon-
gueur, l’une en platine iridiée à 109, l’autre en laiton; |
Quatre porte-microscopes sur lesquels peuvent s'adapter une lunette d’ali-
snement, une lunette pour établir les points de repère sur le sol et
deux mires; |
Quatre supports, munis de deux mouvements perpendiculaires l’un à l’autre
et d’un mouvement de hauteur, pour porter le banc en fer, sur lequel
est fixé le thermomètre métallique ;
Un niveau avec arc de cercle donnant les 10” pour déterminer l’inclinaison
de la règle;
Huit pieds en chêne fort pour recevoir les quatre porte-microscopes et les
quatre supports.
Arr. 2. Cet appareil sera construit dans le délai de dix-huit mois à partir du
jour de la signature du présent marché.
Arr. 3. Le Gouvernement Prussien nous paiera pour la construction de cet
appareil la somme de trente-trois-mille francs, payable à Paris de la manière suivante :
Douze-mille francs à la signature du présent marché et vingt-un-mille francs a
la livraison de l’appareil.
Fait à Paris en triple expédition le onze mars mil huit cent soixante-seize.
(signé) Brunner,
Baeyer.

 
