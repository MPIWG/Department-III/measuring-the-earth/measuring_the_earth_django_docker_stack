 

132

Oesterreich,

Hier liegt folgender Bericht des Herrn Regierungsraths, Professor Dr. Ritter
von Oppolzer vor:

Die Beobachtungen an den selbstregistrirenden Pegeln werden von Seiten der
K. Akademie der Wissenschaften in Wien durch die ständige Commission für die Adria
überwacht, und die Beobachtungen aus den Jahren 1870 —1872 liegen publicirt vor.
(Dritter Bericht der ständigen Commission für die Adria an die K. Akademie, redigirt
vom. Ministerialrath Dr.’ J. Lorenz, Wien, Gerold’s Sohn 1873.) Beobachtungsstationen
‘ sind in Triest, Fiume, Zara, Lesina und Corfu, ausserdem hat die Kaiserl. ieee maria
einen ähnlichen Apparat in Pola aufgestellt. Die Resultate der Aufzeichnungen dieser
letzteren Station finden sich ebenfalls in dem obenerwähnten Berichte. Die Construction
und die Behandlung der. Mareographen ist im ersten Berichte der Adria - Commission
(Seite 66) enthalten.

Wien, 28. December 1875. a | Oppolzer.

 

Preussen.

Gegenwärtig sind 3 Mareographen in Thätigkeit, in Swinemünde, auf der Halb-
insel Hela und in Wilhelmshaven. Der erste ist im Jahre 1870 von dem geodätischen
Institute aufgestellt worden. Die Beschreibung desselben findet sich in der Publication
des geodätischen Instituts „Das Pracisions- So) us. yw. Erster Band, Berlin 1876“
auf. Seite 119 — 121.

Die beiden anderen Mareographen sind von dem hydrographischen Bureau der
Kaiserlichen Admiralität errichtet ‘worden, welches ausserdem noch einen solchen auf

der Insel Rügen in Gang zu bringen beabsichtigt.

Der auf Bestelluug und Kosten des geodätischen Ve angefertigte Mareo-
graph von Reitz, welcher bereits im Generalberichte von 1873 (Seite 15) besprochen
worden ist, wird von Seiten des hydrographischen Bureaus auf ‘der Insel Helgoland
‚aufgestellt werden. Die Beschreibung desselben giebt die von dem Civil-Ingenieur
Herrn F. A. Reitz, verfasste Abhandlung „Weasserstandszeiger für die mittlere Höhe.
Hamburg 1873“, welche bereits den Herren 'Commissaren zugesandt worden ist.

 

|
|

|
|
|

 
