 

12

2. Die Beobachtungen zur Bestimmung der Längendifferenzen zwischen Wien,
München und Greenwich, soweit Bayern daran betheiligt war. Die .diesseitigen Beobach-
tungen wurden unter Mitwirkung des Herrn Directors von Lamont vom Herrn Oberst-
lieutenant von Orff ausgeführt.

3. Die in Aussicht genommene und bereits eingeleitete D Gong
zwischen München, Genf und Strassburg unterblieb aus drei Gründen: erstens, weil die
abnorm schlechte Witterung im Monat Mai fast alle astronomischen Beobachtungen
vereitelte, zweitens wegen der im Monat Juni durch Gewitterstürme veranlassten Ver-
heerungen in der Nordost-Schweiz, und drittens wegen der gleichzeitig eingetretenen
Beschädigung des Bodenseekabels, welche in Verbindung mit den genannten Stürmen die
telegraphische Verbindung zwischen München und Genf unterbrach.

im Bezu2s auf trisonometrische Bestimmungen:

1. Die Recognoscirung der Hauptdreieckspunkte Ochsenkopf und Döbraberg
zur Verbindung derselben mit den Sächsischen Hauptdreieckspunkten Stelzen und Ka-
pellenberg, und die für die Winkelmessungen erforderlichen Durchlichtungen der die
Bayerischen trigonometrischen Punkte umgebenden Waldungen.

2. Der Bau massiver Beobachtungspfeiler und hölzerner Beobachtungsgerüste.
Von diesen Pfeilern hat der auf dem Ochsenkopf eine Höhe von 6” und der auf dem
Döbraberg eine solche von 4”. Ihre Construction wird seinerzeit mit den Beobachtungen
veröffentlicht werden. Die Winkelmessungen sind auf Kosten der Königl. Sächsischen
Regierung auszuführen, da sie lediglich den Anschluss des Sächsischen Netzes an das
- Bayerische bezwecken. Mit Ausnahme solcher Anschlüsse an die Netze der Nachbar-
länder, sind in Bayern die trigonometrischen Arbeiten vollendet. |

In Bezug auf das Präcisionsnivellement:

1. Der Druck und die Vertheilung der „Vierten Mittheilung über das Bayerische
Präcisionsnivellement von ©. M. von Bauernfeind.“ München, 1876, Verlag der Königlichen
Akademie, in Commission bei @. Franz.

Aus dieser Mittheilung geht hervor, dass Bayern im Ganzen sechs doppelt
nivellirte Polygone mit einem Umfange von 2945 Kilometer besitzt, deren mittlerer
Fehler selbst dann, wenn die irgendwo mit -einem Ablesungsfehler von 1 Decimeter
behaftete Fichtelgebirgsschleife nicht ausgeschlossen wird, nur 2,23 Millimeter pro Kilo-
meter beträgt. Nach Verbesserung dieses Fehlers wird sich, wie dieses bei den übrigen
Polygonen der Fall ist, der mittlere Kilometerfehler des ganzen Bayerischen Höhen-
netzes nur auf wenig mehr als 1"" berechnen.

In der vorstehend angezeisten Abhandlung ist eine kurze Betrachtung über die
Ausgleichung geometrischer Höhennetze enthalten und eine Näherungsmethode der Aus-
gleichung in Aussicht gestellt”), welche zwar nicht die kleinste Summe der Fehlerqua-

*) Diese Ausgleichungsmethode ist unterdessen mit dem in der nachfolgenden No. 2 enthaltenen
Titel erschienen.

  

11 el 100801) 118 a) Ri

Abe tr

1h kur ll

jh ian dahl ata in 11:

 
