fol,

[YIN Moores me

Italien.

Statt eines besonderen Berichts ist dem Central-Bureau die Abhandlung ,,Dei
Mareografi esistenti in Italia lungo i mari mediterraneo ed adriatico e dei risultati
dedotti dalle rispettive rappresentazioni mareografiche. Memoria del- Prof. Cav.. Alles-
sandro Betocchi. Roma 1875“ zugegangen, welche den Herren Commissaren übermittelt _
worden ist. In dieser wird, mitgetheilt, dass an den italienischen Küsten vier Mareo-
sraphen aufgestellt. sind, bei Livorno und Ravenna je einer und bei Venedig zwei.

 

Niederlande.

An Stelle des Berichts sind zwei von Herrn Z. Cohen Stuart verfasste Ab-
handlungen eingesandt worden: |

1) Uitkomsten van de in 1875 en 1876 uitgeveerde Nauwkeurigheids -Water-
passing. (Metallographirt.) Delft 1877.

2) Mededeeling omtrent de in 1875 en 1876 uitgeveerde Nauwkeurigheids-
Waterpassing. (Overgedrukt uit de Notulen der Verdagering van het
Koninklijk Instituut van Ingenieurs van 13. Februarij 1877.)

| ll OL Al

In keiner von beiden werden indessen Mareographen erwähnt.

al M

 

Norwegen.

Nach den im Generalbericht von 1875 (Seite 181) enthaltenen Angaben sind
schon seit einigen Jahren zwei registrirende Pegel in Wirksamkeit, der eine an der |
befestigten Insel Kaholmen (Oscarborg) bei Dröbakssund im Christianiafjord, der andere |
in Drontheim. |

I

a
a
=
=
s
à
z
®
=
=
:
=

 
