ARR ri

“SRP PPR YF AM

de LEE Di |

ILE AR

a
à
=
=
s
=
®
x
=
=
=
=

09

 

sichert, dass die Bevollmächtigten stets der belgischen Gastireundsehate: ein bleibendes
Andenken bewahren wiirden.

Die Herren Commissare und auswärtigen Bevollmächtigten schliessen sich den
von dem Präsidenten ausgesprochenen Gefühlen an. |

Herr Hirsch dankt ins Besondere dem Herrn Hauptmann Hennequin fir die
freundliche Hilfe, welche er ihm bei der Abfassung der Protokolle geleistet, und für die
Dienste, welche er hierdurch den Arbeiten der Commission erwiesen hat.

Der Präsident erklärt hierauf die diesjährigen Verhandlungen der pean L
Commission für geschlossen.

Schluss der Sitzung um 4 Uhr 15 Min.

Fünfte Sitzung,

Brüssel, den 10. October 1876.
Präsident: Herr General Ibanez.

Anwesend sind: die Herren Ibanez, Baeyer, von Bauernfeind, Bruhns, Faye, von
Forsch, Hirsch, von Oppolzer, de Vecchi.

Das Protokoll der letzten Sitzung wird vorgelesen, genehmigt und unterzeichnet.

(gez.) C. Ibanez, (gez.) v. Oppolzer,
5 (BOY CRA. » » Ld. Horsch,
»  Bauernfemd, 11 Blaye,
„ :Bu.de Wecchs, Ad. Hirsch.
3 Co Bruhkus,

 
