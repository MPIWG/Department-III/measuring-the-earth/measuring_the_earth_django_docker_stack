 

oe,

contréle de Vinvariabilité, un cylindre creux du même métal que celui des étalons,
sur la surface duquel on tracerait deux traits distants d’un mètre et dont on mesurerait
le changement de volume par le poids de l’eau qu'il contient.

M. Peirce croit qu’un excellent moyen d'obtenir linvariabilité de l'unité de
longueur consisterait à rapporter l'unité de longueur à la longueur de l'onde lumineuse,
car le rapport de ces deux grandeurs peut être déterminé très exactement par des
mesures micrométriques. M. Rutherford est parvenu à construire des réseaux sur verre
d’une telle finesse et d’une telle exactitude, qu'ils ne laissent plus rien à désirer et qu’ils
permettront de mesurer très facilement la longueur de l'onde lumineuse.

M. le Président remercie M. Perrier pour le soin avec lequel il a rédigé son
rapport; puis il annonce à l’Assemblée qu'il vient de recevoir de M. le Maréchal de la
Cour une invitation du Roi pour MM. les délégués à un déjeuner au château de
Wilhelma pour mercredi à 1 heure.

M. le Président fixe la prochaine séance à demain 2 octobre à 1 heure.

La séance est levée à 3 heures.

 

x

LAN DU he) Rie ak

ak nnd

no LAN

ih

jt ml tn a M a tal nn 0

2 samtaiéaunns mamie VAN |
