 

Perey" ren PETE FIRE en nr |

Tr ART amarıeı QUI

1

1

17

wail

Alla be

Quinn Lau RTL TI Wey
ie

 

137
divers appareils qu’il faut rechercher les discordances des réseaux européens; tous les
appareils existants donnent partout des résultats d’une haute précision, mais la compa-
raison que nous proposons d'exécuter entre les appareils par la mesure de bases com-
munes, permettra seule d'éliminer les discordances provenant de la multiplicité des étalons,
et les écarts persistants entre les côtés communs à deux triangulations permettront
d’assigner le degré de précision de chacune d'elles.

I existe certaines régions où des bases nouvelles vont être mesurées: la Bel-
gique se propose de mesurer prochainement une troisième base vers la frontière du
Luxembourg; l'Autriche une base nouvelle à Erlau, près de Tokai; l'Italie une base au
moins dans la vallée du P6 ; la Russie entreprendra certainement de nouvelles mesures
en dehors de ses deux grands arcs de méridien et de parallèle, et on peut prévoir le
moment peu éloigné où le nombre des bases mesurées en Europe sera suffisant pour
asseoir la triangulation sur des fondements solides.

Quelques bases devront être mesurées de nouveau, soit qu’elles remontent à
une époque trop ancienne, où les appareils étaient imparfaits, soit qu’elles n’aient été
mesurées que pour établir la carte d’un pays.

Telle est, entre toutes, la célèbre base de la Somma, l’une des bases du pa-
rallèle moyen; nous citerons encore l’une au moins des bases mesurées soit en Sardaigne
par La Marmora, soit en Corse par Tranchot, la base suisse, la base fondamentale du
Portugal, et une base de la Bavière. Enfin nous dirons que le Dépôt de la guerre de
France se propose de mesurer de nouveau la base de Melun, qui est la base fondamen-
tale de la triangulation française, et se rattache d’une manière si étroite avec le mètre,
avec la règle de Borda N° 1 et avec la toise du Pérou.

Nous exprimons le vœu que les nouvelles mesures de bases anciennes soient
effectuées le plus tôt possible, que les lacunes existantes soient comblées par la mesure
des bases nouvelles que nous avons signalées, que les divers appareils de bases soient
comparés entr’eux comme nous l'avons indiqué, et le but de notre association sera
atteint d’une maniere complete et avec une haute précision en ce qui concerne les bases
et les longueurs des cétés, lorsqu’elles pourront étre exprimées toutes en fonction d’une
méme unité par la comparaison avec la régle géodésique prototype.

Cea aa aan

M. Baeyer prie M. Sainte-Claire Deville de bien vouloir donner quelques ren-
seignements sur les progres de la construction de l’appareil à mesurer les bases com-
mandé par le Bureau central.

M. Sainte-Claire Deville annonce que les règles en platine iridié de cet appareil
sont complètement terminées, et qu’il est occupé à en contrôler la composition par des
analyses chimiques. Au sujet des nouveaux prototypes métriques et comme meilleur
moyen de déterminer les températures, M. Sainte-Claire Deville propose d'employer, comme

18

 

 
