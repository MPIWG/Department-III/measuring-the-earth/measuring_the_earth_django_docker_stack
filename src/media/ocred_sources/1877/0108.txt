 

 

96

2. Proces-verbaux des séances de la Commission permanente de l’Association
géodésique internationale, réunie & Dresde les 21, 22, 28 et 29 septembre 1874. (N’est
pas en vente.)

3. Proces-verbaux des séances de la Commission permanente de l’Association
géodésique internationale, réunie à Paris du 20 au 29 septembre 1875, rédigés par les
secrétaires MM. C. Bruhns et A. Hirsch, publiés pour servir de rapport général pour
l'année 1875 par le Bureau central. Berlin 1876, en commission chez Georges Reimer.

4. Procès-verbaux des Séances de la Commission permanente de |’ Association
géodésique internationale, réunie à Bruxelles du 5 au 10 octobre 1876, rédigés par les
secrétaires MM. C. Bruhns et A. Hirsch, publiés pour servir de rapport général pour
l'année 1876. Berlin 1877, en commission chez Georges Reimer.

5. Astronomisch-geodätische Arbeiten in den Jahren 1873 und 1874 par
Albrecht. Berlin 1875, P. Stankiewicz, imprimeur-editeur.

6. Astronomisch-geodätische Arbeiten im Jahre 1875 par Albrecht. Berlin 1876,
P. Stankiewicz, imprimeur-éditeur.

7. Astronomisch-geodätische Arbeiten im Jahre 1876 par Albrecht. Berlin 1877,
P. Stankiewicz, imprimeur-éditeur.

8. Zusammenstellung der Literatur der Gradmessungs-Arbeiten (par Baeyer et
Sadebeck). Berlin 1876, P. Stankiewicz, imprimeur-éditeur.

9. Das Rheinische Dreiecknetz. Premier cahier: Die Bonner Basis (Baeyer,
Sadebeck et Fischer). Berlin 1876, P. Stankiewicz, imprimeur-éditeur.

10. Das Präcisions-Nivellement. Premier volume: Arbeiten in den Jahren 1867
bis 1875 (Bôrsch, Sadebeck et Seibt). Berlin 1876, P. Stankiewicz, imprimeur-éditeur.

11. Maassvergleichungen. Deuxième cahier: Beobachtungen auf dem Steinheil-
schen Fühlspiegel-Comparator (Baeyer et Sadebeck). Berlin 1876, P. Stankiewicz, im-
primeur-éditeur.

12. Bericht der Special-Commission über die Punkte 52 und 5b des Programms
der im Jahre 1876 in Brüssel vereinigten permanenten Commission der Europäischen
Gradmessung (manuscript). Berlin 1876, imprimerie de P. Stankiewiez2.

13. Voten über die unter 5a und 5b des Brüsseler Programms aufgeführten
‚geodätischen Fragen, zusammengestellt in dem Gentralbureau der europäischen Grad-
messung (manuscript). Stuttgart, imprimerie des frères Kröner.

@. Exécution des décisions de la Commission permanente.

Les comptes-rendus des séances de la Commission permanente à Bruxelles en
1876 contiennent (pag. 12 et 41) un exposé de la manière dont ces décisions avaient
été accomplies jusque là.

Les décisions prises par la Commission permanente lors de sa réunion à
Bruxelles ont été exécutées de la manière suivante :

1. Le Bureau central a joint au rapport général pour l’année 1876 un tableau
des rapports concernant les maréographes installés dans les différents pays.

 

x

a Te CELL OUR LITIT ENT

pati

Tab to ae

Wb ee ey

tbh ash lab di ld arch eye 5:

 
