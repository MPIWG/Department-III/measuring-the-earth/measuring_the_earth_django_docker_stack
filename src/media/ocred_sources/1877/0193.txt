Far or erennnnenrene rt SAP TE |

I Im ul

FI

bins LOMA BSA
A By air

  

 

 

181

B. Le pendule étant suspendu par le couteau le plus rapproché
du centre de gravité.

 

AMPLITUDE
@ du Cae de l'échelle
7 4000
observée enlenleo

p. p.
9 30 1.0 1.0
29 92 0.9, : 1.0
2.29 0.9 0.9
2 C9 ad
aD 0.8 0.9
2 14 0.8) 7) 4208
2,12 0.8 0.8
2 00 0.7 0.8
2-: DA 0.75 0.8
1.559 O75 1 0%
i of 0.75 0.7

 

En faisant ces observations, j'ai vu distinctement la petite vibration subsidiaire

au bout de chaque oscillation provenant du deuxième terme de la formule.

Enfin, jai fait osciller le pendule sur deux supports de flexibilité différente.
L'un d’eux, c’est le trépied métallique de Repsold, auquel se rapportent les mesures de
la flexion données plus haut. L'autre était. obtenu en fixant la partie supérieure du
trépied Repsold à un plateau épais en bois, au moyen de boulons à écrous en bronze
passant par les trois trous servant au passage des pieds. Ces trous sont d’une forme
conique, et les boulons s’y adaptent exactement. J'ai mis sur chaque boulon, entre la
tête du support et le plateau, une rondelle de plomb, de sorte qu’en serrant les écrous

au-dessous du plateau et en comprimant les rondelles, l’on obtient une grande fixité, en

même temps que la position horizontale. Le plateau, qui a une épaisseur de cinq centi-
mètres, a été découpé pour faire place au pendule, et il a été introduit par force entre
une muraille de pierre et un grand pilier en briques. Il a été percé d’une fente, par
laquelle on pouvait faire pénétrer la poulie de la machine Atwood pour mesurer
la flexion.

   

 

 

RS nn nn nam ne

 
