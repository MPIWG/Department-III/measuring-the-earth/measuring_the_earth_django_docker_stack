 

<P PTT Sn nme

ETT PIE OTTRIT PT

PAU

rhs} 10 0 37 000

Les opérations géodésiques en Algérie ont donné entre autres des résultats
importants sur les altitudes du Sahara: des déterminations télégraphiques de longitude
ont été exécutées entre Alger—Bone, Nemours (Alger), Alger—Paris, Alger—Marseille;
elles fourniront la différence exacte de longitude entre les principales stations d’A-
frique et d’Europe.

En Espagne le directeur de lInstitut géographique et statistique a publié un
premier volume sur la mesure des degrés et les premières feuilles de la Carte
d'Espagne; les déterminations astronomiques sont commencées et les travaux de ni-
vellement très avancés.

Le Portugal veut faire faire une triangulation nouvelle dont les travaux préli-
minaires sont commencés. On projette aussi d'y faire des nivellements et des détermi-
nations astronomiques.

L'Italie a étendu son réseau de triangles par delà la Sicile et effectué la jonction
avec la côte d'Afrique. On y a publié un certain nombre de jonc astrono-
miques; les nivellements ont été commencées cette année.

En Roumanie on a continué à dresser des piliers, mais la guerre a fait sus-
pendre les travaux géodésiques. La différence de longitude Jassy—Czernowitz a été
observée astronomiquement.

Différentes publications contiennent des travaux théoriques auxquels l'impulsion
a été donnée par notre Association; des mémoires spéciaux ont fait avancer, non-seule-
ment les calculs de compensation, mais aussi le problème de la détermination de la
véritable grandeur et de la vraie figure de la terre, l’objet principal des travaux de
l’Association géodésique internationale.

Les études de la pesanteur faites au moyen du pend a réversion sont active-
ment poursuivies dans plusieurs pays, et la théorie de ces observations a fait un pas
considérable par les études de MM. Pezrce et Cellérier, dont des détails sont donnés
dans des rapports spéciaux. |

Dans différents pays limitrophes de la mer on a installé des maréographes qui
enregistrent la hauteur de la mer; les observations obtenues par ce moyen fourniront
la base pour la détermination de l'horizon fondamental de l’hypsométrie de l’Europe.

D’après la décision de la Conférence générale de 1874 la Commission perma-
nente a adresse au Gouvernement français un mémoire en faveur de la création à Paris
d’un établissement international des poids et mesures; elle a aussi appuyé auprès du
Gouvernement prussien la demande du Bureau central d’être mise en possession d’un
local approprié à faire des comparaisons d’étalons et de règles géodésiques.

La Commission permanente à provoqué la publication de Cartes générales des
triangulations, des points astronomiques et des lignes de nivellement, de même que des
communications sur la construction des maréographes et un catalogue des publications
sur la mesure des degrés.

Différents sujets discutés dans les séances de la Commission permanente, entre
autres les deux questions de la compensation par groupes et du rattachement des groupes

les uns aux autres ont donné lieu a la nomination d’une Commission spéciale dont les
a

 
