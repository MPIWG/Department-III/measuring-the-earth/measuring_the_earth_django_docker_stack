 

|
|

 

222
meilleure méthode & suivre serait celle de faire un petit nombre de groupes dont chacun
aurait le plus grand nombre possible d’équations de condition.

On sait que dans la Küstenvermessung il y a une compensation qui ne com-
porte pas moins de 86 équations de condition, et je crois que les Anglais sont arrivés
à résoudre un nombre d'équations normales qui dépasse les 70. Dans nos compensa-
tions de lItalie du Sud nous nous sommes maintenus sur les 40 équations, quoique, dans
les derniers travaux du Bureau Central même, l’on ait adopté la limite de 30 équations.

Il en est pour cette question ‘comme pour celle de la compensation des bases:
l'Italie s’est tenue rigoureusement aux procédés de Bessel et Baeyer. Ceci prouve que
si par hasard nous ne sommes pas d'accord avec notre président d'âge sur l’idée de
prescrire absolument ces méthodes à tous les autres associés, ce n’est certainement pas
pour soutenir notre point de vue particulier, mais au contraire pour rendre un hommage
à cet esprit de liberté sans lequel la science est transformée en un règlement.

§ 8.

A mon avis il vaut mieux ne pas établir une limite ni en plus ni en moins sur
le nombre d’équations de condition qui doivent étre compensées dans un méme groupe.
T1 suffit d’avoir éxécuté des travaux pour voir combien de difficultés doit surmonter un
opérateur, difficultés de temps, d'argent, de personnel, de localités, avec lesquelles il
faut compter et qui bien des fois déterminent notre choix. Ce qu'il faut constater d’une
manière bien sûre c’est le degré de précision des résultats, et il est bien possible que ce
degré de précision soit très grand dans une triangulation compensée par petits groupes,
si les observations ont été faites avec tout le soin possible.

Lorsque l’on a un système d'observations qui ne satisfait pas aux conditions
géométriques du réseau, l’on peut dire que ce système est géométriquement impossible.

La compensation a pour but de faire disparaître cette impossibilité en substituant
au système des quantités observées un nouveau système qui satisfait aux conditions
géométriques. Mais la légitimité de cette substitution doit être établie par des consi-
dérations inhérentes à la nature du problème. Soit a une des observations, a’ sa valeur
compensée, la quantité observée a est impossible géométriquement, mais la quantité
corrigée à’, qui est possible géométriquement, pourrait être impossible comme observation
si la valeur de la différence à — a, ou de la correction apportée à l'observation, était
supérieure aux erreurs d'observation.

Si au contraire les corrections apportées aux quantités observées sont toutes dans
les limites des erreurs d'observation, il n’y a pas de raison pour que le système com-
pensé ne soit pas possible et par cela-même acceptable. *)

C’est pour cette raison que je trouve louable la méthode de quelques observateurs
de donner non seulement l'erreur probable du résultat, mais aussi l’erreur probable d’une

*) C'est ainsi que Gauss a jugé la question à propos de la compensation donnée par Mr. Arayenhof

dans son ouvrage sur la triangulation des Pays-Bas.

 

x
z
=
=
à:
a
=
È
=
=
x
U

nl

 
