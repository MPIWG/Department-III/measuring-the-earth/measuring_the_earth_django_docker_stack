 

2 ah TFT green Tr. I: |

IT AM IM ONE CU

It

POCONO NTT BETTIE TT RT Teer

 

105

Il s’agissait avant tout de rassembler tout le matériel des rapports existants
sur les travaux des différents Etats de l'Association géodésique et de l’ordonner de ma-
nière à donner une vue d'ensemble sur ces travaux. Parmi tous ces rapports, je me suis
restreint aux rapports officiels des commissaires des différents pays, parus dans les
publications annuelles du bureau central. — Il en résulte que je n'ai pu tenir compte
que des travaux qui étaient achevés à la fin de l’année 1876 et je me réserve de com-
pléter mon rapport quand les travaux exécutés dans le courant de cette année seront
publiés. Quelques-uns de ces travaux, au sujet desquels j'ai été renseigné déjà par les
délégués de quelques pays, seront cependant déjà mentionnés ici. — Je crois avoir ainsi
assuré la confiance que l’on peut avoir dans le contenu de ce rapport.

J'avais tout d'abord à résoudre la question de la meilleure manière d’ordonner
les riches matériaux dont je disposais, et je me suis décidé sans hésitation pour le pro-
cédé de représenter sur une carte les opérations astronomiques des différents pays.
Pour ne pas surcharger par trop cette carte, j'ai séparé les données relatives aux
déterminations de longitude et j'ai chargé M. P. F. Kühmert, assistant de la Commission
géodésique autrichienne, d’en dessiner la carte (voir carte I).

Dans cette carte les points dont la difference de longitude a été déterminée par
voie télégraphique sont toujours reliés par des lignes à l’encre rouge en général directes.
Les points eux-mêmes sont marqués par.un petit cercle à l'encre rouge à côté duquel
se trouve le nom de l’endroit. Pour éviter de trop nombreux contacts entre ces cercles
et les lignes d’autres déterminations de longitude, il a cependant fallu à plusieurs re-
prises modifier un peu la position géographique de certains points; cette licence ne peut
cependant diminuer en rien la valeur de la carte comme tableau d'ensemble. Là où il
n’a pas été possible d’éviter une rencontre entre les lignes et les petits cercles, j’ai
interrompu la ligne dans sa direction rectiligne et j'ai rétabli la continuité au moyen
dun are de cercle circonscrit au petit cercle du point. Enfin pour quelques lignes
(ligne Vienne—Münich p.ex.) j'ai coupé la ligne par quelques traits verticaux indiquant
le nombre de déterminations indépendantes qui ont été faites de la différence de longi-
tude. — Ces quelques remarques suffiront pour expliquer la signification et lPemploi
de cette carte, au moyen de laquelle on peut d’un seul coup d’œil reconnaître quelles
sont les déterminations encore nécessaires pour compléter le réseau des déterminations
de longitude de l'association géodésique. Le but de ce rapport est en réalité de faire
ressortir les lacunes encore existantes.

Grâce à l’infatigable activité avec laquelle on travaille dans tous les pays à
faire avancer notre entreprise, ma tâche de rapporteur est singulièrement facilitée et
l’on peut bien exprimer l'assurance que le réseau des déterminations astronomiques
laisse fort peu à désirer et que les quelques lacunes qu'il présente et qu'il faudra encore
combler, ne feront que le consolider et n’y apporteront aucune modification essentielle.
Je ne veux qu’indiquer en quelques mots les points qui me semblent défectueux. — U
est bien à désirer tout d’abord que la jonction directe de la France et de Pltalie soit
prochainement exécutée, à présent que la jonction à été opérée entre la France et la

Suisse. — La ligne Marseille— Milan conviendrait le mieux à cette opération. Les
14

 
