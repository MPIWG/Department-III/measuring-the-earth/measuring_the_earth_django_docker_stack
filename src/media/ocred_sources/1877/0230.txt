    

218

Je commencerai donc par la première question, celle de la compensation des
bases, et je la ferai précéder par des données numériques qui peuvent fournir un crite-
rium exact sur les conséquences qui s’ensuivent en adoptant une solution plus tôt
qu'une autre.

83.

1. Si l’on considère l'erreur moyenne des bases mesurées dans les travaux
géodésiques récents on sera convaincu que jamais cette erreur ne dépasse 5; de la
longueur de la base, et que par conséquent elle ne peut influer que sur le septième
chiffre décimal des logarithmes des côtés.

Or une erreur semblable sur les logarithmes des côtés peut tout au plus exercer
(mème dans les triangles médiocrement conformés) une influence sur le chiffre des
dixièmes de seconde des angles.

Il s'ensuit de là, que, si l’on devait modifier les angles observés de manière à
leur faire satisfaire la condition de l'accord entre deux bases, les corrections que dans
ce cas les angles devraient subir seraient extrêmement petites et bien inférieures aux
erreurs à craindre dans la mesure des angles mêmes.

Au reste, pour se convaincre de cette vérité, il suffit de jeter un coup d’auil
sur le tableau des corrections causées par la compensation des bases dans Pouvrage
classique de Séruve sur la mesure d’un arc de méridien de 25 degrés. Tome I*, page 171
et suivantes.

2. Réciproquement, si on prend une triangulation bien faite, comme celles des
Gauss, des Struve, des Schumacher, des Bessel, des Baeyer, des Andrae, etc. etc., et que
Pon cherche quelle est l'erreur moyenne à craindre dans la détermination des côtés
sous l'influence des erreurs des mesures d’angles, il est facile de se convaincre que à
mesure que l’on s'éloigne des bases les erreurs augmentent avec rapidité et portent leur
influence sur le sixième et même sur le cinquième chiffre décimal du logarithme des côtés.

Si l’on consulte, dans l’ouvrage admirablement conçu ,,Den Danske Gradmaaling“
de Mr. Andrae, le tableau existant à page 389, on verra que, l'erreur moyenne à
craindre dans la valeur d’un côté se composant de deux parties, celle dépendant de la
mesure de la base et celle dépendant de la mesure des angles, cette dernière est celle
qui exerce dans une forte proportion la plus grande influence sur l’erreur totale.

Le tableau en question ne résume que la succession de 12 triangles, dont cinq
servent au rattachement de la base au premier côté de la triangulation, et cépendant
lon observe déja au douzième triangle que, sur une erreur moyenne de 32,6 unités du
septième ordre dans le logarithme du côté, 31,8 unités sont dûes à l'influence des me-
sures angulaires.

On sait que ni Bessel,*) ni Baeyer n’ont introduit dans la compensation la con-

*) Il est assez facile de comprendre que Besse! n'ait établi l'accord entre deux bases puisqu'il
n'avait mesuré que la base de Künigsberg.

 

=

Ta Ta RT TE bake ha

IM

ttl

LM ee Ha à

Wb Tee

bh dh labile ld nn 11:

2.4 sms, mia SA |

 
