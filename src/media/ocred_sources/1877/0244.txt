 

 

 

232
berechnete kürzeste Linie UN = ..... Meter
berechmetes. Azimuth ins if 1: 0 ct."

berechnetes Azimuth in N = 0 Lia

Wenn von den Seiten des Hauptnetzes diese doppelten Daten vorliegen, dann

können sie etwa nach folgenden Fragen gruppirt werden:

a. Sind in Europa so viel übereinstimmende Punkte vorhanden, dass man das
Bessel’sche oder ein ähnliches Sphäroid als Fundamentalfläche von Europa
annehmen kann? oder

b. erscheint die Oberfläche von Europa aus verschiedenen nebeneinander liegen-
den Sphäroidflächen zusammengesetzt?

c. Welche Rolle spielen die Lothablenkungen? — Treten sie überall gruppen-
weise auf oder kommen sie auch sporadisch vor*).

Nachdem eine zweckentsprechende Gruppirung der Vermessungs-Resultate statt-

sefunden, wird man erst die Bestimmung der wahren Oberfläche in Betracht ziehen

können. Wie wenig Erfolg eine solche Untersuchung bei beliebig gewählten Punkten

verspricht, davon giebt die Gradmessung in Ostpreussen ein Beispiel.
§ 4.
Recapitulation.

In Betracht des Punktes 5* stimmen die Voten der Mitglieder der Special-
Commission darin überein, dass bei den Ausgleichungen der Gruppen die wirklichen
Fehler (ohne Zwangsbedingungen) angegeben werden sollen. Herr Oberst Ganahl sieht
dies gewissermassen als selbstverständlich an und Herr General Liagre hebt treffend
hervor, dass durch die Ausgleichung einer Gruppe keinerlei Rectification ausgeübt
sondern lediglich das Resultat der Messoperationen dargestellt werden soll. Herr Oberst
Ferrero berichtet ($ 6 seines Gutachtens), dass Italien ganz nach der Bessel’schen Me-
thode arbeite, während Director Peters darauf aufmerksam macht, dass ausserdem die
Einführung von Polar-Coordinaten anstatt der Seiten und Winkel als Unbekannte unter
Umständen von Vortheil sei.

In Betracht des Punktes 5? erklären Herr Oberst Ganahl und Herr Director
Peters direct ihre Zustimmung zu meinem Vorschlage: Jede Gruppe als verant-
wortlich für ihre Resultate anzusehen. — Herr Oberst Ferrero stimmt indirect
dadurch zu, dass zwischen den italienischen, nach der Bessel’schen Methode ausgegliche-
nen Gruppen neue Ausgleichungen schwierig sein würden wegen der grossen Anzahl der

*) Die Bestimmungen der Lothablenkungen entbehren bis jetzt noch jeder sicheren Grundlage.
Sie beziehen sich in jedem Lande auf gewisse Ausgangspunkte und können daher nur die relativen Ab-
weichungen gegen dieselben angeben. Ob diese Ausgangspunkte auf ein und demselben oder auf ver-
schiedenen Sphäroiden liegen, darüber kann erst die angedeutete Gruppirung der Punkte des astronomisch-
geodätischen Netzes und die Ermittelung der Fundamentalfläche Aufschluss geben.

 

x
=
=
=
=
a
=
=
=
=
=
x
q

Lak mil

to |

el Len ns bad a Hann 11:

 
