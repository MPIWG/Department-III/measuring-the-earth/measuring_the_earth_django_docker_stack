 

 

 

 

d ds : ;
Donc, lorsque ¢ est nul, = et —, sont nuls aussi, en sorte que n, et n, sont égaux

à O. À cet instant, auquel # — 0, la force horizontale sur le couteau est, en désignant
par d, et s, les valeurs initiales de ¢ et de 5,
Mg
7 bo = 50:
La premiere forme de en intégrale de la page précédente donne, pour
les deux valeurs de + correspondant & ¢=0,
(+ ha) do + (1 + 24)55 = A;
OP 2.) bo + (1 7 2g) 50 = Ap-

En éliminant d, et so entre ces trois équations on obtient
Mgh
MP (1 + ay) Ay — 2 (+4) Ay = —e (+ x) A, +e (+ xs) A,

d’où, en substituant pour z, et pour x, les valeurs trouvées en bas de la page précé-
dente, on déduit

 

 

 

 

Ay __ M?g? (Ih)
A, e708 +. M?g?h+2Mgch-l
ou approximativement
u Mad)
= a ee ye

Ainsi, en posant A = mek ae on aura

 

cos A 00s (|/ nn m | == A cos Vo N
rel eV)

Le deuxieme terme dans l’expression a o n'étant que la
peut le négliger.

La quantité e peut être déterminée expérimentalement en mesurant la déflexion
= du trépied, produite par une force horizontale égale à l’unité de poids; ce qui s’écrit
8

Ou substituant cette valeur de «, on conclut enfin:

A N
we oe )

Ainsi, Veffet du mouvement du support sur le pendule est de lui donner une

 

 

 

300500 du premier, on

. r . # , x fj —, F
longueur virtuelle plus grande que sa longueur réelle d'une quantité égale à ME =.
Designons la durée d’une oscillation par T, et par Al, AT, etc., les corrections
provenant de la flexion sur J, T, ete., en sorte que, d’apres ce qui précéde,
oe

FA

 

x

1 et (iam mR) bane aa

il

ak niki) Wi

Wee

jh a Lu a Mat a Ai lap nn 1:
