 

AP TRITT TP mr T

IPN TRITT ET

ran

WITT

Ar ala Rh ET LULU NT UNI
TTL ENT TO TREE TREE Sy TEE

rh

mission des noms des auteurs, je crois devoir vous signaler une rectification à faire :
deux azimuts observés à Saligny-le-vif doivent étre rectifiés ainsi qu'il suit:

Bourges : 99° 0’ 21:02,, Dun-le-Roi: 39° 48’ 28729;

ce sont les chifires que je trouvais dans mes papiers et dont je ne puis m'expliquer le
changement à l'impression.
(signé) Yvon Villarceau.

Herr Hirsch erwähnt zunächst die Gutachten, welche von der in Brüssel ein-
gesetzten Commission abgegeben sind und die bisher als Manuscript gedruckt allen
Herren Commissaren mitgetheilt sind.

Ausserdem sind auf die von dem Präsidenten des Centralbureaus gestellten
Fragen eine Anzahl von Antworten von Seiten anderer Commissare eingegangen, die
ebenfalls durch den Druck veröffentlicht sind. *)

Was zunächst die Ausgleichung nach Gruppen anbetrifft, so stimmen die Voten
alle darin überein, dass die Bildung von Gruppen bei sehr grossen Netzen unvermeidlich
ist, und zu wünschen sei, dieselben so gross als möglich zu machen, ohne dass sich
allgemeine Regeln dafür aufstellen lassen. In Betreff der Ausgleichung der einzelnen
Gruppen unter einander und deren Zusammenfassung zum Europäischen Netze sind
dagegen die Ansichten verschieden und verdienen besonders die von den Herren Peters
und Andrae gegebenen Lösungen Beachtung. Auch darüber, ob eine absolute Ueber-
einstimmung der Grundlinien und Anschlussseiten als Bedingungsgleichung in die Aus-
sleichung einzuführen sei, gehen die Meinungen auseinander.

In Folge der vorliegenden Berichte stellt Herr Hirsch daher folgende Anträge:

Aus der Zusammenfassung der eingegangenen Berichte und Meinungsäusserungen
geht hervor, dass die meisten Mitglieder über folgende Punkte übereinstimmen :

1. Es ist wünschenswerth, dass die Ausgleichungs-Gruppen möglichst gross
gewählt werden, ohne dass eine untere Gränze für die Ausdehnung der
Gruppen aufgestellt wird.

2. Was die Ausgleichung der einzelnen Gruppen untereinander und deren
Zusammenfassung zum europäischen Netze betrifft, so ist diese schwierige
Aufgabe in einigen der eingegangenen Berichte in’s Auge gefasst, na-
mentlich von den Herren Peters und Andrae; hoffentlich werden auch
noch andere Arbeiten über diesen Gegenstand hervortreten. Die General-
conferenz empfiehlt der permanenten Commission, diesem wichtigen Gegen-
stande besondere Aufmerksamkeit zu widmen, damit die theoretische Aus-

 

*) Anmerkung: Siehe die Gutachten und Voten im Anhange III.

 
