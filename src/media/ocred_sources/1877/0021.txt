CT PPT PP mm

ab bt deb
DRM PORTE APT

HTT THRIFT TMEV ATI WE

TTA

i

 

da

Lab

dr ds th kahl min | [RIT TE uni
DM NU PTT

9

Ausser geschäftlichen Mittheilungen ist die Geschäftsordnung und das Programm
für die gegenwärtige Versammlung eingehend berathen.

Ueber die Verhandlungen der Permanenten Commission in Paris und Brüssel
sind die Protokolle im Druck in deutscher und französischer Sprache erschienen, sodass
über dieselben kein besonderer Bericht nöthig sein wird.

Ueber die Fortschritte der Europäischen Gradmessung im Allgemeinen kann um
so kürzer referirt werden, da die Specialberichte der einzelnen Länder Ihnen durch die
Publicationen des Öentralbureaus bekannt geworden sind; und wir werden hier noch
weitere Berichte entgegen nehmen. Doch mag erwähnt werden, dass in Skandinavien
die Triangulationsarbeiten weiter fortgeschritten sind und auch die Bestimmung ver-
schiedener astronomischer Punkte ausgeführt worden ist. Während in Schweden nicht
nur trigonometrisch, sondern auch topographisch weiter gearbeitet wird, hat in Norwegen
die Arbeitskraft sich dadurch gesteigert, dass zu dem bisherigen Commissar Herrn Pro-
fessor Fearnley noch drei weitere Commissare getreten sind, die an den Gradmessungs-
arbeiten Theil nehmen.

Von der dänischen Gradmessung steht das Erscheinen zweier Bände, des dritten
und vierten, in kurzer Zeit in Aussicht, aus welchen eine telegraphische Längen-
bestimmung zwischen Kopenhagen und Kiel soeben durch unsern verehrten Collegen
Herrn Professor Peters schon publieirt vorliegt. Der vierte Band wird die letzten
Messungen enthalten und unter Anderem Beiträge zur Theorie der Gradmessung bringen.

In Russland sind die Feldarbeiten für die Europäische Längengradmessung auf
dem 52. Parallel vollendet und werden die Resultate in kurzer Zeit erscheinen. Die
Dreiecksnetze sind sehr wesentlich ergänzt und verschiedene Grundlinien in Gegenden,
in welchen noch Lücken vorhanden sind, gemessen worden. Auch mit Nivellements hat
man begonnen, und wenn auch gegenwärtig durch politische Verhältnisse ein Stillstand
eingetreten, so hoffen wir doch, dass selbiger von keiner grossen Dauer sein wird. Tele-
graphische Längenbestimmungen zwischen den Sternwarten Pulkowa—Warschau— Wien,
Odessa—Berlin u.s. w. sind ausgeführt.

In Preussen hat der Präsident des königlich preussischen geodätischen Insti-
tuts, Herr General-Lieutenant Dr. Baeyer, eine grosse Zahl trigonometrischer, astrono-
mischer und nivellitischer Arbeiten ausführen lassen, worüber eine Anzahl Publikationen
Rechenschaft giebt und über die wir im Berichte des Centralbureaus weiter hören
werden. Auch von der königlich preussischen Landes-Aufnahme sind Triangulirungs-
arbeiten veröffentlicht worden.

In Sachsen sind sowohl die geodätischen als auch die astronomischen Arbeiten
fertig und harren nur noch der Veröffentlichung. :

In Bayern sind, nachdem die Landestriangulation veröffentlicht, die nivellitischen
Arbeiten zu Ende geführt und mehrere telegraphische Längenbestimmungen zwischen
München—Leipzig, Strassburg, Genf, Bregenz, Mailand, Padua, Wien (fünf Mal) und
Prag ausgeführt worden.

In Württemberg wird die Triangulation in nächster Zeit beginnen.

 

 
