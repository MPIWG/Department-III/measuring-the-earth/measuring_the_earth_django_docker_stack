 

1 A PRY |

DUMONT VU

UT ANT

il

BT Tr

rer RB

Ti

a Rome, et en Allemagne la determination de la latitude et de l’azimut en un point
situé & peu prés au milieu du polygone Berlin—Trunz—Varsovie—Breslau.

La derniére question, relative & la jonction par voie géodésique des points
déterminés astronomiquement, ne peut guère être résolue si l’on tient compte de l’état
dans lequel se trouvent actuellement les travaux de triangulation. En fait, les réseaux
trigonométriques s'étendent sur presque tous les points auxquels ont été faites des dé-
terminations astronomiques; de plus, pour la plupart des stations astronomiques il
semble que la jonction avec ce réseau se trouve exécutée, et cependant on ne pourrait
que dans des cas très rares obtenir maintenant des pays intéressés des valeurs défini-
tivement compensées, de sorte qu'un calcul, ne fût-ce qu'approximatif, pour tous les
pays de l’Europe n’est guère possible. Comme d'autre part l’espoir subsiste de voir
dans un petit nombre d'années de nombreux progrès faits dans ce sens, je crois
pouvoir exprimer l'opinion qu'il faut laisser à l'avenir le soin de résoudre cette partie
de la question.

INPI SSL LS

M. Bruhns attire l'attention des délégués français sur le fait que les détermina-
tions de longitude en France étant toutes exécutées entre Paris et d’autres stations, il
serait bien désirable de relier ces stations directement les unes aux autres. On se con-
formerait ainsi à une décision antérieure de la Conférence, d’après laquelle on doit
pour chaque station déterminer la longitude dans trois directions différentes. On obtien-
drait en outre un vaste polygone de différences de longitude et les désidérata du rap-
porteur seraient satisfaits.

M. Perrier communique que les lacunes existant sous ce rapport en France
doivent être prochainement comblées, et qu’on se mettra à l’œuvre le plus tôt possible.

M. Ferrero annonce qu’en Italie aussi on va sans doute faire les travaux désirés.

M. Baeyer déclare que le Bureau central est tout prêt à exécuter la jonction
avec l'Est et qu'on à déjà projeté de déterminer exactement un point en latitude et
azimut entre Berlin, Trunz, Varsovie et Breslau.

M. Zech constate que rien ne s'oppose à la réalisation des vœux exprimés par
M. d'Oppolzer. Certains des travaux proposés sont commencés, d’autres sont projetés;
il ne peut donc pas subsister de doute que les désiderata développés par M. d’Oppolzer
ne soient aussi ceux de la Conférence.

Personne ne demandant plus la parole, le Président déclare le sujet épuisé, et
passe à l’objet suivant de l’ordre du jour, savoir au rapport sur l’article 4 du programme
concernant les positions des étoiles employées dans les déterminations astronomiques.

M. Bruhns, rapporteur, rappelle que le président de la Commission permanente
a confié le rapport sur ce point à M. le professeur Albrecht et a lui et que c’est après
avoir discuté à fond tous les détails de la question que les deux rapporteurs ont rédigé
la note suivante:

iat

 
