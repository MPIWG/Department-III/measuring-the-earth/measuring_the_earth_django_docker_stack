ee

 

mai rl

 

 

is
i
i
a
>
®
it
®
=
®
®
=
=
5
è
à
ns
E

4
#

169

et y, —acQ; cela ayant lieu d’autant plus exactement que les valeurs de p sont plus
petites, ou les changements de Q plus lents, on peut en conclure que ac=K, K cor-
respondant à la valeur statique de l'écart.

Or, pour le terme principal de Q, on ap = V +, ou nommant T la durée
d’oscillation du pendule,

T Be BC

Eo m ea)
icin T, ou l'espace parcouru par le son pendant le temps T, est incomparablement plus
grand que la longueur ¢ du support; le résultat ci-dessus est donc trés-exact, et l’on
peut supposer 7, — KO.

On trouverait, sauf la complication plus grande du calcul, qu'il en est encore
de même en assimilant les supports à deux prismes ou cylindres verticaux ayant par
suite un mouvement transversal et non longitudinal.

Pour étendre ce résultat au cas général, il nous faut rappeler la marche que
l'on suivrait pour intégrer les équations du mouvement vibratoire. En nommant z, Y
les coordonnées d’un point quelconque dans l’état d'équilibre, et x + w, y + v ce qu’elles
deviennent dans celui de mouvement, les équations ont la forme:

d?u d?v
ae Have ei

U et V étant fonctions linéaires des diverses dérivées partielles de wu, » par rapport &
x et y. Quant aux conditions relatives aux surfaces, il faut qu'on ait #— 0, v —0o aux
points où elles sont fixes, que la pression soit nulle aux points où elles sont libres,
et qu'elle soit égale à Q aux points où cette force agit; ces pressions dépendent d’ail-
leurs des dérivées du premier ordre de w ete. Toutes ces conditions seraient satisfaites
par des expressions de la forme:
u—WU = 2pcos(ot +p), v=o = Bg cos (a | 2),
les divers termes des sommes 2 correspondant a ceux dont se compose Q, et p, q étant
des fonctions de +, y, déterminées par des équations, différentielles ordinaires. Pour
toute autre solution des équations, en posant w— wv =u", v—v' =v", on verrait que
wu”, v” devraient satisfaire les mêmes conditions, sauf que la force Q n’agirait -plus nulle
part; il en résulterait, comme on l’a vu plus haut, que le mouvement représentée par
u = u,v =v gubsisterait seul. Nous n’en devons pas moins mentionner la forme
qu’auraient w”, v”, car c’est d’elle que dépend la propriété cherchée. On les trouverait
en assimilant w”, v” & une suite de termes de la forme Z (S cos söi+ S’sin st), ou s
est une constante, et S, S’ des fonctions de x, y; celles-ci seraient alors assujetties à
satisfaire des équations différentielles linéaires; et pour chaque terme de la somme Z
elles contiendraient des cosinus, sinus, ou exponentielles portant sur des expressions de
la forme s 6x, s B y, où B serait une” constante absolue. Dans les conditions relatives
aux limites, æ, y se trouveraient remplacées par diverses dimensions c, c, ec” du corps,
et il en résulterait une équation transcendante déterminant les diverses valeurs de s, en
22

 

 
