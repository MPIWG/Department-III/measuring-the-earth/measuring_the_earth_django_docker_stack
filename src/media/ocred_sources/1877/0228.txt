 

i
N
N
i
1
|
r
i
j

—

eee

|
|
i
i

 

216
Die Quadratsumme w, welche ein Minimum werden soll, ist

b2
men

 

2 Re 2 en) ca À
ee Te ae

Substituirt man hierin für x und z ihre Werthe aus (5), so führt die Bedingung des
Minimums zu der Gleichung:

   

   

ER

(5 nd

he u es b’) ce ie
nae j, +

Die Werthe von x und z folgen darauf aus den Gleichungen (5).
Wenn nur zwei Gruppen, die Gruppen (1) und (2), mit einander zu verbinden

sind, so fällt ¢ weg, und man erhält:

 

zn pa DE +
X Sa gp’? + pP"? " b
g''2 b'! BE b'
Die amours
6? +6 b

die verbesserte Länge von A 5 wird alsdann:

— b' + ger (d" — b’)

ß’ 19
Tr wt
ee oo

Haben beide Werthe von A B, 0’ und 5”, dieselbe Genauigkeit, oder ist das
Verhältniss ihrer Genauigkeiten nicht bekannt, so hat man 5 = P” zu setzen. Als-
dann wird

der verbesserte Werth von AB=4 + =b

Nachdem die Winkel und die Längen der geodätischen Linien in den an ein-
ander gereiheten Gruppen bestimmt worden sind, wird man die Orientirung des ganzen
Netzes aus allen darin beobachteten Azimuthen, nach Bessel’s Vorschlag (Astronomische

Nachrichten, Band 14, Seite on auf solche Weise ausführen, dass die Summe der

Quadrate der von Bessel mit 5” ;, bezeichneten Grössen ein Minimum wird.

Kiel, den 17. März 1877. CAR Pees,.

 

:
:
:
i
=

ak nl

DE tn NN 1!

à ha Me a Hd ad dal ad sus punis à:

 
