 

204

werden kann. Er zerlegte daher die Ausgleichung seines Dreiecksnetzes in zwei völlig
von einander getrennte Theile.

Der erste Theil behandelt die Beobachtungen auf den Stationen nebst ihren
wahrscheinlichen Richtungen und Gewichten.

Der zweite Theil geht von den Resultaten der Stations-Ausgleichungen aus und
behandelt die Ausgleichung des Netzes nach den geometrischen Bedingungen der ein-
zelnen Polygone und Dreiecke.

Die geometrischen Bedingungen selbst sind aber nicht mit mathematischer
Sicherheit festzustellen, denn zur Bestimmung des Excesses der Dreiecke und zur Bil-
dung der Seitengleichungen kann man nur durch die Annahme einer bestimmten Figur
der Erde gelangen, d.h. man muss für jedes Dreieck oder Polygon voraussetzen, dass
die Abweichung seiner Fläche von dem regelmässigen Sphäroid unerheblich sei. Dies
berechtigt aber sicher nicht, diese Voraussetzung auf eine ganze Dreieckskette auszu-
dehnen und zwei oder mehrere gemessene Grundlinien durch Zwangsbedingungen in
Uebereinstimmung zu bringen. Diese Uebereinstimmung kann nur dann stattfinden,
wenn die ganze Dreieckskette auf ein und demselben Sphäroid liegt. Man setzt also
bei einem solchen Verfahren voraus, dass die Krümmungsverhältnisse der Dreieckskette
schon bekannt seien, während sie doch erst durch die Gradmessung ermittelt werden
sollen. Um hier Klarheit zu schaffen, ist die Frage zu beantworten, bis zu welcher
Flächenausdehnung ist es erlaubt eine regelmässige Gestalt anzunehmen ?

Der erste Theil der Bessel’schen Methode liefert also die Resultate der Rich-
tungsbeobachtungen (Winkelmessungen) nebst ihren Gewichten. Der zweite Theil liefert
die Verbesserungen, welche an den Resultaten der Richtungsbeobachtungen (Winkel-
messungen) anzubringen sind, um das Dreiecksnetz in seinen einzelnen Theilen mathe-
matisch richtig zu stellen. Bessel wollte auf diesem Wege eine zuverlässigere Beur-
theilung der Winkelmessung in einem Dreiecksnetz schaffen, als durch den unsicheren
Fehlercomplex der einzeln beobachteten Richtungen möglich ist. Dies ist ihm auch voll-
ständig gelungen, denn seine Verbesserungen geben unter allen Umständen die Abwei-
chungen von den geometrischen Bedingungen des Netzes zu erkennen, unbekümmert
darum, ob die Instrumente gleich oder verschieden, ob die Fehlerquellen der Einzeln-
beobachtungen gross oder klein waren, ob sie sich compensirt oder nicht compensirt
haben. Mit einem Wort: sie lassen die wirklichen Fehler der Winkelmessung in einem
Dreiecksnetz in einer mit der Erfahrung befriedigend übereinstimmenden Weise er-
kennen. So habe ich in der Küstenvermessung, Seite 353, aus 311 Verbesserungen der
ausgeglichenen Richtungen den mittleren Fehler = = 0.34 gefunden, während ihn Pro-
fessor Sadebeck bei Berechnung der Bonner Grundlinie aus 65 Bestimmungen = - 0139
ermittelt hat. Das Instrument war in beiden Fällen dasselbe. Es geht hieraus hervor,
dass man auf diesem Wege zu einer sicheren Vergleichung der in den verschiedenen
Ländern gebrauchten Winkelinstrumente gelangen kann.

 

na) Tee) Se) ee 14

LM 1 mil

Lt unten!

ju mal be a Hat ll nn 1

 
