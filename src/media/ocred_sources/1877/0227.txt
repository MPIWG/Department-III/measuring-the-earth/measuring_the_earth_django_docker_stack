<i een HAT RTT |

IF

eT VT TTT

mr

DL DEPOP EN

Gruppe (3) …
EN

Gruppe (1)

Wenn man

 

 

b' + b”
2

b,

215

Den Gruppen (1) und (2) sei eine Seite 4 B, den
Gruppen (2) und (3) eine Seite CD combina à à
und es sei gefunden:

für Gruppe (1), AB = d’ mit dem mittl. Fehler ¢’

” 29 (2), AB => b" 39 29 29 39 Dr
22 39 (2), ED = c 22 29 29 9 y
29 29 (3), C D = de 39 22 29 29 a

Unter der bereits erwähnten Voraussetzung, dass
die für die einzelnen Gruppen gefundenen Winkel
zwischen den Verbindungslinien der Dreieckspunkte un-
verändert bleiben, werde angenommen, dass die Punkte
A der Gruppen (1) und (2), und die Punkte C der
Gruppen (2) und (3) zusammenfallen, und dass ausser-
dem die beiden Punkte B mit A, sowie die beiden
Punkte D mit C in derselben Richtung liegen.

Die Factoren, womit die aus der ersten Ausgleichung
hervorgehenden Entfernungen in den Gruppen 5.2)
und (3) noch zu multipliciren sind, seien 1+ 2, 1 + y
und 1 + z, so hat man die Grössen x, y, 2 so zu be-
stimmen, dass die Seiten 4. B der Gruppen (1) und (2),
sowie die Seiten CD der Gruppen (2) und (3) in
Uebereinstimmung kommen, und dass ausserdem die
Summen der Quadrate der an b, b”, c!, c” anzubringenden
Correctionen, unter Berücksichtigung der entsprechenden
Gewichte zu einem Minimum wird.

1 ce’ . ..
- — c Setzt, so werden die strenge zu erfüllenden

 

”

Bedingungsgleichungen, da b” — 0’, €" —c', x, y, z als kleine Grössen anzusehen sind,
deren Quadrate neben ihren ersten Potenzen nicht in Betracht kommen,

woraus folgt:

(5) .

baby
ER ey ie) ai ote

 

4 bre Dr

Gu ae €
ey —

 
