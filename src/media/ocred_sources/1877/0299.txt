11 FAT TT

UM AH U AU

UML MN

RR BT ni

MIE CL

DÉS NÉS Se

Italia.

La mia recente nomina a direttore del reale istituto topografico militare, mi ha
procurato l’onore di presiedere la Commissione geodetica italiana, di
vol come commissario dell’Associazione internazionale, e di continuare
incominciata dai miei predecessori generali Ricci e de’ Vecchi.

Nell’occasione di questa conferenza sono nell’obbligo di riferire l’operato e le
vicende della Commissione italiana dopo Vultima conferenza riunitasi a Dresda nel 1874.

Oltre al cambio di presidenza teste accennato, la Commissione ha subito da
quell’epoca qualche variazione. Essa é@ stata accresciuta colla nomina a membro effettivo
del capitano di fregata Magnaghi della marina reale, direttore dell’ufficio idrografico, e
menomata di un membro per la deplorata morte del chiarissimo astronomo Santini, di-
rettore dell’osservatorio di Padova. Mi ‘permetterete, onorevoli colleghi, di esprimere
a nome della scienza una parola di profondo rammarico per la perdita di una tale
sua illustrazione.

La Commissione italiana resta pertanto costituita come segue:

Sedere in mezzo a
Popera cosi bene

Professore Betocchi,

Senatore De Gasparis,
Tenente-Colonnello Ferrero (Segretario), —
Professore Lorenzoni,

Capitano di fregata Magnaghi,
Maggior-Generale Mayo (Presidente),
Professore Oberholtzer,

Professore Respighi,

Tenente-Generale Ricci,

Professore Schiaparelli,

Professore Schiavoni.

I mezzi finanziari di cui ha disposto la Commissione in questo triennio non si
possono dire considerevoli, poiché dal Ministero della pubblica istruzione fu assegnata
per i lavori della Commissione del grado la somma di lire 30,000 annue. Ma se si
considera che, grazie alla buona organizzazione della Commissione, l’opera dei membri
di essa & prestata gratuitamente, poiché i commissari son gia tutti scienziati al servizio
del regio Governo, si comprende benissimo come anche con cosi piccola somma si siano
potuti ottenere risultati soddisfacenti, essendo stata essa totalmente impiegata in acquisti
di strumenti, in lavori di campagna e di stampa, senza alcuna spesa di stipendi e di
amministrazione.

La Commissione italiana possiede una dotazione di strumenti che appare dalla
seguente tabella.

 
