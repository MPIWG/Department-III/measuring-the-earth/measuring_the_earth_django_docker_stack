 

 

14 MOUVEMENT SIMULTANE

L'on peut cependant signaler une différence entre ces deux mouvements:
dans les oscillations du pendule, Pamplitude totale du mouvement
du support, telle qu’elle est accusée par les limites de lexcur-
sion de l'échelle, est produite par une force horizontale qui varie dans
l'espace de */, de seconde, el par degrés insensibles sans aucune se-
cousse, de — > à + . Sl > est la composante horizontale du poids
du pendule correspondant à la déviation maximum de la verticale. Dans
la reproduction artificielle de ce mouvement à laide de poids, lorsqu’a
chaque battement alternatif du métronome l’aide laisse retomber le go-
det, après l'avoir soulevé, il en résulle une secousse très-appréciable
dans la lunette: cette secousse donne même lieu à des vibrations dans
l'appareil, qui rendent l'observation sensiblement plus difficile, dès que
le poids atteint ou dépasse 100 grammes.

Les circonstances ne sont donc pas identiques dans les deux modes
d’experimentalion, et si, comme c’est le cas, on trouve systématiquement
une valeur de & plus forte dans le mouvement du support produit arti-
ficiellement par les poids, il est permis de supposer que l’excursion est
augmentée par la force vive développée par la chute du godet. Cette idée
m'est venue à la suite des expériences faites à Berlin, d’après lesquelles
le rapport & est trés-sensiblement plus fort dans le mode d’experimen-
tation avec les poids que celui qui est donné par les oscillations du pen-
dule, différence que j'ai cru pouvoir attribuer à la cause suivante.

A Berlin, l'installation du pendule dans la grande salle du compara-
teur, située dans le bâtiment affecté à la commission des poids et me-
sures de l'empire allemand, était à plusieurs égards loin d’être favora-
ble. Cette salle présente, il est vrai, sous le rapport d’une constance très-
remarquable de la température, un grand avantage pour les observations
du pendule, mais comme cette constance résulte, indépendamment de
ses grandes dimensions, de Pabsence complète de loute ouverture, ou
jour sur l'extérieur, tous les préparatifs préliminaires d'installation et
d'ajustement sont singulièrement compliqués par la nécessité de recou-
rir en tout à la lumière artificielle, l'obscurité y étant complète; de plus,

 

 

x
=
=
=
=
a
=
®
=
x
=
x
U

ah nik

om!

ja hall a u haha nn 1:

PPFESEEFFPFEGGBEFRRRESTEN

 
