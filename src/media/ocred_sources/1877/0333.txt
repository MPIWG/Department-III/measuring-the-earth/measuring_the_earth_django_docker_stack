i D'UN PENDULE ET DE SES SUPPORTS. 7
c’est-à-dire allongé le bras de levier de 3™™, on répétait la méme série
d’experiences, afin @’obtenir Pexcursion e de l'échelle résultant du méme
poids, mais avec un bras de levier a + 3™™; E et e élant également ex-
primés en millimètres, on à a — on Si D représente la somme des
distances du miroir à l'échelle, et du miroir à la lunette, le grossisse-
ment sera —, et par suite — le facteur par lequel il faut multiplier
l’excursion observée sur l'échelle, pour avoir la déviation correspon-
dante du plan de suspension. Je faisais ordinairement une seconde dé-
termination de E après avoir ramené le coulisseau dans sa position pri-
mitive, au commencement, ou à la fin d’une série d'expériences; dans les
derniers temps je faisais même une détermination complète du grossis-
sement avant et après chaque série d'expériences, el jen prenais la
moyenne. L'on ne peut pas compler en effet sur une invariabilité de
l'appareil, telle que la longueur du bras de levier reste absolument con-
stante pendant un intervalle de temps plus ou moins long; par suite de
tassements, de changements de température, etc., il peut se produire de
très-légères variations dans la longueur du bras de levier, qui donnent
lieu à des variations appréciables, quoique très-faibles, dans la valeur du
orossissement.

Dès le début des essais, j'ai reconnu que la déviation du plan de sus-
pension, produite par une traction horizontale correspondant au poids
d’un kilogramme, ne s'opérait pas dans toute son étendue, immédiale-
ment après l'application de la force. On comprend facilement que la
traction opérée par un poids d’un kilogramme ne pouvait pas avoir
= lieu brusquement, comme cela aurait eu lieu si on l'avait abandonné à

lui-même, en le laissant tomber même de la trés-faible hauteur à laquelle
il se trouvait, lorsque le fil n’était pas encore tendu, le poids étant sup-
porté. Il en serait résulté une secousse qui aurait pu être préjudiciable
à l'instrument, et qui aurait donné lieu, en outre, à des vibrations de l’ap- |
pareil telles que la lecture de l'échelle n'aurait guère été possible. La : |
traction s'opérait graduellement en accompagnant le poids de la main, |
de telle façon que la tension du fil ne fût complete qu’au bout de deux

 

RE NE |

UL RL I HU

(UL

ja!

I KR

a

 
