 

Sp Sn

TL TT PRUE UT eT

FU

WITT

Ei i

2. Le rapport de la Commission spéciale nommée pour s'occuper des ar-

ticles 58 et 5b du programme de la réunion de Bruxelles a été envoyé à tous les dé-
légués au commencement du mois de juillet de cette année. MM. les délégués. ont été
en même temps priés de communiquer leur votes au Bureau central. Les votes reçus
ont été réunis, imprimés et expédiés aux délégués.

3. A propos de la question du pendule, il a été envoyé le 8 janvier de cette
année une circulaire nouvelle à MM. Ibanez, Forsch, de Vecchi, Faye,
Stamkart. Ont répondu: MM. Ibanez, de Vecchi et Plantamour.

M. le professeur Plantamour a Vintention de se rendre cet automne à Berlin
avec son appareil de pendule pour l’expérimenter à la place où ont été faites les obser-
vations de Bessel.*) M. le général Ibanez ne pourra se rendre à Berlin que l’année
prochaine. M. de Vecchi répond que la Commission italienne n’est pas encore en pos-
session d'un appareil de pendule.

L'œuvre de l'Association géodésique, la mesure des degrés en Europe, a fait
depuis sa fondation des progrès tellement considérables, et le champs des recherches
qu'elle à provoquées dans le domaine de la géodésie et de la physique du globe s’est
tellement étendu d'année en année, qu’il devient de plus en plus difficile pour un seule
homme, quelque soit sa force de travail, d’embrasser et de connaître à fond toutes ces
branches d'études. Quant à moi, je sens depuis longtemps que mes faibles forces ne
suffisent plus seules, et surtout lorsque j'avais de nouvelles propositions à faire, j'ai
ressenti le besoin de les appuyer sur l'étude et le préavis d'hommes compétents.

Il est vrai qu'il serait possible d’en appeler, dans chaque cas particulier, à une
Commission spéciale, comme cela a été fait dans la Conférence de Bruxelles: toutefois
ce moyen est compliqué et fait perdre du temps; il n’y a pas de doute que le but aurait
été atteint bien plus facilement et plus nettement par une discussion verbale au sein
d’une Commission, que par des rapports indépendants de différents membres.

C'est par ces considérations que l’idée a pris naissance de m’entourer d’une
Commission formée de savants compétents dans les différentes sciences qui nous inté-
ressent. Une pareille Commission assurerait l'exécution des travaux qui incombent à
l’Institut géodésique, de manière qu’il se trouverait à la hauteur de la Science; elle
offrirait à la Commission permanente toutes les garanties désirables. tandisque actuelle-
ment cette garantie ne repose que sur la confiance personnelle que messieurs les délé-
gués ont bien voulu m'accorder, confiance dont je leur suis profondément reconnaissant,
mais à laquelle je crains de né pas pouvoir satisfaire toujours comme je le désirerais.

Partant de ces idées, j'ai soumis à mon chef, Monsieur le Ministre de l'instruction,
un mémoire développant la proposition de mettre l’Institut géodésique en relation directe
avec l'Académie royale des sciences. Monsieur le Ministre accueillit ces vues et, l’Aca-

Plantamour et

*) M. le professeur Pluntamour a, dans le courant du mois d’octobre, fait des observations avec
son pendule dans la salle du comparateur de la Commission impériale pour les poids et mesures. Ses
observations ont de nouveau confirmé l'influence très marquée des oscillations du trépied sur la durée des

oscillations du pendule. 5

13

 
