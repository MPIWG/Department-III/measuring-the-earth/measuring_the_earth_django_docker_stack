Tem

Km AR

3

  

a ITF BOTT Tr

195

presenta lapprezzabilissimo vantaggio che, se per un accidente qualunque una matita
cessa di segnare, la rispettiva curva pud essere completata esattamente col sus-
sidio dell’altra.

E cid. per riguardo ai mareografi di gid esistenti, mareografi che ho descritto
nella precedente memoria citata in principio.

Ad essi, innanzi che termini il corrente anno, due nuovi verranno aggiunti.
L’uno nel porto di Napoli, l’altro alla estrema punta d’Italia fra il Tirreno e lo Jonio
a Reggio di Calabria. Il primo ad esclusiva cura e spesa del Ministero dei lavori pub-
blici; il secondo a cure e spese della Commissione italiana per la misura dei gradi.
Ambedue i meccanismi, perfettamente eguali a quello di Ravenna modificato. nel modo
sopradescritto, vengono costruiti dallo stesso meccanico Augusto Ricci, e sono gia in
istato di esecuzione inoltrata. Col 30 novembre venturo saranno consegnati a posto, e
col primo gennaio 1878 funzioneranno regolarmente,

A queste notizie relative ai mareografi dipendenti dal Ministero dei lavori pub-
blici altre potrei aggiungerne relative a quelli dipendenti dal Ministero della marina. Me
ne dispenso perd essendo argomento di particolare competenza dell’egregio collega ca-
valiere Giovanni Battista Magnaghi, direttore dell ufficio idrografico della regia Marina,
al quale vengono trasmesse le rispettive rappresentazioni mareografiche.

Diro soltanto come abbia avuto occasione di esaminare da vicino tre di questi
meccanismi. Il primo funziona nel porto di Ancona fin dal 1873; il secondo agli Albe-
roni presso Venezia, attivato anch’esso all’epoca suddetta; il terzo fu da me esaminato in
Genova, presso il sopradetto ufficio idrografico: & il mareografo di Sir William Thomson.
{1 meccanismo dei due primi non differenzia gran fatto da quelli che ho descritti nella
memoria pit. volte citata. La principale differenza !consiste in cid che la carta sulla
quale vengono descritte le curve delle marée, anziché scorrere sopra un tavolo orizzon-
tale, ed essere cambiata a periodi di pit giorni, @ avvolta invece ad un cilindro oriz-
zontale, ed ordinariamente si cambia ad ogni ventiquattro ore. Il terzo mareografo é
quello che maggiormente si discosta da tutti gli altri fin qui enumerati. Consiste in
una macchinetta di estrema precisione e delicatezza costruita appositamente in Inghil-
terra, alla quale nessun altro addebito potrebbe imputarsgi se non che il richiedere cure
diligenti e delicate non facili ad ottenersi in ogni luogo e da qualsivoglia impiegato
subalterno. Ma queste cure e diligenze non possono fargli difetto in uno stabilimento
scientifico qual & l’Istituto idrografico di Genova, pel quale @ eminentemente acconcia.
Anche in questo mareografo la carta & avvolta ad un cilindro, che perd, a differenza
dei precedenti, & collocato verticalmente,

Riandando le cose esposte, si raccoglie come lungo le coste italiane col primo
gennaio 1878 saranno in attivita, sul Mediterraneo, i mareografi di Genova, Livorno e
Napoli; al vertice fra il Tirreno e lo Jonio il mareografo di Reggio di Calabria; nel-
’Adriatico quelli di Ancona, porto Corsini di Ravenna, Alberoni, Lido e Venezia. Sono
in tutto nove mareografi, sei dei quali funzionando da parecchi anni, trovasi gid accu-
mulata la ricca messe di osservazioni mareografiche che vengo attualmente vagliando e
calcolando, e sulla quale conto di pubblicare fra breve pit’ estesa memoria.

25*

 
