| DUN PENDULE ET DE SES SUPPORTS. 11
que limite. Pour obvier 4 cet inconvénient, jai fait faire une autre échelle
portant un certain nombre d’espaces blanes, d'une erandeur déterminée
se détachant sur un fond noir qui les séparait. Ces espaces blancs étaient
au nombre de 14, échelonnés entre 20m et 3m, et j'avais demandé que
la largeur de chacun d'eux fût d’un nombre rond de millimètres que
j'avais indiqué. Cette échelle était mobile dans la direction de la verli-
cale, à l'aide d’une crémaillère et d’une vis de rappel, en sorte que Pob-
servateur pouvait, sans ôter Fœil de la lunette, Vabaisser ou Vélever a
volonté, de façon à placer un point quelconque sous le fil. Avec celte
disposition lon n’avait pas à noter le nombre de millimètres, ainsi que la
fraction correspondante, à chaque limite de l'excursion du fil sur lé-
chelle, mais seulement à observer si le fil atteignait exactement les limi-
tes de tel ou tel espace. Cette observation pouvait se faire avec une assez
erande précision, vu la manière dont les espaces blancs se détachaient
sur un fond noir: au moment où lexcursion de l'échelle coïncidait avec
l’un des espaces, l'amplitude de l'arc d’oscillation était observée dans une
autre lunette placée à angle droit de la premiére, et à une distance de 5
à 6 mètres du pendule.

L'observation se faisait ensuite sur un autre espace blanc, qui était
amené sous le fil à l'aide de la vis de rappel et de la crémaillère; l'ordre
dans lequel les différents espaces étaient successivement amenés étall
celui de leur grandeur, en commençant par les plus grands, et afin de ne
3 pas attendre trop longtemps, jusqu'à ce que, par la diminution de larc
d'amplitude des oscillations, le mouvement du support fat réduit des li-
mites de l’un des espaces à celles du suivant, un aide placé près du pen-
dule amortissait le mouvement du pendule avec un corps mou, tel qu'une
barbe de plume, de manière à ce que les limites du nouvel espace ne fus-
sent que très-légèrement dépassées. Lorsque ces limites étaient atteintes,
l'observateur qui suivait l'excursion de l'échelle donnait le signal pour
que l'amplitude de Farc d'oscillation fût notée par l’autre observateur.
La série des espaces blancs étant terminée dans une des positions du
pendule, lon en recommençait une seconde, une lroisième, etc., après

“i bot PPP, THT TTT in |

am) LI

NT

il

Teen tir it

 

 
