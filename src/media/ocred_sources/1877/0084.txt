 

i
| .
A
I

12

In
Norwegen

sind schon seit einigen Jahren zwei registrirende Pegel in Wirksamkeit, der eine bei
Drontheim, der andere im Christiania-Fjord.
In
Dänemark

geht man mit der Errichtung einer nautischen Sektion für Meteorologie vor, welcher
sämmtliche Messungen des Wasserstandes an den Küsten, sowie die wissenschaftliche
Bearbeitung dieser Beobachtungen übertragen werden.
In
Belgien

arbeitet ein Mareograph in Ostende. Vermittelst des belgischen Nivellements, dessen
Vollendung für 1877 vorgesehen ist, hat derselbe eine Verbindung mit dem Amsterdamer
Pegel, welcher wiederum durch das holländische Nivellement Amsterdam—Salzbergen
mit dem preussischen Salzbergen—Swinemünde und mit der Ostsee zusammenhängt.
Aus diesen Nivellements ergiebt sich, dass das mittlere Meeresniveau bei Ostende um
0273 + 0,03 über dem bisher für die Meereshöhe in Marseille angenommenen Niveau
liegt. Unter dem mittleren Meeresniveau von Ostende liegt das mittlere Meeresniveau zu

Amsterdam um 0023
Swinemünde „ 0.048
Marseille 08.10.

Durch mehrere Anschlüsse des preussischen Nivellements an das französische
ergiebt sich ziemlich übereinstimmend, dass Marseille um 0,74 unter Swinemünde liegt,
d.h. wenn man vom mittleren Niveau in Marseille ausgeht, so findet man das mittlere
Niveau in Swinemünde 0,74 statt 0.

Nach den dem Nivellement de France entlehnten Angaben des Nivellement de
Suisse, 5. 68, würde das mittlere Niveau im Atlantischen Meere im Canal um 0,80
höher liegen, als das mittlere Niveau im Hafen zu Marseille. (Siehe Verhandlungen in
Paris 1875, die Mittheilung von Breton de Champ, pag. 11 etc. und 49 ete.)*)

Bei der Zusammenstellung der einzelnen Nivellements findet man, dass die gegen-
seitigen Anschlüsse überall entweder schon ausgeführt oder wenigstens in sichere Aus-
sicht genommen sind. "

Desgleichen finden wir, dass der Aufforderung, betreffend die Aufstellung von
Mareographen, welche die letzte Generalconferenz an die betreffenden Regierungen ge-

*) Diese Bestimmungen der Niveau-Differenzen der verschiedenen Meere sind jedenfalls nur als
provisorische zu betrachten, da die mittleren Meereshöhen in den betreffenden Häfen, namentlich in Mar-
seille, noch nicht definitiv festgestellt und die Nivellements noch nicht ausgeglichen sind. Hirsch.

 

x
=
3

=
=
=
=

a
=
=

=

=

x

=
a

Mm

tt

Ih niki ih il

hb Ti to Wey

js ahah u addi ae 4 ise
