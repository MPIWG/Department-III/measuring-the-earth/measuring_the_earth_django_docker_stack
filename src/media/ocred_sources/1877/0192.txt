 

 

 

180

provenant de la reduction du centre du couteau à l’extrémité anterieure, il s’ensuit que

la torsion du support par cette force est 2 = 0.0000290 — 5:98. Bien qu'il n’y
ait rien de suspect dans ce résultat, j'ai voulu le contrôler par une expérience directe :
j'ai fixé un miroir à l’extrémité du plan de suspension, et, à l’aide d’une lunette, j'ai
mesuré la torsion par la réflexion d’une échelle, et je lai trouvée de 6". Cette méthode
n’a naturellement pas l’exactitude de l’autre.

Pour arriver à une autre confirmation de la théorie, j'ai fait les observations
suivantes sur la flexion produite par l’oscillation du pendule lui-même dans ses deux
positions, en me servant pour cela d'un microscope assez fort (c’est-à-dire d’un gros-
sissement de 500 diamètres). L'échelle employée a été faite par M. Rogers de l’Obser-
vatoire de Harvard Collège. Elle est divisée avec une exactitude extrême, l'intervalle

entre deux traits étant de (m) d’un pouce anglais. Elle était fixée & 70 millimetres
en avant du centre du couteau, ce qui donne une correction à £ de + 0770019. Si D
est l'amplitude de l’oscillation de part et d’autre de la verticale, l'amplitude double de
la vibration de l’échelle doit étre
2M (= + 0m™0019) 4 ®,

17

dans laquelle M — 6.25 et 2 ZT oy 22) Selon que le pendule est suspendu par le

9
56 56°
couteau le plus rapproché ou le plus éloigné du centre de gravité. C’est la formule
dont je me suis servi pour calculer les quantités données ci-contre.

Hoboken, le 20 mars 1877.

A, Le pendule étant suspendu par le couteau le plus éloigné du centre de gravité.

 

 

AMPLITUDE
du mouvement de l'échelle
& ee
- 4000 un
observée | calculée
P- Pp.
2015 62 2.2 2.2
2 30 2:1 2.1
2 24 2.0 2.1
2022 1.9 2.0
2:2 20 1.9 2.0
2 A9 1.95 2.0
1 43 1.5 1.5
0 47 0.8 0.7

 

x
=
=
ä
a
=
=
à
x
u

a ml

WU mar)

us had Len eh nn 11:
