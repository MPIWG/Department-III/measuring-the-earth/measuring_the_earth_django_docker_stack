 

18 MOUVEMENT SIMULTANE

sur le même support sur lequel il avait été installé dans les observations
déjà publiées, faites dans les années 1865, 1866 et 1874. C'est un sup-
port en bois, qui sert ordinairement à l'installation d’une lunette de Dol-
lond, de trois pouces d'ouverture, appartenant à l'Observatoire, et malgré
l'inconvénient que pouvait présenter le bois, au point de vue de Pélas-
licité, il était @une construction tellement massive, surtout en compa-
raison de la frêle apparence des tubes creux en laiton du trépied, que je

n'avais pas hésité à m’en servir, parce qu’il avait exactement les dimen-

sions requises. Ce support était formé d’un plateau triangulaire en noyer,
de 70cm de coté et de 9e™,5 d'épaisseur, porté par trois pieds carrés, de
noyer également, chaque côté mesurant 10cm,5, ces pieds étaient inclinés
et reliés dans leur partie inférieure par de fortes traverses en noyer lar-
ges de 40cm,5 et d’une épaisseur de 7em,5. Il reposait enfin sur le sol
dallé et voûté de la salle par trois fortes vis de calage, en fer, de 2tm de
diamètre, l'écartement entre les vis était de 1,025, et la hauteur de
la surface supérieure du plateau au-dessus du sol de 62°". Après
avoir terminé les expériences relatives au mouvement du plan de sus-
pension, le pendule étant placé sur ce support, je l'ai enlevé, et après
avoir écarté le support, je l'ai replacé au même endroit, mais le trépied
métallique reposant directement sur le sol de la salle, afin de déterminer,
par la comparaison avec les mêmes expériences répétées dans cette nou-
velle position, l'influence que pouvait avoir le support en bois. Cette in-
fluence est certainement très-sensible, car, comme on peut le voir par
les tableaux des observations qui suivent, le mouvement du plan de sus-
pension est réduit à peu près dans le rapport de 4 : 3, lorsque le pen-
dule est placé directement sur le sol, au lieu d’être installé sur le sup-
port en bois.

Ces expériences du mois de septembre ont été faites dans des circon-
stances qui n'étaient malheureusement pas les mêmes que celles dans
lesquelles les observations du pendule, en vue de la détermination de la
pesanteur, sont exécutées, c'est-à-dire lecomparateur étant en place à côté
du pendule. J'avais en effet enlevé le comparateur dans le but d'obtenir

 

x
=
=
a
a
=
:

11811 :1188

nr a.

u

{MMe Laid

 

|
|

 
