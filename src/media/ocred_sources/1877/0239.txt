ee I |

du he ARE

In!

Jena

PO PP ENT RAT BR TIER I

Dritter Abschnitt,

Bemerkungen und Ergänzungen des Vorsitzenden zu den erstatteten
Gutachten der Herren Commissions-Mitglieder.

sl

Es dürfte nützlich sein, die Fragen, um die es sich handelt hier, wieder auf
ihre einfachste Form zurückzuführen. —

Thatsache ist, dass die Netzausgleichungen seit dem Bestehen der Gradmessung
gruppenweise ausgeführt werden. Jede Gruppe bildet einen in sich abgeschlossenen
Theil. — Was unter Gruppen zu verstehen ist, wird am besten durch das Aufzählen
einiger derselben klar werden.

1. Gruppe — 31 Bedingungsgleichungen.

Gradmessung in Ostpreussen mit der Königsberger Grundlinie und der Anschlussseite :
Memel—Lepaizi an die russische Grundlinie bei Polangen.

2. Gruppe — 86 Bedingungsgleichungen.

Küstenvermessung mit der Berliner Grundlinie und den Anschlussseiten :
Trunz—Wildenhof (Fehler a) an die vorige Gruppe, und

radar a 1 a 1
Darsserort—Hiddensce (Fehler pat

3. Gruppe — 59 Bedingungsgleichungen.
Verbindung des preussischen und russischen Dreiecke in Schlesien mit der Strehlener
Grundlinie und den Anschlussseiten :
Markowiece—Trockenberg (Fehler 600) an die russische Grundlinie bei
Czenstochau, und

) an die dänische Gradmessung.

Schneeberg—Spitzberg (Fehler 5) an die österreichische Grundlinie bei
Josephstadt.
4. Oesterreichische Gruppe in Böhmen — 64 Bedingungsgleichungen,

mit der Grundlinie bei Josephstadt und den Anschlussseiten an die vorhergehende
Gruppe und an die übrigen Gruppen in Böhmen und Mähren.

 
