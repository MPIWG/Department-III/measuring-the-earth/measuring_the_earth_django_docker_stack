 

Ei Hemer WITT en BER

 

vl

Takt are

zii

pen

IR RR ng lem i

ir

 

69

Rapport dw Seerdtaire perpeluel ü la Gonference.de 1894 (Innsbruck).

« Parmi les plus aneiens delegues de l’Associalion, la mort nous a enleve de nou-
veau deux savants confreres. Quelques mois apres notre reunion en Suisse, nous avons
perdu Rodolphe Wolf, Villustre astronome de Zurich, decöd6 le 6 decembre 1895 ä läge de
soixante-dix-sept ans. La perte de cet infatigable travailleur, annoncde par la circulaire du

411 decembre aux delegues de l’Association, a et douloureusement ressenlie par tous les

astronomes et physiciens, qui appreciaient grandement ses mörites et son erudition ; elle

est parlieuliörement regrettee par la Gommission geodesique suisse qu'il a presidee pendanı

Irente anndes avec tact et competence, et pour laquelle il a &eril, comme introduction de
ses publications, le savant memoire sur l’Iisloire des Mesures geodesiques en Suisse (Geschichte
der Vermessungen in der Schweiz), travail dans lequel Wolf a montre& les m&emes qualites
d’erudition consciencieuse qui distinguent & un si haut degre son celebre ouvrage de I’ Histoire
de l Astronomie.

« Ce n’est pas ici la place de relever les beaux travaux de Wolf en astronomie, sur-
tout ses infatigables recherches sur les taches solaires. Du reste, plusieurs n&crologies, qui
ont paru en Suisse et dans les revues scienlifiques, ont justement eelebre les grands merites
du savant astronome et de ’homme aimable et modeste qui, bien qu’il ne frequentät pas nos
conförences, comptait aussi dans l’Association g&odesique de nombreux et fideles amis.

« Nous avons ensuite appris tout recemment le deees de M. le Dr G.-M. von
Bauernfeind, ancien direeteur de l’Ecole polytechnique de Munich, mort le 2 aoüt dernier, &
’äge avanc& de soixante-seize ans. Apres avoir travaille pendant quelque temps comme inge-
nieur de chemin de fer, Bauernfeind entra comme professeur A l’Eeole polylechnique de Mu-
nich, qu’il r&organisa en 1869 et dirigea jusqu’ä sa retraite en 18%.

« Bauernfeind a publie en 1858 un traite de geodesie ei de geometrie pratique
(Elemente der Vermessungskunde), ouyrage sı bien appreeie qu’il en a paru sepi @ditions.
En 1862, Bauernfeind a publi& un travail important sur Vexactitude des mesures hypsome-
triques au moyen du barometre, qui — bien que ses conclusions alent Et& modifiees sous
plusieurs rapports par des recherches ultörieures — a eu le. me6rite de soumeltre la formule
barometrique et ses conslanles au contröle des observations et des exp£riences. Äntre autres
monographies de Bauernfeind, nous citerons encore un Lravail de 1880 sur la relraction
terrestre (Ergebnisse aus Beobachtungen der lerrestrischen Refraclion), objet sur lequel il a
fait plusieurs communications a l’Association g6odesique. Gelle-ci lui doit surtout le « Nivel-
lement de preeision de la Baviere », qu’il a dirig& avec beancoup de soin et dont ıl a publie
les resultats dans plusieurs fascieules, de 1876 A 1888, qui ont paru dans les M&moires de
’Academie de Munich.

« Pour conlinuer les renseignemenis sur le personnel de l’Associalion geodesique, le
Seeretaire annonce que M. Constantin Carusso a donne sa demission de dölecue de la Grece,
dont le gouvernement a formellement aceredit& M. le colonel Hartl aupres de l’Association.
La Commission geodesique italienne a regu plusieurs membres nouveaux, qui comptent desor-
mais aussi parmi les delegues de l’Association. M. le general Ferrero ayant abandonne la

 

€

 
