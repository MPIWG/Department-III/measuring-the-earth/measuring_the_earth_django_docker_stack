 

Darauf hält der Herr Bürgermeister Dr. Mörz folgende Ansprache :
« Hochansehnliche Versammlung !

« Es gereicht mir zur hohen Ehre, als Bürgermeister der Stadt Innsbruck die Gon-
ferenz hier begrüssen zu können.

« Ich begrüsse die Gonferenz als Vereinigung jener grossen Männer der Wissenschaft,
welche dank ihrer Gelehrsamkeit von ihren Regierungen berufen worden sind, diese Wissen-
schaft zu stets höherer Blüthe zu bringen, als Vereinigung jener Gelehrten, deren Ruf weit
über die Grenzen des eigenen Landes gedrungen, deren Ruf international ist, wie das Gebiet
ihrer Forschung.

« Von weiten Landen, aus Westen und Osten, von Nord und Süd sind Sie hergekom-
men, um die reichen Erfahrungen Ihres Wissens einander mitzutheilen und in den Dienst
der Wissenschaft zu stellen, welche allen Völkern der Erde zu Gute kommt.

« Dass Sie bei der Wahl des Ortes Ihrer Versammlung Innsbruck, das kleine Städt-
chen am Inn, auserkoren, gereicht uns zur höchsten Ehre und zur grossen Freude.

« Die Bewohner Innsbrucks können allerdings nichts bieten als die Schönheiten der
Natur.

«Im Namen der Stadt Innsbruck entbiete ich der CGonferenz ein herzliches « Will-
kommen ».

Der Präsident Herr Faye antwortet auf diese verschiedenen Reden in folgender Weise:
« Excellenz !

« Vor Allem habe ich den tiefsten Dank Ihnen, hochverehrter Herr Statthalter, aus-
zusprechen für den freundlichen Empfang den Sie uns im Namen der k. k. Regierung ge-
währen. Ich danke Ihnen ganz besonders für die überaus liebenswürdige Ansprache, die wir
stets in treuem Gedächtniss bewahren werden.

« Wir danken ferner der Universität dieser schönen Stadt, welche uns aufnimmt
und einen der grössten Säle ihres Palastes uns zu Verfügung stellt.

«Wir sind endlich dem Herrn Rector und dem Herrn Bürgermeister zu Dank ver-
pflichtet, welche unserer Eröffnungs-Sitzung beiwohnen und uns freundlich begrüsst haben.

« In der Vergangenheit haben wir bereits der Gastfreundschaft der k. k. Regierung
von Oesterreich uns erfreut, zuerst in der Reichs-Hauptstadt and alsdann in Salzburg, der
Vaterstadt Mozart’s.

« Heute sind wir in der Hauptstadt Tirol’s vereinigt, in Mitten der schönen und mäch-
tigen Gebirge, welche dieselbe wie ein undurchdringlicher Wall umgeben.

« Doch nein, ich irre mich : die moderne Wissenschaft und die österreichischen In-
genieure haben durch gewaltige Tunnel diese herrliche Provinz für den Rest Europa’s zu-
gänglich gemacht, welche von keinem anderen Lande der Weit an grossartiger Schönheit
übertroffen wird.

 

dla ku di

 

nr

rl.) .LUhdiakd.dı Ada

9 id

ul

all yulaikul

Gauunkamadkil sb. au Aledkdu Al

 
