 

224
verlical) ne sont pas paralleles aux aretes des couleaux. Par ex., avec le poids lourd en bas,
l’axe du mouvement est incline sur le plan d’appui de 1770.

Je erois cependant pouvoir conclure que la longueur du pendule ä seconde, obtenue
en 1885-86, n’est pas en defaut A cause de la flexion pour plus de trois ou qualre microns,
et qu’elle nexige pas une correction sensible en raison du glissement des couleaux sur le plan
d’appui, ou par suite d’inexaclitudes dans la mesure de la distance reciproque des couteaux.

Les details des experiences se trouvent exposes dans la note qui porle le Litre
LORENZONI : Nuovo esame delle condizioni del supporto nelle esperienze [alle a Padova nel
1885-86, etc. — Venezia 1893.

 

|
i
|

 
