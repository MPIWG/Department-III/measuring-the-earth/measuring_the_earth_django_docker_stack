 

UI TTHTERT en Knete

ep

Fr

ae

een

105
obtenir des indications sur le procede qu’il convient de suivre dans cette recherche. M. von
Sterneck fait circuler une carte qui montre la distribution de ces stations.

Cette annee encore, un certain nombre d’appareils de pendule ont et& construits A
Vienne, destines la plupart pour l’etranger ; avant leur expedition, on a determine ä l’Institut
geographique militaire les constantes et les temps d’oscillation de ces pendules.

M. le President vemercie MM. les delegues de l’Autriche-Hongrie pour leurs interes-
sants rapports et, suivant l’ordre alphabötique des pays, il donne la parole a M. le professeur
Hard, delögud du Grand-Duche de Bade, qui communique quelques details sur des mesures
de pendule ex&cutees au moyen de quatre appareils de Sterneck dans treize stations ol l’on
a fait des mesures relatives de pesanteur. (Voir Annexe B. II.)

M. le colonel Henneguwin presente ensuite le rapport de la Belgique. (Voir Annexe
B. Il.)

M. le colonel von Zacharie lit le rapport sur les travaux g&odesiques ex&cutes en
Danemark pendant l’annee 1894. (Voir Annexe B. IV.)

M. le Secretaire fait lecture du rapport que M. le general Derrecagaix a envoy& au
President sur les travaux ex&cutes par le Service geographique de France, d’octobre 1893 A
septembre 1894, en ’accompagnant du premier fascicule du Tome XV du Memorial de la
Guerre, a deposer sur le bureau. (Voir Annexe B. Va.)

M. Lallemand fait un rapport oral sur les travaux du Service de Nivellement general
de la France en 1894. Il enverva au Seerötaire, & temps pour la publication dans les Gomptes-
Rendus, son rapport complete jusqu’ä la fin de l’annee. (Voir Annexe B. V».)

M. le colonel von Sterneck parle des travaux ex6deules en Grece sous la direction de
M. le colonel Hartl et annonce que ce dernier enverra son rapport au Seer6taire pour les
Gomptes-Rendus. (Voir Annexe B. Vl.)

En Pabsence de M. Rümker, qui n’a pu assister A la Conference, M. Ferster desire
en son nom communiquer quelques renseignements sur les nouvelles acquisitions d’instru-
ments pour son observatoire, qui le mettenten &tat d’y exdceuter dösormais des determinations
de latitude et de longitude avee tout le degr6 de präcision exig&e par l’Association. On a
accorde & l’Observatoire de Hambourg une lunetie de passage avec micromeötre de Repsold et
avec Vinstallation pour P’emploi de la methode Horrebow-Taleott.

Pendant qu’il a la parole, M. Forster tient & reparer un petit oubli qu’il a fait lors
de la leeture du rapport de M. von Sterneck au sujet des declinaisons des &toiles fondamen-
tales. Les positions moyennes de ces £&toiles devraient &tre donnees par deux &phemerides
differentes : la premiere qui conserve la m&me position initiale et la mäme equalion de mouve-
ment pour chaque &toile comme base permanente d’une reetification continue de ces nom-
bres au moyen de nouvelles observations fondamentales; ensuite la seconde, qui s’adapte

ASSOCIATION GEODESIQUE — 1%

 
