ET ee en

 

mananert u rar

ze)

ERHTTERTERaE

mwrg

 

117

observees dans la r&gion de Moscou, est mise en eirculation aupres des membres de l’as-
semblee !.
M. Hirsch presente ensuite le rapport sur les travaux de la Commission geodesique

suisse. (Voir Annexe B. XI.)
Il distribue des exemplaires du Proc&s-verbal de la derniere sdance de cette (om-

Inission et il fait remarquer d’une maniere particuliere les resultats d’un travail que M.L.
Du Pasquier a ex&cute sur la demande de la Commission pour caleuler l’attraction des mon-
tagnes sur la direction de la verticale dans un certain nombre de stations situ&es pres du
möridien de Neuchätel. Il en ressort le r&sultat important que, sauf pour une des stations
(Middes), l’action des masses visibles rend parfaitement compte des deviations observees dans

les limites des erreurs d’observation et de ealeul.

Pour clore la serie des rapports, M. d’Arrillaga communique celui de Institut g&o-
graphique et stalistique d’Espagne. (Voir Annexe B. X11.)

M. von Sterneck demande la parole au sujet d’une remarque contenue dans le rapport
lu par M. Albrecht, oü il a signale les bons rösultats du micromätre enregistreur de Repsold
pour l’observation des passages des &toiles; il desire constater que la premiere idee de l’em-
ploi de cette excellente methode est due ä M. le Dr Carl Braun, P. S. I., ancien directeur de
l’Observatoire de Kalocsa, dont il tient ä rappeler le merite.

M. von Kalmar appuie chaleureusement cette reclamation de priorite.

M. Helmert, sans contester la priorit du principe, voudrait cependant faire remar-
quer que l’idee de Braun est restee longtemps sans profit pour la science, Jusqu’a ce que
Repsold ait röussi & l’ex&euter d’une maniere pralique.

M. le President croit qu’il ne reste plus & l’ordre du jour que la fixation du lieuet
de l’epoque de la prochaine Conference eöndrale et il estime que, d’apres les precedents de
l’Association, lorsqu’il s’agit d’en modifier ou renouveler l’organisation, il va de soi que la
Conference se r&unisse & Berlin.

Quant & l’epoque, M. Hirsch croit que les conditions climateriques, aussi bien que
les vacances, imposen! le mois de septembre; mais pour fixer le jour precis, il convient de
charger, comme les autres fois, le bureau de la Commission permanente de faire au moment
voulu des d&marches auprös des collegues et de les consulter en dernier lieu par cireulaire.

La Commission permanente adopte a Vunanimile Berlin pour lieu de la prochamme
Conference generale, et pour epoque le mois de septembre.

1 Cette carte sera publiee dans les prochains Comptes-Rendus, apres avoir subi quelques corrections
que M. le Ga! Stebnitski envisage comme necessaires. A.H.

 
