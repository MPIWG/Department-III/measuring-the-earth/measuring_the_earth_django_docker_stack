 

 

ee

Alam

rt

FRE RTTTTEEE RR TLTE

ERTRRER

ieh

Comme, des 566 reperes, celui qui se trouve le plus eloigne d’Amsterdam n’a qu’une
erreur moyenne de 11”"24, on a le droit de conclure que chacun des reperes pourra indi-
quer le niveau zero avec toute la precision que ’on pourra exiger dans les rattachements.

Si, pour sureroit d’exaclitude, on veut indiquer un polygone de reperes determine,
situ& dans le centre du pays, on pourra choisir le polygone Amsterdam, Amersfoort, Zwolle,
Deventer, Arnhem, Nymegue, Ruremonde. Best, Tilbourg, Breda, Zwyndrecht, Overschie,
'saravesande, Ryswyk, Amsterdam.

Il se trouve dans ce perimetre 169, et dans l’interieur du polygone 89 reperes bien
vattaches ensemble et donnant chacun la hauteur A un degr& de pr&cision considerable.

L’erreur moyenne, ainsı que cela resulte du tableau suivant, n’alteint pas Zum,

reperes

Amsterdam
Amersfoort
Zwolle .
Deventer
Arnhern
Nymegue
Ruremonde
Best.
Tilbourg
Breda
Zwijudrecht
Överschie
'saravesande
Ryswyk
Utrecht.

Dans l’interieur \ Gulembourg

du polygone.

Les erreurs movennes des rep£eres intermediaires sont naturellement inferieures aux

Bois-le-Duc
Vught

chiffres de ce tableau.

Les 258 reperes se Lrouvent dissemines sur une superficie d’environ 10.000 km?,
dont la eirconförence a une longueur de 560 km.
ll est done &vident que dans les Pays-Bas il ne peut &tre question d’anomalies qui for-
ceraient de recourir aux reperes seculaires d’Amsterdam.
Un rattachement & la circonference du polygone donnera certainement une precision

satisfaisante.

L’erreur moyenne maximum, 6" 65, dans l’indication du plan de A. P., est de
ASSOCIATION GEODESIQUE — 24

Erreur moyenne
en millimetres.

0,00
3,66
9,68
0,46
4,90
9,69
6,65
9,49
Do
9,48
4,94
4,96
30
4,8%
dd
4,32
4,99
9,07

 
