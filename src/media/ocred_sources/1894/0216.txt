 

Annexe B. V:.

FRANGE

Rapport sur les travaux exeeutes par le Service Geographique de France
(oetobre 1893 — septembre 1894).

Les travaux executes par le Service Geographique de France, depuis la r&union de
Geneve, comprennent:

. A. — EN GEODESIE:

4° La suite des op£rations entreprises en 1893 pour reviser le reseau des Alpes-
Maritimes ; les observations ont &t& conduites cette annee jusqu’ä la station de l’Enchastraye.
90 L’&tablissement, en Alxerie, d’une chaine de premier ordre, entre Biskra et
Laghouat, pour former le troisieme et dernier segment du parallele du Sud Algerien; les
op6ralions ont &i& ex6cutees par M. le Capitaine. de Fonlongue.
Le parallele du Sud-Algerien, dans son entier developpement, s’ötend de la fron-
tiöre du Maroc jusqu’ä Gabes et embrasse 8 degr£s de longitude A la latitude moyenne de 34
degres.
A Par l’achövenient de ce r&seau, la triangulation primordiale de l’Algerie et de la
Tunisie qui comprend deux chaines paralleles et quatre chaines me6ridiennes, dans le sys-
t&me dit A gril, se trouve definitivement terminee. Nous avons dejä fait connaitre que l’en-

semble de ces chaines s’appuie sur huit bases ; de ces huit bases. trois ont &i& deja determi-.
PP ;

n&es; les cing autres seront incessamment mesurees.

3° La triangulation des hauts plateaux algeriens entre les paralleles du nord et du
sud et les chaines meridiennes de Laghouat et de Biskra par MM. les capilaines Dumezil et
Meunier. |

 

sckalbansschsnub uses

erssenmb tät 24 hs annniunä ann nn

 
