 

Seeger un ht |

NH m RETTEN

Bi

Her

Be

N ww

are

 

D7

bli, a l’aide des lignes g6od6siques, d’un point astronomique A l’autre, soixante öquations qui
mettent les devialions de la verticale en rapport avec les corrections des el&ments de lellip-
soide de reförence adopte. On a obtenu ainsi dix-sept equations de contröle de Laplace pour
les azımuls et les differences de longitude observees.

« Nous avons l’intention de soumettre ces @quations, en m&eme temps que le reseau

des differences de longitude, elabor& par M. van de Sande Bukhuyzen, &

et ensuite de commencer l’impression de tout le travail.

« Pour le moment, je me borne ä donner dans le tableau suivant les valeurs defint-
tives des arcs de parallele, reduites ä la latitude de 92°, exprimees en unites du melre inter-
national, suivant MM. Börsch et Krüger, ä& cöte des amplitudes astronomiques d’apres

Ü

une compensalion,

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

     
    
  

 

Bakhuyzen.

| Arc de parallele Difference Difference Difference |

Section | en metre international de longitude astro- || de longitude geode- astronomique |

edit a la latitude de 52° nomique sique geodesique |

| © ’ "m °© ! " | H

Greenwich-Feaghmain . . . | 710474,7 + 3634 da | — 10 20 52,12 | — 10 20 46,58 — 5,54 |

Greenwich-Haverfordwest . | 340821,3 4 24106 » | — 4 57 46,40 | — 4 57 471,53 + 1,43 |
Greenwich-Rosendaäl. . . .. | 165678,0 42090 » ı + 2 24 39,64 || 2 24 15,66 — 6,02
Rosenda&l-Nieuport. . 23783.65 + 34 »ı 4 20 47,28 || + 20 46,86 -+ 0,42
Nieuposi-Bomn . ... ... 298198 +53 » | + 0 27a | 4 1 20 28,38 — 5,64
Bonn Brocken.  ..... 2446534 +3029 » + 3 31 17,48 | — 3 31 8,65 + 8,83
Brocken Göttingen . .... 46253,44 — 263 » | — 40 32,46 || — 40 24,83 — 7,63
Broeken-Leipzig... .. ... 1206223 + 8% » ii 5 2378| + A 45 23.64 —+ 0,17
Leipzig-Grossenhain. . . .. 84460,14 4 938 » | + 1 10 35,81 || + 1 10 54,8 — 9,00
Grossenhain-Schneekoppe. . | 4150443,5 + 2507 » ı + 2 14 A311 | + 2 41 9,69 1 93,42
Schneekoppe-Breslau ..... | 88853,53 + 1629 » ı + A 47 42,70 || + 1 AT 3814 — 4,56
Breslau-Rosenthal. . .... . 0,03 + 0» I - 0,36 0,00 + 0,36
Breslau-Trockenberg . . . . | 126489,9 —+- 2677 » + 150 24,47 || + 1 50 34,23 — 6,76
Trockenberg-Mirow. .... 21228,73 + 504 » | + 98220698 | I 18 32,94 — 4,07
Mirow-Varsovie . ...... 126614,2 41050 » | + 1 50 36,64 || + 1 50 37,74 — 1,13
Leipzig-Rauenberg ..... 68259,88 + 14 » | + 59 36,37 || + 59 38,52 — 2,15
Rauenberg-Berlin...... 183736 — 15» | + 1 35,89 || + 1 36,32 — 0,43
Rauenberg-Springberg . . . | 223089,9 — 3097 » , + 3 44 53,22 | 4 3 14 55,47 — 2,25
Springberg-Schönsee . . . . | 1568024 — 407» + 2 16 3.@| + 2 ı7 0366| — 6,66
Schönsee-Varsovie ..... 1262912 = 107» + 2 Too, ı 27 99 —- 7,08

Fergkmsin . Shrin be In ssch;
Berlin CHORSEE
Haserfordives Greenwich „Broelfen Be, al Warschaur.
NMeuport Be Grossenhain 49"
Rena, COOGER Terreig Miron
Bonn

  

 
