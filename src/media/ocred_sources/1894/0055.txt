 

ER Teen ne ih |

 

Lg

UNE

RN NE
I a at

Here 1

Fer

17

 

49

« Abgesehen von einigen Formänderungen und von der Einführung des Prinzips,
wonach sämmtliche betheiligten Staaten jährliche Beiträge zum Zweck der Deckung der ge-
meinsamen Kosten zu leisten haben, kann man behaupten, dass die internationale Erdmessung
in äbnlicher Weise wie während der zwanzig vorhergehenden Jahre organisirt wurde.

« Heute stehen wir wieder vor der Aufgabe, die internationale Uebereinkunft von
1856 zu erneuern.

«Obwohl meine persönliche Ansicht, wie meist, zu Gunsten der Aufrechterhaltung
des Bestehenden lautet, so lässt es sich doch nicht leugnen, dass gewisse Gründe wissen-
schaftlicher, und leider zum Theil auch nationaler Natur, einigen unserer Üollegen den Ge-
danken eingegeben haben, dass es nützlich wäre, bei Gelegenheit der Erneuerung der Ueber-
einkunft, an dieselbe einige mehr oder minder wichtige Aenderungen anzubringen.

« Trotz meines lebhaftesten Wunsches, dass diese Aenderungen sich auf das möglichste
Minimum beschränken, begreife ich doch, dass es nothwendig ist, die bestehende Convention
einer eingehenden Untersuchung zu unterwerfen, um darin diejenigen Uebelstände zu besei-
tigen, welche die Erfahrung der letzten zehn Jahre etwa ergeben hat. /

« Während die Berathungen der Jahre 1885-1886, in Folge des Todes des Generals
Beyer, mehr einen offiziösen Charakter hatten, befinden wir uns jetzt, nachdem die verschie-
denen Organe der Erdmessung mit einer vollständigen Regelmässigkeit sich bewährt haben,
in der Lage, die ganze Frage mit voller Unabhängigkeit zu behandeln; auch haben wir noch
die nöthige Zeit vor uns, die betreffenden, der General-Conferenz vorzuschlagenden Con-
venlions-Aenderungen gründlich zu berathen.

« Zu diesem Zwecke stelle ich folgende Anträge : 1. Alle Landes-Commissionen er-
halten einige Exemplare der Uebereinkunft von 1886, um deren Bestimmungen studiren
und ihre betreffenden Bemerkungen daran anbringen zu können; 2. Eine Commission von
fünf Mitgliedern wird beauftragt, diese Bemerkungen in Betracht zu ziehen und die Frage zu
erörtern, bis zu welchem Grade es möglich ist, denselben bei dem der nächsten Conferenz
zu unterbreitenden Entwurfe der neuen Uebereinkunft Rechnung zu tragen; diese Com-
mission wird alsdann ein oder zwei Mitglieder beauftragen, den Entwurf der neuen Ueber-
einkunft auszuarbeiten ; 3. Die Arbeiten dieser Commission sollten wenigstens drei Monate
vor der Zusammenkunft der General-CGonferenz beendigt sein. »

Ierr Ferster ıst mit den Vorschlägen des Herrn Ferrero vollständig einverstanden ;
er ist der Ansicht, dass es nothwendig wäre, den Entwurf der Uebereinkunft an alle Dele-
girten, welche wahrscheinlich zu der General-Conferenz nach Berlin abgeordnet werden, zu
schicken, um sie in Stand zu setzen, denselben ihren Regierungen mitzutheilen und zu em-
pfehlen, welche ihnen alsdann die nöthigen Vollmachten zur Annahme der neuen Ueberein.
kunft ertheilen zu können.

Herr Hirsch unterstützt ebenfalls die Vorschläge des Herrn Ferrero, mit welchem er
bereits Gelegenheit gehabt hat sich über diese Angelegenheiten zu verständigen. Andererseits,

und obwohl er von der Nothwendigkeit überzeugt ist, das Terrain sorgfältig vorzubereiten,

ASSOCIATION GEODESIQUR — 7

 
