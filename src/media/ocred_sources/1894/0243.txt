 

 

j
|
|

E
j
;

ee]

Prinyu am lei pen ni

IT rer ill

 

237

Azimut. La direction du couvent Donskoy, observee du clocher d’Iwan Weliki par
M. Miontchinsky en 1892, pour l’orientation de la triangulation de Moscou, est de

n

195 46.0,62 + 0,80°

Le möme azimut, observ& en 1832 de l’autre point et reduit au clocher d’Iwan
Weliki, est de

195 46 4,19

Cette difference de 3.50 comprend la deviation de la verticale qui peut avoir lieu, car
la distance des points d’observation d&passe plusieurs kilomeires.

La latitude du elocher d’Iwan Weliki, d’apres les observations de M. Iveronoff en
1899, qui a profit de l’instrument de passage en l’installant au premier vertical, est de

o d „

55 44 52,93 & 0,19

La m&me latitude, döterminee par feu le professeur Schweizer en 1845-46, est de

° 7 U H

29 Ak 598.20 ı Di

b) En Crimee, le colonel Miontchinsky et M. Kortazzı ont determine les differences
de longitude : Thäodosie-Soudak et Thedosie-Rostow. La premiöre des longitudes repose sur
six nuits d’observations et l’autre sur huit. L’&quation personnelle a et& eliminde par la
methode ordinaire; en outre, elle a &t& determinde par les observations direetes pendant
six nuits. Les correelions des chronomötres ont &t@ determindes par la methode des hauteurs
correspondantes des &toiles. Les resultats des observations faites en 1892 et 1893 sont les
suivanls:

 

 

 

 

 

 

 

Latitudes Differences Longitudes de Nicolaiew | Differences

astronomiques | geodesiq. astronomiques | geodesiq.
o I H ! W „ ° ! 2 I HM n
Simfäröpol (eathedralej: «un | 44-57 16,5-,8E una 0 382 Toms. 230% I 56
Kekeneise (station de la poste) . | 44 23 59,6 | 24 33,9 | + 34,3 || 4 58 144,30 | 58 12,7 | — 1,6
Soudak (eglise) , 2... ..)-4450.25,1-)50 #05... 10.023 008.1 0--7,8| — 36,9

 

 

 

 

 

l,a dötermination des longitudes faites en 1893 permet de fermer le polygone suivant:

ASSOCIATION GEODESIQUE — 32

 
