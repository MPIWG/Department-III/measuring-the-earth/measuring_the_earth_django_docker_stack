 

Von dieser ablehnenden Erklärung des Herrn Tisserand wurde zunächst Herrn Schia-
parelli Kenntniss gegeben, worauf unter dem 16. April die nachstehende Antwort desselben
erfolgte :

« Milan, le 16 avrıl 1894.
« Monsieur le Professeur,

« Je viens de lire la r&ponse envoyee par M. Tisserand, et vos remarques qui y sont
annexees. |

« La lecture de cette röponse n’a pu modifier sur aucun point ma maniere de voir.
Vos remarques me paraissent justifiees. Je me permets seulement d’ajouter que la Societe
astronomique pourra bien juger par elle-meme si c'est dans sa mission de contribuer a l’ins-
titution internationale dont il est question. Je pense aussi qu’un vote exprime par une aulo-
rit& aussi &minente que cette Soeiete, sur le projet en discussion, sera tr&s propre a nous
sclairer sur la valeur de ce projet el sur l’importance des objections qu’on peut y faire.

« Rien ne s’oppose de mon eöt& ä ce que le projet de lettre avec les autres pieces
soit envoye A la Sociele.

« Pai ’honneur (de me dire, avec le respect le plus affectueux, votre devoue

« J.-V. SCHIAPARELLI. >»

In dieser Sachlage erschien es als das allein Angemessene, dem Vorstande der Astro-
nomischen Gesellschaft die Vorlage in derjenigen Fassung zu übersenden, welche von der
Mehrheit der Mitglieder der Spezial-Commission gebilligt war, dagegen den Dissensus des
dritten Mitgliedes dem Vorstande der Astronomischen Gesellschaft nicht vorzuenthalten, jedoch
unter Hinzufügung der beiden, die ausdrückliche Zustimmung des Herrn Schiaparelli näher
begründenden und die günstige Stellungnahme der italienischen Fachgenossen constatirenden
Briefe. Die bezügliche Zusendung erfolgte unter dem 10. Mai an den Vorsitzenden der Astro-
nomischen Gesellschaft, Herrn Professor Gyldön zu Stockholm, und wurde von dem letzteren
unter dem 1. Juli mit der Erklärung beantwortet, dass er die Darlegungen der Mitglieder
der Spezialeommission bei der im Anfang August bevorstehenden Versammlung der Astro-
nomischen Gesellschaft und ihres Vorstandes in Utrecht vorlegen wolle.

Bei der Besonderheit der Sachlage hielt es der Unterzeichnete für seine Pflicht, bei
dieser Versammlung des Vorstandes der Astronomischen Gesellschaft, welcher er selber an-
gehört, persönlich anwesend zu sein, um im Namen der Mehrheit der Specialcommission
nöthigenfalls über den vorgelegten Plan nähere Auskunft geben zu können, zumal in Berück-
sichtigung des Umstandes, dass das dritte Mitglied der Commission, welches gegen den Plan
votirt hatte, als Mitglied des Vorstandes der Astronomischen Gesellschaft in der Lage war,
auch seine Auffassung diesem Vorstande gegenüber persönlich verfechten zu können.

Auf Grund der in Utrecht stattgehabten Verhandlungen, denen indessen leider Herr
Tisserand beizuwohnen verhindert war, ist alsdann ein Beschluss des Vorstandes der Astro-
nomischen Gesellschaft erfolgt, welcher durch das nachfolgende Schreiben des Herrn Vor-
sitzenden jener Gesellschaft, Herrn Professor Gylden, dem Unterzeichneten mitgetheilt wurde:

bh a

 

 
