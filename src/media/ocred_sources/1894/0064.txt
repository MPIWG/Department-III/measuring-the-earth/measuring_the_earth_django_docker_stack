 

98

bleiben sollte, so meint Herr Helmert doch, dass, mit Rücksicht aufdie Bedeutung des Zweckes,
diese Versuche unternommen werden sollten. —

Nachdem der Secretär die Vorschläge der Finanz-Commission in deutscher Sprache
wiedergegeben, bringt der Präsident diese Vorschläge zur Abstimmung, welche sämmtlich
einstimmig angenommen werden, nämlich :

1. Die Permanente Commission billigt die Rechnungen für das Jahr 1893 und er-
theili dem Herrn Direktor des Centralbureuw’s volle und unbedingle Entlastung für seine Ver-
wallung.

II. Die Permanente Commission bewilligt folgende, aus den disponiblen Mitteln zu
eninehmende Credite:

1. Zur Unterstützung der Versuche über die besien Mittel, um den Einfluss der

Feuchtigkeit auf die Nivellir-Latien zu en N
9%. Zur Förderung von Versuchen über die beste Construction von Metallstäben für
Basisstangen . 3000 M.

3. Zur Förderung der Erfindung und Construction von Apparaten, welche zur Messung
der Schwere auf dem Meere dienen können . 6000 M.

Auf die Landes-Berichte zurückkommend, giebt der Herr Präsident zunächst dem
Herrn Professor Schols das Wort zu einer Mittheilung über die Arbeiten in den Niederlanden.
(Siehe Beilage B. VII.)

Bei dieser Gelegenheit bittet Herr van de Sande Bakhuyzen um die Erlaubniss, eine
Notiz seines Collegen Herrn van Diesen über die Frage des Fundamental-Niveaus mitzutheilen.

Auf Wunsch des Herrn von Kalmär, wird diese interessante Notiz, obwohl sie nur
die persönliche Ansicht ihres Verfassers ausspricht, als Beilage der Verhandlungen gedruckt
und auf diese Weise zur Kenntniss der Delegirten gebracht werden. (Siehe Beilage A. Vl.)

Herr Lallemand, welcher einige Bemerkungen über die darin enthaltenen Ansichten
zu machen wünscht, behält sich vor, dieselben in der nächsten Gonferenz mitzutheilen, wo
diese Frage von Neuem zur Verhandlung kommen wird.

Darauf folgen die Berichte für Preussen, zunächst derjenige des Herrn Professor
Helmert über die Arbeiten des geodätischen Instituts in Potsdam. (Siehe Beilage B. 1Xa.)

Nach einer Unterbrechung der Sitzung von 12 bis 9 Uhr, ertheilt der Präsident das
Wort Herrn Oberst von Schmidt zur Verlesung seines Berichtes über die Arbeiten der Preussi-
schen Landes-Aufnahme für das Jahr 1894. (Siehe Beilage B. IX.)

Herr Fierster wünscht einige Worte hinzuzufügen, zunächst betreff der Preiten-

la.

ash hut ie hide Ada

IUvEruT" vn

Aal Laub acseitihien:

Aa ED aa u

an pas Sal au

 

 
