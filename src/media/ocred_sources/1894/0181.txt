 

 

Aananafert mt

T

RT TEEN

Bm

tee

178

Et meme, äAsupposer que la construction du mödimaremetre ou de tout autre ap-
pareil atteindrait une perfection telle qu’on n’aurait plus & se,pr&occuper du reglage ou du
dömontage de l’appareil, et qu’en outre, dans chaque pays, on räussirait par une grande
serie d’observations longtemps continuees, A trouver une hauteur correspondant avec la
hauteur moyenne du niveau de la mer sur un certain point de la cöte, il resterait toujours
la plus grande dilficult6 & resoudre,

En effet, on disposerait alors d’autant de plans differents qu’il existerait de fpoints
d’observation sans qu’on fül en possession d’aucun moyeh pour signaler celui de ces plans
qui s’approcherait le plus du niveau moyen general de la mer.

La condition dans laquelle on se trouverait aurait ainsi beaucoup de ressamblance
avec la situation actuelle, sauf encore cette difference que les nouveaux plans de niveau
n’auraient pas l!ınvariabilil& des plans actuels.

On n’a pas jusqu’ici fait voir comment, dans ce cas, l’unification cherchee se trou-
verait realisee d’elle-meme sans l’intervention arbitraire d’une compensation generale, ni
prouve que celle compensalion serait praliquement nöcessaire pour transporter d’un pays A
Pautre une hauteur choisie, au moyen d’un nivellement.

La Haye, 16 aoüt 1894.
VAN DIESEN.

 
