 

$I Fnee mrereeneng ne 3

IN RI an

ES RRIARRE LU BRlIgIL am InwwT enmin

Datum,

1893 Jan.

Febr.
März
April

Mai
Juni
Juli
Aug.
Sept.
Oct.
Nov.
Dee.
1394 Jan.

Febr.
März
April

Mai
Juni

15
13
18
12
15
13
20
14

16
14
13
18
20
ZI

28

Jahr.

1893.

1894.

°4 4
12
27
28

37
45
55
62
69
79
87
95
05
14
22
26

35
I

Polhöhe.,

3° 35

©7702.0=070707 02 0707020207070 802.62 0 0
Wa
D

153

Paare.

 

2746

Abweichung
vom Mittel.

"

OH:
.09
.08
„II
SL]
18
2a
Jo)
.06
SDA
.Ig
13
O1
.o2
ot
.05
.05
05

o
+0.
—+o

—+o.

Reduetion
Ch.(52);3 7,

"08
.02
.12
ı
19
.17
.Io
.04
.03
Ar
„14
4
‚To
.05
.0o
03
.07
og

.09
.07
.04
.06
02
.oL
.14
02
.o3
.o3
os
.oL
„IL

.07

.08
O2

.04

Monatsmittel bis Dec. 1893 aus No. 3209 der Astronomischen Nachrichten, spätere Werthe von

Herrn Prof. Dr. Bucker dem Üentralbureau gütigst mitgetheilt.

Da Herr Prof. Dr. Becxer die Epochen nur in Bruchtheiljahren angegeben hat, mussten die
Datumangaben in Columne ı auf Rechnungswege aus den Zahlenwerthen in Columne 2 ermittelt werden.

Datum.

1889 Dec.

13890 Jan,
Febr.
März
April
Mai
Juni
Juli
Aug.
Sept.
Oct.
Nov.
Dec.

12
202)
12)
12
IE
17
20
17
19
18
16
16
ıI

Jahr.

1889.95
1890.06

12
19
28
38
47
54

7
9
88

94

Polhöhe.,

20, 30,

23.13
213.218
23.40
23.02
39
23.54
23.53
23.54
23.20
23.27
25
23.08

22.095

23233

Bethlehem.

A= +75’ 23

Abweichung Reduction
vom Mittel, Ch.(52);3 r,

Paare.
i. Reihe
150 — 0.20
45 7720305
122 40.07
63 0.19
140 +0.26
90 70.21
231 0.20
104 40.21
131 —0.04
LT —0.06
129 078
180 —0.25
033 —0.38
E445

+0

+o.

TO:

"19
08
.oo
‚Io
220
025
2a
.ı8
.06
om
„17
„25
.28

or
08
.07
.09
.o6
.04
.04
.03
. To)
.oL
.oI
.oo
‚Io

Reduction
Oh. (52); $ r,

0,

+0.
—o.
0.
—o0,.
OT
.18
12
.o2
‚08
LS
21
22

03
04

18

207
R02)
03
.08
.08
.00
oz
.09
.06
oz
03
.04
.ı6

Monatsmittel abgeleitet aus den Tagesresultaten in No. 274 des Astronomical Jowrnal, wobei die
Beobachtungen am Abend und am Morgen mit gleichem Gewicht vereinigt wurden,

 
