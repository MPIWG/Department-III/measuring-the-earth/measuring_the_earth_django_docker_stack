 

70

direction de l’Institut g&ographique militaire de Florence pour cominander la division mili-
Laire de Bologne, son successeur ä Florence, M. le major-general A. Rosselli, est entre dans
la Commission g&odösique, de meme que M. Guarducer, ingenieur A Plnstitut osographique
militaire de Florence, M. Abetti, direeieur de l’Observatoire d’Arcetri, ä Florence, et M. Rajna,
astronome de l’Observatoire de Brera, a Milan. 2

« Comme membre de la Commission geodesique suisse, le professeur Wolf a et
remplac6 par M. le D" Albert Riggenbach-Burkhardt, professeur ä Bäle.

« Bien qu’il soit desirahle, dans l’interet de l’organisation des Gonlerences interna-
tionales, de connaitre d’avance A peu pres le nombre des membres qui y prendront
part, le Seeretaire a regu seulement de MN. le colonel Zachari et de M. le Lieutenant-Golonel
von Schmidt, de M. le colonel Hennequin et de M. Lallemand, l’annonce prealable de leur
participation. Par contre, il a &t& informe que M. von Helmholtz, par suite de la maladıe qui
l’a frapps, est empöch6 de venir & l’assemblee d’Innsbruck. Le Secretaire est certain que lous
ses collegues regrettent vivement l’absence, si tristement motivee, de Villustre savant el il
demande d’etre autorise A envoyer une depeche A M. von Helmholtz pour lui exprimer les
profonds regreis de l’Assemblee et ses meilleurs voeux pour son prompt retablissement.

« Enfin, M. von Kalmär a inform& le Seeretaire que, par suile de sa nomination A la
direction du Bureau hydrographique de Pola, il sera probablement empeche de prendre part
A la session actuelle de la Commission permanente.

« Le bureau a pris la liberte d’inviter ä nos seances M. le D" Leon Du Pasquier, de
Neuchätel, qui a ex&cut6 un travail remarquable concernant l’attraction des montagnes sur
la verticale dans la Suisse oceidentale. M. Du Pasquier s’interesse vivement, comme geo-
loxue el math@maticien, aux questions de la pesanteur et a eu dernierement au ongres geolo-
gique de Zurich l’occasion de s’entretenir avec plusieurs de ses collögues de Vienne, Göttingen,
etc , sur le projet des observations de pendule, souleve par quelques Academies.

« Une seeonde invitation, adressde par le bureau A M. Harold Jacoby sur son desir,
est resi6e sans rösultat, car le savant astronome de New-Vork, qui a ex&eule depuis une annde,
apres entenle avec M. Fergola, ä Naples, des observations de latitude pour l’etude de la
question des variations de la hauteur du pöle, vient malheurement de nons annoncer que,
pour des raisons de sant6, il ne pourra pas venir ä Innsbruck. Ge fait est d’autant plus regret-
table que, cette annde encore, aucun del&zue officiel des Etats-Unis ne peut prendre part ä
notre session.

« Suivant la decision prise par la Commission permanente dans la derniere seance
de Geneve, le bureau s’est mis en relation avec les collegues d’Autriche et, apres avoir regu
de MM. Tinter et von Kalmär l’assurance que les autorites du pays recevraient volontiers
’Association g&odesique cette annde A Innsbruck, il a soumis, par circulaire du 9 avril, le
lieu accepte ä Vienne, et comme date d’ouverture de la session le 5 septembre au vote
definitif de la Commission permanente. Ayant rencontre P’approbation unanime des membres
de la Commission, le bureau a pu adresser le 18 juin la eireulaire de convocation ä tous les
deleguss Je l’Association, en les invitant a assister nombreux & la reunion dans la belle capitale
du Tyrol, ou d’importants sujets doivent se traiter.

Mails

Mb uk ldiaid

nr

ln

eussenmch uni au sul u

 

 
