 

« Nous ne pouvons pas examiner ici jusqu’ä quel point cette conclusion est justifice,
d’autant moins que, dans les documents & notre disposition, nous n’avons pas trouve de
details suffisants sur la construction de ce genre de mires, et qu’aussi la notice citee dans le
rapport de M. von Kalmär, p. 174, ne permet pas de s’en faire une idee pre£cise.

« 3° Puisque la technique moderne a reussi & fournir dans l’alumınium une matiere
excellente sous plusieurs rapports pour la construction des mires de nivellement, il est tres
probable que le moment n’esi pas &loigne oü les mires en bois actuelles seront remplacees,
pour les nivellements de precision, par des mires metalliques, qui, dans l’idee de M. le pro-
fesseur Vogler, ä Berlin, serviraient en m&me temps de thermometre. A ce point de vue, le
probleme pose ne parait plus @tre opportun et, puisqu’on possede actuellement des moyens
bien plus efficaces pour eliminer les defauts techniques, je ne saurais demander aux fonctlion-
naires de l’Institut de ’Empire un travail aussi compliqu6 et offrant probablement aussi peu
d’utilit& que celui exig& par l’etude des mires en bois.

Le President de I Institut physico-lechnique de ! Empire,

(Signe) von HELMHOLTZ.

« La reponse negative qui precede est molivee probablement aussi par le fait que
l’Institut physico-technique est surcharg6 de travaux; loutefois el sans vouloir contester le
bien-fond& de plusieurs des raisons qui ont motive le refus de M. von Helmholtz, j’estime
cependant que de nouvelles recherches sur cet objet seraient desirables et j’aı maintenant l’es-
poir que M. le Dr Stadthagen, physicien ä la Commission des Poids et Mesures ä Berlin, voudra
bien, d’une maniere privee, mais avec les instruments de la dite Commission, se charger des
exp6@riences voulues. La premiere chose a mettre en lumiere serait la longueur 2 du bois de
sapin coupe suivant la direction des fibres, en fonction de sa temperature & et de son humi-
dite y, cette derniere &tablie par des pesces, c’est-a-dire determiner la surface 2 = (@ Yy),
par des observations varices et en lenant compte des l’abord des effets en retard.

« En ce qui concerne la construction des mires de nivellement en metal, M. le Dr Vo-
gler, cit& dans la reponse de M. von Helmholtz, a bien voulu me remettre une communication
detaill&e qui sera donnde comme annexe. (Voir Annexe A. VII.)

« L’Institut physico-technique a dejä, au mois de mai dernier, entrepris des exp6-
riences sur la dilatation de l’aluminium, desquelles il r&sulte que ce metal, pourvu qu'il soit
porte pr&alablement ä une temperature elevee, se prete parfaitement ä des usages metrolo-
siques d’importance secondaire; toutefois il reste encore A decider si, au point de vue des
effets thermiques retardes, il est preferable au zinc.

Activite administrative dw Bureau central.

« La gestion du fonds de dotation de la Commission permanente a eu lieu comme
jusqu’ä present et j’ai l’honneur de remettre & M. le president les comptes pour l’annee 1895,
accompagne6s des pieces justificalives.

Hei lempeskeshhuei

hi hc

 

 
