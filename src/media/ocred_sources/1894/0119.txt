Fr er

5 m GrFrnTAOTrETEIRgneng 3

Hang Ha

Tat Berta

Feine

Aep

 

113

Une mire en metal (zine, aluminium, ou assemblage de deux me6taux, peu importe)
resterait toujours sujelte aux influences thermiques et, pour restreindre leur effet, autant
que pour permettre une determinalion exacte de la temperature de la regle!, on serait con-
duit A entourer celle-ei d’une enveloppe calorifuge assez @paisse. Or, la meilleure enveloppe
proteetrice serait encore une regle en bois, dans l’äme de laquelle on logerait la tige metal-
lique. On se trouverait ainsi ramene, tout naturellement, A un type de mire analogue & la
mire du Colonel Goulier.

M. Hirsch est &galement de l’opinion que les mires en bois resteronl longtemps encore
preferables aux mires en mötal pour les nivellements, car les tempe6ratures et leurs varialions
rapides ont moins d’influence sur le bois que sur le metal. D’un autre eöte, il Jui semble
que l’on pourrait, par beaucoup de soins, diminuer aussi l’influence de ’humidit& sur les
mires en bois en ameliorant leur fabrication. La plupart des constructeurs, il est vrai, ont
soin de plonger les mires dans l’huile bouillante, et de les peindre ensuite ; avec ces pre-
cautions, les mires, ainsi qu’on a pu le constater en Suisse, ne varient plus avec l’humidite
que dans une mesure presque nögligeable.

Toutefois, M. Hirsch pense que sous ce dernier rapport on pourrait peut-etre realiser
encore quelques progres. Il vient d’apprendre avec plaisir que la Commission des comptes a
P’intention de proposer ä la Commission permanente le vote d’un credit special pour entre-
prendre des recherches dans cette direction.

M. Helmert rappelle que dans son rapport il attache une grande importance aux
recherches sur la meilleure construction des mires en bois employ6es dans les nivellements.
D’apres son opinion, il vaut mieux passer sur le bois plusieurs couches de peinture ä U'huile;
il rappelle aussi que ce procöde a &t& recommand& de divers cötes. Enfin, il se souvient que
dans une r&union scientifigue Siemens a propos& de soumettre les mires en bois A un bain
de paraffine dans le vide pour diminuer considerablement Pinfluenee de P’humidite.

M. Helmert estime qu’il vaut la peine de faire des essais. Si ’on continue ’emploi des
procöd6s actuels, on n’arrivera jamais qu’ä obtenir des resultats moins defectueux ; si, au
contraire, on s’oceupe activement de cette question, il se peut que l’on finisse par decouvrir
un moyen permettant de s’approcher davantage du but que l’on poursuit ä cel egard.

M. van de Sande Bakhuyzen fait remarquer que les mires en bois employees dans le
nivellement des Pays-Bas sont ä peu pres insensibles a l’action de ’humidite , on a reussi A
preparer le bois de facon que l’allongement des fibres ne puisse avoir lieu,

M. le President invite M. Foerster ä pr6senter le rapport de la Commission des finances
pour l’annee 1893.

ı L’exp6rience, en effet, a montr& que des thermometres, m&me noyes dans l’äme d’une regle metal-
lique, ne donnent Ja vraie temperature de celle-ci qu’avec un certain retard; par suite leurs indications ne

peuvent inspirer de confiance que si la temperature ambiante est stationnaire depuis un temps suffisant.

ASSOCIATION GEODESIQUE — 15

 
