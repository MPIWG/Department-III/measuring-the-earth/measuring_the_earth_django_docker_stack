 

IRRE NT per

FRRIMTERg TI

 

DRITTE SITZUNG

Montag, den 10. September 1894.

Präsident: Herr A. Faye.
Gegenwärtig sind :

I. Die Herren Mitglieder der Permanenten Commission : Ferrero, Ferster, Helmert,
Hennequin, Hirsch, von Kalmar, van de Sande Bakhuyzen, von Zacharie.

It. Die Herren Gommissare : Albrecht, d’Arrillaga, Guarduec:, Hard, Karlinski,
Lallemand, Lorenzent, Rajna, von Schmidt, Schols, von Sterneck, Tinter, Tisserand, Weiss.

II. Die Herrn Eingeladenen : Beschoffsheim, Pattenhausen, Tripet.
Die Sitzung wird um 10 '/, Uhr eröffnet.

Der Herr Präsident begrüsst die Herren von Kalmär und Bischoffsheim, welche es
ermöglicht haben, wenigstens an den letzten Sitzungen der Gonferenz theilzunehmen.

Er theilt alsdann einen Brief des Herrn Derrecagaix mit, welcher die Abwesenheit
mehrerer französischen Gommissare erklärt : Herr Oberst Bassot ist durch Gesundheits-Rück-
sichten abgehalten; Herr General Derr6cagaix und Herr Commandant Defforges sind durch
die grossen Manöver im Monat September, an welchen sie theilnehmen müssen, verhindert
in diesem Jahre bei der Versammlung der Erdmessungs-Gonferenz zu erscheinen.

Der Secretär verliesst in deutscher Sprache das Protokoll der zweiten Sitzung; das
selbe wird einstimmig angenommen.

Der Herr Präsident ertheilt das Wort Herrn Helmert, welcher über die von Herrn
Bakhuyzen in der letzten Sitzung gemachte Mittheitung Einiges zu sagen wünscht.

Herr Helmert bemerkt zu den Ergebnissen der Untersuchung des Herrn van de Sande
Bakhuyzen, dass es sich dabei nur um Mittelwerthe handeln kann. Derartige zweigliedrige

 
