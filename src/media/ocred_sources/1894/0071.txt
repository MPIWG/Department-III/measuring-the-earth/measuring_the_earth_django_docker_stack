 

PREMIERE SEANCE

Mercredi, 9 septembre 1894.

La seance est ouverle \ 2 heures.
Sont presenls :
I. Les membres de la Commission permanente :

1. M. H. Faye, membre de !’Institut, President du Bureau des longitudes, A Paris, Presi-
dent de la Commission permanente.

S. E. le lieutenant-general A. Ferrero, senateur, Commandant de division, ä Bologne,
President de la Commission geodesique italienne, Vice-Preösident de la Commission
permanente.

3. M. le professeur A. Hirsch, Directeur de l’Observatoire de Neuchätel, Seeretaire perpetuel

de l’ Associalion geodesique internationale. |

4. M. le professeur D’ f.-R. Helmert, Directeur de Institut royal geodesique prussien et

du Bureau central de l’ Association geodesigue internationale, a Potsdam.

9. M.le Dr W. Feerster, professeur a l’Universite, Directeur de l’Observatoire, A& Berlin.

M. le colonel Hennequin, Directeur de I’Institut cartographique militaire, & Bruxelles.

7. M. le Dr H.-G. van de Sande Bakhuyzen, membre de l’Academie royale des sciences,

professeur d’astronomie et Directeur de ’Observatoire, A Leyde.

. le eolonel von Zacharie, Direeteur des travaux geodesiques, A Gopenhague.

Ne Tr RTTTERETTENT" STTENNITeN
NS

ge

u
)
=

Kain

mr
oo
=

E Il. Les deleques :

1. M. le professeur Th. Albrecht, Ühef de section & Institut geodesique royal, A Potsdam.
2. 8.E.M. F. de P. Arrillega, Divecteur general de P’Institut g&ographique et statistique
: d’Espagne, membre de l’Acad&mie royale des sciences, ä Madrid.
3. M. F. Guarducei, Ingenieur de P’Institut g&ographique militaire et Secretaire de la
Commission geodesique italienne, A Florence.
M. M. Haid, professeur de g6odesie A l’Eeole polytechnique de Karlsruhe.
M. F. Karlınski, professeur d’astronomie et Direeteur de l’Öbservatoire, A Cracovie.
ASSOCIATION GEODESIQUE — 9

SU

Pin

UE
=
ar
ıE
7
=
RE
ar

 
