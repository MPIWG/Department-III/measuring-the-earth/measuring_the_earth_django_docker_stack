 

144

von nahezu ı” Theilwerth versehen ist, von dem Director des Observatoriums Prof. Jon
K. Ress und Prof. Haroı» Jacosy ausgeführt. Erhalten wurden bis zum 22. Juni 1894 im
Ganzen 2350 Sternpaare. N

Resultate der Beobachtungsreihen liegen zur Zeit noch nicht vor.

Bethlehem (Sayre Observatory).
P.= 40° 36° 23. A\-msıt0s2 = ch .23

ı. Beobachtungsreihe: ı. December 1889 — ı3. December 1390.

Beobachter : Prof. C. L. Doouirrue. Die Beobachtungen wurden nach der Horrebow-
Methode an einem Zenittelescop von 8ımm Oeffnung unter Anwendung einer 75 fachen
Vergrösserung ausgeführt und auf 109 Sternpaare ausgedehnt. Erhalten wurden im
Ganzen 1445 Sternpaare.

Die Resultate sind in No. 274 des Astronomical Journal publieirt worden.

2. Beobachtungsreihe: 10. October 1892 — 27. December 1893.

Beobachter: Prof. ©. L. Doouırtıe. Nach Ausführung einiger baulicher Ver-
änderungen im Beobachtungsraume und einiger Verbesserungen am Instrument wurden
die Beobachtungen auf Grund eines Schemas von 107 Sternpaaren, auf ıı Sterngruppen
vertheilt, fortgesetzt. Die überwiegende Zahl der Sternpaare gehörte bereits dem Pro-
gramm der ı. Beobachtungsreihe an. Erhalten wurden in Summa 2796 Sternpaare.

Die Resultate dieser Beobachtungsreihe sind in No. 322 des Astronomical Journal
publieirt worden.

3. Beobachtungsreihe: ı9. Januar 1894 — jetzt.

"Beobachter: Prof. C. L. Doorırrıe. Ueber diese Beobachtungsreihe liegen specielle
Angaben nicht vor, doch bildet dieselbe wahrscheinlich eine Fortsetzung der vorigen
Reihe. Voraussichtlich werden die Beobachtungen noch längere Zeit fortgesetzt werden.

Provisorische Resultate dieser Reihe, die Zeit bis 25. Juli 1894 umfassend, sind
von Herrn Prof. Doorırıuze dem Centralbureau gütigst mitgetheilt worden.

Rockville, MD.
pP = 39 5 11" A) ang? zur eueeinnbeno

Diese Beobachtungsreihe wurde in Cooperation mit den Beobachtungen der Inter-
nationalen Erdmessung in Honolulu auf Veranlassung der U.S. Coast and Geodetic Survey
durch den Assistent Eowm Sıuurm in der Zeit vom ı3. Juni 1891 — 9. Juli 1892
ausgeführt. Die Beobachtungen erfolgten nach der Horrebow-Methode auf Grund
eines Sternschemas, welches 88 Sternpaare auf ı5 Gruppen vertheilt umfasst. Als
Instrument diente ein Zenitteleskop von Troughton & Simms von 76" Oefinung,

Ab rare cha

 

 
