 

U

ei

Ian ya al lite

ERLITT,

. 243

avec les valeurs deduites de Vienne, aussi bien qu’avee la dätermination absolue faite A Zurich
au moyen du pendule a reversion de Repsold ; car on a pour Zurich:

L 9

0,993631 9,80674 par Vienne, d’apres von Oppolzer.
0,993621 9,80664 par Munich, d’apres von Orff.
0,993632 9,80675  mesure directe, faite en 1889 par Messerschmitt.

On peut ajouter que la valeur theorique de la pesanteur, d’apres la formule de Hel-
mert, serait pour Zurich de 9"80670.

L’accord des determinations relatives et absolues est moins satisfaisant pour Neu-
chätel; car, par ses mesures relatives, M. Messerschmitt a deduit 4 — 9"80660, tandis que
les observations que M. Scheiblauer a faites dans le temps avee le pendule ä r&version donnent
9 = 9780631 ; d’est ce dernier nombre qui s’accorde, ä deux unites de la cinquieme deeimale
pres, avec la valeur Iheorique qui est pour Neuchätel 4 = 9"80629.

D’apres les mesures failes avee le magn&tometre de montagne de Meyer, on trouve
qu’une ligne de me&me intensit& horizontale passe par Immenstaad, Singen, Hohentwiel,
Schaffhouse, Achenberg et Egg. Toutefois on reconnait des perturbations locales A des distances
assez faibles, qui atteignent de 1 &2 ou 3 %,.

(Quant aux travaux du Nivellement de pr6eision, comme le reseau hypsometrique de
premier ordre est termine depuis plusieurs anndes, ils se bornent essentiellement & des nivel-
lements de contröle ei ä des raltachements de stations met£orologiques, d’echelles de riviere,
etc. Aınsi, on a ex6&cul6 en 1893, entre aulres, ’op6ration de contröle de la ligne Weinfelden-
Wyl-Werdenberg (116 km.).

Le Bureau topographique de la Gonfederation continue avec beaucoup de soins le
reperage (des points fixes pour en assurer la conservation et l’invariabilitö; en 1893 on a
execul& ces operations sur les lignes de Geneve-Morges-Lausanne-Fribourg-Berne ; Berne-
Olten-Brugg-Zurich,; Zurich-Rorschach-Rheineck. Dans le but de cette conservation, on publie
a parlir de cette annde, par aulographie, la liste des reperes, avec plans de situation et cotes
au-dessus de la Pierre du Niton.

Pour la campagne de cel &td, le programme comprend les nivellements de contröle
des lignes Werdenberg-Wildhaus, Rheineck-Lindau, pour contröler la jonction avec les re-
seaux autrichien et bavarois et en m&me temps le rattachement des limnimetres du lac de
Gonstance el du Rhin, enire Ragatz et Rheineck.

Pour les etudes geodösiques, la Commission a fix& comme programme de celle cam-
pagne, essentiellement les observations asironomiques et de pendule dans la.r&gion du Saint-
Gothard.

 

Dr An. HIRSCH.

 

EN

SE

wi

er

 
