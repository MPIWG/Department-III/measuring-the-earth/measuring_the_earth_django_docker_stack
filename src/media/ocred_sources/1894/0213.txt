 

 

 

ui je

met

a ilrwiı

TR

LIE TRAIL

8

Beilage B. IV.

DANEMARK

Bericht über die geodätischen Arbeiten im Jahre 1894.

I. TRIGONOMETRISCHE STATIONEN

Die in früheren Berichten erwähnte Revision der !rigonometrischen Stationen ist
fortgesetzt worden und so weit gebracht, dass sie voraussichtlich nächstes Jahr beendet
sein wird.”

Im dritten Bande der Dänischen Gradmessung findet sich S. 402-409 eine Note über
die Bewegung der Dreieckspunkte, welche S. 407 eine Zusammenstellung von drei verschiede-
nen Messungen der Winkel des Dreiecks Troldemosebanks-Dyret-Ejersbaonehöj enthält. Diese
Messungen sind beziehungsweise in den Jahren 1847, 1866 und 1867 ausgeführt. Während die
beiden letzteren mit einander in guter Deu sind, weichen sie von den Ergeb-
nissen des Jahres 1847 so viel ab, dass man, in Betracht der Festigkeit der Be
auf welchen die Punkte be Terehnet sind, in der Genauigkeit der Beobachtungen, die Er-
scheinung nur durch Bewegung des Bo hat erklären können. Obgleich eine in 1884-85
ausgeführte Neumessung der betreffenden Winkel keine merkbare Abweichung von den Ergeb-
nissen des Jahres 1867 zeigte, hat man es doch für richtig gehalten, die Messung im laufenden
Jahre zu wiederholen. Wie es aus der umstehenden Zusammenstellung hervorgeht, stimmen
die Resultate von 1894 vorzüglich mit denen von 1867 und es darf als höchst chen

bezeichnet werden, dass die Stationen sich seit 1867 nich! geändert haben.

 
