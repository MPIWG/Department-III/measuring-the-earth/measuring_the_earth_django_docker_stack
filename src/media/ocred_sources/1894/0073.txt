 

en

NH Mae

Hadbah
ui halan

rer

1A RR Amen lee

ri

ni

 

67
de rendre les trösors de la science, et surtout des sciences naturelles, accessibles A des
couches toujours plus nombreuses de la soci6te pour en tirer profit dans l’interet general.

« On s’expligque ainsi que vos travaux, Messieurs, bien que leur caractere scienti-
fique rigoureux les fassent paraitre souvent inaccessibles aux laiques, attirent une attention
toujours plus generale. Ge ne sont toutefois pas seulement les progres dans la connaissance
des dimensions et de la figure du globe, resultant de vos travaux, qui &veillent partout un
inter&t bienveillant pour vos conferences ; mais les peuples et leurs gouvernements saluent,
dans vos r&eunions, la re&alisation d’une des pensees les plus belles et les plus fertiles de notre
siecle, savoir la coop6ration de tous les Etats civilis6s et le travail combined des penseurs
eminents de toutes les nations, pour l’avancement des choses nobles et utiles, communes A
tous les peuples.

« Dans le developpement de ces pensces qui rapprochent les esprits et la r&pression
de celles qui separent les hommes, les nations et leurs gouvernemenis reconnaissent une des
conditions essenlielles de la paix generale, et par consequent de la conservation et de l’avan-
cement des plus grands biens de l’humanite.

« Vous me croirez done sinc£re, je l’espere, Messieurs, si je vous souhaite pour vos
ravaux tous les progres desirables el tout le .succes possible.

« J’en vois d6öja une des garanties principales dans la presence de votre venerable
president, de ce vetöran de la science, qui a bien voulu, pour quelque temps, quitter son
siege parmı les grandeurs intellectuelles de sa patrie, pour diriger avec son savoir et son
experience vos debals et appuyer vos deliberations de son autorite exceptionnelle. »

M. le President donne ensuite la parole aM. le professeur Ehrendorfer, recteur de
’Universite d’Innsbruck, qui s’exprime comme suit :

« Permettez, Messieurs, qu’au nom de l’Universite je salue cordialement Messieurs les
delegues ei la Commission permanente de l’Association g6odesique internationale dans cette
localit& que vous avez choisie pour votre session de cette annee.

«La geodösie est une science intimement liee a de nombreuses branches scienti-
fiques qui s’enseignent dans notre Universite, et qui contribue essentiellernent au progr6s de
toutes les autres avec lesquelles elle a des relations plus ou moins directes.

« Gela est d’autant plus generalement reconnu qu’on sail que la geodesie possede un
grand nombre d’applications pratiques importantes qu’elle promet de developper toujours
davantage.

« Je souhaite donc, au nom de l’Universite, la bienvenue aux membres de l’Asso-
ciation el j’exprime le vau que leurs travaux soient couronn&s de succes. »

Ensuite, M. le bourgmestre, Dr Mörz, prononce l’allocution suivante :
« Tres honores Messieurs,

«Je me sens tres flatt& de pouvoir saluer la Conference g&odesique au nom de
la ville d’Innsbruck.

 
