 

1
ä

TEEN

NT pe nlmin

Amımyn

IT Trier

ie

25

« Dieser Ansicht scheint auch Goulier gewesen zu sein, da er durch seine mühsamen
und zeitraubenden Untersuchungen doch zu keinem anderen Schlusse geführt wird, als dass
für die Ausführung von Präzisionsnivellements die Anwendung sogenannter Kompensations-
latten unumgänglich seı.

« Inwieweit gerade dieser Schluss in seiner speziellen Fassung gerechtfertigt ist,
kommt hier nicht ın Betracht, kann auch umsoweniger beurtheilt werden, als sich in der zur
Verfügung stehenden Literatur Näheres über dıe Einrichtung dieser Art von Latten nicht hat
ermitteln lassen und auch eine darauf bezügliche Notiz auf Seite 174 des von Kalmär’schen
Berichtes eine deutliche Vorstellung hierüber nicht bietet.

«9. Nachdem es der Technik gelungen ist, in dem Aluminium ein für Herstellung
von Nivellirlatten ın mehr als einer Hinsicht vorzügliches Material zu liefern, dürfte aller
Wahrscheinlichkeit nach der Zeitpunkt nicht mehr ferne liegen, wo die bisherigen Holzlatten
bei feineren Nivellements durch metallene verdrängt sein werden, welche nach dem Gedanken
von Professor Vogler in Berlin gleichzeitig als Stangenthermometer dienen. Unter diesem
Gesichtspunkte erscheint dıe vorgelegte Frage auch nicht mehr recht zeitgemäss, und da
man jetzt so viel bessere Mittel hat, die technischen Fehler zu beseitigen, so kann ich nicht
wohl den Beamten der Reichsanstalt eine so weitläufige und wenig Nutzen versprechende

Arbeit auferlegen, wie es die Durchmessung von Holzlalten sein würde.

« Der Präsident der Physikalisch-Technischen Reichsanstalt :

« gez. VON HELMHOLTZ. »

« Die ablehnende Antwort dürfte wohl ihren Grund mit darin haben, dass die Physi-
kalisch-Technische Reichsanstalt mit Arbeiten überhäuft ist. Unbeschadet der Stichhaltigkeit
der angegebenen Gründe der Ablehnung scheint mir doch eine weitere Untersuchung
erwünscht, und ich habe Jetzt Aussicht, dass Herr Dr. Stadthagen, Physiker der Kaiserlichen
Normal-Aichungskommission in Berlin, privatim, aber mit den Apparaten der genannien
Kommission, die gewünschten Versuche anstellen wird. Zunächst dürfte hierbei ins Auge
gefasst werden, die Länge z geradgewachsenen Tannenholzes als Function seiner Temperatur £
und seines durch Wägungen zu besiimmenden Feuchtigkeitsgehaltes y zu ermitteln, also die
Kläche z2= f (xy) aus einzelnen Punkten abzuleiten, wobei auf Nachwirkungen von vorn-
herein zu achten Ist.

« Was die Konstruetion der Nivellirlatten aus Metall anbelangt, so hat Herr Professor
Dr.Vogler, der in dem Antworischreiben des Herrn Präsidenten der Physikalisch-Technischen
Reichsanstalt genannt ist, mir eine längere Mittheilung sehr gefälligst zukommen lassen,
welche ich meinem Bericht als Anhang beifüge (siehe Beilage A. V). Versuche über das
thermische Verhalten des Aluminiums lagen im Mai d. J. von Seiten der genannten Reichs-
anslalt wenigstens in solchem Umfange vor, dass sich die Brauchbarkeit des Aluminiums,
wenn es vorher stark erhitzt wird, für untergeordnete metronomische Zwecke erkennen
liess; jedoch war noch unentschieden, ob es hinsichtlich der thermischen Nachwirkungen
dem Zink vorzuziehen sei.

 
