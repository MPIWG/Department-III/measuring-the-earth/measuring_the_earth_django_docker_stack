 

Tree een

|
'

LU

Ken

Hal

Re RN NT

Kain

 

13

 

Commission eine Anzahl von Exemplaren der erwähnten Denkschrift zu überreichen. Wir
haben in derselben die Gesichtspunkte zusammengefasst, die bei unseren Beratliungen zur
Geltung kamen. Sie hat aber nur den Zweck, die vorläufige Örientirung über die ins Auge
zu fassenden Untersuchungen und (lie Mittel ihrer Ausführung zu erleichtern, wir betrachten
sie nach keiner Richtung hin als ein bestimmtes Programm für die künftige Action.

« Genehmigen Sie, Herr Präsident, bei dieser Gelegenheit den Ausdruck unserer
ausgezeichnetsten Hochachtung und Ergebenheit.

« E. EHLERS U. v. WıLAmowırz-MÖLLENDORFF
«d.Z. Secrelär der malh.-phys. Klasse. d. Z. vorsilzender Secretär der K. Gesellschaft.

« Monsieur Faye, President de lu Commission geodesique internationale a Paris. »

« Das Präsidium hat davon durch folgendes Circular der Permanenten Commission
berichtet :

INTERNATIONALE ERDMESSUNG |
PERMANENTE COMMISSION. Paris und Neuchätel, den 13. Juli 1894.
CIROUBAR

« Hochgeehrte Herren Gollegen !

«Wir haben kürzlich ein Schreiben der Göltinger Gesellschaft der Wissenschaften
erhalten, wonach eine in Götlingen am 15.-16. Mai abgehaltene Delegirtenversammlung der
Akademieen von München, Wien, Göttingen und Leipzig auf Antrag der Wiener Akademie
beschlossen hat, den Zusammenhang zwischen der Intensität der Schwere und dem geologi-
schen Aufbau der Erdrinde zum Gegenstände gemeinsamer Forschung zu machen. Näheres
über Ziel und Mittel dieser Bestrebungen ist aus beiliegender Denkschrift zu ersehen.

« Von der Ansicht ausgehend, dass die Lösung einer so umfassenden Auligabe nur
durch die vereinigten Anstrengungen der Nationen möglich sei und namentlich der Unter-
stützung der Internationalen Erdmessung bedürfe, hat man in Göttingen beschlossen, eine
Conferenz von Delegirten zum 5. September nach Innsbruck zu berufen, zu welcher auch die
Akademieen von Berlin, London, Paris, Petersbure nnd Rom, sowie die Coast Survey der
Vereinigten Siaalen eingeladen sind, um mit unserer gleichzeitig dort tagenden Permanenten
Commission über den besagten Plan sich zu verständigen.

« Dabei wird sowohl am Schlusse der Denkschrift als im Schreiben der Göttinger
Gesellschaft hervorgehoben, dass es bei dieser Vorconferenz vielmehr auf wechselseitige
Information und freundschaftliches Binverständniss über die verfolgten Ziele, durch per-
sönliche Aussprache, als auf Fassung irgend welcher Beschlüsse über die Organisation des
Unternehmens abgesehen sei, welch’ letztere einer späteren, officiellen Gonferenz vorbehalten
bleibe.

« Schliesslich ersuchen uns die Herren Secretäre der Göttinger Gesellschaft, den

 
