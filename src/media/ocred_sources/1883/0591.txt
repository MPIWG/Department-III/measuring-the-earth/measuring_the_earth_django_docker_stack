 

19

Zur Entwickelung der Grösse
| r dn
e k= 42 =
n dr
benutze ich folgende Bezeichnungen und Constanten:
i Temperatur in der Höhe «;
dt

t = — -- Abnahme der Temperatur (in Gr. C.), wenn man um eine Längen-

ae
| einheit (1") höher steigt;

d == 0.003668 Ausdehnungscoéfficient der Luft für 1° C.;

D Dichtigkeit der Luft in der Höhe x;

p Elasticitätsdruck der Luft in derselben Höhe; entspricht also der Dichtig-
keit D bei der Temperatur t oder der Dichtiskeit D(1-+ at) bei 0° Temperatur;
constantes Verhältniss der brechenden Kraft der Luft n2 — 1 zur Dichtig-
keit D;

9, == 929061 und

g = 92 = 979061 A — 9.00259 cos 2 @)

im Meeresniveau, g, unter 45° Breite, g unter der Breite g;
C das ‘fir Luft von mittlerer Feuchtigkeit geltende Verhältniss zwischen

©

Acceleration der Schwerkraft

Elasticitätsdruck und Dichtigkeit;

 

 

 

13

= = 7961.4 Meter.
Jo
Die Gleichung
ae tn
Te a MR
geht, weil
n=Vi+eD
in die folgende über: £
(eal tk d D
ae "Ten ae
Es ist aber
= OD. (1: ot),
daher
4N 1 ar... ee
dx C(i-+et) ds 1l+at dz
oder, wenn substituirt wird
Gp. i dd.
a Pe ) und een
*) Mit Vernachlässigung des Factors (1 — € =), wo € eine von der Meereshöhe des unter dem
&

dagegen 2, wenn dieselbe = 0 ist.

|
4

Lichtstrahl befindlichen Landes abhängige Grösse ist, welche °/, zu setzen wäre, wenn diese Höhe gleich x

 
