 

 

 

 

 

 

 

 

 

 

an

Regierungsrath Nagel, der in Folge eines Augenleidens für diesmal verhindert
ist der Conferenz beizuwohnen, erklärt schriftlich, dass ihn die sächsische Regierung
durch Decret vom 5. September 1. J. autorisirt habe, sich für den Greenwicher Meridian
als Ausgangsmeridian zu entscheiden.

In Uebereinstimmung mit einer Anfrage des Präsidenten erklärt die permanente
Commission, dass die schriftlichen Voten des Generals Gaeyer durch v. Oppolzer und
jene des Prof. Nagel durch Hirsch bei der Abstimmung abgegeben werden.

Hierauf nimmt die Commission einstimmig die in den früheren Sitzungen geltende
Geschäftsordnung an.

Dem §. 1 der eben angenommenen Geschäftsordnung entsprechend, hat die
permanente Commission der allgemeinen Üonferenz in ihrer ersten Sitzung Vorschläge
bezüglich des zu bildenden Bureaus zu machen. Vorbehaltlich der aus der Versammlung
kommenden Vorschläge einigt sich die Commission über die folgende von dem Präsidenten
vorgeschlagene Wahlliste:

(seneral Daeyer, Ehrenpräsident.

Obert Ferrero, Präsident.

v. Bauernfeind und Faye, Vicepräsidenten.

Hirsch, v. Oppolzer, Schriftführer.

Hierauf wird das Programm für die nächste allgemeine Sitzung in der folgenden
Weise festgestellt:

1) Der in beiden Verhandlungs-Sprachen vorzulesende Bericht der permanenten
Commission.

2) Der Bericht des Centralbureaus.

3) Der Bericht des Schriftführers Hirsch, itber die Annahme eines einheitlichen
Ausgangsmeridians und einer gemeinsamen Zeit.

Anknüpfend an dieses Programm und seinen Bericht spricht Hirsch den Wunsch
aus, dass im Interesse der Vertiefung der aufgestellten Fragen eine Specialcommission in
der ersten Sitzung ernannt werde, um einen diesbezüglichen Bericht in einer der fol-
genden diesem Gegenstande ausschliesslich gewidmeten Sitzung der allgemeinen Conferenz
zur Discussion vorzulegen. :

v. Bauernfeind macht auf ein Versehen in der vom Centralbureau her ausgegebenen
Liste der Bevollmächtigten für die Europäische Gradmessung aufmerksam, welche Prof.
Dr. H. Seeliger, Director der Sternwarte in Bogenhausen, nicht als Bevollmächtigten
aufführt.

Der Präsident schliesst die Sitzung um 3% Nachmitta

os,

 

 

 

 

 
