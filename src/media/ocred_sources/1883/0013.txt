 

IX

Anhänge. Annexes.

Annexe I. Rapport sur les déterminations de longitude, de latitude et d’azimut, par
M. H. @. van de Sande-Bakhuyzen.
Annexe II. Rapport sur les triangulations, par M. À. Ferrero.
(Annexé sous forme de livraison spéciale). (Liegt als besonderes Heft bei).
Annexe III. Rapport sur la mesure des bases, par M. le Colonel Perrier.
Annexe IV. Rapport sur l'état actuel des travaux du nivellement de précision exécutés dans les
différents pays de l'Association, par M. Ad. Hirsch.
Annexe V, Rapport sur l’état actuel des travaux faits pour la détermination du niveau moyen
des mers de l'Europe continentale, par M. Ibanez.
Annex VIa. Bericht über die Bestimmung der Schwere mit Hilfe verschiedener Apparate, von
Theodor von Oppolzer.
Annexe VIb. Rapport sur la détermination de la pesanteur à l’aide des différents appareils, par M. Theo-
dore von Oppolzer.
Annex VII. Neue Untersuchungen über terrestrische Refraction, von Carl von Bauernfeind.
Anhang: Zur Theorie der terrestrischen Refraction, von Prof. C. Fearnley.
Annex VIII. Literatur der praktischen und theoretischen Gradmessungs-Arbeiten, zweite Mittheilung, zu-
sammengestellt von O. Bérsch.

Verzeichniss der Tafeln. — Table des Planches.

I. Marégraphe du Havre. Appareil enregistreur.

II. Marégraphe de Nice. Appareil enregistreur.

III. Installation du Marégraphe de Nice.

IV. Rete geodetica italiana.

V. Plan eines Feldobservatoriums auf den astronomischen Stationen lter Ordnung des K. K. militär-

geographischen Instituts.

VI. Skizze von vier Nivellementspolygonen der Alpenländer Oesterreichs.

VII. Carte demonstrative des determinations télégraphiques de differences de longitudes.
VIII. Carte demonstrative des déterminations astronomiques de latitude et d’azimuth.

IX. Canevas trigonométrique indiquant lavancement des travaux géodésiques en Europe.

X. Carte des Nivellements de précision, exécutés ou projetés en Europe.

 
