    

SSS eee

 

servations du pendule a fil, sont moins satisfaisantes, quoiqu’on ne puisse pas lui faire
d’objections importantes au point de vue théorique. ;

Les difficultés qu’il rencontra étaient assez grandes, et ce sont elles probablement
qui lui ont donné l’idée d'exposer, à la fin de son remarquable mémoire sur la longueur
du pendule simple & seconde, les avantages du pendule à réversion qu'il avait modifié,
mais qu'il n'avait pas expérimenté lui même. Il est très regrettable que Bessel nait
pas pu employer l'appareil imaginé par lui et qui constitue un progrès faisant époque
dans les observations de pendule. S'il avait pu lui même fixer les méthodes d'observation,
il aurait écarté bien des jugements défavorables, bien des reproches, qui, en réalité,
doivent être appliqués plutôt aux observateurs qu’à l'appareil.

Dans le courant du présent rapport nous aurons plusieurs fois l’occasion de revenir
sur les avantages du pendule à réversion de Bessel. D'après ces explications et dans
l’état actuel de la question, nous pouvons admettre qu’on peut avec une précision suffisante
éliminer du problème la détermination de l'influence de l'air.

Pour pouvoir établir la relation avec la pesanteur, il faut combiner le temps
d’une oscillation avec certaines mesures de longueur empruntées à l’appareil employé, et
dont l'évaluation exacte présente des difficultés particulières qu’on ne peut pas éviter.
Je veux exposer maintenant les expédients qu'on à proposés pour vaincre cette difficulté.

D'abord il s’agit de la suspension du pendule et de la détermination de l'axe de
rotation du système. — Jusqu'à présent on a employé presque généralement la suspension
sur des couteaux et dans la plupart des cas, on a considéré l’arête des couteaux comme
axe de rotation. —

Cette supposition est, cela n’est pas douteux, plus ou moins arbitraire, et il
faudra en examiner la justesse. Bessel s’est rendu indépendant de cette difficulté en
déterminant la différence de deux pendules suspendus de la même manière, et Finger a
également choisi le même procédé pour son pendule à commutation.

Mais, outre que cette détermination différentielle nécessite la déduction de deux
résultats, et que la différence ainsi trouvée est altérée par des erreurs d'observation qui
se transmettent plus grandes encore au résultat final, il sera toujours préférable d'arriver
dans cette voie au moyen d'expériences directes. — On trouve donc ici une source
d'erreurs dont l'élimination rencontre de grandes difficultés; d’abord il est évident que
le couteau ne peut pas être considéré comme une ligne idéale, mais qu'il présente un
convexité plus ou moins prononcée.

Pour échapper à cet inconvenient Villarceau a proposé l’emploi de cylindres, (Procès-
Verbaux des séances de la Commission permanente tenues à Genève 1879, page 65, An-
nexe) pour remplacer les couteaux dans la suspension du pendule, et il a montré qu’on
peut effectuer toutes les réductions nécessaires avec une précision suffisante.

M. C. S. Peirce (Comptes Rendus de la 6% conférence générale de l'Association
Géodésique internationale, page 31) a été, je crois le seul qui ait soumis à l'épreuve la
proposition de Villarceau, et il a rencontré les difficultés suivantes: d’un cöt& le pendule
cesse bientôt d’osciller, et d’un autre côté les surfaces des cylindres s’usent en peu de

|
|

nn PES

|

sense DB

 

 
