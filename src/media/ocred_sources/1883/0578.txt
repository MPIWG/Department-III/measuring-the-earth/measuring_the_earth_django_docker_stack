 

6

| Beilage

zum Referate des Herrn v. Bauernfeind über terrestrische Refraction.

Uebersicht der neuerdings in Russland ausgeführten Arbeiten über terrestrische Refraction,
zusammengestellt vom Adjunkt-Astronomen der Pulkowaer Sternwarte, Obristen N. Zinger,

aus dem Russischen übersetzt von Lindemann.

Als in letzter Zeit in Russland ausgeführte Untersuchungen über den Einfluss
der terrestrischen Refraction wären zu nennen:

I. Die theoretischen Bemerkungen, welche im Aufsatze des Prof. Kowalski
„Recherches sur la refraction astronomique. Kasan. 1878.“ (p. 51—57)
enthalten sind.

II. Untersuchungen über die Wirkung der terrestrischen Refraction bei kleinen
Entfernungen, im XXXVI. Band. der Memoiren der Militair-Topographischen
Abtheilung des Generalstabs für 1878, in einem Aufsatze des Obristen
Zinger „Nivellirarbeiten mit einem Nivellirtheodoliten auf Eisenbahnen“

(p. 47—61).
| IH. Schlüsse des Kapitäns Pomeranzeff aus Zenithdistanzbeobachtungen terrestri-
| : scher Objecte, welche 1875—76 in Pulkowa von Offizieren der Akademie

des Generalstabs ausgeführt worden. (Die eben beendete Abhandlung
Herrn Pomeranzeff’s ist noch nicht im Druck erschienen.)

1.

Von bedeutender Tragweite ist der Nachweis Prof. Kowalski’s, dass die Grösse
der Refraction, auf dem gewöhnlichen Wege aus gleichzeitigen correspondirenden Zenith-
distanzmessungen an zwei Punkten, unter der Voraussetzung, dass die Refraction an
diesen beiden Punkten dieselbe ist, abgeleitet, eben so zuverlässig ist, d.h. einen Fehler
derselben Ordnung besitzt, wie die nach der Formel

u — 1 fy pat ét)) a

mt Spee 2

*) Hier ist ¢ und ¢’ die Temperatur, p und p’ der Luftdruck an den beiden Punkten; h der
Höhenunterschied der Punkte; c der Winkel zwischen denselben am Centrum der Erde; a der Erdradius;
u der Brechungscoöfficient; « der Ausdehnungscoéfficient der Luft.

 

 
