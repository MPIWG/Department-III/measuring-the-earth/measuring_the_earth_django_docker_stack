 

 

 

 

 

14

die mangelhafte Stabilität mit dem Instrumente an sich nichts zu schaffen hat. Einer
wesentlich solideren Construction des Stativs und somit einer Erhöhung seiner Stabilität
steht nichts im Wege, doch wird hieraus kein nennenswerther Vortheil resultiren, da wie
U. S. Peirce (Bericht der 6. Allgemeinen Conferenz 1880, pag. 86) richtig bemerkt, selbst für
die solideste Construction eine minimale Bewegung kaum ganz zu vermeiden sein dürfte.
Jedenfalls darf man die Unveränderlichkeit des Lagers bei dem Schwingen des Pendels,
wenn nicht vorhergehende Versuche diese Annahme erhärten, nicht a priori als vorhanden
betrachten. In der That haben Kater, Bessel und Andere sich durch die Anwendung des
Hardy’schen Noddypendels von der Stabilität ihres Statives überzeugt; für dieses Pendel
giebt ©. S. Peirce (Methods and Results of pendulum experiments, Washington 1882,
pag. 69) eine mathematische Theorie.

Bei dieser Gelegenheit dürfte es am Platze sein, auf die Benutzung eines ori-
einellen Versicherungspendels aufmerksam zu machen, dessen Anwendung wenig bekannt
ist und welches Lamont bei den Münchener Versuchen durch Oberst von Orff in Vorschlag
gebracht hat.

Man befestigt nahe dem Auflager ein Fadenpendel, dessen Schwingungsdauer
der des Reversionspendels möglichst nahe gebracht wird und schützt dasselbe vor
äusseren Einflüssen durch eine Glasröhre. Hat nun die Lagerfläche eine Bewegung,
welche von dem schwingenden Pendel abhängt, so werden sich die Impulse am Faden-
pendel wegen der nahezu gleichen Schwingungsdauer summiren, und nach einiger Zeit
wird das Fadenpendel recht merkliche Schwingungen zeigen, die übrigens je nach dem
Zusammenfallen der Schwingungszeiten beider Pendel in grösseren oder kleineren Perioden
anwachsen und verschwinden werden.

Oberst von Orff hat in der während gegenwärtiger Session zur Vorlage gelangten
Abhandlung (Bestimmung der Länge des einfachen Secundenpendels auf der Sternwarte zu
Bogenhausen aus den Abhandlungen der k. bayerischen Akademie der Wissenschaften
II. Cl. XIV. Bd. III. Abth.) in eleganter Weise gezeigt, wie man aus der Beobachtung
jenes Fadenpendels einen gesicherten Schluss auf die Grösse der Mitschwingung des
Stativs machen könne, doch verdient dieser Apparat — auch abgesehen von dieser werth-
vollen theoretischen Bemerkung von Orf’s — dass die Aufmerksamkeit darauf gelenkt
werde, als auf einen leicht herstellbaren Controlapparat, um daran das Vorhandensein
des Mitschwingens eines sehr soliden Stativs zu widerlegen oder zu erweisen.

C. S. Peirce war der Erste, welcher durch Versuche, denen man später das
Prädicat der statischen gegeben hat, das Vorhandensein einer sehr bedeutenden Fehler-
quelle bei dem Repsold’schen Reversionspendel practisch nachgewiesen, theoretisch ver-
folgt und mit einer bedeutenden Annäherung numerisch bestimmt hat. Pezrce’s Ver-
dienst in dieser Sache ist ein bedeutendes, indem er und General Baeyer anfänglich vor
Publication seiner entscheidenden und schlagenden Versuche fast allein die Ansicht der
Flexibilität des Statives vertraten und hierbei lebhaften Widerspruch fanden, dem auch
der Berichterstatter sich seinerzeit angeschlossen hatte.

Plantamour hat (in seinen ,Recherches expérimentales sur le mouvement simul-

 

|
|
|
|
|
|
|
|

 
