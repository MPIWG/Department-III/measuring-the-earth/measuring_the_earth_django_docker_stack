 

23 —

Triangulation du Parallèle de Madrid.

 

 

 

DIRECTEURS |
No Bann LATITUDE, |LONGITUDE. ÉPOQUE. INSTRUMENTS. |
DES POINTS. ET OBSERVATEURS. |
152 | Jayalon. . . . ... 40° 13'47"| 16°15'14")| 1863-73 | Barraquer, Cabello. es n° 1, Rep- |
| sold A. |
153 | Palomera...... 40 35 53 | 16 28 8 || 1863 Barraquer. _ Repsold n° 1.
154 | Javalambre . . .. | 40 5 49 | 16 38 52 | 1864 id. id. |
155 | Pefiarroya..... 20.95.04 17 0 27 |. 1868 id. id. |
ua Bmn ......->- 40 143 | 17 239 | 1863-68 | Barraquer, Solano. Repsold n° 1, Rep- |
sold C. |
157 | Peñagolosa. . . .. 4041399 17 19-22 | 1804 Barraquer. Repsold n° 1. |
158 | Espadan . . 39 54 21 | 17 17 29 | 1864-68 | Barraquer, Solano. Repsold n° 1, Rep- |
| so Us |
159 | Desierto ...... 40 5 7 | 17 42 12 | 1864-76 | Barraquer, Hernandez. Repsold n° 1,Rep. 4)
|
Triangulation de la Cote Nord.
160 | S. Sebastian. . AP ADA 9719 OO 1870 Hernandez. Pistor 1 -3. |
161 | Tremuzo . ..... 49 49 35 | 843 11 | 1870 els id. |
Hoe.) Gedeira i. i... 43 10 10 9 8 48] 1870 id. id. |
1634 COMA. a, 5 8 43 516 9 45 38 || 1871 id. ale |
NCA Bason. .... :°.., 43 33 16 9 25 56 | 1870 id. id. |
169 Gistrab.. .. . 43 27 36 | 10 4 47 | 1871 id. id. |
166 | Pradairo:...... 49 491 4002 59 1871 id. id. |
HO Le a 43 2347 | 10.43 341 1871 id. id.
168 | Miravalles..... 42 52 48 | 10 53 42 | 1871 id. id.
169 | Rabe®. . .... 2,48 ©5551 1116.15 | 1872 Eugenio. - Pistor n° 4. :
170 Balancas...... 4381.15 1 19: 94 | 1872 id. id. |
171 | Tazônes. . . 43 39 48 | 19 15 59 | 1872 | Monet. Pistor n° 2. |
172 | Mofrecho...... 43 2407 119 87:53: 1872 id. id. |
das Gontes .\.. .+;.. Aal: 4.1955 45 | 1870 id. id. |
174 | Espigüete. . . . .. 42:56:40! 19:59" 35 | 1871 id. id. |
175 | Valdecebollas . . . | 42 58 0 | 13 18 26 | 1870 1d: id. |
1:76... 16108. . 22.2... AD Mi 24 18 91 40° »1870 | id. id.
Ti Solluve: :...... 43 22 15 | 14 54 32 | 1862 Quiroga, Monet. Ertel n° 2.
178 /AITUITZ N. . . : . 43,6 36. 15.52 55 | 1862 id. Id:
m Anduz »...... 43 15 387.15, 21° 69) 1862 id. id.
180 | Aitzgorri...... 42 57 99 | 15 20 36 | 1862 id. id.
181 | Irumugarrieta. . . | 43 0 0 |15 3848 | 1862 id. id.
182 Faro een 4419 1. 1509.47 | 1862 (Ce id.
deo.) Bealtza.:. . . ...:. ne 022 T5 bb = 7, | 1862-71 | Quiroga, Monet, Urriarte. | Ertel n° 3, Repsold
| sl
184 | La Rhune ..... 1892 16213 | 1862 Quiroga, Monet. Ertel mn? 2:
185 | Biarritz... ... 48 98 97 | 16 700| 1862 rd id.
186 | Baigoura. ..... da 17 26 1602 47 | 1862 id. id.
Triangulation du Parallèle de Palencia.
187 |-Castrove........ 49° 28'48"| . 8° 58' 23" | 1865-76 | Caramés, Monet. Repsold A, Pistor |
| D
188 Galiñeiro ob 42 8 8| 858 18 | 1865-76 id. Se id
ee Avion +... 49 18 9| 992416) 1876 | Monet. > Pistor n° 2. |
190 Gostosa....... 19 928 9 9411) 4860-76. | Oaramös, ‚Monet: Repsold A, Pistor |
701 |(Penama.... . ... - 42 942 | 9 51 40 | 1868-76 id. un |
192 | Laroneo. . ..... 41 52 49 | 957 9 | 1866-76 id. id. |
eixo 42 11 39 | 101854 | 1866 | Caramés. | |
Pe Mairos 6. so. 41 50 55 | 10 20 22 | 1866 sue” en |
AD POOPOR: >...’ . 41 54 53 | 10 40 9 | 1866-76 Caramés, Monet. Repsold A, Pistor |
| m 98 |
196 | Moncalvo. . .... 42 11 53 | 10 51 50 | 1866 | Caramés, Repsold À. |

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

   

 

 

Rat Conti = ol oe ee ee ae

 

SC Et Poe (ee Lan es en en 1

Oo.

fad = pe à
