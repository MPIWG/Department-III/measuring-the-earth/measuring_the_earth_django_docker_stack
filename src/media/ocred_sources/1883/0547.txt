 

se)

5 quia À

come |
je
a

, gm

joo 0

Cu gl

js |

sentent qu'une faible partie de la quantité entière. (C’est le cas pour la question qui
nous occupe, car la pesanteur, dans les localités qui nous sont accessibles, n’est sujette
qu'à des variations insignifiantes. Les méthodes de la détermination de la pesanteur
différeront donc, selon qu’on se proposera de déterminer la valeur ablolue ou la valeur
relative, et il faudra juger également les appareils à ce double point de vue. On a
souvent donné lieu à des malentendus, en ne tenant pas assez compte de la nécessité
d'une séparation des ces deux méthodes.

Les méthodes et les appareils employés pour la détermination absolue de la
pesanteur doivent satisfaire à cette condition: éviter autant que possible toutes les sources
d'erreur qui pourraient influencer le résultat d’une manière constante, ou tout au moins
les déterminer avec une approximation suffisante. C’est à ce point de vue que l’on
choisira la méthode, sans se soucier si elle est plus ou moins compliquée, ou si son emploi
demande plus ou moins de temps, sans regarder non plus si les résultats particuliers se
concordent mieux ou moins bien que ceux qu’on pourrait obtenir à l’aide d'appareils sujets
à des sources d'erreurs systématiques ou à des inexactitudes qu’on ne peut pas rigoureu-
sement déterminer.

Dans le choix des méthodes et des appareils pour la détermination relative de
la pesanteur on se placera & un point de vue complétement different. Les sources
d’erreurs constantes n’auront presque point d’influence, mais il faudra éliminer celles qui
provoquent les différences entre les résultats particuliers du méme appareil.

Au premier coup d’oeil on serait porté à n’ajouter qu'une importance secondaire
aux déterminations relatives, puisqu'elles ne peuvent pas se passer des déterminations ab-
solues comme base, au moins pour les points normaux.

Mais si l’on considère qu’il faudrait déterminer la pesanteur pour le plus grand
nombre possible de points sur la surface de la Terre, si l’on considère en outre que Sur-
tout dans les conditions présentées par des observatoires passagers, les déterminations
relatives peuvent être exécutées avec beaucoup plus de facilité, de vitesse et de sûreté
que les mesures absolues, si l’on considère enfin que dans la recherche de la figure de
la Terre par les observations du pendule, c’est surtout le rapport de la pesanteur qui
intervient, on reconnaîtra que ce sont précisement les déterminations relatives qui ont
une importance particulière pour la géodésie et qu’on pourra restreindre les mesures
absolues à un petit nombre de points favorables pour l'observation.

MM. Walker et Herschel sont arrivés à des conclusions semblables dans la publication
relative à l'emploi des matériaux si riches recueillis aux Indes par MM. Basevi et Heaveside,
et de méme les procés-verbaux de la Conférence générale de Munich (1880, pag. 33)
montrent que des vues analogues ont été émises dans la discussion.

Il va sans dire que dans le présent rapport on ne pourra pas traiter cette
question d’une manière complète et qu’on sera obligé de se contenter d'y énumérer les
expériences recueillies et utilisées en ces derniers temps, et de les analyser aux points.
de vue que nous venons d'exposer. Depuis quelques années on à fait tant de progrès et
tant d’autres sont préparés par des publications et des travaux éminents, que le

 

 
