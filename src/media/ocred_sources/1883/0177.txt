l ( Ua,
Wing
ah
2 Li

Te ty
dt
“Ty
Ver hin
Ment u
Tell
al din
Brey
cho da

ik

‘ident
088 a
mérité
ent de

nant
lent à
odésie

er de

ident
il de
ok

qu
piel
pa

pie

ra
de
(0,

 

Tagen m

163

QUATRIEME SEANCE

de la Conference generale.

Rome, le 18 Octobre 1883.

La séance est reprise à 2h 45m,

M. Hirsch lit une dépêche envoyée par M. le Général Baeyer, dans laquelle il
remercie la Conférence de l’avoir nommé président honoraire dans la première séance.

Ensuite MM. les Secrétaires ». Oppolzer et Hirsch donnent lecture du procès-
verbal de la deuxième séance, en langue allemande et française.

Après avoir tenu compte de quelques observations faites par Messieurs Æennequin,
Betocchi, dé Rossi, Faye, Perrier et Barraquer, le procès-verbal est adopté par la
Conférence.

M. le Président donne la parole à M, van de Sande-Bakhuyzen pour lire le rapport

sur les travaux des Pays-Bas. (Voir Rapport général pour 1883, Pays-Bas.)

M. le Président demande au deuxième délégué de la Commission hollandaise
présent, M. Schols, s’il aurait quelque chose à ajouter au rapport de son collègue; M. Schols
ayant repondu négativement, M. le Président ouvre la discussion sur ce rapport.

M. Faye prie M. v. Bakhuyzen de vouloir lui donner encore quelques explications
sur les marques placées pour indiquer le niveau moyen de la mer à Amsterdam, et dont
il à déjà parlé dans la dernière séance.

M. v. Bakhuyzen explique que depuis deux siècles, pour pouvoir fermer à temps
les écluses qui garantissent le pays contre les hautes mers, on lit régulièrement sur des
marques convenablement fixées le niveau de la mer; les registres de ces observations
ont été toujours conservés; de cette façon on peut aujourd’hui calculer quelle fut, pendant
toute cette époque, la hauteur moyenne des hautes eaux. Dans l’origine il y avait 8 de
ces pierres; maintenant il en existe encore 5; puisque les observations qu’on a continue
à faire sur ces 5 pierres, ont donné des résultats qui ne different des anciens que tout
au plus de 8 millimètres, on peut en déduire linvariabilité à la fois de ces marques et
de la hauteur moyenne de la mer à Amsterdam.

aile

 
