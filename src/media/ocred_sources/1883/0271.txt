 

|
|

u

Au mois de juillet dernier, l’ing6nieur géographe Mansueti, de l'Institut géogra-
phique militaire, en se rendant à une haute station dans les Alpes, tomba dans un
précipice et termina ainsi sa Carrière.

Il n’avait pas encore trente ans, mais il était déjà connu et estimé pour ses
travaux: son avenir s’annoncait sous les plus beaux auspices, lorsqu'il perit pour le service
de la science et du pays.

Il est bien juste que son nom ne passe pas inobservé par vous et qu'il figure
dans le martyrologe de la Science.

Le Président de la Commission géodésique Italienne
A, Ferrero.

Niederlande. Pays-Bas.

Résumé des travaux géodésiques exécutés dans les Pays-Bas en 1883.

Triangulation. Aprés la mort de notre regretté collégue M. Stamkart les obser-
vations trigonométriques ont été suspendues. Comme le nivellement de précision touche
à sa fin, nous avons cru qu'il valait mieux employer tous nos moyens à l’achèvement de
cette partie de notre tâche, avant de reprendre les observations trigonométriques. De
cette manière nous aurons le temps d'examiner en détail les observations qui ont été
faites, afin de savoir jusqu'à quel point nous aurons besoin de les refaire sur quelques
stations. Après l'achèvement du nivellement de précision, nous espérons reprendre active-
ment ces observations et les mener à bonne fin.

Détermination des longitudes. Par suite du manque de calculateurs, les réductions
des observations faites en 1880 et 1881 pour la determination de la difference de lon-
gitude entre Leide et Greenwich ont été confiées aux soins du personel de l’obser-
vatoire de Leide. Pour autant que les travaux de Vobservatoire le permettaient, ces
calculs ont été poursuivis. On espère pouvoir les terminer dans quelques semaines.

Le projet de déterminer en 1883 la différence de longitude Paris-Leide wa pu
étre éxécuté par suite d’empéchement de la part des astronomes Francais. A la, suite:
des pourparlers avec le colonel Perrier, chef du service géographique militaire à Paris,
on à décidé que les observations seront faites aux mois de Mai, Juin et Juillet prochain,
par le commandant Bassot et M. 7. G. van de Sande Bakhuyzen.

Nivellement de précision. Aprés V’achevement du réseau hypsométrique dans les:
provinces du sud et de l’ouest, il était nécessaire de l’étendre dans les provinees du Nord.
Bien qu’en 1875 et 1876 une ligne d'Amsterdam à Nieuwe-Schaus, passant par
Deventer, Assen et Winschoten ait été nivelée pour le raccordement avec le

nivellement Prussien, on n’avait déterminé les cotes que d'un petit nombre de reperes,
33

 
