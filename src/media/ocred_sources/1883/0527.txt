 

DEN SRE RNA

 

 

A

   

19
1) schweres Pendel, schweres Gewicht unten = Tu’.
2) ” ” ” ” chen Fos
3) leichtes ‘ A x unten — Tu,
4) ” ” ” ” oben = To,

Diese Schwingungszeiten wirken auf das Resultat 7, die Schwingungszeit des
Meterpendels, je nach der Construction des Apparates und dem Gewichte des Pendels in
sehr differenter Weise ein; so z. B. wird für den der österreichischen Gradmessung ge-
hörenden Apparat dieser Einfluss durch die folgenden Relationen dargestellt sein:

di 02 ot 15 die — 14 dio LE OG aio

Den grössten Einfluss nimmt sonach auf die Bestimmung der Schwingungszeit
Tu’. Man wird die Beobachtungen derart anzuordnen haben, dass diesen Verhältnissen
Rechnung getragen wird. Die Genauigkeit, mit welcher die Schwingungszeit ermittelt
werden Kann, wird nahezu der Zeitdauer eines Beobachtungssatzes proportional sein, man
wird also die Verhältnisse des Pendels dadurch berücksichtigen, dass man die Beobachtungen
bei vollem Gewicht oben und unten innerhalb derselben Amplitudengrenzen vornimmt.
Hierbei werden die für das schwere Pendel ermittelten Schwingungszeiten wesentlich ge-
nauer ausfallen, da für dieses Pendel die Schwingungsdauer an sich innerhalb derselben Am-
plitudengrenzen eine längere ist und überdies ein Fehler in diesen Bestimmungen einen
geringeren Einfluss auf das Resultat hat, als beim leichten. Pendel. Es würde sich nun
scheinbar empfehlen, die Beobachtungen am leichten Pendel durch Wiederholung einer
grösseren Genauigkeit zuzuführen; aber hierbei darf nicht vergessen werden, dass zu
derartigen Bestimmungen die Kenntniss des Uhrganges nothwendig ist.

Man wird nicht allzuweit fehlen, wenn man die Unsicherheit im täglichen Gange,
selbst in dem Falle, dass die Vergleichsuhr in einem Raume von constanter Temperatur
vor den täglichen Gangschwankungen geschützt ist, mit 0°3 annimmt, sonach die Un-
sicherheit in der Annahme der Schwingungszeit der Vergleichsuhr auf den 300 000. Theil
einer Schwingung schätzt; diese Genauigkeit für die Schwingungszeit des Reversions-
pendels wird bei der Schärfe der angewandten Coincidenzmethode durch Einen Beobach-
tungssatz nicht nur erreicht, sondern sogar übertroffen werden können, da für das leichte
Pendel — schweres Gewicht unten — die Dauer der Beobachtung, ohne dass man zu
Amplituden unter 20° herabgehen muss, auf 48 Minuten ausgedehnt werden kann; bei
vollem Gewicht oben wird dieser Zeitraum etwa 20 Minuten umfassen.

Beschränkt man sich nicht auf die erste und letzte Coincidenz, sondern beobachtet
alle gleichmässig, so werden sich die Schwingungszeiten für das leichte Pendel einschliess-
lich der Unsicherheit des Uhrganges bis auf etwa den 200000. Theil richtig ergeben.
Die Unsicherheit des Ganges aber geht im 3.5fachen Betrage auf dT, letzterer Fehler
doppelt vergrössert auf die Schwerkraft über; jene Unsicherheit wirkt also in 7 facher
Vergrösserung und erhöht die Unsicherheit eines Beobachtungssatzes auf etwa den
30000. Theil der Schwerkraft. Die Wiederholung der Beobachtungen wird die durch
das Zeitmaass eingeführte Unsicherheit zwar verringern und, etwa 10—12 mal erneuert,

3*

 
