 

 

 

10

die Messungen in gleichen Abständen von der Mitte vornehmen müssen, wenn man diese
Fehlerquelle in eben demselben Maasse eliminiren will.

Der Vertauschbarkeit der Schneiden ist ferner zum Vorwurfe gemacht worden,
dass die Festigkeit der Verbindung des Pendels mit den Schneiden nicht hinreichend
gesichert erscheine. Nun ist die verlässliche W irkung der Klemmschrauben eine all-
bekannte Thatsache und wird nur dann in Frage gestellt, wenn der Apparat durch zahl-
reiche Erschütterungen getroffen wird; da solche jedoch im Allgemeinen nur bei Trans-
porten, nicht bei der Beobachtung selbst vorkommen, übrigens ihre Wirkung durch die
Vergleichung des Pendels mit dem Comparator völlig behoben werden kann, so verliert
jener Vorwurf jedwede Bedeutung. Gleichwohl würde es ein Verkennen der Sachlage
sein, wollte man ein derartig construirtes Pendel — die Schneiden ein für allemal
klemmend — als invariables Pendel verwenden, da bei Transporten in der That ein
Nachlassen der Klemmschrauben nicht ausgeschlossen ist.

Die vorausgehenden Auseinandersetzungen zeigen, dass in Beziehung auf absolute
Schwerebestimmung nichts Erhebliches gegen die Vertauschbarkeit der Schneiden ein-
gewendet werden kann, dass man durch dieselbe vielmehr den grossen Vortheil erzielt,
die aus der ungleichen Oylindrieität der Schneiden resultirenden Fehler zu eliminiren;
der Berichterstatter steht daher nicht an, diese Einrichtung für einen besonderen und
unschätzbaren Vorzug des Bessel’schen Pendels gegenüber dem Kater’schen convertiblen
Pendel zu erklären. Bei dem letzteren wird, wenn es zu absoluten Messungen der
Schwerkraft verwendet wird, die Elimination der cylindrischen Gestalt der Schneiden nur
dann eintreten, wenn beide Schneiden die gleiche Gestalt haben, welche Annahme kaum
je wird gemacht werden dürfen.

Es soll hier auf die Vortheile hingewiesen werden, welche die Einhaltung der-
selben Amplitudengrenzen in allen Lagen des Pendels für die Beobachtung bietet. Die
oben gemachten Betrachtungen über die Elimination der Cylindrieität der Schneiden
verlieren ihre Geltung, wenn die in Betracht gezogene Form der Schneiden den that-
sächlichen Verhältnissen nicht entspricht. Wesentliche Fehler in dieser Richtung werden
sich dadurch zeigen, dass — abgesehen von der Reduction auf den unendlich kleinen
Schwingungsbogen — die Schwingungszeiten Functionen der Amplituden werden. Ver-
bindet man nun zur Ableitung des Resultates die in beiden Lagen des Pendels bei
gleichen Amplituden erhaltenen Schwingungszeiten, so wird man der Hauptsache nach
— wie dies schon Bessel in seiner berühmten Abhandlung pag. 146 ff. gezeigt hat —
die aus diesen Formfehlern resultirenden Unterschiede eliminiren, wenn nur diese Ab-
weichungen nicht gerade durch fast discontinuirliche Fehler in den Schneidenformen be-
dingt sind. Diese Elimination wird aber nicht mehr stattfinden, sobald Resultate ver-
schiedener Amplituden in beiden Lagen verbunden werden; es ist deshalb von Vortheil,
die Pendelbeobachtungen so anzuordnen, dass dieselben stets bei einer und derselben
Amplitude beginnen und ebenso mit einer bestimmten Amplitude schliessen, weil man
dadurch zu Resultaten. gelangt, die von der Schneidenform fast völlig unabhängig sind,
und eventuell auch der oben bemerkte Einfluss der Luft eliminirt wird.

|

©

|
3
|

 

 
