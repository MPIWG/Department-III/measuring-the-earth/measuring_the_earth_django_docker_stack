 

y

[ul

il]
ar li
Int
ul if

Annex Via.

Duett)

ÜBER
DIE BESTIMMUNG DER SCHWERE MIT HILFE VERSCHIEDENER APPARATE.

Im Auftrage
der am 15. September 1882 im Haag versammelten Commission der Europäischen Grad-
messung erstattet zu Rom am 22. Oktober 1883

von

THEODOR VON ÖPPOLZER.

Vorbemerkung.

In der am 15. September 1882 im Haag abgehaltenen Sitzung wurde dem Be-
richterstatter der Auftrag zu Theil, der im Jahre 1883 in Rom stattfindenden Allgemeinen
Conferenz über die Pendelfrage, deren Einführung in unser Beobachtungsprogramm ein
Verdienst Plantamour’s bildet, zu berichten. Um diesem Auftrage nach Möglichkeit ge-
recht zu werden, schien es angemessen, mittelst Circulare von allen Mitgliedern der
Gradmessung Beiträge und Mittheilungen zu erbitten. Was nun über diese Anregung
dem Berichterstatter an werthvollen Mittheilungen zugekommen ist, erlaubt sich derselbe,
ohne für den Inhalt eine Verantwortung zu übernehmen, im Anhange zu diesem Berichte
zu veröffentlichen. Er hat bei Abfassung seines Berichtes sich möglichst objectiv zu ver-
halten gesucht, doch liest es in der Natur der Sache, dass seine subjectiven Anschauungen
über die vorgelegte Frage mehr oder minder hervortreten; nichtsdestoweniger hofft er
von dem Vorwurfe der Parteilichkeit frei zu bleiben.

Bericht.

Die Bestimmung der Schwere kann eine absolute oder relative sein. In der
Regel stösst die absolute Bestimmung einer Quantität auf &rössere Schwierigkeiten, als
Annex VIa. 1

 
