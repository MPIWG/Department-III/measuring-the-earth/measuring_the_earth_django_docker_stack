 

port,
day

dis

ents
Dport
Nele

EERO TILL IIT OD

oes

 

LRN EL ST I TT er ee

 

M. le Président met aux voix les 7 résolutions proposées a la fin du rapport de
M. Hirsch, et la proposition ajoutée par M. Perrier; elles sont adoptées soit à l’unanimité,
soit à une grande majorité.

Ensuite M. le Président donne la parole à M. Zbañez pour donner lecture de son
rapport sur les maréographes, selon le point Ill. 5 du programme. Ce rapport
sera publié comme tous les autres parmi les annexes du rapport général. (Voir Annexe
No. V.) Apres la lecture du rapport, M. le Président ouvre la discussion sur cet objet.

M. Betocchi dit que les maréographes qui fonctionnent en Italie, ne sont pas au
nombre de 7, comme il est dit dans le rapport, mais au nombre de 15; cependant on a
relevé seulement les courbes de 7 de ces maréographes.

M. Ibanez déclare vouloir tenir compte de cette rectification dans son rapport.

M. Betocchi ajoute en outre que d’autres maréographes sont en construction; par
exemple deux construits par la municipalité de Puzzuoli, dont un en pleine mer et
l’autre, comme fait observer M. Rossi, près du temple de Sérapis. M. Betocchi n’est
pas d'avis, comme le propose M. Marx dans sa notice, d'apporter aux observations maréo-
graphiques des corrections dûes à l’état de baromètre, à la quantité de sel contenue dans
la mer etc., etc.; il opine qu’on ne devrait pas tenir compte de ces corrections, car il
ne s’agit pas de déterminer un niveau idéal de la mer, mais de fixer le niveau réel.
Quant au désir de M. Marx d’exclure les observations faites par les maréographes placés
à l’intérieur des ports, il dit qu'il faudrait alors mettre de côté presque tous les résultats
obtenus jusqu'à présent; par exemple en Italie le relevé de 14000 courbes serait réduit
& ZETO.

M. Hirsch accepte parfaitement le voeu de M. Ibanez que les nouveaux maréo-
graphes qu’on va installer dans l’avenir, devraient être placés autant que possible en
dehors des ports, bien qu’il estime que, quelle que soit l’exposition qu’on choisisse, la con-
figuration de la côte excercera toujours une influence sur le niveau.

M. Betocchi mentionne à cet égard qu’en Italie les maréographes placés dans
les ports sont mis en communication avec la mer libre par des conduits d'assez grande
dimension, disposition sur laquelle il se permet d'attirer l'attention de ses collègues.

M. Faye s'associe aux opinions de M. Betocchi et désire en outre faire la pro-
position que, pour simplifier le travail et pour arriver plus vite à un résultat, on se
décide à exclure du relevé des courbes maréographiques les parties influencées par les
tempêtes et les cyclones, comme on le fait également pour les observations magnétiques
dont on exclut celles qui sont influencées par des perturbations ou orages magnétiques.

M. Betocchi est du même avis d'autant plus que les parties des courbes maréo-
graphiques dessinées sous l'influence des mouvements cycloniques, sont trop difficiles
à relever.

Le même relève le passage du rapport de M. Ibanez dans lequel il se déclare
fortement contre le système d'envisager la moyenne arithmétique entre le maximum et
le minimum comme véritable moyenne; il semblerait d’après cela que cette méthode
incorrecte est encore suivie dans quelques pays. En Italie on ne la jamais adoptée.

 
