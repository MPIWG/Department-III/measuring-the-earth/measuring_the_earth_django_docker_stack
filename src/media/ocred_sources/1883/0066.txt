 

Dritte Sitzung

der siebenten allgemeinen Conferenz.

 

 

| Rom am 18. Oktober 1883.

| Präsident: Ferrero.
| Schriftführer: Hirsch und von Oppolzer.

Beginn der Sitzung um 2% 15” Nachmittags.

Anwesende Commissare: v. Bakhuyzen, Barozzi, Barraquer, Bassot, v. Bauernfeind,
Betocchi, Christie, Clarke, Cuits, Faye, Fearnley, Fergola, Ferrero, Fischer, Förster,
v. Forsch, Hartl, v. Helmholtz, Hennequin, Hirsch, Ibanez, v. Kalmar, Lorenzom, Léwy,
Magnaghi, Mayo, Nell, Oberholtzer, v. Oppolzer, Perrier, Pujaçon, Respighi, Rümker,
Schiaparelli, Schiavoni, Schols, de Stefanis, Villarceau.

Eingeladene: d’ Atri, Barilari, Battaglini, Blaserna, Cantoni, Cerruti, Cremona,
Dini, Galitzine, Garbolino, Govi, Lazagna, di Legge, Millosevich, Pisati, Pucci, Rosalba,
de Rossi, Q. Sella, Stromei, Tacchim, de Vita.

Der Präsident eröffnet die Sitzung mit der folgenden, im Namen der italienischen
Gradmessungs- Commission, welche den Beschluss gefasst hat, dem General Baeyer in

Anerkennung seiner hohen Verdienste eine goldene Ehrenmedaille zu überreichen, ge-
haltenen Ansprache:

 

(Uebersetzung des französischen Orginales.)
Meine Herren!
| Als die italienische Gradmessungs-Commission sich mit der Frage des Empfanges
| so hervorragender Fremder gelegentlich der siebenten allgemeinen Conferenz beschäf-
| tigte, richtete sich ihr erster Gedanke auf den verehrungswürdigen Gründer dieses Unter-
nehmens, da sie sich sehr wohl bewusst war, dass, indem sie den Nestor der Geodäten
ehrte, sie auch gleichzeitig jenen eine Ehrenbezeugung erwies, welche seiner Initiative

| gefolgt waren, gleichwie die Ehrentitel eines Heerführers auf das Heer selbst zu-
| rückstrahlen. >

 

 

 
