 

Triangulation Primordiale Algérienne. (Partie Orientale.)

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

SS SSS
INDICATION Hisar s DIRECTEURS |
: Noe LATITUDE. |LONGITUDE.|  ÉPOQUE. INSTRUMENTS. | REMARQUES.
4 DES POINTS. | | ET OBSERVATEURS. |
BE 23 Sdimm........ 36° 1/24") 22050 24") 1862 |
N DRE NEGRPISS ee 0 36 19 54 | 23 113 | 1862-63 |
Bo  Mnaguer...... 36 Ve IG) 23" 7.56 | 1862-63 |
DO HERO 5... 36 4 24 | 23 16 51 | 1862-63 |
27 | Schouf Aissa Ben |
DR ee 36 17 46 | 23 24 50 | 1863 |
28 | Tababourt . . - .. 36 32 19 | 23 7 21 | 1863 | :
29 | Temesguida .... | 36 34 9 | 23 25 15 | 1863 = =
30 | Schouf Melouk . : | 36 20 15 | 23 46 30 | 1863 = =
31 | Mouleh el Mcid. . | 36 34 54 | 23 52 23 | 1863 2 =
32 | Zouaoui. . eo 19 10) 24. 6. 35. 1863 à 5
33. Sidi DEISS .. ..... 36 35 20 | 24 13 34 | 1863 > =
2 Quaseh ..2......:7. 36 23 40 | 24 27 19 | 1863-65 2 = |
Se) HORS =... SO cd dk 24-229) 1863 = = |
36 | @uebel Mzära de | = © |
D iretel . .,. . | 36 8 10 | 24 41 5 | 1863-65 S © |
Fa ay........ 36 30 24 | 24 46 54 | 1865 = = |
38 | Mahouna . . . ... 30.223257. 3,924 | 1865 |
BF Aouara.: ...... 30.32 55 1.25 14.9. 1865
20 Ouenkel .. >... 36.14,..31.25.16 18. | 1865
A eNMcid. >... .... 2099350, 2543 21 1865 |
227 B0u Abed:....| 3641 O0 25.41 42; 1865 |
43 | R’hourra. . . :. . | 36 35 56 | 26 245 | 1865
44 | Bone (Phare)... | 36 54 31 | 25 26 22 | 1865
Triangulation Primordiale Algérienne. (Partie Occidentale.)
45 | Nador desSoumata | 36° 25' 1”| 20°10'20"| 1864 \ | a ee
46 | Sidi Ali des Che- | | | Da Tome
ODA +. 36 35:.36..20:..1.:58 | 1864 | | |
47 |-Zaccar Gharbi. . . | 36 19 55 | 19 52 41 | 1864 |
48 | Coudiat el Diss... | 36 4 4120 6 8| 1864 > Capitaine Perrier.
49 | Lari Taourirt. . . | 36 25 45 | 19 36 55 | 1864 |
oo Amrouna: ..... . 30 56 29 1 19 40 29 1864
Bene... .. : . 30 ie | 192715 1864
=2 | Ouarsenis...... S59, 2019. tO Sd. an ar
Poe) Saadia........ 35 54 20 | 19 0 23 | 1864-65 | | en
oe Ouled Hoceir ...| 36 911/19 6 1] 1864-65 | |
55 | Djebel Bioïd. . .. | 36 10 37 | 18 40 51 | 1865 |
a Maralou. : -. . _ | 293 59 16 19.9436 1865
De Sidi Said... :. : Pag 8 30.1895 19 | 1865 |
56 | Keloub Tsour. .. | 35 52 44 | 18 7 49] 1865 ) Capitaine Perrier. |
=>, Dar Chovachi... | 36 555 | 18 2 28 || : 1865 | |
60 | Hachem Daroug. . | 35 59 28 | 17 48 24 1865 | |
61 | Sidi Medjahed. . . | 35 41 16 | 17 53 36 1865 | | |
62 | La Macta. ..... 26 41 2 | Er 30 98 5 .186b: «| | |
63 | Sidi Bou Ziri...| 35 3033| 17 3749| 1867 | | Cercle azimutal de |
| | | | Brunner.
me Halar 00: . .. 35 45 29 | 17 9 46) 1867 | Capitaine Bondivenne. | pee de Gambey
| | et . |
65 | Tafaraoni ..... 35 26 24 | 17 940) 1867 |
66 | Tour Combes . .. | 35 38 6 | 16 57 33 1867 . |
| eo, /Fessaln :. . :... |. 85 16 56 | 16 52 14 | 1868 |
BE 9 68| Ketelle....... | 35 2840 | 1636 1] 1868 |
| Fe 69 | Seba Chioukh... 35 9 49 | 16 18 17 | 1868 > Capitaine Perrier. Cercle azimut.n°2. |
Zr küneher......:. 35.546) 16445 | 1868 |
| v1 | Nador de Tlemcen | 34 47 43 | 16 20 58 | 1868 |
| D | Pilhaousson .. . . "BD O0 31 | 15 58 51 | 1868 |
por 73 | Ras Ashfour.... | 34 33 53 | 15 52 35 1866 | |
IF mal ....... 3». 150 15 58.12 | |
| 5
|

   
