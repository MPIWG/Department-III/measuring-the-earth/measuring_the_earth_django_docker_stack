 

|

Für

Für
Für

Für

Kür

Für

Für

Für

IV.

Für

Für

4
Lorenzoni, Direktor der Sternwarte in Padua.
Magnaghi, Linienschiffskapitän, Direktor des hydrographischen Bureaus 10
Genua.
Mayo, Generalmajor, in Cagliari.
Oberholtzer, Civilingenieur in Rom.
Respight, Direktor der Capitolsternwarte in Rom.
Schiavoni, Professor an der Universität in Neapel.
Schiaparelli, Direktor der Brera-Sternwarte in Mailand.
die Niederlande: Prof. van de Sande-Bakhuyzen, Direktor der Sternwarte in
Leiden.
Schols, Professor am Polytechnikum in Delft.
Norwegen: Prof. Fearnley, Direktor der Sternwarte in Christiania.
Oesterreich: Prof. v. Oppolzer, Vorstand des Gradmessungsbureaus in Wien.
Corvettenkapitän v. Kalmdr, Direktor der Triangulirungsabtheilung des kok.
militärgeographischen Institutes in Wien.
Hartl, Major am k. k. militärgeographischen Institute in Wien.
Preussen: v. Helmholtz, Geheimer Regierungsrath, Professor an der Universität
in Berlin.
Prof. Fischer, Sectionschef am geodätischen Institute in Berlin.
Rumänien; Se. Excellenz General Larozzi, Vorstand der Plankammer in Bukarest.
Russland: Se. Excellenz General v. Forsch, Vorstand der topographischen Ab-
theilung des Generalstabes in St. Petersburg.
Spanien: Se. Excellenz General Ibanez, Direktor des geographischen und stati-
stischen Institutes in Madrid. :
Barraquer, Oberst am geographischen und statistischen Institute in Madrid.
die Schweiz: Prof. Hirsch, Direktor der Sternwarte in Neuchätel.

Speeialbevollmächtigte für die siebente allgemeine Üonferenz:

England: Christie, königlicher Astronom für England, Direktor der Sternwarte
in Greenwich.
Oberst Clarke aus London.
die Vereinigten Staaten von Nordamerika: General Cutts, von der Coast
and Geodetic Survey in Washington.

V. Von der permanenten Commission Eingeladene:

Prof. Förster, Direktor der Sternwarte in Berlin.
Loewy, Mitglied der französischen Akademie, Redacteur der Connaissance des temps

in Paris.

Pujacon, Linienschiffskapitän, Direktor der Sternwarte in S. Fernando und Redacteur

des spanischen astronomisch-nautischen Almanachs.

 

|
|

een

u laa

e
‘
a

 
