 

8. Im Westen der Monarchie.
4. Doppelmessung der Linien:

a. Innsbruck—Landeck—Martinsbruck—Bozen—Franzensfeste, mit Einbe-
ziehung der schweizerischen Nivellements-Marke in Martinsbruck (N. F.240).

b. Zirl--Scharnitz zum Hauptfixpunkte Scharnitz (LXXXII).

c. Bozen—Trient— Landesgrenze mit einer Marke für den Anschluss Italiens
an Marke Bahnwächterhaus No. 280, 400 Meter diesseits der Grenze.

d. Tarvis—Pontafel—Landesgrenze zum erneuerten Anschlusse für Italien,
mit einer Marke am Bahnhofgebäude in Pontafel, 400 Meter diesseits
der Grenze.

An diesen Linien ist der Pfeiler der astronomischen Station Sieg-

mundskron bei Bozen (erbaut 1882) in das Nivellement einbezogen
worden.

y. Im Occupations-Gebiete. |
5. Doppelmessung der Linie: |
Basis bei Ilidze—Sarajevo—Zenica—Maglaj— Velika.
6. Einfache Messung:
Velika—Brod.
Am Schlusse des Sommers 1883 sind mithin in Oesterreich-Ungarn 12300 km
| theils doppelt, theils einfach nivellirt und 2190 gewöhnliche Höhenmarken als Fixpunkte
| 1. Ordnung hergestellt.

Durch die vorstehend eitirten während der Arbeitscampagne im abgelaufenen
Jahre ausgeführten Nivellements sind die Anschlüsse an die Nivellements der angrenzenden
Staaten, die in den früheren Jahresberichten schon erwähnt sind, um vier vermehrt, von
welchen jene

1. an das bayerische Präcisions-Nivellement in Scharnitz, Höhenmarke an der
Pfarrkirche (bayerischer Hauptfixpunkt LXXXII), ferner
an das schweizer Nivellement in Martinsbruck zu der schweizer Höhen-
| marke N. F. 240 und
i 3 an das italienische Nivellement südlich von Ala zur Höhenmarke am Bahn-
| wächterhause No. 280, so wie zu dem 0.4km entfernten Hektometersteine
| 307.8 unmittelbar an der italienisch- tirolischen Grenze,
| als fertie gestellt bezeichnet werden können, während noch ein Anschluss
4. für das italienische Nivellement seitens Oesterreich-Ungarns vorbereitet
ist u. z. jener in Pontafel zur Höhenmarke am Bahnhofe und zu der 0.4 km
entfernten Steinmarke auf dem östlichen Landpfeiler der Eisenbahn-Grenz-
brücke über den Pontebba-Bach.

Mit bayerischen Nivellementslinien ergeben sich nın an den sieben Anschluss-
punkten mit den zu diesen geführten österreichischen Nivellements fünf Anschlusspolygone,
von. welchen vier bereits im vorjährigen Generalberichte besprochen erscheinen, während

34*

vo

23 PEER NII DR AD a

 
