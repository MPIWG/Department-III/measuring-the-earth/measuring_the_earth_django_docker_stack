 

|
|
|
|

184

Quel sera donc ce méridien initial?

Contrairement aux conclusions du Rapporteur, M. Perrier ne pense pas qu’il soit
nécessaire de choisir le méridien d’un des grands observatoires existants. Il importe
seulement que l’origine des coordonnées en longitude soit rapportée à l’un de ces obser-
vatoires, relié lui-même à tous les autres, et, si l’on tient compte de la condition, souvent
exprimée comme un desideratum par les géographes, que ce méridien initial ne doit
couper aucun des grands pays de l’Europe, et doit traverser le moins possible de terres
habitées sur le versant opposé du globe, on est tout naturellement conduit à proposer
comme méridien international un méridien voisin du méridien historique de Vile de Fer.
M. Perrier placerait, par définition, le méridien initial à 18 degrés, un cinquième de quadrant,
à l’ouest du méridien de Greenwich, qui est, parmi les observatoires dont la position est
bien déterminée, le plus occidental des grands observatoires de l’ancien monde. — L'heure
internationale serait celle de ce méridien international et on la déduirait aisément de
l’heure locale, en chaque lieu dont la longitude serait bien déterminée.

M. Perrier se hâte de dire qu’il exprime ici une opinion toute personnelle.

Il ne se dissimule pas, du reste, que le méridien de Greenwich obtiendra dans
la Conférence une majorité considérable. C’est là un fait dont il faut bien tenir compte.
Peut-être même obtiendrait-il l’unanimité dans la Conférence diplomatique, appelée à
prendre plus tard une décision définitive, si cette unanimité même devait entrainer,
à courte échéance, le triomphe complet du système métrique décimal des poids et mesures.

M. Perrier fait, à ce sujet, appel à ceux des collègues qui font partie du Comité
international des poids et mesures, et auxquels il appartient de provoquer ce triomphe
définitif. Toutes les nations civilisées des deux mondes ont adopté le système métrique,
à l'exclusion de tout autre système, sauf la Grande Bretagne; nous avons parmi nous deux
délégués de ce grand pays. Que la Conférence toute entière leur demande, et sa voix
sera entendue, de tenter un grand effort auprès de leur gouvernement pour que l’ex-
tension du système métrique en Angleterre soit, dès aujourd’hui, favorisée par tous les
moyens possibles et devienne bientôt l’adoption absolue et légale.

Mais ce n’est pas tout; une lacune considérable reste encore à combler, même
chez les nations qui ont déjà adopté le système métrique. La division décimale n’est
encore appliquée nullepart à la mesure du temps, et ne l’est qu’en France aux mesures
angulaires.

C’est pourquoi il propose avec son collègue M. Yvon Villarceau, une mesure

x

qu’on trouver peut-être un peu radicale et qui consiste à adopter desormais:

comme unité de temps, le jour et
comme unité d’angles, la circonférence ;

le jour et la circonférence ne comportant plus que des subdivisions décimales.

M. Perrier laisse à M. Yvon Villarceau le soin de développer les avantages in-
contestables de cette proposition qui est comme le corollaire naturel indispensable des
discussions relatives à l’unification des longitudes et des heures, et qui, si elle était

|
|
|
=

 

11
i
ty

PRE AE IIE

EEE Le

 
