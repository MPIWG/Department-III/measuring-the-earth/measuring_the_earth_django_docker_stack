 

UM,
EN hi

la, N

Nata
vum
kannt
Wa Ton
s 185
t,

en Vu
sake
or Var
ak
ier Ter
| ka.
In
oy Bit
ore

 

È
|
E
F
5
5

PTT

EC TNT LON Eo PNT TIE

ET

 

|
|
F
|

i ;
F
É
F
t
Ë
F
|
i
:
FE

21

macht sich durch die Wahl der Massenvertheilung und die Anordnung der Versuche von
der Bestimmung der Trägheitsmomente und dem Luftwiderstande unabhängig; auch kann
nach seiner Ansicht durch entsprechende Variation der Massenvertheilung mit dem-
selben Pendel der Einfluss des Mitschwingens des Stativs bestimmt werden. Es ist
schwierig, ohne mit einem solchen Apparate gearbeitet zu haben, ein Urtheil über die
erreichbare Genauigkeit zu bilden; jedenfalls dürfte die mechanische Ausführung auf
grosse Schwierigkeiten stossen und die Beobachtungsweise, wenn anders man alle con-
stanten Fehlerquellen vermeiden will, sich recht complicirt gestalten. Auch dieser
Apparat scheint dem Berichterstatter weniger als das Bessel’sche Reversionspendel zur
genauen absoluten Schwerebestimmung geeignet.

Auf die in der gegenwärtigen Versammlung zur Vertheilung gelangte Abhandlung
über die Länge des Secundenpendels von Pisati und Pucci konnte der Berichterstatter
in Folge der Kürze der Zeit nicht näher eingehen; nur soviel glaubt er hervorheben zu
müssen, dass er den wahrscheinlichen Fehler von O”"”026 im Endresultate für ziemlich
erheblich erachte und dass derselbe auf eine noch nicht hinreichend genaue Ermittlung
der bei diesen Fadenpendelbeobachtungen nothwendigen Reductionen schliessen lasse.
Die schwierige Elimination der inneren Reibung der Luft dürfte die Hauptquelle dieser
beträchtlichen Unsicherheit sein. Uebrigens bezeichnen die Verfasser ihre Versuche als
vorläufige und hoffen durch Wiederholung derselben im luftleeren Raume die letzten
Schwierigkeiten zu überwinden. Zieht man das Endergebniss aus den bisherigen Aus-
einandersetzungen, so kann dieses in der folgenden summarischen Weise formulirt werden:

1. Für absolute Schwerebestimmungen eignet sich in hohem
Maasse das. Dessel’sche Reversionspendel, wenn man zwei
Exemplare desselben von wesentlich verschiedenem Gewicht
auf demselben Stative schwingen lässt.

2. Nicht nur müssen die nämlichen Schneiden an beiden Pendeln
in Verwendung kommen, sondern dieselben müssen auch an
jedem Pendel vertauschbar sein; als Material für dieselben
empfiehlt sich Achat.

3. Die Beobachtungen müssen in Räumen von nahezu constanter
Temperatur angestellt werden; die Benutzung des Vacuums
ist nicht zu empfehlen.

4. Die Schwingungszeiten müssen in beiden Lagen des Pendels
innerhalb derselben Amplitudengrenzen erhalten werden.

Il. Abschnitt.

Die relativen Schwerebestimmungen.

Nach den Eingangs gemachten Bemerkungen sind es gerade die relativen Schwere-
bestimmungen, welche für die Gradmessung eine erhöhte Bedeutung beanspruchen. Die

 
