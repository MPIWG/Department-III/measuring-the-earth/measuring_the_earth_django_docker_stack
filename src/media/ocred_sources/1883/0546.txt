 

Annexe VI.

 

I AB L

SUR
LA DETERMINATION DE LA PESANTEUR A L'AIDE DE DIFFERENTS APPAREILS.
Lu & Rome le 22 Octobre 1883

PAR

M. THEODORE VON OPPOLZER

chargé de ce rapport par la Commission géodésique permanente, réunie a la Haye
le 15 Septembre 1882.*)

Avant-propos.

Dans la séance tenue à la Haye le 15 Septembre 1882, je fus chargé de
préparer pour la Conférence générale de Rome en 1883 le rapport sur la question du
pendule, question dont l’introduction dans le programme de nos observations constitue un
véritable mérite pour M. Plantamour. Pour satisfaire autant que possible a cette tache,
j’ai jugé convenable d’inviter par circulaire les membres de la commission géodésique in-
ternationale à me faire parvenir des notices sur cette question. Toutes les communi-
cations importantes, qui m'ont été adressées à la suite de cette invitation, se trouvent
publiées dans l’annexe de ce rapport sans responsabilité de ma part pour leur contenu.
Quoique la nature de la question m'ait porté à faire prévaloir plus on moins mes idées
personelles, j’ai pourtant essayé d’être aussi désintéressé que possible, et j'espère qu’on
ne pourra pas m’accuser de partialité.

Rapport.
La détermination de la pesanteur peut être absolue ou relative. La détermina-

tion absolue d’une quantité rencontre ordinairement plus de difficultés, qu’une détermina-
tion relative, surtout si les différences à déterminer par cette dernière méthode ne repré-

*) La traduction a été fournie par les soins de l’auteur.

|
?

 

 
