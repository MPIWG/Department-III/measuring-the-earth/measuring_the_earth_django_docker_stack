m en nn —

 

50

ment lui-même. Rien n’empéche une construction beaucoup plus solide du support et en
conséquence une augmentation de sa stabilité; mais il n’en résultera pas un avantage
appréciable, car C. S. Peirce (Comptes Rendus de la sixieme conférence générale 1880,
pag. 86) a judicieusement remarqué que, même dans les constructions les plus solides, il
sera très difficile de pouvoir toujours éviter un petit mouvement, dans le support. En
tout cas on ne peut pas considérer la stabilité du support pendant l'oscillation du pendule
comme existant à priori, à moins que des expériences antérieures n'aient confirmé cette
supposition. Bessel, Kater et d’autres se sont en effet convaincus de la stabilité de leurs
supports par l'emploi du ,,pendule noddy“ de Hardy; C. S. Peirce (Methods and Results
of pendulum experiments, Washington 1882, pag. 69) a donné une théorie mathématique
de cet appareil. A cette occasion il convient de citer l'emploi d’un pendule de contrôle
très original, dont usage est peu connu et que Lamont à proposé pour les expériences
de Munich par M. le Colonel von Orff. On fixe près du support des couteaux un pendule
à fil dont la durée d’oscillation coincide, autant que possible, avec celle du pendule à
réversion et on le protége des influences extérieures par un cylindre en verre. Si le
support a un mouvement dépendant du pendule en oscillation, ces impulsions sur le
pendule a fil s’additionneront 4 cause de la durée presque égale des oscillations, et le
pendule à fil montrera après quelque temps des oscillations assez appréciables, qui selon
la durée des oscillations des deux pendules, augmenteront ou disparaitront après des
périodes plus ou moins grandes. M. le Colonel von Orff dans le mémoire présenté dans
la session actuelle (Bestimmung der Länge des einfachen Sekundenpendels auf der Stern-
warte zu Bogenhausen aus den Abhandlungen der K. bayerischen Akademie der Wissen-
schaften II. CL, XIV. Band, III. Abth.) a montré d’une manière élégante, comment on peut
apprécier sûrement l'intensité de l’oscillation du support par l'observation du pendule à fil;
mais en dehors de ces importantes remarques théoriques de M. von Orff, cet appareil mérite
qu'on le recommande à l’attention comme un appareil de contrôle, facile à construire, pouvant
démontrer l'existence ou l’absence d’un mouvement oscillatoire d’un support très-solide.

C. S. Peirce fut le premier qui, par des expériences auxquelles on a donné plus
tard l’épithète de statiques, à pratiquement démontré cette source d'erreur très importante
du pendule à réversion de Repsold; le premier aussi qui à entrepris sur ce point des
recherches théoriques en déterminant sa valeur numérique avec une grande approximation.
Le mérite de Peirce sous ce rapport est très grand; car lui et M. le général Baeyer étaient
presque les seuls à soutenir l’idée de la flexibilité du support, avant la publication de ses
expériences concluantes, et ils recontraient une vive opposition à laquelle le rapporteur
s'était également joint dans le temps.

M. Plantamour à fait remarquer (Recherches expérimentales sur le mouvement
simultané d’un pendule et des supports, 1878), qu'il y a une différence entre les mesures
statiques et dynamiques, et il a donné une grande perfection aux expériences dynamiques.
Peirce au contraire, d’après ses recherches, n’attribue (Comptes Rendus de la Commission
permanente 1878, pag. 118) qu'une faible valeur à cette différence et la nie pour ainsi
dire en principe. Toutefois il se voit obligé dans la publication susmentionnée (Methods

 

 
