 

 

 

 

 

 

 

34

und auf die geologische Unveränderlichkeit seiner Lage darbietet. Unvorsichtig würde
es jedenfalls sein, wollte man den Ausgangs-Meridian in eine vulkanische, oder den
Sekular-Bewegungen des Bodens im hohen Gräde ausgesetzte Gegend verlegen. Und
da genau genommen nichts unveränderlich ist und die Erfahrung uns mehr und mehr
lehrt, dass fast überall in mehr oder weniger langen Perioden schwache Bodenbewegungen
stattfinden, so ist es geboten, den Ausgangspunkt der geographischen Längen durch
astronomische Beobachtungen direkt mit anderen naheliegenden Observatorien zu ver-
binden und ihn ausserdem an ein Dreiecksnetz erster Ordnung des betreffenden Landes
anzuschliessen.

Dies wären, mit wenigen Worten angedeutet, alle Anforderungen, welche die
Wissenschaft an die Wahl des Ausgangs-Meridians zu stellen hat. Jedenfalls aber sind
sie genügend, um Punkte wie die Insel Ferro von der Wahl auszuschliessen, welche im
siebzehnten Jahrhundert von der durch Richeliew’s Initiative damals zusammenberufenen,
ersten internationalen wissenschaftlichen Commission vorgeschlagen wurde, und deren
Lage, im Sinne der Länge, während eines ganzen Jahrhunderts um mehr als 25 Mi-
nuten geschwankt hat, bis man dieselbe auf den Vorschlag von Delisle willkürlich
als 20 Grad westlich vom Pariser Meridian gelegen annahm, eine Bestimmung, durch
welche der Ausgangs-Meridian von Ferro nur mehr dem Scheine nach fortbestand, in
Wirklichkeit aber durch den Pariser Meridian ersetzt wurde.

Aus den gleichen Gründen kann weder von dem Pic von Teneriffa, dem Alexander
von Humboldt den Vorzug gab, und noch weniger von der Behringsstrasse die Rede
sein, welche in unserer Zeit mit vieler Beharrlichkeit und wenig Erfolg von Herrn
Beaumont de Boutillier vorgeschlagen worden ist; man müsste denn auf den Einfall
kommen, inmitten dieser Meerenge oder auf jener Bergspitze ein Observatorium herzu-
stellen, lediglich dazu bestimmt, den Ausgaugs-Meridian materiell festzulegen, und dieses
Observatorium ausserdem durch telegraphische Kabel mit den benachbarten Continenten
zu verbinden. Dergleichen kann natürlich nicht ernstlich in Betracht kommen.

Welche Gründe lassen sich überhaupt zu Gunsten der Wahl eines oceanischen
Ausgangs-Meridians anführen? — Unseres Wissens giebt es deren zwei:

Als ersten Grund führt man an, dass es besser sei, um sich der allgemeinen
Zustimmung aller Nationen zu versichern, von allen denjenigen Punkten abzusehen
welche bisher von den grossen Ländern benutzt werden; um die Eigenliebe keines der:
selben zu verletzen, sei es angezeigt, einen sogenannten „neutralen‘ Meridian zu wählen.
Zur Begründung dieser Ansicht hat man an den Convent und dessen akademische
Commission erinnert, welche es wohlweisslich vorgezogen haben, bei der Wahl der Ein-
heit der neuen Maasse und Gewichte alle die vielen damals bestehenden Fusse und Pfunde
bei Seite zu lassen, und welche keinen Anstand genommen haben, den damals gebräuch-
lichen französischen Fuss und das Pfund zu opfern, um anstatt dessen den Meter und
das Kilogramm vorzuschlagen; dadurch hätten sie wesentlich zu der immer allgemeiner
werdenden Annahme des neuen Maass- und Gewichts-Systems beigetragen.

Ohne die theilweise Richtigkeit dieser Behauptung zu verkennen, scheint uns

 

 
