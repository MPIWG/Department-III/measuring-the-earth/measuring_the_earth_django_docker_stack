sd

ELL LN IR LN TL I

EN NRA ENT

era en

PRE I TIA TE EAT

EEE TE
” 5
CF

NE PRIE

REPAS TC CARPE EP SAN EE OT
3 ae Fa Re PS

RENE TEE TT
: ‘

[

|
4
8
|
E

ms

 

Annexe IT.

Rapport sur les triangulations

avec un canevas général à l’échelle de 1 : 10,000,000.

À l'occasion de la Conférence géodésique internationale qui eut lieu à Mu-
nich en 1880, j'ai eu l'honneur de présenter un rapport sur l'avancement des
trangulations en Europe.

Ue rapport consistait essentiellement en une série de tableaux par nations,
contenant l'indication des points trigonométriques, leur position géographique ar-
rondie à une seconde d'arc, la date de l’exécution des stations. les instruments
employés et les noms des opérateurs ; plus, dans une colonne de remarques,
l'indication des publications ayant rapport à ces stations trigonométriques.

Pour l'intelligence de ces tableaux et pour pouvoir embrasser d’un coup
d'œil toutes les triangulations, j'ai joint au rapport un canevas général de
toutes les triangulations européennes.

Les éléments pour la formation des tableaux et du canevas m'ont été four-

“ms par les Commissions des différents Etats de notre Association, ou tirés des

publications, pour les Etats, qui, comme la Grande ae ne faisaient ee
partie de l’Association géodésique.

Le travail que j'ai l'honneur de vous présenter n’est que le résultat de la
mise au courant de celui que j'ai présenté en 1880.

Je vous le présente non comme définitif, mais comme épreuve à corriger.

Ce ne sera qu'après une révision faite par les commissaires de chaque pays,
que le travail sera publié dans le Compte-rendu de la Conférence, par les soins
du Bureau central.

Ce qui manque à mon rapport est un aperçu historique sur les triangulations
exécutées dans chaque pays, car les données enregistrées dans les tableaux ne
sont pas suflisantes et sont difficiles à résumer.

C'est dans le but de remplir cette lacune que je prends la liberté de prier
les Commissions géodésiques de chaque État de bien vouloir faire précéder le
registre de leurs stations trigonométriques d’une espèce de préface historique,
se rapportant aux travaux trigonométriques respectifs et aux circonstances de

1

 

 
