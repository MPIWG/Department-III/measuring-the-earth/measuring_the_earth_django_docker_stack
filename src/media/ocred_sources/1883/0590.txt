 

 

18

 

®
au = (1-8) @ı +2=00 9 — 9°) dp
0

in dieser Form

 

 

 

Do, — D?
um Ag
Po
oder 12,
4 Op, — D?
2 1 u =
wa
Für die Refraction in B ergiebt sich
er EIN aD me PP
es
oder ee ee
3 02 —4 Og,
Cp = + 6 dr rare

In den Gleichungen 11 und 12 ist æ,, wie schon schon früher bemerkt wurde,
immer in der Richtung von A nach B zu rechnen, mithin negativ, wenn die in A ge-
messene Zenithdistanz von B kleiner als 90° ist.

Innerhalb der Grenzen 9, = 0 und 9, = ® findet man

(Gleich. 11) (Gleich. 12)
für po = 0 04 = La — $ À De à
» Po — + 04 = Oa 04 — Oa
~ Po = 42 04 — unbestimmt O64 = best à
» Po = +? D 04 — Qa —$ À 04 = Oa + aoa
” ~, =O 04 = QC — & À OA ar 4 Sa

Bemerkenswerth ist der Fall 9, = + ®, wo e4= .0.. Nur scheinbar findet ein

Unterschied statt zwischen den Ergebnissen der Gleich. 11 und 12, den Fal9g,=3®
ausgenommen.

 

D.
Berechnung des Refractionsfactors hk.

Die Gleichung 8 beruht, wie man unmittelbar erkennt, auf der Annahme, dass
innerhalb der von dem Lichtstrahl passirten Luftschicht % als eine lineare Function
von der Höhe betrachtet werden kann. Zur Ermittelung der Refraction in A oder in
B genügt es dann im Allgemeinen, den Werth von % in genannten beiden Punkten zu
kennen. Zu dieser Kenntniss von % gehört aber mehr als die Messung des Luftdrucks
und der Lufttemperatur am Beobachtungsorte; es erfordert dieselbe vielmehr auch, dass
man die einer Höhenänderung örtlich entsprechende Aenderung der Temperatur er-
mittelt hat.

 

 

 

 
