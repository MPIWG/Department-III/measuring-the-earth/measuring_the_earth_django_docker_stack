 

 

 

 

 

 

 

 

 

für das durch den Anschluss bei Scharnitz neu formirte Polygon die Berechnungen zur
Zeit noch nicht beendet sein dürften.

Für die oben erwähnte schweizer Höhenmarke N. F. 240 in Martins-
bruck resultirt nach den bereits fertigen Berechnungen, abgeleitet auf der
kürzesten Linie von der Höhenmarke im Häuschen des selbstregistrirenden
Fluthmessers in Triest u. z. über Adelsberg, Laibach, Tarvis, Villach, Lienz,
Franzensfeste, Innsbruck, Landeck als Seehöhe orläulesii. ee. 1080.79 %
über dem Mittelwasser der Adria bei Triest.

In gleicher Weise abgeleitet u. z. über Adelsberg, Laibach, Tarvis
beziehungsweise über Adelsberg, Laibach, Tarvis, Villach, Lienz, Franzens-
feste, Bozen etc., sind die Seehöhen der beiden Anschlusspunkte an das
italienische Präeisions-Nivellement d. i. für die Höhenmarke am Bahnhofe in
ee en
und für die Höhenmarke am Bahnwächterhause No. 280 südlich von Ala mit .569.07 m
vorläufig erhalten worden.

Die Nivellements nach drei der hier angefithrten vier Anschlusspunkte, ferner
nach dem Anschlusspunkte Höhenmarke Kufstein Bahnhof (815) *), zweigen von Nivelle-
mentslinien ab, die als „Gebirgs-Nivellements“ bezeichnet werden müssen, nachdem in
ihrem Verlaufe bedeutende Höhen überschritten werden.

Während die schweizer Höhenmarke N. F. 240 einem solchen sogenannten
.Alpenpolygone“ direct angehört, bilden die übrigen hier in Betracht kommenden Nivel-
lementslinien in ihrem Zusammenhange noch 3 weitere derartige Polygone.

Diese sind: (Siehe beiliegende Skizze.)

1. Franzensfeste—Bozen—Martinsbruck (N. F. 240)—Landeck—Innsbruck—Fran-

zensteste.
9. Franzensfeste—Innsbruck— Wörgl—Bischofshofen—Radstadt— Spital a. d. Drau
— Lienz— Franzensieste.

3. Tarvis— Villach—Klagenfurt— Marburg —Pragerhof—Cili—Laibach—Tarvis.

4, Villach—Spital—Radstadt— Neuhaus—Lietzen—Leoben— Bruck a. d. Mur —
Graz— Marburg— Klagenfurt— Villach.

In diesen Polygonen werden die Alpenpässe des Reschenscheideck in 1497 m
Seehöhe und des Brenner in 1372m, so wie auch die Tauern in 1734 m*”) Meereshöhe
zwischen Schaidberg und Obertauern überschritten; ferner wird, beim Uebergange aus
dem Liesing- ins Paltenthal 849 m, beim Uebergang über das Toblacherfeld 1212 m, bei
Hochfilzen an der salzburg-tiroler Grenze 970 m und bei Eben zwischen Bischofshofen
und Radstadt 866 m, dann im Polygone 3 auf der Linie Laibach—Tarvis— Villach bei
Ratschach 869 m absolute Höhe erreicht.

*) Nummer des bayerischen Fixpunkt-Verzeichnisses.

**) Bei der des öfteren als besonders schwierig bezeichneten Linie der schweizer Präcisions-Nivel-
lements zwischen Chiavenna und St. Moriz wurden 1540 m über Repere de la Pierce de Niton, also 1913 m
Meereshöhe erreicht.

|

 

 

 
