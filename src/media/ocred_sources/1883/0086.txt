  

 

 

   

72

der dritten endlich prüfte man die vorgeschlagenen Resolutionen eine nach der anderen
und schritt zur Abstimmung über jede derselben.

Faye eröffnete die Discussion, indem er auseinandersetzte, dass von seinem per-
sönlichen Gesichtspunkte aus sich die vorgeschlagene Reform hauptsächlich, wenn nicht
ausschliesslich, durch das practische und unläugbare Bedürfniss einer allgemeinen und
der ganzen Welt gemeinsamen Zeitzählung empfehle.

Der für diese Weltzeit zu wählende Ausgangspunkt scheint ihm ziemlich gleich-
gültig. Vom Standpunkte der Astronomie dagegen ist er keineswegs von der Noth-
wendigkeit, ja nicht einmal von der Nützlichkeit einer Veränderung der gegenwärtigen
Sachlage überzeugt. Die Verschiedenheit der Ephemeriden, weit entfernt ein Uebel zu
sein, scheint ihm im Gegentheil sehr wünschenswerth, da diese Publicationen den Eifer
für die Astronomie unterhalten; er bedauert sogar das Verschwinden einer gewissen
Zahl älterer Ephemeriden, wie derjenigen von Coimbra, von Mailand, Wien u. s. w. und
würde es nicht gern sehen, wenn der Nautical-Almanac der einzige und ausschliessliche
Regulator der Astronomie auf der Welt würde, wie dies die Consequenz der allgemeinen
Annahme des Greenwicher Meridians wäre. Speciell die Connaissance des Temps, welche
gegenwärtig ihren 210. Band publieirt, hat eine Vergangenheit, welche geachtet zu
werden verdient, und die französischen Astronomen würden sich nicht leicht dazu ver-
stehen, einen grossen Theil der astronomischen Tafeln zu diesem Zwecke umzurechnen.

Es scheint ihm noch schwieriger, die französische Marine, welche seit Jahr-
hunderten gewöhnt ist sich nach dem Meridiane von Paris zu richten, dahin zu bringen, alle
ihre Gewohnheiten zu ändern und das gesammte Material der hydrographischen Karten
und der Piloten neu zu bearbeiten.

Es ist übrigens möglich, dass die französische Regierung sich der vorgeschlagenen
Reform zugänglicher zeigen würde, wenn es gelingen sollte, ihr zu beweisen, dass die-
selbe vom Standpunkte der allgemeinen Civilisation vortheilhaft wäre.

Förster meint im Gegentheil, dass vom specifisch astronomischen Standpunkte
aus die Vereinheitlichung des Meridians und die wichtigen vom Bureau der permanenten
Commisssion vorgeschlagenen Resolutionen von grossem wissenschaftlichen Nutzen seien und
dazu beitragen werden, einen bedeutenden Theil der astronomischen Rechnungen wesentlich
abzukürzen, besonders wenn derselbe Geist des Maasshaltens, welcher bei den Vorschlägen
des Berichtes in Bezug auf Vereinheitlichung der Stundenzählung bestimmend war, uns
davor bewahrt, die Vereinheitlichung der Epochen für die astronomischen Rech-
nungen über die durch die Natur der Sache gegebenen Grenzen hinaus zu treiben. Aber
er erkennt an, dass der Grund, welcher gegenwärtig am meisten zu Gunsten der vorge-
schlagenen Vereinheitlichungsmaassregel spricht, die dringende Nothwendigkeit der
Einführung einer gleichförmigen Zeitzählung im internen Dienst der grossen Öffentlichen
Verkehrsanstalten ist, in denen die Verschiedenheit der Stunden mehr und mehr die
Ursache zahlreicher Conflicte, unnützer Ausgaben und zuweilen wirklicher Gefahren zu
werden beginnt. Die Wissenschaft ist es, welche der Einführung dieser Gleichförmigkeit
-in der pracisen Zeitzihlung vorstehen soll, damit nicht ihre eigenen Interessen bei

 

 
