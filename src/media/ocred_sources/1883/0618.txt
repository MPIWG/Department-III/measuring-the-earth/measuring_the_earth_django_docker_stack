 

 

18

14. Oesterreich.

A. Nachträge zur ersten Mittheilung der Literatur d. p. u. th. G. A.
(G. B. 1880.)
Klein, F. Die wissenschaftlichen Instrumente auf d. niederöster. Gewerbeausstellung 1880.
(Centralzeitung f. Optik u. Mech., 1. Jahrg., 1880, p. 186.)
Lippich und Tinter. Offieieller Bericht über die mathematischen und physikalischen
Instrumente der Weltausstellung in Wien 1873.
Militair-geographisches Institut in Wien. Präcisions-Nivellement in und um Wien.
Ausgeführt in den Jahren 1876—77 von der Triangulirungs-Calcul-A bthei-
lung. (Zeitschr. des öster. Ing.- und Arch.-Vereins, VI. und VII. Heft, 1878.)
Stampfer, S. Theoretisch-praktische Anleitung zum Nivelliren. Achte Ausgabe besorgt
von Herr. Wien, 1877.
Sterneck, R. von. Ueber besondere Eigenschaften einiger astronomischer Instrumente.
(Sitzungsb. d. k.k. Akad. d. Wiss., Bd. UXXVIL Abthl 1. wen
Tinter, W. R. Die Europäische Gradmessung in ihrer Beziehung zu den früheren Grad-
messungsarbeiten. Wien, 1870.
— Astronomische Instrumente. Wien, 1874.
— Geodätische Instrumente. Wien, 1874.
Zrzavy, F. Einfache Formel zur Berechnung der Meridianconvergenz aus rechtwinkligen
sphärischen Coordinaten mittelst einer Hilfstafel. (Sitzungsb. d. Kgl. Böhm.
Gesellsch. d. Wissenschaften. Prag, 1877.)

B. Publicationen aus den Jahren 1881—83.
Czuber, E. Zur Theorie der Fehlerellipse. Wien, 1031.
Finger, J. Ueber ein Analogon des Kater’schen Pendels und dessen Anwendung zu
Gravitationsmessungen. Wien, 1881.
Hartl, H. Ueber den Zusammenhang der terrestrischen Strahlenbrechung und der me-
teorologischen Elemente. (Zeitschr. d. österr. Gesellsch. für Meteorologie,
Bd KVI, Wien, 1881.
Klein, F. Zweck und Aufgabe der Europäischen Gradmessung. (Monatsblätter des
wissensch. Clubs in Wien, Jahrg. III, No. 8. Wien, 1882.)
— Die Figur der Erde. (Mittheilungen der k. k. geogr. Gesellsch. in Wien,
Bd. XXVI. Wien, 1883.
Konkoly, N. von. Praktische Anleitung zur Anstellung astronomischer Beobachtungen
mit besonderer Berücksichtigung auf die Astrophysik. Braunschweig, 1883.
Militair-geographisches Institut. Mittheilungen desk. k. milit.-geogr. Instituts, Band I,
Wien, 1881; Bd. I, Wien, 1882; Bd. III, Wien, 1883.
Jeder Band enthält in seinem officiellen Theile den Bericht der astr.-
geod. Abtheilung über die im abgelaufenen Jahre ausgeführten Arbeiten
(darunter auch die Gradmessungsarbeiten), ausserdem sind im nichtofficiellen
Theile folgende, auf die Gradmessung Bezug habende Aufsätze enthalten:

 

 

 
