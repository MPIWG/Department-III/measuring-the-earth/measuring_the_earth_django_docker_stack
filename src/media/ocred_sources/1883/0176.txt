 

 

 

La medaille que la Commission italienne, avec l'approbation du Gouver-
nement de Sa Majesté, a fait exécuter, rappelle elle-même le titre principal
de l'illustre savant, c’est à dire la création de l'Association dont nous faisons
partie.

Lorsque l’on pense que, si aujourd’hui des savants de toutes les parties du
monde se trouvent réunis dans un but commun, avec le plus parfait accord
et avec la meilleure camaraderie scientifique, nous le devons à l’heureuse
initiative et à l’énergique volonté du général Baeyer, l’on doit trouver bien
naturel que des étrangers à la patrie de lillustre général, lui donnent un
témoignage de leur reconnaissance et montrent par là que le vénérable vieillard
est une de ces personnalités qui ont pour patrie le monde entier. J’ai donc
l'honneur de présenter à la Conférence la médaille destinée au général Baeyer,
avec la certitude de trouver dans le coeur de chacun de vous l'écho des
sentiments qui ont inspiré les Italiens.

M. le Ga Ibanez s'associe, au nom de tous les délégués des autres pays, à la
démarche de la commission Italienne, en s'exprimant ainsi:

Messieurs, Après l’éloquent discours de notre honorable Président,
vous n’attendez pas de ma part une tentative d'ajouter quelque chose au
témoignage des sentiments d’admiration et de reconnaissance, si bien mérités
par notre vénérable fondateur, et si parfaitement exprimés par le Président de
la Commission Italienne.

Mais j'ai la certitude d’être l'interprète de vous tous, en exprimant
l'enthousiasme unanime avec lequel tous les membres étrangers s'associent à
la noble et solennelle démonstration, dont l’illustre maître de la géodésie
vient d’étre l’objet de la part des géodésiens d’Italie.

Je demande à l’Assemblée de se lever toute entière, pour témoigner de
ces sentiments, et je propose de lever ensuite la séance.

L'assemblée s'associe, en se levant, à cette démonstration, et M. le Président
remercie M. le G*! Ibanez de l’approbation chaleureuse qu'il vient de donner, au nom de
l’Assemblée, à la demarche de la commission Italienne; il remet la médaille entre les
mains de Monsieur le Professeur v. Helmholtz, en s’exprimant ainsi:

-Je remercie Son Excellence le Général Zbañez des belles paroles qu'il
vient de prononcer; à présent il me reste a prier M. v. Helmholtz de bien
vouloir se charger de remettre la médaille au Général Baeyer. Transmis par
les mains d’un savant aussi illustre, notre médaille doublera de valeur.“

Le bureau de la Conférence propose et l’Assemblée décide de donner connaissance
au G® Baeyer de l’acte qui vient de s’accomplir, par le télégramme suivant:

„La Conférence géodésique internationale s’est associée avec bonheur à
la démonstration par laquelle la Commission géodésique Italienne vient de
témoigner, en séance spéciale, de la vénération générale pour Votre Excellence,
en lui dédiant une médaille d’honneur.“ Le Bureau.

La séance est levée a 2 30".

 

 
