LR D DRE D ETI EEE a |

INDICATION
DES POINTS.

Observatoire

 

| Krakus

Mogielnica
| Wielki-Salasz. .

Wloszezyce
Ste. Anne
Kobelnica

Terme Nord

Terme Sud
Odporiszow ..

Krulowa ....
St. Martin

Kamieniec
Zassow

Niwiska
Krolewski
Wielopolski . ..

Czarnowka. . ..
Gwoznica. .
Malawa

Kamien.
Hussow

Lysa gora
Circe

 

Lanckorona....

Wandahügel . . ..

Pissana hola. . ..

Base de Tarnow .

Base de Tarnow .

LATITUDE.

49° BA! 58"
49 50 59

50. 3.52

50 4 16

bO. 2 20

49 58 35
49 46 37
49 32 38
49
49
49

46
49

29 26
39 23
43 19

56 13
56 42
49 43 5

49 28 54
49 34 34
4353 9

50 4 23
0 +»

50° 4:1
501.9 10

50 8 54
50 5 54
40-0947

49 48 53
49 57 54
DO 8 3
50 12 49
50 10 49
49 57 51

49 50 21
49 50 24
50 055

18 10
29: 10

50 19 29
50 19 48
50 10 31
DO 29.92
50 10 38

50
49

 

LONGITUDE.

37° 1450”
37.22 52

:37 37 23

 

37 44 7
27 a7 32

af Ol 3
37 42 56
37 44 46

37 58 26
37 54 42
38 11 2i

8 3
38 18.58
38 25 37

38 25 49
33 34 52
38 34 47

33 30 42
38 34 16

38 35 47
38 34 58

38 41
38 38
38 40

69,1
3% 4
au. 0
39 18 54
39 34
39 21

33.20
39 Al 18
39 47 &

39 47
39 55

40 12
| 40 96

40 37
40 19
40 7

 

 

 

Autriche-Hongrie. |

EPOQUE.

1875
1875-76

1848-75-76

1848-75
1848-49-75

1848-75
1848-75
1850-75

1875
1875
1848-75-76

1848
1848
1848-75

1875-76
1875
1848-49-75

1849
1849

1849
1848-49

1849
1849
1848-49-75

1875
1848-76
1848-49

1848

1848 .
1848-76

1876
1875-76
1848-76

1848
1848-76

1848
1848
1848
1848
1848

 

 

DIRECTEURS
. ET OBSERVATEURS.

Tuma.
Kalmar, Tuma.

Ioh. Marieni, Kalmar,
Tuma. ;
Ioh. Marieni, Kalmar.

id.

id.
Nemethy, Tuma.

Rueber, Tuma.

Tuma.
id.
Nemethy, Tuma.

Toh. Marieni.

de:
| Nemethy, Kalmar,

Tuma.
Kalmar.
Ioh. Marieni, Rueber, Kal-
mar.
Rueber.
id.

id.

Toh. Marieni. :

id.
Rueber.
Toh. Marieni, Rueber, Kal-

mar, Tuma.

Kalmar. 3
Ioh. Marieni, Tuma.
Ioh. Marieni.

id.

id.
Ioh. Marieni, Tuma.
Tuma.

Kalmar, Tuma.
Pechmann, Tuma.

Pechmann.
Pechmann, Tuma.

. Pechmann.

id.
id.
id.

1d.

 

INSTRUMENTS.

Starke n° 2 de 10p. | |
Reichenbach .n° 2 |)
de 12 p. et Starke |

n° 2 de 10 p.

Reichenb. n° 1 et 2 | N.
de 12 p. et Starke"

n°2 de 10 p.
Reichenbach n° 1
et 2 de 12p.

Reichenb. n° 1 et 2 || N.
de 12 p. et Starke |

de 10 p.

Reichenb. n° 1 et 2 |

de 12 p.

Starke astron. de |
8p. et n° 2de10p. ||
Reichenb. de 12 p. |
et Starke n° 2 de |

10 p. :

Pp |
Starke n° 2 de 10p. |

id.
Starke astron. de
8 p.et n° 2 de10p.
ne de 12 p.
id.

Starke astron. de |
8 p. et n° 2del0p. |]
Starke n° 2 de 10p. |

id. |
Reich.n°1del2p.et ||
Starke n°2de 10p. |
Reichenb. de 12 p. |

id.

Reichenb. et Ertel |

astron de 12p.

Starke astron. de |

8p. et 10 p.
Starke de 10 p.

Reichenb. de 12 p. |

Reich. n° 1 de 12p.et |
Starken’2delüp. |

Starke n° 2 de 10p. |

10e

id.

Reichenb. de 12 p. |

ad. |
Reich. de 12 p. et |
Starken’2delOp. |
Starke n° 2 de 10 p. |

id.

Reich. n°2 de 12p. et |
Starke n°2del0p. |
Reich. n° 2 de 12 p. |
Reich.n°2del2p.et ||
Starken°2del0p. |
Reich. n° 2 de 12 p. |

id.
iG.
id.
id.

 

 

 

REMARQUES.

374, Russie.

|| N. 375,

376,

 

 

 

 

 

 

 

 
