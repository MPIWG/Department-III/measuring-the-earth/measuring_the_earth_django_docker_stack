 

 

L . 3
| ny
LOTIR We

| Vollegen à

  

| Li
em er Ne

LT

Protokolle der permanenten Commission.

Erste Sitzung

der permanenten Commission.

Rom (Capitol, Salone dei Conservatori) am 14. Oktober 1883.

Präsident: Ibanez.

Schriftführer: Hirsch und v. Oppolzer.

Anwesende Mitglieder der permanenten Commission: v. Bauernfeind, Faye und
v. Forsch.

Der Präsident eröffnet die Sitzung um 2" 15” Nachmittags, indem er die Anwe-
senden begrüsst, und ertheilt den Schriftführern das Wort, um den Entwurf des Berichtes
der permanenten Commission für das Jahr 1883 vorzulesen. Der zur Verlesung gebrachte
Bericht, dem die beiden Entschuldigungsschreiben von General Baeyer und Regierungs-
rath Nagel beigeschlossen sind, wird einstimmig angenommen und es wird beschlossen,
diesen Bericht in der morgigen ersten Sitzung der allgemeinen ÖOonferenz zur Verlesung
zu bringen.

General Baeyer, indem er brieflich seinem Bedauern Ausdruck giebt, aus Gesund-
heitsriicksichten nicht die Reise nach Rom unternehmen zu können, beauftragt Herrn
v. Oppolzer, ihn in der permanenten Commission zu vertreten; weiter erklärt er in der
Meridianfrage, sich für die Wahl des Greenwicher Meridians zu entscheiden und zeigt
an, dass der Sectionschef des geodätischen Instituts, Prof. Fischer, mit der Ueberbringung
des Berichtes des Centralbureaus beauftragt sei.

In Beantwortung einer an den Präsidenten gestellten Anfrage, ob es nicht an-
gezeigt sei, dass Prof. Fischer diesen Bericht des Centralbureaus vorher zur Kennt-
niss des Präsidiums bringe, verspricht derselbe, den Versuch zu machen, sich denselben
rechtzeitig zu verschaffen, um ihn noch für die erste Sitzung der Conferenz in das
Französische übersetzen lassen zu Können.

 
