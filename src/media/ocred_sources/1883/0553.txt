 

Das ete

dei"
la suyenit |

rare, él
À fi u

in |
pert
able june
ioe
gone p

45

temps; ces circonstances l’ont déterminé à abandonner le système: Néanmoins, autant
que le rapporteur peut en juger, la possibilité d'utiliser pratiquement cette ingénieuse
idée n’est point définitivement exclue.

Si l’on considère les couteaux comme des cylindres d’un diamètre très petit, le
pendule à réversion de Besse! offre sur celui de Kater un avantage marqué qui n’a pas
toujours été apprécié. Si les deux couteaux étaient des cylindres de même diamètre, l’in-
fluence de la convexité s’éliminerait absolument, on le sait; mais l'égalité de rayon des
cylindres est une hypothèse qu’on ne peut pas suffisamment justifier; aussi Bessel a-t-il,
dans son pendule à réversion, disposé les couteaux de telle sorte qu’ils peuvent être échan-
ges; de cette façon, il devient possible, d'obtenir deux résultats dont la moyenne est
indépendante de cette source d’erreur.

Cette propriété du pendule à réversion permet donc, si l’on peut considérer les
couteaux comme cylindriques, de prendre pour base de la mesure de longueur la distance
des soumets de ces cylindres, cest-à-dire l’espace compris entre les arêtes visibles des
couteaux, pourvu que l’on fasse les observations dans les deux positions du pendule (poids
plein en haut et poids plein en bas) tant avec les couteaux dans leur position normale
qu'avec les couteaux échangés.

Cet échange des couteaux à été souvent le point de départ des attaques dirigées
contre le pendule de Zessel; on à d’abord fait ressortir que, par suite de pressions pour
le calage, cette opération pouvait amener une déformation des couteaux. Mais une telle
déformation peut être considérée comme inadmissible, vu le genre de serrage employé
dans les appareils de Repsold; on a objecté en outre que le parallélisme des couteaux se
perd par l'échange, et qu'il faut une rectification pénible pour remplir cette condition
pour chaque cas. Mais cette objection contient une erreur assez essentielle: Ces. deux
conditions 1° que les couteaux doivent être perpendiculaires aux axes principaux d'inertie,
et 2° que le plan passant par les couteaux considérés comme paralléles doit contenir le
centre de gravité du pendule, n’ont besoin d’être remplies qu’approximativement; en effet,
toutes les erreurs provenant de ces imperfections n’influencent les résultats que de leur
carré; il suffira done que ces conditions soient remplies une fois pour toutes de la part
du mécanicien par une position à peu près exacte des arrêts tant du pendule que
des couteaux. Même une erreur de quelques minutes en arc dans ce cas n’influencera
pas encore d’une maniere appréciable la septiéme décimale du résultat; or on peut tou-
jours atteindre cette exactitude en y apportant les soins nécessaires; ainsi par exemple
l'erreur de la convergence des couteaux du pendule lourd construit per Repsold pour la
mesure du degré autrichienne n’est que 0’.1, celle du pendule léger 0”.3; elle est donc tout
à fait insignifiante pour le résultat. La déviation des couteaux de la position exacte par
rapport à l’azimut peut être beaucoup plus grande, et elle sera complétement sans in-
fluence quand on peut considérer le pendule comme un solide de révolution par rapport
à l’axe vertical. Les pendules dont on se sert remplissent à peu près cette condition,
de sorte que même des erreurs de plusieurs degrès dans l’azimut restent sans importance
pour l'exactitude du résultat. Une brochure peu connue de M. J. W. Lubbok (Philosophical

 

io eae

 

Fe
——

 

 
