 

|
|

150

temps, de remplacer les heures locales par un certain nombre d'heures normales; ainsi
l'Institut du Canada a proposé de diviser le globe en 24 zones horaires, limitées par
les 24 méridiens principaux, à partir du méridien initial; et dernièrement le savant
astronome M. Gyldén, estimant avec raison que ces intervalles d'heures étaient trop
orands, les a remplacés par des intervalles de 10 minutes, en divisant la Terre par
144 méridiens horaires.

Mais avec le premier système, on imposerait encore aux populations des inéga-
lités entre les deux moitiés du jour qui, eu égard à l'équation du temps, pourraient
aller jusqu’A presque 1" '/,, et cela sans satisfaire aux exigences des grandes admi-
nistrations de communication. Au contraire, il arriverait ainsi que deux stations d’une
méme ligne de chemin de fer, éloignées de quelques kilometres et ressortissant de la même
administration, mais situées des deux côtés d’un de ces méridiens principaux, se trou-
veraient différer de 1" pour le temps de leurs gares. Et avec le système de M. Gyldén
qui violenterait moins les habitudes de la vie quotidienne, les chemins de fer, les postes
et les télégraphes auraient cependant à compter encore avec 144 heures différentes
et avec plusieurs heures à l’intérieur d’un même réseau administratif. Le progrès ne
serait pas sensible.

Enfin l'intérêt de la science aurait plutôt à souffrir qu’à profiter de l’introduction
des heures régionales ou nationales; car, comme nous l’avons dit déjà, toutes les mesures
exactes dans lesquelles entre la notion du temps, sont basées en premier lieu sur le
temps local; non seulement l’Astronomie ne peut déterminer directement que l’heure
locale, mais toutes les coordonnées célestes, géographiques, nautiques reposent sur des
observations faites suivant l'heure locale; la plupart des observations météorologiques,
magnétiques et autres semblables doivent également être distribuées d’après l’heure
locale. Et, d’un autre côté, pour combiner les observations faites en différents lieux,
les rendre comparables et en tirer des conséquences générales pour la science, il faut les
rapporter à une seule et même heure, et non pas à tout un groupe d'heures régionales,
qui ne seraient qu'un intermédiaire soit insuffisant, soit même nuisible.

Il nous semble résulter de cette discussion que, sans vouloir méconnaître les
avantages que Vheure nationale peut présenter dans certains pays, on ne pourra satis-
faire à la fois aux différents besoins de la vie civile, des grands établissements de com-
munications internationales, et de la science, qu’en introduisant, à coté des heures locales,
une seule heure universelle, cosmopolite.

Les administrations des chemins de fer, des grandes lignes de bateaux à vapeur,
des télégraphes et de la correspondance postale, qui recevraient ainsi, pour leur re-
lations entre elles, un temps unique, excluant toute complication et toute erreur, ne
pourraient cependant pas non ‘plus se passer entièrement des heures locales dans leurs
rapports avec le public. Elles se borneront probablement à employer l’heure universelle
dans leur service intérieur, pour les règlements de service, pour les horaires des con-
ducteurs de train et des capitaines, pour les jonctions des trains aux frontières etc.:
mais les horaires destinés au public ne sauraient être exprimés qu’en heure locale ou

 

TS

 
