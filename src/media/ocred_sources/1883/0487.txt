 

Jonctions preparées. Jonctions exécutées où non.

Russie.
Avec la Prusse. En Nimmersatt (Polangen)
et projeté à Thorn.
Autriche et Radziwilow (Brody)
et projeté à Seczakowa.

Pas encore rattachés.

Pas encore rattaché.

Mn
Suisse.
Avec la France aGénéve, La Cure, Morteau, Rattaché.

St. Louis.

, » Bade & Bale, (Säckingen), Stein, Rattaché.
Albbruck, Schaffhouse, Constanz.

» » Bavière à Fussach. Rattaché.

„ » Autriche à Fussach et Martins- Martinsbruck encore à faire du côté
bruck. Autrichien.

» » Italie à Chiavenna, Chiasso, Domo- Rattaché.

dossola.

On voit qu'il manque encore un assez grand nombre de jonctions; comme les plus
importantes qu’il serait désirable de voir s’exécuter le plus tôt possible, nous citerons
les jonctions suivantes:

Entre la Prusse et la Russie & Eydtkuhnen & faire par les deux pays.

Nimmersatt (Polangen) & faire par la Russie.
Thorn & faire par les deux pays.
Prusse et P’Autriche -& Oderberg & faire par la Prusse.
Oswiezim & faire par la Prusse.
Peterwitz & faire par les deux pays.
Autriche et Russie Seczakowa & faire par les deux pays.
Radziwilow (Brody) a faire par l’Autriche.
Autriche et Saxe & Wiesenthal a faire par les deux pays.
Autriche et Baviere Eisenstein |
Liattenwalde a faire par les deux pays.
ins # | Lindau |
ny Autriche et Suisse & Martinsbruck & faire par l’Autriche.
gl. Autriche et Italie à Pontebba |
Peri a faire par les deux pays.
Stilfser Joch
| Italie et France Rien n’a encore été fait; pas même projeté.
| A France et Belgique Rien n’a encore été fait.
: Annexe IY. 2

 
