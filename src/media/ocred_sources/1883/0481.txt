 

Ni a

hy b) Est-ce qu’on observe exactement dans Vhorizontale, ou fait-on la lecture

Tey 4 du niveau à chaque visée, pour tenir compte de l’inclinaison dans la
\ réduction ?

bo c) est-ce que les erreurs instrumentales sont éliminées par une égalité

rigoureuse des distances en avant et en arrière, et comment assure-t-on

a cette égalité?

N d) ou est-ce qu’on détermine les erreurs instrumentales, pour en tenir compte
Rai dans la réduction? à quels intervalles et par quels moyens? comment
le de bh détermine-t-on la distance de la mire?

e) Est-ce que les équations des mires sont déterminées périodiquement? par

Là a quels moyens et par rapport à quel étalon? est-ce qu’une de vos mires a
été comparée à l’étalon de Berne? Indiquer son équation obtenue alors.
1 7. Calculs de réduction et de compensation :

a) comment se font les calculs de réduction, pour tenir compte de l’incli-
naison, des erreurs instrumentales et des équations des mires?

b) avez-vous déjà opéré une compensation de votre réseau ou de certaines
parties? d’après quel principe? tenez-vous compte de la longueur des
lignes seulement, ou aussi des différences de niveau? quel poids relatif
donnez-vous aux lignes doubles et aux lignes simples, formant des poly-
gones? Publiez-vous les résultats directs et compensés ?

8. Exactitude des résultats; quelle est l’erreur probable par Kilometre, deduite
soit des doubles nivellements, soit de la clôture des polygones ?
gr 9. A quelle époque prévoyez-vous pouvoir terminer votre nivellement sur le
hit terrain, et quand les calculs?
ri 10. Publications faites sur les nivellements de précision dans votre pays.
pi
je i Jusqu’au moment ot nous écrivons ce rapport (commencement de Septembre) tous
x les pays ont répondu, à l’exception de la France,*) de la Roumanie et du Würtemberg.

ne ; : . ;
Nous ignorons si nos collégues de ces pays n’ont point envoyé de réponse, parceque dans

sf les trois dernières années on n’a pas fait de travaux chez eux; il aurait été désirable
d’être fixé sur ce fait par une déclaration formelle, comme cela a été fait par le Danemark.

Nous donnons d’abord dans le tableau suivant les longueurs des lignes nivelées,
en indiquant si elles ont été nivelées une fois ou à double dans le même sens ou dans
le sens inverse. Nous mettrons en paranthèse les chiffres correspondants de 1880 pour mieux

le ig ft

pi ip faire voir l’avancement des travaux pendant les trois ans.
gi? *) Dont le rapport nous est parvenue le 12 Septembre.

j 1?
sf

Be een

 
