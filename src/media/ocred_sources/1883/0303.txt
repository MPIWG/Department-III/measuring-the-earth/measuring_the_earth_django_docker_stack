 

th
I

 

desquels on déduit à l’aide des formules de M. Cellerier le résultat le plus probable, exempt
de l'influence de la flexibilité du support, en parties de l'échelle à la température
de 129062

m

L’ = 0.9928151 +53

 

L’accord entre les valeurs les plus probables de la longueur absolue du pendule
simple, ci-dessus consignées, qui proviennent d’observations avec le grand appareil, met
en évidence, qu'on peut procéder, dans la mesure de la pesanteur, soit en eliminant
l'influence des mouvements simultanés du support par la combinaison des résultats obtenus
séparément avec les deux pendules de masses très differentes, soit par les observations
d’un seule pendule, en évaluant de plus, par des expériences spéciales, la dite influence.
Mais sans préjuger ici la question de préférence, laquelle devra toujours être accordée
au point de vue pratique, je remarquerai en passant que la précision du résultat total
obtenu par l'emploi des deux pendules, dépend d’une manière directe du rapport de leurs
masses. D'ailleurs les deux corrections calculées pour la longueur du pendule à secondes,
en employant toujours le pendule lourd et respectivement avec les couteaux et plan en
acier et en agate, présentent une différence bien petite (4 microns), mais qui dépasse de
beaucoup la limite des incertitudes qu'on peut assigner aux valeurs des corrections d’après
leurs erreurs probables. On pourrait donc croire à l'influence de causes dont les effets
sur la transmission au support du mouvement oscillatoire du pendule, ne se sont pas pro-
duits d’une manière tout-à-fait identique dans les deux cas.

Les chiffres qui résultent des observations avec les deux pendules du petit appa-
reil constatent une diminution très-sensible dans la constante du balancement en comparaison
de celle qui, selon les expériences de M. Plantamour, appartient à Vappareil de la Suisse;
ce fait, d’une importance secondaire, tient sans doute a une modification apportée par le
constructeur dans la forme du trépied. Je dis d’une importance secondaire, puisqu’on
ne saurait se dispenser jamais de l’étude des mouvements transmis par le pendule au
support, quelles que soient leurs masses et leurs conditions d'installation. De très-récentes
expériences que j'ai faites, en me procurant un grossissement supérieur à 6000 fois, et avec
le même appareil à miroir reposant sur un pilier bien solide, en pierre de taille, dans
l'édifice occupé par l’Institut géographique où j'avais fait dans le temps les obser-
vations de pendule dans un but provisoire, ont constaté encore une fois l'impossibilité
d'admettre à priori pour le calcul, l'invariabilité dans l’espace de laxe du mouvement
oscillatoire.

Voici tout ce que je dois communiquer, pour le moment, sur mes expériences sur
la mesure de la pesanteur, en me réservant de faire les remarques et d’exposer les con-
clusions que pourraient me suggérer mes travaux, lors de leur réduction définitive.

J. M. Barraquer
Délégué de l'Espagne.

37

 
