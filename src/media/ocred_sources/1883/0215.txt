 

F
FE
|
È
FE

1. L’unificaton des longitudes et des heures est désirable, autant
dans l’intérét des sciences que dans celui de la navigation, du commerce
et des communications internationales: l'utilité scientifique et pratique
de cette réforme dépasse de beaucoup les sacrifices de travail et les
difficultes d’accommodation qu’elle entrainerait. Elle doit donc être re-
commandée aux gouvernements de tous les Etats intéressés, pour être
organisée et consacrée par une Convention internationale, afin que, désor-
mais, un seul et même système de longitudes soit employé dans tous les
instituts et bureaux géodésiques, du moins pour les cartes géographiques
et hydrographiques générales, ainsi que dans toutes les éphémérides
astronomiques et nautiques, à l’exception des données pour lesquelles il
convient de conserver un méridien local, comme pour les éphémérides
de passage, ou de celles qu’il faut indiquer en heure locale, comme les
établissements de port, etc.

IL. Malgré les grands avantages que l'introduction générale de la
division décimale du quart de cercle, dans les expressions des coordon-
nées géographiques et géodésiques, et dans les expressions horaires
correspondantes, est destinée à réaliser pour les sciences et pour les
applications, il convient, par des considérations essentiellement pra-
tiques, d’en faire abstraction dans la grande mesure d’unfication pro-
posée dans la première résolution.

Cependant, pour donner en même temps satisfaction à des consi-
dérations scientifiques très sérieuses, la Conférence recommande, à cette
occasion, d'étendre, en multipliant et en perfectionnant les tables
nécessaires, l’application de la division décimale du quart de cercle, du
moins pour les grandes opérations de calculs numériques, pour lesquelles
elle présente des avantages incontestables, même si l’on veut conserver
l’ancienne division sexagésimale pour les observations, pour les cartes,
la navigation, etc.

Il. La Conférence propose aux Gouvernements, de choisir, pour
méridien initial, celui de Greenwich, défini par le milieu des piliers de
Pinsteument méridien de l'observatoire de Greenwich, parce que ce
méridien remplit, comme point de départ des longitudes, toutes les
conditions voulues par la science, et que, étant déjà actuellement le plus
répandu de tous, il offre le plus de chances d’être généralement accepté.

IV. Il convient de compter les longitudes à partir du méridien de
Greenwich dans la seule direction de l'Ouest à l’Est. a

V. La Conférence reconnait pour certains besoins scientifiques, et
pour le service interne des grandes administrations des voies OY con.
munication, telles que celles des chemins de fer, lignes de bateaux a

1 ea ilite d’ i e heure universelle
vapeur, télégraphes et postes, l’utilité d adopter un . 5

 
