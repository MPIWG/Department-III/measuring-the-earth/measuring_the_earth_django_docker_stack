|
|
|

|

 

 

 

 

 

278

Russland. Russie.

Notre Section topographique militaire a eu, pendant les derniéres années, tant
de besogne à accomplir pour satisfaire aux besoins urgents de l’Etat-major, qu’il ne lui
restait que très peu de temps et de moyens pour des travaux géodésiques. Le seul
travail de ce genre, qui soit à mentionner ici, est la continuation du nivellement de
précision, à laquelle ont été employés quatre topographes pendant l'été passé.

Le nivellement a été poussé jusqu’à la mer Noire, de manière que nous possédons
aujourd’hui un double nivellement continu entre celle-ci et la mer Baltique, qui nous
servira à déduire la différence de leurs niveaux.

En outre deux tronçons de nivellements ont été poussés vers la frontière de
l'Autriche; l’un d'eux suit le chemin de fer de la Galicie et aboutit à Radziwilowa, où
il a été établi un point de repère sur un pilier en maçonnerie. J’invite nos collègues de
l'Autriche à vouloir bien s’y raccorder, lorsque leur nivellement y sera arrivé.

L'autre tronçon, partant de Varsovie, aboutit à Granitza, vis à vis Czakowa, de
l’autre côté de la frontière, où se trouve un point autrichien auquel nous nous rattacherons.

J'ajoute encore que, dans le courant de l’année 1883, M. le Colonel Kuhlberg &
déterminé la longueur du pendule simple en deux points du Caucase, savoir à Baku et
à Schemacha. Ces déterminations offrent un intérêt particulier, attendu que dans ces deux
endroits ou croit pouvoir supposer l’existence de vides souterrains.

Ed. Forsch.

eee

Sachsen. Saxe.

Die astronomischen Arbeiten haben im Jahre 1883 nur in den Berechnungen der
Bruhns’schen Beobachtungen bestehen können, die unter der Leitung des Herrn Prof.
Dr. Albrecht in Berlin ausgeführt worden sind.

Vollendet sind ausser den bereits publicirten Längen, Breiten und Azimuthen
die Berechnungen der Polhöhen und Azimuthe auf den Stationen Lausche, Leipzig
(Pleissenburg), Wachauer Denkstein, Grenzhübel, Wachberg, Markstein und Schwarze-
berg, wofür auch das Manuscript für den Druck fertig gestellt worden ist.

Die Polhöhen haben sich ergeben:

m

 

 

 
