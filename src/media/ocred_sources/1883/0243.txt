 

|
|

 

229

 

 

| di
| |
|
|

4° croupe.

 

 

 

 

Désignation des cotes. sine | 8° groupe. | 11° groupe. en
St. Hubert— Bouillon | 3945270114 | 3945179851 +0"0263
Bouillon— Willerzie | 25269-7805 | 25269-8591 —0.0786
Willerzie—Philippeville | 84907-3379 | 3490778620 | —0.0241
Philippeville—Flavion | 14008.3244 | 14008-3928 | —0.0684

Flavion—Bois de Villers 15998-9282 | 15999-0022 230-0740

|

 

L'’achèvement des calculs relatifs aux 12 groupes fondamentaux et aux 4 groupes
subsidiaires ayant fait connaître les corrections à apporter aux directions du réseau, il a
été possible de terminer les opérations pour 40 Stations, sur les 84 que comporte le
réseau primaire.

Les calculs ont consisté :

1° dans la recherche du point nul à chaque station;
20 dans la détermination des corrections et directions définitives;
30 dans le calcul de l’erreur moyenne à chaque station.

Ces erreurs moyennes permettront d'établir à la fin des travaux l’erreur moyenne
des observations de la triangulation du 1‘ ordre.

Toutes les mesures sont prises pour qu’en 1884, de Juin à Septembre, on puisse
observer une latitude et un azimut dans le Sud du pays. Le point choisi est la station
de 1° ordre Hamipré, dans la province du Luxembourg.

Il était de toute nécessité d’avoir dans cette partie du pays un contrôle aux
calculs géodésiques. Non seulement ces observations permettront de comparer la latitude
astronomique d’Hamipré avec la latitude géodésique, mais serviront à déterminer l’amplitude
de l'arc de méridien correspondant à la chaine de triangles se dirigeant de Lommel vers
Hamipré, c’est-à-dire dans le sens de la plus grande dimension Nord-Sud du pays.

27 Mars 1884.

Le Major d’Etat-Major, Directeur
E. Hennequin.

 
