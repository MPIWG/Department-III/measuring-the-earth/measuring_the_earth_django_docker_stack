 

 

42

erst zwei bis drei Stunden vor Mittag. In den Vereinigten Staaten hätte man dagegen
den Vortheil, dass der bürgerliche Arbeitstag mit dem Universaltage vom gleichen Datum
zusammenfällt. Dieser Vortheil würde nur ein wohlverdienter Lohn dafür sein, dass die Ver-
einigten Staaten von Amerika die Ersten waren, welche den europäischen Ausgangs-
meridian mit Bereitwilligkeit angenommen haben.

Man möge uns noch eine letzte Bemerkung gestatten und zwar, um einen mehr
scheinbaren als begründeten Einwand zurückzuweisen, den man uns vielleicht entgegen-
halten könnte, indem man uns der Inconsequenz zeiht, Greenwich für den Ausgangs-
meridian anzurathen und zugleich die Zeit von dem Meridian aus rechnen zu wollen,
welcher um 180° von Greenwich entfernt ist. Abgesehen davon, dass beide demselben
Meridian-Kreise angehören, kann man mit vollem Recht behaupten, dass der Meridian
von Greenwich der Ausgangspunkt für die Zeitbestimmung ist, da ja die astronomischen
und kosmopolitischen Tage von dem mittleren Mittag von Greenwich an zählen. Man
würde gewiss nicht rationell verfahren, wollte man für einen grossen Theil der Karten
die Längengrade ändern, sowie die Seefahrer und Geographen nöthigen, die ihnen ge-
läufigen Längen um 180° zu vermehren, einzig und allein um den für die bürgerliche Zeit
zum Datums-Ausgang dienenden Meridian auch zugleich als Ausgangs-Meridian für die
Längen anführen zu können, und auch das nur scheinbar, denn wir haben ja nachge-
wiesen, dass in Wirklichkeit der Ausgangs-Meridian durch das Observatorium von Green-
wich bestimmt werden muss.

Wir schliessen hiermit unseren Bericht, indem wir der Versammlung folgende
Beschlüsse unterbreiten:

„Die siebente General-Conferenz der in Rom tagenden Europäischen Gradmessung,
an welcher Vertreter Grossbritanniens sowohl, wie die Direktoren der hauptsächlichen
astronomischen und nautischen Ephemeriden, und ein Abgesandter der Coast and Geodetic
Survey der Vereinigten Staaten Theil genommen haben, hat nach gepflogener Berathung über
die Unification der Längen durch die Annahme eines einzigen Ausgangs-Meridians und
über die Unification der Zeit durch Einführung einer Universal-Zeit folgende Re-
solutionen gefasst:

I. Die Unification der Längen und der Zeit ist sowohl im Interesse der Wissen-
schaften, als in demjenigen der Schifffahrt, des Handels und des internationalen Ver-
kehres wünschenswerth; der wissenschaftliche und praktische Nutzen dieser Reform
übersteigt um Vieles die Opfer an Arbeit und Angewöhnung, welche dieselbe für die
Minderzahl der civilisirten Nationen zur Folge haben würde. Sie ist somit den Re-
gierungen aller betheiligten Staaten anzuempfehlen, in dem Sinne, dass dieselbe durch
eine internationale Convention organisirt und eingeführt würde, damit in Zukunft ein
und dasselbe Längensystem in allen astronomischen und nautischen Ephemeriden, in
allen geodätischen und topographischen Anstalten und Bureaus, sowie für die geogra-
phischen und hydrographischen Karten angewendet werde.

A. Die Conferenz schlägt den Regierungen als Ausgangs-Meridian den von
Greenwich vor, welcher durch die Mitte der auf der Sternwarte von Greenwich für das

 
