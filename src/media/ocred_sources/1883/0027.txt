 

13

Ausgangsmeridians in wissenschaftlichen Gesellschaften, zuletzt im geographischen Con-
gresse zu Venedig aufmerksam gemacht hat — welcher Congress sich dahin entschieden
hatte, die Frage vor die Regierungen und geographischen Gesellschaften der verschiedenen
Länder zu bringen — hervor, dass das Streben nach einer diesbezüglichen Vereinheit-
lichung in einer Stadt wie Hamburg, in welcher alle Interessen sich auf die Schifffahrt
und die Beziehungen mit entfernten Ländern gründen, mit den lebhaftesten Sympathieen
aufgenommen würde.

Die geographische Gesellschaft zu Hamburg, von Seite des Senates befragt, hat
an den letzteren einen Bericht gerichtet, den Herr Dr. Kirchenpauer uns in Abschrift
mittheilt, in welchem die Nothwendigkeit der Einigung über einen gemeinsamen Ausgangs-
meridian anerkannt wird, sie zeigt sich ferner für die Annahme des Greenwicher Me-
ridans als solchen geneigt und schlägt als bestes Mittel, um diese Frage der Lösung
näher zu bringen, vor, dass die permanente Commission der Europäischen Gradmessung
diesen Gegenstand zur Behandlung übernehme.

Der Senat der Stadt Hamburg in Uebereinstimmung mit den Wünschen vieler
wissenschaftlichen Gesellschaften meint, dass man durch eine Verständigung zwischen den
Culturstaaten zur gemeinsamen Annahme des Greenwicher Meridians als definitiven
Ausgangsmeridians gelange und derselbe spricht die Hoffnung aus, dass, wenn die allgemeine
Gradmessungs-Conferenz ihr in solchen Gegenständen allseitig anerkanntes Gewicht in die
Wagschaale legen würde, diese seit Jahrhunderten unerledigte Frage bald eine Lösung
finden würde. Schliesslich wird in diesem Schreiben an uns das Ansuchen gestellt, die
geeigneten Massnahmen einzuleiten, dass die allgemeine Gradmessungs-Conferenz auf diese
Frage eingehe und eine Entscheidung treffe.

Ihr Bureau glaubt in Hinblick auf einen so schmeichelhaften Schritt von Seiten
einer Regierung, welche unserem wissenschaftlichen Unternehmen ein so grosses Interesse
entgegenbringt und welche uns vor wenig Jahren in Hamburg mit einer uns stets in
angenehmer Erinnerung bleibenden liebenswürdigen Gastfreundlichkeit aufgenommen hat,
dass die permanente Commission nicht Anstand nehmen sollte, sich mit einer Frage —
wenn auch diese in keinem direkten Zusammenhange mit unseren geodätischen Special-
studien steht — zu beschäftigen, welche sicherlich nicht bloss vom allgemeinen Stand-
punkt der Civilisation, sondern auch für den Fortschritt der geographischen Wissenschaft
von Bedeutung ist, einem Fortschritt, welchem gegenüber die allgemeine Gradmessungs-
Conferenz nicht gleichgiltig sein wird.

In der That unsere Gesellschaft ist eine der ersten internationalen Einrichtungen,
durch welche gegenwärtig die Regierungen der Öulturstaaten sich geeinigt haben, in
gemeinsamem Wirken das Feld der Forschung und die Interessen der gemeinsamen
Civilisation zu fördern, welche Interessen ihrer Natur nach die politischen Grenzen
weit überschreiten. Wir glauben daher, dass unsere Gesellschaft sich für einen Versuch
zur Einigung über die Längenzählung erwärmen sollte, ein Versuch, welcher nicht nur
von einer grossen Bedeutung für eine Menge von praktischen Interessen ersten
Ranges ist, als da sind: Schifffahrt, Seehandel, Telegraphenverwaltungen, Eisenbahnen,

 
