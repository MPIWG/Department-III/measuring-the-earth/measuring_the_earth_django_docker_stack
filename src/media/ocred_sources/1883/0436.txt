ENDIOATTON

DES POINTS.

LATITUDE.

  

LONGITUDE. |

en

Portugal.
i ee en
|

EPOQUE.

DIRECTEURS
ET OBSERVATEURS.

INSTRUMENTS. ||
|
|

REMARQUES,

 

TT Sr Os

10

li
12
13
14

15

17
18
19
20

 

 

Alearia do Cume .

Alearia Ruiva...

Aljustrel......

Almeirim. . . ...

Cabeca Alta...
Cabo de Ste. Maria
Cabreira ......

Caramullo

Cereal oe hh

laya)

Corvo

Fonte-longa . .

37° 14 32”
37 41 56

3932 3

37 52 48
59, 8 26
38.10 4
40 38 36

38 44 22

40 11 38

40 21 37
37
40
36
41

Th 7
31 55
58 24
38 14

38
39

40
37 47 34
40

ay AT

40 49 38
41

50 45

18 51

bo
—

 

13 59

9° 57'42”||
9 56 21 ||
2 290

 

9 31 59
9 6501|
94 318
958 3 |

8 45 43

8 50 47 |
9 20 45
10 12 48
10 16 31 |
u 30.10
9 39 25

8 46 55

9 29 54

8 58 55

9 35 47

9.610

 

10 26 26

10 2 48 |

9 16 28 |

10 35 36 |

10 2 9

 

 

1876
1876

1850-72

1855-70

1877
1859-74
VOR
1863-82

1852
1851-70

 

 

Pereira da Silva, Avila.

Pereira da Silva, Carvalho
da Silva.

Filippe Folque, Batalha,
Botelho, Veillot.

Filippe Folque, Carvalho
da Silva.

Filippe Folque, Botelho.
Pereira da Silva, Veillot.

Filippe Folque, Albuquer-
que, Carvalho da Silva.

Filippe Folque, Veillot.

Filippe Folque, Albuquer-
que, Carvalho da Silva,

Filippe Folque, Albuquer-
que, Veillot.

Pereira da Silva, Carvalho
da Silva.

Filippe Folque, Leite Vel-
ho, Pego.

Pereira da Silva, Carvalho
da Silva.

Filippe Folque, Leite Vel-
ho, Arbues Moreira, Pau-
lino Corréa.

Filippe Folque, Batalha.

Filippe Folque, Batalha,
Veillot.

Filippe Folque, Botelho.

Pereira da Silva, Corte
Real.

Filippe Folque, Albuquer-
que.

Filippe Folque, Leite Vel-
ho, Carvalho da Silva.

Arbues Moreira, Paulino
_ Corréa.
Pereira da Silva, Veillot.

Arbues Moreira, Corte Real.

 

Univ. de Trough-
ton ne >. |

Univ. de Trough-
ton n°1: |

Theod. de Trough-
ton de 10 p. et |
Univ. de Trough- |
ton n°2;

Univ. de Trough-
ton 1-1,

Théod. de Trough-
ton de 10 p.

Univ. de Trough-
ton n°2. |

Theod. de Trough- |
ton de 10 p. et |
Univ. de Trough-
ton a ds

Cercle répétiteur
et Un. de Trough-
ton n° 2. |

Théod. de Trough- |
ton de-10p. et
Univ. de frough- |
born |
Théod. de Trough- |
ton. de- 10 pee
Univ. de Trough- |
torn in? D. |
Univ. de Trough-
bon n°1.

Théod. de Trough-
ton de 10 p.
Univ. de Trough- |
ton n° À |
Théod. de Trough- |
ton de 10 p.. et ||
Univ. de Trough-
fom ne. |
Théod. de Trough-

ton n° 10.

Théod. de Trough- |

ton de 10 p. et
Univ. de Trough- |
ton n°2. |
Théod. de Trough-
ton de 10 p.
Univ. de Trough-
ton atid: |
Théod.de Trough- |
ton de 10 p. |
Théod. de Trough-
ton dé 10p. et
Univ. de Trough-
ton n° 1.

Univ. de Trough-
ton n°3.

Univ. de Trough- |

ton ur ar

 

id.

 

  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

    

 
