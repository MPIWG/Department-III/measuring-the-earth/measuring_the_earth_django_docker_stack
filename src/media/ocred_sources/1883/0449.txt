Ta oe

Roumanie.

 

 

INDICATION |
N LATITUDE. | LONGITUDE.
DES POINTS.

: a Ka |

I]
|
DIRECTEURS |
ÉPOQUE. INSTRUMENTS. | REMARQUES.
ET OBSERVATEURS. |

 

 

 

 

Boldoveni : .... 45° 6:30"| 40° 12’ 24"

||
1 || N. 462, Autriche.
2 | Hunca Comena .. | 44 5428| 40 9 4| || N. 463, Autriche.
3 | Movila Brinzeni . 4457 9 | 40 27 234 | |
4 | Cimpu Mare... . | 44 46 34 | 40 23 15 | |
D | D. Dregesti . . . . | 44 34 18 | 40 10 34 | |
D OH =... | 44,33 56 | 40 29 O0 ll
D Piquel ... . :. | 144 99 52 | 40 14 9 | | |
8 | St. Corlatel . . . . | 44 23 99 | 40 36 9 |
9 | Costa Isyore.... | 44 17 59 | 40 20 25 | |

10 | Därvari de Jos . . | 44 11 42 | 40 44 50
11 | D, Bacerova. . . . | 44 6 53 | 40 30 16
Ho 1locoasa... .:.. 24217 40) 935.47

 

a 13 | Magura Camiloiu- |
E BN 44 6 24 | 40 59 41 |

21) Obrehi ..:.... 43: 50 8 140: 59. 36
15 | Monila Branga . . | 43 58 48 | 41 20 52

10 Ohilor Baeri. . .. | 45 47 37 | 41,19 17

 

©
=
©
>
=
2
a
4
&
17 | Grindu Gol .... | 43 56 7 | 41 38 56 5
18 | Trei Moghile ...| 43 44 19 | 41 36 29 >
PeCopita: 2. 2... 43 56 5 | 41 51 25 5
= 20 | D. Virtej. .. ... 43 41 15 | 41 55 33 =
ne Of | Wieinia:...... 43 46 2 | 42 14 55 a cy
E Oo Baranza .: .... 43 56 7 | 42 44 47 = a
pa) Osteria... 43 55 12 | 42 22 17 = ar
Of Octatea.. ... .'. | 43. 3.37 | 42 18 40 or
E 3, Lusteria.... ... 4534129 1.2 .§ «2
LE SG Gbrejil.. <2)... 4713 6.2122 2 So
| & HH Böohu........ 44 14 17 | 41:56 15 | E eis
L 28 | Capu Braluluï. . . | 44 20 40 | 41 52 53 5 a
E 29 | M. Casacelor. ... | 44 2 20|42 3 17 > < face
E ee Oporel . :.... 44.35 '8.| 42.523 2 mn ein
i 31 | D. Oltului..... 44 49 36 | 41 54 48 & à tone
32 | Piseu Dobrei ... | 44 49 6 | 42 6 22 “= = ,
33 Crüce înalte. . . . | 44 53 38 | 41 52 19 oS 28
De Hostaru...... 45 4 49 | 42 12 30 i eR
Jo Bocul frumos. . . | 45 9 6 | 41 57,17 2 rare
A PB.  .. . 45 14 31 | 41 45 38 N oe
R pe fosia oi... 45 19 29 | 42 O 18 2 =
a nn ....:, 45 91 33 | 42 21 48 ©
3 39 | Virfu mare. ... . 45 31 34 | 41 47 50 $ SR
po WG Preshe. >... 45 56 34 | 41 46 54 5
E nl, 45 35 97 | 42 6 26 %
B ao Areeril, ....... 45 9 33 | 42 41 37 | >
\ 43 | Adincata...... 44 043 | 42 36 42 £
D. 44 | Magura de Passe. | 43 48 8 | 42 33 25 :
a 22a) Movila Gavanösa. | 43 55 16 | 42 47 6 = Nahe an 22.
1 45 | M. Soium. ..... 43 48 11 | 43 1 37 S
46 | Gradboir...... 43 36 48 | 43 0 35 3
47 | Kresnambasi. . . . | 43 39 29 | 43 17 56 =
fo Badina ....... 43 44 14 | 43 29 21 ee
49'| La Crocea . . ... 251 27 43 15 45 A
4 50 | Stefânesti ..... 3 55 54 | 43 29 51 | 5
4 51 | Levent Tabia . .. | 43 49 40 | 43 37 34 2
ig oe.) PEAssing ,. |... . 4A 1 58 145-4145 2

53 | Keresi Tepesi. . . | 43 51 44 | 43 57 49 |
94 | Topraklik Sirti. . | 44 2 44 | 44 16 24

 

 

 

 

 

 

 

 

 

 

 

É me Genes, 3... A | EQ) ak

, 56 | Badovan...... AAO TA AA lel Al |
a Weer ove... .|.44:10 81 | 43 55° 38 |
4 Dar Pirlitn =... .. 44 15 34 | 43 41 56 |
E 59 | Mosia-Micâ. . . .. 44 9999) 48 54 34 |

10

   
