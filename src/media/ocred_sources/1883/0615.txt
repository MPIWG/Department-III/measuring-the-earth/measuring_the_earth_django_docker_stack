 

 

|

ar
. ee
“en

N

15

Elementi trigonometrici dei punti contenuti nei fogli della carta d’Italia:

Foglio 241 — 1882.
» 242-438 — 1881.
» 245. — 18831.
„ 220 (SB,
» 247 — 1881.
» 204 — 1880.
pedo comn hte aks Li

» 263-64 — 1881.

Celoria, M. E. G. Latitudine di Milano dedotta da distanze zenitali osservate in prossi-

mità del meridiano. Milano, 1883.
_ Sopra una deviazione sensibile del filo a piombo esistente fra Milano e Genova. °

Milano, 1884.

Lorenzoni, G., Celoria, G. e Nobile, A. Operazioni eseguite nell’ anno 1875 negli
osservatorii astronomici di Padova, Milano e Napoli, in corrispondenza coll’
Ufficio idrografico della R* Marina per determinare le differenze di lon-
gitudine fra Genova, Milano, Padova e Napoli. Firenze, 1882.

Lorenzoni, G. Sulle determinazioni di tempo eseguite ad Arcetri nell’ autunno del 1882
colla osservazione dei passaggi di stelle pel verticale della polare. Venezia, 1884.

Nobile, A. Terza determinazione della latitudine geografica del R* osservatorio di Capo-
dimonte. Con esame delle osservazioni fatte il 1820 da Carlo Brioschi.
Napoli, 1883.

Pucci, E. Réduction des observations astronomiques et des angles géodésiques d’une
surface de niveau & une autre. (A. N., Bd. 99, 1881, No. 2363, p. 161.)

_ Sulla teoria delle basi geodetiche. (Giornale matematico publ. p. Prof. Battaglini.

Napoli, 1881.)

Rajna, M. Determinazione della latitudine dell’ Osservatorio di Brera in Milano e dell’
Osservatorio della R* Universita di Parma, per mezzo dei passagi di alume
stelle al 1° verticale. Milano, 1881.

Respighi, L. e Celoria, G. Osservazioni eseguite nell’ anno 1879 per determinare la
differenza di longitudine fra gli Osservatori astromici del Campidoglio in
Roma e di Brera in Milano. Firenze, 1882.

Il. Mecklenburg.

Landes-Vermessung, Grossherzoglich Mecklenburgische.
I. Theil. Die trigonometrische Vermessung.
IH. Theil. Das Coordinaten-Verzeichniss.
IH. Theil. Die Astronomischen Bestimmungen. Azimuth von Granzin, Pol-

 
