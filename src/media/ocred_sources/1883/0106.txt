 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

92

Nach dem Vorschlage Perrier’s wäre noch immer Greenwich der wirkliche Aus-
eangspunkt, man würde nur dessen Namen verstecken; um einer solchen Laune zu
genügen, sollte man die Numerirung der Längen auf einer Menge geographischer und
hydrographischer Karten ändern und verlangen, dass die astronomischen und nautischen
Ephemeriden für einen idealen, von jeder Sternwarte sicherlich entfernten Meridian ge-
rechnet werden? Der Bericht und die Discussion haben zur Genüge erwiesen, dass der
Greenwicher Meridian den Forderungen der Wissenschaft und den practischen Erwägungen
am besten Genüge leistet.

Bei der Abstimmung wird das Amendement des Herrn Perrier mit 18 Stimmen
gegen 4 verworfen und der Artikel III mit 22 gegen 5 Stimmen angenommen.

Die Resolutionen IV und V rufen keine Opposition hervor und werden die erste
mit 24, die zweite mit 27 Stimmen angenommen.

Artikel VI dagegen wird zunächst von Faye bekämpft, welcher die für den
Anfang des Welttages angenommene Definition zu complieirt findet und vorziehen würde,
einfach zu sagen: „Die Stunde von Greenwich wird als Weltstunde gewählt.“

v. Oppolzer fürchtet, dass diese Definition Faye’s Unsicherheiten und Missver-
ständnisse hervorrufe, da der bis zu 24 Stunden gezählte mittlere Tag bis jetzt immer
zu Mittag begann. Man müsste also auf jeden Fall den Vorschlag Faye’s ergänzen,
indem man hinzufügt, dass die Weltstunde zu Mittag beginnt. Dann würde er aber
vorziehen, die von der Commission vorgeschlagene Redaction, so wie sie ist, beizubehalten.

Perrier erklärt sich gegen den Mittag als Beginn des Welttages, weil man mit
diesem System in den Morgenstunden in Europa ein doppeltes Datum schaffen würde.

Christie theilt die Anschauung Faye’s und Perrier’s aus den Gründen, welche
er in der Commission entwickelt hat; er fürchtet, dass diese Anordnung der Annahme
der Weltzeit schade.

Förster erinnert daran, dass er schon in der Commission auf diese Betrach-
tungen geantwortet habe, und hebt nochmals hervor, dass einer der Hauptvortheile der Ein-
führung der Weltzeit darin bestehen werde, die Unterbrechung der Continuität und die
Unsicherheit der Daten im äussersten Osten verschwinden zu machen, und dass dieser
Vortheil gewiss am sichersten mit der vorgeschlagenen Combination erreicht würde.

Hirsch verkennt nicht, dass der von Christie und Perrier angeführte Nachtheil,
dass das Datum des Welttages in Europa in den Morgenstunden vom Datum-des bürger-
lichen Tages abweichen werde, einen ernsten Einwurf gegen die vorgeschlagene Com-
bination begründe. Auch er hat deshalb lange gezögert, ehe er sich derselben anschloss.
Was ihn schliesslich dazu bestimmte, ist der grosse Vortheil, dass man auf diese Weise
den Welttag nicht nur mit dem astronomischen Tage, sondern auch mit dem Tage der
Seeleute in Uebereinstimmung bringe, welche letztere den Mittag als Anfang des Tages
nicht aufgeben können, selbst wenn die Astronomen sich dazu entschliessen würden.
Uebrigens muss man nicht vergessen, dass der von den Gegnern angeführte Uebelstand
das grosse Publicum nicht berühre, da ja dieses fortfahren wird, nach Localzeit und
bürgerlichem Tage zu rechnen, welch letzterer nach wie vor mit Mitternacht beginnen wird.

 

 

 

 

 
