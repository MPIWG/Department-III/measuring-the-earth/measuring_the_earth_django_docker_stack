 

 

aire À

u
WI Nill
mye 8
| Gé
pré
mit

tra & |

des

yp ste

jamais. Ce n’est donc pas dans la mesure des bases modernes et mêmes anciennes, qu’il
faut rechercher la cause des discordances de valeurs obtenues pour des côtés communs à
des triangulations voisines.

Sans parler des erreurs angulaires qui comportent de véritables compensations,
dans une triangulation bien exécutée, la cause de ces discordances réside pour la plus
large part dans le défaut de comparaison des règles géodésiques avec un étalon unique:
la longueur de ces règles n’est pas encore exprimée directement en fonction d’une seule
et même unité.

C’est là une lacune que j'ai déjà signalée dans mes deux précédents rapports de
1877 et 1880, mais je me hâte de rappeler à l'Association qu’elle ne tardera pas à
être comblée.

Le Comité international des poids et mesures a en effet commandé le 18 Sep-
tembre 1882, a la Société Genevoise un comparateur destiné aux comparaisons des régles
géodésiques, a bouts et a traits, de différentes longueurs, jusques et y compris la longueur
de 4 metres; un étalon prototype de 4 métres 15 Centimetres de longueur, gradué de
mètre en mètre, avec un étalon témoin de 4 mètres, identique à la règle prototype,
quant à la forme et à la substance; enfin tous les accessoires indispensables.

La construction des microscopes & micrométre de l’appareil a été réservée à
MM. Brunner freres.

L’appareil va étre monté dans le pavillon de Breteuil, avant la fin de
l’année courante, de sorte que l’étalonnage de la règle géodésique prototype et la
mesure de son coefficient de dilatation pourront être entreprise sans retard et suivies à
partir de l’année 1884 des opérations relatives aux règles géodésiques: étalonnage et
mesure des coefficients de dilatation.

Dans sa séance du 27 Septembre 1882, le Comité international des poids et
mesures à décidé, à l’unanimité moins une voix, que la règle normale géodésique serait
construite en fer forgé.

Bien que nous n’ayons pas à discuter ici cette décision qui est définitive, elle
intéresse trop vivement notre Association pour que votre Rapporteur ne se croie pas
autorisé à exprimer personnellement le regret que l’étalon géodésique international ne soit
pas formé, comme le mètre prototype, de platine iridié.

Sous cette réserve qui m'est toute personnelle, je me hâte de me réjouir, avec
tous les adeptes de la science géodésique, du progrès important qui vient d’être réalisé
dans l'établissement international de Breteuil et qui assure, dans un avenir prochain,
l'unification des triangulations Européennes.

La mesure des bases Suisses, surtout des deux dernières, celles de Weinfelden et
Bellinzona, semble avoir résolu une question intéressante, car elle établit la supériorité
sur le terrain, des appareils monométalliques, sur ceux à règles bimétalliques les plus
perfectionnés.

Vous avez tous été frappés, en entendant les Rapports de nos collègues Suisses,
de la facilité et de la rapidité des opérations sur le terrain, grâce à l’emploi de la

 
