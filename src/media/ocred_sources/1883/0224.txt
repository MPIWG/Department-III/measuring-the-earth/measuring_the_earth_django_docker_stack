 

 

 

maintenant nous supposons deux observateurs avec des instruments identiques, occupes a
déterminer les deux latitudes par l'observation simultanée (a quelques heures ou a
quelques jours près) des mêmes étoiles, la différence de ces latitudes sera évidemment
indépendante des déclinaisons des étoiles observées. Et si les deux instruments sont deux
instruments de passage bien solides et bien symétriques, controlés en azimut par des
mires, employés dans le premier vertical à la manière de W. Séruve, on pourra éviter,
non seulement les erreurs de réfraction et l'effet de leurs anomalies, en observant pendant
une année entière; mais aussi les erreurs des divisions des cercles, des vis micrométriques,
et des flexions de la lunette, et enfin aussi ceux de la flexion de l’axe, et les irrégularités
des tourillons, pouvu que leur construction permette non seulement de retourner avec
facilité et sûreté la lunette, mais aussi d’alterner la position des coussinets eux
mêmes. En observant un nombre suffisant d'étoiles près du zénith, on pourra dé-
terminer la petite différence des deux latitudes avec beaucoup de précision, et
cette précision pourra encore être augmentée considérablement en supprimant les
erreurs personnelles par l’öchange des observateurs et des instruments, exactement
comme on le fait pour les différences de longitude. Il y a plus: si les mouvements
propres des étoiles employées sont assez exactement connus, on pourra même employer
un seul observateur pour les deux stations, en le faisant observer successivement dans
l’une et dans l’autre, pourvu que l'intervalle n’exeède pas une année. Avec une bonne
organisation du systéme d'observation, il est certain que la différence des latitudes pourra
être obtenue avec une précision tout-à-fait comparable a celle qu’on peut obtenir dans
la détermination de la constante de l’aberration par des observations de la même espèce,
c'est à dire à quelques centiémes de seconde près. (C’est une précision peut-être dix fois
plus grande que celle d’une latitude absolue. En répétant ces observations une seconde
fois on pourra constater, au bout de 30 ou 40 ans, des variations qui exigeraient plusieurs
siècles pour être reconnues à l’aide des méthodes ordinaires *). Lorsque ces variations de
la différence des latitudes seront bien constatées pour plusieurs couples d’observatoires, il
n’y aura pas la moindre difficulté à en déduire la solution du problème proposé sur
Vinvariabilité des latitudes.

Tel est, en géneral, le principe des opérations proposées par M. Fergola: les
détails ultérieurs pourront être étudiés par les personnes qui seront chargées de mettre
ce projet en exécution. Il ne serait peut être pas nécessaire de faire ces observations
exclusivement dans les observatoires permahents: on pourrait choisir d’autres couples
de stations placées à peu près sur le même parallèle, ou exactement sur le même parallèle
si l’on veut, à des intervalles en longitude de trois à douze heures. Cependant la con-
sidération exposée par Mr. Fergola à ce sujet mérite d’être remarquée: c’est à dire, que
les observatoires étant des institutions durables, on aurait plus de chance, en les choisissant,

*) Cela suppose naturellement, que les conditions moyennes de la localité, autant qu'elles
influencent les anomalies des réfractions, restent les mêmes pendant tout l'intervalle.

 

 

 

 

 

 
