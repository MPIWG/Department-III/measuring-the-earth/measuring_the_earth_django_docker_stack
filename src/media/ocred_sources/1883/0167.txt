 

£
Ë
Ë
Ë
Ë
È
N

een

ERAT CTP IN RET CT

D

 

Ë
i
E
k
à
Ê

153

numérotage des longitudes à la plus grande partie des cartes, et forcer les navigateurs
et la géographie, d'augmenter de 180° les longitudes auxquelles ils sont habitués, unique-
ment pour pouvoir appeler le méridien dont le jour civil sert d'origine aux dates, égale-
ment le méridien initial des longitudes; et en apparence seulement, car nous avons montré
qu’en réalité le premier méridien doit être déterminé par l'Observatoire de Greenwich.

Nous terminons notre rapport en proposant à l’Assemblée les résolutions
suivantes :

La septième Conférence générale de l'Association géodésique internationale,
réunie à Rome, à laquelle ont pris part des représentants de la Grande Bretagne, ainsi
que les directeurs des principales éphémérides astronomiques et nautiques et un délégué
du Coast and geodetic Survey des Etats-Unis, après avoir délibéré sur l’unification des
longitudes par l’adoption d’un méridien initial unique, et sur l'unification des heures
par l’adoption d’une heure universelle, à pris les résolutions suivantes :

I. L’unification des longitudes et des heures est désirable autant dans l'intérêt
des sciences que dans celui de la navigation, du commerce et des communications inter-
nationales; l'utilité scientifique et pratique de cette réforme dépasse de beaucoup les
sacrifices en travail et en accommodation qu’elle entraïînerait pour la minorité des nations
civilisées. Elle doit donc être recommandée aux Gouvernements de tous les Etats
intéressés, pour être organisée et consacrée par une Convention internationale, afin que
désormais, un seul et même système de longitudes soit employé dans toutes les éphémé-
rides astronomiques et nautiques, dans tous les instituts et bureaux géodésiques et topo-
eraphiques, ainsi que dans les cartes géographiques et hydrographiques.

II. La Conférence propose aux Gouvernements de choisir pour méridien initial
celui de Greenwich, défini par le milieu des piliers de l'instrument méridien de l’obser-
vatoire de Greenwich, parce que ce méridien remplit, comme point de départ des longi-
tudes, toutes les conditions voulues par la science et que, étant déjà actuellement le
plus répandu de tous, il offre le plus de chances d’être accepté généralement.

III. Il convient de compter les longitudes à partir du méridien de Greenwich
dans la seule direction de l'Ouest à l’Est, de 0° & 360°, ou de O0 à 24; les méridiens
sur les cartes, et les longitudes dans les registres devraient être désignés partout en
heures et minutes de temps, en laissant la faculté d’ajouter l'indication en degrés cor-
respondants.

IV. La conférence reconnait pour certains besoins scientifiques et pour le ser-
vice interieur des grandes administrations des voies de communication, telles que chemins
de fer, lignes de bäteaux & vapeur, telegraphes et postes, Vutilité d’adopter une heure
universelle, à côté des heures locales ou nationales qui continueront nécessairement à
être employées dans la vie civile.

V. La Conférence recommande comme point de départ de l’heure universelle
et des dates cosmopolites, le Midi moyen de Greenwich, qui coïncide avec l'instant de
minuit, ou avec le commencement du jour civil sous le méridien situé à 12* ou à 180°

de Greenwich.
20

  

  
