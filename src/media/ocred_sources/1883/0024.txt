 

 

 

10

Die Gefühle, welche wir bei unserer Vereinigung in der ewigen Stadt empfinden,
sind so mächtig und von den gebildeten Menschen so allgemein getheilt, dass wir uns
enthalten können, denselben hier weiter Ausdruck zu geben; doch jedenfalls verbinden
sich mit denselben die Gefühle der Bewunderung und des Dankes für die Italiener,
welche jeder Zeit die Geodäsie und die dieselbe begründenden Wissenschaften gepflegt
haben. Es würde zu weit führen, wollte ich alle um diese Wissenszweige hochverdienten
Männer hier anführen; aber wie kann man an diesem Gegenstande vorbeigehen, ohne den
lorbeerumkränzten Namen Galilei’s oder in besonderer Rücksicht auf unsere Wissen-
schaft die Namen Manfredi, Beccaria, Boscowich, Inghirami, Carlini, Plana, Fergola,
Oriani und Secchi zu nennen.

Italien gehörte seit Gründung unserer Vereinigung stets zu jenen Nationen,
welche den hauptsächlichsten Beitrag zu unserem gemeinsamen Werke geliefert haben und
da sich der Begründer unseres Unternehmens nicht in unserer Mitte befindet, so ge-
statten Ew. Excellenz mir, als seinem Mitarbeiter, der Regierung Sr. Majestät des Königs
von Italien den Dank für ihre mächtige und werthvolle Unterstützung zum Ausdrucke
zu bringen,“

Diese Ansprache des General Ibanez erfreute sich des ungetheilten Beifalles
der Versammlung.

General Ibanez macht im Namen der permanenten Commission, dem $. 1 der
Geschäftsordnung entsprechend, für das Präsidium der allgemeinen Conferenz die folgenden
Vorschläge:

Se. Excellenz Generallieut. Baeyer als Ehrenpräsident.
Oberst Ferrero als Präsident.

Die Herren Faye und v. Bauernfeind als Vicepräsidenten.
Die Herren Horsch und v. Oppolzer als Schriftführer.

Die Versammlung stimmt einhellig diesen Vorschlägen bei und General Ibanez
tritt seinen Vorsitz an Oberst Ferrero ab, der zunächst für die auf ihn gefallene ehren-
volle Wahl dankt und den Schriftführern das Wort zur Verlesung der bisher: üblichen
Geschäftsordnung (vergl. pag. 6) in den beiden Verhandlungssprachen ertheilt; diese
wird von der Versammlung einstimmig angenommen.

Der Präsident ertheilt ferner den Schriftführern das Wort zur Verlesung des
Berichtes der permanenten Commission für das Jahr 1883 in deutscher und französischer
Sprache. Der deutsche Bericht lautet:

Bericht der permanenten Commission für das Jahr 1883.

In der am 25. September 1882 abgehaltenen letzten Sitzung der permanenten
Commission wurde vorbehaltlich der Einwilligung einer hohen italienischen Regierung
der Beschluss gefasst, für das Jahr 1883 als Versammlungsort Rom zu wählen. General
Baulina, der damals noch italienischer Commissar und Mitglied der permanenten
Commission war, hatte es übernommen, in dieser Richtung die nöthigen Massnahmen
einzuleiten und erfreute uns nach kurzer Zeit mit der Mittheilung, dass eine hohe

 

 
