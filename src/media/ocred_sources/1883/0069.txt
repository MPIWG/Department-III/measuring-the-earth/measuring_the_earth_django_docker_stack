 

 

55

Vierte Sitzung

der siebenten allgemeinen Conferenz.

Sn Rom am 18. Oktober 1888.
Präsident: Ferrero.

Schriftführer: Hirsch, v. Oppolzer.

Eröffnung der Sitzung um 2" 45™ Nachmittags.

Anwesende Commissare: v. Bakhuyzen, Barozzi, Darraquer, Bassot, v. Bauernfeind,
Betocchi, Christie, Clarke, Cutts, Faye, Fearnley, Fergola, Ferrero, Fischer, Förster,
v. Forsch, Hartl, v. Helmholtz, Henneqwin, Hirsch, Ibanez, v. Kalmar, Lorenzoni, Léwy,
Magnaghi, Mayo, Nell, Oberholizer, v. Oppolzer, Perrier, Pujagon, Respighi, Riimker,
Schiaparelli, Schiavoni, Schols, de Stefanis, Villarceau.

Eingeladene: d Atri, Barilari, Battaglini, Blaserna, Cantom, Cerruti, Cremona,
Dini, Galitzine, Garbolino, Govi, Lazagna, di Legge, Millosevich, Pisati, Pucci, Rosalba,
de Rossi, Sella, Stromei, Tacchim, de Vita.

Hirsch liest zunächst ein von General Baeyer eingelangtes Telegramm vor, in
welchem er für die in der ersten Sitzung erfolgte Wahl zum Ehrenpräsidenten dankt;
die Schriftführer verlesen dann die Protokolle der zweiten und dritten Sitzung; dieselben
werden, nachdem berichtigende Bemerkungen der Herren Hennequin, Betocchi, de Rossi,
Faye, Perrier und Barraquer Aufnahme gefunden haben, von der Versammlung genehmigt.

Der Präsident ertheilt nunmehr das Wort dem Commissar für die Niederlande
van de Sande-Bakhuyzen zur Abstattung des Jahresberichtes. (Vergl. Generalbericht für
1883, Niederland.) Hierauf fragt der Präsident den zweiten anwesenden Commissar für
die Niederlande, Schols, ob er etwas dem vorangehenden Berichte binzuuuitete habe;
da derselbe verneint, eröffnet er die Discussion über denselben.

Faye fragt um nähere Details über die bereits in der zweiten Sitzung von
v. Bakhuyzen erwähnten alten Fixirungssteine für das Meeresniveau.

v. Bakhuyzen bemerkt, dass man seit zwei Jahrhunderten, um die grossen das
Land sichernden Schleusen rechtzeitig zu schliessen, von entsprechend fixirten Steinen
den Stand des Meeresniveau in fortlaufender Weise regelmässig abgelesen und in den
Registern aufbewahrt habe; daraus lässt sich jetzt mit grosser Sicherheit das mittlere
Hochwasser des Meeres für die damalige Zeit ableiten. Es gab deren 8 Steine, von
denen sich bis zur Gegenwart 5 erhalten haben; da diese letzteren bis auf 8 Millimeter
genau dasselbe mittlere Hochwasser ergeben, so lässt sich daraus auf die Unveränder-
lichkeit dieser Marken und des Amsterdamer Pegels schliessen.

Hierauf erhält Herr Fearnley das Wort zur Berichterstattung für Norwegen.
(Vergl. Generalbericht für 1883, Norwegen.)

 
