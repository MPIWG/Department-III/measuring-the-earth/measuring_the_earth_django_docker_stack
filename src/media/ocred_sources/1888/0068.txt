 

|
|
|
|

16
Andonowits, de Belgrade, par lesquels ces deux Messieurs döclarent qu’il ne leur est pas
possible de prendre part ä la session de celte annee.
Le Secretuire lit ensuite le procös-verbal de la premiere seance el rösume celui-ci
en francais. Sa r&daetion est adopt6e sans observalions.
M. le President donne la parole a M. Ilelmert pour la lecture en francais du Rappor!
sur Paetivitö du Bureau central, dont le texte allemand a ete commanique dans la premiere

seance.
M. Helmert s’exprime dans les termes sulvanis:

La Commission Permanente de notre Associalion, r&eunie l’annde derniere A Nice, a
bien voulu sanetionner le programme suivant des travaux du Bureau central, que je lui avais
soumis:

1. Etude de la distribution g&ographique des points astronomiques, en vue de pre-
parer des propositions pour les d&terminalions astronomiques ultörieures, d’accord avec les
deleguös des Etats de l’Association.

9. Tableau des declinaisons d’ötoiles, rösultant des observations de passage au pre-
mier vertical, employdes aux determinations de latitudes.

3. Achevement de la bibliographie geodesique.

Quant au ne 1, il sera traite dans un rapport special figurant comme Annexe I, jointe
a celui-ci.

Quant au second point, j’ai prie M. le D' Galle d’etablir, dans le courant de cet Ele,
plusieurs tableaux des declinaisons determindes. La plupart des CGommissions geodesiques
n’ayant pas röpondu A la cireulaire envoyee par M. Bakhuyzen, M. Galle a dü faire une
revue minulieuse des publications ayant trait A ce sujet. Toutefois, les donnees sur les
determinations de latitudes, contenues dans les rapports speciaux ins&res dans les Gomptes-

rendus des Conferences geodösiques lui ont rendu de grands services.

On a observe la plupart des £toiles, environ deux centis, seulement dans une sta-

tion,
0 etoiles dans 2 stalıons
6 » 3)
10 > A
24 » De ou plus.

(Juoique Vensemble des &toiles observees dans les 82 stalions, au moyen des pas-
sages au premier verlical, dlöpasse le nombre de trois cents, on ne pourra en deduire qu’un
nombre de deelinaisons beaucoup plus restreint, si l!’on exclut toutes les &toiles qui n’ont ät&
observees que dans une ou deux stalions. En outre, on ne pourra pas caleuler assez exacte-
ment la deelinaison pour un liers environ des 60 etoiles observees dans Lrois slalions ou
plus, parce que les intervalles des epoques d’observalion ne sont pas assez grands pour en
deduire avec sürelö le mouvement propre de ces &toiles. i

 

 

mei

 

 
