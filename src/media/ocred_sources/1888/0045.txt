|

TTERTEEERTET

'ei0,

A,
ber,

mel

ssioß
Jliche
hiel.

Frat

en
nlulE

)

po
gone

kehrt

 

 

nen TRETEN THREE

 

39

Nach einigen Worten zur Einleitung und Erklärung, verliest der Secretär folgenden
Antrag!

« Die Permanente Kommission, in Anbetracht dass die mareographischen Beobach-
tungen, sowie die Präcisions-Nivellements in den meisten Ländern sehr weit vorgeschrilten
sind, ist der Ansicht dass der Augenblick gekommen sei, sich ernstlich mit der Wahl des

dem gesammten Europa gemeinschaftlichen Höhen-Nullpunktes zu beschäftigen.
o oO oO

« Ein solcher internationaler Nullpunkt würde offenbar ein wesentlicher Fortschritt

sein für alle diejenigen Länder, welche ihr hypsometrisches System bisher noch nicht auf

einen genügend gesicherten Ausgangs-Punkt gegründet haben. Aber sogar für diejenigen
Staaten, welche bereits einen nationalen Höhen-Nullpunkt mit der nöthigen Sorgfalt errichtet
haben, würde der dem Continent gemeinsame Nullpunkt die beste Grundlage liefern für die
numerischen Höhen-Relationen zwischen ihren und den übrigen Ländern; diese Relationen
sind nicht nur wissenschaftlich nothwendig, sondern auch für die Praxis der Ingenieure und
ihre grossen Unternehmungen, wie Eisenbahnen, Kanäle, ete., unentbehrlich.

« Demgemäss glaubt die Permanente Kommission schon jetzt die Diskussion über die
Wahl des vortheilhaftesten Nullpunktes eröffnen zu sollen, damit es möglich werde, eine
Lösung dieser schwierigen Aufgabe der nächsten General-Gonferenz vorzulegen.

« Aus zugleich wissenschaftlichen und praktischen Gründen, würde das Bureau der
Permanenten Kommission die Wahl eines Küstenpunktes der Nord-See, welcher keinem der

‚Grossstaaten angehört, vorziehen. »

Der Herr Präsident stellt diesen Gegenstand zur Besprechung und ertheilt Herrn
Lallemand das Wort.

Herr Lallemand drückt sich folgendermassen aus :

«Obwohl er die sehr berechtigten Gründe, welche dem Herrn Sekretär seinen
Vorschlag eingegeben haben, vollständig anerkennt, so scheint dem Herrn Lallemand diese
Frage noch nicht völlig reif zu sein. Das mittlere Meeresniveau ist in der That erst für einige
wenig zahlreiche Punkte genau genug bekannt. Und diese Punkte sind noch nicht und wer-

‚den wahrscheinlich auch nächstes Jahr noch nicht sämmtlich durch Präeisions-Nivellements

verbunden sein. Ausserdem aber können die Boden-Bewegungen, wie solche jüngstens in
Frankreich erkannt worden sind, und die Berücksichtigung der aus den verschiedenen Brei-
ten folgenden Schwere-Gorrektionen bei den llöhen-Zahlen in die Anschlüsse der verschie-
denen Höhen-Netze Widersprüche einführen, welche an Bedeutung den jetzt bekannten
Unterschieden zwischen den Niveau’s der verschiedenen Meere nicht nachstehen.

«Würde es unter solchen Verhältnissen nicht richtiger sein, die jetzt in der Arbeit
begriffenen Bestimmungen noch einige Jahre fortzusetzen? Uebrigens ist die hier zur
Verhandlung stehende Frage weder so dringend, noch von so grossem praktischem Nutzen,
wie die Wahl einer neuen Maasseinheit, des Meters, zum Beispiel. In der That geht man von

einem Höhen-System zu einem andern sehr leicht durch einfache Addition einer Gonstanten

über; während der Wechsel einer Maass-Einheit, wie zum Beispiel der Toise oder der
