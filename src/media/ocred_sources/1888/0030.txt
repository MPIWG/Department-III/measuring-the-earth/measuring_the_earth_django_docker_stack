 

 

 

 

 

 

 

‘
I

34

 

; 4 ar 20)0 1» 1 Far
8. Von dem Druckwerke Verhandlungen der vom 21. bis zum 29, Oktober 1887 auf
der Sternwarte zu Nizza abgehaltenen Gonferenz der Permanenien Commission den Interna
E . . . . Ye, Be s : = 2
tionalen Erdmessung, redigirt vom ständigen Sekretär, A. Hirsch, etc., w urden 750 Exem
plare gedruckt. Davon sind im Oktober und November 1888 versandt worden:
73 Exemplare an die Regierungen der Staaten, welche der Internationalen Erdmessung

angehören, sowie

365 Exemplare an die Delesirten, Behörden, Institute, Gelehrten, Gesellschaften ete., nach

Maassgabe der Versandtliste. (Hierbei sind 96 bei der Salzburger Versammlung ver-
theilte Exemplare mitgerechnet.)

125 Exemplare wurden vom Königlich Preussischen Geodätischen Institute gegen Ver-
oütung der Herstellungskosten erworben und innerhalb Deutschlands zur Verthei-
lung gebracht. Weitere

150 Exemplare sind an die Buchhandlung von G. Reimer in Berlin in Commission ge-
eeben und

38 Exemplare in Bestand verblieben.
9. Von dem Bericht über Lothabweichungen, welcher als Annex zu dem unter 8 ge-
nannten Werke erschien, sind 37 Sonderabzüge zur Versendung gelangt.

gezeichnet: MELMERT.

Der Präsident ersucht den Herrn Direktor Helmert, die übrigen Special-Berichte und
Annexe in der nächsten Sitzung verlesen zu wollen, da die Zeit schon ziemlich vorgeschritten
ist. Der Präsident schlägt vor die Sitzungen mit Unterbrechung je eines Tages abzuhalten, um
den Commissionen für ihre Berathungen und dem Sekretär für die Abfassung der Protokolle
die nöthige Zeit zu lassen, und setzt daher die nächste Sitzung auf Mitwoch um 2 Uhr fest.

Herr Prof. Tinter theilt der Versammlung mit, dass S. E. der Herr Statthalter die
Herren der Versammlung am Donnerstag Abends 8 Uhr bei sich zu empfangen wünscht, und
dass die Stadtverwaltung auf Morgen Dienstag, für die Mitglieder der Gonferenz und ihre
Familien, eine Excursion auf den Gaisberg anbietet.

Die Sitzung wird gegen 4 Uhr geschlossen.

i
|
g

EEE

EEE

 

 

|
j
|
|
|
|

 

 
