 

 

30

 

V. DES ERREURS

 

L’erreur moyenne d’une deterinination isolde de la duree d’oscillation est egale A
PLB + 0,0000033

PLH = 0,0000050

Ces deux erreurs moyennes sont bien, comme l’indique la theorie, dans le rapport
inverse dehäaW.

Chaque valeur de T et. de T’ qui entre dans le calcul de r provient en moyenne de
huit determinations.

L’erreur moyenne de T sera done:

+ 0.000001
Gelle de T sera :
+ 0.0000017

Mais on a, par une formule bien connue, pour l’erreur moyenne de 7

 

 

1 3 -
GE an ? (AT 12 '2 NA /
ae + h AT’? —= +0, 000004

et, par suite, pour l’erreur probable de T:
+ 0,000003

soit 97000 ryiron de la durede theorique observ£ee.

L’erreur probable qui en resulte dans une mesure d’intensite relative de la pesan-

9

,
teur, sur la valeur du rapport J2 sera:
9ı

 

/ 3 2
{ ', (So: N | |
|, (>) 4 — + 0, 0000012
9 \/ a

|
soit 30000 de la valeur de ce rapport.

Ces erreurs probables, notablement plus fortes, mais assurdment plus vraies que
les erreurs probables donnees par le pendule invariable, nous paraissent representer assez

exactement l’erreur ä craindre dans la mesure experimentale de la durde d’oscillation theori-

 

 

 

 
