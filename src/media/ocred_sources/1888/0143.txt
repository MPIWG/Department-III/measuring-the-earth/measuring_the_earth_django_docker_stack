 

Sagen
»cken

 

 

 

Annex N: IVe.

BERICHT

des Professor Dr. Wilhelm Tinter über die im Jahre 1558 ausgeführten
Rechnunesarbeiten. |

Stationen Krakau und Jauerling.

Die definitiven Rechnungen zur Ermittlung der Polhöhe und des Azimuthes auf den
eenannten beiden Stationen sind nunmehr zu Ende geführt. |

A. Station Krakau.

1. Die Bestimmung der Polhöhe des Aufstellungspunktes des Universal-Instrumentes
aus gemessenen Zenithdistanzen der nachstehenden Sterne hat folgende wegen der Biegung des
Fernrohres schon verbesserte Werte ergeben :

Polaris I’ ne 0% o al 32a pB -30

» 2. Reihen) U..C. 592,118 87
% Ursae minoris ..592.020 34
B Gepher\ 92,109 34
a (Gioronae 82,124 98
# Bootis 29,701 11
# Ophiuchi 59,050 94
“ Orionis 992,418 9
“ (‚anis minoris or = SU ar Da IN) 40

Hieraus folet : Polhöhe des Aufstellungspunktes des Universal-Instrumentes aus 962
Doppelbeobachtungen von Zenithdistanzen nördlicher und südlicher Sterne

°© !

050 3 52,346 + 0,064

9%. Aus den beobachteten Sterndurchgängen im I. Vertical sind für die Polhöhe des
Aufstellungspunktes des Passage-Instrumentes folgende Werte erhalten worden :

 
