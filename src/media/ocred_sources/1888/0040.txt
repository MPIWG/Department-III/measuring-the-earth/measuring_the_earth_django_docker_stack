 

|

34
1 “ a 28 oe ° a » E r, 5
nisse der Vorarbeiten ist in der nächsten Session der Permanenten Kommission Bericht zu

erstatten.
(gezeichnet)  FRSTER.

Die in diesem Berichte enthaltenen Anträge werden vom Präsidenten zur Abstim-
mung gebracht und von der Permanenten Kommission einstimmig angenommen.

Der Präsident kehrt hierauf zu der Mittheilung der verschiedenen Landesberichte
zurück und ertheilt zuerst Hrn. General Ferrero das Wort zur Berichterstattung über Italien.

Siehe den italienischen Bericht mit zwei Karten in der Beilage II.

Der Präsident spricht Herrn Ferrero den Dank aus und ertheilt hierauf das Wort den
österreichischen Delegirten und in erster Linie Herrn von Kalmär, der hauptsächlich über
die im letzten Jahre ausgeführten Nivellements berichtet.

Alsdann verliest in Abwesenheit des Herrn von Sterneck Major Hart! den Bericht
desselben über astronomische Ortsbestimmungen ; im Sommer 1888 wurden Polhöhe und
Azimut aufzwei Punkten in Tyrol bestimint.

Herr Major Hartl giebt alsdann eine kurze Uebersicht über die von ıhım geleiteten
trigonometrischen Arbeiten I. Ordnung, namentlich in Siebenbürgen. Darauf kommt der Be-
richt des Majors von Sterneck über die von ihm im Jahre 1888 ausgeführten Schwerebe-
stimmungen zur Verlesung.

Alle diese Berichle werden ın den Beilagen IV erscheinen.

Herr Professor Helmert wünscht aus Anlass der soeben gehörten Mittheilungen der
österreichischen Kollegen einen Antrag zu stellen, der folgendermaassen lautet:

« Die Permanente Commission, indem sie die Wichtigkeit der Untersuchungen über
die Veränderlichkeit der Schwere anerkennt, hat den Wunsch, dass die Beobachtungen des
Herrn von Sterneck in den Alpen durch die Linien

Innsbruck-Kufstein
Botzen-Ala
ergänzt werden. »

Herr Prof. Hirsch stimmt diesem Vorschlage um so bereitwilliger zu, als dann drei
Schwere-Linien über die Alpenkette hergestellt würden, da, was Herrn Helmert vielleicht un-
bekannt gewesen, bereils vor ungefähr 20 Jahren Herr Plantamour die Schwere in Genf und
mehreren andern Punkten auf der Nordseite der Alpen und alsdann auf dem St. Bernhard
bestimmt hat.

Der Antrag des Herrn Ilelmert wird von der Permanenten Kommission einstimmie
angenommen,

Nachdem der Herr Präsident den Officieren des militär-geographischen Instituts für
ihre Berichte gedankt, ersucht er die bei der österreichischen Gradmessungs-Commission

betheiligten Herren, auch ihrerseits Mittheilungen über die von derselben geleiteten Arbeiten
zu machen.

 

 

|

 

 
