 

4

nn  —

G. — TRIGONOMETRISCHE ARBEITEN

Die Beobachtungen im trigonometrischen Netze 1. Ordnung des ehemaligen Gross-
fürstenthumes Siebenbürgen wurden auch heuer fortgesetzt und zwar auf den Punkten :
Varatyk, Mugura-Korbest, Vlegyasza, Pless, Bichär, Drocsa, Dimpu-Cornu, Muntele mare,
Szekelykö, Csolt, Fontinilor, Zigla-Moruc, Dealu-Puszty und Dumbalives.

Einzelne dieser Stationen sind vollständig neu gemessen worden, andere wurden
bloss ergänzt, auf einigen ist erst ein Theil des @yrus horizontis beobachtet.

Neu gebaut wurden die Pyramiden : auf Dumbalives, Zigla-Moruc, Deulu-Kesztey,
Ejszakhegy, Babgyi, Pticlo, Dealu-Gymi und Bihir, ausgebessert vier Pyramiden.

In Bosnien und der Herzegowina wurden die früher oberirdisch noch nicht markırl
ewesenen letzten 6 Gradmessungspunkte stabilisirt, so dass von den 31 in diesen Ländern
gelegenen Punkten 1. Ordnung 30 Punkte dauernd markirt sind. Ein Punkt ist nicht auffind-

yar, weil daselbst ein Fort erbaut wurile.

OÖ
oO

ES, Im. p-
KK. Major,

 

 

 

 
