 

MR

 

 

ERIETULSETLETTEETIEELTE
„

ee

EEE

 

QUATRIEME SEANCE

23 Septembre 1888.

La seance est ouverle ä 1 heure.

r r

Presidence de M. le general Jbanez.
Sont presents :

l. Les membres de la Commission permanente: MM. Bakhuyzen, Faye, Ferrero,
Feerster, Helmert, Hirsch, von Kalmar, Nagel.

ll. Les delegues : MM. d’Avila, Bassot, Bouquet de la Grye, Gapitaneanu, Diaz
Covarrubias, Derrecagaiz, Hartl, Karlinski, Lallemand, Rümker, Schols, Schreiber, Tinter,
Tisserand !.

Il. Les invites: MM. Bischoffsheim, Perrotin, baron Hwrdtl.

Le proces-verbal de la derniere seance est lu en langue allemande et adopte sans
observation.

M. le President demande si, en raison du peu de temps dont dispose la Commission
pour sa derniere seance, on ne pourrait pas se dispenser, ä titre d’exception, du resume en
francais fait au cours de la sdance par le Secretaire. Cette proposilion est approuvee ä
Punanimite.

Le Seerelaire donne conrmaissance de la depeche telegraphique que le Bureau a regue
de Madame v. Oppolzer et dont voici la teneur :

« Gmunden, 22 septembre.

« Profond&ment touchee du tel&gramme que la Commission permanente a bien voulu

m’adresser en souvenir de mon mari, jexprime ma reconnaissance ä M. le general Ibanez
et le prie d’en faire part ä ’honorable assemblee.
« (Signe) Celestine v. ÖPPOLZER. »

1

1M. Weiss s’est fait excuser par une lettre, d’apres laquelle il a et& oblige de retourner a Vienne.
