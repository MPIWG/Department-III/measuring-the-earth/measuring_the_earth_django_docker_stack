 

 

2

 

A

Turin et Mondovi, et qui traverse l’Allemagne depuis le Feldberg dans la Foröt-Noire jusqu a
Pile d’Helgoland.

Allemagne. Il existe dans ce pays des deviations considerables, m&me N les rEgIONS
de plaine. La recherche continue de ces deviations promet de donner des eolaireisseitents
importants sur la formation o6ologique des basses regions de "Allemagne. Le el aslrono-
mique a &te dejä, dans certaines parties, condense au point qwil a &te possible d’y construlre
les formes du geoide. (Comp. la carte ei-jointe des dövialions en latitude dans /’Europe
centrale.) I existe döja en Allemagne 16 points de Laplace (sans la Schneekoppe); on pro-
jette l’ötablissement de 3 nouveaux points (non compris Übagsberg, ainsi que Tarnowılz en
Silösie, qui manque sur la carte.

Dunemark. Malgr& le caractöre prononee de plaine de cette contree et malgre la
faible profondenr des mers sur ses cötes, il existe des deviations remarquables, surtout entre
deux stations d’azimut dans l’ile de Seeland. Il reste & dösirer: 1° qu’on mesure un azimul ä
Copenhague, afın de transformer en un point de Laplace cette station, dont on connall
jusqu’ä present dejäa la longitude; 90 (P’intercaler quelques stations en latitude entre Skagen
et Sophienhoi.

Suede et Norvöge. Les mesures des reseaux «de Lriangles, ainsi que des points astro-
nomiques, au moins en Suede, progressent dans ces pays d’une maniere rejouissanle. Il y
existe 4 points de Laplace : Christiania, Bergen, Stockholm et Lund.

En gendral, il suffit d’un coup d’eeil jet@ sur les carles menlionndes dans ce rapporl,
pour reconnaitre que le nombre des devialions connues n’est pas encore en rapport avec le
nombre des stations astronomiques. Cela tient au manque de donndes necessaires pour le
ealeul döfinitif d’un systeme econlinn des deviations de la verticale. Comme toutefois les
röseaux conlinus de triangles sont le point de d&part de toules ces recherches, Jai engage
M. le Dr Westphal A faire, pour P’usage du Bureau central, un rapport sur Petat actuel des
triangulations. J’ai ’honneur de prösenter & la Conference la carte synoplique qui accom-
pagne ce rapport. Les points de Laplace s’y trouvent consignes.

siene : HELMERT:

Appendice Il au Rapport du Bureau central.

Resume des expeditions de publications geodesiques faites par le Bureau central.
Depuis la Conference de Nice, en 1887, le Bureau central a recu de MM. les delegues
de l’Associalion g&odesique inlernationale, pour &tre distribuses, les publications suivantes :

4 a / mn Ars a Anı > S aplı . ‘ Marc > =
en a le general Schreiber, & Berlin: La Triangulation du royaume de Prusse.
er 5 ae r B 2r Te Sal x N x x IT\ Aya “ br .
riangles de 1° ordre, IV° partie; la Chaine de P’Elbe; Ire section. 74 exeimplaires.

 

 

 

 

 
