 

5]

I

|

— m DD SD un u u u

z

 

 

IE

 

 

61

ze + 0149004 (N) 3) Gew. 1 % = — 0,064 — u + (1) Gew. 2
ee leder an = 0MW —u+() » 2
= +0205+() — (4) » A .„_ ih ou 0.
70166 Den Kl HB) N
2, oe ut RI A) >» A
N ed er Neened

Die Gewichte der einzelnen Bestimmungen sind für jede Linie gleich 1 angenom-
men; demgemäss erhielten die Gleichungen 7, 8 und 9 das Gewicht 2, damit ihre Differen-
zen, welche beobachteten Linien entsprechen, das Gewicht 1 haben. Der Ansatz dieser Feh-
lergleichungen entspricht dem Umstande, dass die Längenbestimmungen Berlin-Breslau und
Breslau-Königsberg wesentlich auf denselben Zeitbestimmungen in Breslau beruhen, sowie
der Erfahrung, dass der Fehler der telegraphischen Uebertragung der Zeit, welche aller-
dings nur auf den genannten beiden Linien und nicht direkt zwischen Berlin und Königsberg
erfolgte, als gering gegenüber der Gesammtheit der Fehler der Zeitbestimmungen betrachtet
werden kann, % ist eine Unbekannte.

Die Normalgleichungen sind die nachstehenden :

 

 

 

 

ENDWERTE
6u—2() >55. 9 oa.
—2u ” 61)— 2) — (3) — (4) ‚= 027.)

A) +30) — DB) — (4) zei 0 un

.— (1) @&%)+9360) ..—- 0 114190) 003

.— 4)— (0) . +34) — 0) . = + 0,006 |(4) = — 0,007

— 2u 9) - (+90) -—_ (01-0210 01!
— u - ; . +26) U (6) = — 0,075
© © ı. 7 |Mm= + 0,484
Hierin musste (wie von vornherein klar) eine der Unbekannten ) . 09

gleich null angenommen werden, da die Summe der Gleichungen sich identisch null er-

giebt.
Der mittlere Fehler für's Gewicht 1 folgt gleich

_, (W209 _ 0'099
a,

Berücksichtigt man auch noch den Schlussfehler des Längendreiecks Königsberg-
Memel-Goldap von 1887, so wird

„_, OS
m = \/ r ı

S
— 2.0034,
