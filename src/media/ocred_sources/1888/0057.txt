 

 

 

ort

 

« En donnant A ces sympathies une nouvelle expression au nom du Haut Gouver-
nement imp£rial, je fais les veux les plus ardents pour que votre Confärence actuelle profite
A lavancement de la grande entreprise que vous reprösentez et pour que, tres honores
Messieurs, vous vous senliez heureux dans notre chere patrie et gardiez les plus agreables
souvenirs des jours passes au milieu de nous. »

M. le D' Schumacher, bourgmestre de Salzbourg, ajoute ensuite, au nom des auto-
rites et de la population de Salzbourg, quelques paroles &loquentes, dont voici le resume :

« Messieurs,

« Vous avez choisi Salzbourg comme lieu de reunion pour cette annee el vous avez
ainsi ajoute son nom ä la liste glorieuse de ces grandes capitales qui ont eu precedemment
’honneur de recevoir dans leurs murs votre Gonference, qui poursuit un but eminemment
scientifique. Bien que la plupart des localitös que vous avez visit&es nous aient &videmment
depasses de beaucoup dans la splendeur de la reception qu’elles vous ont offerte, jai nean-
moins l’inlime conviclion qu’aucune d’elles n’a pu nous surpasser dans la vive sympathie
avec laquelle la population de notre ville suit vos interessants travaux.

« Veuillez done, Messieurs, apprecier les sentiments de respect dont nous sommes
animes envers volre illustre Assembl&e, plutöt par cette sympathie que par les modestes
signes exterieurs que nous sommes en mesure de lui donner.

« Messieurs! Vous 6tes ieci sur le sol classique de l’antique Juvavum; c'est dans
cet endroit que, il y a deja plus d’un millier d’annees, la civilisation de l’Europe cen-
trale a eu un de ses points de depart. Les sciences et les arts ont toujours lrouve un accueil
hospitalier a Salzbourg et, pour cette raison, nous sommes heureux et fiers de ce que volre
celebre Association tienne aujourd’hui ses assises dans nos murs. En ma qualite de bourg-
mestre de la ville de Salzbourg, j’ai ’honneur de vous saluer et de vous souhailer une cor-
diale bienvenue au nom de la population tout entiere. »

M. le general Ibanez, president de la Conference, exprime aux representanis de
’Empire et de la ville de Salzbourg les remereiements de l’Assemblee pour la receplion aima-
ble avec laquelle ils ont bien voulu l’aceueillir, et il met en m&me temps en relief le grand
merite de P’Autriche et de ses geodesiens pour l’avancement de l’aauvre de l’Association in-
ternalionale. Le discours du President est concu en ces termes :

« Monsieur le Gouverneur, Monsieur le Bourgmestre,

« Au nom de l’Association geodesique internationale pour la mesure de la Terre,
dont nous sommes les delögua6s, jJai ’honneur de vous prösenter ses senliments de vive re-
connaissance pour l’aceueil bienveillant que nous recevons du Gouvernement de S. M. ’Empe-
reur et Roi par l’organe de la premiere autorite de cette province, el des autorites de la ville.
