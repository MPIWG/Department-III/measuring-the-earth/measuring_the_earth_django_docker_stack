 

 

 

 

30
halten als im allgemeinen diejenigen in Breite. In mehreren Fällen ergeben sich auf Entfer-
nungen von der Grösse einer Dreieckseite 1. Ordnung beträchtliche Differenzen, die im
Gebirge auf 22°, in der Ebene bis auf 9” ansteigen. (Vgl. Cap. XXV des Werkes « Ordnance
trigonometrical survey. Principal triangulation. ») Für die Azimutbestimmungen sind keine
Attraktionsberechnungen angestellt.

Es fehlen auch Längenbestimmungen zur Gontrole der Azimutbestimmungen durch
Laplace’sche Gleichungen, mit deren llülfe es dann möglich sein würde, die aus den
Azimuten abgeleiteten Lothabweichungen auf ein System zu reduziren. Der einzige Laplace-
sche Punkt ist Greenwich. Die fünf andern Punkte, deren Längenunterschied mit letzterem
Orte bestimmt ist, fallen nicht mit Azimutpunkten zusammen. Diese Messungen von Längen-
unterschieden sind durch Transport von Chronometern oder durch elektrische Signale bewirkt,
aber auch im letzteren Falle durchaus älteren Datums.

Die Niederlande. Hier hat man eine Neumessung des trigonometrischen Netzes be-
sonnen. Astronomische Punkte sind zwei vorhanden; einer derselben ist Laplace’scher Punkt.
Ein andrer solcher Punkt, Ubagsberg, gemeinsam für die Niederlande, Belgien und Deutschland,
ist projektirt. Es wäre zu wünschen, dass längs des Meridianes Marseille-Lomme I, der das Land
mitten durchschneidet, einige Breitenstationen angelegt würden, welche zur Ergänzung der
entsprechenden Untersuchungen in Frankreich und Belgien, namentlich im Ilinblick auf den
grossen Lothabweichungsbelrag von 6 in Lommel, dienen könnten.

Belgien. Die Berechnung des trigonometrischen Netzes ist beendet. Dasselbe enthält
zwei Laplace’sche Punkte, sowie einige andere astronomische Stationen. Man hat bereits
begonnen, durch neue astronomische Messungen die durch die älteren Bestimmungen ange-
zeigten Unterschiede und Lothabweichungen zu prüfen und zu ergänzen.

Frankreich. Die Zahl der Laplace’schen Punkte ist bereits auf 12 gestiegen, was dem
trigonometrischen Netze eine gute Gontrole sichert. Ueberrhaupt vervollständigt man das astro-
nomische Netz immer mehr. Zweien dieser Punkte fehlt noch die Azimutmessung; sie ist auf
der Karte als projektirt bezeichnet. Zu wünschen wäre das weitere Studium der nach Ger-
main’s Untersuchungen an der Küste des Mittelmeeres stattfindenden beträchtlichen Loth-
störungen. Germain bestimmte die vier Stationen Nizza, Sankt Raphael, Toulon und Marseille!.

' Vergl. C. R. 1886 G II p. 1100-4103. Seine Ergebnisse sind:

Breite Abw. Länge Abw.
astr, geod. asit. seod.

° u " " © (e " N n

R \ JS Irre 99 EB NER Pe alten) . . ® c
Mont Gros, Nizza gut 413 43.16,9 33,5 — 16,6 451 42,3 43,6 — 1,3
Danitabeaphael... 00.2.0... 2228 57 Isı 194 126 — 16,1 —
Toulon . a ee 2a mod 339 — 1,6 —

Marseille, Neues Observ. 34 De 5.9 3 3943 38. 7
; Observ 0010 247 583 ss Ma 8

Meereshöhe zu Nizza. ch der Kater arm.

‚zu St. Raphael, nach Germain, 32 m.

Die Abweichungswerthe entsprechen nicht genau dem System meiner Karte, aber doch beiläufie
wenn man zu den Längenabweichungen — 5” addirt. Der Unterschied der Lothabweichune in Breite für Nizza.
welcher zwischen obigem Werthe und dem meiner Karte stattfinde dr
Stationen,

t, entspringt einer Lagenverschiedenheit der

 

 
