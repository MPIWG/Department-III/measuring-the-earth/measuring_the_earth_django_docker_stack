 

 

32

EIER re

M. Helmert donne ensuite la lecture detaillde de ce travail en francais. (Voir Appen-
diee I du rapport de M. Helmert, p. 19.)

M. le President accorde la parole & M. le professeur Ferster pour donner connals-
sance des propositions qwiil a le charg6& de faire & "Assembl&e au nom de la Commission
des latitudes, nommee dans la seance pröcödente. Voici ce document:

Rapport de la Commission pour stunde de la variabilite des latitudes.

La Commission, composee de MM. Bakhuyzen, Feerster, Helmert, Tisserand et Weiss,
a nomın& M. Helmert president, et M. Foerster rapporteur.

Elle a commene& par constater que les resolutions de la Conference generale de
Rome (1883), prises sur la proposition de M. Fergola et recommandant a dix observatoires,
distribuds sur la Terre entiere, des observalions correspondantes de latitudes, sont jusqu’äa
present restdes sans rösultat appr£ciable.

Dans cet tat des choses, et en considerant les indications fournies par quelques nou-
velles series d’observations occasionnelles, parmi lesquelles M. Foerster a cit& un travail
röcemment publie, de M. Küstner de l’Observatoire de Berlin, la Commission a ete unanıme
4 recommander ä la Commission permanente les trois resolutions suivanles !

1) Sans vouloir nuire d’aucune facon aux observations correspondantes des latitudes
qui, sur les recommandations de la Conference generale de 1883, ont peut-etre deja ete
eommencees dans quelques Observatoires, la Commission Permanente exprime l’opinion
qu’elle agirait dans P’interet de ’Association geod6esique internationale, en contribuant desor-
mais avee ses propres forces et ressources & l’&tude de la question de la variabilit€ de la po-
sition de l’axe de rotation dans le globe terresire.

9) M. le Directeur du Bureau central ayant declar& que la collaboration de plusieurs
institutions astronomiques et g&odesiques lui est assurce pour quelques ätudes pr&paratoires
concernant cette question, le Bureau central est charge de pröparer l’organisation de deter-
minations correspondantes de latitudes dans quatre stations au moins, distribuees sur la Terre
entiere et notamment d’ötudier la construction la plus favorable des instrumenis et le choix
de la möthode la plus appropriee dont on devrait se servir d’une maniere absolument iden-
tique dans ces observations. Dans ce but, une somme, qui ne devra pas d&passer S000 fr.,
et qui d’apres le rapport du Bureau central est cerlainement disponible, est mise & la dispo-
sition du Bureau de la Commission permanente.

3) Dans la prochaine session, il sera fait & la Commission permanente un rapport sur
l’emploi de ce credit, employ& soit en totalite, soit partiellement, dans l’interet de cette re-
cherche et sur les rösultats des &tudes preparatoires dont le Bureau central a et& charge.

(signe) FÜERSTER.

Les propositions eontenues dans le rapport ei-dessus sont mises aux voix par M. le
President et adoptees A P’unanimite par la Commission permanente.

 

ee sa

3 5 Ro
EEE Ernie

 

 

 
