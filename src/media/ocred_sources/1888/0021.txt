 

 

Obwohl die Anzahl der auf 82 Stationen im Ersten Vertikal beobachteten Sterne
dreihundert übersteigt, wird sich doch daraus nur eine geringe Anzahl Positionsbestimmun-
gen ergeben, wenn man alle diejenigen Sterne ausschliesst, die nur auf einer oder zwei
Stationen beobachtet sind. Denn diese bilden weitaus die Mehrzahl. Es sind nämlich ea. 200
der Sterne nur auf einer Station beobachtet;

S0 Sterne kommen auf je 2 Stationen

96:5» » yeund >

> » ey ) endlich

Yan) » » 5 nd mehr Stahonen wer.

Von den 60 hiernach auf drei oder mehr Stationen beobachteten Sternen sind aber
wegen Unkenntniss der Eigenbewegung, weil die Beobachtungen zum Theil zeitlich nicht
weit genug auseinander liegen, auch noch nahezu ein Drittel nicht scharf reduzirbar.

Kine beispielsweise ausgeglichene Gruppe von 22 Sternen, welche auf 20 Stationen
vom Geodätischen Institut zusammen 92 Mal beobachtet worden waren, gab mit Benutzung
früherer Angaben über Eigenbewegung, als mittleren Fehler der Bestimmung eines Sternes
auf einer Stalion

„

= (25 (semacht >41):

von diesen Sternen sind aber nur 17 auf mehr als 2 Stationen beobachtet.

Die geringe Grösse des vorstehenden mittleren Fehlers würde einige Aussicht auf
Erzielung einer Anzahl brauchbarer Positionsbestimmungen eröffnen, wenn nicht erfahrunes-
mässig mit Recht zu fürchten wäre, dass den Resultaten eines jeden Beobachters und Instrn-
ments noch konstante und systematische Fehler anhaften, welche die erreichte Genauigkeit
beträchtlich schmälern.

Die betreffenden Zusammenstellungen wurden entsprechend den Bestimmungen der
Permanenten Commission Herrn Bakhuyzen zur weiteren Erwägung übergeben.

ad 3 ist zu erwähnen, dass die von Herrn Professor Dr. Börsch bearbeitete geodä-
tische Bibliographie sich unter der Presse befindet.

‘4. Herr Dr. Börsch jun. hat nach den Quellen eine Zusammenstellung der Grunid-
linien behuls deren Verzeichnung in der Dreieckskarte des Herrn General Ferrero nach
Grösse und Azimut angefertigt. Hiernach sind die Grundlinien in die Dreieckskette, welche
den Verhandlungen in Nizza beigegeben ist, verzeichnet. Aus Gründen der Deutlichkeit
musste aber die Grössenangabe schliesslich unterbleiben.

9. Meinen Bericht über die Pendelmessungen der letzten Jahre, den ich in Nizza vor-
gelragen hatte, habe ich noch etwas weiter ausgearbeitet. In dieser Form ist er in den
Nizzaer Verhandlungen erschienen.

’ Dieselbe ist in den Monaten Mai und Juni versandt worden. Zum Gebrauche der Herren Delegirten

sind 50 Exemplare auf einseitig bedrucktem Papier hergestellt worden, welche das Centralbureau auf Wunsch
abgiebt.

  

  
