 

 

II. AUSGLEICHUNG DER VON DER MAIN-NECKAR-BAHN (ESTLICH GELEGENEN
NIVELLEMENTSZÜGE.

 

 

Die schematische Figur gibt einen Ueberblick über die Nivellementszüge. Die Punkte
G und D sind dieselben wie in Fig. 1. Ausserdem bedeutet :

 

m Marke 7] an der Brücke zwischen Babenhausen und Altheim, nordwestliche Ecke.

n » O©an dem offenen Wegdurchlass zwischen m und Babenhausen, nordwestliche
Ecke.

p Eisenbahngrenzstein zwischen Babenhausen und Langstadt.

H Marke U] an der Güterhalle, Oberramstadt, nordöstliche Ecke, Sockel. |

 
