 

j
)

N;
|

 

Nous indiquerons plus tard les resultats de cette dtude complementaire, nolanıment
quand nous aurons alleint Brest, point d’une importance particuli6re a cause de la longue
durde des observalions faites dans ce port sur le niveau de l’Oc6an.

Dans le cas otı ’hypothese de mouvements du sol se trouverait confirmde, il y au-
rail lieu de se demander si les mouvements sont conlinus comme ceux que l’on observe,
par exemple, sur les cötes de Suede, ou s’ils sont seulement periodiques comme ceux cCon-
states, en parliculier, dans la baie de Naples.

Le meilleur moyen de resoudre cette question sera de recommencer alors, apres un
nouvel intervalle d’une dizaine d’anndes au moins, un troisieme nivellement de preeision,
dont les resultats, compares A ceux des deux premiers, permettront de determiner V’allure du
phenomene. .

La Commission du nivellement general a rösolu de realiser, le cas Ech6ant, ce pro-
gramme, en choisissant dans le reseau secondaire un certain nombre de lignes qui seront
ulterieurement nivelees avec les memes möäthodes et avec les memes instruments que les
lignes du reseau fondamental.

II. — MAREGRAPHES ET MEDIMAREMETRES

Le niveau moyen annuel de la Mediterrande, A Marseille, d&duit des resultats four-
nis par le maregraphe totalisateur install& dans ce port depuis le 1er fövrier 1885, a pre-
sente, d’une annde A l’aulre, les varialions suivantes :

 

 

ANNHESIT 1885 1 1886 1887 1888
-—— 0m,061 — 00,06% — 0: 200
Te sr Em el R
Moyenneocmeradle >» . . ... 00.064

 

 

 

 

 

 

‚Nota. — Les cötes ei-dessus sont rapport6 au plan de comparaison du nivellement
Bourdalou6.

Les quatre medimarömetres de Nice, Marseille, Cette et Port-Vendres fonetionnent
depuis un an d’une mani@re absolument satisfaisante.,

Les niveaux moyens de la mer, en ces qualre points, calcules d’apres les donndes
fournies par ces instruments, el vapportds au m&me plan de comparaison, prösentent les re-
lations de hauteur ci-aprös :

!ÖOnze mois seulement.

 

 
