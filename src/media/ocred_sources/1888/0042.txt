 

 

en n : nel 1 ; > Aha cn.
(1600m), die Längendifferenz mit Berlin und Breslau, und die Polhöhe so

der Schneekoppe IBı M
wohl als das Azimut nach drei Methoden bestimmt; wodurch die Sehneekoppe zu einem astro-
nomisch-trigonometrischen Punkte Je Ordnung erhoben wird. — Im Harze sind ‚die Bech-
achtungen von Breite, Azimut und Länge aus nächtlichen Lichtsienalen auf zwei Stationen noch
im Gange. An der Nordseeküste wird die Insel Wangeroog mit dem 13 Kilometer entfernen

2,

Festlande durch trigonometrische Höhenmessung verbunden, um dadurch die schon früheı
auf Wangeroog bezogene Insel Helgoland und ihren Fluthmesser an das Festland anschlies-
sen zu können. Obwohl trigonometrische Höhenmessungen kaum eine zu diesen Zwecken
genügende Genauigkeit gewähren, so glaubt Herr Helmert doch, auf diese Weise interes-
santes Material zu Studien zu gewinnen.

Die Reduction der früheren Messungen ist grösstentheils beendet und die im näch-
sten Winter stattfindenden Publikationen werden alle vor 1887 gewonnenen astronomischen

Beobachtungen erledigen.

Darauf ergreift Herr Ferster das Wort, um mit Bezug auf die von Mylius von der
Reichsanstalt in Berlin verfasste Abhandlung, welche Herr Foerster unter die Mitglieder der
Versammlung vertheilt hat, die Resultate der Studien zusammenzufassen, die in der letzten
Zeit in Berlin ausgeführt worden sind, um den bei den Libellen bemerkten schweren Stö-
rungen abzuhelfen.

Daraus veht hervor, dass das im Aether der Libellen enthaltene Wasser eine chemi-
sche Aktion auf den Ueberschuss von Alkalien ausübt, welcher sich in der Zusammensetzung
der in der heutigen Technik gebräuchlichsten Glasarten vorfindet; diese Wirkung ist es,
welche nach und nach kleine Ausschwitzungen an den innern Flächen der Gläser hervorruft,
und auf diese Weise den Gang der Libellenblasen und die Angaben der Niveaus stört.

Nach der Ansicht des Herrn Mylius könnte diesem Umstande durch Verminderung
der allzureichlichen Alkalien ın den Präcisions-tläsern, und zum Beispiel durch Ersetzung
derselben mittelst Blei abgeholfen werden ; zugleich aber wäre es nützlich, den Aether der
Libellen noch mehr vom Wasser zu befreien oder auch eine ganz andere, geeignetere Flüs-
sigkeit zu wählen.

Die eifrigste Fortsetzung aller dieser Studien ist gesichert und die wissenschaftlichen
Anstalten Berlins werden stets bereit sein, den Gelehrten und Mechanikern anderer Länder
darüber Bericht zu geben.

Bei der bereits vorgerückten Zeit erklärt der Ilerr Präsident die wenigen Berichte
einiger Länder, welche noch ausstehen, auf die nächste Sitzung verschieben zu wollen,
welche am Sonntag um 1 Uhr statt finden wird.

Vor dem Schluss der heutigen Sitzung aber sei noch die beim Beginne derselben an-
gekündigte Wahl des Ortes für die General-Conferenz des nächsten Jahres vorzunehmen. Der
Präsident erinnert daran, dass bereits im vorigen Jahre in Nizza der Gedanke, die nächste
General-Conferenz in Paris abzuhalten, fast allseitige Zustimmung gefunden habe. Daher er-
laube sich das Präsidium, nunmehr Paris definitiv zum Versammlungsort der General-Con-

 

F
1
|
F

 
