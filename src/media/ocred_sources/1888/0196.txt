 

 

die italiänischen Arbeiten (siehe Annex
Inne 2 Kasten. 2. . a.

Berichte über die Arbeiten Le östreichi-
schen Militär-geographischen Instituts
von den Herren von Kalmdr, von
Sterneck und Hartl (siehe Beilagen IVa
une IVb). :

Antrag des Herrn Helma betreff neuer
Schwerelinien in Tyrol

Berichte der Herren Tinzer und Weiss Der
die Arbeiten der östreichischen Grad-
messungs-Commission (siehe Annex IVe)

Berichteder Herren Bakhuyzen und Schols
über die niederländischen Arbeiten (siehe
Annex V). “u

Bericht des Herrn al As: die
schritte der geodätischen Arbeiten in
Portugal (siehe Annex VI) ;

Bericht über die Preussischen Arbeiten,
vom geodätischen Institut, durch Herrn
Director Helmert (siehe Annex Vlla),

Mittheilung des Herrn ZFoerster über die
von Herrn Mylius erziehlte Verbesserung
der Libellen durch Verminderung der Al-
kalien in den verwandten Glasarten .

Auf Vorschlag des Bureau’s wird Paris
zum Versammlungsort der nächsten Ge-
neral-Gonferenz im Herbste 1889 ein-
stimmig gewählt -

An Stelle des verstorbenen General Berk; ier
wird Commandant Bussot einstimmig
zum Berichterstatter über die Basis-
Messungen erwählt .

Vierte Sitzung, 23. September 1888

Das Protokoll der letzten Sitzung wird ver-
lesen und angenommen :
Antworts-Depesche der Frau von Oppoler
Antrag des Büreau’s betreff der Wahl eines
gemeinsamen Nullpunktes für sämmtliche
Hohen ın Buropa . . - SL
Discussion über diesen Vorschlae, an wel-

 

Pag.

34

35

36

36

3

33
38

39

100

 

 

cher die Herren Lallemand, Hirsch,
Bouquet de la Grye, Edredro, a,
Foerster sich beikeligen - . . .
Der Antrag des Bureau’s wird von der per-
manenten Commission einstimmig gebil-
lt 2 00.0.0 0 0
Bericht des Herrn Oberst Capitaneanu
über die geodätischen Arbeiten in Ru-
mänien (siehe Annex VI) . . . .
Bericht des Herrn General Schreiber über
die Arbeiten der preussischen Landes-
aufnahme (siehe Annex VII) . .
Bericht des Herrn Hirsch über die
tischen Arbeiten in der Schweiz (siehe
Annex X) ; :
Bericht des Herrn General [hans 1 die
Arbeiten in Spanien (siehe Annex ])
Mittheilung des Herrn Baron Teffe über
den wahrscheinlich baldigen Eintritt
Brasiliens in die internationale Erdmes-
sung; er theilt einige brasilianische Kar-
ten und geodätisch-astronomische Ver-
öffentlichungen der Versammlung mit
Zweiter Bericht der Finanz-CGommission

von Lesen Zfoesseen - : :
Die Anträge der Commission werden ein-
stimmig aneenomnen

Arbeitsplan des Gentralbureau’s für 1888-
1889, von Herrn Director Helmert

Auslosung der fünf Mitglieder, der Herren
Nagel, Bakhuyzen, Foerster, Ibanez und
Ferrero, welche dem Reglement gemäss
in der nächsten General-Conferenz aus
der permanenten Commission auszutre-
ten haben -

Der Vorsitzende rchtder e ki Be
und ihrem Vertreter, Grafen Thun, so-
wie der Stadt Salzburg und ihrem Bürger-
meister, Herrn Dr. Schumacher, den leb-
haftesten Dank der Versammlung für die
erwiesene liebenswürdige Gastfreund-
sch us . . u... 2 200.

Pag.

39-42

42

42

m
WW

SS
NS

v

em
L
In
Fn

Hm
Mn

44-45

hd

 

 

 

 
