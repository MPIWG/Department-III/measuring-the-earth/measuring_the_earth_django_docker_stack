Genauigkeit zur Vergleichung führen können. — Es wird beabsichtigt den russischen
Apparat im nächsten Jahre in den Kaukasus zu senden, wo. derselbe theils auf ver-
schiedenen Bergspitzen und anderen Hauptpunkten, theils in dem durch seine vul-
kanischen Erscheinungen so höchst interessanten Gebiete am Südabhange des Gebirges
beobachtet werden soll.

Vicepräs. von Bauernfeind: Ich danke im Namen der Versammlung Herrn von
Forsch und gebe das Präsidium zurück.

Pris. von Forsch: Für Sachsen hat zunächst Herr Bruhns za berichten.

Herr Bruhns: Ich habe in diesem Jahre wenig zu berichten, weil ich als
Mitglied der Commission für die Beobachtung des Venusdurchganges viel mit der Aus-
rüstung der Expeditionen zu thun gehabt habe. Nichtsdestoweniger kann ich mittheilen,
dass von Leipzig aus zunächst alle telegraphischen Längenbestimmungen vollendet sind
und auch in diesem Jahre, trotzdem meine Zeit sehr beschränkt war, die Längen-
differenz zwischen dem Basispunkte bei Grossenhain und Leipzig, sowie zwischen Leipzig
und München vollständig zu Ende geführt worden ist und die Resultate eine der
nächsten Püublicationen sein wird, die ich herausgebe.

Da Herr von Oppolzer die Längendifferenz von München und Wien zu bestim-
men begonnen hat, wird, da Leipzig—Wien längst bekannt ist, wieder ein Dreieck von
Längenbestimmungen vorliegen, von dem ich hoffe, dass es ebenso vollkommen schliesst,
als die schon ausgeführt sind. Ich möchte nur Herrn von Bauernfeind noch bitten,
bald die Ermittelung der Längendifferenz zwischen dem Polytechnikum, wo der Anfangs-
punkt der Längenbestimmung zwischen München und der Leipziger Sternwarte liegt
und der Sternwarte in Bogenhausen, wo Herr von Oppolzer beobachten lässt, aus-
führen zu lassen.

Die Breiten- und Azimuthbestimmungen in Sachsen sind fast fertig, es bedürfen
nur noch einige Beobachtungen einer Revision, so ist z..B. der Punkt Grossradisch,
dessen Azimuth bestimmt war, durch Sturm im Jahre 1868 zerstört und ein neues
Azimuth wird dafür bestimmt werden müssen. |

Was endlich die Arbeiten betrifft, die Herr Nagel und ich vor zwei Jahren
gemeinsam ausgeführt haben, nämlich die Basismessung bei Grossenhain, so ist die
Reduction soweit vollendet, dass nur die Zusammenstellung noch. nöthig ist, und ich
hoffe, dass es möglich sein wird, diese Arbeit im kommenden Sommer, zu publiciren.
Ich freue mich demnach, dass bis auf die Publicationen eines Theiles der astronomischen
Arbeiten meine Arbeiten in Sachsen so gut wie fertig sind. Die Resultate ‚der zahl-
reichen Längenbestimmungen, welche von Leipzig; ausgehen, liegen bis auf die eben
genannten und einige im Lande selbst, Ihnen bereits gedruckt vor.

Pris. von Forsch: Ich danke und bitte nun Herrn Nagel um seinen Bericht.

Herr Nagel: Ich kann mich ebenfalls kurz fassen.

Die bisherigen Jahresberichte haben den Fortschritt unserer) geodätischen Ar-

beiten stets gezeigt und ich füge hinzu, dass ich im Lanfe dieses Sommers wegen

 
