 

70

3. Von Radstadt über Gröbming, Irdning nach Lietzen, einfach . 7 Meilen
4. Nivellement der Seen im Salzkammergut, einfach... . . 20° ,,
5. Von Jedlesee über Kronenburg, Karnabrun, Buschberg nach Laa,

dann Lundenburg, Ung. Hradisch nach Prerau und Olmütz,

Birch Zar À era: ER EN eig
6. Von Troppau über Schönbrenn nach Hans Bchats; Eileen,

Lubien, Kärmark, Leutschau, Eperies, einfach . . . . O0
7. Von Bochnia iiber Tarnow, Pilzno, Dukla, Bartfeld, es;

Maschou, N’ Mihaly, Unghvar, ernfach «6.0 0°. 3.053 273,

8. Von Pilzno über Lancut, Przemysl, Chyrow, Sheet ins
Ungthal nach Unghvär und von Przemysl über Chyrow nach
Lisko, Sanok und Krosno (Dukla) einfach . . . 60,
9. Von Przemysl über Grodek, Lemberg, Tarnopol, Castor Lt
Czernovitz, einfach . HAL Re i RL
10. Von Leutschau über Poprad, Rosenberg, Sillein, Toni nach
Ung. Hradisch und über Jablunka nach Oderberg, einfach . . 46 ,„
> In Summa 443 Meilen.

Im Jahre 1876 wird das Nivellement bis an den Bodensee und Martinsbruck
und zwar in doppelt gemessenen Linien zur Ausführung gelangen.

Präs. von Forsch: Ich danke im Namen der Versammlung und ertheile Herrn
Tinter das Wort.

Herr Tinter: Die schon ım Jahre 1373 in Aussicht genommene Bestimmung der
Polhöhe in Kremsmünster, sowie die Vervollständigung der Azimuthmessung der Richtung
Kremsmünster—Hochbuchberg wurde von mir in den Ferienmonaten August und Sep-
tember dieses Jahres durchgeführt.

Die nöthigen Beobachtungen nahmen die Zeit vom 6. August bis 11. September

{
no

ne

in Anspruch.
An Instrumenten nahm ich ein Universal-Instrument, ein Passagen-Instrument,

eine Pendeluhr von Dorer, ein Chronometer von Weichert mit. Letzteres, sowie
das Passagen -Instrument waren mir durch die Güte des k. k. militär - geographischen
Institutes zu den Beobachtungen überlassen worden.

Das Universal-Instrument wurde auf demselben Punkte, den es im Jahre 1375 zur
Bestimmung des Azimuthes der Richtung Kremsmünster—Hochbuchberg einnahm, aufgestellt.

Um die Polhöhenbestimmung aus Sterndurchgängen im 1. Vertical durch-
führen zu können, musste ich die Güte des hw. Abtes des Stiftes, Herrn Dr. August
Reslhuber neuerdings dadurch in Anspruch nehmen, dass ich an Stelle des Aequatoreales
das Passagen-Instrument auf dem betreffenden Pfeiler aufstellen durfte. Die Kuppeln,
unter denen die beiden genannten Instrumente aufgestellt waren, sind in ihrem Raume
sehr beengt; es ist ein halbwegs bequemes Beobachten nicht möglich, das Aufhängen
einer Pendeluhr nicht denkbar. Die für die Beobachtungen nöthigen Zeitangaben wur-

. den mit Hülfe des Chronometers gewonnen.

 

ch au na

Anhand

 
