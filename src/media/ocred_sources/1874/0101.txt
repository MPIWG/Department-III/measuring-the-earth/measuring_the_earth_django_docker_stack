 

 

 

 

102

polaires permettraient de rattacher en déclinaison les positions des étoiles fondamen-
tales, sans que les distances zénithales observées dépassent la limite de 30°. La
position du pôle nord étant déterminée par les observations faites dans la station la plus
boréale, les distances des étoiles au pôle nord se déduiraient de proche en proche
jusqu'aux étoiles voisines du pôle sud, et comme les distances de ces étoiles au pôle
austral s’obtiendraient directement, les résultats seraient soumis au contrôle consistant
en ce que la somme des distances d’une même étoile aux deux pôles doit être exactement
de 180%. Nous avons montré comment les discordances systématiques, s’il s’en mani-
festait, pourraient être annulées, ou tout au moins réduites, par une correction appliquée
à la constante de la réfraction et par une détermination du coefficient du terme du
Qme ordre de la flexion que l’on n’a pas jusqu'ici mesuré directement.

On ne peut se dissimuler Pimportance qu’il y aurait à faire usage d’une méthode
offrant un moyen de contrôle si simple, alors que les méthodes en usage en sont
absolument dépourvues. Il est impossible en effet de considérer comme suffisant un
moyen de contrôle qui se réduit à comparer les différents catalogues et à constater
leur désaccord, sans qu’il surgisse de leur comparaison la moindre indication sur le
choix à faire entre la plupart d’entre eux. Aussi, avons nous pu dire que le travail
actuellement poursuivi dans les observatoires, fût-il continué pendant un siècle, n’abou-
tira pas à lever les incertitudes qui affectent les déclinaisons des étoiles fondamentales ;
la connaissance de leurs mouvements propres, qui n’est certes pas à dédaigner, pourra
seule en recevoir quelque perfectionnement.

En proposant de substituer aux méthodes actuelles un procédé basé sur le

déplacement de l'instrument et de l'observateur, nous avons supposé que linstru-

ment, consistant en un cercle vertical de moyenne dimension, jouirait de toutes les
qualités qui distinguent les meilleures productions de nos artistes et que cet instrument
serait l’objet d’une étude complète. Il à d’ailleurs été exposé que les tentatives ayant
pour but d'en augmenter la précision seraient sans objet utile, attendu que la part
des erreurs combinées de l’instrument et de l’observateur reste de beaucoup inférieure
a Veffet des perturbations produites par les mouvements de l’atmosphere dans les
réfractions normales.

| Nous voulons examiner de plus près les conditions pratiques de l’exécution du
nouveau projet. L’hémisphere boréal ne présente pas de difficultés sérieuses: en effet a
la latitude de 62°, par exemple, (à environ 2° au nord de l'Observatoire de Poulkowa)
on observerait les passages supérieur et inférieur de la Polaire, à moins de 30° du
zenith; ce qui suffñrait pour déterminer la position du pôle boréal et pour y rattacher
les étoiles, jusqu'à la distance de 58°, sans dépasser la limite assignée de 30° de
distance zénithale.

Dans l'hémisphère austral, on rencontrerait deux difficultés sérieuses: 1° trouverait
on une station habitable au dela de la Terre de Feu ou de 55° de latitude australe?
2° Vétoile o du Sextant, qui seule pourrait remplir le rôle d’etoile polaire et dont
l'éclat se réduit a la 6™¢ grandeur, serait-elle observable de jour, avec une lunette de

 

ad a

hk th ML ld le

 
