PTT PPAR FI eT TT |

par tort

i
R
£
3
&
:
k
3
F

33

 

Brera, il quale sta sotto POsservatorio di 25 metri circa fu, a spese della Commissione
del Grado, costruito un Osservatorio mobile di legno, e dentro di esso fu con salde
fondazioni stabilito il pilastro destinato a portar Vistrumento dei passaggi, che sullo
scorcio del 1869 era stato ordinato ad Ertel di Monaco per la Commissione stessa.
La distanza focale del cannocchiale à di 070, il diametro dell’ obbiettivo ha 66
millimetri di apertura. Ma il grande prisma che serve a piegare i raggi luminosi ad
angolo retto (il cannocchiale è dei cosi detti spezzati) toglie molta luce, e rende le
immagini piuttosto imperfette. Ma nelle parti essenziali l’istrumento si trovd assai
buono. La forza ed il peso della sua base contribuiscono alla stabilita, i poli di rota-
zione sono lavorati con grande esattezza, e Vinclinazione (elemento importantissimo pel
tempo assoluto) si pud ottenere con gran precisione.

5° Noi avevamo ancora provveduto un istrumento per determinare in modo
assoluto Vequazione personale. Svyenturatamente non trovammo nell’ Osservatorio basi
abbastanza solide per appoggiarvi quell’ apparato, e quindi si dovette rinunziare a farne
uso. L’equazione personale fra il nostro Osservatore Signor ingegnere Celoria e i tre
Osservatori svizzeri Plantamour, Hirsch e Schmidt fu determinata direttamente con
osservazioni simultanee ai medesimi strumenti, nei due viaggi che immediatamente
prima e immediatamente dopo le operazioni il Sigt Celoria fece in Svizzera con
questo scopo.

6° Ebbimo ancora ad adoperare alcuni apparati elettrici minori. ‘Tali furono:
una pila di Daniell di 144 piceoli elementi; una bussola da telegrafi per constatare la
forza delle correnti: un reostata d’Hipp, il quale fu impiegato in esperienze sopra i
varii ritardi che frappone alla registrazione del cronografo la penna dei segnali quando
e mossa da corrente di diverse intensita.

4° Ordine generale delle operazioni.

L’esperienza fatta nelle differenze di longitudine fra le stazioni svizzere avendo
dimostrato, che per mezzo dello seambio di segnali cronografici si pud ottenere un grado
di precisione praticamente uguale a quello che e dato dalla registrazione simultanea
dei passaggi di stelle sui due cronografi, di comune accordo si preferi di adottare il
primo sistema, il quale ha il vantaggio grandissimo di lasciare molta liberta agli opera-
tori, mentre il secondo sistema nel caso nostro sarebbe stato meno praticabile atteso
la difficolta che sempre si provod nello spedire segnali regolari a Neuchâtel, la non
costante coincidenza del sereno e dell’ annuvolarsi nelle diverse stazioni, e le perdite
di appulsi stellari dipendenti dallo scambio dei nostri cilindri cronografici, i quali con-
tengono soltanto un’ ora di registrazione, e devono quindi.esser cambiati d’ora in ora.
L’ordine adottato fu questo, che osservate prima la Polare nella sua culminazione in-
feriore e una serie di stelle orarie, intorno a 17 ore siderali si scambiassero i segnali
fra le tre stazioni; dopo di che una seconda serie di stelle orarie e Vosservazione di
0 Ursae Minoris nella culminazione superiore dovea chiudere la serie delle operazioni
di ciascuna serata. Lo scambio dei segnali poi era inteso cosi, che ciascuna stazione

5

 
