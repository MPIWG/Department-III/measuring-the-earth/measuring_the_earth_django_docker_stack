 

44

mercurio alla profondita di circa 30 centimetri sotto il piano della porta, e ben difeso
dall’ azione del vento.

Ad evitare poi l’influenza di qualunque corrente di aria che avesse potuto nuo-
cere alle osservazioni si é avuto l’avvertenza di chiudere i piccoli finestrini che illuminano
la scala: e con cid si é ottenuto che anche nelle ore e nelle giornate di vento forte,
che non sono rare a Monte Mario, la rifiessione dei fili e delle stelle si faceva con tale
regolarita, da far apparire che le osservazioni fossero dirette, o fatte sopra uno stabile
orizzonte metallico.

Sopra ogni stella le osservazioni si sono fatte alternativamente collimando la
stella col filo mobile e il Nadir con uno dei fili fissi, o viceversa collimando la stella
col filo fisso e il nadir col filo mobile e talora anche collimando il Nadir e la stella
coi due fili mobili.

La divisione di coincidenza dei fili mobili coi varii fili fissi a cui veniva ripor-
tata la loro corsa é stata diligentemente e frequentemente verificata durante il tempo
delle osservazioni.

Per ridurre al minimum la corsa dei fili mobili si sono stabilite aleune coppie
di fili fissi di cui furono con tutta accuratezza determinate le distanze, le quali coppie,
secondo il numero dei minuti da essi abbracaciate, venivano usate per quelle stelle che
presso a poco avevano una distanza zenitale eguale.

Le coppie di fili fissi usati nelle varie misure furono 3, e cioé
coppia I distanza 10‘ 55. so
ni UE 5 10 239.78
wall 7 16. 55. 94

ue viti micrometriche furono trovati

Qu

I valori in arco delle rivoluzioni delle
por le 2 =
per, la 22.0. —

€

6820

os

€

he

>

©
rag

oO 44

oh

o ;
01683
0%

048

op

Differenza —
Ma siccome la parte della distanza zenitale da misurarsi colle viti micro-
metriche, coll’ uso dei fili fissi, era ridotta a un piccol numero di rivoluzioni, cosi per
simplicità si & creduto opportuno di adottare per valor comune delle rivoluzioni il medio
di r ed 7’, ossia
D ep:

_e su quella costruire la tavolo per la riduzione delle divisione in arco. L’oculare usato

indistintamente in tutte le osservazioni da un ingrandimento di 110; al quale per deter-
minare il Nadir si applicava l’apparato che dä i fili scuri in campo illuminato. La
nitidezza dei fili tanto diretti che riflessi rendeva speditissima e sicura la determinazione
del nadir il che veniva confermato dal piccolo errore probabile trovato in molte serie
di coineidenze dei fili mobili colla loro immagine riflessa.

Premessi questi cenni sul metodo di osservazione, veniamo ai risultati delle
osservazioni fatte. Nei qui uniti specchj sono dati i risultati ottenuti per ogni stella

 

i

cou hm a, add a un

 
