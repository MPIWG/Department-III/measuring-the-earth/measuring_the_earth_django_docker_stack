ET TFT TORI nn |

wert

Iren

R
=
E
5
:
k
R
E
=
F
}

29

 

Tali operazioni furono eseguite dai Signori Professori Hirsch, Plantamour, Schia-
parelli nei mesi di Giugno e Luglio 1870. Per cura del Professor Schiaparelli si sta

ora attendendo alla pubblicazione di quella parte di lavoro che lo riguarda personal-

mente. In attesa pero di quella pubblicazione, mi pregio di sottoporvi l’annessa relazione
sommaria che il prefato Professore ebbe la compiacenza di rimettermi (vedi pag. 30 etc.).

b) Determinaziont di latitedine ed azimut.

Nell’ anno corrente si incominciarono ad attuare quattro stazioni astronomiche
per la determinazione di latitudini ed azimut (vedi Carta 1).

I Prof. Respighi, direttore del Re Osservatorio del Campidoglio, fece la stazione
astronomica di Monte Mario nel Luglio-Agosto.

I metodi di osservazione da lui impiegati essendo in parte nuovi, io lo pregai
di farmi una relazione, se non definitiva, almeno provvisoria sulla stazione suddetta.
La relazione (vedi pag. 36 etc.) va considerata semplicemente come provvisoria, ed &
questo titolo ho Vonore di presentarla alla Conferenza, a nome del Prof. Respighi.

Allo scopo di esercitare gli ufficiali dell’ Istituto topografico militare nelle
operazioni astronomiche di campagna, incaricai il Professore Schiavone, Direttore della
Sezione di Napoli del suddetto Istituto, di compiere la stazione astronomica di Pizzo-
falcone. I calcoli relativi a questa operazione non essendo ancora ultimati, mi
limito a deporre una copia della relazione sommaria presentatami dal Prof. Schiavone
(vedi pag. 54 etc.).

Nel mese scorso venne incominciata la stazione astronomica di Termoli, la
quale à pressochè finita. E pure in corso di esecuzione una stazione astronomica presso
Vestremo N. W. della base geodetica di Lecce. La prima di queste stazioni e eseguita
dal Professore Nobile, astronomo del Re Osservatorio di Napoli; la seconda dal Pro-
fessore Lorenzoni dell’ Osservatorio di Padova, e dal Capitano De Vita del Corpo di
Stato Maggiore.

A suo tempo verranno comunicati i risultati di tutte queste stazioni.

HE
Pubblicazioni.

Per cura dell’ Istituto topografico militare, e par conto della Commissione
italiana, si stauno ora preparando gli elementi per la pubblicazione dei lavori geodetici
finora eseguiti. Nel corso di quest’ anno alcune pubblicazioni vedranno la luce.

Sono certamente a conoscenza della maggior parte di Voi, tanto la pubblica-
zione del Padre Secchi e del professore Fergola sulla determinazione della differenza di
longitudine tra Roma e Napoli, come la pubblicazione dei professori Nobile e Tacchini
sulla differenza di longitudine tra Napoli e Palermo. Esse debbono considerarsi come parte
integrante dei lavori astronomici geodetici in Italia.

Infine come si é gia detto, il Professore Schiaparelli si occupa della pubblicazione
delle operazioni a cui egli ha preso parte nel 1870.

 
