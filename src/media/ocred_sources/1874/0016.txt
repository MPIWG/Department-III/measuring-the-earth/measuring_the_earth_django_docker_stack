N
Isenburg,
Asse (Bergrücken bei Wolfenbüttel).
(Siehe Protokolle der perm. Commission von 1873, Appendix IV.)
4. In diesem Jahre sind folgende Längenbestimmungen ausgeführt worden:
Leipzig— Brocken,

Brocken— Göttingen ,
Gottingen—Berlin.

Ferner wurden die Untersuchungen über die Lothablenkungen im Harz fort-
gesetzt und die Polhöhen bestimmt:

a. Auf der Station Löwenburg zwischen Tettenborn und Mühlhausen auf dem
Rücken der Hainleite. Die definitive Rechnung differirt nur um —0“3 von
der geodätischen Bestimmung und lässt zwischen Tettenborn und Mühlhausen
einen neuen Nullpunkt vermuthen.

b. Auf der Station Kuhberg östlich von Nordhausen. Die Rechnung gieht hier

eine Ablenkung von —5! 2.

Auf der Station Bornstedter Warte bei Eisleben. Es findet sich —473

Unterschied, also. auch südliche Ablenkung, und nahe zu von derselben

Grösse wie in Kuhberg und Tettenborn.

d. Auf dem Gegenstein bei Ballenstädt. Die südliche Ablenkung der vorher-
gehenden Punkte geht hier in nördliche über, und zwar giebt die Rechnung
-- 87, so dass zwischen Gegenstein und Bornstedter Warte, bei einer
Meridian- Differenz von etwa 15 Bogenminuten, eine Lothablenkung von
13° stattfindet.

(ca)

D. Hauptnivellement.

| Seit der dritten allgemeinen Conferenz ist das Pracisions-Nivellement um die
: nachstehenden Strecken erweitert worden: |
1. Von Erfurt über Nordhausen, den Brocken und Ilsenburg nach Börsum.
2. Von Halle über Nordhausen nach Nordheim.
3. Von Börsum über Kreiensen nach Kassel.
4. Von Magdeburg über Braunschweig und Hannover nach Kreiensen.
Die Strecken 1, 2 und 3 sind doppelt nivellirt, No. 4 aber nur einfach.
Im Laufe dieses Sommers wurde No. 4 wiederholt, und durch doppeltes Ni-

A

vellement die Strecke von Hannover bis nach der Niederländischen Grenze (Salzbergen)
in der Richtung nach Amsterdam hin vollendet, um die Verbindung mit dem Amster-
damer Pegel herzustellen. — Dem Niederländischen Commissar für die europäische Grad-
messung, Prof. Stamkart, sind die Mittel bereits bewilligt und er hat die Fortsetzung
von Salzbergen bis Amsterdam für das nächste Jahr in sichere Aussicht gestellt.

Da sich bei dem Nivellement über den Harz starke Differenzen gezeigt hatten, so
wurde in diesem Sommer die Strecke von Börsum über den Brocken bis nach Tetten-
born in umgekehrter Richtung wiederholt, um mit möglichster Sicherheit den Einfluss

a
wo

i
i
È
iR
=
ak
i
R
!

 
