Tr en ee

TT ASTON TT ee

TT

TTAOTTTT

yore

a

x
s
=
x
3
=
=
È

85 e

présenter à la Conférence une des premières épreuves de la feuille de Madrid, tirée à
la hâte au. moment de mon départ. Comme on verra, la carte est a Vechelle de

ee 60° imprimée en cinq couleurs, le relief du terrain étant representé par des courbes -
de niveau & Véquidistance de 20 métres, mais avec des points intermödiaires d’altitude
déterminée, qui permettraient de, reconnaître, de dix en dix mètres, les principaux acci-
dents du sol. |

La publication de cette première feuille d’une oeuvre qui doit en comprendre
1080, est pour lInstitut géographique un évenement important dont j'ai poursuivi
durant 20 ans la réalisation, et c’est plus tôt en ma qualité de collaborateur que
je me permets d'offrir cette preuve de notre activité à la Conférence géodésique
internationale.

Präs. von Forsch: Ich spreche Herrn Ibanez meinen Dank aus und ersuche die
Herren Commissare für Württemberg ihren Bericht zu erstatten.

Herr Schoder: Nachdem bei der dritten allgemeinen Conferenz die von der
württembergischen Landesvermessung her vorhandene Triangulation als unbrauchbar
erklärt worden war, weil ein Theil der Original-Documente fehlt, wurde die Königl.
württembergische Regierung auf Veranlassung der permanenten: Commission aufgefordert,
eine neue Triangulation ausführen und einen astronomischen Hauptpunkt errichten zu
lassen. Bei einem Zusammentritt der deutschen Commissäre sodann, welcher im December
1872 zu Berlin statt hatte und über welchen der Generalbericht von 1872 das Nähere
enthält, wurde die Aufgabe Württembergs speciell dahin präcisirt, dass es die Ver-

bindung zwischen den badischen und bayerischen Vermessungen durch eine im Süden

des Landes verlaufende Dreieckskette herzustellen habe.

Auf Grund der Berliner Verhandlungen von 1872 wurde von der .württember-
gischen Commission ein detaillirter Plan sammt Kostenvoranschlag übergeben, welcher
die Genehmigung der württembergischen Regierung erhalten ‚hat. Im Laufe dieses
Sommers wurde mit den Recognoscirungen und mit den nöthigen Grunderwerbungen
begonnen, so dass im Frühjahre 1875 wenigstens diejenigen Pfeiler stehen werden,
welche für den Anschluss an Baden nothwendig sind. Ebenso wird die Station Bussen,
in welcher Breite und Azimuth bestimmt werden soll, bis zum gleichen Zeitpunkte
hergerichtet sein (siehe Karte 6).

Nivellement. Das württembergische Nivellementsnetz erreicht dieses Jahr
eine Länge von 1553 Kilometer. Ausser den zahlreichen Anschlüssen innerhalb des
Landes haben wir bis jetzt in 5 Punkten an Bayern angeschlossen, ferner an das durch
Baden geführte Nivellement des Centralbureau einmal in Bruchsal, sodann am Bodensee.
Letzterer Anschluss war übrigens blos möglich dadurch, dass Württemberg eine grössere
Strecke auf badischem Gebiet nivelliren liess. |

Präs. von Forsch: Ich danke im Namen der Versammlung und, da wir mit der
Berichterstattung zu Ende, kommen jetzt die Referate über die geodätischen Fragen 4
und 5, über welche Herr Hirsch zu berichten hat.

 
