A sah da Ada dl Ms

50

ah

Supponiamo di avere un reticolo di 4 fili che si intersechino ad angolo di-45°, i
quale si usa per la determinazione della differenza di declinazione di due astri negli |
stromenti equatoriali; applicato questo reticolo al cannocchiale zenitale in modo che uno
dei fili sia esattamente equatoriale, si faccia coincidere questo filo colla sua immagine
riflessa dall’ orizzonte a mercurio. Cid posto é evidente, che osservando una stella nel
suo passaggio meridiano e notando i tempi degli appulsi di essa stella coi due fili
inclinati col primo di 45°, dall’ intervallo di tempo compreso „fra gli istanti dei due
appulsi, colla nota declinazione della stella, & evidente dico, che si poträ dedurre la
distanza zenitale di questa stella, con quella approssimazione, colla quale possono
ottenersi gli istanti dei due appulsi.

Considerando una osservazione isolata, certo non si potrebbe sperare da essa
una sufficiente esattezza od approssimazione di risultati, ma dal medio risultato di una
serie di osservazioni, si potrebbe sperare la determinazione della latitudine forse con
quella esattezza che si pud ottenere cogli ordinarii strumenti usati nelle osservazioni
geodetiche e cioe altazimut, teodoliti ecc.

Dando poi al reticolo una forma più complessa, ma che certamente non ne i
renderebbe molto difficile la costruzione e l’uso, mi sembra si potrebbe giungere a
risultati non meno pregievoli di quelli che si possono ripetere dai micrometri a :
i fili_ mobili. :
Supponiamo un reticolo costituito di due |
sistemi di fili paralleli ed equidistanti, uno per-
pendicolare all’ altro come nella qui unita figura,
con due fili MM, NN paralleli fra loro paralleli, ;
ed inclinati di 45° coi primi aa, bb e finalmente
un altro filo PP perpendicolare ad MM o NN.

Supponiamo la distanza fra i fili a,a@,a, e
b,b,b p. e. di 2° e la distanza del filo MM al 4
vertice A”, e cosi quello di NNad h p.c. di6! 3

Si supponga finalmente il reticolo fissato al |

cannocchiale in modo che MM ed NN riescano
| esattamente equatoriali.
i Diretto il cannocchiale verso il Nadir, e facendo coincidere p. e. il flo NN
colla sua immagine riflessa dall’ orizzonte a mercurio, e cosi l’altro filo PP alla ri-
( spettiva immagine, se una stella passerä al meridiane ad una distanza dal zenit o dal
N

 

 

 

 

Nadir di pit di 9’, in tale distanza perd da essere compresa nel campo utile del

cannocchiale, come sarebbe in SS, si potranno osservare gli appulsi della stella a tutti

En i sei fili, dai quali colla nota declinazione della stella*si potranno avere le distanze

a gh, qh', qh, che rispettivamente aggiunte alle tre costanti note Rh", Rh', Rh daranno
tre misure della cercata distanza zenitale della stella, mentre dal medio degli appulsi
ai sette fili si potrebbe avere con molt& approssimazione il tempo del passaggio.

Sarebbe poi opportuno che il reticolo fosse portato da un circolo di posizione |
|
|

Em en en

ER

 
