 

 

ee een

 

 

196

Hauptstationen der Europäischen Längengradmessung : Breslau, Leipzig und Bonn, deren
Längendifferenzen unter Zuziehung von Berlin und Königsberg als Referenzstationen bereits
1864-1865 eigens zu diesem Zweeke bestimmt wurden, liess sich vermittelst des sogenannten,
bis jetzt nicht veröffentlichten Rosenthaler Netzes für Breslau, aus den Daten der Königl.
Preussischen Landesaufnahme für Leipzig und aus den Resultaten des Bonner Basisnetzes
für Bonn in befriedigender Weise bewerkstelligen. »

Eine andere Arbeit von allgemeinerem Interesse wurde nach Abfassung der unter
den Publikationen erwähnten « Uebersicht der Arbeiten des Königl. Geodätischen Instituts
unter Generallicutenant z. D. Dr. Baeyer etc. » im vergangenen Frühjahre begonnen, welche
theilweise gegenwärtig unter dem Titel « Lothabweichungen, Heft 1 » veröffentlicht werden
konnte. Diese Arbeit verfolgt in erster Linie den Zweck, parallel dem Fortgange des Euro-
päischen Gradmessungsunternehmens vorläufige Ergebnisse für die specielle Erdgestalt in
Europa in der Form von Lothabweichungen gegen ein bestimmtes Referenzellipsoid und
mit Bezug auf einen Nullpunkt von centraler Lage abzuleiten. Indem bei den betreffenden
Rechnungen die sich darbietenden Kontrollen aus dem geometrischen Zusammenschluss der
Dreiecksnetze und aus dem Laplace’schen Theoreme beachtet werden, bietet sich in zweiler
Linie die Gelegenheit, gröbere Mängel der ausgeführten Arbeiten zu erkennen und Versuche
über praktikable Ausgleichungsmethoden grosser astronomisch-geodätischer Systeme anzu-
stellen. Einestheils durch diesen Umstand der Ausnutzung von Kontrollen, anderntheils
aber auch durch die ganze Art der Berechnung unterscheidet sich die befolgte Rechnungs-
methode von den früher im Geodätischen Institut ausgeführten Berechnungen von Lothab-
weichungen.

Zur Veröffentlichung sind in Heft 4 nur Lothabweichungen in Norddeutschland
gelangt, aber die vorbereitenden Rechnungen für eine Fortsetzung über Belgien bis Green-
wich, Paris und Brest einerseits und nach der Schweiz andrerseits sind bereits begonnen.
Es stellen sich der Ausführung solcher Rechnungen übrigens grössere Schwierigkeiten ent-
gegen, als man erwarten sollte, wenn man die Uebersichten der astronomischen und geodä-
tischen Messungen betrachtet, welche den Verhandlungen der « Allgemeinen Gonlerenz etc. »
1883 beigegeben sind; denn vieles beobachtete Material ist noch picht publicirt und oftmals
ist die Beziehung der astronomischen Punkte zu den Dreiecksnetzen nur schwer festzu-
stellen.

Da aber der Nutzen einer Arbeit, welche die Ableitung vorläufiger Resultate der
Lothabweichungen zum Zweck hat, für die homogene Durchführung des Gradmessungs-
unternehmens ganz zweifellos ist, so darf das Geodätische Institut wohl auf die Unterstützung
der Gradmessungskommissare rechnen, wenn eine solche für die Ueberwindung einzelner
Schwierigkeiten erforderlich werden sollte.

Es möge hier noch eines Nebenresultates gedacht werden, welches sich bei den
Berechnungen für die Lothabweichungen ergab. Dasselbe betrifft die Vergleichung einiger
Grundlinien mittelst der verbindenden Dreiecksnetze. Geht man von der Berliner Grund-
linie aus, so sind die ermittelten Unterschiede in Einheiten der siebenten Stelle des Loga-
rithmus :

   

 

 

 
