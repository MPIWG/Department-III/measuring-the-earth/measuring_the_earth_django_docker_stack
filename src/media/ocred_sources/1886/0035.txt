 

1]

haben inzwischen unter der belebenden Wirkung der Gradmessun
ihren Siegeszug über ganz Europa ausgedehnt.

Aber nicht blos alle Massbestimmungen und ihre Hülfsmittel sind an Reich-
thum und Genauigkeit gewachsen, sondern auch die Probleme haben sich ver-
tieft und erweitert und sind zugleich immer fruchtbarer und bedeutsamer an
Ausblicken und Verbindungen nach anderen Seiten der Forschung und der Praxis
geworden. |

Niemand wird von derjenigen Darstellung aller dieser Ergebnisse und
Probleme, welche unser Kollege Helmert in den letzten Jahren gegeben hat,
nähere Kenntniss genommen haben, ohne ein Gefühl der Ergriffenheit von der
Grösse und Tiefe der Sache, welcher unsere Gemeinschaft ihre vereinigten An-
strengungen widmet.

Noch in allerneuester Zeit haben auch Forschungsgebiete, welche an-
scheinend von der Geodäsie weit entfernt sind, dringende Anforderungen an die
Erdmessung zu stellen begonnen, welche ich in Kürze erwähnen will, weil sie
ein deutliches Beispiel von der Verkettung und von der Fruchtbarkeit aller
tieferen Forschung gewähren. Es hat sich nämlich erkennen lassen, dass die
Methoden und Hülfsmittel zur Bestimmung der Spannung oder des Druckes gas-
förmiger Körper, wie sie in der Thermometrie, Thermodynamik, Meteorologie
u.s. w., sowie bei zahlreichen Aufgaben der Chemie auftreten, nicht länger von
Ort zu Ort mit der bei diesen Bestimmungen schon erreichten Schärfe verglichen
werden können, wenn nicht die Erdmessung für jedes der physikalischen oder
chemischen Institute, von denen absolute und genaue Gasspannungsmessungen
ausgeführt werden, eine directe Bestimmung der rein lokalen Intensität der
Schwere liefert. Die allgemeine Kenntniss der Erdgestalt und der Variationen der
Intensität der Schwere mit der geographischen Breite und der Meereshöhe sind
nicht entfernt mehr hinreichend, um absolute barometrische Bestimmungen,
die an verschiedenen Orten ausgeführt werden, mit einander hinreichend strenge
vergleichbar zu machen. Durch Darbietung von Messungen der Intensität der
Schwere an physikalischen und chemischen Instituten der erwähnten Art, insbe-
sondere auch an das Internationale Mass- und Gewichtsbureau zu Breteuil bei
Paris, wird die Erdmessung sich eines Theiles des Dankes und der Verpflichtun-
gen entledigen, welchen sie der Internationalen Mass- und Gewichtsorganisation
schuldig geworden ist für die von derselben getroffenen Einrichtungen zu fun-
damentalen Vergleichungen der geodätischen Messstangen jedes Systems mit dem
Internationalen Prototyp des Meter.

Ist auch durch diese ausgezeichneten Veranstaltungen für unsere Vereini-
gung die Aufgabe nicht erledigt, selber Veranstaltungen zu unablässigen, expe-
rimentellen Studien über die beste Art der Grundlinienmessung herzustellen,
worüber Ihnen Kollege Helmert späterhin weitere Mittheilungen machen wird,
so ist doch durch das Entgegenkommen der verwandten Internationalen Insti-

os-Organisation

 
