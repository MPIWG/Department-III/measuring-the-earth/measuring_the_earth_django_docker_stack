 

 

 

 

 

 

 

Bien que ces excellentes dispositions 4 Breteuil ne dispensent pas notre
Association de prendre de notre chef certains arrangements en vue d’études
expérimentales continues sur la meilleure méthode à employer dans la mesure
des bases, arrangements au sujet desquels notre collègue Helmert vous fera des
communications ultérieures, 11 faut cependant reconnaitre que, grâce aux avances
de l’Institut international de Paris, on peut rattacher, d’une maniére satisfaisante,
nos mesures géodésiques aux prototypes des poids et mesures que cet Institut
international a en outre la mission de conserver.

I n'y a qu'un terrain où le développement des travaux géodésiques ait été
peut-être plus lent qu'on ne l'avait espéré en 1864; c’est celui des recherches
théoriques et du reliement des résultats des triangulations entre eux et avec le
système des déterminations astronomiques, c’est-à-dire le terrain même de la
uéodésie proprement dite. Nous avons toulefois lieu d'espérer que le renforce-
ment de la situation dirigeante qui résulte de la nouvelle Convention pour votre
Commission permanente, el son active coopération avec le Bureau central, feront
naitre bientôt aussi dans cette direction un développement réjouissant et fécond.

M. von Slruve désire ajouter quelques mots au discours présidentiel et s'exprime en
tes termes :

€ Lorsqu'il y à trente ans, on parvint à convoquer la première réunion devant
s’oceuper de la mesure des degrés de longitude, ce furent surtout l'intérêt personnel et
lactive coopération du prince de Prusse de cette époque, le glorieux empereur et roi qui
occupe actuellement le trône, qui rendirent possible la réunion et la firent aboutir. Il est
donc juste que nous présentions tout particulièrement nos remerciements et nos hommages
à Sa Majesté l'Empereur Guillaume. »

Après une suspension de la séance pendant un quart d'heure, et après que Messieurs
les Ministres et les autres hauts fonctionnaires d'Etat se furent retirés, M. le Président déclara
de nouveau la séance ouverte en faisant tout d’abord à l'Assemblée quelques communications,
ainsi que plusieurs propositions.

En ce qui concerne en premier lieu la fixation définitive du programme des travaux
pour chacune des séances, M. le Président propose de consacrer la séance d’aujourd’hui es-
sentiellement a la lecture du rapport de la précédente Commission permanente, ainsi que
de celui du Bureau central, en vue d'établir par ce moyen la connexion nécessaire entre le
passé de l'Association géodésique et la nouvelle phase de son développement.

La seconde séance serait consacrée essentiellement à l'exécution de la nouvelle
Convention internationale par la nomination du Secrétaire perpétuel et des Membres tempo-
raires de la Commission permanente. Dans les séances subséquentes, l'Assemblée entendrait
les rapports de plusieurs délégués sur les progres des travaux géodésiques dans leurs pays
respectifs, ainsi que diverses communications scientifiques, qui donneront peut-être lieu à des
discussions.

   
  
  
 
 
   
   
  

cia ine oe

 
