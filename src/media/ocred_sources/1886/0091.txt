TEE TEE TEE

ee

|
i

 

 

 

Avant de nous livrer à l’étude des problèmes scientifiques et d’organisation
de notre Association, qui vont nous oceuper, et avant de vous faire approuver
l’ordre du jour de la séance d’aujourd’hui, ainsi que le programme général de
nos délibérations, en vue de leur adoption ou de leur modification par voie
d’amendements, permettez-moi d'ajouter aux éloquentes paroles de bienvenue
prononcées par S. E. M. le Ministre, une exposition rapide des phases successives
par lesquelles a passé notre entreprise depuis sa fondation, en 186%, dans ce
lieu même, et des perspectives qui s’ouvrent pour son avenir.

Je ne crois pas me tromper si, parmi les motifs de la nomination qui
m’honore et qui me fait occuper aujourd’hui cette place, à laquelle notre excellent
collègue Helmert aurait eu plus de droits, grâce aux nombreux services qu’il à
rendus pour élucider et vivifier les recherches géodésiques, — si, parmi ces mo-
tifs, je reconnais certains égards dus à ancienneté; car, de tous les délégués ici
présents, je suis en effet celui qui a commencé le plus tôt à collaborer à lentre-
prise de la mesure des degrés. Je suis toutefois surpassé de beaucoup sous ce
rapport par mon honorable collègue, M. le Conseiller privé von Slruve, que vous
avez choisi pour lun des vice-présidents, l'usage ayant établi que le président
est désigné parmi les délégués de l'Etat qui reçoit la Conférence. Cest lui qui,
en commun avec son regretté pére, avec le général Baeyer et Alexandre de
Humboldt, a eu la joie de participer aux vastes idées organisatrices qui, grâce à
l'énergie indomptable de notre vénéré chef, M. le général Baevyer, décédé l'an
dernier, ont fait nailre l'Association géodésique internationale.

Il m'a été donné de coopérer activement à l’entreprise avec cet homme
excellent, d’abord comme l’un des secrétaires des Conférences générales de Berlin
en 1864 et 1867, et ensuile comme membre du Bureau central, de 1865 à 1868.
Des conflits douloureux, d’une nature générale, et qui ne seroht exposés
avec calme que dans l’histoire de la mesure des degrés, me séparerent a cette
époque de celui qui avait daigné offrir son amitié à un homme beaucoup plus
jeune que lui, et me procurer ainsi une joie et un honneur exceptionnels; bien
plus, il m'avait donné, par le charme de sa forte et profonde nature, des heures
d'une communauté de sentiment dont le souvenir restera à jamais gravé dans
mon esprit.

C'est la première fois, depuis sa mort, que je prends part de nouveau, en
qualité de délégué, aux délibérations pour la grande entreprise qui, en dépit de
son âge très avancé, enthousiasmait cet homme distingué.

Permettez-moi, Messieurs, de déposer en ce grave moment, sur sa tombe
et par la pensée, une couronne d'immortalité comme souvenir de piété. Des
hommes d’une telle valeur et doués d’une force de volonté pareille à la sienne,
font du bien autour d'eux, comme les puissances naturelles insondées ; ils nous
soutiennent, dans la lutte des opinions, même quand nous doutons parfois de la
profonde sagesse de ces manifestations de la volonté.

 
