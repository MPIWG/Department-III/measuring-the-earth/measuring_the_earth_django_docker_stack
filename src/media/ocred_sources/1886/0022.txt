 

 

 

 

 

 

3

SE

= were 5 om

 

  

Arr. 3. Dans la première Conférence générale de PAssociation, en 186%, dans la-
quelle les premières règles d'organisation ont été établies, non seulement on a chargé M. le
vénéral Baeyer de la direction du Bureau central; mais il a été désigné également pour faire
partie de la Commission permanente, composée alors de sept membres. Lorsque, plus tard,
à la Conférence générale de 1871, le tour était venu pour M. le général Baeyer de sortir de
la Commission, d’après les Statuts, la Conférence à pris à l'unanimité la résolution suivante :

« Le Président actuel du Bureau central, M. Baeyer, est nommé membre perpétuel
de la Commission permanente. »

[ s’ensuit que la disposition de l’article 3 du nouveau projet, d’après laquelle le Di-
recteur du Bureau central sera désormais Membre perpétuel de la Commission permanente,
correspond à la règle déjà adoptée. La suite de ce même article définit en outre les obliga-
tions du chef du Bureau central, vis-à-vis de la Commission permanente, d’une mamiére
plus précise que cela n’a été fait par l’ancienne disposition, d’après laquelle le Bureau cen-
tral devait être l'organe exécutif de la Commission permanente.

ART. 4. La nomination d’un Secrétaire perpétuel, qui, d’après Particle 7, sera dé-
sormais convenablement rémunéré, s'explique par Pallocation d’un budget à la Commission
permanente. Si l’on a eu jusqu’à présent deux Secrétaires, c’était surtout en vue de l’obliga-
tion de rédiger les comptes-rendus des Conférences, à la fois en allemand et en français. A
l'avenir, on pourra confier les comptes-rendus dans les deux langues au seul Secrétaire per-
pétuel, puisque celui-ci sera mis en état de se procurer, au besoin, l’aide nécessaire pour
les travaux devant paraitre en deux langues.

Art. 5. Le nombre des Membres de la Commission permanente, fixé en 1864 à sept,
a été porté à neuf, lorsqu’en 1867, par suite de l’extension de l’entreprise, son nom d’Asso-
cation internationale pour la mesure des degrés dans Europe centrale a été changé en
Association internationale pour la mesure des degrés en Europe.

Aujourd’hui, après l’entrée de la France en 1872, apres que les délégués du Coast
and geodetic Survey des Etats-Unis ont participé aux Conférences et aux travaux de I’ Associa-
tion; enfin, aprés Vadhésion de la Grande-Bretagne en 1884, il parait être en rapport avec
cette nouvelle extension de l’entreprise, de porter à neuf le nombre des Membres à élire par

la Conférence générale et à renouveler, d’après un certain tour de rôle, abstraction faite des
deux Membres perpétuels.

ART. 6. Il parait rationnel de restreindre, pour le moment, à dix ans la durée de la
dotation annuelle de la Commission permanente, puisque, d’après l’état actuel de l’œuvre,
on parviendra probablement à développer, dans ces dix ans, le but de toute l'entreprise
d’une manière définitive. À la fin de ce temps, ou bien on saura que l’on peut se passer dé-
sormais d’une organisation spéciale pour l'étude d'ensemble des mesures géodésiques, ou
bien les avantages d’une telle organisation se seront montrés avec une évidence telle, qu'il
sera facile de prolonger la Convention.
