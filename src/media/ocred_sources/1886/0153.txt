129

  
  

Determination de la latitude de Dunkerque, par M. Defforges, faite simultanément
avec une nouvelle détermination de la latitude de Montsouris, par M. Brullard. 1886.

(Cette opération donne l'amplitude de l’arc Dunkerque-Paris, indépendamment des
erreurs des déclinaisons des étoiles observées.)

INTENSITÉ DE LA PESANTEUR

Etude de l'appareil Villarceau, observatoire de Paris, par M. Defforges. 1883.
Mesure de l’intensité de la pesanteur, par M. Defforges, au moyen d’un nouveau pen-
dule construit sur ses indications et par une méthode nouvelle :
Paris et Pic-du-Midi 1884
Pars et lyon... 1660
Paris et Dunkerque . 1886

| ALGÉRIE

GÉODÉSIE

; Triangulation du S.-E. de la province de Constantine, par MM. Brullard et Gueneau’

de Mussy. 1883.
Prolongement du parallele algérien de Bône à Tunis et raccordement avec l’obser-
vatoire de Carthage et la triangulation italienne, par MM. Brullard et Barisien, 1884-1885.
(La triangulation algérienne est donc reliée d’un côté avec l'Espagne et de l’autre
côté avec l'Italie; le pourtour de la Méditerranée est couvert d’un réseau continu de triangles.)
Méridienne de Laghouat, mesure des angles entre le parallèle algérien et Laghouat.
par M. Bassot et les officiers de la Section de Géodésie. 1886.

PUBLICATIONS

Tome XII du Mémorial du dépôt de la querre, comprenant les observations faites sur
la méridienne de France, de 1871 à 1883, entre Perpignan et Paris.

Sous presse, en commun avec l'Espagne, le récit des opérations relatives à la jonc-
tion géodésique et astronomique de l'Algérie avec l'Espagne par-dessus la Méditerranée.

 

COLONEL PERRIER.

AT

  
  
 
    
   
 
   

 

    
   
 
 
 
 
    
   
  
  
   

 
