 

|
|
+

 

35

Norwegen, wobei er auch neue, von ihm angestelle Versuche über terrestrische Refraction
besonders erwähnt, woraus hervorgeht, dass, um diese bis auf ein Procent zu ermitteln,
man die Temperaturabnahme bis auf '/s99) Grad genau kennen müsse. (Siehe Anhane..)

Herr Oberst Zachariae berichtet kurz über die Arbeiten in Dänemark, welche sich
momentan auf Präcisions-Nivellements in Jütland beschränken, mit denen man sechs Mareo-
graphen, drei an der West- und drei an der Ost-Küste aufzustellen, in Verbindung bringen
wird.

Schliesslich erstattet Herr Oberst Schreiber über die von der Königlichen Landes-
aufnahme ın Preussen in den letzten Jahren ausgeführten Arbeiten kurzen Bericht. (Siehe
diesen Bericht, sowohl als den des Herrn Professor Helmert über die Arbeiten des geodä-
tischen Instituts in dem Anhange.)

Der Herr Präsident dankt den Herren Ferrero, Falcoiano, von Kalmar, von Oppol-
zer, von Sterneck, Hennequin, Nell, Fearnley, Zachariae und Schreiber, auf das Lebhafteste
für die interessanten und vielversprechenden Mittheilungen über die Arbeiten in ihren Län-
dern.

Mit Zustimmung der Versammlung bestimmt der Präsident die nächste Sitzung aul
Montag den 1. November 10 Uhr Vormittags.

Die Sitzung wird um 4'/, Uhr geschlossen.

TER

 
