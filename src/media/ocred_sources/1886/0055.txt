nn Pr ete nil

 

ee ee een HON

 

 

ol

an dreissig verschiedenen Punkten, von denen zwölf bis fünfzehn auf Inseln des Oceans
vertheilt sind, solche Beobachtungen sorgfältig und mit grossem Erfolge angestellt hat,
sondern auch für die Zukunft ist zumal durch Mitwirkung der Flotten der verschiedenen Länder
die Sammlung reichen Materials dieser Art zu erhoffen.

Herr Folie wünscht die Aufmerksamkeit der in dieser Richtung thätigen Beobachter
auf die wichtige Frage zu lenken, ob wirklich periodische Schwankungen der Schwere-
Intensität an ein und demselben Orte bestehen, oder nicht? Bei der jetzt fast allgemein
verbreiteten Hypothese von einem flüssigen Kerne der Erde ist es a priori nicht unwahr-
scheinlich, dass in demselben periodische Ebbe- und Fluth-Erscheinungen bestehen, welche
dann nothwendig auch Variationen der Schwere zur Folge haben müssten.

Der Präsident unterbreitet der Conferenz den Vorschlag der Permanenten Commission
betreff der Erneuerung derselben, wonach diese durch Ausscheiden bei Gelegenheit der
dreijährigen Conferenzen, von zunächst fünf durch das Loos zu bestimmenden Mitgliedern,
und alsdann nach weiteren drei Jahren, durch Ausscheiden der vier anderen Mitglieder zu
geschehen habe. Dieser Modus wird von der Versammlung genehmigt, indem die Wieder-
wählbarkeit der ausscheidenden Mitglieder, in der Zukunft wie in der Vergangenheit, als
selbstverständlich bezeichnet wird.

In Betreff der Bestimmung über die Beschlussfähigkeit der Permanenten Commission,
für welche in der Commission ausser den zwei ständigen Mitgliedern noch die Gegenwart
von vier anderen als nothwendig vorgeschlagen war, wird die Bemerkung gemacht, dass es
vorzuziehen sei, in dieser Beziehung keinen Unterschied zwischen den ständigen und den
anderen Mitgliedern zu machen. Dieser Punkt wird an die Permanente Commission zur
Berathung verwiesen.

Herr von Struve wünscht zu wissen, ob einzelne Miglieder, welche, wie z. B. sein
College Herr Stebnitzky, wegen der grossen Entfernungen, und durch andere Arbeiten in
Anspruch genommen, zuweilen verhindert sein könnten, persönlich bei den Versammlungen
der Commission zu erscheinen, sich durch Stellvertreter ersetzen lassen könnten.

Der Herr Präsident glaubt im Sinne der Versammlung diese Frage verneinen zu
müssen, da nach dem allgemeinen Princip : « Delegatus non delegare potest, » ein von der
General-Conferenz in Folge persönlichen Vertrauens derselben gewähltes Mitglied unmöglich
diese Vertrauensstellung auf einen Anderen ohne Weiteres übertragen könne.

Herr von Struve, indem er auf die der Versammlung vorgelegten, vortrefflichen
Pläne zum Neubau eines geodätischen Instituts bei Potsdam zurückkömmt, erlaubt sich,
aus dem Schoosse der Versammlung den Wunsch hier zur Aeusserung zu bringen, dass in
einem besonderen Saale dieses Instituts ein Bild oder eine Büste des unvergesslichen
Generals Baeyer, als Ehrendenkmal für den Begründer der Europäischen Gradmessung
aufgestellt werde.

General Ferrero unterstützt diesen Antrag auf das Lebhafteste und wünscht ihn
zugleich dahin zu erweitern, dass die Büste des General Baeyer von den Bildnissen der

 
