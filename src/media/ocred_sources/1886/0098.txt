 

 

 

74

En sa qualité de commissaire d’Autriche, Herr a activement collaboré aux travaux de
la mesure des degrés; nous mentionnons entre autres ici sa participation aux déterminations
de longitude de Vienne-Fiume et Vienne-Kremsmiinster, et ses mesures de latitude et

 d’azimut sur les stations du Spiglitzer et du Petschner Schneeberg et de Wétrnik.

En 1872, Herr a été nommé directeur du Bureau des poids et mesures d’Autriche en
même temps que conseiller ministériel; c’est surtout à son activité infatigable et à son émi-
nent talent d'organisation qu'est due en Autriche l'introduction relativement rapide et facile
du système des poids et mesures métriques. Parmi ses ouvrages scientifiques, on doit citer
surtout son € Traité de mathématiques supérieures » qui, ayant paru en 4857, a eu plusieurs
éditions. Parmi les papiers du défunt, on a trouvé le manuscrit presque achevé d’un Traité
d'astronomie sphérique, qui est publié par les soins de M. le professeur Ténter.

Apres avoir souffert longtemps d’une pénible maladie, Herr a terminé sa vie ac-
tive le 30 septembre 1884; tous ceux qui ont connu de plus près notre savant collègue,
ont reconnu chez lui, sous une écorce un peu froide et réservée, un caractère d’une bonté
exceptionnelle et d’une remarquable énergie. Ses nombreux amis regrettent profondément la
perte de cet homme de bien, du savant consciencieux et digne, du collègue aimable et sûr.

Il nous reste à mentionner les autres changements qui se sont produits dans le per-
sonnel de l'Association :

À la place de feu M. Villarceau, a été nommé délégué français, M. Tisserand, que
nous avons le plaisir de voir assister à notre Conférence actuelle.

En Danemark, M. le conseiller privé Andrue, ayant donné sa démission comme direc-
teur de la mesure des degrés, a été remplacé par M. le lieutenant-colonel Zachariae, qui lui
est adjoint en même temps comme commissaire danois pour l’Association géodésique.

Enfin, M. le général Burozzi, ayant été nommé chef d’Etat-major de S. M. le roi de
Roumanie, a donné le 16 juin dernier sa démission de commissaire A Association g@é0-
désique.

Dr Ap. Hirscu.

Ce rapport ne soulevant pas de discussion, la parole est donnée à M. le professeur
Helmert pour lire son rapport sur l’activité du Bureau central. Il s'exprime en ces termes :

Depuis la septième Conférence générale à Rome, le Bureau central a publié les deux
volumes suivants :

1. Unification des longitudes par l'adoption d’un méridien initial, et l'introduc-
lion d'une heure universelle. Extrait des comptes-rendus de la septième :
Conférence générale de l'Association géodésique internationale, réunie à
Rome, en octobre 1883, rédigé par les secrétaires A. Hirsch et Th. von
Oppolzer. Publié par le Bureau central de l'Association géodésique inter-
nationale.

 

 
