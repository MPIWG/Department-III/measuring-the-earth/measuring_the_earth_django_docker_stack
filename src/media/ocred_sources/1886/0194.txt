   

 

 

170

Es wurden nivellirt : |
2) im ehemaligen Grossfürstenthume Siebenbürgen doppelt die Linien : |
a) Deés—Magyaros—Szäsz—Régen—Csik— Szereda—Kronstadt ;
b) Alvinez—Hermannstadt—Westen—Fogaras—Kronstadt ; |
c) Kronstadt—Bahnwächterhaus Nr. 307 an der rumänischen Grenze ; |
d) Westen---Vorcontumaz (Rother Thurmpass); die beiden letzteren als Anschlussli- | |
| nien an Rumänien; |

einfach die Linie :

Alvincz—Tovis—A pahida.

An diesen Linien ist der Beobachtungspfeiler in Kronstadt Schlossberg (astronomi-

| scher Punkt I. Ordnung) in das Nivellement einbezogen worden, und wurden durch doppelt
ausgeführte geometrische Anschlussnivellements :

i der trigonometrische Punkt I. Ordnung Közreszhavas (zugleich projectirte astrono-
mische Station Il. Ordnung), so wie

die meteorologischen Stationen zu Gsik-Somlyö und Hermannstadt angeschlossen.

@) in Croatien :

doppelt die Linien :

Rann—Agram,

Agram—Lekenik,

» —Jaska,

ı » —Verbovec.

 

In Agram wurde die trigonometrisch bestimmte Domkirche in das Nivellement ein-
bezogen.

Mit Schluss des Jahres 1885 beträgt die Gesammtläge der theils doppelt, theils ein-
fach nivellirten Linien rund 14600\m und befinden sich auf diesen 2528 Höhenmarken als
Fixpunkte I. Ordnung.

| Zu den bereits in den früheren Berichten aufgeführten Anschlüssen mit den Nach-
I barstaaten kommen nunmehr die obenerwähnten zwei mit Rumänien, und zwar bei Bahn-
Il wächterhaus Nr. 307 nördlich von Predeal und bei Vorcontumaz im Rothen Thurmpasse,
welche jedoch vorläufig nur unsererseits gemessen sind.

Die vorläufige Zusammenstellung der Nivellements um Agram hat gezeigt, dass die
| Erschütterungen der Jahre 1880-81 wirklich nur ein Beben der Erde waren, welches da-
selbst auf den Nivellements-Linien keine merklichen Niveauveränderungen verursachte.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
