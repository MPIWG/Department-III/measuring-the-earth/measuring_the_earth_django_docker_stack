 

66

massen compensirt durch die ungewöhnlich günstigen Witterungs-Verhältnisse, welche in der
zweiten Hälfte September und die ersten drei Wochen des October ne ie Messungen
wesentlich förderten. oo

Im Netze I. Ordnung wurde während dieser Zeit auf 91 Punkten beobachtet.

Das Project für eine Grundlinie nördlich von Kronstadt und deren Entwickelung auf
die Seite 1. Ordnung Väarhegy—Pilisketetö war in Wien nach den im Institute vorhandenen
Original-Aufnahmssectionen ausgearbeitet worden und erwies sich bei der Recognoscirung
als vollkommen zweckentsprechend.

Der Zeichenbau und die Winkelmessung auf den beiden Basisendpunkten sowie aut
den beiden Entwicklungspunkten (Schlossberg bei Kronstadt und Lindenbusch) sind durch-
geführt; die Messung der circa 4130 langen Grundlinie erfolgte erst im nächsten Jahre.

GC. STABILISIRUNG DER TRIGONOMETRISCHEN PUNKTE IN BOSNIEN UND
HERCEGOVINA.

Um die trigonometrischen Punkte, welche durch die in den Jahren 1879-1883 fur
Zwecke der Catastralvermessung im Occupationsgebiete ausgeführte Triangulirung bestimmt
wurden, dauernd zu erhalten, wurde deren Stabilisirung (oberirdische Markirung) angeord-
net und hiefür bestimmt, dass die Punkte des Hauptnetzes (welches theilweise auch für die
Europäische Gradmessung verwendet wird), deren unterirdische Marke aus einem Steine mit
eingegossenem Zinkkegel besteht, oberirdisch durch ein Steinprisma von 170—176 Höhe
und 0"2—0”3 Querschnittsseite zu markiren sind. Der Markstein hat die Inschrift «M. T. »
(Militär-Triangulirung) und die Jahreszahl der Zeichenerrichtung zu erhalten und ist (wenn
Felsboden dies nicht hindert) 0”5 tief in den Boden einzumauern. Auf der oberen horizon-
talen Fläche wird ein Kreuz derart eingemeisselt, dass der Schnittpunkt der beiden das
Kreuz bildenden Linien vertical ober dem Zinkkegel sich befindet.

Alle oberirdischen Markirungen sind durch grosse Stein- oder Erdhaufen vollstän-
dig zu bedecken, um sie vor Beschädigungen möglichst zu bewahren.

Zur Ausführung dieser Stabilisirungsarbeiten wurden drei Officiere bestimmt und
jedem derselben fünf Infanteristen als Militärhandlanger beigegeben. Die Arbeit begann im
südlichen Theile der Herzegovina und wurden — gegen Norden fortschreitend — ausser 904
Nebenpunkten auch 7 Gradmessungspunkte stabilisirt.

D. PRAECISIONS-NIVELLEMENT.

In der abgelaufenen Arbeitsperiode wurde das Nivellement im ehemaligen Grossfür-
stenthume Siebenbürgen fortgesetzt und überdies die Nachmessung einiger Linien in Uroa-
tien vorgenommen, letzteres um zu constatiren, ob durch das Erdbeben vom 9. November
1880 in der Umgebung von Agram Niveauveränderungen hervorgebracht wurden.

22

 

|

 

 
