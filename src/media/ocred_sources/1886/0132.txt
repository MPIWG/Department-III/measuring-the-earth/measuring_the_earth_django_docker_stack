|

   

Sr

ee

 

 

 

 

 

108

 

La Commission accepte cette offre avec remerciements.
M. von Oppolzer, revenant encore une fois sur les huit rapports spéciaux qui ont été

répartis, prend la liberté de rappeler que, parmi ces rapports, la réfraction terrestre ne
figure pas cette fois-ci, et il s’en remet à la Commission de juger si cette lacune ne devrait

pas encore être comblée.

M. Ferrero ne croit pas que la réfraction donne lieu à un travail permanent et né-
cessaire pour la mesure des degrés. Aussi longtemps qu'un savant, qui a fait de la réfraction
une étude spéciale, était membre de la Commission, il paraissait naturel de ajouter au
programme de ses travaux.

M. Bakhuyzen estime qu’on pourrait encore, à l’heure qu’il est, prier M. von Bauern-
feind de se charger de nouveau de ce rapport. Il n’est pas du tout nécessaire que tous les
rapporteurs soient membres de la Commission permanente.

La Commission décide à cet égard que, si M. von Bauernfeind ou tout autre délégué
veut se charger d’un nouveau travail sur la réfraction terrestre, elle lacceptera avec la plus
grande reconnaissance.

La clôture de la séance a lieu à 4 heures !/,.

 

 

 

 
