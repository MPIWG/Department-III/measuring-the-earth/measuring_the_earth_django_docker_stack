89

 

périodiques de marées ne sy fissent sentir el n’eussent nécessairement pour conséquence des
variations de la pesanteur.

M. le Président soumet à la Conférence une proposition de la Commission permanente,
touchant le mode de renouvellement de ses Membres, et d’après lequel on procéderait de la
manière suivante : à l’époque de la première des Conférences triennales, cinq Membres, dé-
signés par le sort, déposeraient leur mandat et les quatre autres Membres en feraient autant
après une nouvelle période de trois ans. Ce mode de faire est approuvé par l’Assemblée, en
stipulant que les Membres sortant de charge seront rééligibles à Pavenir, comme ç’a été le
cas dans le passé.

En ce qui concerne les conditions nécessaires pour que la Commission permanente
puisse délibérer valablement, la Commission avait proposé la présence nécessaire, dans les
séances de celle-ci, de quatre Membres en dehors des deux Membres perpétuels; mais on
fait l'observation qu'il serait préférable de ne pas établir de distinction à cet égard entre les
Membres perpétuels et les Membres temporaires. Ce point est renvoyé à lexamen de la
Commission permanente.

M. de Struve désire savoir si certains Membres, comme par exemple son collègue
M. Stebnitzky, seraient autorisés à se faire remplacer par des suppléants, dans le cas où ils
seraient empêchés d'assister aux assemblées de la Commission, en raison des grandes
distances à parcourir ou de travaux absorbant tout leur temps.

M. le Président pense être Vinterpréte des sentiments de PAssemblée en répondant
négativement aA la question posée, parce que, d’après le principe général : « Delegatus non
delegare potest, » un membre élu par la Conférence générale, en raison de la confiance per-
sonnelle qu’il lui inspire, ne peut, de son chef, conférer à un autre cette marque de confiance.

M. de Struve revenant sur les plans présentés à l’Assemblée pour la construction
nouvelle d’un Institut géodésique à Potsdam, émet le vœu qu'il soit placé dans une salle
particulière de cet édifice un portrait ou un buste du général Baeyer, de glorieuse mémoire,
comme un monument élevé en l'honneur du créateur de l'Association européenne pour la
mesure des degrés.

M. le général Ferrero appuie vivement cette proposition et désire en outre que le buste
du général Baeyer soit entouré des portraits des géodésiens les plus éminents d’autres pays et
d’autres époques. De cette façon, on créerait une sorte de Panthéon géodésique, et Phom-
mage qu'on rendrait au général Baeyer en serait encore rehaussé.

Cette idée est reçue chaleureusement et appuyée par l'Assemblée.

M. Helmert est heureux de pouvoir annoncer à la Conférence que, d’après les plans

adoptés, une salle convenable est disponible pour un pareil but.

La séance est interrompue par le Président 4 12 heures !/,, et la séance de relevée
fixée à 2 heures.

12

 

 
