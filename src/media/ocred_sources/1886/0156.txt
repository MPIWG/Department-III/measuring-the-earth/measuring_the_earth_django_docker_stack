 

SS

 

SSS

i
D
N
Îl

 

 

|
|
1
}
i.
À

 

132

 

repères disparus et publie des errata rectifiant les indications devenues inexactes des cata-
logues de nivellement.
Paris, le 18 octohre 1886.

 

ro
SE

 

 

 

 

L’Ingénieur des Mines, Secrétaire du Comité du Nivellement,

Cu. LALLEMAND. « I

L’Inspecteur general des Ponts et Chaussées, en retraite, [
Vice-Président de la Commission du Nivellement, | I
MARX. ru

Annexe V?. | |
NOTE a

SUR - | I

Ä i

LE PRINCIPE FONDAMENTAL DE LA THEORIE DU NIVELLENENT |
Par M. CH. LALLEMAND | I

m

La théorie actuelle du nivellement géométrique est basée sur le parallélisme des | |
surfaces de niveau !, lequel suppose lui-même la constance de la pesanteur dans toute l'éten- | |
à 9 oe
due du globe ?. ô À
E

1 Surfaces (définies par Clairaut et étudiées par Laplace) normales en tous leurs points à la direc- I

tion du fil à plomb, et sur lesquelles, par suite, un déplacement quelconque s’effectue sans travail de la pesanteur.
? Ceci se démontre facilement. En effet : |

Le travail de la pesanteur, quand on suit un contour fermé, est nul. En d’autres termes, ce travail, I

entre deux points donnés, est indépendant du chemin qui les réunit. F
Soient AB, ab deux surfaces de niveau voisines ; À et h’ leur écar- k

tement en A et en B; g et g l'intensité de la pesanteur aux mêmes points. [

Le travail de la pesanteur de A en 0, en passant par a, est gh; x i

en passant par B, gle | I

Or: |

127 |

dh — GW \ à

\ À BE

D'où : | |

he g
h' g

 

L’ecartement varie donc en raison inverse de la pesanteur. Ceile-ci, par suite, devrait Gtre cons-
tante pour que les surfaces de niveau fussent équidistantes en tous leurs points, c'est-à-dire parallèles.

; Nous n’avons pas hesité de recevoir a cette place l’interessante notice de M. Lallemand, bien qu'elle sorte
du caractere des rapports des differents pays, tels qu’ils sont publies d’ordinaire dans nos Annexes. Il va sans dire
qwelle est publiée sous la responsabilité exclusive de l’auteur. Le Secrétaire perpétuel, A. H.

 
