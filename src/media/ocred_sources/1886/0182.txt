   
 
   
      
   
   
   
     
   
 
    
   

158

  

Hiebei ergab sich auf dem Lebhegy an einem der Beobachtungstage eine sehr auffällige
Erscheinung in Bezug auf die Stammdrehung. Bei hohen Instrumentenständen kommen zwar solche +
Drehungen in höherem oder geringerem Grade immer vor (weshalb dann auch stets nur zwei bis
drei Objecte in einem Satze eingestellt werden), nehmen aber. gewöhnlich einen ziemlich regel-
mässigen Verlauf.

Auf dem Lebhegy jedoch war die Stammdrehung so gering, dass der Beobachter stets alle
fünf Objecte in einen Satz vereinigen konnte. So geschah es auch am 2. November und ergaben
sich dabei bis 44 10™ Nm. sehr gute Messungsresultate. Der um 4" 11” (kurz vor Sonnenuntergang)
begonnene Satz jedoch zeigte in sehr auffallender Weise eine ganz plötzlich auftretende, unge-
wöhnlich rasche Stammdrehung, wie aus der nachstehenden Zusammenstellung ersichtlich ist :

 

 

NAME DES OBJECTES

Westlich. Oestlicher

Johannes- Basis- Kalvarıen- Basıs- Baj
berg. endpunkt berg. endpunkt temetes.

1 u a 1 u

     
           
     
          

 

Wahrer Wertdes Winkels. 2... 1. 0... 0 36 18 40 34

Beobachteter Wert, Messung von links gegen rechts.. . 0 WO 22 19 29 16
Zeitdauer der Beobachtung 10 Minuten.

Beobachteter Wert, Messung von rechts gegen links. . 0 LO 22 47 LA
Zeitdauer der Beobachtung 5 Minuten. in

Fehler bei der Messung von links gegen rechts . . . — 5 —A11 —18

Fehler bei der Messung von rechts gegen links

er

Zwischen den Beobachtungen gegen rechts und gegen links war ein Intervall von circa 1/,

        
        
   
   
 
   
 
 
      
   
   
   
   
   
   
 
    

Stunde.

In dieser Zusammenstellung sind von den Winkeln nur die Sekunden angegeben, die Grade
und Minuten aber (als hier nicht in Betracht kommend) weggelassen. Die Stammdrehung hatte nach
dem Vorstehenden plötzlich den Betrag von sehr nahe 2” per Zeitminute angenommen.

D. HERRICHTUNG DES BASISTERRAINS UND MESSUNG DER GRUNDLINIEN.

  

Das Terrain ist für den Zweck einer Basismessung sehr günstig. Der Boden besteht
— mit Ausnahme einer beiläufig 300m langen Strecke in der Nähe des Westlichen Basisend-
punktes — aus lehmigem (plastischem) Sand ohne Schotter und bildet die denkbar beste
Unterlage für den Messapparat. Die Unebenheiten des Bodens sind nicht sehr beträchtlich
und wurden theils durch einige Einschnitte, theils durch Aufwerfen von Dämmen (deren
höchster auf eine kurze Strecke 1"7 Höhe hatte) beseitigt.

Die auf diese Weise hergestellte Grundlinie hat in ihrem Profile vom östlichen Basis-
punkte ausgehend bis auf etwa 350m von der Basismitte ein Gefälle von fast 13m, bleibt dann
auf einer Strecke von 1050m nahezu horizontal und steigt endlich bis zum westlichen End-
punkte wieder um 41m,

Die Basis wurde in sechs Theile, das fünfte und sechste Sechstel überdies noch in

 

 

 

 

 

 

 

 

 

 

 

 

 
