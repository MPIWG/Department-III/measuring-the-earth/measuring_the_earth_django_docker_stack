203

sumpfigen und mit Waldungen bedeckten Gegenden Polygonal-Linien mit Nivellir-Theodoliten
gemessen. Dergleichen Orts-Bestimmungen haben bei den Aufnahmen in Finland, Polen und
im Fluss-Gebiete der Prypet eine vortheilhafte Anwendung gefunden.

CG. PENDELBEOBACHTUNGEN.

Die Schwere-Bestimmungen im Kaukasus, mit dem der kais. russ. Academie der
Wissenschaften gehörigen Repsold’schen Reversionspendel, wurden von Oberst Kuhlberg
weiter fortgesetzt. Im Jahre 1883 bestimmte derselbe die Länge des Secunden-Pendels in
 Schemacha und Baku. Nach Beifügung der früher (1881) von Hrn. Kuhlberg unmittelbar be-
stimmten Correction für das Mitschwingen des Stativs (+ 0,0650 Linien) erhält man folgende
auf das Meeresniveau reducierte Endresultate (public. in Astr. Nachr. No 2593) :

 

Höhe in engl. |Länge d. Secunden pendels in Linien der)

Breite. L. v. Aten Mer. 5
Fuss. Fortin. Toise bei + 13,1 R.

1

Baku .. ... 40 22 0 6730 9 440,1960

Schemacha . 40 37 45 66 18 20 440,2198 |

Obgleich die Gorrection unseres academischen Reversionspendels für das Mitschwingen
des Stativs durch die Versuche des Ob. Kuhlberg (1881) unmittelbar abgeleitet wurde, hielt
ich es für angemessen, diese Correction noch aus der Schwingungsdauer eines leichten Pen-
dels abzuleiten. Zu diesem Zweck bestellte ich im Jahre 1884 ein leichtes Pendel in der
Werkstätte von Repsold in Hamburg, und nach Empfang desselben unternahm Oberst Kuhl-
berg im Jahre 1885 wiederholt seine Untersuchungen über das Mitschwingen des Stativs im
Gebäude des physikalischen Observatoriums zu Tiflis. Das diesmal von Oberst Kuhlberg er-
haltene Resultat (Astr. Nachr. N° 2689) stimmt ganz genau mit demjenigen von 1881 überein.
Somit besitzen wir gegenwärlig drei von einander unabhängige Bestimmungen der Correction:
des acad. Pendelapparates, nämlich:

1. Nach Untersuchungen von Havyside in Kew + 0,0631 Pariser-Linien.
2. » Kuhlberg » Tiflis 1881) + 0,0650 »
3. » »  » » (1885) + 0,0665 »

D. RECHENARBEITEN.

a) Die Berechnung und Bearbeitung der von uns in Bulgarien ausgeführten Trian-
gulirung ist von dem ehemaligen Chef der Triangulation, General-Major Lebedew, vollendet.

 
