 

 

i
5

liche; — immerhin wird dieser Ruhm vor den folgenden Jahrhunderten allmäh-
lich erblassen, — der Ruhm aber, der ihm bleiben wird, gründet sich auf seine
Organisation der wissenschaftlichen Arbeit innerhalb der Staaten nicht allein,
sondern vor Allem im Verhältniss von Staat zu Staat, durch das Zusammen-
schliessen, sei es zur Lösung einzelner Aufgaben, wie zur Erforschung der Son-
nenfinsternisse, des Durchgangs von Planeten, des Erdmagnelismus, sei es zur
Erfüllung dauernder Zwecke.

Hier hat die Konferenz der europäischen Gradmessung die Bahn gebrochen,
den Weg geebnet für die grossen internationalen Schöpfungen zur Feststellung der
Mass- und Gewichtseinheiten, der elektrischen Maasseinheiten, des Post- und Tele-
graphenvereins. Als bei den Verhandlungen in Rom der Begründer Ihrer Organi-
sation gefeiert werden sollte, konnte es nicht sinniger und zutreffender geschehen,
als durch die Inschrift der Medaille, welche die italienische Kommission mit Ge-
nehmigung der Königlichen italienischen Regierung zu Ehren des Generals
Baeyer hatte schlagen lassen. € Nalionum sodalicium excitavil » so lauteten die
Worte; er war der Schöpfer der internationalen Vereinigung. Richtig hiermit
ist gekennzeichnet das höchste Verdienst und der unauslöschliche Ruhm eines
langen, den erhabensten Zielen der Wissenschaft rastlos gewidmeten Lebens.
Dankbar legen wir den Kranz der Anerkennung und Verehrung auf dem Grabe
des Verewigten nieder. Sein Scheiden ist verklärt durch das Bewusstsein, dass
das Werk, das er geschaffen, mit ihm nicht vergehen, sonder dauern und immer
mächtiger sich entfalten wird.

Einen bedeutungsvollen Schritt nach der weiteren Ausgestaltung der Verei-
nigung zu thun, dahin sind die Vorschläge der preussischen Regierung gerichtet.
Nicht allein scheint der Zeitpunkt gekommen, wo die thatsächliche Erweiterung der
Aufgabe von der «Gradmessung » zur « Erdmessung » offiziell anerkannt werden
darf, sondern darüber hinaus drängen die bisherigen Erfahrungen dazu, den in-
ternationalen Charakter der Vereinigung stärker in die Erscheinung treten zu
lassen. Kann das Ziel schliesslich vollständig nur durch das Zusammenwirken
aller Staaten erreicht werden, so begrüssen wir doch dankbar den Beitritt jedes
neuen “Staates. Weiter aber wird die Organisation der Vereinigung, das Gentral-.
bureau wie die Permanente Kommission mehr den internationalen Beziehungen
anzupassen, die finanzielle Selbständigkeit der letzteren durch Beiträge der Staa-
ten sicher zu stellen und der Permanenten Kommission eine wirksame Leitung
des Gentralbureaus zu gewähren sein.

Von Bedeutung für Ihre Entschliessungen kann es sich erweisen, dass gegen-
wärlig das preussische geodätische Institut einer durchgreifenden Reorganisation
unterzogen wird. In Folge der schärferen Abgrenzung seiner Aufgaben wird es
seine volle Kraft rein wissenschaftlichen Zielen widmen, und würde, wenn ihm die
Stellung des Gentralbureaus von Neuem übertragen werden sollte, mehr denn je
befähigt sein, die Messungsergebnisse der einzelnen Staaten zusammenzufassen

 
