 

 

 

 

 

 

150

  

vielleicht grösseren Unsicherheit unterliegt als es voraussichtlich der Fall sein wird mit
ihrer geodätisch zu berechnenden Länge.

Dann kommt die Reihe an die astronomischen Publicationen. Hoffentlich werden die
zur Gradmessung jährlich bewilligten Mittel, welche die Nationalversammlung in den letzten
Jahren nur widerstrebend und sehr bedingt eingeräumt hat, nicht eingezogen werden, bevor
die davon bedingte Berechnung und Publication sämmtlicher für die Gradmessung ausge-
führten astronomischen Bestimmungen in den nächsten zwei bis drei Jahren vollzogen wer-
den kann.

Mareographen. III Heft « Vandslandsobservationer » ist publieirt. Es enthält: Regis-
trirte Beobachtungen bei Oscarsborg für 1880-81, bei Stavanger, Bergen, Kabelväg, Vardö
für 1883 und als Anhang Hochwasserbeobachtungen bei Hammerfest in den Jahren 4882 und
1883 und bei Carlsö in 1884. Unter der Presse ist IV Heft, worin die Beobachtungen bei
Christiania für die zweite Hälfte von 1885, Oscarsborg 1882, Stavanger, Bergen, Kabelväg
und Vardö 1884-85 vorliegen werden.

Eine neue Station ist in Arendal seit Anfang dieses Jahres in Wirksamkeit.

Im Sommer 1885 wurden sämmtliche Mareograph-Stationen von Oberstlieutenant
Haffner inspieirt. An jeder der Stationen Arendal, Stavanger, Bergen, Kabelvag, Hammer-
fest, Vardö und Throndhjem liess er bei dieser Gelegenheit in der nächsten vom Meere be-
spülten möglichst verticalen Felsenwand eine Höhenmarke nebst verticaler in Centimeter
getheilter Scale — um für den Mareographen als unwandelbarer Referenzpunkt zu dienen —
in einer im nächsten Heft « Vandstandsobservationer » näher zu beschreibenden Weise an-
bringen. In Ghristiania, Christiansund und Tromsö ist für eine ähnliche Vorrichtung die
passende Localität gewählt worden.

Für Präcisionsnivellements sind allerdings die nöthigen Apparate angeschafft; was
aber die Ausführung betrifft, werden wir bessere Zeiten abwarten müssen. Ein darauf be-
züglicher Antrag ist zweimal von der Nationalversammlung abgelehnt worden.

Bestimmungen der Schwere sind bei uns noch gar nicht vorgenommen. Dass aber
auch solche für die physische Geographie so wichtigen und in einem Gebirgslande besonders
interessanten Untersuchungen durch den kräftigen Impuls der internationalen Verbindung
werden in nicht gar zu ferner Zukunft hervorgerufen werden müssen, ist um so weniger zu
bezweifeln, weil jedenfalls Mittel dazu vorhanden sein werden, auch wenn die bisherige Be-
willigung zu den Gradmessungsarbeiten eingezogen werden sollte.

Die Lage der Sternwarte in Ghristiania — mit freier Aussicht nach zwei um mehr
als 20 und 30 kilometer entfernten kahlen Bergrücken — eignet sich gut für Untersuchungen
über die terrestrische Refraction. Ks ist daher auf meinen Antrag eine kleine Summe bewil-
ligt worden damit planmässige Beobachtungen und Untersuchungen dieser Art durch vor-
läufige Versuche, Construction zweckmässiger Apparate, u. s. w. in bester Weise vorbereitet
werden können. Vor allem gilt es bequeme und schnellwirkende Mittel zu beschaffen um
den Unterschied der in (10m) verschiedener Höhe gleichzeitig stattfindenden Temperatur

 

 
