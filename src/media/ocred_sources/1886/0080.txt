 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

96

 

  
    
  
 
 
  
   
 
    
   
   
    
  
  
  
  

Regel zwei bis drei Wochen dauern, werden zum grossen Theile mit der kritischen Unter-
suchung der im Bureau geleisteten, wissenschaftlichen Arbeiten, sowie mit der Aufstellung
eines detaillirten Arbeitsplanes für das nächste Jahr ausgefüllt. Ein ähnliches Verhältniss
würde sich jedenfalls auch im Schoosse unserer wissenschaftlichen Vereinigung nützlich er-
weisen, und da hier, wie dort, der Direktor des Bureau’s zugleich Mitglied der Commission
ist, so liegt in einem solchen Verhältnisse auch keinerlei, einer wissenschaftlichen An-
stalt nicht zuzumuthende Bevormundung.

Herr Foerster giebt in seinem Namen, sowohl als in dem von Herrn Professor Hel-
mert, die Erklärung ab, dass das von den Herren Ferrero und Hirsch skizzirte Verhältniss
zwischen der Permanenten Commission und dem Centralbureau vollständig nicht nur ihren
Ideen entspreche, sondern auch von Seiten des Herrn Ministers als das richtige angesehen
werde.

Die Permanente Commission beschliesst daher : Die wissenschaftliche Oberleitung
der Erdmessung wird von der Permanenten Commission ausgeübt; speciell wird dieselbe in
ihren Jahresvereinigungen die nöthige Zeit auf die kritische Untersuchung und systematische
Zusammenfassung der im Laufe des letzten Jahres geleisteten Erdmessungarbeiten verwen-
den, um, soweit als es möglich ist, die daraus sich ergebenden Resultate abzuleiten und die
etwa noch vorhandenen Lücken den betreffenden Staaten zur Ausfüllung zu empfehlen. Bei
dieser Arbeit wird das Centralbureau durch vorbereitende Zusammenfassung des Beobach-
tungsmaleriales der Permanenten Commission hülfreich zur Hand gehen. Ausserdem wird
künftig die Permanente Commission in jeder ihrer Vereinigungen den ihr vom Centralbu-
reau unter breiteten Arbeitsplan für das nächste Jahr feststellen.

qe

l

Der Präsident erinnert daran, dass die Commission sich nochmals mit den zur |
Gültigkeit ihrer Beschlüsse nöthigen Majoritätsbedingungen zu befassen habe. |
Herr Hirsch schlägt vor, dass man, ohne Unterscheidung der ständigen Mitglieder,

einfach die Gegenwart von sechs Mitgliedern der Permanenten Commission als Bedingung
aufstellen möge; höchstens wäre est vielleicht angezeigt, unter diesen die Anwesenheit, sei |
es des Präsidenten oder des Vicepräsidenten, als nothwendig zu erklären. |

Herr von Oppolzer ist mit dem ersten Vorschlage des Herrn Hirsch einverstanden,
meint aber, dass man die letzterwähnte Beschränkung nicht einführen solle, da sonst mög-
licher Weise die Commission im Falle der Krankheit ihrer Präsidenten beschlussunfähig wer-
den könne.

Herr Faye theilt ebenfalls diese letzte Ansicht und glaubt, dass im Falle der Verhin- "oa
derung des Präsidenten, sowohl wie des Vicepräsidenten, der erstere, wie in allen ähnlichen
Fällen, seine Autorität einfach an ein anderes Mitglied der Commission zu übertragen das
Recht habe.

Die Commission bestimmt also als einzige Bedingung für die Gültigkeit ihrer Be-
schlüsse, die Gegenwart von sechs ihrer Mitglieder.

Professor Hirsch theilt eine Depesche mit, welche er gestern von llerrn Bischoffsheim
