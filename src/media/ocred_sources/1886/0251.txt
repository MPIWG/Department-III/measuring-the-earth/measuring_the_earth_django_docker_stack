997

darf. Hierfür spricht auch die durchgeführte Untersuchung nach dem Gauss’schen Fehler-
vertheilungsgesetz.

Schliesslich ist von mir noch die Genauigkeit des Nivellements im flachen und im
gebirgigen Terrain dadurch untersucht worden, dass ich die Nivellementslinien nach beiden
Terrainlagen ordnete und für die so erhaltenen Serien die wahrscheinlichen Kilometerfehler
berechnete. Dabei hat sich ergeben für

flaches Terrain hügliges Terrain

mm mm
r—= + 9,49 2209)

also nahe gleiche Genauigkeit, was wohl zunächst auf den Umstand zurückzuführen sein
dürfte, dass die optische Kraft der von Weisbach in Anwendung gebrachten Nivellirinstru-
mente eine grössere Zielweite als 75 Schritt — 60m nicht gestatteten. Im sächsischen Nivelle-
ment hat sich sonach die der Berechnung zu Grunde gelegte Annahme über das Wachsen der
mitileren und der wahrscheinlichen Nivellementsfehler proportional der Quadratwurzel aus
der Streckenlänge als zulässig erwiesen.

Die Höhen der in den Hauptlinien mit nivellirten Zwischenpunkte, sowie die in
den an zwei Hauptpunkte sich anschliessenden Nebenlinien befindlichen Punkte wurden
unter der Bedingung ausgeglichen (eingeschaltet), dass die bereits durch die Hauptausglei-
chung gefundenen Höhen der Endpunkte dieser Linien ungeändert blieben.

In der Publication giebt alsdann Tabelle I eine Zusammenstellung der Nivellements-
linien mit den Öriginalzahlen beider Nivellements und die Berechnung der definitiven Höhen,
Tabelle II die Zusammenstellung der fünfzig Polygone mit ihren Abschlüssen und endlich
Tabelle III die « Alphabetische Zusammenstellung » der durch das gesammte Landesnivelle-.
ment bestimmten Höhen, und zwar sowohl in Bezug auf das Mittelwasser der Ostsee, als in
Bezug auf Berliner Normal-Null.

Dresden, im Januar 1887.

A. NAGEL.

 
