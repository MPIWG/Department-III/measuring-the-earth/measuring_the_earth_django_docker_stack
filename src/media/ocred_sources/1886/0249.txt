Die Summe der Quadrate der übrigbleibenden Fehler und der daraus abgeleiteten
mittleren Fehler der Beobachtungen ergiebt die Werthe

[vo] = 0,159425 [on] — 0,152311
m — + 0225 m' = + 0'295

Die Uebereinstimmung der beiden mittleren Fehler beweist, dass die Beobachtungen
im Meridian und in der Richtung der geographischen Längen als gleich genau zu betrachten
sind.

Mit Hilfe dieser mittleren Fehler und der durch die Auflösung der Gewichtsglei-
chungen erhaltenen reeiproken Gewichte sind die den Grössen &, y, z oben beigefügten
mittleren Fehler berechnet worden.

Die Lothabweichungen der sechs Punkte berechnen sich nach den Formeln

o —o=2+D.cosa.y+Dsina.z
W— = a'+D.cosa.y’+D sine.2’

£

worin für æ, y, z und #’, y’, z die oben gefundenen Werthe zu substituiren sind.

Unter Beifügung ihrer mittleren Fehler finden sich hiernach die wahrscheinlichsten
Werthe der Lothabweichungen :

a zo 2 STATION X — 2

— 0, 198 + 0, 099 Leipzig Be 0,270 2 0,09
+ 0,182 + 0,184 Wachauer Denkstein — 2,593 + 0,184
— 0744 = 0,167 Grenzhübel . . . 203741 = 0,167
— 0,672 + 0,161 Wachberg . . . + 1,845 + 0,161
+ 0,094 + 0,161 Markstein . . . + 0,638 & 0,161
+ 0,684 + 0,173 Schwarzeberg . . :— 1,896 + 0,173

Von den aus der Ausgleichung hervorgegangenen Azimuthen A und A’ der Abscissen-
achsen ausgehend lassen sich in Verbindung mit den Grössen G und C, d. i. dem Zuwachs
der Lothabweichungsdifferenzen pro Kilometer, die bezüglichen Verhältnisse leicht gra-
phisch darstellen.

Eine solche Darstellung ist in Folge der Anregung des Herrn Prof. Dr. Th. Albrecht
in Berlin auf dem beiliegenden Kärtchen gegeben'; in dasselbe sind die Parallelen mit ihrer
Achse für die Lothabweichungen im Meridian von 0.1 zu 01 roth und für die Lothabweichun-
gen in der Richtung der geographischen Längen von 0 9 zu 0.2 {blau eingetragen. Dieses

1Siehe am Schluss des Bandes. N H.
29

 
