 

86

Sir Robert Ball, Director der Sternwarte in Cambridge.

Herr F. Stratton, Oajus College, in Cambridge.

Herr Dr. A. Schuster, Honorär Professor der Universität in Manchester, Präsident der Seis-
mologischen Gesellschaft.

Herr B. F. E. Keeling, Director der Sternwarte in Helwan, Aegypten.

Herr Ronald W. Giblin, Director der geodätischen Arbeiten in Siam.

Herr General v. Stubendorf’, früher Delegierter von Russland bei der Erdmessung.

Herr Prof, E. Jäderin der Polytechnischen Schule in Stockholm.

Herr Dr. V. Carlheim Gyliensköld, Secretär der Commission für die schwedische Expedition
zur Ausmessung eines Meridianbogens in Spitzbergen.

Herr Hauptmann @. Perrier des militär-gographischen Dienstes in Paris.

Herr HZ. A. Deslandres, Director der Sternwarte in Meudon.

Herr Dr. Th. Wittram, Astronom an der Sternwarte in Pulkova.

Der Päsident dankt den Herrn Seeretär für seinen Bericht und fordert die Delegirten
auf zur Wahl eines Vice-präsidenten für die Periode 1909—1916, da Herr Darwin, nach
Art. 5 der Uebereinkunft, nur provisorisch an Stelle des verstorbenen Herrn Zachariae zum
Vice-präsidenten gewählt worden ist. Vierzehn Staaten betheiligen sich an der Wahl und da
alle Stimmen auf Sir George Darwin ausgebracht sind, ist er als Vice-präsident gewählt.

Auf Anfrage des Präsidenten erklärt Herr Darwin sich bereit die Wahl anzunehmen,
und dankt seinen Kollegen für das ihm geschenkte Zutrauen.

Nach der vorläufigen Tagesordnung fordert der Präsident die Delegirten auf die Mit-
glieder der Finanzkommission zu wählen. Er schlägt folgende Herren vor:

HH. Forkster,
PoıncAR&,
ÜELORIA,
Weiss,
GAUTIER,
Tırrmann,
ARTAMONOFF,

Dieser Vorschlag wird einstimmig genehmigt.
Der Präsident ertheilt Herrn Helmert das Wort für seinen Bericht über die Thätig-

keit des Zentralbureaus seit der 15en algemeinen Konferenz in Budapest in 1906.
Herr Helmert verliest folgenden Bericht.

Meine Herren,
Seit der Konferenz in Budapest habe ich den in der Uebereinkunft der Internatio-

nalen Erdmessung vorgeschriebenen Jahresbericht über die Tätigkeit des Zentralbureaus für
die Jahre 1906, 1907 und 1908 am Schlusse jedes Jahres erstattet und den Herren Dele-

 

ä
!
°
:

DI)

nn

Lak md om ı

dl

ku aa mm

um he a

2.4 sumasaaahiem. wann Bu |

 
