 

1

62

NT ET TeET

M. le President en remerciant M. le rapporteur met aux voix, d’abord la premiere
partie de la proposition contenue dans le rapport: d’approuver les comptes de l’Association
pour l’exercice de 1906, 1907 et 1908. et de donner d&charge pleine et entire & M. le
Directeur du Bureau central, et d’approuver la haute direction de la gestion des affaires
administratives par le Bureau de l’Association.

/ Oetie proposition est adoptee.

M. le President met ensuite aux voix les autres propositions de la commission des ;

finances, qui sont egalement adoptees.
 M. le President donne ensuite la parole ä M. le Sceerdiaire perpetuel, qui fait la lecture
des differents veux presentes au Bureau. -

I. La 16me Oonference generale de l’Association g6odesique internationale exprime sa
reconnaissance aux observateurs charges du service des latitudes dans les stations
internationales, pour leur zöle et leur devouement; elle pr6sente en m&me temps
ses remerciements particuliers aux autorites qui, dans les differents pays, surveil-
lent les stations internationales de latitude, et permettent ainsi la continuation in-
interrompue des observations. Le Bureau de l’Association est pri& d’exprimer ces
remerciements &:

nl

Ina

ak nn

1%, La Commission geodesique italienne & Milan,

2°. Coast and geodetic Survey ä& Washington,

8°, M. le Professeur Porter & Cincinnati,

4°, La Commission g6odesique du Japon & Tokio,

5°. La section topographique militaire de l’Etat-major göneral russe ä St. Petersbourg,
5°. M. le Professeur Porro & La Plata,

a. 7%. M. le Professeur Cooke & Perth.

| La Üonference remercie aussi M. Backlund, directeur de l’observatoire de Poulkovo,
; pour l’organisation qu’il a faite des travaux de coop6ration pour le service des latitudes.

 

Il. La Conference exprime sa grande satisfaction de l’entree de la Röpublique Argentine
dans l’Association et prie le Gouvernement de cet Etat de bien vouloir faire ex6cuter i
des mesures de la pesanteur sur son territoire, specialement dans les rögions meri- :
dionales; ces mesures devant prösenter un interdt tout partieulier pour la recherche
de ia figure de la terre dans l’hemisphöre austral.

II. La Conference prie les services g&od6siques de la Russie et des Indes de prendre
en consideration le fait, qu’il serait d’une sup6erieure importance pour les &tudes de
la distribution des masses continentales dans l’Asie ceutrale, de rechercher les moyens
d’etablir une triangulation sur le versant ouest du plateau central de l’Asie, afin de
relier entre elles la triangulation des Indes et la triangulation russe du Turkestan.

IV. La Conference emet le vau que des dömarches directes ou indirectes soient faites,
pour amener l’adhesion des grands &tais de la Chine et du Bresil & l’Association
göodesique internationale.

V. L’Association geodesique internationale ömet le vau,

 

Te

SEES ee

asundinenn we va |

 

 
