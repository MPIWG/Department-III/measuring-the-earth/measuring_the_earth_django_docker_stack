 

156

ist aber auch dadurch nach den bisherigen Erfahrungen nicht zu erwarten. Jedenfalls liegt
von der Berliner Basis ein umfangreiches Beobachtungsmaterial vor, welches im Laufe des
Winters noch manche Ergänzung erfahren und nach seiner vollständigen Durcharbeitung
zur Klärung der die Drahtmessungen betreffenden Fragen beitragen dürfte. Eine wertvolle
Förderung ist auch dadurch zu erwarten, dass das Königlich Preussische Geodätische Institut
unabhängig von den Arbeiten der Landesaufnahme die Berliner Basis auch mit ihren Drähten
und zwar in einer anderen Anordnung gemessen hat.

Soweit sich bis jetzt die Ergebnisse der Arbeiten auf der Berliner Basis übersehen
lassen, bestätigen sie das bereits früher hier gewonnene Urteil, dass die Drahtmessungen für
praktische Zwecke unter gewissen Vorsichtsmassregeln vollkommen ausreichen, dass sie aber
den Forderungen von Präzisionsarbeiten ohne weiteres auch heute noch nicht genügen. Un-
leugbar sind noch gewisse Einflüsse vorhanden, welche nicht aufgeklärt sind und daher auch
nicht in Rechnung gestellt werden können. Auch der Wert der Drahtkorrektionen an sich
ist nach unseren Messungen fortgesetzter Aenderung unterworfen; wahrscheinlich verläuft
diese für jeden Draht in gewissem Masse gesetzmässig, für Drähte verschiedener Provenienz
wird sie jedenfalls verschieden sein. Am bedenklichsten erscheint mir der noch nicht auf-

 geklärte Widerspruch zwischen den Drahtkorrektionen, wie sie sich aus Richung und Feld-

messung ergeben, und der nach unseren auf 2 Grundlinien gewonnenen Erfahrungen für
unsere Drähte rund !/,g0000 der Länge beträgt.

London, den 22.9. 09. VON BERTRAB.

Oberst und Abteilungschef im grossen
Generalstabe.

 

1. an MA uk

ah

ko ll

Mh Ah mn nn

3 ak Incann msndih dad aa

2. sera, an BA

 
