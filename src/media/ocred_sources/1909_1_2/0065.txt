m

me Teer yore

iii

er

en ren meer

59

en mer. Üette idee, soutenue par M. Bouquet de la Grye, a &t6 mise & l’etude et sera pro-
chainement r6alisee. Gräce au concours de M. Baillaud, Directeur de l’observatoire de Paris,
et de M. le commandant Ferrier, les voies et moyens ont &t& etudies par la commission
interministerielle de Telegraphie sans fil; les ressources financieres necessaires, d’ailleurs
minimes, ont &te demandees au Parlement et seront sans nul doute accordees. L’heure sera
transmise de l’observatoire & la Tour Eiffel, qui doit &mettre les signaux chaque jour & midi.
Des pr&cautions sont prises pour 6viter toute confusion avec les signaux ordinaires.

Il existe des appareils de r&:eption, dont la construction est simple et le prix mi-

nime, de sorte qu’ils pourront &tre acquis par des navires de faible tonnage, et depourvus
de chronomöätre.

\

Les signaux pourront &tre pergus & une grande distance dans l’Atlantique et la Me-
diterranee. On ne peut pas compter, par les procedes en question, atteindre une precision
superieure & une !/, seconde, ou une seconde, ce qui.suffit cependant pour le but pratique
que nous avons en vue. :

Mais nous nous sommes preoccupes egalement de rendre la telegraphie sans fil ap-
plicable & un but scientifique, par exemple & la determination d’une difference de longitude.
Voici & quelle occasion. Sur l’initialive de M. Kyinitis, on a etudie un projet de mesure de
la difference Paris-Athönes. Oette mesure sera faite d’une part par cäble, car on ne saurait
abandonner les anciennes methodes pour se fier uniquement & une methode nouvelle qui n’a
pas encore &t& experimentee, mais on voudrait la doubler par une mesure par telegraphie sans fil.

Le gouvernement grec se propose en eflet d’installer une station radiotelegraphique
sur un point culminant des environs d’Athönes.

A cette distance, on ne peut employer le cohereur & limaille, qui permettait l’emploi
des methodes d’enregistrement; il faut se servir des detecteurs &lectrolytiques et du telephone.
De lä naitront sans doute de grandes diflicultes.

MM. Claude et Driencourt ont r&cemment determind la difference Paris-Brest avec le
telephone, mais par la telegraphie ordinaire.

Nous voudrions, pour experimenter la mäthode nouvelle, faire recommencer ai diffe-
rence Paris-Brest par les m&mes observateurs qui sont habitues ä l’emploi du telephone,
mais avec la telegraphie sans fil entre le poste de la Tour Eiffel et celui d’Ouessant. O’est ce
que l’on fera, je l’espöre, des que M. Driencourt sera revenu de la mission qu’il accomplit
actuellement, et qui doit prendre fin au mois d’Octobre.

M. Foersiter communique & la Conference qu’en Allemagne on se propose de creer
une organisation, analogue & celle dont M. Poincard a parle&, au moyen de signaux envoyes
par la telögraphie sans fil de la station de Nauen. Ües signaux seront aussi d’un grand
inter&t pour la comparaison des indications de l’heure obtenues ä differents observatoires. On
a constate que, pendant les p6riodes ou l’etat du ciel ne permettait pas de faire des obser-
vations, les indications de l’heure donne&es par des observatoires bien organises presentaient
des differences de 0,7 & 2,5 secondes. En faisant connaitre, par telegraphie sans fil, les
resultats qu’on a obtenus aux differents observatoires, on pourra obtenir une indication de
’heure plus exacte.

 
