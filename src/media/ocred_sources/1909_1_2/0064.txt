 

EEE EanG

58

Apres avoir remercie M. le Seeretaire, M. le President donne la parole & M. Eötvös
pour la lecture du rapport sur les travaux geodesiques faits en Hongrie.

M. Eötvös deelare que dans le rapport qu’il a dejä presente A la Conference (voir le
rapport, Annexe A. XIX) on trouve aussi les r&sultats des autres travaux geodesiques ex&cutes
en Hongrie: les valeurs de l’intensite de la pesanteur determindes au moyen d’un appareil
de pendule, et les deviations de la verticale deduites de la comparaison des latitudes astro-
nomiques avec les latitudes geodesiques determinees pendant les dernieres annees, de sorte
qu’il n’a plus rien & ajouter.

M. le President remercie M. Kötvös, et demande si quelqu’un des delegu6s a & presenter
un rapport sur les travaux geodesiques exe&cutes dans la Republique Argentine; personne
n’ayant recu un tel rapport, M. le President donne la parole & M. LeArl, qui presente le
rapport sur les travaux geodesiques ex&cutes en Grece. (Voir le rapport, Annexe A. XXIJ).

M. le President remercie M. Lehr! et invite les delegues qui auraient ä presenter
des rapports sur les travaux geodesiques ex&cutes en Portugal et en Roumanie & en faire
la lecture. Aucun des delögues ne se prösente; on avait bien regu la nouvelle que M. Oom
de Portugal viendrait ä& Londres, mais il n’est pas venu ä la Üonference.

DI n’y a done plus de rapports & presenter sur les travaux geodösiques ex&cutes
dans les differents 6tats appartenant & l’Association.

M. Hoerster, ayant demande la parole, rappelle ä la Oonference le rapport intäressant
presente par M. Gusllaume & la Conference de Budapest sur les exp£riences faites ä Breteuil
avec les fils d’invar, et fait part aux delegu6s qu’il tient encore quelques exemplaires de la
brochure de MM. Denoit et Guillaume & la disposition de ceux qui voudraient l’avoir.

Il communique ensuite ä& la Conference qu’il proposera & la Commission du meötre de
s’oceuper ä& Breteuil d'une comparaison entre les fils d’invar et les rubans d’invar, dont on a
discute, dans nos premieres seances, les differentes qualites par rapport & la mesure des bases.
M. Foerster espere qu’on pourra prendre alors une deeision definitive sur les avantages que
presentent les deux me&thodes de mesures.

M. le President remercie vivement M. Foerster, President de la Commission du mötre,
de son interessante communication sur les expöriences qu’il voudrait faire ex&cuter ä Breteuil ;
il se permet de prier M. Foerster de faire en sorte que ces exp£@riences soient non seulement des
experiences de laboratoire, mais des experiences faites sous les mömes conditions que celles que
l’on rencontre pendant les operations sur le terrain: enroulement et d&roulement frequents des
fils et des rubans, accompagnes des differentes dificultes que le terrain pourrait prösenter.

M. Foerster promet de faire exe&cuter les exp6riences d’une manidre aussi pratique
que possible.

M. le President propose de suspendre la seance pendant un quart d’heure; elle est
reprise & 11 heures 35 min.

M. Poincare fait la communication suivante.

M. le Lieutenant de vaisseau 7issot a recemment proposs d’organiser un service
regulier de signaux horaires envoy6s par telegraphie sans fil de la tour Eiffel aux navires

 

Io ad ak

rn ra a A A A

al

naar

ak {mid

 

 
