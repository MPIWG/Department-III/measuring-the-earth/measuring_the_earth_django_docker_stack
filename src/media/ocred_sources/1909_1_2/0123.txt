Te

Sram mer;

Vet RNEERE- TERN TIT PweI er weR eTn

117

Der Präsident ertheilt nachher Hermm Hecker das Wort für eine Mittheilung über
die in Potsdam an der Drehwage des Herrn Hötvös angebrachten Aenderungen,

Herr Hecker hat den Apparat für photographisches Registriren der Balkenlage ein-
gerichtet. Er bedient sich zweier gleicher Balken deren Aufhängedrähte von Platin sich
sehr nahe neben einander befinden.

An einem der Arme befindet sich eine kleine Masse, der andere trägt einen Draht an
dessen Ende sich eine fast eben so grosse Masse befindet. Die Arme beider Balken sind in
kleiner Entfernung einander parallel, und die beiden Arme mit den Drähten sind einander
entgegengesetzt.

An beiden Balken und an dem den Apparat einschliessenden Kasten sind kleine
Spiegel befestigt, welche auf eine sich langsam vertikal bewegende photographische Platte
das Licht einer hell beleuchteten Spalte zurückwerfen. Wenn alles in Ruhe ist, bekommt
man auf die Platte drei parallele Linien; die mittlere rührt von dem am Kasten befestigten
Spiegel her. Eine Bewegung des ganzen Apparats verräth sich also durch eine Abweichung
der mittleren Linie, Bewegungen der beiden Balken durch Abweichungen der Seitenlinien.
Herr Hecker hat vorläufige Versuche angestellt, bei denen er an der einen oder der anderen
Seite eine Bleimasse von ungefähr 30 Kilo stellte; er fand dann, dass die an aufeinander
folgenden Tagen beobachteten Abweichungen einander völlig gleich waren. Der Schwingungs-
dauer des Balkens war ungerähr 24 Min., und die Geschwindigkeit der photographischen
Platte 15 mm. pro Stunde. (Siehe den Bericht, Anhang B, XV),

Herr Darwin fragt, ob es nicht besser wäre anstatt Platindrähte die von Boys erfundenen
Quartzdrähte zu benutzen.

Herr Hötvös bemerkt, wie er auch schon in Budapest gesagt hat, dass im allgemeinen
gut behandelte Platindrähte sehr unveränderlich sind, und grössere Drehwagen zu benutzen
gestatten. Bei Anwendung von Quartzdrähten muss der Apparat kleiner sein, und da die
den Balken ablenkende Kräfte den Massen, d, h. den Volumina der an den Balken befestigten
Gewichte, dagegen die von Strahlung und Luftströmungen herrührenden Störungen den
Oberflächen proportional sind, ist es besser nicht zu kleine Apparate zu benutzen.

Herr Eötvös hatte auch seine Beobachtungen schon angefangen, ehe Herr Boys seine
Quartzdrähte angefertigt hatte, und aus seinen Untersuchungen ging nicht hervor, dass
Aenderungen seiner ersten Construction wünschenswerth waren.

Herr @%ll fragt, in welcher Weise Herr Zötvös den nachtheiligen Einfluss der Luft-
strömungen aufgehoben hat, welche, wie schwach diese auch sein mögen, immer zu
befürchten sind.

Herr Kötvös antwortet, dass er die durch Strahlung hervorgerufenen Luftströmungen
durch einen doppelwandigen oder dreiwandigen Metallkasten völlig eliminirt hat; er hat
auch wohl durch ein Schlangenrohr bei den Wänden Wasser strömen lassen.

Der Präsident dankt den Herren Hecker und Bötvös für ihre wichtigen Mittheilungen,
und ertheilt Herrn Bourgeois das Wort für seinen Bericht über die geodätischen Arbeiten
des militär-geographischen Dienstes in Paris.

Herr Bourgeois theilt einige Bemerkungen über die in seinem Bericht (Siehe Bericht,

 
