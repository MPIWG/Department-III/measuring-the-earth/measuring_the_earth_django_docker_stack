 

Tamm

SET ARE TEITREFORNTTAETTTTT

alas

15

L’article 5 de notre Convention stipule que les membres du Bureau sont elus par la
Conference generale, mais que, en cas de vacance, le remplacement provisoire sera fait par
la Commission permanente, par voie de correspondance, ou, s’il le faut, en seance, par cette
commission convoquee ad hoc. Conformement & cet article le Bureau a prie, par circulaire
du 21 Aoüt 1907, MM. les membres de la Commission permanente de bien vouloir communi-
quer par &erit, avant le ler Decembre 1907, leurs votes pour la nomination d’un Vice-president
provisoire de l’Association & la place de M. le lieutenant-general Zacmarıar.

Par circulaire du 8 Decembre 1907, j’ai eu l’'honneur de communiquer a MM. les
delegues, que Sir Grorer H. Darwın avait &t& Elu Vice-president provisoire & l’unanimite des
18 votants. Tout & l’heure MM. les membres de la Commission permanente seront appeles a l’elec-
tion du Vice-president pour la duree de notre Convention, c’est-a-dire jusqu’a la fin de 1916.

Afın de donner suite aux resolutions prises dans la Conference generale a Budapest,
votre secretaire a expedie plusieurs lettres:

1°. A MM. les Ambassadeurs de la Grande Bretagne, de la Russie, de l’Italie, des Htats
Unis et du Japon, ainsi qu’ä M. le Ministre de la Republique Argentino I a Berlin, pour
leur faire connaitre les remerciements presentes par l’Association geodesique internationale
aux astronomes et aux institutions qui, par leur cooperation, ont tant contribus aux beaux
resultats acquis par le service des latitudes.

2°, A MM. les Ambassadeurs de la R£publique francaise et de l’Italie, pour leur
communiquer le vou de l’Association, que les gouvernements frangais et italien veuillent
bien se concerter pour assurer, dans le plus bref delai possible, le rattachement geodesique
direct entre la Corse et le continent Italien.

3%, A M. l’Ambassadeur de la Republique frangaise et a M. le Ministre de la Suisse,
pour leur communiquer le voeu, que les gouvernements frangais et suisse se mettent d’accord
pour profiter des op6erations de triangulation ex&cutees dans le Jura, le long du meridien de
Lyon, pour rattacher entre eux les röseaux geodesiques frangais et suisse.

4° A M. l’Ambassadeur de l’Italie pour lui communiquer le veu de l’Assoeiation,
que les nivellements projetes dans le bassin de la Mer soient rattaches au niveau
moyen de la mer.

5%, A M. l’Ambassadeur de la Republique francaise et & M. le. Ministre de Rou-
manie, pour leur commüniquer le vau, que les gouvernements frangais et roumain se mettent
d’accord pour faire determiner la difference de longitude Paris-Bucarest.

6%. A MM. les Ambassadeurs de l’Autriche-Hongrie et de la Russie et& M. le Ministre
de la Roumanie, pour leur communiquer le vau, que des accords soient conclus entre les
gouvernements interesses, afin de continuer les determinations de longitude necessaires PoE
fermer le polygone Paris Potschar Bucarest-Poulkovo ou Vienne.

7%,-A. M. l’Ambassadeur de l’Autriche-Hongrie, pour lui.faire part du vau de
l’Association, que le gouvernement hongrois veuille bien favoriser les travaux de M, le Baron
Eörvös pour determiner la variation de la pesanteur, d’abord dans la plaine hongroise et,
si possible, aussi sur le terrain d’un volcan actif, par exemple le Vesuve.

 
