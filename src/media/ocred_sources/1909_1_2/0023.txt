TH TREFFER ETAGE

IR

Ce resultat a &t6 communiqu& aux Delegues representant les differents Htats dans
la 15e Conference generale de l’Association, tenue au mois de Septembre & Budapest, et &
l’unanimite la resolution a et& adoptee, que la Convention, expirant le 31 Decembre 1906,
est prorogee pour une nourvelle periode de dix ans, a partir du ler Janvier 1907, sans
aucune modification des articles.

La part contributive & payer annuellement par votre Gouvernement sera donc, d’apres
V’article 9 de la Convention,..... Mark, ou environ..... francs,

Conformement aux dispositions de l’article 5, il a &t& procede, dans une des seances
de la derniere Conference, a l’election du Bureau, qui se trouve constitu& de la maniere
suivante pour la prochaine periode decennale:

President M. le General Bassor, delegue de la France, Vice-president, M. le General
ZACHARIAE, delegu&e de Danemark, Secretaire perpetuel, M. le Professeur van DE SANDE
Bıxnuyzen, delegue des Pays-Bas, membre de droit M. le Professeur HrLmert, Directeur
du Bureau central & Potsdam. |

Nous prions Votre Excellence de bien vouloir porter le contenu de cette lettre a la
connaissance de Votre Gouvernement.
Veuillez agreer......

Le Secretaire perpetuel Le President
VAN DE SAaNnDE BAKHUYZEN. General BAssor.

Cette lettre n’a pas ete adressee au representant diplomatique de Serbie, puisque
celui-ci ne m’avait pas fait connaitre l’intention de son gouvernement sur la prorogation
de la Convention. Enfin le 17/30 Mai 1907 nous avons recu la communication que la
Serbie ne partieipera plus & notre Assoeiation.

Vingt etats avaient donc donne leur adhesion a la prorogation, et le 1er Janvier
1907 la nouvelle Convention est de nouveau entree en vigueur pour une periode de 10 ans.

Depuis cette date, deux Etats sont encore entres dans notre Association, 19 la R6-
publique Argentine, dont j’ai deja fait mention, et 2° le Chili, dont le representant diplomatique
ä Berlin m’a Ecrit, le 4 Janvier 1908, que son gouvernement lui avait donne des instructions
pour adherer a l’Association geodesique internationale, et pour payer la quote-part de huit
cent Marks. En ce moment nous comptons donc 22 Etats contractants, dont 17 en Europe,
4 en Amerique et 1 en Asie.

M. le Ministre de Chili & Berlin a port€ & ma connaissance que M. A. BartRAnD
et M. Lvis Rıso PArron ont ete nommes delegues du Chili & notre Conference.

J’ai et encore inform par MM. les Representants diplomatiques & Berlin des Gou-
vernements des Etats Unis, de la Russie et de la Norvege, que leurs delegues a la 16° Con-
ference generale de notre Association seraient: pour les Etats Unis, MM. Trrrmann et Hayroro,
pour la Russie, M. le General d’Arramonxorr, pour la Norvege M. le Prof. GEELMUYDEN.

* 3

 
