u

en

ee en Bart

109
40 Jahren, sich vermuthlich durch lokale Senkungen der Erdkruste die Höhe verschiedener
Höhenmarken geändert hat. (Siehe die Berichte, Anhang A IVa und A. IV).

Der Präsident dankt Herrn SeAmidt und ertheilt Herrn Gautier das Wort.

Herr Gautier legt einen inhaltsreichen Bericht vor über die in der Schweiz ausgre-
führten Arbeiten und weist hin auf die durch Pendelbeobachtungen und die durch Berechnung
der Massenanziehung bis zu einer ziemlich grossen Entfernung erhaltenen Resultate. (Siehe
den Bericht, Anhang A. XVII). a

Der Präsident dankt Herrn Gautier und giebt Herrn Zittman das Wort, der einen Auszug
seines umfangreichen Berichts mittheilt über die in den Vereinigten Staaten ausgeführten
geodätischen Messungen und über die aus den dortigen Dreiecksmessungen abgeleiteten Resul-
tate für die Grösse und Figur der Erde, nachdem die geodätischen Coordinaten, in der Weise
wie Herr Hayford in Budapest mittheilte, für die Anziehung der Massen verbessert waren.
Aus allen bis 1909 ausgeführten Beobachtungen würde die Tiefe der Compensation 122,2 km.
betragen. (Siehe den Bericht, Anhang A. XXa).

Herr Helmert erinnert daran, dass durch die Arbeiten der Herren Titimann und Hayford
in den Vereinigten Staaten die Geodäsie in eine neue Periode eingetreten ist, die sich von
der früheren durch die Anwendung der Pratt’schen Theorie auf die Bestimmung der Grösse
und Figur der Erde unterscheidet. Er beglückwünscht seine amerikanischen Collegen, wert
diese neue Periode eröffnet haben. (Beifall).

Der Präsident bringt‘ Herrn Tiltmann seinen warmen Dank, und ertheilt Herrn 7erao
das Wort, der den Bericht über die in Japan ausgeführten Arbeiten verliest. (Siehe die Be-
richte, Anhang A. Xa, A X und AX.c). Herr Nakano wird später einen Bericht über Längen-
bestimmungen vorbringen.

Der Präsident dankt Herrn Terao, und ertheilt Herrn Gillis das Wort für seinen Be-
richt über die in Belgien ausgeführten Arbeiten. (Siehe den Bericht, Anhang A. D).

Der Präsident dankt Herrn Gsllis und ertheilt Herrn Ziso Patron, dem zum ersten
Male an der Conferenz theilnehmenden Delegirten von Chile, das Wort. (Beifall).

Herr Asso Patron giebt folgenden Auszug aus seinem Berichte.

In den nördlichen Theilen von Chile ist eine Oberfläche von 80000 Quadratkilometer
recognoseirt und zwischen den Parallelen von 17° und 23° ist ein Dreiecksnetz entworfen;
in 7 Stationen ler Ordnung sind die Messungen ausgeführt; in den nächsten Häfen sind
ein Fluthmesser und zwei Medimaremeter errichtet; ferner sind 74 kilometer doppelt ni-
vellirt. In Central-Chile befindet sich ein Dreiecksnetz, dessen Messungen in 27 Stationen
ausgeführt sind, und das eine 7666 Meter lange eine Eisenbahnschiene entlang mit einem
stählernen Band von 49,60 m. gemessene Grundlinie enthält. Südlich des 372 Parallels ist
ein Netz von 13 Dreiecke entworfen; es wird ein Fluthmesser in Talcaguano errichtet und
49 kilometer sind doppelt nivellirt. a den Bericht, Anhang A. II).

Der Präsident dankt Herrn Riso Patron, und beglückwünscht die Conferenz mit dem
Beitritt von Chile, der für die Förderung der Geodäsie von grosser Wichtigkeit sein kann.

Da Niemand weiter das Wort verlangt, wird die Sitzung um 1 Uhr aufgehoben.

 
