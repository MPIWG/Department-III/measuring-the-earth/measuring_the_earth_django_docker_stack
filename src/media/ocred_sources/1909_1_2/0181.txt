more

u

 

165

 

Ueber die Längen der geodätischen Linien geben Ausweis Heft I und II der „Längen-
gradmessung”, sowie Heft III und IV der „Lotabweichungen”; über die Bögen zwischen
Bobrysk bis Orsk siehe oben Nr. 1, über Bruxelles und Ubagsberg Nr. 2 und Nr. 3, über
Killorglin vergleiche das oben genannte englische Werk. Aus 8. 57—114 das II. Heftes der
„Längengradmessung”’ findet man die folgenden A-Gleichungen zwischen den hierhergehörigen
Nachbarstationen; ihnen sind die auf die 5 neuen Stationen Killorglin, Bruxelles, Ubagsberg,
Grodno und Bobrysk bezüglichen Gleichungen zugefügt. Aus der zweiten Spalte ergibt sich

  

die Herkunft der %-Gleichungen der dritten Spalte.

 

 

|
Feaghmain-Greenwich Lggrdme. 1.8.57 = — 5.94 — 0.2986 &5  +0997 71 — 03749 u
Killorglin-Feaghmain 8.0, 9, 162 ra = +962 + 0.012585 -+1.00424 + 0.0201u
Haverfordwest-Greenwich | Leggrdmg. 1.8 59! , = 7113 - 010% "107% fıı
Greenwich A —. 1.0000 A,
Rosenda&l-Greenwich Lggrdme. 11. 8. 60 | 2, = —5.99 -0.0519&5 0.990179 + 0.0860 u
Nieuport-Rosendael n n 651 %= +042 +0.00758 -+1.0018%2 + 0.0125 u
Bruxelles-Nieuport 3.0. 8. 164 = +100 + 0.0345&, + 0.993821: -+ 0.0577 u
Ubagsberg-Bonn Lotabweich. IIl. S. 25 | A& = +059 —0.0245& 1.002471, —.0.0413 u
Bonn-Nieuport Lggrdmg. 11.8. 66 | 2, = —5.46 + 0.092338, + 0.989771, + 0.1549 u
Brocken-Bonn 7 „ 71|A = +926 +0.0779&, + 1.032227, + 0.1296 u
Göttingen-Brocken " " 74| nr = —155 —0.0148&5 + 0.994027, — 0.0241 u
Leipzig-Brocken „ „ 76 | A = +0.03 + 0.038825 + 0.989675, + 0.0626 u
Grossenhain-Leipzig „ n 80 | Ay = — 9.03 + 0.0257 &5 + 0.999218 + 0.0425 u
Schneekoppe-Grossenhain n n 8 | Ao= +3.63 + 0.046585 + 0.987475 + 0.0777 u
Breslau-Schneekoppe „ 7 86 | 1, = +452 + 0.027980 + 1.008020 + 0.0470 u
Rosenthal-Breslau n " 88 | ra= + 0.36 0.0000 &, + 1.0004 A,, 0.0000 u
Trockenberg-Breslau n " 9% | A = — 6.68 + 0.038885, +09848 1 + 0.0653 u
Mirow-Trockenberg n " 9 | r4= +4.09 + 0.006683 + 1.008423 + 0.0112 u
Rauenberg-Leipzig " „ 11 | a5= —2.12 + 0.022585 +1.0248 28 + 0.0367 u
Berlin-Rauenberg n „106 | Ar = — 0.44 + 0.0006 &5 + 1.0012A,5 + 0.0010 u
Springberg-Rauenberg „ „ 109 | ar = —256 +0.0755&, +1.015821, + 0.1189 u
Schönsee-Springberg „ n 112 | 1a = — 6.52 + 0.0530 5, + 0.9988 A,;, + 0.0821 u
Warschau-Schönsee „ „ 114 | a9= +714 + 0.047888 + 0.9782 Ag + 0.075lu
Grodno-Warschau 8.0. 8. 160 ?9o= — 8.94 0.066359 1.0339 1 0.1043u
Bobrysk-Grodno 8.0.9 160 ia = +3-96 + 0.1245 85 + 0.985% A909 + 0.1907 u

 

 

Die zur Berücksichtigung der neuen Längenbestimmungen sowie der neueren Längen-

netz-Ausgleichungen nötige Unterlage findet man in der letzten Spalte von Nr. 4.

Da bei der Neu-Ausgleichung von den A-Gleichungen auf 8. 191 im Heft II der

„Längengradmessung”’ ausgegangen wurde, so war noch zu beachten, dass diese Gleichungen
bereits einer Ausgleichung unterzogen worden waren; hierüber siehe 8. 138 des genannten
II. Heftes und folgende. Aus dieser Ausgleichung gingen die „Verbesserungen” auf, 185 u. f.
hervor; von ihnen konnten, nach einem Vorschlage des Herrn HELMERT, die 8 S und 35T,
also die Verbesserungen der Strecken und der Azimute als brauchbare Näherungen beibe.
halten werden, während die Längenverbesserungen 3L wieder eliminiert wurden, indem sie
mit umgekehrtem Vorzeichen den A-Gleichungen der 8. 191 zugefügt wurden. Die $L
selbst sind:
