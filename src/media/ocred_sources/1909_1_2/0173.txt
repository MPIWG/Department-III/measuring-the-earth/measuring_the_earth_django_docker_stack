ee

mn

ie

TER TR DwTT

BEILAGE A, vb.

PREUSSEN.

Königl. geodätisches Institut in Potsdam.

Bericht über die Arbeiten in den Jahren 1907, 8 und 9.

Das Geodätische Institut führte ausser den in dem Tätigkeitsbericht des Zentralbureaus
genannten internationalen Arbeiten noch die nachstehend verzeichneten praktischen Arbeiten
im Landesgebiet und wissenschaftlichen Untersuchungen aus.

1. Im Anschluss an das neue Dreiecksnetz der Königl. Landestriangulation in Ost-
preussen wurden in Memel die geographische Breite und das Azimut zweier Dreiecksseiten
astronomisch bestimmt, um die älteren Bestimmungen daselbst, die nicht ganz einwandfrei
sind, zu kontrollieren bezw. zu ersetzen.

2. Um den Gang der Lotabweichungen in Länge für das Gebiet der europäischen
Längengradmessung in 52° Br. im preussischen Staatsgebiet eingehender kennen zu lernen,
wurden die geographischen Längendifferenzen Potsdam-Jena-Gotha-Göttingen astronomisch
bestimmt und zwar nach dem gewöhnlich vom Geodätischen Institut angewandten Verfahren.

3. Für das Studium der Lotabweichungen in Breite im Harze machte sich die
Beobachtung der geographischen Breite an noch 14 Punkten nötig, die am Universal nach
der Methode der Meridianzenitdistanzen erfolgte.

4. Bei Gelegenheit der Messung der neuen Berliner Basis von Seiten der Königl.
Landesaufnahme hat das Geodätische Institut diese Grundlinie mit 5 Invardrähten unter
Benutzung seines JÄDzrın’schen Apparats nachgemessen. Zur Bestimmung der Drahtlängen
diente wie früher die Hilfsbasis von 240 m. im Institutsgelände, deren Länge von früher
her bekannt ist. Die Ergebnisse sind sehr befriedigend.

5. Das Netz der Pendelstationen wurde durch 20 neue Stationen im mittleren Teile
des Landesgebietes erweitert. Hierbei kamen ausser den Messingpendeln auch Nickelstahl-
pendel zur Verwendung, die sich durchaus bewährten.

6. Die Reihe der 8 Ostseepegel wurde durch einen 9ten Pegel ergänzt, der in eine
grössere Lücke eingeschaltet wurde. Im übrigen blieb der Pegel-Dienst an der Küste der-
selbe wie früher.

7. Die Beobachtungen an den beiden Horizontalpendeln in der 25m. tiefgelegenen
Brunnenkammer ergaben im ganzen drei Reihen von 2 bis 2!/, Jahren Dauer. Herr Prof.

 
