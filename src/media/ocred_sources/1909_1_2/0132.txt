 

126

zeigten. Durch gegenseitige Mittheilung der Uhrcorreetionen mittels Funkentelegraphie würde
man wahrscheinlich genauere Resultate erhalten.

Der Präsident dankt den Herren Poincare und Foerster für ihre wichtigen Mittheilungen,
und ertheilt Herrn Helmert das Wort, der sein Programm für die Thätigkeit des Öentralbureaus
in den nächsten Jahren, deutsch und französisch verliest. (Siehe den Bericht, Anhang B. III).

Da Niemand das Wort verlangt, dankt der Präsident Herrn Helmert für seinen Bericht
und fordert die Herren Delegirten auf, Vorschläge für den Ort der nächsten Öonferenz
vorzubringen.

Herr Schorr erlaubt sich im Namen des Hamburger Senats die Delegirten einzuladen
im Jahre 1912 zur 17. Conferenz in Hamburg einzutreffen, wo sie des herzlichsten Emp-
fangs versichert sein können.

Herr Weiss erinnert daran, dass Oesterreich einer der ersten Staaten war, die der inter-
nationalen Erdmessung beigetreten sind, und vom ersten Jahre ab verschiedene Beobachtungen
zum Zwecke der Cooperation mit der Erdmessung hat anstellen lassen; die oesterreichische
Regierung würde sich glücklich schätzen, wenn die 17e Conferenz in Wien abgehalten würde,
und die stadt Wien würde das möglichste thun um die Delegirten herzlich zu empfangen.
Herr Lallemand erinnert seine Üollegen daran, dass seit der Gründung der inter-
nationalen Hrdmessung noch keine Üonferenz in einem der Länder Nord-Europa’s statt
Sefunden hat und er glaubt, dass es angemessen wäre den Ort der nächsten Üonferenz in
einem dieser Länder zu wählen.

Herr Geelmuyden erklärt dass, obwohl er von seiner Regierung nicht zu einer officiellen
Einladung ermächtigt sei, die Delegirten, wenn sie 1912 in Christiania zusammentreffen
sollten, dort eines herzlichen Empfangs und einer grossen Gastfreundschaft versichert sein können.

Herr Helmert bemerkt, dass im Jahre 1912 gerade 50 Jahre seit der Gründung der
internationalen Eirdmessung in Deutschland verstrichen sein werden; seiner Meinung nach
wäre es also richtie zur Feier dieses Jubiläums in einer deutschen Stadt, z.B. Hamburg,
zusammen zu kommen,

Der Präsident verliest den auf die Wahl des Ortes der Generalconferenz bezüglichen
Artikel unserer Uebereinkunft und erinnert, dass wir darüber hier keinen Beschluss zu fassen
haben; er versichert jedoch, dass das Präsidium bei seiner Wahl sich durch die hier vor-
gebrachten Anträgen werde leiten lassen.

Der Präsident ertheilt Herrn Backlund das Wort zur Verlesung der Beschlüsse der
Commission für die Frage der Breitenvariation. Es sind folgende:

1°. Die Beobachtungen auf den 4 internationalen Nordstationen und soweit möglich
in Tschardjui und Cincinnati weiter fortzusetzen;

2°. Dem Beobachter in Gaithersburg, Dr. Koss, eine Beihilfe von 10 000—15 000 Mark
zur Aufstellung eines photographischen Zenitteleskops zu gewähren (die genaue Feststellung
der Summe bleibt dem Präsidium überlassen). Ausserdem wird dann Dr. Ross, sobald die
Beobachtungen in Gang sind, eine jährliche Beihilfe von 2000 M. zugesichert.

8%. Die argentinische Republik zu ersuchen die Beobachtungen in Oncativo bis auf
weiteres fortzusetzen.

 

an en 1; u he

Oro}

nk

in labbun oh

ah

EIutn TEN

 

 
