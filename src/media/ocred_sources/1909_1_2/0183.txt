rıram en we TE Tee TR NTOT

mer nn

I 1m

verr

Be

ST TErREEER IRRE DIT

ie
5

 

167

8 3.

Aus diesen Unterlagen erhielt ich die folgenden 28 y-Gleichungen;; sie werden weiter-
hin auch als „Fehlergleichungen” bezeichnet.

Die Unterschiede zwischen den absoluten Gliedern der Fehlergleichungen der drei
Paare von Nachbarstationen: Nieuport-Rosendaöl, Rauenberg-Berlin und Breslau-Rosenthal
betragen nur wenige 0’.l; um nicht einzelne lokale Abweichungen doppelt vertreten zu
haben, wurden zur weiteren Ausgleichung gliedweise gemittelte Gleichungen benutzt.

 

 

 

 

 

 

INT 5
Station Index Fehlergleichungen Darstellung
Feaghmain 1 | +0.98994 —0.2312u — 3.668 — 0.140995 = 9 Aguog 2 0008
Killorglin a | +09899 09188 4 2.966 01394 = 1.2.4924 — 0.0950
Haverfordwest | =6:0.9975:. , —0.119 57 00.099, DB ı m +.1.672 —0.0701
Greenwich 0.1175 1.0000 0.0000 0.000 0.0000 = +1.808 -— 0.044]
. \ +0.9994  +0.05890 °— s006 0a u
Bruxelles b | -#.0.9979 ° 3.0.0980 208.144 20.0591 0 = m ca oe
Ubagsberg e-|. 4.0.9966: 0.1835 2 2090 no — 4395 —.0.0134
Bonn 5-|. +0992 10190  Taı 009 _ 419 — 0.0077
Brocken 6 | #099 10989 100 Fo +-2.278 0.0104
Göttingen | 209008 #099 00 "oo en — 2.957 + 0.0070
Leipzig 8. 1 :0,9858 2.0,9769 0 1.050, Sea +2.793 0.0197
Grossenhain 9 —- 0.9829 +0.3023  — 6.583 401828 = m — 2.552 +- 0.0259
Schneekoppe 10.1 21.0970 +0.3504& — 4221 +02117 = 0 + 0.158 + 0.0371
3 1
Rn ni \ Los na 0.995. :09994 — mn a on
Trockenberg 13 | +09672 8041880 5.187.100 0 oe
Mirow 14 1.0.9672 7.0.4961 35 Tocsaı 9: + 2.469 -+- 0,0581
Bauern “ | +.0.9821 ° 40.9981 = 9196 101790. = | Fre 0,
Springberg 17 7 +09741 +08065 3a oa een
Schönsee 18 | 09607 +09 — 739 1050 u 20
Warschau 19 | +09587 404651 2 280 2085 -. ma 0
Grodno 20 | +0964 +0547 — Tees Lose un ae
Bobrysk 31 | +0998 06370 — 5 >,
Orel 32--| 40.9917  +0.2870 — 2618. 1.0304 m | I. 00,
Lipetzk 23 | +0995 10868 — 764 TosalE = a
Ssaratov %4 |: 40.9338 - 41,0179  — 2504 10908 a ı cc .
Ssamara > | +o018 10015 —-D55 "05 a
Orenburg 6 | +08  +12l65 9:0 Lose
Orsk 97 | 209858 1a ms oe a

 

 

Die Unbekannten „, einerseits, u und &, andererseits gehen hier in verschiedener
Weise ein; während die Koefficienten von „, nur zwischen + 0.9 und + 1.0 schwanken,
wachsen die von u und &, allmählich in fast gleicher Weise an und zwar ist dieses Ver-
halten begründet in der Form der Koeflicienten von u und Z, in den ursprünglichen Lot-

*

22
