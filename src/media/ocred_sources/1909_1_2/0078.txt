 

72

für unsere Geodäten in verschiedenen Welttheilen erwachsenen Vortheile enorm gewesen sind.

Diese Meinung wird von vielen getheilt, wie aus einer Menge von Privatbriefen von
den Geodäten der grossen Aufnahme von Indien, von Sir Davın Gr und von den Geo-
däten in Australien und Canada, an mich hervorgeht. Bei den früheren Versammlungen
war ich fast immer der einzige Vertreter Gross-Britanniens, hauptsächlich deshalb, weil der
grösste Theil unserer geodätischen Arbeiten in Gegenden stattfinden, welche durch gewaltige
Wasserstrecken vom Mutterlande getrennt sind. Ich war aber der Meinung dass die Con-
ferenz jetzt, da sie in London stattfindet, für die Brittische Geodäsie eine besondere Bedeu-
tung habe, und es freut mich, dass die verschiedenen Regierungsbehörden, denen ich meine
Ansicht unterbreitete, sofort eingesehen haben, wie wünschenswerth das Zusammentreffen der
Vertreter der verschiedenen Theile des Brittischen Reiches auf dieser Öonferenz ist.

Die geodätische Aufnahme von Indien ist in ausgezeichneter Weise durch den ab-
sichtlich aus Indien zu uns gekommenen Director des trigonometrischen Dienstes vertreten ;
wir haben Vertreter von Canada und Australien; wir haben Sir Davıp GiLL und, wie ich
glaube, auch Sir Wıruıam Morris, der die grosse Arbeit der südafrikanischen Triangulation
zustande gebracht, und das grosse Unternehmen, die Messung des 80° Meridians östlich von
Greenwich begründet hat.

Gross-Britannien schloss sich etwas später als die meisten anderen Länder bei der
Erdmessung an, und ich habe versucht die Frage zu beantworten, ob diese Verspätung
unseres Beitritts von einem Mangel an Thätigkeit auf geodätischem Gebiete in unserem
Reiche herrühre. Ich habe versucht alle nationalen Vorurtheile, denen wir manchmal unter-
worfen sind, zu beseitigen, und bin nach sorgfältiger Erwägung zu der Ueberzeugung ge-
kommen, dass diese Verspätung nicht einem Mangel an nationaler Thätigkeit zugeschrieben
werden muss.

Wenn ich bedenke, dass die „Ordnance Survey of Great Britain” im Jahre 1791
begann und ununterbrochen fortgesetzt wurde in den Jahren, während welcher der Krieg
Ruropa aus einander riss, dass die grosse Aufnahme von Indien, eine der grössten geodä-
tischen Unternehmungen der Welt, vor vielen Jahren zum Abschluss gebracht ist, dass man,
wie ich schon sagte, in Süd-Afrika sehr rührig gewesen ist, dass Canada einen ausgezeich-
neten Anfang mit seinen geodätischen Arbeiten gemacht hat, und das Australien bereits
tüchtige Arbeit geliefert hat, so glaube ich zu der Erklärung berechtigt zu sein, dass Gross-
Britannien seinen gebührenden Antheil an der gemeinschaftlichen Arbeit geliefert hat, und
dass wir nicht, wenigstens nicht in nennenswerther Weise, hinter den grossen Unternehmungen
Frankreichs, Deutschlands, Russlands, der Vereinigten Staaten und der anderen Länder Europa’s
zurückgeblieben sind.

Ich fürchte, dass ich Ihre Geduld vielleicht schon zu lange in Anspruch genommen
habe, aber ich möchte zum Schluss noch einige Worte sagen über den schönen Saal, in
welchem wir die Ehre haben uns zu versammlen. Wir Geodäten sind damit beschäftigt die
Oberfläche der Erde zu messen und ihre Figur und Zusammenstellung zu bestimmen. Die
Institution of Civil Engineers ist eine Gesellschaft von Männern, welche Geodäten sind in
einem anderen Sinne des Wortes. Sie haben versucht die Oberfläche der Erde dem Willen

PL a0)

are (hd 1 A I

a

aka

uk hr A

nun A u aha a mann 11:

ak

|
|
|

 

 
