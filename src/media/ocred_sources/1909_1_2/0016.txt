 

 

 

10

Australie: enfin, sur l’instigation de Sir Davın GILL, vous avez commenc& en Afrique et vous
poursuivez, avec une vigueur qui fait notre admiration, ce grand &uvre du 30° meridien,
qui s’etendra du Cap au Caire et qui, reli6 plus tard au grand are russo-scandinave, donnera
une chaine continue de 106 degres d’amplitude, la plus etendue que l’on puisse tracer sur
le globe.

Avec un pareil passe, avec les entreprises que vous poursuivez aujourd’hui, avec les
projets que vous formez pour l’avenir, vous deviez nous appartenir.

Monsieur le ministre, nous remercions votre Gouvernement de nous avoir fourni
l’oceasion de vous t&moigner notre gratitude pour l’accession de la Grande Bretagne ä
notre Association.

Permettez moi de vous exprimer aussi, au nom de tous mes Collögues, nos vifs re-
merciements pour l’accueil bienveillant que vous nous röservez.

La SoeietE Royale de Londres a bien voulu nous faire souhaiter la bienvenue par
notre Üollegue Sir GEORGE DArwIN: nous la prions d’accepter l’expression de notre recon-
naissance pour cette marque de sympathie: son President, Sir ARCHIBALD GEIkIz, le celebre
geologue, s’est trouve dans l’impossibilite de venir ä cette seance: nous aurions &t6 heureux
d’etre accueilli par un tel maitre dans une science qui s’aide de la nötre, comme la nötre
s’aide de la geologie.

Messieurs et chers Collegues,

Je souhaite la bienvenue aux Delegues qui viennent pour la premiere fois partieiper
a nos travaux, en particulier aux reprösentants des deux nouveaux Etats, r&ecemment entres
dans notre Association, la Republique Argentine et le Chili.

Je declare ouverte la 16° Conference generale de l’Association geodesique internationale.

Sur la proposition du President la seance est suspendue pendant un quart d’heure
et est reprise a 11!/, heures.

En rouvrant la seance, M. le President fait observer que, pendant cette Conference,
il y aura beaucoup & faire, de sorte qu’il sera probablement n&cessaire de se r&unir chaque
jour; les seances commenceront toujours ä 9°/, heures.

Il donne ensuite la parole au seeretaire perpetuel pour la lecture de son rapport.

M. Bıkuvyzen s’exprime comme suit:

Messieurs,

Avant de vous rendre compte de la maniere dont, depuis la derniere Conference &
Budapest, le Bureau de l’Association a rempli la täche preserite par notre Convention, j’ai
a vous signaler les changements survenus dans la liste de nos delegues.

 

3
ä
a
ü
x
ü
ü
°

ak tik

iu an dl

11 dh a u äh ha ann ©

sun wach il |
