nAanmIe ren een

Ten

Te ARTNET

2l

M. W. F. King, Direeteur de l’Observatoire d’Ottawa (Canada).

M. G. A. Knibbs, Chef du Bureau de statistique & Melbourne,

M. le Capitaine Jack du service trigonome6trique pour la mesure de l’are dans Uganda.

M. @. T. Me Caw du service trigonome6trique pour la mesure de l’are dans Uganda,

M. le Lieut.-Colonel ©. F. Close, Chef de la Section g6ographique de l’Etat major-general.

M. H. P. Hollis, Astronome & l’Observatoire de Greenwich.

M. A. R. Hinks, Lecteur de Geodeösie et astronome ä l’Observatoire de Cambridge.

Sir Robert Ball, Directeur de l’Observatoire de Cambridge.

M. F. Stratton, Cajus college, Cambridge.

M. le Dr. A. Schuster, Professenr honoraire & l’Universitö de Manchester, President de
l’Association sismique.

M. B. F. E. Keeling, Directeur de l’Observatoire de Helwan en Esypte.

M. Ronald W. @iblin, Directeur des travaux geodesiques en Siam.

M. le Gön6ral v. Stubendorf, ancien delögus de la Russie aupres del’ Association geodesique.

M. le Prof. E. Jäderin de l’&cole polytechnique & Stockholm.

M. le Dr. V. Carlheim Gyliensköld, Seerstaire de la commission pour la mission seientifique
suedoise pour la mesure d’un arc de meridien au Spitzberg.

M. le Capitaine @. Perrier du service geographique de l’armee, Paris.

M. H. A. Deslandres, Direeteur de l’Observatoire de Meudon.

M. le Dr. Th. Wittram, Astronome & l’observatoire de Poulkovo.

M. le President remercie M. le Secretaire de son rapport et invite les delögues a
s’oecuper de l’election d’un vice-pr6sident pour la periode 1909—1916, M. Darwin ayant
remplae& provisoirement M. Zachariae d’apres Y’art. 5 de la Convention. Quatorze Etats
prennent part & l’leetion; tous les billets portant le nom de Sir George Darwin, celui-ci
est nomm& vice-pr6sident a l’applaudissement de la Conference.

Sur la demande de M. le President, M. Darwin deelare accepter sa nomination, eb
remercie ses collegues de la confiance qu’ils ont bien voulu lui t&moigner.

D’apres l’ordre du jour provisoire, M. le President invite les delegues a nommer la
commission des finances et propose les membres suivants:

MM. Forster,
Po1ncARE,
CELORIA, .
WEISS,
GAUTIER,
TITTMANN,
AÄRTAMONOFF.

Öette proposition est adoptee & l’unanimite.

La parole est donnde a M. Helmert pour la lecture de son rapport sur les travaux
du Bureau central, depuis la 15° Conference generale a Budapest en 1906.

 
