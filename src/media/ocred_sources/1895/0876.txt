eh

x. - Prusse, B.

 

Veröffentlichungen. — 1. Gradmessung in Ostpreussen und ihre Verbindung mit

10.

Il.

12,

Preussischen und Russischen Dreiecksketten. Ausgeführt von F. W. Bessel und J.J.
Baeyer. Berlin 1838.

. Die Küstenvermessung und ihre Verbindung mit der Berliner Grundlinie. Ausgeführt

von der Trigonometrischen Abtheilung des Generalstabes. Herausgegeben von J.J.
Baeyer. Berlin 1849.

. Die Verbindungen der Preussischen und Russischen Dreiecksketten bei Thorn und

Tarnowitz. Ausgeführt von der Trigonometrischen Abtheilung des Generalstabes.
Herausgegeben von J. J. Baeyer. Berlin 1857.

. Die Königlich Preussische Landes-Triangulation. Hauptdreiecke. Erster Theil. Zweite

Auflage. Herausgegeben von Bureau der Landes-Triangulation. Berlin 1870.

. Die Königlich Preussische Landes-Triangulation. Hauptdreiecke. Zweiter Theil. Erste

Abtheilung. Herausgegeben vom Bureau der Landes-Triangulation. Berlin 1873.

. Die Königlich Preussische Landes-Triangulation. Hauptdreiecke. Zweiter Theil. Zweite

Abtheilung. Herausgegeben vom Bureau der Landes-Triangulation. Berlin 1874.

. Die Königlich Preussische Landes-Triangulation. Hauptdreiecke. Dritter Theil. Heraus-

gegeben von der Trigonometrischen Abtheilung der Landesaufnahme. Berlin 1876.

. Die Königlich Preussische Landes-Triangulation. I. Verzeichniss der Druckwerke

der Trigonometrischen Abtheilung der Landesaufnahme. — II. Die Dreiecksmessun-
gen I. Ordnung 1876-1887. — III. Die Ergebnisse der Hauptdreiecksmessungen 1876-
1885. Berlin 1887.

. Die Königlich Preussische Landes-Triangulation. Hauptdreiecke. Vierter Theil. Ge-

messen und bearbeitet von der Trigonometrischen Abtheilung der Landesaufnahme.
Berlin 1891.

Die Königlich Preussische Landes-Triangulation. Hauptdreiecke. Fünfter Theil. Ge-
messen und bearbeitet von der Trigonometrischen Abtheilung der Landesaufnahme.
Berlin 1895.

Die Königlich Preussen Landes-Triangulation. Kauptdreiecke. Sechster Theil. Ge-
messen und bearbeitet von der Trigonometrischen Abtheilung der Landesaufnahme.
Berlin 1894.

Die Königlich Preussen Landes-Triangulation. Hauptdreiecke. Siebenter Theil. ece. ecc.
Berlin 1895.

 

bahn

 
