 

   

YL.-- France et Alverie,

 

et pour l’aplatissement

ed

— 308,64

valeur rösultant de la combinaison de P’arc du Perou avec Parc francais entre Dun-
kerque et Formentera.

A ces op6rations se rattachent les noms de Delambre, Möchain, Biot, Arago, co-
lonels Henry, Bonne, Corabauf, Brousseaud, Peytier, Hossard, Levret, commandants Du-
rand et Delcros. (')

La triangulation frangaise a &te reli6e en 1861 et 1862 A la triangulation anglaise
par dessus le Pas de‘ Calais, simultanement et separement par des officiers francais
et des ingenieurs anglais. La mission francaise comprenait trois officiers de l’ancien
corps d’Etat-Major: MM. le colonel Levret, les capitaines Beaux et Perrier.

Cette meme triangulation a &t6 rattachee aussi avec la triangulation belge, avec
celle de la Suisse, avec celle de ’Espagne par plusieurs contacts le long de la chaine
des Pyrenees, et enfin, plus röcemment, avec la nouvelle triangulation du royaume d’Italie.

Les caleuls ont &tE exe&cutes par les anciennes möthodes, sans qu’on ait oper6 la
compensation des quadrilateres et l’accord des bases. Ce complöment indispensable de
toute triangulation scientifique est reserve & un avenir prochain.

Il importait, en effet, avant d’entreprendre la r6feetion des calculs, de röviser l’oeuvre
reconnue imparfaite de Delambre et Mechain, et de proc&der & une nouvelle mesure de la
chaine comprise entre Dunkerque et Perpignan. L’operation confide au capitaine Perrier
a et& entreprise en 1870 et continuee les annees suivantes: elle a &t& terminde en 1888.

Les directions ont &te observees par voie de reiteration & l’aide des cercles azimu-
taux perfectionnes de Brunner, munis & l’oculaire d’un fil mobile comme les cercles
astronomiques. — Aux signaux en bois ou en maconnerie ont et substitu6s des heliotropes
pendant le jour et des collimateurs optiques pendant la nuit; — c’est & l’occasion de
cette mesure que les observations de nuit ont &t& introduites d’une maniere systema-
tique dans la pratique des observations, & partir de 1875, et ont donne& les r6sultats les
plus satisfaisants. La chaine comporte 88 stations ; elle s’appuie sur les anciennes bases
de Melun et de Perpignan, mesurees par Delambre. Une nouvelle base fondamentale,
remplacant celle de Melun qu’il est impossible de remesurer, a 6&t6 fixee entre Villejuif
et Juvisy; une base de verification est etablie, au nord de la chaine, pres de Cassel; on
a remesure la base de Perpignan. . |

Les observations ont &te faites par M. le genöral Perrier (35 stations), M. le L.' co-
lonel Bassot (32 stations) et M. le comm.! Defforges (21 stations).

&

 

(') Voir les Tomes VI et VII du Memorial du Depöt de la Guerre rödiges par le colonel Puissant, et le Tome IX
redige par Peytier et Levret.

 

karriere kahslu nähen
