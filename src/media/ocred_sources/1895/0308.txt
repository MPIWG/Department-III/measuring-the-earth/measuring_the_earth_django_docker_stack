 

 

et ä ’appel nominal des Etats, l’ensem-
ble de la nouvelle Gonvention

Huitieme seance, 12 octobre 1895 .

Le Secretaire annonce qu'il a regu un
rapport provisoire sur les travaux ja-
ponais ; le rapport definitif paraitra
comme Annexe B. XVIll ;

Rapport du President et votes sur les
questions financieres. A:

Sur la proposition de la nn Berl per-
manente, la Conference decide de ne pas
modifier le taux des contributions pour
l’annee 1896 oh

Elle adopte de meme la ana con-
cernant les remboursements aux Rtats
qui ont vers& leurs contributions capi-
talisees a ee

Resume de l’etat de situation Ehaness
de l’Association g&odesique internatio-
nale as

Rlections reglementaires

Les quatre membres sortants de la eo
mission permanente, MM. Bakhuyzen,
Ferrero, Foerster et Hennequin, sont
reelus

Pag.

180

184

184

181-183

182

182

182-183 .

183-184

 

302

En remplacement du general Ibafez et de
M. Davidson, MM. d’Arrillaga et Titt-
mann sont &elus membres de la Commis-
sion permanente :

M. le President annonce que, Ion la
nouvelleConvention sera imprimee, il en
sera expedie cing exemplaires a chaque
delegue

Election du bureau Ns a De
internationale: M. Faye est nomme
President, M. Ferrero, Vice-president,
et M. Hirsch, Secretaire perpetuel

La Conference se declare d’accord avec le
mode propose par le President, suivant
lequel la nouvelle Convention sera sou-
mise, par le bureau de la Commission
permanente, a la ratification des gou-
vernements, par l’intermediaire de leurs
representants diplomatiques ä Berlin

M. Lallemand remercie le gouvernement
prussien, au nom de la en pour
son accueil hospitalier ;

MM. Weiss et Faye expriment a M. le
President Foerster, et M. von Orff a
M. le Secretaire Hirsch les remercie-
ments de l’Assemblee

Clöture de la XIme Conference generale .

Pag

184

18%

184-185

185

185-186
186

ernennen he

|
!
j
|

 

 
