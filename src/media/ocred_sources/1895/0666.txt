 

 

 

em

I. — Autriche-Hongrie.

 

 

 

 

 

 

 

 

 

 

 

| |
INDICATION a DIRECTEURS | .
N DES POINTS LATITUDE ıı EPOQUE ET OBSERVATEURS INSTRUMENTS REMARQUES
E I
I ÜBEROT 4:00. 49023! 4"| 300 26' 59" 1865 Ganahl, Sterneck. | Starke n° I de 10 p. |
33 | Arber.......22.... 49 649 | 30 48 6 1865 Wagner. Starke n° 2 de 10p. |
39 | Doubrava.......... 49 25 59 | 30 52 8 1865 id. id.
40 | Tok............... 49 39 25 | 31 30 40 1865 id. id.
41 | Volini vreh........ 49 22 28 | 31 28 38 | 1865-67 | Sterneck. Ertel de 9 et Star-
on. L.de 109,
42 | Rossberg........... 49 3229 315425 | 1862-64 | Merkl, Breymann. a n° I et5 de |
pP.
43 | Kubäny ........... 48 59 32 | 3129 0 1865 Sterneck. Ertel de 9 p. Ä
44 | Kamejk ........... 49 14 2 | 31 57 40 1864 Wagner. Reichenbach n° 2 de
. 12 p.
5 | Bridnik .........:. 49 23 38 | 32 37 32 1864 Breymann. Starke ne I de 10 p.
46 | Mezi vraty......... 49 36 11 | 32 20 14 1864 Zezschwitz. Starke ne 2 de 10 p.
=) |Beony.........0 49 54 55 | 32 27 12 | 1864-67 | Breymann, Hartl. Starke n° 2 et 3 de
10 p.
48 | Melechau.......... 49 38 42 | 3259 4 1864 Breymann. Sa od ı0 p.
4) | Markstein.......... 49: 500 se5l 8| 1867 Hartl. Starke ne 3 de 10 p.
50 | Spitzherg.......... 49 18 45 | 33 10 39 || 1866-67 id. Id. n°2 et 3 de 10 p.
Bl | Blaskor ........... 49 29 39 | 33 29 45 | 1864-65 | Vaener. Starke n° 2 de 10 p.
02 | Hora..... nor. 49 10 17 | 33 22 8 | 1866-68 | Hartl, Sterneck. Starke n° 2 de 10 p.
| et Univ. de 10 p.
53 | Ambrozug......... 49 21 34 | 33 44 51 1865 Sterneck. Starke n° | de IO p.
2 | Bapolle...... .. 49 11 27 | 33 55 54 1866 Ganalıl. Starke Univ. de 13 p.
55 | Maydenberg ....... 48 52 13 | 34 18 59 | 1875-76 | Trojan, Hartl. Starke ne 3 de 10 p.
0... 48 48 34 | 35 0 44 || 1868-75-79 | Sterneck, Cenna, Hartl. Starke Un. de 10 p.
et Starke n? 2 et 3
de8p. et 1Op.
DI | BEdo............ 49 10 19 | 34 58 32 | 1875-76 | Cenna, Trojan. Starke n® 2 de 8 p
Dose, 0.0. 49 32 57 | 34 43 Al | 1875-76 | Cenna, Sterneck. Starke, n° 2 de 8°p.
et5 de 10 p.
59 | Rachsthurn........ 48 27 39 | 34 56 27 || 1875-76-79 | Cenna, Rehm, Hartl. Starke a“ 2 de sp.
et nlet3del0p.
60 | Hundsheimer Berg. 48 759 | 34 36 18 || 1875-76-79 | Gyurkovich, Rehm, Hartl. | Starke ne | et 3 de
10 p.
61 | Buschberg......... 48 34 40 | 34 3.43 || 1867-68-75 | Sterneck, Trojan, Hartl. Starke Un. de 10 p.
76 et 13 p. et Starke
29 de Op.
62 | Hermannskogel.....| 48 16 17 | 33 57 35 | 1872-76 Hartl, Rehm. Starke n° 3 et 5 de
10 p.
63 | Laaerberg I....... 48 933 | 34 350 1867 Ganahl. Ertel. astr. de 12 p.
Observatoire.
63a| Türkensehanze..... 4413581340 2 1877 Rehm, Hartl. Starke n° ] et 3 de
10 p.
64 | Amninger.......... 48 254 | 33 54 44 | 1867-75-76 | Pott, Hartl. Id. n° 2 et3 de 10 p.
65 | Schöpfl............ 48 5 19 | 33 34 49 || 1867-72-74 | Pott,Sterneck, Rehm,Hartl. nn n0#2,.3 et 4
e 10 p.
66 | Spittlmais......... 48 47 21 | 33 36 30 | 1867-68-73 | Ganahl, Gyurkovich, Cenna.| Ertel astr. et Rei-
74-75 chenbach n®° 1 de 12
p. et Starke n° 1 et
3.de 10. p.
67 | Predigstuhl ....... 48 485733 2 17 1867 Kalmär. Starke n° 2 de 10p.
68 | Jauerling.......... 48 20 26 | 33 0 16 | 1872-73-75 | Randhartinger, Hartl. Starke n 3 de 10 p.
69 | Kohont............ 48 46 11 | 32 14 55 | 1867-76 | Sterneck. Id. ne 1 et5 de 10p.
40 | Yeirmik .......... 49 118 | 32 14 59 | 1865-67 | Ganahl, Kalmär. Id. n° 1 et2de 10p.
7] | Schöninger........ 48 51 59 | 31 56 58 1867 Hartl, Starke n° 3 de 10 p.
72 | Sternstein.... ....| 48 33 39 | 31 56 0 | 1862-76 | Breymann, Sterneck. Id. n° 5 et6de 10 p.
73 | Viehberg........... 48 33 41 | 32 17 22 | 1867-73-76 | Kalmär, Gyurkovich, Ster- | Starke ne 2, 3 et 5
at ; neck. de 10.p.
74 | Mayerhoferberg....| 4822 7 | 31 35 46 || 1862-74-76 Breymann, Rehm, Ster- | Starke n® 3 et 5 de
! neck. 10 p.
75 | Kuhenöd .......... 48 23 10 | 31 55 14 1874 - Rehm. Starke n?’ 4 de 10 p.
\

 

 

 

 

 

 

 

 

 

 

 

 

 

 

N. 4, Baviere,

bee
