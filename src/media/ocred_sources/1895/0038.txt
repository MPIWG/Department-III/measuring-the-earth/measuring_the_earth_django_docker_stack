 

32

22. Von Herrn Professor E. Hammer in Stuttgart : Neue Azimul-
besiimmung in Bussen 1894 und hieraus sich ergebende Lage der Dreiecks-

punkte auf dem Bessel’schen Ellipsoid . . . . . AManm NW 100 Exemplare.
23. Die astronomisch-geodätischen Arbeiten des K. u. K. militär-
geographischen Instituts in Wien, V. Band, Wien 1895 . . . s0 >

24. 1. Misura della Bash del Ticino (0 di Somma). 11. Mislira della
Base di Ozieri (Sardegna). Publicazioni dell’ Istituto geografico militare,
Pirenze a ArEHORR, 100 »

25. Von der a onmissich® Vor Haalaligen der 1894 in Innsbruck ab-
gehaltenen Gonferenz der Permanenten Commission der Internationalen Erdmessung g:

875 Exemplare sind gedruckt worden ; davon gelangten im Monat Mai zur Ver-
theilung :

135 Exemplare an die Regierungen ;

A544 ) an die Delegirten, Behörden, Institute, Gelehrten, Gesellschaften, ete.
80 > sind bei G. Reimer in Commission, und
206 » im Bestande verblieben.

Ausserdem hat das Preussische Geodätische Institut auf eigene Kosten noch 195 Ab-
züge anfertigen lassen.

Wegen der zur Versendung gelangten Veröffentlichungen des Preussischen Geodäti-
schen Instituts, die vorstehend nicht angeführt werden, wird auf den Landesbericht für
Preussen Bezug genommen.

Der Director des Centralbureaus der Internationalen Erdmessung :

HELMERT.

Herr Director Helmert schliesst an seinen Bericht die Bitte an die Mitglieder der
Versammlung, ihm gefälligst die in der ersten Adressen-Liste fehlenden oder unrichtig
angegebenen Daten berichtigen zu wollen, damit er die neue Liste möglichst correct drucken
lassen könne.

Der Herr Präsident dankt Herrn Helmert für seinen Bericht und fügt an denselben
einige geschäftliche Mittheilungen über bei dem Gentralbureau eingelaufene Schreiben. Zunächst
hat Herr Defforges, der sich zur Zeit in Rumänien befindet, den Ausdruck seines herzlichen
Bedauerns an die Generalconferenz gerichtet, dass er am Erscheinen verhindert ist. Ferner
ist von Seiten der Kgl. Griechischen Gesandischaft ein entsprechendes Bedauern von Herrn
Cleon Rangabe eingelaufen, und endlich hatauch Generallieutenant z. D. Dr. Schreiber, früher
eines der geschätztesten und bedeutendsten Mitglieder der Gonferenz, dem Gentralbureau
geschrieben, um sein Bedauern auszudrücken, an der Versammlung a theilnehmen zu
können.

Demnächst ersucht der Herr Präsident Herrn Albrecht, den allgemeinen Bericht des

 

na A il ee

rbb hidseh

Ba en

säctmndhh hi A

 
