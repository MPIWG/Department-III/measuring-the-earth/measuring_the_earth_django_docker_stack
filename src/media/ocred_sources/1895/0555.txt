irren:
Yen wmwen

ir bekuinahd lsbäha
NNARUTTTERT TPTRTHTTRTT

aka
mm

Vitrnis
moarımır

eh

kh

E
ir
5
BE
in
er

13
43
22
sr
sr
su
22

Beilage B. IX.

NORWEGEN

Bericht über die Fortschritte der unter der oberen Leitung der N orwegischen

Commission der internationalen Erdmessung stehenden Arbeiten währen
der letzten Jahre.

Wie schon früher erwähnt (siehe « Verhandlungen der Conferenz der permanenten
Commission in Salzburg, 1888 », pag. 96) wurden die Arbeiten, welche der Gradmessungs-
commission oblagen, vom Jahre 1890 ab, unter die dort genannten wissenschaftlichen Insti-
tutionen vertheilt. Der Ausschuss des Storthings fand, der kleinen jährlich in Vorschlag
gebrachten Summe wegen, eine eigene Bewilligung im Staatsbudget für die Commission
nicht länger aufrecht halten zu müssen.

Infolge dessen haben die Direktoren dieser Instilutionen die direkte Leitung der
einzelnen Arbeiten übernommen, wogegen die gesammte Commission, wie früher, als Rath-
geber in geodälischen und astronomischen Fragen, und als Repräsentant für Norwegen in
der internationalen Erdmessung verbleibt.

Die Art der Arbeiten und ihre Bearbeitung ist übrigens ganz nach dem festgestellten

Programm fortgeführt und habe ich die Ehre, unten die einzelnen Berichte der Direktoren
mitzutheilen.

1. Astronomisch-geodätische Arbeiten.

(Direktor: Professor H. GEELMUYDEN.)

Die Berechnung der für die Gradmessung ausgeführten astronomischen Beobachtungen
ist vollendet und die Resultate sind publieirt in dem soeben distribuirten Heft : «Astronomische
Beobachtungen und Vergleichung der astronomischen und geodälischen Resultate. Chris-
tiania, 1895,» worauf man sich hinzuweisen erlaubt. Man findet auch da eine Vergleichung

der astronomischen und geodälischen Resultate einiger Beobachtungen in Netzen erster
Ordnung ausserhalb des Gradmessungsnetzes.

 
