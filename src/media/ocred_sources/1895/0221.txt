Henne aan E
Tran men a

er

m — en nn

Va mw

TRARNERUTEE NY

TIMarIEI TUT

Wit gr

AUT

ala

LE Eee
TR FRI TTFETTRTTI

;E
3

215

Endlich schlägt Herr Forster noch vor, die vorübergehende Bestimmung, welche in
Form des Artikel 16 auftritt, aus der Convention wegzulassen, und den Text dieses Artikels,
wie er in der dritten Sitzung modificirt worden ist, in der Form eines besondern Beschlusses

der Generalconferenz vorzulegen, nachdem diese die Verhandlungen über die Convention
abgeschlossen haben wird.

Herr Ferster wünscht noch, dass man sich darüber verständige, auf welche Weise
man der Conlerenz und den Regierungen die Verwendung der Dotation, welche für den
Breitendienst genehmigt werden wird, zu erklären hat; da aber die Stunde schon vorgerückt
ist und man um 2 Uhr zur Generalversammlung zusammentreten muss, so wird beschlossen,
diesen Gegenstand auf die folgende Sitzung zu verschieben.

Die Sitzung wird um 12 Uhr geschlossen.

 

 
