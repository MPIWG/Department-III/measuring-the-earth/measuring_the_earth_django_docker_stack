ie TE ERTL;

KLEE Jensen

ii

UATTLA

Amor)? © 1 RTRATAR (U) ARINgIL BLUE PL Wu

 

  

VII. — ltalie.

 

 

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

INDICATION 2
Jo AT N) m r TAm 7 / n)

N DES POINTS LATITUDE |\LONGITUDE | EPOQUE DIRESTRURS OBSERVATEURS INSTRUMENTS REMARQUES
230 | M. Beslar......: 429.49 052): 29252252 . 1887 Col. De Stefanis. | Ing. Guarducci. | Starke di 27 cm.
23, Me Nettore > 42 49 27 | 30 56 21 Top. Tacchini.
233 M. Amiata. ....... 420313.29 1714 1893.» | Ine. Derehr, id. id.
234 | Poggio Masson-

Hr 2... 42 57 46 | 28 09 42 1891 Col. De Stefanis. | Ing. Guardueei. id.
236 | Isola Capraia (M.‘*°

Castello)... 4302 571 | 27 28Al 1885 id. Lieut. Quaglia. id.
231 | Me bennino....... 43.06 03 | 30 33 10 1886 id. Ing. De Berar- id.

dinis.
233 | Poggio Montieri.. | 43 07 33 | 28 40 05 1880 Col. Ferrero. » Garbolino. id.
239 | Capo d’Arco....... ı 43 I 01 > 20 50.) 1880 » DesStefanis.| » nn Berar- id.
inis.
240 | Alta 8. Egidio.... | 43 18 40 | 2940 03. 1887: id. Cap. Pavese. id.
24) | D.°° Vigenanone.... 1231957 | 2901 14 1887 id. Ing. Guarducci. id.
242: VYitalba 0.2... a2 52,28 155 1893 Ing. Derechi. Top. Taechini. id.
243 | Isola Georgona .... | 43 25 38 | 27 33 28 1894 id. Ing. Loperfido. id.
244 ı M. Caııa 43.27 39 | 30 22 06 1886 Col. De Stefanis.  » Guarducei. id.
245 | M. Conero........ 43.3306 ı al 16.11 1886 id. id. id.
2 . id. ) :
246. kucardo.- .:. ....: 43.38.51.) 28 46.53 1887 id. Cap. Pescetto. \ id.
247. | Pratomagno ...... | 43.39 16 | 29 18 38 1887 id. » Pavese. id.
249-) Carpegna......... 43 48 06 | 29 59 02 1886 id. Ing. Derchi. il.
252 | Monteluro........ As 0231 29 26 19 1886 id. » Guarducci. id.
254 | M. Levane........ 43.5956 2916 55) 1886 id. » Derchi. id.
261 | Bertinoro.... 44 08 30 | 29 48 10 | 1886 id. id. id.
XV.
RESEAU DES ABRUZZES, MOLISE ET ROME.
Mile) Taborno..... ..... 41°05' 31”) 32°16'05”| 1873 Col. Chid. Ing. D’Atri. Repsold di 27 cm. |
173. Bilata.... 0.00. 41 06 50 | 31 56 38 | 1873 id. id id. |
175 | S. Lucia d’Apice.. Deja mentionne | |
184 | Roccamonfina ..... AL. 10 44 38.3807, 1009 id. id. id. |
186:| Bieeart.... ...... Deja mentionne || |
188 | Anzio =... 41 2648301722 | 1875 Col. Rerreror id. id. |
189 | Montemilette ..... 41 2056 2202 IE 188 » Chio. Cap. Almici. Starke di 27 cm. |
100 Saraeeno Al 21.22 ,322409, 189 id. id. id.
195 | Semprevisa ....... 4] 34 09 | 3045 22, 1884 Col. De Stefanis. | Ing. Garbolino. | Repsold di 27 cm. |
198 | M. Pisarello...... Al 28 36 30.1621 |, 1884 id. id. id. |
299, Ma Meta... ar A, 3,5015 | 1873 Col. an Cap. Almici. Id. |
; » De Veeehl. Ing. DAtri. Repsold di 32 em. |
20. | Rucole 41 43 22 | 32 33 20 | 1867-73 oe sei. ee |
203, M. (110... 2 41.458 5022|. 188 » DesStefanis.| Ing. Garbolino. | id.
204 | Fiumieino ........ Aal 46 14 | 29 598.22 1884 id. id., id.
205 | Cornacchina........ 41 86 06 | 31 18 06 | 1875 Col. Ferrero. gg. D’Atri: Repsold di. 27 cm.
208 | Spinalveto........ 41 53 45.) 32.0707 | 182 » Chiö. Cap. Magsia. id.
209 | M. Mario (Roma).. | 41 55 25 | 30 06 59 | 1884 » = en ng Garbolino. a) id.
: 5: » De Veechi. | Cap. De Vita. Bistor. di 27, em.

210 | Termoli......... 42 00. 13 | 32 39 40° 1868-73 } , cnis, J We en
211 | M. Serrasecea..... 42 0) 14 | 30.49 21 -1892 Ine. Derchr. Ing. Cloza. id.
212 | Gennaro.. ....: 42 03 36 | 30. 28 15 1886 Col. De Stefanis.! » Guarducei. 10.
213 | M. S. Ansino..... Deja mentionne
214 | Maiell2.......,.. „42 09 08 | 31 5.0 1873 » Chiö. Cap. Maggia. id.
216 | Sirente:....../2:.: 42 08 4l | 31 16 31 1892 Ing. Derchi. Top. Tacchini. id.
2 Bee Benne. . .. 42 10 23 | 32 22 46 1872 Col. Chiö. Cap. Maggia. id.
218 Soralte..-........ Deja mentionne
219 | Cappuceiata....... 412 19 5, | 31.28 >52 1892 Ing. Derchi. Ine. Gineyri. id.
22] | 7.7 Mucchia:....: 2.233 29.2 1873 Col. Chiö. Cap. Maegia. id.
228 | S. Branco ... ... A2 2% 52 | 31 08 19 1892 Ing. Derchi. Ing. Ginevri. id.

 

 

 

= La station definitive doit &tre encore executee. '

 

 

 

 

 
