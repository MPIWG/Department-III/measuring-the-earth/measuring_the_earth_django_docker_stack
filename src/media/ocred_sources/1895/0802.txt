 

I A

VIII. — Italie.

 

Furono eseguite le osservazioni angolari a tutti i punti di 1° ordine della rete geo-
detica compresi nell’ Italia continentale, nella Sardegna e nella Sicilia; mancano sola-
mente quelle ai punti Capraia e Montecristo, perch® interessanti il progettato colle-
gamento della Sardegna al continente. -

Le posizioni geografiche sono calcolate di punto in punto adottando gli elementi
di Bessel; le longitudini sono contate a partire dal meridiano dell’ isola del Ferro.

I confronti ottenuti con gli Stati limitrofi, dopo compensate le reti, sono riportati
nello specchio che segue. Come vedesi, le differenze sono di poca entitä e mantengonsi
costanti, ciö che depone in favore dell’ accordo fra le varie triangolazioni.

 

 

 

 

 

 

 

m ATIO SL
DELLE LATITUDINI 00MUNI DELLE LONGITUDINI COMUNI
see ae ger, SDIMEHRENZA | 2 0 0 0 erzıı DINRHRONZA
ITALIA STATI LIMITROFI ITALIA STATI LIMITROFI En
rm B m E

Franeia. Francia.
Monte Dabor 459.06 49" A506, Sl. 2... 2dlsanı 24° 13’ 39" un
» Chaberten A 5 52 44 57. 54 —ın | 2a 245] 2A 2A 53 — 2
Roceamelone. 49 12 4512183 — 2 24 44 25 24 44 28 — 3

Svizzera. Svizzera.
Limidario o Ghiridone.. 4062.07 23. 46°07' 26" — 3 26° 18 Al" 26°18' 45" Be au
Basodmer 2... 02.0.0. 46 24 4] 46 24 44 — 3 26 07 54 26 07 59 — 5
eramesmo 46 21 46 46 21 49 on 26 30 24 26 30,28 — 4
Menene. 2.2 22 46 07 24 46 07 27 — 3 26 48 29 26 48 34 — 5

Austria. Austria.
PelBosan 2.2... Aa BUN Ma — 3" Sana dal 33059. Agl — 8
Bremiti......0... 0 42 O0 8 42 07.21 — 3 3310, 16 33 10) 22 oo
Giovanmecho 41507011 41 50 04 — 3 33.38 21 33. 38 26 — 5
Bello nn AS Aa 45 42, 24 — 3 | 2,.2.28.29, 39 28 29 46 — 7
Pasubloszen. ae As, Ay 92 45 AT 35 — 3 | 28 50,25 28 530,32 — 7
Ba 45 52 24 45 52 27 og 29 27 45 ab 8
Ulmer anal. 46 03 53 46 03 55 22 30 54 02 30 54 10 8

| ai Be

 

 

 

ba su la ha äh aus u

 
