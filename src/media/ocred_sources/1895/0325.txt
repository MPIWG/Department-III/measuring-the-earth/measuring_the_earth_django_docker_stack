ö

ä
i

 

N Mm WTRTTTAAETTEENTFETER "YOPI RE

a au

FEnpweT pen

ü

Be

Am

 

OT

spricht dasselbe keineswegs den günstigsten Bedingungen zur exacten Bestimmung
dieser Coordinaten. Infolge dieser Ungleichförmigkeit des Materials habe ich zu einer
grossen Anzahl von Combinationen meine Zuflucht nehmen müssen und habe es nicht
vermeiden können, dass je nach der mehr oder minder günstigen Vertheilung der
Stationen die Gewichte der Coordinaten der einzelnen Punkte sehr verschieden aus-
gefallen sind. | |

Speciell für die Termine 1891.0—91.4 und 1892.7 lag ausreichendes Material
zur Bestimmung der Lage des Momentan-Poles überhaupt nicht vor; die in der nach-
stehenden Tabelle in Klammern aufgeführten Coordinaten dieser Polpunkte haben
daher aus dem Verlaufe der Curve während der übrigen Zeit interpolirt werden
müssen. Es brauchte dies aber aus dem Grunde nicht ganz willkürlich zu geschehen,
weil für diese Termine wenigstens insoweit Beobachtungsmaterial vorhanden war,
dass Näherungswerthe für die Radienvectoren in den betreffenden Meridianen aus
demselben abgeleitet werden konnten.

Wenngleich daher, worauf ich schon im Eingange meines Berichtes hin-
gewiesen habe, die gegenwärtig erhaltenen Coordinaten der Bahn des Poles noch des
definitiven Characters entbehren, so sind die in der nachstehenden Tabelle enthaltenen
Werthe doch dazu geeignet, sich über den Verlauf der Polbewegung innerhalb des
Zeitintervalles von 1890.0—1895.2 ein angenähertes Bild zu machen.

Coordinaten des Momentan-Poles bezogen auf den Mittleren Pol.

% Yy Combination. Gewicht.
(in 455 Secunden) m; Dy
1890,0 — 27.1 — 8.3
i ae Be Prag, Berlin, Potsdam, Bethlehem. 2.7 1,0
a ed
40 0 ze
5 15.4 12.6
: ir ns Pulkowa, Prag, Berlin, Bethlehem. 24 11
8 + 9.0 — 23.2 |
9 = 722.79
91,0 (— 20) (— 24)
I (—26) (—10)
2.1020) (468)
> (oe (co)
no de
5 + 6.5 + 16.6 }
6 418,4 + 3.4
m 025,200 |
8 +25.4 — 15.7 \ Pulkowa, Prag, Berlin, Strassburg, 6 RB,
9 18,6 — 16.8 { Rockville, San Francisco, Honolulu. 3. ;
92.0 -- 6.1 — 13.4
T — 1:2 — 8.1
2 — 18,9 — 19 )
3 — 24.9 + 0.4 \ Pulkowa, Prag, Berlin, Strassburg, Ss ä
4 — 22.8 + 5,0 San Francisco, Honolulu. ; >
5 — 9.0 +15,6 \ Kasan, Pulkowa, Berlin, Strassburg, > S
6 + 6.1 +-19.9 San Francisco. s M

 
