 

TRKIreSe

tkırasısaz
en wen.

i

aa a anna

 

ri Be

DI ee TTTTTNeETTETeg"*eeREHN

 

Ik

ala
TITTEN

 

ik Bam
TR RT TR TI

2
3
=
Ei
a

  
 

!
f
!
’
|
|
l

105

Forster comme President de la Xl® Conförence gencrale, et comme Vice-Prösidents M. le
professeur van de Sande Bakhuyzen et M. Y’amiral von Kalmar.

En prenant place au fauteuil prösidentiel, M. le professeur Fierster adresse A l’Assem-
bl&e le discours suivant:

« Messieurs,

« Je vous remercie sincerement de ’'honneur que vous m’avez fait en m’appelant aux
fonctions de President de la Conförence generale. Je tächerai de tout mon pouvoir de re-
pondre ä votre confiance.

« Permetiez-moi, pour introduire nos deliberations et en me raltachant aux grands
irails retrospeclifs par lesquels Monsieur le Ministre a salu& cette Assemblee, de traiter d’un
peu plus pres, quoique encore brievement, le developpement de l’Association geodesique,
depuis qu’elle a regu en 1886, & Berlin, son organisation conventionnelle et sa dotation.

« Les rapports detailles sur les progres de nos travaux depuis la derniere Conference
a Bruxelles seront presenles dans le cours de nos s6ances par les rapporieurs speciaux qui en
ont Ele charges. Puisque nous desirons tous certainement le renouvellement de l’organisation
convenlionnelle (dont la dotation cessera ä la fin de 1896), il n’est que naturel, en jetant un
coup d’eeil sur les grands progres r6alises dans ce domaine depuis 1886, de les mettre en
rapport avec les trails prineipaux de la Gonvention et avec les ressources materielles que
cette derniere a mises A notre disposilion.

« Une pareille tendance ne pourra toutefois influencer notre appre&ciation que dans le
sens du contentement au sujet des rösultats de l’organisation r&cenie, mais elle ne parviendra
pas ä determiner notre jugement sur la situation actuelle.

« Au premier plan du tableau que je vais esquisser devant vous se trouve un groupe
de progres importants qui n’ont, il est vrai, aucun rapport immediat avec l’organisation
acluelle et avec les ressources finaneieres decoulant des contributions internationales. Ce sont
plutöt les resultats des Lravaux geod6siques, astronomiques et de nivellement qui se sont de-
velopp6s dans les differents pays pendant les vingt dernieres anndes. Un trait particulier de
ce tableau doit &tre reconnu dans le fait qu’on a röussi depuis 1886 toujours davantage A
condenser les mesures des differents pays par des travaux theoriques et de calcul, gräce
essentiellement & l’aclivit& du Bureau central qui, töl apres 1886, a recu un perfectionne-

ment considerable dans son personnel seientifique aussi bien que dans ses installations et ses
instruments.

«Le nom deM. Helmert estsur vos lövres et je n’ai pas besoin d’ajouter que ce deve-
loppement rejouissant de la geodesie est dü, au moins indirectement, A l’organisation con-
ventionnelle qui nous regit.

« Dans ce domaine, il convient de mentionner surtout les grandes recherches sur les
deviations de la verticale, qui embrassent l’Europe presque enti6re.

«A ces etudes sont venues s’ajouter ensuite les mesures approfondies de l’intensite de

ASSOCIATION GEODESIQUE — 1%

 
