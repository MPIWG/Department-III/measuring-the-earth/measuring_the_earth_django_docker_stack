Fe en

tar

|
l
\
|

 

 

 

 

— 13 —

v1. — France et Algerie.

 

 

INDICATION

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

N° LATITUDE LONGITUDE EPOQUE INSTRUMENTS REMARQUES
DES POINTS . ET OBSERVATEURS i
Triangulation de Jonetion de la Base de Bordeaux au Parallele Moyen.
259 | Chantillae......... | 45°19'30”] 17°24 58” . a! an
z . seneral de la Kuerre,
>32 | Chadenae 2.2...) D&ih mentionnee. Tome VI
261 | Soubran...2....... 42 ı u 352
262 | St. SAWR. ........: 49, 8954|, 7 1253
263 | Le Gibault......... 45 10 54 | 17 32.44
264 | La Pouiade........ 4a 2329 172245
265 | St. Andre de Cub-
Te... 445951 | 17 236 5
266 | Montagne......... 44 55 53 | 17 31 59 Serel
267 Ta Same.  .... 444611 | 1721 2 ln
268 | Bordeaux ........: 24509 7 5 4 ne
269 | Blanquefort ........ 44 54 38 | 17 139 Colonel Brousseaud, lieu- De a a
20 Dmsım 4458 39 | 165311 ) 1826-27. | tenanis Kossand, Lexrei| les  Baun
211  Captieux......... 44 51 38 | 16 48 58 et Dupuy. a
2a meld. 45 4l 34 | 23 51 36 od u eu
273 | Bellachat.......... 45 3230 \24 4 9 executees 20 rEpe-
SA Brne. 000. 521 9|2351 2 tions.
275 | Encombres......... 4a 1451 242 651
276 | Mont Jourvet....... 5 28) 42 | 22 18 Il
277 | Roche Chevriere...| 45 17 37 | 2423 8
278 | Mont Tabor....... 45 051 2419839
79 Ambin...:....... AD 025 24923255
280 | Chaberton......... AA 57 54 | 24 24 48
281 | Albergian......... Aa 03243932
282 | Rochemelon........ 45 12 13 | 24 44 28
Triangulation du Parallele de Rodez. (Partie Orientale.)
283 | Puech de Monsei- \ m
me 41° 12582) 20935 352
62ei63 Rodez, La Gaste... Deja mentionnes.
284 | Cougouille......... 23 Si Bil. 20) Auf 21
285 | Puech d’Aluech....| 4420 9|21 4583
286  ZW’Hort de Dieu....' 44 719 | 21 14 40
287 Roc de Malpertus.. 44 24 5 | 21 30 33
- 288 | Le Guidon de Bou-
a 44 6 59 | 21 56 56
289 | Dent de Rez....... 44 25940922 957
290 | Montmou.......... AA 19 24 1,22 23 31
Grand Montagne..., 43 58 34 | 222546 \ ]|g94.05 Capitaine Durand, Ad
Mont Ventoux..... 44 102712256 31] "> | lieut.= Rozet et Levret. Id.
Les Houpies....... 43 42 44 | 22 38 45 |
Le Leberon........ 494850 | 25 U
Eure -....:22...0%- A723 1293 20 38
St. Julien........- AS Al 2 23 34 18
Mourr6 de Chenier. | 43 50 30 | 24 052
Les Monges........ 44 1046 | 223 5] 28
Le Grand Coyer....| 44 6 1'!2421 12
Le Grand Berard..'| 44 26 57 | 24 19 25
La Chains. ....-.. .< 4344 5 24 1953)
Cheiron ........... A853 24 3789

 
