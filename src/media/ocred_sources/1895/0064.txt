 

Ü
N

1
j
Mi
IR
un
d '
s
E
fe
h
#0
3

see

FÜNFTE SITZUNG

Mittwoch, 9. October 1895.

co)

Der Präsident Herr Fwrster eröffnet die Sitzung um 3 Uhr 15 Minuten.
Es sind anwesend:

Die Herren Mitglieder der permanenten Gommission : Faye, Forster, Helmert, Henne-
oO } ’ 9 ’
quin, Hirsch, von Kalmar, van de Sande Bakhuyzen, von Zachariae.

Die Herren Delegirten : Albrecht, d’Arrellaga, Bassot, Bouquet de la Grye, CGeloria,
Gobo de Guzman, Fergola, Geelmuyden, Guarducei, Haid, Karlinski, Koch, Lallemand,
’ g I Y ’ ’ 3
Löw, Lorenzoni, Miyaoka, Nell, Oberhoffer, Omori, von Orff, Rajna, Rosen, von Schmidt,
? ’ i 2 9 ’ 3)
Schols, Seeliger, von Sterneck, Tisserand, Tillmann, Westphal, Weiss.

Die Herren Eingeladenen : Börsch, Erfurth, Galle, Haasemann, Hecker, Hegemann,
Kühnen, Malthias, Marcuse, Mertens, Dr. Meyer, Schnauder, Schumann, Serbl, Stadthagen,
Thiesen, Vogler, Weingarten, Weinstein.

Der ständige Secretär Herr Hirsch verliest das Protokoll der vorigen Sitzung.

Zu einer Reklamation des Herrn Bakhuyzen, welcher der Herr Secrelär alsbald
Rechnung trägt, fügt letzterer die allgemeine Bemerkung hinzu, dass es im Interesse der
Kürze des Protokolls nicht nur gestattet, sondern sogar geboten sei, mehrfache Ausführungen
desselben Redners mit einander zu verbinden. Das Protokoll wird alsdann von der Versamm-
lung genehmigt.

Da es nicht möglich gewesen ist, die Vorschläge der niederländischen, französischen
und österreichischen Delegirten, an deren Abfassung auch noch andere Vertreter betheiligt
sind, rechtzeitig im Druck fertig zu stellen, so schlägt der Herr Präsident vor, zunächst einen
Bericht des Herrn Helmert über die Messungen der Schwerkraft anzuhören. Es empfehle sich
das umsomehr, als einige Gäste der Versammlung beiwohnen, für welche dieser Gegenstan«
ein besonderes Interesse biete, und welche unter Umständen geneigt sein könnten, auch
ihrerseits Mittheilungen zu dem Gegenstande zu machen.

 

 

IM

id

gapkäh

lad

bb yulaukuc

 

lub u.

an uch bh au

 
