 

 

Te

lön attribuant, comme on )’a generalement lait jusqu’alors, aux seules erreurs acci-
dentelles, les &carts de fermeture des polygones niveles, on est fatalement conduit & des
chiffres trop forts pour Ja valeur probable du coefficient kilometrique de ces erreurs.

Ainsi, pour le röseau frangais, ee coefficient serait plus que doubl& (1"""7 au lieu de
0""8); pour le resean prussien, l’Ecart serait ä peu pres le meme (1""5 au lieu de 0""8).

Toutes choses egales J’ailleurs, le desaccord entre les deux chiffres eroit avec la
erandeur des sections et des polygones. On se trouve ainsi conduit A attribuer aux räseaux
a mailles Etroites, par rapport aux reseaux A mailles larges, un apparent exces de preöei-
sion, alors meme que les coefficients kilometriques des erreurs aceidentelles et syst&matiques,
correetement calcules, seraient exactement les m@mes dans les deux cas.

Cette maniere de prösenter les choses est done trompeuse.

Les reseaux A mailles &troites prösentent, il est vrai, ’avantage d’assurer une meil-
leure compensation des erreurs systematiques; mais, pour ces reseaux, comme pour les
reseaux A grandes mailles, il est n&cessaire d’indiquer dösormais le coefficient d’erreur syst£-
matique, aA cöte du coefficient d’erreur accidentelle.

6° Pour trois des quatre reseaux de nivellement vises dans le tableau ci-dessus, la
part des erreurs systmatiques dans l’ensemble des &carts de fermeture est de beaucoup
superieure ä celle des erreurs accidentelles.

Si, en effet, celles-ci pouvaient Etre absolument &limindes, les &carts de fermeture ne
diminueraient en moyenne que de 13 & 19 °/o, soit de 1/8 & 1/5 de leur valeur; tandis
que la suppression complete des erreurs syst&maliques reduirait immediatement de moitie, ou
a peu pres, les Ecarts de fermeture en question.

Le reseau prussien, seul, fait exception A cette regle : ”’annulalion des erreurs aceci-
dentelles reduirait de 30 °/, l’&cart moyen de fermeture de ses polygones, tandis que la
suppression des erreurs syst&matiques abaisserait seulement de 26 °/o cet cart moyen.

7° Pour augmenter la precision des altitudes des reperes, et l’exactitude avec
laquelle, par exemple, sont actuellement dötermindes les denivellations des mers entre elles,
il ne suffirait done pas de chercher, au prix de nouvelles complications dans les methodes
et les instruments, une röduction des erreurs accidentelles des nivellements. Il serait, en
een6ral, beaucoup plus utile de diminuer les erreurs syst&matiques.

Malheureusement, de ce cöte, les progres sont plus difficiles & r&aliser, les erreurs
systömaliques ne paraissant jusqu’alors pr&senter aucune relation, ni avec les instruments,
les methodes, ou les operateurs; ni avec la nature du sol, les circonstances atmospheriques,
ou l’orientation des cheminements.

8° Dans l’ötat actuel des choses, le meilleur moyen d’altönuer l’effet des erreurs sys-
(ömatiques dans un reseau de nivellement est encore de restreindre les dimensions des
mailles. En lollande, par exemple, la longueur moyenne d’une section ne depasse pas 40
kilometres, alors qu’elle atteint 70 A 135 kilometres dans les quatre r&eseaux ci-dessus vis6s.

 

rd ana ee

bh

LA
