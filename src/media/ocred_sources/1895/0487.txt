Van mp WETTER en nt

ara

lu rl

HE

ih
iR
a=
IE
um
43
ie
s>
er
“E
sE

 

167

XVI. Messungen durch die Coast and Geodetic Survey, U. S.A.

Die Tabelle ist im wesentlichen eine Reproduktion einer von Mr. Chas. A. Schott auf
Anordnung von Herrn General Duffield, Superintendent G and G. Survey, zusammengestellten
Uebersicht der vorläufig ermittelten Ergebnisse aller Pendelmessungen der (. and G. Survey,
welche mir handschriftlich zur Verfügung gestellt wurde, wofür ich mich zu grossem Danke
verpflichtet fühle.

Ich gebe hier einen Auszug des Begleitschreibens von Mr. Schott :

« As is well known the gravitation researches of the Survey, theoretical as well as
practical, were in charge of C. S. Peirce from 1873 to the close of the year 1891, and after this
day they were carried forward, mainly experimentaly, by Dr. J. C. Mendenhall. The impro-
vements made by him in the apparatus and in the method of swinging, and in the intro-
duction of half-seconds’ pendulums (and even quarter-seconds’ pendulums) at last placed the
field work upon a thorough practical footing. It will be seen by the tables that Ihe most recent
extension of the pendulum work is mainly due to him and to Assistants E. D. Preston and
G. R. Putnam, who continued on the lines thus marked out.

« The partial results of the observations for gravity are scattered over a number of
the annual reports, and since no definitive values have as yet been reached, Ihe results as
they stand, have been collected, systematicaly arranged, as far as possible.

« To facilitate references to the gravity researches of the Survey the following prin-
cipal reports may be cited:

Appendix No. 15, C. and G. S. Report for 1876, 0. S. Peirce, observations ol 1873-76,

» Del, » » 1883, » » 1879-80,
» on » >» 1884, E. Smith, » 1889-83,
» » 14, » » 1888, E.D. Preston, » 1883-87,
) „12 » » 1890, » » 1889-90,
» » 15, » > 1891, J.C. Mendenhall, » 21,

» » 12 » > 1893, E. D. Preston, » 1891-99,
) 4, » » 1894, G. R. Putnam, » 1894. 1

« For the sake of uniformity all tabular results have been made to depend on an
assumed standard value of absolute measure at Washington for which it has been thought
expedient to adhere to Ihe value heretofore adopted, viz : 9,80100 m for the station at the
Smithonian Institution (northest part of building) and later on, when transferred to the base-

ı Vergl. auch die* Berichte über die Pendelmessungen in den Verhandlungen zu Nizza, 1887, p.
83 und 15, und die Verhandlungen in Paris, 1889, sowie die Verhandlungen in Brüssel, 4892, welche
letztere eine mehr chronologische Uebersicht der Stationen (ohne Messungszahlen) geben.

 
