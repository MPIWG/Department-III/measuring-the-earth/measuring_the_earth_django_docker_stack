Han merwarer
Nennen

ANDRE, KUN

NR

it
"

ia

ü
Im
ES
=

®

IR
ie
BE
HE

Annexe C. I.

RAPPORT

de M. le D’ Ch. Guillaume aM. le Professeur Feerster sur les recherches faites
au Bureau international des Poids et Mesures concernant les mötaux Pro-
pres a la confection des regles etalons.

A la suite des recherches faites au Bureau international sur les metaux destines A la
construction des regles &talons, recherches communiqu6des au Comite international des Poids
et Mesures dans sa session de 1892, la Societ# Genevoise pour la construction d’instruments
de physique entreprit la confection de regles de 1 metre en bronze blanc, le nickel ne parais-
sant pas, & cette &poque, pouvoir &tre obtenu industriellement A un 6tat d’homog£neite suffi-
sante. Le bronze employ& par la Soeidte Genevoise 6tait l’alliage A 40 °/o de nickel, brevete
par la maison Baasse et Selve & Altena (Westphalie), sous le nom de Constantan. Les exp&-
riences faites au Bureau, sur des fils ou des lames de ce metal, avaient donn& des rösultats
satisfaisants. Mais on reconnut bientöt que les harres de plus fortes dimensions contenaient
souvent des pailles ou fissures qui les rendaient absolument impropres & la confection d’&ta-
lons de 1 metre de longueur. Des essais faits au Bureau international sur de forts barreaux
atteignant 6m de diametre et destinds A la construction de poids &talons ne furent pas plus
satisfaisants. Meme apr&s un nouveau tirage et un forgeage, ces barres contenaient des
cavites de plusieurs centimetres cubes. Certaines parties du metal purent ötre ulilisees, mais
le reste dut etre mis au rebut.

En presence de ces insucees repeles, il parut desirable de reprendre, de concert
avec des &tablissements industriels, des essais avec d’autres alliages de nickel, et du nickel
pur. (es essais ont conduit ä des rösultats encourageants que j’indiquerai dans ce Rapport,
en meme temps que quelques essais sur le constantan.

Constantan. Les essais ont port& sur une lame de mm d’epaisseur, de 30 om de

longueur et de 125 de largeur. On a trouve pour le module d’elasticit du metal
16 600 kg/mm?

 

 
