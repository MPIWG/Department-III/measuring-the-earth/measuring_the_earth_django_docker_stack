 

ü
“
|
'
B

N

184

des Werkes mit wenigen Ausnahmen im wesentlichen anscheinend mit der Attraktion der
Gebirgsmassen.

In Schweden hat das bereits in den « Verhandlungen zu Freiburg» erwähnte grosse
Lothabweichungssystem in der Nähe des Küstenpunktes Bredskär eine Erweiterung durch
eine lokale Untersuchung mit 7 Breitenstationen erfahren ; Herr Prof. Rosen hat die Absicht,
diese interessante Studie noch weiter fortzusetzen ; vergl. die «Verhandlungen in Brüssel,
1892 »,.8::020.

Es ıst zu bemerken, dass zur Zeit das System der norwegischen Lothabweichungen
noch ganz isolirt ist; mit dem schwedischen System könnte es aber leicht verbunden werden,
da die dazu erforderliche kurze Dreiecksverbindung von Dragonkollen nach Osten zu bereits
gemessen ist. Ebenso wäre es erwünscht, dass die vorhandene Verbindung Schwedens mit
Dänemark bei Kopenhagen-Lund nutzbar gemacht würde, um das schwedische Lothabwei-
chungssystem mit dem centraleuropäischen in Verbindung zu setzen. Hierdurch würde eine
Aufgabe gelöst werden, deren Erledigung schon General Baeyer als eine der ersten der inter-
nationalen geodätischen Vereinigung ins Auge fasste. — Bei dieser Gelegenheit möchte ich
auch dem Wunsche Ausdruck geben, dass die Insel Bornholm mit Schweden durch ein Drei-
ecksnetz verbunden werden möchte. Es würde dadurch die Lücke in dem Meridianstreifen
zwischen der schwedischen und der deutschen Ostseeküste theilweise ausgefüllt werden und
ein wichtiger Beitrag für die Figur der Erde an jener Stelle zu erlangen sein. Im Falle des
Entstehens von Schwierigkeiten für eine genaue und vollständige Verbindung würde es schon
genügen, für die geographische Breite allein eine hinreichend genaue geodätische Ueber-
tragung von Schweden nach Bornholm herzustellen.

In Russland ist zu der Längengradmessung in 52° Breite diejenige in 47.5 Breite
hinzugetreten, welche bei 19° 42° Ausdehnung in Länge von Kischinew bis Astrachan reicht.
Beide Parallelbögen sind ausser durch die bekannte grosse russische, sie im Westen Russ-
lands durchschneidende Breitengradmessung noch durch einen östlich liegenden Meridian-
bogen, sowie einen solchen von mittlerer Lage verbunden. Professor Shdanow hat den fünf
Bögen ein Rotationsellipsoid angepasst, das wieder die Abplattung des Bessel’schen Erdellip-
soides zeigt, aber die grosse Halbaxe um 320 m. grösser hat:

a — 6377747, a — 1: 999,7.

3 Vergl. ausser den «Verhandlungen in Brüssel, 1892», S. 506 u. ff, sowie 5. 617, besonders die
Zusammenstellungen in den « Verhandlungen in Genf, 1893 », S. 183 und in Bd. 50 der « Sapiski der kriegs-
topographischen Abtheilung des russischen Generalstabes, » publ. von Generallt. Stebnitski, 1893, S. 313 u. f.,
wozu hinsichtlich Orenburg-Orck noch in Bd. 52 eine Verbesserung tritt, die die relative Lothabweichung beider
Orte in Länge von 35.52 auf 14736 reducirt (siehe den franz. Auszug zu Bd. 52, S. 1/2).

Es sei hier auch an die Berechnung derjenigen Ellipse erinnert, welche General Bonsdorff der
russisch-skandinavischen Gradmessung angepasst hat (Fennia, I, Nr. 15; Helsingfors, 1889; nach Bd. 42 der
Sapiski). Er fand

a— 32372 56337, «= 1:298.6;

also auch nahezu die Bessel’sche Abplattung, aber gegen 950 = für die grosse Halbaxe mehr als Bessel. Vergl.
hierzu den Lothabweichungsbericht in den « Verhandlungen von Nizza, 1837», 8.7.

 

ch bush aaa u

u

 
