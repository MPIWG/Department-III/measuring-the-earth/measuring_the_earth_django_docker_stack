 

 

196

welches die Erfahrung vollkommen gerechtfertigt hat. Es wird vorgeschlagen, die gegenwärtige
Anzahl von 11 auf 7 Mitglieder zu reduzieren, denn es gelingt immer leichter, weniger zahl-
reiche Räthe zusammenzubringen, und diese arbeiten auch mit grösserem Fleiss, denn
jedes Mitglied fühlt einen grösseren Theil von Verantwortlichkeit. Herr Hirsch begreift nicht,
wie es möglich wäre, dass ein halbes Dutzend wissenschaftlicher Spezialcommissionen und
ein Verwaltungscomitö mit der Harmonie und dem Zusammenhang arbeiten könnten, welche
zum Gelingen nöthig sind.

Andererseits ist der französische Entwurf, die leitende Commission aus so vielen
Mitgliedern, als Staaten der Convention angehören, also aus 98 zusammenzusetzen, seiner
Ansicht nach mit der Unterdrückung der permanenten Commission gleichbedeutend, denn
man kann offenbar eine solche Versammlung nicht alle Jahre einberufen ; das wäre nichts
Anderes als eine Generalconferenz selbst. Man wäre so genöthigt, daneben noch einen
engeren, mit der Verwaltung beauftragten Rath zu haben, was auf den niederländischen
Entwurf zurückkommen würde.

Herr Faye erklärt in kurzen Worten die Gedanken, welche die französische Gom-
mission in ihren Vorschlägen geleitet haben. Nach französischer Ansicht ist es natürlich und
gerecht, dass in einer wissenschaftlichen Vereinigung, bei der die souveränen Staaten ver-
treten sind, jeder Staat mit vollkommener Gleichheit im Verwaltungsrathe vertreten sei, wie
es in der ersten Zeit der «Mitteleuropäischen Gradmessung » der Fall war, während das
gegenwärtige System einigen Ländern den Vorzug gewährt, in der permanenten Commission
vertreten zu sein, was nicht verfehlt für die andern, welche nicht stimmberechtigt sind,
etwas Demüthigendes zu haben. Die französische Commission schlägt also, indem sie sich
auf die überlieferten Gebräuche der « Erdmessung » beruft, die gerechteste Organisation
vor, welche zu keiner nationalen Eifersucht Veranlassung giebt.

Herr Ferster gesteht, dass die französischen Vorschläge betreff der Anzahl der Mit-
glieder der permanenten Commission in seinen Augen das Mass überschreiten; eine so
beschaffene Commission wäre wirklich nichts anderes als eine Generalconferenz, welche
dann immer noch eines engeren (omites bedürfte. Im Anfange unseres Bestandes, wie auch
in der späteren Entwickelung, wurden übrigens die Mitglieder der permanenten (Gommission
nicht als Vertreter ihrer Länder, sondern vielmehr als Gelehrte, die ihrer persönlichen Ver-
dienste wegen gewählt wurden, betrachtet, und sie waren mit der Ueberwachung der Inte-
ressen der internationalen Wissenschaft, nicht der einzelnen Länder, beauftragt. Es scheint
Herrn Feerster, dass man an diesem Gesichtspunkte um jeden Preis festhalten sollte. Aus
demselben Grunde genügt auch das niederländische System mit einem einfachen Verwaltungs-
comite für eine internationale wissenschaftliche Vereinigung nicht.

Herr Hirsch wünscht den verschiedenen Gründen, welche man geltend gemacht, nur
noch die einzige;Bemerkung hinzuzufügen, dass das neue französische System in Wirklichkeit
mit der Aufhebung der permanenten Commission, welche man in Frankreich zuerst vorge-
schlagen hat, zusammenfällt. Eine Versammlung, welche aus den Vertretern der sämmtlichen

3
4
3
3

arena ee hei

Aal

hehe

sus male bb a bh Äh aa

 
