a um IRA All

Jul

FT EIrTTTTERTTERR LLTIBINGIL ALL KIEL}

 

 

nn

v1.—France et Alverie.

 

 

 

Mtakiai erneuern ERTTRTE N N.

N°

 

 

INDICATION
DES POINTS

 

LATITUDE |LONGITUDE| EPOQUE

 

 

 

 

DIRECTEURS
ET OBSERVATEURS

 

INSTRUMENTS

 

REMARQUES

 

Lablia. ... .....
Kef Azrou.........
Ras Eddaieh.......
ANUA. 0.00:
Chechar .  .. .

Tagueltiout .......
Mähmel®. ... .....;-
Amar Kaddon......
TOuUgoar...........
Bourzel... ......
Mettlilt .........%
Ahmar..
El Arrousine......
Bou Dokhan.......
Mimouna..........
Guenama...........

Dokhan :... .......
Mergueb .........:
El 6roun..........
Serdoun...........
Lazereg...........
GKouron............

GKouron...:........

 

 

Ras Merkeb ....... \

Guern-Arif........
Mimouna-Cherguia.
Touila-Makna......
Garet el Ghachaona.
Ksel.. 3:

Bou Derga.........
Geryville..........

Triangulation du Parallöle Sud-Algerien. (Partie Orientale.)

 

Deja mentionnes.
Sala. 2 | 02309 32
34 2512| 626 33
34 1156 | 6 20 41
34 31 14 | 6 12 54
212,613
34394 | 55511
34 2223| 544 24
34 43 37 | 5 33 42
34 24 37 | 52710
an
34 45 14 | 5 11 44
> 2% 1056
4012,52 1%
35 1222| A 59 58
34 59 49 | 4 30 57
30 18 45 | 4 34 10
>19 9, 41838
SV, 3.50

 

Deja mentionnes |

352.[9, 25.

ZojgT g"
35 9383| 25819
34 49 36 | 2 50 5l
ss 3D|ı 2551
3449 27 | 2 19 34
5 12012 85
3A43.4, 2 1
345851 | 15324
34 1429| 138% 1894
342638 | 139 10
025, 15%
“u, 08
34 1932| 05931
312 2.332
A493 093
34 2459 | 0 31.50
342945 | 0 1420

 

Deja mentionnes

 

Capitaine de Fonlongue.

Capitaine de Fonlongue.

 

Petit cercle azimutal
a 4 mieroscopes du
service geographi-
que.

Petit cercle azimutal
&a 4 microscopes du
service g&eographi-

“ que.

 

Triangulation du Parallele Sud-Algerien (Partie Oceidentale.)

Dej& mentionnes. |

34° 2 | 025020.

3349 8 eıs dl

3340,32, 0.47 34

23539! 052

33 13 26 1.20.19 1889 Capitaine Barisien.
328 L60b5

333733) ! 1012

334058 .12 1027

/

|

Petit cercle azimutal

& 4 mieroscopes du

service g&ographi-
que.

 

|

 
