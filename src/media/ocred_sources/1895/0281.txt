a ee en

ee a nr STERNEN TR

(1

In m KAM U Ik

num

am Lingen pam niır

 

2
Art. 9. — Die Festsetzung der Beiträge geschieht nach folgenden Abstufungen :

ı) Staaten mit einer Bevölkerung bis zu 5 Millionen zahlen 240 M. (300 Fr.) jährlich ;

b) Staaten mit einer Bevölkerung von mehr als 5 bis IQ Millionen zahlen 400 M.
(500 Fr. jährlich) ; |

c) Staaten mit einer Bevölkerung von mehr als 10 bis 20 Millionen zahlen 800 M.
(1000 Fr.) jährlich ;

d) Staaten mit einer Bevölkerung von mehr als 20 Millionen zahlen 1800 M. (2250
Fr.) jährlich.

Eventuelle dauernde und vorübergehende Erhöhungen der Dotation werden nach
denselben Abstufungen repartirt.

Die Höhe der Beiträge der einzelnen Staaten wird durch Beitritt eines neuen Staates
nicht geändert. Der neu hinzutretende Staat zahlt den seiner Bevölkerungsstufe entsprechen-
den Beitrag.

Art. 10. — Die Zahlungen aus der Dotation der Erdmessung erfolgen durch das
Gentralbureau auf Anweisung des Präsidenten oder in seiner Verhinderung auf Anweisung
seines Stellvertreters.

Art. 11. — Die Abstimmungen in der Generalconferenz bei der Wahl des Präsidenten,
desVicepräsidenten und des ständigen Secretärs, sowie bei allen geschäftlichen Entscheidungen
geschehen nach Staaten, wobei jeder Staat eine Stimme hat.

Die bei der Generalconlerenz nicht direkt vertretenen Staaten können ihr Stimmrecht
auf einen der anwesenden Delegirten übertragen ; doch darf keines der anwesenden Mitglieder
mehr als eine solche Vertretung übernehmen.

Zur Giltigkeit der gefassten Beschlüsse ist die Anwesenheit oder die Vertretung der
Delegirten von mindestens der Hälfte der in der Gonferenz vertretenen Staaten erforderlich.

Die Beschlüsse der beralhenden permanenten Commission erfordern zu ihrer Giltig-
keit, dass mindestens ein Drittel ihrer Mitglieder in der vom Präsidium festgestellten Frist ihr
schriftliches Votum eingesendet hat.

Art. 12. — Für wissenschaftliche Fragen werden von der Generalconferenz Spezial-
commissionen eingesetzt, wobei es jedem Delegirten freisteht, welcher dieser Commissionen
er beitreten will. Soll eine Beschlussfassung eintreten, so erfolgt die letztere in der allge-
meinen Conlerenz, wie bei allen wissenschaftlichen Angelegenheiten, mittelst absoluter
Stimmenmehrheit aller in der Sitzung anwesenden Delegirten.

Art. 13. — In gemischten oder zweifelhaften Fällen muss die Abstimmung nach
Staaten (Art. 11) erfolgen, sobald dies von sämmtlichen Delegirten, welche die Stimme eines
Staates abzugeben haben, verlangt wird.

Art. 14. — In Fällen von Stimmengleichheit entscheidet sowohl bei Abstimmungen
nach Staaten, als auch bei solchen nach Delegirten die Stimme des Vorsitzenden.
Art. 15. — Die vorstehenden Festsetzungen haben solange Giltiekeit, bis sie durch

eine neue Verständigung der Staaten abgeändert werden.
Nachdem die Erneuerung der permanenten Commission durch die Generalconferenz

 
