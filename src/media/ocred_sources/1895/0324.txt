 

4

 

Kasan.  Pulkowa,. Wien. Prag. Berlin. Potsdam. Karlsruhe, Strassburg. Bethlehem. Rockville. 8. Francisco. Honolulu,
1894.0 40109 — — -_ — -tolıı +ol!ız +0'o8 -+o’ıı — _ = |
ı 0.08 — — == — 40.07 +0.13 40.01 +0.06 — — 4
2 40.05 — — — — 40.02 40.08 —o.0oı +0.01 — = ae i
3 0.00 = — — — —0.01 0.02 —0.02 —0.06 — — i
4 —0.02 — _ — — 0.01 —0.04 —0.03 —0.14 — nn on i
5 —0.03 — u — — -40.02 —0.10 —0.03 —0.18 _ -_ e ]
j 6 —0.03 — _ — — —0.03 —0.1I6 —0.03 —0.17 — — = ;
Y 7 —0.03 —_ — E= — —0.08 —0.20 —0.03 —0.I0 — _ rn :
8 —0.03 — — — _ —0.09 —0.17 —0.02 —0.02 — — Be !
| 9 —0.03 — — — — -—0.10 —0.10 —0.01 40.06 — — a |
95.0 —0,04 — — — —0.11 —0.08 40.01 40.10 — — —_
| 7.007 —_ — —ı 0,14 0. 0.00 40.12 — — —
2 —0.08 — _ — — 0.14 —0.16 —0.05 40,09 — — —

Bezeichnet man diese Werthe mit /p, die auf den Meridian von Greenwich
bezogenen westlichen Längen der Beobachtungsstationen mit A und setzt ein Coor-
dinatensystem voraus, dessen positive X-Axe nach Greenwich hin gerichtet ist, während
die positive Y-Axe einer westlichen Länge von 90° entspricht, und dessen Coordinaten-
anfang mit einer mittleren Lage des Poles zusammenfällt, welche durch die eingeführten
Näherungen zur Ermittlung der /p bestimmt wird*), so ergeben sich, von der Fehler-
gleichung:

 

Ag+v=xzcosA + ysınd
ausgehend für die rechtwinkligen Coordinaten des Momentan-Poles bezogen auf eine
mittlere Lage des Poles die Ausdrücke:
[fY cos A] [sin °A] — [49 sin A] [sind cosA]
. [sin 22] [cos °4] — [sin A cosA]? |
__ [49 sin A] [cos A] — [A cos A] [sind cosA]
nn [sin 4] [cos 2] = [sinAcoA?_

Die Gewichte der Coordinaten sind:

 

 

. [sind cosA]?
—[fcosA —
Mr | [sin 24]
; [sin A cosA]?
= /sın 2A:
Py [ \ [cos °A] |
| Um auf Grund der obigen Formeln zuverlässige Werthe für die Coordinaten
\ des Momentan-Poles zu erhalten, ist es nothwendig, die Resultate von Stationen mit
einander zu combiniren, welche in betreff der geographischen Längen eine geeignete

Vertheilung um die Erde herum aufweisen. Da nun aber das gegenwärtig vorhandene
Beobachtungsmaterial nur auf dem Wege der freiwilligen Cooperation erlangt worden
\ ist und weder in bezug auf die Auswahl der Stationen, noch auf die Dauer der Be-
obachtungen an den einzelnen Orten von vornherein ein bestimmter Plan vorlag, ent-

f ger ren = Bien ur Kenn

*) Eine Unbestimmtheit in der Lage des Coordinatenanfanges geht aus dem Umstand hervor,
| dass der Natur der Sache nach nicht eigentlich eine Bestimmung der Coordinaten x und y selbst,
sondern nur eine solche der Coordinatendifferenzen möglich ist; doch verbleibt die Unsicherheit
welche hieraus erwächst, vollständig innerhalb der Genauigkeitsgrenzen, mit welchen sich die Ooor-
dinatendifferenzen aus dem vorliegenden Beobachtungsmaterial ermitteln lassen,

sarreüshmuchhub Kali au

 

   
