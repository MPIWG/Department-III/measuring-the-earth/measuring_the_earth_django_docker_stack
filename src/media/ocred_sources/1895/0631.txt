va

Hein ae
Nee

3 111) ROW eremmrEmEEEnERE— Tee A An AA 111 571 Ei 1
N MeraT WRITER TOTER SENT "N

ia,

LT

Hain

Ent irır © LIRLRRLITR LU Ring EM LLTEmLN

Beilage C. I.

MITTHEILUNG

über einen

Neuen Nivellirapparat und eine metallische Nivellirlatte,

PROFESSOR OA NV OCT ER

Schon im Jahre 1886 hatte ich die Ehre, Gast der Conferenz zu sein. Ein Vortrag
des Herrn van de Sande Bakhuyzen gab damals Kunde von Jen erfreulichen Erfolgen des
Präcisionsnivellements der Niederlande. Der mittlere Kilometerfehler, nur in einer Weg-
richtung, betrug ungefähr I mm, und aus dem Abschluss grösserer Schleifen ergiebt sich ein
geringerer Betrag des mittleren Kilometerfehlers als aus den kleineren Polygonen, woraus
Herr van de Sande Bakhuyzen den Schluss zog, dass die Anknüpfung der Nivellierzüge an
die Höhenmarken einen beträchtlichen Beitrag zum mittleren Fehler geliefert habe.

Der Vortrag war geeignet, die Frage anzuregen, ob nicht bei Anwendung kleinerer
Zielweiten, als in den Niederlanden genommen worden waren, der mittlere Fehler noch ver-
ringert, eiwa auf das wünschenswerthe und theoretisch wohl erreichbar erscheinende Mass
von !/, mm. eingeschränkt werden könne. Freilich müsste dann entweder mit sehr fein ge-
theilten Skalen gearbeitet werden oder es waren, bei der in Holland üblichen Einstellung der
Sicht auf gegebene Zielpunkte der Latte, starke Libellenausschläge hinzunehmen. Beides zu
vermeiden gelang wohl nur durch ein Nivellirinstrument nach Art des Kathetometers, und
ein Kathetometer von Fuess, welches einem solchen Instrument als Vorbild dienen konnte
(und auch gedient hat), sah ich noch in demselben Jahr in der Ausstellung: wissenschaftlicher
Apparate gelegentlich des Naturforschertages in Berlin. Ein Versuch des Obersten Goulier
zur Konstruktion eines verschieblichen Nivellierfernrohrs, den ich auf der Pariser Weltaus-
stellung 1839 vorfand, galt mir als ein Beweis für die Richtigkeit des Grundgedankens, und
veranlasste mich, von dem Landwirtschaftministerium Mittel zur Ausführung meines geplanten
Schiebfernrohrs zu erbitten, dass dann von F. Reinecke (in Firma A. Meissner in Berlin)
gebaut ward.

|

 
