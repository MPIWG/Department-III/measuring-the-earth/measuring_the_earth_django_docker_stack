 

2 ee Teen SEELEN

um

ma)

ILL AR

IjElb)

  

 

ji

ER

 

 

 

 

 

 

 

— 13 — nr
VII. — Italie,
| : | | |
INDICATION | 2 i
NO | LATITUDE 'LONGITUDE| EPOQUE DIRECTEURS OBSERVATEURS | INSTRUMENTS REMARQUES
DES POINTS | | | |
|
XI.
RESEAU DE BARI. \
83 | Mostarico ........ Deja mentionne.
88 | Stormara......... 3925953 | 320 19,231 1872 Col. Chi. Cap. Maggia. |Starke di 27 cm.
94 | Nocara.......... Deja mentionne. |
105 | T.re Scanzano .... | 40 15 22 | 34 24 39 1872 id. (6 id. id.
een > u ap. Barbieri. ;
113 | Pistieel .... ....2.. 40 23 21 342 12 34 1871 id. | Lieut. Mottura. \ id. |
4 La Serra... ... Deja mentionne, \
115 | T.”° Mattoni...... 40 24 09 | 34 31 5l 1875 Col. Ferrero. Cap. Magsia. Bes di 27 em.
121 | Taranto. 2... ... Deja mentionne.
130 | Colabarile........ 2023.32 15340891 1871 » Chio. ' » Colucei. | Starke di 27 em.
Ing. D’Atri.
136 | M. Cambio ....... A037 32 | 34 31 25. 1871°72 id. ca Colueci. id.
Ing. Garbolino.
141 | Traseoni ......... Deja mentionne. |
142 Orsetti en 40 40 32 | 3445 13 | ısmı id. » Garbolino. id.
8 » Peloso........ a8 5 a |
149 Shecchin se Deja mentionnes. |
150 | Jazzotello......... 40 46 31 | 3425.19.) 1971-72 id. en
154 | M. Serio...:..... 40 48 59 | 34 48 54 | 1871-72 id. id. Repsold di 27 em. |
159. Berrinl......... 40 49 41 | 34 59 37 | 1872 id. id. id“
160 | Chiancaro....... . A054 7,3116 5, 1874 Col. Ferrero. id. id.
164 | Casamassima...... 4097 11 | 345505 | 132 » Ehlo. id. id.
165 | Serraficaia ....... D6ja mentionne. |
166 | Conversano....... 40 58 03 | 34 46 48 I 1874 » Ferrero. id. id. |
174: Ruwo... :.. =... D6ja mentionne. N
176 | Bari (Faro) ...... 41 08 17 | 34 3035 | 1874 » Chio. id. id. )
183 KBranı. Deja& mentionne.
XIr.
REGION A L’EST DE LA MERIDIENNE DE MILAN. TRIANGULATION DU VENETO.
Reseau principal, developpement de la base de Udine et liaison avec le r&eseau principal.
292 | Copparo.......... 44°53' 40" | 29°29' 39"|| 1883 Col. De Stefanis. | Ing. Derchi. Starke di 27 em. |
294 | Riearolo.. >. ..... 44 57 13 | 29.05 58 1883 id. » Gineyri. id. |
296. Donada........... 45.02 13 2952 44 1883 id. Top. Gra. id. |
297 , S. Benedetto Po.. | 45 02 30 | 28 35 33 1883 id. Ing. Domeniconi. id.
306 | M. Venda ........ 45 18 47-| 29 21 10 1883 id. Top. Gra. id. |
311 | Solferine ......... 45 22 17 | 28 13 39 | 1882  \Col, Ferrero. |Ing. Derchi. id.
313 k Venezia.......... 45 26 01 | 30 00 10 1882 =. Id. » Domeniconi. id.
313b! Padova (Osservat.) | 45 24 05 | 29 31 56 1882 id. » Derchi. id.
317 | Calvarina......... 45 30 32 | 28 56.41 | 1882 id. id. id.
323. @aorle......... 415.35 55 3033 | 188% id. Ing. Cloza. id. |
250, M. Baldo........ 45 42 20 | 28 29 39 | 1882 id. » Ginevri. id. |
sslh) Aquilleja.......... 45 46 10 | 31 02 04 | _ 1882 id. » DAAU. id. |
332 , 0der20 =... ...... 45 46 55 ı 30 0925 | 1882 id. » Domeniconi. id. |
334 | M. Pasubio....... 45 47 31 | 28 50 25 | 1882 id. » Cloza. id. |
341 | M. della Grappa.. | 45 5223 | 2927 46 | 1882 id. » Domeniconi. id. |
343 | S. Vito al Taglia- | } |
mento...... 45 5453 | 30 31 15 || 1882 id. » Cloza. id. |
B43b| VIECO.. 45 56 19 | 30 44 00 1874 Ten. Col. Chiö. | Cap. Maggia. id.
345 | Base di Udine.... | 45 58 47 | 30 41 42 1882 Col. Ferrero. Ma)j. Maggia. id. |
Est.m° S.-0. |
346 , Base di Udine.... | 45 59 38 | 30 43 55 1882 id. id. id.
Est.” N.-E. | |
346h| Pantianicco....... 1501 171,045, 184 Ten. Col. Chiö. | Cap. Maegia. id.
3418 Ydine .s..2:...5% 46 03 51 | 30 54 03 | 1874-83 | Col. Ferrero. Ing. Cloza. id. |

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
