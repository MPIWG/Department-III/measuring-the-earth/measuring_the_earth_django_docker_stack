terra
Yin.

 

La Anl

TIP pen IT

ü

   

Vsabı

is

x

B
m
u.
FE
ib
’B

PREMIERE SEANGE

Vendredi, 27 septembre 1895.

La seance est ouverte ä 2 heures et demie.
Presidence de M. Faye.
Sont presents les membres de la Commission permanente :

1° M. H. Faye, membre de Institut, President du Bureau des longitudes, & Paris,
President de la Commission permanente.

9° M. le professeur Ad. Hirsch, Direceteur de ’Öbservatoire de Neuchätel, Secretaire
perpetuel de l’Association geodösique internationale.

30 M. le professeur Dr F.-R. Helmert, Directeur de Institut royal geodesique prus-
sien et du Bureau central de l’Association göodesique internationale, ä Potsdam.

4° M. le D' W. Feerster, professeur & l’Universite, Directeur de l’Observatoire, A Berlin.

5° M. le gensral Hennequin, Directeur de I’Institut cartographique militaire, &
Bruxelles.

6° M. le Dr H.-S. van de Sande Bakhuyzen, membre de l’Academie royale des
sciences, professeur d’astronomie et Directeur de ’Observatoire, ä Leyde.

7° M. le contre-amiral von Kalmär, chef du Bureau hydrographique, & Pola.

M. le President demande ä M. le Secrötaire des renseignements sur les membres
absents. ;

MN. le Secretaire r&pond que M. le general Ferrero, qui, malgre ses hautes fonctions
diplomatiques A Londres, avait espere pouvoir assister & la Conference, en est empeche ä
notre regret par l’6tat de sa sants, qui l’oblige & faire dans ce moment une cure ä Karlsbad.
M. le göneral Stebnitski a annonee le 9 aoüt qu'il est cette fois encore empeche de prendre
part aux sessions de la Commission permanente et de la Conference. En meme temps, il a
envoy& le rapport sur les travaux exöcutes en Russie pendant P’annde derniere.

M. Foerster demande ä donner quelques explications sur le renouvellement de la
Commission permanente ä faire par la Conförence generale. Bien qu’il soit possible que

 
