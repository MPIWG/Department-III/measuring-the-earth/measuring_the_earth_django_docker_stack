en

UNNA

Fe Wir" LIRTERLARR, Lam mL ALL

 

. 157

Vor —_ Italie,

 

 

56
58
59

61
64
65
68
82
89
91

96
Km

122
125
129
131

152
133

134

138
144

145
147

161
167
172

178
179

180

 

INDICATION
DES POINTS

M. Bonifato ......
S. Croce di 8. Ste-
fano *

nee ee

Bolius

Montalfano
M. Cuecie........
Palermo (Specola).

en ee

 

 

 

 

 

 

 

 

 

 

 

 

LATITUDE a EPOQUE DIRECTEURS OBSERVATEURS INSTRUMENTS REMARQUES

Denen

Salon 112, 302.37. 4227| 1895 Ing. Derchi. Ing. Gineyri. Starke di 27 cm.

37 58 30 | 32 02 48 1895 dh id. id.

31 58 45 | 29 42 57 1877 Col. Ferrero. Ing. Garbolino. | Repsold di 27 em.

37 59 28 | 31 48 29 1895 Ing. Derchi. » Loperfido. | Starke di 27 cm.

33.02 07 30 15 12 1895 id. » Ginevri. id.

38.06 17,3] 1130 1895 id. >» Loperfido. id.

38 06 49 | 30 55 47 1895 id. Top. Taechini. id.

38 06 36 | 31 01 03 1850 Ing. Schiavoni. | Reichenbach di 32 cm.
xVvIl.

RESEAU DE LA SARDAIGNE ET DEVELOPPEMENT DE LA BASE DE OZIERI.

Guardia dei Mori.
S. Pancrazio

T.e di M. Ferru..
M. Serpeddi
M. Linas

eo. ne

S.Giovannidi Sinis
Gennarzentu
M. Santo di Baunei
M. Urtieu
M.Rasu di Bono..
P.t* Catirina
Capo Comin®e.....
Ittireddu........ °
M. Santo di Tor-

Monserrato.......
Base di Ozieri....
Bst N BR.
Base di Ozieri....
Bst.a2’S.-0.
Bisareio.
M. Korte... 2...

N.® $S.* di Bonaria.

S. Leonardo
M. Limbara

—oreoe..

Isola Tavolara....
Sa Curi

P.t* della Scomu-

nica
P.* Tejalone.....
GUARDIA VEC-

oe 000. Lee

eo o ern 0e

T.”° di Capo Testa.

 

38051! 38"
39 02 49

39
39

39
39
39

4l
41
41
4]

09 44
13. 19

18 12
21 59
26 52

82 22
00 23
03 14
08 36
25 16
28 49
31 43
32 50

34 40
39 Ol
36 26
36 383

38 50
43 14

43 45

44 06
51 08

54 43
8 19

05 52
12 49

13 21
14 11

 

26° 04 22"
26.29 42
25 86 40

26 44 49

2 19 59
26 87 36
26 16 5l

26 06 08
26 58 56
26 45 23
26 16 15
26 40 04
zu) 45
21.29 02
26 34 04

26 26 44
26.39.12
26 35 26

26 33 01

 

20,34 23
25 55 21

26 20 53

26 36 14
26 50 23

21 28 19
27 12 14

25 57 28
21 08 13

27 03 46
26 48 29

 

 

182

1881
1882

1880

1881
1881
1880

1881
1882
1882
1881
1879
1881
1882
1879

1881
1879
1879
1879

1879
1878

1879

1879
1879

1882
1882

1885
1883

1883
1879

Col. Ferrero.
id.
id.
id.
id.
id.
Cap. de Vaiss.
Magnashi.
Col. Ferrero.
id.

s
er
=

id.
Cap. de Vaiss.
Magnashi.
id.

Col. Ferrero.
Cap. de Vaiss.
Magnaghi.
Col. Ferrero.

id.
Col. De Stefanis.

» Ferrero.

id.
id.

 

 

Cap. Simi.
id.
id.
id,

Lieut. Oro.

Ing. Domeniconi.

X
Lieut. de Vaiss.
Troiano.
Cap. Simi.
id.
id.
Cap. Pavese.
> Sımık
Lieut. Oro.
Cap. Simi.
id.

Cap. Pavese.
Lieut. Pavese.
Cap. Simi.

id.

Lieut. Pavese.
» de Vaiss.
Aubry.
Lieut. de Vaiss.
Mirabello.
Cap. Simi.
Lieut. de Vaiss.
Mirabello.
Lieut. Zoppi.
id.

Cap. Simi.

»  Pavese.
Maj. Simi.
id.
Lieut. de Vaiss.
Lasagna.

» Aus points marques avec * on a repete les observations eoncernant le reseau occidental.

Starke di 27 em.

} Pistor di 28 em.

Starke di 27 em.
id.
Pistor et Martins.

Starke di 27 cm.
Repsold di 27 cm.
id.
Starke di 27 cm.
Repsold di 27 cm.
id.
id.
id.

Starke di 27 em.
id.
Repsold di 27 em.
id.
Starke di 27 em.
Pistor et Martins.
.di27 cm.
Repsold di 27 cm.

di 27 em.
| Starke di 27 cm.

id.

 

id
Repsold di 27 em.
id.

di 27 em,

 

Repsold di 27 cm.
Repsold di 27 cm.

Troughton and Simms

Troughton and Simms

 

Troughton and Simms

 
