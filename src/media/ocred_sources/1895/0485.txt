hau Pr erregen RETTET! 'r

an an

ayeminır

iB
7
:E
iE
22
ir
3
22
z+
.>
13
:E
SE

Wa

XV. Messungen durch italienische Beobachter.

Die italienische Gradmessungskommission besitzt für relative Messungen einen
Schneiderschen Apparat, System v. Sterneck, sowie einen Apparat des Systems Defforges.
Mit dem ersten Apparat wurde 1892 Padua an Wien, Türkenschanze, g = 9,80866, ange-
schlossen und für g derselbe Werth wie 1891 (siehe Tab. I) mit einem Apparat des militär-
geographischen Instituts in Wien erhalten. Der Anschluss ist zwar 1892 nur ein einseitiger,
indessen zeigten sich 1893 die Pendel kaum verändert (wie in der Regel bei den älteren
dergl. Apparaten).

Im Jahre 1893 wurden mit demselben Apparat noch das Observatorium Brera bei
Mailand, sowie Rom, kgl. Ingenieurschule, Platz von Pisati e Pucei, an Padua angeschlossen ;
ferner ist 1892 Paris, Observatoire, Pfeiler von Defforges, mit dem Apparat von Defforges an-
geschlossen worden. In allen diesen Fällen handelt es sich auch um einseitige Anschlüsse,
doch dürfte das wenig auf sich haben.

In die Tabelle sind die Werthe von g für Mailand, Rom und Paris nach diesen rela-
Iiven Messungen, ausgehend von Padua, g = 9,80671, eingetragen, wobei die Angaben des
Memoirs von Herrn Prof. Lorenzoni in den Verhandlungen zu Innsbruck, 1894, p. 219-221,
sowie die beiden daselbst genannten Abhandlungen zu Grunde gelegt wurden. Alle diese re-
lativen Ergebnisse sind zweifellos sehr genau, doch dürfte die Beachtung des Mitschwingens
die Sicherheit noch erhöhen.

Auch die Ergebnisse der absoluten Bestimmungen mit einem Besselschen Faden-
pendel in Rom, sowie mit einem Reversionspendel in Padua sind in die Tabelle mit auf-
genommen. Vergl. hierzu die Abhandlungen : Vincenzo Reina, Sulla lunghezza del pendolo
semplice a secondi in Roma, esperienze eseguile dai prof. G. Pisali ed E. Pucci, Roma, 1894.
Giuseppe Lorenzoni, Relazione sulle esperienze istituite nel R. Osservatorio astr. di Padova per
determinare la lunghezza del pendolo semplice a secondi, Roma, 1888.

Der auffallend kleine Werth, welchen Herr Professor Lorenzoni in Padua durch seine
absolute Bestimmung mit dem Repsoldschen Reversionspendel-Apparat, Pendellänge 1 m,
erhielt, veranlasste ihn den Einfluss des Gleitens genau zu studiren. Vergl. hierüber die Ab-
handlung : G. Lorenzoni, Nuovo esame delle condizioni del supporto nelle esperienze falle a
Padova nel 1885-86, etc., Venezia, 1893; oder Verhandlungen in Innsbruck, p. 222.

Das Ergebnis war, dass dieser Einfluss als ganz minimal festgestellt wurde.

 
