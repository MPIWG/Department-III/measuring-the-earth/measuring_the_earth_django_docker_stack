terra

rn wen.

Ne RT RTETTERENTTERET "TEN OT!

Een

LEER
Tapamın

ibn

Nikkor di naachallaiid Inbumihehr-eii
Ka UI BEL Bu I DIE 7, BI II TEE N DIET

|
|

213

übertragungen annehmen. » Diese Klausel findet ihren besten Platz am Ende des vierten
Absatzes des Artikels 5. Man wird sie daselbst schon im gedruckten Texte finden.
Die Commission ist damit einverstanden.

Herr Bassot hält es für nöthig, den zweiten Absatz des Artikels 45 zu discutiren,
welcher die Dauer der Convention auf 20 Jahre festsetzt. Er wünscht die Gründe zu kennen,
welche die Spezialeommission bei dieser Verlängerung von 10 auf 20 Jahre geleitet haben.

Der Herr Präsident eröffnet die Verhandlung über diesen Gegenstand und giebt
Herrn Bakhuyzen das Wort, welcher erklärt, der 10jährigen Dauer der gegenwärtigen Con-
vention den Vorzug zu geben. Die Regierungen wünschen wahrscheinlich nicht, sich für
einen so langen Zeitraum zu binden, und es wäre ausserdem wünschenswerth, die Conven-

tion je nach den Erfahrungen, welche man machen wird, abändern zu können, ohne 20
Jahre warten zu müssen.

Herr Forster antwortet, dass die Möglichkeit einer Revision nach dem Artikel 15
zu jeder Zeit vorhanden ist; denn letzterer besagt, dass sie so lange in Kraft bleibt, bis sie
durch eine neue Verständigung der Staaten abgeändert wird. Nur unter dieser Bedingung
hat man eine Dauer von 20 Jahren in Vorschlag bringen zu sollen geglaubt, um der inter-
nationalen Erdmessung mehr Beständigkeit zu verleihen, wie es wünschenswerth oder gar
nothwendig erscheint, sobald die Aufgabe der Erdmessung wächst und dieselbe Beobachtungen
und Experimente von längerer Dauer unternehmen soll.

Herr Hirsch wird für die vom Entwurfe empfohlene 20jährige Dauer stimmen ; aber
er erklärt, dass er persönlich eine Convention von unbestimmter Dauer vorgezogen hätte,
und mit Rücksicht auf die Freiheit der Regierungen vorgeschlagen hätte, festzustellen, dass
ein Staat berechtigt ist, sich von der Uebereinkunft nach vorheriger einjähriger Aufkün-
digung zurückzuziehen.

Herr Lallemand glaubt, dass man eher im wirklichen Werthe der von der Erd-
messung ausgeführten Arbeiten als in einem die Regierungen bindenden Artikel der Statuten
die Sicherheit für den Fortbestand der Erdmessung suchen müsse.

Herr Feerster ist der Ansicht, dass die Aufgaben, welchen sich die Erdmessung
widmet, nicht nur vom wissenschaftlichen Standpunkte so zu sagen ewige sind, sondern dass
auch die praktischen Vortheile, welche aus ihrer Thätigkeit folgen, sich nicht auf einen Zeit-
raum von 10 Jahren begrenzen lassen. Die Erfahrung hat ja dargethan, dass jede neue Ent-
deckung, jede Entwickelung und Vervollkommnung der wissenschaftlichen Thätigkeit wenig-
stens indirekt in hohem Grade fördernd auf den national-ökonomischen Fortschritt und auf
das intellektuelle und moralische Wohl der Menschheit einwirkt; es unterliegt also keinem.
Zweifel, dass sich die Regierungen immer mehr daran gewöhnen werden, in den internatio-

 

 
