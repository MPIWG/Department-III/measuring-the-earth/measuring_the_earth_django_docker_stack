 

i

h
I"

 

146

parmi les 28 Etats de l’Association, iln’yen a que 17 qui soient representes. Une pareille
organisation a done dü paraitre a la Commission permanente comme peu pralique, voire
meme impossible. Si l’on voulait supprimer la Commission permanente et ne conserver que
la Conference generale, il serait alors indispensable de er&er une sorte de Conseil adminis-
tratif' de 3ou 4 membres. Les raisons qui s’y opposent ont dejä et& developp6es et M. Hirsch
ne peut donc que recommander l’adoption du systeme intermediaire entre les deux exir&mes,
contenu dans l’avant-projet. Si ce projet de la Commission permanente est, comme il le desire,
adopt6 par la majorite des delegations, on peut esp6rer que MM. les delegues frangais tien-
dront compte de ce fait, de sorte qu’il n’y aurait pas ä craindre que ’un des pays qui onl

iouß un röle si important dans la e&odesie voulüt maintenant se relirer de l’Association inter-
p 8 :

nationale.

Enfin, M. Hirsch ne peut s’empecher de faire la remarque qu’une de&elaration oflicielle
au nom d’un gouvernement, comme celle qui se trouve dans le texte communique par M.
Bouquet de laGrye, entraine ä des consöquences assez graves, ä mesure qu’elle lie le gouver-
nement en question d’une maniere qu’il pourrait regretter plus tard, et qu’en indiquant dans
cette declaration une condition sine qua non de ’adhesion de ce gouvernement, elle pourrait
eire consideree comme une pression exerc6e sur les autres Etats egalement souverains.

Une Conförence internationale comme celle-ci, dans laquelle un certain nombre
d’Etats indöpendants sont appel6s A &laborer le projet d’une convenlion, ne peut en cffei ac-
complir sa mission que si les instructions des differentes dölögations ne reyelent pas un
caraclere absolu, car dans ce cas une entente r&ciproque serait difficile, sinon impossible.
Des mandals imperatifs, aussi bien que des declarations offieielles liant les gouvernements,
sont done dangereux dans de pareilles assembl&es internationales et M. Hirsch eroit pouvoir
ajouter, d’apres son experience, qu’ils sont contraires a usage.

M. le President voudrait d’une maniere encore plus explieite que M. Hirsch pre-
senter une motion d’ordre, en invitant MM. les delögues & ne pas faire intervenir Jeurs gou-
vernements dans la discussion, car il ne s’agit ici d’abord que de trouver la meilleure voie el
les lormules les plus rationnelles pour une entenle reeiproque. Une pareille entente n’exelut
pas, dans certains cas, des concessions, mais si d’un cöle quelconque on vient declarer qu’on
est li& par un mandat imperalif, on renonce necessairement A toute discussion. I compren-
drait au besoin qu’un groupe de delegu6s füt influence tacitement par un semblable mandat
de son gouvernement dans la maniere de lraiter une certaine question, mais en lout cas
toute delögation devrait älre en mesure, lorsqu’elle reconnail qu’'une autre solulion a plus de
chance de r&ussir, de recourir A son gouvernement pour le convaincere que leurs instruclions
reposaient quant ä ce point sur des supposilions qui ne correspondent pas A la realıte.

Apres avoir traduit en frangais la motion d’ordre de M. Feerster, M. Hirsch ajoute
que, dans son opinion, le telegraphe offre un moyen excellent d’arriver ä une entente
dans des associations internationales. Il arrive souvent, dit-il, que dans des conferences
diplomatiques, le reprösentant d’un Etat, qui trouve la maniere de voir de son gouverne-

 

na hd le

FONERO REN

halbe hashu

asus bb a bh ha a

’
|
|

 
