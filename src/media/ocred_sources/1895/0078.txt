 

22

Herr Weiss verliest folgende Erklärung, wonach die österreichischen Abgeordneten
dem Art. 7 in der Fassung des Amendemenis Bakhuyzen zustimmen :

Nach der Beantwortung meiner Anfrage über die Bedeutung der Denkschrift des
Herrn Foerster erlaube ich mir die nachstehende, von der österreichischen Gradmessungs-
Commission einstimmig angenommene Erklärung abzugeben :

Die österreichischen Commissionsmitglieder verkennen keineswegs, dass zur Organi-
sirung solcher Arbeiten, die von den einzelnen betheiligten Staaten nicht allein ausgeführt
werden können, sondern ein internationales Zusammenwirken bedingen, eine Erhöhung der
Dotation erforderlich sein werde. Dieselben werden auch die Einrichtung und gedeihliche
Fortführung derartiger Arbeiten stets mit Freuden begrüssen, und bei ihrer Regierung die
Bewilligung der hierzu nöthigen Geldmittel befürworten, sobald für diese Arbeiten ein defi-
nitiv ausgearbeiteles, von der Generalconferenz angenommenes Programm vorliegt.

Abgesehen davon, dass die Regelung des -Polhöhendienstes wohl eine mehr der
Astronomie als der internationalen Erdmessung zufallende Aufeabe ist, und auch abgesehen
davon, dass der volle Mehrbetrag der Erhöhung der Dotation nur dieser Aufgabe zugewendet
werden soll, für die anderen im Avant-Projet in Art. 6, Absatz 4 angegebenen Arbeiten eine
weitere dauernde oder vorübergehende Erhöhung des « Mindestbeitrages » von 60.000 Mk.
(Art. 7, Abs. 3) in Aussicht gestellt wird, kann beim Fehlen eines definitiven Programmes
weder über die Kosten noch auch den Zeitpunkt des Beginnes der Arbeiten etwas Bestimmtes
festgestellt werden. Die österreichische Commission ist daher derzeit nicht in der Lage, für
die Erhöhung der Dotation in der Form zu stimmen, in welcher sie im Avant-Projet gestellt
wird, hingegen könnte sie dies Ihun in der Form des Amendements Bakhuyzen zum Art. 7
des Gontreprojekts. ;

Herr Feerster wünscht ein Missverständniss in Bezug auf die Ausführungen des Herrn
Helmert, welches bei der Uebersetzung ins Französische durch den Herrn Seeretär zu Tage
getreten ist, aufgeklärt zu sehen, und ersucht zu dem Ende Herrn Helmert um eine kurze
Wiederholung seiner Worte.

Herr Helmert entspricht diesem Wunsche und betont zusälzlich, dass bei grossen
gemeinsamen Arbeiten nur ein Einziger die Verantwortlichkeit übernehmen kann, weil sonst
eine gedeihliche Entwicklung unmöglich sei. Diese letzte Instanz und der Träger der Verant-
wortlichkeit müsse der Direktor des Centralbureaus sein. Wenn das Amendement den Sinn
habe, dass der Direktor des Centralbureaus nicht gezwungen sein soll, gegen seinen Willen
etwas auszuführen, so würde sich dass vielleicht durch eine andere Fassung deutlicher her-
vorheben lassen.

Herr Hirsch hält sich keineswegs für unfehlbar, doch ist er sicher, die Aeusserungen
des Herrn Helmert, soweit er dieselben hat verstehen können, genau in französischer Sprache
wiedergegeben zu haben. Um jeder Unklarheit vorzubeugen, ersucht er Herrn Helmert, ihm
seine Aeusserung schriftlich zu überreichen, damit er dieselbe wörtlich übersetzen könne.

ehe

IFF we}

na UI a

aus, anhenadhh ah  he hheL n a nn

 

 
