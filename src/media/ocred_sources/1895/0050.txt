 

 

 

1

Genauigkeit für ein Sternpaar mit den entsprechenden Ergebnissen der Okularbeobachtungen
noch als vorläufig zu betrachten. Einige ausgezeichnete Reihen der letzteren haben in der
That Pointirungs-Genauigkeiten erkennen lassen, die den vorliegenden Ergebnissen aus den
photographischen Aufnahmen mindestens gleich zu stellen sind. Indessen sind das besonders
hervorragende Leistungen ; bei der Entscheidung zwischen der Güte verschiedener Methoden
kann immer nur in Betracht kommen, was ein mittlerer Beobachter unter mittleren Ver-
hältnissen zu leisten vermag oder geleistet hat. Wenn im Allgemeinen auf einer Sternwarte
derartige Instrumente immer etwas grössere Genauigkeit ergeben, als auf einer astronomi-
schen Station, so wird das hier nicht gelten können, denn die Umstände auf der Berliner
Sternwarte sind in jeder Beziehung ungünstig und werden wohl schliesslich zu einer Ver-
legung derselben zwingen. Die in der von Herrn Foerster verfassten und vertheilten Denk-
schrift ins Auge gefassten 4 Stationen würden im subtropischen Klima liegen und Jedenfalls
so ausgewählt werden können, dass in dieser Beziehung günstigere Bedingungen für die
Sicherheit der Ergebnisse zu erreichen sind. i

« Eine andere Untersuchung theils experimenteller, theils rechnerischer Art, die von
der hiesigen Sternwarte ausgeführt wurde, dürfte auch in etwas näherer Beziehung zur Erd-
messung stehn. In der Eröffnungsrede habe er bereits erwähnt, dass durch eine Verbindung
der Horrebow Talcott’schen Methode mit Beobachtungen im ersten Vertikal und in verschie-
denen Azimuthen, wozu ein Universal-Durchgangs-Instrument dienen kann, eine Kenntniss
von Declinationen der Sterne vielleicht zu erlangen ist, die der Messung an getheilten Kreisen
sich an die Seite zu stellen oder unabhängige Gontrollen für letztere zu bieten vermag. Ins-
besondere kommt hierbei in Betracht, dass nicht die absoluten Refractionen in die Resultate
eingehen, sondern nur die Refractionsdifferenzen, wie dies bei der Horrebow-Methode der
Fall ist. Namentlich stellt sich bei diesem Verfahren das Verhältniss zwischen der Bestimmung
von Zenith- und Aequatorealsternen sehr günstig, sobald die Zeitbestimmungen und die Uhren
die genügende Genauigkeit besitzen.

« Eine dritte Angelegenheit scheint der Erdmessung zunächst etwas fern zu liegen.
Bei Gelegenheit der Polhöhenbeobachtungen hat man sich auch mit der Aberration beschäf-
tigt, und die bekannten Gruppenabschlüsse haben ziemlich unabhängige Bestimmungen nach
der Küstner’schen Methode geliefert. Er halte es aber für eine mehr formale Auswertung der
Beobachtungen, wenn auch Beobachtungsreihen, die in Breiten um 20 oder 30° herum aus-
geführt wurden, zu diesem Zwecke Verwendung gefunden haben, weil für solche sin. o einen
zu kleinen Werth hat. Deshalb werden also auch die geplanten vier Stationen unter 37°
Breite keinen entscheidenden Beitrag zu dieser Frage liefern. Vielleicht kann man aber daran
denken, in höheren Breiten in der angedeuteten Richtung thätig zu sein, ohne dass es nöthig
wäre, so ununterbrochene Beobachtungsreihen zu unterhalten wie sie für die Bestimmungen
der Lage der Pole nöthig sind und zur Wahl von subtropischen Stationen führen. Die Aber-
rationsfrage steht in Verbindung mit Verfeinerungen der Ausnutzung der Lichtbewegung,
die wir der neuesten Zeit, namentlich den Arbeiten des amerikanischen Physikers Michelson
verdanken. Man erinnert sich, dass wir durch Michelsons Methoden, wie die Messungen in
Breteuil bewiesen haben, die Vergleichung der Wellenlänge mit dem Meter bis au

EA EL Eh

rn

dd

4
=

 
