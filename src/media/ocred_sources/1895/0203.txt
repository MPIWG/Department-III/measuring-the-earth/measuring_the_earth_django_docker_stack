Vene

Hits adase

En

AM AIR a0

ii li | Anl

ETRETTETREITERURG TITARINEIT AR LTFRNIT FRI N

|
|
|
|

197

verbundenen Länder besteht, wäre weder eine Commission, noch permanent, denn man
könnte sie offenbar nur alle zwei oder gar drei Jahre zusammenberufen.

Ilerr Hirsch glaubt übrigens die erste Generalverhandlung beendigt; er möchte,
dass man heute noch dazu käme, die Ansicht der Mehrheit der permanenten Commission
über den ersten der drei Hauptpunkte für die Erneuerung der Convention festzustellen, d.h.
zu erfahren, ob die Mehrheit nach dem Plane des vorläufigen Entwurfs ein internationales
Comite von 7 Mitgliedern annimmt oder ob sie ein Verwallungscomite von 4 Mitgliedern
wünscht, oder endlich ob sie sich für eine Commission von 98 Mitgliedern entschliesst.

Herr Faye zöge vor, mit der Abstimmung zu warten bis die Verhandlung über alle
Artikel des Entwurfs völlig erschöpft sei.

Nach einer kurzen Unterbrechung der Sitzung möchte Herr Feerster bevor man zur
Abstimmung schreitet, noch eine kleine Aenderung zum ersten Paragraphen des fünften Ar-
tikels vorschlagen ; er wünscht den Ausdruck «internationales Comite » durch den früheren,
in der bisherigen Geschichte der Erdmessung gebräuchlichen Ausdruck « Permanente Com-
mission » zu ersetzen.

Herr Hirsch pflichtet dieser Aenderung ebenfalls bei und Herr Faye sieht darin einen
glücklichen Einfall, der zur Verständigung beitragen könnte.

Herr Hirsch wünscht noch vor der Abstimmung daran zu erinnern, dass die per-
manente Commission nichts endgültig beschliessen kann; sie hat nur einen Entwurf für die
Generalconferenz auszuarbeiten.

Der Herr Präsident erklärt die Verhandlung über den ersten Punkt geschlossen und
lässt abstimmen. Aus der Abstimmung geht hervor, dass der erste Paragraph des Artikels 5
des abgeänderten Entwurfs durch vier Stimmen (die Herren Feerster, Hennequin, Helmert
und Hirsch) gegen drei (die Herren Bakhuyzen, Faye und von Kalmär) angenommen wird.
Er lautet : Die permanente Commission besteht aus fünf wählbaren Mitgliedern, welchen der

ständige Secrelär und der Director des Centralbureaus mit beschliessender Stimme zugesellt
werden.

Die Sitzung wird um 4 !/, Uhr aufgehoben.

 
