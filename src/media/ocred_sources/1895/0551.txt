aan

Hirn
vetmmmen.

eh ergo 0 eh hen 119131 27 1
ET TR RTTRAETTIRERETA "STE TRUTTTEN

Ha EN
NOFRRARSTTERT LEN

iin
nuımn

it
Fan

Adi band la dhhäheh a
TE TE TR

 

 

Annexe B. VII.

ITALIE

RAPPORT
sur les travaux executes par Ja Commission g6odesique italienne en 1895.

(Avec 3 cartes. — Voir nos IX-X1.)

TRAVAUX DE L’INSTITUT GEOGRAPHIQUE MILITAIRE.

Triangulation (voir planche I). — Apres la derniere r&union de la Commission per-
manente (Innsbruck, 1894) on a ex&cut& les observations angulaires aux stalions suivantes
pour completer le reseau de premier ordre de la Sicile :

Monte San Giuliano, Marsala, Capo, Granitola, Salemio, Monte delle Rose, Bonifato,
Roccaficuzza, Busamhra, Monte Guceio, Monlalfano, San Galogero, CGammarata, Giafaglione,

Gastelluccio, Durrä, Pollina, — et on a r&p£ete les observalions, pour ce qui concerne le
reseau occidental de la Sicile, A Grecuzzo, Santa Croce, San Salvatore, Chiebbo et Monte Cane.
Nivellement de precision (voir planche II). — En 1895 on a nivel& a double :

A: La.ligene Foggia-Bari dep ur Snlpeınen 22 11884kmı
309153 259. >Napohr-Anlettades aa mann 9259
er lalliene Parma-Guastalla. dest een 2. 39.»

la derniere pour la substituer ä la ligne de Reggio-Guastalla, pour laquelle on a des raisons
de croire que le terrain ne pr&sente pas les meilleures conditions de stabilite.

Maregraphes (voir planche Il). — Les maregraphes en activite sont indiques sur la
planche II; on a poursuivi les d&epouillements des diagrammes du maregraphe de Genes jus-
qu’au mois d’avril 1894.

Travauz de caleul (voir planche I). — On a acheve les calculs de compensation des
reseaux XV et XVI, de sorte qu’a l’exception de celui de la Sicile oceidentale (XVIN), tous les
reseaux trigonometriques de !’Italie sont compenses.
