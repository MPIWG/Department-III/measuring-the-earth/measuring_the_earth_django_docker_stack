tw wohn mise 1:65}
Te TTTRETTEETTTTERT TORTE TINTEN

En

Hit
TrrarOT

HT]

DTFERTETIR

Mater nahe ie
DE ae TTFnRImEH: RT

 

     

—V—

Il est cependant & remarquer que, lorsque les travaux trigonometriques
doivent avoir une etendue considerable, l’on doit s’attendre ä.ce que l’erreur
moyenne soit environ de 1”, et ne descende pas beaucoup au-dessous de 0.9”.
Cela tient a ce que l’emploi d’un nombreux personnel ne permet pas de compter
sur des observateurs exceptionnellement habiles; et, en outre, dans un grand
pays les conditions de terrain ne sont pas toujours egalement favorables.

Les &tudes qu’ici nous ebauchons & peine devraient &tre poursuivies en
classant les observations d’apres toutes les eirconstances de lieu et de temps
dans lesquelles elles ont &t& faites.

Il ne faut du reste pas oublier que, malgre tout le soin employe pour
eliminer les causes d’erreurs constantes, il est impossible que les observations
n’en soient pas affectees a un certain degr£. |

Nous avons de cela une preuve dans le fait presque general que l’erreur
moyenne d’un angle, ou direction prise comme unitd de poids r&sultant de
la compensation d’un reseau, est plus grande que celle qui resulte de la
simple compensation des stations trigonometriques.

Quelques exemples a cet egard sont utiles; je les donne dans le Ta-
bleau N° I., que je dois & l’obligeance du Directeur de l’Institut Geodesique
de Potsdam.

Si les erreurs dont les observations sont affeetdes &taient exelusivement
accidentelles, la contradietion entre les erreurs moyennes, avant et apres la
compensation, ne devrait pas se presenter.

Ala verite, soit les erreurs qu’on appelle constantes, soit celles que l’on ap-
pelle accidentelles, peuvent etre affecetdes par des causes de nature periodique.

La plus grande partie des erreurs qui font osciller les observations autour
de leur valeur moyenne sont dues soit aux conditions physiologiques de
l’observateur, soit aux conditions variables de l’atmosphere.

Or, en laissant de cöte les influences purement aceidentelles, il est &vident
que les’ conditions de l’observateur doivent varier avec les heures du jour, en
raison des fonctions de l’organisme humain, qui sont essentiellement periodiques.

Il en est de m&me des influences inherentes a l’atmosphere, qui produisent
des röfractions irregulieres. Or, ces influences dependent des pressions, des

temperatures, de la direction des rayons solaires, en. general de causes en

grande partie soumises a des periodes plus ou moins compliquees.

 
