 

 

eine allseitige Verständigung höchst schwierig, ja unmöglich würde. Imperative Mandate so-
wohl wie offizielle bindende Erklärungen im Nainen der Regierungen seien daher bei solchen
internationalen Versammlungen gefährlich und, nach den Erfahrungen des Redners, auch
nicht gebräuchlich.

Der Herr Präsident möchte noch bestimmter als es Herr Hirsch gelhan, einen An-
trag zur Geschäftsordnung stellen, indem er die Herren Delegirten ersucht, ihre Regierungen
nicht in die Discussion hineinziehen zu wollen, weil es sich hier nur darum handle, den
besten und vernünftigsten Weg zu einer gegenseitigen Verständigung zu finden. Eine solche
Verständigung schliesse auch gegebenenfalls eine Nachgiebigkeit nicht aus. Wenn aber von
irgend welchen Seiten erklärt werde, dass die Stellungnahme auf einem imperativen Mandate
beruhe, so sei dies für die Discussion geradezu Lödtlich. Ein solches imperatives Mandat könne
stillschweigend die Behandlung einer bestimmten Angelegenheit durch eine Delegirtengruppe
beeinflussen, aber es müsse jede Delegation auch in der Lage sein, wenn sie hier erkenne,
dass eine andere Ansicht zutreffender ist, ihre Regierung zu überzeugen, dass das imperalive
Mandat auf nicht zutreffenden Voraussetzungen beruhe.

Nachdem Herr Hirsch diese Bemerkung zur Geschäftsordnung auf Französisch wie-
derholt hat, fügt er hinzu: Nach seiner Meinung biete der Telegraph ein ausgezeichnetes
Mittel der Verständigung gerade für solehe internationale Associationen. Es komme in ähn-
liehen diplomatischen Conferenzen häufig vor, dass der Vertreter eines Staates, der die An-
sicht seiner Regierung nicht in Uebereinstimmung mit derjenigen der grossen Mehrheit der
übrigen Länder findet, es für sein Recht und seine Pflicht erachte, seine Regierung: tele-
graphisch über die momentane Lage aufzuklären, so dass sie die Möglichkeit habe, sowohl
im eigenen Interesse wie im Interesse des gemeinsamen Werkes ihre Instructionen abzu-
ändern. Ein analoges Verhalten empfehle sich auch hier.

Herr Tinter verliest folgende Erklärung :

« Schon in der Zusammenstellung der beim Gentralbureau eingegangenen Vorschläge,
betreffend die Erneuerung der Erdmessungs-Öonvention, wird man finden, dass Oesterreich
in den Vorschlägen für die Erneuerung der Convention auch ein grosses Gewicht gelegt hat
auf die Organisalion der permanenten Commission. Es wird darin vorgeschlagen, dass die
ausscheidenden Mitglieder nicht sofort wieder wählbar sind, dass die in der Zwischenzeit von
einer allgemeinen Conferenz bis zur nächsten eintretenden Vacanzen im allgemeinen unbe-
setzt bleiben, dass die Commission ihren Präsidenten und Vizepräsidenten wählt, und dass
die permanente Commission sich mindestens alljährlich einmal in den Räumen des Kgl. geo-
dätischen Instituts zu Potsdam versammelt. Wie aus dem neuen Entwurf zu entnehmen, ist
in vieler Beziehung diesen Wünschen, bezw. diesen Erwägungen, die wir zum Ausdruck
gebracht haben, und die nach reiflichen Berathungen von der österreichischen Gradmessungs-
commission ohne jede Beeinflussung der Regierung einstimmig angenommen worden sind,
Rechnung getragen worden. Seither ist von Seiten des Spezialeomites ein Entwurf für die

Masilähsrsu

Ei

nah

screen EL a ann
