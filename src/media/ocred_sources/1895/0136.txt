 

 

130
M. Bassot donne lecture de son Rapport. (Voir Annexe A. II.)

M. Hirsch desire, A l’occasion du Rapport de M. Bassot, altirer l’attention sur le fait
röjouissant que le Bureau international de Breteuil, qui doit son origine A Vinitiative de
’Association göodösique, a fourni une nouvelle preuve de ses grands me£rites scientifiques el
de son importante activile, qui resulte des donn6es fournies par M. Bassot. M. Hirsch entend
relever le fait que les comparaisons des principaux 6talons employ6s en g6odesie, et speciale-
ment des regles de bases qui ont servi dans les divers pays, ont conduit & un accord parlait,
au delä de toute attente, des difförents reseaux de triangles. Get accord est non seulement
(res important pour la science, mais il fournit en m&me temps un motif puissant en faveur
du travail international sur le lerrain scientifique. Il espere que M. le Directeur du Bureau
central sera, dans quelques anndes, en mesure de relever encore avec plus de force les r&sul-
tats excellents et les grands avantages qui decoulent de cette nouvelle determination preeise
des &quations des regles et &lalons.

M. Henneguin fait remarquer au sujet du Rapport sur les bases que, quelques jours
avant son döpart de Bruxelles, la copie belge de la toise a &t& expedide ä Breteuil pour &tre
compar&e au Bureau international des Poids et Mesures avec le metre.

Apres avoir remerei6 M. Bassot, M. le President prie M. Guarducei de faire lecture
du Rapport sur les triangulations, redig& par M. le general Ferrero.

M. Guarducci lit au nom de M. Ferrero une introduction ä ce Rapport qui sera
imprims A Florence et envoy& au Secretaire pour la publication des Gomptes-Rendus. (Voir
Annexe A. Ill.)

M. le President pense &tre V’interprete de la Conference en remerciant vivement
M. le general Ferrero, malheureusement absent, pour le Rapport preeieux qu'il a redige avec
tant de soin, malgr& ses nombreuses occupations. ‘

Il prie ensuite M. von Kalmar de rapporter sur les progres des nivellements de pre-
eision dans les trois dernieres anndes. Ce Rapport, que M. von Kalmär distribue ä tous les
membres de l’Assemblee, sera accompaen& d’une carte qui n’est pas encore completemenl
terminde et que l’auteur promet de faire parvenir au Secrötaire avant la fin de l’annde. (Voir
Annexe A. IN.)

M. le President vemercie M. von Kalmär au nom de la Conference pour son Rapport.
Quant aux autres Rapports speciaux, il est d’avis de renvoyer celui sur les mesures de la
pesanteur et les döviations de la verticale A une seance ulterieure, afin que quelques savanls,
qui arriveront la semaine prochaine, et qui s’inleressent parliculierement a ces questions,
aient l’occasion d’en entendre la lecture.

Il invite ensuite M. Albrecht ä communiquer le Rapport sur les mesures de longi-
tudes, de latitudes et d’azimuts. (Voir Annexe A. V.)

 

an

sau main na Dh nn

|
|

 
