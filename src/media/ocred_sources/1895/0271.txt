ann

ee

ae een.

er reTEn

ln

m

RR TU PRBI URL INTER

=
7
+

Anhang II®.

ENTWURF

der permanenten Commission für die Erneuerung der internationalen
Erdmessungs-Convention.

Art. 4. — Das Centralbureau der internationalen Erdmessung mit seinen bisherigen
Attributionen bleibt mit dem Geodätischen Institut zu Berlin in solcher Weise verbunden,
dass der Director des Geodätischen Instituts zugleich Director des Gentralbureaus der inter-
nationalen Erdmessung ist, und dass die Kräfte und Mittel des Instituts auch den Zwecken
der letzteren dienen.

Art. 2. — Das oberste leitende Organ der Erdmessung ist die Generalconferenz der
Delegirten der betheiligten Regierungen, welche alle drei Jahre zusammentritt.

In der Zwischenzeit werden unter der periodischen Controlle der Generalconferenz
die Angelegenheiten der Erdmessung durch eine permanente Commission verwaltet, welche
von der Generalconferenz gewählt wird.

Der permanenten Commission liegt auch die Wahl des Zeitpunktes und des Ortes der
Generalconferenzen ob, sowie die Einladung der Delegirten der betheiligten Staaten zu den
Generalconferenzen, unter Angabe der Berathungsgegenstände.

Art. 3. — Der Director des Gentralbureaus ist als solcher ständiges Mitglied der
permanenten Commission, welcher er alljährlich einen Bericht über die Thätigkeit des Gentral-
bureaus zu erstatten und den Arbeitsplan der letzteren für das folgende Jahr zur Genehmigung
vorzulegen hat.

Art. 4. — Die Leitung der Veröffentlichungen der Erdmessung und die Führung der
Correspondenz der permanenten Commission mit den Regierungen und den Delegirten, sowie
die sonstige laufende Geschäftsführung der permanenten Commission unter Oberleitung ihres
Präsidenten und in Gemeinschaft mit dem Director des Gentralbureaus, bleibt dem ständigen
Secretär übertragen, welcher durch die Generalconferenz von 1886 gewählt ist. Bei einer
künftigen Neubesetzung dieses Amtes erfolgt die Wahl des Nachfolgers ebenfalls durch die
Generalconferenz. Durch einen von dem Präsidium der bezüglichen Generalconferenz zu
vollziehenden Vertrag werden die Anstellungsbedingungen seitens derselben festgesetzt.

Art. 5. — Die permanente Commission (Art. 2) besteht aus fünf Mitgliedern, zu

ASSOCIATION GEODESIQUE — 3%

 
