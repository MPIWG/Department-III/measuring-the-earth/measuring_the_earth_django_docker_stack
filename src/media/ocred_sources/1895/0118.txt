 

 

 

In.

 

Bureau central!; puis de soumettre ce projet, d’abord par eorrespondance, aux collögues qui
avaienl &t& empeches de se rendre A Neuchätel.

« Monsieur le General Ferrero nous a r&cemment envoy& son adhesion A ce projet,
en maintenant son droit de proposer plus tard encore quelques amendements, et en röservanl
l’approbalion de son Gouvernement. Monsieur von Kalmar vient &galement de nous annoncer
son adhesion, sauf quelques modifications qu’il se reserve de proposer & la Conference sur
certains points de detail.

«Dans ces conditions, le projet que nous avons aujourd’hui ’'honneur de prösenter
a Messieurs nos Collegues peut donc &tre envisags& comme rösultat des deliherations de la
Commission speciale.

« Nous prions en m@me temps Messieurs les Del&gues de bien vouloir communiquer
ce projet egalement aux Hauis Gouvernements de l’Association g&odesique internationale,
comme document ä consulter pour les instructions qu’ils voudront donner A leurs Delegues
a la Gonference gendrale de Berlin, au sujet du renouvellement de la Convention de 1886.

« Ge premier avant-projet pourra, nous l’esperons, servir utilement de base de dis-
eussion a la Commission permanente dans sa r&union & Berlin, fixce au 7 septembre.

« Berlin et Neuchätel, le 15 juin 1895.

« Pour la Commission speeiale :

« Dr W. FERSTER.
« D’ An. Hırscn. »

« Avant-projet de la Commission speciale pour le renouwvellement de la Convention geodesique

internationale.

«Art. 1. — Le Bureau central de l’Association geodesique internationale conserve
les attribulions qui lui ont &i& conförees lors de la fondation et reste attach& A V’Institut g6o-
desique de Berlin, en ce sens que le Directeur de V’Institut gdodösique est en me&me temps
Directeur du Bureau central de !’Association geodesique internationale, et que les ressources
et les moyens scientifiques de I’Institut sont mis ögalement au service de l’Associalion.

« Art. 2. — L’organe superieur de l’Association g&odösique est la Conference gene-
rale des delöguds des Gouvernements interessös. Cette Conförence se r&unit tous les trois ans.

« Dans l’intervalle des sessions, les affaires de l’Association internationale sont gerdes
par un Comite international nomme par la Conference, et sous le contröle de cette derniere.

«ll appartient au Comite international de fixer la date et le lieu des Conferences
gönerales, ainsi que d’y convoquer les delögu6s des Etats contractants, en indiquant l’ordre du
jour de la session.

«Art. 3. — Le Directeur du Bureau central fait de droit partie du Comit& international,
auquel il prösente chaque annde un rapport sur l’activil&ö da Bureau et & la ratification du-
quel il soumet le programme pour les travaux du Bureau central pendant l’annde suivante.

ı Voir Appendice Ia et In.

3
ä
1
i
3
Ei
ä
i

nah

FERIEN

tabs

 

 
