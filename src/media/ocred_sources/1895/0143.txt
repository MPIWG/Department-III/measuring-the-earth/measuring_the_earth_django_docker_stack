Varna

Hhrrasae
nme

ren ee

it
van

num

TTTT

bahn

se

&

=
se
:=
;=

®

E
<=
3
33
sr
se
3

137

 

excellentes faites avec cette derniere möthode font reconnaitre une preeision de pointage au
moins A peu pres egale & celle des mesures photographiques. Mais ce sont lä des rösultats
exceptionnels, et pour decider entre la valeur de differentes möthodes, on ne peut mettre en
ligne de compte que les rösultats obtenus par un observateur de force moyenne dans des
circonstances moyennes. $’il est vrai qu’en gäncral de pareils instruments donneront loujours
une pre&cision plus grande dans un observatoire permanent que dans une station astronomique,
on ne pourrait invoquer cet argument dans ce cas, puisque les conditions ä ’Observatoire de
Berlin sont, sous tous les rapporis, tellement defavorables, qu’elles necessiteront probable-
ment le transfert de cet, &tablissement. Les quatre stations dont M. Ferster parle dans le
memoire qu’il a distribu& seraient places dans la region subtropicale et choisies en tout cas
de facon ä offrir des conditions sup&rieures pour la securit& des rösultats.

Une autre recherche, basde sur des exp£eriences et des caleuls, pourrait interesser
aussi la geodesie. Dejä dans son discours d’ouverture, M. Foerster a indiqu& qu’une com-
binaison de la methode Horrebow-Taleott avec des observations au premier vertical ou dans
difförents azimuts, pour lesquelles on peut se servir d’un instrument universel, promet peut-
ötre d’obtenir des döclinaisons d’&toiles parfaitement comparables aux declinaisons fournies
par les cercles divises, et qu’elle donnera en tout cas des contröles independants pour ces
dernieres. Surtout il ne faut pas oublier qu’avec cette methode ce ne sont pas les refractions
absolues, mais seulement les differences de röfraetion qui influencent les resultats de la m&me
maniere que dans la methode Horrebow. Enfin, avec cette methode, le rapport entre les
determinalions d’6toiles zenithales et öquatoriales est Ir&s favorable, pourvu que les döter-
minations de l’'heure et les pendules soient d’une preeision suffisante.

Une troisitme question parait d’abord n’avoir qu’une relation &loign&e avec la geo-
desie. A l’occasion des observations de latitude, on s’est pr&oceupe aussi de l’aberration et
les combinaisons de groupes connues ont fourni des determinalions assez independantes,
d’apres la methode de Küstner. M. Feerster estime toutefois que ce serait une utilisation plutöt
apparente de ces observations, si on voulait employer, pour en deduire ’aberration, aussi
des series obtenues dans les latitudes comprises entre 20 et 30°, attendu que dans ce cas la
valeur de sın 9 est trop faible. Pour cette raison, les quatre stations qu’on se propose de
ereer sous la latitude de 37° ne pourront pas contribuer d’une maniere deeisive A fixer la
valeur de l’aberration. Cependant, on pourrait peut-etre songer ä rendre &egalement service
dans cette direction, par des observations sous des latitudes plus &levees, sans qu’il füt neces-

saire d’obtenir des series d’observations aussi longues et aussi continues qu’il est näcessaire

pour P&tude dela position du pöle, dans l’interet de laquelle on a dü donner la. pröfärence
a des stations situ6es dans la r&gion subtropicale. La question de l’aberration est en rapport
avec les progres qu’on a faits ces derniers temps dans la connaissance des mouvements
de la lumiere, surtout ensuite des travaux du physicien ame6ricain, M. Michelson. On se
souvient qu’on a pu realiser ä Breteuil, par les m&thodes de Michelson, la comparaison de
la longueur d’onde lumineuse avec la longueur du metre, ä m pres. Les determinations

e .r A r . 1
terrestres de la vitesse de la lumiere pourront peut-ätre posseder une exactitude de —,
environ.

 

ASSOCIATION GEODESIQUE — 18

 
