 

LE er een PRINT ST EERNTERUNI NY

UT IRT Dana AU

 

 

349

Gebäude befestigt wird. Dieses Stativ bietet wesentliche Vortheile gegenüber dem früheren
Pfeilerstative, sowohl bezüglich seiner Festigkeit als auch Transportabilität, da das Fort-
schaffen des Steinpfeilers entfällt.

Auf den heurigen 65 Stationen hat sich bei der Verwendung dieses Wandstatives
keinerlei Anstand ergeben.

Die Festigkeit der Aufstellung desselben scheint eine absolute zu sein, da das ange-
wendete Wipp-Verfahren keinerlei Bewegung des Pendels wahrnehmen liess, obwohl die
Wippvorrichtung direct an das Pendelstativ angesetz wurde. Die Schwingungszeiten der
Pendel auf dem neuen Stalive sind um etwa 80—90 Einheiten kleiner als auf dem alten.

Die Resultate der vorjährigen Beobachtungen in Ober- und Nieder-Oesterreich (1894)
sind im XIV. Bande der Mittheilungen des k.u. k. militär-geographischen Institutes publi-
eirt. Dieselben sind in mehrfacher Hinsicht von Interesse. Zunächst zeigt es sich, dass die
Wahl der Stationen, ihre Enifernung von einander u. s. w. eine ganz entsprechende war.
Aus den zahlreichen, dicht aneinander liegenden Stationen ergab sich eine unerwartet grosse
Menge von Detail in der Vertheilung der Schwerkraft im Innern des Landes. Es ist zu er-
warten, dass, wenn einmal grosse Flächen in dieser Weise durchforscht sein werden, es
möglıch werden wird, den Ursachen der so auffallenden Verschiedenheit der Schwere nach-
zuforschen.

Im vergangenen Herbste 1894 war es mir infolge einer sehr freundlichen Einladung

. der kaiserlich-russischen geographischen Gesellschaft ermöglicht, relative Schwerebestim-

mungen auf den Sternwarten in Pulkowa bei St. Petersburg und Moskau auszuführen; die-
selben sind bei den daselbst vorhandenen, sehr günstigen Umständen, vollkommen gelungen
und erscheint jetzt Wien mit den meisten Ländern Europas direct verbunden und Oppolzers
Wert für die Schwere dahin übertragen.

Auch ın dem abgelaufenen Jahre wurde eine grössere Anzahl von Pendelapparaten,
grösstentheils an das Ausland, geliefert, darunter bereits fünf neuartige Wandstative. Die
Constanten und Schwingungszeiten der Pendel wurden immer wieder in Wien bestimmt.

Ich erlaube mir am Schlusse dieses Berichtes noch einige Worte beizufügen über
das Ergebnis der von mir ausgeführten Beobachtungen der Variation der Schwerkraft an ein
und demselben Orte mit dem von mir hiezu construirten sogenannten Barymeter.

Wenn auch immerhin angenommen werden muss, dass bei der Reduction der mit
diesem subtilen Apparate ausgeführten Beobachtungen nicht allen Umständen vollkommen
Rechnung getragen wurde, so glaube ich doch den Resultaten mit aller Wahrscheinlichkeit
zu eninehmen, dass die Schwerkraft kleinen Veränderungen bis zum Betrage von 3 bis 4u. der
Secundenpendellänge unterworfen ist. Die erhaltenen Resultate sind im XIV. Bande der Mit-
theilungen des k. u. k. militär-geographischen Institutes eingehend besprochen.

Obzwar dieser Gegenstand nicht im unmittelbaren Zusammenhange mit den Zielen
der Erdmessung steht, so habe ich mir doch erlaubt, denselben hier kurz zu berühren, weil
ich glaube, dass derselbe hier Interesse finden dürfte.

Wien, im Dezember 1895. von STERNECK, Oberst.

ASSOCIATION GEODBSIQUE II — 32

 
