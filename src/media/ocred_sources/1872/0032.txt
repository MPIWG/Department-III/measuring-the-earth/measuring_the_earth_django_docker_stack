 

|
i
i

ee ee

noe

Höhenaufnahmen zu machen, dass Höhenschichtenkarten im Massstab von mindestens 1: 100,000
angefertigt werden können, worauf Herr Hügel erklärt, dass dieses in Hessen geschieht,
und Herr Bauernfeind, dass in Bayern solches Material vorhanden, jedoch noch nicht voll-
ständig veröffentlicht ist. Herr Schoder theilt in dieser Beziehung mit, dass in Württemberg
bereits ein Zehntel des Landes genau nach Horizontalecurven aufgenommen ist.

Ebenso berichtet Herr Nagel, dass in Sachsen gegenwärtig eine geoenostische Karte
mit Höhenschichten bearbeitet wird.

Auf Antrag von Herrn Bruhns wird beschlossen, die Nivellements nicht mit in den
Kostenanschlag aufzunehmen, weil diese schon zum grossen Theil ausgeführt oder in der
Ausführung begriffen sind und weil anzunehmen ist, dass die einzelnen Regierungen die
noch fehlenden Nivellements im Interesse der Europäischen Gradmessung, der Technik und
der Topographie ausführen lassen werden.

Nachdem hiermit die Angelegenheit der Nivellements erledigt ist, verlässt Herr
von Morozowicz die Sitzung, an welcher weiter Theil zu nehmen er verhindert ist.

Herr Bauernfeind verliest sodann das Gesammtresultat der Berathung über die Or-
ganisation der Reichscommission und nachdem einige Redaetionsänderungen, welche die
Vorstandschaft anzubringen zweekmässig gefunden hatte, genehmigt sind, wird die endgültige

Fassung der 8 Paragraphen dem gegenwärtigen Protokoll beigelegt.
iB fe aol ae: e:

Bestimmungen über die Einrichtung und den Wirkungskreis der Reichscommission
für die Deutschen Gradmessungsarbeiten.

Soe
Für die Herstellung einer in allen Beziehungen dem gegenwärtigen Stande der
Wissenschaft und Technik entsprechenden Gradmessung wird eine dem Reichskanzler-Amte
unmittelbar untergeordnete besondere Commission gebildet, welche die Bezeichnung ,,Reichs-

Commission für die Deutsche Gradmessung‘ zu führen hat.

5.2

Diese Commission besteht aus denjenigen Miteliedern, welche die Deutschen Re-
gierungen als ihre Vertreter bezeichnen.

Die Mitglieder der Reichseommission versehen ihre Functionen als ein durch ihre
Landes-Regierungen jederzeit widerrufbares Nebenamt und beziehen dafür. angemessene
Funetionsbezüge nebst den regulativmässigen Tagegeldern und Reisekosten für Geschäfte
ausserhalb ihres Wohnorts.

$. 4.

Die Reichscommission wird von einer permanenten Commission geleitet, welche aus

drei Mitgliedern besteht und die Bezeichnung führt: „Vorstandschaft der Reichscommission

iss sach Ib hi ll id lh ald he

 

 
