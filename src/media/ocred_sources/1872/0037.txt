RTT ET TTT RETR TT TITTY ene n

rooney

We OTT BRETT TT IED op en mone

On

Conferenzen der mitteleuropäischen resp. Europäischen Gradmessung von der Ansicht aus-
gegangen sei, den von Sr. Excellenz seiner Zeit näher bezeichneten Sphäroidstreifen mög-
lichst speciell zu untersuchen und sich nicht mit blossen Dreiecksketten zu begnügen,
weshalb noch damals einzelne Staaten, wie z. B. Oesterreich, namentlich für Böhmen, die
Schweiz und Sachsen umfänglichere Gradmessungs-Arbeiten eingeleitet hätten.

Die Reichscommission erklärt sich mit der Ansicht einverstanden, dass der von ihr
aufgestellte Entwurf und Kostenanschlag das Minimum dessen bezeichne, womit den in der
Kntschliessung des Reichskanzler-Amts vom 20. November c. bezeichneten Aufgaben genügt

werden könne.
2) Neue Dreiecksketten.

Der

von Morozowicz hat in einer neuen 1870 erschienenen vermehrten Auflage der Hauptdreiecke

vegenwartige Chef der Preuss. Landes-Triangulation, Herr General - Major
den Wünschen der permanenten Commission so vollständige entsprochen, dass diese Arbeiten
nunmehr als ein schätzenswerther Beitrag für die Gradmessung anzusehen sind. Dasselbe
gilt noch in erhöhtem Grade von allen seitdem von ihm ausgeführten Dreiecken der ersten
Ordnung, die sich nach Form und Inhalt den Arbeiten des geodätischen Instituts genau
anschliessen, wovon die nachstehenden Resultate seiner Dreiecke der ersten Ordnune in
Holstein in den Marken und in Schlesien den Beweiss liefern, die er dem Centralbiireau
als Beitrag für die Gradmessung übergeben, und dadurch den Zuwachs der Preussischen

Gradmessungs-Triangulation beträchtlich erweitert hat.
A. Dreiecks-Netze in Schleswig-Holstein.
Richtungen und Entfernungen der Dreieckspunkle.
Basis (südlicher Endpunkt.)

Log. s (in Metern.)

0 , I!

Basis, nördl. Endpkt. | 0 0 0000 | 3,1000, a

Bocksberg | | 3 ‘11 54084 | So o

Bornbeek 0 Sur = 81 44. 5469 ı en
| |

Havighorst 238 34 20,256 4,0085620 . 4

Basis (nördlicher Endpunkt.)

Basig, ‚südl. Endpkt, „sear 50 448. | 0 0 0,000 | 3,2690259. 41

Hayishoist., rade ay 5 ne | 81: Dl. 60008 | 4,1516302 . 0

Bocksbetgn., .. de 2 se | 188-23 Bags | 3,5808273 .5

Bornbeck . ‘ : | 304 21 49,460 | 3,9338718..,9
3 à

 
