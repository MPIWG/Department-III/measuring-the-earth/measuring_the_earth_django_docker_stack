a u EEE ee

en

eS ER TR

meet

 

i
f
f
I
I
|
}
}
i
{
|
|
1
{
|

ee

A
Hi
u

EEE

EEE

een

in cui alla normale N si assegnö un valore »variabile a seconda delle varie latitudini corris-
pondenti alla media di ciascuno dei grandi triangoli che compongono la rete.
Nel calcolo per le equazioni dei lati ed in quello dei triangoli si adoperarono
tavole a dieci decimali ritenendo esatta nel computo dei lati l’ottavo decimale del logaritmo.
Napoli, 6. Novembre 1872.
Il Professore di Geodesia

J. Schiavoni.

2. Relazione sul procedimento del calcolo della Base Geodetica misurata presso
la foce del f. Crati.

Note relative.

Con l’onorevole foglio del Comando Generale del Corpo Nr. 1166 del gno 11 Ottobre
p. p. anno 1871, ebbi Vincarico del caleolo della Base del Crati. Appena compivasi la
determinazione della sua lunghezza ottenuta dalle due misure fatte sul terreno, io mi
affrettai a darne partecipazione alla S. V. onde assicurarla della esattezza ed accordo delle
due misure, e quindi della perfetta riuscita del calcolo.

Perd al compimento del lavoro mancava ancora:

1° Livellazione tra i due Estremi della Base ottenuta dalla inclinazione delle

spranghe nelle due Misure.

20 Altezza assoluta dei detti due Estremi e quindi proiezione della Base al

livello medio del mare. ;

3° Coordinamento di tutto il calcolo ed osservazioni di Campagna, onde riunito

rimanesse a documento nell’ Archivio Tecnico del nostro Corpo.

Ora che tuttocid ¢ completato, mi onoro riferirne alla 8. V. il procedimento del
suddetto calcolo in tutti i suoi particolari, facendo seguito all’ altra mia relazione sul
metodo tenuto nella misura sul terreno, relazione che ebbi gia Vonore far tenere alla S. V.

La Base Geodetica del Crati, a seconda delle superiori disposizioni venne misurata
col nostro apparato di Base costrutto dal meccanico Ertcl come quello di Bessel. Quale
apparato si trova gia sommariamente descritto dal Luogo T!. Generale Mse. Ricci nel
suo rapporto sulla misura della Base di Catania nel 1867, Command!° in quell’ epoca
del Corpo.

Elementi sui quali s’e fondato il calcolo. Il suddetto apparato sin dal 1858,
quando si ebbe dal macchinista costruttore, fu campionato esaminandolo in tutte le sue
parti; detto lavoro di campionatura fu eseguito serupolosamente, come lo indica il risulta-
mento ottenuto in tutte le misure di Base, in cui si é adoperato. Quindi gli elementi gia
determinati dal suddetto esame sono da ritenersi esatti e fondarvi il calcolo in parola.
Due sole ricerche dovevano ripetersi:

La prima (indispensabile) cioé: determinazione degli elementi per la

 

Ad ha à al le mandat 2 à

rue,

 
