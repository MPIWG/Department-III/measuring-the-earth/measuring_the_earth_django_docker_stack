  

EEE ee

RER or a:

ee

ss Se ee

EEE

=

NE SN NE IS

eee

ner

 

D

forti differenze; secondo perché nel modo come ha proceduto tutto il lavoro di altitudini
non ha la precisione richiesta per l’applicazione dei minimi quadrati.
Cid posto si ottengono le altezze assolute dell’ Estr. S. BE. = 11",1442
id id dell’ Estr. N. O. = 13”,5842
Determinazione dell’ altezza media della linea di Base sul livello
del mare.
La misura di detta Base ebbe luogo in un terreno pressocchè piano, di tal che le
sue leggiere ondulazioni offrono piccole differenze di livello rispetto alla normale terrestre,

e quindi piccolissima influenza sulla proiezione al livello del mare della distanza misurata. |

Sicch® puö ben ritenersi che ciascun Tratto di Base giornalmente misurato abbia
un’ inclinazione costante, ed eguale alla differenza di livello dei due punti, che lo consti-
tuiscono. Fondato su cid, si & calcolato l’altezza assoluta dei due Estremi del tratto gior-
naliero; la loro media ha offerto l’altezza del Tratto sul livello del mare.

Il medio aritmetico delle altezze assolute di tutti i Tratti misurati giornalmente,
da con sufficiente esatezza l’altezza media della Base sul livello del mare.

Detto ealcolo & stato eseguito con tutto il rigore sulla prima e seconda ellisura;

perd nella prima si à proceduto in senso contrario della misura sul terreno, cioe da N. O.

a 8. E., perch® solo a N. O. si aveva precisamente l’Altezza della spranga sulla pietra

Estremo Base.
I] risultamento ¢ il seguente:
Altezza assoluta della linea di Base 1% Misura = 13,1320
22 Misura = 13,0930
Quindi l’Altezza Assoluta della linea di Base @ il medio delle sudd cioe 18,1125;
il cui accordo giustifica la precisione del lavoro sul terreno.
Correzioni alla Base per la sua proiezione al livello del mare.
La nota formola per proiettare all livello del mare una lunghezza misurata, ©

b — B — 2 essendo

b. lunghezza della misura,
B. la sua proiezione sul livello del mare,
N. la normale terrestre del sito,
h. Valtezza assoluta media della linea misurata.
Nel caso nostro alla latitudine media del posto di misura,
cioe 39°.55’ log. N = 6.8051945 quindi
log. b = 3.4653166,326 (v. pag. 123 Calcolo della Base.)
log. h = 1.1176689,000
166,000
Compl. log. N = 3.1948055,000

 

7,7778076,326 log. 0,006000.

 

eves eth te 4 Du hd db à a le

 
