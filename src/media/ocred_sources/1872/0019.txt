SITET CTT RETR ATT UTITRWY N SOTTT TATEN N

yep

+ DEEE

mnt

reer

nn Be =

führung zufolge Genehmigung des k. und k. Reiehskriegs-Ministeriums das Militair. geogr.
Institut übernommen hat. |

Der für diese Arbeit zunächst bestimmte Offizier, Herr Lieutenant Steffan führte
unter der Leitung des Herrn Prof. Dr. Tinter ein Probe-Nivellement zwischen der auf der
hohen Warte befindlichen kk. Central-Anstalt für Meteorologie und dem Donau-Pegel an der
Ferdinandsbrücke in Wien aus.

Nachdem dieses befriedigend ausgefallen war, begann der genannte Offizier das
definitive Präeisions-Nivellement vom Fluthmesser in Triest und führte dasselbe auf der
Chaussee über Opeina bis Adelsberg aus.

Von dieser Hauptlinie wurden auch noeh Seiten-Nivellements und zwar zum Bahn-
hofe in Triest und zu dem trigonometrischen Punkte Opeina ausgeführt.

Im heurigen Jahre und den folgenden wird dem Programme entsprechend mit 4,
womöslich mit 6 Parthien dasselbe fortgesetzt werden.

Die für Gradmessungszwecke im Jahre 1872 verausgabten Beträge belaufen sich
auf 16500 fl. 6. W. ‘In diesem Betrage sind die Kosten der — durch den Herrn Prof.
Dr. Tinter ausgeführten Polhöhen- und Azimuth-Bestimmung auf dem Jauerling mit inbegriffen.

Die pro 1873 präliminirte und bereits bewilligte Summe beträgt 21500 fl. ö W.

Wien, am 21T. Pebruar 187.
Ganahl,
Oberst.

2. Bericht über die Bestimmung des Längenunterschiedes zwischen Pfänderberg
bei Bregenz und Zürich, sowie über die Bestimmung der Polhöhe und des
Azimuthes auf dem Pfänderberg im Sommer 1872.

In dem Sommer 1872 wurde die Längenbestimmung des Punktes erster Ordnung
„Pfänder‘“ bei Bregenz gegen Zürich ausgeführt; in Zürich beobachtete Professor R. Wolf,
am Pfänder der Unterzeiehnete. Die Beobachtungen sind innerhalb des Zeitraumes vom
7. Juli bis zum 19. August gelegen, und die diesbezüglichen Bestimmungen sind an 14
Abenden gelungen.

Um die persönliche Gleichung aus dem Resultate eleminiren zu können, sind ent-
sprechende Massnahmen ergriffen worden. Vorerst begab ich mich mit den bei der Zeit-
bestimmung in Verwendung gebrauchten Instrumenten vom Pfänder nach Zürich und
beobachtete an 7 Abenden (21. August bis 2. September) gemeinsam mit Wolf, der ebenfalls
die bei der vorausgehenden Längenbestimmung angewandten Instrumente benutzte. Diese
Methode der Bestimmung der persönlichen Gleichung leidet bekanntlich sehr unter dem Mangel,
dass die Unsicherheit der Instrumentalfehler ganz in das Resultat übergeht; bei der grossen
Verschiedenheit der Instrumente (ich benutzte ein gebrochenes Passageinstrument von 30
Linien Objektivöffnung von Starke, während Wolf den grossen #zölligen Kern’schen Meridian-

General-Bericht f. 1873. 3

 
