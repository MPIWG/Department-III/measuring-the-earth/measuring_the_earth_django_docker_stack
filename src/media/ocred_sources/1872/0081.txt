rm

u”

Ä
|
;

a

o

Il

8 Librae gab für 6% 59% Chr. Zeit: Uhrcorrection — "249 495,60

Spica „ ” 7.008 ” ” ni end 495,69

der Gang des Chronometers in 17 Minuten war a: Os,
also gaben diese Beobachtungen bis auf 0*,02 dasselbe Resultat, und die hier gebrauchte
Bessel’sche Refraetion wird also durch diesen Versuch bewährt.‘

cemeOu d ema ms.

Diese Methode zur Prüfung der Richtigkeit der angewandten Refraetion wird gerade
für die Acquatorgegenden der fast senkrechten Bewegung der Sterne wegen recht genaue
Resultate ergeben: in der That entsprieht im vorliegenden Falle einer Aenderung der
Zenithdistanz von 8 Librae und « Virginis im Betrage von 1” eine Aenderung des Uhr-
standes von resp. 0°,06 und 0°,07. Hiernach würde unter Berücksichtigung des Umstandes
dass die mittlere Refraetion in 51° Zenithdistanz: 142, in 82% Zenithdistanz: 6,5 beträgt
cine Aenderung der Refraction im Betrage von 1% den Uhrstand aus den Beobachtungen
von 8 Librae um 0°,04, aus denen von @ Virginis um 0°,23 ändern, also 0°,19 Differenz
ergeben, während oben nur 0°,02 beobachtet sind.

Baeyer.

 
