 

TN PT LOIRE

amt

t
ib
E
iR
=
ak
k
k
È

ausgedrückten Zahlen auf der Voraussetzung beruhen, dass der von Repsold unserm Pendel
beigegebene in Pariser Linien eetheilte Maassstab richtig sei. Sobald wir einen von der
internationalen Meter-Commission hergestellten neuen Normal-Meter zur Verfügung haben,
werden wir nicht verfehlen, die aus der Vergleichung desselben mit unserm Pendel-Maass-
stab sich etwa ergebenden Correeturen obiger Zahlen zu publiciren, und alle unsere
Messungen in neuem Meter-Maass auszudrücken.

Bei dieser Gelegenheit sei es mir gestattet, indem ich mir einen ausführlicheren
Bericht über die Resultate der internationalen Meter-Conferenz für einen andern Ort vor-
behalte, hier nur kurz zu erwähnen, dass die für die Europäische Gradmessung so äusserst
wichtige Maassregulirung in Paris insofern wesentlich gefördert worden ist, als daselbst die
Herstellung neuer, für alle Länder gleichwerthiger, sowohl unter sich als mit dem bisherigen
metre des archives genau verglichener Meter-Prototype beschlossen, und zugleich den Re-
gierungen die Gründung eines internationalen Maass- und Gewichts - Instituts empfohlen
worden ist, unter dessen Attributen auch die Vergleichung aller bisher in der Wissenschaft
verwandten Maasseinheiten und noch jetzt benutzten Maassstäbe mit dem neuen Meter
sich befindet. |

Indem ich von dieser Abschweifung auf unsere Schweizerischen astronomisch-eeodä-
tischen Arbeiten zurückkomme, habe ich zunächst noch zu erwähnen, dass die 1870 aus-
veführte Längenbestimmung Mailand —Simplon— Neuenburg noch nicht fertig redueirt ist;
doch lässt sich voraussehen, dass dieselbe noch in diesem Jahre dem Drucke übergeben
werden kann.

Was die übrigen bereits in früheren Jahren ausgeführten Ortsbestimmungen unserer
astronomischen Stationen betrifft, so hat Herr Plantamour für den Rigi, Weissenstein und
Bern sowohl die Breiten- und Azimuth-Messungen, als auch die Pendel- Beobachtungen
fertig reducirt, so dass die betreffenden Publicationen wahrscheinlich noch in diesem Jahre
erfolgen werden. Was den Simplon betrifft, so sind die Breiten-Beobachtungen bis auf
Anbringung der definitiven Declinationen redueirt; Azimuth und Pendel-Beobachtungen sind
noch zu berechnen.

Endlich sind in der Schweiz im letzten Sommer (1872) Breite, Azimuth und Schwere
für die an unserer Ost-Grenze gelegene Station Gäbris (Canton Appenzell) bestimmt, und
durch die Längenbestimmung zwischen diesem Punkte, Zürich und dem Pfänder in Vorarl-
berg der Anschluss an Oesterreich erzielt. Dieser schon in früheren Jahren verabredete
Anschluss kam zur Ausführung, sobald von österreichischer Seite Herr Professor von Oppolzer
für die geodätischen Orts-Bestimmungen gewonnen war. In einer Anfangs Juni in Zürich
zwischen den Herren Oppolzer, Wolf und Plantamour stattgefundenen Conferenz wurde
verabredet, die Operationen am 11. Juli zu beginnen; leider wurde der Letztere durch eine
schwere Krankheit verhindert, vor Ende Juli auf den Gäbris zu kommen, und da Herr
von Oppolzer seinerseits seinen Aufenthalt auf dem Pfänder nicht so lange ausdehnen

konnte, um eine genügende Anzahl Bestimmungen mit Herrn Plantamour zu erhalten, so

 

 
