re

Als Mitglieder der Commission sind von den resp. Bundesregierungen folgende
Herren bezeichnet worden:

I. Von Preussen: Ausser Ew. Excellenz die Herren:
1. Professor Dr. Peters, Director der Sternwarte zu Altona,
2. Professor Dr. Sadebeck, Sectionschef am Königlichen geodätischen Institut
zu Berlin,
3. Professor Dr. Bremiker, Sectionschef am Kôniglichen geodätischen Institut
zu Berlin,
4. Dr. Weingarten, Professor an der Königlichen Bauakademie zu Berlin,
II. Von Bayern die Eerren:,
5. Professor Dr. Bauernfeind, Direetor der Königlichen polytechnischen Schule
zu München,
6. Dr. Seidel, ordentlicher Professor der Universität und Conservator der mathe-
matisch-physikalischen Sammlung des Staats zu München.

ASSET TANTE TREE Leu

Ill. Vom Königreich Sachsen die Herren:
7. Professor Dr. Bruhns, Director der Sternwarte zu Leipzig,
8. Herr Nagel, Professor an dem Polytechnicum zu Dresden,
IV. Von Württemberg:
9, Herr Dr. Schoder, Professor an der Königl. polytechnischen Schule zu Stuttgart,
V. Von Baden:
10. Herr Jordan, Professor an der polytechnischen Schule zu Carlsruhe,
VI. Von. Hessen:
11. Herr Obersteuer-Director Dr. Hügel zu Darmstadt,
Vil. Von Mecklenburg-Schwerin:

A TN

12. Herr Geheimer Kanzlei-Rath Paschen zu Schwerin,
VIII. Von Sachsen Coburg-Gotha: j
13. Herr Geheimer Regierungs-Rath Hansen zu Gotha.

Die Grossherzoglich Oldenburgische Regierung hat mit Riicksicht darauf, dass eine
zum Eintritt in die Reichs-Commission geeignete Persönlichkeit augenblicklich nicht disponibel
sei, auf die Ernennung eines Vertreters verzichtet.

Das Reichs- Kanzler - Amt
gez. Delbrück.

An den Königl. General-Lieutenant z. D.

Herrn Dr. Baeyer Excellenz hierselbst.

B. MA. No, 2372’ B. |

Auszug aus den Protokollen. Erste Sitzung am 18. December 1872. Anwesend die
Vertreter: Dr. Baeyer, Dr. Peters, Dr. Sadebeck, Dr. Bremiker, Dr. Weingarten, Dr.
Bauernfeind, Dr. Seidel, Dr. Bruhns, Prof. Nagel, Dr. Schoder, Hr. Jordan, Dr. Hügel.

NE

 
