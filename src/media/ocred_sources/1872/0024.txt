 

a ae
leu P rue. té sem

Bericht des Centralbüreaus der Europäischen Gradmessung.

1. Bildung einer Deutschen Reichs-Commission.

Erlass des Bundeskanzler-Amtes vom 20. November 1872 an den General-Lieutenant
z. D. Dr. Baeyer: Der Bundesrath hat auf die in einem Druck-Exemplar beigefügte Präsi-
dial-Vorlage vom 4. Juni 1871 in seiner Sitzung vom 14. Oktober 1571 beschlossen:

Die hohen Bundes-Regierungen zu ersuchen, einer Verständigung dahin zuzustimmen:

1. dass für die deutschen Gradmessungs-Arbeiten eine Reichs-Commission gebildet
werde;

2. dass die Reichscommission den Auftrag erhalte, einen einheitlichen Plan für die
deutschen Gradmessungs-Arbeiten zu entwerfen, dem alle bereits ausgeführten
probehaltigen Arbeiten einzufügen sind, und einen darauf gegründeten Kosten-
überschlag zu entwerfen und vorzulegen.

Er ging bei diesem Beschlusse von der Voraussetzung aus, dass die Entscheidung
über die Annahme oder Ablehnung der von der Reichscommission seiner Zeit zu machenden
Vorschläge noch vorbehalten bleibe.

Die sämmtlichen hohen Bundesregierungen haben ihre Zustimmung zu der Bildung
der gedachten Reichsscommission und zwar theils unbedingt, theils unter Beifügung der-
jenigen Vorbehalte, beziehungsweise Bemerkungen erklärt, welche das in auszugsweiser Ab-
schrift ergebenst beigefügte Sitzungs-Protokoll des Bundes-Rathes vom 9. Februar d. J.
(§. 25) enthalt. Sodann sind dem Reichs-Kanzler-Amt von den Regierungen derjenigen
Bundesstaaten, für deren Gebiet seither schon Gradmessungs-Commissionen gebildet worden
sind, die zu Mitgliedern der Reichscommission bestimmten Persönlichkeiten bezeichnet

worden. Dem Zusammentreten der Commission steht sonach zur Zeit nichts mehr entgegen.

Ew. Excellenz sind dem Reichskanzler-Amt von dem Herrn Minister der Unterrichts-
Angelegenheiten als einer der Herren Vertreter Preussens bezeichnet und werden mit Zu-
stimmung des gedachten Herrn Ministers ergebenst ersucht, den Vorsitz in den Sitzungen
der Commission zu übernehmen. Sie wollen demgemäss die übrigen Herren Mitglieder der
Commission hiervon gefälligst benachrichtigen und sich über den Tag des ersten Zusammen-
tretens mit denselben verständigen. Die Berathungen können, falls Hw. Excellenz nicht
ein geeigneteres Lokal zur Verfügung steht, in dem Gebäude des Reichskanzler- Amtes,
Wilhelmstrasse No. 74, stattfinden. Einer gefälligen Benachrichtigung hierüber, sowie von
dem Tage, an welchem die Verhandlungen beginnen werden, darf diesseits ergebenst ent-

=
©

gegengesehen werden.

Besondere Instructionen in Betreff der Leitung der Verhandlungen hat das Reichs-
Kanzleramt Ihnen nicht zu ertheilen, da die Aufgaben der Commission durch den erwähnten
Beschluss des Bundes-Raths ihre feste Begrenzung bereits gefunden haben.

a cou ae ii dh dbl lh an

 

 
