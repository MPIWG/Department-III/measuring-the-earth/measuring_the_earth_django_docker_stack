TNT TET RTT PTT TON FT TPT OR TI)

Lu 2

3 Tagen dasjenige des Hohen-Freschen (Oesterreichisches Signal in Tyrol), an 2 Tagen
dasjenige des Sentis und einmal das des Hörnli (Schweizerisches Signal) bestimmen. An
einigen Tagen konnte ich ausserdem Winkel zwischen diesen Signalen messen, aber ohne
Beobachtung des Polaris.

Endlich hat Herr Plantamour auf dem Gäbris die Pendelbeobachtungen in ähnlicher
Weise wie auf den übrigen Stationen ausgeführt.

HI. Nivellement.

Die nivellitischen Arbeiten wurden in den letzten Jahren bei uns mit demselben
Eifer und in gleicher Weise fortgesetzt wie früher. Wenn die längst beabsichtigte Publication
der vierten Lieferung unseres „Nivellement de précision de la Suisse“ noch nicht erschienen
ist,*) so liegt dies nur an dem Umstande, dass, während unsere früheren Polygone in der
ebenen Schweiz und im Jura mit seltenen und unbedeutenden Ausnahmen sehr befriedigend
abschlossen, dies nicht mehr der Fall ist, seit wir in das Gebiet der Alpen gelangt sind.
Wir sind dadurch genöthigt, die betreffenden ausgedehnten Linien von Neuem zu nivelliren,
und können die Resultate erst dann publieiren, wenn wir durch die Uebereinstimmung der
Doppel-Nivellements die nöthige Garantie für deren Richtigkeit erlangt haben, welche wir
sonst in dem Polygonal-Abschluss fanden.

Der Umstand, dass wir in unserm grossen Alpen-Polygon, wo wir das Hochgebirge
zweimal, über den Gotthard und den Simplon, überschritten haben, einen so bedeutenden
Abschlussfehler von 1",2 fanden, wie er uns bei den früheren Operationen niemals vor-
eckommen war, und der bei unserer Beobachtungsmethode nur mit geringer Wahrschein-
lichkeit als aus Beobachtungsfehlern hervorgehend angesehen werden kann, brachte mich
auf den Gedanken, dass vielleicht die Lokal-Attraction des Gebirges auf das Niveau einen
derartig störenden Einfluss ausüben könnte, dass auf den genauen Abschluss eines Höhen-
Polygons, wie er unter normalen Bedingungen nothwendig innerhalb der Beobachtungs-
fehler erfolgen muss, nicht mehr gerechnet werden darf. Eine nähere Untersuchung dieser
Frage, welche in einer Notiz enthalten ist, die in dem „Proces-verbal de la onzième séance
de la commission géodésiques Suisse, Neuchatel le 5 Mai 1872“**) abgedruckt ist, ergab
mir in der That das Resultat, dass bei einer selbst sehr mässig vorausgesetzten Loth-Ab-
lenkung (von 10”) die Höhen-Differenz zwischen Fuss und Gipfel der Gotthard-Strasse durch
das Nivellement um 2",4 zu klein erscheinen muss. Ferner, wenn man zwischen zwei auf
den entgegengesetzten Abhängen eines Gebirges gelegenen Punkten, wie Luzern und Locarno,
nivellirt, so genügt es, dass entweder die Intensität der Lothabweichung ‘auf den beiden
Seiten des Gebirges verschieden, oder der Winkel zwischen der Nivellements-Richtung und
der Ablenkungs-Ebene nicht der gleiche sei, um die Höhendifferenz um eine Grösse fehlerhaft
zu erhalten, welche von derselben Ordnung ist, als der uns vorliegende Abschluss-Fehler.

*) Die 4. Lieferung ist seitdem Ende Mai versandt worden.
**) Von diesem Protokoll sind dem Central-Biireau die nöthige Anzahl Exemplare zur Vertheilung

an die Herren Uommissare zugestellt.
General-Bericht f. 1873. Q:

 
