Ti en nn ore o>

A a FR RER TNT ITT TTT

gm na

IT

he

Quindi l’aitezza assoluta (sul livello medio del mare), del detto scalino di TÆ Schiavonia
& 2™,7958 e quella del P. P.= 17,8158. (Il P.P. di Schiavonia è la sommità ‚del parapetto
della Torre.) Ora da osservazi zenitali reciproche (non contemporanee) fatto dal sull Cap“
sig. Maggia alla T£ Schiavonia ed all’ Estremo S. E. (Base) si hanno i seguenti risultamenti.

Staz. Estr. S. E. (Base) — Dz. di Schiavonia JOU. OW og
Differenza di livello = ae ne 6™,8484
Staz® Schiavonia, — Dz. Hstr. 8. E. (Base) 909.031.4385

Difereuza di ivelo „na. 22.2... a0)

Differenza media di livello 6,1782.

Onde assicurare una maggior precisione si © ricercata la suddetta differenza di
ivello passando pei punti Polinara e Buffaloria che direttamente si legano alla Base.
Seguono i risultamenti, i quali perö hanno metä di peso rispetto al precedente direttamente

ottenuto.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Birch a ee |
„N |
| | Angoli | > |
Nomi dei punti. | a 01. d M. | secondo tra Schi-
ie le pro- Medie. vonia €
| venienze. Y’Estr. S. E.
| p11. 909.0530" 84 — 330 260) Sa
Schiavonia X - © U Bical 9,3000
| 90. 00.28.28 120 | 20] 9100
 Buffaloria | | | 6,6150
100, 1355 26" age a eo) ae
oe aL | 89. 49.50, 93| +4,95 | — 1,70 | 15,9400 109
(Schiavania | 20° 32-18, 10| —3,66,| +2,69 || 116,9400 en
cN1aVONl :
I} 89. 34.04, 7041,26 | —945 | 117,3200 117
Polinara | bi 6,5150
ie 19.30, 23) — 3,66 | +4,35 | 128,6800 1193 big
Hn & el 188. 44.22, 77) #20, 215 | 123,6100

 

Di tal che la differenza media di livello si otterra da 6,7782 x 2.4 6,6150 + 6,5150
4

 

ossia = 6,6716.

Ora Vattezza assoluta della Torre di Schiavonia (piano di paragone) & 17,8158 —
Quindi quella dell’ Hstremo S. E. della Base risulta = 11”,1442.

N.B. Non si & stimato servirsi di tutti i punti del poligono attorno alla Base, e
compensarne le quote; pmo perché i risultati delle osservazioni zenitali a Cassano accennano

 
