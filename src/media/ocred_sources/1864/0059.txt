I Tune

ve TFA SH AT Tee TI INT |

ram ann

iR!

Bye a

TER HIN

. DB

gegebenen Ausdruck, noch nicht genau genug bestimmt ist; weil, wie die nachfolgende Unter-
suchung zeigt, ungeachtet der Kleinheit des Winkels, doch die mit (ee)? behafteten Glieder
noch einen bemerkbaren Einfluss erlangen.

Um jede Unsicherheit über die erwähnte Azimuthal-Differenz zu ae werde
ich dieselbe ohne Einschränkung zu bestimmen suchen.

Zieht man in meiner Schrift: das Messen auf der sphäroidischen Erdoberfläche, die
Gleichung für cote’ auf Seite 64 von der Gleichung (77.) auf Seite 62 ab, so erhält man

 

 

 

 

cosuigu . ee cos u’ (sinu'— sin) Bi
cotgo'— cotg ae’! — 1 —— — sınu’ cotgw : 1— ee cos uw)?
5 S sin Ei: cosU sin ( )
cos u tgu i
mas 7 BNUNCOLE
sin a 5
nun ist
2 1.3.5 3 '
(1— eecos’u) —= 1+-$eecos? W322 (eo) cos!u + ———— : Te (ee)’cos’w-+ --
=1+P.

Man erhält daher
ee cosu' (sin — sin u)

 

 

 

cotgv'— cotge’ = cosu teu( en — sin u’ (cotgw — cotgw) +

 

 

 

 

 

sin® sino® cOSUsINnW
th
a+-uwN\N .. (B—w
2c0s( = Jain (
1 an 2 2
sinw sino sin sin
sin (w — W)

cote ww — cotew = — -
& 5 sın @SINW

sinw — sıny = 2eos(" 2 )sin eo »

 

 

sin (@'— v)

cote do’ — cotze! = — -
5 5 sın «'sın®’

Vertauscht man sin(@'—v') mit dem Bogen, was stets erlaubt ist, und setzt = w+# so
findet man .

sin @'sin®' | cosu'tgu cos (w-+ 4X)2sin 40 —sinw'sinz

sin (w-+ x)
: Wu . (W—u u
+ 2eecosu cs )sin( 5 )+eecos u| ]

 

av! — — -
sin wo sin 1”

 

 

+ 5 - Se - 14)

wo
eecosu _.
a, i — —— (sinw'— sin“).
[ ] = eoswtgu— sinu’ cosw+— ., ( )

g*

 
