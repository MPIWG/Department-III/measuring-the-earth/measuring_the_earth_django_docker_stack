Te en em in) ee

KU: FF 7 Are TRFTT DIyT |

4

MU AR

LI I}

N

unter der Leitung von Gauss ausgeführten Triangulationen angewendet wurde, konnte nur
als ein vorläufiges Resultat der Basismessung angesehen werden, da die Reduction auf den
Meeresspiegel und mehrere andere Correctionen noch nicht berücksichtigt waren. Da diese
Reductionen an Grösse beträchtlich die Unsicherheiten der Messungen selbst, die mit grosser
Sorgfalt ausgeführt sind, übersteigen, war eine neue Bestimmung nothwendig, und Herr Pro-
fessor Dr. Peters in Altona hat auch die Güte gehabt eine ausführliche, mit der grössten
Genauigkeit durchgeführte Berechnung sämmtlicher Correctionen vorzunehmen, durch welche
die Länge der Basis sich nun stellt wie folgt:

a, die Länge von 1505 Messstangen ohne Correction . . . . . . .. 3010,00000 Toisen
b, Summe der mit den Glaskeilen gemessenen Intervalle und der in

Betracht kommenden ganzen und halben Durchmesser der Ablö-

 

thungs-Oylinder vo... ia /uooh Ma. DR +3,58389 „

c, Länge der Ergänzungsstange Il lt. ; +1,22106 °,
d, Correction wegen Neigung der Ash. Punk: gegen ‚die

Diothlinie 1. et wie an. mel. —0,00008 „

e, - »„ Abweichung der Stangen vom Alignement . — 000051 ,

£ a » fehlerhafter Längen der Messstangen . . . 0025
g; 5 »„ Abweichung der Temperatur der Messstangen

von 13°R. —0,19906 ,

h,. Beduction ‚auf.die Oberfläche des Meesa I . 2. 2.2.2.2. — 0,02264 „

Länge der Braacker-Basis nach der neuen Berechnung = 3014,48021 Toisen.

Es findet sich aber auch in dieser Berechnung ein schwacher Punkt, nämlich die
sub g angeführte Correction wegen der Temperatur der Messstangen. Eine mit Abbildungen
versehene Beschreibung des bei der Basismessung angewandten Apparats hat Schumacher in
der Schrift: „Schreiben an Dr. Olbers in Bremen etc. ete., Altona 1821* veröffentlicht, und
ınan wird daraus ersehen, dass die Temperaturen nicht durch Metallthermometer, sondern
durch gewöhnliche, eingelegte Thermometer bestimmt sind. Dies ist nun an und für sich ein
misslicher Umstand, aber viel schlimmer stellt sich die Sache, da die Ausdehnbarkeit der
Stangen nur aus einigen im Felde vorgenommenen Messungen der Stangenlängen am Abend
und am Morgen abgeleitet wird. Es kann aber diesem Uebel abgeholfen werden. Im Jahre
1853 wurde nämlich die Stange No. IV. des Schumacherschen Basisapparats nach Pulkowa
gebracht, um direct mit den dort gesammelten Etalons verglichen zu werden. Bei dieser
Gelegenheit wurde nun auch die Ausdehnung dieser Stange für 100° erhalten, und wenn
man den von Struve (Siehe „Arc du meridien entre le Danube et la mer glaciale,* pag. 51)
angegebenen Werth der Ausdehnungscoefficienten berechnet, dann erhält man für die Cor-
rection sub 9: — 0,22812, statt — 0,19906.

Mit dieser Berichtigung, welche auch von Professor Peters adoptirt wird, findet man
dann die Länge der Braacker-Basis:

— 3014,451 Toisen,

 

 
