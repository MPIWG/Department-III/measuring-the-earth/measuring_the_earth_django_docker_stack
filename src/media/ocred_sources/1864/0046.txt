 

I.

in dieser Dreiecksreihe eine genügende Genauigkeit haben, müsste vor Allem untersucht
werden. Die Bestimmungen der Längenunterschiede der sechs Sternwarten würden natür-
licherweise ein Hauptmoment bei dieser Längengradmessung bilden. Zum Glück ist die
nothwendige äussere Bedingung, um diese Bestimmungen auf telegraphischen Wege ausführen
zu können, im Wesentlichen erfüllt, indem eine ununterbrochene elektrische Telegraphenlinie
von Petersburg über Helsingfors, Abo, Stockholm und Christiania schon vorhanden ist. Dass
die Telegraphverbindung zwischen Abo und Stockholm den grossen Umweg über Tornea
macht, erschwert allerdings die Ermittelung der Längendifferenz dieser Oerter, bildet aber
durchaus kein unüberwindliches Hinderniss; übrigens dürfte eine direetere Trelegraphverbin-
dung zwischen denselben Städten in nicht sehr ferner Aussicht stehen. — Es steht zu hoffen,
dass sowohl der Kaiserl. Russische Abgeordnete bei der Oonferenz, Exe. Generallieutenant
von Blaramberg, als auch der Director der Pulkowaer Sternwarte sich der Sache annehmen
werden; ebenso dürfte man sich der Hoffnung hingeben können, dass die Norwegischen
Herren Astronomen und Geodäten ihre tbätige Mitwirkung nicht versagen werden. Nachdem
die anderweitigen Beiträge, welche von Schwedischer Seite für die mitteleuropäische Grad-
messung geliefert werden sollen, abgerechnet sind, so fallen auf Schweden für diesen beson-
deren Zweck keine weiteren Arbeiten, als die Theilnahme an der Längenbestimmung zwischen
Stockholm und Abo, und nöthigenfalls eine Revision der Dreiecke, welche von Stockholm
hinüber nach den Alandsinseln führen.

Wenn ich noch hinzufüge, dass, in Folge des von der Conferenz ausgesprochenen
Wunsches, Pendelbeobachtungen an einigen Schwedischen astronomischen Punkten ausgeführt
werden sollen, so habe ich hiermit sämmtliche Arbeiten angegeben, welche Schwedischerseits
die Theilnahme an der mitteleuropäischen Gradmessung erheischt.

Das erste Bedürfniss, was sich in Anbetracht dieser Arbeiten herausstellte, war, ein
sicheres Längenmaass zu besitzen. Bei einem Besuch, den ich im Herbst 1861 an der Stern-
warte in Pulkowa machte, wurde desshalb auf meine Bestellung von dem dortigen Mechaniker,
Herrn Brauer, eine Copie der Doppeltoise, welche Struve bei der Gradmessung in den OÖst-
seeprovinzen Russlands anwendete und welche später allen Basismessungen bei der grossen
scandinavisch-russischen Gradmessung zu Grunde gelegen hat, verfertigt. Ueber die Ver-
gleichungen, welche zwischen dieser Copie und ihrem Original ausgeführt worden, ist ein
Aufsatz in den Acten der Schwedischen Academie der Wissenschaften publieirt. Es ist ein-
leuchtend, dass die Maasseinheit, welche wir auf solche Weise für die Basismessungen in
Schweden erlangt haben, die Struve’sche Toise ist; da aber das Verhältniss der Struve’schen
Toise zu der Bessel’ schen, welche letztere als Grundmaass bei der mitteleuropäischen Grad-
messung dienen soll, von Struve selbst scharf ermittelt ist, so ist damit auch die Relation
der neuen Schwedischen Doppeltoise zur Bessel’schen Toise mit Schärfe gegeben. Ueber
den Basisapparat, welcher auf Grundlage dieser Doppeltoise verfertigt worden ist, sind einige
Notizen im Generalbericht pro 1863 schon mitgetheilt.

Nachdem ich im Sommer 1862 Recognoscirungen, um geeignete Terrains für Basis-

 

x
=
Ü

int ch a u Ah a Hm

|
1
|
|

 
