vi FF FRI SET STE RT IT

a N ge u!

Ueber die

sphärische Berechnung sphäroidischer Dreiecke.

Bes hat in No. 3. der astronomischen Nachrichten Formeln mitgetheilt um aus

den Polhöhen, Azimuthen, den Winkeln und Seiten eines sphäroidischen Dreiecks, die Win-

kel eines sphärischen Dreiecks zu finden, das mit den sphäroidischen gleiche Seiten hat,

diese Ausdrücke der Winkelverbesserung bis incl. der 3ten Potenz der Seiten und der 2ten

Potenz der Excentricität entwickelt.

Bei der mitteleuropäischen Gradmessung kommen aber

Erdbögen von 25 bis 30 Grad und mehr vor, wo diese Formeln nicht mehr ausreichen,

wenn man die Tausendtheile einer Erdsecunde nicht ganz vernachlässigen will. Ausserdem

bieten die Daten, welche für unser astronomisch-geodätisches Netz gewonnen werden, näm-

lich Polhöhen, Azimuthe, Entfernungen und Längenunterschiede die Mittel dar, diese Auf-

gabe auf einem ähnlichen Wege zu vereinfachen und ohne Einschränkung aufzulösen.

 

 

General - Bericht f. 1864.

 

Dis seien Bu]. sl. ss
n,n", n’" die Seiten und Win-
kel eines sphäroidischen Drei-
ecks; o', 0", o" dieGrundlinien
dreier sphärischen Dreiecke mit
dem Pol, die dieselben Azi-
muthe wie die sphäroidischen
Seiten und die reducirten Brei-
ten zu Winkelpunkten haben,
nämlich ACP, BCP und ABP.

In dem Isten Dreieck AOP
ist die Breite in A=w' die in
C=u!; der, Bogen AU,
und die Azimuthe ın A und
ın C, @ und .a®

Im 2ten Dreiecke BOP ist
die Breite in B=u", der Bo-
gen BC=0" und die Azimuthe
in C und m Ba" und a.

 

 
