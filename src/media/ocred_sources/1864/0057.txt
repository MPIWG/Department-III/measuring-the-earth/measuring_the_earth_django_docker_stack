nn

1. Bestimmung der Winkel, welche dem sphärischen Dreieck angehören,
dessen Seiten o’, 0" und o"" sind.

Zunächst sind nach den Gleichungen (9.) die sphärischen Längenunterschiede ®', @",

In m meer

@" zu suchen. Die Coefficienten erhalten wir wie folgt

 

 

 

l.a' = 6,8815832 — 10 l.b’ —=4,10445 — 10
l.a” = 6,6485581 — 10 l.b" = 3,87168 — 10
l.a'" = 7,0006383, — 10 l.bt" = 4,22397, — 10
1.20'.o' = 1,4082383: 1.20”. 0" = 1,2514846; 1.20'".0'" = 1,6786680
1.2.0 Lorso,. ' ROM.
ee 8,02717 — 10; ee 1,68789 — 10; a 8,35418, — 10
— 0,0106 — 0,0049 + 0,0226
+ 25,6025 + 17,8433 — 41,7164
: -- 25,5919 + 17,8384 — 41,6938
{ an’ —ı 6° 0’ Oo - hl _. 4° (0) on — zo! a. 10° [0% or
E a! = 6° 0} 25,5919; a! — 4° 01 11",8384; — a"! — — (10° 0’ 41',6938)
Das Minuszeichen von ®"' zeigt nur, dass, wenn ®' und ®" von Westen nach Osten positiv
zählen, dann @'"' von Osten nach Westen in entgegengesetzter Richtung zählt. Wir nehmen

and a AA

es daher zur Berechnung von Ok’, Oh’ und Ok" nach den Gleichungen (3.) ebenfalls positiv.
Hiernach findet man OL = w + a" — w"! = — 4!" 2655,

 

 

Mm _ zn! NZZ ol ' " m

tee aaa, EST = 400 19",9707; te a)

1.sin 9,0197895 sin 8, 8441853 sin9,2402139

1.OL0,62976 HUN 0. ee 0,6297663

. l.cosw'.cosu'' — 9,5498098 1. cosw" cosu'" — 9,5684857 cosu’ cosu'" — 9,5991635

= cpll.N’ = 2,1089255 epll.N'" = 2,1089495 cpll.N'" — 2,1089837

1,3082911, 1,1513868,, 1,5781274

oh! = — 20,3372 Oh! = —14",1706 Ok" = + 37,8554

In Ok" ist OL—= o"" — w' — wo" positiv.
Werden diese Verbesserungen den Winkeln n’, n”, n'" hinzugefügt, so erhält man die
gesuchten sphärischen Winkel, mit denen das Dreieck wie folgt berechnet werden kann.

 

 

 

n' = 44° 6'36”,296 cplsin 0,1573663,4 0,1573663,4
n” — 56° 17 0,408 1.sin o' — 8,9107618,8 8,9107618,8
nt" —= 80° 5148" 712 l.sinn” — 9,9186599,8 l.sinn’'" — 9,9934803,2

13" 25” 416 1. sin 0" — 8,9867882,0 1.sin o’" — 9,0616085,4

General-Bericht f. 1864. 8

 

 
