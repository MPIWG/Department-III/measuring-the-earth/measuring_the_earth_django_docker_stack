EIER

Tr in E

Ten. IT |

Im an!

ma RL

en a a y

u

Die in dem Organisations- Vorschlage gewünschte Ueberweisung von Messinstrumenten
aus den Beständen des grossen Generalstabes würde ohne Weiteres erfolgen.

Durch diese Erklärung des Königlichen Kriegsministeriums erscheint die Organisation
des Oentralbureaus sicher gestellt und wenn dasselbe auch in diesem Jahre sich auf die noth-
wendige Geschäftsführung beschränken muss, so wird es doch im nächsten Jahre seine volle
Thätigkeit beginnen können. | |

Berlin im Februar 1865.

14.8 Rivssstrkha nd.

General-Lieutenant von Blaramberg Exe. theilt in einem Schreiben vom 7. Februar
1865 mit, dass nächstens der XXVI. Theil der Memoiren des Kriegs-Karten-Depots erscheinen
und die Uebersendung desselben erfolgen werde.

15. Sachsen. “Komereich)

Bericht über die im Jahre 1864 ausgeführten geodätischen Arbeiten im Königreich
Sachsen für die Mitteleuropäische Gradmessung.

Das in dem Generalbericht über die Mitteleuropäische Gradmessung für das Jahr 1863
aufgestellte Dreiecksnetz über das Königreich Sachsen konnte insofern noch nicht als ein
definitives gelten, als darin nicht allein einige Sichten als vorhanden angenommen waren,
über deren wirkliches Vorhandensein noch nicht die völlige Ueberzeugung erlangt war, son-
dern auch — um der Königlichen Staatsregierung wegen Praesumirung der Kosten etwas
Abgeschlossenes vorlegen zu können — darin einige Punkte aufgenommen werden mussten,
die später bei der definitiven Feststellung voraussichtlich eine Abänderung zu erleiden hatten.

In ersterer Beziehung hat sich durch die speciellere Untersuchung im Jahre 1864
herausgestellt, dass die Visur zwischen dem Keulenberge und Gross-Radisch unmöglich ist.
Achnliches steht zwischen einigen anderen Punkten auch noch zu erwarten, so dass die bei-
den im vorjährigen Berichte erwähnten Netze erster und zweiter Ordnung nun zu einem
Netz erster Ordnung verschmolzen werden müssen, und die daselbst mit dritter Ordnung
bezeichneten Dreiecke von jetzt an als solche zweiter Ordnung rangiren.

In Betreff der definitiven Feststellung der Netzpunkte ist man hauptsächlich bemüht
gewesen, die Verbindung des südlichsten Punktes „Kapellenberg“ mit „Leipzig“ durch Drei-
ecke herzustellen, deren westliche Eckpunkte zugleich zum Anschluss an die Thüringischen
und Preussischen Dreiecke benutzt werden können. Leider haben diese Untersuchungen im
Laufe des letzten Sommers, der so ausserordentlich wenig Fernsicht gewährte, nicht abge-
schlossen werden können. Jedoch ist man wenigstens vorläufig dahin gelangt, an die Stelle
des früher im Fürstentbum Reuss angenommenen Punktes „Sorge“ den „Kühberg bei Netzsch-
kau“ treten zu lassen, der wiederum eine Aenderung des Punktes „Spitzberg bei Grasslitz
in Böhmen“ nach sich gezogen hat, indem an dessen Stelle der in der Nähe desselben be-

 

    
