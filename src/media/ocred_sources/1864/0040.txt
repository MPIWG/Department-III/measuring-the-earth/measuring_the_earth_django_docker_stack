 

0...

versehen. Diese Inschrift lautet für Dreieckspunkte 1. Ordnung z. B. „Station Keulenberg
der Mitteleuropäischen Gradmessung. K. Sachsen. 1864,“ der später, wenn die Messung
vollendet ist und die Resultate feststehen, noch die geographische Länge und Breite so wie
die Meereshöhe des Punktes hinzugefügt werden soll; und für Dreiecke 2. Ordnung z.B.
„Station Katzenstein der Kön. Sächs. Triangulirung. 18364.

11. Die höheren Pfeiler werden mit hölzernen Standgerüsten dergestalt versehen
dass sie mit den erstern ausser jeder Berührung stehen. Für Pfeiler von geringerer Höhe,
wird ein solches Standgerüst vorübergehend durch aufgestellte Böcke mit darauf gelegtem
Fussboden gebildet.

12. Für jeden Pfeiler wird ein Grundriss angefertigt, welcher die umliegenden Ter-
raingegenstände, insbesondere aber auch die vier Seitenfestlegungssteine enthält.

13. Ueber jeden Pfeilerbau wird ein besonderes Actenstück geführt, welches die
Verhandlungen mit den betreffenden Grundbesitzern und Bauunternehmern, das Protocoll
über den Bau und die Festlegung, die genaue während des Baues aufgenommene Zeichnung
mit eingeschriebenen Dimensionen, den sub 12. erwähnten Grundriss, sowie die Zusammen-
stellung der endlichen Kosten enthält.
| Die vorstehends aufgeführten Grundsätze finden zum Theil ihre Erläuterung durch
die beigefügte Zeichnung der vier Pfeiler für „Keulenberg“, „Fichtelberg“, „Stelzen“ und
„Aschberg“, welche den verschiedenen Höhen entsprechen, für welche etwa sämmtliche Pfeiler
auszuführen sind.

In Bezug auf den fixirten Punkt „Stelzen“ ist noch zu bemerken, dass der betreffende
Pfeiler an die Stelle des früheren excentr. Festlegungssteins für das Signal „Stelzenbaum“
der in den Jahren 1851 bis 1855 von der trigonometrischen Abtheilung des K. Preussischen
Generalstabes ausgeführten Triangulation von Thüringen gesetzt worden ist. Da diese Trian-
gulation in einem besonderen Werke*) veröffentlicht ist, dürfte es nothwendig sein, anzu-
führen, dass die durch die beiden Messingeylinder des neuen Pfeilers fixirte Vertikale genau
durch das Kreuz des alten Festlegungssteins geht, sowie, dass das Kreuz des oberen Cylin-
ders 0,3878 Toisen über und das Kreuz des unteren Oylinders 0,8542 Toisen unter der
oberen Fläche des alten Festlegungssteins liegt. Da nun nach S. 96, 121 und 182 des an-
geführten Werkes die Höhe der Oberfläche des Festlegungssteins zu 313,407 Toisen über
der Ostsee angegeben ist, so liegen die Oberflächen der beiden Festlegungscylinder des neuen
Pfeilers beziehendlich 313,795 und 312,553 Toisen über der Ostsee.

Was die Erwerbung des Grund und Bodens für jeden Pfeiler anbelangt, so
finden hauptsächlich zwei Modalitäten statt: entweder wird derselbe in der Grösse von einigen
OD Ruthen um den Pfeiler für den Staatsfiscus angekauft, oder es wird gegen Gewährung
einer Entschädigung eine Dienstbarkeit auf eine solche Fläche dergestalt gelegt, dass der
Besitzer sich verpflichtet dieselbe nur als Grasland zu nutzen und den mit den Messungen

 

*) Die Triangulation von Thüringen. Ausgeführt in den Jahren 1851 bis 1855 von der trigonometr, Abtheil.
des K. Preuss. Generalstabes. Berlin, 1859.

 
  

0 han Bl

Yraalıı

ru

al

|

han sh ds, dh a Han

1 sun Aliaraärnen nina SA

 
