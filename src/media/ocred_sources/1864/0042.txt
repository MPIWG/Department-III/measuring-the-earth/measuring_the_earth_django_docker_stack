 

NUR

war. Die specielle Durchführung der Arbeit, die Discussion der Beobachtungen ist enthalten
in der Schrift: „Bestimmung der Längendifferenz zwischen den Sternwarten zu Berlin und
Leipzig, auf telegraphischem Wege ausgeführt im April 1864 von Prof. ©. Bruhns und Prof.
Förster. Leipzig 1865.“

Es war beabsichtigt den bei den preussischen, österreichischen und sächsischen Ver-
messungen gemeinschaftlichen Dreieckspunkt zu Jauernick bei Görlitz in Länge auf telegra-
phischem Wege zu verbinden und die Breite und das Azimuth irgend einer der vielen von
dem Punkte ausgehenden Richtungen zu bestimmen; durch die Ungunst des Wetters einen-
theils, durch die im August nicht mögliche Herstellung der Telegraphenleitung anderntheils
musste die Bestimmung verschoben werden. Es wurde daher ein anderer Punkt noch aus-
gewählt und dazu der mathematische Salon in Dresden gewählt. Die Telegraphenleitung war
schnell hergestellt, die Längen- und Breitenbestimmungen konnten am 18. September beginnen
und bis Ende October wurden an 6 in Dresden und Leipzig gleichzeitig heitern Abenden
dieselben Sterne zur Zeitbestimmung beobachtet und die Uhren durch Signale und Coinci-
denzen mit einander verglichen. Die Beobachter hatten gewechselt und während Dr. Engel-
mann in Leipzig, beobachtete ich in Dresden und umgekehrt. Die sogenannte Registrirme-
thode sollte auch angewandt werden: durch Störung in den Beobachtungen, indem es an
mehreren Abenden während des Registrirens trübe wurde, sind die Beobachtungen aber zu
unvollständig geblieben und ihr Gewicht gegen die der andern Methode zu gering. Es hat
sich bei der Coincidenz-Signalmethode wieder gezeigt, dass beide Methoden der Uhrverglei-
chung dasselbe geben; die Differenz

05,03,
welche ich erhalten, liegt innerhalb der Fehlergrenze.
Das Resultat der Längenbestimmung Leipzig-Dresden ergiebt die Längendifferenz
52 22,002509,03.

Die Breite wurde an 12 verschiedenen Tagen durch Zenithdistanzen der Polarsterne
« Urs. min. und d Urs. min. und y Aquilae, @ Aquilae, « Arietis bestimmt. Die nach Süden
beobachteten Sterne haben nahe dieselbe Zenithdistanz als die nach Norden hin und im
Ganzen sind mehr als 300 einzelne Zenithdistanzen gemessen. Die andere Methode der
Breitenbestimmung durch Beobachtungen von Sternen im ersten Vertical konnte wegen des
ungünstigen Wetters nicht ausgeführt werden. Das Resultat aus den beobachteten Sternen
ist, wenn man die Deklinationen aus dem Berliner Jahrbuch entnimmt,

pa 1, rt

‘Die Publikation der Längen- und Breitenbestimmungen, ausgeführt im Jahre 1863 und
1864 in Freiberg, Dablitz bei Prag und Dresden erfolgt noch im Sommer d. J.

Entsprechend den Berathungen der preussischen, österreichischen und sächsischen
Commissare im April 1862 in Berlin und den Beschlüssen der Gradmessungsconferenz im
October v. J. habe ich bei dem Königl. Sächsischen Finanzministerium beantragt, für Sachsen
noch die Punkte |

 

i
x
ü

1 I un had da Ah a mn

|
|

 
