Ku TR Tree HT

au

IR

a m

je

Bl

Ba I ne

m

Grenze; endlich in Sehblesien, welche sich jedoch nur darauf beschränkte, das Polygon
„hohe Heide* festzustellen. Das Resultat aller dieser Recognoscirungen war die Wahl fol-.
gender Punkte, als:

Im Südwesten von Böhmen, dann Mähren und Oesterreich: Blaschkow,
Spitzberg, Markstein, Suchahora, Manhartsberg, Predigtstuhl und den astronomischen Punkt
Weternik bei Lischau. Die Wahl des letzteren machte es möglich, die Thurmpunkte Wit-
tingau und Wessely aus dem Hauptnetz hinauszuwerfen.

Im Westen von Böhmen wurde statt des Trzemein der Tock gewählt, Dobrava
und Böhmerwald beibehalten. Wegen einer zu erwartenden Anbindung an das baierische
Netz auf Kubany, Volini-Vreh recognoseirt und die Sicht von beiden nach Arber constatirt.
Auf den Punkten Böhmerwald und Dobrava wird gegenwärtig, während des Signalbaues, die
weitere Verbindung mit dem baierischen Netze, so wie auf Cebon und Glatre jene mit
Sachsen nach dem neu gewählten Punkte Aschberg erforscht.

In Schlesien ist mit den gewählten Punkten Habich und Hurky das Polygon „hohe
Heide* sehr zweckmässig abgeschlossen. Ä
Signale wurden auf folgenden Punkten gebaut: Blaschkow (gem. 'Stein-
pfeiler), Spitzberg (Pyr. mit 4° hohen Inst. Stand), Markstein (3° hoher gemauerter Pfeiler),
Weternik (gem. Pfeiler), Tock (Pyr. mit 44° hohen Stand), Bernstein (94 Schuh hoher gem.
Pfeiler), Cebon (fehlen die Angaben), Dobrava und Böhmerwald wird jetzt gebaut. Auf dem
Ieschken wurde die zerstört gewesene Pyramide heuer wieder errichtet und auf Sbalava ein
neuer 5° hoher Inustrumentenstand eingesetzt.

Beobachtungen: Auf den Punkten Hornberg, Passeky, Sbalava, Melechau, Svidnik,
Mezy Wratach, Kameigh, Rossberg, Studeny-Vreh, Donnersberg und Bösig, dann die im
vorigen Jahre unvollendeten Messungen auf Pötzney und Dablitz heuer beendet.

Auf Bischofskoppe, hohe Heide und Bradlstein sind die westlichen Richtungen beob-
achtet, um das Polygon Schneeberg abschliessen zu können.

Auf Zban und Brno wird heuer noch beobachtet, ebenso auf Blaschkow.

Nachdem im Sommer 1862 und 1863 die gleichzeitigen Zenithdistanzmessungen zum
grössten Theile mit Instrumenten ohne Mikroskop-Ablesung gemacht wurden, was den öster-
reichischen Angaben, gegenüber den sächsischen und preussischen, einen minderen Werth
geben würde, so wurden diesen Sommer in der Richtung nach Wien von der Schneekoppe
an neuerdings gleichzeitige Zenithdistanzmessungen mit mikroskopischen Ablesungen und zwar
auf den kürzeren Linien, nämlich über Sviein, Augezd, Kunietickahora, Sbalava ausgeführt,
und werden dieselben noch heuer über Blaschkow nach Spitzberg fortgesetzt. Diese Mes-
sungen sind ein Theil des Nivellements, welches zwischen der Ostsee und dem adriatischen
Meere durchgeführt werden wird.

Ferner wurden gleichzeitige Zenithdistanzen gemessen vom Jeschken nach Jauer-
nik, vom Vellis nach Schneekoppe und Kunieticka, von Visoka nach Kunieticka und Me-

lechau und von letzterem nach Pötzney, Svidnik und Mezy-Wratach, von Kameigh nach
General-Bericht f. 1864. 4

 

 
