un

ETHERNET

WulicaRl

ü
i

 

145

Aus dieser Zusammenstellung geht hervor, dass auf der Ostseite der Insel
das Maximum der Bewölkung und das Minimum der Zahl der klaren Nächte auf den
Sommer, dagegen das Minimum der Bewölkung und das Maximum der Zahl der
klaren Nächte auf den Winter fällt. Indess gestalten sich die Verhältnisse im Norden
eher günstiger als im Süden,”) so dass südlich vom Parallel + 38° auf ca. ıoo,
nördlich auf ca. ı20o klare Nächte im Jahre gerechnet werden kann. Im Jahres-
durchschnitt stellt sich der Bewölkungsgrad annähernd gleich demjenigen in Berlin.

Dagegen kommen auf der Station Aomori am Nordende der Insel die un-
eünstigen Einflüsse der Westseite schon derartig zur Geltung, dass der Bewölkungs-
grad im Winter bereits denjenigen des Sommers übertrifft und dass in dieser Jahres-
zeit nur auf ca. ıo klare Nächte zu rechnen ist.

Noch ungünstiger aber gestalten sich die Verhältnisse auf der Westseite be-
sonders in der Nähe der Meeresküste. In Akita und Niigata übersteigt der mittlere
Bewölkungsgrad im Winter die Zahl 8; im Durchschnitt sind in dieser Jahreszeit
nur 5 klare Nächte zu erwarten.

Dieser Witterungscharaeter der verschiedenen Jahreszeiten kommt auch in
den Reeenmenegeen deutlich zum Ausdruck. Diese betragen für die einzelnen

Stationen:

Regenmenge
Ostseite. -  Nordende. ‚Westseite.
Berlin
Tokio Choshi Fukush. Nob.-Ish. Miyako Aomori Akita Niigata Jamag.
Frühling 402 mm 465 mm 252mm 245mm 277 mm 137 mm 347 mm 28o mm 21Ionmm 152 mm
Sommer 385 319 497 347 460 308 453 398 464 202
Herbst 485 479 373 301 399 416 509 502 298 134
We u. a 334 ee a
Jahr 1405 1457 1281 1005 1300 24, 7727 1714 1205 396

und stellen sich daher gegenüber der Regenmenge in Berlin auf den doppelten bis
dreifachen Betrag.

Vom meteorologischen Standpunkte aus bieten daher alle Stationen auf der
Ostseite hinreichend günstige Bedingungen; am vortheilhaftesten aber gestalten sich
dieselben wohl für Masuda und Midsusawa.

2. Italien.

Während sich nach dem Obigen die meteorologischen Verhältnisse im Japan
als befriedigend erweisen und einigermaassen denen in Berlin gleichen, stellen sich
dieselben in Italien ausserordentlich viel günstiger.

Gleichwie für Japan liegen auch für Italien umfassende Beobachtungsreihen
über den Bewölkungsgrad vor, welche in den „Annali dell! Ufficio centrale meteoro-
*) Die etwas abweichenden Zahlen von Fukushima sind zum Theil darauf zurückzuführen,

dass die Beobachtungen an diesem Orte nur 2°, Jahr umfassen,
RER
92

 
