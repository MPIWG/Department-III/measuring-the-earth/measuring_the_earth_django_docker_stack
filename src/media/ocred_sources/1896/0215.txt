 

FI RR DURING RL Im nen

ner |

209

werthe und ordnet dieselben nach der Temperatur des photographischen Instruments, so er-
hält man:

Bi Mittel. 0 & (020004)
ee,
18960 Bebtanı ir a

a ul
a ze ee
1805 Nov. a0 ar |
1800 a 2 a 5 31 240) —30 | +1
WER |

Gesammtmittel = 1572260

 

Aus den in vierter Columne stehenden Mittelwerthen erkennt man, dass eine ge-
setzmässige Abhängigkeit des Scalenwerthes von der Temperatur innerhalb der Grenzen’
— 9° und — 6° kaum vorhanden ist.

Rechnerisch hätte sich die Brennweite für At gleich — 10° C. um etwa 0.13 mm.
verkürzen können und der Winkelwerth für 1* der kleinen Schraube — 0.1 mm. hätte um
etwa 0.0014 grösser werden sollen. Hiervon ist in den obigen Zahlen, deren Verlauf im
Ganzen ein systematischer überhaupt nicht genannt werden kann, nichts zu bemerken ;
vielmehr entsprechen zufällig gerade den höchsten Temperaturen zu Anfang der Reihe auch
die grössten Winkelwerthe der Schraubenumdrehung.

Werden die einzelnen Abendmittel der Scalenwerthe nunmehr nach der Reihenfolge
der Platten und Cassetten geordnet (vergl. nachfolgende Tafel), so lässt, der Einfluss sich un-
tersuchen, den die mil jedem Tage wechselnden Plattenschichten und vor allen Dingen auch
die verschiedenen, Lagerung und Focalabstand der Platten bestimmenden Cassetten etwa aus-
geübt haben könnten.

Die Abweichungen vom Mittelwerth unter den einzelnen Werthen nachstehender Ta-
fel halten sich nach Platten und ganz besonders nach Cassetten geordnet, in so kleinen
Grenzen, dass die Scalenwerthe als frei von systematischen Fehlern der genannten Art an-
gesehen werden dürfen. Es verdient vielleicht noch Erwähnung, dass an den beiden Aben-
den mit den relativ grössten Abweichungen, in nachstehender Tafel durch die Zeichen: :
markırt, die Qualität der photographischen Bilder als besonders schlecht mit 3.4 notirt wor-
den ist.

 
