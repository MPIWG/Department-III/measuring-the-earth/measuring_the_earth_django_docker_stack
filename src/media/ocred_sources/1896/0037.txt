1. TERN RTTTEAHEFSERTÄETTEN "SON

51

internationale, que les Btats-Unis ont ’intention d’adherer A la nouvelle Convention de l’Asso-
ciation ge&odesique internationale, a la condition toutefois que celte adhösion ne döploiera
ses effets que lorsque le Gongr&s aura vol& les erödits necessaires pour le payement de la
part contributive incombant au Gouvernement des Etats-Unis. Le Congres sera nanti, dans
sa prochaine session, de la demande de ces credits.

Le soussign& saisit cette occasion pour renouveler ä M. le professeur Hirsch l’assu-
rance de sa consideration la plus distingude.

(Signe) Edwin-F. Uur.

Le Secr6taire donne ensuite connaissance d’une lettre de M. Celoria, datee du 17
octobre, qui fait entrevoir que l’Italie sera reprösentee & la Conference de Lausanne par
M. le general Ferrero ei que si ce dernier &tait empeche, la Commission geodesique italienne
enverrait en tout cas son rapport ä teınps.

En effet, l’envoi de ce rapport a &t@ annonce par une lettre de M. Guarducci, secre-
taıre de la Commission italienne A Florence ; le bureau va le faire distribuer aux membres de
l’Assemblee.

M. le President donne la parole aM. Foerster pour lire le rapport de la Commission
des finances. Voici le texte de ce document:

Rapport de la Commission des Finances.

La Commission a examin& les comptes de M. le Directeur du Bureau central/pour
’exercice de 1895. Elle a trouve les depenses en regle et justifiees par des pieces & l’appui.

La Commission a pris connaissance des receiles et de l’Etat des fonds disponibles,
soit ä la fin de l’exercice de 1895, soit & l’eEpoque actuelle.

Elle propose donc d’approuver les comptes du Bureau central de l’Association pour
l’exercice de 1895 et de donner decharge pleine et entiere ä M. le Directeur du Bureau cen-
(ral pour sa gestion.

Quant & la pr&vision des döpenses pour l’annde 1897, a basersur les recettes accordees
parla nouvelle Convention, cen’est plus la Commission permanente, mais le bureau de l’Asso-
ciation qui aura le soin d’y pourvoir, aussitöt que possible, apres que les pouvoirs de la Com-
mission permanente auront pris fin. Mais il incombera encore ä la Commission permanente
le soin de soumettre & la fin de l’annde, ä tous les Gouvernements qui ont adher& A la Con-
vention de 1886, non seulement un rapport financier sur la p£eriode entiere de 1887 & 1896,
mais en möme temps aussi une explication justificative concernant l’utilisation scientifique
de l’actif qui, & la fin de la dite p&riode decennale, c’est-a-dire au 51 decembre 1896, sera
rest& disponible pour les buts de l’Association.

Get actif sera alors mis & la disposition du bureau de l’Associalion pour ötre admi-

 
