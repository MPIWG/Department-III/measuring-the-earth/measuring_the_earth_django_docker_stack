1 eilt

VE

Ihn

INN ARI

gen

LT RRIEN TURN

345

Pour un pendule de 150 de longueur, par exemple, avec un microscope reduit &
une simple loupe grossissant 4 fois, conditions admissibles A la rigueur, l’erreur probable ä
craindre serait ä peu pres 8" ä& Im, soit environ 1”.

En admettant, pour un appareil ä transporter sur le lerrain, un pendule court de
0"35 seulement de longueur, avec une lentille concentrant une forte lumiere sur index, et
un oculaire grossissant 10 fois, !’erreur probable A craindre serait encore de 10P A Am, oR
de2?.

3° Nivelle & bulle d’air.

Avec une fiole ä bulle d’air, d’apres des experiences tres soigndes, ex&culdes par
le Dr Reinhertz !, on aurait A craindre dans la position de la bulle, en raison de sa « paresse »,
une erreur lindaire probable :

p= "0066 V 0,
oe exprimant en mötres le rayon de courbure de la fiole.

A celte erreur s’en ajoute une aulre, constante et dgale A + 0""033 environ, due ä
Fincertitude de "’appreciation de la position de la bulle entre ses repöres 2.

L’erreur lineaire probable rösultante serait ainsi :

 

 

P = 2 OSB + OmmoOG = ar ones HR,

et ’erreur angulaire probable eorrespondante :

em r v P
Kan Non

A
O 0
\ \

 

 

 

Avec une nivelle de 200 metres de rayon, admissible pour un instrument fixe d’obser-
valoıre, Perreur probable A craindre serait ainsi de 05 A 4m, soit de 04.

Pour des nivelles de 50m de rayon, comme celles des niveaux employees pour le
reseau fondamental du Nivellement general de la France, l’erreur en question deviendrait A
peu pres 1 A Am, ou 0'2.

IV. — RESUME ET CONCLUSIONS

Le tableau ci-apres r&sume, pour les diverses hypolheses faites, les erreurs angu-
laires probables ä craindre dans le calage de l’axe vertical de P’instrument.

! Zeitschrift für Instrumentenkunde, 9° et A0e livraisons de 1890.

: L’eil, en effet, n’apprecie guere qu’ä 0""2 pres ’equidistance des extremites de la bulle aux traits
correspondants de repere, grav6s sur la fiole; d’oü resulte une incerlitude maxima de 0W" 1 dans la position de
la bulle et une erreur probable correspondante trois fois moindre, soil + 0"Mg33,

ASSOCIATION GEODESIQUE — 39

 
