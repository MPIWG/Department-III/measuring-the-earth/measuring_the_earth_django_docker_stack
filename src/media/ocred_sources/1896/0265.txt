N TA 79 "TÜR nn IN |

ILL M

wer ırmır

ann a hin ı

Iran |

Ge dernier resultat etait facile A prevoir. En effet, la temperature, «dans ce cas, variant,
entre la mire d’avant et le niveau, exactement comme entre ce dernier et la mire d’arriere,
les erreurs partielles de r&fraction sur les deux lectures d’arriere et d’avant sont 6gales et,
par suite, s’annulent dans la difference !.

11. Abaque de lerreur de refraclion. — Si, dans la formule (19), on remplace p
par son äquivalent :

et les logaritlimes neperiens par des logarithmes ordinaires, il vient :

m a log (# —. 92)

 

 

at a bin 2 |
) et gan ). )g eh 3 4 a
(21) 1 ige enge RE 23 |
08 ———
0)

L’abaque? (figure 3) donne, sans calculs, la valeur de E quand on connait celles de
B, 9, L, D, lo REIHE L, et a to:

La mesure, assez delicate, des differences l, — t, et i, — L,, pourrait, comme la
propose M. Fearnley, s’effectuer A l’aide de thermometres differentiels tres sensibles.

! Ce resultat pouvait egalement se deduire de l’equation (13).
Quand on y fait:
L; m L:
elle donne, en effet :
E

|

2 Get abaque est etabli d’apres un procede nouveau, dont nous avons expose le principe dans une note
intitulee: Sur une nouvelle methode generale de calcul graphique au moyen des abaques hewagonaux
(Comptes rendus de l’Acad@mie des sciences de Paris, 5 avril 1886). Il presente cette particularit qu’on y a
fait disparaitre, par un artifice auquel nous avons donn& le nom d’elimination graphique, la variable auxiliaire d.

La theorie et le mode general de construction de ces abaques sont decerits dans un ouvrage, actuel-
lement en preparation.

 
