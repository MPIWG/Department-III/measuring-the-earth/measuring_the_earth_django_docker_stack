TEEN

COMPTES BENBUS

DES SEANCES

N - DE LA COMMISSION PERMANENTE

WASSOCIATION GEODESIQUE INTERNATIONALE

REUNIE A LAUSANNE DU 15 AU 21 OCTOBRE 1896

Rediges par le Secretaire perpetuel
A. HIRSCH,

Suivis des Rapports sur les travaux geodesiques accomplis dans les differents pays
pendant la derniere annöe.

AVECG TREIZE CARTES ET PLANCHES

 

Gar

ä
VERHANDLUNGEN

DER vVoM 15. Bıs 21. OCTOBER 1896 IN LAUSANNE ABGEHALTENEN

GONFERENZ DER PERMANENTEN COMMISSION

DER

INTERNATIONALEN ERDMESSUNG

Redigirt vom ständigen Secretär

A. HIRSCH,

Zugleich mit den Berichten über die Fortschritte der Erdmessung in den einzelnen Ländern
während des letzten Jahres.
ALL
MIT DREIZEHN LITHOGRAPHISCHEN TAFELN UND KARTEN /
ee RI IE nn —

ee Sal, 1897

2 VERLAG VON GEORG REIMER IN BERLIN
RE
>

 

mr IMPRIME PAR ATTINGER FRERES, A NEUCHATEL

=)

 

 
