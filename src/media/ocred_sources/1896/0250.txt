 

‘
%
i
v
fi

a RE

34%

bable de P’erreur & craindre de ce chef serait & peu pres la moili6 de ce maximum, soit
0""035 environ.

Si Fon Egale cette valeur A l’expression pr&cedente, on en tire, pour l’inclinaison
probable correspondante de l’axe optique de la lunette,

2 02030
ZIRS<G.

Avec une lunelte fixe d’observatoire, pour laquelle on aurait, par exemple,
Re in, Me
ven rarement depasses dans la pratique, l’erreur probable correspondante serait d’environ
"A A mötre, soit 08.

Avee des lunettes comme celles employees au Nivellement general de la France,
mesurant 035 de distance focale, l’erreur probable pour un grossissement 3 de l’oculaire,
serait de A6# A Im, soit & peu pres 3”, en supposant toutefois absolument immobile la sur-
face du bain, condition d’ailleurs impossible ä realiser sur le terrain.

2° Pendule avec index repere au microscope.

Soient :
L, la longueur du pendule;
G, le grossissement du mieroscope.
Vu ä travers celui-ci, l’Ecart effectif entre l’index et le trait fixe de repere, supposes
en coincidence, est amplifie dans le rapport de 1&G
D’apres ce que nous avons dit plus haut, cet cart a pour valeur apparente maxima
0""07, pour valeur probable apparente 0""017 et, par suite, pour valeur probable reelle,

0 > 0 3 5
ee .

L’erreur angulaire probable correspondante de calage est

02035
el

prineipale du cristallin &tant, d’a peu pres 0'045, deux points places a 0"'25 de l’eil, distance normale de la
vision distincte, pourraient former leurs images sur le möme bätonnet de la retine, et, par suite, paraitraient
confondus en un seul, si leur ecartement etait inferieur a

0Mas
0045

E — 0967.

 

;
3
3
3
ä
3
;
i
3
3
i
ä
1

are

rn nn a a nen

 
