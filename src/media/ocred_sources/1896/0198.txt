 

 

re

EEE

re

Se ee E5 Fe
RER EEE

ee

 

ET REIT

EZ

BEER

er

 

 

 

192
Es wird also: re |
Rs wird als ir V 63514 — + 0126,
un 109—13

2

Die Mittelung der für die beiden Beobachter getrennt erhaltenen Einzelpolhöhen
hat also nach Ausweis der & für die einzelnen Sternpaare nur in wenigen Fällen eine
Erhöhung der Genauigkeit herbeigeführt; im Durchschnitt ist die Genauigkeits-
vermehrung gänzlich belanglos. Es zeigt sich also, dass die zufälligen Ausmessungs-
fehler gegenüber den systematischen Fehlern wesentlich zurücktreten,

Il. Vergleichung der photographischen Resultate mit den gleichzeitig am
visuellen Zenitteleskop erhaltenen.

In der Zeit von April 24. bis Juni ı5. wurden von den sich gegenseitig ab-
lösenden Beobachtern Schnauper und Hecxer an dem visuellen Zenitteleskop von 68 mm
Objektivöffnung folgende Polhöhen erhalten, welche wegen Polhöhenschwankung um
dieselben Beträge korrigirt worden sind, wie die entsprechenden photographischen.

 

4
3
i
3
3
3
i

er

Au hah

habe
