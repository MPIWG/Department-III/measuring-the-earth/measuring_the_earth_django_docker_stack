 

304
page 4 du Rapport sur les travauz exccutes par la Commission geodesique italienne en 1895,
presente ä la Conference generale de Berlin. — On peut done &viter ici des r&petitions inutiles
en se bornant & rendre compte des calculs et des operations nouvellement ex&cutees :

a) Difference de longitude Naples-Milan (1888).

Les calculs relatifs & cette operation, ex&cutce au moyen de deux instruments de
passages portatifs Repsold, apparlenant ä la Commission, sont termines dans les deux stations
el donnent le r&sultat suivant :

Longitude de Milan (Centre Obs.) — Long. de Naples (Centre Obs.) = 20"15°522 + 0008 (err. pr.).

C'est le resultat de seize soirdes d’observalion, separdes en deux series de huit soi-
r&es chacune par l’Echange des observateurs (prof. Fergola et Dr Rajna).

Pour chaque serie on trouve un accord salisfaisant, et !erreur probable du resultat
d’une soirde est comprise (les poids des differentes soirees n’elant pas les m&mes) entre
+ 0.031 et + 0.043.

Les calculs des observations de M. Fergola ä Naples et a Milan ont &te executes ä
Naples par M. le Dr Angelitti.

La Commission se reserve de revenir dans une autre occasion sur la comparaison
de la nouvelle valeur avec celle obtenue en 1875, ainsi que sur les resultats de la substitution
de cette nouvelle valeur dans les öquations de clöture des polygones astronomiques italiens.

b) Difference de longitude Milan-Crea (1896).

Gette determination, ex&culee dans les mois de juin, juillet et aoüt, a el& contrariee
par le temps exceptionnellement mauvais. Les observateurs &Elaient M. le prof. CGeloria et
M. le Dr Rajna.

En suivant l’exemple de Y’Institut geodesique prussien, el d’apres la proposition de
M. le prof. Schiaparelli, on a employe dans les observations le microme&tre enregistreur de
Repsold.

Les experiences preliminaires ex6cutdes avec ces micrometres, adaptes aux instru-
ments porlatifs de Repsold par les constructeurs m&mes, ont conduit A la necessite de modi-
fier le syst&me des conlacts pour les rendre plus sürs et, en m&me temps, pour faciliter
l’enregistrement et le releve des signaux sur nos chronographes & plumes. Ges modifications,
dont les details seront exposes dans une autre occasion, sont dues A M. Milani, mecanicien
a l’Observatoire de Brera, et ont &t& ex&cutees par lui-möme avec l’habilet@ consciencieuse
qui lui est habituelle.

Travauz execules a ’Observaloire de Naples sous la direchion de M. Fergola. —
(Voir Travaux executes a l’Observatoire de Brera, a). )

Travauz execules a V’Observaloire d’Arcetri (Florence). — M. Abetti a de nouveau
determine l’azimut de la mire meridienne de l’Öbservatoire d’Arcetri; au moyen de cetle
donn&e et de l’angle Meire Meridienne, M® Senario, observ& par l’Institut g&ographique mili-
taire, on oblient une nouvelle valeur de l’azimut de M® Senario a Arcetri, savoir :

21°36 34.91 + 045

    

eh ER hie Mails

bu

lab u luulun.

Ak lu rd

han)

isst, abummali Mahn aa

 
