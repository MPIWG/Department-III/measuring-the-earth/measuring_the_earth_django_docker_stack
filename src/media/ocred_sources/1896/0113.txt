rem

IT ERTTTTET 7”

107

Der Herr Präsident dankt den Delegirten und dem Secretär für ihre wichtigen Be-
richte, welche in sehr interessanter Weise über die in den verschiedenen Ländern gemachten
Fortschritte der Erdmessung Auskunft geben.

Der Herr Präsident spricht den Behörden der Eidgenossenschaft, des Kantons Waadt
sowie der Stadt und Universität Lausanne den lebhaften Dank der Permanenten Commission
für den liebenswürdigen und freundlichen Empfang aus, welchen dieselben der geodätischen
Gonferenz gewährt haben. Mit grosser Genugthuung hat der Präsident bei allen Rednern,
welche das Wort ergriffen haben, nicht nur ein lebhaftes Interesse für die Arbeiten der Erd-
messung bemerkt, sondern auch ein hervorragendes Verständniss der Bedeutung des von der
internationalen Vereinigung unternommenen Werkes.

Der Herr Präsident giebt ferner ein kurzes Bild der Thätigkeit der Permanenten Com-
mission, welche bald ihre Befugnisse an die neuen Organe der vom General Baeyer geerün-
deten Erdmessung übertragen wird.

Herr Bakhuyzen ist sicher, im Sinne der ganzen Versammlung zu handeln, indem
er dem Herrn Präsidenten für die Unparteilichkeit, mit welcher er, wie gewohnt, die Ver-
handlungen geleitet hat, und dem Herrn Secretär für die Gewissenhaftigkeit bei der Erfüllung
seiner mühevollen Thätigkeit den lebhaftesten Dank ausspricht.

Die Sitzung wird um 4 Uhr aufgehoben und die Conferenz von 1896 als geschlossen
erklärt.

 
