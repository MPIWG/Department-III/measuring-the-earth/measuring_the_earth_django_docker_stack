5 RTTTTERTA 7 TTNITeTR nn 41

ka

Wurm

Te geTTT

JRR

TIRaamn 1

Aus der Fehlerzusammenstellung $. 210 folgt, dass die grössten, den aus der Ueber-
einstimmung der Abendmittel abgeleiteten Normalwerth 00025 um das Doppelte übertref-
fenden Fehlerbeträge zu den kleineren Distanzen 1 1'0 und 11.4 gehören, bei welchen die
Sicherheit der Scalenwerthbestimmung naturgemäss am geringsten sein muss (die Distanz
96 kommt wegen zu geringer Messungszahl hier nicht in Frage).

Im Uebrigen können die wahrscheinlichen Fehler, selbst für das grösste Intervall
von 409, noch als innerhalb der durch den Normalfehler gegebenen Grenzen liegend be-
trachtet werden. Die Frage nach etwaigen Distorsionswirkungen der Plattenschichten kann
daher, wenigstens wass ihren Einfluss auf den zur Reduction benutzten Scalenwerth betrifft,
im negativen Sinne entschieden werden. Dass inder That merkliche Distorsionen der emplind-
lichen Schicht bei den vorliegenden Aufnahmen vorgekommen sind, wird im Folgenden durch
geeignete Discussion der unmittelbaren Plattenausmessungen ermittelt werden.

Die Frage, ob sämmtliche sechs Cassetten praktisch dieselbe Brennweite haben darf,
nach den obigen Zahlenwerthen definitiv bejaht werden.

Vereinigt man nunmehr zur Ableitung des Endresultats sämmtliche in der Tabelle
S. 208 enthaltenen Scalenwerthe und setzt die Gewichte proportional der Anzahl der Messun-
gen, so ergiebt sich der definitive Winkelwerth für !/,, mm. (= 1" der Sehraube am Mikro-
meter) zu

15.2255, (81) w. F. + 00003.

Die zur Bestimmung des Winkelwerthes benutzten Plejadenaufnahmen sind abwech-
selnd in beiden, um 180° verschiedenen Lagen des Fernrohrs ausgeführt worden. Ausserdem
variiren die gemessenen Sternstrich - Distanzen von der stets durch n Tauri bezeichneten
Mitte des Gesichtsfeldes nach Süden (5 Distanzen) zwischen 10’ und 41’, nach Norden (2 Dis-
tanzen) zwischen 41’ und 17’. Aus diesen Aufnahmen lässt sich daher über die Lage der
Plattenebene zur optischen Axe und, falls die benutzten Plejadensterne keine systematisch
wirkenden Fehler im Sinne Ad, zeigen, auch über die Bildverzeichnung des Objectivs Auf-
schluss erhalten.

Wenn die Plattenebene nicht senkrecht zur optischen Axe steht, müssen die in einer
Lage des Instruments bestimmten Winkelwerthe von den in entgegengesetzter Stellung herge-
leiteten um den Betrag von 2 i abweichen. Im Mittel aus den südlich von der Gesichtsfeld-
mitte (Stern n) liegenden, gleichmässig auf beide Kreislagen vertheilten Distanzen n — 34
n — 19 und n — 22 folgt aber

I

 — R— + 0.041 (pro, mm. und, Az, 176-7 mm), 2. — 21 Differenzen ;
w. Fö:+0:05
im Mittel aus den nördlichen Distanzen n — 32 und n — 24

Kr 0.016 (pro | mm und Az — 139 — 5.5 mm), n — 14 Differenzen ;
w. F. + 0'095.

Obwohl eine minimale Neigung angedeutet erscheint, ist ihr Betrag doch kaum als

 

 
