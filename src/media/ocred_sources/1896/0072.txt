 

 

66
Commission zur Kenntniss zu bringen, dass die französische Regierung die passenden Maassre-
geln ergreife, um dem Parlamente einen Gesetzt-Entwurf vorzulegen betref! des nöthigen
Gredites um die Theilnahme von Frankreich an der Erdmessung sicherzustellen. Diese Mit-
theilung ist in völliger Uebereinstiimmung mit der Bemerkung in der Depesche der Deutschen
hreichskanzlei, wonach die förmliche Beitrittserklärung Frankreichs in Aussicht steht.

«Schliesslich, um die Correspondenz über die Ratification der Uebereinkunft zu ver-
vollständigen, erwähnt der Secretär noch, dass die Gesandtschaft von Chile ihm, unter dem
17. Juli geschrieben hat, sie werde demselben die Ratification der Uebereinkunft mittheilen
sobald dieselbe ihr von Seiten ihrer Regierung zugekommen sei.

« Aus allen diesen Aktenstücken, welche wir soeben resumirt haben, geht hervor, dass
bis zum jetzigen Augenblick ! die Uebereinkunft vom 11. October 1895 von 14 Staaten ratifi-
ziert worden ist, und dass Frankreich seine Absicht kundgegeben hat, dem von der Berliner-
Gonferenz angenommenen Abmachungen beizutreten.

« Demgemäss sind also von den bisherigen 21 Erdmessungs-Stauten 14 der neuen Con-
ventson beigelreien, während folgende 6 Staaten : Oesterreich-Ungarn, Russland, Rumänien,
Serbien, Argentinien und Chile, ihre schliessliche Entscheidung dem Bureau noch nicht mit-
getheilt haben.

« Man würde sicherlich im Irrthum sein, wenn man aus der Verzögerung der Beitritis-
Erklärung den Schluss ziehen wollte, als ob diese Staaten die Absicht hätten sich von der
internalionalen Erdmessung zurückzuzichen; im Gegentheil ist es mehr als wahrscheinlich,
dass, wenn nicht die Gesammitheit, doch, wenigsten die grosse Mehrheit derselben ihren
definitiven Beitritt bis zum Ende dieses Jahres erklären werden.

«Wie dem auch sein mag, und obwohl im gegenwärtigen Augenblick erst 14 von den
21 Staaten beigetreten sind, so darf man nicht vergessen, dass die Erklärungen dieser Staaten,
wonach dieselben der neuen Erdmessung wie sie durch die Uebereinkunft vom 11. October
1895 begründet ist, angehören wollen, keineswegs von den eventuellen Entscheidungen der
übrigen zur Uebereinkunft von 1886 gehörigen Staaten abhängig sind, und dass man folglich
schon jetzt die internationale Erdmessung als für die Dauer von 10 Jahren erneuert betrach-
ten muss.

«Nur die finanziellen Bestimmungen der neuen Uebereinkunft, welche sich auf die

Dotation und die Jahresbeiträge beziehen, sind natürlich bis zu einem gewissen Grade von
der Anzahl der schliesslich beigetretenen Staaten abhängig.

« Das Bureau glaubt daher die Ansicht aussprechen zu dürfen, dass die permanente
Commission, um der Schluss-Resolution der Uebereinkunft von 1895 nachzukommen, be-
schliessen muss, sich für den 31. December 1896 aufzulösen und die Geschäfte dem neuen
Präsidium zu übergeben.

«Die Permanente Commission wird entscheiden, ob sie nochmals vor dem Ende des
laufenden Jahres zusammentreten will, zu dem Zwecke, je nach den alsdann vorliegenden
Umständen, die Finanzmaasregeln für das Jahr 1897 zu beschliessen, welches als eine Art

! Nämlich bis zum Erscheinen der Verhandlungen. A.H

aan
na
a
x.

ae TRAIL

ET lin eh

I eh

is mhk a ha
a — A nn

A

:
|
1

 
