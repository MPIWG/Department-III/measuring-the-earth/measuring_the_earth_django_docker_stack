TERRAIN "* ST N

Nas A

UNI AM

une warten

Ring

5. Parallel von Midsusawa. De 39 3
Combination F: Japan, Midsusawa 00002... 8 214%
Italien. Cotrone Claim). — Wı

Ostamerika, Dovr Doawae) 2. —. + 75.5
Westamerika, Ukiah (Oo.  . .,_. + 123.3

Px Py
Me 1.88 » — 1092 =o0.
? Py 9 en 95
N: = 65%0 und. 155.0 Pos — 1.98 Pym — 8.87 A =,0.09

Diese Combination erweist sich vom mathematischen Standpunkte aus als sehr
günstig, indess gestalten sich die seismischen Verhältnisse von Calabrien ungünstig.

Combination G: Japan, Mdusawa. se... 2... 2.20... Az —14ı%
Italien, GCacehan Samen. 2... — 9.0
Östamerika, Dover Delaware) ° .- ..... + 75:5
Westamerrka, Usah Caltomien,.  .. + 123.3

Pax Py
ir: y=1I. Fee
2 93 Dy 79 Dtm 23
N 33 09 und 12109 Dres — 208 Pin. > 1.71 d= 0.32

Diese Combination unterscheidet sich von der vorhergehenden dadurch, dass
die Station in Calabrien durch eine solche auf der Insel Sardinien ersetzt worden ist,
welche letztere vorzügliche seismische und meteorologische Verhältnisse darbietet. Die
mathematische Grundlage der Combination hat dadurch allerdings eine Herabminderung
erfahren, doch ist sie bei den sehr befriedigenden socialen, seismischen und meteoro-
logischen Verhältnissen aller 4 Stationen noch als hinreichend günstig zu betrachten.

Cömbmation Hz: Japan, Midsusawa . 1 11,
Ttahen, Cacları lm). — 9.0
Ostamerika, nördl. von Washington (Maryland) . + 77.1
Westamerika, Thal des Sacramento (Californ.) . + 122.0

Aa Day
we 20 m, — so] mu, = 0.193 |
N = 35°%6 und 1256 PieMas, — 2.00 Dam 1.78 AN = 0.25

Der Ersatz der ost- und westamerikanischen Stationen durch ca. 120 Kilometer
weiter landeinwärts gelegene Orte hat neben einer weiteren Besserung der meteoro-
logischen Verhältnisse der westamerikanischen Station eine günstigere Gestaltung der
mathematischen Grundlage herbeigeführt.

 
