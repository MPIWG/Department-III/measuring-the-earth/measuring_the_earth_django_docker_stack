N RT 7 "Tree

wu arl

u en

“|

Ring

TIRATaEN |

281

pour les 47 de ces triangles ou l’on a employ& exclusivement des heliotropes comme point
de mire.

Pour tous les triangles, au nombre de 73, pris ensemble, on trouve comme erreur
moyenne d’un angle compense sur la station

+ 096

En partant des erreurs moyennes pour l’unite de poids d’apres la compensation des
stations, on lrouve avec le poids 12 comme erreur moyenne d’un angle compense sur la
station :

pour les 12 dernieres stalions

ai 0A
contre

+) 0A

pour les 46 stations mesurees avant 1892, et de
AU
+05
pour toutes les stalions ensemble, au nombre de 58.

De ces dernieres valeurs il resulterait que les observations pendant les dernieres
annees auralent et& faites avec une plus grande preeision que les observalions anlerieures,
tandıs que des erreurs de clöture des triangles il resulterait tout le contraire.

Cette contradiction s’explique pour une partie par les deplacements que peuvent
avoir subi un cerlain nombre des stations, par suite du tremblement de terre du 17 mai 1892,
lorsque les observations des stalions : @. Sandaran Galah, T. St Dohar Dohar et D. Maleja
etaient deja terminees. Ges deplacements n’ont pas d’inlluence sur la compensalion des sta-
tions, mais ils se font sentir dans les erreurs de clöture des triangles. Pour ce qui concerne
le deplacement qu’ont subi quelques-unes des stations, parmı lesquelles la station primaire
de @. Malintang, n® 41, on peut consulter la communication faite par le soussign& a l’Aca-
demie royale des sciences, ä Amsterdam !.

L’erreur de clöture 6norme de 4.19 du triangle T. Si Dohar Dohar, D. Muleju,
D. Loeboek Raja, doit &tre attribude pour la majeure partie A ces deplacements.

Du reste, il est fort probable que les directions qui passent par dessus ou bien le
long des hautes montagnes ont Eprouv& des perturbalions considerables par suite des röfrac-
tions laterales.

1 J.-J.-A. Muller. De verplaatsing van eenige triangulatie-pilaren’in de Residentie Tapanoeli (Sumatra)
tenger olge van de aardbeving van 17 mei 1892. Verhandelingen der Koninklykc Akademie van Wetenschappen
te Amsterdam Eerste seclie. Deel. III, no 2. Amsterdam, 1895,

 
