RER II IN |

N RETTET °

LITT Fee

104

b) Meteoroloeisches.

Für Felton selbst existieren keine fortlaufenden meteorologischen Aufzeichnungen,
dagegen liegen dieselben für Santa-Gruz, nur etwa 2 kın. südöstlich von Felton vor, welche
sehr nahe auch das Klima von Felton darstellen. Dieselben sind mir durch Vermittelung der
Coast-Survey aus dem nordamerikanischen Weather-Bureau mitgetheilt worden; die Tempe-
ralurangaben habe ich aus Fahrenheit in Celsius, die Niederschlagsmengen aus Zollen in
Millimeter verwandelt.

Sanla-Gruz (bei Felton).

Mittl, Temp. Niederschlags-Menge. Mittl, Temp. Niederschlags-Menge,

S mm o mm
Januar st sr 4104 195 Juli: vb ans deal7,0 0
Februar . . 12,5 75 August r sub. 15,9 0
März. 11,0 253 September . 16,0 6
Ahnil Se : 49:5 42 Gelober . . 14,6 rw
Ma > 19,2 6 November . 19,5 89
Jun 22. 14,4 0 December . 19,3 89

Aus diesen Mittelwerthen einer zehnjährigen Beobachtungsreihe folgt für das ganze
Jahr die mittlere Temperatur 13°.5 und eine Niederschlags-Menge von 712m. Die Bewöl-
kungsziffer im Mittel für das ganze Jahr beträgt etwa 3. Das Klima von Felton kann daher
für sehr milde, gleichmässig und heiter gelten.

c) Seismologisches.

In dieser Gegend von Californien finden überhaupt keine Bodenerschütterungen
stall.

Station Petersburg (Virginia).

a) Topographisches.

Die von der State-Geological-Survey in Washington 1873 herausgegebene Map of
Virginia dient am besten zur Orientierung.

b) Meteorologisches.

Aus Petersburg selbst liegen keine meteorologischen Aufzeichnungen vor, dagegen
für die etwa 40 km. nördlich gelegene Stadt Richmond, deren atmosphärische Bedingungen
nahezu denjenigen von Petersburg entsprechen. Aus einer zehnjährigen Beobachtungsreihe
sind nach den Angaben des Weather-Bureaus folgende Daten abgeleitet worden,

 
