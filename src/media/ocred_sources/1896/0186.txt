 

 

I. Resultate aus den Beobachtungen am photographischen Zenitteleskop.

Das photographische Zenitteleskop wurde aufgestellt in dem kleinen Wellblech-
hause des Königl. Geodätischen Institutes auf demselben Platz, auf dem 1893/94
das visuelle Zenitteleskop dieses Institutes gestanden hatte. Bei senkrechter Stellung
des Fernrobres befand sich das Objektiv dicht unter dem Dachfirst, das Ende der
Thaukappe reichte ca. ıs cm in die freie Luft, so dass Schichtungen der Luft im
Innern des Beobachtungsraumes keinen schädlichen Einfluss ausüben konnten. Der
Spalt des Raumes ist ı m breit bei ca. 6 Um Grundfläche, sodass die Ventilations-
verhältnisse günstig sind. Der Spalt wird durch Zurseiteschieben eines Dachtheiles
geöffnet, der gleichzeitig die Belichtungsvorrichtung centrisch über das Instrument
brachte. Diese Vorrichtung besteht aus zwei horizontalen, unten mit schwarzem Stoff
beschlagenen und um Charniere drehbaren Klappen, die sich durch kurze, anschraub-
bare Holzstangen vom Innern des Häuschens aus bedienen lassen, und die bei
geöffnetem Spalt jede für sich in der betreffenden Kreislage des Fernrohres das
Sternlicht bis zu 30% Zenitdistanz vollständig abschliessen. Bei senkrecht stehendem
Fernrohr ist zwischen der Thaukappe und der Unterseite der Klappen ca. 10 cm
Zwischenraum.

Zur Beleuchtung des Beobachtungsraumes diente eine Dunkelkammerlampe, die
den unteren Theil desselben gelb, den oberen schwach roth erleuchtete. Die Niveaux
und das Schreibpult wurden im Bedarfsfalle durch eine roth gefärbte elektrische
Tasterlampe mit Licht versorgt.

Als Kollimator war im östlichen Meridianhaus ein zehnzölliges Universalinstrument
aufgestellt worden. Die Vertikalaxen beider Instrumente befanden sich in demselben
Meridian, die Horizontalaxen im gleichen Niveau, und die optischen Axen wurden
gegebenen Falles nicht nur parallel, sondern auch möglichst nahe zusammenfallend
gestellt. Zur Erzielung besserer Bilder wurde hierbei das photographische Zenitteleskop
auf 5o mm abgeblendet.

Von den vorhandenen 6 Kassetten, die eine freie kreisförmige Oeffnung von
5o mm besassen, wurden 5 derartig abgeändert, dass durch Einsetzen einer durch-
brochenen Messingplatte nur ein Spalt von 9 mm Breite freiblieb, der dem Bügel mit
dem Meridianstahlfaden gerade Durchgang gewährte. Diese Abänderung wurde deshalb
getroffen, um auf jeder Platte 2 Sternpaare aufnehmen zu können. Von der expositions-
fähigen Fläche jeder Platte wurde dadurch mehr ausgenutzt, es machte sich für jeden
vollen Beobachtungsabend das Neuladen von nur ı Kassette nöthig, und die Platten
eines solchen Abends konnten ohne Schwierigkeit gleichzeitig in derselben Schale
entwickelt werden. Zu diesem Zwecke wurden die Platten in horizontaler Richtung
um ıo mm kürzer gewählt, als es die Innenabmessungen der Kassetten gestatteten,

 

3
ı
3
3
3
3
3
3
3
ä
1

Peaerere Vi

bahn

cs am a sa a cn

 
