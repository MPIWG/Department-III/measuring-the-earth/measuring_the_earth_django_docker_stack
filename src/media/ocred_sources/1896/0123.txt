|

I Te ET

lim

i
i
.
E

erhöhen. Denn es wird wohl mit Recht zu erwarten stehen, dass durch eine Steigerung
der optischen Kraft der Instrumente die persönlichen Fehler der Beobachter, deren
Existenz an kleineren Instrumenten nachgewiesen ist, welche aber ihrem wesentlichen
Antheil nach vermuthlich nur aus einer verschiedenartigen Pointirung der Sternbilder
hervorgehen, eine Herabminderung erfahren werden. Auf eine erhöhte Leistungs-
fähigkeit der Instrumente wird um so mehr Gewicht zu legen sein, weil man dann
auch hinsichtlich der Grösse der Sterne und der Ausführung der Beobachtungen
während der Tageszeit an weniger enge Grenzen gebunden ist.

Auf Grund der oben abgeleiteten Coordinaten des Momentan-Poles hezogen
auf den mittleren Pol können die Reductionen von den Mittelwerthen der Polhöhe,
des Azimutes und der Länge auf die momentan geltenden Werthe mittelst der in
meinem vorjährigen Bericht gegebenen Formeln:

P—- pp — + zecosiA + ysinA
@ — % = + (ycosA — #sinA)secp
A—Ah = — (ycosA — zsinA)tangy

in denen A die westliche Länge bezogen auf den Meridian von Greenwich bezeichnet,

berechnet werden.

Potsdam, October 1896.

Th. Albrecht.

 
