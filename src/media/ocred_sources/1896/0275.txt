ET N

TE

I.kadnn

LUINULLARI

IR RR DRIN N RT Teen

269

Variation de lerreur maxıima de refraction

avec la longueur maxima des niveldes.

Dax

  

 

 

 

 

 

  

 

 

 

 

1 |
0.5— a
SIE
B X
Ö- T ir 3 FE J
en : 1
Valeurfs de
! —.L
100 200 Bor

Longueur des nivelees
2 Bi 5 R}
Figure 6.

La rencontre avec l’axe horizontal (pour m = 0) a lieu, d’autre part, en un point C, tel que :

Welt ‚@H-e)

m ©

 

Ces deux limites correspondent d’ailleurs :
la premiere, & 9m = © (en vertu de l’equalion 24) ;
la seconde, a 2m = 0 (equation 25).

Dans l’exemple choisi plus haut (n° 42), et represente sur la figure, on a:

8, = 0,9 inet vi), 600.mErres,

ASSOCIATION GEODESIQUE — 35

 
