 

 

 

62

nommen werden, da die Ausstellung am 18. geschlossen wird; der andere, der natürlich vom
Wetter abhängig ist, kann erst später festgesetzt werden. Der Secretär meint daher, dass die
weite Sitzung morgen, Freitag, um 10 Uhr im Bundesgerichtspalaste (Montbenon) stattfinden
und der 17. October zur Reise nach Genf bestimmt werden könnte. Sonntag oder, wenn Herr
Dumur es vorzieht, Dienstag der 20. October würde zum Ausflug nach Montreux in Aussicht
genommen, während die 5. Sitzung auf Montag den 19., und die 4. und, wenn nöthig, die
o. Sitzung auf Mittwoch und Donnerstag verlegt würden. »

«Nach diesen Angaben und Vorschlägen über die Organisation der jetzigen Gonferenz,
kommt der Secretär auf die Thätigkeit des Bureaus im vergangenen Jahre zurück und er-
wähnt zuerst, dass in Folge der ausnahmsweisen Fülle von Stoff, die Verhandlungen
(ler General-Gonferenz von Berlin in zwei Bänden veröffentlicht werden mussten, von denen der
erste, welcher am 15. Mai erschienen ist, die Protokolle der Gonferenz und der Permanenten
Gommission enthält, während der zweite, welcher die Spezial- und Landesberichte sowie noch
andere Dokumente umfasst, am 28. Juli veröffentlicht wurde. Diese beiden Bände sind
zweifellos schon seit lange den Regierungen und Delegierten zugekommen, durch die Sorg-
[alt des Gentralbureaus, welches auch eine gewisse Anzahl Exemplare nach Lausanne dirigiert
hat, zu Handen der Mitglieder der Versammlung.

« Der Secretär geht alsdann zu dem wichtigsten Theile seines Berichtes über, in
welchem er die Schritte des Bureaus darlegt, durch die dasselbe den von der General-
Gonferenz erhaltenen Auftrag erfüllt hat, die neue Uebereinkunft den Hohen Regierungen der
Erdmessung zu unterbreiten und deren Ratificalion zu erbitten.

« Das Bureau ist diesem Auftrage nachgekommen, indem es den diplomatischen Ver-
tretern der Erdmessungs-Staaten in Berlin je 5 Exemplare der Neuen Uebereinkunft mit
folgendem Begleitschreiben übersandt hat:

INTERNATIONALE Paris und Neuchätel, den 19. Februar 1896.
ERDMESSUNG.

Eurer Excellenz

Haben wir die Ehre hiermit fünf Exemplare der Neuen Uebereinkunft der inter-
nalvonalen Erdmessung zu übermitteln, welche von der am 30. September 1895 in Berlin
zusammengetretenen General-Gonferenz der Erdmessung angenommen worden ist; an dieser
Gonferenz waren 17 der Erdinessungs-Staaten durch ihre mit Instructionen der betreffenden
hegierungen versehenen Delegierten vertreten.

Nachdem bereits Anfangs des letzten Jahres von den Delegierten der einzelnen
Staaten die Wünsche und Vorschläge der nationalen geodätischen Commissionen betreff der
Erneuerung der Uebereinkunft uns mitgetheilt waren, hat eine in Innsbruck im September
1894 ernannte Spezial-GCommission einen ersten Entwurf ausgearbeitet, welcher alsdann von

=
ü
z

FIRreTuT TEmuT FITmWER TROTY N TEN

Lak al li il

ua

small a ha

 
