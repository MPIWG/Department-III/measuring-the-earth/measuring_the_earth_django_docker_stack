 

320

Leichtere Bewölkung des Himmelsgrundes hat daher kaum störend gewirkt.

Noch weniger störend wurde ein durch diffuses Mondlicht erleuchteter Himmels-
grund befunden ?; in Folge desselben ist überhaupt nur ein Paar bei enorm hellem Mond-
schein verloren gegangen, während an vier Tagen kurz vor Vollmond ganze Sternpaargrup-
pen und die Plejaden photographirt worden sind.

Auch die Plejadenaufnahmen mit Sternen 6—7ter Grösse gelangen vollständig dicht
beim hellerleuchteten Mond. Als besonders charakteristisch kann die Plejadenplatie vom 24.
Januar gelten, wo der Mond (zwei Tage nach dem ersten Viertel) in Declination !/,° und in
Rectascension nur etwa 0"4 von dem Centralstern Alcyone abstand und mit unbewaffnetem
Auge kaum etwas von den Plejadensternen zu erkennen war. Natürlich muss bei der Ent-
wickelung auf solche, durch Mondlicht etwas verdunkelte Platten besondere Rücksicht genom-
men werden.

Im Ganzen sind also durch zufällige Störungen, wenn noch zwei beim Entwickeln
verdorbene und zwei zu spät exponirte Platten hinzugerechnet werden, etwa 9°/, der ge-
sammten Plattenzahl für die Ausmessung verloren gegangen ; auch für optische Beobach-
lungsreihen würde hierdurch das für Störungen durch « vis major » zulässige Maass kaum
überschritten sein.

Berlin, Sternwarte, im August 1896. A. MARCUSE.

1 Aehnliche Erfahrungen hat auch Wolf in Heidelberg gemacht. Vergl. Astr. Nachr., Nr. 3319.
? Auf der Georgetown Sternwarte (bei Washington) sind ähnliche Erfahrungen gemacht worden. Vgl.
Publ. of the Georgetown Obs., S. 86.

 

!
Pf
s
3
3
ä
a
a
3
3
ä
ı

Pre 7}

i

 
