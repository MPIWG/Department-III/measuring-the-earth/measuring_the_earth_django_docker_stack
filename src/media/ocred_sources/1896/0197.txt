N TE ES ee La

UL AM IM ALU

[N

RINGE WRITE Tone

LT RN I

.. V ao. a V m

11013 111—13

Die grössten Abweichungen entfallen hiernach keineswegs auf die schwieriger
zu erhaltenden Sternpaare, sondern im Gegentheil auf solche Paare, welche fast an
allen Beobachtungstagen erhalten worden sind.')

Ordnet man diese für die einzelnen Paare als mittlere Fehler einer Polhöhe
erhaltenen Werthe nach den Distanzen der Spuren, so erhält man:

mm mm

Distanz: o.ı Scuwnauper:=6!os Hecker: =b0"25 Distanz: 2.8 Scunauper: =E0”7ı3 Hroker: =0"10
OR 31 18 SA! BE 28
0.3 23 23 3.5 49 4!
0.7 19 27 4.2 17 18
0.8 26 10 4.4 33 27
0.8 31 25
Der 05 06
17 19 14

Für die Distanzen von o.ı mm bis 1.7 mın ergiebt sich aus den betreffenden
[vv] ein durchschnittliches es von = 0/25 und ein ez von & o'23, für die Distanzen
von 2.8; mm .bis- 4-4 mim! eines’ — IE .0!3nlund erı==i* las. Bs findet: also! ‚bei
beiden Beobachtern ein Anwachsen des mittleren Fehlers mit der Distanz statt, und
diesem schliesst sich gut an, dass für das Skalenpaar «@, y Cor. bor. bei 10.4 mın Distanz
der mittlere Fehler einer Doppelmessung für beide Beobachter # 6“2 wird, also
für eine einfache Messung der halben Distanz (die ja nur in die Polhöhenpaare ein-
geht), = 0'66 beträgt.

Werden die in der Tabelle auf Seite 188 und 189 für die beiden Beobachter Scuwauper

= - © = S Jal ars
und Hecker getrenntgegebenen Werthe zu Mitteln vereinigt, also en gebildet, so folgt
für die Ableitung der mittleren Fehler & dieser Mittelwerthe:
Vz [vv]: 98 Anzahl: 2 m. Eu: =2oro
2 53 3 05
3 391 4 ı1
4. 3436 5 29
5. 555 3 17
V.. 17949 10 45
24, 4777 ri 22
6. 7393 10 29
Mi 11862 17 27
4. 11676 16 28
VER 2 2378 9 17
3% 626 10 08
5. 2320 9 17
63514 109

 

') Das Paar V. ı hat bei beiden Beobachtern einen ganz extremen m, F.; es ist aber durchaus
nicht das am schwierigsten zu messende, obgleich die eine Spur ziemlich schwach ist,

 
