 

RRERET EEE

za

tamar

ER TRETE

EEE ZEESBELERE

RETTET TE LER SENETTEEEEEN er

174

Es würde zu weit führen, wenn ich an dieser Stelle auf alle Einzelheiten dieses
Berichtes eingehen wollte. Ich möchte indess das Eine ausdrücklich hervorheben,
dass die genannten Astronomen, welche sich aufs Eingehendste mit den beiden
Beobachtungsmethoden beschäftigt haben, hinsichtlich der Verwendbarkeit der photo-
graphischen Methode für den Zweck der internationalen Breitenbestimmungen vom
Standpunkte der Erprobung zu einem negativen Resultat gelangen, und möchte
erwähnen, dass sich der Genauigkeitsgrad für diese Methode bei den Versuchsreihen
in Potsdam weniger günstig ergeben hat, als für die optische Beobachtungsmethode.

Ich kann mich nach gewissenhafter Erwägung aller Factoren, welche bei der
Frage der Wahl der Methode von entscheidender Bedeutung sind, dem Urtheile der
Herren Schxauper und Hrcxer nur anschliessen. Ich neige der Ansicht zu, dass die
Anwendung der optischen Methode wesentlich grössere Garantieen für
das Gelingen der Beobachtungsreihen bietet, als der Gebrauch der compli-
cirteren und von erheblich mehr Factoren abhängigen photographischen
Methode und möchte deshalb befürworten, für den geplanten internationalen Pol-
höhendienst an der optischen Beobachtungsmethode festzuhalten. Nachstehend sei
mir gestattet, Ihnen in Kürze die Gründe auseinanderzusetzen, welche mich, lediglich
sachlichen Erwägungen folgend, zu dieser Auffassung der Sachlage geführt haben.

Dem optischen Beobachtungsverfahren ist vorgeworfen worden, dass es der
nöthigen Homogenität ermangele, insofern bei den Einstellungen der Sterne der
Einfluss verschiedenartiger persönlicher Auffassungen der Sternbilder nicht ausgeschlossen
erscheine. Ein solcher Einfluss wird aber in erheblicherem Grade nur an optisch
schwächeren Instrumenten, sowie bei fehlerhafter künstlicher Beleuchtung der Fäden
zu befürchten sein. Denn bisher sind persönliche Gleichungen im Betrage von mehr
als einigen Hundertelsecunden nur in den nachstehenden drei Beobachtungsreihen
nachgewiesen worden:

Kasan, 2. Reihe 1893.5 — 1895.0 Grarschew — Trozkı —= —+ 0.06
Frag, ı. kele 1889.2 — 1892.4 WEINER — Guss = 0.11
— 2. — 1895.2 — 1896.5 SPITALER — LiegLem —= + 0.09

welche aber ausschliesslich an älteren kleineren Passageninstrumenten von nur
68 mm Oeffnung ausgeführt worden sind. Von Instrumenten dieser Art wird man
nicht die Vollkommenheit der optischen und mechanischen Ausführung erwarten
können, welche heutzutage Zenitteleskopen von ca. ıoo mm Oeffnung (unter dieses
Maass würde man wohl sicher nicht herabgehen) gegeben werden kann, und es wird
daher weniger der Methode, als vielmehr der geringeren Vollkommenheit der Instru-
mente zur Last zu legen sein, wenn in Fällen dieser Art persönliche Unterschiede
von erheblicherem Betrage nachzuweisen sind. Es steht daher zu erwarten, dass bei

 

|

1
3
3
a
3
3
3
3
3
3
1
1

blut hainieen

a

bat

su am ab as LE

nnnnnesinanäinsn mann hi

 
