Ss

ul

FERIKd ML paenn

BA

Beilage A. I.

Ueber die Wahl der Stationen

für den internationalen Polhöhendienst.

Die Auswahl der Stationen hängt von einer grösseren Anzahl von Factoren
ab und ist im Allgemeinen so vorzunehmen, dass allen maassgebenden Bedingungen
soweit als irgend angängig entsprochen wird.

In erster Linie ist für die Auswahl bestimmend die vorhandene Vertheilung
von Wasser und Land auf der Erdoberfläche.

Nächstdem wird aber in ganz hervorragendem Maasse auf die socialen und
hygienischen Verhältnisse der einzelnen Landestheile Rücksicht zu nehmen sein. Denn
man wird nur dann mit Sicherheit auf einen Erfolg der Beobachtungen rechnen
können, wenn den Beobachtern alle diejenigen Erleichterungen zu Gebote stehen,
welche der Aufenthalt in vollständig der Gultur erschlossenen Gegenden mit sich
bringt. Die Stationen werden thunlichst an einer Eisenbahnlinie in der Nähe einer
grösseren Stadt zu wählen sein, und es wird sich als vortheilhaft erweisen, dieselben
in erreichbare Nähe einer Sternwarte zu legen, weil von einer solchen aus eine
Controle über die Ausführung der Beobachtungen ausgeübt werden kann.

Nicht minder wichtig erscheint die Erfüllung der Bedingung, dass das Zu-
sammenwirken der Stationen es ermöglichen soll, die Coordinaten der Polbewegung
so scharf als möglich zu bestimmen. Hierzu wird einerseits gehören, dass die geo-
syaphischen Längen der einzelnen Stationen sich in geeigneter Weise zu einander
ergänzen, sowie andererseits, dass die meteorologischen Verhältnisse der betreffenden
Gegenden günstig sind und in ausgiebigster Weise die Vornahme astronomischer
Beobachtungen gestatten.

Endlich sind auch noch in hervorragendem Grade die seismischen Verhältnisse
der im Uebrigen geeignet erscheinenden Oertlichkeiten in Betracht zu ziehen. Denn
in erdbebenreichen Gegenden wird ausser der unmittelbaren Gefährdung der Station
schon die aus den häufigen Erderschütterungen hervorgehende Einschränkung der
Beobachtungsgelegenheit von Nachtheil sein, und es wird in Gebieten starker seismischer
Thätigkeit die Gefahr nahe liegen, dass die Lothlinie im Laufe der Zeit localen
Aenderungen unterworfen ist. Dergleichen locale Beeinflussungen werden aber bei
einer Beobachtungsreihe, welche dazu dienen soll, neben den rascher sich vollziehenden

3

 
