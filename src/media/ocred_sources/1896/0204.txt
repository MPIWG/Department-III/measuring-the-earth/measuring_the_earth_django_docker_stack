 

198

Die Photographie liefert:

A. dauernde Eindrücke, die

B. automatisch und

C. gleichzeitig erhalten werden, und bei denen

D.- eine Summation der Lichtwirkung stattfinden kann.

A. Die dauernden Eindrücke wurden durch geeignete Entwickelung auf der
photographischen Platte, als Träger der lichtempfindlichen Schicht, erhalten. Bedingung
für das Entstehen messbarer Eindrücke ist einerseits eine gewisse chemische Intensität
der Bestrahlung, andererseits eine gewisse Einpfindlichkeit der photographischen Platte,
die sich leider immer erst a posteriori constatiren lässt, und deren Voraussetzung
deshalb stets reine Vertrauenssache bleibt. Da nur ein verhältnissmässig kleiner Theil
des Spektrums photographisch wirksam ist, so muss diese Wirkung durch andere
Achromatisirung des photographischen Objektivs möglichst zusammengehalten werden,
wodurch seine Verwendung für visuelle Zwecke beinahe unmöglich wird. Ferner sind
im Allgemeinen die photographisch noch fixirbaren Sterne heller, als die bei
gleicher optischer Mächtigkeit der benutzten Instrumente unmittelbar sichtbaren,
wodurch eine wesentliche Einschränkung in der Auswahl der Objekte eintritt. Die
Intensität der photographischen Eindrücke hängt nun allerdings auch von der Dauer
ihrer Wirkung auf das Plattenelement ab. Für Sternspuren ist diese Dauer aber durch
den linearen Durchmesser des Sternscheibehens in der Bildebene und durch die lineare
Bewegungsgeschwindigkeit, also durch die Brennweite des Objektivs sowie die Deklination
des Gestirnes gegeben. Demnach wird bei wachsender Deklination die Anzahl der
Sterne, welche noch Spuren ergeben, wachsen, und die Spuren der Sterne, die
bereits bei geringerer Deklination erhalten wurden, werden kräftiger ausfallen. Da
die Breite der schwächsten noch messbaren Spuren, also der Durchmesser des die
Spuren erzeugenden Sternscheibehens, bei dem benutzten Zenitteleskop etwa 6" beträgt, so
würde daraus folgen, dass Sterne, welche bei einer Expositionszeit von rund 0.4 X sec Ö
noch keine messbaren Bilder ergeben, auch keine messbaren Spuren zu zeichnen
vermögen. Wird also, wie es a priori am Vortheilhaftesten erscheint, für die Spuren
der Komponenten eines Sternpaares gleiche Intensität verlangt, so muss der nördliche
Stern stets schwächer gewählt werden als der südliche, und hierdurch wird die Anzahl
der verfügbaren Sternpaare noch weiter eingeschränkt.

Bei diesen Verhältnissen sind aber günstige Luftzustände vorausgesetzt. Bei
stark wasserdampfhaltiger Luft oder auch nur leicht verschleiertem Himmel wird die
Intensität der chemischen Strahlung viel mehr herabgesetzt, als die der sichtbaren,
so dass die photographische Methode versagt, wenn die visuelle noch anwendbar ist.
Ferner bewirkt auch Unruhe oder Unschärfe der Bilder eine Vertheilung der chemischen
Wirkung auf eine grössere Fläche, also geringere Wirkung auf die Plattenelemente,
und damit Schwächung der Sternspuren, welche dadurch im Grenzgebiete bis zur
Unmessbarkeit herabgedrückt werden können.

 

i
3
i
3
a
73
+
|
3
i
i
1

rer y}

bett

abs mh bb bh a ns

 
