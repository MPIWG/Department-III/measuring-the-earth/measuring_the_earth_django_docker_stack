 

 

78

genommen und (8. 200 a. a. O.) erläutert hat. Dort finden sich auch einige Bemerkungen
über den Pendelapparat und das Verhalten der Pendel während der Reise. -Die neuen
Daten für diese Stationen weichen nur um geringfügige Beträge von den früheren ab.

Gruppe 3. Messungen der Kaiserl. deutschen Marine.
(Ref. Stat. Potsdam, G.1.)

Der Inhalt unserer Tabelle ist dem Bericht 1903, Tab. Vg: Stationen der Kaiser-
lich deutschen Marine entnommen und durch Anbringung der Reduktion — 0.018 em sek”?
auf das Potsd. Syst. bezogen worden.

Gruppe 4. Messungen des Königl. preußischen Geodätischen Instituts zu Potsdam.

A. Messungen auf Auslandstationen.*)
(Ref. Stat. Potsdam, G.1.)

Von den in unsere Tabelle aufgenommenen Stationen sind die vor dem Jahre
1906 bearbeiteten in den Berichten 1900, 1903 und 1906, Tab. Vd: Messungen durch
das preußische Geodütische Institut nachgewiesen und erläutert. Bei der Aufstellung
der Tabelle wurden die Angaben der Berichte sorgfältig mit den betreffenden Publi-
kationen verglichen, wobei sich jedoch nirgends Abweichungen fanden.

Neu hinzugekommen sind die Stationen Odessa, Tiflis und Bukarest, die Herr
Prof. Hscrer im Frühjahr 1909, gelegentlich seiner baro-thermometrischen g-Messungen
auf dem Schwarzen Meere, an Potsdam angeschlossen hat.”*) Von diesen Stationen wurde
Bukarest im Jahre 1900 von mir bereits gegen Potsdam festgelegt; das Haczer’sche
Resultat weicht von dem meinigen um + 0.001 em sek”* ab. Auch für Odessa und
Tiflis liegen jetzt russischerseits neue Verbindungen mit Pulkowo vor, die ich bei Be-
spreehung der russischen Arbeiten (Tab. VI, Gr. 3 und Gr. 6, A dieses Berichts) mit den
Hrcxer’schen Ergebnissen verglichen habe.

Prof. Hrorer benutzte zu seinen Arbeiten einen nach meinen Angaben vom Instituts-
mechaniker M. Fecmser hergestellten Vierpendelapparat mit 4 Halbsekundenpendeln, von
denen zwei (Nr. 5 u. 7) von Fronser, die beiden andern (Nr. 6 u. 8) von Srückraru
(Friedenau) gefertigt waren. Von den Pendelkonstanten wurden nur die Luftdichte-
koeffizienten neu bestimmt; es fanden sich dafür folgende Werte:

 

*) In den Berichten 1900, 1903 und 1906 sind alle Messungen des preuß. Geodätischen
Instituts in eine Tabelle (Vd) zusammengefaßt; ich habe sie hier, der größeren Übersichtlichkeit
wegen, in zwei Unterabteilungen zerlegt, von denen die eine (A) nur Messungen auf Auslandstationen,
die andere (B) nur solche auf deutschen Stationen enthält.

**) Diese Arbeiten sind inzwischen unter dem Titel: Bestimmung der Schwerkraft auf dem
Schwarzen Meere und an dessen Küste, sowie neue Ausgleichung der Schwerkraftmessungen auf dem
Atlantischen, Indischen und Großen Ozean von Prof. Dr. ©. Hscxer, Berlin 1910, erschienen.

nn behashabanl ,

 

 
