 

rarıwsche Halbsekundenpendel beigegeben, für deren Konstanten Herr Prof. L. Haasemann
im Frühjahr 1901 nachstehende, in 10°” Sternzeitsekunden angegebene Werte fand:

Temperaturkoeffizient Luftdichtekoeffizient

Pendel Nr. 13 471.81 #0.10 655.8 6.2
ld 48.76 # 0.13 642.0 # 10.6
26 48.25 #0.15 640.4 3.9
ey 48.41 =# 0.14 650.1 8.8

Auf allen 3 Stationen der Expedition fanden die Pendelbeobachtungen im luft-
verdünnten Raum bei etwa 60 mm Druck statt, wobei die Pendel ca. 12 Stunden
schwangen. Zur Bestimmung der Schwingungsdauer stand ein Narpın’scher MZ-Chro-
nometer (55/8555) und eine Halbsekunden-Pendeluhr von Sreasser & Ronpe (Nr. 231),
beide mit elektrischer Kontakteinrichtung versehen, zur Verfügung; außerdem hatte die
Expedition 4 weitere Chronometer (2 SZ, 2 MZ) an Bord, die gelegentlich zu Ver-
eleichungen herangezogen wurden.

Die Anschlußmessungen in Potsdam haben folgende, auf den Normalort der
Schwerebestimmungen (ZH = 87 m) bezogene Werte der reduzierten Schwingungsdauern
ergeben:

Pendel 13 Pendel 14 Pendel 26 Pendel 27
Potsdam, 1901, Frühjahr: 0°5076010  0°5077985 0:5099080 0°5097910
a 1904, 5 B 6023 1939 9088 OT
Änderung in 8 Jahren: =. Be 48 1,

die eine genügende Konstanz der Pendel erkennen lassen.

Die erste Station, welche die Expedition auf dem Wege zum Südpolargebiet
erledigte, war Porto Grande (genauer: Am Porto Grande, dem Hafen von Mindello)
auf der Kapverdeninsel Säo Vicente. Hier beobachtete Prof. von Drysanskı 2 Pendel
(Nr. 18 u. 14) über je 12 Stunden. Leider konnten infolge Ungunst der Witterung
keine Zeitbestimmungen ausgeführt werden; der Gang des zu den Pendelbeobachtungen
allein verwendeten Chronometers Narpın mußte durch Vergleichung mit den 4 Bord-
Chronometern, zwischen deren Standbestimmungen ein Zeitraum von rund 3 Monaten
lag, abgeleitet werden. Aus der eingangs genannten Publikation ist nicht ersichtlich,
weshalb die Nardinstände nur auf ganze Sekunden abgeleitet wurden; es wird jedoch
mitgeteilt (8. 326), daß die so ermittelten Nardingänge auf etwa 2 Sekunden unsicher
sind, während an einer andern Stelle (S. 360) die Unsicherheit dieser Gänge ohne nähere
Begründung zu 0.75 Sekunden angenommen wird.*) Beiden Angaben würden Unsicher-
heiten im Stationsmittel der Schwingungsdauern von rund 120 bezw. 45 - 10°” sek oder
in g von etwa 47 bezw. 18. 10°° cm sek? entsprechen. Berücksichtigt man noch, daß

*) Die erste Angabe rührt von Herrn Regierungsrat Dr. Domke, der die Zeitbestimmungen,
die zweite von Herrn Prof. Hassemann, der die Schweremessungen der Expedition bearbeitet hat, her.

 

|
|
1

on. Mehsshelban ln ne,

 
