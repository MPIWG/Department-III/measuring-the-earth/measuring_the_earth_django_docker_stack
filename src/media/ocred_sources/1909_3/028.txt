Teer neRTTTTEEFTEENTEETTT "TUN

 

  
 

 

 

 

 

 

 

I—1 0 —1.00) —2.00| —1.20| —1.20| | |
| | . | | | |
| | | | . | |
| | | —0.80| —0.80) |
as Il Hnsvae |
. | |
— 1.20 | 021 —1.20| —1.20 |
zoo 06 oe et
—0.67) —0.67| . ER . 1 —1.80) —1.00 | —1.60
—0.67| —0.07|  . : 0 —1.00 | —1.00,
| —0.67| —1.67| . & | | |
ı +8.13| —0.67 ® 0 20 |
zu Ba |
: 474.00 R | I
20) Neon) onso | |
oa ee | ae
le REEnNIO em eng — 2.00, —1.00
i "ost hiess, +4.60 1.00, —0.80 .326
ae ao
a | —1.00 0.80) . | +9.40| — 0.420
zZ | —0.02 En 6.02) 0.00) 0.00, 0.001 0.00) 0.00) 0.00) 0.00| nom
| | 2
[p#]| +1.67 | —2.93 | +1.67| 0.00 | —2.80) —0.80| +1.00| —2.20 | +1.00| —4.40 =

 

 

Die Gleichungen sind in Spalten angeordnet und dementsprechend in vertikalem
Sinne zu lesen; beispielsweise würde die Normalgleichung [5] lauten:

5 330) 3260) 1:30 17.9105 900.10: mc”

Am Fuße der Spalten sind die Koeffizientensummen X gebildet, die von ihrem
Sollbetrage O, infolge der Abrundungsfehler, um die angesebenen belanglosen Größen ab-
weichen; die darunter stehenden numerischen Werte [94] der Absolutglieder geben die

Summe
19

=[p4), = + 0.01.1073 em sek,

v=0
wodurch der numerische Teil der Normaleleichungen E kontrolliert ist. Aus den Absolut-
gliedern 4 der reduzierten Fehlergleichungen folst noch die später zu verwendende Summe

[» 44] = 165.80 . 1076 em? sek *.

Auflösung der Normalgleichungen E. Wie schon bemerkt wurde, besteht
unter den Gleichungen E eine lineare Abhängigkeit: jede der 20 Gleichungen ist identisch
mit der negativ genommenen Summe der 19 übrigen. Wir drücken dies kurz durch
die Formel nn

=p=0

0
aus, wonach die Summe aller 20 Gleichungen identisch null wird und folglich alle Unbe-

3

 
