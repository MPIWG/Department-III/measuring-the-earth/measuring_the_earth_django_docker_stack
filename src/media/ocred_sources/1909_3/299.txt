 

288

Unter den 2736 tabulierten Stationen kommen jedoch viele wiederholt vor. Um
die eigentliche Zahl der räumlich von einander gesonderten Beobachtungsorte zu ermitteln,
betrachten wir 2 unter gleichem Namen aufgeführte Stationen als verschieden, wenn
sie in ihrer geogr. Breite oder Länge um mehr als 1’ von einander abweichen, oder wenn
sich ihre Meereshöhen um mehr als 50 m unterscheiden. Danach würden beispielsweise
in Wien 2 gesonderte Schwerestationen vorkommen, das Militär-Geographische Institut
und die Sternwarte; ebenso würden Kolar (mine surface) und Kolar (mine underground),
die zwar in Breite und Länge übereinstimmen, in Höhe aber um 798 m von einander
abweichen, als 2 verschiedene Stationen zu zählen sein.

Es sind hiernach unter den 2736 tabulierten Stationen 2398 räumlich ge-
sonderte Beobachtungsorte vorhanden, wovon 2340 verschiedene Namen führen.

|
!
i

 

 
