Trrerun MEETTREEFTERTTTTEN 3" TUTN

 

13

/ pP, Toy yon BR [pw] Rn
Id = en Dale (p] 1 ne pı
“ 1-2, & ae Leo \
-20+U1-9)Q-.. -ucnte=Aats N

und die ihnen entsprechenden Normalgleichungen:

: PD, PıPsa 9 Br 5 [pe] u 1
(1 zn 9... —H (wi wi — Ad
En PN a a
zen E 2) 9... — pm 1) nt

Die Determinanten beider Systeme sind null. Den vorstehenden Ausdrücken
lassen sich leicht folgende praktische Regeln zur Bildung der reduzierten Fehler- und
Normalgleichungen entnehmen:

1. Die reduzierten Fehlergleichungen einer Beobachtungsreihe ergeben sich aus
den ursprünglichen (Tabelle ©), indem man diese mit ihren Gewichten zum
Mittel vereinigt und dieses Mittel von den einzelnen Gleichungen subtrahiert,
wobei [pe] gleich null zu setzen ist;

»

die entsprechenden Normalgleichungen gehen aus den reduzierten Fehler-
gleichungen durch Multiplikation der letzteren mit ihren Gewichten hervor,
wobei die Produkte pe fortzulassen sind.

Mit Rücksicht auf den einfachen Zusammenhang der beiden Gleichungssysteme
genügt es, eins derselben aufzustellen. Wir geben in der folgenden Tabelle D die aus
Tabelle © nach den vorstehenden Formeln abgeleiteten reduzierten. Fehlergleichungen.

D. Reduzierte Fehlergleichungen.

 

 

Reihe 1—5.
No as | 6 M 8 | 9 Io neo
en ne nl | ee | | Se
Netzp.| 19 | 16 ee a ee | 4
l | | | |
(0) SR Ba I 5 41/2

 

a

 

 

 

 

 

 

 

 

 

 

 
