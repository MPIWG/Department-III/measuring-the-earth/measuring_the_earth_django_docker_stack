231

Diese Zahlen lassen erkennen, daß man bei den Pendeln 57 und 38 die Mittelwerte ihrer
Schwingungsdauer auf den Außenstationen mit den entsprechenden Mittelwerten in Kew
vergleichen darf, während dies bei Pendel 39 wohl kaum noch zulässig ist. Mr. Curee
hat es zwar auch hier getan; er sucht aber die eingehenden systematischen Fehler dadurch
abzuschwächen, daß er den Ergebnissen aus Pendel 39 überall nur halbes Gewicht beilegt.
Etwas zweckmäßiger erscheint mir das folgende Verfahren. Pendel 39 hat sich von
1901.6 bis 1902.6 zwar stark doch zeitlich-linear geändert; von da ab bleibt es konstant,
und es gilt daher für den Mittelwert der Schwingungsdauer aus Kew (1901.6) und (1904.7)
nahezu dieselbe Pendellänge, wie für den Wert Christchurch (1901.9), während Christ-
church (1904.4) nur mit Kew (1904.7) vergleichbar ist. Beachtet man dies, sowie die
vorangehenden Bemerkungen über die Pendel 37 und 38 und unterscheidet die Schwin-
gungsdauern für die beiden Beobachtungsepochen einer jeden Station durch die Buch-
staben a und 5, ihre Mittelwerte durch m, so würden zur Ableitung von g nachstehende
Stationswerte und -unterschiede in Betracht kommen:

Stationswerte: Pendel 37 Pendel 38 Pendel 39 Pendel 39
Kew 0.5087776 m 0°5087732 m 0°5087925 db 0?5088064 m
Christehurch 0.5089590 m 0.5089508 m 0.5089738 b 0.5089876 «
Winter Quart. 0.5083203 m 0.5083132 m 0.5083339 m —
Stationsunterschiede: Pendel 37 Pendel 383 Pendel 39 Mittel
Kew-—- Christchurch — 1814 — 116 —- 1813 — 1801
— WinterQuart. + 4573 —+ 4600 + 4586 + 4586.

Die beiden Werte für Kew—Christehurch aus Pendel 39 stimmen hier fast genau überein.
Mit 4 (Kew) — 981.201 cm sek”? (Potsd. Syst.) ergeben sich folgende g-Werte, neben
die ich vergleichsweise Mr. Cnrer’s Resultate (im Potsd. Syst.) setze:

nach Borrass nach CureE
Christchurch 980.507 = 0.005 980.513 = 0.007
Winter Quart. 982.972 = 0.003 982.986 = 0.015.

Die m. F. sind in beiden Fällen aus den Stationswerten der 8 Pendel abgeleitet, bei
Mr. Curse entsprechend seiner Gewichtsannahme,

Zur Ableitung von (Melbourne) benutzt Mr. Curez nur Kew (1901), was für
Pendel 87 zulässig, für Pendel 39 aber, im Hinblick auf die Wertereihe Pj— Ps: S. 280,
nicht ganz zweckmäßig erscheint. Er gibt dem Pendel 89 halbes Gewicht und findet

damit im Potsd. Syst. die Werte:
w

Pendel 37 Pendel 39 Mittel
9 (Melbourne 979.937 980.038 979.971 0.048,

 
