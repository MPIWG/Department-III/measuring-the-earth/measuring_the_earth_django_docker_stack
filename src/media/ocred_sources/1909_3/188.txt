Tabelle VIII. Französische Messungen.

Gruppe 1. Arbeiten mit älteren Pendelapparaten.

A. Messungen mit Fadenpendeln.

Das hier mitgeteilte Material ist dem Bericht 1900, Tab. XIIa: Messungen
durch Bıor und Marsıeu entnommen worden. Obgleich die Messungen sämtlich ab-
soluter Art sind, so lassen sich aus ihnen doch 2 Reihen bilden, die eine die zufälligen
Fehler der g-Werte erheblich überschreitende systematische Verschiedenheit aufweisen.
Die Ursache dieser Verschiedenheit ist nach Herrn Prof. Herserr (a. a. 0. S. 257) wahr-
scheinlich in der Anwendung anderer Pendel bei der zweiten Reihe zu suchen. Für
beide Reihen muß daher die Reduktion auf das Potsd. Syst. gesondert ermittelt werden.

Die 1. Reihe umfaßt die von Bior, Marsmu und Bouvarn in den Jahren 1808,
1509 und 1817 bearbeiteten Stationen, unter denen Bordeaux, Paris, Dünkirchen und
Leith Fort (Edinburg) für die Ableitung der Reduktion auf das Potsd. Syst. in Frage
kommen. In der folgenden Tabelle sind die Ergebnisse der alten Messungen und daneben
die neueren Vergleichswerte, bezogen auf die geogr. Positionen der alten Messungen,
aufgeführt.

 

 

Messungen von 1808, 1809 und 1817. | Neuere Vergleichswerte im Potsd. Syst.
p ir Me obeob)) |y(Potsd.8) Beobachter Quelle: Ber. 1909,
Bordeaux 44°50.4 —0°34” 17m 980.603 | 980.575 N Corver, 1894 nal VAL En 2, &
£ : 5 5 | 593 J Eserancox, 19099 „ VII, „26
Paris 48 50.2 2 20 75 980.995 | 980.939  Netzausgl. —
Dünkirchen 51 2.2 223 4 981.226 | 981.174  Derrorges, 1889 VI.
Leith Fort 55 58.6 —310 21 seen. oo m.
s = a 2 ı 611 } Granzr, 1892 = ww .oB
: 5 5 5 627 ) Lauer, 1898 : mo

23

 
