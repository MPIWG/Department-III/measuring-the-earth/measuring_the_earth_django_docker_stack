207

Diese Werte sind augenscheinlich durch systematische Fehler stark beeinflußt; angewendet
wurde für alle Pendel der Mittelwert aus den vorstehenden Zahlen: (43.4 #& 0.29) 107
Sekunden. Für die Dichtekonstanten wurden die im Bericht 1906, S. 225 mitgeteilten
Werte beibehalten.

Über die Konstanz der Pendel geben die nachstehenden Reihenmittel ihrer
reduzierten Schwingungsdauern Auskunft; zum Vergleich habe ich diesen noch die Werte
vorangestellt, die der Beobachter vor Beginn seiner Weltreise 19083—1904 in Padua beob-
achtet und in der eingangs genannten Publikation S. 123 u. f. ebenfalls veröffentlicht hat.

Reihe Zeit Pendel 32 Pendel 33 Pendel 34 Pendel 35 Mittel
Padua A 1903.71 0°5075923 0°5076258 0°5073168 0°5076932 0°5076070
B 1904.13 5924 6235 3144 6941 6061
C 1902.29 5930 6231 3156 6908 6056
D 1904.97 5933 6249 3139 6959 6070
Padua 1 1907.15 0.5075927 0.5076223 0.5075030 0.5076962 0.5076036
IL 1907.17 5937 6231 5038 6967 6048
Potsdam Ill 1907.23 0.5074342 0.5074634 0.5073435 0.5075367 0.5074445
V 1907.30 ABAl 4641 3435 5365 4446
Padua VI 1907.35 0.5075942 0.5076239 0.5075050 0.5076968 0.5076050.

Jede der Reihen A—D enthält 48 einzelne Bestimmungen der Schwingungsdauer
(12 Sätze zu je 4 Pendeln); für die Reihen T— VI ist der Umfang aus der vorangehenden
Übersicht zu ersehen. Bildet man noch einfache Stationsmittel so ergibt sich

Zeit Pendel 32 Pendel 33 Pendel 34 Pendel 35 Mittel
Padua 1904.28 0°5075927 0°5076243 0°5075152 0°5076935 0°5076064

    

; 1907.16 5932 6227 5034 6965 6040
Potsdam 1907.26 4342 4637 3435 5366 4445
Padua 1907.35 5942 6239 5050 6968 6050.

In der Zeit 1904.28—-1907.16 haben sich daher die Pendel um +5, — 16,

— 118 und + 80; zwischen 1907.16 und 1907.85 aber um + 10, +12, +16 und +3,
im Mittel also um + 10 Einh. d. 7. Dez. geändert. Herr Prof. Arzssıo vereinigt die Reihen
Padua I, II und VI zum einfachen Mittel und erhält so mit S(Padua) — 0°:5076043,
S (Potsdam) — 0!5074445 und g (Potsdam) = 981.274 cm sek”? den Wert g (Padua)
— 980.656 em sek ?, der von dem auf $. 197 abgeleiteten und unsrer Tabelle zugrunde

gelegten Normalwerte für „g(Padua) im Potsd. Syst

p x H 9 (Potsd. Syst.)
Padua, Sternw. 45°24:0 11°52/3 19m 980.658 =+ 0.002 cm sek °

nur um — 0.002 em sek”? abweicht.

 
