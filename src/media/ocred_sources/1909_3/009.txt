 

 

 

Tabelle III. Dänische Messungen. j u
Messungen der dänischen Gradmessungs-Kommission =
Tabelle IV. Schwedische Messungen.
Messungen der schwedischen Gradmessungs-Kommissiin . 2 2 2 2 2 2 an 20 e. 17,118
Tabelle V. Norwegische Messungen.
Gruppe 1. Messungen der norwegischen Gradmessungs-Kommission 19120
Gruppe 2. Messungen der Polarexpedition von Friwısor Nansen . . 2. .... . 119, 122
Tabelle VI. Russische Messungen.
Gruppe 1. Messungen mit älteren invariabeln Pendeln . 123, 144
Gruppe 2. Messungen mit Rersorv’schen Reversionspendeln. ... 2.2... . 124, 145
Gruppe 3. Messungen der Kaiserl. Nikolai-Hauptsternwarte zu Pulkowo . ..... 127, 147
Gruppe 4. Messungen der Kaiserl. Universitäts-Sternwarte zu Kasan. ....... 130, 148
Gruppe 5. Messungen im Auftrage der Geographischen Gesellschaft von Finnland . 132, 150
Gruppe 6. Messungen des Kaiserl. russischen Generalstabes re 185, 150
Gruppe 7. Messungen im Anschluß an das Physikalische Institut der Universität in
St-»Beterspurge es ee lee land 142, 160
Tabelle VII. Schweizerische Messungen.
Gruppe 1. Messungen der schweizerischen Gradmessungs - Kommission vor dem
Jahre 100 2 2 aan. Er. . 161, 169
Gruppe 2. Messungen der schweizerischen Gradmessungs- Kommission nach dem
Jahre 1900 . 1615 172
Tabelle VII. Französische Messungen.
Gruppe 1. Arbeiten mit älteren Pendelapparaten . . 177, 187
Gruppe 2. Arbeiten mit neueren Pendelapparaten en . 180, 188
Gruppe 3. Messungen des Service geographique de lArmee. ... 2... 2... . 183, 190
Tabelle IX. Spanische Messungen.
Gruppe 1. Messungen des Geographisch-statistischen Instituts zu Madrid 192, 194
Tabelle X. Italienische Messungen.
Gruppe 1. Messungen der Königl. Sternwarte zu Padua... ... . 195, 208
Gruppe 2. Messungen des Militär-Geographischen Instituts zu Florenz . . 197, 208
Gruppe 3. Messungen auf Sizilien und benachbarten Inseln und Küsten . . 198, 209
Gruppe 4. Messungen in Oberitalien (Piemont)... . . . . 204, 212
Gruppe 5. Messungen durch italienische Expeditionen . 205, 214
Gruppe 6. Messungen der Königl. italienischen Marine .... . 2... ce. 205, 214
Tabelle XI. Englische Messungen.
Gruppe 1. Relative Messungen mit invariabeln Karer’ schen Sekundenpendeln . 215, 233
Gruppe 2. Messungen durch die Survey of India... ... NE DR ONE . 224, 238
Gruppe 3. Messungen durch englische Expeditionen ...... lee 228, 240

 

nk enuan «

 
