 

Br

242

Messungen mit Pergcr’schen Reversionspendeln.

Unsre Tabelle gründet sich auf den Bericht 1900, Tab. XVII bu.c: Messungen
durch Mr. E. D. Preston und durch Mr. GREELY.

Die nachstehende Zusammenstellung enthält in chronologischer Folge die Stationen,
die für die Ableitung der Reduktion auf das Potsd. Syst. in Frage kommen.

Messungen von Preston (1883, 87; 8I—90)

Vergleichswerte im

Potsd. Syst.

 

Jahr

 

Station p

1883 | Lahaina 20°52'
1883 |\ ‚
1887 nl 21 18

|
a s. Franeisco | 37 47.5,

|
1887 | M. Hamilton 37 20.4
1887 | Washington 38 53.3
1889 | St, Paul de — 8 49

Loanda
1890 Bridgetown 113 4
1890 | Georgetown — 7 56
1890 Green Mount. — 7 Sl

|

1890 | Jamestown 18 55

| |
1890 | Kapstadt |--33 56

| |

38

 

14

14 22

 

|
|
3.978.845

|

4.978.954

.7) 114979.942

.6 1282,979.646

10.980.100

 

1978.137

18.978.160
978.237

ou

686/978.084
10/978.659

11979.606

 

H g(beob.)| g(P.$.) Jahr Beobachter | Quelle: Ber. 1909
BT |
978.898, 1819, Frrremer Tab. VIII, Gr. 1B
|
|
978.947| 1892| Presvon \ . S
926, " 5 If” IN
924, 1896) Murrox& \ - 1. >B
965 1901 Srockerr ı
979.965, 1891, MENDENHALL BEXE 2,
9591 1893| Drrronees | „ VI, „3
960 1904| HEcKRER 5 I an
979.660 1891 MexvennaLı Re
626 1893 Derrorgzes | Ya,
980.113; — | Netzausgl. =
978.202) 1894, v. LEIDENTHAL Tab. I, Gr.2B
193) 1897| v. Rervurvank J
199 1898| Lonsen N ae
978.335 1897 Ropuer IL, „ioB
978.328) 1822| Sazınz I XI SB
315 1825| Durerrey | = VIEL 2, SB
327, 1830 FosTEr | XL, 1D
978.130 1830| Foster in x, ,ıD
978.670 1829 Lürzn EN
686) 1830 Foster „a XL 5; ED
682| 1895| Gussenmarn 100008
979.666, 1818| Frrvemser END 5 dB
661 1829 Foster ..,Ww
652) 1894 v. LeiDentHaL |
656, 1895| GassenmarR > 1.5, 2B
648, 1897| v. REITERDANK |

 

 

659, 1898| Lorsch

” ; IH, „ 3

Redukt.
auf das
Potsd. S.

10.

id),
o
=
a

0.
o
10

nn
—0.

0

048

007
028
030
o11l

023
017
018

014
020

er
1
or

rbhssculsnnsbehsisaiersuuniie

 

 
