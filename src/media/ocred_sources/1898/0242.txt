 

228

 

Dit ; Einfaches Mittel!
Dan Mittlerer Mittleres Mittel u
Paar. Q der ehler : der nach
Messungen. on e" Differenzen. Gewichten.

5 731 — o.ıI FoKo3

Vvı 1.89 — 0.17 =12/0. 03

IN 3 1.92 — 0,13 2.0.05

o 1.97 -+ 0.07 =20.04 En Bo
vi 6 2.00 — 0.11 =/007, . oo es
VI4 2.04 — 0.06 22 90.03

d 2.08 -+ 0.03 =E 0.04

AV 23 2.22 -+- 0.04 ei 0.07

V6 2.28 -+ 0.02 ei 0103

a 2.62 + 0.02 ZE 0.04

Ve 2.67 —— 0.37 =eoN04:

VA 235 — 0.10 = 0.05 _
IV; 5 — 0.17 SIE 0108 41 N er
IV 6 5.65 — 0.15 230,00
VII 2 6.19 — 0.09 =E 0.08

N ı 6.92 — 0.55 =1=70.10

Gesamtmittel: 2.51 — 0'0g — 0108.

Aus demselben Material ergeben sich für die einzelnen Paare die folgenden,
nach der Intensität der schwächeren Spur geordneten mittleren Fehler einer viertel
Differenz beider Messungen, welche gleich sind dem von den Messungsfehlern her-
rührenden Anteil des mittleren Fehlers eines Polhöhenwerts:

3

NV2lı6r  ==0208 g ei 0403 d =E olos IVvea, 2 0%04
N 3 20.06 IV =2o.04 Yo 200 V 2 20.04
INo6 ı 20.05 Ren 2.0.04 a 320.08 Nale2) 20,08
Wat 0.o e 2240.08 a  -Elor.on NIT 2, 130.06
b = 0.08 e 12 0.03 f =E 0.04 WS 25005
Vr 2003 VS 220.08 Vi 200; RMV 2 2004

Es zeigt sich hier im allgemeinen eine Abnahme des m. F. mit zunehmender
Intensität. Aus dem Mittel = o’osz dieser m. F. folgt für den mittleren Fehler der Poin-
tierung einer Spur mit dem Messapparat der Wert #0'245 = #0:0032 = 1.6 Tausendstel-
millimeter.

Um die Polhöhenresultate der einzelnen Sternpaare von den Fehlern der an-
genommenen Deklinationen zu befreien, wurden zunächst aus allen Abenden, an denen
alle 6 Paare einer Gruppe beobachtet waren, die Reduktionen der einzelnen Paare
auf das Gruppenmittel abgeleitet. So konnte aus der Übereinstimmung der einzelnen
Werte dieser Reduktionen für jedes einzelne Paar auch der mittlere Fehler einer
Beobachtung frei von Fehlern der angenommenen Deklinationen und von den

28

 

A Mailldhakı | u ddl ul

rd a a ME

kt

1 ul

{kun ku BEL a
