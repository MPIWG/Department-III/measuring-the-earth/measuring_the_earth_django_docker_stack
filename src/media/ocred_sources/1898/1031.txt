TI men rm nn

 

DAR

Ei)

Mer VRLRANAER IL MINEN DM LEE

 

 

     

— 815 —

XII. — Portugal,

 

Renseignements sur la triangulation de I ordre.

La triangulation portugaise de 1° ordre comprend 132 signaux g6odesiques, cons-
truits en maconnerie et dont la plupart a la forme d’une pyramide rectangulaire &
base carree. Dimensions moyennes: cöte de la base, 3 mötres; hauteur, 9 metres,

Il y a longtemps (1836-1863) que Vobservation de ces sommets a 6t6 faite par la
möthode de la repetition des angles, avec des theodolites & deux lunettes. Ces travaux
avaient seulement pour but la recherche des elöments pour la levee de la carte.

Toute la triangulation s’appuie sur la base Batel-Montijo — 4787,941 brasses de
Ciera = 10823,894 meötres. Ce rapport a besoin d’ötre verifie.

Le Portugal ayant adher6 au plan de l’Association g6odesique internationale apr&s
que l’Espagne a projet& son reseau fondamental, on a reconnu la necessit& de choisir
un certain nombre de triangles qui pussent completer d’une maniere uniforme le sys-
teme fondamental de la triangulation iberique; et apr&es des combinaisons diverses on
a adopt6 le plan ci-joint, qui est simple et systematique.

En consequence, le reseau geodesique fondamental fut r&duit & 68 sommets, sans y
comprendre les extrömites DB, M de la base. |

La direction de l’Institut G&ographique du Portugal ayant considere que les an-
ciennes observations d’angles, quoique ex&cut6es avec tous les soins generalement en
usage dans la premiere moiti6 de ce siecle, ne pourraient point rivaliser ou möme ötre
comparables avec celles qu’on obtient maintenant par des methodes perfectionnees, a
acquis des instruments modernes et a proced&e & de nouvelles observations dans les.
susdits 68 sommets et dans les extremites de la base. |

On a determine dans les stations 15 directions azimutales, & peu pres, pour chaque
point g&odesique. Chacune de ces directions est le rösultat de 4 visees, dont les deux
premieres sont obtenues avec le cercle vertical & gauche, et les deux autres avec le
cercle & droite. Pour chaque vis6e on a fait dans le cercle 4 lectures micrometriques.

 
