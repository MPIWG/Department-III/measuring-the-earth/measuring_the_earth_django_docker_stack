 

me über den Stand der Arbeiten im
Jahre 1898 durch Herrn Oberst von
Schmidt. (Siehe Beilage B.J)... .

Bericht des Herrn Prof. Haid über die
in Baden ausgeführten Pendel-Beob-
achtungen. (Siehe Beilage B. II).

Bericht des Herrn Prof. Celoria über
die Arbeiten der geodätischen Com-
mission in Italien, während der Jahre
1897 und 1898. (Siehe Beilage B. IIIa).

Bericht des Herrn Bassot über die in
Frankreich von 1896-1898 ausgeführ-
ten Arbeiten. (Siehe Beilage B. IVa.)

Bericht des Herrn von Orff über die in
Bayern von 1896-1898 ausgeführten
Arbeiten. (Siehe Beilage B. V).

Mittheilung des Herrn Anguiano über
die Ernennung einer geodätischen
Commission in Mexico und deren
Betheiligung an der Vermessung des
grossen Amerikanischen Meridianbo-
gens.

Bericht der oast ne ode Survey,
von Herrn Preston. {Siehe Beilage
B VI) a

Bericht des Herrn Sagasta über die spa-
nischen Arbeiten. (Siehe Beilage B.
VII.)

Mittheilung des Han rn Rense uhet die
neue in Breteuil entdeckte Legierung
von Stahl und Nickel.

Bemerkungen des Herrn Hirsch über
denselben Gegenstand. ,

Discussion über die Bedeutung A
Metalls .

Vierte Sitzung, 7. October 1898.

Geschäftliche Mittheilungen des Se-
NE ee

Verlesung und Annahme des Proto-
kolls der 3. Sitzung.

Bericht des Herrn Guarducei über nn
vom Militär Geogr. Institut in Flo-
renz ausgeführten Vorbereitungs-Ar-
beiten zur Verbindung von Malta mit
Sicilien. (Siehe Beilage B. III»).

Spezialbericht des Centralbureaus über
die Lohabweichungen, von Herrn
Boersch. (Siehe Beilage A. Ile).

974

 

 

Pag.

26

28

29-30
31-38

31-32

 

Spezialbericht des Centralbureaus über
die Längen, Breiten und Azimut-Be-
stiimmungen, von Herrn Albrecht.
(Siehe Beilage A. IV).

Discussion über eine Neu- Bemmung
der Längen-Differenz Paris-Green-
wich und Annahme eines darauf be-
züglichen Beschlusses.

Antrag des Herrn Preston betreff er
Neu- Messung des Peruanischen Bo-
gens. Pension und Ueberweisung
dieses Antrages an eine Commission,
bestehend aus den Herren Bassot,
Preston und Sagasta .

Antrag des Herrn Preston, eine oo.
mission zur Behandlung der bisheri-
gen Gradmessungen und Schwere-
Bestimmungen, zu ernennen

Discussion und Ueberweisung dieses
Gegenstandes an das Centralburean.

Spezialbericht des Herrn Bouquet de la
Grye über Mareographen -Beobach-
tungen. (Siehe Beilage A. V). .

Bericht des Herrn Darwin über die
Jüngsten Arbeiten in England und
Ostindien. (Siehe Beilage B. XXI).

Fünfte Sitzung, 10. October 1898. .

Geschäftliche Mittheilungen und Ver-
lesung des Protokolls der 4. Sitzung.

Bericht und Anträge der ernannten Brei-
ten-Commission, von Herrn Preston.

Discussion und Beschlüsse über die
acht Anträge der Commission .

Mittheilung eines Vorschlages des
Herrn Vogler, in den Breiten-Statio-
nen Höhenmarken zum Studium der
Lothschwankungen zu errichten. Dis-
cussion .

Spezialbericht des 2 Ferr ero ker
die Triangulationen. (Siehe Beilage
A. VII Tome Il).

Sechste Sitzung, 11. October 1898.

Verlesung und Genehmigung des Pro-
tokolls der 5. Sitzung.

Vorläufiger Bericht des Herrn Heltoenı
über die relativen Messungen der

Pag.

33-34

39-36

38

39-44

39-40

40-441

41-43

43

 
