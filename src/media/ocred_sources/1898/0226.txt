STEEETETEE

Seammnmemanee

 

FERTITEITTEIET

Bee see peu

Te

242

kürzere Zeit; in knapp 2 Stunden geschah die Messung von rı2 Polhöhenpaaren in
beiden um 180° verschiedenen Lagen der Platten, gegenüber reichlich 4 Stunden am
Wanschaff’schen Apparat. Die prinzipiell als Nachteil zu betrachtende Verwendung
der Messschraube als Transportschraube bei dem Toepfer’schen Apparat hat sich praktisch
als bedeutungslos herausgestellt, indem die von Herrn Dr. Hroxer am Toepfer’schen
Apparat wiederholten Messungen von Platten der 1896er Reihe denselben mittleren
Fehler ergeben, wie die ursprünglichen Messungen mit dem Wanschafl’schen Apparat.
Auch hat sich aus meinen weiter unten mitgeteilten Untersuchungen der 21 mittleren
Revolutionen der Schraube vor und nach der ganzen Messungsreihe (rund 1000 Messungen)
keine Abnutzung feststellen lassen.

Platten und Entwickler.

Im Januar ı897 trafen die bei Dr. Scureussser bestellten neuen Platten
(Nummer der Emulsion 5824) ein, die nach Angabe der Fabrik von möglichst gleicher
Qualität, wie die für die früheren Reihen benutzten, sein sollten. Es stellte sich aber
eleich bei den ersten Aufnahmen heraus, dass sie bei weitem leistungsfähiger waren,
als die früheren. Bei dem altbewährten Rufe der Schleussner’schen Fabrik lässt sich
daher wohl kein anderes Urteil fällen, als dass es bisher überhaupt nieht möglich

ist, so hochempfindliche Emulsionen mit auch nur annähernd derselben Gleich-

mässigkeit herzustellen, wie die Emulsionen mittlerer Empfindlichkeit.

Gemeinschaftlich mit Herrn Professor Arserenr wurden sehr ausführliche
Versuche angestellt zur Feststellung der günstigsten Methode der Entwicklung mit
Rodinal; an möglichst gleichmässig klaren Abenden machte ich Reihen von Auf-
nahmen der Plejaden und der Praesepe, welche dann verschieden lange in Entwicklern
verschiedener Konzentration (1:10, ı:ı5, 1:20) mit und ohne Bromkalium ent-
wickelt wurden. Die Resultate dieser Versuche decken sich vollkommen mit denen
von Professor Max Worr (Astronomische Nachrichten No. 3319, Band 139, Seite 105);
"Bromkaliumzusatz erwies sich als schädlich, und der konzentriertere Ent-
wickler ergab die günstigsten Resultate, auch bessere, als wenn man anfangs mit
verdünnterem Entwickler (1:30) arbeitet und nachher mit konzentrierterem zu Ende
entwickelt, ähnlich wie es Dr. Marcvuse gethan hat (M. 5, Seite 13). Was die Dauer
der Entwicklung betrifft, sO zeigte es sich, dass man etwas günstigere Resultate
erhält, wenn man bis zu einer ziemlich starken Schleierbildung entwickelt und in
dem den Schleier stark abschwächenden sauren Fixierbade fixiert, als beim gewöhn-
lichen Fixierbade, für welches nur eine schwache Verschleierung der Platte zulässig
ist. Ferner hat sich gezeigt, dass Verstärkung mit Sublimat zwar die kräftigen
Sternspuren bedeutend verstärkt, aber die schwächsten Spuren merkwürdigerweise
wieder verschwinden lässt, wahrscheinlich infolge der bedeutenden Vergröberung des
Korns, ähnlich wie die schwächsten mit blossem Auge sichtbaren Spuren bei An-
wendung einer Vergrösserung, die das Plattenkorn sichtbar macht, vollkommen un-
sichtbar werden.

 

a a a Meldhukı dı ud ul

ak mi

 

 
