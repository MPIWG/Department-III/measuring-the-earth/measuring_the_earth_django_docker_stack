ne

 

Kine

Fl)

un

mal

Fa

a)

289

EONGIT ID

 

 

 

 

 

Annee et mois

1883
Juni-Juli

1883
Juni-Juli

1883
Juni-Juli

1883
Juli-August

1895
August-Sep-
tember.

Elimination des
equalions personnelles

 

 

TITRE DE LA PUBLICATION

 

Directe Bestim-
mung

Wechsel
der Beobachter und
Instrumente.

 

 

REMARQUES

 

XII. Band der astron. geodä-
tischen Arbeiten desk.u.k. mil.-
geographischen Instituts.

» »
» »
» »

Astronomisch -geodätische Arbei-
ten in Bayern. Heft 1. München
1896.

 

Da die Sternwarte Bamberg nicht
geodätisch bestimmt war, wurde
die Längenbestimmung auf den
benachbarten Hauptnetzpunkt
Altenburg geodätisch übertra-
gen ; die Längendifferenz Mün-
chen, Sternwarte (Pfeiler unter
der westlichen Kuppel) — Alten-
burg ergab sich hierdurch zu:

+ 257,017 + 0,008 (w. E.)

 

 

 

 
