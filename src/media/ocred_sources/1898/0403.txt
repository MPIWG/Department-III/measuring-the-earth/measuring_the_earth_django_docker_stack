 

385

 

|
N

France, de l’Espagne etdu Portugal, le travail du Survey, consistant A accunmuler et & reduire
les observations mar&graphiques, a &t& en progres constant depuis un demi-siecle.

Les principaux resultats que nous avons obtenus sont eontenus dans la publication
dont le titre est: Tide Tables by the U. S. Coast and Geodetic Survey,
d’observations ont &ie failes ei röduites en dehors de celles qui sont publiees.

Ce que nous avons depouille embrasse la cöte des Ktats-Unis d’une facon suffisante
pour pouvoir predire en de nombreux points l’heure et la hauteur des marses.

Les courants qu’elles engendrent &tant generalement faibles par comparaison avec
ceux qui existent sur les cöles ouest de l’Europe, les observations de cet ordr
soins du Survey ont durd seulement quelques jours dans chaque station.

Les constantes harmoniques qui s’y referent ont &t& caleulees seulement dans quel-
ques cas.

mais beaucoup

e faites par les

a

Le but de ces observations 6tait de fournir une indication suffisante pour estimer le
moment du renversement du courant dü A la marde et le maximum de
points situes sur la route des navires.

Ces informations sont plus compleötes sur la cöte de l’Atlantique que sur celle du Pa-
cifique. D’ailleurs les courants sont en r£alite plus en connexion avec les mardes
mier cas que dans le second oüı l’inegalite diurne est notable.

Dans plusieurs localites les courants ont 616 observes ä diverses profondeurs pour
mieux s’assurer de la circulation des eaux. L’exemple le plus important des &tudes de cette
nalure a et& developpe par Mitchell dans le rapport du Survey pour 1887.

Notre service reconnaissant la dependance mutnelle des mar&es et les varialions de

leurs caract&res dans diverses rögions a commeneg, il ya cing ans, ä compulser les observations
faites dans toules les regions du globe terrestre.
ces resultats.

sa vitesse en des

 

dans le pre-

man) Kun

Nos Tide Tables contiennent une partie de

(MT

A l’heure actuelle les observations des mardes et mäme des courants permettent d’ob-
tenir les valeurs horaires des premieres et les maxima et minima des vitesses des courants.
Les principales de nos stations, c’est-A-dire celles oü de longues series d’observa-

tions ont &t& enregistrdes, sont: |

1° Fort Hamilton N.Y.; 9 Reedy Island, Riv. Delaware; 3° Washington, D.C.;
4° Fernandina, Floride ; 5° Presidio, Californie ; 60 Seattle, Washington.

Les deux premieres stations sont pourvues d’indicateurs automaliques donnant aux
navires la hauteur de la marde (voir Notice to mariners, nos 177-202).
| Le nombre total des ann&es dont les courbes ont &t6 enregistrees sur les maregra-

phes par les soins du Survey est d’environ 350.

Les mar&graphes adoptes par le Survey ont &te d6crits et figures dans les annexes 7 ä
9 du rapport pour l’annde 1897.

Les anciennes observations faites par notre office ont &t6 discutees par le superin-
tendant Backe: les personnes qui ont travaill& dans la m&me voie sont Meech, Pourtales,
Shott et Avery. Les prineipaux rösultats numeriques obtenus figurent dans les rapporis de
1853-1864, sous le titre de: Tide Tables for Ihe use of navigators.

am

a

TTTaTT

ME IRIR TI IR RAATAA II IRA In Tr

— 49

 
