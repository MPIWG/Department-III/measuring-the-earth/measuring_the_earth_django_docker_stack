 

156

maniere plus active ; ainsi, depuis 1886, nous ne le voyons plus prendre part ni aux Gonfe-
rences gen6rales, ni aux reunions de la Commission permanente. Gependant, pour prouver
que l’oeuvre de l’Association internationale avait toujours pour lui un interöt particulier,
nous n’avons qu’a mentionner la publication de la partie russe de la mesure de l’arc du 52 me
parallele, due principalement & son activite, son initiative pour l’arrangement des observa-
tions des d@viations de latitude A Tachkent et enfin ses relations scientifiques avec la plupart
de ses collegues aupres de l!’Association.

M. le professeur Hammer lit le rapport sur les travaux du Wurtemberg. (Voir
Annexe B. XIII.)

En outre,. M. le professeur Koch distribue aux membres de l’Assemblee des exem-
plaires de son M&moire : « Sur les mesures relatives de la pesanteur », publie dans la
« Zeitschrift für Instrumentenkunde ». Ge memoire comprend : 1° Un mode de suspension
du pendule qui exclut sa participation aux oscillations ; 2° La transmission de P’heure ä la
station de campagne en utilisant ’horloge normale de la station centrale ; 3° Etudes experi-
mentales sur la variabilit& des pendules.

Le Secretaire donne connaissance d’une lettre dans laquelle MM. Bassot et Bourgeois
expriment leurs regrets de ne pouvoir assister ä la derniere scance, attendu que des affaires
de leur service les obligent A retourner & Paris.

La derniere seance, destinde essentiellement & la lectiure de rapports nalionaux, est
fixee a demain, mercredi 12 octobre, ä 10 '/, heures. Cette s&ance sera prösidee par le vice-
president, M. le general de Stubendorff, que M. Faye prie de bien vouloir le remplacer, parce
quil est oblige de quitter Stuttgart le lendemain malın.

M. le President prend conge de ses collegues et les prie de l’excuser s’il ne peut
rester avec eux jusqu’a la clöture de la Conference.

M. Foerster est assure de l’assentiment de toute l’Assemblee en exprimant & M. Faye
les plus vifs remerciements pour la maniere consciencieuse et impartiale avec laquelle il a
rempli les fonctions de la presidence.

La seance est levgeeä 5h. 20.

 

=

ne ae HH Meilldlıkı

[ELLE

113 Iurto ll

A. suorebnsshiennn, mania A|

 
