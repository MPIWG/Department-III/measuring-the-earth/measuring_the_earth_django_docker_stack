a

En |

TE

iu am mann) a

El

11

KLERRREN. m Lam wm Hi

mie In Aa ER

 

 

—- 379 —

xV. - Roumanıe.

PREPACE

La triangulation de la Roumanie comprend trois reseaux de triangles.

Le premier s’etend dans la partie sud du pays et contient 134 sommets formant
des chaines de triangles et, sur certains points, des polygones.

Ce reseau a 6t6 ex&cut6 par les oficiers de 1’Institut Geographique de Vienne
de 1855 & 1857; il s’appuie & une base qui a &t& mesurde aux environs de Slobozia.

Les observations ont e&t& faites par la methode de repetition. |

Le second röseau de triangles contient 98 sommets qui constituent des polygones
lesquels couvrent toute la surface de la Dobroudja; il est appuy& au cöt&: Devcea-
Säapata pris comme base. Ce cöt6 fait partie d’une ancienne triangulation faite dans
cette province en 1855 par les mömes officiers de l’Institut G&ographique de Vienne.

Le reseau actuel a &te ex&cut6 par les ofhiciers du Depöt de la guerre de l’Arm6e
roumaine de 1880 & 1882.

Les observations ont &te faites par la methode de r&iteration.

Le troisieme reseau est en voie d’ex6cution: il a &t6 entrepris en 1875 et couvre
dsjä la partie nord de la Moldavie. Jusqu’a prösent il a &t6 determine 33 sommets
dont huit appartiennent aux r6seaux des pays limitrophes, & savoir: six en Autriche
(Bucovine) et deux en Russie (Bessarabie).

Les triangles qui r&unissent ces sommets, ainsi que ceux qui joindront les points
suivants, formeront des polygones contigus destinds & couvrir la surface entiere du pays.

Ce röseau commence par les officiers du Depöt de la guerre de ’Armde roumaine
est continue maintenant par les officiers de la section g&odesique du grand Etat-Major
roumain.

Les observations on &t6 &galement faites par la methode de reiteration, par s6ries
de 4 repetees de 10 & 12 fois.

 
