 

|
|
\

 

U a!

an

Fre an

Mr Tara Ti

103
de Menone ne correspond pas exactement avec le point italien de meme nom, que TVidentite
est presque certaine pour Basodine et Cramosino, et au moins probable pour Ghiridone.

Pour le rattachement, nous nous en sommes done tenus au triangle Basodine-Gramo-
sino-Ghiridone, dont les angles, dans le sens de la valeur suisse moins la valeur italienne,
presentent les differences suivantes :

— 1790; + 1/56; + 0'83,
tandis que les differences pour le triangle Ghiridone-Cramosino-Menone s’elevent ä
+ 686; — 3’39;, — 3'146.

Si maintenant on place les triangles limitrophes, suisses et italiens, Basodine-Cramo-
sino-Ghiridone, dans la meilleure position reciproque, les distances des points de meme nom

ne d&passent qu’une fois legerement 0.1, tandis qu’on trouve pour les differences probables
dans les longueurs des cötes :

Base du Tessin moins Bases suisses — 19 unites de la 7e deeimale du logarithme,
contre 9 uniles, si l’on fait la jonetion au cöte non identique Ghiridone-Menone (Voir Rapport

pour 1897, p. 90). L’erreur de elöture de l’equation de Laplace pour Simplon-Milan s’abaisse
ainsi ä — 1'8.

Apres avoir recu de M. le Directeur Gautier, & Geneve, par l’intermödiaire de M.
Messerschmitt, de nouvelles valeurs pour les azimuts des mires A Geneve, toutes les öquations

de Laplace montrent, en Suisse, des erreurs de clöture favorables, et m&me extraordinaire-
ment faibles dans le nord de !’Italie.

Afin de donner une idde approximative sur le cours des deviations de la verticale
dans la region &tudide, nous indiquons, dans la liste suivanle, pour les points oü la latitude
et la longitude ont &t& determinees, les valeurs provisoires des deviations en latitude (£) et
en longitude orientale (X), dans le sens astron.—geodes.,oü l’on n’a tenu compte pour les % que
des longitudes dötermindes astronomiquement. Les el&ments de l’ellipsoide terrestre dont on
s’est servi sont ceux de Bessel ; pour le point de depart Bonn, on a pris les valeurs des de-

viations adoptees dans Ja mesure de l’arc de longitude sous le 52° parallele (voir « Längengrad-
messung », 2° cahier, p. 205):

& A
Bm , ı _ m —.58
Mannheim. , . © — 1,1
Strasbourg. UGRl-SEEn 6 — 9,4
Rigi. STDISBR I Sala — 42
Simplon . . „sion — 5,9
Min... 1.0 —+ 3,5
Turin .„ .„ .„‚erodisee ps -. 35,6
Genes?ip2 eiapellelengh + 0,2
Niee 391 ailjrozon aeg arg + 0,9

 
