Te een

 

[Me

IR THRNAER IL) IRIKN. BR LIE Jimi

ml

 

 

   

 

an

IH. — Belgique.

 

Resume historique.

X

Le plan primitif &labor& par le General Nerenbürger pour les travaux de triangu-
lation destines & contribuer & la determination de la figure de la terre, comprenait:
1° La mesure de trois bases vers les sommets de la figure triangulaire que pre-
sente le territoire belge;
2° La liaison de ces bases deux ä& deux par des chaines de triangles, dont l’une
devait &tre dirigee de l’est & l’ouest dans le sens du parallele de 51°, une autre dans
le sens de la m6ridienne et la troisieme en diagonale dans la direction N.-O.—S.-E.
Les bases de Lommel et d’Ostende furent mesurees de 1851 & 1853, & l’aide de l’ap-
pareil de Bessel appartenant au Gouvernement royal prussien. La mesure d’une troisieme
base dans la partie meridionale du pays ne put ötre effectuee, l’appareil des regles
ayant &t& necessaire au General Baeyer pour mesurer en 1854, en Silesie, une base de

raccordement avec la Russie. On eut soin d’&talonner les regles avant de les renvoyer

& Berlin.

Dös 1851, on commenca & observer les angles des triangles qui se rattachent im-
mediatement, d’une part, & la base de Lommel, d’autre part, & la base d’Ostende. On
etendit sur tout le pays un reseau de 219 triangles de premier ordre reliant entre eux
84 points geodesiques; les op&rations sur le terrain furent terminees an 1873. L’instrument
employ& fut le th&odolite de Gambey dont le cercle, arme de 4 verniers, permettait la
lecture avec une approximation de dix secondes centesimales, et l’on adopta la methode
de la röitöration, chaque angle devant ötre determine par 60 mises.

Cependant, on s’est &carte de ce qui avait &t6 decide au debut des operations, en
ce sens que les observations ont &t& faites, en chaque station, de la meme maniere et
avec les mömes soins, de sorte que les mesures d’angles servant & la constitution des
chaines n’ont pas &t& isoldes de celles qui se rapportent au travail de remplissage. Tout
le röseau forme ainsi un ensemble dont les directions sont li6es les unes aux autres &
chaque station par les &quations des directions probables. Pour operer exactement, il

 
