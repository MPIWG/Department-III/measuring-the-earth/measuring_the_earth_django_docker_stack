 

REES EERGEEESSASSREEEREE TEE

379

|
!
\

M. R. Steckel, du Department of Public Works, a fait un nivellement geodösique de
Montreal a Quebec, et il täche de relier le niveau moyen de la mer avec ce nivellement aussi
loın que possible dans l’estuaire du Saint-Laurent.

Les maregraphes des Indes Orientales.

J’abröge une lettre que le Major Burrard a eu la bonte de m’adresser. I] ne m’a pas

paru necessaire de donner les details des proc&des par lesquels on depouille les traces des
maregraphes.

Pendant les vingt-eing dernieres annees, le « Survey of India» a eu AU stations mare-
graphiques. Parmi ces stations, il yen a sept qu’on va retenir comme observatoires perma-
nents; dans la liste qui suit, leurs noms sont imprimes en majuscules. Aux stations de moindre

importance, on n’a eu l’intention que d’obtenir eing ans de traces. On a ainsi deja termine les
observations ä 27 des 40 stations.

Je donne maintenant la liste, avec les dates des commencements et des fins, les nom-
bres des annees deja acquises et l’&chelle des tracks.

 

Liste des Maregraphes indiens.

 

 

 

 

 

 

 

 

 

 

 

 

 

Fchelle Date du Date de la fin Nombre
| STATION commencement des observations des annees
| des tracks. | ges observations, et 6tat actuel. dejä acquises.

{ ı 1 Sun. - 1897 fonetionnant. 1

3 porn > 1898 fonctionnant. _

u | S2ADENn ze 1879 fonctionnant. 18

3 | A Maskat . E 1893 1898 5
| (4 premiere

i 5 Biene >. u es 1892 fonctionnant. 5
NOKARACHTe me -- 1881 fonctionnant. 1

E | Hansa 1874 1875 1

8.Nowanar Eco. ı.2. 1874 1875 1

9 Okha Point. I 1874 1875 1
| 10 Porbandar.. 2. 2. 1898 fonetionnant. —
11 Bhavnagar . = 1889 1894 5
| 12 BOMBAY (Apollo Bandar). I 1878 fonctionnant. 20
13 Bombay (Prince’s Dock) . 15 1888 fonctionnant. 10

13

 

MIR I IRLHANAER NLLIRIREE Bw LInFaRTT

 
