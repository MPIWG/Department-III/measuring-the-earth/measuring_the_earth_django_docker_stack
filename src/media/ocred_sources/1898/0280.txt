 

Ä
|
|
|
N
v

v
hi
i
N

262

 

in Breite, von denen 27 im VII. Bande des Schweizerischen Dreiecknetzes und 6 in dem
Proces-verbal der 41. Sitzung der schweizerischen geodätischen Commission vom Jahre 1898
enthalten sind, hier nicht aufgeführt.

Die im Vorstehenden besprochenen Lothabweichungen sind nun auch benutzt worden,
um mehrere Geoidprofile zu konstruiren. Zu dem schon früher abgeleiteten Geoidprofil im
Meridian von Neuchätel sind jetzt noch solche in den Meridianen des St. Gotthard (bereits ım
Bericht von 1895 erwähnt) und von Bern — in einer Ausdehnung von 200 km. und von
90 km. — und ein Parallelbogenprofil von 130 km. Ausdehnung für die Breite 47°2’ hinzuge-
kommen. Die ermittelten Abweichungen des Geoids vom Ellipsoid sind in Hinblick auf die
sichtbaren Massen auch hier wieder sehr gering, indem sie nur wenige Meter erreichen, in
Uebereinstimmung mit den Resultaten der Schweremessungen.

Auch Attractionsberechnungen sind ausgeführt worden. In den Astr. Nachr., Bd. 141,
S. 75/76, sind 14 derartige ältere Rechnungsresultate für die Anziehung im Meridian, und
ausserdem auch für 6 von diesen Punkten für die Anziehung in Länge, die sich auf sehr
verschiedene Entfernungen, aber alle auf über mehr als 90 km., erstrecken, und die von
Scheiblauer, Haller,Denzler, Du Pasquier und Messerschmittherrühren, angegeben, und zugleich
mitden beobachteten Werthen verglichen worden. Endlich sind von Herrn Messerschmall für die
4 Stationen Gurnigel, Gurten, Lägern und Hohentwiel die Attractionsbeträge ermittelt worden,
die die sichtbaren Massen für die ersten drei Stationen bis auf90 km. und für Hohentwiel
bis auf 60 km. Entfernung berücksichtigen. Nimmt man wieder die Lothabweichung für Bern
gleich null an, so sind die verhältnissmässig gut übereinstimmenden Lothabweichungsdiffe-
renzen im Mittel

mn

in Breite: Beob. minus Rechn. = — 7
in Länge: >» » Br 8.

und

Im allgemeinen lassen sich diese Differenzen durch einen entsprechenden Massen-
defect unterhalb der Alpen erklären, worauf ja auch die Pendelbeobachtungen hinweisen.

Es verdient aber hervorgehoben zu werden, dass, wenn man die Massen nur bis zu
ca. 3d km. Entfernung berücksichtigt, die beobachteten und berechneten Lothabweichungen
nahezu einander gleich gefunden werden, wenn man ausserdem noch mit Helmert für Bern
+4’ und + 3” als Lothabweichungs-Gomponenten in Breite und in Länge annimmt.

Dasselbe Verhältniss findet Herr Messerschmitt auch für verschiedene Punkte in den
bayerischen Alpen und für die von Pechmann bearbeiteten Punkte in Oesterreich bestätigt.

In allen diesen Untersuchungen kann man also wieder eine Bekräftigung für die aus
den Lothabweichungen und aus den Schweremessungen erschlossene geologische Natur der
Alpen finden.

ITALIEN

Hier scheint allein die Bestimmung der Lothabweichung in Breite für das geodätische
Observatorium der Universität Palermo (la Martorana) vorzuliegen?, nachdem schon früher die

° A.Venturi. Sulla latitudine della Specola geodetica della Martorana in Palermo. Atti della reale

 

=
ü
z

ee a

Lake 1 il!

u nn |

near al banken aaa un

u. 2mmeiinsiienen mania NL |

 
