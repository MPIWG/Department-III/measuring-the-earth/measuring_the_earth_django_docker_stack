 

ee

 

HE

KWllı

ul

RER ILLARINEN. DL NET zw nr

Ja

369

 

places. En 1888, nous avons publi6 dans les Comptes-Rendus de l’Acadömie des Sciences
(seance du 19 nov. 1888) les moyennes annuelles pour le& ports de Brest, de Cherbourg et
du Havre.
Ge relev& qui, pour Brest, portait sur 35 anndes, avait d’abord &t& fait en prenant
la moyenne de toutes les hauteurs, de quart d’heure en quart d’heure, apr&s les avoir cor-
rigees de la pression barometrique et de l’influence due ä la direction etä la vitesse du vent ; ce
travail considerable a pu ensuite &tre simplifi6 en ajoutant un chiffre constant A la moyenne
des hautes et basses mers, auxquelles on avait fait subir les deux corrections pr6citees.

Ce chiffre constant varie beaucoup d’un port ä un autre; ä Brest oü la sinusoide est
presque reguliere, il est de + 29 millimetres; ä Cherbourg de 48 millimetres; au Havre de
176 millimetres.

Nous avons dü &galement corriger les rösultats de l’influence d’une petite onde dont
la duree est de 18 anndes et A Brest. nous avons trouve que la hauteur de la pluie influait
sur le niveau de la mer. Le mar&graphe plac& A P’embouchure d’une petite riviere, la Pen-
feld, a ses courbes surelevees lorsque les eaux douces sont plus abondantes.

La formule de cette correclion trouvee empiriquement est 0
H etant la hauteur annuelle de la pluie.

Comme suite aux tableaux publies en 1888, nous donnons, pour Brest, le Havre et le
Socoa, les chiffres obtenus pendant les onze dernieres anndes.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

BREST LE HAVYRE LE SOCOA
er —Z ä nn es TE Su TEE N RT —— a a
H. moy. | H. moy. “ H. moy. |
corr. de lal Onde ven Plnie eorr. dela | Onde ent - g corr. de la Ode |
Pression |Lunaire | SR Resultat | Pression |Lunaire | Resultat | Pression . | Resultat\
barom6e- | millim. | Millim. | millim. baroms- | millim. | Millim. barome- | Lunaire |

trique trique trique
SS 1 = ni mic mo ; =
1887 | 201 | 2 1) 01 010 4,753 |+1|+ 8| 2768 |2"306 |-L 1 \2,307
| 88 A an N 155 so, | 0. 81% 320. Io
9 91—-2—- 10223 2393| a > 5 u a. 2
90 144 | — 2 | -—- 18| + 2) 486 806; 2) 12 26 3a N
a 4164| —3|— 2% — 2 483 Sr, | - 911 18, 82 | 0, 30
92 4 — 3|— I) — 31 468 1792| 913 22. 88 06, = 3, 80
ı 8 139) — A, 9-55 8 en 20 I|— A| 316
I 294 4801 —5|— 211 — 8 446 802 | —5|+10) 807 sliı; 0, 00
95 a5 ah 2 55-548 79 slaen= 5 297
BI ABB — At 5 aa me are aı 22 72
1.197 5011 — 2|- Ib 31 48 7501 —2)# 80 7781| .83212.921 390
[1892 ....Moy. 58 Moy. 4,808 Moy. 2,313

 

 

D’apres les r&sultats obtenus A Brest, anterieurement ä l’annee 1887, on ne pouvait
en realite admeitre que des mouvements presque insensibles dans le sol (!/; de millimetre
d’exhaussement par an). Si l’on compare la moyenne des hauteurs correspondant &A l’annde
1368 (4'476), äccellede ce tableau (4458), une difference de 18 millimetresen 24 ans donne-

ar

 
