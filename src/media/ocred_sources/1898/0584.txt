 

928

TABLEAU I. — Conditions generales des bois experimentes.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

&n = .. a REGLES HUILEES
oO ı 2 en = innen
=&, sen
ae ESSENCES DES BOIS = a S = I Augmentation Volume
eu Ss 23 = proportionnelle | proportionnel
- De Ss
= S au = Q : s Er
Z = a 5 du poids apres d’huile
> Aa huilage. absorbee.
BOIS RESINEUX millimetres. p- 90. Palo:
I 2
ei Bchpm 2.2.2 ..232.12.8€6. „.:10,8:35,5170,68 27 20
28 Sapın (bone detabh) . - . 2 02202 0.0 => 2]
ea) 2 2... Denise. 0,42 2,5] 0,42 6 3
Dam 2... .2 2.) Sec ..|0,8 22,8! 0,42 1 5
BOIS NON RESINEUX
3 en, — _. ..,:ec..10931,0) 0,41 50 22
14 CRıme .  . . Denise .|1,0 3,01 0,74 34 29
26 Be... Idem .\0,14 3,0! 0,74 38 al
’
22 Deo 0 ..2. „| dem .\1.0&2,5| 0,78 B 26
17 Peuplanı oc... ® 2,5450 04 70 32
298 | Acajou femelle (cedrat) . . . 2 2245,06 0.2 Il 5
15 Memo 0. yes ? 59a%.V 0,66 52 36
IE 00... 20,8) Denise. 10,4& 1,6) 0,8 22 19
20 ok 0.  .. . 0. 2 0,.34283| 0,61 4 2
Du Heime 0... ns. 2 0,7325 0 16 13
(*) Bois de choix offerts par M. Wolf, facteur de pianos.

 

 

 

 

Une etuve cylindrique (fig. 3), en töle de cuivre, a triple enveloppe, permettait d’en-
fermer simultanäment toutes les regles dans un milieu clos, dont la temperature et l’etat
hygrometrique pouvaient a volonte, soit varier suivant les besoins, soit ötre maintenus con-
stants pendant plusieurs jours.

A cet effet, l’intervalle entre les deux cylindres interieurs &tait, suivant le cas, rem-
pli d’eau ou de glace pilee. Le compartiment exterieur &lait occupe par un matelas isolant
de laine min£rale.

Sous l’etuve, et en communication avec l’espace rempli d’eau, se trouvaient un bouil-
leur B et, au-dessous de celui-ci, une rampe R de gaz ä flamme reglable. Deux agitateurs a
permettaient de r&yulariser la temperature du liquide. La temperature du compartiment in-
terieur 6tait indiquee par deux thermombetres i loges dans des tubes de cuivre pänetrant la-
teralement jusqu’au centre de l’Etuve.

 

bahn had ana

 
