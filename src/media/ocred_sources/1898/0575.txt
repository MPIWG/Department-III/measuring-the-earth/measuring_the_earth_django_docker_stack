|
|
i

 

 

U]

nlln

ein)

MIR FRE RRNR IL HIN BIN

ia nr

The difference of longitude between Bulawayo and the Cape Observatory was
determined by exchange of telegraphie signals on four nights in the period May 18
to 31, Mr Sirmms having determined his personal equation at, the Observatory relative
to the Cape Observatory. shortly before his departure for Bulawayo,

Astronomical latitude and azimuth were also determined at Bulawayo during the
same time.

A reconnaissance was then made for the selection of a base-line and a suitable
site was found near the Insiza River. The work of measurement was begun on
June 26 and finished on July 29, this included a period of preliminary drill and
measurement of the base in both forwards and backwards. The length of the base
is 113 English miles.

The Jäderin apparatus was then returned to the Royal Observatory for re-com-
parison with the Cape standards. For this purpose it became necessary to level and
lay down another short base-line on the side of the Observatory hill; because, in con-
sequence of an abnormally wet season, the original base-line was inundated by the
overflow of the Black River. At the new base-line I devoted much time along with
Mr Robinson (Secretary and Computer of the Survey) to comparisons, experiments
aud general study of the Jäderin apparatus, and it is believed that in future ib
will be possible to increase very considerably the accuracy of work with this most
convenient apparatus.

Meanwhile the selection and beaconing of the triangles for connecting the base-
line with the chains of previously selected points, and for strengthening the original
chain south of Bulawayo (where the work of selection has been exceedingly difficult)
were carried out.

This done, the regular measurement of horizontal and vertical angles and the
determination of astronomical latitudes and azimuths has been steadily pushed forward.

Up to December 1 the following points have been occupied with the 10-inch
Repsold theodolite, and the horizontal and vertical angles of surrounding points have
been measured:

Bulawayo, Thaba Inyoka, Oriterion, Usher, Inüga, Golati, M’Quilembywe, Tsetsa,
Tuli Road, Thaba Induna, Inxela, M’Pochu.

Astronomical longitude has been determined at Bulawayo,

Astronomical latitude at Bulawayo, Thaba Inyoka and Golati.

Astronomical azimuth at Bulawayo, Thaba Inyoka and Tsetsa.

The total work accomplished is shewn on the diagram.

When the triangulation was originally planned it was assumed that it would be
necessary to avoid the northern part of Rhodesia and to follow the telegraph line
from Salisbury across Portuguese territory to Livingstonia and along the western coast
of Lake Nyassa to Tanganyika, if the scheme of an arc of meridian from the Cape
to Cairo was to be carried out. Recent investigation has shewn that the country
directly northwards from the western point of Portuguese East Africa (long. 30° E.)
to Lake Tanganyika is practicable for triangulation, and the railway from Bulawayo
will follow that route.

Mr Rhodes has intimated that so soon as the financial arrangements have been
made for the extension of his railway from Bulawayo to the Zambesi, he will place
at my disposal the funds necessary to carıy a geodetic arc of meridian from southern

 
