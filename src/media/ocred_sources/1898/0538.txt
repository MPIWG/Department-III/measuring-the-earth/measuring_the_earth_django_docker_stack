 

AS4

Ueb. N.N.
7] Stockheim, nordwestliche Ecke vom Sockel des Betriebsgebäudes . 151 17705
© Büdingen, Betriebsgebäude 134,49788
7] Hirzenhain, Brücke daselbst . 938,76888
© Gedern, Rathhaus 315,92368
DJ] Kefenrod, Brücke daselbst g 258,82318
DJ Hitzkirchen, Sockel der Bürgermeisterei 315,36013
7] Schlierbach, Kilometerstein 19,4 . 156,10492
7] Grabenhain, Brücke an der Strasse nach Breifnkleindn 428,14692
© Freiensteinau, Hofgebäude des Freiherrn von Rieulesel . 436,30994
7] Uerzell, Fels daselbst a. d. Kreuzung d. Strassen n. Schlüchtern u. Sa 331,39421
TI Altenschlirf, Meilenstein 170 414,49061
DJ Hainzell, Brücke daselbst 210,38235

ER] una.

2 j Romerz, ans
© Gr. Lüder, Betriebsgebäude der Ban ie Bulda
Bei iiesem Nivellement,

411,90863
284,74665
273,26323

sowie auch nachher wurde stets in der ee verfahren,

wie dies im Generalbericht von 1879, Seite 95 angegeben ist. Ausserdem wurden die Soll
Nivellirlatten von je 3 Meter Länge jedesmal vor Beginn der Arbeit und nach dem Schluss
derselben mit einem Normalmeter verglichen. Die Resultate dieser Vergleichung sind in der
folgenden Tabelle zusammengestellt. Die Correctionen wurden dann mit dem mittleren Werthe

der beiden Latten berechnet.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Die hier folgende schematische Figur gibt einen Ueberblick über die Nivellemenis-
züge im nördlichen Theile von Oberhessen, wobei die Pfeile die Richtung des Steigens an-
geben. Was die Höhenmarken an den einzelnen Punkten betrifft, so bestehen dieselben ent-
weder aus in das Mauerwerk eingelassenen Messingbolzen mit eylindrischer Bohrung, oder es
‚sind in Form eines Quadrats eingemeisselte Zeichen. Zu den ersteren gehören Friedberg,
Butzbach, Giessen, Reiskirchen, Grünberg, Lich, Hungen, Ranstadt, Schotten, Gedern,
Gr. Lüder, Lauterbach. Was die übrigen Punkte betrifft, so findet sich die Bedeutung der-

selben in dem späterfolgenden Höhenverzeichniss angegeben.

| | I.
Jahr Latte I Latte Il Sn. =, Jahr Latte I N Latte II en en
beiderLatten | | beiderLatten
ı mm mm | mn | En |
vorher [3999 990 12999,895 | | vorher 2998,946 2998.746 |
1894| nachher 21000, 70» |3000.328 | 3000,2056 [1896 nachler 3000.232 12999,758 | 2099,418
Mittel 13000.2995|3000,1115 | Mittel |2999,584 299, 28) |
vorher 2999,810 |2999,912 | vorher 12999, 712 |2999,620 |
1895| nachher 2999,721 |2999,810 | 2999,8133 1897 nachher 3000, 154 '3009,156 2999,9105 |
Mittel |2999,7655|2999,861 | Mittel 2999,93 '2999,888 N

are shi kai

 

 
