 

TE ee

 

ai

ai

Mir TORTE TOFTRHRE ORT LIT

7

 

comme dans le rapport que j’ai prösente ä l’Assemblee de Lausanne (Gomptes-Rendus, p. 16
ei 69), qu’& m’en reförer A une notice que j’ai publise dans les Astronomische Nachrichten,
Bd. 143, n? 3430, p. 345, sur mon &tude de Vinfluence de l’elastieit6 du pendule ; cette
notice sera suivie d’un m&moire plus detaille.

Les premieres recherches de MM. les Dr Kühnen et D' Omori n’ont pas montre une
grande difference entre le rösultat obtenu avec le pendule de M. le professeur Lorenzoni (muni

de ses couteaux) et ceux des autres pendules de P’Institut, et enfin avec le resultat d’Op-
polzer, transport& de Vienne.

A cette occasion, je constate que nous n’avons pas rencontr& les diffieultes que von
Oppolzer a signalees dans son rapport de 1883 (Comptes-Rendus de home, Annexe VI, p. 12
et 40) au sujet de la visee des couteaux en agate &clairds artificiellement. Au lieu de la
bande obscure de 20 mierons que von Oppolzer a vue en 6clairant & la fois le fond et le
couteau, nos couteaux en agate n’ont montr6 qu’un fin trait gris, de quelques microns
toul au plus, qui fournirait m&me un excellent objet de pointe. II est probable que les
miroirs d’Eclairage employ6s par von Oppolzer n’avaient pas linclinaison voulue. Nous
aurons du reste l’occasion d’examiner ce point de plus prös, puisque la Commission g&od6-

sique autrichienne a bien voulu nous präter le pendule de von Oppolzer pour des exp6-
riences.

Le commencement des observalions definitives avec les differents appareils a ete
retard6 par plusieurs causes; il nous importait surtout de pouvoir faire oseiller tous les
pendules avec les m&mes couleaux et sur les m&mes supporis. M. Repsold nous a dejä fourni,
il ya plusieurs mois, quelques couteaux en agate applicables ä tous les pendules; par contre,
le support que construit le m&canicien de l’Institut, M. Fechner, n’est pas encore comple-
tement termind. Le comparateur exige aussi encore quelques adjonctions ; toutefois j’espere
eire en &tal de complöter ces renseignements & la prochaine Conförenee generale.

9. L’elude des mires en bois n’a pas fait de progres ces derniers temps par suite de
differents emp&chements de M. le D' Stadthagen (voir Comptes-Rendus de Lausanne, p. 17,
18 et 70); mais ce dernier a, avec mon consentement, publie les re&sultats de ses observa-
tions dans les Annales de physique et chimie, 1897, T. 61, p. 208-294. On a röserve la
discussion et la communication des r6sultats complets pour l’Associalion geodösique inter-
nationale. L’appareil a impregner le bois (Comptes-Rendus de Berlin, p. 26) a ete deerit
dans le Beiblatt zur Zeitschrift für Instrumentenkunde, 1897, Heft 16.

B. Activite administrative.

1. Le Fonds de dotation pour 1887-1896, de meme que le nouveau Fonds pour
1897-1906, ont et& geres comme d’habitude. En reservant la Justification de l’emploi de la
dotation qui, d’apres l’art. 7 de la nouvelle Convention, sera publide dans les Gomptes- Rendus
de la Conference generale, je prends la liberte de vous communiquer ce qui suit sur
l’etat de situation et le mouvement des Fonds.

— 183

 
