!
i
3

 

 

I AR

UFIninen Bm inne er

n'Imr I RTTaNAmR

47

Bureau in Breteuil eine Station zur Vergleichung der Pendel-Apparate zu errichten, zur
Ausführung gekommen sei.

Ilerr Forster, als Präsident des internationalen Maass- und Gewichts-Comites, erklärt,
dass die Einrichtungen für die betreffende Station in Breteuil so weit vorgeschrilten seien, dass
dieselbe in kurzer Frist in Thätigkeit treten könne. Indessen fragt er sich, ob die neuen ver-
feinerten Pendelmessungen, an denen sich auch das Centralbureau betheiligt, nicht eine gewisse
Aenderung der früheren Anschauung der Sache herbeigeführt haben. Er glaubt, dass die Ver-
gleichung der Intensitätsbestimmungen an verschiedenen Punkten durch differenziale Pendel-
messungen ausgeführt werden könnte. Die Vereleichung der Apparate unler einander setze
jedenfalls eine sehr grosse Kenntniss der Besonderheiten der Theorie und Praxis der aller-
feinsten Pendelbeobachtungen voraus, so dass eine hlosse Vergleichungsstation der Pendel-
Apparate nichterschöpfend sein würde. Nachdem die verschiedenen Methoden und Instrumente
für die absoluten Bestimmungen in Potsdam genügend vorgeschritten sein werden, dürfte
die Bestimmung der Constanten für die wichtigsten Pendel-Apparate in einer (Centralstation
die ganze Arbeitzu einem erwünschten Abschluss bringen. Vorläufig aber, und bei der gegen-
wärtigen Lage der Sache scheine ihm die letztere Vergleichung an einer und derselben Stelle
noch nicht unmittelbar erforderlich zu sein.

Ilerr Hirsch füg! den Bemerkungen des Herrn Forster hinzu, dass einer der hauptsäch-
lichsten Gründe für die Verzögerung der Einrichtung für die Pendelstation in Breteuil in der
Entdeckung des neuen Metalls Invar zu suchen sei, da dasselbe nicht nur bei der Ausführung
der Apparate dieser Station selbst die grössten Dienste zu leisten verspricht, sondern auch
wegen seiner äusserst geringen Ausdehnung bei der llerstellung der künftigen Pendel-Apparate
eine hervorragende Rolle spielen dürfte. Uehrigens glaubt Herr Hirsch, dass man bei dieser
ganzen Frage vor Allem die absoluten und relativen Schweremessungen unterscheiden müsse.
Die ersteren würden jedenfalls immer in den einzelnen Ländern nur in einigen Haupt-Stationen
ausgeführt werden ; für die Bestimmung der Constanten und der Gleichungen der für die
absoluten Messungen verwendeten, stets sehr delicaten Instrumente wird die Station in
Breteuil jedenfalls die wichtigsten Dienste leisten. Einstweilen aber kann man die Schwere-
Netze der benachbarten Länder, welche zum grössten Theile auf relativen, mit Halbsekunden-
Pendeln ausgeführten Messungen beruhen, dadurch vergleichbar machen, dass die Beobachter
der einzelnen Länder mit ihren Apparaten die Bestimmungen der Schwere in geeigneten
Punkten der Nachbarländer wiederholen, oder gleichzeitig mit den dortigen Beobachtern
ausführen. So hat zum Beispiel die schweizerische geodälische Commission Herrn Dr
Messerschmitt mit einem unserer Sterneck’schen Pendel nach Wien und Padua geschickt, um
dort mit den Herrn von Sterneck und Lorenzoni die Bestimmung der Schwere an den be-
treffenden Punkte zu wiederholen. Derselbe wird künftig die gleiche Arbeit auch in Paris und
in Potsdam ausführen. Es wäre offenbar erwünscht, dass ähnliche Anschlüsse der benach-
barten Netze zwischen den betheiligten Ländern möglichst allgemein ausgeführt würden.

Herr Helmert hebt hervor, dass es bei den äusserst schwierigen Pendelmessungen
nicht nur auf den Apparat, sondern auch auf den Beobachter ankomme, da die Art, wie die

 
