a ee 7

 

ir!

VER N a m

l

n'r

 

 

— 141 —

vIl»s — Grande Bretagne (Inde).

 

 

 

 

 

 

No INDICATION £ DIRECTEURS
Dik poners LATITUDE LONGITUDE| EPOQUE Er On INSTRUMENTS. REMARQUES
60:| Shawalk ...... . 280 27' 24”) 69° 44' 36” 1859-60-61 | Keelan, Tieut. J. Herschel, | T. et S. 36".
lieut. H. R Thuillier.
61 | Näzir-da-posht ...., 28 33 59 | 69 39 17 1859-60 id, id.
62 | Daowäla.......... 28 20 13 | 69 50 30 1860-61 id. id.
63 1,.Mlanl. a... 28 34 15 | 69 50 46 1859-60 id. id.
64 | Kundri (old)...... Ve 1860 Keelan. Waugh 24" n® l.
65 | Kunäri (new) ...... 2821712383 69 57 51 1860 Herschel, Thuillier. 'T. 21 8. 36.
66 7 Biwarl- 0... 28 38 7695914 1860 Keelan, Herschel, Thuil- | Waugh 24" n® | et
lier. T.86.8.,30..
67 ! Madädaläri........ 2342 12.6952. 2 1860 Keelan. Waugh 24" n® |.
68. Sarhimı au... 28 33 33 | 70 5 40 1860 Keelan, Herschel, Thuil- | Waugh 24" n? 1 et
lier. T. ei.8. 00.
69.1 Mirapuınr :...... 28 4518 | 70.042 1860 Keelan. Waugh 24" n® 1.
70 | Chakarwäli........ Al 1091 1860 id. id.
71. | Kahirı 2.2.2... 28 49 22 | 70 11 14 1860 id. id.
73.\.-Lalgoshi- ......... 28 5259| 10 254 1860 id. id. .
73 | Shähpur.......... 28 43 16 | 70 1918 1860-61 Keelan, Herschel. | Waugh 24° ne l et
T. et.854 80%
74 | Hamidpur......... 28 57. 3 10 1139 1860 Keelan. Waugh 24" n® 1.
75-4088: ....:20.. 28 53 39 | 70 20 22 1860-61 Keelan, Herschel. Waugh 24" n° | et
DT: e08. 30.
7621. Dagoss..2...0020. 2% 725,022 8 1860 Keelan. Waugh 24" n® ].
77 | Gulshera.......... 293 5197013 Al 1860 id. id.
78: Tarlas a... 28 58 29 | 10.205595 1861-60 Herschel, Keelan. T. etS. 36" et Waugh
2A. ne L
79:.\| Ismail 2... 291 98 | 701959 1860 Keelan, Herschel, Thuil- | Waugh 24" n° | et
lier, lieut. J. P. Basevi. | T. et S. 36.
80 } Gapola.......... 29 818 | 70 29 45 1860 Keelan, Thuillier, Herschel. id.
8j | Banjıwar . ....... 2848 2] | 70 29 21 1861 Herschel. T. ob.8. 25%
89 | Bälgarl... ...... 28 38 58 | 70 29 42 1861 id. id.
83 |.Chaharlar..... .;. 28 52 53 | 70 38 24 1861 id. id.
84 | Kaluwäll, 2... 28 43 19 | 70 39 31 1861 id. id.
85 | Gangan...... >29 171 6| W226 1860 Basevi, Herschel. id.
86-1 Hajipar........ 29 21 16 | 70 19 38 1860 id. id.
87 | Thal Megräij...... 29 15 36 | 70 38 18 1860 Herschel, Thuillier. id.
gg | Islampur... =... 2320 2 0.20 20 1860 Basevi, Herschel. id.
so. Jalmala:.,......- 29 24 28 | 70 34 38 1860 id. id.
90 | Kambar Shäh ..... 29232 0| 032331 1860 id. id.
91.7, Dal... .... 2. 29 3321 | 902232 1860 Basevi, Herschel, Thuillier. id.
92 | Sher Jatoi.......- 29 29 15 | 70 40 50 1869 Basevi, Herschel. id.
93: ! Dalura..23...22... 29 38 41 | 70 33 14 1860 Basevi, Herschel, Thuillier. id.
94 | Din-Kä-Kotla...... 29 37. 32 | 043 22 1860 Basevi, Herschel. ; id.
95..Jhakanr: 2... 2. 29 46 40 $ 70 43 25 1860 Basevi, Herschel, Thuillier. id.
96 1: Tobwala . -........ 29 49 45 } 70 34 36 1860 id. id.
9%. Abein 2.2.0.0 5 29 43 10 | 70 52 12 1860 Basevi, Herschel. Sn id.
98 | Naharwäla........ 29 56 20 | 70 40 40 1860 Basevi, Herschel, Thuillier. id.
99 | Bhutewäla........ 29 53 53 | 10 50 16 1860 \ id. id.
100. 1.-Dorata.. - :: 2... 30. 2.321520. 47.92 1860 Basevi, Thuillier. Es id.
101.) Hwals. 2... 30.42.61 70.38 29: 1860 Basevi, Herschel, Thuillier. id.
102: | Mara... 02... 2. 30 141) 30 90 16 1859 Basevi. 2 id.
[03 | Hotwalaer 2... 30 11 29 \ 70 44 32 1859-60 Basevi, Thuillier. id,
104 | Khemyäla ........ 30 9 46 | 70 56 46 1859 Basevi. I id.
105 | Mohana...-.: ».-- 30:17. 30.170.546 | 1859 Basevi, Thuillier. id.
106 | Guhman..........- 30 20 29 | 70 42 21 1859 id. id.
107°) Mahiwäla- .-...... 30 13548 1-11. 251 1859 id. id.
108 | Abbaswäla.......: 30 2418| 71 235 1859 id. id.
109 | Näzichand......... 30 25 59 1 70 52 13 1859 id. id.
110 | Khandikot ........ 30 27 28 | 70. 41:20 1859 id. id.
111: Gad.as.2. 05 ..| 30 34 56 | 70 45 35 1859 id. id.
112 | Dera Din Panäh...| 30 34 2 | 7056 6 1859 id. id.
113 30 43 37. | 70 51 291 1859 id.

 

| Pr

 

 

 

 

 

 

 

Basevi.

 

 

 
