 

|
!
!

ame Kl

Une

ER I Li

Mei

 

409

 

Une troisiöme experience est en ce moment en cours d’
et Mi-Amiata (115 kilometres). On 6erit que le m
d’apercevoir la lumiere du projecteur, mais cependant
gaz et le fonctionnement de tous les appar

execulion entre Mt-Senario
auvais lemps n’a pas encore permis

on a constale que la production des
eils se font tres r&gulierement.

On est done autorise A esperer qu’on pour

ra, au printemps prochain, commencer les
operations de rattachement.

Stultgart, 7 octobre 1898.
F. GuarDuccı.
Note suppl&mentaire du 25 novembre 1898.

Je viens complöter mon rapport en communiquant les rösultats obtenus dans
rience qui a eu lieu avec le nouveau projeeteur entre Mt-Senar

qui dtait en cours d’exdceution lorsque j’ai donn& lecture du r

l’expe-
io ei Mt-Amiato, experience
apport meme.

A Mont-Senario on avait plac& le nouveau projecteur
d’une Junetie & 0"40 d’ouverture ; A M!-Amiata, une |
a laquelle on avait adapte une source lumineuse OXY-

a gaz oxy-acötylönique pourvu
ampe (collimateur systeme Lepante)
acetylenique pareille A la precädente.

Dans la soirde du 4 octobre, la station de Mt-Amiat
miere blanche, espaces de deux minutes, en brülant, pour
de mdlange (magnesium ei chlorate de potasse).

aa produit deux eclairs & lu-
chacun de ceux-ci, 19 grammes

Au bout de 5 minutes, Mt-Senario a repondu & ces signaux par un 6clair A lumiere
rouge obtenu en brülant 24 grammes de melange (magnösium et chlorate de strontiane).

Tous ces signaux ont &t6 apercus A l’eeil nu par les deux stations et, dans les
ces Eclairs paraissaient sous forme de fl

une pelite distance de l’objectif.

lunettes,
ammes presque globulaires d’une chandelle placde A

Quelques minutes apres la production du dernier eclair, on a mis en fonction les
sources lumineuses, et les appareils ont 616 apercus r&ciproquement par

les deux stations,
de sorte qu’on a pu 6tablir la correspondance telegraphique.

A Mi-Amiata, la lumiere envoyee par le grand projecteur de Mt-Senario se presentait,
a Peeil nu, comme un phare de premier ordre, de sorte que les telegraphistes ont pu rece-
voir les signaux sans l’auxiliaire de la lunette.

A la Iunette du th&odolite (47mm d’ouverture et 35 de grossissement), cette Tumiere
S’apercevait avec les dimensions apparentes de la planete Jupiter.

A M'-Senario la lumiere du projecteur Lepante de Mt-Amiata n’6tait pas visible A l’eeil
nu, mais dans la lunette du grand projecteur elle presentait l’apparence d’une &toile de Tre

grandeur et dans la lunette d’un thäodolite analogue au precedent, d’une &toile de Zme ou
Ame grandeur.

 
