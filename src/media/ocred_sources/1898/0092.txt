 

 

 

en

VIII. Suisse. Invits par lintermediaire de M. le Prof. Hirsch:
1. M. le professeur Raoul Gautier, Directeur de l’Observatoire de Geneve,
secrötaire de la Commission geodesique suisse.
Pour ne pas trop occuper le temps de la Conference, toujours assez restreint pour
nos delib&rations, le Secrötaire se borne A ajouter encore quelques renseignements concer-
nant le personnel des delegues de l’Association.

Nous avons ä döplorer la perte de deux de nos anciens collögues des plus distin-
gu6es, que la mort a enleves & la science depuis la Conförence de Lausanne. La nouvelle du
d&ees si inattendu du cel&bre astronome Tisserand, qui a consterne non seulement la France,
si justement fiere de l’&minent directeur de ’Observatoire de Paris, mais les savants du
monde entier, qui appreciaient hautement la valeur de ce grand theoricien de la mecanique
cöleste, nous est parvenue le 20 octobre 1896 pendant la r&union de Lausanne, qui a exprime
immediatement par lölegramme ses sentiments de condoldance & la famille du defunt. Je suis
sür d’etre Porgane de la Conference generale unanime, en renouvelant ici le t&moignage du
profond et general regret que la perte si pr&maturde du grand astronome francais a fait
&prouver ä ses anciens collegues de l’Assoeiation. Car, si Tisserand n’a pas fait de la g&od6sie
Pobjet principal de son infatigable activite, les liens qui unissent les deux sciences sont telle-
ment &troits et nombreux que la g&od6sie se ressent n&cessairement de la disparition d’un
des plus glorieux travailleurs qui a accompli de si grands progres dans l’astronomie moderne.
L’Associalion geodesique internationale restera toujours fiere d’avoir compte Tisserand au
nombre de ses coop£rateurs.

Au mois de mars 1897, nous avons perdu un autre collögue, le D’ Ch. Schols,
membre de l’Academie royale, qui professait les sciences geodesiques A l’Ecole polytechnique
de Delft. Schols appartenait depuis longtemps, comme del&gu& neerlandais, A notre Associa-
tion, qui a profite largement de ses travaus, el il a assiste ä& de nombreuses assemblees
annuelles, dans lesquelles nous avons tous pu apprecier le jugement sain de ce savant dis-
tingue et la correction de cet aimable collegue.

J’ajoute encore que M. Karlinski a prevenu le Secrötaire, par lettre du 2 septembre
dernier, qu’il est emp£che, par un deuil de famille et par la maladie, de prendre part & la
Conference. Vous regretierez avec moi l’absence de notre collegue autrichien et en parlicu-
lier les tristes causes qui la motivent.

Ensuite, M. le General Zachariae m’a avise par une carte du 95 septembre que,
retenu ä Copenhague par le service militaire jusqu’au soir du 8 octobre, il craint d’arriver
trop tard pour participer utilement aux Lravaux de la Conference. J’ai immediatement repondu
par depöche telögraphique & notre collegue que les seances de la Conference dureraient pro-
bablement jusqu’au 42 octobre et je l’ai engage & venir si possible a Stuttgart, de sorte que
nous pouvons encore esperer le voir assister a nos dernieres s6ances et A entendre de sa
bouche le rapport toujours si interessant sur les travaux danois.

 

l

a ae a Marllähıa

a

ua Inn |

5
1
|
1

 
