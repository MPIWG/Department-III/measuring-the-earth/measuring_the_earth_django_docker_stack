 

N
ü
IE

 

ART

TR gen

mi jPir

949

L’huilage & chaud parait cependant restreindre d’une maniere sensible la variahilite

du coeffieient thermique d’allongement quand ’humidite varie (voir fig. 11 et tableau V
ei-apres).

INFLUENCE DE L’HUMIDITE

Allongements par melre, ü une iemperalure vorsine de 15°,
de diverses rögles experimentees, en passant d’un air sec (elat hygrometrique, 214,5)
a un air lumide (lat hygromelrique, 739).
- (Resultats extraits du tableau III, avant dernier groupe de colonnes.)

Allongements
par metre.

cum. emm
200

200

 

150 150

100 100

50 50

 

0 0
Reglesn”® 1 28005 6 18 7.202.26 22
Bois resineux Bois non resineux
| ee Bois naturel.
ERCENDE = Bois peint.
er Bois huile.
Bier d:

 
