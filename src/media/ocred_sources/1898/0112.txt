 

Zumichn astlali.iniog or8,7 zu ae,
Gähris-aioa us 12 ..0=6947 4 0,2
Pandn ....: 8 _ 8)
Weissenstein. - — 91 + 40
Bee 7 40
Neuchätel. . - — 13,9 - 15,3
Geneye KU. . 06 7192,8 + 0,0
ee... "2 20,0
nn — 1,7
Bros nontimmil 8359 + 3,1

Il convient de signaler dans cette liste les grandes deviations relatives en latitude de
Milan par rapport au Simplon et le tr&s grand &cart en longitude de Turin par rapport A
Milan, Gönes et Nice.

Avce ces rösultats, on a termine provisoirement les caleuls qui ont &i& execules A
partir de Bonn, point de la mesure de l’arc de longitude en Europe, vers le sud et le sud-
ouest. Avant de procöder & la compensation de toutes ces donn&es, on a l’intention de publier
les rösultats pour les differentes lignes dans la forme adoptide pour la mesure de V’arc de
longitude sous le 52° parallele. De cette maniere, on pourra toujours facilement tenir comple
des lgöres modifieations qui se produiraient peut-Eire apres coup dans les donnees astronO-
miques et göodesiques employees.

On a de m&me avanc& les calculs de deviation dans le möridien de Vienne au sud
de la Schneekoppe, point de la mesure d’arc de longitude en Europe sous le 52° parallele.

Dans ce travail, nous avons 616 utilement second&s par M. le colonel von Sterneck,
qui nous a communique surtout les mesures d’angles destindes A relier les points de Laplace
Laaerberg prös Vienne et Pola (Observatoire) aux triangles de la chaine meridienne de Vienne,
deja compensee et publice. Sur la base de ces mesures et d’autres donndes de&ja imprimees,
nous avons röalise la jonction de Dablie (pres Prague), Laaerberg et Pola, travail dans
lequel il a fallu ex&cuter des compensations avec 3, 13 et 8 &quations de condition. Gomme
erreurs moyennes des directions compens6des dans ces stalions on A trouve les valeurs :

210 0, 0m.
Ensuite on a deduit les lignes :

Schneekoppe-Dablic,
Schneekoppe-Laaerberg,
Laaerberg-Pola,
et comme contröle
Dablic-Laaerberg,
en mö&me temps que leurs &quations de d&viation. Dans le calcul de ces lignes on a utilise
autant que possible beaucoup des nombreux points de cette region dans lesquels on n’a de-

 

Iıdidi ulk

ne Hd Marlldde

u had.

ab til

 

;
N
|

 
