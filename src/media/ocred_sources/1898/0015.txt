 

en

 

mu RT

Ta N gi!

Im

nr

7

Technischen Hochschule, gewissermassen in der Eigenschaft als Hausherr, Sie willkommen
zu heissen und Ihnen bei Beginn Ihrer heutigen Conferenz in alter schwäbischer Sitte ein
herzliches « Grüss Gott » zuzurufen. Wenn wir es auch nicht wagen dürfen, den Glanz der Ver-
anstaltungen, die Ihnen bei Ihren Tagungen in den Hauptstädten der Welt, Paris, Rom,
Berlin, Wien und anderen, entgegengebracht wurden, zu erreichen, wenn auch unser kleines
Land in seinen Mitteln beschränkt ist gegenüber grösseren und reicheren Staaten, so glaube
ich doch die Hoffnung aussprechen zu dürfen, dass es Ihnen in unserem Schwabenlande recht
behaglich und wohl sein dürfte, und dass wir durch Herzlichkeit und Gemüthlichkeit das
ersetzen, was etwa an Glanz und Pracht uns fehlt. Ganz besonders aber ist es die Technische
Hochschule, welche an den heute beginnenden Verhandlungen das grösste Interesse nimmt.
Hat sie doch von Anfang an durch Entsendung von Mitgliedern ihres Lehrkörpers als Dele-
gierte zunächst der europäischen Gradmessung, dann der internationalen Erdmessung in einer
dauernden Wechselwirkung mit Ihnen gestanden. Sind auch die Träger von so manchen
Namen von gutem Klang, wie Bauer, Schoder, Zäch, die einstens unsere Hochschule zierten
und die an Ihren Verhandlungen mitzuwirken pflesten, leider nicht mehr vorhanden, so sind
dafür neue Kräfte in die Lücken getreten, die nun ihrerseits bestrebt sind, an der Lösung
der schwierigen und wichtigen Aufgaben und Arbeiten, wie sie von Ihren Conferenzen
behandelt werden, nach Kräften mitzuwirken.

« Welch grosses Interesse Seine Majestät unser König an der Förderung aller Wissen-
schaften nimmt, das ist Ihnen ja soeben aus dem beredten Munde unseres Herrn Kultusmi-
nisters Excellenz Sarwey kundgegeben worden. Wie sehr sich aber auch unsere Staatsregie-
rung und an ihrer Spitze wieder unser Kultusminister es sich angelegen sein lässt, die
immer anspruchsvoller hervortretenden Bedürfnisse der modernen Wissenschaft zu befriedigen,
das bezeugt die Entwickelung unserer Technischen Hochschule, die noch immer nicht
abgeschlossene Vergrösserung derselben durch die Errichtung neuer Institute. Die Besichti-
gung dieser Institute ist zwar nicht in dem Programm enthalten, das die Sehenswürdigkeiten
von Stultgart aufführt, aber ich glaube kaum hinzufügen zu müssen, dass es mir und
meinen Kollegen die allergrösste Freude bereiten wird, wenn sich Gelegenheit bietet, dem
einen oder andern von Ihnen, die sich für diese interessieren, als Führer bei einem Besuche
dieser neuen Institute zu dienen. Ich möchte diese Freude noch mit um so grösserem Nach-
druck aussprechen, als dadurch Gelegenheit gegeben wird, dass Sie die Deberzeugung nach
Hause nehmen, dass, so klein unser Schwabenland ist, in der Pflege des Unterrichtswesens
und ın der Förderung der Wissenschaften es sich mit jedem anderen Lande messen kann.
Ich glaube daher aussprechen zu dürfen, dass die Umgebung, das Milieu, in welchem die
XII. Generalconferenz der internationalen Erdmessung stattfinden wird, im Allgehräiheh kein
ungünstiges ist. Und so darf ich daher wohl den Wunsch aussprechen, ua Ihre Berathungen
zu einem glücklichen Ende geführt werden möchten, dass Ihre auf schwäbischem Boden
gefassten Beschlüsse eine Förderung und neue porsekniie der exaktesten von allen Wissen-
schaften anbahnen möchten, zum Segen und Nutzen der ganzen gesilteten Welt.

Herr Professor Dr. von Brill hält folgende Ansprache :

STETTEN

 
