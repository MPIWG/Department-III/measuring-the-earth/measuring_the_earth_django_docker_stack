 

 

I — Autriche-Hongrie.

 

INDICATION

 

 

 

 

 

   
  
  
   
    
  
  

 

 

LATITUDE |LONGITUDE| KPOQUE a | ST
DES POINTS a Di ET OBSERVATEURS | Solnıs
‚Spianjoch.......... a 2: 1853 Breymann. | Starke n° 4 de 10 p.
‚966 | Glockthurm ....... 46 53 39 | 28 19 52 1853-54 Breymann, Ganahl. | Id. n® 4 et6de 10 p.
7 | Hohe &eige........ 41 02] 283427 1853 Ganahl. Starke n° 6 de 10 p.
| Habich Sp...... = 0,47 2411285719 1853 id. id.
Gr. Kreuz Sp...... | 46 51 21 | 28 52 48 1853 id. id.
Hochwildspitze.....| 46 45 59 | 28 41 16 1854 id. id.
 Kraxentrag ....... 41 023.29 15.20 1852 Feuerstein. Starke de 10 p.
oe 4052 16 209 25 17 1883 Ritter. Starke n° 2 de 8 p.
Birkenkogl........ 46 40. 52 | 29 55 22 1883-84 Rehm. 10, ne I de 10 p.
| Marmolada ........ 1884 id. id. ’
Boen............ 46 21 41 | 28 51 28 1883-84 Englisch, Hartl. Id. n22 et3del0p.
Sasso rOSS0........

Bla. 2... 46 048 | 28 14 36 1884 Rhem. Starke n° I de 10 p.
Balıo .. ..n...;.. 45 42 24 | 28 29 46 1884 Hartl. Starke n° 3 de 10 p.
Pasubio........... 45 47 35 | 28 50 32 1884 id. id.
Brappa............ 4a 532 21,29 27 53 1853 Nemethy. Starke n® ] de 10 Di
'Cima @Asta....... 1883-84. | Ritter. Starke n° 2 de 8 p.

ei ne 2 de 10. p.
Base de Kronstadt. | 1885 Rehm. Starke n2 de 8 p.
' | Terme Sud-Est.
| Base de Kronstadt. 1885 id, id.
Terme Nord-Ouest.
Lindenbusch........ 1885 id. Id
Schlossberg. ....... 1885 id. { id.
Poskien........... .1885 Schwarz. Starke n° 5 de 10 p.
| Cserna gora....... 1885 id. id.
Wieszi wielki...... 1885 id. id.
Skoroset .......... 1885 id. id.
Iesorm........... 1835 id. id.
Brusturossa ....... 1885 id. id.
Bliznica .......... 1890 Zinauer. Starke n°’4de10p.
| Siroky vrh........ 43 424 | 40 57 42 | 1890-97 | Rehm, Schwarz. Starke n°] de 10 p.
et n’517de8p.
Fenyöhegy...... 400413114) 1413 1890 Truck. Starke n° 1 de 8p.
Sennik..:........:. 1890 Schwarz. Starke n’5de 10 p.
Nleneul.......%... 43 12024] 2051 1890 Zinauer. starke n°4de 1Op.
Vstri vrh.......... 48 15 17 | 40 54 10 1890-97 Rehm, Schwarz. Starke n° 1 de 10 p.
etn’517de8p.
Kamionka......... As 393,31 | A] 17.47 1890-97 Bersa, Guthmann. Id. n°2 et5de 10p.
SERomla ......... 1890 Zinauer. Starke n°4 de 10 p.
Eop Ivan ....,.:... 1890 Schwarz. Starke n°5 de l1Op.
Oröghegy......... 46 34 37 | 35 45 12 1895 id. Starke n°3del0p.
Retschek Berg.... 47 0 8/3531 3 189 _ id. 1.
Orög Futone...... 4, 13 55 55 Al 58 1895 Truck. Starke n°2 de 10 p.
Höröshegy........ A, 112 52916 1895 id. id.
Saghesy .......... 1895 Rehm. Starke n° I de 10 p.
MDSa. en. 1894 Rehm, Pisinoff. id.
Bleswa ......... 1894-95 v. Filz, Mayer. Id.n°4et5delOp.
Moldavica......... 1894 ve Bla Starke n’4de10p.
Antina Livadia.... 1894-95 Rehm, v. Filz, Mayer. Starke n® 1, 4 et 5
de 10 p.
Dumacsa.......... 1894-95 Rehm, Schwarz. id. n° let2de1l0p.
Kudritzerkopf..... 1894-95 | v. Filz, Mayer. Id.n°4et5delOp.
Retisovra.......... 1895 Schwarz. Starke n°3 de 10 p.
. Lagerdorf......... 1895 id. id.
Base de Werschetz
(Terme Ouest)... 1895 id. id.
Base de Werschetz

dierme Est) ..... 1895 id. id.
Skamnie.......... 1894 id Starke n’2 de 10 p.
Mosura.......... 1894 Scharf. Starke n’5de 10 p.

 

 

 

 

 

 

 

 

 

|

|
|
|

|

|
|

 

 

 

 

 

—

REMARQUES

|| Italie,

id.
id.

‚Id.

Habe,
