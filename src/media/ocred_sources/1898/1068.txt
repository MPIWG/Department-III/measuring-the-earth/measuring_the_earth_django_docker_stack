       

— 352 —

XIV. —Prusse, B,

 

INDICATION

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

a 2: DIRECTEURS i |
N° Dis PoInTs LATITUDE |LONGITUDE | EPOQUE Er or ey renes INSTRUMENTS | REMARQUES
1
Thüringisches Dreiecksnetz,
221 | Dohra............ 50° 16’ 46" | 29° 18’ 41" ; g | _ Veröffentlichung
388 | Stelzen............ 50 29 22 | 29 37 5| & | Ss | m & | 3-12 Sachsen, N. 3.
Bu Beim... 50 41 18 | 29 26 ı1 © 2 | 7 | id. N.6.
360 | Kuhberg.......... 80.306 1 1.2953 34 | 2 & | zZ = |
| m =
Rheinisch-Hessischg Dreieckskette.
353 | Gr. Gleichberg....| 50°23'22"| 28° 15' 37" 1) [
283 | Inselsberg .....:..|5051 9 | 28 8 4 |
284 | Meissner.......... ol 13-46 | 27 31 52 | |
al a 1 DE 30,55 41927523 | || Geod. Inst. N. 41.
362 | Kreuzberg ........ RR 10.27 88 53 | Baiern, N. 73.
363 | Taufstein .......... 0831 6 26 54 22 | id. N.106, Geod.Inst.
364 | Steigekoppe....... 80 2 37 | 26 58 25 | N.
365 | Felelberg......... 0 14 1 26 730 | id.N.35, id. N.14.
366 | Meliboons......... 49 43 33 | 26 18 14 | id.N.75, id. N.20.
367 | Donnersberg....... 40 81 33 | 25 35 39 | id. N.116, id. N.19,
368 | Lufdenberg ....... 50.016 25.17 25 |
369 | Erbeskopf ........ 49 43 54 | 24 45 29 | Good. Inst. N. 18.
370 | Loeberg .......... 49 44 58 | 24 14 12 - = I
27 | Muxerath......... 2945| 2356. 2 = 3 = |
372 | Weisser Stein..... 233 A218 7 3 = |
373 | Michelsberg....... 80 30 50 | 24 29 30 & 2 > | IESENS:
374 | Hohe Acht........ 50 23 14 | 24 40 42 © S o | Noch nicht veröffen
375 | Prümscheid ....... E00 8 | 24 22 11 = z Eee
376 ı Langschoss......... 0240 3 | 235722 Geod. Inst. N. 6.
377 IEENDR........... 0 47 16 | 24 32 5
378 | Mündt.........:.. 1122| 24 648 |
819-| Velbert........... 91220. 1 724 41:56 |
380 | Hinsbeck ......... »1 29.9 | 2356 56 |
381 | Fürstenberg. ...... 51 3858| 24 829 ® |
382 | Stimmberg........ DL AU 2 | 24.55.27 |
383 | Balverwald ........ DE 21 23 | 25:29 47 |
318 | Billstein.......... 01 20° 2.1.26 17 43 |
316 | Soester Warte..... 51 4 92543 36 |
814. -Notmin .:.......... 157 2 25 AA
|
Basisnetz bei Bonn,
384 | Basis (nördlich) ...| 50°46' 4"| 24° 4 "| | Noch nicht veröffen-
385 | Basis (südlich)... 50 45 8 | 24 44 44 | | tlicht.
386 | Bergheim. ........ 50 46 48 | 24 45 37 | 3 | ;
387 | Gielsdorf ......... 50 43 25 | 24 40 53 N | S | = |
388 | Löwenburg........ 50 39 55 | 2455 4 2 2 | 2 |
373 | Michelsberg. 50 30 50 | 24 29 30 3 7
377 | Birkhof....... =, 00047 1062432 5 | = |
Süellicher Niederländischer Anschluss.
313 | Bentheim ......... 520 18'15"| 24e 49! 29" | s | o Noch nicht veröffen-
314 | Nottuln ........... BD 2 5 44 5 | = | = ne
382 | Kimmberg ........ 51 40 2 | 24 55 27 S < | 3 |
381 | Fürstenberg....... DI 3808| 24 8 29 = 2 | =
380 | Hinsbeck ......... 51 20 © | 23 56 56 = S | =
| °©
| A
