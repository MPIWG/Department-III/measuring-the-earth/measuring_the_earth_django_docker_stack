 

 

— 368 —

   

 

XIV.—Prusse, B.—Erreurs de clöture des triangles desquels ont &te mesur6s les trois angles.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

NOMBRE |
. LECTURE : DES | ERREURS |

: AUTEURS DES INSTRUMENTS is NOMBRE | OBSERVATIONS | DE GLÖTURE |

5 EMPLOYfS oder DES | Exkcunkes | DE |

S ET ne VERNIERS | POUR | CHAQUE 4 | REMARQUES

5 DIAMETRE DU CERCLE AN, OU DES ,ÜHAQUE ANGLE| TRIANGLE | |

£ HORIZONTAL ı MICROSCOPES | Se | % | |

i MIcRoscoHE | |  cHaquE | | |
| DIRECTION | u. Fe |
| | |

15 | 10 zölliger Theodolit N. 1 von Pistorı Ablesung herarköp-! 24 10%, 226 | | 0,0511 | Veröffentlichungen
und Martins (Theilungsdurch- | mittelbarvon | Mikrometer. | | | EN.
messer — 355®®) und die 10 zöl- | '/,sekunden. | | | |
liger Universal Instrumente N. I | |
und II von Pistor und Martins | | |
(Theilungsdurchmesser 265"), | | | | |

Bu id. id. id. 24 | [0”,694 | 0,4816 |

17 id. id. id. 24 BI 247 | 4,5550 |

18 id, id. id. 24 | > ds; ern |

19 id. id. id. 24 |0 ‚613| 1:8,3758

20 id. id. id. 24 1,7276; 2,1542
21 id. id. id. 24 | I0 ,716| 0,5127 |
22 id. id. id. 24 10 ‚692 | | 0,4789
23 id. id. | id. 24 1,280, 1 3625
24 .“ id. | id. 24 |1 ,366\ , | en
25 id. id. | id. 24 |1 ,623| 2,634]
26 id. id. | id. 24 Io ,2nı| | 0,0734
27 id. id. | id. 24 10 „148 | | 0,0219
28 id. id. | id. 24 ı0 ‚544| 0,2959
29 id. id. | id. 24 |0 ‚356 | 0, 1267
XV.
SCHLESWIG-HOLSTEINSCHE DREIECKSKETTE 1869.

I !15zölliger Theodolit von Ertel (Thei- Ablesung| 4bezw2Mi-| 24 | 107,371] 0,1376 | Veröffentlichung
Jungsdurchmesser = 355"®) sowie | mittelbar von | kroskop-Mi- | | | >
die 10 zölligen Universal Instru- | '/, sekunden. ı krometer. | |
mente N. 1 und II von Pistor | | |
und Martins (Theilungsdurch- | | | |
messer — 265"), | | | | |

2 id. id. id. 24 | F1 5241) 2,3226 |

3 id. id. id. 24 | 1”, 464 | 2, 1433

4 id. id. id. 24 j0 ‚all 0,0967 |

> id. id. id. 24 10,554 0, 3069 |

6 id. id. id. 24 10 ‚270 | 0,0729 |

1 id. id. id. 24 | |0 ,788| 0,6209 |

0 i I. in 4 | or0ss| 0.00

1d, 1d. ld. | 9, „uvao |
10 id. id. id. 24 [1 ,281| 1,6410
11 id. id. id. 24 |1 ,004| 1,0080 |
12 id. id. id. 24 0,345! 0,1190 |
13 id. id. id. 24 0 ,245| 0,0600 ı
14 id. id. id. 24 0,640 | 0,4096 |
15 id. id. id. 24 0,295) 0,0380 |
16 id. id. id. 24 0 ,851| | .0,7242 |
17 id. id. id. 24 |1,699| 2,8866 |
18 id. id. id. 24 |0 ,026| 0,0007 |
19 id. id. id. 24 0 ,093| .0,0086 |
20 id. id. id. 24 |0 ,„486| 0,2362- |
% E id. id. 24 |1 ,201) 1,4424

id. id. id. 24 0,818 | | 0,6691 |
23 id. id. id. 24 0 ,.35411:.0.1253 |
24 id. id. id. 24 0 ‚957 | | 0,9158 |
25 id. id. id. 24 054179] 1 0,0306 ı

ins ar hm a ah Äh a en
