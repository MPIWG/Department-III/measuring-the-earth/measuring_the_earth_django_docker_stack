ae u

 

 

.- 0

XII. — Prusse, A.

 

MECKLENBURG.

Vorbemerkung.

Als General Baeyer i in den Jahren 1839- 40 mit seinen Beobachtungen für die Küsten-
vermessung Mecklenburgisches Gebiet erreicht hatte und bei dieser Gelegenheit die
starke Fehlerhaftigkeit der bis dahin daselbst vorhandenen Karten erkannte, legte er
den Regierungen der Grossherzogthümer Mecklenburg-Schwerin und Mebarg Strelitz
einen Entwurf zu einer trigonometrischen Vermessung des Landes vor. Im Jahre 1853
wurde den Vorschlägen Baeyer’s Folge gegeben und eine Commission für die Landes-
vermessung eingesetzt. Während der Jahre 1853-1860 wurden sodann durch die Landes-
vermessungs-Commission unter der wissenschaftlichen Leitung des Mecklenburgischen
Vertreters bei der Europäischen Gradmessung, F. Paschen, die Winkelbeobachtungen
für das Dreiecksnetz von Officieren nach der Bessel’ a Methode zur Ausführung
gebracht. Zur Verwendung kamen 2 zehnzöllige Universal-Instrumente von Pistor und
Martins. Auf einigen Anschluss-Stationen wurden auch Winkelmessungen der Küsten-
vermessung und der Dänischen Gradmessung mit herangezogen, während für die Sta-
tionen Höbeck und Siek nur Preussische bezw. Dänische Beobachtungen zur Verwen-
dung kamen. Da eine Grundlinie nicht gemessen wurde, schloss man das Dreiec} zsnetz
an 6 Seiten der das Land umschliessenden Preussischen Dreiecksketten ' Küstenvermes-
sung” und ” Elbkette ” an. Bei der Netzausgleichung wurde auf diese Identität der Seiten
keine Rücksicht genommen, sondern es ist nur bewirkt worden, dass die Differenzen
in den Logarithmen der 6 Anschlusseiten möglichst klein sind und die Summe Null
ergeben.

Zur Zeit des im Jahre 1873 eingetretenen Todes von Paschen waren die Rechnungen
noch nicht ganz vollendet ; ihre Fertigstellung und die Veröffentlichung der Resultate
wurde durch die Herren En lapr Köhler in Schwerin, Professor Bruhns in Leipzig
und Professor Foerster in Berlin bewirkt (Siehe: Ge herzoskich Mecklenburgische

 

darettahege hl

rn hd hr Ad Aa Me

nn a
