 

   

   
     
       
     
       
       
 

LA BASE MESUREE | ERREUR

————— — | MOYENNE TOTALE |
| DE |
|
|

 

a N | GA BASE OALOTDNm

 

ERREUR | pm LA BASE GALCULER |
MOYENNE a I a | REMARQUES
CERETT — < | BT rn \ I|
DE | ||

 

A MESURE | EN UNITGS. _ EN UNITHS|
LA MESURE | my parrtıns |, ONITES| mN PARTIES |
D'UN ANGLE a DU a DI
DE LA DE LA aa
aa le ORDER onsomin: | 7. ORDER
\ NGUE R N JEUR | r e
| le Hu Log. | Sn  DU:D0G,

Sg

         
 
 
       
       
      

 
 

während bei den Basisnetzen N.6 bis 8 der Zusammenhang zwischen Kette
und Netz allein durch die gemeinschaftliche Seite — d. i. die angegebene
abgeleitete Dreiecksseite — stattfindet. Nur bei diesen letzteren ist daher x
hinsichtlich der Auswahl der abgeleiteten Dreiecksseite weder ein Zweifel
noch eine Willkür möglich.
| | (?) Die mittleren Winkelfehler sind bei Nr. 1 bis 3 aus den Dreiecks-
| 1 | schlussfehlern (« Rapport sur les triangulations par le General Ferrero »), bei
6.8 | = 9.9 Nr. 4 bis 8 aus den Ausgleichungen der bez. Ketten oder Netze unter Re-
440 000 ? duetion auf das Normalgewicht 12 eines Winkels — eine einfache Rich-
 tungsbeobaethtung oder doppelte Winkelbeobachtung als Gewichtseinheit
| angenommen — berechnet.
|) Ba95794 7 ö° 40 | 1 | 64 | : ji 9.6 Di Dan 6 ‚bie = Mu die a l> des Beobachtungen
A, 5309194. I R en ; —— ’ abweichend von dem normalen Verfahren so festgesetzt, dass für die aus der
| 675 000 450 000 | ıı Basis abgeleitete Seite bei einem bestimmten Arbeitsaufwand ein möglichst
| | grosses Gewicht erreicht wurde; die Gewichte der einzelnen Winkel sind
: nn ne 1 : 1 : || infolge dessen unter einander sehr verschieden, Es ist übrigens zu bemerken,
A, 4857879. 1 0-97 NR Zr 6, 9 | er 10, 0) | dass der Erfolg dieser Massnahme in den mittleren Fehlern (Spalte 11 und 12)
A 2 | 625 000 | 430 000 | deutlich hervortritt.
| In den älteren Netzen — N.1 bis 3 — ist das normale Gewicht 12 zum
| ' Theil erheblich überschritten, zum Theil auchwieder bei weitem nicht erreicht.
Bei Nr.4 (Braak) weicht das Gewicht eines Winkels für die Stationen von
e | der Basis bis zur ersten Vergrösserung nur, wenig von 24 ab. Bei Nr.5
| | (Oberhergheim) ist symmetrisch nach Winkeln beobachtet, so dass hier das
I as EL: == 2 | Gewicht allgemein genau 12 oder 12,5 beträgt. x =
200 000 I (6) Die mittleren Fehler der abgeleiteten Seite in Bezug auf die gemessene
|

 

12,8

Fa ©. 11840:000

1
| 638 000

       
 

      
       
           
             
           
     
     
     
 
   
  
 
 
  
    
  
    
  
  
   
   
  
     
    
 

 

 

 

 

4,5671008.8 | 0, 70 a 21
210 000 Ve en : 2 kenn I
Basis sind für Nr.1 bis 3 nach dem in: « Veröffentlichung des König]. Preuss.
3 Geodätischen Instituts. Die Europäische Längengradmessung in 52 Grad Breite.
Berlin 1893 », Seite 245 erläuterten Näherungsverfahren unter Einsetzung der
in Spalte 10 angegebenen Winkelfehler bestimmt, für Nr. 4 bis 8 nach
| « Hauptdreiecke », I. Theil, Seite 21 bezw. VI. Theil, Seite 233 und 234 scharf
berechnet worden. ’

| |
4, 6201688 0,90 er ee 31
145 000

 

| | | (') « Gradmessung in Ostpreussen » und « Hauptdreiecke », VII. Theil,
| | | Abschnitt A.
|»... &) « Die Küstenvermessung u. s. w.» und « Hauptdreiecke », VII. Theil,
| Abschnitt B. . -
| (?) « Die Verbindungen der preussischen und russischen Dreiecksketten
| bei Thorn und Tarnowitz » und « Hauptdreiecke », V. Theil, Abschnitt A.
| | (!9) « Hauptdreiecke », II. Theil, 1. Abth. und VII. Theil, Abschnitt K.
|| (!1) « Die Königlich Preussische Landes - Triangulation. Uebersichtsheft
| | || Berlin 1887 ». -
| (12).« Hauptdreiecke », VI. Theil.
| | (13) « Die Königlich Preussische Landes-Triangulation. Uebersichtsheft
| | Berlin 1887 »
N | (!') « Hauptdreiecke », IX. Theil, $. 309.

 

 

 

|
4,1533091.9 ® BEN er, 91.09 ET 91.78 (!) Are du Meridien entre le Danube et la Mer Glaciale, ete., par Struve
Sl 4 el 206 000 3 | 200 000 \ | T. I, 8. 76. Diese und die folgenden dem Werke entnommenen Längen sind
| || in Toisen angegeben.

1 OL, 8

| — (2) L. e., 8. 76. Die Grundlinie ist selbst Dreiecksseite.
ı 211 000 (UL. ec, 8. 9.

l i (6) L. e., 8. 98-99.

en || (%) L. e., S. 100-101. Ko
36,79 | 41,7% IE: (960.18, 1017 109, Be
(&) 1. c., B. I 8.19:

1184668.80 | 1, | A _ Dan
1800 | 101000

 

 

 

 

  
 

ne ee | 1 OLSEN
429850 1,2650 |, _— 753,49 ——— 57, (AD. 6.,:Br ll, 8, 89
us | 81300 | ‚76000 ran
ar | | |
rt 4,2327361.10 1.00 | > 2 20.8 | id Be 30.60 |
ea | 145 000 142 000 E |
© v i ls YA. I 1.90 | su 13.6 | Dr 15.1@ |
a = = | 300 1° ro
Fr |
4 al, Be | 1 | 5 1 | | ER
, #2585010.50 10, 9 le 1.862 | ——— | 36,69 |
r 220,00 2 | | 1213:0002 | |
F ı. |
-14,2435324, 91) | 1. to | 22 35.8 En
a | =. ma. |.

 

ni
180}
art
>
—_
=
> |
Sn

444g —_ ur
/ Ze 73 000

 

 

   
