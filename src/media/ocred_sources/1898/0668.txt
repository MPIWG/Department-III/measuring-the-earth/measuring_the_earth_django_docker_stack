EEE

ee E

 

Anhang zu Beilage A. IIa,
Beilage A. IIb,

Beilage A. Ile.

Annese A. III.
Beilage A. IV.

Annese A. V.
Beilage A. VI.

Annesxe A. VII.

I80

Bericht über eine neue Reihe von Polhöhenbestimmungen am
photographischen Zenithteleskop, angestelltim Jahre 1897,
von H. B. Wanach (mit einer Tafel) en: :

Bericht über die Wahl der Station für den internationalen Pol-
höhendienst in Japan, von H. Prof. Tanakadate (mit einer
IN) 0 a ee

Bericht über die Lothabweichungs-Bestimmungen, 1898, von
H. Prof. Beersch . anal an ne sehrrg:

Rapport sur les mesures de bases, par M Bassot . ann,

Bericht über die Längen-, Breiten- und Azimut-Bestimmungen,
erstattet vom Centralbureau, durch H. Prof. Albrecht

 

Pag.

209-248

249-256

236-362

Rapport sur les marögraphes, par M. Bouquet de la Grye ... 363-387
Bericht über die relativen Messungen der Schwerkraft mit
Pendelapparaten, von H. Prof. Helmert #2 215088-890)
Rapport sur les triangulations, par M. le general Ferrero (avec
une carte et quatre planches) » 2.0.00. Volumeäpart
B. Berichte der Delegierten über die Arbeiten in ihren Liendern.
Rapporis des delögues sur les (ravaux dans leurs pays. 393-522

Beilage B. I.
Beilage B. II.
Annexe B. IIla.

ee B. III,
Annexe B. IVa.
Annexe B. IVb.
Beilage B. V.
Annexe B. VI.
Annewe B. VıIr.

Annese B. VIII.

Beilage B. IX.

Beilage B. Xa.

Preussen. Bericht der Trigonometrischen Abtheilung der k.
preussischen Landesaufnahmeüüber den Stand der Arbeiten
im Jahre 1898, von H. Oberst von Schmidt (m. einer Karte)
Baden. Bericht über die im Grossherzogthum Baden ausge-
führten Pendelbeobachtungen, von H. Prof. Haid (mit
einer Tafel) ee
Italie. Rapport sur les travaux exscutäs par la Commission
geodesique italienne dans les anndes 1897-98, par M. le
prof. Celoria (avec trois cartes) ee
Italie. Rapport sur les travaux preparatoires pour la jonction
de Malteäla Sicile, par M. Guarducei (avec une planche) .
France. Rapport sur les travaux ex&cutes en France (1896-98),
par M. le colonel Bassot (avec une carte) ao.
France. Rapport sur les travaux du Nivellement general de la
France en 1897 et 1898, par M. Ch. Lallemand a:
Bayern. Bericht über die in den Jahren 1896-1898 ausgeführten
Erdmessungsarbeiten, von H. Generalmajor von Orff ;
Etats-Unis. Report of the Geodetie operations in the United-
States, by M. Z.-D. Preston (with four plates) =,
Espagne. Rapport sur les travaux en Espagne, par M. B.-M.
aa ee aa
Portugal. Rapport sur les travaux geodesiques ex&cutes aux
iles Saint-Michel, Sainte-Marie et Terceira, de l’archipel des
Acores, par M. le comte d’Avila >,
Schweden. Bericht über die Arbeiten in denJahren 1896-98. von
Hrn. Prof. P.-G. Rosen . nen
Oesterreich. Bericht über die von österreichisch-ungarischen
See-Offizieren in den Jahren 1895 bis 1898 ausgeführten
Schweremessungen von Hrn. Contre-Adm. von Kalmär

393-396

402-406
407-409
440-1

412-431

AuT-r4g

450-458

494-498

 

 
