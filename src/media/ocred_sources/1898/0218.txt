 

 

 

 

 

 

Eu

206

eine nicht unwesentliche Steigerung in der Genauigkeit der Bestimmung der z-Üoor-
dinate erzielen, wenn es sich früher oder später als angängig erweisen würde, eine
zweite Beobachtungsstation in den europäischen Meridianen hinzuzuziehen. Wenn
beispielsweise eine solche in Portugal, etwa bei der Stadt Cartaxo, 60 Kilometer
nordöstlich von Lissabon, errichtet werden könnte, so würden sich die Gewichts-

_ verhältnisse für das Zusammenwirken aller 7 Beobachtungsstationen auf:

Px Py
= E— 1.58
Pz t Py
N = 16.8 und 106.8 VMas. —. 3.712 Mr — 2:74 d =.0.98

 

 

Dz = 2.80 n,3o

stellen und es würde somit eine noch günstigere Form der Fehler-Bllipse und eine
grössere Uebereinstimmung in der Genauigkeit der Bestimmung beider Coordinaten
erzielt werden.

Der Ersatz der Station in Portugal durch eine solche auf dem italienischen
Continente, etwa in Cotrone, ergiebt etwas weniger günstige Verhältnisse, indem sich
in diesem Falle die Gewichte auf:

Px Py £
D, = 2.08 =—=.19.7 1 — — = 1.56
I Py 3 1 Px 4 pn, 5
N 2aa Und 114.4 lan, — 45109 Mum 2.50 A 1.02

stellen würden.

Was die Wahl der Beobachtungsmethode anlangt, so liegen gegenwärtig
die Resultate der von Herrn Astronom B. Wanach ausgeführten dritten Beobachtungs-
reihe zur Prüfung der photographischen Methode vor. Dieselben wurden einem Be-
schlusse der Lausanner Konferenz entsprechend zunächst Herrn Professor Forrsrer
vorgelegt. Nach weiterer Diskussion zwischen demselben und dem Direktor ‚des
Centralbureaus entstanden die am Schlusse der Wanacr’schen Arbeit abgedruckten
Voten der Herren Forrsrer und Hermerr, welche trotz des günstigen Ausfalls der
3. Reihe in: Anbetracht aller Umstände doch zu Gunsten des visuellen Verfahrens
beim internationalen Polhöhendienst lauten.

Wenn hiernach über die bei den Beobachtungen zunächst in Anwendung zu
bringende Methode bereits eine gewisse Einigung erzielt worden ist, so wird es doch
unter allen Umständen vortheilhaft sein, die Beobachtungen so einzurichten, dass auf
möglichst zuverlässige und fehlerfreie Resultate gerechnet werden kann. In dieser
Beziehung wird die konsequente Anwendung eines geradsichtigen Reversionsprismas
von Vortheil sein und es werden besonders auch die geeignetsten Dimensionen der
Fernrohre, über welche zur Zeit noch nicht disponirt ist, festzusetzen sein.

 

sr a 1 a. Te HH Malle |

kl ui

 

:
i
|
|

 
