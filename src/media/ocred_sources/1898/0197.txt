 

'
}

 

INA

IM

Pr RR I

"

x 189
Gründen besonders der leichtern Communication zwischen der Station und Washington
für die 30 Kilometer nordwestlich von Washington gelegene Station @Gaithersburg
(Maryland) der Baltimore — Ohio Railroad ausgesprochen. 1 Kilometer südlich
von der Ortschaft Graithersburg hat sich auf Fulk’s Farm ein geeigneter Auf-
stellungsort gefunden, dessen geographische Breite zu 39° & 10” ermittelt worden
ist. Dieser Ort bietet alle Vorbedingungen zu einer erfolgreichen Ausführung der
Breitenbeobachtungen.

Die Umgebung — vergl. die beiden Karten auf Tafel II — ist hügelig,
lässt aber das Auftreten localer Refractionsanomalien nicht befürchten. Die Wahr-
scheinlichkeit einer weiteren Bebauung der Gegend ist sehr gering, so dass voraus-
sichtlich der ungehinderte Bestand der Station auf eine lange Zeit hinaus gesichert
sein wird. Trotzdem wird der Aufenthalt auf dieser Station für den Beobachter
infolge der bequemen und intensiven Verbindung derselben mit Washington mittelst
Eisenbahn, Post, Trelegraph und Telephon ein durchaus angenehmer sein.

Cineinnati.

Die Beobachtungen werden auf dem Terrain der 8 Kilometer nordöstlich: von
der Stadt Cineinnati gelegenen Sternwarte ausgeführt werden und zwar voraussichtlich
gleichwie auf den übrigen Stationen in einem kleinen, eigens zu diesem Zwecke her-
gestellten Beobachtungshause. Das Observatorium liegt auf einem Hügel von etwa
20 Meter Höhe über dem umgebenden Terrain, ca. 100 Meter über dem Ohio-Fluss
und 247 Meter über dem Meeresspiegel. In seiner Umgebung sind keinerlei Einflüsse
vorhanden, welche das Auftreten von Refractionsanomalien befürchten liessen. Nach
den bisher bei den Meridianbeobachtungen gemachten Erfahrungen wird im Winter
auf 8 klare Abende pro Monat zu rechnen sein, während sich die Verhältnisse im
Sommer noch wesentlich günstiger gestalten.

Die geographische Breite des Observatoriums beträgt 39° 5° 19”.

Ukiah.

Für die westamerikanische Station bestand ursprünglich die Wahl zwischen
der Umgegend der Stadt Ukiah im californischen Küstengebirge und der Stadt
Marysville im Thal des Sacramento. Die Recognoscirung von Ukiah seitens der
Coast and Geodetie Survey hat aber für diesen Punkt ein so befriedigendes Resultat
ergeben, dass es nicht nothwendig erschien, über die Verhältnisse von Marysville
gleichfalls Erhebungen anzustellen. Professor Prrreuerr spricht sich so günstig. über
Ukiah aus, dass es nach seiner Ansicht zweifelhaft erscheint, ob ein geeigneterer
Ort für dergl. Beobachtungen an der Pacificküste überhaupt aufgefunden werden könne.

Die Station würde hiernach — vergl. die Karte auf Tafel II — in dem 3 bis
5 Kilometer breiten und ungefähr 15 Kilometer langen, von NNW nach SSO ver-
I*

 
