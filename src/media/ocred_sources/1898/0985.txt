 

Im men ver

a Tree Rem

Ka nel

ein

ul ı

ai

MI IPIR I DRUERURER ILLImInNN aM.

 

— 269 —

IX. —Italie,

 

 

 

 

 

 

 

 

 

no | INDICATION ;
Dr Bone LATITUDE |LONGITUDE| EPOQUE | DIRECTEURS | OBSERVATEURS INSTRUMENTS REMARQUES
119 | T.re Chianca....... A027 30 | 35252 4A 1872 Col. Chid. ca Maeeia. Pis
121 | Taranto. ......2.. 40 28 30 | 3453 386| 1871 id, a
123 | Oria (Chäteau)....| 40 29 53 | 35 18 23 1872 id. Lieut. Mottura. | Stark 2
26 | S Teresa. 40 31 47 | 35 35 18 1872 id. Cap. Colueei. . nn a
135 | Trazzonara........ 40 37 24 | 35 03 26 1872 id. id. id.
140 | Brmdist......... 40 39 15 | 35 37 56 1872 id. Ing. D’Atri. Repsold de 27 cm.
l Trascon . 40 2 | 34 55 a i Cap. Colueci.
41 ra 40 3 3 50 34 1871 712 id. I Garbolino. Starke de DT em.
143 | Carovieno......... 40 422722 353 19 19 1872 id. | Cap. Colueei. | id.
9 ecehia.......... 40 45 5 ; id.
149 Sp 45 20 |35 0837) 1872 id. ne eine id.
XL.
RESEAU DE BARI,
83 | Mostarico......... Deja& mentionne.
88 | Stornara.......... | 39059! 53" | 34°15' 23") 1872 Col. Chio. Cap. Maggeia. |Starl
94 | Nocara.ı 2... D&ja& mentionn&. 3 sei nn
105 | T.ro Scanzano..... 40 15 22 | 34 24 39 1872 id.
18: | Bisticn 40 23 21 | 34 12 i 2 Barbieri
l isticei _ 54 1871 id. Sa Mora .
M4 Ta Serra... .. . Deja mentionne.
115 | T.re Mattoni...... 40 24 09 | 34 31 51 1875 » Ferrero. Cap. Maggia. Repsold de 27 em.
21 | Barantor. D6j& mentionne.
130 | Colabarile......... 40 33 32 | 34 08 31 1871 » Chi. » Colueci. Starke de 27 em.
i Ing. D’Atri.
136 | M. Cambio......... 40 3732 323125), 19,272 id. cab. Colucci. | id.
- ; Ing. Garbolino.
141 | Trasconi.......... Deja mentionne.
142 | Orseti - \ 40 40 32 | 34 45 13 1871 id. » Garbolino. id.
148 | M. Peloso.........) ee : ;
149 | Specchia.......... , Deja mentionnes.
- : Lieut. Mottura. :
150 | Jazzotello......... 40 46 31 | 34 25 19 | 1871-72 id I \ id
154 [| M. Serio......... .. 40 48 59 | 34 48 54 | 1871-72 id. id. Repsold de 27 em.
los Berrmı. 40.49 4] | 34 59 37 1872 id. id, id.
160 | Chiancaro......... 40 54 17 | 34 16 15 1874 » kEerrero. id. id.
164 | Casamassima...... 407 37 I | 34 35205 1872 » Chiöo. id. id.
165 | Serraficaia........ Deja mentionne. |
166 ı Conversano ....... 40 58 03 | 34 46 48 1874 » Ferrero. id. id.
174 As en Deja mentionne. |
176 | Bari (Phare) en A 08 7 13430 5 1874 » Chio. id. | 10%
183.) Teani.2. ........ Deja mentionne.
x.
REGION A L’EST DE LA MERIDIENNE DE MILAN. - TRIANGULATION DU VENETO.
Reseau principal, developpement de la base de Udine et liaison avec le r&seau principal.
292 | Coppaso. | 44°53' 40"| 29029! 39" 1883 Col. De Stefanis. |Ing. Derchi. - Starke de 27 cm.
pp
294 | Eican0lo 44 57 13 | 29 05 58 1883 id. » Ginevri. id.
296. Donadar  . .. ..... do 02 13 | 29 52 44 1883 id. Nop. Grar id. |
297 | S. Benedetto Po...| 45 02 30 | 28 35 33 1883 id. Ing. Domeniconi. id. |
306) M. Venda:......... 45 1347 29 2210 1882 id. Top. Gra. id.
3ll | Solferino.......... 45.22 17 | 28 18,39 1882 » Kerrero. Ing. Derchi. id.
ala Venezia... 45 26 01 | 30 00 10 1882 id. » Domeniconi id.
313b Padova (Observat.).| 45 24 05 | 29 31 56 | 1882 id. » Derchi. id
317... Galvarına . Aa 30 32 238 50 4] 1882 id. id. id.
3231 Gaorle.. 2, . 45 35 55 |. 30, 398.12 1882 id. » Cloza. id.

 

 

 

 

 

 

 

 

  

 

 

 

 

 

 

 

 

 

 

 

 
