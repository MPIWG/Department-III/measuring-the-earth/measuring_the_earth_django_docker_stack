Se

MR a

Ri

map

Pr I ÜRT EAN LAIEN N Hm

m

 

TIL: — Grande Bretagne (Inde).

— 165 —

 

 

 

 

 

„| INDICATION | 5 DIRECTEURS
N DES POINTS LATITUDE LONGITUDE EPOQUE ET OBSERVATEURS INSTRUMENTS REMARQUES
LE. Nelasat...2...200L 170 2A, 17° 28.102 1871 Rogers, Barrow 24" no 2,
12 | Hudasi .. 0... 29a ı 72 2 8 1871 id. id.
13 | Impagat .......... 16 42 32 | 74.39 12 1871 id. id.
14 }.Pirlagat.......... 16.5] 35. | 77.18.37 1871 id. id.
15 | Kottapalle......... 16. 28 19177 22 59 1871 id, id.
16-, Darur 0. I6 13.35 17 39.36 1871 id. id.
17. Naikal. 00. 1643 18 |77 A327 1871 id. id.
18 | Chintalikunta ..... 16 20 50 | 76 59 49 1871 id. id.
19 | Maliabad ......... 16.8.8172 20 6 1871 id. id.
20 | Janikale. ......... 16. 0 50.0.0 51.25 ll. id. rd
21 | Kere Balagal ..... 15 48 43 | 77 40 58 1871 id. id.
33: Adonk... 20... 15.38 50.| 77 1646 1871 id. id.
23 | Pulikonda .......: 15.28.20 .11.89, 56 1871 id. id.
24 , Katomoräj........- 15 25.20 | 7158 33 1871 id. id. .
25 | G00Iy 2... 15 646..29739 5 1871 id. id.
25 "Gadekals.......... lo. 715717 14 25 1871 id. id.
27 | Udarpidurga ...... 14.49 2|7220 3 1871 1d. id.
28 | Devarakonda...... 14 40 29 | 77 38 56 | 1870-71 id. id.
29.| Honnur.. 2... 14 55.19. |°17.0 2 1871 id. id.
30 | Devädelabetta..... 14.33 36 | 21.790 1870 id. id.
31 | Kondapalli........ 14 3] 49 7724 15 1870 id. id.
32 | Urakonda......... 4 159 44 |, 77.90 29 1870 id. id.
33 | Pavagada......... 14.615 772 10 42 1870 id. id.
34 | Yerrakonda....... 135456 | 27 393.1 1870 id. id, -
35. Iunaclur.... ...... 14 222,18 120 1870 id. id.
36 | Ambäjidurga...... 132334, 8 14 1870 id. id.
37 | Dodnirmanga ..... 13 35 4977,83 16 1870 id. id.
38 | Mäkalidurga...... 132016 7220 1870 id. id.
39 , Halasurbetta...... 13 938 | 77 37 13 || 1866-67-70 | Bramfill, lieut. W.M. Camp- | T. et S. 24" no I et
bell, Rogers. Barrow 24" n? 2.
40 | Rämadevarabetta..| 13 19 19 | 77 956 1867-70 Campbell, Rogers, id.
41 | Savandurga....... 12.50 0 70.1795 1867 Campbell. m. eu Ss. 24 mol.
42 | Bannergatta ...... 12 2838 0 34 33 1866-67 Branfill, Campbell. Bet s 24. nl &
Barrow 24" n° 2.
43 | Rangaswänmibetta..| 13 127 | 76 58 17 1867 Campbell. Ten s. 22 ne
44 | Hemagirl........- 124343 1 258 1867 id. id.
45 | Bänatimaribetta...| 12 33 52 | 77 22 58 1867 id. id.
46 | Tirthapalli........ 13.2203 07053 5 1866-68 Branfill, Rogers. T.e6 S 2m)
Barrow 24" n° 2.
47 \ Nandigudi......... 1912. 877.53 48 1866 Braniill. Tel Ss 21.00
43  KRolan.. 13 847 18 53,49 1866 id. id.
49 | Bhupatamma...... 12 3347.18 54] 1866 id. id.
50; Hosur...2.20. 422 12 43 32 | T7 50 23 || 1866-67-68 | Branfill, Campbell, Rogers. | T. et S. 24" n°1 et
Barrow 24" n® 2.
5] | Turukungutta ..... 12 58 331 20.42 44 1867 Campbell. Barrow 24" n® 2.
52 | Bangalore (BaseLi-
ne-S.N. End)... 13 041 | 772259) 126, id. id.
53 | Bangalore (Base Li-
ne N..E. End)... -: 3 4155 2395 1867 id. id.
54.| Mandur: ....2...2: 13 456 214359 1867 id. id.
55 | Devarabetta....... 12 37 271 1037.30 1867 id. T,etbS. 24" nl.
56 Anchettidurga Ines 12 3518 1717.53 29 1867 id. id.
57 | Mariyälam........ 12 22 40 | 77 42 20 1867 id. id.
58 | Koppabetta ....... 12 21 45 | 77 29 44 1867 n id. id.
59 | Ponnäsibetta...... 12, 8: Kl 211 398.45 1867 id. id.
60 | Guttiräyan........ 12.19.49. 717951 20 1867 id. id.
61 | Bandhallidurga....| 12 12 10 | 77 20 13 1867 id. id.
62 | Karadiguttar .. ... II 59.2977. 5238 1867 104 id.
63 | Bodamalai........ Il 545. 77.383 20 1867 id. id.
64 | Pälamalai....... "1.11.41 28 9742 8 1869 Branfill. id.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
