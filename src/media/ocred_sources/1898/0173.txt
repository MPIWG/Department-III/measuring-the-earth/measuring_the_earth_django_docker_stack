|
H
i

 

 

WIN AR

Kl

Dil

Fa

IR I RDARNAR 1)

165
8

Gäbris — Pfänder,
Jigi — Weissenstein,
Weussenstein — Bern,
Bern — Neuenburg,
Neuenburg — Genf,
Strassburg — Solitude,
Strassburg — Grand — Paris (Pantheon),
Paris — St. Martin de Chaulieu — Brest und
Grand — Talmay.

Ausserdem wurden vorerst zur Controle die direeten Linien:

Bonn — Feldberg i. S.,
Rigi — Pfänder und
Rigi — Genf

berechnet. Hierbei sind Laplace’sche Punkte: Bonn, Mannheim, Strassburg,
kügi, Simplon, Zürich, Gäbris, Pfänder, Weissenstein, Bern, Neuenburg, Genf,
Paris und Brest; in Opel, Durlach, Feldberg und Solitude sind Polhöhe
und Azimut, in Talmay Polhöhe und Länge, in St. Martin de Chaulieu nur
die Polhöhe bestimmt worden; Grand ist kein astronomischer Punkt und
wurde nur aus Zweckmässigkeitsgründen eingeschaltet.

Die Berechnung der Linien Süunplon — Limidario (Ghiridone) — Mai-
land, Mailand — Turin, Turin — Nizza und Mailand — Genua ist vorbereitet.
Nachdem uns nämlich Herr Generallt. vr Brxepioris die Resultate der
Winkelausgleichungen der Dreiecksnetze westlich des Meridians von Mai-
land zugänglich gemacht hatte, haben wir die schliessliche Berechnung
der Dreiecke, soweit sie noch nicht aus den bereits erschienenen Heften
der „Zlementi geodetici dei punti della carta d’Italia“ zu entnehmen waren,
selbst bewirkt und diese Rechnungen durch Anschlüsse an bekannte Seiten
controlirt. Ebenso verdanken wir Herrn Generallt. pe Bexevicris die
Mittheilung der Anschlussdreiecke für die Sternwarte in Nizza.

Die Gesammtheit der erwähnten Linienzüge bildet ein zusammen-
hängendes Ganzes, das wir auch einer einheitlichen Bearbeitung unter-
werfen möchten.

Die Längen der geodätischen Linien beruhen vorläufig von Bonn
bis zum Rigi auf der alten Bonner Grundlinie, wie sie im Rheinischen
Dreiecksnetze benutzt ist, in der Schweiz auf dem im Schweizerischen
Dreiecksnetz angenommenen Mittel der drei Schweizer Grundlinien, in
Italien auf der neuen Basis am Ticino und in F rankreich auf der alten
Basis von Melun. Zum Anschluss des Rheinischen Netzes an das Schweizer
Netz wurden die in beiden. Netzen identischen Fünfecke: Feldberg i. S. —

[2

 
