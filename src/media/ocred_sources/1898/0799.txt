De ET

5 Km Terme

IE El

ae

DEU HEIM IL DAMNEN ml Kl

IT MIE.

 

 

 

     

—89 —

vI..— France et Algerie.

 

 

 

INDTEATTON

 

LATITUDE u EPOQUE

DIRECTEURS

 

 

 

 

T0
N en oa INSTRUMENTS REMARQUES-
Triangulation de Jonetion de la Base de Bordeaux au Parallöle Moyen.
259 | Chantillae......... |: 45°19 30° | 17024 58” ee
or Ara eneraldela Guerre,
Ss a a, \ Dejä mentionnes. Do
261 | Soubran. 2.0. a2 74a 82
262 | St. Savin.... 2... 45 834 | 7 12 53
263 | Le Gibault........ 45 10 54 | 17 32 44
264 | La Pouiade........ 29, 9.29, 102245
265 | St. Andre de Cub-
ZA. 44 5957,40 1256
266 | Montagne......... 44 55 53 | 17 31 59 N
267 | ba Same 44 46 11 |\1721 2 Cereles repstiteurs .
268 | Bordeaux ......... 400 7 5 4 a 4 verniers qui
269 | Blangquefort........ 44 54 38 | 17 139 Colonel Brousseaud, lieu- au a
270 | Leugean........... 44 58 39 | 16 53 11 ) 1826-27. | tenants Fessard, Levret | lecture de 20" cen-
271 | Captienx ......... 44 51 38 | 16 48 58 et Dupuy. a,
3a Trbd 45 41 34 | 23 51 36 chaque angle furent
273 | Bellachat.......... 43230 |24 4 9 ex6cutees 20 repe-
24 Bm. 4521 9 | 23 51 42 tions.
275 | Encombres......... Aa or 2a ol .
276 | Mont Jouret....... 49:29 42 | 24 18 11
277 | Roche Chevriere...| 45 1737 | 2423 8
278 | Mont Tabor....... 45 651 | 24 13 39
ZI" Ambine. 20.00 A» 925 | 24 32 53
280 | Chaberton......... AA 57 54 | 24 24 48
281 | Albergian 4 027022392
282 | Rochemelon........ Aa aa | 28
Triangulation du Parallöle de Rodez. (Partie Orientale.)
283 | Puech de Monsei- | td.
ME 412 12,082| 20239. 2832
62et63) Rodez, La Gaste...| Dejäa mentionnes.
284 | Cougouille......... 2351 9, 20472
2855 | Puech @’Alueech....| 4420 9 | 21 458
286 | L’Hort de Dieu.... 44 719 | 21 14 40
287 Roc de Malpertus..| 44 24 5 | 21 30 33
288 | Le Guidon de Bou-
auet. 2. 0: 44 659 | 21.56 56 .
289 | Dent de Rez....... 44 25 46 | 22 957
290 | Montmon.......... 44 19624: 22023 31 N
291 | &rand Montagne...| 43 58 34 | 22 25 46 1824.25 Capitaine Durand, Id
292 Mont Ventoux..... 44 1027| 2250 3] E \ lieut.t* Rozet et Levret. ’
293 | Les Houpies........ 43 42 44 | 22 38 45
294 | Le Leberon........ 43 48 56 | 23 750 \ 5
295...Bburer.. 2.0.2... 44 7.23.11 23.20658
296: St. Julien. .. 22. A3 AL 21 123.24 \o
297 | Meourr6& de Chenier.| 43 50 30 | 24 0 52
298 | Les Monges........ | 44 19.46 | 23 510.28
299 | Le Grand Coyer.... 44 6 1242112
300 | Le Grand Berard..| 44 26 57 | 24 19.25
301 | La Chaims......... 43 44 51 | 24 19 31 |
302: | Cheiten u... 43 48 53

 

 

 

 

 

 

 

 

 

2A 37 59

 

 

 

 

 

 

 

 

 
