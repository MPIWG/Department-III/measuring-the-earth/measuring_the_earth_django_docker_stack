Tele

if
i

 

 

ma Rt

nr

MIET IRLRRNAUR ILLIMIHTN BO LIUENL

5 439
micrometric measurements of zenith distances. In the determination of elevations neces-
sary to reduce the base lines along the transcontinental are to sea level, the latter method
has been employed across the Allegheny and Rocky mountains. The spirit levels are con-
tinuous from Sandy Hook to Denver and Öolorado Springs. They are checked by zenith
distances from the Chesapeake Bay to the Ohio River, and supplemented by the same
method from Denver to the Pacific coast, where the spirit levels are not yet completed.
Permanent magnetie observations have been in operation at Philadelphia, Key West,
Madison, Los Angeles, and each one has furnished records for five consecutive years; with

‚one exception a self-registering apparatus has been continuously and exelusively employed

in each locality. These data added to records from 1,100 widely distant points, many of
which are secular variation stations, furnish preeious material for the study of the earth’s
magnetism. The work of the Survey in the investigation of the force of gravity has been
carried on both within and without the limits of the United States, Twenty-eight foreign
stations have been occupied, ineluding points in Europe, Asia, Africa, Australia, and many
islands in both the Atlantie and Pacific. New light on the subject of volcanie formation,
as wellas on the constitution of the earth’s erust, has come from this work. Fifty-nine
stations have been observed at home, including a line across the continent. Half-seeond
pendulums are now exclusively employed, and the determinations are purely differential.
The period of oseillation is usually known .to within a few millionths of a second.

In the field of physical hydrography most comprehensive studies have been made.

The exploration of the Gulf Stream, including a study of its density, temperature, and
currents, the geology of the sea bottom, the establishment of eotidal lines, the determina-
tion of the ocean depth from earthquake waves, and other specialties in the domain of
hydrography have been made a part of the regular work. The hydrography of the coast
to the head of tide water has been developed side by side with the triangulation and
topography.

The practical results of the Survey are shown in the publication of the annual reports,
the issue of charts, Notice to Mariners (corrected monthly), Coast Pilots for Atlantie, Pacific,
and Alaskan waters, tide tables (now extended to foreign waters), and various miscellaneous
publications in special lines of research.

PRESENT AND FUTURE OPERATIONS,

A resurvey of Chesapeake Bay, the measurement of an are through the United States
on the ninety-eighth meridian, and the development of Alaskan geography are among the
projeets of Dr. Pritchett, the present superintendent of the organization. All these have
been carried on during the last two years. The line of transcontinental precise levels is
being pushed westward with all available means. Primary triangulation on the Paeifie
coast has been resumed, and will soon be completed from San Francisco to the Mexican

5 boundary. Hydrographie surveys are in progress along the Atlantic seaboard, on the

Pacific, and at the mouth of the Yukon in Alaska. Numerous topographie, astronomic, and -
magnetic parties are employed in the interior.

 
