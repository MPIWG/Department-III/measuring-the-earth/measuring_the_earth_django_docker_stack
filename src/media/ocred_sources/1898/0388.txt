 

370

rait une sur@levation du sol de pres d’un millimetre par an. Mais si !’on remarque qu'en
1817 on avait preeisement ce chiffre de 4"458 ei que les variations annuelles affectent une
forme sinusoidale, la conclusion A en tirer est que le granit de la Bretagne est stable et que
les varialions annuelles doivent tenir ä des causes non mises en &quation, telles que la densitö
de l’eau de la mer.

Quoi qu’ilen soit, pour le niveau moyen, nous devons adopter la moyenne gen6rale
de toutes les observations, soit 4472, chiffre qui doit &tre encore augment& de 7 millimetres
par suite de la hauteur de la cuvette du barometre au-dessus de ce niveau moyen.

D’autre part, d’apres le nouveau nivellement de M. Lallemand, le zero du marögra-
phe est a — 4, 462 par rapport au zero de Marseille, cela donne + 0017, pour la cote du
niveau moyen de la mer ä Brest.

A Cherbourg nous avions trouv& pour le niveau moyen 3663 au-dessus du zero du
maregraphe dont la cote nouvelle esi — 3706, ce qui donne pour niveau moyen — 0043.

Au Havre, nous avions trouve un affaissement moyen annuel de 2 millimetres, chiffre
appreciable ; les nouvelles observations le reduisent & 06. Jusqu’ä nouvel ordre on doit le
considerer comme n’ayant aucune significalion r&elle. Le niveau moyen general est A"801 ;
or le zero du mar6graphe est ä la cote — 4.968, ce qui fournit pour le niveau moyen celle
de — 0167. |

M. Hatt, pour le caleul des eoefficients harmoniques de la mar6e, a analyse les courbes
de quelques ann&es sans les corriger de l’influence du vent; ses resultats different peu de
ceux que nous avons oblenus.

Enfin, les medimar&mätres de M. Lallemand ont fourni toute une serie d’elements
du niveau moyen.

Nous plagons dans un m&me tableau les resultats des niveaux moyens depouill&s
jusqu’ä ce jour ; ils sont rapport&s au z&ro de la mer & Marseille.

Medimaremetres. Maregraphes. Calcul de Ponts
m M. Hatt. et Chaussees.

Me. 0,040
La molat . :.... + 0,080
Massalle. ..... 0
Marlele. . - 2.2. 2—0,020
Port-de-Boue . .. — 0,050
Be .. 20 0,030
Boni vendses .... 0 4 n
Bo. 1.2.0. = 0,160 -+- 0,034 0,02
biazymizso. 2... 0,210 5
La Rochelle-La Palice +- 0,070 — 0,36
le Sablesı or 2... —.0,030
Saint-Nazaire . . — 0,10

DUDSOn, 222, 2.:-.,0,060
Pl 20.00: 0,050

 

uch

a ad: a hd Marlldhuau |

ak tn

ih do nl

za

pen n

 
