 

   

 

 

— 273 —

Ix  jale

FE ae nee ee

 

 

 

 

 

 

 

 

 

 

 

 

INDICATION \ a
N DES POINTS LATITUDE LONGITUDE | EPOQUE DIRECTEURS OBSERVATEURS INSTRUMENTS REMARQUES
all
XV.
RESEAU DE LA SARDAIGNE ET DEVELOPPEMENT DE LA BASE DE OZIERI.
6... 3 Roro. 0... 38°51' 38"| 26004’ 22] 1882 Col. Ferrero. Cap. Simi. Repsold de 27 cm.
58 | P.ta Severa....... 39 02 49 | 26 29 42 1881 id. id. Starke de 27 cm.
59 | Guardia dei Mori..| 39 09 44 | 25 56 40 1882 id. id. Repsold de 27 cm,
61 | 8. Panerazio...... 39 13 15 | 26 44 49 | 1880 id. | Lieut. Oro. | Pistor de 28 em.
- 64 | Tre di M. Ferru... 39 18 12 | 27 15 59 1881 id. Ing. Domeniconi| Starke de 27 em.
: 65 | M. Serpeddi....... 39 21 59 | 26 57 36 1881 id. id. id.
i 68  M.Imasın....... 39 26.52 126 165] 1880 Cap. de Vaiss. | Lieut. de Vaiss. | Pistor et Martins de
| Magnaghi. Troiano. 27 cm.
E82 | 8.GiovannidiSinis.| 39 52 22 | 26 06 08 | 1881 |col. Ferrero. Cap. Simi. Starke de 27 em.
89 | @ennargentu...... 40 00 23 | 26 58 56 1882 id. id. Repsold de 27 cm.
91 | M. Santo di Baunei. | 40 03 14 | 26 45 23 1882 id. id. ice
| M Urtieu.. 40 08 36 | 26 16 15 1881 id. Cap. Pavese. Starke de 27 cm.
E 117 | M. Rasu di Bono...| 40 25 16 | 26 40 04 1879 id. » Simi. Repsold de 27 em.
F 122 | P.ta Catirina...... 40 28 49 | 27 1145 1881 id. Lieut. Oro. } al
? 125 | Capo Comino...... 40 31 43 | 27 29 02 1882 id. Cap. Simi. id.
129 | Ittireddu....... .. 40 32 50 | 26 34 04 1879 id. id. id.
131 | M. Santo di Tor-
zalba 0... 40 34 40 | 26 26 44 1881 id. Cap. Pavese. Starke de 27 cm.
132 | Monserrato........ AV, 35 017226 39 12 1879 id. Lieut. Pavese. id. .
133 | Base de Ozieri....| 40 36 26 | 26 35 26 1879 id. Cap. Simi. Repsold de 27 cm.
# Terme N.-E.
= 134 | Base de Ozieri....| 40 36 33 | 26 33 01 1879 id. id, id.
= Terme S.-O.
= 158 | Bisarcio .... A385 0123 1879 id. Lieut. Pavese. |Starke de 27 em.
144 | M. Föorte....... 40 43 14 | 25 55 21 1878 Cap. de Vaiss.| » de Vaiss. |Pistor et Martins de
= Magnaehi. Aubry. 2 En.
= 145 | N.a S.a di Bonaria.| 40 43 45 | 26 20 53 1879 id. Lieut. de Vaiss. | Troughton and Simms
= Mirabello. de 27 cm.
147 | S. Leonardo....... 40 44 06 | 26 36 14 1879 Col. Ferrero. Cap. Simi. Repsold de 27 cm.
2 157 | M. Limbara....... 40 51 08 | 26 50 23 1879 |Cap. de Vaiss. |Lieut. de Vaiss. | Troughton and Simms
= Magnaghi. Mirabello. de 22 em.
161 |, Isola Tavolara ....| 40 54 43 | 27 23 15 1882 Col. Ferrero. Lieut. a Starke de 27 cm.
: : - id. ;
167 Saluı. 40 58 19 | 27 12 14 | 1882 id. Cs sn \ dad
- 172 | P.ta della Scomu- i
: nich 2. 200.5 41 05 52 | 25 57 28 1885 » DeStefanis.| » Pavese. id.
178 | P.ta Tejalone...... 41 12 49 | 27 08 13 1883 » Ferrero. Maj. Simi. Repsold de 27 cm,
179 | GUARDIA VEC- \
CHIA......0 2 41 13 21 | 27 03 46 1883 id. id. id.
180 | T.re di Capo Testa..| 41 14 11 | 26 48 29 1879 ide 2. Lieut. de Vaiss. | Troughton and Simms
Lasaegna. de 27 em

 

 

 

 

 

 

 

 

 

 

Mr LDRLRANAER IL IRINI RL ren

 
