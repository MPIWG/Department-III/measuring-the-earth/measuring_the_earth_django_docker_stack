 

nn re

 

A

EAN LI A

IT

1 185
ausser den im vorjährigen Bericht projectirten 4 Stationen in Japan, Italien und
Nordamerika (Ost- und Westküste) noch zwei weitere, von der I. E. nur subventionirte
Stationen theilnehmen, im asiatischen Russland: eine neue Station bei Tschardjui, und
im centralen Theile von Nordamerika: die Sternwarte in Oineinnati.

Bei den Vorbereitungen fand das Centralbureau das regste Entgegenkommen
bei den maassgebenden Personen und den Institutionen der betreffenden Länder. In
Bezug auf Mizusawa sandte Herr Professor Tanarıparz im Juni 1898 seinen aus-
führlichen Bericht über die locale Untersuchung an mich privatim ein; bald darnach
folgte auch derselbe Bericht auf offieiellem Wege durch die japanische Gesandtschaft
in Berlin. Herr Professor Örrorıa gab in Briefen vom 8, Januar und 15. u. 20. Mai 1898
weitere Erläuterungen über die Localität der Station Carloforte und die bei der Wahl
der Beobachtungszeit daselbst zu beachtenden Umstände. Herr Prof. Dr. Prircuerr,
Superintendent der Toast and Geodetie Survey der Vereinigten Staaten von Nord-
amerika, machte am 21. Januar, 25. Februar und 23. Mai 1898 Mittheilungen über
die westamerikanische Station Uliah, die ostamerikanische Station Gaithersburg und
über die günstigste Beobachtungszeit für die amerikanischen Stationen. Weitere
Schreiben in Betreff der Einrichtung des Dienstes auf letzteren liegen vor vom
16. November und 21. December 1898.

Die günstige Gelegenheit, welche die Stuttgarter Conferenz durch die An-
wesenheit der Vertreter von Japan, Russland, Italien und Nordamerika bot, habe ich
voll ausgenutzt, um mich mit denselben über die Details der Einrichtung des Dienstes
und der Stationen zu besprechen, und soweit nöthig auch Verträge zu entwerfen, die
zwischen der Internationalen Erdmessung und den Landesbehörden abzuschliessen
sein werden. Das grosse Entgegenkommen, welches ich allgemein fand, muss ich
mit herzlichem Danke hervorheben; das warme Interesse und die selbstlose Art, wie
es geboten wurde, stellen der weiteren Entwickelung des Polhöhendienstes das schönste
Prognostikon. Bereits vor der Stuttgarter Conferenz konnte ich erfreulicherweise mit
Herrn Generallt. vox Sruzexnporrr Exec. über die Anlage einer russischen Station
verhandeln; ebenso besuchte mich Herr Prof. Dr. TunaraparE aus Tokio in
Begleitung des für Mizusawa als Beobachter in Auge gefassten Herrn Dr. Kımura,
bei welcher Gelegenheit schon Besprechungen über die Errichtung der Station Mizu-
sawa stattfanden. Der letztgenannte Astronom arbeitet seit Ende October im Central-
bureau; auch Herr Professor Taxaravarm kam noch einmal am 9. December 1898 zu
einer Besprechung nach Potsdam.

Gleich nach Rückkehr von der Stuttgarter Conferenz habe ich in Gemeinschaft
mit Herrn Geheimrath Arsrecur die erforderlichen Schritte gethan, um die recht-
zeitige Fertigstellung der vier neuen Zenitteleskope in die Wege zu leiten. (Die
wichtigsten Libellen waren schon seit Jahresfrist von C. Reıcaer in Berlin vollendet
worden; den Unterbau der Instrumente hatte Herr Wanscharr zur Zeit der Conferenz
auch fertig). Die Instrumente für T'schardjui und Oineinnati, die nicht neu beschafft
werden, bedürfen wenigstens gewisser Ergänzungen, welche vorbereitet wurden. Die
Entfernung, in welcher die Meridianmire errichtet werden kann, wurde -für alle

 
