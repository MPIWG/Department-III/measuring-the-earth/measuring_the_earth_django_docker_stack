Hl Besser ra

Raum

INN ART

ME IR 7 I RUN IHRE BR LIKE. wm ii

 

 

— 259 —

IX. halle,

brevi notizie sui lavori trigonometrici

eseguiti in Italia.

-_—_ 2.

Le triangolazioni del Marieni, () del padre Inghirami, (°) quella del parallelo medio ©)
e la triangolazione del Fergola (*) erano abbastanza esatte per servire di base a qualsiasi
ricerca di alta geodesia. Di un ordine di esattezza alguanto minore era la triangola-

zione dell’isola di Sardegna del generale Lamarmora, (*) le cui osservazioni andarono
del resto perdute in un incendio.

7

Dovendosi por mano da parte dello Stato Mageiore italiano alla costruzione di una
carta del Regno, e dovendosi ritornare sui punti antichi, parve naturale di riordinare
la rete generale e di procedere ad osservazioni e calcolo conformemente agli ultimi
progressi della scienza.

A questo fine contribui la creazione nel 1865 della Commissione geodetica inter-
nazionale.

Era allora direttore dei lavori geodetici il colonnello E. de Vecchi.

Dal 1869 al 1877 fu direttore dei lavori geodetici il colonnello E. Chiö, e poscia
fino al 1883 il colonnello A. Ferrero, ora tenente generale, e presidente della Commis-
sione geodetica italiana; in seguito i colonnelli De Stefanis e Maggia; e quindi P’inge-
gnere geografo F. Derchi. |

I nomi dei direttori e degli operatori delle varie epoche veggonsi riportati all’ap-
posita colonna dello specchio che si acclude.

Detto specchio fu redatto in base alle istruzioni date dal prefato generale Ferrero
incaricato dall’ Associazione geodetica internazionale della .compilazione del ra
generale sul progresso dei lavori di triangolazione dei varii Stati.

pporto

(') Trigonometrische Vermessungen in Kirchenstaate und in Toscana. Wien, 1846.
(°) Dal 1817 al 1841.

() Operations geodesiques et astronomiques pour la mesure d’un are du
() Annalö civili del Regno delle due Sicilie, maggio e giugno 1838.

parallele moyen, 1821, 22, 23.
() Notice sur les operations geolesiques faites en Sardaigne, 1839. |

 
