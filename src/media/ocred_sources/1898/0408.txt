 

390

Herr Prof. Gork aus Washington zur Zeit mit einem neuen Anschluss von Amerika an Europa
beschäftigt, sodass dem erwähnten Mangel wohl bald abgeholfen sein wird.

Für eine erneute Ableitung der Pendelformel wird es auch günstig sein, dass ım
hohen Norden in etwa 70° Breite neue Bestimmungen in Grönland vorliegen (PUTNAM, v. Dry-
GALSKI), sowie in Sibirien (Wıukırzkı). Ob Nansen’s kühne Polarfahrt auch auf dem Gebiete
der Pendelmessungen von Erfolg begleitet war, ist mir noch unbekannt.

Von den zahlreichen Arbeiten der letzten Jahre möchte ich u. a. noch hervorheben
die Spezialuntersuchung von Rıcco am Aetna und die Erforschung des Verlaufes der Schwer-
kraft an der Küste des Rothen Meeres durch zwei von der Wiener Akademie der Wissen-
schaften in Verbindung mit der österr.-ungar. Marine ausgeführte Expeditionen.

Eine Ergänzung dieses letzten Unternehmens soll eine von der Göttinger Ges. d. Wiss.
mit Unterstützung durch das Preuss. Geodät. Institut nach Deutsch-Östafrika entsandte Ex-
pedition liefern. Bereits hat sich gezeigt, dass die Küstenstation Dar-es-Sa laam keine beson-
dere Störung aufweist.

Untersuchungen des Verlaufes der Schwerkraft von Nächenförmiger Ausbreitung sind
ausserhalb Oesterreich-Ungarns und des Küstengebiets der Adria nur in geringem Maasse vor-
handen. In Oesterreich-Ungarn hat die untersuchte Fläche 175000 km? Ausdehnung. Sehr
genau ist von dänischer Seite die Insel Bornholm von ca. 550 km? Fläche mit 15 Stationen
untersucht. Auch hat das Preuss. Geodät. Institut eine Fläche von 6000 km? östlich von Göt-
tingen mit 26 Stationen beseizi.

Ein recht dichtes Liniennetz mit werthvollen Ergebnissen bietet die Schweiz. Hier
wie in Norddeutschland hat man sich bemüht, Schwerestörungen und Lothstörungen in
Parallele zu stellen. Die Untersuchung der Beziehung beider Arten von Störungen wird
überhaupt eine Aufgabe der Zukunft bilden.

ich darf hier noch bemerken, dass der Kreis der Verwendung der Ergebnisse für
die Schwerestörungen durch Herrn Prof. Pızzerrı eine Erweiterung erfahren hat, indem er
nachwies, dass ihre horizontale Aenderungsgeschwindigkeit in enger Beziehung zur Krüm-
mung der Lothlinien steht. Man kann also daraus die Aenderung der Lothabweichungen mit
der Höhe ableiten.

Hiervon lässt sich in verschiedener Weise Gebrauch machen bei der strengen Ab-
leitung der Geoidgestalt aus Lothabweichungen im Gebirge, wo die Lothkrümmung Einfluss
erlangt. Die weitere Untersuchung zeigt, dass dieser Einfluss wesentlich derselbe ist wie bei
geometrischen Nivellements.

Wenn es noch nöthig wäre, das Interesse der Geodäten an den Schweremessungen
zu beleben, so würde die zuletzt besprochene Verwendung der letzteren dazu geeignet sein.

Oktober 1898.

 

UM lähshu hide

ie did

und.

Lab u luubun nun

lb auto ital

cn haha hy nad nd ara un

 
