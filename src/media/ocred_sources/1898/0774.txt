 

V. — Espagne.

 

 

Ne
Ei

 

INDICATION
_ DES POINTS

LATITUDE |LONGITUDE

 

|

EPOQUE

DIRECTEURS
ET OBSERYATEURS

INSTRUMENTS

|
| REMARQUES

 

 

 

 

 

io.

 

 

Aitama....... ON,
Monduber..........

Valdihuelo
Escusa........:....

Santos... ........

Pehas Gordas......
Altomira.......:..
Berninchez........
Bienvenida........

(Sohle...

Javalon. ....... .

Palomera..........
Javalambre........
Peharroya.........
Pina..... 5

Mremuzo::...... .

‚40 34 58

02906 5 4

 

Triangulation du Parallöle de Badajoz. (Suite.)

 

1865

1867-69 -

1867

Comm. du genie Ibarreta.

Comm. du genie Ibarreta,
comm. d’art. Solano.
id.

Triangulation du Parallele de Madrid.

38 38! 58" | 17°24' 23” |
39.033 | 1124 22
38 48 12 48 7)
| |
40921: 4 | 10229 31"
AU 35 28 | 10 32 24
0279 105
40 51 50 | 10.40 52
05 ariı 93]
40 29 57 | 12 35 38
40 56 27 | 12 35 54
4038 713 929
021521 13.254

40 30 5 | 14 24 46

AU |] 25: 14 22 29
40 10 56 | 14 50 51
403529 | 1A 51 21
15 19 20
013 0.1528 |
A243, 15484
a ]0 5, 15 5443

40, 4341 16 15 14

 

 

 

40 3553 | 16 28 8
40 549 | 16 38 52 |,
40 23 24 | 17 027
40 148 | 17 239
40 13 22 | 17 19 2
39 54 21 | 1717 29
5 u .29
|
I
42042) 4") 9019: 32r|
243) 8811
2101| 9 88)
3 516| 94538
433316 | 925 56,

1861

1861
1860
1861
1860
1860
1859
1859
1861

1860

1860
1863
1863
1863
1863
1863
1863-75

1863
1863-73

1863
1864
1863

1863-68

1864
1864-68

1864-76

1870
1870
1870
1871
1870

 

 

 

Comm. d’art. Coreuera,
capitaine du genie Bar-
raquer.,

id.
Comm. d’art. Corceuera.

id.
Comm. d'art. Corcuera,

capitaine du genie Bar-
raquer. }

Lieuten. col. d’E.-M. Ruiz
Moreno (D.M.), capitaine
d’E.-M. nn

i

Cap. du genie Barraquer.
id.

id.
id.
id.

Cap. du g6nie Barraquer,
cap. d’E.-M.L. Puigcer-
ver.

Cap. du genie Barraquer.

Cap. du genie Barraquer
et comm. d’art. Cabello.

Cap. du nn Barraquer.

id.
Kor

Cap. du genie Barraquer,
cap. d’art. Solano.

Cap. du genie Barraquer.

Cap. du genie Barraquer,
cap. d’art. Solano,

Cap. du genie Barraquer,
cap. d’art. Hernändez.

| Cap. d’art. Hernändez.
id.

id.
eh,
ja

 

 

Repsold (sans mar-
que).

Repsold n° 1, Rep-
sold C.

Repsold (sans mar-
que).

Ertel n® 1.

Ertel n® 3.

id.
Repsold n? 1.
id.

id.
Repsold n° 1, Rep-
sold ©.

Repsold n® 1.
Repsold n° 1, Rep-
sold A.
Repsold n® ].
ide

id.
Repsold n? 1, Rep-
sold C.
Repsold n° 1.
Repsold n° 1, Rep-
sold ©.
Repsold n° 1, Rep-
sold A.

Pistor ne 3,

 

 

van schein dh
