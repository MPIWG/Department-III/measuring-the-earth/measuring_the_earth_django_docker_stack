R
h
R
N
l
i

 

Benutzt man zur Ableitung von w nur die 17 der photographischen und
visuellen Reihe gemeinsamen Paare, so ergiebt sich:

Sıuel & = # 0.18,, photographisch u = =# 0!162,

und leitet man zw, nur aus der Zeit von April ı3 bis Juni 24 ab, weil vor- und
nachher die visuellen Beobachtungen durch Vernachlässigung der einen von beiden
beobachteten Gruppen gegenüber den photographischen im Nachteil sind, so ergiebt sich:

visuell u, = = 0'219, photographisch u —= =# 0.179,
und dementsprechend der mittlere Fehler eines vollen Abends:
visuell = 0'063, photographisch =# 0.052.

Daraus, dass die mittleren Fehler für die photographische Reihe durchweg
kleiner sind, darf man nicht auf eine grössere Genauigkeit der photographischen
Methode an sich schliessen, denn das benutzte visuelle Zenitteleskop ist weniger
stabil gebaut, als das photographische; das spricht sich auch in der geringeren
Konstanz der Niveaus des visuellen Instruments aus. Auch sind die Dimensionen
des visuellen Zenitteleskops (Objektivöffnung 68"", Brennweite 87m) viel geringer als
die des photographischen (Objektivöffnung 13 5"", Brennweite 135°).

Dass die vorliegende Reihe eine etwas grössere Genauigkeit der photo-
graphischen Resultate ergeben hat, lässt sich gegenüber dem ungünstigeren Resultat
der vorigen Beobachtungsreihe (Scmmn.-H., Seite 197) schwer erklären; die einzigen
Unterschiede zwischen beiden Reihen bestehen in der Anwendung verschiedener Mess-
apparate und dem für meine Reihe angebrachten Umhüllungsrohre. Dass letzteres
etwas günstig wirken musste, war vorauszusehen; dass es aber einen so grossen
Einfluss haben sollte, kann man schwerlich annehmen, besonders in Berücksichtigung
der auf Seite og mitgeteilten Erfahrung. Dass aber der Messapparat keine wesentliche
Änderung der Genauigkeit hervorgebracht haben kann, folgt daraus, dass Herr
Dr. Hroxer, der auch mit dem Toepfer’schen Apparat eine grössere Anzahl von
Platten der Reihe Scrm.-H. nachgemessen hat, keine wesentliche Verbesserung der
Resultate dadurch erhalten konnte.)

Zur Vergleichung mit den früheren Reihen photographischer Polhöhen-
bestimmungen seien hier nochmals die in gleicher Weise abgeleiteten mittleren
Fehler einer Beobachtung zusammengestellt:

*) Herr Scnnauper erklärt es für möglich, dass die geringere Genauigkeit der vorigen Reihe
herrühren kann von der schlechteren Sichtbarkeit und daher auch schlechteren Messbarkeit der Stern-
spuren auf den benutzten Platten, deren rapides Verderben nicht rechtzeitig bemerkt und durch Be-
stellung neuer Platten unschädlich gemacht worden ist. Diese Unterlassung war verursacht worden
durch die Erfahrung, dass Scrrxussner’sche Platten sich sonst durch sehr grosse Haltbarkeit aus-
zeichnen (vergl. auch die Fussnote auf Seite r95 des Berichts Scnn.-H.), Doch erscheint diese Er-
klärung dem Verfasser nicht ausreichend, da auch Dr. Marcusz trotz besserer Platten eine nur wenig
grössere Genauigkeit erreicht hat.

38

 

rue a 1a Te WETTE MATT AL. zul ul

ah nik

1 ano nel

 

a
i
|

 
