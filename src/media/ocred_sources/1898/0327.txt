a

 

Dan

a

mm

I TER RR

309

 

 

 

 

 

Annee et mois

1898
mars-avril.

1898
decembre.

1898
novembre.

1898
decembre.

1894 mars
et avril.

1897 juin.

1897 juillet.

Positions movennes

des etoiles

| Berliner
| Jahrbuch

| On a observ6

‚23 couples, emprun-

| tes au Catalogue

| de Respighi,
Auwers et Green-

wich (Ten Year Gat.)

 

|  Ona observ6 |
14 etoiles, dont 5 du)
‚Berliner Jahrbuch.)
| |
ı  Onaobserve |
9 etoiles fondamen-,

ıtales, 3au nord et6
au sud. |
|

' On a employe 37)
‚etoiles, dont 14 em-,
|prunt6es. au B. A.J.|
ILes positions desau-|
jtres ont ete tirees|
de plusieurs catalo-|
|gues. |

Six etoiles, dont 2
'fondamentalestir6es)
IduB. A»J. Les au-ı
'tres positions ont;|
'et& deduites de dif-
förents catalogues.

 

 

TITRE DE LA PUBLICATION

 

Non publie.

Latitudine del R. Osservatorio as-
tronomico di Catania determi-

nata nel 1894 dal Dr. T. Zona. |

(Pubblicazioni dellaR. Commis-
sione geodetica italiana, Firenze
1896.)

Non publie.

REMARQUES-

Resultat provisoire.

On a renonce & l’usage du micro-
metre enregistreur electrique.

A. — 4429 52.75 +0.06°)
B. = 44 29 52,82 + 0,16”)

*) poids = %0 = au nombre de
couples.

**) poids = 6-=— au nombre des
etoiles.

 

 

 

 

 

 
