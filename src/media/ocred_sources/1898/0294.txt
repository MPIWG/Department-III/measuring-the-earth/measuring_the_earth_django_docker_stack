    

Rn

a

ee

 

en

EEE EEE TEE ee enge

Die in früheren Berichten2? von Herrn Professor Helmert aus den vorläufigen Ergeb-
nissen dieser Arbeit gezogenen Schlüsse, besonders in Bezug auf die ım 52. Parallel er-
mittelte starke Krümmung, die im Gegensatz zu den Meridianbogen sehr nahe dem
Bessel’schen Ellipsoid entspricht, werden durch die definitiven Resultate durchaus bestätigt.
Die Schlussergebnisse sind in voriger Tabelle zusammengestellt.

Diese Zusammenstellung zeigt, dass für das Gebiet der Längengradmessung bei beiden
Ellipsoiden der Anschluss in Breite, wie es bei der geringen Amplitude in Breite von weniger

als 3° auch natürlich ist, gleichwerthig erscheint, dass aber für die Darstellung in Länge dem

Bessel’schen Ellipsoid vor dem Glarke’schen der Vorzug gebührt. Für Bessel’s Ellipsoid ist

2& —- 3999, 2% = 320,7,
für das Glarke’sche dagegen
SE 341692, 2%, 590,6.

Das durch die Längengradmessung in 52° Breite mit grosser Schärfe in Länge und
Breite festgelegte Lothabweichungssystem war nun geeignet, als Grundlage für ein europäisches
Lothabweichungssystem zu dienen, mit dessen Berechnung und Ableitung das Gentralbureau
auf der Versammlung der Permanenten Commission in Lausanne 1896 beauftragt wurde’.

Ueber den Plan und den Fortschritt dieser seit Beginn des Jahres 1897 im Gange
befindlichen Arbeit und über verschiedene hierbei bereits erhaltene Resultate sind, um hier
unnöthige Wiederholungen zu vermeiden, die Thätigkeitsberichte des Gentralbureaus für
1897 und für 1898 (die gleichfalls in diesen Stuttgarter Verhandlungen enthalten sind), ins-
besondere aber hierin die Specialberichte der Herren Professoren Börsch und Krüger, zu
vergleichen.

2» Verhandlungen in Berlin, 1895, II. Theil, S. 190; Verhandlungen in Brüssel, 1892,

S. 506/509.
» Verhandlungen in Lausanne, 1896, S. 33/3:

s
=
ä
z
ü
ü
ü
i

ka mn I

tn!

i
|
|

 

 
