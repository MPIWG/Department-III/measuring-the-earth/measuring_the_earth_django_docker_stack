 

|
{
!

 

U AR

el

ne

MP? I RTARIER IL

121

M. le President invite M. le professeur Haid 4 lire son rapport sur les observations

de pendule ex&cutees dans le grand-duch& de Bade pour la determination de la pesanteur.
(Voir Annexe B. II.)

M. Celoria lit ensuite son Rapport sur les travaux de la Commission geodesique
ilalienne pendant les anndes 1897 et 1898. (Voir Annexe B. Illa.)

M.le President ayant demand&si !’on ne desirerait peut-ötre pas des explications au sujet
de ce rapport, M. Celoria le fait suivre de quelques details relatifs ä l’importante jonelion entre
Malte et la Sıcile, sujet qui a deja occupe& autrefois l’Association geodesique. Cette op6ration,
pour laquelle le gouvernement anglais a donnd son consentement, sera enireprise au prin-
temps de 1899 par ’Institut g&ographique-militaire de Florence. I prie M. le President de
donner, dans une des prochaines söances, la parole ä M. Guarducei pour fournir quelques
renseignements sur les travaux pröliminaires exseutes & Institut dans ce but.

M. le colonel Bassot lit le Rappori sur les travaux exscutes en France dans les
annees 1896-1898. (Voir Annexe B. IVa.)

M. le general von Orff communique le rapport sur les travaux geodösiques ex&cutäs
en Baviere dans les anndes 1896-1898. (Voir Annexe B. V.)

Avant de passer ä la lecture du rapport des Etats-Unis, M. le President donne la
parole & M. Anguiano, qui desire faire quelques communications ä ce sujet.

M. Anguiano fait savoir d’abord que le Gouvernement mexicain a inserit dans le
budget föderal la somme ne&cessaire pour la creation d’une Commission g&odesique dont les
travaux, on doit l’esperer, pourront commencer des l’annde prochaine.

Ensuite, il annonce que le Gouvernement des Ktats-Unis a invit& celui du Mexique
prendre part au grand travail que le « Coast and geodetic Survey » se propose d’executer,
et qui consiste dans la mesure du grand arc möridien passant par le 98° de longitude Ouest
de Greenwich. Cet are commence A un point de ’Oc&an Pacifique, pres d’Acapulco, sous la
latitude de 15° environ. Il traverse le territoire mexicain sur une longueur approximative de
1200 kilometres, pour entrer ensuite sur le territoire des Etats-Unis et se terminer enfin
dans les possessions britanniques du Canada sous le 70° degr& de latitude.

On aura ainsi, dans quelques anndes, au Mexique, un reseau trigonometrique
de premier ordre qui non seulement aura une grande importance pour les recherches scien-
tifiques de l’Association g6odösique, mais permettra en m&me temps de continuer le leve
general topographique du Mexique. M. Preston donnera sans doute des renseignements plus’
preeis sur cette grande entreprise.

M. Preston demande la permission de lire en anglais le Rapport du Coast and
geodetic Survey. (Voir Annexe B. VI.)
M. le President exprime ä MM. Preston et Anguiano sa vive satisfaction pour Pini-

tiative prise dans leurs pays en vuede mesurer le grand arc de meridien qui s’etendra ä travers
— 16

 
