VE

1 ni Teen Sm

EM u

NULL AR

Mi rIr  ]RTARNAMM ILLIWITI BALL 2 Ham Lu

 

 

— 307 —

XI. — Pays-Bas,

Notice sur la triangulation des PAYS-BAS.

 

Lorsque le Gouvernement Neerlandais recut en 1861 Tinvitation de partieiper ä
l’entreprise de la mesure des degres de l’Europe centrale, on etait generalement d’avis
qu’il sufhrait de calculer de nouveau la triangulation que le lieutenant general Krayen-
hoff a ex&cutee dans le commencement de ce siecle.(') Une &tude approfondie de cette
triangulation faite par feu D” L. Cohen-Stuart (*) fit voir que cette triangulation ne pou-
vait pas concourir avec les triangulations ex6dcutdes plus tard. Le soin de revoir cette
triangulation fut alors confie & feu F. J. Stamkart.

Apres la mort de M. Stamkart les observations furent suspendues pour &tre reprises
apres l’achevement du nivellement de preeision. Comme la verification des observations
effectuses fit reconnaitre que ces mesures n’ötaient pas assez exactes, la Commission
Geodesique Neerlandaise, qui devait continuer les observations, se vit obligee de con-
clure qu’il fallait renouveler toute la triangulation.

Apres avoir regu du Gouvernement lautorisation d’effectuer la nouvelle triangula-
tion on a commenc6 en 1885 par faire les reconnaissances n&cessaires, qui furent achevees
en 1896. — Deduction faite des stations prussiennes pour le raccordement avec la trian-
gulation de la Prusse et qui ont 6t& observ6es par la Landes-Aufnahme, ce reseau
contient 77 stations & observer par nous. En y ajoutant les 3 stations astronomiques
qui ne font pas partie du reseau trigonomöätrique de 1” ordre (Leyde, Sambeek et
Wolberg) et deux stations secondaires qui doivent servir pour le raccordement de
Leyde, on a en tout un nombre de 82 stations & observer.(®)

En 1888 on a commence la mesure des angles sur les trois stations de la province
de Groningue, qui devaient servir pour le raccordement du reseau N6erlandais avec
celui de la Landes-Aufnahme de Prusse dans la province de Hanovre.

() Krayenhoff, Preeis historique des op6rations geodesiques et astronomiques faites en Hollande.
La Haye, 1827.

0) F. Kaiser en L. Cohen-Stuart, De eischen der medewerking aande ontworpen Graadmeting in Midden-
Europa voer het Koningrijk der Nederlanden. Amsterdam, 1864.

(@) Voir la planche 4 dans les Comptes-Rendus des sdances de la Commission Permanente de l’Asso-
ciation G&odesique internationale reunie A Lausanne du 15 au 21 octobre 1896,

 
