 

|
'
j

Sa re Teen ee |

Hana Rn

Dina

ein

FIInaN. Bm Liam

TE

__

Etats, qui vont se prononcer ulterieurement, le payement integral de leurs parts
contributives pour l’annde courante.

En attendant, l’Association peut pour !’
vrement immediat de son budget complet, p

annee actuelle renoncer au recou-
arce que l’entreprise ä laquelle la plus
grande part de ses ressources est destinde, C’est-ä-dire organisation et Pentretien
de quatre stations astronomiques sous le m&me parallele, dans le but d’ötudier les
variations de latitude, ne pourra probablement ätre mise en @uvre completement
que dans un an ou deux, de sorte que les ressources mäme incompletes, dont
nous disposerons au premier moment, suffiront aux travaux preparatoires et aux
commandes des instruments, si nous sommes assur6s qu’au moment de la mise

en activite complete du service des latitudes nous retrouverons les arriere
premier exercice.

Ss du

Nous espörons que les Hauts Gouvernements approuveront cette mani£ere,
qui nous semble correcte, de rösoudre la diffieulte signalee.

L’article 2 de la nouvelle Convention dispose, dans son troisiöme alinda :
« Pour les affaires administratives non prevues, le bureau de l/’Association pren-
« dra par correspondance l’avis d’une Commission permanente consultative,
« composee des delegues designös A cet effet offieiellement par chaque Etat, &
« raison d’un delögus par Etat.» Afin de pouvoir constituer sans retard cet
important organe de l’Association et consulter prealablement la Commission per-
manente sur certaines questions en vue de la r&union de la Conförence gene-
rale qui sera peut-&tre convoqude pour cet automne, nous prions les Hauts
Gouvernements des Etats qui ont adher6, de bien vouloir dösigner le plus töt

possible leurs delögues a la Commission consultative, et de faire connaitre leurs
representants au Secrötaire perp6tuel soussigne.

 
