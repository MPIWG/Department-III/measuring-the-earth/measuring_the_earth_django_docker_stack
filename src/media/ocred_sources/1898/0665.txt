 

a ee

 

Wa

LymmLL

ZERHABR ILhIBINTN. BMI

Mer [R

Rapport sur l’activit6 du Bureau cen-
tral en 1898, par M. le professeur
Helmert. (Voir AnnexeA. Ib),

Communication de M. le professeur
Albrecht sur les travaux pröparatoi-
res du service international des lati-
tudes. (Voir Annexe A. Ila).

Rapport special de M. le professeur
Tanakadate sur le choix d’une sta-
tion au Japon pour le service inter-
national des latitudes. on Annexe
A. Ib) -

Nomination d’une rrealil, Commission
des latitudes, compos6de de MM. Bak-
huysen, Preston, Bouquet de la u
Celoria et Ferster .

Nomination de MM. Bassce Fe rster,
Hennequin, Rosen et Tinter ocmme
membres de la Commission des finan-
ces.

Troisieme seance, 5 octobre 1898

Rapport special de M. Bassot sur les
Mesures de bases. (Voir Annexe A.
IIl)

Rapport de la Sentten. En rl ige
de la «Landesaufnahmme » de Prusse sur
l’ötat des travaux dans l’annee 1898,
par M. le colonel von Schmidt. (Voir
AnnexeB I).

Rapport de M. le Baer Haid sur
les observations de pendule ex&cutees
dans le grand-duche de Bade. (Voir
Annexe BI).

Rapport de M.le Dec m en sur
les travaux de la Commission g6ode-
sique italienne pendant les anndes
1897 et 1898. (Voir Annexe B. IIla).

Rapport de M. Bassot sur les travaux
executes en France, de 1896-1898.
(Voir Annexe B. IV.)

Rapport de M. von Orff sur les Iran
executes en Baviere, de 1890-1898.
(Voir Annexe B. V). :

Communication de M. Anguiano sur uk
creation d’une Commission g6odesi-
que au Mexique et sa participation &
la mesure du grand arc möridien de
’Ameörique du Nord

Pag.

. 101-418

119

119

119

149

120-125

120

120

121

124

124

121

421

 

Rapport du «Coast and geodetic Sur-
vey» des Rtats-Unis, par M. Preston.
(Voir AnnexeB. VI).

Rapport de M. Sagasta sur les dran
espagnols. (Voir Annexe B. VII) .

Communication de M. PFerster sur le
nouvel alliage en acier-nickel, decou-
vert a Breteuil ei.

Observations de M. inch, sur le
meme objet i

Discussion sur l’importance de ce se
liage.

Quatrieme seance, 7 octobre 1898.

Communication d’affaires par le Seere-
taire . .

Lecture et sdorien Ei nass rules
de la troisiäme sdance.

Rapport de M. Guarducei sur er u
vaux preparatoiresex&cutes par l’Ins-
‚titut militaire g&ographique de Flo-
rence concernantla jonction de Malte
ala Sieile. (Voir Annexe B. III) .

Rapport special du Bureau central sur
les deviations de la verticale, par M.
le professeur Boersch. (Voir Annexe
A. De) :

Rapport special du Bude Con sur
les longitudes, latitudes et azimuts,
par M. le professeur Albrecht. (Voir
Anmnexe A I).

Discussion concernant une nmel
determination de la difference de
longitude Paris-Greenwich et adop-
tion d’une rösolution & cet &gard .

Motion de M. Preston relative & une
nouvelle mensuration de l’arce du
Perou. Discussion et renvoi de cette
motion & une Commission compos6e
de MM. Bassot, Preston et Sagasta.

Motion de M. Preston de nommer une
Commission pour l’utilisation des
travaux göodesiques et des determi-
nations de la pesanteur

Discussion et renvoi de cette au
Bureau central

Rapport special de M. Boreidt die la
Grye sur les observations marägra-
phiques. (Voir Annexe A. V)

8

Pag.

.. 1241-422

122

.. 122-133

. 128-124

. 124-125

126-133

‚120 12%

127

127°

127

. 127-128

. 128-129

. 130-4131

131

. 131-133

133

|
N
j
}

EN EEE ER

ee:

 
