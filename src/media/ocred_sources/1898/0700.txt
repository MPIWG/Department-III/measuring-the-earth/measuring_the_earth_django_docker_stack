TR ANA

 

BASE MESUREE | PRÄCISION DE LA BASE GALCUL

 

 

NOMS

DES PAYS

 

 

 

NOM ET ANNEE DE LA MESURE

LONGUEUR

DE LA BASE

EN PARTIES
DE LA
LONGUEUR

—
ERREUR MOYENNE |
|
|
|
|
|
|

MESURfE

EN UNITHS | NOMS DES POINTS EXTREMES

DU |
7° ORDRE
DU LOG.

 

LONGUEUR

M.

()

 

 

Italie ©...

 

Foggia, 1858.

Napoli, 1862...

Catania, 1865

Crati, 1871.....

Lecce, 1872

Rame, 1874... 2.2.
Somma (Ticino), 1878

Order, 189..,.2..%,,,

Piombino, 1895

3030, 421
663,119
3692, 180
2919, 553
3044, 230
3248, 579
9999, 538
3402, 229

4621, 507

£

1335 000

Er.
972 000
592.000

I
749 000

1
1041 000
8
1502 000
I
2.280 000

nn
1877 000

ı:
952 000

|
|
3,8 | Biceari-Montedoro ...........

4,5  Camaldoli-Taburno...

 

: ' Montirossi-Perriere

|

|
5,9 S. Salvatore-Mostarico

|

Soleto-S. Eleuterio
|

Udine-S. Vito al Tagliamento.

|
|
| < ee
\ Busto Arsizio-Campo dei Fiori
||

|

||

| Bonaria-Rasu

|

u

Massoncello-Montieri..

26909, 994
43336, 129

30758, 606

30481, 487

15751, 880

33805, 809

29409, 418

43611, 065

45056, 138

 

 

Prusse®,,

(Landes-
Aufnahme)

 

Könisberg, 1834 7
Berlin, 1846 ®

Strehlen, 1854 ®

 

(%)

1822, 3599

2336, 4029

62, 5825

5875, 3295

 

(3)

Su
690 000

1
1390 000

1
1340 000
a
2 140 000

 

|

| (

| Könisberg-Galtgarben ”
(Gradmessung in Ostpreussen)

| Eichberg-Müggelsberg ®

(Küstenvermessung, sudl. Theil)

Goy-Rummelsberg
(Schlesische-Dreieckskette)

 

Baursberg-Bombeik 9
(Schleswig-Holsteinsche Kette)

 

(2)

21014,130 |

36906, 441°
26146,298

37415, 321

 

 

 
