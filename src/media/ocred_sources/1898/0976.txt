     

   

 

— 260 —

IX. — Italie.

 

Furono eseguite le osservazioni angolari a tutti i punti di 1° ordine della rete geo-
detica compresi nell’ Italia continentale, nella Sardegna e nella Sicilia ; (') mancano sola-
mente quelle ai punti Capraia e Montecristo, perch& interessanti il progettato colle-
gamento della Sardegna al continente.

Le posizioni geografiche sono calcolate di punto in punto adottando gli elementi
di Bessel; le longitudini sono contate a partire dal meridiano dell’ isola del Ferro

I confronti ottenuti con gli Stati limitrofi, dopo compensate le reti, sono riportati
nello specchio che segue. (?) Come vedesi, le differenze sono di poca entitä e mantengonsi
costanti, ciö che depone in favore dell’ accordo fra le varie triangolazioni.

 

 

 

 

 

 

 

 

 

 

 

VATORIL
DELLE LATITUDINI COMUNI DELLE LONGITUDINI COMUNI
Werne DITTERENZA | Fr | DIRHERENZA
ITALIA STATI LIMITROFI TE ITALIA STATI LIMITROFI I-ER
in BE I E
Francia. Francia.
Meontenbabor.......... . 45°. 06' 49” Aloe al ı 00 24° 13. 397 24° 13' 39” = 02
» Ghaberton ..... 44 57 52 44 57 54 — 2 24 24 53 24 24 53 — 0
Kocciamelone......... AS 12 11 4912 15 — 2 24 44 27 24 44 28 — |]
Svizzera. Svizzera.
 Limidario o Ghiridone.. 46°. 07' 23” 46°.07' 26” a 26° 18’ 43” 26018, 15% — 27
Basedme.. ......... 46 24 41 46 24 44 —. 9 26 07 56 26 07 59 „nn
-Gramosno.n 2... 46 21 46 46 21 49 — 83 26 30 26 26 30 28 — 2
Menone.. 0.2... 46 07 24 46 07 27 en 26 48 31 26 48 34 — 93
Austria. Austria.
Belacesa... .......... 42223 30” A223 33% — 3” 33055! 42” 33059, 48% — 6%
Deemame.. 42.07 18 a2 all — 3 33.10 16 33 10) 22 9
Govanniechio......... A] 50 01 41 50 04 — 3 33 28 2] 33.38 26 — 5
Balder. ....2...2.28 A A2 2 45 42 24 — 3 28 29 4] 28 29 46 =>
Basublor.: 2.2... An Au 32 4547 35 —_.ı 28 50 27 28 50 32 5
Grappa... 2.02.22 45 52 24 a2 53 29 27 48 2 2 53 — 5
02 ....... 46 03 53 46 03 55 | ..n 30 54 05 30 54 10 2,5

 

 

 

Nelle sedute della Commissione permanente riunita a Nizza nel settembre del 1887
il relatore generale Ferrero pose in evidenza la necessitä d’introdurre nei Rapporti
sulle triangolazioni l’error medio angolare provvisorio da attribuire alle singole reti.

() Per la Sieilia vennero riosservate nel 1895-97 tutte le stazioni dell’antica rete, le quali precedente-
mente erano state eseguite con strumenti ripetitori ed a solo scopo cartografico.

() In questo specchio le longitudini dei punti dell’ Italia settentrionale sono alquanto differenti da quelle
riportate nello speechio degli anni precedenti, perch& in seguito alla compensazione delle reti XIV, XVeXVI
eseguite in questi ultimi anni, risultö la differenza di longitudine tra M. Mario e Genova di 2" pi piccola di
quella precedentemente calcolata

a hs
