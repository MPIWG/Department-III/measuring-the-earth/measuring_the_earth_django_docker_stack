 

927

 

II. — ORGANISATION DES EXPERIENCES.

Les experiences du colonel Goulier, effectudes de seplembre 1883 & mai 1884, ont
port& sur quatorze blocs de bois de douze essences difförentes (voir le tableau | ci-apres), al-
ternativement soumis & Pinfluence de la chaleur et de l’humidite.

On a employe& des bois anciens, bien secs, sans neuds el, autanl que possible, de droit
fil dans le sens de leur longueur, debites depuis trois ans sous la forme de minces regles

de 1710 de longueur et 13 millimetres sur 7 de section.

Pour chaque nature de bois, quatre regles
(fig. 1), de maniere A presenter ä peu pres la
extremites de chaque regle, dans des trous perces suivant l’&paisseur, on avait viss6, A un

metre de distance d’axe en axe, deux petits eylindres filetes de laiton (fig. 2) portant chacun
transversalement, sur la Lranche, un trait de repere.

avaient &t& prises dans un m&me moreeau
m&me constitution moleculaire. Aux deux

Plan et coupe de Vextremite d’une regle.

Mode de debit des bois

et numerolation des regles.

 

9208

 

rum Ba
nen

 

 

   

 

 

 

 

 

Bios:

Kig. 2,
(Grandeur naturelle.)

(Grandeur naturelle.)

Des quatre rögles provenant d’un m&me bloc, ’une, m
plaqueites de laiton, &tait conserv6e A l’etat nalurel ; la seconde, marquee du chiffre 2, avail
ete recouverte, en octobre 1880, de trois couches de peinture & ’huile et au blanc de ceruse ;
la troisieme, affectee de l’indice 3, avait &l& bouillie, pendant une heure & une heure et
demie, dans de l’huile de lin & une temperature voisine de 150 degres, puis maintenue dans
"huile pendant vingt-quatre heures Jusqu’ä complet refroidissement ! ; enfin, la quatricme,
= portant le chiffre 4, avait &i& mise en reserve pour des essais ulterieurs.

Avant de commencer les experiences, on avait eu
essence de bois, sa densite, l’epaisseur des couches
par les regles soumises & cette preparation.

arquee du chiffre 1 sur les

7)

soin de mesurer, pour chaque
annuelles et le poids de l’huile absorbe&e

Les resultats obtenus sont consignes dans le tableau ei-apres :

 

"On a aussi fait quelques essais avec des r&
rante-huit heures dans un melange, A volumes
que ce mode de preparation ne diminuait en rie
n’a pas poursuivi cette tentative.

gles qui avaient &t& maintenues pendant qua-
egaux, de glyc£rine et d’eau ; mais on a vite reconnu
n la sensibilite des bois a action de Phumidite et l’on

In U RRNARR IL IRIEN. BR LINE mL

Mimi

 
