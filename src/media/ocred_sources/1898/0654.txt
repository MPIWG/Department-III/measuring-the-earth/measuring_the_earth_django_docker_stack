 

 

2 m.

main et Varsovie, et a communiqu6 un tableau des erreurs moyennes dans le
transport des bases aux premiers cötes du röseau de premier ordre (voir G.-R.
de 1894, p. 76-79). Mais le principal sujet a &t& l’&tude des variations de latitude,
que M. Albrecht a trait@ de nouveau dans un rapport (voir Annexe A. II, p. 131
a 156), qui comprend aussi les observations amöricaines de San Francisco et de
Honolulu. — De son cöte, M. Marcuse a &tudi& 6galement les mouvements du
Pöle Nord, d&duits des observations de 1891 & 1894 (voir C.-R. 1894, Annexe
A. III, p. 157-162 avec un tableau). M. le Directeur Helmert enfin a examine Ile
mouvement reel du Pöle Nord, dans une notice communiquöe pendant la möme
session (voir p. A0 et 90, ainsi que Annexe A. IV, p. 163-166).

Dans la Conference generale de Berlin, en 1895, le Bureau central a rendu
compte du caleul de compensation de Parc de longitude sous le 52° parallele (voir
G.-R. de 1895, p. 118, 119). Il a prösent& deux cartes synoptiques, l’une indiquant
l’etat actuel des travaux trigonomötriques en Europe (voir Carte I du 2e Tome des
C.-R. de 1895), Vautre ceux des Etats-Unis, dressee par le Coast and geodetic
Survey (Carte II du 9° Tome). Sur la premiöre, M. Helmert signale la lacune des
triangles autour du 47 !/,° en Autriche-Hongrie, devant joindre la chaine russe
aux chaines suisse et francaise (voir C.-R. de 1895, p. 119), et sur Pautre carte il
releve comme nouvel elöment important la grande chaine transcontinentale am&-
ricaine suivant le 39me parallele.

L’intöressante question des variations de latitude fait l’objet d’un rapport
de M. Albrecht (voir Annexe A.I, p. 1-26, avec 2 cartes) ; et M. Helmert lui-m&öme
presente de nouveaux rapports sur les mesures relatives de la pesanteur au moyen
du pendule (voir Annexe A. VI, p. 118-179, avec 5 cartes) et sur les döviations de
la verticale (voir Annexe A. VII, p. 180-191).

M. le Directeur du Bureau central a soumis en mäme temps dans son
rapport les comptes de gestion des fonds de dotation de l’Association pendant la
periode triennale de 1893/95. (Voir Appendice V®, p. 290-291.)

Les m&mes Comptes-Rendus de 1895 contiennent encore sous forme d’Ap-
pendices les documents suivants :

« Resume des propositions recues par le Bureau central au sujet du re-
nouvellement de la Convention d’octobre 1886. » (Voir Appendice P, p. 257
a 264.)

terms

 
