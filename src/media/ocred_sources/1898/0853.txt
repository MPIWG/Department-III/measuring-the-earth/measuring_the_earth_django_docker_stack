  

   

 

 

— 137 —

|
|
|

VII.» — Grande Bretagne (Inde).

 

no| INDICATION |

LATITUDE LONGITUDE

on DIRECTEURS
DES POINTS | me

ET OBSERVATEURS

 

 

 

INSTRUMENTS REMARQUES

 

 

 

 

 

N. W. Quaderilateral.

Karachi Longitudinal.

 

 

 

 

 

 

 

 

 

Surantäl (Sironj).. | 24°14'20”| 77°40' 43”) 1849 Capits.T.Renny, A.Strange.| T. et S. 36".
Käamkherä » ..1 235945 | 7743 6 1849 id. id.
1 | bosalli.......... 24 619 3 e 1849 id. id.
2 | ABan een 23.00 93 0 28.99 1849 Pk Strange, lieut. H. id.
ivers
3 Dımslar. 22.02 =. 0 28 | 7 \& &0 1849 en ce Rivers, id.
r. C. Lane.

4 | BRämpür.......... 241150 | 11 25 42 1849 id. id.
5 | Salot............. 24.14 52 | 1215 2 1849 Renny, Strange, Rivers. id.
6 | Damd............ 24:4 3 71.05 1849 Renny, Strange, Lane. id.
7 | Hatni........... 24:30 29 | 7213.29 1849 Renny, Strange. id.
8 | Nändna..,........ 24 22 23 | 76 58 54 1849 Renny, Strange, Lane. id.
9 | Maätä-kä-hürä..... 24 14 11 | 76 36 48 1849 id. id.
10 | Dawa .........-.- 23 49 18 | 76 36 57 1849 id. id.
pl | Sartal,.:....2... 24.30 4 16.31 16 1849 Strange. id.
12 |, Rangäon ........-. 23:54 35 | 16.23 6 1849 Renny, Strange, Lane. id.
13 | Kusalpürä........ 2A 3 So DA 1849 Strange. id.
14 | Bänskati......... 24 34 50 | 76 15 59 1849 id. id.

15 | Panchäwa........ 24.7 45 |\ 19 56 48 1849 id. id. >

i6.| @uraria....o. 0... a3 65. 1849 id. id. °

7 Khaıei... 24 14 14 | 75 43 28 | 1849-50 id. id.
i 18 | Nimthur.......... 24 32 1 | 75 47 34 1849-50 \ id. id.
= 19 | Dhamnär......... 24 11 38 | 1520 0 1850 id. id.
= 20 | Rämpürä......... 24 28 44 | 75 26 51 | 1849-50 id. id.
SE, Buda. 2. 24 14 12,75 8315 1850 Strange, Lane. id.
= 22 | Nanka Hüäro..... 24 31 48 | 75 14 32 1850 Strange. id.
= 23 | Aramlia......... 2424 IK A098 1850 id. id.
=. 94 | Bälagarıa...... . 24 10 22 | 74 57 48 1850 id. id.
1 25 | Gopälpüra........ 24 17 34 | 74 46 55 1850 id. id.
j 26 | Mendki...,...... 24 38 16 | 74 53 12 1850 id. 0.
27 \ Barra Sädri...... 2A 23.21, 1A 29 14 1850 id. id.
28 Sand. .....0.3%- 2A 43 6 | 74 3258 1850 id. id.
29 | Tanarn . 0.002... 2443 4| A110 1851 id. id.
30 | Borikalor......... 24 20 52 1.74 12 34 1851 id. id.
3 31:| Bharak..........- 25.822) 4162 1851 Il. id.
: 32 | Lakarwas .......- 24 31 48 | 73 49 42 1851 id. id.
33 | Tikl-..,.2...008 24 55 38 | 73 50 44 1851 id. id.
34 Berater 94 AT 1).| 73.36.02 1850 id. id.
35 | Marwar:...... . 24 26 20 | 73 32 45 1851 id. id.
36 | Mäl Niver........ 24 59 22 | 73 36 29 1850 id. id.
37 k Zelle. .:. 25.2... 24134 20.| 3 9 15 1851 id. id.
38 | Kännagar ........ 24 58 29 | 73 18 59 1850 Id, id.
39 | Belka........... 24 46 55,789 15 1851 Lane. id.
40.:,.Maärd............. 24 24.9 | 12 32.20 1851 id. id.
41 | Bonik........ 25 352 | 72 51 54 1850 Strange. id.
42 | Gürü Sikkar.....: 24 38 58 | 72 46 39 1850 Strange, Lanc.. id.
43.1. dera]...... >: 2425 0,1230 2 1851 Lane. id.
44 ı Sunda..... ı... 241465 2 211 1851 Strange. : id.
: 45 | Bargäon.........: 24 40 29 | 72 14 55 1851 id. id.
: 46. ). BIFOBB: 4.0. 24 26 39) 7213-4 1851 id. id.
3  4HTBanie,.. 24.49 8|72 14 2 1851 id. id.
z 48 | Siiora.2....0 21303512 63 1851 id. id.
® 49 7. Atithok 0: 24 A222 Al 1851 id. id.
3 50: Ball 24 52 50 | 721 50 1851 id. id.
x 5l | Khamkharia.....- 2A Be 8 1851 id. id.
z 52 | Koklan. an | 24 46 43 |71 5331 | 185 id. id.

& | I

 

 

n'r

 
