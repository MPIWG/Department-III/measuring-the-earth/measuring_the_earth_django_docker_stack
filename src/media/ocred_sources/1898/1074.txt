— 358 —

XIV. — Prusse, B— Erreurs de clöture des triangles desquels ont et mesures les trois angles

 

N. D’ORDRE

AUTEURS DES INSTRUMENTS
EMPLOYES
ET
DIAMETRE DU CERCLE
HORIZONTAL

LECTURE
EN
SECONDES
DU VERNIER
OU DU
MICROSCOPE

NOMBRE
DES
VERNIERS
OU DES
MICROSCOPES

NOMBRE
DES
OBSERVATIONS
EX£CUTEES
POUR
CHAQUE ANGLE
OU POUR
CHAQUE
DIRECTION

ERREURS
DE CLÖTURE
DE
CHAQUE
TRIANGLE
A

Ba

REMARQUES

 

 

 

 

 

 

15 zölliger Theodolit von Ertel (Thei-
lungsdurchmesser = 355"n),

 

Ablesung un
mittelbar von
2sekunden
bezw mittel-
bar von'/,se-
kunden.

id.

 

bis 1839 ein-
schl 4 nonien
Von 1840 ab
4 Mikroskop-
Mikrometer,

id.
id.
id.
id.
id.

id.

Lil®,

 

24

 

0", 189

0
0
0
0
l
0
l
1
l
0

 

KÜSTENVERMESSUNG SÜDL. THEIL 1841-46.

15 zölliger Theodolit von Ertel (Thei- |
lungsdurchmesser — 355"n),

Ablesung
mittelbar von
'/, sekunden.

id.

id.
id.
id.
id.
id.

4 Mikroskop-
Mikrometer.

id.
id.
id.
id.
id.
id.

24

0", 597

 

 

Veröffentlichungen
N. 2 und N. 12,

Veröffentlichungen

N.2 und N. 12.

 

 

 

 
