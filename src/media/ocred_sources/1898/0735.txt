|
!
\

219g

I. — Autriche-Hongrie.

  

 

INDICATION

 

 

DIRECTEURS

 

  

 

 

 

MI IRIR I DR UEAIAR LLhNIRTI 0m Lian pm Lin

 

 

 

 

 

 

 

 

 

 

[0 Un & Do n / T 1 2 6 1 URL
N DES POINTS LATITUDE |LONGITUDE SED ET OBSERVATRURS Be BRD,
614 0wası.... 1894 Scharf, Schwarz. Starke n°2 et 5 de
10 p.
6la  Hegyes.. 2... 1894 Scharf. Starke n®5 de 10 p.
616 | Segenthan......... 1894 Schwarz. Starke n°2de 10 p.
617 , Kurtics..... 0... 1894 id. Side u
618 | Balvanıa .... A482 6, 12, 580 8.202 1897 Truck, Guttmann. Starke n?5 et 442 de
10.»
619 | Heja toja......... 481947, 388 2 1897 v. Sterneck. Starke n? 3 de 10 P-
620 , Poronya tetö...... 4829 I >32 931 1897 | id. id.
621 | Rubahegsy........ A821) 3 1 37 42 2 1897 Lehrl, v. Sterneck. Starke n® 442 et, 3
de 10 p.
622 | Als6ö Bagolyhegy ..| 48 3 47 | 38 19 22 1897 Truck. Id. n? 442 de 10 p.
623 | Strarsahaloem...... A8 351 |, 38 44 19 1897 v. Sterneck. Starke n® 3 de 10 p.
624 | Frankhegy........ 48.1233 38 3251 1897 id. id.
625 | Magoska.......... As 2, 2 5& 9718 1897 Guttmann. Starke n°5 de 10 pP.
626 | Ujlaki bercz...... AS 13.3839 5 1897 Guttmann, v. Sterneck. Id.n®3et5del0p.
627. Milien.. 2.2.2.2... 48 3439 | 39 730 1897 Truck. Id. n? 442 de 10 p:
628 | Magashegy........ 4824 9391750 1897 Guttmann, Schwarz. Starke n’5 de 10 p.
629 | Tokajehegy ....... As 715,539 259 1897 Mayer, v. Sterneck. Starke n° 3de lOp.
630 | Asszonyhegy ...... 48 12 37 | 3946 36 1897 Schwarz. Starke n’517de8p.
631 | Kiralyhegy........ 48 28 8 | 39 37 42 1897 Schwarz, Truck. Starke n?517de8p.
et n’ 442 de 10 p.
632 | Pustawmik......... 48 42 30 | 39 28 28 1897 Guttmann. Starke n’5del0p.
633 | Putka Helmeecz.. 41832 57,40 322 1897 Schwarz. Starke n’51l7de8p.
634 | Zapsony hegy..... 48 16 54 (40) 753 1897 Guttmann. Starke n’5de 10 p.
635 | Beregi Nagyhegy..| 48 11 29 | 40 21 12 1897 Schwarz. Starke n°517de8p.
636 | Nagyhegy......... 28 2514,10 22 5 1897 id. id.
= .637:| Makoviea......... 48 39 38 | 40 15 47 1897 id. id.
- - 638 | Poprieni vrh...... 48 46 > Al 8 1897 Guttmann. Starke n’5de 10 p.
= 639 | Polonina runa.....| 43 48 40 28 36 1897 Schwarz. Starke n’5l7de8p.
2.6901 810).22......2. 48 37 . 40 51 29 1897 Guttmann. Starke n’5 de 10 p.
641 | Fekete hegy...... 1897 Schwarz. Starke n’517de8p.
= 642 | Kakas hegy....... 1897 id. id,
= 643 | Base de Szätmar
. (Terme 8.0)... 1897 id. id
= 644 | Base de Szätmar :
a (Terme N. R.).... 1897 id. id.

 
