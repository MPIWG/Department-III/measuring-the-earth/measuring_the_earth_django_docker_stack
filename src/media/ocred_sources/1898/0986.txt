 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

— 270 —
IX, — Italie
 INDICATION | | |
DES POINTS LATITUDE |LONGITUDE | EPOQUE DIRECTEURS OBSERVATEURS INSTRUMENTS REMARQUES
I ei
M. Baldo:.. ..... 45°42' 20" | 28°29' 39" 1882 Col. Ferrero. Ing. Ginevri. Starke de 27 em.
Aquileja.......... 45 46 10 | 31 02 04 1882 id. 0» DoAter, id- |
Hderzo.....,..:... 45 46 50 | 30 09 25 1882 id. » Domeniconi id.
M. Pasubio........ 45 47 31 | 28 50 25 1882 id. » Cloza. id.
M. della Grappa...| 45 52 23 | 29 27 46 1882 id. » Domeniconi id.
Ss. Vito al Taglia- |
MENKO au 45 54 53 | 30 31 15 1882 id. » Cloza. id.
Vico. er... 45 56 19 | 30 44 00 1874 Lieut. Col. Chiö.| Cap. Maggia. | id. |
Base de Udine....| 45 58 47 | 30 41 42 1882 Col. Ferrero. Maj. Maggia. id.
Terme S.-O. a /
Base de Udine.....| 45 59 38 | 30 43 55 1882 id. id. id.
Terme N.-E.
Pantianicco....... 40 01 17 30.41 50 1874 Lieut. Col. Chiö.| Cap. Maggia. id.
Udine............: 46 03 51 | 30 54 03 | 1874-83 | Col. Ferrero. Ing. Cloza. id. I
Muzayeo.:........ 45 58 40 | 31 28 11 en
Spilimbergo....... 46 06 35 | 30 34 08 | 1874-83 |Col. De Stefanis. | » Derchi. id. un
M. Cavallo........ 46 07 46 | 30 09 39 | 1884 id. >» Cloza. id.
Mi 2Z0C......... 46 08 23 | 29 40 25 1883 Ko » Derchi. id.
Cima dAsta ...... 46 10 35 | 29 16 08 nn
Moon... 46 21 25 | 31 06.05 | 1884 id, en nr a
M. Verzegnis...... 46 21 46 | 30 34 10 1884 id. Ing. Guarducei. id.
Marmolada........ 46 26 03 | 29 30 54 1882 » Ferrero. Cap. Simi. id.
M. Antelao........ 46 27 06 | 29 55 32 1885 » DeStefanis.| Top. Tacchini. id.
M, Peralba........ 46. 37 45 | 30.23 01 1882 » Ferrero. Cap. Simi., id.
xl.
REGION Ä L’EST DE LA MERIDIENNE DE MILAN.
Reseau de la Lombardie.
Mu lesarı 2... | 44°33' 55'| 28°06' 56" 1883 Col. De Stefanis. | Cap. Pescetto. | Starke de 27 em. N
Modena........... | 44 38 45 | 28 35 22 1885 id. Ing. Guarduceci. id. |
M. Carametto .....| 44.40 46 | 2725 56 | 1883 id. » Derchi. id. |
M. Benice......... 44 47 05 | 26 58 52 1877 » Ferrero. > Puece, id. I
Parma (Observat.) 44 48 01 | 27 59 20 1883 » DesStefanis.. » Derchi. id. |
Miadana, ...;...., 44 55 33 | 28 11 04 1883 id. » Guarducci. id. |
8. Benedetto Po... Dejä mentionne. )
Biaeenza. ..,... ‚45 03 00 | 27 21 39 | 1883 id. id. id. |
Cremona.......... 45.08 00 | 27 41 19 1883 id. » Ginevri. id. |
Bamaı 2.0.0... 45 11 04 | 26 48 59 1879 >» Nerneno: | > Jadanza. id. |
Kali... 2... ... ' 45 18 49 | 27 09 59 1883 » DesStefanis. » Domeniconi id. |
Solferino ......... D&ja mentionnd. |
Milano (Duomo)..., 45 27 50 | 26 51 19 1879 » Ferrero. » Guardueci. id. |
Milano (Brera)..... 45 28 15 | 26 51 10 1879 id. id. id. |
Narl............ 45 32 09 | 27 35 33 1882 id. » Ginevri. id. |
Ip... 45 34 37 | 27 11 35 || 1882 id. id, id. |
M. Baldo.......... D6jä mentionne.
Corno Blacea ..... 45 47 27 | 28 02 39 1882 id. » Derchi. id. |
M. Palanzuolo..... 45 51 42 | 86.51 54 1881 id. » Ginevri. id. |
M. Arera.......... 45 56 03 | 27 28 46 | 1882 id. id. id. |
Pizzo Menon...... 46 07 24 | 26 48 28 1881 id. id. ide |
Corno Baitone ....| 46 10 21 | 28 06 17 || 1883 » DesStefanis. » Cloza. id. |
M. Disgrazie...... 46 16 09 | 27 24 47 1884 id" id. id. |
M. Cramosino..... 46 21 46 | 26 30 24 1881 x» Norrero, Gap. Bona,. id. |
Cima di Piazzi....| 46 25 00 | 27 56 56 | 1883 | » DeStefanis. Ing. Guardueci. id.
Me, 46 29 46 1883 id. » id. |

Pizzo Tambd

 

 

 

 

 

 

 

 

 

 

26 56 44

 

 

 

 

 

 

Domeniconi

 

 

 

 

reed ehe

2 nich Mb hi an
