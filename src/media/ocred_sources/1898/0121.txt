 

| NN een rennen 00

Ba huye inf

far

Da AR

dt

Pr TRIER TI FIRE TI

113

9. Pendule italien oseillant sur les couteaux en acier du pendule lourd

mm

dk. “3 ı io L = 994,946
6. Pendule leger, flexible, de II. G., oseillant sur les couteaux en agate

du pendule ä demi-seconde. La correclion appliquee pour la

flexion s’eleve a — 0"""366. (Voir Helmert, Contributions, p. 28). L = 994.957

Sur le nouveau coussinet, on aexecutd Jusqu’ä präsent la serie suivante d’observations,
dans lesquelles on a utilise pour tous les pendules la möme paire de couteaux. Les temps d’oseilla-
tion ont&le determinds, soit avec la grande amplitude (19° environ), soit avec la petite amplitude
(4 environ); chacune de ces combinaisons a 616 repetde, apres avoir tourne le pendule
autour de son axe de longueur.

mm Observateur.

1. Pendule ä demi-seconde de I. G., grande amplitude L — 994,911 Dr Furtwängler.
» » 227 »
petite » 233 »
» » 231 »
2. Pendule leger aulrichien, grande » LE = 992931 »
» » 236 >
petite » 241 »
» » 2992 »
994,238
3. Pendule lourd autrichien, grande amplitude L = 994,937 D* Kühnen.
» > 216 D* Furtwängler.
petite » 226 Dr: Kühnen.
» » 223 D" Furtwängler.
394 20)
4. Pendule italien, grande amplitude L = 994,938 Dr Kühnen.
» » 237 D: Furtwängler.
petite » 237 Dr Kühnen.
» » 234 Dr Furtwängler.
| 994,237
5. Pendule lourd de PI. G., grande amplitude L = 994,230 Dr Kühnen.
» » 233 D" Furtwängler.
petite » 229 D" Kühnen.
» » 233 D" Furtwängler.

994,931

Chaque resultat individuel est deduit d’une combinaison complete d’observations, en
eliminant V’inegalite des eouteaux, soit par l’&change des poids, soit par l’&change des cou-
teaux. Le schema d’observations a &t& dans la regle le suivant :

— 15

 
