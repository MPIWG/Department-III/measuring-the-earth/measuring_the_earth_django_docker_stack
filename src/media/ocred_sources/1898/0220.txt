 

 

 

 

208

 

Beziehung grosse Verschiedenheiten im Sommer und im Winter auftreten. Da über-
dies aus den eben angegebenen Gründen (erhöhte Refraktionsstörungen, Neigungs-
schwankungen infolge der rasch sich verändernden Temperatur, und zeitweise Nebel-
und Wolkenbildungen zur Zeit des Sonnenunterganges) eine allzugrosse Annäherung
an den Sonnenuntergang nicht rathsam erscheint, so wird das beste Auskunftsmittel
sein, dass man einen Mittelweg einschlägt und die Beobachtungen im Sommer
frühestens 14" und im Winter 24% nach Sonnenuntergang beginnen lässt. Auf diese
Weise gelangt man zu dem nachstehenden Beobachtungsplan, welcher auf der bei-
liegenden Tafel graphisch dargestellt ist.

Gruppe. Sternzeit. Beobachtung. Dauer der

 

 

ei

 

Gruppe. Anschlüsse.

I of— zh Sept. 23 — Dec. 6 75 Tage a

II 2 —4 nn 2 — Jan. 4 Da a
III 4 — 6 Dee. 7 — Jan. 30 Se < 2
1V Berg Jan. 5 —- Bebr. >24 , m
V 8 — ı0 Jan. 31 — Mäız 2ı co a
vi Io — ı2 Febr. 25 — April ı5 Bo, a
VOII: 12 — 14 März 22 — Mai ıı u . 2
VII 14 — ı6 April 16 — Juni 8 Sn Bi
IX. ı6 — ı8 Mai ı2 — Jui 9 co 2
X 18 — 20 Juni 9 — Aug. ı3 00 >
RX 20 — 22 Juli 10 — Sept. 22 To an
XL 22 — 24 Aug. 14 — Noy. ı so, vo 2
”

h

Die Beobachtungen liegen im Sommer zwischen 9" Abends und 3' Nachts und
im Winter zwischen 7" Abends und ı" Nachts. Die Beobachtungszeit beträgt während
des ganzen Jahres gleichmässig 4" ohne Zwischenpause. Die Dauer der Anschlüsse
schwankt zwischen 25 und 4o Tagen.

Was ferner die Anzahl der Sternpaare anlangt, welche innerhalb einer
jeden Sterngruppe zu beobachten sein würden, so empfiehlt es sich, je 6 Sternpaare
zur eigentlichen Breitenbestimmung auszuwählen, deren Komponenten in Intervallen
zwischen 4” und ı5” aufeinander folgen, und deren Zenitdistanz-Differenzen den Be-
trag von ı5' nicht überschreiten. Als Grenzwerth der absoluten Zenitdistanz würde
der Betrag von 25° festzuhalten sein. Zu diesen 6 Sternpaaren würden 2 weitere
Sternpaare in etwa 60° Zenitdistanz hinzuzufügen sein, um innerhalb der Beob-
achtungen selbst Anhaltspunkte zur Feststellung etwaiger Refraktionsanomalieen zu
gewinnen.

Helmert. Albrecht.

 

ee Mel ih ul

Ik ll

 

 
