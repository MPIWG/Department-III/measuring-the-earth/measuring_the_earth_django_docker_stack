DER ee rene ee +

 

=

oma

L MU 1

TET ERE RTF TI

53

Dritte Sitzung

der permanenten Commission.

Im Haag am 12. September 1882.
Der Präsident eröffnet die Sitzung um 2 Uhr.

Derselben wohnen alle in der Vormittagssitzung erschienenen Mitglieder bei,
und überdies die Herren Bosscha und Buys-Ballot.

In Fortsetzung der in der Vormittagssitzung gepflogenen Discussion über ge-
brochene Fernrohre äussert Herr Fearnley, dass er Gelegenheit gehabt, eine grosse An-
zahl von Prismen aus verschiedenen optischen Instituten zu prüfen und dabei eine nicht
geringe Ungleichartigkeit ihrer optischen Eigenschaften zu constatiren, so dass dieselben
theils als vorzüglich, theils als ganz mittelmässig bezeichnet werden müssen. Er meint,
dass die von Herrn v. Oppolzer bezeichnete Ursache in den ihm vorschwebenden Fällen
nicht zutreffe, dass hingegen viel auf den Schliff des Prismas ankomme, der häufig
cylindrisch ausfalle; ausserdem dürfte die Einschaltung eines dicken Glaskörpers in den
Strahlenkegel von nicht günstigem Einfluss auf die Schärfe des Bildes sein.

Herr Villarceau hält ebenfalls dafür, dass die Benützung eines Prismas die Bild-
schärfe stets ungünstig beeinflusse, sucht aber den Hauptfehler gebrochener Fernrohre
in der Variabilität des Collimationsfehlers, welche er mit Rücksicht darauf, dass sehr
kleine lineare Verschiebungen in den Auflagspunkten des Prismas verhältnissmässig grosse
Aenderungen in dem Collimationsfehler bedingen, 30 mal höher schätzt als für die
geraden Fernrohre. Aus dieser Ursache steht Herr Villarceaw nicht an, die Anwendung
gebrochener Fernrohre für genaue Beobachtungen zurückzuweisen.

Herr Oudemans fürchtet den eben erwähnten Nachtheil nicht, weil innerhalb eines
mässigen Zeitintervalls der Collimationsfehler wohl als constant betrachtet und durch
eine symmetrische Anordnung der Beobachtungen kleine Fehler in den Annahmen über
diese Quantität eliminirt werden können.

Herr ». Backhuyzen glaubt, dass man neben den Nachtheilen der gebrochenen
Fernrohre auch deren Vortheile berücksichtigen müsse, unter welchen namentlich die
Verwendung sehr niederer Lagerstützen ins Gewicht fällt, weil hierdurch die Stabilität
des Instrumentes erhöht und der schädliche Einfluss der von dem Beobachter aus-
strahlenden Wärme auf ein Minimum herabgesetzt wird.

Herr Perrier macht an die letzte Bemerkung anknüpfend darauf aufmerksam, dass

 
