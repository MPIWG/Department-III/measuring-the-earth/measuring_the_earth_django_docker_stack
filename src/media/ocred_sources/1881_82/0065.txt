er zz

 

=

Teper ies

mire

=
8
:
E
=
=
5

à

57

 

keit thatsächlich auf die Höhe der Präcision des Reversionspendels erhoben werden könne.
Bei dem Gewinn, welchen die Wissenschaft in diesem Fall erführe, seien vergleichende
3eobachtungen an beiden Apparaten auf mehreren Stationen anzubahnen und zu diesem
Zwecke schlage er vor: „Die Pendel-Commission werde beauftragt, zwischen denjenigen
Mitgliedern der Europäischen Gradmessung, welche den Cellerier’schen Apparat besitzen,
einerseits und denjenigen, die den Villarceau’schen Apparat zu benutzen in der Lage
sind, andererseits ein Einvernehmen zu dem Zwecke herzustellen, um die Resultate der
beiderseitigen Beobachtungen vergleichen und das Endergebniss der nächsten Allgemeinen
Conferenz vorlegen zu können.“

Herr Perrier und v. Oppolzer unterstützen diesen Antrag, der hierauf von der
Commission angenommen wird.

Der Präsident stellt an Herrn Faye, welcher durch einen Eisenbahnunfall leider
verhindert war, an den ersten Sitzungen theilzunehmen, die Frage, ob er den Aus-
führungen seiner französischen Collegen etwas hinzuzufügen wünsche ?

Herr Faye verneint dies, will aber die Versammlung auf den wichtigen Einfluss
aufmerksam machen, den die meteorologischen Verhältnisse, besonders die barometrischen
Störungen auf die Niveauverhältnisse des Meeres ausüben; um daher den Resultaten
des Mareographen die möglichste Sicherheit zu geben, erscheine angezeigt, mit den
mareographischen auch meteorologische Beobachtungen zu verbinden, wodurch die ersteren
unmittelbar um den Barometerstand corrigirt werden könnten.

Herr Ibanez bemerkt hierzu, dass in Spanien die Mareographen stets neben meteo-
rologischen Stationen mit registrirenden Apparaten installirt sind.

Herr Oudemans erwähnt der Reductionsarbeit, welcher Herr Bouquet de la Gruye
die Brester Pegelbeobachtungen unterzogen hat und wobei der Einfluss des Barometer-
standes und der Salzgehalt berücksichtigt erscheinen. |

Herr Hirsch anerkennt zwar vollkommen die Bedeutung der von Herrn Zaye auf-
geworfenen Frage für die Oceanographie, glaubt aber, dass für die Zwecke der Geodäsie,
welche vor allem eine möglichst grosse Zahl von Beobachtungen und Vergleichungen des
mittleren Niveaus an verschiedenen Hafenplätzen verlangen, von der vorgeschlagenen
Berücksichtigung meteorologischer Verhältnisse Abstand genommen werden könne. In
der That betragen für Europa die Aenderungen der Meereshöhe, welche durch die auf
40mm zu schätzenden jährlichen Barometerschwankungen im Laufe eines Jahres hervor-
gerufen werden, an einem Orte beiläufig 0”5. Aber indem der mittlere Barometerstand
für einen und denselben Ort innerhalb der viel engeren Grenzen von 1—2™™ sich bewege,
genüge es wohl, die mareographischen Beobachtungen über einige Jahre auszudehnen,
um den Einfluss der Barometerschwankungen auf die Niveauverhältnisse auf 1°% herab-
zumindern, eine Genauigkeitsgrenze, die sich aus anderen Gründen in der Ermittlung
des mittleren Meeresniveaus überhaupt schwer erreichen lässt.

Herr Ferrero erwähnt der diesbezüglichen interessanten und verdienstvollen Ar-

beiten der Engländer in Indien, welche die Unabhängigkeit des mittleren Jahresniveaus
8

 

 
