ee à

 

ir!

JB ini

a
=
=
=
=

z
=
=
=

115

Aus dieser Zusämmenstellung geht die wichtige Thatsache hervor, dass die
Niveauunterschiede der betreffenden Meere bereits mit einem hohen Grade von Ge-
nauigkeit ermittelt worden sind und dass die Europäische Gradmessung keine vorläufige
willkürliche Nullfläche nöthig hat, um ihre Höhenmessungen in richtigen Meereshöhen
angeben zu können.

Der Präsident des Königlichen Geodätischen Instituts.
Baeyer.

B. Specialberichte.

I. Bericht des Präsidenten über die unter seiner Leitung ausgeführten Nivellementsarbeiten.

Die Untersuchungen über die Wasserstandsbeobachtungen zu Swinemünde, mit
denen ich den Assistenten Dr. Seöbt beauftragt hatte, gelangten im Frühjahr 1881 zum
Abschluss. Die Ergebnisse derselben, welche im Wesentlichen in dem Nachweise gipfeln,
dass sich die relative Höhenlage der Ostsee gegen die Küste seit mehr als einem halben
Jahrhundert nicht geändert hat, und dass ferner das Mittelwasser aus 54 jährigen
Wasserstandsbeobachtungen mit der grossen Genauigkeit von = 6.1 Millimeter festge-
stellt ist, wurden bald darauf unter dem Titel „Das Mittelwasser der Ostsee bei Swine-
münde“ veröffentlicht. —

Im Sommer 1881 erfolgten die im Generalberichte pro 1880 in Aussicht ge-
nommenen weiteren Revisionsmessungen auf der Linie Swinemünde— Konstanz; ausserdem
erledigte Dr. Se:dt in meinem Auftrage die vom Minister der öffentlichen Arbeiten ge-
wünschte Weiterführung des „Präcisions-Nivellement der Elbe“ und zwar von der Seeve-
mündung bis auf die Insel Neuhof. Die Berechnung der Beobachtungen wurde im
Winter 1881 vorgenommen und gegen Ende des Jahres erschien die letzterwähnte Arbeit
unter dem Titel „Präcisions-Nivellement der Elbe, II. Mittheilung‘“ im Drucke.

Die letzten, zur Erlangung definitiver Nivellementsresultate auf der Linie Swine-
münde— Konstanz noch erforderlichen Messungen kamen im Frühjahr 1882 zur Ausfüh-
rung und es konnte nunmehr die nach dem Generalbericht pro 1880 pag. 29 vom
Assistenten Dr. Seibt vollständig umgearbeitete Hauptnivellementslinie dem Drucke über-
geben werden; die Veröffentlichung dieses ersten Heftes der definitiven Nivelle-
mentsarbeiten des geodätischen Instituts geschah im Sommer 1882 unter
dem Titel „Gradmessungs-Nivellement zwischen Swinemünde und Konstanz.“

Nachdem dieses Werk die Presse verlassen hatte, wandte sich Dr. Sebt in
meinem Auftrage zunächst dem praktischen Theile der Umarbeitung der Hauptnivelle-
mentslinie Berlin—Denekamp zu und es war möglich, im Laufe der 1882 Campagne
sämmtliche Messungen zu absolviren, welche sich auf der genannten Linie zu deren defini-

tiven Erledigung als nothwendig erwiesen. Ausserdem wurde im Sommer 1882 noch
157

 
