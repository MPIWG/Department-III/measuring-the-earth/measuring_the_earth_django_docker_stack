129

 

 

Hinmessung. Rückmessung.
| Nagel Helmert Nagel Helmert
f T D L M L
à Raschütz bis Stein I 352 262.235 262.156 261.982 262.185
Siem > ss PE 394 264.097 263.621 262.585 262.777
5 TE: I 300 325.243 324.882 325.082 325.259
„he, x IV 394 207.833 207.622 208.073 207.893
A » V 336 238.083 238.129 237.893 238.094
s V „ Basiszwischenpunkt
Gros a A IA 362.334 362.159 361.500 361.912
;
2191 795.82 794.569 793.115 794.120
Grossenhain bis Stein VI 384 161.379 161.218 160.544 160.429
Stein = Vie = +  NIT 462 355.748 355.601 355.956 355.875
Fe Vile NT 436 323.485 323.451 324.492 324.503
a Se lela 5 5 IX 432 284.440 284.346 283.804 283.674
: s IDX = x 302 196.431 196.390 197.872 197.973
E : X :,.. Basıspunkt
F rere: 351 316.485 316.602 315.496 315.547
Ganze Länge:. 4570 703.793 704.177 703.209 704.121 \
|

Es ergiebt sich demnach die Länge der Hinmessung zu 4570 Tois. 704.985 Lin.

are » Ruickmessung ,, 4570 (| 10560 |
im Mittel also „48704 „104528,
Seh
mit dem mittleren Fehler «u = = 3.161.

sam a

Die Ausgleichung des trigonometrischen Hauptnetzes ist als vollen-
det zu betrachten und es erübrigt nur noch die Genauigkeitsberechnung, die Berech-
nung der Entfernungen, der Coordinaten und der geographischen Positionen, worauf die
Publikation dieses Netzes als II. Abtheilung der „Astronomisch-geodätischen Arbeiten
für die Europäische Gradmessung im Königreiche Sachsen“ erfolgen wird.

Nivellementsarbeiten haben seit dem letzten Bericht nicht stattgefunden.
: Nach Vollendung der Rechnungen fiir das trigonometrische Netz sollen noch einige Er-
gänzungsnivellements und dann die Gesammtausgleichung des Nivellementsnetzes aus-
geführt werden, worauf die Publikation als IV. Abtheilung der mehrerwähnten Sächsischen

Arbeiten erfolgen kann.

Na

Dresden, im Mai 1883.
A. Nagel.

Dees

17

1 [wwii

a
=
=
x
®
x

 
