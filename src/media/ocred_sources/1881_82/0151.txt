7 ee wy

CELL "7

TR AR

ml il

=
a
=
8
=
2
z
=
>
à

Normalnull. Mittelwasser der Ostsee bei Swinemünde.
Gaildorf, Glasmarke DATE 3297754
Goldshöfe HM. 470-967 471.179
Herrenberg, Glasmarke 430.154 430.366
Horb HM. 393-188 393-400
Mergentheim HM. 206-601 206.813
Mühlacker HM. 242.413 242.625
Nördlingen HM. 430.514 430.726
Nonnenhorn, Glasmarke 420.378 420.590
Osterburken HM. 249.667 249.879
Plochingen HM. 255-383 255.595
Radolfszell HM. 399.636 399.848
Stuttgart HM. 251.497 251-709
Süssen HM. 366.681 366-893
Tübingen HM. 323.851 324-063
Ulm HM. 478-638 478.850
Weikersheim, Glasmarke 235-868 236-080
Würzburg HM. 194.434 194.646

2. Die Winkelmessungen,

143

welche 1881 wegen des Schwarzwald-Nivellements ausgefallen waren, wurden von Mitte
August bis Mitte September 1882 fortgesetzt und in dieser Zeit der Punkt Bussen
bei sehr ungünstiger Witterung erledigt, so dass jetzt von württembergischen Punkten
blos noch Lichtenegg und Waldburg zu erledigen sind.

Stuttgart, 17. April 1883.
Schoder.

Il. Astronomische Arbeiten.

Im Laufe des August wurde zu gleicher Zeit mit der Winkelmessung auf Bussen
auch das Azimuth bestimmt, nachdem schon im Jahre 1878 die geographische Breite
festgestellt worden war.

Die Witterung war sehr ungünstig.

Es sind damit die astronomischen Arbeiten in Württemberg — Azimuth und
Breite von Solitude und Bussen — zu Ende geführt.

Im Laufe des Sommers wird die Berechnung definitiv festgestellt werden.

Zech.

 
