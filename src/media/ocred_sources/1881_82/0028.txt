 

20

TROISIÈME SÉANCE.

La Haye, 12 Septembre 1882.

La séance est ouverte à 2h.

Outre les membres qui ont assisté à la séance du matin, sont présents MM.
Bosscha et Buys-Ballot.

En continuant la discussion sur les instruments & lunette brisée, M. Fearnley
tient à constater qu’ayant eu l’occasion d’éprouver un assez grand nombre de prismes
de différente provenance, il a observé de grandes différences dans leurs qualités optiques,
les uns étaient irréprochables, les autres réellement médiocres. Il croit que la cause
indiquée par M. v. Oppolzer n’est pas la seule, mais que la méthode employée par les
artistes pour tailler et polir les surfaces, explique que ces dernieres, au lieu d’étre
planes, deviennent plus ou moins cylindriques. I admet du reste que l’interposition
d'une certaine épaisseur de verre dans le cône des rayons lumineux peut influencer
d’une manière fâcheuse la qualité des images.

M. Vallarceau estime que l'emploi des prismes comporte toujours une perte au
point de vue optique, mais que le défaut principal des lunettes brisées doit être cherché
autre part, savoir dans l'instabilité de la collimation qui en est la conséquence forcée et
qu'il évalue à 30 fois plus grande que dans les lunettes droites; et cela s'explique
puisque de très faibles changements dans les points d'appui du prisme se traduisent
par de fortes variations angulaires. Pour cette raison M. Villarceau n'hésite pas à
répousser l'emploi de ces instruments pour des observations de précision.

M. Oudemans répond que la variabilité de la collimation, laquelle cependant ne de-
vient sensible qu'après un certain intervalle de temps, peut toujours être éliminée par
un arrangement convenable des observations.

M. Backhuyzen croit qu'il ne faut pas s’appésantir seulement sur les défauts de
ces instruments, mais qu'on doit reconnaître aussi leurs avantages, parmi lesquels il
cite surtout la possibilité qu’ils offrent, de donner aux supports des dimensions bien
plus réduites, et de diminuer ainsi les fâcheuses iufluences exercées par la proximité
de l'observateur sur ces supports et sur la stabilité de l'instrument.

M. Perrier répond que cette dernière cause de perturbations est moins à craindre
pour les instruments à lunette droite, puisque l'observateur se trouve placé d’une façon
symétrique entre les deux supports de l’axe.

Cette discussion étant épuisée, M. le Président passe au Nr. 7? du programme
et donne la parole & M. Villarceaw qui lit une notice sur les déterminations de la pe-

Lm) | bet RRR ed à à

1181

al nu

ni!

a theme ban a hu mann à:

 
