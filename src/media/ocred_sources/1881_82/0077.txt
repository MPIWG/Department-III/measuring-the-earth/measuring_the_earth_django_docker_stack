en

(1 DR

in!

69

Baden,

Nachdem der Assistent im geodätischen Institut Dr. Secb¢ in seiner Schrift:
„Das Mittelwasser der Ostsee bei Swinemünde, Berlin 1881“ nachgewiesen hatte, dass |
die Annahme einer Senkung der Küste bei Swinemünde auf einem unmittelbaren Fehler
beruhe, gelangte er zu dem Resultat, dass das Mittelwasser der Ostsee seit 1826 eine
constante Niveaufläche des Erdsphäroids darstelle. Damit war der Wunsch der früheren
General-Conferenzen, dass die betreffenden Gradmessungs-Commissionen die Niveauver-
hältnisse ihrer Meere sorgfältig ermitteln möchten, von Seiten des geodätischen Instituts
für den Preussischen Antheil an der Ostsee vollständig gelöst, und die von Bessel ein-
geführten Höhenangaben über dem Mittelwasser der Ostsee dergestalt legitimirt, dass
die Höhen über dem Mittelwasser der Ostsee einen integrirenden Theil der Bessel’schen
Methode ausmachen. Die Deutschen Nivellements der Europäischen Gradmessung sind
daher auf das Mittelwasser der Ostsee zu beziehen.

Da die Grossherzogliche Staats-Regierung mir die Gradmessungs-Arbeiten im
Grossherzogthum übertragen hat, so habe ich dafür Sorge zu tragen, dass meine Grad-
messungs-Arbeiten in Baden nicht mit anderen Arbeiten vermischt werden, die mit der
Bessel’schen Methode nicht im Einklange stehen.

Zu dem Ende wollte ich die Festpunkte an den Stationsgebäuden mit Schildern
versehen, welche die Höhenzahl über der Ostsee tragen.

Die Generaldirection der Grossherzoglich Badischen Staatseisenbahnen hat je-
doch Bedenken getragen, die Anbringung dieser Schilder zu genehmigen, weil der. Null-
punkt der Gradmessungs-Nivellements ein anderer sei, als der der Landesaufnahme.

Die Gradmessungs-Nivellements sind aber von denen der Landesaufnahme prin-
cipiell und in so vielen Punkten unterschieden, dass eine Vereinigung der beiderseitigen
Nivellements von vorn herein als ausgeschlossen betrachtet werden muss. Ich kann
daher in eine Aenderung der Höhenzahlen meines durch das Grossherzogthum geführten
Gradmessungs-Nivellements nicht einwilligen und mache darauf aufmerksam, dass in
den Nivellements der Landesaufnahme sämmtliche Bolzen, die mit: Höhen über N. N.
bezeichnet sind, der erforderlichen Zuverlässigkeit entbehren, wie solches

1. in meiner Schrift: Ueber die Nivellements-Arbeiten im Preussischen Staate.

Berlin 1881

näher dargethan, und

2. in der Zeitschrift für Vermessungswesen, Jahrgang 1882, Heft 17, Seite 420
von dem Chef der trigonometrischen Abtheilung der Landesaufnahme selbst.
erklart worden ist.

 

 
