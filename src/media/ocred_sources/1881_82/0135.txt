MORE

RP TN |

10 RPP APD

i UM il

LE AR

127

ist allseitig gewiirdigt und vielfach hervorgehoben worden; der Unterzeichnete beklagt

überdiess durch ihn ‘den Verlust eines herzensguten, treuen Freundes, eines thatkräftigen,
rastlosen Mitarbeiters.

Durch Verordnung des Königlich Sächsischen Finanzministeriums vom 30. Juli
1881 wurde der Unterzeichnete beauftragt, die sämmtlichen auf den Sächsischen Theil
der Gradmessung Bezug habenden Papiere, Beobachtungsbücher, Rechnungsunterlagen,
Inventargegenstände etc. aus dem Nachlasse des verstorbenen Prof. Dr. Bruhns zu
übernehmen, und durch fernerweite Verordnung desselben Ministeriums vom 21. October
1881 fand der Vorschlag des Unterzeichneten Genehmigung, die Bearbeitung und Her-
ausgabe der Bruhns’schen astronomischen Gradmessungsbeobachtungen dem Herrn Prof.
Dr. Albrecht, Sections-Chef im Königlich Preussischen geodätischen Institut, zu übertragen.

Durch letzteren hat diese Bearbeitung eine wesentliche Förderung erfahren, so
dass bereits im November 1882 ein Theil der Sächsischen astronomischen Arbeiten
als 1. Heft der III. Abtheilung der „Astronomisch - geodätischen Arbeiten für die Euro-
päische Gradmessung im Königreiche Sachsen“ erscheinen konnte.

Dieses Heft enthält:

1. Die Längenbestimmung Leipzig—Freiberg, mit dem Resultat: Trigonometrische
Station Freiberg östlich vom Centrum der Sternwarte in Leipzig:

3” 44.547 + 0°046;
2. Die Längenbestimmung Leipzig—Dresden, welche ergeben hat: Pfeiler auf

der Plattform des mathematischen Salons in Dresden östlich vom Centrum
der Sternwarte in Leipzig:

52 21.951 5 0.025,
3. Die Längenbestimmung Leipzig—Grossenhain, mit dem Ergebniss: Basis-
zwischenpunkt Grossenhain östlich vom Centrum der Leipziger Sternwarte:
42 39 100 — 0,01;
4. Die Bestimmung der Polhöhe des Basiszwischenpunktes Grossenhain mit
dem Resultat:
5.0 137 2005 => 020%:
5. Die Bestimmung des Azimuths der Richtung Basiszwischenpunkt Grossenhain
—Collm mit dem gefundenen Werth:
2699 53 30393 0.16;

6. Die Bestimmung der Polhöhe der trigonometrischen Station Freiberg, welche
gefunden worden ist zu:

50° 54’ 11/50 Æ 011:

 
