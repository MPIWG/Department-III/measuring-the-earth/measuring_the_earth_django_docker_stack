PPT PEN TUT TIT ere

mt

Antrag des Herrn Hirsch auf Vergleichung

Seite

Bericht des Herrn Hirsch über die Arbeiten

Seite

 

der Resultate, welche das Reversions-Pendel in der Schweiz !.. 2. „201, 60

und der Villarceau’sche Apparat an den- Discussion über die Eichung geodätischer
selben Orten ergehen 57 Maassstäbe 2. 61

Discussion über den Einfluss des Barometer- Vorlage des Programm - Entwurfs für die

standes auf die Niveauverhältnisse der Meere 57 7. Allgemeine Conferenz der Europäischen

Bericht des Herrn van Diesen über die hollän- Gradmessung und Bezeichnung der Be-
dischen Mareopraphen .. 3. - 58 richterstatter tiber die einzelnen Punkte. . 62

Bericht des Herrn Capitaneanu über die Ar- Discussion über einige weitere Punkte des
beiten m.Rumänien ... 2 58 Programms ............ 62
Wahl des Herrn Prof. Nagel zum Mitgliede Wahl zweier Mitglieder der Pendelcommission 63

der permanenten Commission ........ 58 Wahl des Ortes für die nächste Generalver-
sammlung .......... 2... 63
Fünfte Sitzung, 15. September Nachmittags. Schluss „2.0...  . 65

Bericht des Herrn Fearnley über die Arbeiten ‚ Sechste Sitzung, 15. September Abends.
in Norwegen... ......2..2... 60 | Unterzeichnung des Protokolls. ......... 65
General-Bericht
über den Fortschritt der Arbeiten für die Europäische Gradmessung
in den Jahren 1881 und 1882.
Rapport général
sur les progrès des travaux pour la mesure des degrés en Europe
dans les années 1881 et 1882.

Seite
Baden. Bade (Baeyer) ; 69
Bayem. Baviere @. Bunny. 70
Dänemark. Danemark (Andrae) . 74
Frankreich. France (Perrier) . 75
Hessen. Hesse (Nell) . I Ses 77
Holland. Hollande (v. d. Sande Boda: yzen, Ont neh 84
Italien. Italie (Baulina, Ferrero) Er 85
Oesterreich. Autriche (v. Oppolzer, v. Kal 89
Portugal. Portugal (de Arbués-Moreira) . ; a 111
Preussen. Prusse (Baeyer, Sadebeck, Bérsch, Albr ae Bach : 113
Rumanien, Roumanie (Barozzi, Capitaneanw) 123
Russland. Russie (v. Forsch) . 125
Sachsen. Saxe (Nagel) 126
Schweiz. Suisse (Hirsch) . . . . ee 130
Spanien. Espagne (Ibanez, Bare 134
141

Württemberg. Wurttemberg (Schoder, Zech)

 
