LUN er ore =

ee Ta nn |

QUOTE 7

Tyan

Tree

=
=
x
æ
x
à
=
x

 

An den im Haag abgehaltenen Sitzungen derpermanenten Commission nahmen Theil:

L Im Namen der kgl. niederländischen Regierung:

Se. Excellenz der Minister des Aeussern: Rochussen.

IL Die Mitglieder der permanenten Commission:

Se. Excellenz Generallieutenant Ibanez aus Madrid, Präsident.
Herr Professor Dr. von Dauernfeind aus München, Vice-Präsident.
Herr Professor Dr. Hirsch aus Neuchätel, Schriftführer.

Herr Professor Dr. v. Oppolzer aus Wien, Schriftführer.

Herr Faye, Mitglied des Institut de France, aus Paris.

Herr General-Major Daulina aus Florenz.

II. Die Bevollmächtigten:

Herr Professor van de Sande Backhuyzen aus Leiden.

Herr Oberst Darraquer aus Madrid.

Herr Professor Dr. Dosscha aus Delft.

Herr Oberstlieutenant Capitaneanu aus Bukarest.

Herr van Diesen aus dem Haag.

Herr Professor Fearnley aus Christiania.

Herr Oberst Ferrero aus Florenz.

Herr Professor Oudemans aus Utrecht.

Herr Oberst Perrier, Mitglied des Institut de France, aus Paris.
Herr Professor Schols aus Delft.

Herr Villarceau, Mitglied des Institut de France, aus Paris.

 
