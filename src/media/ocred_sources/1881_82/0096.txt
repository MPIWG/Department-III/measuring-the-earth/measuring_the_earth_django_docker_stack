 

88
Sous la direction du Commandant Magnaghi a été établi un maréographe tres
perfectionné dans le port de Génes.

Travaux astronomiques.

1. Dans les deux dernières années furent déterminées les différences de longi-
tudes suivantes :

a) Milan—Paris.

b) Milan — Nice.

c) Rome—Cagliari.

d) Padoue—Florence—Rome.

2. Furent déterminées la latitude et un azimut aux stations de Milan, de
Parma et de Florence.

3. Le professeur Respighi a fait à l'observatoire du Capitole des recherches
sur l'intensité de la gravité; tandisque le professeur Lorenzoni a installé à l'observatoire
de Padoue un pendule de Repsold, de nouvelle construction, et permettant d'observer
les oscillations dans le vide.

Pour abréger ce rapport sur les travaux trigonométriques et astronomiques j'ai
fait construire le canevas à l’échelle de 1 millionième.

En outre j'ai fait construire des canevas à l'échelle de 1 à 2 millions et demi,
qui sont distribués.

Calculs et publications.
Pour tous les travaux trigonométriques et astronomiques dont il a été question,
les calculs sont en cours d'exécution et les publications ne tarderont pas à paraître.
Une partie de ces publications ont été distribuées à La Haye.

III. Conclusion.

Le cavenas qui a été mis sous les yeux de la Commission montre l’état actuel de
nos travaux. Pour la partie trigonométrique on a indiqué aussi les observations que l’on
compte faire dans les trois années 1883-84-85. Pour la partie astronomique le pro-
gramme des trois années n’est pas encore arrêté et sera l’objet de la prochaine réunion
de la Commission italienne qui aura lieu à Rome dans le mois d'octobre ou de novembre.

La Haye le 12. septembre 1882.
Le Major Général
Président de la Commission géodésique Italienne

Jean Baulina.

 

|
j
|

 
