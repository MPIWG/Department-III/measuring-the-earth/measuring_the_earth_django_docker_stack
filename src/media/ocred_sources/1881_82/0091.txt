| a wenn werner

nn |

(0) PRIT PTT

TOUT ART

erin

TTF TT

=
=
à
m
z
=
=
R

 

26.
21.
28.
22.
30.
ou,
32
DA.
34.
36.
3%
38.
32.
40.
41.
42.
43.
44,
Aly,
46.
47.
48.
49,
50.
51.
D2,
59.
54.

83

@Guntersblum
@Oppenheim .

 

@Nackenheim
@Bodenheim .
@Laubenheim

OMainz .

@Mombach

@Budenheim .
©Heidesheim .

Olungelheim
@Gau—Algesheim

[| |Warterhaus No. 134, Saal
@Bingen
@Büdesheim Denes an
@Gensingen— Horrweiler .
@Sprendlingen .
©@Gau—Bickelheim
@Armsheim

@aAlzey

@Kettenheim .
@Gundersheim .
@Nieder-Florsheim
@Monsheim

@Wôrrstadt
@Nieder-Saulheim
@Nieder-Olm
@Klein-Winternheim
@Gonsenheim

[_INierstein, Sockel am méfie. coude :

In obigem Verzeichniss bedeutet:

© einen Messingbolzen mit centrischer Bohrung, mit davor angebrachter Schutzplatte.
© einen an der bezeichneten Stelle eingemeisselten Kreis.

91.021
90.708
92.263
89.927
88.398
87.890
88.525
88.344
88.529
93.383
93.162
96.346
84.740
83.853
107.945
109.192
119.751
121.267
143.288
194.011
209.347
199.250
174.841
155.502
192.101
159.057
135.928
170.344
120.301

 

|) ein eingemeisseltes Viereck.

Die Schutzplatten vor den Messingbolzen werden in der nächsten Zeit entfernt
und durch Höhenschilder ersetzt, auf welchen die Meereshöhe der betreffenden Punkte
angegeben ist.

Dr. Nell.

11°
