a eT NE |

li ho RE

D'ART

TPIT ITT

&
=
R
=
à
mz
=
=
=
E
&

55

 

 

Nachdem der Präsident den italienischen Bevollmächtigten für ihren Bericht
sedankt hat, bittet er Herr v. Backhuyzen, den Bericht der holländischen Commission
vorzutragen.*) Im Verlaufe desselben gibt Herr v. Backhuyzen eine ausführliche Aus-
einandersetzung über den in Holland bei den Präcisionsnivellements befolgten Vorgang,
der zu einem hohen Grade von Genauigkeit geführt hat.

Herr Oudemans erläutert hierauf eine von ihm geführte Neuberechnung der von
Stamkart gemessenen Haarlemer Basis. Dieser Rechnung liegt die im Bericht über die

Triangulation von Java, Section I. auseinandergesetzte Methode zu Grunde. Lässt man die
Coeff, Eisen
Coeff. Zink — Coeff. er)

unbestimmt, so erhält der Ausdruck für die Gesammtbasis folgende Gestalt:
noL Ein -U +r' DH. +”... ı P-.oer og Sa, 9,0

Die vier Messstangen sind sowohl in Amsterdam als in Batavia mit dem Meter-
maassstabe des Repsold’schen Apparates, welcher sowohl in der Beschreibung dieses Ap-
parates in den „Astronomischen Nachrichten‘ als im Berichte des Herrn Oudemans „NOr-
malmeter“ genannt wird, verglichen worden.

Die Zusammenstellung dieser Vergleiche giebt die wahrscheinlichen Werthe von
T., Do, HL; IV., 2 9.R und 5 ausgedruckt in linearen Functionen von N, d. i. die
Länge des Normalmeters bei jener Temperatur, in welcher der Stahlmaassstab die-
selbe Länge hat, wie der Zinkmaassstab, sowie in Functionen von 7. Man erhält zum
Beispiel für Io:

 

Werthe der vier Messstangen und ihrer Ausdehnungscoefficienten (

I, = NN +4: T+tu
P= y-T+é |
worin » sich wenig von der Einheit unterscheidet. Werden diese Werthe in dem Aus-
drucke für die Basis substituirt, so findet man die Länge der Basis:
„N, tn T+ m;
diese hängt also nur von der Länge des Normalmeters und dem Coefficienten 7’ ab.
Glücklicherweise stimmen die für diesen letzteren Coefficienten zu Amsterdam
und Batavia gefundenen Werthe völlig überein, was hingegen bezüglich der Länge der
Messstangen nicht der Fall ist. Der Unterschied von 70 Mikrons, welchen die beiden
Vergleichungen des Normalmeters mit den Messstangen gezeigt haben, erklärt sich
dadurch, dass bei der Reduction eine um 7° fehlerhafte Annahme über die Temperatur
unterlaufen ist, welchen Fehler übrigens Stamkart selbst schon entdeckt hatte.
Der Präsident dankt hierauf den niederländischen Berichterstattern für ihre
anregenden Mittheilungen, die er durch Angaben über das so vollkommene System der
Mareographen in Holland morgen ergänzt zu sehen hofft, und schliesst die Sitzung um
4 Uhr 15 Minuten.

*) Siehe den Generalbericht für 1881 und 1882, Holland.

 

 
