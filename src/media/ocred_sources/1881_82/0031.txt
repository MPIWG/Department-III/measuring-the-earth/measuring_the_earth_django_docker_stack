ee

 

MT RT

TT PTT

a
=
à
à
z
à
=
=
R

23

QUATRIÈME SÉANCE.

La Haye, Mercredi 13 Septembre 1882.

Présidence de M. Zbañez.

La séance est ouverte a 1'/,®.

Sont présents les membres de la Commission permanente: MM. v. Bauernfeind,
Baulina, Faye, Hirsch, v. Oppolzer;

Les délégués: MM. Backhuyzen, Baraquer, Bosscha, Capitaneanu, v. Diesen, Fearnley,
Ferrero, Oudemans, Perrier, Villarceau;

Comme invité: M. le Capitaine Perruchon.

Les Secrétaires font lecture du Procès-verbal de la dernière séance.

M. Oudemans demande qu’on y donne quelques développements à sa communi-
cation sur le nouveau calcul de la base de Haarlem. —

M. Hirsch ne demande pas mieux que de faire droit à la réclamation de son
collègue, si M. Oudemans veut bien lui fournir sa communication par écrit.*)

Le Procès-verbal de la 3° séance est adopté. —

L'appareil de M. Villarceau étant arrivé, M. le Président donne la parole à M.
Villarceau, pour faire la démonstration de son instrument qui fonctionne devant les yeux
de l'assemblée. On montre des feuilles d'enregistrement qui donnent une idée de la
remarquable régularité de marche de l’appareil et de la facilité du relevé. Enfin pour
ceux d’entre les collègues qui n'auraient pas connaissance de la théorie de son regu-
lateur, M. Villarceau développe sur le tableau les principales équations.

M. le Président remercie M. Villarceau de cette intéressante démonstration.

M. Hirsch est très reconnaissant aux collègues français d’avoir mis sous les yeux
de la Conférence l'appareil qui, malgré quelques imperfections de construction, semble
pouvoir rendre de réels services pour les déterminations de la pesanteur relative, dont.
M. Hirsch reconnaît la grande importance pour l'étude de la figure terrestre.

Sans vouloir mettre en doute que l'appareil Villarceau réalisera réellement le
degré de précision que son inventeur en attend d’après la théorie, il serait désirable
que la preuve expérimentale fût donnée de ce qui, pour le moment, n’est encore qu’une
présomption, savoir que l'exactitude de l'instrument qui actuellement va jusqu'au 5505
pourra être portée au même degré que celle que le pendule à réversion muni de son

*) Elle est réproduite en effet dans le Procès-verbal de la 3° séance d’après la rédaction de
M. Oudemans. Les Secrétaires.

 
