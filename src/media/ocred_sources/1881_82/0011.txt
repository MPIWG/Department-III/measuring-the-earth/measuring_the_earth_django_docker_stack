Te nn

un. rer

Ont pris part à la réunion:

IL Le Représentant du Gouvernement Hollandais:
Son Excellence, Monsieur Rochussen, Ministre des Affaires étrangères.

II. Les Membres de la Commission permanente:

Son Excellence, Monsieur le General Ibanez de Madrid, President.
M. le Prof. v. Bauernfeind de Munich, Vice-Président.
M. le Prof. Hirsch de Neuchätel | Sn
M. le Prof. v. Oppolzer de Vienne ©
M. Faye, Membre de l’Institut, de Paris.

M

. le Général Baulina de Florence.

Ill. Délégués:
. le Prof. v. d. Sande Backhuyzen de Leide.
. le Colonel Baraquer de Madrid.
le Prof. Bosscha de Delft.
le Lieutenant-Colonel Capitaneanu de Bucharest.
van Diesen de La Haye.
le Prof. Fearnley de Christiania.
le Colonel Ferrero de Florence.
le Prof. Oudemans d’Utrecht.
e Colonel Perrier, Membre de l'Institut, de Paris.
le Prof. Schols de Delft.
. Villarceau, Membre de l’Institut, de Paris.

ee

IV. Invités:

. Caland, Inspecteur en chef du Waterstaat, de La Haye.

. Hubrecht, Secrétaire général au Ministere de l’Interieur, de La Haye.
. le Général van der Star, Chef de l’Etat-mayor général, de La Haye.
. l'Ingénieur Vervey de La Haye.

ss

 
