 

 

 

112

Bases.

Apres avoir terminé notre triangulation, il nous restera encore la tache impor-
tante de fixer la longueur de nos côtés par des mesures de bases. L’ancienne base
Batel—Montijo nest pas acceptable dans les conditions actuelles de la science, et
son remesurage offre quelques difficultés à cause du terrain. Il était donc nécessaire de

_ choisir les localités pour effectuer de nouvelles mesures. Dans ce but on a déjà choisi

une plaine sur la rive gauche du Minho (près de la Galice), où on pourra mesurer
une petite base avec des raccordements aboutissant aux côtés de premier ordre 5. Paio
—Peneda et Peneda—Galineiro. Une autre base sera mesurée dans les plaines de
YAlemtejo. Mais pour réaliser ces importants travaux, il nous faut la comparaison de
nos règles avec l’etalon fondamental ou avec des étalons déjà comparés. Nous traiterons
prochainement des moyens de résoudre cette question.

Maréographes.
Celui qui a été établi dans Cascäes a continue de fonctioner tres regulierement.
Une station météorologique lui est adjointe.

Nivellements géométriques de précision.
Ce service a été organisé. Par Vintervention de notre honorable collègue
M.. Hirsch on a acquis de M. Kern, d’Aarau, 4 mires completes comparées avec l’étalon
de Berne. Des repères en bronze ont été fondus. Les travaux ont commencé à partir
du maréographe de Cascäes et continuent suivant les lignes projetées, qui aboutiront à
la frontière espagnole pour se relier aux lignes mesurées dans ce pays. L'erreur kilo-
métrique ne dépasse pas 3.5 millimètres, jusqu'à présent.

Stations astronomiques. — Intensité de la pesanteur.

Les travaux astronomiques pour la détermination des latitudes et des azimuts
sont en retard, faute de personnel, ce dernier ayant été employé à d’autres travaux;
mais ils seront repris bientôt avec vigueur, les règles pratiques pour leur exécution
étant déjà établies.

Les observations de l'intensité de la pesanteur avec un pendule à réversion de
Repsold dépendent encore d’études préliminaires de l'instrument, et elles seront entre-
prises, lorsque ces études nous donneront les garanties nécessaires pour espérer des

résultats définitifs.

Carlos Ernesto de Arbues-Moreira.

 

a
i
3
i

sat ss

ju ame al hi a dh eid ll adie phan

|
i
|
|

 
