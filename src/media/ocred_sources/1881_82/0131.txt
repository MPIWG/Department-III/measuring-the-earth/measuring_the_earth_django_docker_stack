ee ee

 

a

PL RTE

a
=
=
x
®
=
>
=
x

123

und Herr Dr. Westphal praktisch arbeiteten, so wurde doch die Ausgleichung des
ganzen Netzes nach der Rückkehr nach Berlin noch einmal aufgenommen und noch
einmal vollständig durchgeführt unter Elimination der durch die fehlerhafte Kreistheilung
bewirkten Standverschiedenheit bei den Stationsbeobachtungen. Es mussten zu diesem
Zwecke alle Stationen von Neuem ausgeglichen und die Rechnungen für die Netzaus-
gleichung wiederholt werden; in Folge dessen verzögerte sich die Herausgabe des II.
Heftes des rheinischen Dreiecksnetzes bis zum April 1882. Die Bearbeitung des trigo-
nometrischen Nivellements Helgoland—Neuwerk aus den Beobachtungen der Jahre 1878
und 1881 konnte daher erst nach den Sommerarbeiten 1882 in Angriff genommen werden
und sind die Rechnungen soweit gediehen, dass die Publikation derselben noch im
Laufe des Jahres 1883 erfolgen wird. :

Fischer.

Rumänien Roumanie.

Rapport présenté à la Commission internationale de la mesure des Degrés, sur
les travaux du Dépôt général de la guerre de Roumanie en 1882.

Le Dépôt général de la guerre de Roumanie continuant à être exclusivement
occupé avec la construction de la carte générale de la Dobroudja, ainsi qu'avec la
délimitation ou mieux dire avec la détermination et la séparation des terrains dus aux
habitants de cette province de ceux qui doivent appartenir à l'Etat, la Roumanie n’a
à inscrire durant cette année aucun travail géodésique relatif à la mesure des degrés.

Le seul personnel qui a pu être soustrait au travail actuel de notre Dépôt de
guerre, à été le personnel astronomique qui a effectué pendant cet été une détermination
de latitude et d’azimut à Constance, ainsi qu’une determination de la différence de
longitude entre Constance et Bucareste par le transport de chronomètres, laquelle sera
en outre déterminée au moyen d'échange de signaux électriques, dès qu'il nous
sera possible. :

De méme une détermination de la différence de longitude entre Bucareste et
Jassy est projetée et sera effectuée au plus tard au printemps prochain.

La publication des travaux astronomiques relatifs à la determination de la
différence de longitude entre Jassy et Cernowitz a été achevée.

Le fait le plus important pour nous durant ce dernier temps a été l’augmen-
tation de notre matériel tant astronomique que géodésique; pour ce qui regarde surtout
le premier, je tiens à porter & la connaissance de la Commission que nous. possédons
aujourd’hui tout ce qui est nécessaire pour monter deux observatoires astronomiques

portatifs.
16*

 

 
