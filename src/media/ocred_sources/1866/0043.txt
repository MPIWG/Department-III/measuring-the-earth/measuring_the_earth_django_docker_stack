er

N NT TV |

ae

Bis Ende des Jahres 1866 sind sächsischerseits 13 Pfeiler des Hauptnetzes und ausser
3 Basispfeilern noch 42 Pfeiler des Netzes der zweiten Ordnung ausgeführt worden.

Die beabsichtigten Winkelmessungen liessen sich in Folge des Krieges nicht zur Aus-
führung bringen, dagegen konnten die im Herbst des Jahres 1865 begonnenen nivellitischen
Arbeiten im Jahre 1866 fast ungestört ihren Fortgang haben. Die Ausführung dieser Arbei-
ten ist im Wesentlichen dieselbe geblieben, wie schon im Generalbericht vom Jahr 1865 referirt
worden ist.

Von den projectirten 46 Hauptnivellirungslinien des Königreichs Sachsen sind mit
Ende des Jahres 1866 14 Linien vollständig gemessen worden, so dass noch 32 Linien zu
nivelliren übrig bleiben, wovon voraussichtlich in diesem Jahre noch 12 Linien absolvirt wer-
den, so dass mit dem Ende dieses Jahres nur noch 20 Linien, also nicht mehr die Hälfte des
ganzen auf der Karte V des vorjährigen Generalberichtes angegebenen Nivellirungsnetzes zu
nivelliren übrig bleibt.

Die bereits zur Erledigung gekommenen Nivellirungslinien sind:

1. Dresden-Freiberg.
2. Dresden-Altenberg.
3. Fréiberg-Altenberg.
4. Dresden-Döbeln.
5. Döbeln-Freiberg.
6. Chemnitz-Freiberg.
14. Chemnitz-Döbeln.
8. Freiberg-Sayda.
9. Sayda-Altenberg.
10. Freiberg-W olkenstein.
11. Wolkenstein-Sayda.
12. Wolkenstein-Chemnitz.
13. Wolkenstein-Schwarzenberg und
14. Chemuitz-Schwarzenberg. ;

Die Genauigkeit der Ergebnisse der Nivellements von diesem kleinen, vorläufig nur
aus 14 Linien bestehenden Nivellirungsnetze ist, wie zu erwarten stand, ziemlich günstig
ausgefallen.

Hiernach liegt z. B. Altenberg über dem durch einen starken Messingbolzen am Bahn-
hofsgebäude der Böhmischen Eisenbahn zu Dresden vorläufig fixirten Nullpunkt:

a) direct gemessen = 633,6325 Meter,

b) über Freiberg gemessen 297,5205 -+ 336,0655 = 633,5860 Meter, wonach die
Höhendifferenz 0,0465 Meter beträgt. Ferner liegt Döbeln über Dresden:

a) direct gemessen = 61,3700 Meter,

b) über Freiberg gemessen 297,5205 — 236,1560 = 61,3645 Meter, so dass die Höhen-
differenz nur 0,0055 Meter ausfällt. Ferner Chemnitz liegt höher als Dresden:

6*

 
