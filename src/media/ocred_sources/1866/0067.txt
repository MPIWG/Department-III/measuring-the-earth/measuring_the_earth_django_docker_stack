er

10 PIE YT SETS |

1 mi

x
=
=
=
m
à
=

a

rung, die ich durchweg benutzte, noch ein tadelloses Bild. Das Fadennetz besteht aus
15 Fäden, in 5 Gruppen vertheilt, von beziehungsweise eirca 6 und 9 Abstand. Die Axe
hat 17,5 Zoll Länge; die Libelle (1 Theil = 1"032) bleibt beim Umlegen an der Axe hängen.
Die Aufstellung auf gusseisernem Gestelle ist sehr solide und das Instrument lässt in Bezug
auf seine Leistungen nichts zu wünschen übrig.

Zur Zeitmessung diente eine gute Pendeluhr von Dorer mit Quecksilbercompensation ;

ausserdem standen zwei Chronometer (von Dent und Vorauer) zur Verfügung.

Beobachtungs- und Reductionsmethoden.

Bei den Beobachtungen im ersten Vertical wurde das Instrument zwischen dem öst-
lichen und westlichen Durchgange des beobachteten Sternes jedesmal umgelegt. Nur bei den
Sternen 7 Ursae maj. (1863 und 1864) und « Urs. maj. (1865), so wie 46 und 6 Cygni, deren
kleine Meridianzenithdistanz dieses Verfahren gestattete, fand die Umlegung bei jedem der
beiden Durchgänge statt, nachdem der Stern die halbe Anzahl der Faden passirt hatte. Die
Reduction der Beobachtungen habe ich in der Weise ausgeführt, dass aus jedem einzelnen
in beiden Kreislagen beobachteten Faden ein von den Instrumentalfehlern unabhängiges Re-
sultat der Polhöhe abgeleitet wurde. Einem so erhaltenen Resultate wurde das Gewicht 1
beigelegt; die Mittel der aus jedem Sterne an den einzelnen Beobachtungstagen folgenden
Resultate sind weiter unten angesetzt, und die beigefügten Gewichte bezeichnen zugleich die
Anzahl der in jeder Kreislage beobachteten correspondirenden Fäden.

Nebst den im Folgenden angeführten Fundamentalsternen wurden im ersten Vertical
auch noch einige andere dem Zenith nahe kommende Sterne (9, ö, e, 0°, 32, w', w*, und
f! Cygni) beobachtet, deren Resultate ich bei einer anderen Gelegenheit mittheilen werde,
da sie vorläufig mit den aus den Fundamentalsternen gezogenen doch nicht verbunden wer-
den können. Ich habe übrigens nicht Anstand genommen, Fundamentalsterne bis zu 6° Me-
ridianzenithdistanz zu beobachten, und mit vollkommenem Erfolge, indem der für solche Sterne
sich ergebende mittlere Fehler einer Beobachtung nicht merklich grösser ist, als für andere
dem Zenith näher kommende Sterne.

Bei den zum Behufe der Polhöhenbestimmung gemessenen Circummeridianzenith-
distanzen habe ich die Sterne so gewählt, dass in nahe gleichen Zenithdistanzen südlich und
nördlich vom Zenith nahezu die gleiche Anzahl von Beobachtungen erhalten werden konnte,
und den Zenithpunkt des Kreises zwischen den einzelnen Beobachtungssätzen regelmässig
geändert. Bei « Urs. min. wurden in jeder Kreislage jedesmal 5, bei den übrigen Sternen
5—10 Hinstellungen gemacht.

Behufs der Reduction wurden an jede einzelne Lesung die Correction der Libelle,
die’ Refraction und die Reduction auf den Meridian angebracht und auf diese Weise die
Meridianzenithdistanz am Kreise erhalten. Durch Verbindung je zweier in entgegengesetzten
Kreislagen erhaltenen, von der Mitte gleich weit abstehenden Werthe ergab sich die wahre

Meridianzenithdistanz des Sternes und mit Zuziehung der Declination ein Resultat der Pol-

9 #

 
