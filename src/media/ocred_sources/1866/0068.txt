 

ae

höhe. Jedes so erhaltene Resultat, welchem das Gewicht 1 beigelegt wurde, beruht demnach
auf je einer in beiden Kreislagen gemachten Einstellung. Nur die Mittel der aus jedem
einzelnen Sterne folgenden Resultate können weiter unten angeführt werden.

Die Refraction habe ich Warnstorff’s Hülfstafeln entnommen, die Reduction auf den
Meridian bei allen Normen nach der strengen Formel

sin4Az = EA sin 4t”
berechnet.

Bei den Azimuthbeobachtungen in den Jahren 1864 und 1865 habe ich zwei Einstel-
lungen auf das Signal und den Polarstern in der einen Kreislage, sodann in umgekehrter
Ordnung ebenso viele Einstellungen in der anderen Kreislage gemacht; nur im Jahre 1863
hielt ich es wegen der geringeren Stabilität des angewendeten Instrumentes für nöthig, in
jeder Kreislage vor und nach der Beobachtung des Sternes zwei Einstellungen auf das Signal
zu machen. Aus einem solchen Satze wurde auf bekannte Weise ein Werth des Azimuthes
abgeleitet, und der Ort des Meridians am Kreise von Satz zu Satz regelmässig bis zur Er-
schöpfung der Peripherie geändert.

Die Zeitbestimmungen beruhen durchaus (mit Ausnahme der ersten Beobachtungstage
1863, wo ich ein kleines im Meridiane aufgestelltes Passageinstrument von 15 Zoll Brennweite
hierzu benutzte) auf beobachteten Azimuthaldifferenzen des Polaris und eines Fundamental-
sternes. Ich wählte diese Methode, weil sie, bei genügender Genauigkeit, den Beobachter am
wenigsten an eine bestimmte Zeit bindet, ein Vortheil, der bei ungünstiger Witterung nicht
zu unterschätzen ist. Drei Antritte des Zeitsternes und zwei Einstellungen des Polaris in
jeder der beiden Kreislagen zeigten sich vollkommen genügend, um mit dem Starke’schen
Universalinstrumente die Uhrcorrection bis auf 0,15 genau zu erhalten.

Die Sternpositionen sind dem Berliner Jahrbuche entnommen, und an den zur Be-
stimmung der Polhöhe dienenden Declinationen die Auwers’schen Correctionen (Astr. N.
No, 1549 u. s. f.), an jener von & Can. maj. auch der periodische Theil der Eigenbewegung
angebracht. Durch Hinzufügung dieser Correctionen wird die Uebereinstimmung der einzelnen
Sterne im Allgemeinen erhöht; bei einigen Sternen jedoch wird durch die Beobachtungen die
Wahrscheinlichkeit einer ferneren nicht unbeträchtlichen Correctur mit ziemlicher Bestimmt-
heit angedeutet. Der mittlere Ort von s Urs. maj. ist dem Wolfers’schen Verzeichniss (Berl.
J. 1867) entlehnt und die jährliche Eigenbewegung in Declination = —0"28 angenommen.

Um übrigens keinen Zweifel zu lassen, füge ich im Folgenden den Resultaten die
angenommene mittlere Declination des Sternes für den Anfang des Jahres nebst der nach

Dr. Auwers angebrachten Correction, letztere schon für die Epoche der Beobachtung gel-
tend, bei.

wer ds ect hr a

cy bombe de bahn

 
