Station Rappotitz.

Verzeichniss jener Sterne, welche zu den astronomischen Beobachtungen auf
Rappotitz benutzt worden sind.

Zur Zeitbestimmung. i

 

 

Datum. Name der Sterne. Datum. Name der Sterne.
ee sun un. nm we ae an nu nn u un.
17. Maı a Ursae majoris 4, Juni ß Librae
- 7-Cépher U 0. Die 8 Ursae minoris
- y Ursae majoris - 5 [ibrae
- “4 - Napeinis = ö Ophiuchi
22e y Ursae majoris Os & Ursae minoris
- y Virginis - & Virginis
: - & Ursae minoris - n Ursae majoris
| 5 5 Viregimis 2 n Bootis
E = n Ursae majoris 107. = ß Ursae minoris
5 23 & Ursae minoris 5 8 Librae
; a Ophiuchi - ö Ophiuchi
- u Herculis - u Herculis
- y Draconis : y Draconis
; 28, - a Ursae majoris - Ô Ursae minoris
i Y - . © Aquilae
- n Virginis - 0 -
- & Ursae minoris 12,7 a Ursae minoris
- a Bootis - a Virginis
ol - é - 2 a -
- 6 Librae - m Ursae majoris
: - a Coronae borealis 18 - a Ursae minoris
: - a Serpentis - a Cassiopeae
- ¢ Ursae minoris 2 6 Arietis
= 1. Jun. y" Virginis . œ -
= - a Ursae minoris 20. = a Ursae minoris
: - «  Bootis = œ  Virginis
1 2e y' Virginis - n Ursae majoris
- 12 Canum ven. i n Bootis
- & Ursae minoris 21s a Ursae minoris
2 Ô = - - a Virginis
- & Aquilae - n Ursae majoris
= w - - n Bootis
: 0 : 22 oe Ursae minoris
- y - - a Virginis
- œ - - n Ursae majoris
4, - B Ursae minoris = n Bootis
- y Bootis

 

 
