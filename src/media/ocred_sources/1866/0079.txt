FT IRATE TY |

ind ham PR

a

Königsberg 1865, April 21—24.

No. 5. AR,—AT, = — 05,16 Gewicht 3
No. 6. AR,—AT, = — 05,19 - 8
No. 1. AR,—AT, = — 0,08 - 4
No. 8. AR, HOT, = —O 10 UE

Die No. 1, 3, 5, 7 enthalten die Resultate von solchen Durchgangsbeobachtungen
beider Astronomen, bei welchen jeder Stern von einem und demselben Beobachter an allen
Fäden genommen und bei denen die Oerter der Sterne durch den Wechsel der Reihenfolge
der Beobachter an zwei aufeinanderfolgenden Abenden eliminirt wurden. Die Gewichtseinheit
enthält die Durchgänge von zwei Sternen mindestens an je 10 Fäden.

Die No. 2, 4, 6, 8 enthalten solche Reihen, in denen bei demselben Sterndurchgang
jeder Beobachter die Hälfte der Fäden nahm. Die Gewichtseinheit enthält dann etwa 3 sol-
cher Durchgänge.

Bildet man aus den obigen Zahlen den Unterschied der relativen persönlichen Glei-
chungen beider Beobachter in entgegengesetzten Lagen der Achse des Fernrohrs und zwar:
(AR, — AT,)— (AR, — AT,) = AG,

so findet man mit Rücksicht auf die Gewichte:
April, 6-10. AG = +0°,20
April 21 — 24. AG = — 05,02.

Zwischen beiden Bestimmungen lagen die zahlreichen Durchgangs-Beobachtungen der
eigentlichen Längen-Bestimmung, und es hat nach den obigen Zahlenwerthen von AG deutlich
den Anschein, als ob der Anfangs sehr beträchtliche Einfluss der Bewegungs-Richtung auf
die persönliche Gleichung im Verlauf der Beobachtungen geringer geworden sei.

Beide Beobachter waren nämlich zu Anfang der Operation trotz ihrer bewährten Uebung
in anderen Durchgangs-Beobachtungen in den Registrir-Beobachtungen am gebrochenen Fern-
rohr (also mit verschiedenen Bewegungs-Richtungen der Sterne) als Neulinge zu betrachten,
so dass die Veränderung der Gewohnheit in verhältnissmässig kurzer Zeit nicht gerade un-
wahrscheinlich ist. Noch deutlicher zeigen sich die Spuren der Abnahme des Unterschiedes
AG mit der Zeit, wenn man die erste Gruppe der Bestimmungen theilt. Man findet:

April 6— 8. AG = + 08,37
fy osc) AG Song
- 21—24. AG = —0,02.

Unzweifelhaft ist diese Veränderung ein sehr bedenklicher Punkt bei der Ableitung der Längen-
differenz; denn welches Gesetz soll man für die Veränderung von AG in der Zeit zwischen
April 10—20. annehmen. — Glücklicherweise geben die Beobachtungen der Längendifferenz
selbst noch ein Hülfsmittel zur Bestimmung von Grenzwerthen von AG und beweisen, dass
die schnelle Abnahme von AG, welche in den ersten 3 Tagen, April 7—10., stattgefunden
hat, solchen Fortgang gehabt haben muss, dass man schon vom 2. Tage der Längenbestimmung

 
