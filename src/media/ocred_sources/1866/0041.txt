du APE APN TPT TT NE |

IM N

[LI

ji lu

a
=
:
x
x
æ
=
~

 

se A

die mit der Bessel’schen Toise stattgefunden haben, wobei B = 864 Linien der Toise von Peru
ist, von denen die Bessel’sche Toise 863,9992 Linien zählt.

 

 

 

Bezeichnung der Stäbe, Jahr der Anfertigung Beobachter, welche
Nr. und Namen der Künstler, welche sie angefertigt T—B die Vergleichung
haben. ausgeführt haben.
1. | Bessel’sche Toise von Fortin 1823 — 0,00080
2. | Schumacher’sche T. v. Fortin 1821 + 0,00253 15
3. | dito v. Gambey 1831 - | 000491 Bel
4. | Copie Nr. 10 d. Bessel’schen Toise von
Baumann 1852 — 0,00099 1852
5. | Belgische Copie Nr. 11 von Baumann Baeyer.
1802 _ —0,00100
1853
6. | Russische Toise v. Fortin 1821 —0,00013 |. W. Struve.
7. | Italienische Angabe der Toise von Peru, P | —0,05380 1865
8. | dito von Ertel - — 0,06107 Schiavoni.
1865
9. | dito von Spano — 0,03812 Baeyer.

Anmerk. 1. Nr. 4 und 5 sind lediglich Copien der Bessel’schen Toise. Sie sagen nichts weiter aus,
als dass sie bis auf die angegebenen Grössen mit der Bessel’schen Toise tibereinstimmten; über eine Veränderung
der Länge der Bessel’schen Toise seit ihrer Anfertigung können sie keinen Aufschluss geben.

2. Der kleine Unterschied von Nr. 6 scheint auf einem glücklichen Zufall zu beruhen, dem man es

verdankt, dass die Preussische und die Russische Toise als identisch angesehen werden können.

3. Nr. 2, 3, 7, 8, und 9 zeigen dagegen Differenzen, die so weit über die Grenzen der Beobachtungs-

fehler hinausgehen, dass sie kaum anders, ‘als durch eine Längenänderung der Stäbe erklärt werden können, und
da von fünfen nur eine positiv ist, so scheint dies auf eine Verkürzung der Stäbe im Laufe der Zeit hinzudeuten.

Es leidet wohl kaum einen Zweifel, dass die verschiedenen Meterstäbe ganz ähnliche
Differenzen zeigen werden, und dass man genöthigt sein wird Messungen, die mit Metern
von verschiedener Länge ausgeführt wurden, auf das eine oder andere reduciren zu müssen,
wenn man sie auf ein und dieselbe Masseinheit bringen will. Daher ist mit der allgemeinen
Annahme des Metermaasseg nur dann ein Vortheil für wissenschaftliche Messungen gewonnen,
wenn dafür gesorgt wird, dass durch zeitweise Regulirungen die Meter in den verschiedenen

Ländern in sicherer Verbindung mit der ursprünglichen Einheit erhalten werden.

i# Russland

Von dem Kaiserlichen Bevollmächtigten, Herrn Generallieutenant von Blaramberg

Excellenz, ist dem Centralbüreau der XXVII. Band der Memoiren des Kriegskarten-Depots

General - Bericht f. 1866. 6
