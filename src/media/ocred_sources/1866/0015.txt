Tine en een en ee

Te ETT! |

D 1 pi

CL RIZ LITE RIT]

4

a

Minister um diese Ermächtigung gebeten, und hoffentlich wird sie noch zeitig genug gegeben
werden, um die beabsichtigte Längenbestimmung zu ermöglichen.

Im General-Berichte für das Jahr 1865 gab das Central-Bureau em Verzeichniss von
201, für Polhöhen-Bestimmungen gewählten, Sternen, und äusserte es die Absicht, sich
über die Beobachtung dieser Sterne mit den Sternwarten zu Leiden und Leipzig in Verbindung
zu setzen, Ich bin nachher mit dem Central-Bureau übereingekommen, dass die Leidner Stern-
warte die genaue Bestimmung der Declinationen dieser Sterne zu ihrer Hauptaufgabe stellen
wird, sobald die dortigen Fundamental-Bestimmungen, welche ihrem Ende nahe sind, geschlos-
sen sein werden. Unter den 180 Fixsternen, welche, in den letzten Jahren, an der Leidner
Sternwarte als Fundamental-Sterne beobachtet wurden, kommen schon 83 des obengenannten
Verzeichnisses vor und die Bestimmung der Declinationen der 118 übrigen Fixsternen wird
wahrscheinlich keinen Zeitraum von drei Jahren erfordern.

Da er vielleicht das systematische Umwechseln der Beobachter bei Längen-Bestim-
mungen überflüssig machen kann, glaube ich hier meinen Apparat zur absoluten Bestimmung
von persönlichen Fehlern erwähnen zu dürfen. Schon im Jahre 1851 habe ich diesen Apparat
öffentlich erwähnt; schon im Jahre 1859 hat auch der verstorbene Astronom M. Gussew damit,
in Leiden, Beobachtungen angestellt, und im Jahre 1862 habe ich eine öffentliche Beschrei-
bung und Abbildung dieses Apparates gegeben, aber er hat durchaus keine Beachtung finden
mögen. Bei diesem Apparat lassen sich die verschiedenen Phänomene, welche in der Astro-
nomie beobachtet werden, und besonders Durchgänge von Fixsternen, wie diese sich in Fernröh-
ren verschiedener Grösse und Gattung zeigen, sehr getreu nachahmen. Der Apparat lässt den
wahren Augenblick des Phänomens, auf 0°,01 genau, bestimmen und der absolute Fehler einer
Beobachtung ergiebt sich aus deren Vergleichung mit diesem Augenblicke. Anfangs bestimmte
ich den wahren Augenblick des Phänomens durch Coineidenz von Uhrschlägen, aber jetzt
wende ich dazu den Registrir-Apparat an und, durch eine ganz einfache Einrichtung, lässt sich
dabei die Summe der Fehler des letztgenannten Apparates, auf 0°%,01 genau, erkennen.
Mein Apparat ist einfacher und transportabler als der Apparat des Herrn C. Wolf in Paris,
dessen Beschreibung und Abbildung vorkommt in den Annales de lobservatoire impérial de

Paris. Mémoires. Tome VIII. Paris 1866.

Leiden, 27. Februar 1867.
F. Kaiser.

 
