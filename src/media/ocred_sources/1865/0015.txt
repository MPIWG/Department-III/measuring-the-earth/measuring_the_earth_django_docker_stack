rn SEE dre lie

(ho RE PTT PT TP ee |

a

1. Beide Observatorien waren durch eine Drahtleitung mit einander verbunden, die
ohne alle Beschränkung den Beobachtern zur Disposition stand; daher ward es möglich
264 Zeitsterne in derselben Zeit von 8 Nächten correspondirend zu beobachten und zu re
gistriren, während welcher bei der Altona-Schweriner Operation, weil bei dieser nur eine
beschränkte Benutzung der Leitung stattfand, nicht mehr als 149 correspondirende Beob-
achtungen registrirt werden konnten.

2. Die Beobachtungen wurden ın 8 Nächten, während der Zeit vom 28. August bis
12. September ausgeführt. An Sternen wurden jede Nacht correspondirend auf beiden Sta
tionen beobachtet: |

a) die Polarsterne 0 und @ Urs. min.,
b) kurz vor und nach jedem Polarsterne 5 bis 6 Fundamentalsterne,
c) 20 bis 26 Zeitsterne in 4 Gruppen bei abwechselnder Lage der Horizontalachse.

Die unter b und c aufgeführten Sterne wurden ohne Ausnahme auf beiden Sta-
tionen registrirt.

Die Auswahl der Sterne unter c ist so getroffen, dass in jeder der 4 Gruppen zwar
1 bis 2, in der Nähe des Zeniths eulminirende Sterne vorhanden sind, die überwiegende
Mehrzahl dieser Sterne aber in einer Höhe culminirt, die im Durchschnitt sehr nahe den
Höhen der beiden Polarsterne zur Zeit ihrer oberen Culmination gleich ist.

3. Da die Länge der Leitung nur etwas über eine Viertelmeile betrug, so konnten
die Signale der Beobachter von beiden Stationen aus jederzeit ohne Relais gegeben werden,
während die Registrirung der Secundenschläge der Pendeluhr immer durch ein Relais ver-
mittelt wurde, in dessen Kette überdies zur Verminderung der Funkenbildung am Strom-
unterbrecher in der Uhr ein Condensator von Tiede in Berlin eingeschaltet war. Die Gleich-
heit der Kräfte, mit welcher die Anker die Schreibstifte der Beobachter anzogen, wurde
nach dem Gehör beurtheilt und nöthigenfalls hergestellt, ein Verfahren, welches in dem Falle,
wenn, wie bei dem hiesigen Registrir- Apparat, die Drahtrollen der Electromagnete sehr un-
gleiche Leitungswiderstände haben, der Untersuchung der Anziehungskräfte durch ein Gal-
vanoskop bei Weitem vorzuziehen sein dürfte.

Die Berechnung der Längenbestimmung ist dem grösseren Theile nach vollendet,
und wenn dieselbe auch jetzt noch nicht bis zu den Resultaten fortgeführt ist, so hat die-
selbe doch auf der andern Seite auch keine Veranlassung gegeben, an dem guten Gelingen

der Operation irgendwie zu zweifeln.

II. Die Bestimmung der Polhöhe

des Observatoriums auf der Central-Telegraphenstation ist ganz so, wie die Polhöhe des
Observatoriums der Landesvermessung, mit denselben Instrumenten, nach denselben Methoden
und mit Benutzung derselben Sterne bestimmt, so dass wegen der Beschreibung der Opera-

tion wohl die oben citirte Veröffentlichung in den Astronomischen Nachrichten einfach in

Bezug genommen werden darf.

 
