en

TERS Doe See oe rey

"1 en AR ANTENNES PTT

I

in

Denis pre ln

|

=
x
æ
=
~

 

Lo

tung erhält man z. B., in Theilen der respectiven Seiten ausgedrückt, die folgenden mittleren
Fehler für:

Die Grundlinie AB . a ea
Die Dreiecksseite Nicolai-Snoldelev . = 1: 214500.
u L 5 Nieolar-Malmo »., = 1.191908.
Malmé-Falsterbo »= 11090500.

” ” ”

5 Darserort-Hiddensé = 1: 133200.
Nach der Küstenvermessung pag. 313 hat man für die Dreiecksseite Darserort- Hiddensö:

Log. Darserort-Hiddensé = 4,3301455.

Mit dem oben angeführten Werthe verglichen giebt dies eine Differenz von 72 Ein-

heiten der siebenten Decimale, oder ın Theilen der Seite ausgedrückt:

I

Diese Uebereinstimmung muss als eine vollkommen befriedigende angesehen werden.
Die dänischen und die preussischen Messungen haben nämlich durchgängig dieselbe Genauig-
keit, und die erwähnte Dreiecksseite wird mit der Kopenhagener Grundlinie durch zwölf zwi-
schenliegende Dreiecke, mit der Berliner Grundlinie aber durch 25 verbunden. Bezeichnet
man daher mit u den für diese Seite aus der Kopenhagener Grundlinie abgeleiteten mittleren

Fehler, dann wird man gewiss nicht sehr irren, wenn der entsprechende Fehler für die Ablei-

u 25 oe

tung aus der Berliner Grundlinie zu u 13 angesetzt wird. Für die Differenz der beiden Be-

| ‚37 El.
stimmungen erhält man hiernach den mittleren Fehler: u V5 oder mit Einführung des oben

a WV ae à
gegebenen Werthes von 4“

ae
75860

5. Rrankreiech

Nach einer Mittheilung der Direktoren der Sternwarten in Leipzig und Wien der
Herren Dr. Bruhns und Dr. v. Littrow, haben die früher in Aussicht genommenen Längen-
bestimmungen zwischen Leipzig und Paris und zwischen Wien und Paris bis jetzt noch nicht
stattgefunden. Seit dem Jahr 1864 sind überhaupt über die Kaiserlich französische Coope-
ration keinerlei Mittheilungen mehr hierher gelangt. Da dem Centralbüreau die Hindernisse
unbekannt sind, durch welche die gegenseitigen Communicationen unterbrochen wurden, so
bedauert dasselbe im wissenschaftlichen Interesse die dadurch eingetretene Stockung in den
wechselseitigen Beziehungen und Verbindungen der gleichartigen Operationen, und glaubt
dem Wunsche nach Austausch der beiderseitigen Thätigkeit zu gegenseitiger Ergänzung und
Unterstützung bei dem grossen Unternehmen, am besten dadurch Ausdruck zu geben, dass
es seine Mittheilungen ungestört fortsetzt, indem es sich gern der Hofinung hingiebt, bald
die Schwierigkeiten beseitigt und die Gegenseitigkeit der Mittheilung wieder hergestellt zu sehen.

2

General-Bericht f. 1865.
