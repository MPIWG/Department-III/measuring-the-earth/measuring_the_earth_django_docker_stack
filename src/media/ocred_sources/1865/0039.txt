PRT PTT PT YT STP HET |

RCI

I

jw ii

Dei Di

z
x
=

2. Bericht über die Gradmessungsarbeiten in Norwegen vom Direktor der Sternwarte
in Christiania Herrn Dr. Fearnley.

Indem ich über den jetzigen Stand der in Norwegen für die mitteleuropäische
Gradmessung auszuführenden Arbeiten zu berichten habe, erlaube ich mir meinen bei der
Conferenz im October gegeben mündlichen Bericht theilweise zu resumiren.

Aeltere Messungen. Bei Christiania war 1835 eine 3103,567 Metres lange Grund-
linie auf dem Eise 3mal mit Holzstangen gemessen worden. Darauf gestützt wurde eine
Dreieckskette nach Drontheim und eine nach Kongsvinger geführt, welche von dem später
als Chef der Ingenieurbrigade verstorbenen General Broch in den Jahren 1835—36 gemessen
wurden. Mit diesen Dreiecksketten in Verbindung ist von dem jetzigen Chef des topogra-
phischen Bureaus Oberstlieutenant Naser 1852—53 eine Triangulirung nach Bergen geführt.
Die Signale waren bei diesen Messungen meistens steinerne Pfeiler, welche aus dem an Ort
und Stelle befindlichen Materiale möglichst regulär gebaut wurden. Die geschätzten Vertical-
achsen dieser Pfeiler repräsentirten die Dreieckspunkte. Selbstverständlich waren unter
solchen Umständen die Centrirungen nicht sehr scharf; auch war es nicht immer möglich
eine hinreichende Anzahl Repetitionen der Winkel zu erhalten. Trotz dem scheinen die
Fehler der centrirten Winkel die von der Conferenz angenommene Grenze nicht zu über-
schreiten, wenn die kleinsten Dreiecke ausgeschlossen werden. Ob aber die Verbindung
dieser Dreiecksreihen mit den neueren sich mit genügender Sicherheit bewerkstelligen lässt,
ist: etwas zweifelhaft, weil die alten Signale (Dreieckspuncte) bei der Detailvermessung zum
Theil zerstört oder umgebaut worden sind.

Neuere Messungen. Die für die Gradmessung in Angriff genommenen Arbeiten
beziehen sich zunächst auf die Verbindung zwischen Christiania und Stockholm. Von nor-
wegischer Seite sollte eine Dreieckskette von dem ersten Orte in südlicher Richtung geführt
werden um an die Seiten des schwedischen Dreiecks Dragonkullen -Vagnarberg-Koster zwi-
schen Frederikshald und Strömslad angeschlossen zu werden. Mit dem trefflichen zu unserer
Disposition gestellten schwedischen Basisapparat sollte für diese Dreiecksreihe eine Grund-
linie gemessen werden und mindestens noch eine für die nördlichen Reihen.

Sommer und Herbst 1863 wurden zu Recognoscirungen und anderen Vorbereitungen
verwendet. In dem südlichen Netze, wo kein recht günstiges Terrain für ein Grundlinie zu
finden war, wurde schliesslich ein Gebirgsplateau Egeberg bei Christiania gewählt. Hier
ward eine Grundlinie von 2025 Toisen Länge Mai — Juni 1864 zweimal gemessen. Bei der
Messung waren ausser dem Unterzeichneten und Oberstlieutenant Naser die bei der topo-
graphischen Abtheilung des Generalstabes angestellten Herren Capitaine Broch und Lieute-
nant Sejersted, gelegentlich auch Capitaine Daa und Observator Mohn behülflich. Im August
desselben Jahres ward nördlich von der kleinen Stadt Levanger am Drontheimsfjord eine

zweite Grundlinie gemessen in einem flachen Littorale, Rinleret genannt, welches einer Ca-

 
