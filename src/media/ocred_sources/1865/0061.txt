2 RP TPT. EFTTA eT

Ian an on

int

rm

x
=
x
=
i
x
à
=

 

ja ee

dello Stato Maggiore; e ad ogni richiesta della Commissione pud farsene copia. Intanto
non sara inutile che noi esponessimo i risultamenti finali, i quali trovansi compendiati nel
quadro No. 1.

Esso quadro contiene la prima misura della base sulla prima pagina, dove

Nella 1*. colonna son registrati i numeri d’ordine delle tre misure indipendenti.

Nella 2*. colonna & messa la lunghezza A di ciascuna spranga moltiplicata pel numero
delle sue ripetizioni; ed inoltre v’ ha registrata la somma S di tutti gli spazii intercetti tra
le spranghe.

Nella 3*. colonna, rimpetto a ciascuna spranga, è registrata la somma di tutti i pro-
dotti della costante m ad essa spranga relativa pe’ termometri metallici; e v ha pure segnata la
somma R delle riduzioni, che ogni spranga deve subire per ridursi all’ orizzonte.

Nella 4*. colonna, in relazione ad ogni misura, vedesi notata in linee la lunghezza
della base.

La seconda pagina del quadro contiene la seconda misura: e tutto @ quivi disposto
come nella pagina precedente. Deve perd notarsi che nella 4°. colonna, alle tre lunghezze
ottenute v’ ha sommata la quantita 15',4026; che esprime l'intervallo tra l’ultima spranga e
Yestremita della base, il quale nel ritorno si é misurato sul piede di legno.

Finalmente a pid del quadro v’ ha il calcolo della lunghezza media della base; ed a
dritta la proiezione di essa al livello del mare.

8. 6°. Intendiamo ora far motto sulla rete geodetica circostante alla base, la quale
rete & disposta come nella Fig. 1?.

La triangolazione indicata ® stata eseguita con un teodolite di Reichembach di 12
pollici di diametro: esso & fornito di quattro nonii per mezzo de’ quali si apprezzano distin-
tamente 2”. Il Cannocchiale di tale istrumento & di Frauenhofer, ha lingrandimento di cirea
45 volte ed & spezzato.

Il metodo impiegato nelle misure angolari & stato il seguente.

Due operatori alternativamente osservavano e registravano le osservazioni.

Ogni angolo veniva osservato in condizioni atmosferiche favorevoli ed in giorni di-
versi 50 volte dall’ uno e 50 volte dall altro operatore.

Il procedimento fu quello della moltiplicazione, e propriamente si muoveva da un
punto della circonferenza dato dalla lettura di quattro nonii; si quintuplicava l’angolo e si
leggevano i nonil; dappoi s’invertiva il Cannochiale e si ritornava verso l’origine e di nuovo
si leggeva. A tal modo si completava una serie di 10 mesure.

Una seconda serie si eseguiva muovendo da un’ origine diversa, e cosi via proce-
dendo si completavano le 10 serie, dal cui medio si deduceva l’angolo chiesto con un errore
abbastanza piccolo come si vedrà nel quadro No. 2. Giova intanto notare che una stazione
non si lasciava mai senza completare ıl giro d’orizzonte.

$. 7°, Per accennare cid che riguarda i segnali, cominceremo da quelli stabiliti alle

due estremitä della base.
General-Bericht f. 1865. 9
