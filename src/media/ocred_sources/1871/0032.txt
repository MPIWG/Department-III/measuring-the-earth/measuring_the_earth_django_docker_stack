 

26

Rete e calcoli tra la base di Puglia e la Dalmazia.

La triangolazione italiana spiegata tra la base di Puglia e la Dalmazia, nel fine di
protrarre oltre la rete meridiana movente da Capo Passero è distinta in due parti.

La prima di queste, la quale da’ punti meridionali Biccari, Ascoli, Cerignola, Torre-
pietre procede sino a punti settentrionali Tremiti, Giovannicchio; & stata compiuta dalla
Stato Maggiore Italiano, quasi tutta negli anni 1868 e 1869 e piccola porzione nel 1864.

La seconda parte, la quale & compresa nel quadrilatero Giovannicchio, Tremiti, Lissa,
Lagosta, & stata eseguita nell’ anno 1869 contemporaneamente da Uffiziali Italiani ed Au-
striaci. Chiariamo intanto esistere un’ altra triangolazione a Nord di Lissa, Lagosta, la quale
® stata eseguita tutta dagli Uffiziali dell’ Istituto geografico di Vienna.

Ora venendo a’ calcoli giova dilucidare che sebbene la Commissione Italiana avesse
disposto di compensare la nostra rete in vari pezzi, di cui ciascuno non offerisse nel medio
più di 30 equazioni di condizione; pure lo Stato Maggiore Italiano affın di trarre il miglior
partito possibile da un lavoro cosi speciale, com’ & quello del collegamento di due Stati, ha
creduto di compensare in un sol pezzo, e colle sole proprie osservazioni, la rete compresa
tra’ punti Biecari, Ascoli, Cerignola, Torre pictre, sino a Lissa, Lagosta.

Cosi eseguito tale lavoro ei lo presenta alla Commissione internazionale e dichiara di
attendere gli accordi coll’ Istituto geografico di Vienna affin di menare a compimento un cal-

colo tanto importante con porre in relazione le osservazioni rispettive colle due basi.

Rete di Calabria che fa seguito a quella di Sicilia.

Alla triangolazione orientale di Sicilia, che muove dalla base di Catania e procede
fino al lato Patti, Trefontane, fa seguito quella di Calabria, la quale attaccandosi al lato sud-
detto, procede verso Nord sino a’ punti Montea, Serracastellara, Sordillo e Trionto. Questa
rete & stata compiuta quasi tutta nell’ anno 1869; e nel volgente anno se nè pure incomin-
ciato a preparare il calcolo.

Perd questo si & momentaneamente sospeso, giacch® nella futura stagione si ha in-
tendimento di collegare i due punti Santacroce e Lipari, affin di aggiungere alle due reti
contigue, un altro lato di passaggio, oltre quello Patti, Trefontane. Come pure perchè si
nutre speranza di congiungere i punti Montea, Stromboli. Il concetto che regolera la com-

pensazione di tale rete, verrà accennato or ora.

Base di Calabria, comparazione delle spranghe di misura, rete
? P Ss ,

circostante alla base.

La grande distanza, che intercede tra la base di Catania e quella di Puglia, fece
sentire alla Commissione Italiana il bisogno di una base intermedia da misurarsi in Calabria.
E lo Stato Maggiore Italiano dopo avere nel trascorso anno disignato il luogo più conve-
niente allo scopo, ch’é la Foce del Crati, ne ha in questo anno compiuta Ja misura, merce

’ ° | . . . ‘ ® . . .
l’Apparato di Bessel. Le particolarità riguardanti tale misura saranno indicate in una rela-

sd ME hdi :à Rédis i) Asa À

rd.

hth

Lula

14a lh hn, dill DAM bi A esate aids

 
