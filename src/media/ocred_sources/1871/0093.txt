PTE ET N N

87

2. Die in jüngster Zeit von Professor Bruhns mit gutem Erfolg in Mannheim ange-
wendete Methode im Vertical des Polarsterns, deren Detail in der Publikation der
astronomisch - geodätischen Arbeiten des geodätischen Institutes vom Jahre 1870
niedergelegt ist. Die Beschränkung, dass diese Methode nur in der Nähe des Me-
ridians anzuwenden ist, entfällt alsbald, wenn man, wie es in der Regel geschieht,
die Azimuthmessung durch Intercalation einer Mire vornimmt.

Sämmtliche Anträge werden einstimmig angenommen.

Herr Bruhns: Es ist im Oentralbüreau im Jahre 1865 eine Anzahl von Sternen nam-
haft gemacht, deren Declinationsbestimmung gewünscht wurde. Ein Verzeichniss derselben
befindet sich in dem Generalberichte für 1865 und die Sternwarten Leiden, Leipzig und
Neuenburg übernahmen es, die genauen Bestimmungen auszuführen. Die Leidener Stern-
warte hat die Sternpositionen publieirt, auch habe ich gestern schon mitgetheilt, dass die
Beobachtungen in Leipzig vollendet und die Reductionen ausgeführt sind, die Publication
derselben steht bevor. Neuenburg hat sich einverstanden erklärt, da verschiedene Hindernisse
die dortige Bestimmung nicht ermöglicht haben, wenn die Positionen aus den Beobachtungen
der beiden Sternwarten Leiden und Leipzig abgeleitet werden. Die Commission bat sich
daher geeinigt Ihnen den Antrag zu stellen:

Die Conferenz der Europäischen Gradmessung wünscht, dass durch das Üentral-
büreau bis Ostern 1872 die definitiven Declinationen der Gradmessungssterne aus
den Leidener und Leipziger Beobachtungen, mit eventueller Zuhülfenahme der
Pulkowaer und Greenwicher Positionen, abgeleitet werden. |

Die Arbeit würde zunächst die sein, die Leidener und Leipziger Beobachtungen zu
vergleichen und falls sich beträchtliche Abweichungen zeigen sollten, würden die genannten
Cataloge zu Hülfe zu nehmen sein. In Betreff der Eigenbewegung hat Herr Kaiser schon
vorgearbeitet und da ich hoffe, dass mir zum Theil die neu reducirten Bradley’schen Positio-
nen zu Gebote stehen, würde mit deren Hülfe die Eigenbewegung leicht abgeleitet werden
können. Ich hoffe auch, dass die Kräfte des Centralbüreaus ausreichen, die im Anfange
festgesetzte Zeit innezuhalten und empfehle im Namen der Commission die Annahme des
Antrages. |

Der Antrag wird einstimmig angenommen.

Herr Bruhns: Es liegt mir noch ob über die Frage 3 des Programms, betreffend
die Bestimmung der Intensität der Schwere, zu berichten. Aus den Generalberichten ist
allen Herren bekannt, dass Pendelbeobachtungen im letzten Jahre von den Herren Plantamour,
Hirsch, Peters und mir gemacht oder veranlasst sind. In Russland hat sich Herr Sawitsch
ebenfalls mit Pendelbeobachtungen beschäftigt. Mit Ausnahme der absoluten Bestimmungen
von Herrn Peters, die mit dem Bessel’schen Fadenpendel angestellt wurden, sind alle übrigen
Beobachtungen mit Reversionspendeln von Repsold und einem von Lohmeier angestellt. Ueber

die Frage der Anwendung des Reversionspendels und welche Construction zu empfehlen sei,

ist schon in der vorigen Conferenz viel discutirt und man ist damals zu dem Beschlusse ge-

 
