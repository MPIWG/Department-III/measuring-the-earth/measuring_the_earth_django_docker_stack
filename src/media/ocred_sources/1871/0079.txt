TI nn en

RT PT NV!

13

diesen zu tragen ist und eine Rückäusserung über die Bereitwilligkeit, diesen Antheil zu
tragen, erbittet. Es scheint mir unnöthig über diesen Geschäftsgang einen Beschluss zu
fassen; überlassen Sie dem Centralbüreau und der permanenten Commission, die Sache so
praktisch als möglich anzufassen.

Herr Seidel: Mit diesen Aufklärungen erkläre ich mich befriedigt und lasse meine
Bedenken fallen.

Der Antrag der Commission, vorgetragen von Herrn Ibanez, wird darauf einstimmig
genehmigt.

Präsident v. Fligely: Ich ersuche nun Herrn Baur über die Präcisions-Nivellements
Bericht zu erstatten.

Herr Baur: Der erste Gegenstand, über welchen die geodätische Commission in Be-
treif der Nivellements zu berathen hatte, bot sich dar durch ein von Herrn Baeyer einge-
reichtes Schreiben, in welchem die Errichtung eines registrirenden Pegels auf Helgoland als
besonders wünschenswerth bezeichnet und motivirt wurde. Die Commission konnte sich die-
sem Wunsche nur anschliessen. .Sie beschäftigte sich darauf von Neuem mit dem Beschlusse,
welcher bereits 1864 über die Errichtung von Registrirapparaten, um die mittlere Höhe des
Meeres festzustellen, gefasst wurde. Die Berathung über diese Gegenstände führte zu fol-
genden Anträgen, welche die Commission an die Conferenz bringt:

1. Die Conferenz der Europäischen Gradmessung möge an die königlich englische
Regierung das ergebenste Gesuch richten, auf der Insel Helgoland, welche sich
ihrer Lage nach besonders dazu eignet, einen registrirenden Pegel zu errichten.

2. Die Oonferenz der Europäischen Gradmessung erweitert ihren Beschluss vom
Jahre 1864 dahin, dass die an das Meer grenzenden Staaten, welche bei der Eu-
ropäischen Gradmessung betheiligt sind, dringend ersucht werden, an möglichst
vielen Punkten durch Registrirapparate die mittlere Höhe des Meeres festzustellen.

oo

Um diesem Beschlusse weitere Folge zu geben, ersucht die Conferenz die Herren
Commissare der betreffenden Staaten, der permanenten Commission bis zu ihrer
nächsten Zusammenkunft Bericht zu erstatten, ob und an welchen Punkten die
Aufstellung solcher Pegel ausgeführt ist oder in Aussicht steht.

Herr Herr: In der gestrigen Sitzung der geodätischen Commission waren Zweifel
darüber, ob im Adriatischen Meere schon registrirende Pegel bestehen. Durch Mittheilung
des Herrn Dr. Jelinek ist dieser Zweifel gehoben und ich kann mittheilen, dass sich registri-
rende Pegel in Triest, Pola, Fiume, Lesina und Zara befinden, nur ist augenblicklich nicht
bekannt, ob der Apparat in Zara in Thätigkeit ist, die übrigen sind in regelmässiger Function.

Die durch Herrn Baur gestellten Anträge werden darauf einstimmig angenommen.

Herr Bruhns: Da in mehreren Staaten registrirende Pegel in Thätigkeit sind, würde
es für die Commissare, in deren Ländern Pegel zu errichten sind, wünschenswerth sein, die
Einrichtung der bestehenden kennen zu lernen, ich spreche daher als Wunsch aus,

da es zur Ausführung des 6. Antrages vortheilhaft ist, die Construction der ver-
Generalbericht für 1871. 10

 

 
