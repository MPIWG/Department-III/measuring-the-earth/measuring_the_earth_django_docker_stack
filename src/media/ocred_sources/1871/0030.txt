 

24

 

ment der Main-Weser-Bahn in Frankfurt a. M. begonnen und wurde zunächst die Verbin-
dung von Frankfurt mit Baden durch eine vierfache Nivellirung der Main - Neckar- Bahn
hergestellt. Weiter wurde ein Anschluss an Preussen bewerkstelligt durch Nivellirung der
Ludwigsbahn von Mainz resp. von Darmstadt nach Bingen. Weiter ist in diesem Sommer
die Verbindung mit Bayern durch Nivellements der Main-Rhein-Bahn nach Aschaffenburg
angebahnt. Die Verbindung mit Rheinbayern bei Worms wird im Augenblick vollzogen.
Ueber die Resultate der verschiedenen Nivellements behalte ich mir erst nach ihrer gänzli-
lichen Vollendung einen ausführlichen Bericht vor. In Hessen soll nämlich das ganze nicht
unbedeutende Eisenbahnnetz nivellirt werden und davon sind bis jetzt 217 Kilometer vollen-
det. Durch Nivellirung des ganzen Eisenbahnnetzes werden sich dadurch viele Polygone er-
geben und ebenso viele Controllen geschafft werden.

Herr Hirsch trägt auf Schluss der Sitzung an, damit die Constituirung der zwei Com-
missionen vor sich gehen könne. |

Der Antrag wird angenommen und die Sitzung um 1 Uhr 45 Minuten geschlossen.

Zweite Sitzung
der

dritten allgemeinen Conferenz der Europäischen Gradmessung,

Wien, Freitag den 22. September 1871.

Anfang der Sitzung 10 Uhr 30 Minuten.

Präsident: Herr v. Fligely.

Schriftführer: Herr Brukns und Herr Hirsch.

Präsident v. Fligely ladet vor Eingang in die Tagesordnung zu einer Excursion nach
dem Semmering am folgenden Tage ein, welche Einladung dankbar angenommen wird.

Auf der Tagesordnung stehen geschäftliche Mittheilungen, dann die Fortsetzung der
Berichte der einzelnen Mitglieder über den Stand der Arbeiten in den von ihnen vertrete-
nen Ländern.
Das Protokoll wird verlesen.

Ma dits à; des |) A à à

ss hui

ia am dé a Mb a ba Ah nada mu à

 
