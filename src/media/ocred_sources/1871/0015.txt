Tee

ve FR TFT TE TI |

PENDU I

PPT

PP PET POP DIET LUEUR TRE TNT

wert

9

können. Die bereits für den 19. September 1870 in Wien anberaumte Sitzung musste auf
dieses Jahr verschoben werden.

In der letzten allgemeinen Oonferenz 1867 in Berlin erweiterten Sie, nachdem zu
unserer Gradmessung noch die westlichen und östlichen Staaten Europa’s getreten waren,
die Mitteleuropäische Gradmessung zu der Europäischen; die permanente Commission
hat der ihr auferlegten Verpflichtung gemäss sich bestrebt, die Zwecke derselben möglichst
zu fördern. Es liegt ihr zunächst ob, Ihnen in aller Kürze einen Bericht ihrer Thätigkeit
abzustatten.

In den vier Jahren von 1867 bis 1871 versammelte sich die permanente Commission
viermal: das erstemal gleich nach der allgemeinen Conferenz in Berlin am 6. October 1867
daselbst, in welcher Sitzung sie sich constituirte und eine Commission zu Maassvergleichun-
gen wählte. Die zweite Oonferenz in Gotha, vom 8. bis 10, October 1868 war nur eine
berathende, da die nöthige Anzahl der Mitglieder zur Beschlussfähigkeit fehlte. Die dritte
Versammlung, vom 23. bis 29. September 1869, fand in Florenz statt und die vierte am
vorgestrigen und gestrigen Tage hier in Wien, um die nöthigen Vorbereitungen zu der all-
gemeinen Conferenz zu treffen. Die Protokolle aller dieser Verhandlungen sind bis auf die
letzte in Ihren Händen, so dass es hinreicht, in wenigen Worten anzugeben, in wie weit die
permanente Commission, den ausgesprochenen Anforderungen gemäss, die Europäische Grad-
messung seit der letzten Conferenz als gefördert betrachtet.

Die alljährlich von unserm Centralbüreau herausgegebenen Berichte haben Ihnen über
die Fortschritte der Europäischen Gradmessung möglichst detaillirt schon Aufschluss gegeben
und eine allgemeine Uebersicht wird daher genügen.

In Scandinavien sind die en zu Ende gebracht; es fehlt nur
noch die Bestimmung der astronomischen Punkte und ihre Verbindungen mit dem Haupt-

dreiecksnetze.

Eine weitere Publikation über die längst vollendeten Gradmessungsarbeiten in Däne-
mark steht bevor.

Die Vollendung der Längengradmessung auf dem 52. Parallel rückt immer näher
heran, die Herren Baeyer und Forsch lassen die bisherigen Lücken ergänzen.

In Russland sind zum Zwecke der Europäischen Gradmessung telegraphische Längen-
bestimmungen zwischen Pulkowa, Abo, Helsingfors und Stockholm ausgeführt.

In Deutschland sind in allen Ländern die Arbeiten sehr gefördert. In Preussen ist,
den Wünschen der Conferenz und der permanenten Commission entsprechend, unter Leitung
des Herrn General Baeyer ein permanentes geodätisches Institut ins Leben getreten, das so-
wohl trigonometrische, als auch astronomische und nivellitische Arbeiten ausführt und sich sehr
angelegentlich mit Maassvergleichungen beschäftigt.

In Sachsen sind die trigonometrischen Arbeiten gefördert, die nivellitischen nahe
vollendet, von den astronomischen fehlt nur wenig, die Basismessung soll im nächsten Jahre

ausgeführt werden.
Generalbericht für 1871. 2

 
