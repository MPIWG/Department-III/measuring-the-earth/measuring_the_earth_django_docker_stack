ET TEE PT 1° 1

85

3. Wird es für besonders wünschenswerth erachtet, dass alle Breitenbestimmungen
nach beiden Methoden, sowohl durch Cireummeridianhöhen als auch im ersten Ver-
tical ausgeführt werden. |

Bekanntlich giebt der aus der Uebereinstimmung der Beobachtungen berechnete wahr-
scheinliche Fehler nicht immer ein ganz sicheres Maass für die erreichte Annäherung an die
Wahrheit, deshalb bildet noch immer die Uebereinstimmung der Resultate mehrerer Methoden
die durchgreifendste Controle.

4. In weiterer Verfolgung der Ansicht, dass eine möglichste Vervielfältigung der Me-
thoden eine Sache von hoher Wichtigkeit ist, wird empfohlen, die in Amerika bei
der Coast Survey schon seit vielen Jahren eingebürgerte Methode der Breitenbe-
stimmung mittelst des Zenith- Teleskops auch bei der Europäischen Gradmessung
einzuführen.

Eine genaue Beschreibung dieser Methode, welche von den amerikanischen Beobach-
tern für eins der expeditivsten und sichersten Mittel der Breitenbestimmung angesehen wird,
enthält der Report of the Superintendent of the U. S. Coast Survey for 1857 #) auf den wir
deshalb in Bezug auf das Detail der Methode verweisen.

5. Ferner, dass bei Breitenbestimmungen im ersten Verticale nur im Nothfalle und
mit äusserster Vorsicht Universalinstrumente benutzt werden sollen. Beim Ge-
brauche derselben wird auf vo. Oppolzer’s Vorschlag angerathen, dasselbe zwischen
der Ost- und Westpassage eines Sternes nicht umzulegen, an aufeinander folgen-

den Abenden aber für denselben Stern in verschiedenen Kreislagen anzuwenden.

6. Um die periodischen Fehler der Schrauben der Ablesungsmikroskope zu berück-
sichtigen, empfiehlt sich als das bequemste Verfahren, eine von den russischen
Beobachtern eingeführte Einrichtung. Sie besteht darin, dass in’s Mikroskop statt
eines, zwei Doppelfäden (welche in einer um eine halbe Schraubenrevolution ge-
ringeren Distanz als das Intervall der einzelnen Theilstriche liegen) eingespannt
werden. Dass dabei die sogenannte Run-correction der Mikroskope mit hinreichen-

der Schärfe zu bestimmen ist, wird hier nur der Vollständigkeit wegen erwähnt.

Uebergehend auf die Methoden der Azimuthbestimmung, so sind nebst der seit
lange üblichen Methode der Bestimmung dieses Elementes mittelst Horizontaldistanzen des
Polarsterns in neuerer Zeit zwei weitere eingeführt werden.

Die erstere wurde von Director v. Littrow bei der allgemeinen Conferenz des Jahres
1864 proponirt, und kann als Methode der Azimuthbestimmung durch das Mittagsrohr als
Collimator bezeichnet werden. Es wird nämlich nach derselben das Universalinstrument so

aufgestellt, dass es in die Ebene des Mittagsrohres gedreht und der Winkel des terrestrischen

#

*) Account of the method and formula for the determination of the astronomical latitude by means
of the zenith telescope, as used in the Survey of the coast of the United States. By C. A. Schott.
(Appendix 31.)

 
