|

EEE UT RSR TR

RÉ ee Re

TE

 

spe sine

12

Bis zum Schluss des Jahres 1869 war das Centralbiireau sowohl hinsichtlich seiner
Geldmittel als auch seiner wissenschaftlichen Kräfte sehr unsicher gestellt. — Das Budget
musste jährlich vom Hause der Abgeordneten neu bewilligt werden. — Professor Sadebeck
vom Magdalenen-Gymnasium in Breslau war auf 1/, Jahr zum Centralbüreau beurlaubt;
Professor Bremiker erhielt durch das Interesse, welches Se. Excellenz der Herr Handelsmi-
nister Graf v. Itzenplits an der Gradmessung nahm, jährlich in den Sommermonaten Urlaub,
um an den Winkelmessungen Theil zu nehmen, und Professor Börsch benutzte seine Ferien,
um an dem Hauptnivellement zu arbeiten.

In den Wintermonaten waren die Arbeitskräfte auf Professor Sadebeck und einige
Assistenten beschränkt. Die astronomischen Arbeiten allein, welche unter Leitung des Herrn
Director Bruhns in Leipzig stehen, behielten ihr Personal auch im Winter bei. Erst mit der
Gründung des geodätischen Institutes, vom 1. Januar 1870 ab, ist das Centralbiireau in
den Stand gesetzt worden, einen geregelten Arbeitsplan fiir Sommer und Winter einfiihren
zu können.

Es war nöthig, alle diese Schwierigkeiten hervorzubeben, um der Ansicht vorzubeu-

D ?
gen, als sei das Centralbüreau mit ausreichenden Kräften versehen gewesen oder als hätten

dieselben so leicht beschafft werden können. .

ÄA. Greodätische Arbeiten.

Triangulation für die Europäische Längengradmessung unter dem 52. Parallel.

1. Die im Jahr 1847 von mir bei Bonn gemessene Grundlinie wurde berechnet und
die alten Winkelmessungen zu ihrer Verbindung mit dem Hauptdreiecksnetz aus-
geglichen (Generalbericht pro 1868).

2. Von der Seite Michelsberg-Löwenburg der Bonner Grundlinie wurde eine Drei-
eckskette bis zum Anschluss an die Belgische Seite Ubagsberg-Roermonde neu
gemessen, ausgeglichen und berechnet (Generalbericht pro 1870).

©

Die Winkelmessungen von der Bonner Grundlinie bis zum Anschluss an die Kur-

hessischen Punkte Hasserod, Diinstberg, Tautstein, die im vorigen Jabre durch

den Krieg unterbrochen wurden, werden in diesem Jahre fertig. — Die Kurhessi-
schen Dreiecke selbst, bis zum Anschluss an die Gaussische Seite Brocken-Insels-
berg sind schon im Generalbericht pro 1866 mitgetheilt.

4. Die Winkelmessungen einer neuen Dreieckskette zur Verbindung der Berliner und
Leipziger Sternwarte bis zum Anschluss an die Seite Brocken-Inselsberg, nebst
den Ausgleichungen auf den Stationen werden ebenfalls in diesem Jahre fertig.

5. Zur Verbindung von Leipzig und Breslau durch eine einfache Dreieckskette ist

die Winkelmessung beendigt, und der Anschluss an die Kette Zobten-Trockenberg

(bei Tarnowitz), welche bereits in den Verbindungen der Preussischen und Russi-

schen Dreiecksketten, Berlin 1857, veröffentlicht ist, hergestellt.

Ma bh dar ll Ms hai 2

ic

Hal

à a than an a, bd a en an ti sadn 2

 
