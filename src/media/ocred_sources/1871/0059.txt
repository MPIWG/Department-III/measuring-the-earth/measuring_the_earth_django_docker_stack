SF PIPES YI YT Meer he Tt

:
r
;
È
:
4
F

Vierte Sitzung

dritten allgemeinen Conferenz der Europäischen Gradmessung.

Wien, Freitag den 26. September 1871.

Anfang der Sitzung 1 Uhr.

Präsident: Herr v. Fligely.

Schriftführer: Herr Bruhns und Herr Hirsch.

"Präsident v. Fligely: Bevor wir zur Tagesordnung kommen habe, ich den Herren mit-
zutheilen, dass sich Herr Oberst Barozzi als bevollmächtigter Vertreter der rumänischen Re-
gierung in unserer Mitte eingefunden hat und erlaube ich mir in Ihrem Namen Herrn Ba-
rozzi zu begrüssen.

Präsident v. Fligely stellt den Mitgliedern, welche die Uebersichtskarte von den Grad-
messungsarbeiten in Europa noch nicht erhalten haben, selbige zur Verfügung, ersucht die
Herren, ihm über etwa vorkommende Fehler bis Neujahr zu berichten , und erklärt sich
bereit, die Karte dann neu drucken und an die Commissare vertheilen zu lassen, welches
Anerbieten mit Dank angenommen wird.

Herr Bruhns verliest das Protokoll der letzten Sitzung, welches mit einer kleinen
Modification genehmigt wird.

Präsident v. Fligely: Unsere Tagesordnung enthält die Vornahme der Wahlen in die
permanente Commission und die Entgegennahme von Berichten. Für den ersten Gegenstand
ist ein Verzeichniss sämmtlicher Bevollmächtigter, welche wählbar sind, angefertigt.

Herr Herr: Bei der Mittheilung in Betreff derjenigen Mitglieder der permanenten
Commission, welche ausgeloost wurden, erhoben sich bezüglich des Herrn Baeyer einige
Zweifel darüber, ob Herr Baeyer als Präsident des Centralbüreaus ständiges Mitglied der
permanenten Commission sei oder nicht. Es ist zwar damals von Seiten der Conferenz eine
Entscheidung in dem Sinne getroffen worden, dass Herr Baeyer als ausgeschieden zu be-
trachten sei und sich einer Neuwahl unterziehen müsse, ich erlaube mir aber, trotz dieser
Entscheidung den Antrag zu stellen:

die Conferenz möge beschliessen, dass der gegenwärtige Präsident des Centralbü-
reaus, Herr Baeyer, ständiges Mitglied der permanenten Commission sel.

Der Antrag wird einstimmig angenommen.

 
