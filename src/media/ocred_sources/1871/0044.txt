 

38

Diınıe (3)
Laaerberg I (Pfeiler) und Andreasberg (Pfeiler).
Vom 2. bis 16. September (20 halbe Tage). Durchschnittlich 1382 ° für den ganzen Tag.

+
i
à
j
3
3
a
a
3
a
3
3
a
3
q
1

Distanz Anzahl Höhenunterschied ın
dieser der In- :
Namen der Repéres. |gebrochenen | strumen- Wr. Klafter. Differenz.
Linien in ten-

Wr. Klafter. stande. Te Messung. I. Messung. | IL. Messung. Messung.

 

SEE SEEN SE SE Teig re ae 5

 

 

 

 

El en Br D en —
Laaerberg I— Oberlaa. . . . 1389 38 + 38°.8665 | +38°.8652 | + 0°.0013
Oberlaa — Johannesberg . . . 1176 29 —. 11.6987 | — 11.6958 | — 0.0029
Johannesberg — Oberlanzendorf . 1778 3 + 17.9553 | + 17,9529) + 0.0024
Oberlanzendorf — Pellendorf . 563 17 — 0.9302 | — 0.9296 | — 0.0006
Pellendorf — Andreasberg . 2008 40 | — 27.8373 | — 27.8368| —0.0005
| Linie (3) | poi, | 35 3 | + 16.3956 |+ 16.3959] —0.0003 |
} | Mittel “163558  Mittl. Fehl. = 0.0046 .
; : : aus dem Präcisions-Nivellement . . 16°.3558 |
i Hohenunterschied / a Lo
i aus gleichzeitigen Zenithdistanz-Messungen . . . - - 16.4310
| Differenz. . 0.0752
| Tinie (4)
4 Laaerberg I (Pfeiler) und Anninger (Pfeiler).
4 - ; aus dem Präcisions-Nivellement (Linie 1 und 2). . . 223°.3692 |
I Höhenunterschied | N a
aus gleichzeitigen Zenithdistanz-Messungen. . . - -  223.4740 |
N Differenz. . 0.1048
| ine (dD)
| Eichkogl (Pfeiler) und Andreasberg (Pfeiler). i
| u : aus dem Präcisions-Nivellement (Linie | umd Ss): . .10°.9718 |
a . Höhenunterschied : D Vo
" aus gleichzeitigen Zenithdistanz-Messungen . 76.7880
a Differenz . - 0.1898
s | Linie (6)
4 Anninger (Pfeiler) und Andreasberg ( Pfeiler ).
q benne old aus dem Präcisions-Nivellement (Linie 1, 2 und 3) . 2591100
aus gleichzeitigen Zenithdistanz-Messungen. 239.6090

 

Differenz. . 0.1160

Was den Vergleich dieses Präcisions-Nivellements mit den aus gleichzeitigen Zenith-

distanz -Messungen hervorgegangenen Höhenunterschieden betrifft, so muss bemerkt werden,

dass um diesen vollständiger herzustellen, zu verschiedenen Jahreszeiten und zahlreicher aus-

geführte Messungen zu Grunde liegen müssten.

|
|
|

 
