1 PPR PPP "YT eg |

ror

CONCEPT

mt

tft

:
R
=
à
&
=
ë
=
E
Ê
r
a
=

 

Herr Hirsch: Indem ich nochmals zu dem Gegenstande der Ocularstellung zurückkehre, stimme ich
Herrn Oppolzer bei, dass bei absolut centraler Beleuchtung alle die erwähnten Erscheinungen verschwinden;
bei Tageslicht ist keine Spur von diesen Differenzen vorhanden. Ich gebe aber Herrn Oppolzer zu beden-
ken, dass die Erzielung einer absolut centralen Beleuchtung bei vorhandenen Instrumenten ihre Schwierigkeit
hat. Jede kleine Drehung des reflectirenden Spiegels oder der Prismen ändert bekanntlich die Beleuchtung
und kann das Doppeltsehen der Fäden hervorbringen. Die Ocularstellung, bei welcher man nur ein Bild er-
hält, ist daher ein Mittel, die Feblerquelle zu vermeiden und bei jeder Aenderung der Beleuchtung an-
wendbar. Was die theoretische Ausführung betrifft, welche uns Herr Fearnley mitgetheilt hat, so erwähne
ich, dass schon zu Anfang dieses Jahrhunderts Carlini ähnliche Beobachtungen gemacht und in den Mailän-
der Ephemeriden für 1819 die erwähnte Erklärung gegeben hat. Mir scheint die Ocularstellung viel leichter
erhältlich zu sein, als die centrale Beleuchtung. Da aber allzu grosse Vorsicht nichts schadet, so kann
man sowohl die scharfe Einstellung des Oculars, als auch die centrale Beleuchtung empfehlen.

$) Zu Breiten- und Azimuthbestimmungen.

Herr Weiss: Im Jahre 1864 und 1867 wurde ausgesprochen, dass die Methode der Durchgangsbeob-
achtungen im ersten Vertical eine wünschenswerthe Controle für die Methode der Circummeridianhôhen bei
Breitenbestimmungen ist. Ks wäre nun wichtig hinzuzufügen, dass wo möglich beide Methoden angewandt
werden sollen, weil es in den letzten Jahren wieder mehrfach vorgekommen ist, dass die Uebereinstimmung
einer Methode und der daraus berechnete Fehler nicht immer ein sicheres Kriterium für die erreichte An-
näherung an die Wahrheit abgiebt. Die einzige Controle ist die Anwendung mehrerer Methoden.

Herr v. Oppolzer: Ich möchte dies fast als obligatorisch aussprechen, denn die Vergleichung verschie-
dener Methoden giebt nach dem Generalberichte von 1867 ganz beträchtliche Differenzen.

Herr Bruhns: Ein grosser Theil des Unterschiedes in den Resultaten liegt in den Declinationen der
Sterne. Ich möchte vorschlagen, dass die Anwendung beider Methoden als dringend wünschenswerth, wie
früher, bezeichnet wird. |

lerr Weiss: Von eben diesem Gesichtspunkt ausgehend, dass die Vervielfältigung der Methoden ei-
gentlich die beste Gontrole sei, erlaube ich mir der Commission vorzuschlagen: man möge zur Bestimmung
der geographischen Breite auch bei uns eine Methode einführen, die schon seit Jahren in Amerika ange-
wandt wird, nämlich die mit dem sogenannten Zenithteleskop. Eine genaue Beschreibung derselben ist in
den Berichten der Coast Survey von 1857 gegeben und ich will in kurzen Worten das Princip dieser Me-
thode angeben. Das Universalinstrument wird so eingerichtet, dass man auch Deelinationsdifferenzen mit
demselben messen kann; es werden nun zwei Sterne ausgewählt, deren Culminationszeit nicht weit entfernt
ist und deren Zenithdistanz nördlich und südlich nur um eine geringe Grösse verschieden ist, eine Grösse,
die geringer sein muss, als das Gesichtsfeld des Fernrohrs. Wenn man auf die Zenithdistanz des ersten
Sternes in der Oulmination einstellt und dann das Fernrohr im Azimuth um 180° dreht, kömmt man auf die
andere Seite des Zeniths und findet dort den zweiten Stern, auf welchen der Mikrometerfaden einzustellen
ist. Die Differenz der Zenithdistanz, welche durch das Mikrometer gemessen wird, giebt in Verbindung mit
der Declination der Sterne unmittelbar die Breite.

Im Jahre 1867 wurde weiter erklärt, dass für Durehgangsbeobachtungen im ersten Vertical in erster
Linie Passageninstrumente, dagegen Universalinstrumente nur mit grösster Vorsicht anzuwenden seien. Hier
wäre nochmals zu betonen, dass das Universalinstrnment nur mit äusserster Vorsicht‘bei Beobachtungen
im ersten Vertical angewendet werden darf, weil selten das Universalinstrument in seiner Lage unver-
änderlich ist.

Herr v. Oppolzer: Ich erlaube mir dem beizufügen, dass ich beim Gebrauche des Universalinstrumen-
tes zur Bestimmung im ersten Vertical folgendes Verfahren eingeschlagen habe: ich habe immer je zwei
Sterne ausgewählt, bei welchen die Westpassage des erstern beendet war, bevor der andere in die Ostpas-
sage eintrat; während der ganzen Passage des ersten Sternes liess ich das Universalinstrument stehen
ohne umzulegen, setzte die Libellen mit grosser Vorsicht auf und stellte nur das Fernrohr auf die Zenith-
distanzen ein; darauf wurde umgelegt und der zweite Stern ebenfalls ohne jede weitere Umlegung beobach-
tet. Am andern Abend beobachtete ich die Sterne in den dem ersten Abend entgegengesetzten Lagen des
Instrumentes. Bei der gewöhnlich empfohlenen Methode geschieht die Umlegung während der Culmination
des Sternes, beim Universalinstrument ist es aber zweckmässiger nicht während des Durchgangs umzulegen.
