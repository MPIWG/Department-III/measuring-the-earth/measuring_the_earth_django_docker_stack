1 Pr TPT PTT NT Y ITT YT HN ttt

Inhalt

Erste Sitzung
der Dritten allgemeinen Conferenz der Europäischen Gradmessung.

Verzeichniss der anwesenden Bevollmächtigten und Eingeladenen

Eröffnung der ersten Sitzung, Begrüssungen

Verlesung der Geschäftsordnung.

Wahl des: Büreaus’ der Versammlung 2.2.2... 008000,

Programm der zu discutirenden Fragen .

Bildung zweier Commissionen zur Vorberathung ee jieaeen :

Bericht über die Thätigkeit der permanenten Commission, von Herrn Ent : :

Bericht uber die Thatigkeit des Preussischen neodätischen Instituts und des AU ee  Centtal

büreaus seit der zweiten allgemeinen Conferenz, von Herrn Baeyer

A. Geodätische Arbeiten.
B. Astronomische Arbeiten. ne.
OC. ‚Bauptnivellement .... . 2... 2. nn
a
E. Theoretische Arbeiten 3

Bericht der Herren Jordan und Bruhns uber die been in Baden

Vermuthete Anomalie der Schwerkraft bei Mannheim :

Bericht der Herren Bauernfeind und Seidel über die Arbeiten, in Dauer ;

Bericht des Herrn Hügel über die Arbeiten in Hessen.

Zweite Sitzung.

Geschäftliches, Büreau der Commissionen wi,

Mittheilung ition die Constituirung der Commis ae al one Geld pay

Bericht des Herrn Schiavoni über die Arbeiten in Italien. . . . .,. . ..

Bericht des Herrn Paschen über die Arbeiten in Mecklenburg. . . . ; i

Vorlesung eines Briefes des Herrn Kaiser uber die Arbeiten zur nenne an dr Beidsner Stein:
warte,

Dritte Sitzung.

Geschäftliches. . . . i a ase

' Bericht des Herrn Gana über die Trans Rrbaiten in sstanisidn’ Ungarn

Triangulation, Breiten - und Azimuthmessungen

Seite

OO I Et Qt

12
13
14
15
17
19
20
21
23

24
25
25
31

32

33
33
34

 
