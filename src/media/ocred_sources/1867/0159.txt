Tre we we ee en

 

(LLL AM ALIEN

it

 

155

mission und durch diése, soweit es irgend möglich ist, auch den Staaten, die es
etwa wünschen, wieder zur Verfügung stehen werden. Das Centralbureau wird aber
in den Ländern, die es wünschen, die Triangulationen nicht ganz selbstständig und
unabhängig unternehmen, sondern es ist mein Wunsch, dass es nur die Instrumente
hergebe und auf Verlangen auch persönliche Hülfe, dass aber die Commissare der
betreffenden Länder diese Arbeiten leiten und zwar so, dass sie sich diese Arbeit
für ihr Land vollständig aneignen als ihre eigene Arbeit. Ich habe schon im ver-
flossenen Sommer, da Baden den Wunsch der Uebernahme der Gradmessungs-
Arbeiten vom Centralbureau ausgesprochen, in Carlsruhe zur Sprache gebracht, dass es
für die Grossherzogliche Regierung in jedem Falle höchst wünschenswerth sein würde,
dass einer ihrer Gelehrten, und namentlich der Professor der höheren Geodäsie an dem
dortigen Polytechnicum an den Arbeiten Theil nehme und ich zweifle nicht, dass
dieser Wunsch erfüllt werden wird. Die Absicht, die ich dabei habe, ist die, dass in
allen deutschen Ländern Greodäten vollständig und gründlich durchgebildet werden und
dass namentlich bei den polytechnischen Schulen die Lehrer durch die praktischen
Arbeiten, welche sie mit durchmachen, Material für den Unterricht gewinnen.

Schliesslich danke ich dann noch der Versammlung für die Nachsicht und das
Vertrauen, welches Sie mir geschenkt haben. Ich habe leider das nicht leisten
können, was Sie von mir fordern konnten, aber die Herren Schriftführer haben mich
in meinem Amte so kräftig unterstützt, dass ich die Versammlung ersuche, den Herren
Schriftführern den Dank der Conferenz dadurch zu erzeigen, dass Sie sich von Ihren
Plätzen erheben.

(Geschieht.)

Herr Bruhns: Wir sind Ihrer gütigen Nachsicht zu Danke verpflichtet. Ich danke
Ihnen besonders, dass Sie mir die Sache der Protokollirung so leicht gemacht und das
Büreau durch vollständige Einreichung Ihrer Berichte so sehr unterstützt haben.

Herr Hansen: Meine Herren, ehe wir auseinandergehen, haben wir noch eine
Pflicht zu erfüllen: Wir müssen unsern Dank der Königlich Preussischen Regierung
abstatten, welche schon so viel für unsere Angelegenheit gethan hat; zuerst haben wir
zu danken für die von der Königlich Preussischen Regierung in den letzten drei Jahren
zu unserem grossen Unternehmen bewilligten Geldmittel, zweitens für die, als gewiss in
Aussicht gestellte Vermehrung dieser Mittel und drittens für die bereitwillige Ueber-
lassung dieses Lo:ales. Ich ersuche die Heren diesen Dank durch Aufstehen von Ihren

Sitzen auszudrücken.
(Geschieht.)

Herr Förster: Meine Herren, mit Zustimmung Sr. Exellenz des Herrn General-
Lieutenant Baeyer erlaube ich mir zur Beantwortung dieses Dankes einige Worte zu erwi-
dern. Seien Sie überzeigt, dass unsere Regierung die ehrenvolle Bedeutung im höchsten
Maasse anerkennt, welche Ihre Zusammenkunft in unserer Hauptstadt hat. Seien Sie
überzeugt, dass unser Dank durch die That bewährt werden werden wird, und dass die
Königliche Regierung — ich bin zu dieser Versicherung ermächtigt — die von ihr

20

 

 
