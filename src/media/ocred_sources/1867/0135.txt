He

 

Jam ann

a RT

134

Wissenschaft liege. Es würde genügen, dass man nur den practischen Grund anführt,
welcher auf das Meter als dieses Maass hinweist, dessen Wahl man befürwortet; und
zwar weil es nach der Lage der Sache heute das einzige ist, welches Aussicht hat,
als Einheitsmaass von sämnitlichen Staaten Europa’s angenommen zu werden. Ich
glaube also, wir verlieren nichts und vermeiden es, zu viel zu sagen, wenn wir diesen
Passus weglassen.

Herr Zfirsch: Ich bemerke, dass der Vorschlag des Herrn [Herr identisch ist
mit dem Vorschlage des Herrn Simons, und ich bin meinerseits vollständig damit ein-
verstanden, in der Hoffnung, dass Herr v. Strwve in der Fassung des ersten Punktes,
wo wir uns nur im Interesse der Wissenschaft für ein einheitliches Maasssystem er-
klären, die genügende Garantie dafür schen möge, dass wir eben nicht in den Verdacht
gerathen, uns ausserhalb unserer Befugniss zu bewegen. In dieser Hoffnung stimme
ich dem Vorschlage der Herren Simons und Herr bei, so dass der Antrag 5
lauten würde:

„Da unter den möglicherweise in Betracht kommenden Maassen das Meter
die grösste Wahrscheinlichkeit der Annahme für sich hat, so spricht sich
die Conferenz für die Wahl des metrischen Systems aus.“

Herr Hansen: Nach der Hinweisung auf die bereits vorhergehende nn
des Interesses der Wissenschaft bin ich jetzt damit einverstanden.

Präsident Baeyer: Verlangt noch Jemand das Wort?

Herr ». Struwe: Meine Herren, ich habe das Wort jetzt nicht mehr ergriffen,
weil ich bereits in der Commission meine Ansichten auseinandergesetzt habe. Ich halte
es nicht für geeignet, dass wir uns hier mit’Fragen beschäftigen, die, wenn auch zu-
unserer Competenz gehörig,
Allgemeinen reichen. Aus diesem Grunde votire ich auch gegen alle diejenigen‘
Fassungen, wenn ich sie auch nicht bestreite, die eine solche Tragweite haben. So:
habe ich bei dem ersten Passus votirt, und so votire ich auch bei dem zweiten.

doch weiter greifen, als die Geodäsie und Wissenschaft im

Herr Hansen: Es ist mir eigentlich nicht recht einleuchtend, dass dieser Passus
eine besondere Tragweite habe oder eine solche, welche unsere Competenz überschritte.
Wir erklären in unserem letzten Passus, dass aus dem Grunde, weil das Meter
die Aussicht hat, als allgemeines Maass angenommen zu werden, wir auch das-
selbe annehmen wollen. Ich kann darın in der That eine solche Tragweite nicht
finden, welche die Competenz, die uns von unseren Regierungen ertheilt ist,
überschritte.

Präsident Baeyer: Wir können jetzt wohl zur Abstimmung schreiten.

Der Antrag 5 wird in der letzten vorgelesenen Fassung angenommen.

Herr /irsch: Der folgende Punkt lautet nach dem Antrage des Herrn Dove’
und nach dem Vorschlage der Commission:

„Es wird empfohlen, das Metersystem, wo es eingeführt wird, ohne Aende-
rung mit consequenter Durchführung der Decimaltheilung anzunehmen. Die

Einführung des metrischen Fusses ist namentlich zu widerrathen.“
Herr Bruhns: Ich kann diesen Vorschlag nur befürworten. Ich weiss unter
ke

 

 
