MORE

 

IL AR

111

verfahren sei. Nur zu sagen: das Mittel solle angenommen werden, halte ich fiir etwas
willkürlich.

Präsident Baeyer: Wenn Herr Schering das übernehmen wollte, so würden wir
es mit grossem Dank annehmen.

Herr Schering: lehı würde andere Kräfte für geeigneter halten, diese Lösung zu
versuchen und habe selbst nur geglaubt darauf aufmerksam machen zu müssen, dass
eine eingehendere Behandlurg versucht werden möchte.

Herr Hansen: Ich bin der Meinung, dass wenn Dreiecksnetze mit einander
verbunden werden sollen und sich nur ganz kleine Fehler zeigen, dass es dann ganz
in der Ordnung ist, eine Grenze der Näherung zu setzen; da die mittleren Fehler der
Resultate weit grösser sind, als man vielfach geglaubt hat, so wäre es überflüssig, bei
kleinen Differenzen grosse Arbeiten zu machen. Für den Abschluss oder die
Berechnung von Dreiecksnetzen, die so gross sind, dass ıhre Behandlung in
einem Ganzen fast an die Unmöglichkeit streift, habe ich bereits in meinem Buche ein
Verfahren angegeben, welches die Ausgleichung betrifft. Es ist von grosser Wichtig-
keit, dass in einem ausgedehnten Dreiecksnetz viele Grundlinien gemessen werden.
Allerdings wird es dabei etwas schwierig sein, die Summe der Fehlerquadrate in Bezug
auf das ganze Netz zu einem Minimum zu machen, allein wir haben so oft Fälle in
der angewandten Mathematik, wo man mit grösster Strenge nicht ausreicht und aus
einem oder dem andern Grunde von der vollkommensten Strenge etwas abgehen muss.

Herr Herr: Ich wollte nur bemerken, dass ich keinen Anlass finde, gegen den
Bericht zu stimmen, allein ich möchte bemerken, dass ich dem Antrag des Herrn
Schering wohl zustimmen kann, der nicht weiter geht, als der Versammlung zu
empfehlen, dass sie einladet oder darauf aufmerksam macht, ob sich nicht Jemand
fände, sich mit dieser Aufgabe zu beschäftigen und seiner Zeit uns in dieser Be-
ziehung eine Vorlage zu machen oder das Resultat seiner Bemühungen durch den Druck
zu veröffentlichen. Die Versammlung vergiebt dadurch gar nichts, wenn dieser Antrag
angenommen werden sollte; es wird höchstens dem Ersuchen nicht gewillfahrt, und
daraus braucht sich Niemand ein Gewissen zu machen, weil das Ersuchen an Niemand
persönlich gerichtet war. Aber bei wissenschaftlichen Dingen, welche ein eingehendes
Nachdenken über einen Gegenstand erfordern, scheint es mir nothwendig, dass derjenige,
welcher in der Frage etwas zu Tage fördern will, auch die Zeit habe, darüber nachzu-
denken. Aus diesem Grunde begrüsse ich den Antrag des Herrn Schering, der mir
ganz geeignet scheint, ein Verfahren anzubahnen, mit grosser Freude, nämlich das
Verfahren, dass von der Conferenz ausgesprochen werde, nach dieser oder jener Rich-
tung sei es wünschenswerth, dass competente Männer sich mit dieser Aufgabe befassen
und event. die Resultate ihrer Forschungen unseren Arbeiten baldmöglichst zu Gute
kommen zu lassen. Ich glaube, es wird auf diese Weise eine nützliche Anregung
gegeben und vielleicht eine Arbeit gemacht, die sonst nicht gemacht würde.

Herr Jansen: Nach der Ansicht des Herrn Schering soll, soviel ich im Augen-
blick verstehe, von der strengen Erfüllung der Bedingungs-Gleichungen etwas aufgegeben
werden, bei dem von mir erwähnten Verfahren würde dies nicht der Fall sein.

 

 
