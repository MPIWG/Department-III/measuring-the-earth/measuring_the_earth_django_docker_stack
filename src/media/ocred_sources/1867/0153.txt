 

man ART

ul

149

Unterschied der mittleren Meereshöhe in verschiedenen Häfen am Mittelmeer trotzdem
grösser ist, als der Unterschied der mittleren Meereshöhe in den Häfen am Ocean.

Herr Förster: Diese letztere Mittheilung meines Herrn Vorredners erläutert
ganz das, was ich hervorheben wollte. Ich habe nur darauf aufmerksam machen wollen,
dass der formelle Verlauf der Fluctuationen des Niveaus eines Meeres es vorzugsweise
bedingt, ob dasselbe zur expediten Festlegung der mittleren Meeresfläche geeignet ist.
Je einfacher die Periodieität jener Fluctuationen bei beliebiger Grösse derselben ist, je
weniger länger dauernde und erst in längerer unbekannter Periode compensirte Auf-
stauungen zu befürchten sind, desto geeigneter ist irgend ein Meerestheil für unsere
Zwecke. Es ist überdies bekannt, dass bei starken Vibrationen unregelmässige anhal-
tende Spannungszustände weniger zu befürchten sind. Ich habe also nur die Richtung.
in welcher die Experimente anzustellen sind, hervorheben, aber keine besondere Aen-
derung des Entwurfs vorschlagen wollen.

Präsident Baeyer: Ich meine, dass wenn wir Beobachtungen von allen Meeren
gemacht haben werden, man dann später diejenigen aussuchen kann, welche die Bedin-
gungen erfüllen, welche Herr Förster hier erwähnt. !

Herr Dove: Ich möchte noch ein Wort hinzufügen. Wenn ich die Temperatur
Berlins bestimme dadurch, dass ich sie von Stunde zu Stunde beobachte, so bestimme
ich die Temperatur der Zeit, aber nicht die mittlere Temperatur von Berlin. Es ist
die wahre Temperatur der Luft immer eine andere als die mittlere. Wenn das Meer
in Bewegung ist, so muss die Frage erst untersucht werden, ob das Mittel der beobach-
teten Stände der Voraussetzung entspricht, dass das Meer in Ruhe ist. |

Präsident Baeyer: Da Niemand sich weiter zum Wort meldet, lasse ich über
den siebenten Commissionsantrag abstimmen.

Er wird angenommen.

Ich ersuche den Vorsitzenden der siebenten Commission, Herrn v. Forsch, den
Bericht der siebenten Commission erstatten zu lassen.

Herr v. Forsch: Die Herren v. Morozowicz und Schering sind Berichterstatter.

Herr v. Morozowiez: „Da specielle Fragen der Commission nicht vorlagen,

dieselbe nach Punkt 11. des Programms vielmehr nur allgemeine Grund-
sätze zu erörtern hatte, so nahm zunächst Herr Geheim-Rath Hansen das
Wort, um den Mitgliedern der Commission mitzutheilen, dass ein Werk von
ihm über die Methode der kleinsten Quadrate so eben in Druck vollendet
sei, in welchem er die Ergebnisse seiner langjährigen Studien auf dem Felde
der Geodäsie veröffentlicht habe. Ueber den Inhalt dieses Werkes hielt Herr
Hansen der Commission einen ebenso interessanten, als lehrreichen längeren
Vortrag, aus welchem die Commission glaubt, nur diejenigen Momente hier
mittheilen zu müssen, die zur Begründung der der allgemeinen Conferenz
zu unterbreitenden Anträge dienen sollen.
Herr Hansen sagt:
1. In einem trigonometrischen Netze sind die Bedingungs-Gleichungen in
beiden vorkommenden Formen, ob Winkel- oder Seiten-Gleichungen,

 

 
