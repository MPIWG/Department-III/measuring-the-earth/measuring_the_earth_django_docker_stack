 

ad a |

a

messungs-Inspector Kaupert, der früher in Hessen die Aufnahme mit sehr glücklichem
Erfolge geleitet, ist augenblicklich an den Vermessungen in Nassau betheiligt. Wir
hegen daher nach den schon ohne besondere Unterstützung erreichten Resultaten keine
zu sanguinischen Hoffnungen, wenn wir bei der Aussicht auf andere Mittel darauf rech-
nen, bis zum Schlusse dieses Jahrhunderts die Original-Aufnahmen von 6200 Quadrat-
meilen vollenden zu können. Mit Sicherheit lässt sich die Aufgabe aber nur dann lösen,
wenn wir mit Bestimmtheit auf die Vorbereitungen durch die Landes-Triangulation
rechnen können. Sehr günstig ist es nun, dass selbige unmittelbar dem Generalstabe
attachirt ist, und dass sie dadurch die Aufgabe hat übernehmen können, denjenigen
Gang in ihren Arbeiten zu verfolgen, der den topographischen Arbeiten günstig und
welchen der Generalstab auch durch andere Gründe bewogen, ihr vorgezeichnet hat.

Es ist selbstverständlich, dass der Gencralstab bei seinen topographischen Ar-
beiten an den Schöpfungen der geodätischen Arbeiten, wie sie durch die Bemühungen
der verehrten Herren ins Leben gerufen werden, das grösste Interesse hat; es ist aber
ebenso einleuchtend, dass er seiner Aufgabe nach, gar nicht von dem Wege abweichen
darf, mit der Landestriangulation Hand in Hand sein Ziel, welches ihm durch andere
Verhältnisse gesteckt worden ist, so und nicht anders zu verfolgen.

Wenn ich nun in dem Wiener Protocoll gelesen habe, dass auch Seitens der
permanenten Commission primäre Triangulationen in den Preussischen Staaten befür-
wortet sind, so glaubte ich es als eine Schuldigkeit erachten zu müssen, von dem Vor-
haben des Generalstabes Mittheilung zu machen.

Es liegt die schon vorhin erwähnte Denkschrift in diesem Augenblicke den
höchsten Staatsbehörden vor, die über die topographischen Arbeiten des Preussischen
Generalstabes zu entscheiden haben. Mehr über die Einzelnheiten der Denkschrift
zu sagen, bin ich nicht in den Stand gesetzt. Wenn sie aber die höchste Bestätigung
erlangt hat und wir mit allen Kräften arbeiten, ist die Aussicht vorhanden, die topo-
graphischen Aufnahmen in 33 Jahren vollendet zu sehen.

Präsident Baeyer: Ich spreche dem Herrn v. Sydow den Dank der Versamm-
lung aus und glaube, dass wir Alle & meinschaftlich darauf hinwirken werden, dass
recht gute topographische Arbeiten ausgeführt werden können.

Meine Herren, da wir schon von 10 Uhr an Sitzung gehabt, möchte ich, wenn
die Versammlung einverstanden ist, die heutige Sitzung schliessen.

Alle erklären sich einverstanden.

Präsident Baeyer: Ich schliesse die Sitzung und beraume die nächste auf morgen .
12 Uhr an mit der Tagesordnung: Berichte der Commissionen.
Schluss der Sitzung 2 Uhr.

 

 
