Time were.

PRP OPere

peer

mire

Tr

DETENTE

Br

Die permanente Commission erstattet der Conferenz in ihrer ersten Plenar-
sitzung Bericht über ihre Thatigkeit in den verflossenen drei Jahren und über den
Fortschritt der Mitteleuropäischen Gradmessung im Allgemeinen und ersucht die Herren
Mitglieder, über den neuesten Stand der Arbeiten in den einzelnen von ihnen vertretenen
Staaten Mittheilung zu machen.

§ 4.

Die Conferenz hält ihre Plenarsitzungen an den vom Bureau festzusetzenden

Tagen und Stunden.
8.0.

Der Präsident handhabt die Ordnung in den Plenarsitzungen und leitet die
Verhandlungen; er setzt in Uebereinstimmung mit dem Bureau die Tagesordnung für
die Plenarsitzungen der einzelnen Tage fest und verkündet sie bei der Eröffnung der
betreffenden Sitzung,

Sb.

Wenn nach gepflogener Erörterung in der Plenarsitzung Abstimmungen über die
Anträge von Berichterstattern nöthig sein sollten, so erfolgen dieselben durch Aufstehen
und Sitzenbleiben. — In solchen Fällen sind nur die von den hohen Staatsregierungen
ernannten Conferenzmitglieder stimmberechtigt.

Sr 2

Anträge, welche nicht Gegenstände des in der ersten Plenarsitzung beschlossenen
Programms betreffen, auch mit diesem nicht im Zusammenhange stehen, sowie etwaige
schriftliche vor die Conferenz zu bringende Mittheilungen solcher Art sind vorher bei
dem Bureau einzureichen. Dasselbe entscheidet über deren Zulässigkeit in der laufenden
Sitzungsperiode. Bezüglich soleher Anträge und Mittheilungen kann jederzeit der An-
trag auf Uebergang zum Programm für die laufende Sitzungsperiode gestellt werden.

S: 8.

Bei Eröffnung jeder Plenarsitzung der Conferenz bringt das Bureau die in-
zwischen überreichten Vorlagen, welche sich auf die Sache beziehen, zur Kenntniss der
Versammlung. Dergleichen Vorlagen können auf Beschluss der Versammlung wie auch
des Bureaus in dem gedruckten Rechenschaftsberichte mebr oder weniger vollständig
erwähnt oder ganz in denselben aufgenommen werden. Sie sind schliesslich dem Archiv
der Mitteleuropäischen Gradmessung einzuverleiben.

oe

Die Redaction der Verhandlungen der Conferenzen übernimmt die permanente

Commission und sorgt fiir den Druck und die Vertheilung.
Si EO:

Die Wahlen für die ausscheidenden Mitglieder aus der permanenten Commission
werden von dem Bureau in einer der letzten Plenarsitzungen als erster Gegenstand der
Tagesordnung vorgenommen, jedoch ist hierauf in der vorhergehenden Sitzung aufmerk-
sam zu machen.

2

 
