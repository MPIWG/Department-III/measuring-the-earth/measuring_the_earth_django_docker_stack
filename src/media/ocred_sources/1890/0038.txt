 

Es geht aus denselben unter Anderem die erfreuliche Thatsache hervor, dass das
Netz des europäischen Nivellements im Oktober 1889 bereits eine Gesammtlänge von
118 000 km darstellt, und dass als Durchschnitt der letzten sechs Jahre die Jahresleistung
auf ungefähr 6600 km zu schätzen ist. |

Der Bericht des Herrn von Kalmär enthält ausserdem interessante hesultate der in
Oesterreich-Ungarn und in Deutschland über die Lalten-Längen und deren Variabilität ange-
stellten Untersuchungen. Der Berichterstatter spricht den von der Permanenten Commission
getheilten Wunsch aus, dass ihm baldmöglichst auch von den übrigen Ländern die auf diesen
Gegenstand bezüglichen gefundenen Daten mitgetheilt würden.

Herr Hirsch unterstützt diesen Wunsch des Herrn Berichterstalters auf das lebhaf-
teste, da er glaubt, dass die möglichst genaue Ermittlung der Gleichungen sämmtlicher hei
den in Europa ausgeführten Nivellements benutzten Latten, sowie die Kenntniss der Varia-
bilität derselben für die Ausgleichung des europäischen Höhennetzes und für die Anschlüsse
der verschiedenen Landes-Nivellements unentbehrlich ist.

Ausserdem hebt Herr Hirsch den von Herrn von Kalmär hinzugefügten, in dem
antographischen Druck nicht enthaltenen Passus hervor, wonach trotz der von Herrn Kalmär
ausgesprochenen Meinung, dass die Europa umspülenden Meere ein und derselben Niveau-
Kläche angehören, der Beschluss der Generalconferenz, bezüglich des europäischen Niveile-
mentsnullpunktes aufrecht erhalten bleibt, welcher das Gentralbureau beauftragt, diese
Frage zu studiren und in der nächstjährigen Versammlung der Permanenten (ommission
hierüber Bericht zu erstatten.

Ohne in die Disenssion der Frage selbst eintreten zu wollen, welche passender der
nächstjährigen Versammlung vorbehalten bleibt, hält Herr Hirsch diese Reserve für um so
mehr geboten, als es sonst den Anschein gewinnen könnte, dass die von Herrn Lallemand
in seiner Mittheilung ausgesprochenen, zunächst rein persönlichen Ansichten, von der Nutz-
losiekeit eines gemeinsamen Nullpunktes sowie von der Unmöglichkeit einer allgemeinen
Ausgleichung der europäischen Netze, von der Permanenten Commission ohne Weiteres
getheilt würden.

Auf die Frage des Heren Bassot, in welchem Jahre die Discussion und die definitive
Beschlussnahme über diesen Gegenstand zu erwarten stehe, erwidert der Sekretär, dass nach
den in Paris ausgesprochenen Absichten und nach der jetzigen Sachlage zu urtheilen, der
Bericht des Central-Bureau’s wahrscheinlich im nächsten Jahre der Permanenten Commission
vorgelegt werden wird, so dass diese ihre Vorschläge der nächsten Generalconferenz unter-
breiten und letztere ihre endgültigen Beschlüsse im Jahre 1892 fassen kann.

Der Herr Präsident dankt Herrn von Kalmar für seinen interessanten Bericht, und
ersucht die Finanz-Commission, wenn sie bereit ist, ihren Bericht zu erstatten.

Herr Prof. Feerster, welcher zum Berichterstatter dieser Commission ernannt ist,
erklärt, da die Permanente Commission wahrscheinlich noch einige Beschlüsse fassen dürfte,
welehe vielleicht Ausgaben mit sich bringen werden, die Finanz-Commission sieh heute in

 

a ra} HB HAAR LAU 1 (ATI

lo nik I ı

I nt ihn

ii ss umall bb a lbs Äh a in

 
