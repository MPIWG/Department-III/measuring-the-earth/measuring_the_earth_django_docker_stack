 

|
!

hu TER Teen ir

Lau

UI AM AN N|

INN AM

RT TRTRERE TOT RRINAH ER RR HR LLUL

&E

nung zu Gebote, auch das Personale war zahlreicher als im Vorjahre und schon wesentlich
besser instruirt. Einigen der griechischen Offieiere konnte ich theils bald nach Beginn, theils
im weiteren Verlaufe der Feldarbeit Theodolite anvertrauen und selbständige Arbeiten über-
tragen, welche von diesen Herren mit vorzüglichem Erfolge durchgeführt wurden. Aus dem
Skelette auf der Beilage ist ersichtlich, welche Beobachtungsstationen in Attika und im Pelo-
ponnes im Laufe des Jahres 1890 erledigt wurden.

Um für die Berechnung der geographischen Positionen der Dreieckspunkte die erfor-
derlichen Ausgangsdaten zu bekommen, hatte ich mir von Wien ein Universal-Instrument
(Eigenthum der österreichischen Gradmessung) und ein Chronometer (Johannsen, Eigenthum
des k. und k. Mililär-Geographischen Institutes) leihweise verschafft und bestimmte damit
die Polhöhe eines eigens zu diesem Zwecke auf dem Nymphen-Hügel bei Athen errichteten
Marmor-Pfeilers und das Azimuth der Richtung Parnes. In der Zeit vom 12. Juni bis 1. Juli
1890 beobachtete ich 9 Sätze zu 12 Einstellungen Zenith-Distanzen des Polaris und erbielt
als Mittel aus diesen 252 Bestimmungen :

o = 37 58 20,54

Leider war ich während der oberwähnten Zeit und auch später anderwärlig so viel-
[ach in Anspruch genommen, dass ich nicht dazu kam, diese Breitenbestimmung durch
Beobachtungen von Sternen zu ergänzen, welche südlich vom Zenith des Standpunktes eul-
minieren und so musste ich mich vorläufig darauf beschränken, die Biegung des Fernrohres
direet (durch Gollimierung mit zwei Theodoliten) zu bestimmen. Es ergab sich für die Biegung
im Horizonte :

und damit die corrigierle Breite :

an

Das Azimuth der Seite Nymphen-Hügel-Parnes fand ich aus achtzehn Sätzen (je vier
Einstellungen Polaris und ebensoviele des terrestrischen Objectes)

0° 13° 47.12 von Nord gegen West gezählt.

Wien, 31. Dezember 18%.
HARTL,

k. und k. Oberstlieutenant.

 
