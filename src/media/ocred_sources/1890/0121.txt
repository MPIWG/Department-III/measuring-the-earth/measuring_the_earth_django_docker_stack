 

|
I

 

IUTANIE I OAM

ET TRTFRRORRTTFARRTERRRT N TLFTTERRTTTPRRNT UI DU

 

Annexe B. II.

FRANGE

Rapport sur le projet d’une mesure nouvelle de l’are du Perou.
pI

Les mesures de degr6 ex&cutdes au sieele dernier ne comptent plus aujourd’hui pour
Petude de la Terre, et ne figurent pas dans les calculs les plus recents, ceux du colonel
Clarke. Une exception est faite cependant pour l’are du Perou, mesure vers 1745, par les
Academieciens francais Bouguer et La Condamine, avec la collaboration d’officiers espagnols
d’un haut merite. Le fait est que celte mesure satisfait parfaitement aux &l&ments de Vellip-
soide terrestre de Clarke.

Gependant il faut reconnailre que l’etendue de cet arc important est un peu Lrop
faible et que les procedes ou instruments du xvın® siecle sont bien au-dessous de ceux qu’on
yappliquerait aujourd’hui. (est pourquoi le Bureau des longitudes, assur& du bon vouloir
de P’öminent President de la Röpublique de l’Equateur, M. Antonio Flores, a mis ä ’etude
il va deux ou trois ans la question de la revision de cet are, el a soumis ses vues ä ce sujel
a ’Acadömie des sciences (historiquement interessee dans la question) el au Gouvernemenl
francais. Nos collegues se souviennent que, dans la derniere reunion generale de l’Association,
iA Paris, le dölegud des Ktats-Unis a adress& publiquement A la France, sous la forme la plus
courtoise, une invitation de proceder le plus töt possible A la revision de cet arc. Je, suis
autorise A declarer A l’Associalion que notre Gouvernement n’a pas perdu de vue cette allaire,
Elle a meme &t& trait&e en conseil des ministres il ya quelques mois, et son ex&culion a ee
confiece aux trois ministres les plus directement interesses, ceux de I’Instruction publique,
de la Guerre et de la Marine, qui ont sous les yeux les propositions et avant-projets du Bureau
des longitudes. Il ya done lieu d’esperer qu’elle recevra bientöt une solution salisfaisante.

RAYE.

 
