 

170

La diminution absolue a&—ß de l’amplitude, au bout d’une oscillation entiere, est due
a deux causes : 1° A la resistance du fluide dans lequel se meut le pendule ; 2° au froltemen!
qui s’exerce entre le couleau et le plan de suspension.

Ba lor de CoulembAa = Ba SS

exprime le double effet de la resistance du fluide consider&e successivement comme propor-
tionnelle A la premiere et a la seconde puissance de la vitesse.

Examinons l’effet du frottement :

Soit f le eoefficient du frottement de roulement qui convient au couteau el au plan
de suspension,

?.5 le rayon de courbure moyen du couteau entre les points A et B,

TB

er oda

2

 

h la distance du centre de gravile au point de suspension ;

l’effet du frottement sera, pour une oscillation enti£re :

2 Bi KEN 9 f ” & »
v h 3
a l’oscıllation suivante.
L h

La diminution d’amplitude due au frottement ne sera donc pas conslante, comm‘
on l’admet generalement, A moins que la section de l’aräte n’ail un rayon de courbure
constant, c’est-A-dire ne soit circulaire.

Le decroissement di au frottement de la demi-amplitude « A un instant queleonque
est done donne par la formule :

 

o, reprösentant le rayon de courbure moyen de la courbe de l’ar&te du couteau entre
les points extrömes de contact de cette courbe sur le plan de suspension, ä l’amplitude

consideree :

aa oda
vo,

 

il 1

ara A IE MARITA AL 1

Ik)

Il:

ln hı

 

 
