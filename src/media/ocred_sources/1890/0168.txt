 

 

162

L’azimut de la direetion Portalban-Neuchätel a ete trouve

2 N" ”

A — 359 59 54,99 + 0,45

tandis que la valeur r&ciproque de la direction Neuchätel-Portalban, r&sultant de Pazimut de
la mire me£ridienne de l’Observatoire placee dans cet endroit, r@duite au pilier d’obser-
vation, est :

Ak — 359 59 56,96 + 0,17

. ee ö 5 „
ce qui donne pour la deviation en longilude — 2,81.
On a mesure egalement & nouveau l’azimut de Chaumont-Neuchätel el on a trouve

W

1800615 + 0,60,

ce qui s’accorde avec la valeur inverse Neuchätel-Chaumont

" n

0002 Evo

Les travaux de caleul, abstraction faite des r&äductions des observations de 1889, ont
eu pour objet d’abord de terminer la jonction des trois bases au r&seau principal; ensuite on
a calcul& les cötes des triangles de premier ordre et de ceux des jonctions des bases, et on a
etabli les coordonnees g&ographiques des sommets du reseau, d’apres la methode spheroidale
indirecte de Gauss. Les resultats de ces travaux sont actuellement sous presse et forment,
avec les observations astronomiques dans le röseau de la base du Tessin, au Gaehris et au
Simplon, le Tome V des publications de la Commission geodesique suisse !.

Lacombinaison des coordonnees astronomiques et g&odesiques pour les quatre slalions
pres du meridien de ’Observatoire de Neuchätel donne, pour cette region, des d&viations de la
verticale assez fortes dans le sens de l’attraction exerc&ee par les montagnes du Jura; car en
prenant Berne pour point de depart, on constate pour

Portalban une deviation vers le Nordde . — 95,50
Neueckalelere 2 u susnaraı, 2,10, WON SV 2245,09
Chaumantah Hu SuolineTaren 2, Ne ir
rote-desamt au U 10199 IB DORIS BI ET

. . RR IR . . ‚ H . A
chiffres qui doivent etre diminues de 4 si l’on admet avec M. Helmert pour Berne me&me
une deviation vers le Sud de cette valeur. Ges fortes deviations de la verticale sont d’autant
plus remarquables que l’action du massif du Jura, renforcee il est vrai, pour trois des stalions,

' Ce volume et les tirages a part viennent de paraitre.

 

HAEL.,

ya van Ta AT U MA 1

ll

kind ı

ll

 

 
