 

|
|

AM AU

NUM STERT

mm

SIR TRITARRE TURNIER m N!

39

 

der Lage befindet, zunächst nur den ersten Theil ihres Berichtes, welcher sich auf die
technungsablage vom vergangenen Jahre bezieht, zu erstatten.
Dieser Bericht lautet folgendermassen :

« Die Finanz-Commission hat die vorgelegten Rechnungen des Ilerrn Directors des
Central-Burean’s für das Jahr 1889 geprüft. Dieselbe hat die Ausgaben in Ordnung und
durch Belege gerechtfertigt gefunden; sie hat ehahalll Kenntniss von dem Einnahmen-Gonto
und von dem Stande der augenblicklich verfügbaren Fonds genommen.

« Die Commission schlägt daher vor:

« Die Rechnungen der Permanenten Commission für das Jahr 1889 zu billigen und
dem Herrn Director des Gentral-Bureau’s für seine finanzielle Verwaltung volle Decharge zu
ertheilen.

« Freiburg, 19. September 1890.

«Der Präsrident : Der Berichterstatter :
«tl MAYR. F(ERSTER. »

Da Niemand betreff dieses Berichtes das Wort verlangt, bringt der Präsident den-
selben zur Abstimmung. Derselbe wird von sämmtlichen Mötgliedern der Permanenten Com-
mission genehmigt.

Der Herr Präsident geht alsdann zu den Berichten der einzelnen Länder über, und
ertheilt, der alphabetischen Ordnung folgend, zuerst dem Herrn Oberst IHennequin für Belgien
das Wort.

Herr Henneguin berichtet zunächst über einige astronomische Bestimmungen von
jreite und Azimut, welche in Hamipre, Lommel und Nieuport gemacht worden sind..

Betreff der Nivellements gibt er Aufschluss über die Anschlüsse an Frankreich und
die Niederlande, welche im Jahre 1889 ausgeführt und auf das mittlere Meeresniveau in
Östende, wie dasselbe aus den Aufzeichnungen des Mareographen von 1878 bis 1885 her-
vorgeht, bezogen sind. Es geht daraus hervor, dass das Meeresniveau ın Ostende um 0'170
unter dem Nullpunkt von Marseille und um 0162 unter dem mittleren Meeresniveau in
Amsterdam zu liegen scheint. Der Anschluss an das deutsche Netz ist in diesem Jahre begon-
nen und wird wahrscheinlich im nächsten Jahre zu Ende geführt werden. (Siehe Beilage B. 1.)

Hierauf verliest Herr Oberst von Zachariae einen kurzen Bericht über die geodäti-
schen Arbeiten in Danemark während des Jahres 1800. Die Gradmessungs-Triangulation ist
längst beendet ; einige Revisionsarbeiten zur Erhaltung der älteren Stationen sind in Jütland
ausgeführt und werden nächstes Jahr auf den Inseln ın Aneriff genommen. Zur Vervollstän-
digung der früheren bereits publieirten werden noch einige Breitenbestimmungen längs des
Meridians von Skagen ausgeführt. An Nivellementslinien sind im laufenden Jahre 150 km
vollendet und für weitere 150 km Linien 72 Fixpunkte unterirdisch angebracht worden.
(Siehe Beilage B. II.)

VERHANDLUNGEN — 5

 
