  

 

f

 

inkl

Man

A|

al

Mm) 1 BARRIERE ILL ARINgIN ML n pi

centimetres prös. Les operations ulterieures ne feront probablement que confirmer cette
hypothese en la generalisant.

Cependant, une objection se presente naturellement A l’esprit. La d&pression primi-
livement constatde entre la Mediterrande et ’Ocean paraissait justifide d’une maniere salis-
(aisante par la salure plus forte et la densit& plus grande (1,029 au lieu de 1,027) de l’eau
du bassin mediterraneen ; le seuil de Gibraltar ayant 400 mötres environ de profondeur, le
niveau de la Möditerrande devrait, d’apres le principe des vases communicants renfermant
des liquides d’inegales densites, se trouver de 080 A 1 metre au-dessous de celui de P’Atlan-
tique. On se demandera comment aujourd’hui le niveau des deux nappes peut etre le m&me.
ä quelques centimetres pres.

La meme objection sera faite contre l’quilibre de la Baltique et de la mer du Nord.
L’eau de la premiere est presque douce (densite 1,005), tandis que celle de la mer du Nord
possede la densitö normale (1,027) de Veau oc£anique. Les deux mers communiquant
ensemble par plusieurs detroits, dont le plus profond, le Grand Belt, presente des fonds de
18 metres au minimum, un caleul trös simple montre qu’il devrait exister entre les deux
nappes une dönivellation de 040 en moyenne. Or, les op£rations tres soigndes failes en
Prusse par Institut göodesique et par le Service topographique militaire, accusent tr&s
sensiblement le m&me niveau pour les deux mers. Comment les deux choses peuvent-elles se
concilier ?

A ces critiques on peul r&epondre que les anomalies en question ne sont probable-
ment qu’apparentes. L’egalitö constatde du niveau semble annoncer que les irrögularites dans
la salure et dans la densite de l’eau des mers, comme aussi les couranis, portent seulement
sur une mince tranche superficielle, suivant l’opinion &mise par M. Pamiral italien Magnaghi
au dernier Congres geographique international. Il ya la un fait qu’il serait interessant de
verifier par des recherches directes.

Quoi qu’il en soit, l’ancienne hypothöse de Y’uniformit6 du niveau des mers, primili-
vement admise d’apres les lois de la möcanique des Nuides, puis abandonnee sur la foı de
mesures inexactes, parait en voie de se r&habiliter, au moins dans ’ensemble et abstraction
faite peut-&tre de quelques anomalies locales.

Des lors, il est probable que, la Hollande et l’Allemagne mises A part, les carts, si
r&ellement il en existe, entre les z&ros d’altitudes des divers pays, sont de l’ordre m&me des
erreurs des nivellements; par suite, sauf dans les zones frontieres, ces &carls ne peuvent
fausser sensiblement la comparaison des altitudes de deux points 6loignes appartenant ä des
reseaux differents.

Autrement dit, si l’Allemagne et la llollande abaissaient spontandment de 15 & 20
centimetres leurs z&ros pour les ramener A la hauteur du niveau moyen de la mer du Nord,
P’unifieation des altitudes se trouverait ipso facto re&alisce sur toute l’Europe, dans la mesure
oüı elle est utile pour les besoins de la pratique. |

Pour rendre tout A fait comparables les altitudes des points situ6s, de part et d’autre,

au voisinage des limites des differents r&seaux, il ne servirait d’ailleurs A rien d’adopter un
ANNEXES — 24

 
