 

i
F
|
|
|

m AR UERMARIBLL AU

EÜRERINm Um Liam pm

im

PREMIERE SEANCE

Lundi 15 septembre 1890.

La sdance est ouverte & 2 heures 40 minutes.
Sont presents:
I. Les membres de la Commission permanente :

10 S. E. M. le general Marquis de Mulhacen, de Madrid, President.

90 M. le professeur Ad. Hirsch, Direeteur de l’Observatoire de Neuchätel, Secrelaire
perpetuel. Ä

30 M. le professeur R. Helmert, Directeur de Institut geodesique de Prusse et
Directeur du Bureau central de l Association geodesique internationale, aABerlın.

4° M. H.-G. van de Sande-Bakhuyzen, Directeur de l’Öbservatoire de Leyde.

50 M. Faye, membre de Institut et President du Bureau des Longitudes, a Paris.

6° M. le göneral A. Ferrero, Directeur de UInstitut geographique militaire de Florence.

7° M. le professeur W. Foerster, Directeur de ’Observatoire de Berlin.

8° M. le capitaine de vaisseau von Kalmdr, Directeur des triangulations & UInstitut

l.-R. geographique militaire de Vienne.
90 M. le colonel von Zacharide, Directeur du Service g6odösique, A Aarhus.

II. Les delegues :

1° M.le professeur Albrecht, chef de seetion & "Institut gcodesique royal, A Berlin.

90 M. Antonio Jose d’Avila, major A l’Etat-major, pair du royaume de Portugal, A
Lisbonne.

30 M. le commandanı Bassot, chef de la section geodesique du Service geographi-
que de l’Armee, ä Paris.

4° M. Bouquet de la Grye, membre de V’Institut, ä Paris.

5° M. Constantin Garusso, A Athenes.

6° M. le commandant @. Defforges, chef de bataillon, du Service geographique de

l’armee, a Paris.
7° M. Haid, professeur A !’Ecole polytechnique de Karlsruhe.

 
