 

|

vn

5 me mTRAeTIIRTeg Fre

VW ERI URAN Ih |

f

iE
IE
um
im

®
um
\®
i-
iB

13

auf die mehr oder minder grosse Leistungsfähigkeit der angewandten Instrumente zurück-
zuführen —, haben sich diejenigen in Strassburg in so hohem Grade von einer erst nach-
träglich erkannten Fehlerurs sache! beeinflusst gezeigt, dass aus ihnen eine sichere Schluss-
folgerung in Betreff der in Rede stehenden Frage nicht gezogen werden kann. Aus diesem
Grunde ist im Folgenden nur der Resultate auf den Stationen Berlin, Potsdam und Prag Er-
wähnung gethan.

Die Schlussfehler bei Wiederbeohachtung derselben Sterngruppe nach Ablauf von
einem Jahre ergaben sich für die einzelnen Stationen :

Berlin. Potsdam. Prag.
Sterngeruppe | 0% —_ _
) il Al —_ (2 — 018
> 1 —_ os 05
» IV oo on 209
» V — ı\ı.
und betragen daher im Mittel für Berlin, Potsdam und Prag bezw. — 0,51, — 028 und

0.

Diese Differenzen sind erheblich grösser als nach Maassgabe der Genauigkeit der Kin-
zelbeobachtungen zu erwarten war. Die nalıe ( Gleichheit der Mittelwerthe auf allen drei Sta-
tionen deutet indess darauf hin, dass diese Unterschiede nicht ausschliesslich auf Beobach-
tungsfehler zurückzuführen sind, sondern dass nehen diesen noch eine anderweilige allen
drei Stationen gemeinsame Pehlerursache mitwirken muss. Als solche glaubt Ilerr Dr. Küstner

die Unrichtigkeit der angewandten Aberrationsconstante® ansehen zu müssen. In der That

würde sich durch Annahme einer um 0.07 grösseren Aberrationsconstante der mittlere
Sehlussfehler zum Verschwinden bringen lassen und eine solehe Annahme erscheint schon
aus dem Grunde sehr glaubhaft, weil auch die Beobachtungen von Nyren auf die Nothwen-
diekeit einer Vergrösserung der Strune e’schen Aberrationsconstante hinweisen.

Die Ausgleichung dieser Schlussfehler ergiebl die nachstehenden Verbesserungen der
aus den unmittelbaren Beobachtungen hervorgegangenen Reductionen je zweier aufeinander

folgender Sterngruppen :

Berlin. Potsaam. Prag.

Gruppe Il - 0,08 24007 _
»o 2 he kehils 0,00: --.008
» IV.I1] 08 0,00 00

' Ungenügendes Functioniren des Niveaus (vergl. Astronomische Viert eljahrsschrift, 25. Jahrgang,

S. 160).
2 Bei Berechnung der scheinbaren Oerter der Sterne wurde von den Constanten für die Sterntage
im Berliner Astronomischen Jahrbuch Gebrauch gemacht, denen die Struve’sche Aberrationsconstante zu

Grunde liegt.

 
