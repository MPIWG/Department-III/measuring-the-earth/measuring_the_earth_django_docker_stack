 

EN Ar ma Se

a

NINA

la

TIRFRLL FREI LLIE

  
 

=
=
m
m
u
i:

TROISIEMR SEANCE

Vendredi 19 septembre 1890.

La seance est ouverte A 2 !/, heures.

Präsident : M. le general Marquis de Mulhacen.

oO
Sont prösenls !

I. Les membres de la Commission permanente: MM. Bakhuyzen, Faye, Forster,
Helmert, Hirsch, von Kalmar et von Zachariae.

II. Les delögues: MM. Albrecht, d’Avila. Bassot, Bouquet de la Grye, Garusso,
Defforges, Haid, Hennequin, Lallemand, Morsbach, Nell, Rümker, Schols ei Tisserand.

III. Les invitöes: MM. Lüroth, Jordan, Koch, Meyer, Schneyder, Steinmann et Stie-
kelberger.

M. le President donne la parole au Secretaire pour lire le proces-verbal' de la
deuxieıne sdance. (ie proces-verbal, rösum& ensuite en langue francaise, est adopte A ’una-
nimite apres une courte observation de M. Lallemand, d’apres laquelle la notice qu'il a Iue
dans la derniere s6ance n’est pas la m&me que celle qui a paru dans la « Revue scientifique »
sous le titre : Le niveau des mers en Europe et Vunification des altitudes, et qui a eie dis-
tribuee aux membres de l’Assemblee.

M. Defjorges, ä propos du proces-verbal et du rapport de M. Helmert sur la pesan-
teur, demande ä la Conference la permission d’insister sur la concordance des rösullats
obtenus separ&ment, par M. de Sterneck, dans le Tyrol, par M. Erasmus Preston, du Coast
Survey, aux iles Hawai, et par lui-möme dans les Alpes maritimes.

A peu pres A la me&me. &poque, ces trois observateurs, avec des instruments diffe-
renis et des methodes differentes, ont abord& le meme probleme en Autriche, en Oceanie el
en France. Ils ont obtenu un resultat identique. 1ls ont tous trois constale que la formule
de Bouguer, en y introduisant la densite geologique moyenne de la montagne, dans les
rögions olı ils ont observe, represenle d’une maniere assez satisfaisante la variation de la
gravit6 avec Valtitude.

 
