 

172

 

oraduces du vide A la pression d’une atmosphere, nous avons obtenu, avec les pendules de
Brunner du Service g&ographique, des rösultats inatiendus qui nous ont paru dignes d’etre
communiqu6s a l’Associalion.

Le calcul assez long, dont nous venons d’esquisser la marche, a ÖL6 exdeute par
approximalions successives. Nous allons le resumer brievement pour en donner une idee
aussi pr&cise que possible.

Nous prendrons comme unites la minute sexag&simale d’are, la minute de temps et
le millimetre de mercure.

A la station de Rosendael (1888), on a observ£& les d&croisseinents suivants.. (Pendule
ione de brunner, PL. D.)

Pression Aa B Amplitude initiale
e en en & r
210 mm. 334 en 420 min. 90.3
008» 10,5 en 164 » 1 5,3
766 » 177 en 2 96,4

Ges observations ont 6te faites A dessein A de faıbles amplitudes pour atlenuer aulant
que possible l’effet du rayon de courbure; nos premieres öludes ä Breteuil nous ayant montre
que cel effet, pour nos couleaux, est presque nögligeable aux faibles amplitudes.

Posant

da r
en h

a h

c’est-A-dire negligeant pour une premiere approximation le premier et le troisieme terme de
la formule complete, on a:

 

 

On trouve aınsi :

2. log e = 0.00226 & la pression 210mm,
>» 10.002977 ) 389
»: _ 0800 » 766

(es nombres croissent comme la racine carr6e de la pression correspondante.

(est un premier resullat de l’experience; dans la loi du d&eroissement de ’amplitude,
le coefficient du deuxieme terme, celui qui exprime leflet de la resistance de l’air consideree
comme proportionnelle ä la vitesse, est lui-möme proportionnel A la racıne carrdce de la
pression.

all

art aD HR MT 1. 11

ak nik)

a

Jamaica bannen ua m

kakı

 

 
