 

F

u

IN EM aa

all

ii am lei Aeinliı

    

m
=
se

97

 

M. le President met ensuite aux voix les propositions de la Commission speciale ;
elles sont adoptees ü Uunanimite par la Commission permanente.

M. le professeur Helmert soumel le programme suivant pour Vactlivit& du Bureau
central pendant l’exereice 1890-1891 :

1° Etude de la variabilite des latitudes. — Communication aux membres de la Com-
mission permanente des observalions exdeuldes jusqu’äa ce jour. — Üontinuation de ces
recherches avec le concours des observatoires, suivant la deeision de la Commission per-
manenle.

90 Question du niveau fondamentul commun pour les allitudes de ’ Europe. — Grou-
pement de toutes les donndes qui s’y rapportent et @laboration d’un rapport pour la pro-
chaine session de la Commission permanente. _

30 Arc du parallele moyen. — Caleul des lignes geodesiques. — Publication.

Üe programme est ralifie sans aulre discussion.

Apres avoir ainsi liquide les objets ä l’ordre du jour, M. le professeur Hirsch obtient
la parole pour proposer ä la Commission permanente de se prononcer sur Ja declaration
suivanle !

« La Commission permanente de U’ Association geodesique internationale, &tanl infor-
mee'par plusieurs de ses membres qu’ils ont &t& consultes dernierement par leurs gouverne-
ments sur la valeur du meridien de Jerusalem comme meridien initial, et sur Putilite de sou-
mettre celle question et celle de U’heure universelle A une nouvelle Conference speciale, &
convoquer prochainement !

« Döclare qu’il n’existe aucune raison de changer les r&solulions prises sur ce sujet
en 1883 par la Conförence g&odesique internationale de Rome, dont la prineipale, qui recom-
mandait le meridien de Greenwich pour ımeridien initial, a el& ratifide par la Ires grande
majoritd des Etats reprösentes dans la Conference diplomatique de Washington en 1884.

Cette declaration ayant te döveloppde par P’auteur de la proposition, puis soumise
ii une discussion, elle est adoptde par la Commission permanente & la majorit& de 7 voix
Core >.

M. le President vappelle ä la Commission permanente qu’elle a encore & designer le
lieu dans lequel elle liendva sa session de ’annee prochaine.

M. le general Ferrero propose Florence; il croit pouvoir assurer que tout serait fait
pour recevoir dignement la r&union de ’Association g&odesique.

”

Le nom de Bruxelles ayant ee egalement prononc6, M. le colonel Hennequin declare
que la Belgique, qui a d6jä recu une fois Ja Commission permanente, prefererait que Bruxelles
füt choisi celte fois comme siege de la Gonference generale de 1892.

PROCES-VERBAUX — 13

 
