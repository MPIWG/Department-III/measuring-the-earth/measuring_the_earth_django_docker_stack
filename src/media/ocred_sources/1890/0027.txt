 

VE ee eu

i NR TINTE TASFeHITN \

Kin

In]

LI 1a

1

1:
IE
m
im
=
um
=
im

24
Der Prisident ersucht Herrn Prof. Albrecht, einige Erläuterungen betreff des ım
Sitzungssaale aufgestellten neuen Universal-Transits von Repsold zu geben.

Herr Albrecht hebt die eigenartige Verbindung des Transitinstrumentes mit dem
Theodolith hervor, und bespricht insbesondere die Einrichtung zur Registrirung der Faden-
durchgänge, welche von Dr. Repsold in den Astronomischen Nachrichten in Vorschlag gebracht
und an diesem Instrument zum ersten Male praktisch ausgeführt worden ist. Das Beobach-
tungsverfahren beruht im wesentlichen darauf, dass der Stern mit dem beweglichen Faden
verfolgt und in beständiger Biseetion mit demselben gehalten wird, und dass sich durch seit-
lich angebrachte CGontacte jedes volle Zehntel der Schraubenumdrehung selbst registrirt.

Die Prüfung dieses Beobachtungsverfahrens hat in betrefl der Sicherheit, mit welcher
inan dem Stern mit dem Faden folgen kann, das Resultat ergeben, (dass die mittleren Fehler
der einzelnen Registrirungen von derselben Ordnung sind, wie die mittleren Fehler der
Durchgangsbeobachtungen bei Anwendung der gewöhnlichen Registrirmethode. Da aber bei
lem neuen Verfahren die Contacte in viel engerem Zeitintervall auf einander folgen, als dies
bei der Registrirung an festen Fäden ausführbar ist (am vorgeführten Instrument entspricht
eine Schraubenumdrehung einer Fortbewegung des Sternes im Aequalor 58), so wird
während eines gleichen Zeitraums ein viel umfassenderes Beobachtungsmaterial erhalten.

Von ganz besonderem Interesse sind die Resultate, welche die Prüfung dieses Beoh-
achtungsverfahrens hinsichtlich des absoluten Betrages der persönlichen Gleichungen ergeben
hat. Nach vorgelegten Beobachtungsreihen sind die persönlichen Gleichungen innerhalb sechs
Beobachtungscombinalionen auf ganz winimale Beträge (höchstens einige hundertstel Secun-
len) herabgegangen, während die gleichzeitige Bestimmung der persönlichen Gleichung
innerhalb derselben Beobachtercombinationen unter Anwendung des gewöhnlichen Regis-
trirverfahrens Beträge bis zu 0.32 ergeben hatte.

Voraussichtlich wird eine Herabminderung der persönlichen Gleichung um deratige
Beträge auch mit einer erheblich grösseren Gonstanz der Werthe Hand in Hand gehen, unıl
as steht daher zu erwarten, dass die Anwendung des neuen Beobachtungsverfahrens einen
wesentlichen Fortschritt in der Sicherheit der astronomischen Zeitbestimmung und somit
auch der Längenbestimmungen herbeiführen wird.

Der Präsident fragt, ob von einigen Mitgliedern noch weitere Auskunft über be-
stimmte Punkte gewünscht werde. ;

Herr Hirsch sieht in dem von den Ierren Repsold bei diesem Instrumente verwer-
theten Gedanken die physiologische Registrirung durch eine mechanisch electrische Registri-
rung zu ersetzen, einen für die Zukunft viel versprechenden Fortschritt. Er interessirt sich
um so mehr für das Gelingen des automatischen Registrirungs-Verfahrens, als er bereits im
Jahre 1866 ein solches bei den Versuchen benutzt hal, welche er damals mit künstlichen
Sternen angestellt hal, um die sogenannte absolute persönliche Gleichung, oder mit anderen
Worten die physiologische Zeit bei den Durchgangs-Beobachtungen zu bestimmen. Offenbar
ist es für die astronomische und speciell für die Längen-Beobachtungen von grosser Bedeu-
tung, auf diese Weise den numerischen Betrag der persönlichen Gleichung so bedeuten(d

 
