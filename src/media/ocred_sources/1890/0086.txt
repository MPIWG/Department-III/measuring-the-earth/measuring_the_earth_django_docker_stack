 

 

 

 

Ss0
Cette coneordance dans les rösultats de recherches poursuivies d’une fagon indepen-
dante permet peut-&tre d’inferer que la loı est gencrale et que les details du relief dans les
massifs montagneux sont pour peu de chose dans les anomalies de la pesanteur, les causes
de ces anomalies devant &tre recherch6es plutöt dans des variations de la densit& au-dessous
el au voisinage de la surface de niveau zero.

M. le professeur Helmert a la parole pour proposer ä ’Assembl&e, conformement a
son rapport sur les mesures de pesanteur, lu dans la derniere seance, la resolution sul-
vanle.

«La Commission permanente, en reconnaissant Pimportance des observations de
pendule de M. de Sterneck en Tyrol, exprime le desir que ces observalions soient continuees
vers le Sud jusqu’a Padoue, et vers le Nord jusqu'ä Munich, et autant que possible par
M. de Sterneck lui-m&me et avec ses instruments. »

M. Hirsch croit que la proposition de M. Helmert se justifie d’elle-m&eme au point
de vue scientifique. Pour l’appuyer, il se borne ä faire observer qu’une pareille initiative de
la Commission permanente, par laquelle on faciliterait la continuation dans les pays voisins
le certains travaux g&odösiques importants commenees dans Fun des Etats de l’Association,
est parfaitement conforme ä Pesprit et & la lettre de la Gonvention geodesique.

La proposition de M. Helmert est ensuile adoptee & l’unanimite.

M. le President, A qui on a demande de divers cötes de fixer la date et l’'heure
de la prochaine seance au debut de celle-ci, desire consulter ’Assemblee sur les deux
propositions qui lui sont parvenues, savoir s’il faul siöger dimanche ä partir de 1 heure,
ou bien s’il est pröförable de renvoyer cetle scance au lundi ä 10 heures. Il met aux voix
cette alternative, sur laquelle tous les membres presents & l’Assemblee ont le droit d’Emettre
leur vote; 11 voix s’6tant prononcees en faveur de la seance du dimanche et 10 voix pour celle
de lundi, M. le President fixe la prochaine seance au dimanche 21 septembre, ä 1 heure.
Il croit, sans en @tre completement sür, que tous les objets encore A l’ordre du jour pour-
ront &tre liquides dans cette söance, qui serait ainsi la derniere de la session. Il reserve
cependant ce point ä l’Assemblee elle-meme qui en deeidera suivant les cırconstances.

M. le President donne la parole A M. von Kalmär pour lire son rapport special sur
les nivellements de pröcision ex6cutes en Europe pendant Pannce derniere. Ce rapport sera
publie in-extenso comme Annexe aux Comptes-rendus. (Voir Annexe A.)

Il en rösulte entre autres le fait röjouissant que le reseau des nivellements de l’Eu-
rope avait deja atteint, en octobre 1889, un döveloppement de 118000 km. et quen
moyenne des dernieres anndes P’accroissemen! annuel peut &tre övalud & 6.600 km. environ.

Le rapport de M. von Kalmär contient en outre d’interessants rösultats coneernant
les recherches faites en Autriche-Hongrie et en Allemagne sur la longueur des mires et leur
variabilit&. Le rapporteur exprime le veu, partage par la Commission permanente, que les
donndes qui existent sur ce m&me sujet dans d’autres pays lui soient communiquöes le plus
töt possible.

sun DT ORTE TE 1. LU

Uli

anni) Il

ih Dana

mh hen

 

 
