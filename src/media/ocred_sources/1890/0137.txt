 

|
'

N Dr TAT 9* Free

AM

IF aM

TERTIARFR 11 FARIMAHT EINE AR Li

7

im

Annexe B. VI.

PAYS-BAS

Les operations pour la triangulation des Pays-Bas ont Et& poursuivies cette annede
par deux brigades d’ingönieurs. Une de ces brigades a et chargee des reconnaissances et de
"installation des stations. L’autre brigade a commence les observations des angles sur les
stalions de premier ordre.

Par suite de la decision prise röcemment de faire ex&cuter une triangulation secon-
daire en connexite direcle avec la triangulation primaire, on a dü donner ä la reconnaissance
assez d’extension pour rechercher les points secondaires qui devront &tre observes de concert
avec les stations primaires, afın de former un canevas pouvant servir de base & la Lriangu-
lation de second ordre. (es reconnaissances sont ä präsent presque terminees pour la partie
Sud-Est de notre territoire.

En outre, cette brigade a construit les installations pour les observations sur les
points primaires de cette partie du territoire, savoir !

Lemelerberg, Veluwe, Lutphen, Winterswrk, Hettenheuvel, Imbosch, Rhenen, Flierne-
bery Oss, Oirschot Beek, Venray, Nederweert, Klifsberg ei Ubagsberg.

Sur les stations environnantes olı les installations pour les observations n’ont pas
encore &l& construiles, on en a fait de provisoires pour les heliotropes, de sorte que nous
possedons A prösent toute une serie de stations qui sont prötes pour les observations.

En dehors de ces stations primaires, on a construil dans cette parlie du reseau une
sörie de points secondaires pour &tre observ6s en meme temps que les points primaires; ce
sont les stations de Beekbergen, Lunteren, Nymegen, Mil (en cours de construction), Sambeek,
Venlo et Ruremonde.

La brigade de reconnaissance s’oceupe en ce moment des recherches qui doivent
servir pour former un projet de raccordement avec la Belgique.

La seconde brigade, qui est chargee des observalions sur les points primaires, a
commenc6 ses operalions dans le cours du mois de mai. Le temps pluvieux et les brouillards
qui ont rägne& presque tout l’&t4 ont entrave ces observations outre mesure ; de sorle que
jusqu’ä prösent nous n’avons pu faire les observations que sur les deux stalions de /mbosch et
Hettenheuvel.

 
