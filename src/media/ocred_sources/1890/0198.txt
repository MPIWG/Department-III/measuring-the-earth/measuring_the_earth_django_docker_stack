 

Rapport de M. le Colonel von Zachariae
sur les travaux executes en Danemark
(voir Annexe B. II, p. 143)

Rapports sur les travaux en France, pre-
sentes par MM. Faye (voir Annexe B.
Ill, p. 445), le General Derrecagaix
(voir Annexe B. IP, p. 116), Bouquet
de la Grye et Lallemand (voir Annexe
B 1%, p. 120).

Rapport de M. le Lieutenant- Soul Han
sur les travaux en Grece, lu par M.
Carusso (voir Annexe B. IV, p. 123)

Rapport de M. le General Ferrero sur les
travaux en Italie SB Amnnexe B.
p 122) :

Rapport de M. sonen sur en ea des
Pays-Bas (voir Annexe B. VI, p. 431).

Rapports de MM. von Kalmär, Hartl et
von Sterneck sur les travaux de la
« Gradmessung» en Autriche-Hongrie
[voir Annexes B. VII? et B. VIIb, p. 133
et 142)

Quatrieme seance, 21 seplembre 1890.

Communication par le Secretaire de quel-
ques lettres parvenues au Bureau
Rapport de M. d’Avila sur les travaux exe-
cutes en Portugal (voir Annexe B. VII,
ei... ee
Rapports sur les avalt en Do pre-
sentes par MM. Foerster. professeur (voir
Annexe B. IX pı u, Helmert pro
fesseur (voir Annexe B. IXb, p. 149) et
le Colonel Morsbach (voir AnnexeB. a
p.1a2) 4%... eo.
Rapport de M. Ile ana nen sur

Pag.

32

)
KD

84-85

36

36

86-87

 

les travaux russes pendant l’annee 1889,
lu par le Secretaire (voir Annexe B. X,
p. 155

Rapport de M. le De irsch sur die

Iravaux ex6cutes en Suisse (voir Annexe
B. XI, p. 169)

Discussion au sujet de delahe: Boa ai
Rapport suisse concernant les deviations
de la verticale et communication de M.
le professeur Steinmann sur la structure
geologique des Alpes

Deuxieme rapport de la on en fi-
nances presente par M. le professeur
Fcerster. Le Rapport est approuve .

Rapport de la Commission speciale sur la
question de la variabilil& des latitudes,
lu par M. le professeur Ferster .

Discussion sur les propositions &noncees
dans ce rapport : :

Ges propositions sont adoplees al’ ii

Programme pour l’activite du Bureau cen-
tral pendant l’exercice 1890-1891, pro-
pose par M. le professeur Helmert et ra-
tiie par la Commission permanente .

Sur la proposition de M. Hirsch, la Com-
mission permanente declare maintenir les
conclusions de la Conference generale de
Rome relatives au premier möridien et a
l’heure universelle . . i

Choix du lieu pour la prochaine oniesence
de la Commission permanente

Le President, M. le Marquis de Mulhacen,
exprime la reconnaissance de l’Assemblee

Gouvernement grand-ducal. a l’Uni-
versitl@ et a la ville de Fribourg, ainsi
qu’a la Societe du musee pour leur gene-
reuse hospitalite

Pag

87-88

33

88-90

89-90

90-96

96
97

In

I8

aaa WAR 1 (1a =

kml

I na hl

3 unamalb hc a ha

 

 
