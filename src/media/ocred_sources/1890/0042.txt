 

Länder und namentlich die italienischen, haben die beständige und hingebende Arbeit des
Herrn Respighi im Interesse der Iirdmessung gewürdigt, und die Astronomen aller Länder
schätzen die bedeutenden Leistungen des verstorbenen Collegen wie sie es verdienen.
(Siehe Beilage B. V.)

Herr Schols berichtet über die Arbeiten in den Niederlanden. Dieselben betreffen
unter Anderem Recognoseirungen in dem Südosttheile des Landes, welche gleichzeitig für
die Messungen erster und zweiter Ordnung ausgelührt worden sind. Das ausnahmsweise
ungünstige Wetter dieses Sommers hat die Beobachtungen erster Ordnung leider nur auf
den beiden Stationen Imbosch und Hettenheuvel fertig zu bringen gestattet. (Siehe Bei-
lage B. VI.)

Für Oesterreich-Ungarn erstattet Herr von Kalmdr Bericht über die Gradmessungs-
Arbeiten des k. k. militär-geographischen Instituts im Jahre 1890.

Die in Nordost-Ungarn und Ostgallizien ausgeführten Nivellements umfassen einige
hundert Kilometer. Aus den speziellen Untersuchungen über die Variabilität der Nivellir-
Latten geht hervor, dass dieselben sich im Laufe einer Sommercampagne um etwa 150 u.
verlängern; im Laufe des Winters kommen dieselben sehr nahe auf ihre frühere Länge
zurück.

An astronomischen Arbeiten wurden im Sommer 1890 an 16 Punkten des böhmi-
schen Netzes Polhöhe, Azimut und Schwere bestimmt. Die Triangulirungsarbeiten auf den
Punkten erster Ordnung zwischen dem 41. und 42. Meridian sind fortgesetzt worden. Ein
besonderer Bericht des Herrn von Sterneck gibt Auskunft über die von ihm im Jahre 1890
ausgeführten Pendelmessungen. (Siehe Beilage B. VII? et VIP.)

Die noch ausstehenden Berichte von Portugal, Preussen, Russland und der Schweiz
verschiebt der Präsident auf die nächste am Sonntag stattfindende Sitzung.

Die Sitzung wird um 5'/, Uhr geschlossen.

dl uud

AErRTaT- TIgat TIMDE TRILNLE TUNG LIICTEETE)

ak midı ı

ll)

su ybahamhlh äh aa had suuaintuishnatnundit ML u a

 

 
