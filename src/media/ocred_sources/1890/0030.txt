  

 

24
einein einzigen Pendel ohne vorausgegangene und nur mit nachfolgender Anschlussinessung
in Wien erfolgte) gar keine Gompensation, was mit der geologischen Beschaffenheit jener
Gegend, nach Suess ein Einbruchsgebiet, zusammenhängen dürfte.‘ |

So erweisen sich die Schweremessungen auch als ein wichtiges Hülfsmittel für geolö-
sische Forschung. | | |
Im Interesse der Geodäsie und Geologie liegt es, die Verhältnisse der Schwerkraft
in den österreichischen Gebirgsländern) noch weiter zu studiren, was sehr zweckmässig
dadurch geschehen kann, wie esllerr v. Sterneck auch beabsichtigt, dass das im vorigen
sowie diesem Jahre untersuchte Netz böhmischer Punkte durch eine Kette von Stationen
nach Süden bis Dalmatien ergänzt wird.

Ausserdem ist es zur Vollendung der Erforschung des Verlaufes der Schwerkraft
im Gebiete der Tyroler Alpen unerlässlich, dass durch eine Reihe von Stationen sowohl nach
N. wie nach $. Anschluss an die Messungen des General von Orff in München und des Pro-
fessor Lorenzoni in Padua gewonnen wird. Es würde im Interesse der Sache liegen, dass
Ilerr v. Sterneck diese Anschlussmessungen nieht nur in Tyrol, sondern auch in Italien
und Bayern ausführt.

No Agonmolrz naahldne En HELMERT.

Das Jahr 1890 hat eine reiche Ausbeute an Lothabweichungen geliefert, indem für

mehrere Länder umfangreiche Berechnungen auf diesem Gebiete publizirt worden sind.

Pür Italien hat Herr Col. de Stefunis mehrere Reihen Lothabweichungen vegeben !.
Die Mittheilungen erstrecken sich auf Breite und Azimui für 11 Punkte im Süden, von der
Südspitze Sieiliens in 36° 43° Breite bis Termoli in 43° 1° Breite, einschliesslich 4 ‚öster-
reichischer Ansehlusspunkte an den Küsten Dalmatiens und Albaniens; 3 Lokalitäten ın
Rom und 2 in Neapel sind für je 4 Punkt gerechnet. Ferner auf Breite und Azimut für
% Punkte im Norden, einschliesslich Nizza. EndlichZauf die Längenunterschiede von 1 1 über
oanz Italien vertheilten Punkten, davon je ein französischer und schweizer Anschlusspunkt.
Den Berechnungen dürfte Bessel’s Ellipsoid zu Grunde liegen. Sie bestätigen die schon
[rüher gemachte Wahrnehmung, dass Italien das Land der interessanten Lothstörungen ist.

Grössere Abweichungen in Breite von 10 his zu 20” zeigen Termoli, Corfu, Nizza,
Mailand.

Für Sachsen hat Herr Prof. Nugel am Schlusse seines grossen Werkes über die
Haupttriangulation dieses Landes relative Lothabweichungen in Breite für 17 Punkte, in
Azimut für 12 Punkte und in Länge für 4 Punkte mitgetheilt?. Iiervon betreffen 7 Punkte
Leipzig und seine Umgebung, wovon schon in den « Verhandlungen von Berlin » die Rede
war. Neu bekannt werden 9 Punkte in Breiten- und 7 Punkte ın Längenstörung.

Im Allgemeinen zeigen sich im Anschluss an mein Lothabweichungssystem für
Y Processo verbale delle sedute della Commissione?Geodetica Italiana tenute in Roma nei siorni4e5

dicembre 1889. Firenze 14890. p. 24-26.
2 Astr.-geod Arbeiten für die Buropäische Gradmessung im Königreich Sachsen. II. 1890.

 

fi

arm aaa RUE NEM 1

FILPIERt

elle

Ialı haha

uk landan

vl na bs bahn

 
