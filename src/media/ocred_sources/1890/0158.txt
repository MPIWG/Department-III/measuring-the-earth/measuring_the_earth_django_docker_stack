 

Beilage B. IX.

BERICHT

der Trieonometrischen Abtheilung der Königlich Preussischen
Landesaufnahme über die Arbeiten des Jahres 159.

TRIANGULATION ERSTER ORDNUNG

I. Die Beobachtungen in der Rheinisch-Hessischen Kette (vergl. Verhandlungen der
neunten Allgemeinen Konferenz, Bericht für 1889 und Netzbild zwei), wurden fortgesetzt.
Im nördlichen Theile der Kette, wo mehrere Richtungen den Kohlen- und Industriebezirk
‘von Dortmund-Essen durchschneiden, wurden durch den dichten, nur an Sonntagen sich
mindernden Rauch die Arbeiten sehr erschwert und verlangsamt.

Es sind von zwei Sektionen nur die acht Stationen Velbert, Balverwald, Fürsten-
berg, Steigekoppe, Feldberg, Meliboceus, Donnersberg und Lufdenbert erledigt. worden.
Station Velberg hat allein elf Wochen in Anspruch genommen.

Auf der Station Fürstenberg sind auch die den Anschluss an das Niederländische
Netz vermittelnden Richtungen nach Hetienheuvel und Winterswyk beobachtet worden (vergl.
Bericht für 1889, Triangulation erster Ordnung, \V).

IL. Das Bonner Basisnetz (vergl. Bericht für 1889, Netzbild zwei) wurde mit Beob-
achtungs-Vorrichtungen versehen.

Die Basis läuft der im Jahre 1847 durch General Baeyer gemessenen in einer Ent-
fernung von 70 bis 90 Metern annähernd parallel und ist 2515 Meter lang.

Die Messung der Basis soll seitens der Abtheilung in einem der nächsten Jahre
vermuthlich 1892, mit dem Bessel’schen Apparate erfolgen ; in wissenschaftlichem Interesse
und unmittelbarem Anschlusse an diese Messung wird das Königliche Geodätische Institut
die Basis mit seinem Brunner’schen Apparate ebenfalls messen.

III. Das Niederrheinische Dreiecksnetz, welches den von der Rheinisch-Ilessischen
Kette umschlossenen Raum ausfüllt und im Jahre 1889 fertig rekognoszirt war, ist im ver-
flossenen Sommer zur Hälfte bebaut worden.

rau RHEIN I AL

IL

kind)

ll

lau

se ad |

 

 
