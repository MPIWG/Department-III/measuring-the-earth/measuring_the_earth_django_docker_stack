 

sowie auch auf dem Chasseral bestimmt werden. Für das nächste Jahr seien weılere Unter-
suchungen der Art im Meridian von Bern in Aussicht genommen.

Auf die Bemerkung des llerrn Faye, dass die in der Mittheilung des Herrn Hirsch,
als aus den Schweizer Beobachtungen mit Wahrscheinlichkeit hervorgehende Ilypothese von
Hohlräumen unter den Alpen dem jetzigen Standpunkte der Geologie nicht wohl entspreche,
erwidert Herr Ilirsch, dass er natürlich nicht von absolut massenfreien llöhlungen, sondern
von sogenannten relativen Hohlräumen, d. h. mit anderen Worten von Massen-Defecten
infolge wesentlich geringerer Dichtigkeit habe sprechen wollen.

Da die Permanente Commission das Vergnügen hal, einen gelehrten Geologen der
Universität Freiburg ihren Sitzungen beiwohhen zu sehen, so spricht Herr Hirsch den
Wunsch aus, derselbe möchte der Versammlung einige Auskunft über die unter den Geolo-
gen jetzt vorherrschenden Ansichten betreff der Struetur der Alpen geben.

Herr Prof. Steinmann kommt dieser Aufforderung in freundlichster Weise nach, und
drückt sich folgendermassen aus :

\Wenn man das Vorhandensein von Massendefecten in den Freiburger Alpen aus dem
oeologischen Baue der Gegend erklären will, so darf man nicht wohl in erster Linie daran
denken, dass wirkliche llohlräume unter dem Alpengebirge vorhanden seien. Vielmehr müsse
man versuchen, Unterschiede im speeifischen Gewichte der betreffenden Gesteinsmassen zur
Erklärung herbei zuziehen. Solche sind in der That vorhanden. An der Zusammensetzung
des Juragebirges betheiligen sich vorwiegend kalkige Gesteine der Jura- und Kreideformation
von verhältnissmässig hohem speeifischem Gewichte und unter diesen dürften in nicht allzu-
orosser Tiefe krystallinische Gesteine von noch höherer Eigenschwere vorhanden seien.
Die äussersten Ketten des Alpengebirges in der Gegend von Freiburg bestehen dagegen
vorwiegend aus Flysch, einem thonig-schiefrigen Gesteine des älteren Tertians von sehr
geringem specifischem Gewichte. Zufolge einer bereits von Studer vertretenen Auffassung,
welcher ich mich anschliesse, sind die äussersten Theile des Alpengebirges über das
gesenkte Molassenvorland hinübergeschoben, so dass unter dem Fiysch zunächst die speeifisch
leichten Mergel und Sandsteine der Molasse, und erst unter diesen Gesteine mit höherer
Eigenschwere, analog jenen des Juragebirges zu suchen sein würden. Für den vorliegenden
Fall scheint daher die Möglichkeit nicht ausgeschlossen, die beobachteten Abweichungen in
ler Massenanziehung aus der Vertheilung specifisch verschieden schwerer Massen zu erklären.

Der Präsident dankt Herrn Prof. Steinmann für seine interessante Mittheilung, und
ersucht darauf Herrn Prof. Förster den zweiten Theil des Berichtes der Finanz-Gommission
zu erstatten. Derselbe lautet :

« Infolge der vom Herrn Director des Gentral-Bureau’s gelieferten Daten, betrug am
Ende des Jahres 1889 das gesammte verfügbare Activam der Dotation der Permanenten

Commission 17893 M. — 92366 Fr., anstatt des im vorjährigen Bericht vorgesehenen
Activums von 16000 M. — 20000 Fr. Wenn man dazu die Resultate der ersten acht Monate

des laufenden Jahres, sowie die ungefähre Schätzung der noch weiter bis Ende 1890 zu

art fa IBM Mk. dla,

alu ln ı

HASTE!

vr

1m 4 bla

Ei

 

 
