 

7%

On voit ainsi que les mesures de la pesanteur constituent une ressource importanle
pour les recherches geologiques.

Il est dans lVinteröt de la g&od6sie et de la gäologie de poursuivre lP’etude de la
pesanteur dans les Alpes d’Autriche ; on arrivera utilement au but d’apres le plan de M. von
Sterneck,"en eomplötant le reseau des points de Bohöme, examines l’annde derniere el celte
annde-ci, par une chaine de stations allant du cöt& Sud jusqu’en Dalmatie.

En outre, pour completer l’ötude de la pesanteur dans la region des Alpes du Tyrol,
il importe de la rattacher des cötes Nord et Sud aux mesures du general von Orff a Munich
et de M. le professeur Lorenzoni A Padoue. I serait dans l’inter&t de cette elude que M. von
Sterneck ex&entät ces mesures de jonction non seulement en Tyrol, mais aussi en Italie et
en Baviere. »

Aotıt 1890. HELMERT.

Le second rapport est concu dans ces termes:

« L’annde 1890 a fourni une riche moisson en mesures de devialions, puisque pour
plusieurs pays on a publi6 des caleuls nombreux dans ce domaine.

Pour l’/talie, M. le colonel de Stefanis a donn& plusieurs series de devialions!. es
communications s’etendent sur la latitude et l’azimut de 11 points au sud, A partir de la
pointe meridionale de la Sieile par 36° 43° de latitude Jusqu’ä Termoli par 43° 1’, y compris
A points de jonction autrichiens sur les cötes de Dalmatie et d’Albanie; trois stations a home
et deux A Naples y figurent pour un point. Elles comprennent aussi les latitudes et azimuls
pour 4 points au nord, y compris Nice. Enfin, elles embrassent les differences de longitude
de 11 points röpartis sur tonle V’Italie, parmi lesquels un point de jonction avec la France el
un avec la Suisse. Les calculs paraissent eire faits avee les elements de "’ellipsoide de Bessel.
Cette publication confirme la remarque faite d&jä anlerieurement, que l’Italie est le pays par
excellence des deviations interessantes.

En latitude, on a constate de grandes deviations allanl de 10. 20, pour Lermoli,
Corfou, Nice et Milan.

Pour la Saxe, M. le professeur Nagel, A la fin de son grand ouvrage sur la triangu-
lation de premier ordre de ce pays, communique des devialions relalives en latitude pour
17 points, en azimut pour 42 points et en longitude pour /% points?. Dans ce nombre,
7 points concernent Leipzig el ses environs; ıl en a deja. &t6 question dans les Comples-
?endus de Berlin; 9 points pour les deviations en latitude el 7 pour celles en longitude
sont nouveaux.

En general, en se rallachant a mon systeme des deviations pour l’Europe centrale
(avec Rauenberg, dövialion en latitude et en longitude de +4 9” environ), on trouve en Saxe
des d&viations en latitude essentiellement positives d’environ 3” el de m&me des devialions

1 Processo verbale delle sedute della Commissione Geodetica Italiana tenute in Roma nei giorni 4 e 5
dicembre 1889. Firenze 1890, p. 24-26.
? Astron.-geod. Arbeiten für die europäische Gradmessung im Königreich Sachsen. I. 1890.

ul

anna ae VRNTNE W EA1 T

FIZEL

kann) 1

I to 1

mh a Äh

 

 
