 

I

Sm Tee FAT 73 Serge

lin FERIEN]

all

i

=
'=
m

=
m
=
=
=
iS
As

«Wenn auch an unserer Hochschule, wie an fast allen Universitäten Deutschlands, ein
Lehrstuhl für Geodäsie fehlt, so darf ich Sie doch versichern, dass Sie an der hiesigen Uni-
versität einem regen Interesse für diese Wissenschaft begegnen werden. Und besonders ist
dies jetzt der Pall, da durch die neuesten Untersuchungen, die ja gerade durch die Lommis-
sion veranlasst sind, das Dogma von der Gonstanz der Polhöhe so erschüttert ist. Die Frage
nach den Schwankungen der Erdoberfläche oder der Erdaxe wird jeizt und künftig eine
wichtige Aufgabe für die Commission bilden. Die Antwort, welche die Beobachtungen er-
Iheilen weriden, die Gesetze die sich herausstellen werden, sind aber nicht nur für Geodäsie
und Astronomie von Interesse, sondern auch für andere Wissenschaften, die sie benülzen
werden, um aus ihnen wichtige Schlüsse auf Vergangenheit und Zukunft des organischen
und unorganischen Lebens unserer Erde zu ziehen.

«Das Band, welches die Geodäsie schon jetzt mit anderen Wissenszweigen verbindet,
wird in Zukunft noch enger werden. Und so tragen Ihre Bestrebungen und Erfolge auch
dazu bei die Einheit der Wissenschaften, an einer Stelle wenigstens, neu zu befestigen.

«Ich bitte Sie, hochgeehrte Herren, meine und meiner Gollegen sympathische
Wünsche für die Förderung Ihrer Arbeiten entgegennehmen zu wollen. »

In Namen der Grossherzoglichen Regierung richtet Herr Prof. Hard folgende
Begrüssungsworle an die Versammlung :

« Hochansehnliche Versammlung !

« Es ist mir der ehrenvolle Auftrag zu Theil geworden, Sie, hochgeehrie Herren, und
insbesondere die hochgeehrten Herren Mitglieder der Permanenten (ommission der interna-
tionalen Erdmessung im Namen der Grossherzogl. Staatsregierung zu begrüssen. Meine hohe
Regierung ist Ihren Porschungen und Arbeiten stets mit grossem Interesse gefolet; sie wird
denselben jetzt, wo Baden der internationalen Vereinigung beigetreten ist, erhöhtes Interesse
widmen, auch im Kalle, dass ihre Arbeiten sich über unser Land wieder erstrecken sollten,
denselben mit besonderem Wohlwollen entgegenkommen.

« Ich darf Sie versichern dass es der Grossherzogl. Regierung zur grossen Freude ge-
reicht, mit Ihrer diesjährigen Zusammenkunft eine Stadt unseres engeren Vaterlandes beehrt
zu haben, und heisse ich Sie, hochgeehrte Herren, im Namen meiner hohen Regierung wie-
derholt aufs Herzlichste willkommen. »

Hierauf begrüsst der Oberbürgermeister von Freiburg, Herr Winterer, die Versanım-
lung mit folgenden Worten :

« Hochgeehrte Versammlung !

« Nachdem Sie soeben die Begrüssung der Vertreter unseres Landesfürsten sowie der
Grossherzogl. Regierung und unserer Universität entgegen genommen haben, erübrigt mir nur,
Ihnen auch noch den herzlichsten Willkommeruss der Stadt Freiburg zu entbieten, in welche

 
