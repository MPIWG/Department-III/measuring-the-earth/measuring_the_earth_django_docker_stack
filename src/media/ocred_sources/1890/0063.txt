EIN rm eTeNTmm 3# Spare

 

UI] m

MARI

NER KENN.

  
    

ein

=

=
Er
g=
m
=
-
im:

Is all

3

Le Secrslaire communique d’abord plusieurs lettres «d’excuses de MM. Davidson, Der-
röcagaix, le general Liagre et le general Stebnitzki, que d’autres oceupalions ou des raisons
de sant& empechent d’assister A la session actuelle. M. Davidson est retenn par la continua-
tion des fravaux geodösiques qu’il a entrepris cel &t& dans la Sierra Nevada; le general Der-
röcagaix n’a pu venir A Fribourg A cause des manauvres des 1er et Yme corps d’armee, aux-
quelles il prend part et qui ont lieu dans le courant de septembre. Le Secretaire est convaincu
que la Commission permanente, ainsi que l’Assemblee toute entiere, regrette l’absence de
ces collegues distingues.

D’un autre cöte, plusieurs deleguds ont annonc6 A Vavance au Secretaire leur inten-
tion d’assister A la Conference. (est lä un bon exemple qui merite d’Etre suivi generalement,
car il est extr&mement desirable, en vue des pr&paratifls A faire pour la session, de connaitre
le nombre approximatif des membres qui y prendront part.

Parmi ces communications, le Seerdtaire releve encore en parliculier un avis du
Bureau central annoncant la nomination de M. le professeur Haid comme delegue du
Grand-duch& de Bade et, au nom du Bureau de la Commission permanente, il souhaite la
bienvenue ä ce nouveau collegue.

Le Bureau s’est permis aussi cette fois, comme il l’a fait preeödemment, d’inviter &
la Conference de cette annde MM. Bischoffsheim et Perrotin, qui nous ont offert a l’Observa-
toire de Nice la brillante hospitalit& dont nous avons tous gard& le souvenir reconnaissant.
Tandis que M. Perrotin est retenu & son Observatoire par des recherches speciales, difficiles
A interrompre, la Commission a le plaisir de constater au milieu de nous la presence de
M. Bischoffsheim, dont les grands merites pour le developpement de l’astronomie et de la
o&odesie viennent d’etre reconnus si justement par une des premi6res autorits scienlifiques.

En outre, le Bureau s’est permis d’adresser une invilation pour la session actuelle ä
M. le professeur Jordan, qui n’a cess& de suivre nos travaux avec le plus vif interet depuis
"’origine de l’Assoeiation pour la mesure des degres.

Enfin, la Commission a la salisfaclion de voir assister a ses scances plusieurs des
savants professeurs de ’Universite de Fribourg.

En ce qui eoncerne les finances, M. le Directeur du Bureau central presentera,
comme d’habitude, les comptes de l’annde derniere ä la Commission, et fera un rapport
detaill& sur l’6tat actuel des finances. Suivant usage &tabli, !’Assembl&e confiera probable-
ment A une Commission sp6ciale le soin d’examiner serieusement cette parlie importante de
son administration. Comme du reste le Bureau, se conformant a une decision de la Gom-
ınission permanente, a rödigs au mois d’ayril dernier un rapport special qu’il a alvesse aux
oouvernements de la Convention, contenant un expos& aussi complet que possible de l’etat
des finances A celte &poque, en m&me temps qu’un lableau des contributions pour la pe-
riode trisannuelle allant de 1890 A 1892, il ne reste plus en ce moment au Secrelaire qu’ä
insieter sur le fait que la situation finaneiere actuelle peul &tre caracterisee comme Lres
satisfaisante. En effet, l’Assoeiation g&odesique internationale dispose actuellement, pour les
difförents travaux qu’elle poursuit, d’un fonds de 30 000 marks environ (37 500 franes). La

rentr&e des contributions annuelles pour l’exereice courant est de m&me r6jonissante, puis-
_ PROCESS VERBAUX 3

 
