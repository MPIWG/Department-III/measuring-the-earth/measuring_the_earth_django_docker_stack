 

|
N

TOTEN 79 FT TgITRaTT TEL HAN

a

ma a FAN ANIEI ar

ill

ll

ala

Unnii 2 RA RRIHER LARA Am LU AL

par celle du bassin du lae, dont la profondeur atteint jusqu’ä 300 m., se trouve cependant
contrebalancde par V’attraetion contraire des Alpes au Sud, dont le centre d’attraclion est
sans doute plus &loign&, du moins pour Neuchätel et les deux points au Nord, mais dont les
inasses soulevdes sont considörablement plus puissantes que celles du Jura. Pour la station de
Portalban’surtout, qui g6ologiquement est d&ja situee sur le terrain alpestre et dont la
distance au centre d’attraetion effective des Alpes n’est certes pas plus grande qu’au centre
des masses Jurassiennes, on ne peut suffisamment s’expliquer la deviation vers le Nord que
par l’hypothese de vides relatifs ou de defauts de masse au-dessous des chaines soulevees
des Alpes. De me&me les faits curieux que la deviation negative est plus forte A Chaumont, sur
la erete de la premiere chaine du Jura, qu’ä Neuchätel, situe au pied de cette derniere, el
qu’on constate encore & Tete-de-Ran, place sur le sommet de la chaine principale, une forte
döviation vers le Nord, conduisent 6galement & la supposition de defauls de masse au-dessous
de la premiere chaine du Jura.

Ges recherches intöressantes se poursuivent actuellement par des observations astro-
nomiques dans deux points situes &galement ä peu pres dans le möridien de Neuchätel, au
pied ei sur la premiere chaine Nord des Alpes, ainsi que sur le Chasseral, ’un des sommels
les plus öleves du Jura.

Quant aux nivellements, on a ex&culd dans la campagne de 1889 916°"5 de lignes,
soil de chemins de fer, soit le long de l’Aar et de la Reuss; l’erreur moyenne de ces opera-
tions secondaires reste au-dessous de Zum par kilometre, sauf pour une partie du nivelle-
ment de la Reuss, olı s’est glissee une erreur de 0/1 soit d’&criture, soit de lecture, et qui
sera corrigee par un nouveau nivellement de la section Lucerne-Brugg que l’on ex&cule
vor elle

Puisque la solution de la question du choix d’un niveau fondamental general a
&t& renvoyde malheureusement de nouveau A lrois ans, la Suisse se voit oblig6e de publier le
catalogue de ses altitudes compensees, par rapport au point de döpart national, la Pierre

du Niton.
X. MIRSCH.

 
