 

M ap anne nt

 

Se

Se

254

somme des trois angles mesurés sur deux angles droits. A notre avis, les autres con-
ditions sont forcées.

Par ce changement, les angles deviennent sphériques et les triangles peuvent
s'appliquer sur des sphères dont les rayons, sans être rigoureusement définis, sont égaux
à ceux des sphères osculatrices de la surface générale du globe en chacune des petites
facettes de contact représentées par les éléments triangulaires qui la composent.

La longueur d’un arc coupant une pareille chaîne pourrait se calculer par parties
courbes dans chaque triangle, en s’aidant de la trigonométrie sphérique mais, comme il
est plus simple d'opérer par la trigonométrie rectiligne, on considère jusqu'au bout les
triangles plans et l'arc qui les traverse devient un polygone d’un grand nombre de côtés.
Quand les directions les plus probables ont été recherchées à chaque station, on ob-
tient les valeurs les plus probables des angles et l’on n’est aucunement en droit d’y
apporter de nouvelles corrections. Les valeurs des côtés, calculées à l'aide de ces
angles, sont celles que l’on peut raisonnablement adopter et, si plusieurs valeurs ont
été obtenues pour un même côté, on devra prendre la moyenne en tenant compte des
erreurs moyennes des différents résultats.

L'écart est-il plus grand que la somme des erreurs moyennes, c'est un indice
certain que les angles n’ont pas été observés avec la précision voulue; dans le cas con-
traire, la triangulation doit être acceptée comme suffisamment bonne. Cependant
l'erreur moyenne d’un côté dépend des angles mesurés, l’écart devra dont encore rester
au dessous d’une certaine fraction de la longueur du côté; à la première conférence,
l'association géodésique a fixé & la 25000° partie l’écart maximum, mais on pourrait
exiger une approximation plus grande.

Là devrait se borner, d’après nous, tout le travail de la compensation et en dé-
finitive, on est obligé de le faire pour des côtés communs à des triangulations voisines
par suite du peu d’unite dans les méthodes pratiques d’observation.

Le point 5° du programme de 1876 peut donc étre scindé en deux parties: la
première relative à l'équation établissant entre les bases d'un même réseau Vaccord le
plus parfait, semble devoir être résolue affirmativement. Si les angles ont été bien me-
surés, l'équation de condition se vérifiera d'elle même, si au contraire les angles sont
fautifs, les erreurs les plus probables seront rendues apparentes et constitueront les
corrections à apporter aux angles pour obtenir, par le calcul suecessif des triangles, une
valeur de la base d'arrivée différant le moins possible du résultat de la mesure directe.

La seconde partie parait exiger une solution inverse, car le côté commun à deux
triangulations voisines peut appartenir à des réseaux établis sur des ellipsoïdes très dif-
férents et des-lors les suites de triangles qui unissent ce côté aux bases mesurées dans
des pays limitrophes, ne pourraient sans danger être transformées en une seule chaîne.
Il est plus simple de demander à chaque pays les portions d’arcs mesurés sur leur terri-
toire et de les faire entrer dans le calcul définitif avec des poids ou des importances
dont la commission permanente indiquerait le mode de formation.

Supposons que les deux valeurs d’un côté commun à des triangulations voisines
soient parfaitement égales, les azimuts ne le seront pas dans les deux réseaux et l’on

 

x
=
=
=
>
FE
z
=
=
=
x
=
U

1

ah ul 11

ui ul

sh en a Hd Ad tal ad pau pus à:

 
