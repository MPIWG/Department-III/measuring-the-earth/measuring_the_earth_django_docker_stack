 

18}

ma

mt cave 0 à uk ih A eke BR rn à '
OE ORT RE RRA BAG BELT ET TET

173

apporté d’Amerique, avait été presqu’abimé dans le transport; aussi, me suis-je trouvé
force de faire usage de l'instrument que la grande autorité du général Baeyer avait
prononcé défectueux, malgré la perfection du travail qui fait honneur à l'atelier célèbre
d’où il sort. |

Voila comment j'ai été amené à faire quelques expériences dans le but de me-
surer et de tenir compte de ce défaut.

On peut s’imaginer un support si disloqué que le pendule, en oscillant de l’un
côté à l’autre, jetterait la pièce, sur laquelle il repose, d’une position dans une autre,
sans rencontrer, jusqu'aux points d'arrêt, d'autre résistance que celle de l’inertie et de
la friction. Mais un tel jeu n'existe point dans les supports que je connais, ce que j'ai
vérifié en les observant avec un fort microscope et en constatant qu’ils retournent tou-

jours à la première position de repos après toute flexion, si petite ou si grande

qu'elle soit.

Le mouvement auquel on a à faire est, en effet, une flexion oscillatoire d’un
corps élastique. L’amplitude de cette oscillation est à peu près en raison d’un cinq mil-
lième à celle du couteau inférieur du pendule; c’est pourquoi on peut négliger le carré
de cette fraction.

Le plan sur lequel le couteau repose est courbé, sans doute, par le mouve-
ment du pendule; mais je néglige cet effet, en me bornant à considérer le mouvement
de la partie au-dessous du milieu du couteau. Quand on applique à cette partie une
force horizontale perpendiculaire au couteau, elle fait un mouvement de révolution
autour d’un axe situé en arrière, et au-dessus du pied, à une distance d’un mètre en-
viron. Or, on peut négliger la différence entre une révolution passant seulement par
quelques secondes d’arc et une translation. ŸIl y a encore une certaine variation minime
dans la pression verticale du pendule sur le support; mais elle est évidemment loin de
produire un effet sensible sur la durée d’une oscillation.

Si Von désigne par

m la masse d’une particule:
r sa distance de la tranche du couteau;

—

» l'angle, dans la position de repos, entre la verticale et la perpendicu- |
laire abaissée de la particule sur la tranche du couteau; w/ua. 7, 7%

{ la masse du pendule:

! la longueur du pendule simple faisant une oscillation dans le même
+ temps que le pendule actuel;

g laccélération de la pesanteur :

dans l'Æneyclopedia Britannica de faire usage de deux pendules à réversion, ayant la même forme, mais
des poids différents, afin que l'on puisse tenir compte de l'erreur provenant de la flexion. Bessel, dans
son grand mémoire sur la pesanteur à Künigsberg, fait remarquer que cette cause exerce la même
influence sur son pendule long que sur le court, et c'est pourquoi leffet s'élimine. La construction
de plusieurs supports de pendules, comme celui du capitaine Bassevi, montre une juste appréciation de
cette difficulté.

 
