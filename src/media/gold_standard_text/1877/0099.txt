 

ILS per RTT tere

mer ee

Ieee TT een

mr

DT

PTIT

En as

 

87

81S
A l'ouverture de chaque séance générale, le bureau fait part à la Conférence
des communications et propositions qui lui auront été remises. Suivant la décision de
la Conférence ou du Bureau, ces communications peuvént être mentionnées dans les
comptes-rendus avec plus ou moins de détails, ou y être reçues in-extenso: Elles seront
définitivement incorporées dans les archives du Bureau central de l'Association géodésique.

\

So

La Commission permanente est chargée de la rédaction, de la publication et. de
la distribution des comptes-rendus de la Conférence.

§ 10. :
Les élections destinées à remplacer les membres sortants de la Commission

permanente doivent figurer en premier lieu dans l’ordre du jour d’une des dernières
séances. Le Bureau est obligé d’en avertir l’Assemblée dans la séance précédente.

Oe

M. Ibanez propose au nom de la Commission permanente de constituer le Bu-
reau de la Conférence de la maniére suivante:
M. Baeyer, président honoraire,
M. Zech, président, |
MM. de Bauernfeind et Faye, vice-présidents,
MM. Bruhns et Hirsch, secrétaires.
L’Assemblée ratifie ces choix, et M. Zech prend la présidence en remerciant
la Conférence de la confiance well témoigne et qu'il s’efforcera de mériter.
Le programme pour la Conférence présente a été envoyé à MM. les délégués
en même temps que l'invitation d’y assister. M. le Président demande si lon a des
modifications ou des adjonctions à proposer à ce programme.

Personne ne demandant la parole, le Programme est mis aux voix et adopté à
l'unanimité.

PROGRAMME

pour la cinquième Conférence générale de 1877.

I. Affaires de l'Association: Rapport de la Commission permanente. Rapport
du Bureau central.

II. Rapports des délégués des différents pays sur les travaux exécutés depuis
l’année dernière.

 
