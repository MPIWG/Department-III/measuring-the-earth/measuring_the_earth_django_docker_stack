 

22

dass die Grösse der terrestrischen Refraction in verschiedenen Azimuthen und mit
Benutzung der nivellitisch festgestellten Höhendifferenzen der von Mannheim aus sicht-
baren Stationen Melibocus, Donnersberg und Hornisgründe sich schon jetzt werde

bestimmen lassen.

Herr von Bauernfeind fügt die Bemerkung hinzu, dass er schon im Jahre 1857
den Hohen Miesing im bayerischen Gebirge im Anschlusse an das Nivellement der
bayerischen Eisenbahnen nivellirt habe im der doppelten Absicht: erstens um die Genauig-
keit der barometrischen Höhenmessungen zu bestimmen, worüber man damals noch sehr
im Unklaren war, und zweitens, um seine im Manuscripte bereits fertige Theorie der
atmosphärischen Strahlenbrechung an directen trigonometrischen Messungen zu prüfen.
Zeuge dessen seien seine in den Jahren 1862 und 1864 erschienenen Schriften: „Die
Genauigkeit barometrischer Höhenmessungen ete.“ und: „Die atmosphärische Strahlen-
brechung auf Grund einer neuen Aufstellung über die physikalische Constitution der

Atmosphäre.“

In neuerer Zeit, und zwar in diesem Frühjahr, habe er weiter die Verbindung
zwischen den bayerischen und sächsischen Dreiecken, nämlich zwischen den Punkten
Döbra und Ochsenkopf einerseits, dem Kapellenberg und Stelzen andrerseits, zu gleichem
Zwecke benutzt, um für die Richtigkeit seiner eben bezeichneten Refractionstheorie
weitere Beweise beizubringen und wenn möglich, Beiträge zur Kenntniss der Lateral-
refraction zu liefern. Er habe gestern schon in seinem Berichte darüber gesprochen,
allein die Sache nicht näher angeführt, weil die Beobachtungen noch nicht berechnet
sind. Indem er aus der Triangulation die Horizontalentfernungen und aus besonderen
Nivellements die Höhenunterschiede der trigonometrischen Punkte genau kennt, ist es
ihm möglich, die Neigungswinkel zwischen diesen Punkten im Voraus zu berechnen und
sie dann mit Werthen zu vergleichen, welche die Refractionsbeobachtungen ergeben. Diese
seine Untersuchungen erstreben dasselbe Ziel, das Herr General Baeyer im Sinne ‚hat,
die Idee dazu rühre aber von ihm, dem Redner, her.

Der Präsident schlägt vor, da Herr Plantamour schon heute abzureisen beab-
sichtigt, zunächst die Frage 5 des Programms zu verhandeln, und da die Versammlung
dies genehmigt, bittet er Herrn Plantamour um die Berichtèrstattung über diesen

Gegenstand.

Herr Plantamour erwähnt, dass von den bisher ausgeführten nicht sehr zahlreichen
Pendelbeobachtungen noch so wenig veröffentlicht worden sei, dass es sich vorerst nicht
lohnen würde, die veröffentlichten Resultate übersichtlich zusammenzufassen ; er beschränkt
sich daher wesentlich darauf, der Conferenz über einen wichtigen Punkt zu berichten,
welcher die Bestimmung der Intensität der Schwere durch das Reversionspendel beträcht-
lich zu beeinflussen scheint. Es hat sich nämlich herausgestellt, dass das schon früher
vermuthete Mitschwingen des Stativs einen sehr merklichen Einfluss auf die Ableitung
der Pendellänge ausübt. Nach den theoretischen Untersuchungen der Herren Cellérier
und Peirce und nach den experimentellen Versuchen, welche der letztere in Genf, Paris,
Berlin und Washington ausgeführt hat, ist es möglich, die Quantität der aus dieser

 

À 1.4/4) 14

212 1 ed, 1100411 1186 HA |) Mt li

M

LI!

(ie wiki ll

ni late ml

jst uhh bn fs lM abd dl ua sn

 
