 

 

area tae ea a ee aed

 

 

 

tions du système métallique formé par le pendule et son trépied. Cette participation du
pilier aux mouvements causés par le pendule oscillant, qui se trahit dans les valeurs
différentes obtenues par M. Plantamour pour le déplacement horizontal du plateau de
suspension du même pendule oscillant sur différents piliers, a été, en effet, observée
par moi directement à Neuchâtel, où j'ai appliqué au pilier — un monolithe en calcaire
pesant plus de huit quintaux et cimenté sur le rocher naturel — un appareil à miroir
de touche, en tout semblable à celui qui a servi aux mesures du déplacement du trépied.
En faisant osciller le pendule j'ai vu, avec un grossissement de 3000, le pilier lui-même
suivre parfaitement ces oscillations avec une amplitude de 0#135; en appliquant ensuite
au plateau de suspension un poids de 3100 grammes, égal à celui du pendule, la traction
horizontale de ce poids a produit un déplacement du pilier de 7“54, tandisque sous
l’action de la même force le trépied lui-même avait été déplacé horizontalement de
15077. Il résulte ainsi de cette expérience statique, que la déviation horizontale du
pilier est 17.3 fois plus faible que celle du plateau, tandisque par l’oscillation du pen-
dule j'ai trouvé 16.1 pour ce rapport.

Il faut en conclure que la nature, le poids et l'installation du pilier, sur lequel
on place le trépied de l'instrument, ne sauraient être sans influence sur la quantité de
la c@rrection qu’il faut appliquer à la longueur du pendule, pour tenir compte de cette
perturbation.

In Folge nicht genügender Uebereinstimmung zwischen den Resultaten, die in
verschiedenen Jahren und von verschiedenen Beobachtern auf denselben Stationen für
die Winkelbeobachtungen im Schweizer geodätischen Netz erhalten worden sind, mussten
im Jahre 1877 auf einer Reihe von Stationen die Beobachtungen wiederholt werden.

Die Arbeiten des laufenden Jahres umfassen:

1. Wiederholung der Beobachtungen auf den Stationen einer Dreieckskette,
welche das geodätische Netz der Westschweiz mit der Italien und Frankreich
zugleich gemeinschaftlichen Seite Tr&lör—Colombier verbindet und sich von
Nord nach Süd durch Savoyen zieht.

2. Wiederholung der Beobachtungen im Kanton Tessin zur Berichtigung einiger
Stationen der Dreieckskette, welche unseren geodätischen Alpenübergang an
die zweite Anschlusslinie mit Italien, Ghiridone—Menone, fortführt.

3. Aus den im Eingang erwähnten Gründen wurden auch mehrere Stationen
der mittleren Schweiz einer wiederholten Beobachtung unterzogen.

 

 

x
=
3
=
=
=
=
a
=
=
=
a

vn | het

AN tal

Eon Ind

It nl bé a Add il ace m 11

PPPFESEEFFPRERGEFERREEN

x TEICHE x AYREON NN RENTE EE EE 3
Fee TT LUS SENSE SET h

 
 
