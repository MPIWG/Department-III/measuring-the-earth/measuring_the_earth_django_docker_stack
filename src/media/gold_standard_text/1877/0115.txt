 

Kt TR TP TY" |

ITU CRT LME OT NET CAT

it

rn

wr

TRO PT TE UOTE RT JR TI

 

103

M. Plantamour constate que les observations de pendule faites jusqu’à présent
sont si peu nombreuses et qu’on en à publié un si petit nombre, que ce serait une
tâche ingrate pour le moment de vouloir en résumer les résultats. Il se bornera donc
à appeler l'attention de la Conférence sur un fait important, qui semble destiné à
influencer considérablement la détermination de la pesanteur par le pendule à réversion:
Il a été constaté que l’oscillation du trépied, qu’on avait déjà soupçonnée précédem-
ment, exerce une influence sensible sur la détermination de la longueur du..pendule
simple. Les recherches théoriques de MM. Cellérier et Peirce, de même que les expé-
riences faites par M. Peirce à Genève, à Paris, à Berlin et à Washington permettent de
déduire la grandeur de la correction qui résulte de cette perturbation pour la longueur
du pendule simple. Les communications de notre collègue américain ont engagé M. Hirsch
à Neuchâtel et M. Plantamour à Genève à faire une étude approfondie de ce phénomène
sur le pendule suisse. Leurs résultats confirment ceux obtenus par M. Peirce, en leur
ajoutant toutefois un fait nouveau, à savoir, que ce ne sont pas seulement les oscillations
du trépied, mais aussi celles du pilier qui exercent une influence troublante.

M. Plantamour donne quelques détails sur l'appareil auxiliaire indiqué par
M. Hirsch et construit par M. Hipp à Neuchâtel, qu'il a ensuite fait perfectionner dans
l'atelier de construction de Genève et sur les résultats qu’ils ont obtenus.

Dans sa forme perfectionnée par M. Plantamour cet appareil à miroir d’attou-

2 . : x 1 x
chement permet de déterminer la correction de la longueur du pendule à geo pres de

sa valeur ou à 2 unités près de la 7e décimale de la valeur de la longueur du pendule.

M. Plantamour, qui va se rendre à Berlin avéc l'instrument suisse, y poursuivra
les recherches sur cette matière, et il en rendra ensuite compte dans un mé-
moire détaillé.

M. Hirsch complète le rapport de son collègue en faisant une communication
détaillée sur la méthode d'observation suivie, tant statique que dynamique, il fait re-
marquer en outre, que ses observations et celles qui ont été faites à Genève semblent
démontrer que la grandeur des perturbations dépend en partie de la localité et de
installation de l'appareil.

M. Peirce rappelle, que déjà le Dr. Young et Kater se sont occupés de cette
question, mais que la grande stabilité des piliers employés par Kater et Sabine et le
peu de sensibilité de leurs appareils rendaient moins apparente Vinfluence de la pertur-
bation exercée sur leurs mesures de longueur du pendule.

M. Peirce lui même avait principalement en vue de mettre hors de doute
l'existence des oscillations du trépied et de déduire théoriquement la correction de la
longueur du pendule qui en résulte.

La déduction théorique faite par lui sans connaître le mémoire déjà publié
auparavant par M. Cellérier s'accorde pleinement, dans son résultat, avec celui du ma-

x

thématicien de Genéve. Ses résultats d’observation, qui semblent exacts a pres de

1
100
leur valeur, présentent un accord suffisant avec les résultats obtenus par les observateurs
suisses, si l’on tient compte de la différence des deux appareils. Il a observé les oscil-

 
