 

En Tre ee

PPT ETAT CORP Oriel Ph

UT

mer

\ ia à

it
a
a
ia
43
=
1
a
a
®
®
3
iR
8
a
sh
*

MN

begangene Totalfehler nach Delambre nicht 18 Linien, d.i. 0"04 oder

der ganzen
Länge. Bei den neueren Grundlinien ist dieser Fehler noch geringer, bei der Basis von
Madridejos z.B. nur

1
250,000

5.850.000° ;

Den Grund fir den Mangel an Uebereinstimmung zwischen den europäischen
Netzen darf man also nicht in der Messung der Grundlinien oder in der Unvollkommenheit
der verschiedenen Apparate suchen; im Gegentheil, die vorhandenen Apparate geben
sämmtlich Resultate von hoher Genauigkeit; aber nur die vorgeschlagene Vergleichung
der Apparate unter einander durch die Messung einer und derselben Grundlinie wird
diese Unterschiede, welche aus der Verschiedenheit der Maassstäbe entstehen, auszu-,
gleichen vermögen, und aus den Differenzen, welche zwischen den gemeinschaftlichen
Seiten zweier Triangulationen hervortreten werden, darf der Grad der Genauigkeit einer
jeden derselben bestimmt werden.

In verschiedenen Gegenden sollen neue Grundlinien gemessen werden. Belgien
hat die Absicht, binnen Kurzem eine dritte Grundlinie an der luxemburgischen Grenze
messen zu lassen, Oesterreich eine neue Basis bei Erlau in der Nähe von Tokai, Italien
zum Mindesten eine Grundlinie im Thale des Po, und Russland wird sicherlich ausser-
halb der beiden grossen Meridian- und Parallelbögen neue Messungen veranstalten, und
so dürfte der Augenblick nicht mehr fern sein, wo die Anzahl der in Europa gemessenen
Grundlinien ausreichend sein wird, um die Triangulation auf eine solidere Grundlage
zu bringen.

Einige Grundlinien werden der Nachmessung bedürfen; "entweder weil sie zu
einer Zeit gemessen worden sind, wo die Instrumente noch zu unvollkommen waren, oder
weil sie nur zu dem Zwecke gemessen worden sind, um eine topographische Karte des
betreffenden Landes herstellen zu können.

Zu diesen gehört vor allen die berühmte Basis von la Somma, eine der Grund-
linien des mittleren Parallelkreises. Wir erwähnen noch zum Mindesten eine von den
auf Sardinien vom La Marmora, oder auf Corsica von Tranchot gemessenen Grundlinien,
die schweizerische, die portugiesische Fundamental-Grundlinie und eine bayerische.
Endlich zeigen wir noch an, dass das französische Depöt de la guerre die Absicht hat,
die Basis von Melun nachmessen zu lassen, da dieselbe der ganzen französischen Trian-
gulation zu Grunde liegt und in enger Beziehung zum Meter, zur Borda’schen Mess-
stange No, 1 und zur Toise von Peru steht.

Wir sprechen schliesslich den Wunsch aus, dass die Nachmessungen der alten
Grundlinien sobald als möglich ausgeführt, die vorhandenen Lücken durch die. oben
erwähnten neuen Grundlinien ausgefüllt und die verschiedenen Basis-Apparate auf die
angegebene Weise mit einander verglichen werden möchten. Wenn dann sämmtliche
Messungen durch die Vergleichung mit dem neuen geodätischen Prototypmaassstabe in
ein und derselben Einheit ausgedrückt werden können, so wird der Zweck unserer Ver-
einigung in Betreff der Grundlinien und Seiten vollkommen und mit hoher Genauigkeit
erreicht sein. = :

 

 

 
