   

   

 

244

einander vernachlässigt, mehr als genügend. — Sollte das in einzelnen Fällen nicht
zutreffen, oder wünschte man aus irgend einem Grunde einen genaueren Werth der
geoditischen Linie zwischen zwei astronomischen Punkten zu erhalten, so würde ich zu
diesem Zwecke vorschlagen, aus dem vorhandenen Material die kürzeste, aus Dreiecken
der besten Form bestehende, Kette herauszuheben und für sich auszugleichen; dabei
wären, wo möglich, alle Seitengleichungen, die nur wenig zur Genauigkeit, sehr viel
aber zur Schwierigkeit der Rechnung beitragen, zu vernachlässigen, hingegen, wenn in
der Nähe der Kette sich zwei Grundlinien befinden, sie jedenfalls mitzunehmen, denn
eine zweite Grundlinie ist für die Genauigkeit des Resultats von grösserem Werthe, als
zehn Diagonalen. In diesem letzten Satze wäre denn auch meine Meinung enthalten über
p. 5° des Programms.

St. Petersburg, den 3/15. August 1877.
Ed. Forsch.

”. Votum des Herrn Geheimen Etatsrath Andrae, eingegangen den 29. August 1877.

Bei der Beantwortung der zwei sub 5° und 5b vorgelegten Fragen glaube ich
einige Bemerkungen, die allgemeine Aufgabe der Gradmessungen betreffend, voraus-
schicken zu müssen.

Es kann die Erde nur im Grossen und Ganzen als ein elliptisches Rotations-

‚sphäroid betrachtet werden. Da die Dichtigkeiten in den Schichten der festen Erdrinde

sehr unregelmässig vertheilt sind, und da die physische Oberfläche überall kleinere oder
grössere Erhöhungen und Vertiefungen darbietet, muss die wirkliche oder mathe-
matische Oberfläche der Erde, welche durch die von der mittleren Meereshöhe aus-

gehende Niveaufläche definirt wird, auch nothwendig eine ganz unregelmässige Form

haben. Es heisst daher, wie schon Gauss bemerkt hat, den Gegenstand aus einem
falschen Gesichtspunkte betrachten, wenn man nur von Localablenkungen der Lothlinie
spricht, indem solche Abweichungen überall erwartet werden müssen und nur ganz
ausnahmsweise in einzelnen Punkten verschwinden können. Alle bisherigen Grad-
messungen haben aber die Möglichkeit bewiesen, eine sphäroidische, ebenfalls von
der mittleren Meereshöhe ausgehende, elliptische Rotationsfläche bestimmen zu können,
an welche sich die mathematische Oberfläche mit ihren überall vorkommenden wellen-
förmigen Unebenheiten so genau anschmiegt, dass der Abstand zwischen beiden Flächen

‘oft nur einise Zoll beträgt und in der Regel kaum einige Fuss überschreitet. Es ist

die Aufgabe der Gradmessungen, diese sphäroidische Fläche für die ganze Erde,
oder für gegebene Theile derselben, zu bestimmen.

Die mit den Theodoliten gemessenen Winkel beziehen sich auf die Verticalen der
Stationen, das heisst auf die oben definirte mathematische Erdoberfläche. Da das Drei-

- ecksnetz nicht auf dieser unregelmässigen Fläche berechnet werden kann, projieirt man

 

x
3
=
3
a
=
=
x
x
i

tt

Lil

ak ml il

lb (tu

mal a a M A tan ad pond nn

PFEFFER |

 
