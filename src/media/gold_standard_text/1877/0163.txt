 

‘+i TARA PP |

MORIN

AL AR

Lutte Let L'ORN-TNLUAL MR (AUR ANA
NE TR BE ya 102016

pod

D’apres le plan d’exécution publié dans le rapport général pour 1876, ce nivellement
doit être terminé dans l’année 1879, et offrira des points de jonction avec la Suisse,

la
Bavière, la Prusse, la Saxe et la Russie.

Les instruments employés ont été construits
par Stampfer-Starke; les lunettes ont un grossissement de 36 fois et 3 fils hor

izontaux.
Un niveau assez grossier

est fixé à l'instrument, et sert à lui donner une position
horizontale approximative," que l’on perfectionne au moyen d’un niveau dont la sensibilité
est de 3—6 secondes. A chaque instrument sont joints 1 mire et 2 plaques de terre.
Les mires sont divisées des deux côtés, mais les zéros des deux divisions ne
coincident pas.
En \

Italie

on a entrepris, en 1876, un nivellement ayant pour but de relier les mers environnantes
entre elles et aux pays situés à la frontière nord. On travaille actuellement dans ce

but à la jonction avec les trois points fixes du nivellement suisse à la frontière des
deux pays.

En
Russie

le nivellement fut commencé en 1873 par la ligne Petersbourg—Moscou, et l'on pro-
jetait d’obtenir cette année-ci la clôture du polygone Pétersbourg—Dünabourg. Tous les
nivellements ont été exécutés deux fois en sens contraire. Les erreurs déterminées par
ces doubles nivellements et considérées pour elles-mêmes, donnent une exactitude suffi-
sante, mais elles sont presque toutes de même signe, ce qui accuse la présence d’une

erreur constante, dont on n’a pas encore trouvé l'explication. (Hypothèse que le terrain
céderait sous la mire.)

Au sujet du nivellement en
Espagne,

MM. les délégués viennent de recevoir une carte spéciale de ces nivellements, qui leur
en donne une vue d’ensemble,

MAREOGRAPHES.

L’Espagne

possede deux maréographes, l’un à Alicante pour la Méditerranée, l’autre à Santander
pour l'Océan Atlantique. Un troisième doit être installé à Cadix.

 

Kal a el nl.

 
