 

ERS ERICH ER ER

 

 

À 1 4/1 1

8

ee) Se ee

nivellirten Linien und der Häfen, wo Mareographen in Thätigkeit sind.
Welche Linien sind noch zu nivelliren und in welchen Häfen ist es wün-
schenswerth, neue Mareographen zu errichten, um die mittleren Wasser-
stände der Meere und ihre Höhen-Unterschiede festzusetzen ?

Da nach der Geschäftsordnung und nach dem Programm — als Tagesordnung
4 wird Punkt 1 und 2 des Programms verkündet — zunächst der Bericht der permanenten
Commission über ihre Thätigkeit seit der letzten Conferenz und über den Fortschritt
der Europäischen Gradmessung im Allgemeinen entgegen zu nehmen ist, giebt der 2
Präsident Herrn Bruhns das Wort, der den Bericht der permanenten Commission
erstatten wird.
Herr Bruhns verliest den Bericht. Selbiger lautet: :

La ulbu

Bericht der permanenten Commission.

wih Lau te MN

Der Geschäftsordnung gemäss hat die permanente Commission der Conferenz
Bericht über ihre Thätigkeit seit der letzten Conferenz und über den Fortschritt der
Europäischen Gradmessung im Allgemeinen zu erstatten.

Seit der letzten allgemeinen Conferenz in Dresden sind drei Jahre verflossen,
in welchen die permanente Commission sich viermal versammelte. Unmittelbar nach
der Dresdener Conferenz 1874 constituirte sich die permanente Commission, indem sie
Herrn General Ibanez zum Präsidenten, Herrn von Bauernfeind zum Vicepräsidenten,
die Herren Bruhns und Hirsch zu Schriftführern wählte. Zugleich wurde mitgetheilt,
dass die französischen Herren Commissare im Namen ihrer Regierung die permanente
Commission einlüden, sich im nächsten Jahre 1875 in Paris zu versammeln, welches
bekanntlich schon im Jahre 1872 geschehen sollte, aber durch den Todesfall des dama-
ligen Directors der Pariser Sternwarte, Herrn Delaunay, verhindert war. Die zweite
Sitzung der permanenten Commission fand demnach in Paris vom 20.—29. September 1875
im Hötel des auswärtigen Ministeriums statt, wo sämmtliche Mitglieder der permanenten
Commission anwesend waren. Im Jahre 1876 versammelte sich zum dritten Male die
permanente Commission, einer freundlichen Einladung der Königlich Belgischen Regierung
folgend, in Brüssel in der Zeit vom 5.—10. Oktober, wo nach Erledigung der wissen-
schaftlichen Gegenstände für die fünfte allgemeine Conferenz Stuttgart gewählt wurde,
nachdem die Königlich Württembergische Regierung ihre Bereitwilligkeit, die Conferenz
in Stuttgart zu empfangen, ausgesprochen hatte.

Zum vierten Male endlich hat sich die permanente Commission gestern hier
versammelt, um einige Vorbereitungen für die gegenwärtige Conferenz zu treffen.

a mme a a gy sn 11:

 
