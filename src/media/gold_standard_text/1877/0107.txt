 

A TR MY TTT YT TIT TTT

IT RT ET

FTP

ar

TL EN EUR EH D TAN

 

=]

D’Eisenach & Cobourg.

De Rheine ä Hamm.

De Hamm & Creuzthal (sur la ligne de Hamm 3 Giessen).
10. De Victorshéhe & Friedrichsbrunn.
11. De Riesa le long de l’Elbe jusqu'à Magdebourg.

Dans le courant de cet été, M. l'assistant Seibé a fait une révision des lignes
de Falkenberg à Torgau et de Magdebourg à Brunswick et continué le nivellement de
l’'Elbe de Magdebourg jusqu’à sa jonction avec le nivellement de Hambourg à Geest-
hacht. M. le professeur Dr. Bôürsch a nivelé à double la ligne de Salzberge à la fron-
tière hollandaise près de Denecamp et opéré sa jonction avec le nivellement hollandais.

Il à aussi exécuté le second nivellement de Salzberge par Lôhne et Hanovre à
Brunswick et révisé la ligne de Jerxheim 4 Kreiensen.

© ©

La longueur des lignes nivelées depuis 1874 est de 2 2. 2250 Kilom.
En 1874 on avait déja nivelé 2... 2550

9)

De sorte que la longueur totale du nivellement est de 4800 Kilom.

Le Gouvernement anglais ayant, grâce aux bons offices du gouverneur de l'ile
d'Helgoland, autorisé l'installation sur cette île du nouveau maréographe de Reitz, le
Bureau Imp. hydrographique de Berlin, qui s'était chargé de l'installation et du service
de cet appareil, a commencé les travaux nécessaires à son établissement.

L'Institut géodésique projette pour l’année prochaine un nivellement trigono-
métrique qui reliera l’île d'Helgoland à la côte en passant par les îles de Neuwerk et
Wangeroge. Le president de l’Institut à déjà. choisi les stations du nivellement et le

A ,

gouverneur de l’île d'Helgoland à autorisé l'érection des piliers nécessaires.

E. Comparaisons d’etalons.

Cette branche des travaux se trouve malheureusement encore dans la méme

situation qui a été signalée dans les comptes-rendus des séances de la Commission per-
manente a Bruxelles (pag. 9 et 38).

F. Publications de l'Institut géodésique.

Ont paru depuis la quatrième Conférence générale réunie à Dresde les publi-
cations suivantes : ?

1. Comptes-rendus des séances de la quatrième Conférence générale de l’Asso-
ciation géodésique internationale, tenues & Dresde du 23 au 28 septembre 1874, rédigés
d’après des notes sténographiées par les secrétaires de la Commission permanente
C. Bruhns à Leipzig et A. Hirsch à Neuchâtel: publiés pour servir de rapport général
pour l’année 1874 par le Bureau central, avec six cartes. Berlin, en commission chez
Georges Reimer, 1875. :

 
