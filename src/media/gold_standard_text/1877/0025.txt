Tr en ni

il a eT me nr

PURPORT TM

PER

eo

paar

1374

CNT CET

hä

ih

ze
rE
è
A
R
z=
38
=
2
®
Ë
È
<>
E
=
is

13

Ausführung dieser Arbeiten als interimistischer Sections-Chef betraut worden. Er hat
mit seiem Assistenten Dr. Westphal im laufenden Jahre zuerst auf den Stationen Cöln
und Buchholz die früheren Winkelmessungen vervollständigt und darauf auf den Sta-
tionen Donon, Sulzer Belchen (im Elsass) und Feldberg im Schwarzwalde, so wie auf
den drei schweizerischen Anschluss-Stationen Röthifluh, Wiesenberg und Laegern die
Beobachtungen absolvirt und somit die Winkelmessunsen des badisch-rheinischen Netzes
vollständig beendigt.

Die von dem Königlichen geodätischen Institute in Bezug auf die Winkel-
messungen beendigten Triangulationen bestehen aus dem märkisch- -thüringischen, dem
hessischen und dem rheinisch-badischen Netze. Die beiden ersten zwischen Berlin und
dem Taunus haben zusammen eine Längenausdehnung von 375 Kilometern (Fläche = 36000
_]Kilom. = 655 geographischen Quadratmeilen). Das rheinisch-badische Netz zwischen
der belgischen und schweizerischen Gränze ist zwischen Roermonde und Röthifluh 457
Kilometer lang (Fläche = 40000 [_]Kilom. — 736 geographischen Quadratmeilen).

©. Astronomische Arbeiten.

Im Jahre 1875:

1. Auf dem Herkules bei Cassel wurde die Polhöhe durch Messung von Zenith-
distanzen und Beobachtungen im ersten Vertikal bestimmt und das Azimuth des Meisner durch
Winkelmessung zwischen dem Polarsterne und dem Heliotropen auf dem Meisner gemessen.

2. Im weiteren Verfolge der Untersuchung über die Loth-Ablenkung im Harze
und im Thüringer Walde wurden an nachfolgenden Punkten Polhöhen gemessen:

Schildberg bei Seesen, Osterode, Hils bei Eimbeck, Langelsheim, Mansfeld,
Monraburg bei Cölleda, Dolmar bei Meiningen, Heldburs bei Coburg, Harzburg, Dienkopf
bei Dingelstädt, Craula bei Langensalza, Pfarrsberg bei Merseburg, Eckartsberga, Sachsen-
burg, Kyfihäuser, Lohberg bei Quenstädt.

Der grösste Betrag der Loth-Ablenkung ergab sich für Harzburg: + 13”51.

Im Jahre 1876:

1. Zwischen der Berliner Sternwarte und dem Pfeiler auf der Citadelle in
Strassburg wurde die Längendifferenz auf telegraphischem Wege ermittelt.

2. Zwischen der Mannheimer Sternwarte, dem Pfeiler auf der Citadelle in Strass-
burg und der Bonner Sternwarte sind ebenfalls die Längendifferenzen auf telegraphischem.
Wege bestimmt worden.

3. Auf dem höchsten Punkte des Schwarzwaldes, dem Feldberge bei Freiburg

. B. wurde die Polhöhe durch Messung von Zenithdistanzen und Beobachtungen im
an Vertikale bestimmt und das Azimuth des Sulzer Belchen unter ee des
Universal-Instrumentes und des Passagen-Instrumentes gemessen,

4. Ausserdem betheiligte sich das Institut an der Ausführung der Längen-

bestimmung Strassburg—Genf.

 

 
