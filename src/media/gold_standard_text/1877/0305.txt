I mi ve

1 RY Tee

ML MT AM LIE HU

LUI

TR LOT AE | ET FE TLE

nm]

293

werden — wahrscheinlich binnen kurzer Zeit — registrirende Pegel in den folgenden
Häfen in Wirksamkeit treten: Oscarsborg (schon seit 5 Jahren in Thätigkeit), Frederiks-
stad, Arendal, Christiansand, Stavanger, Bergen, Throndhjem (schon in Thätigkeit),
Namsos, Tromsö und Vardö. Wir beabsichtigen ausserdem solche, wo möglich, an zwei
oder drei in offener See gelegenen Inseln anzubringen. Sämmtliche Pegel durch Nivelle-
ments mit einander zu verbinden wird mit grossen Schwierigkeiten verknüpft sein und
für mehrere nur durch trigonometrisches Höhenmessen ausführbar. Immerhin wird doch
die Anzahl und die Lage derjenigen Punkte, deren gegenseitige Verbindung durch directes
Nivellement bewerkstelligt werden kann, eine befriedigende sein. Auf einer vorgelegten
Karte sind die projectirten Nivellementszüge angegeben und zwar die Linien erster
Ordnung, welche doppelt nivellirt werden sollen, roth angelegt, die Linien zweiter Ordnung
blau. Mit einem blauen Kreise sind diejenigen Punkte bezeichnet, wo Pegel angebracht
werden sollen. Im nächsten Jahre wird man hoffentlich die Nivellirungsarbeiten an-
fangen können.

Die Tabulirung und Discussion der registrirten Pegelbeobachtungen bei Oscars-
borg sind in Angriff genommen und es werden bald die vorläufigen Resultate des ersten
Jahrganges vorliegen.

Schliesslich beehrt sich die Commission anzuzeigen, dass 400 Exemplare einer
kleinen Uebersichtskarte*) der norwegischen Dreiecke in diesen Tagen an das Central-
bureau abgeschickt worden sind.

Christiania, d. 29. Juni 1877. Fearnley.

Dem Berichte des Professor Fearnley habe ich, was die Arbeiten der Grad-
messungscommission betrifft, nur hinzufügen, dass die darin erwähnte Verbindung mit
Schweden durch Beobachtungen an den 4 Stationen, Follahögda, Haarskallen, Hervola
und Kjölihougen ausgeführt ist. Dagegen ist es mir ein Vergnügen, die Ausdehnung
unseres Netzes bis zu dem nördlichsten Punkte Norwegens in Aussicht stellen zu können.
Wie bekannt war anfangs i. J. 1862 nur von der Messung eines Meridianbogens von
Palermo bis Christiania die Rede, dieser Bogen wurde jedoch schon beim ersten Berliner
Congress i. J. 1864 auf Veranlassung des jetzt verstorbenen Direktors der Sternwarte
Herrn Professor Hansteen bis nach Levanger verlängert. Es konnte auch damals von
einer grösseren Ausdehnung unseres Netzes der grossen Kosten wegen wohl kaum die
Rede sein, da die allgemeine Landestriangulirung, die der Gradmessung eine helfende
Hand hätte reichen können, sich damals noch im südlichen Theile des westlichen Norwegens
bewegte. Als mir aber vor zwei Jahren die Leitung der Triangulationsarbeiten anvertraut
wurde, waren diese schon so weit gegen Norden fortgeführt worden, dass ich sie an

 

*) Die Karte wird mit dem Berichte vertheilt.

 
