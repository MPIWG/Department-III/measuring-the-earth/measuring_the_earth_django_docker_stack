Se

Prt : TE

in!

pa Lui

aber aha an N à ae
TT RTE ai

187

Pendule suspendu par le couteau le plus rapproché
du centre de gravité.

oo,

INSTANT MOYEN INTERVALLES RÉDUCTION

ta ; INTERVALLES CORRIGES
de dix passages de 298 oscillations à l'arc infiniment petit

|
SSS A SSS SSS SSS SS SSS SP SAE

 

15> 53" 22.4041

 

58 22.3579 29939538 — 00198 299°9340
16 3 22.3058 299-9479 0.0031 | 299.9348
6 22725381 299.9473 | == 0-0087 | 299-9386
13 222119 299.9588 | —0-0062 299.9526
18 22-1554 299-9435 | — 0:0044 299.9391
23 22.1011 299-9457 |  —0-0031 299.9426

En moyenne T, = 1.0065104.

Ainsi nous trouvons:
IT, =101271%4
I’, = 10952

= — 1.012445

Correction diurne du chronomètre + 2859 . + 0.000060

Temp. bout pesant .eñi bas 148187 3. .. \ __ 0.000010
„. bouf pesant en hau 19/00, J

Te à 13° 1.012495

ee

En comparant cette valeur avec celle que nous avons obtenue avec l’autre sup-
port, nous trouvons une différence de 0.000198. La différence selon le calcul des
expériences sur la flexion aurait dû être de 0.000191, ce qui présente une concor-
dance suffisante.

Acceptez, cher Monsieur, expression de mes sentiments dévoués et reconnaissants.

C. S. Peirce

Assistant U,S. Coast Survey.

 
