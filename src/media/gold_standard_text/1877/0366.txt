 

40 | MOUVEMENT SIMULTANE

5 Expériences faites à Genève, le pendule étant posé sur le grand pilier

en mollasse, et le comparateur étant en place.

 

 

Espace Valeur

blanc

 

y | a | Q | Equations de condition

 

calculée.

€

 

or eENEn EErnEnEnnnEnsunn

13 novembre, de 2" 4 4" aprés midi. a = 2™,695 D = 8723mm grossissement 3236.

Moyenne de deux observations pour chaque espace, faites en parcourant Véchelle en sens opposé.

 

 

Nee D B Pr B
3 | 4,388 (S255) k = 0510 k’ = 2,979 2,295 + 0,016
To" 3,634 109056) k - 0817 k’ = 2,242 2,264 + 0,022
4 | 3,399 15025 k == 0.6655 ko = 2,262 2,248 -— 0,014
9. 1.3,054 eg |) 6 © Dos) Ke = 2 2225) 2.9285 | = 0,006
16 12,417 : 1108 | k= 0.9065 kK = 21045) 2,1715 | © 0,020
il) 2,1215 0,9845| k — 1,016 k’ = 2,155 2,136 — 0,019
8 | 1,802 002) k = 160! ke = 2,091 2,090 20001
12 | 1,502 Oe k — ieee k = 2.0515) 2,0245 | — 0,027
13, | 1,190 O65 | kK 1,00 ki = 1,903 1,950 004
14 | 0,947 Do k | 19455 k = 17843 1,839 2:00 001
équations finales 10 k —10,5235 k’ —21,2405 écart moyen = 0,018
—{0,5235k 413,035 k’—-21,726 Te 2,2 009
lp PB 8
d’où k —2,461 poids 1,504 erreur moyenne 0,020
k' —0,3195 » 1,960 > 0071 0 0180

Wexperienee statigne avait donnd k == 24,683 par l'action d'un poids de 175 suspendu

pendant 40°.

45 novembre, de 2h à 4ù après midi. a — 3"m061 D—8723mm grossissement 2849.

Moyenne de deux observations pour chaque espace, faites en parcourant l'échelle en sens opposé.

 

ve p u p P
7 4,078 1.1079) k — 0,586 k — 2,388 2,301 —- 0,003
4 3,814 1,5805) k — 0,688 k = 2,443 2,374 — 0,039
9 3,427 1,4525| k — 0,6885 k’ — 2,359 2,355 — 0,004
0 112.713 1,1805| k — 0,847 k’ — 2,298 2,299 + 0,001
11 | 2,381 1,0525| k — 0,950 k’ = 2,262 2,263 +. 0,00!
8 2,023 0,95 k — 1,084 k’ = 2,193 2,216 + 0,023
12 1,685 Oo, le ee bb ke 2,1185 2,156 + 0,040
13 | 1,335 DOOD KL 14818) L = 2,098 2,064 + 0,041
14 | 1,062 0,523 | k — 1,876 k’ = 1,998 1,937 2101088; |
équations finales 9 k — 9,4345 k’ =20,0465 écart moyen + 0,023
— 9,4345 k 111,403 k’=-20,4825 ae i Se 00385
I u 7
doük = 2,597 poids 1,195 erreur moyenne Æ 0,032
k’= 0,3517 » 1,518 > 0,029 Q’ = 0,138.

 

 

 

 

L'expérience statique avait donné k = 29,844 par l'action d'un poids de 1Kilo8 suspendu pendant 40°.

 

 

 

x
=
=
à
a
=
è
=

4884/1188

ae

ju hall ak u a mn à:

 
