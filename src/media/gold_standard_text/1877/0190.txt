 

 

178

Les quantités calculées supposent que l’axe coupe le niveau du plan de suspen-
sion & une distance de 17355 derriére l’extrémité antérieure du plan de suspension.

B. Expériences faites dans la verticale du bout antérieur.

Hoboken, 12 mars 1877. Température 149 C. Observateur le sous-assistant Surrx.

 

 

 

POSITION DE L'ÉCHELLE FLEXION EN RÉVOLUTIONS

relativement de la vis micrométrique
au niveau de la suspension A i er
— au-dessous, — au-dessus observée | calculée

|

 

 

 

— 0m44 + 0.196 + 0.196
0.000 + 0.340 | + 0.332
+ 0.395 + 0.446 | + 0.454

Les quantités calculées Supposent que l'axe coupe la verticale du bout antérieur
du plan de suspension à une distance de 107 au-dessus du niveau de ce plan. Il n'y
a rien de surprenant à ce que l’axe instantané soit au-dessus du plan de suspension.
Supposons, en effet, que la flexion demeurât exclusivement dans les trois pieds du sup-
port. En ce cas là, le mouvement du bout supérieur de chaque pied serait perpendicu-
laire au sens général du pied, et, en même temps, perpendiculaire au rayon du cercle
de révolution, de facon à ce que le pied se dirigerait directement vers l’axe fixe. L’axe
est sans doute en arrière du support à cause de la flexion du plan lui-même.

J’ai fait à Genève, à Paris, à Berlin et à New-York des expériences pour dé-
terminer la valeur numérique de ©. L’expérience de Geneve, faite le 13 septembre
1875, n’était qu'un essai. Mais j'avais une bonne poulie, que j'avais empruntée à l'atelier
de la Société Genevoise pour la construction d'instruments de physique, et j'ai obtenu
comme valeur approximative

> — OO
La poulie, dont je me suis servi à Paris, avait un frottement très considérable,
circonstance à laquelle on peut attribuer le fait, que les nombres trouvés s’ecartent sen-
siblement de ceux que j'ai obtenus à l’aide de meilleurs appareils. Voici les chiffres :
Le 18 janvier 1876, chez M. M. Brunner (Tempéra-
D 5 00863 -
Le 7 mars 1876, à l'observatoire de Paris (lempé-
Bie OO = 0.0011.

A Berlin, j'ai fait usage d’une poulie très délicate, qui tourne, sur de grands

galets, afin de diminuer le frottement. Elle appartient au cabinet de physique de l'Institut

M4

 

=

res TO DIT baa edi

lh

ak nn Ws

Wie Peet)

sth ba as Ala bd lly ye
