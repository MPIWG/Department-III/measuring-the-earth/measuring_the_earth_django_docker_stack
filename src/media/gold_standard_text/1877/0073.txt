 

DL emo

oyna niet

ITR

A da eh

ii hi

i
ik
SE

5
25
A
a
:
i
44
+

61

In der Praxis lässt sich die Einstellung mittels der Mikrometerschraube viel
feiner ausführen, als durch die Schraube, mit welcher die ganze Alhidade gedreht wird;
welche weniger empfindlich ist und trotz der Gegenwart einer Druckfeder plötzliche
Sprünge befürchten lässt, die nach einer einmaligen Einstellung hervortreten und auf
die Ablesung nachtheilig einwirken. Ein solcher schädlicher Einfluss ist aber bei mikro-
metrischen Einstellungen nicht zu erwarten.

Zu bemerken ist noch, dass der von der Schraube benutzte Theil ein sehr ge-
ringer ist, da nur die Ablesungen an der Mikrometerschraube des Okulars immer auf
den Nullpunkt der Collimation redueirt zu werden brauchen und alle Einstellungen auf
einen oder höchstens zwei Theilstriche der Trommel fallen. Bei dem Kreise des Depöt
de la guerre ist die Höhe eines Ganges der Mikrometerschraube gleich 0700033 und

der benutzte Theil der Schraube beschränkt sich auf = eines Millimeters. Man kann

50

annehmen, dass die Schraube in einem so kleinen Intervall regelmässig ist, ausserdem
kann die Untersuchung mit der grössten Genauigkeit mittels des Instrumentes selbst
ausgeführt werden. Bei der Anwendung von Mikroskopen mit Mikrometern für die Ab-
lesung und bei dem Gebrauche eines beweglichen Fadens, welcher die Wiederholung der
Einstellungen gestattet, werden die beiden zufälligen Fehler der Ablesung und Einstellung
für eine beobachtete Richtung in einem gegebenen Moment für jeden auf ohngefähr
+ Secunde herabgedrückt.

Mit gut getheilten Instrumenten würde also eine kleine Anzahl von Beobachtungen
ausreichen, um die höchste Genauigkeit in den Winkelmessungen zu erreichen. Aber
der Einfluss der Atmosphäre und die Strahlenbrechung, durch welche die Lichtstrahlen
abgelenkt werden, bringen laterale Fehler in den einzelnen Messungen hervor. Das
einzige Mittel, die durch die Atmosphäre veranlassten Fehler zu beseitigen oder abzu-
schwächen, besteht darin, dass man dieselben als zufällige Fehler betrachtet, die Beob-
achtungen an verschiedenen Tagen und zu verschiedenen Stunden, bei den verschieden-
sten Zuständen der Atmosphäre oft wiederholt und sich die Bedingung stellt, Strahlen
zu vermeiden, welche die Erdoberfläche oder Gegenstände auf derselben streifen.

Aber diese Bedingungen können nicht vollständig erfüllt werden, denn die
Heliotropenlichter sind während des grössten Theils des Tages unbestimmt und stark
schwankend, so dass sie nicht mit Sicherheit eingestellt werden können. Der Beobachter
an einem Fernrohre mit festem Fadennetze ist genöthigt, die wenigen Tagesstunden
abzuwarten, wo die Bilder klein und hinreichend begränzt erscheinen.

Hierbei ist noch zu erwähnen, dass die Anwendung des beweglichen Fadens
einen unschätzbaren Vortheil darbietet, indem es dadurch möglich ist, selbst bei ver-
waschenen und unruhigen Bildern die Einstellungen zu vervielfältigen und dabei die
Einstellungen auf die Mitte des Bildes mit einer Schärfe zu erreichen, welche man von
einer einmaligen Einstellung nicht erwarten kann. Es wird also dadurch die Dauer der
Beobachtungszeit wesentlich verlängert.

Um die Beobachtungen bei den verschiedensten atmosphärischen Zuständen zu
vervielfältigen, haben wir auch Nachtbeobachtungen zu Hilfe genommen, welche seit 50
Jahren in der practischen Geodäsie in Europa verworfen worden sind.

 

SER pee

 
