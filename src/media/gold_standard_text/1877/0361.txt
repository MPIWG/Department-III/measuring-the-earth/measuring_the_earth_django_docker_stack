a PTT |

am au nn

a m

li!

PT Tr ARE BR |

D'UN PENDULE ET DE SES SUPPORTS.

30

4° Expériences faites à Genève, le pendule étant placé sur le support en bois
et le comparateur étant en place.

 

| Espace
| blanc.

 

     

 

 

 

 

 

   

 

Equations de condition

A. Par les oscillations du pendule.
9 novembre 187%, de 7" 1}, à 101 soir. a — 2mm,720 D— 8693", grossissement 3196.
Moyenne de trois observations pour chaque espace, le pendule étant suspendu par le couteau

le plus éloigné du centre de gravité.

le plus rapproché du centre de gravité.

Valeur
calculée

 

 

i

 

Ne p P P

3° | 4,443 | 142.38 | 1,643 | k — (ok - 20: 2.121 + 0,017
7 3.680 | 120,7 12394) k — 0 1117 = 2/00 2,645 + 0,005
4° 19,442 | 1130| 135 | k 0! k = 2 aes 2,611 — 0,027
9°) 3,092 | 103,0 | 1,1895| k= Ost kk —- 2500 2,558 — 0,042
10 ‘| 3,448 | 89.0 | 1,028 | k 00m k = 2002 2,465 — 0,083
11 2,148 75,0 |0866| k-—- Lii5 k - 2209 2,363 — 0,040
8 | 1,895 | 67.0 054 | k= fo ek _ 22% gore — 0,003
19 1 51 60:3 | 0,606) k = Pre — 20 2 152 = 0,007
13° | 4,205 | 52,7 | 06086 k 1a E _ i Os 2,001 + 0,033
14° | 0,959 | "440 | 0508) k=— 1,905 ko = ven 1,811 = 0,016

Moyenne de quatre observations pour chaque espace, le pendule étant suspendu par le couteau

: NB. Pour les cinq derniers espaces, observés dans les deux modes de suspension, on a formé les
| équations de condition, en prenant pour Q la moyenne des valeurs obtenues dans les deux posi-
| tions pour le même espace et pour la même valeur de y.

 

11 9,128} 149 0) 0,928
8 | 4,825 | 134,0 | 0,830
12 1,521 | 115.0 0.713
13 | 1,208 | 99,2 | 0,615
14 0,959 87,4 | 0,5415]
équations finales 10 k —11,230 k° —23,597 écart moyen + 0,027
In
11,230 11.215 ki---25,352 4 204 h 0010
! 8

D:
d'où k —3,149 poids 1,147 erreur moyenne 0,037
en 161 » 40.03 0 =0,223

 

 

 

 

le plus éloigné du centre de gravite.

 

L'expérience statique avait domné k = 3y,676 par l'action d'un poids de 176 suspendu pendant 40s

10 novembre, de 2% 4 4% aprés midi. a = 3"",003 D=8693"n, grossissement 2895.
Moyenne de quatre observations pour chaque espace, le pendule étant suspendu par le couteau

N° U 4 | Ie? D P-

7. | 4062 |,124,95) 144984) k= 0691 K 23 2 852 — 0,021
À | 3.800 446,15 1 348) k = 070 x 28 2,830 — 0,011
9 | 3.444 11065 | 1,280.) k- 8 k 2008 2,797 — 0,021
10 | 2,702 | 88,5 | 1,022 | k= 00785 k — 201 2718 -- 0,094
11 | 2,372 | 78,75 0,878 | k — 1,143 k’ = 2,711 2,641 == 0,070
g | 9,005 | 6555 | 0,756 | k — 1,222 K 200 2,555 20.199
19 | 4,679 | 58.0 | 0,670 | k- Lie K 20 2,474 0.088
13: | 4330 L 49,0 0,566 ı k  Nıoı k 290 2,344 — 0,006
14 | 1,089 | 4205 0491| k 20008 KM 205 2,222 0.007

 

écart moyen + 0,047

ee 7 ze —— 0,0655
7

équations finales 9 k —10,9815 kK’ =23,447
“40,9815 k -b 15,1605 K = 21,119

 

D.
d'où k =3,182 poids 1,048 erreur moyenne 0,064
k =0,474 >» 1,169 » 0,049 0 —0,140
L'expérience statique avait donné k = 34,620 par l'action dun poids de 475 suspendu pendant 405

 

 

prepa er re er

 

PE

 

 
