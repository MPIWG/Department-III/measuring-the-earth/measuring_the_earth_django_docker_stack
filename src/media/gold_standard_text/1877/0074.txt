 

 

62

Uebrigens ist noch zu erwähnen, dass gute Bilder, welche sich bei Tage nur in
den Momenten zeigen, wenn die Luftschichten der Atmosphäre im Gleichgewichte sind,
auch bei Nacht erscheinen, dass aber dann die Perioden des Gleichgewichtszustandes
viel länger dauern, als bei Tage.

Die ersten Resultate, welche wir erhalten haben, sind-auf der Conferenz in
Paris im Jahre 1875 mitgetheilt und in den General-Bericht (Seite 144 ff.) aufgenommen
worden; wir werden uns begnügen, dieselben hier auszugsweise zu wiederholen.

Die angewendeten Licht-Signale sind optische Collimatoren, in deren Brenn-
punkte man das Licht einer Petroleumlampe concentrirt. Das beleuchtete Objeetiv der
Collimatoren ist auf grosse Entfernungen sichtbar, erscheint wie eine einfarbige Lichtkugel
mit scharfer Begränzung und lässt sich leicht und sicher biseciren.

Um Vergleichungen zu gewinnen, haben wir in der Sommer-Campagne von 1875
auf allen 10 Stationen, welche in einem mässigen Gebirgslande von 200 bis 750 Meter
Höhe lagen, sowohl Beobachtungen bei Tag als auch bei Nacht angestellt. Jede nächt-
liche Reihe ist nur die Wiederholung einer Tages-Reihe, je zwei zusammengehörige
Reihen sind identisch, indem sie sich in Betreff des Instrumentes und des Beobachters
nicht von einander unterscheiden. ‚Nur die Beschaffenheit der anvisirten Objecte und
die Beobachtungsstunden waren verschieden.

In Gemeinschaft mit Capitain Bassot haben wir gefunden, dass der mittlere
Fehler einer einmaligen Beobachtung, wie er aus dem Mittel von 37 beobachteten
Richtungen erhalten worden ist,

bei Tag-Beobachtungen — a2 1 49

iff, = 0:02
bei Nacht-Beobachtungen = = 1.47 Dit — 0:02,

und dass das Mittel des wahrscheinlichen Fehlers einer endgiltigen Richtung, welche aus
der Beobachtung von 20 Reihen hervorgegangen, in beiden Fällen = # 0.23 ist.

Die Unterschiede, welche sich bei einer Richtung zwischen den bei Tag und bei
Nacht gefundenen Resultaten aussprechen, treten bald in dem einen, bald in dem ent-
segengesetzten Sinne auf, sind im Allgemeinen klein, und für zwei Richtungen zusammen
immer kleiner als 0°3.

Die Berechnung der Dreiecke hat uns ein Mittel fiir die Untersuchung gegeben,
wie weit die bei Tag und bei Nacht beobachteten Richtungen den geometrischen Bedin-
gungen der Triangulation geniigen. Wir haben dabei gefunden, dass der Schluss-
fehler der Dreiecke

bei Tag-Beobachtungen zwischen —0!38 und + 1'13
bei Nacht-Beobachtungen zwischen — 0.19 und + 0-77

liegt. Das Mittel der Schlussfehler von 8 berechneten Dreiecken ist dem absoluten
Zahlenwerthe nach .

0:63 für die Beobachtungen bei Tage,

Odile et ‘i Nacht,

 

x
=
3

=

=

a

a
=

È

=
=

x

4

ak ini

UE run

Ian vl ha m dd m 11

2. ssmmitésunt, vamité SA |
