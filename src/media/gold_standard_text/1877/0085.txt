 

en 7:

ee Re EI

APP ETAT

NT

OTL RN TR RRR ET HOTTIE MITT °F RU 1 TF

13

langen liess, in dankenswerther Weise von denselben entsprochen worden. Wenn Referent
Wünsche aussprechen soll, so sind es folgende 2, welche das Nachbarland Baden betreffen:

1. Es ist wünschenswerth, dass das Präcisionsnivellement, welches die General-
direction der badischen Eisenbahnen ausgeführt hat, wissenschaftlich bear-
beitet und publieirt werde.

2. Es möge bei Wiederholung des Nivellements des Centralbureaus die für das
Bodensee-Polygon noch fehlende Strecke Constanz—Friedrichshafen nivel-
lirt werden.

Was endlich die besonders für Süddeutschland, wo jedes Land einen andern
Horizont hat, ja in einem Lande (Württemberg) bis vor Kurzem dreierlei Horizonte
gebraucht wurden, höchst wichtige Frage des definitiven Nullpunktes betrifft, so wird
dieselbe noch so lange zu vertagen sein, bis von den erst aufgestellten Pegeln Resultate
vorliegen werden.

Präsident Zech ertheilt, bevor über die beiden letzten Anträge des Herrn Schoder
abgestimmt wird, Herrn Betoccht das Wort, der einige Mittheilungen über die in Italien
benützten Mareographen zu machen wünscht.

Herr Betocchi beschreibt die in Italien angewandten Mares ae legt Zeich-
nungen eines solchen von William Thomson construirten und in Genua aufgestellten In-
strumentes vor, und bespricht die erhaltenen Resultate, besonders mit Rücksicht auf
Hebung und Senkung der Küsten; er legt eine kleine Abhandlung vor, die im Berichte
aufzunehmen beschlossen wird. ”)

Herr Hirsch hat zu dem Berichte des Herrn Schoder die kurze Bemerkung zu
machen, dass der Unterschied zwischen dem mittleren Niveau des Atlantischen Oceans
und des Mittelländischen Meeres in Marseille nicht so sicher ist, wie Herr Schoder auf-
geführt hat. Denn das mittlere Niveau des Mittelländischen Meeres beruht auf einer allzu
unsicheren Basis, so dass die von Herrn Breton de Champ in Paris angegebene Niveau-
Differenz von 75em. sich vorläufig nur auf einen ungenügend fixirten Nullpunkt im hinteren
Hafen von Marseille bezieht, dessen Identität mit dem mittleren Niveau des Mittel-
meeres noch keineswegs erwiesen ist. Es wäre vor Allem zu wünschen, dass in Marseille
selbst ebenfalls ein Mareograph aufgestellt und das wahre Niveau des Meeres dort
wissenschaftlich‘ ermittelt werde.

Herr Perrier erwidert, dass genaue Untersuchungen darüber angestellt werden
und das definitive Resultat bald ermittelt sein würde.

Herr Nagel bemerkt zu dem Berichte des Herrn Schoder, dass die Genauigkeit
des sächsischen Nivellements von seinem verstorbenen Collegen Wersbach nur aus zwei
Polygonen abgeleitet ist, und bei der Genauigkeit der Nivellements im ganzen Lande gegen
70 Polygone mitzusprechen haben. Er ist noch nicht zum Abschlusse mit der Ausgleichung

*) Siehe Anhang II. zu den Verhandlungen.
10

 
