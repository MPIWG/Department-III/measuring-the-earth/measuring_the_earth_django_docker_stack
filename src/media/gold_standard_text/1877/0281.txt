En sn, pet

Wf

Poe ee vs ee en

IE RM 0d

WEL A

ALR UA RIT EN

mer

269

Warnemünde 2... 2. — 0152 | Colbergermiinde.... — 0.145

Stralsunds. do 0 10 Stolpmünde. 4h44 0104

Wiehe -oosmcvlea fe ee Neufahrwasser .... —0.014

Swinemünde . . es —0.048 À Pillau i icicle eee a Oe
Mémel st, ER SE

31. Août 1877.
Le Major: Æ. Adan.

Oe

Dänemark.

Im letzt verflossenen Jahre ist die erste Abtheilung des vierten Bandes der
Gradmessung, die Bestimmung der Längendifferenz zwischen den Sternwarten in Copen-
hagen und Altona enthaltend, beendigt, und der schon früher angefangene Druck des
dritten Bandes fortgesetzt worden. Die Veröffentlichung dieses Bandes ist durch die
Vollführung des Druckes der erwähnten Längenbestimmung etwas verspätet worden,
wird aber jetzt in verhältnissmässig kurzer Zeit stattfinden können. Im vorigen Jahres-
berichte habe ich schon die definitiven Resultate der dänischen Messungen angegeben,
erlaube mir aber hier dieser Mittheilung einige Bemerkungen, die auch allgemein auf
alle ähnlichen Resultate geodätischer Messungen angewendet werden können, beizufügen.

Es ist die Aufgabe der geodätischen Messungen die relative Lage sämmtlicher
Dreieckspunkte auf der sphäroidischen Erdoberfläche zu bestimmen. Wenn die directen
Resultate der Messungen, bevor ihre Combination mit den astronomischen Beobachtungen,
durch die absoluten Lagen auf dem Sphäroid, etwa mittelst der berechneten geo-
dätischen Breiten und Längen aller festgelegten Punkte, angegeben werden, kann man
offenbar nicht diese Coordinaten selbst als definitive betrachten. Sie sind nämlich
nicht reine Ergebnisse der Messungen, indem die Berechnung nothwendig auch mehr
oder minder angenäherte Werthe für die Dimensionen des Sphäroids, für die absolute
Lage eines Ausgangspunktes und für das Azimuth einer ersten Dreiecksseite anwendet,
und wenn man später die, aus einer Verbindung der geodätischen und der astronomischen
Resultate hervorgehenden, definitiven Werthe dieser Grössen°einführt, können auch dadurch
beträchtliche Aenderungen der Coordinaten bewirkt werden. Bei zweckmässig gewählten
Ausgangswerthen und wenn der Umfang der Messungen nicht überaus gross ist, kann
man aber stets behaupten, dass diese Aenderungen für die Bestimmung der relativen
oder gegenseitigen Lage der Punkte gar keine praktische Bedeutung haben. Die
aus den vorläufigen Coordinaten abgeleiteten, verschiedene Punkte verbindenden geodä-
tischen Linien, und die zwischen diesen Linien gebildeten Winkel sind daher als un-
abänderliche und definitive zu ‘betrachten. Wenn diese Unabänderlichkeit nicht
wirklich existirte, wenn die erwähnten Linien und Winkel erst aus einer Berechnung
des Dreiecksnetzes mit den definitiven Ausgangswerthen bestimmt werden könnten,

 
