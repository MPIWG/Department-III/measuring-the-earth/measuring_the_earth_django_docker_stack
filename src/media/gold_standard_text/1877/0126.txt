 

 

 

 

 

 

 

 

 

 

 

 

 

 

114
5 | | | |
„nee In ea, | Réduction du ca-| |
Déclinaison Vierteljahrsschrift Erreur Déclinaison ee Erreur |
1875.0 ee probable | 1875.0 | Dons | probable |
géodésique | En
0 l ” | ai 11
mode | 034. orks oft 6
— 2 | + 0.38 = 0.09 6
+ 3 14 + 0.42 =, 0.107 5
+86 + 0.34 = 0.07 10
+ 12 52 + 0.21 10.07 8
rn; + 0.49 ab 0408 8
+ 22 13 + 0.37 0.23 6
+27 36 | + 0.46 3204.16 8
+ 33 28 | + 0.21 = 0.08 7
ra | m = et ? Réduction du os
no D Po. = 0.10 ie Déclinaison |talogue de Safford) Erreur
a7 55 aie 0). 80 a=, 0 10 13 au catalogue de |
+ 52 23 + 0.58 26 0. 08 13 1875.0 | Ja Vierteljahrs- | probable
+ 57 44 | + 0.65 a= 0410 12 pe |
+ 62 5 + 0.32 fake On 12 ao ah
+ 66 47 + 0.19 ale 0,32 2 |
+71 53 + 0.55 “© 08 3 Lao!
+ 76 32 — 0.35 a 0.47 2 0,
+ 82 14 + 0.22 — I Se 0.
+ 88 4 — 0.19 Org 3 or
| Jo,
en ©.

 

   

 

 

Pour pouvoir tirer parti de ces réductions, il nous a semblé & propos de traiter
séparément les déclinaisons comprises entre —10° et + 30°, et les déclinaisons com-
prises entre + 30° et + 60°. Dans le premier intervalle nous n’avons en effet que
des étoiles des catalogues de la „Vierteljahrsschrift“ et de l'Association géodésique,
tandis que dans le second nous avons des étoiles des trois catalogues. Nous avons
complètement renoncé à utiliser les valeurs obtenues pour les réductions des déclinaisons
comprises entre + 600 et + 900, d'abord parce que cela ne nous a point semblé né-
cessaire, et ensuite parce que le nombre des réductions correspondant à cet intervalle
était trop faible.

Pour ce qui concerne d’abord la zone de — 100 à + 30°, on peut sans autre
employer pour la réduction du catalogue de la „Vierteljahrsschrift“ au catalogue de
l'Association géodésique la valeur + 0/38, moyenne arithmétique des moyennes isolées
contenues dans le tableau précédent. Les écarts qui subsistent en adoptant cette valeur
moyenne et dont les valeurs sont: — 0:04, 0.00 + 0.04 — 0.04 — 0.17 + 0.11 — 0.01
et + 0.08, ne trahissent en effet aucune périodicité, et sont tous compris dans les
hmites des erreurs probables.

Dans l’autre zone, par contre, qu'il nous a fallu réduire à l'intervalle compris
entre les limites + 35° et + 60°, parce qu’il n’existait entre les déclinaisons de 30°
a 35° que deux étoiles pour établir la relation du catalogue de Safford au catalogue de

 

 

=
=
>
=
~
à
a
=
:
=
a
=
x

ann

Wb tet ur

ju th en hs AM ei ld adil tn sie 11:

2.4 rinnen mein Wh |
