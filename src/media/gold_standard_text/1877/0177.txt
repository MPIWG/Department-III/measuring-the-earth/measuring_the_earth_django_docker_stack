HR a

nm Dern ee me |

(dM OLIPL

a RT

PP OPERA AR B/S FT

 

  

 

 

165

eee

en nommant C le moment d’inertie du pendule par rapport au couteau, on aura Zr? dm
— C, et l'équation prendra la forme:
d9

ae | mgh sind + w.

C a
En posant | = —, on pourra l'écrire :

d29 qe u
mar Sin @ + me

di?

\

se trouve au § 4 d’un mémoire sur le

Cette équation coincide avec celle qui
e de Geneve, t. XVII, 27°

pendule inséré dans les Mémoires de la Société de Physiqu
partie. Le nombre y" est supposé très-petit, fonction du temps, et dû à une action
troublante quelconque. Nous ne répéterons pas ici les calculs, basés sur la méthode de
la variation des constantes arbitraires, par lesquels on trouve l’altération correspondante
de la durée d’oscillation et de l'amplitude, et nous nous bornerons à définir les lettres

x est à chaque instant l’amplitude du mouvement qui se réaliserait si l’action

employées :
troublante venait à cesser; & est un angle défini par l'équation cos 9 = COS a COS 2p + sin 79,
_: gi l’on nomme

de sorte qaw’il croît de o à 7 pendant une oscillation: wu représente ae à

2 mg Sin À «
ation de sorte qu’elle soit augmentée dans le
amplitude pendant une oscillation, on trouve:

 

 

à l'accroissement relatif de la durée doscill
rapport de 1a1-+ 0, eta, la diminution de I
: fa ucosddd, a, = 5; wu tang (a sind dd, en négligeant les termes de

th
l'ordre de w2.
En substituant les valeurs de p’ et de u,

GY

il en résulte, dans le cas actuel:

iI eh ne d?xı d2yı | cose
D — à | sin Be COP aaa (Er d o.
les valeurs de x,, y, en fonction du temps,
mouvement dû à la pression du

 

Dans cette formule il faut substituer
et pour cela chercher le vrai mouvement du point 0”,

pendule sur le support.

Pour cela reprenons les deux premières équations du mouvement, Savoir:

d? x . d?y
BER NSS NE \ A aa .
Zn IM X=.—P, x I da.

aux axes fixes étant æ, + X COS 6,

 

les coordonnées du centre de gravité par rapport
y, + h sin 0, ces équations pourront s’ecrire:

en d2.(cı + h cos 9) d2.(yı + h sin 9)

P et Q ne sont point très-petites, tandis que le balance-

Ces valeurs des pressions
eur est uniquement dû, est imperceptible.

ment des supports, ou le mouvement Co quel
Elles n’entrent donc dans les valeurs rigoureuses de 21, Y, qui sen déduiraient, qu’af-
fectées de très-faibles coefficients; par suite, dans P et Q elles-mêmes nous devons
ou les termes de l’ordre de l’action troublante, qui n’en produiraient

négliger 21, Y;
Nous aurons donc simplement:

dans à que de l'ordre du carré de cette action.

 
