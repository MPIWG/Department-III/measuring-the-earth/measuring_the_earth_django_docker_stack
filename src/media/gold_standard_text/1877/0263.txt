Di en mess nu

“RFF TAT Te en

a RT AM Kl

Kl

il

il

rg!

a

251

In den beiden mittleren Gruppen II und IH, wo aus jeder Grundlinie (gs
und 9,) zwei Anschlussseiten abgeleitet werden, kann man nicht die entsprechenden
Fehler der beiden Seiten als unabhängige betrachten, indem sie so gut wie immer aus
zwei Theilen, einem gemeinsamen und einem abgesonderten, zusammengesetzt sind. Die
Berechnung aus der gemeinsamen Grundlinie wird nämlich nicht durch zwei abge-
sonderte Dreiecksreihen geführt, sondern fängt in der Regel damit an, sich durch eine

kleinere oder grössere Anzahl gemeinsamer Dreiecke zu bewegen. Wie in der Figur
angedeutet sollen daher auch:

mit f, +f, und /;, + f, die Fehler in den Werthen a, und b,, sowie:

mit 7; +/, und f, +f, die Fehler in den Werthen 5, und c, bezeichnet
werden.

Die strenge zu erfüllenden Gleichungen beschränken sich offenbar hier auf
folgende drei:
ee ee,
laden, al
ee = Ge

oder einfacher geschrieben, indem man die Differenzen: log a, — log a,, log b, — log bi,
log ec, —log ce, resp. mit d,, d, und d, bezeichnet, und diese Grössen sowie sämmt-
liche fi, fs -..: /s, die nun auch als Fehler der entsprechenden Logarithmen aufzu-
fassen sind, in Einheiten der nt", etwa der 7" Decimale der Logarithmen ausdrückt:

a; SK ep
d, = ip Prag Mapa pole oben à (7)
d, = tl a ee

Mittels einer sehr leichten Correlaten-Ausgleichung werden nun die Fehler nach
der Methode der kleinsten Quadrate bestimmt, indem man dabei die bekannte Quadrat-
summe [p/ff| auf ein Minimum reducirt. Zur vollständigen Durchführung dieser höchst
einfachen Lésung bleibt es somit nur noch tibrig zu zeigen, wie die den Fehlern:

Kaas han fis
entsprechenden Gewichte :

Dis Doo Ue ee. oe
festgesetzt werden miissen.

Es genügt hier sich der gewöhnlichen Schätzung zu bedienen, nach welcher man
das Gewicht einer, mittels einer Reihe von » Dreiecken berechneten Seite, der Grösse }

n

proportional ansetzt. Wenn die Fehler: f,, f, ....f. respective durch: »,,#, 2%
32*

Rs

 
