149

Pulkowa. A= —30° ı9

Die Beobachtungen in Pulkowa zur Bestimmung der Breitenvariation sind zwar unter Wechsel des
Beobachters am Passageninstrument im IL Vertikal fortgesetzt worden, doch konnten die Resultate
derselben zur Zeit noch nicht abgeleitet werden.

In betreff der älteren Beobachtungsreihe hat der Director der Pulkowaer Sternwarte, Herr
Wirkl. Staatsrath Dr. Backtunp, dem Centralbureau mitgetheilt, dass inzwischen Herr Dr. Nyréx seine
Beobachtungen am Vertikalkreise redueirt habe und dass die Reculiate derselben vollständig mit dem
in meinem vorjährigen Bericht ermittelten Verlauf der Polhöhe übereinstimmen. Es scheinen daher
thatsächlich die Beobachtungen am Passageninstrument in den letzten Jahren von einer zur Zeit noch
unbekannten Fehlerursache beeinflusst zu sein.

 

 

Warschau. = —21! 2

Datum, Jahr, Polhöhe. a ee. Differenz.
1. Reihe. Esrenrevcur.

E 1890 Juli 24 1890.56 52.19 55082 je oar (—0"03)
: Aug. 26 65 5.71 +0.27 0.25 (+-o.02)
Sept. 30 75 Bo G3 +0.29 —o.22 (--0.07)
Nov 7 85 CEE —o.29 +o.11 (+o. 18)
Bec 16 96 5.68 +0.24 —0.05 (0.29)
5 1891 Jan. 26 1891.07 es +o.1I —0.18 (to. 29)
: März 9 19 5.38 —0.06 —0.27 (+o.21)
€ April 15 29 520 —0.24 — 0.28 (0.04)
Mai 7 35 5.08 0.41 —0.22 (=-0.19)
— 22 39 Son —0.37 — 0.16 (—0.21)
Juni 8 44 520 —O.14 —0.08 (—0.06)
— 23 48 5.49 0.05 —0.02 (0.07)
Jul 6 51 De —+0.10 +0.03 (-bo.07)
— 17 54 5.56 —-o.12 —o.08 (0.04)
— 31 58 5.60 : —o.16 —+o.14 (0.02)
Aug. 20 64 5.68 —o.24 0.21 (-+0.03)
Sept. 6 68 Gams 0.29 —o.26 (0.03)
— 17 jh 5.68 40.24 0.28 (—0.04)
= = 28 74 5.61 -+0.17 10.29 (—0.12)
Ger 12 78 ce —-o.08 —o.29 (—0.21)
— 27 82 5.40 —0.04 +0.29 (—0.33)
Nov. 25 90 5 27 —0.17 —-0.24 (—0.41)
1892 Jan. 14 1892.04 Den; —0.29 +0.05 (—0.34)
März 7 18 hr —0.23 — 0.15 (—0.08)
April 16 29 Ba —0.09 —0.24 (0.15)
Mai 16 37 526 —0.08 —0.25 (+o.17)
Juni 13 45 37 - ——0.07 —0.20 (+0.13)
Juli ri 53 5.45 +o.ox —0.10 (+-o.11)
— 31 58 5.52) - +0.08 —0.04 (+0.12)
A, 172 62 Giese +0. 08 —o.o: (0.07)
— 23 65 D —-o.10o —-0.04 (0.06)
Sept. 3 68 51.08 0.19 --0.07 (bo. 12)
— 13 70 5.64 —o.2o —o.o9 (Ho.11)
— 13 73 5.65 0.21 +o.11 (--o. 10)

=
r
13

 
