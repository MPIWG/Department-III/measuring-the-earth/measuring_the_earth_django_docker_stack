 

 

136

Vom mathematischen Gesichtspunkte aus können hiernach nur die Combi-
nationen B, & D, E, F, 6,H, L und M in Betracht kommen; es wird nun zu unter-
suchen sein, wie sich für diese die socialen, seismischen und meteorologischen Ver-
hältnisse gestalten.

I. Sociale und hygienische Verhältnisse.

1. Japan.

Nach der gefälligen Auskunft seitens des japanischen Commissars Herrn

Dr. Omori bieten die Ortschaften Otawara, Shirakawa, Fukushima, Sendai,

Midsusawa und Aomori sämmtlich gute sociale und hygienische Bedingungen dar.

Es sind Hauptstädte des Landes, welche an der von Tokio nach dem Nordende der

Insel Nippon führenden Eisenbahnlinie gelegen sind. Otawara hat 10000 Einwohner,
Shirakawa 13000, Fukushima 18000, Sendai 74000, Midsusawa 8000 und Aomori24000.
Sendai ist die grésste Stadt von Nordjapan, welche aus Anlass der landschaftlichen
Schénheiten ihrer Umgebung viel von Fremden besucht wird. Masuda ist ein
Vorort von Sendai, in + 38° 10’ Breite gelegen, welcher: gleichfalls günstige Auf-
enthaltsbedingungen bietet.

2. Italien.

Ueber die italienischen Verhältnisse hat der Vice-Präsident der italienischen
Gradmessungs-Commission, Herr Prof. G. Cenorra, dem Centralbureau in dankenswerther
Weise Auskunft ertheilt.

Nach dieser ist Cotrone eine Handelsstadt von 7000 Einwohnern in der
Provinz Puglien an der Mündung des Esaro, 30 Meter über dem Meere gelegen, Sitz

einer Unter-Prafectur. Dieselbe bietet zwar erträgliche Aufenthaltsbedingungen, indess

liegen die hygienischen Verhältnisse ungünstig, denn die Umgebung ist während des
Sommers und Herbstes infolge der stagnirenden Gewässer insbesondere des Esaro von
Malaria heimgesucht, die auch die Stadt nicht vollständig verschont.

Scicli ist eine Stadt in der Provinz Syrakus von 12000 Einwohnern, 7 Kilo-
meter von der Küste entfernt, in 217 Meter Meereshöhe, welche sammt ihrer Um-
gebung.mit Ausnahme einiger Gegenden am Fluss Erminio als fieberfrei zu betrachten
ist. „Die Stadt bietet nur ein dürftiges Unterkommen; dagegen sind die Aufenthalts-
bedingungen in der g Kilometer nordöstlich gelegenen Hauptstadt Modica mit 38000
Einwohnern und 450 Meter Meereshöhe als gute zu bezeichnen.

Licata ist eine Handelsstadt von 18000 Einwohnern, welche im Südwesten
der Insel Sieilien,an_ der Mündung des Salso und am Fusse des Hügel S. Angelo
liest. Stadt und Umgebung erfreuen sich einer gesunden Lage; nur in einem 5 Kilo-
meter ‘entfernten Distriet, Molacea genannt,- herrscht Malaria. ' Licata "besitzt mehrere
mittelmässige Hotels, bietet aber sonst als Aufenthaltsort hinreichend günstige Be-
dingungen. "Das Klima’ wird freilich als drückend und feucht, und das Wetter als
sehr veränderlieh bezeichnet. |

!
3
ä
3
3
i

ds ahd.

at dates

ar

jui

a ma bu aa MA a i

mate LA

 
