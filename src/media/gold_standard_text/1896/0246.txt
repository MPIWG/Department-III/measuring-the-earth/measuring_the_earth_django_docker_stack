 

240
Ce repérage s’effectuerait automaliquement au moyen de trois pointes métalliques,
d'acier ou de platine, disposées en triangle, et dont les contacts successifs avec le mercure
fermeraient autant de courants électriques ; ces courants agiraient à leur tour, par linter-
médiaire d’électro-aimants, sur les pointes correspondantes, pour arrêter aussitôt leur mou-
vement de descente.
Par construction, lorsque les trois pointes toucheraient la surface du mercure, laxe
de rotation de l’instrument devrait être vertical.
Consulté au sujet de la valeur de cette idée, nous avons cherché à nous rendre

compte du degré de précision susceptible, dans les conditions les plus favorables, d’être:

atteint avec un dispositif basé sur ce principe.

Tout d’abord, sans nous arrêter aux sérieuses difficultés pratiques de réalisation d'un
semblable appareil, et aux inévitables erreurs dues à l’inertie des pièces en mouvement,
nous avons voulu savoir avec quelle exactitude la position d’une pointe métallique, amenée
au contact d’un bain de mercure, pouvait être repérée, au moyen d’un courant électrique
actionnant soit une sonnerie, soit un galvanomètre. Sur notre demande, M. Klein, lhabile
chef du Dépôt des instruments de l'École des Ponts et Chaussées, a fait à ce sujet de nom-
breuses expériences dont voici les résultats :

Une vis micrométrique ! terminée par une pointe conique ? reliée au pôle négatif
d’une pile formée d’un élément Leclanché, était disposée au-dessus d’un bain de mercure *
communiquant avec l’autre pôle. On faisait lentement descendre la vis jusqu’à ce que le pas-
sage du courant fut signalé par une sonnerie électrique ou par un galvanomètre très
sensibles #, intercalés dans le circuit; puis on faisait sur le tambour, en regard d’un index
fixe, la lecture du nombre correspondant de microns.

Après avoir franchement ramené la vis en arrière, on recommençait l'expérience
jusqu’à trente fois de suite, en notant chaque fois la lecture faite sur le tambour.

Plusieurs séries de mesures ont ainsi été faites avec du mercure, tantôt simplement
essuyé avec un chiffon propre, tantôt lavé avec de l’eau légèrement additionnée d’acide mitrique,
puis séché avec du papier buvard ; la surface du bain tantôt laissée à nu, tantôt recouverte
d’une couche protectrice de 5" de pétrole.

La comparaison des lectures individuelles avec leur moyenne a montré que les
écarts suivaient à peu prés la loi des erreurs accidentelles ; qu’ils étaient à peu près indépen-
dants du degré de pureté du mercure et du mode employé pour signaler la fermeture du
courant; mais que la présence d’une couche de pétrole avait pour effet de doubler ou de
tripler leur valeur. Les écarts maxima des positions extrémes de la pointe par rapport à sa
position moyenne ont été respectivement de 6 & 7" dans le premier cas, mercure à nu, lin-

1 Diamètre de la vis, 476; pas 1/2mm, tambour de 50mm de diamètre, divisé en 400 parties de 16
de largeur, représentant chacune 1 micron d’élévation ou d’abaissement de la vis.

2 Hauteur de la pointe 4mm; diamètre à la base 3mm,

3 Diamètre du bain 44mm :; hauteur 37mm.

i Longueur de laiguille du galvanomètre 60mm; déviation imprimée par le courant de la pile,
environ 459,

    

AM A NA Ua sh A a AU dee dss

rer F}

À ot ham sh anne

nmel

 
