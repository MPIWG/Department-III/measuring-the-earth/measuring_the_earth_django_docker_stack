 

i
i
‘
3
i
i
i

 

122

 

 

 

| Datum. Jahr. Polhöhe, Paare. ann D Differenz.

| 1090 Jan 5 1800 O0N Ho vire 0s) 42 er 0/26 = 0.08

| Febr. 14 12 15.47 163 —0.39 —0.29 0x0

i März 2ı 22 ro Gi 89 0-25 —0.24 Bio pine

| April 6 27 15.64 96 — 0.22 —0.19 — 0.03 |
i Mai 14 37 15.77 60 —0.09 —0.04 —0.05

It Juni 17 46 26.87 29 0.01 40.09 — 0.08

i Jul 13 55 16.14 76 +0.28 —+0.20 +0.08
i Aue, 13 62 16.14 39 0.28 40.25 +0.03 j
i Sept. 18 72, 16.03 93 40.17 0.23 — 0.06 |
| Oct. © 80 16.06 84 +0.20 +o.15 40.05

N) Nov uf 86 16.00 24 0.14 —o.o7 0.07

| Dec. ı8 96 15.74 67 0.12 0.08 — 0.04 |
1 1801 Jan 28 1891.08 15.69 26 --0.17 — 0.22 0.05 1
i Rebe 15 13 75.89 124 —0.27 — 0.26 — 0.01 ]
i Marz 17 DIE 15.64 84 — 0.22 — 0.27 +0.05 1
i April 22 31 13.58 88 —0.28 —-0.26 —0.02 =
j Mai ı5 By) 15.68 116 —o.18 —0.17 —0.01 4
i Juni 16 46 15.90 70 +0.04 0.02 +0.06

i Juli... 15 54 15.94 D —-o.08 —o.10 —0.02 |
à Aug. 23 64 16.09 67 +0.23 +o.22 +o.o1 3
i Sept. 17 7X 16.21 157 +0.35 +0.28 +0.07 3
i Oct rs 79 16.16 135 0.30 +0.28 0.02 4
| Noy. 13 87 16.21 101 -+0.35 +0.25 +o.10

1892 Jan. 9 1892.02 15.07, 718 —+o.11 +0.07 +0. 04

Febr. zo 14 26.93 80 — 0.03 —O.1I +-0.08

N Marz 20 22 72 154 — 0.14 — 0.20 0.06

1] April 11 28 75.01 132 —0.25 —0.25 0.00

4 Mai 20 39 Tec: 126 —0.31I — 0.26 — 0.05 :
| 15.86 3527 1!
\ 2. Reihe. Srrrarer und Lregrein. |
by i
| 1895 März 19 1895.21 Ser 5.15.79 AT —0713 —0"!10 — 0.03 j
| April 15 28 25.91 68 — 0.01 —0.09 0.08 1
Mai 18 38 15.76 88 — 0.16 — 0.04 —0.12 =
N Juni 19 46 15.90 117 — 0,02 0.00 "0.02 i
à Jul © 54 No. 17 64 0.25 --0.03 0.22

; Arc, nr 61 16.14 72 —o.22 0.03 —o.19

N Sept. 17 71 15.91 173 —0.01 —0.01I 0.00

i Oct, ro 80 15.35 76 — 0.07 —0.06 — 0.01

! Noy. 16 87 15.83 76 —0.09 —0.09 0.00

| 1896 Jan. 18 1896.05 15.66 56 —0.26 —0.14 —0.12

I Febr, 14 12 15.69 92 —0.23 —0.14 —0.09

i Marz 19 21 16 774 94 — 0.18 —O.II —0.07

| April 19 30 25.07 40 —0.0I —0.07 0.06

| Mai 20 38 15.91 72 —O.0I —0.0I 0.00

î Jun 8 44 15.09 112 +o.07 40.04 +0.03

15.92 1241

ı. Reihe. Monatsmittel mit Ausnahme der je zwei zusammengezogenen Monate Dec. 1889/
1] Jan. 1890 und Dec. 1891/Jan, 1892 auf Grund erneuter und einheitlicher Ausgleichung der Resultate.
2. Reihe. Monatsmittel mit Ausnahme der zwei zusammengezogenen Monate Dec. 1895 /Jan. 1896,
Die Resultate, welche für beide Reihen aus einer provisorischen Reduction der Beobachtungen
hervorgegangen sind, sind von Herrn Prof. Dr. Wrixex dem Centralbureau gütigst mitgetheilt worden.

Die Beobachtungen werden von den Herren Dr Sprrarer und Lier weiter fortgesetzt.

Be

ee

|

 
