 

270

L’allure de la courbe ~ montre que lerreur maxima de réfraction croît plus vile que
la longueur maxima des nivelées. — Dans le cas représenté sur la figure, cette erreur devient
décuple, par exemple, lorsque la longueur normale des nivelées passe de 50 à 200 mètres.

14. Influence de la hauteur de l'instrument au-dessus du sol. — Le rôle de H
dans l'expression de r,, (équation 29) se détermine à peu près comme celui de L,,. La seule
parlie de r„ qui varie avec H est :

O10),
Bl) 1 @) = =
: A +c
| dm étant d’ailleurs lié à H par l’équation (26).
| Si l’on effectue, comme dans le cas précédent, l'élimination de à, entre ces deux
dernières relations !, et que l’on porte, sur deux axes rectangulaires, les valeurs respectives

Ceci posé, soit à obtenir ® (ôm) pour une grandeur donnée de Lm (150 mètres par exemple). Soit a le
point correspondant sur l'échelle OX, on a :

Lm = Oa; om == aa’ = Da”

Rabattons Oa” en Oa, sur l’axe OX, et tracons l’ordonnee A,aı de la courbe ®, nous avons :

A,aı == @ (om).

| Reste 4 multiplier cette grandeur par L,, pour avoir 4 (Lm).

Pour cela projetons A, en A, sur la verticale «Z du point 400 metres de l’öchelle OL (l’hectometre
étant choisi comme unité de longueur). Joignons OA’, et prolongeons jusqu’a la rencontre en A, avec la verti-
cale du point a.

Il vient :
Asa Oa Lin

Ac Où TH 1
D'où :
A2a — Lm® (dm)
Pour multiplier une seconde fois par Lm, on projettera de méme A> en 4’: sur «Z; puis A’2, oblique-

ment, en A; sur la verticale du point a.

On aura encore :
Asa = Lm X Asa —= Lm°® (Gm)

Le lieu des points A» est la courbe y cherchee.

1 Voici comment, dans ce cas, s’effectue l’elimination graphique de è». Reproduisons (fig. 7) la courbe
D (à) (fig. 5). L’equation (26) peut s’ecrire :

oL m

Bo 0) Ci 0.

Portons sur deux axes rectangulaires OX et OY, en abscisses et ordonnées, les valeurs respectives de
H et de ôm. L'équation précédente, du 2€ degré par rapport à ces deux variables, se traduit par une hyperbole
équilatère By, dont les deux asymptotes sont respectivement :

 

1

yt ha ee

dba seele

hrs eh a en

 
