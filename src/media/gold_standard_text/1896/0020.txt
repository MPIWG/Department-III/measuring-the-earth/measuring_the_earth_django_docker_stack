 

 

 

ar

 

Il n'y a que les dispositions financières de la nouvelle Convention, concernant la dota-
tion et les parts contributives annuelles, qui: sont nécessairement influencées, jusqu’à un
certain point, par le nombre des États qui auront définitivement adhéré.

Le bureau croit done pouvoir émettre lavis que la Commission permanente, pour
se conformer à la résolution finale de la Convention de 1895, doit décider qu’elle se dissout
pour le 31 décembre 1896, et transmettre ses pouvoirs au nouveau bureau de VAsso-
ciation.

La Commission permanente jugera si elle entend être convoquée de nouveau avant la
fin de l’année, dans le but d’arrêter, suivant les conditions du moment, les mesures finan-
cières pour l’année 1897, qui pourrait, au besoin, être envisagée comme une année de tran-
sition, ou si elle préfère charger de ce soin le nouveau bureau, après qu’il aura pris, par
correspondance, conformément au 3m alinéa de l’article 2 de la Convention, l'avis de la
nouvelle Commission permanente consultative.

Après avoir remercié le Secrétaire pour son rapport, M. le Président donne la parole
à M. le professeur Helmert pour communiquer le rapport du Bureau central.

Rapport sur l’activité du Bureau central depuis la Conférence de Berlin,
octobre 1895.

A. Travaux scientifiques.

1° Après avoir publié en 1893 le premier fascicule de l'ouvrage Sur la mesure des
degrés de Jetilude en Europe sous le parallèle du 52: degré, depuis Valentia (Feaghmain)
en Irlande jusqu'à Varsovie, ouvrage dont les premiers calculs ont été commencés en 1888,
j'ai enfin aujourd’hui la satisfaction de vous en présenter le 2me fascicule, qui a été rédigé
par M. le professeur Börsch et M. le Dr Krüger.

En attendant, une grave erreur qui avait été commise à l’extrémité Est de l'arc de
longitude près de Orsk, en Russie, a été éliminée grâce à une seconde mensuration. On peut
donc envisager comme terminée cette importante mesure de degrés de longitude, qui a été

inaugurée, comme on sait, par les astronomes Wilhelm et Otto Struve.

Si le calcul de compensation de la partie occidentale de ce parallèle, comprenant
seulement 31°4, tandis que l’arc entier embrasse 68"9, a occupé deux calculateurs pendant
huit années environ, c’est cependant peu de chose en comparaison de la grande dépense de
temps et d'argent exigée par les observations elles-mêmes.

D'un autre côté, il est à remarquer qu’on a exécuté le calcul au Bureau central de
la manière la plus complète, en utilisant partout, d’après le principe moderne suivi dans les
études des mesures d’are, les contrôles fournis par les équations de Laplace. J’ai déjà eu
l’occasion d’en citer un petit exemple lors de la Conférence de Berlin en 1886. En effet,

i

del Le HI Nat à à

alla

aka ia

IE Trot!

am pi a uma 4 |

|
|

 

 
