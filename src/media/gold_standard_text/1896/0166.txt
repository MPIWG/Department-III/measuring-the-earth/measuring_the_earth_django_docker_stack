 

SEN

EE EE ET

2 Ea pe

RAT RP PAP PM QE DRE TO PE SP ETS RRER SR EE SPEARS

GE EEE EEE GE DF DE OLS IT IG LD PLEA LEERE Er

Beilage A. III.

BERICHT

über

DIE WAHL DER STATIONEN

DEN. INTERNATIONALEN POLHOHENDIENST

von

ADO MARCUSEK z

l. WAHL DER STATIONEN.

Um den älteren Fergola’schen Vorschlag, säculare Polhöhen-Aenderungen auf beste-
henden, unter gleicher Breite liegenden Sternwarten zu ermitteln, in dem Foerster’schen
Sinne zu erweitern, nämlich alle periodischen und säcularen Schwankungen der Erdachse
auf besonderen Stationen fortlaufend zu bestimmen, habe ich seit mehreren Jahren eine
Anzahl von Vorschlägen ausarbeiten können, welche in den Akten der Berliner Sternwarte
betreffend die Internationale Erdmessung 1892 bis 1895 enthalten sind. Von den zusammen-
fassenden Berichten hierüber, welche ich im Januar und April 1896 Herrn Director Helmert
eingereicht habe, erlaube ich mir im folgenden einen kürzeren Auszug betreffend die Wahl
der Stationen nebst den zugehörigen meteorologisch-geologischen Daten zu geben.

Um die Componenten der Erdachsenbewegung im periodischen und säcularen Sys-
tem eindeutig zu bestimmen, genügen fortlaufende Messungen identischer Polhöhen-Stern-

 

;
3
3

sh Abd a aL

188 bias

al

Ate had.

Lili dh dd dst dd bs

a Am a nn lsd an

 
