 

302

III. — NIVELLEMENTS DE PRECISION

La ligne du chemin de fer Madrid ä Alsasua par Zaragosse et Pampelune a été
nivelée à double sur un parcours de 307 kilomètres avec 66 repères principaux et 40 secon-
daires, et elle sera terminée dans toute sa longueur avant la fin de cette année.

On a complété la ligne Guadalajara-Cuenca en nivelant la dernière section d'une lon-
eueur de six kilomètres.

On a entrepris les travaux sur la ligne Badajoz-Huelva et les contrôles qu'on à
Lrouvés nécessaires dans quelques sections du N.-E. de la Péninsule, nivelées depuis longtemps.

IV. — MAREOGRAPHES

De février 1894 à février 1895, les oscillations diurnes maxima ont été de 07383 à
Alicante, de 3650 à Cadix, et de 4686 à Santander; les oscillations minima respectives
de 0"079, 0"730 et 1107.

Les variations du niveau moyen à Cadix et à Santander ne différent guère de celles
observées l’année précédente, où elles ont été de 1°"9 et de 6"/"2. À Alicante (Méditerranée)
la variation n’a pas dépassé 1°°5 au lieu de 2°"8 constatée en 1893-94.

V. — PUBLICATIONS

Les ouvrages publiés durant la présente période sont les suivants : Tome X.
Mémoires de l'Institut géographique et stalishique.

Ces Mémoires comprennent :
Détermination de la différence de longitude entre Madrid et Tetica. (Exposition

détaillée des opérations faites pour déterminer directement la différence de longitude men-

tionnée. )
Nivellements de précision de I’Espagne (11™e partie). — Ligne de Zaragosse 4 Puente
de Behovia, par Pampelune. (Observations maréographiques et météorologiques d’Alicante,
Cadix et Santander.)

Détermination expérimentale de Vintensité de la pesanteur à Pampelune. (Procédés
d'observation et de calcul suivis pour déterminer la valeur absolue de la pesanteur.)

Tome XI, [. Détermination des différences de longitude Madrid-Desierto et Desierlo-
Perpignan.

Tome XI, IL. Détermination expérimentale de l'intensité de la pesanteur à PObserva-
Loire astronomique de San Fernando.

Madrid, octobre 1896.

F. COBO DE GUZMAN,

Directeur général de l'Institut géographique et stalislique,
Délégué de l'Espagne.

Aste Ji ol.

bb hat

bait a

ame ph na

Hi th A AA MMB hh LAN AA TRES CETTE TES EP

    

Ss

 
