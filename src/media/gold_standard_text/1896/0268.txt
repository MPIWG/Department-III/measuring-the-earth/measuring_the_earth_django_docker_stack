 

 

Figure 4.

distance a, ane hauteur 6 tie (c étant ce que nous appellerons le coefficuent sladimeétrique
de la lunette)!.

Afin de diminuer l’influence de la réfraction sur la lecture du fil niveleur, on s’as-
treint — en réduisant au besoin les porlées — à rendre toujours possible la lecture du fil
stadimétrique le plus rapproché du pied de la mire. Pour restreindre le moins possible la
longueur des nivelées en terrain incliné, on fait en sorte que ce fil atteigne la limite de la
division dans le bas de la mire d’amont (fig. 4).

IV. La longueur L des nivelées ne dépasse jamais un certain maximum L,, déter-
miné à l’avance d’après les conditions atmosphériques, la puissance de la lunette et le mode
de division de la mire.

Il s’agit de voir comment la réfraction varie lorsque linclinaison du terrain croit
progressivement depuis le palier (p — 0) jusqu’à la verticale (p — co).

Nous avons établi précédemment (n° 9, équation 16) l’expression de l'erreur E de
réfraction, en fonction de la variable auxiliaire Ô.

Déterminons maintenant la relation qui existe entre à et la pente p.

On a d’abord, entre les deux quantités p et à, la relation générale :

> bare
combinaison des équations (15) et (20).
Maintenant, deux cas sont a distinguer :

1 Nous négligerons, dans cette étude, la petite erreur provenant de ce que, avec une lunette non
anallatique, les distances fournies par la stadia doivent être complées à partir du foyer principal antérieur de
l'objectif, et non depuis l’axe vertical du support de linstrument,

 

;
;
3
3
=
3

de dd

 
