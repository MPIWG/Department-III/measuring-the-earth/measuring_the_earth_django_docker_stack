 

256

 

D D Ni
| h, + c=—(H + ce) + Fe +o(1 + Im)

aa

a EP) = =H + 9(1 ery)

Posons :
D

bi ~ Fo

Cy

Nr ° OO 5
à étant une variable auxiliaire comprise, en valeur absolue,

entre: 0 = 0, pour D = 0, ouc = co} valeurs limites
et 0 = 1, pour D = Wetc—0 \ de D et de c(!).

Mettons D en facteur commun dans |’équation (8) et remplagons les termes de la
parenthèse par leurs valeurs en fonction de 0; nous aurons, en introduisant le facteur K

(équation 12) :

 

AR + D. D 4 1 : 1 0 1 A N

D'autre part, si dans la formule (1), on remplace successivement h et ¢ par h, et 4,
puis par H et 4, enfin par h, et £,, el si l’on retranche membre à membre les équations

ainsi obtenues, il vient :

 

[ h, + c 1 + à 1 +0
17 t. — = l | DRE LR EEE — | | og ee wl Ben, ea Ne
un
m; Hl ee .
Wins bitty ei boloe ssp = ——8 low. (tac 0)
( ) 9 4 € h, + € (
Dou:
a) t, — ty = b log (1 + À)
ei
t. — I, log (1 -|- à)
(| 8 emma! 3 a trs EE
19 cer log (À — à)

9\ /
>

= designant une nouvelle variable auxiliaire, de valeur toujours positive.

1 En effet, d’après l'équation (14), D étant égal à 2 (H — hi), a pour maximum 2H, quand h, = 0;
c, d’autre part, est une quantité forcément positive (n° 2).

 

$
4
3
3
i
qa
1

ve ét dsl ds M bd al

tem LL al aa nn

 
