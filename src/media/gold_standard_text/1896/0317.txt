N PRT. Serr

À A Lil

WELL RE

PRT BE FR

LU RAE

TABLE DES MATIERES

INHALTSVERZEICHNISS

Procès-verbaux des séances de la Commission permanente de PAssociation géodésique
à Lausanne du 45 au 24 octobre 1896.

internationale, réunie

Premiére séance,

Liste des membres présents :

Discours d’ouverture prononcé par M. ne
chet, Conseiller d’Etat du canton de
Vaud à

Discours de M. = De Te Be
teur de l’Université de Lausanne

Réponse de M. Faye, Président de l’Asso-
ciation géodésique a gia

Rapport du Secrétaire perpétuel à la
Commission permanente.

Rapport sur l’activité du Bureau
depuis la Conférence de Berlin, par M.
le Directeur Helmert de

Programme de la session et communica-
tion d’affaires par le Secrétaire

Deuxième séance, 16 octobre 1896

Lecture et adoption du procès-verbal de la
première séance Palo es

Communication de M. Anguiano, délé-
gué du Mexique, au sujet de l’adhesion
de son Gouvernement à la nouvelle Con-
vention

Lettre du général Br atiano, ion ats a
Roumanie, sur la prochaine adhésion du
Gouvernement roumain et sur les tra-
vaux ue exécutés dans son pays

Lettre de M. Æ. de P. Arrillaga annon-
çant la ee par l'Espagne de la
nouvelle Convention. Il délégue sa voix
a M. Helmert. ee ae

Lettre de M. le général l'errero qui ex-
cuse son absence et délègue sa voix à
M. Hirsch .

15 octobre 1896.

Pag.

3-21

23

23-24

24

 

Invitations adressées à l’Assemblée par
le Conseil d'Etat, la Municipalité et
l'Université de Lausanne or

M. le President lit une lettre de M. le
Ministre de l’Instruction publique de
France, annongant les dispositions pri-
ses par le Gouvernement en vue de
l’adhésion de la France à la nouvelle
Convention re SO.

Discussion sur les ratifications de la nou-
velle Convention et les divers sujets qui
s’y rattachent. ae

M. Hirsch propose de deliberer sur la
dissolution de la Commission perma-
nente et de nommer une Commission
Speciale des tmamces; 2

MM. Foerster et Helmert exposent leurs

idées au sujet des contributions a fixer
avant d’avoir recu toutes les adhe-

sions 6 e s 0 a :
Résolution adoptée a l’unanimite, par
laquelle la Commission permanente
transmet ses pouvoirs au nouveau bu-
reau de l’Association. . . :
Nomination de MM. Bakhuyzen, es
ter et Zachariae comme membres de
la Commission des finances .
Discussion sur le Rapport a adresser aux
Gouvernements par la Commission per-
manente et le nouveau bureau de |’ As-
sociation :
Rapport du’ Bureau ons ii par M. AL
brecht, sur letat eh des études
concernant les variations de latitude
(Voir Annexe A. I)

Pag.

25-28

vo
or

25-26

19
I

 
