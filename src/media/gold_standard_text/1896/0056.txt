 

50
qui, sans être autorisé à faire une déclaration officielle, croit pouvoir affirmer que la Confé-
rence serait bien reçue dans la capitale du Danemark.

M. Foerster donnerait la préférence à Hambourg, un peu plus facile à atteindre et
où la Conférence n’a pas encore siégé.

M. Bakhuyzen propose de tenir la prochaine Conférence à Amsterdam et M. le Prési-
dent se déclare pour le choix de cette ville.

M. Hirsch désire que la Conférence se réunisse à Vienne, où elle n’a pas siégé
depuis bien longtemps. Cela offrirait en outre l’avantage de raviver, dans certains milieux
autrichiens, l’interet pour notre ceuvre et d’activer peut-élre les décisions au sujet de
l’adhésion à la nouvelle Convention.

Quant à l’époque, M. Bassot exprime le vœu que la prochaine Conférence générale
se réunisse en 1897 vers la fin d’août, ce qui paraît être l’opinion générale des membres de
la Commission permanente.

M. Hirsch doit réserver à cet égard son opinion, attendu que le nouveau bureau ne
pourra prendre de décision définitive qu’au moment où les circonstances le lui permeltront.

M. le Président invite les membres de la Conférence, qui ont des rapports à pré-
senter sur les travaux exécutés dans leurs pays, à bien vouloir en faire la lecture.

Ces rapports ont été communiqués dans l'ordre suivant :
Pour la Belgique, par M. le général Hennequin (Voir Annexe B. 1);

» le Danemark, par M. le général von Zachariae (Voir Annexe B. ID;

» la France, M. Ch. Lallemand annonce que la compensation du réseau fondamental du
Nivellement général de la France a été exécutée au printemps dernier et qu’on en a déduit
les altitudes rectifiées du niveau moyen de la mer en divers points des côtes françaises.

M. Lallemand présente ensuite une Étude théorique sur les erreurs causées par la
réfraction aérienne dans le nivellement géométrique ; il communique également, au sujet de
la dilatation du bois des mires, une note de laquelle il résulte que la contradiction précé-
demment signalée entre le résultat des travaux du colonel Goulier (dilatation liée à l'humidité
relative de l’air) et les constatations du Dr Oertel (dilatation proportionnelle à l'humidité
absolue) n'existe pas en réalité.

Enfin, M. Lallemand expose les résultats de l'étude qu'il a faite d’un projet de niveau
électrique automatique qui a été récemment soumis au service du Nivellement général de
la France. Malgré l'apparence séduisante de l’idée, la substitution d’un bain de mercure à la
classique nivelle à bulle d’air aurait plus d’inconvénients que d’avantages, sans compter
une perte considérable de précision (Voir Annexes B. III ® » © d.),

M. van de Sande Bakhuyzen rend briévement compte des travaux néerlandais sur

 

a
z

dia

ee

DL TTL il

ta |

i)

‘iran hs Mh dl apy
