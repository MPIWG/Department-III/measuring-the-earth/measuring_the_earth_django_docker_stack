N TREE Tr TH

UL AR Ihm

il

ili

Tp eerie

aR

L'ART LRU

251
pour la confection des mires de nivellement, ont porté sur 14 essences différentes de bois,
étudiées, soit à l’état naturel, soit après ébullition dans l’huile de lin, soit enfin à l'état de
règles recouvertes de trois couches de peinture au blanc de céruse.

Les conclusions, sommairement résumées, de ces essais, ont été les suivantes :

1° Les dimensions varient moins dans le sens des fibres du bois que dans le sens
perpendiculaire.

2° De tous les bois, les essences résineuses sont les moins sensibles à l’action de
l'humidité.

3° Dans les circonstances ordinaires de la pratique, lallongement des règles est
proportionnel à Paccroissement de l’état hygrométrique, sauf pour les bois résineux (voir
ci-après, 7°). |

4° Les règles peintes varient moins, et surtout moins vite, de longueur que les règles
bouillies dans l'huile, ou laissées à l'état naturel. L’huilage n’a qu'un effet minime.

90 L'action de la température est sensiblement la même pour chaque espèce de bois,
qu'il soit à l’état naturel, peint, où huilé, L'allongement est proportionnel à l'accroissement
de la température entre les limites ordinairement observées dans les opérations sur le
terrain.

6° Pour le sapin ordinaire, peint, aux trois quarts sec, habituellement employé pour
la confection des mires, le coefficient d’allongement thermique varie avec l’état hygromé-
trique, comme l'indique le tableau ci-après :

État hygro- Allongement
métrique 1. par mètre.
: Arsen: 5. San ee) ae tO! 4°9
Pour 1° d'augmentation | ; ; ie
au ” ' Airsmoyennemenisec! 7 8,3604 10°3
de la température à
; P Kir. tres humide 0.003 2557 21008 45

7° Sous l’influence d’une humidité croissante, le bois des mires s’allonge d’abord,
jusqu’à un certain degré hygrométrique, puis se contracte au delà. Le maximum de lon-
gueur correspond à un état hygrométrique variable, comme il suit, avec la température.

Temperalure 280 lc ee ı 10
Etat hygrométrique correspondant au maximum de longueur 801 754 704

Pratiquement, on peut admettre que le bois des mires s’allonge suivant une loi
linéaire, entre 10¢ et 604, puis garde une longueur constante entre 604 et 1004. Le coeffi-
cient de variation hygrométrique, à peu près constant entre 104 et 604 d'humidité relative,
varie avec la température, comme l’indique le tableau ci-apres :

Allou sement

Température
Températ par metre.

P I! d’accroissement de \ Lu 2 > 16,
up JH LOTS de . Ba Be
a lo ea Sr femmperé = 21% 18°
état hygrométriqu

Ye 4 ( Aipchanl 2000 al 19

1 La graduation adoptée pour l’état hygrométrique est celle de lhygromètre de Saussure, dans

laquelle la cote 01 correspond à l'air absolument sec, et la cote 1004 à l’air saturé d'humidité.

 
