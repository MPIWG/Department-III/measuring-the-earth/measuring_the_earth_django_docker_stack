 

aj
a
à
ar
=z)
3
=
=
a

aye 1

84

 

Botschaft der Vereinigten Staaten von Amerika.

Berlin, den 15. October 1896.

Der unterzeichnete Botschafter der Vereinigten Staaten von Amerika in Berlin hat
die Ehre, dem Herrn Prof. Dr. Ad. Hirsch, standigen Secretar der internationalen Erdmessung,
mitzutheilen, dass die Vereinigten Staaten die Absicht haben, der neuen Uebereinkunft der
internationalen Erdmessung beizutreten, unter der Bedingung, dass dieser Beitritt erst in
Wirksamkeit treten wird, wenn der Congress die nöthigen Credite für die Zahlung der den
Vereinigten Staaten obliegenden Jahresbeiträge gewährt haben wird. Der Congress soll, in
seiner nächsten Sitzung, um die Gewährung dieser Credite angegangen werden.

Der Unterzeichnete benutzt diese Gelegenheit, um Herrn Professor Hirsch von Neuem
seine vollkommene Hochachtung auszudrücken.

 

 

ITALIE

Lal

(gez.) EDWIN-F. UHL.

 

| Ferner theilt der Secretar einen Brief des Herrn Celoria vom 17. October mit, worin
i derselbe in Aussicht stellt, dass Italien durch Herrn General Ferrero vertreten sein werde,
und wenn dieser verhindert sein ‘sollte nach Lausanne zu kommen, dass die geodätische
Commission in jedem Falle ihren Bericht rechtzeitig einsenden werde.

In der That ist die Absendung dieses Berichtes durch einen Brief des Herrn Guarducci,
Secretärs der italienischen Commission in Florenz, angezeigt; das Bureau wird denselben
unter die Mitglieder der Versammlung vertheilen lassen.

Den

Der Herr Präsident ertheilt Heren Foerster das Wort, um den Bericht der Finanz-
commission zu verlesen :

a ne €
ss

RE

Lara OT

 

Bericht der Finanz-Commission.

Fern ER

1. Die Commission hat die Rechnungen des Herrn Directors des Centralbureaus für
das Jahr 1895 sorgfältig geprüft, und hat sämmtliche Ausgaben in Ordnung und durch die
nöthigen Belege gerechtfertigt gefunden.

Die Commission hat ebenfalls von den Einnahmen und von dem Conto der verfügbaren
Fonds, sowohl für Ende 1895 als für den gegenwärtigen Augenblick, Kenntniss genommen.

Auf Grund dieser Prüfung schlägt die Commission vor, die Rechnungen des Central-
bureaus der internationalen Erdmessung für das Verwaltungsjahr 1895 zu billigen und dem
Herrn Director des Gentralbureaus für seine Verwaltung volle und unbedingte Entlastung zu
ertheilen. :
i 2. Was den Ausgaben-Vorschlag für das Jahr 1897 anlangt, welcher auf die in der
«Neuen Uebereinkunft» gewährte Dotation begründet werden muss, so ist dies nicht mehr |
die Permanente Commission, sondern das neue Präsidium der internationalen Erdmessung,

Eisen

 
