 

Sah bien La

La ne INI TRW TTA"

MUR

(WILL

DRAG AWE UL TT ewer reer

FLA |

Beilage A. VI.

BERICHT
über die

am photographischen und am visuellen Zenitteleskop

erhaltenen Resultate.

Die Vergleichung der photographischen und der visuellen Beobachtungsmethode
ist von den Verfassern in der Zeit vom 24. April bis ı5. Juni 1896 vorgenommen

worden.
Die Beobachtungen nach der photographischen Methode, die bereits Herr

Professor Küsryer (Astronomische Nachrichten No. 8015, Seite 241) in Vorschlag gebracht
hatte, wurden erhalten mit dem Zenitteleskop, welches nach Angabe des Herrn Dr. Marcuse
vom Mechaniker Wanschaff in Berlin konstruirt worden war. Die eingehende Be-
schreibung dieses Instrumentes findet sich in dem Bericht des Herrn Dr. Marcus
in den ,Verhandlungen der 11. Allgemeinen Conferenz der Internationalen Erdmessung“,
Seite 303—310. Dieser Bericht soll im Folgenden kurz mit B. M. bezeichnet werden.

Die Beobachtungen nach der visuellen Methode sind am Zenitteleskop des
Geodätischen Institutes ausgeführt worden.

Der vorliegende Bericht umfasst drei Abschnitte:

I. Resultate aus den Aufnahmen am photographischen Zenitteleskop.

IT. Vergleichung dieser Resultate mit den gleichzeitig am visuellen Zenit-
teleskop erhaltenen.

III. Kritik der photographischen Methode mit besonderer Anwendung auf die
fortlaufenden Polhöhenbestimmungen.

 
