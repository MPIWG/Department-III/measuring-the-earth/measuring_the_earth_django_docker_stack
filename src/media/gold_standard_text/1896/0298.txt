 

     

 

 

 

Bone

 

 

 

 

 

 

Schwere 9, (in Differenz
Ort. Jahr. Monat. Beobachter. Metern) reducirt

auf das in Metern. |
Meeres-Niveau. |
@ | -| 1892 October Müller 9.78346 ue
| 1892 | November | Müller 9.78655 | ine a
Bu | 1893 | November | Guberth bb al om

| (is Hcbeudr | Müller 9.78073) 96 9 |
Sinvapore À 1894 Jänner | Müller 091) > | 0.00008
’| 189% Jänner Guberth 083 |

Tanjong-Priok, 1893 Marz | Miller 97819 7 + 0.00000
Batavia | 1894 | Februar Guberth 19 |

‘ | 1893 | November Müller 9.78198 | x

| Amboina . . ; . 1894 | September Guberth 203 | + 0.00005 |
| 1898 Mai Müller |9.80013 he
Melbourne . i 189% April Gaberth 018 + ().00005 |
| 1893 Juli Müller | 9.79703
Sydney 5, eo Mai Guberth | 723 + 0.00020 |

 

 

 

 

 

! Wenn die geographische Position an einem Orte verschieden war, so wurde, mit
Berücksichtigung des Breitenunterschiedes, die Schwere stets auf die niedrigere Breite
reduciert. |

2 Mit Gewichten. |

 

S. M. Schiff « Saida », welches im October vorigen Jahres nach Ostasien abging, wird
erst im März 1897 zurückkehren, und die Rückkunft von S. M. Schiff « Albatros », welches
zur selben Zeit eine Mission in die Südsee (Melanesien), sowie nach Australien antrat, und
ausser magnetischen Instrumenten, ebenfalls einen Pendel-Apparat mitgenommen hat, ist
noch nicht bestimmt.

Es waren somit alle vier Pendel-Apparate, welche der Kriegs-Marine gehoren, im
April |. J. auf Schiffen in Verwendung, daher konnten die anfangs Mai abgegangenen zwei
Missions-Schiffe « Frundsberg » und « Panther » mit Pendel-Apparaten nicht ausgerüstet
werden.

Mit dem Erlasse Abth. 6/M. S., Nr. 3117 vom 18. April |. J. hat aber die Marine-
Section den Ankauf von zwei weiteren vollständigen Pendel-Apparaten angeordnet, und es ist
die Beschaffung derselben im Zuge.

Damit wächst die Anzahl der Pendel-Apparate der k. und k. Kriegs-Marine auf sechs,
und es wird diese Anzahl wohl genügen und im Zukunft alle Missions-Schiffe mit solchen
Apparaten betheilen zu können.

 

3

hi à dd

oh not dette

À am pb bh a ann

nan tm A

 
