 

 

68

stand, den ich schon 1892 in Brüssel erörtert habe; nämlich der, dass der Krümmungsra-
dius des Parallels merkwürdig klein ist: er beträgt nur 198m mehr, als wie auf Bessels
kllipsoid, aber 486m weniger als auf Clarkes Ellipsoid. Da aber alle grossen Breitengradmes-
sungen, gleichviel welche Abplattung man annimmt, die grösseren Dimensionen für das Erdel-
lipsoid bestätigen, welche Clarke fand, so führt die Längengradmessung in 52° Breite zur Ent-
deckung einer enormen kontinentalen Anomalie des Geoids. Bis jetzt kannte man derarlige
Erscheinungen noch nicht, und ich möchte daher diese Erweiterung unserer Kenntniss von
der Figur der Erde als eine epochemachende bezeichnen.

Dieselbe veranlasst mich einige! Worte über die zusammenfassenden Berechnungs-
arbeiten beizufügen, welche ich demnächst im Centralbureau in Angriff nehmen möchte.
Nach der Uebersichtskarte vom Stande der Publikationen der Dreiecksnetze, welche ich im
Vorjahre in Berlin vorzulegen die Ehre hatte, können im Anschluss an die Längengradmessung
geodätische Linien und Lothabweichungen von Bonn nach Süden durch die Schweiz bis Ge-
nua und Nizza gerechnet werden, ferner von der Schneekoppe über Wien und Dalmatien nach
Sicilien. Ausserdem möchte ich den russischen Theil der Längengradmessung in 52° Br.
sowie die russische Längengradmessung in 47 '/a° Br. und die grosse russisch-skandinavische
Breitengradmessung an die westlich gelegenen Gradmessungen anthliessen lassen, um aus
allen ein einheitliches System von Lothabweichungen zu formen.

In einigenJahren wird man auch ganz Deutschland, Dänemark und grössere Flächen-
theile von Oestreich, sowie die neuen Arbeiten in Frankreich und Spanien anschliessen
können; ja es eröffnet sich schon die Perspektive im Zusammenhange hiermit um den ganzen
westlichen Theil des mittelländischen Meeres herum rechnen zu können. Bei dieser Gelegen-
heit kann ich nicht umhin, auf die bekannten, schon oft erwähnen Lücken im Dreiecksnetz
erneut hinzuweisen, die durch die fehlende Verbindung von Dänemark und Schweden, sowie
von Sardinien — Corsica mit dem italienischen Festlande gebildet werden und deren Ausfül-
lung immer dringlicher wird.

Was nun die erwähnten zusamraenfassenden Lothabweichungsberechnungen anlangt,
so liegt es in der Natur der Sache, dass dieselben mit dem Fortschreiten der Messungen mehr
und mehr hervortreten ;'si, sind ja das zu erstrebende Endziel der ganzen Gradmessungsar-
beit. Wenn auch die einzelnen Lander in der eingehendsten Weise die Berechnungen in
ihrem Gebiete ausführen, so bleibt doch immer noch viel für die Zusammenfassung zu thun.
Diese aber ist Aufgahe des Gentralbureaus. Leider kann das Centralbureau zur Zeit mit diesen
Rechnungen nicht mehr als zwei Mathematiker betrauen, welche überdies noch oftmals andere
Arbeiten ausführen! müssen. Zur raschen Förderung;der Sache ist es aber nöthig, noch mehr
Rechner zu beschäftigen. Ich erlaube mir daher den Vorschlag, dass aus dem Dotationsfonds
alljährlich bis zu 6000 M. für Honorirung einiger Rechner zur Verfügung gestellt werden,
welche unter der speziellen Leitung meiner ausgezeichneten Mitarbeiter im Centralbureau
sich den zusammenfassenden Arbeiten widmen würden. Vielleicht könnte auch der ganze
ersparte Rest des Dotationsfonds aus der Periode 1887 — 1896 diesem Unternehmen zu

Gute kommen. Ich empfehle meinen Vorschlag der besonderen Beachtung der Finanzkom-
mission,

ae RA Mate à

Tl

11

al nikon Wy

We Teer

hanna ht a Hautes ten nn

 
