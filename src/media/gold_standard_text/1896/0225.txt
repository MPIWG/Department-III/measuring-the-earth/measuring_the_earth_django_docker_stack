VE N |

RETTET

il

it

Wu ml TI

PT pente

RPT

TAN |

NT

219

Immerhin gestatten die bei der vorliegenden photographischen Messungsreihe ge-
machten Erfahrungen, besonders weil der Verfasser auch nach der optischen Polhöhen-
methode Jahre hindurch gearbeitet hat, die Frage nach der Einwirkung der erwähnten
äusseren Umstände auf die photographischen Breitenaufnahmen einigermaassen zu be-
leuchten.

Gegenüber sonstigen photographischen Präcisionsaufnahmen, welche meistens aus-
serhalb des Meridians, bei wechselnder Höhe der Sterne geschehen, ergab sich zunächst für
die vorliegende Beobachtungsart der Vortheil, dass die Extinction des Sternlichtes für beide
Componenten eimes jeden Polhöhenpaares während der ganzen Reihe nahezu constant blieb.
Ferner fielen die sonst bei milgeführtem Instrument unvermeidlichen, durch das Uhrwerk
veranlassten Erschütterungen und Störungen der Abbildung hier fort.

Eine Vergleichung der photographischen und optischen Polhöhenmethode unter
Zugrundelegung ganz identischer Sternpaare dürfte im Allgemeinen weder rathsam noch auch
möglich sein. Bei vorliegender Reihe haben z. B. die beiden ursprünglich als I, und Il,
ausgewählten Sternpaare

Zo. 1088 6.5 opt. R. 1602 6.8 opt.
20.419606 >» 39 Camel. 6.9 »

die optisch sehr brauchbar gewesen wären, nachträglich fortgelassen werden müssen, da
*Z. 1142 und * 39 Camel. photographisch schwächer als 7.5 sich herausstellten. Auch bei
allen übrigen Sternen ist auf möglichst zuverlässige Bestimmung des Unterschiedes zwischen
optischer und photographischer Helligkeit Gewicht gelegt worden. Von im Ganzen 49 Ster-
nen haben bei 27 zum Theil beträchtliche derartige Helligkeitsdifferenzen constatirt werden
können.

Es kann daher sehr wohl vorkommen, dass ein Stern, der auch bei ungünstiger Luft
im Fernrohr deutlich gesehen wird, keine oder kaum messbare Spuren auf der Platte giebt.
Ebenso gut kann aber auch der umgekehrte Fall eintreten.

Ein durch Dämmerlicht beleuchteter Himmelsgrund scheint nach den bisherigen Er-
fahrungen die empfindliche Platte in der That etwas ungünstiger als das Auge zu beeinflussen.
Obwohl bei den vorliegenden Messungen die Dämmerungsstunde möglichst vermieden wurde,
sind doch drei Sternpaare wegen zu heller Dämmerung verloren gegangen. Jan. 27 und 98
kam 34 Minuten nach Sonnenuntergang ein Stern der Grössenclasse 5.3, Febr. 13 32 Minu-
ten nach Sonnenuntergang ein Stern der Grössenclasse 9.7 nicht mehr auf den zu dunkel
erscheinenden Platten zum Vorschein.

Durch Bewölkung sınd im Ganzen 9 Paare verloren gegangen, von denen 7 bei tota-
ler Bewölkung des Himmels als unter keinen Umständen auch sonst sichtbar auszuscheiden
sind. Es können daher eigentlich nur 2 Paare gerechnet werden, welche durch Wolken-
schleier aufgenommen worden sind. Dagegen gelangen die Aufnahmen für 15 Polhöhenpaare
durch mehr oder weniger dichte Girruswolken, obwohl einer der Sterne fast 7. Grösse pho-
tographisch war.

 
