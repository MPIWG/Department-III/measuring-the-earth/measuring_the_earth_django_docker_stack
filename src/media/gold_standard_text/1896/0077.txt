eisen

Die mit Leinölfirniss oder Schellack präparirten Stäbe zeigen leider immer noch zu
grosse Empfindlichkeit gegen Feuchtigkeit, als: dass für die Praxis ein Nutzen damit zu
erzielen wäre. Es wird aber nach geeigneteren Behandlungsweisen auch fernerhin gesucht
werden.

B. Geschäftliche Thäligkeit.

1. Der Dotationsfonds wurde wie bisher verwaltet.
9. Die Versendung zahlreicher Erdmessungs-Publikationen fand auch in diesem

Jahre statt.

Oktober 1896.
HELMERT.

(. Uebersicht der Versendung von Erdmessungs-Publikationen durch das Gentralbureau.

1. Memorial du Dépôt général de la Guerre. Tome XV. Publié par M. le Général

Derrecagaiz. Observations de pendule. Paris, 1894 . . . . . . . 80 Exemplare.
2. Mittheilungen des K. u. K. a Instituts.

XIV Band. 1894, Wien 1895, ose. 80 >
3. Die astronomisch-geodätischen A ses K. u. K. Militar.

geographischen Instituts in Wien, VI. Band. Wien, 1895. . . . 80 d

A. Astronomische 1 der Oesterreich.-Gradmessungs- ne
sion: Bestimmung der Polhöhe und des Azimuts auf den Stationen Spieg-
litzer Schneeberg, Hoher Schneeberg und Wetrnik. Herr Professor Dr

Tinten. Wien) 18959»2. sehn: 94 )
5. Von Herrn Professor Dr Th. Tu in Pe tae ah er

den gegenwärtigen Stand der Erforschung der Breilenvarialion . . . 250 »
6. Yom K. und K. Gradmessungsbureau in Wien: a

Arbeiten. VII. Band. Längenbestimmungen . . . ....124 »

7. Abrisse, Koordinaten und Höhen Ga von ah Tri igo-
nomelrischen dun g der Königl. Landesaufnahme bestimmten Punkte.

XIII. Theil. Regierungsbezirk Potsdam. Berlin 1896 . . . . Hoelea 79 »
8. Procès-verbal de la 39e Séance de la Comnussion nn
Suisse, 11 MAMA . »_,.. +0) 48 »

9. Von der D Donne en Ba der Komgkiehen os
aufnahme in Berlin: Die Königliche Preussische Landes- Triangulation.
Houptdreiecke:: VIE. Theil:Berlin, 18966232 2280 sb. gant wa #29 »
10. Von der Permanenten Commission: Verhandlungen der vom
25. September bis 12. Oktober 1895 in Berlin abgehaltenen X]. Allgemeinen

we

 
