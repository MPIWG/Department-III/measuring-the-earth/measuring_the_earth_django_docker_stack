 

266

= — [09-5449],

expression tirée de |’équation (26), où l’on a supprimé les indices m des lettres 3 et L
|Péquation (26) n’étant qu’un cas particulier d’une relation générale].
Posons :
5 > L 2
(28) ie = [#-9-30+9],
m

6h

 

€ étant une nouvelle variable auxiliaire.

Remplacer, dans l'équation (27/), L, par L (c’est-à-dire par £L») revient à multi-
plier par €2 le second membre de cette relation.

En désignant, dans ce second cas, l’erreur cherchée par p, au lieu de r (toujours afin

d’éviter des confusions), on a donc :
p = Er

Pour connaitre la variation de p entre :

= Om

(ou mieux depuis d = 0, valeur minima de 4,,), et
H—e
ee
H + c

il suffit de chercher la variation du produit :
w (0) — E2p (À),

lequel ne diffère de » que par une série de facteurs constants dans l’espece.

Pour déterminer graphiquement l'allure de W, nous porterons (fig. 5), sur les ordon-
nées de la courbe ®, les valeurs obtenues en multipliant graphiquement — par une construc -
uon dont nous donnons ci-après un exemple (n° 13) — ces ordonnées par le carré du
facteur ¢.

Ce facteur, fonction linéaire de à (équation 28), est représenté sur la figure par une
droite, telle que AB, ayant pour ordonnée à l’origine :

Où PL 3
a, (pour ¢: == 0),

SN

oes

 

 

9 I,

et rencontrant l’axe des abscisses a une distance :

iH =e pone
ee eee COG D

Avec les données de l’exemple précédent, jointes a c = 0,06, on a:

 

$
3
À
i
3
3
4
3
1
i

 
