PE N À

NH RATTE. 7°

nl

ILE

169

schwinden. Dies geschah zuletzt im Jahre 1867; 1882 und 1884 fanden grosse destructive
Erdbeben statt.

Die atmosphärischen Bedingungen sind gleichfalls sehr ungünstige ; liegen doch die
Azoren auf der bekannten Zugstrasse der über den Atlantischen Ozean eilenden Depressionen.
Häufige Regenfälle und während des Winters fast beständige dichte Nebelbildungen beein-
trächtigen das Klima.

Das Leben auf den Azoren, die unter portugiesischer Herrschaft stehen, gleicht für
den Fremden einer Verbannung.

Der Breite von Terceira würde im asiatischen Russland ein Terrain etwa ein Grad
südlich von Samarkand (Sarafshan) entsprechen ; daselbst sind die klimatischen Bedingungen
günstig, dagegen können die politischen Zustände kaum als gesicherte gelten.

In Japan würde die correspondierende Station nördlich von der bedeutenden Stadt
Sendai liegen, wo sowohl die meteorologischen als auch seismologischen Bedingungen
günstige sind.

In Californien endlich. käme ein Ort nördlich von San Franzisko in Frage. Dort
sind die meteorologischen Bedingungen wegen häufiger Nebelbildungen, wenigstens nahe der
Küste, nicht so günstig wie im südlichen Californien.

Im vorstehenden Bericht ist die Combination der Stationen: Licata-Shirakawa-Fel-
ton-Petersburg für den internationalen Polhöhendienst empfohlen worden. Es soll jedoch
durchaus nicht verhehlt werden, dass, von denselben Grundbedingungen und allgemeinen
Betrachtungen ausgehend, nicht noch andere, in mancher Hinsicht vielleicht günstigere Vor-
schläge sich machen liessen. Vorläufig kam es nur darauf an, den allgemeinen Rahmen für
die Gruppirung derartiger Stationen zu entwerfen. Erfahrungsgemäss wird eine Entscheidung
über die endgültige Lage derselben wohl erst nach eingehendsten Ermittlungen an Ort und
Stelle erfolgen können.

serlin, im Mai 1896.
A. MARCUSE.

 
