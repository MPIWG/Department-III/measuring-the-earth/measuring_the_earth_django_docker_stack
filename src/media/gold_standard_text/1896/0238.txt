 

932

Il. — DOUTES EXPRIMES SUR L’EXACTITUDE DES LOIS DU COLONEL GOULIER.

En 1892, notre honorable collègue M. l’amiral de Kalmar a fait adopter par la Con-
férence générale de Bruxelles ! le vœu suivant :

« Vu les valeurs très différentes indiquées dans ce rapport ? pour le coefficient de
dilatation du bois de sapin, valeurs variant entre 35 et 9° par métre et pour 1° centigrade ;
vu qu'on croit avoir constaté une variabilité de ce coefficient avec l’humidité de Pair ; enfin
pour décider si la dilalation du bois de sapin dépend de l'humidité relative ou de la tension
de lu vapeur, le Bureau central est invité à étudier ou à faire étudier l'influence de ces
divers éléments sur la dilatation du bois de sapin, ce qui est très important pour les réduc-
tions des mires employées dans les nivellements de précision ».

D'autre part, dans une lettre du 9 novembre 18933, adressée au professeur von
Helmholtz, président de Pinstitut physico-technique de Berlin, M. le professeur Helmert,
Directeur du Bureau central de l'Association, s'exprime ainsi, 4 propos des variations de
longueur des mires de nivellement pendant leur séjour sur le terrain :

« En général, on constate un allongement croissant qui dépend de la température
ascendante et de lhumidité. Mais 1l existe, sur la nature de cette dépendance, certains
doutes qui ont été augmentés par les recherches précises du colonel francais Goulier.....

« Les résultats obtenus par lui n’expliquent toutefois pas les augmentations de la
longueur des mires, observées pendant l'été, car l'humidité relative varie bien dans le cours
de la campagne, mais, dans l’ensemble, elle n’augmente pas en général.

« L’accroissement de la température seule ne peut pas servir d’explicalion.

€ On doit plutôt admettre, d'après les résultats d’Oertel dans le nivellement bavarois,
que la longueur des maires augmente avec l'humidité absolue de l'air ».

Ayant pu nous procurer le travail ci-dessus visé du Dr Oertel *, nous avons recherché
la cause des anomalies signalées par MM. von Kalmär et Helmert.

III. — OBSERVATIONS DU D' OERTEL.

A l’occasion du nivellement de quelques lignes accidentées de la Bavière et du Pala-

! Procès-Verbaux de la Conférence de Bruxelles, page 400.

2 Rapport de M. Lallemand sur les expériences du colonel Goulier. (Même volume, p. 664.)
3 Voir Comptes rendus de la Conférence d’Innsbruck, en 1894, Procés-Verbaux, p. 82.

i Das Prdctsions-Nivellement in Bayern, rechts des Rheins. — Munich, 1893, p. 3 a 10.

  

MM Lh haha Mani dd dees sb

ta odd.

À tm pen a M a cn

 

 
