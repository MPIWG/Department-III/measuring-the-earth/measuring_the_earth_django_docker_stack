 

 

i
|
|
|

116
Um ferner festzustellen, bis zu welchem Grade der Annäherung die resultirende
Bahn des Poles sich den Pea eh auf den einzelnen Stationen an-
schliesst, habe ich von den obigen Coordinaten « und y ausgehend für die am Schlusse
dieses Berichtes zusammengestellten Beobachtungsreihen die Werthe o — gy auf Grund
der Formel:

P— Po — 2.0084 4 y Sin À

berechnet und durch Vergleichung derselben mit den beobachteten Monatsmitteln die
Beträge abgeleitet, welche durch die ermittelte Polbewegung nicht dargestellt werden.
Die verbleibenden Reste — vergl. die Zahlenwerthe in der letzten Columne der Zu-
sammenstellung der Beobachtungsergebnisse — zeigen vielfach den Character syste-
matischer Abweichungen, was darauf schliessen lässt, dass auch diese Beobachtunes-
reihen zum Theil noch von systematischen Seelen beeinflusst sind.

Wenngleich man im Allgemeinen die Grösse dieser Differenzen als ein Maass
für die mehr oder minder grosse Zuverlässigkeit der verschiedenen Beobachtungs-
reihen wird ansehen können, so ist das gegenwärtig vorliegende Beobachtungsmaterial
doch noch nicht umfangr eich genug, um auf diesem Wege einen zuverlässigen Auf-
schluss über die Güte der einzelnen Reikön zu erlangen. Es kommt besonders hierbei
in Betracht, dass sich der Natur der Aufgabe nach für Stationen, deren Resultate
einen hervorragenden Einfluss auf die Bestimmung der Coordinaten ausüben, die
beobachteten und die berechneten Werthe besser an einander anschliessen müssen.
als für weniger einflussreiche Stationen, sowie dass jede Hinzuziehung einer Station
mit systematisch beeinflussten or bei dem geringen Umfange des Beobachtungs-
materiales die Güte des Anschlusses für are mit normalem Verlauf der Folhohe
schon merklich herabmindert.

Immerhin wird man aber aus der Grösse der verbleibenden Reste ebenso wie
aus den Erfahrungen, welche in Potsdam (vergl. „Verhandlungen der Innsbrucker Con-
ferenz“ pag. 19) und Warschau (vergl. pag. 121 dieses Berichts) gemacht worden
sind, entnehmen, dass man in peinlicher Beseitigung aller Störungsursachen, aus denen
eine systematische Beeinflussung der Resultate hervorgehen könnte, nicht weit genug
gehen kann. Insbesondere wird man darauf bedacht sein müssen, die Beobachtungen
in Räumen vorzunehmen, in denen die Gleichheit der Refractionen nach Norden und
nach Süden hin sowohl innerhalb der einzelnen Nacht, als auch für den ganzen Ver-
lauf des Jahres durch völlig symmetrische Gestaltung derselben und streng centrische
Aufstellung des Instruments thunlichst verbürgt erscheint. Man wird hinsichtlich der
Lage dieser Beobachtungsräume selbst auf die weitere Umgebung Rücksicht zu nehmen
haben und Autfstellungsorte vermeiden, welche einen merklichen Unterschied der
Terrain- sowie der Culturverhaltnisse im Norden und im Süden aufweisen.

Auch wird man auf eine thunlichste Vervollkommnung der Instrumente hinzu-
wirken haben und die Leistungsfähigkeit derselben besonders in optischer Beziehung

 

3
à
3

ae de 2 10 hie

LIL

11

Las tabu

ia Li

iii

ass A à LA LL UE La

  
