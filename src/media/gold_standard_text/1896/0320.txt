 

Discussion über den von der Perma-
nenten Commission und dem neuen
Präsidium gemeinsam an die Regie-
rungen zu richtenden Bericht

Bericht des Centralbureaus über den
jetzigen Stand der Breitenfrage, mit-
getheilt von Herrn Albrecht (Siehe
Beilage A. 1.),

Dritte Sitzung, 19. October 1896

Verlesung und Annahme des Protokolls
der zweiten Sitzung : :
Der Secretar verliest eine Dee
der Amerikanischen Botschaft in Ber-
lin, worin die Ratification der neuen
Uebereinkunft durch die Vereinigten-
Staaten angezeigt wird.

Bericht der Finanzeommissiön,
von Herrn Foerster es

Nach stattgefundener Dee geneh-
migt die Permanente Commission ein-
stimmig die Rechnungen des Jahres
1895 und beschliesst den disponiblen
Rest für die Untersuchung der Lothab-
lenkungen zu verwenden ni

Die Commission beschliesst einstimmig,
dass ein Bericht über die geschäftsfüh-
rung und die wissenschaftlichen Arbei-
ten der letzten Periode, gemeinsam von
der alten Permanenten Commission und
dem neuen Präsidium der Erdmessung
an die Regierungen zu richten ist

Verlesung der Notiz des Herrn Albrecht
über die optische und photogra-
phische Methoden für die Breiten-
beobachtungen (Siehe Beilage A V)

Discussion über diesen Gegenstand, an
. welcher die Herren #oerster, Faye,
Bakhuyzen, Helmert, Hirsch, Lalle-
mand und Bassot sich betheiligen .

Die Permanente Commission beschliesst
mit 5 gegen 4 Stimmen, in Cagliari eine
dritte vergleichende Beobachtungs-
reihe anzustellen, und mit 8 Stimmen,

verlesen

die Instrumente so ausführen zu lassen,
dass sie später für die Anwendung der
photographischen Methode umgeformt
werden können. .

Pag.

80-82

82

83-95

83

83-84

k-87

88

88

88-95

314

 

 

Vierte Sitzung. 24. October 1896

Verlesung und Annahme des Protokolls
der dritten Sitzung .

Die Permanente Coan at al

den am Ende der letzten Sitzung gefass-
ten Beschluss zurück und betraut, auf
Vorschlag der Herren Helmert und
Foerster, das Central-Bureau mit der
Ausführung der dritten vergleichenden
Reihe von Breitenbeobachtungen

Auf Vorschlag des Herrn Foerster be-
schliesst die Conferenz der Familie Tis-
serand telegraphisch ihr Beileid auszu-
sprechen ee

Herr von Zachariae erklärt, dass die von
Herrn Helmert hervorgehobene Lücke
nicht zwischen den Dreiecken von Dä-
nemark und Schweden, sondern inner-
halb des schwedischen Netzes besteht .

Discussion über die Wahl der vier Breiten-
Stationen, an welcher sich die Herren
Faye, Helmert. Bakhuyzen
und Hirsch betheiligen

Vorschläge betreff des Ortes der nächaten
Generalversammlung; werden vorge-
schlagen : Kopenhagen, Hamburg, A m-
sterdam und Wien

Mittheilung der Landes-Berichte

Bericht über die Arbeiten in Belgien, von
Herrn Hennequin (Siehe Annexe B. I.)

Bericht über die Arbeiten in Dänemark,
von Herrn von Zachariae (Siehe An-
nexe B. II.) sue

Bericht über das Nivellement von Frank-
reich, zugleich mit mehreren Notizen,
von Herrn Lallemand (Siehe Annexe
B. IN à,

Bericht über die Arbeiten in den Nieder-
landen, von Herrn Bakhuyzen (Siehe
Annexe B. IVa.)

Bericht über die Vermessung von Suma-
tra, von Herrn Hauptmann Muller
(Siehe AnnexeB. IV ».).

Bericht über die Arbeiten in Preussen,
Geodätisches Institut, von Herrn Hel-
mert (Siehe Beilage B. Va)

Bericht über die Arbeiten in Pr

Foerster.

pet)

eussen,

Pag.

96-107

96

97-98

98-4 04

10%
105-106

105

105

105

105

105

106

 

ay lh i ek ad ak I TUT TEE TER

Lilli

Wal tlic

en _

  

N

  

 
