my

F

 

(EL et RUE

CRU

I ER 8 Lie:

267

as Te A OF == 0,9,

valeurs répondant au cas représenté sur la figure.
On vérifierait aisément que :

Pour à — 0, ona: a, el par suite : U U

» es Onn » é ar > » ==)
H —e

» 0 = re » à === 0, y Wh ()
H + c

Ainsi, la courbe part de l’origine 0 comme la courbe ®. En raison de linégalité
(255), £ étant toujours supérieur à 4, W s'élève d’abord au-dessus de æ; puis, après avoir
atteint un maximum ! en N”, elle rencontre la courbe ® en M, sur ue correspondant
Ad — du; elle revient enfin à l'axe des BR en B, et, cette fois, tangentiellement à cet
axe, comme 1} est facile de s’en assurer *

La seule portion utile de cette Kae est comprise entre le point M et le point B.

Ainsi, dans les conditions où nous nous sommes placés, lorsque l’inclinaison du
terrain eroît depuis le palier jusqu’à la verticale, l'erreur de réfraction varie proportionnel-
lement aux ordonnées du triangle curviligne OMB.

Nulle en palier, cette erreur croît d’abord à peu près proportionnellement à à et par

1 Ce maximum a lieu sensiblement pour :

A H —e
3 H+e

ae
= —- à
3 1

La valeur correspondante de est :

| 1 H—e
8 (H =e) + 6) log | 1 — Tn

Ip zer: Eee ee a a a FF IE
2 Lim?

9 52 2

Pour que ce maximum se présente avant la rencontre des deux courbes en M, il faut :

a

om >

 

ce qui exige {équations 26 et 28 bis) :

Cette condition est remplie dans l'exemple choisi plus haut et représenté sur la figure.
2 On verifierait aisément, en eflet, que :

FE po) des
ze. Vom —— 3
dé rt irre

 
