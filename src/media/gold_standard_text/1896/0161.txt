in IN |

en ap

Supplément À à l'Annexe À. Il.

OFFICE OF THE COAST AND GEODETIC SURVEY
Washington, D. C., September 29, 1896.

Dr. F. R. Helmert, Director ofthe Central Bureau of the International Geodetic
Association.

Dear Sir,

In response to your invitation in circular dated Potsdam, June 20, 1896, respecting
the selection of the most suitable places for the’ observation of the variation of latitude, I
have the pleasure to venture some remarks about the American stations, which may be of
service to you when finally deciding the question. This Survey, of course, is directly in-
terested in the furtherance of this research, inasmuch as our more important astronomic re-
sults, accumulated during nearly half a century, will need a correction to refer them to the
mean position of the pole of rotation, and this applies especially to the astronomic observa-
lions connected with the measure of ares{ yet beyond the contribution expected from each
of the contracting States; this Survey could give no additional help in doing supplementary
work in addition to that laid out by the Commission, though it is hoped and is almost cer-
tain that one or more of our astronomic observatories will lend their active support.

It is already known to you that in general fair climatic and other desirable conditions
exist on both our Atlantic and Pacific coasts within the latitudes indicated, thus making the
selection of the stations largely a matter of local convenience. Confining my remarks to the
four combinations pointed out by Dr. Albrecht as especially favorable/ I would suggest the
following stations as best fitted for occupation and within close latitude requirement.

H. © — 39°8". Eastern station : Leesburg, a railroad station 58 kilometres from
Washington; D. C. Population ahout 1800. 0 == 39°7, x = 77°35’ W. of Greenwich. No
other station closer to Washington is as favorable, unless we deviate several minutes from
the given latitude.

Western station : Marysville, a railroad station north of Sacramento. Population about
4300, 0.398, 3 21235.

G. 9 = 39°8’. Eastern station : Dover, Delaware, a railroad station. Population
about 2900. 9 = 39°10’, 1 = 75°34’.

 

ASSOCIATION GEODESIQUE — 21

 
