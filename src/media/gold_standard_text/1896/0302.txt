 

Beilage B. VII.

BERICHT

über die Gradmessungsarbeiten der astronomisch-geodätischen Gruppe des
k. u. k. militär-geosraphischen Institutes im Jahre 1895.

Die im vorjährigen Berichte angeführten Arbeiten der astronomisch-geodälischen
Gruppe sind sämmtlich zur Ausführung gelangt.

Ueber Antrag der Direetion des k. u. k. militär-geographischen Institutes hat das k.
u. k. Reichs-Kriegsministerium mit Erlass Abth. 5, Nr. 220, vom 7. Februar 1896 die Aus-
führung nachstehender Gradmessunssarbeiten für das Jahr 1896 genehmigt:

1. Zweite, beziehungsweise dritte Nivellements auf den Linien: Suczava-Jacobeni
98 km., Jacobeni-Borsa 78 km., Gzik Szereda-Kronstadt 125 km., Apahida-Tövis 90 km.,
Alvinez-Piski 44km., Szegedin-Csaba 90 km. und Verseez-Vukovar 244 km. Weiters eventuell
die doppelte Messung der Strecke Neusatz-Semlin 140 km.

9. Fortsetzung der Schwerebestimmungen im nordwestlichen Theile von Ungarn, im
Anschlusse an die bisherigen Bestimmungen.

Die Publication der vom k. u. k. militär-geographischen Institute für Gradmessungs-
zwecke ausgeführten Arbeiten ist nach mehrjähriger Unterbrechung wieder aufgenommen
worden.

Im Monate August des vorigen Jahres sind zwei Bände, der V. und VI. erschienen.
Für 1896 wurde die Herausgabe von drei weiteren Bänden bewilligt ; es wird enthalten der
VII. Band Nivellement, der VIII. trigonometrische und der IX. astronomische Arbeiten.

Es ist zu hoffen, dass von nun an diese Publicationen in rascher Aufeinanderfolge
erscheinen werden.

A. PRÆCISIONS-NIVELLEMENT.

Im Sommer 1895 wurde von einer aus vier Nivelleuren zusammengesetzten Abthei-
lung die 280 km. lange Strecke Amstetten-Klagenfurt nivellirt. Es wurden hiebei 78 Höhen-
marken gesetzt und vier meteorologische Stationen einnivellirt.

 

j
3
a
4

dde 4188 43.188 hdi à

Ad ae ian

ati bem bib aa

 
