ayy to À

So mere ARERR ©"

 

Uebergangsjahr angesehen werden kann, oder ob sie vorzieht diese Aufgabe dem neuen Prä-
sidium anheimzugeben, nachdem dasselbe, gemäss Artikel 2, Paragraph 3, der Ueberein-
kunft, durch Gorrespondenz die Ansicht der neuen Permanenten Commission eingeholt haben
wird. »

Der Präsident dankt dem Secretär für seinen ausführlichen Bericht und ertheilt dem
lerrn Professor Helmert das Wort um über das Gentralbureau zu berichten.

Bericht über die Thätigkeit des Centralbureaus seit der Conferenz in Berlin,
October 1895.

A. Wissenschaftliche Thätigkeit.

«1. Meine Herren! Heute habe ich endlich die Freude Ihnen das 2. Heftdes Werkes
über die Kuropaeische Längengradmessung im 52. Breitengrade, von Valentia (Feaghmain) in
Irland bis Warschau, vorlegen zu können, nachdem bereits 1893 das 1. Heft publieirt wurde
und im Jahre 1888 die ersten zusammenfassenden Rechnungen begonnen worden waren.
Dieses 2. Heft ist von den Herren Professor Börsch und Dr Krüger bearbeitet worden. Da
inzwischen auch an dem östlichen Ende der Längengradmessung bei Orsk in Russland durch
Nachmessung ein grober Fehler beseitigt worden ist, so kann die Längengradmessung,
welche bekanntlich von den Astronomen Wilhelm und Otto Struve inaugurirt wurde, als been-
det angesehen werden.

Wenn die zusammenfassende Berechnung des westlichen Theiles des Längenbogens,
der von den 68°9 des ganzen Bogens nur 31-4 umfasst, die Zeit von zwei Rechnern etwa 8
Jahre hindurch beansprucht hat, so fällt das doch nicht allzusehr gegenüber dem grossen
Aufwand von Zeit und Geld für die Beobachtungen selbst ins Gewicht. Andererseits ist her-
vorzuheben, dass bei der im Ceniralbureau ausgeführten Berechnung möglichste Vollstän-
digkeit erstrebt wurde, indem nach dem Grundsatz der modernen: Behandlungsweise der
Gradmessungen überall die Kontrollen ausgenutzt wurden, welche die Gleichungen von La-
place gewähren. Ein kleineres Beispiel hierzu hatte ich schon der Berliner Konferenz von
4895 vorgelegt. Die Ausnutzung der genannten Kontrollen durch eine gemeinsame Ausglei-
chung hat nun in der That die Lothabweichungen in Länge bis zu 1,5 Bogensekunden verän-
dert gegenüber den Ergebnissen des älteren Verfahrens, welches lediglich die geodätischen
Parallelbögen mit den astronomischen Längenunterschieden verbindet. Auch gewährte diese
Ausgleichung der Laplaceschen Gleichungen das Mittel, die relativen Lothabweichungen in
Breite, selbst noch zwischen den Endpunkten Feaghmain und Warschau, sicher herleiten
zu können und hierdurch eine sichere Basis zur Verbindung der Breitengradmessun-
sen, welche diese Linie schneiden, zu erhalten. Zugleich ergab sich hierbei, dass der
Parallel in 52° von einer ebenen (Curve an mehreren Stellen um einige hundert Meter ab-
weicht.

Wichtiger als dieses leizte, nicht gerade überraschende Ergebniss ist aber ein Um-

 
