 

226

on obtient pour Gopenhague
4 = 9,81575 à Copenhague,

valeur qui s'accorde très bien avec le résultat trouvé plus haut et basé seulement sur Potsdam.

La station de Copenhague est située à une hauteur d’environ17". La réduction au niveau
de la mer, y compris la correction pour le relief du terrain, peut être évaluée à 4 ><10 —°,
et pour la valeur réduite de g, on a donc

go = 9,81579

En désignant par yo laccélération normale au niveau de la mer a la latitude 4, on
peut, d’après les dernières publications de M. Helmert, calculer yo par la formule

Yo — 977800 (1 + 0.005310 sin? X) + 35.10 — 5
qui, pour la latitude 55°41"12" de la station de Copenhague, donne :
ne DRS HTE.
Done y. s’accorde admirablement avec go, ce qui indique qu'à Copenhague la valeur

de la pesanteur est normale.

ZACHARIA.

 

3

FAM Aso MOB Ah bd ohh ab dd hah

te sd.

tdi bn

Hibbs babel de

a mama pen aa M ae ann

|
|

 
