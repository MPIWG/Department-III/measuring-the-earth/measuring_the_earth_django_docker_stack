 

 

nion actuelle, oü va étre constituée, pour une période de dix ans, notre Association, sous
une nouvelle forme, plus grande et plus puissante.

« D'abord, c’est la quatrième fois qu’elle aura siégé en Suisse : en premier lieu à
Neuchâtel, sous les auspices du directeur de l'Observatoire, M. Hirsch, notre secrétaire per-
pétuel; puis deux fois à Genève, sous les auspices de MM. Plantamour et Gautier, dont les
noms comptent si honorablement dans la science, enfin, pour la dernière fois dans le canton
de Vaud, à Lausanne, ville où les travailleurs comme nous doivent se sentir comme chez
eux, dans ce milieu éminemment scientifique et où l’enseignement de tous les ordres à jeté
depuis des siècles un si vif éclat.

« Enfin, s’il nous reste quelques moments, nous les consacrerons à l’étude de la magni-
fique exposition de Genève. Il y a là beaucoup à apprendre en fait de science et de ses plus
belles applications. Peut-être visiterons-nous, si le ciel le permet, les sites enchanteurs de
Glion et de Montreux, où tant d'étrangers viennent chercher la santé et le bien-être, grâce à
la température si admirablement ménagée à l'abri de vos montagnes. |

« Vous voyez bien, Messieurs, à en juger par cet accueil, que nous avons été bien
inspirés en choisissant Lausanne pour lieu de celte importante session. »

M. le Président invite le Secrétuire perpétuel à rendre compte de la gestion du bureau
de l'Association pendant le dernier exercice.

vy

Rapport du Secrélaire perpétuel à la Commission permanente.

eee TP NOTE

En réservant, comme d'habitude, au Rapport de M. le Directeur du Bureau central
et à ceux des Délégués des différents pays le soin de rendre compte des travaux scientifiques
accomplis dans le dernier exercice, le Secrétaire se borne à esquisser 4 grands traits l’acti-
vité du bureau de la Commission permanente et à exposer les faits principaux qui intéressent
l'Association géodésique internationale. |

En ce qui concerne d’abord le personnel des délégués de PAssociation, le Secrétaire
a le regret de rappeler que l’un de nos collègues, Ludwig Philipp von Seidel, professeur de
mathématiques à l’université de Munich, nous a été enlevé par la mort le 13 aout dernier, a
l’äge de 75 ans. Bien qu’il ait été inscrit dans la liste des délégués, presque des l’origine de
l'Association, ce savant distingué n’a pas pris une part aclive aux travaux el aux séances
géodésiques, car il s’occupait spécialement plutôt d'astronomie et d'optique, branches dans
lesquelles il a laissé des travaux remarquables, connus suffisamment du monde scientifique
pour nous dispenser de les rappeler ici en détail. Toutefois Seidel a toujours suivi avec
intérêt le développement de notre œuvre, et l’autorité dont il jouissait avec raison dans son
pays nous a été sans doute favorable.

M. Ollo von Struve a donné sa démission de délégué, depuis qu'il a quitté la direc-
tion de l'Observatoire de Poulkowa.

 
