An in

À rep ”*

url

SMT UTE ETT FRAT Tere

Whe

Combination Q: Australien, Gosford, zo" nördlich von Sydney À = — 151°4
Atrika, Malmesbury, 60! nördlich von Kapstadt — 18.7
Amerika, SternwartesSamtiagoy lies joe). + 70.7

Für beide Combinationen ergiebt sich die mathematische Grundlage zu:

Dx Py
D = TS DENT nn 900
4 5 Py 3 De + By
N = 29.6 und 119.6 Dian = 194 Dam — 1.00 1904

Diese Combinationen erweisen sich somit als nicht günstig; doch wird eine
nicht unerhebliche Besserung herbeigeführt, wenn man, an dem Parallel — 33° 27’
festhaltend, die afrikanische Station möglichst weit nach Osten, etwa nach Grahams-
town verlegt. Man erhält alsdann:

Combination R: Australien, Gosford, 50™ nérdl. von Sydney . 4= — 151.4
Avrika, Grahamstown, Kaplande = (2 2 — 26-5
Amerika, Stermwarte Santiago 29 m 2. + 707
Pau Py
Dé L- D, — 12 SS = 9.56
if 55 y 7 Da + py
N= -33°0 und 123.0 DMaz. = 1-84 Da 1. 12 A 0.92

Wenngleich auch diese Combination hinsichtlich der mathematischen Grund-
lage noch Einiges zu wünschen übrig lässt, so lassen sich doch unter Rücksichtnahme
auf die socialen Zustände günstigere Verhältnisse auf der Südhalbkugel nicht auffinden.

Bei dieser Sachlage wird es angezeigt erscheinen, sich gegenwärtig wegen der
erheblich besseren Vorbedingungen auf die Nordhalbkugel zu beschränken. In einem
späteren Stadium der Untersuchung würde aber eine Beobachtungsreihe auf der Süd-
halbkugel, welche auf der Combination R basirt, als eine willkommene Vervollständigung
der Beobachtungen auf der Nordhalbkugel zu betrachten sein.

Wenn im ganzen Verlauf dieser Untersuchung meist nur von ganz bestimmten
Oertlichkeiten die Rede gewesen ist, so soll damit nicht der Meinung Ausdruck gegeben
werden, dass an denselben streng festgehalten werden müsse und dass etwa die Be-
obachtungen innerhalb der betreffenden Städte selbst auszuführen seien. Man wird
im Gegentheil von Letzterem absehen und in der näheren oder weiteren Umgebung
der genannten Ortschaften in dem zu wählenden Parallel geeignete Stationspunkte
ausfindig zu machen suchen, welche die günstigsten Bedingungen für die Ausführung
der Beobachtungen bieten. Insbesondere wird man streng darauf zu sehen haben, dass

6%

 
