 

SS

Noms des Pays.

Désignation
de

la base.

4

 

 

 

Norvege
|

Suisse

Supplement
au rapport
de 1880

 

Base de Bodoe
derriere la petite ville
de Bodoe :

Latitude 67° 15’ N
Longitude 32° 0’
a l’Est de Vile de Fer

Base de Weinfelden

Base de Bellinzona

Base d’Aarberg

 

 

 

 

 

 

 

sous la direction de
M. le G?! Ibanez de
M. le Colonel Dumur et|)
de MM. les Professeurs
Hirsch et Plantamour

 

 

Année Noms Erreurs
de la des Longueurs. | probables et
mesure. observateurs. relatives.
| 1882 | Li-Colonel Seyersted | 4366,6779 | — 0,0018
Capitaine Overgaard | (Longueur are
Lieutenant Ebbesen | provisoire)
|
|
1881 | Sous la direction de | 2540,29996 | + 0,00073 |
M. le Docteur Hirsch | ar
et de M. le Colonel |
Dumur, par onze |
officiers du génie de |
l'armée Suisse |
1881 | Sous la direction de | 3200,36109 | + 0,00037 |
MM. le Professeur TOUTE
Plantamour et de M. le
Colonel Dumur, par |
les mémes officiers |
1880 | Ingénieurs et officiers | 2400 07955 |. 0,00042 |
Espagnols et Suisses | | a.

 

 

|

\u

 
