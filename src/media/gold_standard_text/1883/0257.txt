 

DERE

IM

J

Ik }
I u

DA
ai

tx
ke

à

np

de France.

Tableau No. 4. (Suite)

 

Maximums et Minimums des différents niveaux -de la mer dans
chaque année.

 

 

 

 

 

 

 

 

|
|

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

|
|
Indication |
| Hauteur |
en | Maximums. | Minimums. moyenne. | hrs
Marégraphes. oS
Année. Mois. Jour. hauteur. | Année. Mois. | Jour. (hauteur. |
Marégraphe d'Arcachon.
1877 [Septembre 9 |+8"289| 1877 ; Octobre 31 —1972| +07525 |
| 1878 | Octobre 26 3.274) 1878 Mars 20 1.90) 0.53 |
Marégraphe 1879 Avril 8 3.31 | 1879 | Octobre 16 1.74 0.60
d’Arcachon | 1880 | Février 101 3.11 | 1880 | Janvier 14 1.621 0.50 |
ee... _ || 1881 | Février il 8.30 || 1881 Mars 2 1.68 0.54 |
situé à l'extré-| 1882 | Octobre 27 3.87*| 1882 | Février 20 22061. 0.01 | *) Tempéte du
mité du débarca- | | 27 Octobre.
dere d’Eyrac. | |
Le rapport | Moyenne calculée +0.518 La différence entre
entre les ordon- | | la hauteur moyenne
nées et les vari- | | observée et la moy-
ations du niveau | enne des hautes et
de la mer est | basses mers est de
egal a "in. | | 0724.
Marégraphe de Cette. ¢
(| 1874 | Octobre 16 oa 1874 | Janvier 29 |—0.288| +0.240 Les hauteurs sont
| 1875 | Octobre 20 0.436| 1875 | Février 6 (a) | 0.256 | rapportées au zéro du
Marégraphe || 1876 | Décembre 4 0.636| 1876 Mars 13 0.304) 0.198 | nivellement Bourda-
| 1877 | Janvier 8 (by. | 1877 Mars it 0.240) 0.202 | loue.
de {| 1878 | Octobre 19 0.416] 1878 | Janvier 30 0.206) 0.252 (a) Le crayon est
Cette. || 1879 | Décembre 3 0.642) 1879 Mars 10 0.196 0.220 | sorti de la feuille à
| 1880 | Octobre 9 0.496! 1880 | Décembre 11 0.252] 0.268 || — 0.384, de sorte que
U 1881 | Janvier 19 0.608 1881 | D&cembre 24 0.244) 0.206 | la cote minimum n’a
I | pu être enregistrée.
(b) Le crayon est
Moyenne calculée |-+ 0230 | sorti de la feuille a

 

 

ol”

 

 

—- 0.656, de sorte que
la cote maximum n’a

pu être enregistrée.

(c) Hauteur moy-

|| enne pour l’ensemble

de l’année.

 
