 

|
}
|
|
|

 

47

Der Präsident meint, diesen Antrag einer Specialcommission zuweisen zu sollen
und designirt mit Zustimmung der Versammlung die Herren v. Bakhuyzen, Christie, Cutts,
Schiaparelli, Villarceau für diese Commission. Die genannten Herren erklären sich
hierzu bereit.

Hierauf macht der Präsident der Versammlung die Mittheilung, dass die italie-
nische Commission, um in den Sitzungen der allgemeinen Conferenz ihr numerisches
Uebergewicht nicht im Uebermaasse zur Geltung zu bringen, beschlossen habe, nur mit
den vier Stimmen der dirigirenden Commission bei eventuellen Abstimmungen votiren
zu wollen.

v. Oppolzer bedauert diesen Beschluss, in Folge dessen eine so grosse Zahl
hervorragender Gelehrter auf die Abgabe ihres gewichtigen Votums freiwillig Verzicht
geleistet hat, umsomehr, da bei wissenschaftlichen Fragen keine nationalen Interessen
im Spiele sind, und bezeichnet den Beschluss der italienischen Commission als einen Act
besonderer Courtoisie.

Der Präsident erklärt nunmehr auf den Punkt Il. des Verhandlungsprogramms
überzugehen, stellt die Reihenfolge der Berichterstattung nach der deutschen Alphabet-
ordnung der Länder fest und ersucht Herrn v. Bauernfeind den Bericht für Bayern abzustatten.
(Vergl. Generalbericht für 1883, Bayern.) Herr v. Bauernfeind bringt anschliessend
an seinen Bericht die folgenden Druckwerke zur Vertheilung.

1) Das bayerische Präcisions-Nivellement. Sechste Mittheilung von Carl Max
v. Bauernfeind.

2) Neue Beobachtungen über ‘die tägliche Periode barometrisch bestimmter
Höhen von Carl Max von Bauernfeind.

3) Ergebnisse aus Beobachtungen der terrestrischen Refraction von Carl Max

von Bauernfeind.

4) Bestimmung der Länge des einfachen Secundenpendels auf der Sternwarte

zu Bogenhausen, ausgeführt durch Carl von Orf.

An diesen von Herrn v. Bauernfeind abgestatteten Bericht knüpft sich in so
weit eine Discussion, als die von dem Berichterstatter gemachte Bemerkung, dass
Gebäude, Dämme, Brücken etc., die bereits einige Decennien erbaut sind, noch eine
wohl verbürgte Senkung zeigen, Herrn v. Bakhuyzen zu der Mittheilung Veranlassung
giebt, dass die vor 200 Jahren zur Fixirung des damaligen mittleren Meeresniveau in
den Niederlanden gesetzten Steine nach den jüngst ausgeführten Nivellements nur ganz
unbedeutende, im Maximum auf 7 Millimeter steigende Differenzen in ihrer relativen
Höhe gezeigt haben.

Herr Major Hennequin erstattet hierauf den Bericht für Belgien. (Vergl. General-
bericht für 1883, Belgien.)

Herr Oberst Perrier erhält nun das Wort zur Erstattung des Berichtes über
die in Frankreich zur Ausführung gelangten Arbeiten. (Vgl. Generalbericht für 1883,
Frankreich.) Er leitet seinen Bericht mit der Bemerkung ein, dass er die von ihm im
Haag übernommene Verpflichtung, mit dem Villarceawschen Regulator die Schwere in

 
