 

Ce n’est la qu'un résultat provisoire, qui pourra être modifié mais légèrement par
les calculs définitifs.

Opérations astronomiques.

L’une des missions de l’Académie des Sciences, constituée pour observer le passage
de Vénus en Amérique, a été confiée au Dépôt de la Guerre, sous la direction du Colonel
Perrier, assisté de M. le Com‘ Bassot et de M. le Cap® Defforges.

La mission s’est installée aux Etats-Unis, à St. Augustin, en Floride.

M. Perrier observait avec un équatorial de 8 pouces; M. Bassot avec un
équatorial de 6 pouces, M. Defforges a obtenu 200 photographies du passage avec une
lunette horizontale de 6 pouces.

La longitude a été déterminée télégraphiquement, par rapport à Savannah, une
des stations du réseau des longitudes américaines, par M. le Cap*® Defforges de l'Etat-
major francais et M. Preston assistant du Coast-Survey. Les deux observateurs ont occupé
successivement les deux stations de Savannah et de St. Augustin, chacun avec son
instrument méridien, éliminant ainsi dans la moyenne des résultats, l’équation personnelle
des observateurs et celle des instruments.

M. le Commandant Bassot a mesuré la latitude de la station par quatre séries
de distances zénithales d'étoiles observées à leur passage au méridien.

Mesures de la pesanteur.
Pendule à réversion de Repsold.

Des opérations ont été entreprises à l'Observatoire de Paris, par M. le Cap.
Defforges, dans le but de déterminer l'intensité de la pesanteur, à l’aide d’un pendule à
réversion, construit en 1880 par MM. Repsold frères, à Hambourg, pour le Dépôt de la
Guerre. Ce pendule à une longueur de 056.

L’étalon a été comparé à deux règles du Bureau International des Poids et
Mesures, par M. Benoît, adjoint au Bureau International et M. le Cap° Defforges.

La durée de l’oscillation a été déterminée par la méthode des coincidences.

Le support de Repsold, plus stable que ceux qui avaient été livrés antérieure-
ment par les mêmes constructeurs, reposait sur un fort cube de pierre très solidement
fondé. La flexibilité du support à été ainsi réduite à un nombre assez faible; on a
trouvé par une série d'expériences statiques très concordantes:

e — 0.000008

par suite, la correction, due à cette cause d’erreur, de la longueur du pendule simple
déduite des observations, calculée par la formule de MM. Peirce et Cellérier, atteint une
valeur relativement assez faible:

di = 02000029
3 Centiemes de millimetres environ.

 
