 

Annexe III.

tet Oh

SUR

LA MESURE DES BASES

PAR

M. le Colonel PERRIER.

ie Comptes-Rendus des Conférences géodésiques internationales, tenues en 1877
à Stuttgart et en 1880 à Munich, font connaître tous les détails relatifs aux bases pri-
mordiales mesurées en Europe et en Amérique, jusque et y compris l’année 1880.
Depuis cette époque, six nouvelles bases primordiales ont été mesurées dans les
divers pays qui font partie de votre association. Savoir:
1 Base en Autriche-Hongrie.
1 Base en Amérique (Etats-Unis du Nord).
2 Bases en Norvège.
2 Bases en Suisse.
On trouvera dans les tableaux suivants, conformes au modèle précédemment adopté,
les résultats obtenus et les autres indications utiles à connaître sur chacune de ces
nouvelles bases.

Annexe III. 1

 

 
