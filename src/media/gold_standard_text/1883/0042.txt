 

 

 

 

 

 

28

Die Unification der geographischen Längen durch die Wahl eines einzigen
Ausgangs- Meridians.

I. Der Nutzen der Längen-Unification.

Das Problem, mit dem wir uns heute beschäftigen, stammt in der That schon
aus früher Zeit. Schon im Alterthume aufgestellt, hat besonders seit Beginn der Neu-
zeit, nachdem der Mensch durch die grossen geographischen Entdeckungen Besitz
von der ganzen Erde genommen, das Problem des Ausgangs-Meridians die Geographen,
die wissenschaftlichen Gesellschaften, so wie die Regierungen fast aller civilisirten
Länder beschäftigt. Es würde uns zu weit führen, hier im Einzelnen alle die zum Zwecke
der Unification der geographischen Längen gemachten Vorschläge und Versuche in
Erinnerung zu bringen; wir können um so mehr davon abstehen, als unser vielbetrauerter
College Bruhns in seinem Bericht an den im Jahre 1879 in dieser selben Hauptstadt
tagenden meteorologischen Congress die allgemeinen Grundzüge dieser Geschichte
gegeben hat.

Dennoch dürfte es nicht ohne Interesse sein, gerade an der Hand dieser
Geschichte sich über die hauptsächlichsten Ursachen Rechenschaft zu geben, an welchen
bis jetzt sämmtliche Versuche mehr oder weniger gescheitert sind, um durch die
gewonnene Erkenntniss die vielen auf diesem Felde begangenen Irrthümer leichter ver-
meiden zu können.

Die Hauptschwierigkeit, zu einer allgemeinen Uebereinstimmung in der Wahl
des Ausgangs-Meridians zu gelangen, liegt augenscheinlich zunächst in dem Umstande,

‘dass von dem Augenblicke an, wo wir die Erde als ein Revolutions-Sphäroid ansehen

müssen, überhaupt kein natürlicher Ausgangs-Meridian existirt, das heisst ein Meridian,
der von vornherein angezeigt, oder gar durch die Natur als Längen-Ausgangspunkt
geboten wäre; während die geographischen Breiten naturgemäss und nothwendig vom
Aequator aus gezählt werden. Die scheinbaren Widersprüche, welche man zwischen
den aus den verschiedenen Gradmessungen hervorgegangenen Werthen für die Ab-
plattung zu finden geglaubt hatte, und in Folge deren einige Geodäten zu der Hypothese
eines dreiaxigen Erd-Ellipsoides gelangt waren, verschwinden immer mehr, so dass
man nicht mehr daran denken kann, den durch die grosse oder die kleine Axe des
Erd-Aequators führenden Meridian als Ausgangspunkt vorzuschlagen. Da also sämmt-
liche Meridiane gleichwerthig sind, andererseits auch der Erd-Magnetismus in Folge der
mit der Zeit sich ändernden Declination und der sich daraus ergebenden Verrückung
der magnetischen Meridiane nicht für die Wahl des geographischen Ausgangs-Meridians
dienen kann, so ist diese Wahl nothwendigerweise eine willkürliche und demzufolge rein
praktischen und Zweckmässigkeits-Rücksichten unterworfen.

| Die Folge hiervon war, dass fast in allen, und zwar nicht nur in den durch
ihre maritime Macht oder ihren .wissenschaftlichen Rang hervorragenden Ländern
sondern auch in den übrigen Staaten die Längen nach einem Special-Ausgangs-Meridian

  

  
