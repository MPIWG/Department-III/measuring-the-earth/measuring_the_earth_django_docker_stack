   
   
    
    
    
  
  
       
 

35

  

Triangulation du Paralléle de Bourges. (Partie Orientale.)

 

 

 

 

 

 

 

 

 

 

 

 

bn |
\ N | | | ee ee
Ka INDICATION | Bes nt ne |
N N° LATITUDE. |LONGITUDE.| ÉPOQUE. INSTRUMENTS. | REMARQUES.
DES POINTS. | ET OBSERVATEURS.
© 183 | Mont-de-Sidge. ... | 47°16"12") 29°35’ 0”
184 | Seurre ....... 46 59 53 | 22 48 35 |
185 | Mont-Roland. . .. | 47 7 33 | 23 8 22 ur Z
186 | Signal de Bréri.-. | 46 47 31 | 23 14 32 ae 5
187 | Mont-Poupet. : . . | 46 58 22 | 23 33 4 | : 25 =
188 | LesRoches deMont- | | | = 8 2 =
Truecon .* .,. : 47 14 35 | 23 44 54 | oh coe a
189 | Le Tantillon. ... 47 4 43 | 24 16 10 | a ce 3
F 190 | Le Chasseron . ... 4651 7 | 24 12 10} = SS 8
= 1917| Sk Sorlin...... | 46 43 31 | 23 48 13 | eS 3
F 192 | Mont-Tendre. . . . | 46 35 42 | 23 58 28 | eo D
D 195 | La Dole ...... | 46 25 26 | 23 45 50 |
4 194 | Lausanne. . . ... | 46 81 22: | 24 17 57 |
ie 4 | |

Triangulation du Paralléle de Bourges. (Partie Occidentale.)

 

 

 

 

 

 

 

me. 42 | Bourges ......
= - 43) Dun-le-Roi..... rey woe
89 | Saligny-le-Vif . . Déjà mentionnés.
| MOonlic . . :.. ..
i 195 | La Creusette ... . | 46° 52'33”| 19° 47' 17 °
poo Menétréol .. ..*. | 47 1 7 | 19°30 18
E 94 | Puit-Berteau'. ..: | 47 14 O | 19 45.7 .
Ë 198 | Les Annets. . . . . Alt 24 49 F6 17 =
: Er imeray. :....:. 47043. 19°126 ©
= 200 | La Ronde . .... 47 16 43 | 18 54 20 2
201 | Les Maisons-Rou- eS
. a. 47 6 23 | 18 34 23
_ 202 | LaPagodedeChan- E
È MIO. . 47 23 30 | 18 37 59 5
_ 203 | Ste. Catherine de à
. Fierbois . . . .. 4% 998118195 =
| 204 | Marmande..... 46 07 4 1810 19 ee a
5 poe Chinon ....< ... . AT 13. S18. 3 25 a B a
D 206 | Loudun...>... 47 0 39 | 17 44 34 a es .
= 207 | Peu-Penon..... 47 16 11-| 17 46 28 a so S
= 208 | Puy Notre-Dame. | 47 7 32 | 17 25 39 a Se ‘2
Er 209 Croix oree..... 282133 17% 3638 ak oe es
mm 210) Alligny....... 47 18 83 | 17 20 12 = a a |
D 211 | La Salle... ... AZ 9 991 17 1 30 a Sos à À
Le me} Angers... .-.... 48 98/11 | 17 :6 32 I D
Fr. D La Colle... - .‘. . | 47 O 29 | 17.12 18 2
214) St. Michel-Mont- 4
: | Malehus . .-.-. . 46 49 56 | 16 46 44 n
= 215 | Bressuite...... 4150 53 |.17 10,14 a
=. 216 | La Fribaudière. . | 47 19 17 | 16 44 55 S
- 217 | La Giubliniere . . | 46 57 40 | 16 38 43 2
"> 218 | Moulin Bondut: . | 47 12 44 | 16 25 59 ©
3 Zee | Maisdon....... 47 5 52 | 16 16 40 | a
E 220 | La Croisette du
u Brulot...... 46 57 48:| 16 2 35
D 221) Nantes || 4713 8| 16 6 42
E 222 | La Barre de Vue. | 47 10 39 | 15 45 42
fee 22 | Bouin......../| 46 58 31 | 15 39 49
| D 222 | La Plaine . .... LEO bi 19:26 15
ie: 225 | memrmoiver....| 47° 0 8. 15 25 16

 

 

 

|

au ne

  
