ON.

pis
oJ,
va

al

}

 

SEMI AR ET TT PN Te PT CE N

 

NE

nn

EIERN

 

a

M. v. Helmholtz ajoute que le but de sa proposition est de préserver de la
destruction les repères qui ont de l'importance, non seulement pour les travaux de la
mesure des degrés, mais aussi pour d’autres recherches Scientifiques, par exemple pour
la géologie.

Pour la Prusse en particulier une recommandation de la part de la Conférence
internationale ne serait pas sans utilité pour la conservation des repères fixés par
l'Institut géodésique, à côté des repères officiels installés par la „Landesaufnahme“; car
d’après sa manière de voir on peut, sans danger, laisser subsister les uns à côté des
autres, ces deux espèces de repères, pourvu qu'on les distingue bien et qu’on donne aux
ingénieurs, appelés à s’en servir, les instructions nécessaires.

M. Hirsch soutient vivement cette proposition, en faisant observer les grands
dangers auxquels sont également exposés ces repères dans les autres pays à cause de la
passion de destruction dont sont possédés certains individus, en Suisse surtout Messieurs
les touristes, et il. démontre la nécessité de les faire protéger par des mesures
administratives.

M. Perrier désire que la protection dont on veut couvrir les repères de nivelle-
ment soit étendue aussi aux signaux de triangulation. Messieurs v. Helmholtz et Hirsch
acceptent volontiers cet amendement.

M. v. Bauernfeind fait observer que dans son pays, on a déjà pensé depuis
longtemps a protéger les repéres et les signaux contre la destruction, cependant il constate
qu’en retournant aprés 14 ans sur la ligne du Fichtelgebirge, il a trouvé trés peu de
repères détruits. La proposition de Messieurs v. Helmholtz et Fischer avec l’amendement
de M. Perrier est approuvée à l’unanimité.

M. le Président passe ensuite au troisième point du programme, comprenant les
rapports spéciaux sur les progrès faits dans les travaux pour la mesure des degrés
Tl donne la parole & M. v. Bakhuyzen pour lire son rapport sur les travaux astro-
nomiques, savoir sur les déterminations des longitudes, latitudes et azimuths. (Voir
Annexe Nr. I.)

Pendant cette lecture on distribue deux cartes exécutées par l’Institut géographique
Italien: l’une représentant les différences de longitude, l’autre les déterminations de
latitudes et d’azimuts.

M. v. Bakhuyzen désire qu'avant de donner définitivement son rapport à l’imprimerie,
on envoye un certain nombre d'épreuves aux différents commissaires pour y corriger
éventuellement quelques données.

M. Ferrero cède la présidence à M. Faye, pour lire le rapport sur Pétat des travaux
de la triangulation européenne (Voir Annexe No. II). Pendant la lecture de ce rapport
on distribue & MM. les commissaires des épreuves de ce méme rapport, sur lesquelles M. le
rapporteur prie les membres d’apporter les corrections éventuelles qu’ils trouveront nécessaires.

M. Perrier, en parlant de la lacune dans la triangulation de Tunisie mentionnée
par M. Zerrero, ajoute qu’elle sera remplie dans le courant de l’année prochaine; en
même temps il assure que le voeu exprimé par M. Ferrero d’un rattachement de la Corse

22

 
