35

  

at j senen Figuren würden sich analog den Längenbestimmungen oder trigonometrischen
TR ; BAR mungen Sehr werthvolle Controllen ergeben, welche einen Einblick in die
à à! Verlässlichkeit der Bestimmungen gestatten würden.
. Way Hier in Kronstadt, wo ich mit der Bestimmung der Polhöhe und des Längen-
ty Ë unterschiedes mit Budapest und Sarajevo beschäftigt bin, bestimme ich eben den Unter-
u 3 schied der Schwere auf drei Punkten von sehr verschiedener Höhe, die telegraphisch zu
| K à è 3 CRE . . . . . . .
de à | verbinden mir gelungen ist. Die zu erzielenden Resultate, die seinerzeit in den „Mit-
L, a Ë theilungen des geographischen Institutes“ erscheinen werden, sollen nicht nur zur Prüfung
it der Verlässlichkeit der Methode dienen, sondern wesentlich einen Beitrag liefern für die

Anhaltspunkte zur Reduction der Pendelbeobachtungen auf ein gewisses Niveau.

Mita ; Diese Reduction besteht aus zwei Theilen; der eine umfasst die Reduction vom
EL 8 Beobachtungsorte auf eine Ebene, auf welcher man sich die Berge und Unebenheiten
aufgesetzt denken kann; der zweite hingegen jene von dieser Ebene auf das Normal-

in niveau (Meereshorizont).

ck Als Beitrag zur Ermittelung des ersten Theiles bemühe ich mich zahlreiche

altern Bestimmungen des Unterschiedes der Schwere in der Ebene und auf verschieden geform-

bet Bm q ten Bergen auszuführen, für den zweiten Theil jedoch solche in den Bergwerken, und zwar

age ia | in horizontal liegenden Stollen, wo ich wie z. B. heuer im Winter in dem Francisci Erb- .

ht a > stollen zu Krusna hora bei Beraun in Bohmen den Unterschied der Schwere am Stollen-

à heals | Mundloche und in verschiedenen Entfernungen im Stollen, demnach unter Erdschichten

nu ‘ von verschiedener, jedoch bekannter Mächtigkeit und Dichte zu ermitteln mich bemühte.

vor re lil Ë Ich glaube, dass es nur auf Grundlage zahlreicher solcher Bestimmungen seinerzeit

dea Hit 4 : gelingen wird, die Reductionsformeln für Pendelbeobachtungen den Thatsachen anzupassen,

Ali Bi : was für die Verwerthung der Resultate der Pendelbeobachtungen für den allgemeinen

sk Calcul sehr nothwendig erscheint.

ugs q Wenn ich mir hiermit erlaubt habe, Ihnen, hochgeehrtester Herr Regierungsrath,
| über meine Ansichten und Arbeiten bezüglich der Pendelbeobachtungen Mittheilung zu

Selm, machen, so geschah dies nur in der Ueberzeugung, dass schon manchesmal durch das

pp © 5 Zusammenwirken Vieler, und Ansammlung verschiedener, auch sehr kleiner Beiträge, ja
u selbst mangelhafter Arbeiten, den berufenen hervorragenden Männern der Wissenschaft

ot Ë das Zustandekommen der Lüsung schwieriger Aufgaben etwas erleichtert wurde, und nur

pl mé q in dieser Richtung bitte ich die Veranlassung zu diesem Schreiben zu erblicken.

os : v. Sterneck,

oP Kronstadt, den 24. Juni 1883. Major.

oe |

gp

   
