 

   

50

15) Coordinate di Bonne calcolate di Grado in grado in latitudine e longitudine
supposto il parallelo medio alla latitudine di 50",

C. Reale Accademia dei Lincei.

1) Sulla lunghezza del pendolo a secondi ricerche di G. Pisati et E. Pucev.

2) Esperienze fatte al R. osservatorio del Campidoglio per la determinazione

del valore della gravità. Memoria del socio Lorenzo Respigh.

An Ferrero’s Bericht für Italien schliessen die italienischen Commissare
de Stefanis, Betocchi und Schiaparelli weitere Mittheilungen an.

De Stefanis erwähnt, dass nähere Details über die im militär-topographischen
Institute ausgeführten Arbeiten in den Publicationen enthalten sind, welche zum Theile
zur Vorlage gebracht wurden, zum Theil binnen Kurzem erscheinen werden. Betocchi
hebt hervor, dass zu den bereits vorhandenen zahlreichen Mareographen drei weitere,
nämlich in Palermo, Messina und Cagliari, in Function gebracht worden seien, und ein
vierter noch an einer später zu bestimmenden Lokalität aufgestellt werde. Die Ab-
nahme der von den Apparaten registrirten Kurven sei im Gange, doch erfordere diese
Operation viel Zeit.

Schiaparelli kommt auf eine von Perrier im Berichte über Frankreich gemachte
Bemerkung zurück, dass die Reductionsarbeiten für die Länge Paris-Mailand französischer
Seits so gut als beendet betrachtet werden Können, und knüpft die Mittheilung an, dass die-
selben von italienischer Seite ebensoweit vorgeschritten seien; die Mailänder
Uhrcorrectionen, sowie die Ablesung und Reduction der ausgetauschten Zeitsignale
seien bereits vollständig hergestellt, so dass in wenigen Tagen das definitive Resultat
abgeleitet werden könnte.

Anknüpfend an den italienischen Bericht hebt Faye, der seinen Vorsitz wieder
an Ferrero abgetreten hatte, hervor, dass es möglich sei, durch geodätische Operationen
die heutzutage in Italien so lebhaft verfolgten vulcanischen Erscheinungen näher zu
studiren. Ein Blick auf die ausgezeichnete italienische Generalstabskarte genügt, um
zu zeigen, dass das Massiv des Aetna vollständig isolirt ist, so dass man die Local-
attraktion desselben durch Breiten- und Längenmessungen leicht studiren könne; wenn
es gelungen, die mittlere Dichtigkeit desselben annähernd zu bestimmen, so würde diese
Messung zu entscheiden erlauben, ob unter dem Vulcane wirklich grosse Hohlräume
bestehen, wie dies bisher vielseitig behauptet worden ist.

De Rossi fügt hieran die Bemerkung, dass ihm derartige Bestimmungen bei
einem thätigen Vulcane nicht ausreichend erscheinen, und meint, dass ausserdem ähnliche
Untersuchungen an einem erloschenen Vulcan, wie z. B. am Monte Cavo in Latium
auszuführen wären, welcher Berg sich ebenfalls durch seine isolirte Lage hierzu eigne.

Der Präsident Zerrero weist darauf hin, dass bereits vor etwa 3 Jahren Herr
Schiavoni im Schoosse der italienischen Commission ähnliche Vorschläge gemacht habe,
und hofft, dass diese vorläufig aus Mangel an disponiblen Mitteln zurückgestellten
Arbeiten in den nächsten Jahren zur Durchführung gelangen werden.

   
