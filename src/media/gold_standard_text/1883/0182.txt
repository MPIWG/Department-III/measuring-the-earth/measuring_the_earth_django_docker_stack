 

|
|
|
|
|

168

pour le rattachement de la Corse et de la Sardaigne, je prie de se constituer en com-
mission MM. Clarke, Bassot, de Stefanis, Perrier, Magnagh.

„Je prie MM. les commissaires de se réunir aussitôt après la séance. Les deux
commissions peuvent traiter ensemble les questions qui leur sont renvoyees.“

La Conférence approuve ces propositions et les membres désignés se déclarent
prêts à faire partie des deux commissions.

M. le general Ibanez présente le rapport sur les travaux espagnols pour la mesure
des degrés, et fait distribuer à cette occasion une carte représentant graphiquement les
travaux exécutés en Espagne, ainsi que le dernier volume des annales de l’Institut géo-
oraphique (Memorias del instituto geografico y estadistico, T. 19).

Il présente en outre les feuilles de la carte d'Espagne éxécutées l’année dernière;
elles sont maintenant arrivées au nombre de 21 feuilles.

En se rapportant à la différence de niveau entre l'Océan et la Méditerranée dont
M. Jbañez à parlé dans son rapport, M. Perrier dit qu'en France aussi on à trouvé un
résultat qui se-rapproche beaucoup du chiffre espagnol, savoir 0780.

M. Ibañez demande à M. Perrier, si cette valeur se base sur les anciens nivelle-
ment de Bourdaloue; M. Perrier répond que oui, mais il ajoute que ce nivellement, bien
qwinférieur en exactitude aux opérations actuelles, suffit cependant pour établir le
chiffre approximatif, qwil a indiqué.

M. v. Oppolzer ajoute & cette occasion que par les nivellements autrichiens allant
de Trieste par la Baviére et les lignes de la „Landesaufnahme“ prussienne, on a trouve
des différences semblables entre la mer Adriatique et la Mer du Nord.

M. v. Bakhuyzen fait observer que le zéro normal d'Amsterdam mentionné par
M. v Oppolzer, ne correspond pas au niveau moyen de la mer à Amsterdam, mais se
rapporte plutôt aux hautes eaux moyennes; ce qui fait que la différence trouvée doit être
diminuée de 16 centimètres.

M. v. Bauernfeind ajoute encore que le nivellement bavarois se rattache aux
repères donnés par l'institut géodésique prussien, dont les cotes, fournies par la Landes-
aufnahme différent assez sensiblement.

M. le Président donne la parole à M. Barraquer pour rapporter sur les expériences
de pendule faites en Espagne. (Voir Rapport Général pour 1883, Espagne.)

M. v. Helmholtz demande la parole pour présenter, en son nom et en celui de
M. Fischer la proposition suivante:

„La Conférence géodésique internationale, adresse aux gouvernements
des Etats, qui font partie de l’Association géodésique, la prière de bien vouloir,
autant que cela.est possible par des mesures administratives, faire conserver
les repères qui ont servi dans les différents pays aux nivellements de précision,
afin qu’on puisse les utiliser plus tard pour l'étude des changements de niveau
qui seraient intervenus avec le temps.“

(signé) v. Helmholtz.
(signé) Fischer.

 

 
