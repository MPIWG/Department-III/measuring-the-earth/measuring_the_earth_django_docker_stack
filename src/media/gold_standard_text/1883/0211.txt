 

Ht
ps

 

 

i
E
E
|
:
|
|
F
|
|
1
|
Ë
|
|

197

M. Villarceau ayant retiré sa proposition, l’article IL est adopté par 24 voix.

La resolution III, recommandant Greenwich pour point de départ des longitudes,
étant mise en discussion, M. v. Helmholtz aimerait savoir quelles garanties on aura pour
la conservation indéfinie et précise du méridien de Greenwich, qui, d’aprés le rapport de
M. Hirsch, serait déterminé par le milieu des piliers de l'instrument méridien de cet
observatoire. Or ces instruments et leurs installations ne durent pas éternellement, et
avec le temps il s'opère des changements dans les observatoires.

M. Christie répond que toutes les fois qu’on change quelque chose à l’emplace-
ment d’un instrument à l'Observatoire de Greenwich, comme probablement dans tous les
autres, on prend les soins les plus minitieux pour fixer, par des repères, l’ancienne po-
sition de l’instrument; il n’y à donc rien à craindre de ce chef.

M. /Zirsch ajoute que, l'observatoire de Greenwich se trouvant rattaché géodé-
siquement au réseau de premier ordre, et surtout relié astronomiquement, par des déter-
minations télégraphiques de longitude, à d’autres observatoires nombreux, la colline de
Greenwich pourrait même disparaître par un tremblement de terre, sans que le méridien
fondamental fût compromis ou rendu incertain.

M. Perrier estime cependant que, la jonction entre l'Angleterre et la France
reposant forcement sur quelques triangles seulement, l'observatoire de Greenwich est
géodésiquement moins bien fixé que d’autres points. Toutefois ce n’est pas à ses yeux
le principal motif contre le choix proposé; c’est qu’il préfère un méridien initial océanique,
dont il à exposé, dans la commission, les avantages nombreux. En s’y référant, il pro-
prose l’amendement qu'on choisisse pour premier méridien celui qui est situé à 18° à
l'Ouest de Greenwich.

M. Villarceau a des scrupules géodésiques et astronomiques contre le choix pa-
tronné par la commission; il croit qu'un point de l’équateur vaudrait mieux pour définir
le méridien initial.

M. Foerster ne croit pas nécessaire de répéter tous les arguments qu’on à fait
valoir, contre le choix d’un simple méridien de calcul pour l’origine des longitudes, et
toutes les raisons, par lesquelles on à combattu les méridiens océaniques. Avec la pro-
position de M. Perrier, ce serait encore Greenwich, qui serait le véritable point de départ,
seulement on en cacherait le nom; et pour satisfaire à pareille fantaisie, on obligerait à
changer la numération de longitude pour une foule de cartes géographiques et hydro-
graphiques, et on prétendrait que les éphémerides astronomiques et nautiques fussent
calculées pour un méridien idéal, éloigné certainement de tout observatoire? Le Rapport
et la discussion ont surabondamment établi que le méridien de Greenwich satisfait le mieux
à toutes les exigences de la science et aux considérations pratiques.

Au vote, l'amendement de M. Perrier est rejeté par 18 voix contre 4, et Var-
ticle III adopté par 22 voix contre 5.

Les resolutions IV et V ne rencontrent pas d’opposition et sont adoptées, la pre-
miére par 24, et la seconde par 27 voix.

L’article VI par contre est combattu, d’abord par M. Faye, qui trouve la définition

 
