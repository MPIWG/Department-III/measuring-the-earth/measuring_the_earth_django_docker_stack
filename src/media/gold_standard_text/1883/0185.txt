 

 

|
Ê
f

|
|
|

SEPTIEME SEANCE

de la Conference generale.

Rome, le 20 Octobre 1888.

Président: M. Ferrero.

Secrétaires: MM. Hirsch et v. Oppolzer.

La séance est ouverte a 3° 15”.

Sont prösents les Délégués: MM. v. Bakhuyzen, Barozzi, Barraquer, Bassot, v. Bauern-
feind, Betocchi, Clarke, Faye, Fearnley, Ferrero, v. Forsch, Hartl, v. Helmholtz, Hennequin,
Hirsch, Ibanez, v. Kalmar, Lorenzoni, Magnaghi, Nell, Oberholtzer, v. Oppolzer, Perrier,
Pujacon, Respighi, Rümker, Schiaparelli, Schiavoni, Schols, de Stefanis, Villarceau.

Les Invités: MM. d’Atri, Barilari, Blaserna, Cremona, Galizine, Garbolino, Garcia-
Villar, Giordano, Lasagna, di Legge, Pisati, Pucci, Rosalba, Strome:, Tacchini, de Vita.

Messieurs les Secrétaires donnent lecture, en langue française et allemande, des
procès-verbaux de la 5m et 6m séance; ils sont approuvés après qu'on à tenu compte
de deux petites rectifications proposées par Messieurs v. Bakhuyzen et v. Bauernfeind.

M. Zirsch obtient ensuite la parole pour donner lecture de son rapport sur les
nivellements de précision. (Voir Annexe No. IV.)

A la fin de son rapport M. Hirsch exprime le désir que l’Institut géographique
militaire italien à Florence veuille se charger de la confection d’une carte des nivellements
de précision, puisque le Bureau central ne possède pas les moyens nécessaires pour l’exécuter.

M. le Président répond, qu’il se fera un devoir de communiquer à la Direction
de l'Institut géographique militaire de Florence le désir exprimé par M. Zürsch;
M. de Stefanis qui lui a succédé dans la direction des travaux trigonométriques, se
chargera volontiers, de surveiller l’exécution du canevas des nivellements de précision.

M. de Stefanis se déclare prêt à se charger de ce travail. La discussion étant ouverte
sur le rapport concernant les nivellements, M. v. Forsch fait des communications sur les
jonctions des nivellements russes avec les réseaux autrichiens et allemands; il fait observer

22*

 
