 

 

 

 

 

254

sule italienne et aux iles, soit par des lignes longitudinales soit par des lignes trans-
versales.

Il est à souhaiter que l’Institut géographique militaire puisse donner un plus
erand développement aux travaux dont il s’agit, moyennant le concours d’autres Ministères,

surtout de celui des travaux publics, qui est si intéressé à la connaissance de l’hypsométrie
du pays.
Etudes maréographiques.

5° Les nivellements de précision se rattachent aux maréographes dont une
partie appartient à la Commission géodésique, une partie au ministère des travaux publics
et à celui de la marine. Notre collègue M. le professeur Betocchi, sous la direction duquel
sont placés les travaux de cette nature, vous donnera sans doute des renseignements utiles.

Travaux astronomiques.

6° Le canevas que vous avez devant les yeux, vous donne également l’etat actuel
des travaux astronomiques. L’on a marqué par des lignes droites rouges les différences
de longitudes déterminées par le télégraphe, et par des lignes à traits rouges les diffe-
rences de longitudes qui sont projetées. Nous profiterons de la présence des astronomes
étrangers pour nous mettre d'accord avec eux sur les opérations de longitudes qui intéressent
les états limitrophes, et pour nous renseigner sur l’existence des lignes télégraphiques
nécessaires.

Les déterminations de latitude et d’azimut sont indiquées sur le canevas. Dans
l'avenir, ces déterminations seront développées suivant les exigences scientifiques de notre
Association, et selon les moyens dont nous disposerons. N'ayant pas besoin pour ces
opérations futures de nous mettre d’accord avec les délégués étrangers, je n’ai pas Cru
nécessaire de les faire indiquer sur le canevas.

Pour ce qui concerne les observations de l'intensité de la pesanteur, la Com-
mission à chargé plus spécialement de ces recherches M. le professeur Lorenzoni, directeur
de l'Observatoire de Padoue. |

M. le professeur Respighi, qui a participé 4 tous les travaux astronomiques de la
Commission, a en outre continué de s’occuper de la formation d’un catalogue d’étoiles.

Quant aux calculs concernant les deux différences de longitude Milan-Paris et
Milan-Nice, nous sommes heureux de constater qu’il ne sont pas restés en arriére de ceux
exécutés en France pour le méme but. Les corrections de la pendule de Milan sont
entièrement déterminées, le relevé des signaux echangés et leur réduction sont complets;
de sorte qu’il suffira d’un travail de peu de jours pour arriver à la déduction définitive
des résultats.

Publications.

79 Je termine ce rapport en donnant ici une liste des publications de la Com-
mission géodésique jusqu'à ce jour.

mme

OPT

 

 

ee

 
