 

ee e
„dam x

z-

jr got |
i
or Fis ;

N.
à |

ml E
ME af jy |

 

 

 

 

 

 

 

‘zZ
Z
7 z

 

37

reicht unten in die Cisterne C (eines gewöhnlichen
Wiild’schen Barometers) mit Lederbeutel und Schraube o,
durch deren Drehung das im Instrumente einge-
schlossene Quecksilber auf die Spitze p eingestellt
wird (mit Hilfe eines Mikroskopes). Der obere Schenkel o
steht mit der Cisterne durch ein ein wenig nach-
giebiges Rohr in Verbindung und kann mit dem
Schlitten s durch die Schraube o’ ein wenig gehoben
und gesenkt werden, um das Quecksilber bei »’ ein-
spielen zu machen; die Grösse dieser Bewegung wird
an dem angeschmolzenen Spiegelstreifen So, der eine
Theilung trägt und unter Sw gleitet, mit einem
Mikroskop mit Mikrometer gemessen. Die erhaltenen
Ablesungen geben, nach Reduction auf 0°C., die
Grésse h,—h,. Bei der Beobachtung sollen, aus be-
kannten Griinden, mehrere Spitzenpaare in Anwendung
kommen. Bei b sind Bunten’sche Spitzen anzubringen.

Die Flüssigkeit, deren Druck beobachtet werden
soll, ist derart zu wählen, dass sie weder Quecksilber
noch Fett angreift und die Spannung ihrer Dämpfe
bei 0° C. eher grösser als kleiner als 1 Atmosphäre ist.
Man kann zum Beispiel wasserfreie schwefelige
Säure, die einige Zeit mit Quecksilber geschüttelt
worden ist, zur Anwendung bringen.

Die Flüssigkeit wird in das Gefäss @ ein-
geschlossen und die Luft in bekannter Weise ent-
fernt. Zu diesen Operationen dient der Verbindungs-
hahn h. Bei der Beobachtung wird G in ein Gemisch
aus zerstossenem salzfreiem Eise und Wasser gestellt.

In dem Holzkasten werden zur Bestimmung der
Temperatur einige Thermometer an passenden Stellen
befestigt.

Für den Transport wird das Quecksilber zuerst
in h fallen gelassen, dann h geschlossen, das Queck-
silber alsdann ganz angehoben (wie bei den Wild’schen
Barometern) und das Instrument umgekehrt.

Bemerken will ich nur, dass das ganze Instru-

ment ziemlich klein im Querschnitt ausfallen Könnte und ferner, dass G recht gross (zum

Beispiel 200 ccm) zu wählen ist.
Wien, den 2. Mai 1883.

J. Marek.

 

 

 

 
