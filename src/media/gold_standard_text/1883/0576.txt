 

|
|

—I

10;

+

wird; für die unterste Schichte genügt es (wegen des aus örtlichen Ein-
flüssen entspringenden häufigen Wechsels der Dichtigkeitsverhältnisse), eine
durchschnittliche gleichmässige Dichtigkeit anzunehmen.

. Die Höhe der gleichmässig dichten untersten Schichte lässt sich in einem

gegebenen Falle durch das auf Seite 42 bis 44 der „Zweiten Mittheilung
über die terrestrische Refraction“ beschriebene Messungs- und Rechnungs-
verfahren bestimmen. Ob es gelingen wird, für die fragliche Höhe der
untersten und ihre Dichtigkeit unregelmässig ändernden Luftschichte aus
einer grösseren Reihe solcher an verschiedenen Orten und zu bestimmten
Zeiten angestellten trigonometrischen Messungen eine allgemein gültige
Formel zu finden, mag dahin gestellt bleiben.

. Unter der Annahme einer solchen untersten gleichmässig dichten Luftschichte

von mässiger Höhe zeigen die den beobachteten und berechneten Refractionen,
sowie die den hierauf beruhenden trigonometrischen Höhenbestimmungen
entsprechenden Kurven die gleiche tägliche Periode wie die barometrisch
gefundenen Höhen, und da die Amplituden aller dieser Kurven mit jener
der Wärmekurve übereinstimmen, so hängen sämmtliche hier in Betracht
gezogene Messungsresultate in erster Linie von der Temperatur der Luft
und die Abweichungen zwischen Beobachtung und Rechnung von der wegen
der Bodenstrahlung kaum jemals ganz zu überwindenden Ungenauigkeit der
Temperaturbestimmung ab.

Die statt der Gleichheit beobachteten grossen Unterschiede in den schein-
baren Zenithdistanzen der stark geneigten Richtungen Kampenwand—Höhen-
steig und Kampenwand—Irschenberg, ja selbst die minder grossen Unter-
schiede der scheinbaren Zenithdistanzen in der Richtung Irschenberg—
Höhensteig lassen sich zwar allein aus allenfallsigen in diesen Richtungen
stattfindenden Lothabweichungen erklären, wahrscheinlicher ist es aber,
dass Lothabweichung und Störung des Dichtigkeitsgesetzes in der untersten
Luftschichte zusammen die fraglichen Differenzen hervorgerufen haben.
Eine genaue Entscheidung hierüber ist nur von der Seitens der K. Bayeri-
schen Gradmessungs-Commission in Aussicht genommenen Bestimmung der
Grösse der Lothabweichungen auf den Punkten Kampenwand, Irschenberg
und Höhensteig zu erwarten.

Als das Gesammt-Resultat meiner aus fortgesetzten Studien gewonnenen
Anschauungen über den ge&enwärtigen Stand unserer Kenntniss der atmos-
phärischen Strahlenbrechung glaube ich schliesslich den Satz aufstellen
zu dürfen:

Die in den Jahren 1864 und 1866 von Bauernfeind aufgestellte Theorie
der atmosphärischen Strahlenbrechung genügt allen Anforderungen zur Be-
stimmung der astronomischen und der terrestrischen Refraction, wenn das
ihr zu Grunde liegende Dichtigkeitsgesetz auf. die von lokalen Störungen

 

|

a a nn sa a

 
