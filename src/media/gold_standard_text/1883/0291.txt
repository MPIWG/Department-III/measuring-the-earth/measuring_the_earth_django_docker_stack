 

 

V. Bericht des Sections-Chefs Dr. Löw.

Im Sommer 1883 wurde der mir unterstellten Section vom Herrn Präsidenten
behufs genauerer Umgrenzung des Störungsgebiets des Harzgebirges die Bestimmung der
Polhöhe und eines Azimuthes auf Neubau bei Aschersleben, und der Polhöhe auf den
Punkten Huyseburg bei Halberstadt und Köterberg bei Höxter aufgetragen. Es konnten
jedoch uur die beiden ersteren Punkte erledigt werden. Die Beobachtungen wurden von
Herrn Werner und mir ausgeführt. Die definitiven Reductionen sind noch im Gange;
die vorläufigen Resultate zeigen jedoch, dass die ausgewählten Punkte in der That nahe
der äusseren Nulllinie und daher sehr nahe auf dem den Berechnungen zu Grunde gelegten
Bessel’schen Ellipsoide liegen.

Die Hauptthitigkeit der Section bestand in der Sichtung und Aufarbeitung älteren
Beobachtungsmaterials, hauptsächlich in der weiteren Bearbeitung des Märkisch-Thüringi-
schen Netzes. Aus dem Berichte meines Amtsvorgängers Herrn Geh. Reg.-Raths Sadebeck
vom vorigen Jahre sind die allgemeinen Angaben über die Anordnung der betreffenden
Kette zu ersehen, und wie derselbe Bericht angiebt, wurde in jenem Jahre die Aus-
gleichung bis zur Berechnung der definitiven Richtungen, einschliesslich der Prüfung der
Bedingungsgleichungen, fortgeführt. Von den Assistenten der Section: den Herren Werner
und Dr. Börsch, wurde die Bearbeitung des Netzes fortgesetzt und in Bezug auf Rech-
nung zum Abschluss gebracht.

Bei der Berechnung der Entfernungen der Dreieckspunkte ist die Länge der Seite
Inselsberg— Brocken, hergeleitet aus der Bonner Basis, zu Grunde gelegt, so dass eine
Vergleichung der Bonner und der Berliner Basis mittelst der Seiten Eichberg—Glienicke
und Glienicke —Colberg, welche die Verbindung zwischen der Küstenvermessung und der
Märkisch-Thüringischen Kette bewirken, erfolgen kann. Die Länge der Berliner Grund-
linie, hergeleitet aus der Bonner Grundlinie, wird gefunden zu 3.0787103 (Toisen), wäh-
rend die directe Messung ergab 3.0787220. Die Differenz beträgt daher 117 Einheiten der
7. Decimale oder 37459 der Länge.

Gelegentlich der Winkelmessungen sind auf den Punkten: Petersberg, Strauch,
Hubertusberg, Herzberg und Hagelsberg des Märkisch-Thüringischen Netzes die Polhöhe
und das Azimuth, wie es eben die zu Gebote stehende Zeit zuliess, mehr oder weniger
vollständig bestimmt. Mit Genehmigung des Herrn Präsidenten wurden alle diese Be-

obachtungen, auch die nicht vollständigen, vedueirt und sind dieselben beinahe vollendet.
Löw.

 
