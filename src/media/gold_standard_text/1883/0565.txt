Ti as |
Lu
lu ti |
a y :
Tei |
M, bnp à
lin Dis |
de ler à
ment 7

\
Vu

determi |

ee |
fe mm
pine
100 des 4
| root 4
dés pe
set |

ps
as
sible?

 

 

ie big ;
Q CR i |
Mt ag, |

if |
ry

57

est porbablement la source principale de cette incertitude notable; d’ailleurs les auteurs
déclarent que leurs expériences ne sont que provisoires, et ils espérent vaincre les der-
niéres difficultés en effectuant leurs expériences dans le vide.

En résumant toutes les considérations qui précédent, on peut en tirer les con-

clusions suivantes.

1° Le pendule à réversion de Bessel possède à un très haut degré
toutes les qualités requises pour les déterminations absolues
de la pesanteur, si l’on fait osciller deux pendules d’un poids
essentiellement différent sur le même support.

2% Il faut non seulement employer les mêmes couteaux pour les
deux pendules, mais ces couteaux doivent en outre pouvoir
être échangés pour chaque pendule. Les couteaux en agate
sont préférables à ceux en acier.

3° Il faut faire les observations dans des localités d’une tempéra-
ture presque constante; l’emploi du vide n’est pas recomman-
dable.

4° Les durées d’oscillation doivent être observées pour les deux
positions du pendule dans les mêmes limites d'amplitude.

ll. Partie.

Déterminations relatives de la pesanteur.

D’après les remarques faites au commencement de ce rapport, ce sont précisément
les déterminations relatives de la pesanteur qui ont une importance particulière pour
la géodésie.

Les méthodes proposées dans ce but peuvent étre divisées en 4 groupes, savoir:

1° Vemploi du pendule invariable,

2° Vemploi du régulateur de Villarceau,
3° Vemploi de l’élasticité des gaz,

4° Vemploi de l’elasticite des métaux.

Nous allons examiner séparément pour ces quatre groupes les matiéres abon-
dantes qui se rapportent 4 chacun deux.

1. Le pendule invariable.

Si l’on fait abstraction des changements produits par la température, l’expérience
nous apprend que, dans les limites de précision qu’on peut atteindre, il est possible de
construire des pendules invariables.

Les expériences faites sur ce pendule démontrent qu’il y a un avantage réel
dans lemploi des couteaux en agate; l’acier en effet est plus sujet aux déformations, et

Annexe YVIb. 8

 

|
|
i
|
|

 
