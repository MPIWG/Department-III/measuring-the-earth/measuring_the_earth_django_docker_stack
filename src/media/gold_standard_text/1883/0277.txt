 

SS 7 € OS

wv

=

 

 

emmener EIRE A DIN TRE PRET

263

b. Ein Registrirapparat von Zipp.

c. Das nach den Angaben des Herrn k. k. Regierungsrathes und Professors
Dr. Ritter v. Oppolzer construirte Schaltbrett,*) auf welchem sich alle zur
Regulirung der jeweiligen Richtung und Stärke der Ströme dienenden Vor-
richtungen (Commutatoren, Rheostat etc.) befinden.

Die Installirung der astronomischen Instrumente wird durch die vorher beschriebene
Anordnung der Pfeiler wesentlich vereinfacht, indem man nur nöthig hat, mit dem Univer-
sal-Instrumente die Richtung des Meridianes zu bestimmen und dann die Einführung
des einen Passagenrohres in den Meridian und des zweiten Passagenrohres in den de
Vertical einfach durch Collimirung auf das Fernrohr des Universal-Instrumentes bewirken
kann. Auch das Rectificiren der drei Instrumente in Bezug auf den Collimationsfehler,
die Einstellungskreise etc. wird durch die angegebene gegenseitige Stellung der Pfeiler
erleichtert und entfällt überdies das Erheben der Reductions-Elemente der Instrumenten-
Aufstellungen unter einander.

Auf dem Széchényihegy bei Budapest stand noch — wie bereits erwähnt — das
im Jahre 1877 zum Zwecke der Längenunterschied-Messungen mit Wien Türkenschanze,
Krakau und Pola Sternwarte gebaute Observatorium.

Dieses wurde anfangs Mai 1883 ausgebessert, und wurden sodann die zu den
Beobachtungen erforderlichen Instrumente und Apparate installirt, welche Arbeiten am
12. Mai beendigt waren.

In der Zeit vom 13. bis 18. Mai waren hier die für die drei projectirten
Stationen bestimmten Beobachter vereinigt, um die persönliche Gleichnng zu bestimmen,
was an 4 Abenden durch Beobachtung von Sternpassagen (ungefähr 1000 Fadenantritte
von jedem Beobachter) geschah.

Der Bau des Observatoriums in Kronstadt, auf der nördlichen vorspringenden
Kuppe des Schlossberges, in der unmittelbaren Nähe des Kastells und die Aufstellung
der Instrumente daselbst war am 2. Juni beendet.

Nicht so günstig gestalteten sich die Verhältnisse am südöstlichen Endpunkte
der Basis nächst Sarajevo, wo die Beschaffung sowohl des nöthigen Baumateriales, als
auch der Arbeitskräfte zum Baue des Feldobservatoriums nicht unbedeutende Schwierig-
keiten bereitete.

Diese und andere Ursachen verzögerten die Aufstellung der Instrumente, so dass
hier mit den Beobachtungen erst am 13. Juni begonnen werden konnte.

Bis 13. Juli waren 8 Beobachtungs-Abende zwischen den 3 Stationen: Basis
Sarajevo, Kronstadt und Budapest gelungen, wovon an fünfen gleichzeitig auf allen drei
Stationen dieselben Sterne beobachtet werden konnten, die übrigen Abende aber nur je
zwei Stationen umfassten.

*) Das Schaltbrett der österreichischen Gradmessung, von Dr. Th. Ritter v. Oppolzer. Sitzungsbe-
richte der kais. Akademie der Wissenschaften LXIX. Band, 1874.

 
