 

 

288

En appliquant à ces résultats inmédiats les formules de M. Cellérier, afin d'éliminer
les effets de la flexibilité du Support, on à pour la longueur absolue du pendule simple
à Madrid (Observatoire astronomique latitude 40° 24 30”, altitude 657")

Par les Déterminations 1 et 2 1 - 0929525 75)

a : 1 et 4 0.9929365 +81
. 3 et 2 0.9929620 +65
3 et 4 0.9929440 +88

d’où l’on déduit, en ayant égard aux poids, le résultat total le plus probable
L — 0.9929518 +34

D'autre part les expériences spéciales sur les déplacements de l'axe de sus-
pension fournissent les corrections suivantes à introduire dans les valeurs ci-dessus
du pendule simple, obtenues séparément, pour tenir compte du balancement du support:

Pour la Combinaison Pendule lourd couteaux et plan en acier +.0.0002056 33
on ” > 5 : » n enagate +0.0002016 +4
” ” ” ” leger ” ” ” ” ” +0.0000931 0,
et par conséquent, les longueurs corrigées du pendule simple, seront:

D’après la ire Détermination Los 0.9929610 28

2 : 0.9929574 E27

ome = 0.9929460 +49

4me 7 0.9929448 +42

et le résultat total le plus probable

L = 0.9929554 +17

Petit appareil.

Valeurs calculées pour la longueur du pendule simple faisant & Madrid (Obser-
vatoire astronomique) une oscillation dans une seconde de temps moyen et dans le vide,
exprimées en parties de l’échelle aux temperatures moyennes des observations, et sans
corriger de l’effet du balancement du support.

Par la 5™¢ Détermination Pendule lourd, Couteaux et plan en agate 0.9926659 4-18
Te Ban

0 » » léger neue » : 00 as

T= 69948

|
|
|
|

 

 
