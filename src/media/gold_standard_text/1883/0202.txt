 

 

 

188

mais aussi de théorie et de principe, que la „Connaissance des temps“ a subis dans le
courant de son existence, et principalement sous la direction de M. Loewy, lequel n’a
jamais hésité a introduire les réformes les plus radicales, quand il les a reconnues utiles,
et qui par ce radicalisme a mérité grandement de l’astronomie et de la „Connaissance
des temps“.

M. Hirsch ne peut reconnaître comme fondés, dans l’exposé de M. Loewy, que les
arguments en faveur de la conservation du méridien local pour les éphémérides de pas-
sage. Pour les phénomènes des marées, il y a en effet certaines données, par exemple
les établissements de ports, qu'il conviendra toujours de donner en temps local.

En conséquence, il propose de modifier la fin de la première résolution, en y
introduisant la réserve ,que le méridien unique doit être employé dans les almanachs
astronomiques et nautiques, à l'exception des données pour lesquelles il convient de con-
server un méridien rapproché, comme pour les éphémérides de passage, ou bien qu’il
faut indiquer en heure locale, comme les établissements de ports etc.“

Avec cet amendement, la première résolution est adoptée à l'unanimité, avec la
réserve de la part de M. Faye, qu'il croit toute cette réforme uniquement justifiée par
les besoins pratiques de l’unification de l’heure. —

La deuxième résolution du rapport, devenue maintenant le No. III, recommandant
pour méridien initial celui de Greenwich, a été adoptée par toutes les voix contre celle
de M. Faye.

A propos de la troisième résolution, M. v. Oppolzer fait remarquer qu'après avoir
émis une déclaration de principe en faveur de la division centésimale, il serait peu’
conséquent de parler de 360°.

D'un autre cöte, MM. Rümker et Magnaghi se déclarent contre la désignation
des longitudes en heures sur les cartes, du moins sur les cartes marines, où le navi-
gateur a besoin de trouver les longitudes indiquées en arc. On convient de laisser
de côté la seconde partie de cette proposition, et de se borner & recommander que, dé-
sormais, les longitudes soient comptées seulement dans un seul sens, de l'Ouest à l'Est
à partir du méridien initial.

Dans cette forme la éroësième résolution (devenue No. IV) est adoptée par 6 voix
contre une.

La quatrième résolution (actuellement le No. V), en faveur d’une heure universelle
pour Certains besoins scientifiques et pratiques, est adoptée à l’unanimité.

La cinquième proposition Au rapport a soulevé une longue discussion.

M. Christie voudrait supprimer la différence entre le jour astronomique et le
jour civil, en adoptant le minuit de Greenwich pour le commencement de l’un et de
l’autre. Il ne voit pas de nécessité de conserver le midi comme origine du jour astro-
nomique, Surtout maintenant que le travail astronomique du jour gagne de plus en plus
sur le travail de nuit. Ce dualisme donne souvent lieu à de fâcheuses erreurs de date.
D'un autre côté, M. Christie craint que la différence qu'on introduirait ainsi entre la date

a
5
F
:

nn

 

a SS

|
|
|
i]
| 4
14
|
|
|

Wy

 
