 

 

gang 1882) qu’on pourrait, vu les circonstances, considérer comme rigoureuses, et qui
font atteindre le but sans trop de calculs.

La détermination des corrections à appliquer à la durée de l’oscillation provenant
de l'influence de l'air est bien plus difficile. Cette influence se fait sentir de trois
manieres.

D'abord ce milieu oppose une certaine résistance à l’oscillation du pendule. Cette
résistance, dans les limites des amplitudes dont il est question, n’exerce pas directement
une influence appréciable sur la durée de oscillation, mais elle se fait sentir indirecte-
ment par la diminution des élongations, en causant des changements appréciables dans
la réduction à l’arc infiniment petit.

Ensuite le pendule, même quand ilse trouve en repos, est sujet à la poussée del’air, dont
l'effet peut être calculé en général avec une exactitude suffisante, mais il reste toujours le
doute, que cette poussée pourrait être une fonction de la vitesse du pendule en oscillation.

Enfin à la surface du pendule en oscillation adhèrent des particules d’air qui
sont mises en mouvement avec celui-ci; à cause du frottement intérieur de lair, elles
entraînent alors d’autres particules d’air et augmentent ainsi le moment d'inertie du
pendule. Tandis que les deux premières influences peuvent être déterminées théoriquement
sans trop de peine et avec une exactitude suffisante, la détermination théorique de la
dernière rencontre des difficultés presque insurmontables, de sorte que jusqu’à présent elle
n’a été possible que pour peu de formes très simples du pendule et encore avec une assez
faible approximation. Le présent rapport devra donc d’abord citer les méthodes pro-
posées pour la détermination de cette influence, et les examiner au point de vue de leur
valeur et de leur sécurité.

Pour éliminer cette source d'erreur on peut faire osciller le pendule dans le vide,
ou bien déterminer empiriquement les coëfficients par un arrangement convenable des
observations, comme Bessel l’a fait avec son pendule à fil, ou bien on peut se servir du
pendule à réversion de Bessel. Mais on peut faire des objections contre chacune de ces
méthodes. A la determination dans le vide s’attachent bien de complications, mais on
peut toujours la designer comme un moyen radical; néanmoins dans ce cas on peut
encore craindre que par suite de l’adhésion des particules d’air restent attachées a la
surface du pendule influencant ainsi, quoiqu’à un moindre degré, la précision des résultats.
En outre les expériences qu'on à faites sur ce point (voir la publication susmentionnée
du India Survey pag. [60|—[94]) ont montré qu’il est assez difficile de déterminer la valeur
précise de cette réduction; car d’un coté on ne peut pas parvenir a faire le vide absolu,
comme la théorie le demande, et d’un autre côté on ne peut pas formuler d’une manière
rigoureuse la loi qu’on est obligé de déduire des observations empiriques, de sorte que la
réduction au vide des observations faites dans l’air raréfié reste toujours entachée d’une
certaine incertitude et inexactitude.

Si l’on fait osciller dans le vide le pendule à réversion de Besse, les avantages
de ce pendule sont modifiés où amoindries par certains inconvénients; d’un côté, il n’est
pas toujours facile de rétablir après le retournement les conditions antérieures; d’un autre

|
|

 

 

 
