 

156

DEUXIEME SEANCE

de la Conférence générale.

Rome, le 16 Oct. 1883.

Présidence de M. Zerrero.

Sécrétaires MM. Hirsch et v. Oppolzer.

Le président ouvre la séance à 21 30.

Présents MM. les commissaires: v. Bakhuyzen, Barozzi, Barraquer, Bassot,
v. Bauernfeind, Betocchi, Christie, Clarke, Outts, Faye, Fergola, Ferrero, v. Forsch, Fearnley,
Fischer, Förster, v. Hartl, v. Helmholtz, Hennequin, Hirsch, Ibanez, v. Kalmar, Lorenzont,
Loewy, Magnaghi, Mayo, Nell, Oberholtzer, v. Oppolzer, Perrier, Pujagon, Respighi,
Riimker, Schiaparelli, Schiavoni, Schols, de Stefanis, Villarceau.

Invités MM. d’Atri, Barilari, Blaserna, Cannizzaro, Cremona, Garbolino, Gori,
Lasagna, di Legge, Pisati, Pucci, de Rossi, Tacchini, dalla Vedova.

MM. Hirsch et v. Oppolzer donnent lecture du procès-verbal de la dernière séance,
en langue francaise et allemande; il est accepté, aprés que M. Perrier a demande et
obtenu une legère rectification au sujet de ce que M. Villarceaw avait dit hier.

M. le Président suspend la séance pour quelques minutes pour présenter a
Vassemblée S. E. M. le Duc Zorlonia, Syndic de Rome, qui, empéché d’assister a la séance
d'ouverture, exprime aujourd’hui la satisfaction que lui et la Municipalité de Rome
éprouvent de pouvoir offrir l'hospitalité du Capitole à une assemblée scientifique de cette
importance. L'assemblée témoigne, par des applaudissements, de sa reconnaissance pour ce
gracieux accueil.

M. le Président après avoir repris la séance, lit un télégramme qu'il vient de
recevoir de S. E. M. le Ministre des affaires étrangères, actuellement à Naples: Le voici:
Colonnello Ferrero presidente Conferenza geodetica Roma.

,Assente della Capitale impedito intervenire, mi associo alle dichiarazioni
del mio collega ministro della pubblica istruzione e la prego dare anche in
mio nome il benvenuto agli illustri stranieri scienziati eminenti, rendendosi
interpreta dei miei sentimenti di ammirazione e delle vive simpatie dell’Italia.

(signé) Mancini.

 

 

 
