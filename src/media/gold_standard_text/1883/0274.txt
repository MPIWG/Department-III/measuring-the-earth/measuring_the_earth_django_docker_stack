 

 

 

 

 

 

 

 

 

 

 

 

 

ait pu achever, ils furent retardés dans leur travail par des brumes continuelles, jusqu’a
la fin du mois de Septembre, de sorte qu’ils ont été obligés de laisser pour l’annee
prochaine les Stations Leer et Pilsum, situées ainsi que Emden sur le territoire
Allemand.

Nivellement de précision. Le nivellement de précision à été continué sous la
direction de MM. Z7. G. van de Sande Bakhuyzen et G. van Diesen par M. l'ingénieur
C. Lely, assisté par des ingénieurs qui venaient de sortir de l’Ecole Polytechnique et par
quelques élèves du cours supérieur de cette école.

Les lignes nivelées ont été les suivantes:

Sur le territoire des Pays-Bas:

19 Gorkum-Papendrecht-Zwyndrecht-Dordrecht-Moerdyk, longueur
41 Kilometres.

20 Breda-Filburg-Oirschot-Best, longueur 45 Kilomètres.

39 Filburg-Sught, longueur 18 Kiïlomètres.

49 S’Hertogenbosch-Grave-Nymegen, longueur 49 Kilometres.

Pour la jonction des nivellements Néerlandais et Belges:

59 Woensdrecht-Putten, longueur 11 Kilometres.
6° Breda-Strybeck, longueur 10 Kilométres.

7° Maarheeze-Budel, longueur 6 Kilometres.

8° Pey-Maeseyck, longueur 10 Kilometres.

99 Wyk (Maastricht)-Visé, longueur 12 Kilometres.

Enfin on à remesuré quelques parties des lignes Arnhem-Deventer et Moer-
dyk-West-Kapelle, sur une longueur totale de 13 Kilometres.

Une partie de la première ligne à été nivelée trois fois, le reste de cette ligne
ainsi que les lignes 2—9 deux fois; de sorte que la longueur totale des cheminements
est de 443 Kilometres.

Pour autant que les caleuls sont achevés, aucun polygone nouveau n’a été fermé
par les nivellements exécutés en 1880. Seulement l'erreur du polygone Amersfoort-
Deventer Zutfen-Arnhem est monté de 1 à 4 millimètres par le renivellement de
quelques parties de la ligne Arnhem-Deventer.

Publications sous le titre: Publications de la Commission géodésique Neer-
landaise I. Détermination à Utrecht, de l’azimut d’Amersfoort par J. A. C. Oudemans,
membre de la Commission. La Haye chez Martinus Nyhoff 1881; un rapport sur la
détermination de l’azimut Utrecht-Amersfoort à été publié par les soins de la
Commission et envoyé aux différents membres de l’Association géodésique internationale.

Personnel de la Commission. La Commission géodésique Neerlandaise à été com-
plétée par la nomination de M. Ch. M. Schols, professeur à l’Ecole polytechnique à Delft,
comme membre de la commission, par arrêté royal du 23 Juillet 1881. La Commission
lui à confié la charge de Secrétaire à la place de M. J. Bosscha.

Quoique ne se rapportant plus à l’année 1881, la Commission doit faire mention
de la perte qu’elle a subie par la mort de son président M. F. J. Stamkart décédé

 

TEE TR PRE LLL LLL LL A I er

tt

OS. om

rennes

an m np eee

 

 

eee — — —

 
