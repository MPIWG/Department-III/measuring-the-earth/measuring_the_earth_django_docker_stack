|

 

 

 

 

 

 

 

 

 

 

174

Passant à un autre point du rapport, il est d’avis que trois ans ne suffiraient pas pour
fournir la véritable moyenne de la mer; il faudra pour l’océan surtout embrasser toute
la période lunaire des marées.

M. Hirsch se déclare contre l'exclusion d’une partie quelconque des courbes,
quelles que troublées qu’elles soient; théoriquement on s’expose ainsi à fausser la véritable
moyenne qu'on veut obtenir. Du reste les tempêtes excercent sur la hauteur de l’eau
non seulement une influence positive, mais dans d’autres phases une influence dépressive
dont l'intensité se compensera avec l’autre nécessairement à la longue; et enfin il lui
semblé impossible de fixer les limites où commence et où finit le caractère cyclonique
du mouvement de l’eau; la limite entre un fort vent et une tempête serait nécessairement
arbitraire.

M. Jbañnez se déclare parfaitement d'accord avec cette manière de voir et demande
que la Conférence se déclare formellement contre toute exclusion d’une partie quelconque
des données fournies par les maréographes, attendu que, par un pareil procédé, on s’expose
à fausser les données de l'observation. La difficulté signalée par M. Zetocchi pour le
relevé de ces courbes tourmentées n'existe en réalité pas, puisque le planimètre parcourt
facilement le zig-zag le plus compliqué; dans la pratique de ses maréographes, il n’a
jamais rencontré aucune difficulté de cette nature.

M. Betocchi a voulu dire seulement, que les contours de ces zig-zag se confondant
facilement, l'exactitude des relevés devient forcément moindre. Toutefois lui même ne
les à jamais exclus complêtement du calcul.

M. v. Æelmholtz estime qu’on peut échapper assez facilement à l'inconvénient des
mouvements trop violents des vagues, si l’on met le puits du maréographe en communi-
cation avec la mer au moyen d’un canal d’un diamètre relativement faible par rapport
aux dimensions du puits. Il partage l'opinion de MM. Hirsch et Ibañez qu'il n’est pas
permis d’exclure du calcul une courbe quelconque, quelle que tourmentée qu’elle soit par
des tempêtes, puisque on ne peut parvenir à éliminer les nombreux termes périodiques
d’une période plus on moins longue, que par une continuité rigoureuse des courbes;
l'exclusion d’un intervalle quelconque ne serait admissible que si l’on pouvait parie
compte par le calcul de l'influence des termes périodiques contenus dans cette partie, ce
qui n’est guère possible.

M. Ibanez remarque que la communication entre la mer et le puits du maréo-
graphe au moyen d’un tuyau relativement étroit proposée par M. v. Zelmholtz, est en
effet employée dans la plupart des maréographes.

M. v. Bakhuyzen croit cependant que si l’on faisait le tuyau de communication
trop étroit, non seulement les oscillations du maréographe se trouveraient très retardées,
mais certains mouvements caractéristiques de la mer, à périodes courtes, comme on en
observe par exemple au Helder, ne se marqueraient plus du tout.

Une discussion s'engage entre MM. v. Bakhuyzen, Ibanez et v. Helmholtz sur les
limites à observer pour les diamètres de ces tuyaux de communication. M. v. Felmholtz
estime qu'il faudrait fixer ce diamètre par rapport à celui du puits de telle façon que

 

 

|
|
|
|
|

 
