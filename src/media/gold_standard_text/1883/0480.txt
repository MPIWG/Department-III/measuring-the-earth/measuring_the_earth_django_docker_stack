 

|
|

 

 

 

2

j'ai eu l'honneur de vous adresser il y a trois ans, en vous priant de bien vouloir y ré-
pondre, si possible avant le 15 Juin.

Il s’agit naturellement de compléter les données pour les trois années écoulées,
d'indiquer les modifications survenues et les progrès réalisés; là où il n’y a pas eu de
changements, veuillez en faire la remarque.

Vous faciliteriez grandement le travail de coordination, si vous vouliez suivre,
dans votre réponse, l’ordre et le numérotage des questions posées et tenir compte des
observations faites dans mon rapport de 1880 (Comptes Rendus de 1880. Annexe VII).

En ajoutant un croquis, ou une carte à petite échelle, représentant le réseau de
votre pays, exécuté ou projeté, vous fourniriez au Bureau Central la possibilité de faire
exécuter une carte d'ensemble des nivellements de précision.

Veuillez agréer, Monsieur et très honoré collègue, l'assurance de ma parfaite
considération.

Dr. Ad. Hirsch.

Questionnaire.
1. Longueur (en Kilomètres) des lignes nivelées jusqu'à présent:
a) une fois;
b) nivelées à double dans le même sens;
c) nivelées à double en sens inverse.
Suivent-elles les chemins de fer, les routes ou des canaux?

2. Combien de repères des différents ordres at-on fixé? quelle est leur distance
moyenne? de quelle nature sont-ils? comment sont-ils fixés? sont-ils à trait
horizontal, fixés sur des murs, ou des cylindres verticaux à surface horizontale,
fixés dans le sol?

8. a) Dans quels points votre réseau est-il rattaché à ceux des pays limitrophes ?

sur quelle frontière et dans quels points la jonction reste-t-elle à faire?
b) dans quels points aboutit-il à la mer et existe-t-il des maréographes dans
ces points?

4. À quel plan de comparaison provisoire avez-vous rapporté les cotes de votre
réseau? quel est le repère fondamental qui le détermine? et quelle cote pro-
visoire attribue-t-on à ce repère fondamental ?

5. Description sommaire des appareils employés:

a) Ouverture et grossissement des lunettes? nombre et distance des fils
du reticule?

b) Valeur d’une partie de niveau?

c) Longueur et division des mires; de quelle facon reposent-elles sur le sol?
par quel moyen assure-t-on leur verticalité?

6. Description sommaire des méthodes d'opération:

a) Est-ce que toutes les lignes sont nivelées à double et en sens inverse?

 

 
