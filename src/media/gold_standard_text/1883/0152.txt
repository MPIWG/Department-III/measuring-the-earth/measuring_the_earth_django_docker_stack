 

|
|
|
|
|
|
|

|

138

Rapport

sur l'unification des longitudes par Vadoption d’un premier Meridien unique, et sur
l'introduction d’une heure universelle.

Pour la première fois, notre Association scientifique doit s'occuper d’un objet qui,
tout en ayant une utilité générale pour la science, et sans manquer de rapports évidents
avec les études spéciales que nous poursuivons, a cependant avant tout une importance
pratique.

Si notre Commission permanente a cru devoir porter la question de l'unification
des longitudes au programme de cette Conférence, elle l’a fait, d’abord parce qu’elle
en à été nantie par quelques Gouvernements faisant partie de l'Association; mais aussi
parce qu’elle à jugé qu’en s’occupant de ce problème, elle ne sortirait ni de sa compétence
scientifique, ni de son mandat spécial.

En effet, il ne viendra à l’esprit de personne, de contester la convenance de con-
sulter les astronomes et les géodésiens, en premier lieu, sur des questions de longitudes
et d'heures, puisque ce sont eux qui les déterminent scientifiquement et qui s’en servent
continuellement dans leurs études. Et notre Association géodésique, qui a le plus con-
tribué a étendre et a perfectionner les déterminations télégraphiques de longitudes, et
qui a couvert le continent Européen d’un réseau de longitudes fondamentales, ne doit
elle pas s’interesser a l’accomplissement du progrès incontestable qu’on réalisera pour
la géographie, en ramenant toutes les longitudes à un seul et même méridien initial?

Du reste, Messieurs, les temps ne sont plus où la science pure croyait déroger
à sa dignité, en s’occupant, à l’occasion, de sujets d’une utilité pratique générale, et où
les gouvernements et les administrations croyaient pouvoir se dispenser de consulter les
savants Sur des questions qui touchent à la science. La théorie et la pratique, sans se
confondre, ne sont plus en opposition; l'idéal moderne comprend à la fois le culte de
la vérité abstraite et les applications utiles.

L'œuvre de notre Association, tout en ayant pour but essentiel l'étude de la
figure et des dimensions terrestres, n’est nullement compromise, mais au contraire se
trouve singulièrement favorisée par le fait, qu’elle fournit en même temps aux grandes
administrations civiles et militaires les bases des relevés topographiques, et aux ingénieurs
les repères fondamentaux pour leurs nivellements.

Pourquoi n’aiderait elle pas à réaliser un progrès important pour la navigation,
la Cartographie et la géographie en général, ainsi que pour le service des grandes
institutions modernes de communication, les chemins de fer et les télégraphes?

Si la compétence, en même temps que la convenance de s'occuper de cette
question, sont reconnues par l’Association géodesique, sa Commission permanente a cependant

 

 

 
