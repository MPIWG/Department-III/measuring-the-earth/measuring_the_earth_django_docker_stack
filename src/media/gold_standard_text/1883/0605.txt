 

|
|
|

I. Amerika.

A. Nachtrage zu ,,Literatur der praktischen und theoretischen Gradmessungs-
Arbeiten‘ bearbeitet von M. Sadebeck, Berlin, 1881. (Enthalten im General-Berichte
der E. G. für 1880. Anhang IX.)

1. Dem „General Index of Scientific Papers contained in the annual reports of
the U. S. Coast and Geodetic Survey from 1845 to 1880 inclusive; Appendix No. 6,
Report for 1881“ entnommen.

Bond, W. C. Differences of longitude between Cambridge and Liverpool observatories.
D O0. LIU pm)
Boutelle, C. O0. Geodetic night-signals. (R. C. S. f. 1880, pp. 96—109.)
Cutts, R. D. Memoranda relating to the field-work of a secondary triangulation.
(R. C. 8. f. 1868, pp. 109—139.)
Davidson, G. Improved open vertical clamp for telescopes of theodolites and meridian
instruments. (R. C. 8. f. 1877, pp. 182—83.)
— Description of a new meridian instrument. (R. C. S. f. 1879, pp. 103—9.)
u Meridian and equal-altitude instruments. (R. C. S. f. 1867, pp. 138—9.)
Einbeck, W. Improvement on the Hipp-chronograph. (R. C. 8. f. 1872, p. 266.)
Gould, B. A. Report and tables on the declinations of standard time-stars. (R. C. S.
f. 1865, pp. 152—4.)
_ Report and tables on the positions and proper motions of the four polar stars.
(i. E.,8. £. 1865,. pp:4155—9.)
Hilgard, J. E. The relations of the lawful standards of measure of the U. S. to those
of Great-Britain. (R. C. S. f. 1876, pp. 402—6.)
— Comparison of American and British standard yards. (R. C. S. £. 1877,
pp. 148—81.)
—  Base-apparatus. (R. C. S. f. 1880, pp. 341—4.)
—  Intervisibility of stations. (R. C. 8S. f. 1873, p. 137.)
—  Anexamination of three new 20-inch theodolites. (R. C. S. f. 1877, pp. 114—47.)
— Two forms of portable personal equation apparatus. (R. C. 8S. f. 1874, pp.
156—9.)

 

 
