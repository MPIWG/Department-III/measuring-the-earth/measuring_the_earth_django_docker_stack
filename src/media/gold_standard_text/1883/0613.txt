 

 

 

J 1 urn
armani“ F
Kaum:

 

 

13

Bureau des longitudes. Connaissance des temps ou des mouvements célestes a l’usage
des astronomes et des navigateurs pour 1883, 84 et 85. Paris, 1881, 82 et 83.

Comité international du bureau des poids et mesures. Procès-verbaux des séances de
1881. Paris, 1882.

— Travaux et mémoires du bureau international des poids et mesures. Tome I.
Paris, 1881.

Faye, H. A. E. A. Sur une lettre du général Stebnitzki relative à la figure de la terre.
(0. 2. 1883. No. 6, p. 508)

Loewy. Description sommaire d’un nouveau système d’&quatoriaux et de son installation
à l’observatoire de Paris. (C. R. XCVI 1883, mars.)

— Deux méthodes nouvelles pour la determination des ascensions droites des
étoiles, polaires et de l’inclinaison de l’axe du instrument méridien au
dessus de l’équateur. (C. R. XCVI 1883, avril.)

— Nouvelles méthodes pour la détermination de la position relative de l’équateur
instrumental par rapport à l'équateur réel et des déclinaisons absolues des
étoiles et de la latitude absolue. (C. R. XCVI 1883, mai.)

Loewy et Périgaud. Détermination de la flexion horizontale, de la flexion laterale et
de la flexion de l’axe instrumental du cercle méridien de Bischoffsheim à
Vaide du normal apparail. (C. R. XCIL 1881.)

— Etude des flexions de Grand Cercle Méridien.

Rouget, Ch. Sur un procédé d'observation astronomique à l'usage des voyageurs, les
dispensant de la mesure des angles pour la détermination de la latitude,
du temps sidéral et de la longitude. (C. R. XCII 1881, pp. 27 et 69.)

Villarceau, Y. Mécanique céleste. Exposé des méthodes de Wronski et composantes
des forces perturbatrices suivant les axes mobiles. Paris, 1881.

== Expériences sur l’emploi des régulateurs isochrones a ailettes dans la
détermination de la pesanteur relative, faites 4 l’Observatoire de Paris.
(General-Bericht d. E. G. pro 1881/82. Annexe.)

—  (Compensirung der Biegung der astronomischen Fernröhre. (©. R. XCIIL, p. 866.)

Wolf, C. Historische Untersuchungen über die Urmaasse des Pariser Observatoriums.
£, I. und III. Apch. (Anm. de chim. er -plys. Jan, 1882)

8. Hamburg.

Reitz, F. H. Ueber einen Hülfsheliotropen am Fernrohre. (Z. f. Instr., I. Jahrgang,
1881, p. 338.)
— Das Periheliotrop. (Z. f. Instr., III. Jahrgang, 1883, 3. Heft.)
— Apparat zum Messen yon Grundlinien. (Z. f. V., Bd. X, 1881, p. 233.)

 
