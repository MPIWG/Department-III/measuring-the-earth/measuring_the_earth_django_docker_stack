 

k
k

na ann nn

253

Pour avoir un système complet et rationnel de bases, distribuées de manière que
l'erreur probable des côtés les plus éloignés ne depasse pas les limites établies, il reste
encore une lacune. En effet dans le quadrilatére, qui à, à peu près, pour sommets les bases du
Tessin, d’Udine, de Foggia, et d'Ozieri, il n’y à aucune base mesurée. C’est vers le centre
de ce quadrilatère, c’est à dire dans le voisinage de Grosseto, en Toscane, que se trouve
l'endroit le mieux placé pour y mesurer une base.

Cette base sera mesurée probablement avant la fin de 1886.

Etalonnage.

30 Jusqu'en 1879 les comparaisons des mesures se faisaient sous la direction
du Professeur Schiavoni dans le local de Pizzofalcone à Naples; mais depuis la suppression
de la section de Naples il a fallu chercher à Florence un local pour faire les comparaisons
et les étalonnages des mesures. La Commission géodésique avait proposé de profiter du
terrain adjacent à l'observatoire d’Arcetri pour y bâtir un local approprié à cette sorte
de mesures: mais n'ayant pu obtenir la cession du terrain en question, la direction
de l'Institut géographique à fait adapter pour les étalonnages un local dans d’assez
bonnes conditions, annexe de l’Institut même, avec l’avantage d’une plus grande com-
modité pour les observateurs.

Outre le comparateur de Bessel en usage chez-nous depuis 1858, et un ancien
comparateur de Troughton, l’on doit installer dans ce local un comparateur de Wanschaff
et un comparateur spécial pour les mires de nivellement.

Il serait cependant à désirer qu'un accord s’établit entre la ‘Commission géodé-
sique et la Commission des poids et mesures, pour que celle-ci construisit un local dans
les conditions les plus parfaites pour les comparaisons des mesures de haute précision.

Comme il est inévitable que la Commission royale des poids et mesures construise
un établissement complet et à la hauteur de la science moderne, il est à souhaiter que
la Commission géodésique puisse trouver, dans cet établissement modèle, les comparateurs,
les étalons et les chambres à température constante, dont elle aurait besoin pour l’&ta-
lonnage des règles géodésiques.

Nivellements de précision.

40 L'état actuel des nivellements de précision est indiqué sur le canevas par
des lignes rouges qui suivent les routes sur lesquelles ils ont été exécutés.

Ces nivellements ont eu leur développement dans la Haute Italie, pour les coor-
donner avec les levés topographiques de cette région; mais en satisfaisant à ce but pratique,
on a obtenu en même temps un résultat très utile pour la science, en réunissant la mer
de Genes avec l’Adriatique et en joignant ces mers avec la Baltique et la mer du Nord
par l'intermédiaire des nivellements de la Suisse et de l’Autriche.

Il n’est pas besoin de dire que ces nivellements seront étendus à toute la penin-

 
