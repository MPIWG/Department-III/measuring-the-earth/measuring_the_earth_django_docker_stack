 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

  

128

de la Commission permanente, et ont provoqué un vote par correspondance pour decider
si ce sujet devait étre admis dans le programme de cette année. — La circulaire était

ainsi concue:

Madrid, le 18 Décembre 1882.

Monsieur et trés honoré collégue.

Votre Bureau a recu derniérement, de la part du Sénat de la ville libre et
hanséatique de Hambourg, la demande de s’occuper de la question du Premier Méridien,
commun pour toutes les nations, et de provoquer une décision de notre Commission sur
ce sujet. Monsieur le Dr. Kirchenpauer, Chef du Département du Commerce et de
Navigation de Hambourg, aprés avoir rappelé que cette question du Premier Méridien a
été souvent traitée dans des réunions scientifiques, et dernièrement dans le Congrès
géographique à Venise, qui a décidé de la porter devant les Gouvernements et les
sociétés géographiques des différents pays, explique que les tendances d’unification dans
ce domaine sont accueillies avec une vive sympathie dans une ville comme Hambourg,
où tous les intérêts sont basés sur la navigation et sur les rapports avec les pays
lointains.

La Société géographique de Hambourg, consultée par le Sénat, lui à adressé un
rapport, (dont M. le Dr. Kirchenpauer nous communique une copie) dans lequel, recon-
naissant la nécessité d’une entente sur un Premier Méridien général, elle se prononce en
faveur de l’adoption de celui de Greenwich, et propose, comme meilleur moyen d’avancer
la solution de la question, d’en nantir la Commission permanente de l’Association g&odesique.

Le Senat de Hambourg, partageant les voeux des sociétés scientifiques, pour que,
par une entente des pays civilisés, on parvienne a s’accorder généralement sur le choix
du Méridien de Greenwich comme Premier Méridien définitif, espére que, si la Commission
géodésique internationale voulait mettre dans la balance le poids de son autorité géné-
ralement reconnue dans ces matiéres, cette question, en suspens depuis des siécles,
trouverait bientöt une solution. En conséquence, il nous prie de prendre les mesures
convenables pour que la Commission géodésique internationale s’occupe de la question et
prenne une décision.

Votre Bureau, Monsieur, estime que, vis à vis d’une démarche aussi flatteuse de
la part d'un Gouvernement qui a témoigné à notre oeuvre scientifique un si grand
intérêt, et qui nous a reçus, il y a quelques années, à Hambourg, avec une aimable hospitalité
dont nous avons tous gardé le plus agréable souvenir, la Commission permanente ne
saurait se refuser à s’occuper d’une question qui — si elle n’a peut-être pas de rapports
directs avec les études géodésiques spéciales auxquelles nous travaillons — ne manque
certainement pas d'importance, non-seulement au point de vue général de la civilisation,
mais en particulier pour le progres des sciences géopraphiques, qui ne saurait étre
indifférent à l’Association géodésique internationale.

  
     
  
   
   
   
 
 
 
 
   
   
   
 
 
 
   
   
   
 
  
  
   
      
    
   
 
 
 
   
   
     
   

 

sialic 2
AE RENTEN TEEN

 

 
