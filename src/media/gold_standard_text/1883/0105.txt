 

i

a

> O4

a

B.S

a

se 5

oe

 

ete SCTE EEE PET NP TE

|
F
|
|
i
|
|
|
|

91

übrigens an, dass er die Construction auf der Decimaltheilung des Kreises basirter acht-
stelliger trigonometrischer Tafeln angefangen habe, welche bald erscheinen werden.
Hirsch glaubt nicht, dass die Centesimaltheilung des Kreises irgendwo für astro-
nomische und geodätische Instrumente, ausser auf den Trommeln der Micrometerschrauben,
gebräuchlich sei. Selbst in Frankreich sind solche Kreise, so viel er weiss, eine seltene

‘Ausnahme. Z/irsch bedauert, dass seine verehrten französischen Collegen, auf ihren

ersten von der Commission. verworfenen Vorschlag zurückgehen; denn der von dieser
angenommene Artikel II ist das Resultat eines Compromisses zwischen den absoluten
Gegnern und den übertriebenen Anhängern der Decimaltheilungen und dieser Compromiss
würde in dem Augenblick fallen, in welchem man auf die Decimaltheilung der Zeit und
des ganzen Kreises zurückgreifen wollte.

Nachdem Villarceau seinen Vorschlag zurückgezogen, wird der Artikel II mit
24 Simmen angenommen.

Die Resolution III, welche Greenwich als Ausgangspunkt der Längenzählung
empfiehlt, wird zur Discussion gebracht. v. ZZelmholtz wünscht zu wissen, welche Garan-
tieen man für die fortdauernde und genaue Erhaltung des Greenwicher Meridians habe,
welcher nach dem Berichte des Herrn Zörsch durch die Mitte der Pfeiler des Meridian-
Instrumentes dieser Sternwarte bestimmt wäre. Die Instrumente und ihre Aufstellung
dauern nicht ewig und mit der Zeit vollführen sich Aenderungen auf den Sternwarten.

Christie erwidert, dass jedesmal, wenn man etwas an der Aufstellung eines
Instrumentes auf der Greenwicher Sternwarte ändere, man wie auf allen anderen Stern-
warten die grösste Sorgfalt aufwende, um durch Markirungen die alte Lage des Instru-
mentes zu fixiren, so dass also aus diesem Grunde nichts zu fürchten sei.

Hirsch fügt hinzu, dass sogar, da die Sternwarte von Greenwich geodätisch mit
einem Netze erster Ordnung, und besonders astronomisch durch Längenbestimmungen mit
zahlreichen anderen Sternwarten verbunden ist, der Greenwicher Hügel durch ein Erd-
beben verschwinden könnte, ohne dass der Fundamentalmeridian gefährdet oder unsicher
gemacht würde.

Perrier meint nichtsdestoweniger, dass die Sternwarte von Greenwich geodätisch
weniger gut fixirt ist, als andere Punkte, da die Verbindung zwischen Frankreich
und England nothwendigerweise nur auf einigen Dreiecken beruht. Das ist übrigens in
seinen Augen nicht das Hauptmotiv gegen die vorgeschlagene Wahl; er zieht den ocea-
nischen Anfangsmeridian vor, dessen zahlreiche Vortheile er in der Commission aus-
einandergesetzt hat. Indem er sich hierauf bezieht, schlägt er das Amendement vor,
man möge als ersten Meridian denjenigen wählen, welcher 18° westlich von Greenwich liegt.

Villarceau hat geodätische und astronomische Bedenken gegen die von der Com-
mission begünstigte Wahl. Er glaubt, dass ein Punkt des Aequators besser dazu dienen
würde, den Anfangsmeridian zu definiren.

Förster hält es für unnöthig, alle Argumente, welche man gegen die Wahl eines
einfachen Rechnungsmeridianes als Ausgangspunkt der Längenzählung angeführt, und alle
Gründe, mit denen man die oceanischen Meridiane bekämpft hat, zu wiederholen.

12*

 
