 

31

welchen die Benutzung eines naheliegenden Meridians für die Sternwarten eines jeden
Landes bietet, indem dadurch die Interpolation für die Astronomen erleichtert wird; die
Variationen der gegebenen Grössen, um deren Interpolation es sich handelt, sind ge-
wöhnlich so schwach und so regelmässig, dass die Grösse des der Entfernung der
Meridiane entsprechenden Tages-Bruchtheils ohne Bedeutung und man in keinem Falle
zur Anwendung der Logarithmen genöthigt ist.

In der Meteorologie endlich, und in andern Zweigen der Erd-Physik, z. B.
im Gebiete des Erd-Magnetismus, hat zwar die Ortszeit eine überwiegende Bedeutung,
doch ist es nothwendig, für gewisse Untersuchungen die Beobachtungen auf denselben
Moment zu reduciren und synoptische Karten zu construiren, so dass auch diese Wissen-
schaften jedenfalls aus der Unification der geographischen Längen und der Zeit Vortheil
ziehen würden. Der meteorologische Congress zu Rom hatte daher diese Unification
auch schon vor dem in Venedig zusammengetretenen geographischen Congresse angeregt.

Es lässt sich also wohl behaupten, dass in unserer Zeit, wo die unermessliche
wissenschaftliche Arbeit, welche die Menschheit vollbringt, nicht mehr in zwei oder drei
bevorzugten Mittelpunkten zusammengedrängt ist, sondern sich über alle eivilisirten
‚Länder erstreckt, von denen jedes einen kleineren oder grösseren Theil zum gemein-
schaftlichen Schatze beiträgt, die Unification der geographischen Längen als ein wirk-
liches Bedürfniss für alle diejenigen Wissenschaften anerkannt und gefordert werden
muss, welche in irgend einer Weise mit der Geographie in Verbindung stehen.

Vom Standpunkte des praktischen Nutzens ist der Vortheil noch bei weitem
grösser und augenscheinlicher. Ist es wohl nöthig noch weitläufig auseinander zu setzen,
welche Wohlthat es für die Seefahrer aller Länder sein würde, für ihre täglichen Rech-
nungen nur noch mit einer einzigen Längenart zu thun zu haben, wenn der Kapitain,
in welchem Meere er auch steuere, welcher Küste er sich nähere, die von ihm zu be-
nutzende Specialkarte nach demselben Meridiane gezeichnet fände, welcher dem nautischen
Jahrbuche zu Grunde liegt, nach dessen Angaben er seine Ortsbestimmungen gerechnet
hat? Ist es schon für den Astronomen und Geodäten in seinem Arbeitszimmer, für den
Generalstabs-Officier in seinem Bureau und für den Cartographen in seiner Werkstätte
unangenehm, sich mit der Umrechnung der verschiedenen Längen aufhalten zu müssen,
wie viel mehr noch für den Seefahrer am Bord des Schiffes, mitten im Sturme, wo
durch diese Reductions- und Uebertragungsarbeit stets eine kostbare Zeit verloren geht
und zuweilen gar verhängnissvolle Irrthümer herbeigeführt werden können. Auch für
die Topographie und Cartographie würde ein einziger Ausgangsmeridian von unleugbarem
Nutzen sein. In den topographischen und hydrographischen Bureaus beschäftigt man
sich allerdings vorzugsweise mit den Aufnahmen des eigenen Landes; aber man ist doch
genöthigt, seine Karten an die des Nachbarlandes anzuschliessen, man muss die Auf-
nahmen der andern Staaten für die Hydrographie der an die eigenen Colonien grenzenden
Meere zu Rathe ziehen; weshalb also zu der Verschiedenheit der Maasstäbe und Pro-
jectionen auch noch die der Längen hinzufügen wollen?

Und welchen grossen Vortheil würde die nicht officielle, d. h. die industrielle

 
