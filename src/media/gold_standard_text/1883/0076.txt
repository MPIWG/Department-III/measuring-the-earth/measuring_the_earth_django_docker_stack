  

 

 

 

 

62
Der Berichterstatter ersucht schliesslich, dass das Centralbureau angegangen
- werde, ihm von der letzten Correctur seines Berichtes eine genügende Anzahl von
Exemplaren zur Verfügung stellen zu wollen, um dieselben den einzelnen Commissaren
zur definitiven Richtigstellung vorzulegen.

Der Präsident Zerrero tritt nunmehr seinen Vorsitz an /aye ab, um über den
jetzigen Stand der Gradmessungstriangulationen Bericht zu erstatten. Dieser Bericht
findet sich als Annex II. den vorliegenden Protokollen beigegeben.

Perrier theilt bezüglich der in Ferrero’s Bericht hervorgehobenen Lücke in
den das mittelländische Meer einschliessenden Dreiecksketten in Tunis ‘mit, dass
dieselbe im Laufe des nächsten Jahres ausgefüllt werde; betreffs eines in Ferrero’s
Berichte ausgesprochenen Wunsches, Corsica mit Toscana einerseits und mit Frankreich
andererseits zu verbinden, fügt er hinzu, dass die letztere Verbindung in dem franzö-
sischen Arbeitsprogramm enthalten sei und er gegründete Aussicht habe, dieselbe in
nicht allzuferner Zeit ausführen zu können; zwischen Corsica und Nizza würde sich
mit Vortheil neben der telegraphischen Längendifferenzbestimmung eine solche mit Hilfe
von Lichtsignalen empfehlen.

Ferrero übernimmt nun wieder den Vorsitz und ertheilt Faye das Wort,
welcher den Wunsch ausspricht, dass bei diesen grossen geodätischen Verbindungen
auch gegenseitige Höhenwinkel zum Studium der terrestrischen Refraction ge-
messen wirden.

Perrier wird nun von Seiten des Präsidenten zur Berichterstattung über die
in den drei letzten Jahren ausgeführten Basismessungen und die hierzu verwendeten
Apparate eingeladen; der Bericht selbst ist als Annex IIIf. dem vorliegenden Bande
beigeschlossen.

Hirsch dankt Herrn Perrier für seinen sehr ausführlichen Bericht und unter-
stützt den Antrag des Berichterstatters, irgend eine Basis durch zwei verschiedene
Apparate zu messen, nämlich einen mit bimetallischen Messstangen und einen mit
Quecksilberthermometer und mit Messstangen aus einem Metalle versehenen Apparat;
nur wünscht er, dass beide Apparate Strichmaasse enthalten und bei dem ersteren
(bimetallischen) das Zink nicht zur Verwendung gelange. Als eines der wesentlichen
Resultate der in den letzten Jahren in der Schweiz mit dem Ibanez’schen Apparate
ausgeführten Messungen sieht Fürsch die gewonnene Erfahrung an, dass es möglich
und nützlich sei, diese Messungen so anzuordnen und einzurichten, dass sich der Aus-
dehnungscoefficient der Messstangen aus den Basismessungsoperationen selbst bestim-
men lasse.

Die nächste Sitzung wird auf Samstag den 20. Oktober 3 Uhr Nachmittags
anberaumt, da am Morgen dieses Tages die Meridiancommission eine Sitzung hält. Der
Präsident schliesst die Sitzung um 5" 15” Nachmittags.

 

 

 

 
