— 95 —

Espagne,

ee ee ne
| | | 1
| : DIRECTEURS | 4
LATITUDE. [LONGITUDE.| ÉPOQUE.
DES POINTS. |

|

   

ENDICATTON ||
INSTRUMENTS. | REMARQUES.
ET OBSERVATEURS. |

| |
I it rn “2 1 Hs u

 

 

Triangulation du Meridien de Salamanca.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

I 1 | Peüas.... | 43°39'16”| 11°49'20”| ‘1872 | Monet. Pistor n° 2. || Publiés dans les
: D} Gamonal .…. ... | 43 13 43 | 11 43 29 | 1872 id. id. lea
ME 5] Trigneiro. . . . .. 43 16 36 | 12 10.21 | . 1871 id. id. | État oo
Fran 4} Braña-Caballo... . | 43 0 13 | 12 1 45 | 1871 ie. id. | Madrid, 1875.
à 5 | Mampodre . .... 3 12 298 53 | 1871 id. id.
Frey Bes... } 42 41 3 | 1219 57|| 1868 Caramés. Repsold A.
| 2 vehlae.....:. | 42 85 20 | 11 54 53 | 1868 id. id.
| 8 | Matadeon. . . ... | 42 19 53 | 12 17 24 | 1868 id. id.
wi 9 | Casas-Viejas . . . . | 42 12 58 | 11 44 14 | 1868 id. id.
Le 10,8. Vicente... .. 2126| 12.1659) 1868 id. id. |
IE: 11 | Campanarie . 41 4858 | 11 38 19 | 1864 Sanchir. Repsold n° 2.
D 1 PEuentes... . .. Al Sf 435) 12.1151 | 1864 id. Pistor n >
4 13 | Teso-Santo. .... 41.14 18 1146 33 | 1564 id. Repsold n° 2, |
E 14 | Castillejo...... 41 210 59 2.11 12, | 1863 Ahumada, Sanchir. id. |
D 5 | Corral . . . . ... 40.49 0 | 12 3 29 |.” 1860 Corcuera, Barraquer. Ertel n° 1. |
© 16 | Diego Gômez ... | 40 55 37 | 11 36 8 | 1860 id. id. |
™ 17/| Francia.......| 40 30 44 | 11 30 14 | 1861 id. id. |
À 181} Calrvitero. : . . . . 20:17 31 | 11 55 55 | 1861 id. ic.
\ 19 | Santa Bärbara .. | 40 050 |.11 36 16 | 1863 Sanchir, Ahumada. Repsold n° 2.
= 20) Miravete...... 39 49 48 | 11 55.35.) ~ 1863 id. id. |
21 | Valdegamas . . 39 34 36 | 11 35 22 | 1863 id. id. |
3 22 |-Pedro Gômez . . . | 39 23 36 | 11 57 16 | 1863 id. id. |
2° 23.| Montanchez .. . . | 39 12 53 | 11 32.57) 1863 id. id.
E 27} Magacela. . .... done 44 | 1] 56, 4 | 1862 Ibarreta, Solano. Repsold (sans
4 marque).
i meeGliva. ost... 284557: 11.31.27: 1863 id. id.
3 26 | Santa Ines..... 92.50 2.310.) 1563 id. id.
iR 27 | Bienvenida. . ... 3316 26. 11 5041| 1863 id. id.
5 E28 | Hamapega..:.. 38 319| I 53 50% 1864-65 Monet. Ertel n° 2.
ë. er Tentudia... }..… ss 3»: 1120 3 1864 id. nels
me 0 | Cebron . ...... 3089 321 18.44 53 1864 id. id.
k Bert ........ 37 32 49 | 11 16 55 | 1864-72 | Monet, Hernandez. Ertel n° 2, Pister
LE: Ho.
iE 32 | Bujadillos..... ot AAg. 1749 ME) 1864 Monet. Eirteln2 2.
k Do} Regatero. . : . .. 3710 I | 11 2299| 1861-79 Monet, Hernandez. Ertel n° 2, Pistor |
3 ne.
R 34 | Gibalbin...... 29 5.1520 1863 Ruiz Moreno (D. J.) Ertel n° 2.
à 35 | Esparteros..... Eto 101 deh BA 1864 Monet. id.
E: OU ROME. DL... 00 29 20 | 12:15. 1. 1863 id. 1d- |
N Oi Aljibe. >. .... cy 60 2051) 1275 59. 1863 id. id. |
…._ 38 | S. Fernando. 0 2055... 11 2828 1863 Ruiz Moreno (D. J.) id. |
& Nee... me. 36 193,10: 11.19. 5 1863 Monet. id. |
È oe was 2 . 96 + 6: 845) 12° 7-40 1863 id. id.
ki
E Triangulation du Meridien de Madrid.
k Pie) Slatias .. ..... 43° 29'14"| 13°52'10"|| 1862-70 | Quiroga, Monet. Ert.n° 2, Pistorn®2, ||
4 2 PCerredo.: :.. .. 45:23:19 | 1423: 39. || 1862 id. Ertel n°2. |
F Pe yalers....... 43 8 45 | 13 59 26 | 1861-71 Tel. Ert.n°2,Pistor n°2. |
k Fe :........ 43 2) 28 0143 2 1861 id. Ertel n° 2. |
: 45 | Altotero .. 4240/3547 14 891 || 1861-62 id. id. |
E a AMAyA~ ..:.. . | 42 39 42 | 13 31 41 | 1861-70 id. Ert.n°2, Pistorn®? 2. |
5 #0) Onintanilla :... |.42 13 98 | 13 47 29 | 1862 10e Ertel n° 2. |
i He N° Millan... ... 42-13-54; 14 27 58 | ‘1861 id. id. |
4 ee valdosa....... 41 56 58 | 14 9 59 1861 id. id. |
E my Oreda.. .... .. rat 53 27) 13°36 4 1862 id. id. |
4 ee nupio.:........| 41 25 58 | 13 52 45 1861 id. id. |
= 22 | Moratilla, ..... ı 4 297 44 | 14.26 14 | 1861 id. id.
E I

kr Mensen rca
x 2

REITER ee
Re

 
