 

VI. Von der italienischen Gradmessungscommission Eingeladene:

Die Herren: d’Atri, Barattieri, Barilari, Battaglini, Blaserna, Capellieri, Caporali,
Cannizzaro, Cerutti, Cremona, Garbolino, Giacomelli, Giordano, Govi, Helbig,
Lazagna, di Legge, Malwolti, Pisati, Prosperi, Pucci, Rosalba, de Rossi, Q. Sella,
Tacchini, dalla Vedova.

Sitzungslocal:

Salone dei Conservatori in Campidoglio.

Programm fiir die im Jahre 1883 in Rom versammelte siebente
allgemeine Gradmessungs-Conferenz.

I. Jahresbericht der permanenten Commission und des Centralbureaus.

II. Berichte der Bevollmächtigten über die Fortschritte in den Arbeiten der von ihnen
vertretenen Länder.

III. Uebersicht über den gegenwärtigen Stand der Europäischen Gradmessung.

1. Astronomische Bestimmungen der Längen, Breiten und Azimuthe. Bericht-
erstatter: v. Dakhuyzen.

9. Triangulirungen. Berichterstatter: Ferrero.
3, Basismessungen und die hierzu verwendeten Apparate. Berichterstatter: Perrier.
4, Präcisionsnivellements. Berichterstatter: Horsch.
3 5, Flutmesser. Berichterstatter: Exc. Ibanez.
| 6. Bestimmung der Schwere mit Hülfe verschiedener Apparate. Berichterstatter:
v. Oppolzer. |
7. Neue Untersuchungen über die Refraction. Berichterstatter: ©. Bauernfeind.
“ 8. Die neuen Gradmessungspublicationen. Berichterstatter: Exc. Baeyer.
IV. Ersatz der vier dem Reglement gemäss austretenden Mitglieder der permanenten
Commission und Wahl eines siebenten Mitgliedes an Stelle des zurückgetretenen
Herrn General Baulina.
V. Berathung über die Wahl eines gemeinsamen ersten Meridians, zum Behufe der
Vereinheitlichung der geographischen Längen, sowie über die Einführung einer
pps allgemeinen Zeit neben den Localzeiten. Berichterstatter: Zürsch.

eu!

 

 
