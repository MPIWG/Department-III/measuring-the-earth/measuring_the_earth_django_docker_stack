 

26

Schreiber, 0. Die Anordnung der Winkelbeobachtungen im Göttinger Basisnetz. (Z.
i Wa, Bd. Ki; 1882, pi 129.)
Schur, W. Bestimmung der geographischen Breite der Strassburger Sternwarte nach
der Methode von Horrebow. (A.N., Bd. 105, Jahrg. 1883, No. 2479, p. 1)
Sundell, A. F. Aenderungen in der Brennweite eines achromatischen Objectivs durch
Temperaturvariation. (A. N., Jahrg. 1882, No. 2450.)
Vogler, Ch. A. Grundzüge der Ausgleichungsrechnung. Braunschweig, 1882.
Weingarten, J. Ueber die Verschiebbarkeit geodätischer Dreiecke in krummen Flächen.
(Sitzungsbericht d. Akad. d. Wiss. in Berlin, Bd. XLI, Jahrg. 1882, p. 453.)
— Ueber die Differentialgleichung der Oberflächen, welche durch ihre Krümmungs-
linien in unendlich kleine Quadrate getheilt werden Können. (Sitzungsb.
d. Akad. d. Wiss. in Berlin, Bd. XLIII, Jahrg. 1883, p. 1163—66.)
— Ueber die Eigenschaften des Linienelements der Flächen von constantem
Krümmungsmaass. (Crelle’s Journal für die reine und angewandte Mathe-
matik, Bd. 94, 1883, p. 181.)
— Zu der Abhandlung: Ueber die Eigenschaften des Linienelements etc. (Crelle’s
Journal, Bd. 95, 1883, p. 325.)

Werner, W. Die Winkelmessungen bei Tage und bei Nacht. (Z. f. Instrumentenkunde,
| Bd. IH, Jahrg. 1883, p. 225.) Hierzu Bemerkungen von H. Bruns.
| (Ebenda, p. 308.)
| —  Ueber die Methode der „Coast and Geodetic Survey“ zur Auflösung von

Normalgleichungen. (Civilingenieur, Bd. XXIX, Heft 2, 1883.)
| _ Die Wasserstandsbeobachtungen in Norwegen. (Civilingenieur, Bd. XXIX,
| Heft 2, 1883.)
| Westphal, A. Der Basisapparat des General Ibanez und sein Verhältniss zum älteren
| Spanischen Apparat. (Zeitschr. f. Instrk., Bd. I, 1881, p. 173.)
Wittstein, Th. Ein Zusatz zur Methode der kleinsten Quadrate. (A. N., Bd. 102, 1882,
No. 2446, p. 339.)

(8. Russland.

Kuhlberg, P. Resultate aus Pendelbeobachtungen im Kaukasus. (A. N., Bd. 99, 1881,
NO: 2810, 4h. 202)
a Untersuchungen iiber den Einfluss des Mitschwingens des zum Russischen
akademischen Reversions-Pendel- Apparate gehörigen Stativs auf die Länge
des Secundenpendels. (A. N., Bd. 101, 1882, No. 2416, p. 243.)
Memoiren der militär-topographischen Abtheilung des Generalstabes.
Band XXXVII. Herausgegeben vom Generallieutenant v. Forsch. St. Peters-
burg, 1880.
I. Astronomisch-geodätische Arbeiten im Europäischen Russland.

 

tt site —

|
|
4
|
|

mm

 

 
