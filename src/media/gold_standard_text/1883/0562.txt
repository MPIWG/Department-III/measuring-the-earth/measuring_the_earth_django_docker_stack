 

54

Dans ces conditions, d’aprés l’opinion du rapporteur, les influences perturbatrices
de l'air, de même que la proximité inévitable de l'observateur, causeront des
inconvénients sérieux; mais, néanmoins, cette proposition mérite une attention réelle, et
elle pourrait devenir utile si l’on trouvait une disposition convénable.

Quant à la précision avec laquelle on peut déterminer la pesanteur avec le
pendule à réversion de ÆRepsold, il faut d’abord considérer l'incertitude de la mesure de
longueur sur le résultat. Celle ci passe en entier dans le résultat qu'il faut déterminer.
La mesure de longueur elle même donne lieu à une double cause d'erreur, savoir: l'erreur
de la mesure et celle de la réduction provenant de la température; la seconde cause
d'erreur ne peut être réduite à son minimum que lorsqu'on observe dans des localités
d'une température presque constante. Pour des déterminations absolues de la pesanteur
qu’on ne fera qu’en peu d’endroits et dans des localités pourvues de toutes les ressources,
il sera toujours possible de satisfaire à cette condition, de sorte qu’on peut considérer
comme faible l'erreur provenant de l'incertitude de la température.

On peut estimer qu’en prenant la moyenne de plusieurs observations, cette inex-
actitude ne dépassera pas un micron. La comparaison, au moyen du comparateur, de la
distance des couteaux avec l’échelle dont la correction par rapport à l’étalon normale doit étre
supposée connue, présente des difficultés à cause des phénomènes d'irradiation susmen-
tionnés, mais l'erreur qui en résulte atteindra à peine trois microns, en prenant la moyenne
des mesures répétées sur les deux couteaux, de sorte qu'on peut présumer que l'erreur
dans la détermination de la pesanteur, en tant qu’elle dépend de la mesure de longueur,
atteindra à peu pres la 300000° partie de la quantité totale.

En tenant compte des circonstances données, il faut considerer la précision ainsi
obtenue comme suffisante, et on peut à peine l’atteindre pour l’autre élément qui entre
dans le calcul, savoir la durée de l’oscillation. Les durées d’oscillation se présentent
dans les méthodes proposées ici de quatre manières, savoir :

1) Pendule lourd, poids lourd en bas — Tu’

2) ” ” ” ” „ haut = To’
3) n Loc .; DS 0%
4) ” ” ” ” „ haut = To!

Ces durées d’oscillation entrent d’une manière très-différente dans le résultat 7,
durée d’oscillation du pendule-métre, selon la construction de lappareil et le poids du
pendule. Pour Vappareil de la mesure du degré autrichienne par exemple, cette influence
sera exprimée par la relation suivante:

arena — ogi, — begin + 0.6 aio.

Tu a done la plus grande influence sur la détermination de la durée d’oscillation.
On disposera les observations de manière à tenir compte de ces circonstances. La précision
avec laquelle on peut déterminer la durée d’oscillation sera à peu près proportionelle à
la durée de l'observation; on tiendra done compte des circonstances particulières du pendule

 

 
