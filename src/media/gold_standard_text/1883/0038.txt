 

 

 

 

 

 

 

 

16.

17

18.

19.

20.

21.

22.

23.

24.

25.

26.

27.

28.

24

Verhandlungen des Wissenschaftlichen Beirathes des Königlichen Geodätischen
Instituts zu Berlin im Jahre 1881. Berlin 1881. (Als Manuscript gedruckt.)
Verhandlungen des Wissenschaftlichen Beirathes des Königlichen Geodätischen
Instituts zu Berlin im Jahre 1882. Berlin 1882. (Als Manuscript gedruckt.)
Protokoll der am 24., 25. und 26. April 1862 in Berlin abgehaltenen vor-
läufigen Berathungen über das Project einer Mitteleuropäischen Gradmessung.
Berlin 1882. Verlag von P. Stankiewicz’ Buchdruckerei.

Entwurf für die astronomischen Arbeiten der Europäischen Längengrad-
messung unter 52° Breite vom Jahre 18653. Berlin 1882. Verlag von
P. Stankiewicz’ Buchdruckerei.
Protokoll der Sitzungen der Permanenten Commission der Mitteleuropäischen
Gradmessung in Leipzig vom 3. und 4. September 1865. Berlin 1882.
Verlag von P. Stankiewicz’ Buchdruckerei.

Protokoll der Sitzungen der Permanenten Commission der Mitteleuropäischen
Gradmessung in Neuenburg vom 6. bis 10. April 1866. Berlin 1882. Verlag
von P. Stankiewicz’ Buchdruckerei.

Protokoll der Sitzungen der Permanenten Commission der Europäischen
Gradmessung in Wien vom 25. bis 30. April 1867. Berlin 1882. Verlag von
P. Stankiewicz’ Buchdruckerei.

Verhandlungen des Wissenschaftlichen Beirathes des Geodätischen Instituts
zu Berlin im Jahre 1883. Berlin 1883. (Als Manuscript gedruckt.)

Register der Protokolle, Verhandlungen und Generalberichte fir die Europäische
Gradmessung vom Jahre 1861 bis zum Jahre 1880. Vom Geheimen Regie-
rungsrath Professor Dr. Sadebeck. Berlin 1883. Verlag von P. Stankiewicz’
Buchdruckerei.

Astronomisch-geodätische Arbeiten in den Jahren 1881 und 1882. Von Pro-
fessor Dr. Albrecht. Berlin 1883. Verlag von P. Stankiewicz’ Buchdruckerei.
Verhandlungen der vom 11. bis zum 15. September 1882 im Haag vereinigten
Permanenten Commission der Europäischen Gradmessung, redigirt von den
Schriftführern A. Hirsch, Th. v. Oppolzer. Zugleich mit dem Generalbericht
für die Jahre 1881 und 1882, herausgegeben vom Centralbureau der Euro-
päischen Gradmessung. Berlin 1883. Verlag von Georg Reimer.
Gradmessungs-Nivellement zwischen Swinemünde und Amsterdam. Von Dr.
Seibt. Berlin 1883. Verlag von P. Stankiewicz’ Buchdruckerei.

Absolute Höhen des Gradmessungs-Nivellements zwischen Berlin und Denekamp.
Auszug aus dem Vorhergehenden. Von Dr. Seibt. Berlin 1883. Verlag von
P. Stankiewicz’ Buchdruckerei.

Der Präsident
des Königlichen Geodätischen Instituts.
Baeyer.

 
