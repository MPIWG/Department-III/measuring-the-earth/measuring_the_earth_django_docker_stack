 

Vorwort.

Es lag anfänglich in der Absicht des Centralbureaus, schon der in Rom abgehalte-
nen siebenten Allgemeinen Conferenz der Europäischen Gradmessung einen Nach-
trag zu der Literatur der praktischen und theoretischen Gradmessungsarbeiten
vorzulegen. Mit der Zusammenstellung dieser Arbeit war der Geh. Regierungs-
rath Sadebeck beauftragt worden. Durch die Versetzung desselben in den
nachgesuchten Ruhestand kam die Arbeit jedoch nicht zu Stande, auch fand
sich später in dessen dienstlichen Papieren kein Material dazu vor, so dass
die ursprüngliche Absicht aufgegeben werden musste, und die Publication erst
jetzt als Anhang zu dem Generalberichte für das Jahr 1883 erfolgen kann.
Leider darf aber diese zweite Mittheilung wohl ebenso wenig auf Vollständig-
keit Anspruch machen, wie die erste, was bei derartigen Zusammenstellungen
von wissenschaftlichen Arbeiten aus allen Culturstaaten und bei den fort-
während hinzukommenden neuen Erscheinungen auf dem Gebiete der Geodäsie
seinen natürlichen Grund hat. Was nun speciell den gegenwärtigen Nach-
trag zu der Literatur der Gradmessungsarbeiten anbetrifft, so war man bei
der Kürze der Zeit, ausser den von den Herren Commissaren erbetenen, aber
nur ganz vereinzelt eingegangenen Mittheilungen, auf die an das Central-
bureau eingeschickten officiellen und privaten Publicationen, auf die ver-
schiedenen fachwissenschaftlichen Zeitschriften, auf buchhändlerische Kataloge
und auf das, was man durch Zufall fand, angewiesen. Die nächste Mit-
theilung wird ausser den nach 1883 erschienenen fachwissenschaftlichen
Publicationen auch noch die beiden ersten Mittheilungen als Ganzes neuge-
ordnet enthalten. Um möglichste Vollständigkeit zu erzielen, würde der
Unterzeichnete jede Mittheilung über neue oder in den Verzeichnissen noch
1*

 
