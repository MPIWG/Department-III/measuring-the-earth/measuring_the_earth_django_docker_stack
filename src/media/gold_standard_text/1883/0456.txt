een

N°

 

 

 

INDICATION
DES POINTS.

Puokio-vaara .
Teiri-harin . ..
Halosen-vaara.
Repo-Kangas .

Revonpesämaa. . .

Palo-vaara....

Linnunsilma....
Pitkäselkä

Laton-Mäki .

Sarvl-Kangas . ..
Hyypän-Mäki . ..

Isoniemi

Kaakama-waari. .

Solotojevo

Nowoselki
Skepowo
Tretiaki
Mohllany
Krynki

Kuchtschinctz. .

Sokalka
Chadowa
Suhowol

Dlugilung . ....

Wissowaty

Krapiwnitza. . ..

Kripnitza
Dobrzmewka. .

Mohnata-Gura. .

Suhowoltze
Gornowo...
Koritziny
Semiatizi
Kaliski

Drguitsch
Tschaple

Pustelnik
Kobelka
Milosna.
Varsovie

Zegrz
Zakrotschym. .

 

LATITUDE.

64° 44' 51"

64 40 40
64 43 31
64 48 31
64 49 38
64 49 37
64 51 11
64 55 36
64 49 36
65 0 25
65 3 48
65 9 43
65 20 41
65 23 19
65 40 1

| 65 49 21

65 49 45
66 8 25

Triangulation de Pologne. Jonction à Thorn.

 

00

Triangulation de Finlande.

LONGITUDE.

|

45° 09521

45 37 56 |
43 33 33 |
43 49 35 |
44 90 12 |

44 34 59

43 21 39 |
43 35 49 |
42 49 30

43 17 18 |
49 27 58 |
42 54 9 |

42 54 41

42 30 16 |
42 13 8 |
42 40 21 |

41 49 21

41 51 48 |

nl

} Déjà mentionnés.

53.2918
53 14 97
53 24 54
53.19.
53 34 55
53 26 20
53 35 38
53 34 38
5a 25.21
53'270
53 17 52
Dov DTD
53.13 5D
53 3 3
92.9113
52 50 3
D2 43 37
52 36 44
52 32 53
52 99 1
52 24 50
92: 19.17
52 32 60
52 20 48
92 32 23
SE 21

23 11

28 28

26 23

16 17

2 20 28
13 35

2 14 41
28 36

26 33

19 11

 

 

41 50 54
41 44 15
41 33 28

AL ZT 18

41 17 35 |

ASS
40 58
40 47
40 50
40 36
40 22
40 22
40 42
Al |
40 40

530 16' 19") 42° 955")

|
I

41 9 45:
40 41 56 |
Al 0 14
10 40 3

oO
40 21 30 |
40 98 57 |
40 8 58
40 13 14 |
39 51 88 |
39 97 95 |
89 33 30 |

39 32 46

39 16 35 |
59 216 |
99 611 |
38 51 46 |

0805 29
38 40 40
38 42 9
38 17 13
38 15 52

 

 

ÉPOQUE.

ET OBSERVATEURS.

 

 

DIRECTEURS

stedt (après 1835).

;

Rosénius, Oberg et Mélan
Wolc

(Officiers d’Etat-Major).

Mr.

Mrs.

1 de Tenner.

énéra

,

Lieut.-G

 

 

 

INSTRUMENTS. |
|
|

3 p.

Univ. de Reichenbach de 1

3 Instruments Universels d’Ertel.

Instr.

|
|
|
|

eSB PE LE ELLE DDR TT PANNES PANNE PE DR ER EE

|

REMARQUES.

| Publies dans le
| Volume XXIII des
|| publications de la
|| section topographi-

2 Théodolites de 12 p.

 

 

|| que de l'Etat-Major.

fl

PRET.

33

Le ce 27 82 0 où ne 20 19

 
