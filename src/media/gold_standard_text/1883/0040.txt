 

 

 

 

 

 

   

 

26

unserer Zeit strebt im Gegentheil dahin, den Cultus der abstracten
Anwendungen zu verbinden.

haftlichen Arbeit offenbar die Er-
ben, so erwächst

einander; das Ideal
Wahrheit mehr und mehr mit deren nutzbringenden

Obwohl wir als Hauptzweck unserer gemeinsc
forschung der Gestalt und der Dimensionen der Erde anzusehen ha
doch unserm grossen Unternehmen eher Vortheil als Schaden aus der Thatsache, dass
unsere Arbeiten zu gleicher Zeit auch dazu dienen, den grossen Civil- und Militär-
verwaltungen die Grundlagen zu ihren topographischen Aufnahmen, sowie den Ingenieuren
die Ausgangspunkte für ihre Nivellirungsarbeiten zu liefern.

Warum sollte unser Verein also nicht auch hier mithelfen, wo es sich um einen
wichtigen Fortschritt, für die Schifffahrt, für die Cartographie und Geographie im
Allgemeinen, sowie für den Betrieb der grossen modernen Verkehrsmittel, der Eisen-
bahnen und Telegraphen handeit?

Wenn demnach sowohl unsere Berechtigung als die Angemessenheit, sich mit
dem Gegenstande zu beschäftigen, ausser Frage steht, so hielt es die permanente
Commission behufs gründlicher Erörterung aller einschlagenden Gesichtspunkte doch
für angemessen, die Direktoren der grossen astronomischen und nautischen Jahrbücher,
welchen für diese Gegenstände offenbar eine unbestrittene Competenz zukommt, zur
Theilnahme an den Berathungen hinzuzuziehen. Andererseits schien es uns unmöglich,
eine allgemeine Einigung über die Annahme eines einzigen Ausgangs-Meridianes ohne
die Theilnahme Englands zu erzielen, welches in seiner Eigenschaft als erste See-
Macht der Erde und in Anbetracht seiner über alle Welttheile sich erstreckenden
Besitzungen das meiste Interesse an der Verwirklichung dieses Fortschrittes haben muss.

Da Grossbritanien bis jetzt bei der Europäischen Gradmessung sich nicht
betheiligt hat, so waren wir in der Lage bei der englischen Regierung anzufragen, ob
dieselbe geneigt sein würde, sich bei unserer diesjährigen Conferenz vertreten zu lassen,
und an den Verhandlungen über eine für sie speciell so wichtige Frage Theil zu nehmen.
7u unserer grossen Genugthuung fanden die betreffenden Schritte bei der englischen
Regierung das freundlichste Entgegenkommen, so dass wir heute mit Freuden England
durch Männer von hervorragender Autorität vertreten sehen, und ausserdem bedeutende
Gelehrte in unserer Mitte begrüssen, welche unserer besonderen Einladung gefolgt sind,
um an diesen Berathungen Theil zu nehmen.

Bevor wir nun auf den Gegenstand selbst übergehen, sei es uns gestattet, die
Stellung dieser Versammlung und die Tragweite ihrer Beschlüsse etwas näher ins Auge
zu fassen. Wir sind von unseren Regierungen weder beauftragt, endgültige Beuahinese
zu fassen, noch sind wir mit den nöthigen Vollmachten ausgestattet, um eine inter-
nationale Convention abzuschliessen, welche — mit Vorbehalt der Ratification durch
die Regierungen — für die von uns vertretenen Staaten bindend sein würde. Ebenso
eee eh bilden wir einen jener freien, aus Gelehrten und Freunden der Wissenschaft

eliebig zusammengetretenen Congresse, welche aus eig |
jedes officielle Mandat sich nee ae re ee = —
allgemein nützlichem Interesse zu discutiren und die sich darauf beschrä Sr . en
é nken miissen,

  
