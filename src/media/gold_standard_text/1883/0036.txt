 

 

 

 

22

Das Mittelwasser der Ostsee liegt
02056
unter Normal-Null.
Der Nullpunkt des Amsterdamer Pegels aber liegt
02186
über Normal-Null.
Folglich ist Normal-Null durchaus nicht identisch mit dem Nullpunkt des
Amsterdamer Pegels.

ÆE. Maassvergleichungen.

In Veranlassung des Schreibens des Herrn Professor Dr. Förster vom 8. September
1880 hat die permanente Commission in der Sitzung vom 17. September 1880 in München
ihren Vorschlag:
dass künftig nicht nur die in dem Schreiben des Herrn Förster berührte
Frage, sondern überhaupt alle die Gradmessung interessirenden Maassver-
gleichungen dem internationalen Comité für Maass und Gewicht zur Unter-
suchung überwiesen werden

zum Beschluss erhoben.

Infolge dessen sind die Maassvergleichungen des Centralbureaus an das genannte
Comité übergegangen.

Ich musste dem Beschluss der permanenten Commission zustimmen, weil das
Centralbureau noch immer kein geeignetes Laboratorium besitzt, und weil die Herren
Förster und Schreiber die von mir 1846 entdeckte und von einer Belgischen Commission
1854 geprüfte und als richtig befundene Veränderung der Ausdehnungs-Coefficienten in
einer Art und Weise bestritten, die eine unparteiische Untersuchung wünschenswerth,
ja nothwendig machte. Diese unparteiische Untersuchung hat bereits stattgefunden.
Die Schweizer Gradmessungs-Commission hat zunächst bei Messung ihrer Grundlinien
mit dem Spanischen Basisapparat eine entschiedene Aenderung der Ausdehnungs-
Coefficienten an dem 4 Meter langen Eisenstabe constatirt, und dann hat General
Ibanez den vor etwa 20 Jahren bestimmten Ausdehnungs-Coefficienten des Stabes im
vorigen Jahre von Neuem bestimmt und eine erhebliche Aenderung bestätigt gefunden.
(Proces-verbal de la Commission geodesique Suisse. Neuchätel 1883. Seite 24.)

Die Herren Zörster und Schreiber hätten vorher bei Etalonnirung der Bessel'schen
Messstangen die Bestimmung der Ausdehnungs-Coefficienten nicht unterlassen sollen.

F. Publicationen.

Seit der 6. Allgemeinen Conferenz im Jahre 1880 sind folgende Drucksachen
erschienen:

&

 

 

 
