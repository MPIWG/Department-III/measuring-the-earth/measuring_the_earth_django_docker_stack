 

12

 

Fehler durch geeignete Apparate zu) bestimmen; bei der Vollkommenheit der ee
form, welche Repsold an den von ihm gelieferten Stiicken erzielt, sind Poe die ne
so gering, dass sie sich der Beobachtung meist entziehen, 1m höchsten Falle geringe
Bruchtheile eines Mikrons erreichen, also völlig vernachlässigt werden können.

Von der Vollkommenheit der Schneiden überzeugt man sich am einfachsten,
wenn man dieselben leicht gegen eine Planfläche drückt und den durchfallenden Licht-
streifen beobachtet; sehr kleine Fehler lassen sofort diesen Streifen sehr ungleich breit
erscheinen. .

Dass selbst vollkommen gearbeitete Stahlschneiden im Verlaufe der Zeit
Aenderungen ihrer Form erleiden können, davon hat der Berichterstatter sich zu über-
zeugen Gelegenheit gehabt und Repsold’s Erfahrungen bestätigen diese Wahrnehmung. Es
émpfiehlt sich deshalb die Anwendung von Achatschneiden, welche wohl kaum dergleichen
Verziehungen unterworfen und überdies vor der Gefahr des Rostens sicher sind. Anderer-
seits haben sich nach den Erfahrungen, die in Indien gesammelt wurden, auch die Stahl-
schneiden der invariablen Pendel frei von solchen Aenderungen gezeigt, so dass der
Schluss erlaubt scheint: es sei nicht jede Stahlsorte derartigen Einflüssen unterworfen
oder diese wirkten nur durch kurze Zeit nach der Anfertigung, worauf dann ein bleibender
Ruhezustand folge.

Die Messung des Schneidenabstandes bietet insofern eine Schwierigkeit, als es
bei der durch Repsold getroffenen Einrichtung des Bessel’schen Pendels auf die Ver-
gleichung der Schneidenkante mit einem Strichmaasse ankommt. Die Schneide kann
licht oder dunkel gemacht werden und man hat gehofft, das Mittel der Einstellungen auf
die lichte und dunkle Schneide werde frei sein von der Irradiation. Von diesem Aus-
kunftsmittel dürfte aber nur dann ein Erfolg erwartet werden, wenn in der That die
Schneide eine völlig scharfe Kante darstellte, so dass die sichtbare Abgrenzung der dunklen
Schneide der Lage nach mit jener der lichten identisch wäre; bei dem thatsächlichen
Verhältnisse jedoch wird im Allgemeinen die Abgrenzung der lichten Schneide in etwas
von der Kante gegen den Körper der Schneide hin verschoben erscheinen, da man als
solche die Linie des höchsten Reflexes der vertical einfallenden Beleuchtungsstrahlen
wahrnehmen wird.

Bei Stahlschneiden, die offenbar der idealen Kantenform näher gebracht werden
können, war mir dieser Unterschied nicht sehr aufgefallen, bei Achatschneiden jedoch
zeigte sich die lichte Schneide um nahezu 20 Mikron fehlerhaft pointirt. Man konnte
sich leicht von dem Vorhandensein dieser Fehlerquelle überzeugen, wenn man die Schneide
gleichzeitig von oben und hinten beleuchtete. Das ganze Sehfeld war dann licht, nur in
der Mitte von einem etwa 20 Mikron breiten, dunklen Streifen durchsetzt, welcher dem
Abstand der dunklen Kante von der Stelle des stärksten Reflexes entsprach. Der
Berichterstatter hält es darum für vortheilhaft, auf die Pointirung der lichten Schneiden
ganz zu verzichten, nur die dunkle Schneidenkante einzustellen und den Einfluss der
Trradiation durch geeignete Mittel unschädlich zu machen. Als ein solches erscheint es
ihm z. B., wenn man den einen Faden des Mikrometers mit der Kante fast zur Berührung

 

een EEE Er EEE

 
