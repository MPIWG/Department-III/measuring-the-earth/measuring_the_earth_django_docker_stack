 

era din
enr st
alt
parie

ef que ti

sorte IE

ce
ont
nn?
os is
Dass im
yo!

gut,

b
5 W

Dé oe

ASEAN RRC MEETS

15

En Prusse on renonce à la compensation polygonale pour les nivellements des
grandes lignes principales pour lesquels on prend la moyenne entre les doubles opérations.
On ne fait la clôture des polygones que pour des réseaux secondaires formés, si nous
avons bien Compris, par des cheminements simples.

En Russie on n’a publié jusqu'à présent que des résultats provisoires.

En Suisse on s'occupe actuellement de la compensation du réseau: les travaux
préparatoires ont fourni la preuve que pour un terrain tant soit peu accidenté, l'élément
des différences de hauteur parCourues ne peut pas être négligé et que pour tous les pays
montagneux et surtout pour les passages des hautes Alpes, il l'emporte sensiblement sur
la longueur des lignes nivelées, pour l'influence sur l'exactitude des résultats. L'importance
relative qu’il convient de donner à ces deux éléments dans la détermination des poids,
dépend dans chaque cas de circonstances multiples, surtout de l'incertitude et de la
variabilité des mires employées.

Les recherches qui sont en cours en Suisse, ont en outre conduit à la conclusion,
que les différences entre les résultats des deux opérations ne peuvent pas être prises
sans autre comme mesure de l’exactitude des opérations, attendu que le tassement inter-
vient comme source d'erreur constante, qui s’élimine plus ou moins dans la moyenne
de deux opérations conduites dans le sens inverse, mais qui peut fortement augmenter
la différence entre les deux opérations. IL faut donc distinguer à cet égard entre les
doubles nivellements faits dans le même sens ou dans le sens opposé.

Nous regrettons beaucoup de n’avoir pas reçu des réponses catégoriques à la
dernière question du No. 7? de notre questionnaire, demandant si l’on publie les résultats
directs des opérations ou seulement les résultats compensés. Il nous semble de la plus
orande importance d’insister de nouveau sur la nécessité de publier partout d’abord les ré-
sultats des nivellements tels quels, c. à. d. réduits pour l’inclinaison et les erreurs instru-
mentales, la où il y a lieu, et enfin en tenant compte de l’équation des mires. Mais dans
l'incertitude qui règne encore sur les principes à suivre et sur les méthodes à em-
ployer pour la compensation hypsométrique, il serait très regrettable, si l’on ne livrait
(comme on l’a fait en France, où l’on à compensé les polygones en donnant des poids
proportionels au nombre des répétitions) à la publication que des chiffres compensés d’une
manière quelconque et si l’on empêchait ainsi de recourir aux nombres déduits directement
des observations mêmes, pour établir plus tard l’ensemble du réseau hypsométrique de
l’Europe et des grandes lignes reliant les mers.

Avec les reserves qui résultent de ce que nous avons dit plus haut sur la signi-
fication de Verreur qui peut être conclue de V’accord des deux opérations de la même
ligne, nous donnons, comme la dernière fois, le tableau suivant des erreurs probables
kilométriques qui ont été indiquées par les différents pays:

1. Bavière. L'erreur déduite des 7 premiers polygones, à l'exclusion de
celui du Fichtelgebirge est de + 1™™6.

 
