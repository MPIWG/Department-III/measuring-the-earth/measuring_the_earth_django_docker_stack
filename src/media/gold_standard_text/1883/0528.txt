 

 

20
eine Genauigkeitsgrenze von einem Hunderttausendtel erreichen lassen; dennoch bleibt
die Genauigkeit der Bestimmung der Schwingungszeit hinter jener der Längenmessung
weit zurück, wenn man nicht ungebührliche Anforderungen an den Beobachter stellen
wollte. Um die Genauigkeit der Längenmessung bei der Zeitmessung zu erzielen, wären
nämlich mindestens hundert Bestimmungen der 4 Schwingungszeiten nothwendig, eine
Forderung, deren Erfüllung kaum ein Aequivalent für die darauf gewandte Arbeit bietet.

Man wird aus dem Gesagten die Folgerung ziehen können, dass bei der absoluten
Bestimmung der Schwere die Genauigkeit ohne übermässige Arbeitsleistung bis auf etwa
den 100000. Theil der Gesammtgrösse erreicht werden kann, was ungefähr dem 100.
Theile eines Millimeters beim Secundenpendel entspricht. Da aber im Allgemeinen bei
den Schätzungen die Fehlergrössen eher zu gross als zu klein angenommen werden, so
dürfte diese Genauigkeitsgrenze im Durchschnitte stets zu erreichen sein.

Es obliegt dem Verfasser dieses Berichtes, ehe er die Erörterung der absoluten
Schwerebestimmung beendet, auf jene Methoden, welche hierfür in neuerer Zeit vorge-
schlagen wurden, hinzuweisen, weil dieselben nicht mit Sicherheit als allgemein bekannt
vorausgesetzt werden dürfen; ältere Vorschläge, deren Kenntniss mit Ausnahme etwa von
Prony’s ,Lecons de mécanique analytique données a l’école polytechnique, Paris 1815,
Vol. II, pag. 338 ff.“ wohl schon verbreitet ist, bleiben hierbei natürlich unbeachtet.

Govi (vergl. pag 3) hat in den Schriften der Turiner Akademie 1866 einen Vor-
schlag zur absoluten Bestimmung der Schwere gemacht, auf welchen die Aufmerksamkeit
des Berichterstatters zu lenken Prof. Schiavoni die besondere Güte hatte. Dieser Vor-
schlag geht im wesentlichen dahin, die vier Schwingungszeiten eines Pendelstabes zu be-
stimmen, der auf einer Schneide hängt und auf dem ein Laufergewicht in vier ver-
schiedenen Positionen von genau messbarer relativer Lage festgestellt werden kann.
Aus den so erhaltenen vier Schwingungszeiten und den drei durch die vier Stellungen
des Laufergewichtes gegebenen Abständen wird sich nach Govi, wenn man von dem Ein-
flusse der Luft absieht, die Länge des Secundenpendels berechnen lassen. Als Vortheil
dieses Apparates könnte hervorgehoben werden, dass dabei die Schneidenform nicht in
Betracht kommt und überdies in Folge der Benutzung nur Einer Schneide, die allerdings
wenig zu befürchtenden Fehler, welche beim Reversionspendel aus dem mangelnden
Parallelismus der beiden Schneiden entstehen, vermieden werden; andererseits aber
zeigen sich auch erhebliche Nachtheile darin, dass zum Zwecke der Elimination des Luft-
einflusses die Beobachtungen im Vacuum gemacht werden müssen, und dass der entwickelte
Ausdruck für die Länge des Secundenpendels, der in Bruchform auftritt, sich im Allge-
meinen dem Ausdrucke 0:0 nähern wird, sonach die unvermeidlichen Beobachtungsfehler
bedeutend vergrössert in das Resultat übergehen werden. Der Berichterstatter glaubt
daher, dass dieser Apparat sich zur genauen absoluten Schwerebestimmung nicht empfehle.

Finger’s Commutationspendel (vergl. pag. 7) beruht auf der Grundidee: durch
Commutation der Massen — daher der Name dieses Apparates — die Reversion des
Pendels zu ersetzen und dabei mit der Benutzung Einer Schneide auszureichen. Finger

nn

u te

|
|
|

 

 

|
|
|
+
|
|
|
|

 
