 

LE

TROMPETTE ENT EIRE ee

&

et

ON

I
Ë
Ë
F
q
F
E

CERN TL TT IT SS TE TT eee

187

M. Loewy croit que l'innovation projetée n’est pas satisfaisante. D'un côté, le
calcul des données théoriques pour le même méridien dans les diverses publications
faciliteront bien la comparaison des résultats; mais, d'un autre côté, on introduit dans
chacune de ces publications une sorte de dualisme préjudiciable aux intérêts scienti-
fiques. En utilisant ces documents, il faudrait à l’avenir, dans beaucoup de recherches,
effectuer un travail spécial de raccordement.

M. Loewy est conservateur, en sa qualité de directeur d’une éphéméride. Il trouve
que si l’on veut rompre avec des traditions plusieurs fois séculaires, il faut que les
réformes proposées aient une valeur incontestée et reposent sur des raisons de premier
ordre; il faut que les perturbations jetées dans les usages conduisent au moins dans
l'avenir à une économie sérieuse de travail; et finalement il faudrait, pour l'adoption
d’une mesure aussi radicale, qu’en pesant le pour et le contre, la balance penchät en
sa faveur d’une façon éclatante. Or, dans le cas actuel, en se plaçant au point de vue
astronomique, M. Loewy pense que ces conditions essentielles ne se trouvent pas réalisées
d'une manière suffisante, et il préfère donc le statu quo. —

Monsieur Rümker, sans partager en rien le point de vue purement négatif de
M. Loewy, croit cependant qu'il y à certaines parties des almanachs nautiques, qu'il fau-
drait continuer à exprimer en temps local, comme par exemple la plupart des données
qui se rapportent aux calculs des marées.

M. Foerster repond à M. Loewy, en appuyant ses indications par un exemplaire
de la „Connaissance des temps“ pour 1884 qu’il a sous la main; il démontre que pour
toutes les données de cette éphéméride, qui ne sont pas destinées a faciliter l’obser-
vation immédiate de certaines phases locales des mouvements et phénomènes célestes,
c’est-à-dire, pour les coordonnées écliptiques du soleil, de la lune, et des planètes, pour
les coordonnées linéaires du soleil et pour toutes les coordonnés et distances lunaires,
qui sont calculées d'heure en heure ou de 3 heures en 3.heures, l’unification des époques
sera d’une grande utilité, soit pour la rédaction des éphémérides, soit pour les astro-
nomes qui en font usage. Pour le calcul consciencieux de toutes ces parties des éphé-
mérides, qui comporte aussi des comparaisons assez étendues avec les résultats des
autres grandes éphémérides, M. Foerster évalue le travail annuel, qu’on économisera par
l'unification proposée, à quelques semaines d’occupation de plusieurs calculateurs exerces.
Et l’époque de transition une fois passée, il n’existera aucun désavantage qu'on puisse
citer vis à vis de cette simplification considérable et permanente.

Le dualisme et les discontinuités que M. Loewy redoute comme conséquence
de l'unification des méridiens, n’existeront pas. En effet, au lieu de la distinction prin-
cipale, qui existe à présent dans la ,Connaissance des temps”, entre les données calculées
soit pour les époques des culminations à Paris, soit pour le midi et minuit de Paris,
il y aura simplement la distinction entre les données pour les culminations à Paris
et les données calculées pour les époques du temps universel. Quant à la discontinuité
causée, d'après M. Loewy, par l'introduction de l'heure universelle dans les éphémérides,
c'est une très petite affaire, comparée avec les changements non seulement de forme,

24*

 
