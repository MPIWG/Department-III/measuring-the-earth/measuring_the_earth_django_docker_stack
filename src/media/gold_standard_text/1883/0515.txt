 

sel

werner

TE TEE TRETEN TERN

Nee

2

Arge en er

eo ipuen

T

Apparat sei der geeignetste zur Elimination des Lufteinflusses und verdiene den Vorzug vor
anderweitig construirten Pendeln, die im luftleeren Raum beobachtet werden müssen, oder
sei mindestens gleichwerthig mit solchen. Derselbe Vorrang wird dem Commutations-
pendel zugeschrieben werden müssen, welches Finger (Ueber ein Analogon des Kater’schen
Pendels, Sitzungsberichte der Kaiserl. Akademie der Wissenschaften in Wien, Bd. LXXXIV,
II. Abth. Juniheft 1881) in Vorschlag gebracht hat.

Weniger befriedigend erscheinen die von Bessel in Anwendung gezogenen Me-
thoden *), um den Einfluss der Luft auf seine Fadenpendelbeobachtungen zu bestimmen,
wenngleich sich vom theoretischen Standpunkte aus nicht allzugewichtige Bedenken da-
gegen erheben lassen.

Die sich hier entgegenstellenden Schwierigkeiten waren bedeutend und dürften
Bessel wohl die erste Anregung gegeben haben, am Schlusse seiner berühmten Abhand-
lung über die Länge des einfachen Secundenpendels die Vortheile des von ihm modificir-
ten, aber nicht von ihm selbst erprobten Reversionspendels auseinanderzusetzen. Es ist
sehr zu bedauern, dass Bessel selbst nicht mehr in der Lage war, den von ihm ange-
gebenen Apparat, der einen epochemachenden Fortschritt in den Pendelbeobachtungen
darstellt, in Anwendung zu ziehen und die Beobachtungsmethoden für denselben fest-
zustellen, denn manche Vorwürfe und abfällige Urtheile über diesen Apparat wären dann
unterblieben, weil sie in der That weniger diesem als dem Experimentator zur Last fallen.

Es wird sich im Laufe des vorliegenden Berichtes noch mehrfach Gelegenheit
ergeben, auf die Vortheile des Bessel’schen Reversionspendels zurückzukommen. In Folge
der oben gemachten Auseinandersetzungen kann man annehmen, der gegenwärtige Stand
der Pendelfrage sei ein derartiger, dass die Bestimmung des Einflusses der Luft mit
genügender Sicherheit aus dem Problem eliminirt werden kann.

Die Schwingungszeit muss aber, damit der Uebergang auf die Schwerkraft ge-
macht werden könne, mit gewissen, dem angewendeten Apparate zu entnehmenden Längen-
maassen verbunden werden, deren richtige Frmittelung besondere Schwierigkeiten bietet
und doch bei der absoluten Bestimmung der Schwere nicht umgangen werden kann.
Auf die Hilfsmittel, welche man zur Ueberwindung dieser Schwierigkeiten in Vorschlag
gebracht hat, soll nunmehr näher eingegangen werden.

Zunächst kommt. hierbei die Aufhängung des Pendels und die Ermittelung der
Drehungsachse des Systems in Betracht. Bislang ist fast allgemein die Aufhängung auf
Schneiden in Anwendung gebracht und in der überwiegenden Anzahl der Fälle die
Schneidenkante als Drehungsachse angesehen worden.

Es unterliegt gar keinem Zweifel, dass diese Annahme mehr oder minder will-
kürlich ist und auf ihre Richtigkeit geprüft werden muss. essel hat sich bei seinen
berühmten Untersuchungen von dem Nachtheile dadurch befreit, dass er die Differenz
zweier Pendel, die in gleicher Weise aufgehängt waren, bestimmte und ebenso ist Finger

*) Aehnlicher Methoden haben sich Pisati und Pucci bei ihren Experimenten bedient.

 
