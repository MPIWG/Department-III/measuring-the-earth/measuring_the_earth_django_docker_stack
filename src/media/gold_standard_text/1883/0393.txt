IT Sn

 

Bavière et Palatinat.
| |

DIRECTEURS (*) |

 

ENDIOATION

thie
LATITUDE. |LONGITUDE.|| ÉPOQUE. REMARQUES.

ET OBSERVATEURS.

h le. DES POINTS. |

 

(*) Vom Jahre 1801 bis 1807 war der Ingenieur Oberst Bonne, und von 1808 bis 1828 der Sternwarte Director Soldner Leiter
Landestriangulation; von da ab gab es keinen technischen Direetor mehr.

 

 

 

 

 

 

A) Die sieben Kreise rechts des Rheins.

 

 

 

 

INSTRUMENTS. |

 

Pe Neneer.. es. es AT 43) 2") 97° 49/27" oe Mader,v. Amman, Hermann. a we „Ppyerische
-19-2 Landes-Verme: g
2) Altenburg ..... | 49 52 51 | 28 32 0 a Mader, Wieland, Rathmayer ae le
| 49- Munich, 1878.
3 | Altomünster. . 48 23 18 | 28 55 18 | 1806-16-18 | Bonne, Mader, Hermann,
| 19-11 v. Amman.
D pen + 9. :. | 49 6:46 | 30 48° 1 1811 Soldner.
Bee ARtGn o.oo... |; 48 5 56 | 380 23 22 | 1810-11-29] Soldner, Hermann, Rath-
54 mayer.
6 | Attenhausen..... 48 33 45 | 29 40 13 | 1804-07 | Bonne.
71 Auerberg. - .... . 47 44 8 | 28 24 1805-10-11 | Rathmayer, Mader, Bonne, ok
15-16-21-53| Hermann, v. Amman. s
54 =
8 | Aufhausen . . ... ‚48 52 24 | 29 56 55 1805 Brousseau. a
9 | Aufkirchen. . ... 45 18 24 | 29 31 39 || 1801-54-56 | Henry, Fritz, Wieland, Bon- ©
57 ne, Rathmayer. Re,
10 | Augsburg. ..... | 48 21 42 | 28 33 54 || 1805-10 | Bonne, Glaser, v. Amman. 3 St
D Hans 2... ... 50 7 58 | 28 39 55 | 1823-42-48 is Rathmayer, Wie- $ ÊE
and. 2 8
12 | Benedictenwand . | 47.39 12 | 29 7 49 | 1852-54-55) Rathmayer. DCE RE
FeBax.!....:.. 50:22 281.29 26 39 1823 Mader. Mrs
14 | Blöckenstein. . 48 47 9.| 81.27 13 1811 Soldner. = ce
D Granberg |...’ 50 6 31 | 28 17 56 | 1817-48 | Rathmayer, Wieland. HS 4
16) Braunau : .. .:. 48 17 34 | 30 41 48 || 1810-11-14 | Soldner, Mader. SE
7 Breitsöl........ 49 54 11 | 27 5 29 | 1821-22-35 | Mader, v. Imsland. aS =
18 | Brennberg..... 49 419 | 30 3 50 || 1804-05-10} Brousseau, Soldner, Mader. = sf 2
En DU Saar
rep inuck. 2... . . 49 34 23 | 28 38 59 | 1821-42 | Mader, Wieland, Soldner. Sees
20°} Burgstall...... 49 58 29 | 29 4 382 || 1820-23-42 | Wieland, Weiss, Mader, v. man
43-49-50 Imsland. u
I  Bisen........ 48 942 | 2713 8 nein N. 7, Württemberg.
22 | Castlberg...... 48.37 47 | 29 11 46 1806 Bonne. HE 8
29 |-Coburg. . . . .- . 50 15 57 | 28 39 25 || 1822-23-30 | Mader, Wieland, v. Brand, ne
48-52 Rathmayer. TA
24 | Denkendorf . ... | 48 55 1 | 29 7 19 | 1804-05 | Brousseau. 3,8 des
25 | Dillenberg. .. . . 49 27 10 | 28 26 39 || 1822 | Mader. ono
pe Meera. 5... 50 16 43 | 29 18 27 || 1820-21-23) Mader, Wieland, Weiss. ee ie N. 1, Saxe.
| 43-49-50-51 337%
: : 52 sie og
27 | Döllberg ...... 49 19 30 | 29 248 ||1820-22-30| Hermann, Mader. TAÈE
28 | Dreifaltigkeit . . . | 48 40 30 | 30 3 22 || 1804-06-10 | Bonne, Soldner. À à 3 5
29: | Edelsberg. . . ... 47 35 31 | 28 9 40 | .1816-21 | Mader, Hermann. Store e
DU MÉdKOr. .... .… , 47 94 46 | 29 5 90 | 1852-53-54| Rathmayer. Sie
31 | Eichelberg..... 49 5 5 | 29 22 40 | 1805-09 | Brousseau, Soldner. me So 5
32 | Eisbrunn...... 48 46 29 | 28 19 9 1810-11 | v. Amman. een
2, Esehörs..... . . 47 50 53 | 28 2 27 || 1810-11-16} v. Amman; Mader, Hermann = 2
LAS 9] One Se
34 | Eulbacherhof . . . | 49 39 55 | 26 44 34 | 1822-35 | Mader, v. Imsland. Sun
35 | Eeldberg.-.....1501359| 26 7 21 1822 Mader.
36 |. Friedberg |... . 48 21 20 | 28 38 40 ||1810-11-16| Glaser, Mader, v. Amman.
37 | Fürstenstein. . . . | 48 43 16 | 30 59 35 | 1810-11 | Soldner.
38 | Gammersfeld. . . . | 48 48 16 | 28 43 15 |1804-11-12| Brousseau, Soldner, v.
Amman.
39 | Georgenberg. . . . | 47 56 42 | 28 20 56 | 1805-04-10| Glaser, Bonne, v. Amman.
u
40 | Gobernauserwald. | 48 6 6 | 30 58:33 1812 Glaser.

 

 

(Directeur) der

3

 
