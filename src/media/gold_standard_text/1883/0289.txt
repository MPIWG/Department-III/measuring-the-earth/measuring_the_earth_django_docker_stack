 

Ë
i

ee

oo
|
ae

IV. Bericht des Sections-Chefs Prof. Dr. Fischer.

Wie bereits im vorjährigen Bericht erwähnt, war durch die Nachmessung der
fehlenden Winkel in der ostpreussischen Kette von 1858 nachgewiesen worden, dass der
als Stationscentrum angenommene und festgelegte Punkt Schodehnen nicht mit dem
Stationscentrum von 1858 identisch sei. Dadurch war die Nachmessung der ganzen
Kette erforderlich und wurden die noch fehlenden Winkelbeobachtungen im Sommer
1883 ausgeführt. Da von vornherein nicht eine Neumessung, sondern nur eine Ergän-
zung der fehlenden Richtungen auf den früheren Dreieckspunkten beabsichtigt war, so
waren wir an das Festhalten der alten Punkte gebunden, sonst hätte auf Grund einer
neuen Recognoscirung das Netz anders gelegt und Dreieckspunkte gewählt werden können,
wo die Ausführung mit weniger Schwierigkeiten und Kosten verknüpft gewesen wäre.
So mussten wir aber theils ausgedehnter Wälder wegen hohe Holzpfeiler bauen, theils
waren wir gezwungen, uns bis über 20 Meter weit vom Centrum aufzustellen, weil
merkwürdiger Weise gerade immer im Centrum die Richtungen nach den Objecten durch
Windmühlen oder Fabrikschornsteine verdeckt wurden. Die Arbeiten begannen im Juni
und wurden die Winkelmessungen auf den noch fehlenden Stationen am 1. September
beendet. Nach Vollendung der Stationsausgleichungen führten die Herren Dr. Simon
und Dr. Westphal die Netzausgleichung der neuen Kette 1858 in Verbindung mit der
alten Kette 1859 aus. Der mittlere Fehler einer Beobachtung ergiebt sich wie früher
zu 1:33, jedoch lässt der wenig günstig übereinstimmende Werth der Seite Sommer-
feld—Wildenhof, abgeleitet aus der Gradmessungsseite Algeberg—Kallningken, mit dem
der Küstenvermessung die Fortsetzung der Neumessung wünschenswerth erscheinen.

Bei der Bearbeitung des trigonometrischen Nivellements: Helgoland—Neuwerk—
Wangeroog und Neuwerk—Kugelbake richtete ich die Untersuchung auch darauf, ob
sich vielleicht ein Einfluss der Gezeiten auf die trigonometrische Höhenbestimmung er-
kennen lasse, veranlasst durch den Umstand, dass, ausser im Nordosten, Neuwerk
während der Ebbe in einem Umkreise von 10 Kilometer Radius trocken liegt. Für das
Nivellement Neuwerk—Kugelbake besteht ein solcher Einfluss nicht, weil bei Niedrig-
wasser die ganze Distanz trocken ist; dagegen hat die hierauf gerichtete Discussion der
Messungen ihn sicher bei Helgoland— Neuwerk nachgewiesen. Die Zenithdistanzen, welche
im Jahre 1881 zur Zeit der Fluth gemessen wurden, geben den Höhenunterschied im
Mittel um 0,5 Meter grösser, als die bei Ebbe angestellten; aus den Messungen des
Jahres 1878 beträgt diese Differenz 0,8 Meter in demselben Sinne. Da in der Zeit
von 10 Uhr Vorm. bis 4 Uhr Nachm. beobachtet wurde, so fiel an einigen Tagen der
Uebergang von Niedrigwasser zu Hochwasser oder umgekehrt in die sechsstündige
Beobachtungszeit, sodass an solchen Tagen der Verlauf des Einflusses verfolgt werden
konnte. Vereinigt man daher für einen solchen Tag die Messungen, welche in der Nähe
der Ebbe resp. Fluth stattfanden, in je ein Mittel, so ergiebt sich als Differenz des
Höhenunterschiedes Helgoland—Neuwerk in dem Sinne Fluth weniger Ebbe für:

35*

 
