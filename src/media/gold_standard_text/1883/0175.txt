TROISIEME SEANCE

de la Conférence générale.

Rome, le 18 Octobre 1883.

Présidence de M. Ferrero; MM. Hirsch et v. Oppolzer remplissent les fonctions
de Secrétaires.
La séance est ouverte à 21 15m.
Sont presents les Délégués: MM. v. Bakhuyzen, Barozzi, Barraquer, Bassot,
v. Bauernfeind, Betocchi, Christie, Clarke, Cutts, Faye, Fearnley, Fergola, Ferrero, Fischer,
Foerster, v. Forsch, Hartl, v. Helmholtz, Hennequin, Hirsch, Ibanez, v. Kalmar, Lorenzoni,
Loewy, Magnaghi, Mayo, Nell, Oberholtzer, v. Oppolzer, Perrier, Pujacon, Respighi, Riimker,
Schiaparelli, Schiavoni, Schols, de Stefanis, Villarceau. |
Les Invités: MM. Barilari, Battaglini, Blaserna, Cantoni, Cerruti, Cremona, @’ Atri, |
Dimi, Galitzine, Garbolino, Govi, Lasagna, di Legge, Millosevich, Pisati, Pucci, Rosalba, |
de Rossi, Q. Sella, Stromei, Tacchini, de Vita.
M. le Colonel Ferrero, au nom de la Commission géodésique italienne, qui a dé-
cide de dedier au G?! Baeyer une medaille d’honneur, en signe de reconnaissance pour
ses grands mérites pour la géodésie, adresse à l’assemblée le discours suivant:

Messieurs !

Lorsque la Commission italienne eut à s'occuper des réceptions à faire
aux illustres étrangers, à l’occasion de la septième Conférence internationale,
sa première pensée s’est portée sur le vénérable fondateur de l'Association,
parce que, en honorant le nestor des géodésiens, elle savait très bien qu’elle
aurait en même temps honoré ceux qui en avaient suivi l’initiative; de même
que les titres d'honneur des chefs des armées rejaillissent sur les armées
elles-mêmes.

Le docteur Baeyer espérait prendre part à cette Conférence, et nous
avons partagé son illusion jusq’au dernier moment. Si le général avait été
présent, cela aurait été bien beau, car au Capitole il aurait reçu un témoignage
du respect et de l'admiration des Italiens.

 

 
