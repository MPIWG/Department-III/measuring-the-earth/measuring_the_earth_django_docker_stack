   

Marégraphes.

MINISTERE

Nivellement général

 

| DES Annexe à la Circulaire en date du 20 Xbre 1882. de

|: TRAVAURX PUBLICS,

a Art la France.
des

du Maregraphe de Nice.

Cartes et Plans.

arg,
unnnnn?z EN

Coupe en travers

x i inca a eis pec tala

montrant la communication entre la mer et le marégraphe.

 

 

 

i ir an

1 Fesenpe.

| : Pour prévenir l'encombrement du puits du flotteur et rendre en
- outre la marche de ce dernier aussi régulière que possible, les orifices
14 de communication des eaux entre le dit puits et le bassin du carénage
EB 2 messer an ont dü Etre munis des vannes ci-après désignées, savoir :

| CMMI L EEE, CH a — Vanne mobile en tiie de OMU0S d’épaisseur, 0M30 de largeur et
1 Om50 de hauteur, percée de trous de 002 de diamètre et espacés
| les uns des autres de OMS, susceptible d’être enlevée et remise à
I volonté au moyen de la tige de manœuvre dont elle est armée et des
|

|

CLS LL IL coulisses en bois qui la fixent contre la paroi du puits de vérification
gg WEISE KICK DE, intérieur en face de Vorifice du puits du flotteur.

GL LL b — Vanne fixe en bois de OMS d'épaisseur, 0M50 de largeur et Om25
LIL As WERKES de hauteur, scellée & Om(4 de distance de Vorifice du puits du flotteur
VALS MMA CML EEE et en face de cet orifice.

mole dw cavtnase LS © — Grande vanne mobile en bois de OM05 @’épaisseur, 050 de largeur
MEI LL, et 200 de hauteur, percée de trous de 0™U3 de diamétre, espacés de
ri CLS LL) Om05 les uns des autres, établie obliquement dans le puits de vérification
| CZ LL LL intérieur.
d — Vanne fixe en bois de Om0S d'épaisseur, 0m50 de largeur et 0m25
de hauteur, scellée & 010 de distance de Vorifice d'émission de l'eau
et & 0m05 au-dessus du seuil de cet orifice.
— Vanne mobile en tile de OM03 d’épaisseur, en tout semblable @ la
vanne de même nature à et identiquement placée dans le puits de
vérification extérieur.
f — Tube en tôle galvanisée dont l'établissement, reconnu nécessaire pour
régulariser le mouvement du flotteur, doit être effectuée incessamment.

   

 

©

 

Chambre

de |

“Appareil

  
  

: —— > ————10-¢ 9. —
l'Appareil | a

Se pue

  

 

     
    

    

SON

 

tation

FÉES SIN SEE

terificalion

r

Wreur

j Ç HZ, £ vs
een cohabiter AAALAA AA AD.

 

 

‘ ee
u

 
