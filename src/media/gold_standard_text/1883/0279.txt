 

Ë
3
|

IE NI RIT ET

letzterer nunmehr nach Wien verfügen konnte, um da auch mit dem Beobachter von
Kronstadt die persönliche Gleichung ein zweites Mal zu bestimmen, was am 2. September
beendet war, und wodurch indirect auch die zweite persönliche Gleichung zwischen den
Beobachtern von Kronstadt und Budapest (Pola) abgeleitet werden konnte.

Zu erwähnen ist noch, dass der Leiter der Instituts-Sternwarte, Major von Sterneck,
welcher die astronomischen Beobachtungen in Kronstadt ausführte, dort auch den Unter-
schied der Schwere zwischen 3 Punkten von beträchtlichen Höhenunterschieden, mit
seinem eigenen, provisorisch hergestellten Pendel-Apparate bestimmt, und überdies im
November und Dezember 1883 die im Vorjahre begonnene Untersuchung über die Schwere
auf der Krusnahora bei Beraun in Böhmen fortgesetzt hat.

B. Trigonometrische Arbeiten.
1... Lriangulinune in; Tirol:

a. Signalbau. Pyramiden wurden errichtet auf den Punkten 1. Ordnung: Rödt-
spitze, Eidex, Grosse Kreutzspitze, Hochwildspitze und Hinterthalkogel (letz-
terer im Herzogthum Salzburg gelegen). Dann auf den Punkten 2. Ordnung:
Vilanders und Stivo, sowie auf dem als astronomische Station in Aussicht ge-
nommenen Punkte Siegmundskron bei Bozen.

b. Beobachtungen wurden vorgenommen auf den Dreieckspunkten 1. Ordnung:
Pasubio (2102 m), Eidex (2740 m), Roen (2115 m), Birkenkogel (2905 m),
Cima d’Asta (2848 m) und Grossglockner (3798 m).

Die Messungen auf den erstgenannten zwei Punkten sind beendet.

Auf Roen unterblieb die Messung nach der Marmolada (3260 m), weil ungewöhnlich
hohe Firn- und Eismassen auf letzterem Berge die Sicht nach der nicht auf dessen
höchster Spitze stehenden Pyramide verlegt hatten und es eines ganz unverhältniss-
mässigen Kostenaufwandes bedurft hätte, um die in der Visur gelegenen Hindernisse,
welche in niederschlagsärmeren Jahren nicht vorhanden sind, wegzuräumen.

Auf Birkenkogel ist noch die Messung nach dem auf italienischem Gebiete liegen-
den Antelao (dessen Pyramide wahrscheinlich zerstört ist) nachzutragen, auf Cima d’Asta
aber wurden die Beobachtungen durch den Eintritt sehr ungünstigen Wetters unterbrochen
und konnten der bereits vorgeschrittenen Jahreszeit wegen nicht wieder aufgenommen
werden.

Auf dem Grossglockner wurden in Folge der im vorjährigen Berichte erwähnten
nothwendig gewordenen Abänderungen des Netzes die Richtungen nach der Rödtspitze
und nach dem Hinterthalkogel gemessen.

Von Stationen 2. Ordnung wurden absolvirt: Vilanders und Stivo; überdies
wurde das in der Nähe von Bruneck, so wie das zwischen Lienz und dem Grossglockner
gelegene zur Uebertragung der Präeisions-Nivellements-Coten auf die Hauptpunkte
dienende Netz kleinerer Dreiecke u. z. ersteres durch Messung auf Spitzhörndl (2273 m)

und auf zwei Punkten bei Bruneck, letzteres durch die Beobachtungen auf Rottenkogel
34

 
