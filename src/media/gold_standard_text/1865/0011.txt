PT

TIR ann

°F i PPT Te N

à HA Of

in!

jm |

Mien me mes

®
=
=

 

Lo

94 oil a nd

Nachrichten über die Theilnahme des Königreichs der Niederlande an der mittel-

europäischen Gradmessung.

Als, am Anfange des Jahres 1864, Herr Professor L. Cohen Stuart sich zu der Ver-
öffentlichung seiner Untersuchungen über Krayenhoff’s geodätische Vermessungen entschlos-
sen hatte, habe ich sogleich meine Vorschläge über die Theilnahme meines Vaterlandes an
der mitteleuropäischen Gradmessung bei meiner Regierung eingeschickt. Da es sich ge-
zeigt hatte, dass die Messungen Krayenhoff’s sich nicht zu der mitteleuropäischen Grad-
messung benutzen liessen, schlug ich die Messung einer neuen Dreieckskette vor, wozu, mit
den astronomischen Arbeiten, eine Summe von 20000 Gulden erforderlich schien. Seine
Excellenz der Minister des Innern brachte diese Summe auf das Budget des Staates, und im
Monat Januar des Jahres 1865 wurde dieselbe von unserer Volksvertretung genehmigt. In
meinem Schreiben an Seine Excellenz, Herrn General-Lieutenant Baeyer, vom 25. Februar
1865, erwähnte ich diese Entscheidung, welche mir damals nur durch die Zeitungen bekannt
war; aber die sich darauf beziehenden Zeilen sind, im General-Bericht für das Jahr 1864,
nicht mit abgedruckt. Da die erforderliche Geldsumme vorhanden war, erwartete ich täglich
die Ermächtigung des Ministers, um die Arbeit anzufangen, aber, aus mir unbekannten Ur-
sachen, hielt es bis zum Ende des Monats August vor, bevor ich von meiner Regierung ein
Schreiben über diese Angelegenheit erhielt. Aufgefordert um Vorschläge einzuschicken, über
die Art und Weise, wonach die Arbeit sollte angefangen werden, habe ich sogleich meinen
Vorschlag erneuert, um Herrn Professor Cohen Stuart die Leitung der geodätischen Arbei-
ten zu übertragen, und für ihn und mich selbst die Ermächtigung erbeten, um die erfor-
derlichen Instrumente zu bestellen. Ich erhielt bald darauf die erwünschte Ermächtigung,
aber mit der Nachricht, dass Herr Professor Cohen Stuart, seiner jetzigen Stelle, als Di-
rector der Polytechnischen Schule in Delft, wegen, die Leitung der geodätischen Arbeiten
nicht auf sich nehmen könnte und mit der Aufforderung, um darüber neue Vorschläge ein-
zuschicken. Ich habe unmittelbar darauf Herrn Professor M. Hoek in Utrecht, als einen
würdigen Stellvertreter des Herrn Professor Cohen Stuart, empfohlen, aber, nach einem an-
haltenden Abwarten, erhielt ich ein Schreiben des Herrn Ministers vom 1. December 1865,
mit der Nachricht, dass Herr Prof. Hoek die Leitung der geodätischen Arbeiten abgelehnt
hatte und dass mein Freund Herr Dr. F. J. Stamkart in Amsterdam, sich diese Leitung
hatte gefallen lassen. Es ist mir höchst erfreulich, dass Herr Dr. F. J. Stamkart, welcher
sich, seines schon vorgerückten Alters ohngeachtet, ausserordentlicher Kräfte des Geistes und
des Körpers erfreut, keine neuen und anstrengenden Arbeiten scheute und es nicht versagt
hat seine grossen Talente der mitteleuropäischen Gradmessung zu widmen.

Herr Dr. Stamkart hat ein Instrument zur Winkelmessung, bei Herrn Pistor &

Martins in Berlin, bestellt, und, können die Winkelmessungen im nächsten Sommer nicht an-

)%*
2
