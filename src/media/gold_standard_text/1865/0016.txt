 

a Le

Nur in einem Punkte fand eine Abweichung des früheren Verfahrens statt. Bei
diesem hatte ich sowohl die Polhöhenbestimmungen als auch vor und nachher in jeder Nacht
die Zeitbestimmungen allein gemacht. Da dies Verfahren für einen Beobachter, wenn 8 Sterne
im ersten und letzten Vertical beobachtet werden, etwas zu anstrengend ist, so übernahm
diesmal der eine Beobachter die Polhéhen, der andere die Zeitbestimmungen, und zwar in
der Art, dass die Zeitbestimmungen nicht an dem Orte der Polhöhenbestimmung, sondern
im Observatorium der Landesvermessung angestellt und von dort nach dem ersteren Orte
hin telegraphisch übertragen wurden.

Im Ganzen sind 62 vollständige Sterndurchgänge, d. h. solche bei denen der Stern
sowohl östlich als auch westlich vom Meridian durch die Fäden des Fernrohres gegangen
ist, beobachtet. Von diesen Durchgängen sind 9 als unsicher sogleich bei der Beobachtung
bezeichnet, so dass als zuverlässig nur 55 übrig bleiben.

Berechnet sind die Beobachtungen noch nicht, unter der Voraussetzung aber, dass
sie den Polhöhenbestimmungen im hiesigen Observatorium der Landesvermessung gleich-
kommen, lässt sich der wahrscheinliche Fehler des Resultats, abgesehen von den Fehlern in
den Declinationen der Sterne, zu

a 0%, 112
abschätzen.
B. Trigonometrische Messungen.

I. Die Vorarbeiten zur Veröffentlichung der trigonometrischen Messungen sind so
weit fortgeführt, dass:

1) die Messungen der Hauptdreiecke und der gegenseitigen und gleichzeitigen trıgo-
nometrischen Höhenmessungen,

2) die Herleitung der Resultate aus diesen Messungen und

3) die Resultate selbst übersichtlich geordnet und druckfertig vorliegen.

Ii. In Bezug auf die gewöhnlichen Nivellements, im Gegensatze von den trigono-
metrischen Höhenmessungen sind weitere Erfahrungen gesammelt. Die bisher gewonnenen
Resultate lassen sich so aussprechen:

1) die in den Berliner geodätischen Conferenzen vom Jahre 1864 meinerseits ge-
machte, auch in das Protocoll Seite 28 aufgenommene Mittheilung, dass die hier ausgeführten
gewöhnlichen Nivellements die hiesigen, mit den vorzüglichsten Hülfsmitteln angestellten
trigonometrischen, gegenseitigen und gleichzeitigen Höhenbestimmungen an Genauigkeit 5 bis
7 mal übertreffen, wird durch Wie weiter vollführten, über mehr als 7 Meilen ausgedehnten
Nivellements durchweg und in noch höherem Maasse bestätigt.

2) Die Fehler der diesseitigen gewöhnlichen Nivellements haben ihren Grund ganz
überwiegend in den Fehlern der Latte, oder in einer nicht senkrechten oder nicht sorg-

fältigen Aufstellung der letzteren. Sorgt man für eine thunlichste Abminderung der aus

dieser Quelle herrührenden Fehler, so kann dadurch allein die Genauigkeit der diesseitigen

Nivellements noch erheblich gesteigert werden.

 

0 pme en ag flail LAA lid lh aca mann

at semenanncinans, mins sil |

 
