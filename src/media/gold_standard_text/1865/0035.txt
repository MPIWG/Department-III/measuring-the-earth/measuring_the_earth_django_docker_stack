Ih re en

2 RATE TNA SE EN |

Ebenso die Höhe des Fixpunktes im Rathhause zu Altenberg:

hin! jui ahead ‚08a Ders

zumeks.' 1.0 dB.

also tn :Mittel 411099 6410 2
Es ist zu erwarten, dass auf weniger bergigem Terrain die Differenzen noch kleiner
ausfallen. |

Die Art und Weise, wie die Nivellirungsarbeiten einer Linie, z. B. Dresden-Schandau
vollzogen werden, ist folgende:

Der eine Assistent beginnt in einem Endpunkt, z. B. in Dresden, der andre gleich-
zeitig im andern Endpunkte, z. B. Schandau, und nivellirt dem erstern entgegen; wenn sie
sich begegnen, machen sie sich gegenseitige Mittheilungen über die Anhaltepunkte und Fix-
punkte, und wechseln zur grössern Sicherstellung auch noch mit ihren Gehilfen.

Freiberg, den 5. Februar 1866.
Julius Weisberg.

2. Bericht über die geodätischen und astronomischen Arbeiten im Königreich Sachsen
von Professor Dr. C. Bruhns.

Im Jahre 1865 wurden astronomische Bestimmungen im April, Juni, Juli, August und
September ausgeführt. Die erste Arbeit, nach Verabredung mit dem Director der Gothaer
Sternwarte, Herrn Geh. Reg.-Rath Hansen, war die Längenbestimmung Gotha-Leipzig, zu
der Herr Dr. Auwers in Gotha und ich die Beobachtungen anstellten. Ueber das Resultat
dieser Arbeit, bei der die beiden Methoden, die des sogenannten Registriren’s und die der
Coincidenzen, angewandt wurden, hat Herr Geh. Reg.-Rath Hansen zu berichten übernommen.

Als zweite Arbeit wurde die Längenbestimmung zwischen Leipzig und Wien im Monat
Juli ausgeführt. Während bei der vorhin erwähnten Längenbestimmung Leipzig-Gotha die
Beobachter durch Wechseln ihrer Stationen die persönliche Gleichung eliminirten, wurde zwi-
schen Wien und hier die persönliche Gleichung einmal im Juni in Leipzig, das andere Mal
im August in Wien bestimmt. Die Instrumente waren vollständig gleich, beides gebrochene
Passagen-Instrumente aus der Werkstatt der Herren Pistor und Martins in Berlin. Im Juli
wurde an sechs gleichzeitig heitern Abenden in Wien und hier dieselben Sterne nach der
Auge- und Ohr-Methode beobachtet und die Uhren auf telegraphischem Wege durch Coinci-
denzen und Signale zwischen den Beobachtungen selbst miteinander verglichen. Das schöne
Wetter im Juli begünstigte die Beobachtungen so sehr, dass in dem kurzen Zeitraume von
noch nicht 14 Tagen die eigentlichen Beobachtungen zur Längenbestimmung zu Ende ge-
bracht wurden. Da die benutzten Polsterne erst am Berliner Meridiankreise bestimmt wer-
den mussten, ist es noch nicht möglich gewesen, das Resultat dieser Längenbestimmung ab-
zuleiten.

Im September wurde, wieder vom herrlichsten Wetter begünstigt, die astronomische

Bestimmung des Jauernick bei Görlitz, des Endpunktes der Sächsischen Längengradmessung,
5 *#

 
