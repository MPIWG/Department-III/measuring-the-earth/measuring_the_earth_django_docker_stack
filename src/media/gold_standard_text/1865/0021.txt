MRC

Le A ar PPP PTT AT |

Lu n

Man beschränkte sich dabei aus den oben angeführten Gründen wieder auf die Signal- und
die Coincidenz-Methode, modificirte jedoch die erstere nach einem sinnreichen Vorschlage des
Hrn. Dir. Förster dahin, dass die Signale nicht durch das Ohr aufgefasst werden sollten, wo-
bei sehr veränderliche Gleichungen auftreten, sondern auf dem Registrirstreifen beider Sta-
tionen zu notiren waren. Für diese Art der Beobachtung müssen selbstverständlich an jedem
der beiden Orte gemeinschaftliche Sterne, übrigens nur lokal, — werden, um die Gleich-
mässigkeit der Zeitscale herzustellen.

Bis Ende August war an beiläufig 120 Sternpassagen die persönliche Gleichung
zwischen den genannten Beobachtern mit Auge und Ohr so wie bei Registrirbeobachtungen
ermittelt, wobei diese Herren zunächst an ihren gebrochenen Fernröhren das später in
Wien bestätigte, höchst interessante Resultat fanden, dass bei Beobachtungen mit Auge und
Ohr die persönliche Gleichung mit der scheinbaren Bewegungsrichtung der Sterne sich ändert,
mit anderen Worten: dass sie bei K. O. und K. W. verschieden sein kann. Im vorliegen-
den Falle ergab sich ein Unterschied von nicht weniger als 0°.1. Diese Entdeckung machte,
eine weitliufige Revision der Dablitzer Beobachtungen nothwendig, die durch früher über-
gangene constante Differenzen zwischen den Zeitbestimmungen bei K. O0. und K. W. sofort
dieselbe Erscheinung zu erkennen gaben, und zwar so, dass die Unterschiede der Zeitbestim-
mungen von einer Lage des Kreises zur andern bei Bruhns und Weiss entgegengesetzte
Zeichen haben. Bei Registrirbeobachtungen wurde ein solcher Unterschied nicht bemerkt.

In den Nächten des 12., 16., 18., 21., 22., 23., 24., 26, September und 2. October gelang
die Längenbestimmung meist vollständig. Am 4. October traf Herr Dir. Förster in Wien
ein, wo er an drei Tagen wieder durch etwa 120 Sterndurchgänge mit Dr. Weiss sich
verglich.

Was die früheren Arbeiten betrifft, so sind die Dablitzer Beobachtungen vollständig
berechnet und werden nächstens dem Drucke übergeben. Die gewonnenen Resultate können,
wie ich schon hier zu bemerken nicht umhin kann, als sehr befriedigend gelten. Auch die,
im J. 1864 auf dem Laaerberge bei Wien vorgenommenen Breiten- und Azimuthmessungen
sind grösstentheils reducirt, so dass die Berechnung der Längenbestimmungen des vorigen
Jahres hoffentlich bald wird in Angriff genommen werden können. Herr Dir. Förster hat die
Güte die dabei in Anwendung gekommenen Polarsterne und für die Längenbestimmung Wien-
Berlin auch so viele der Zeitsterne, als zur Ermittlung der Instrumentalfehler nöthig sind
am Meridiankreise der Berliner Sternwarte bestimmen zu lassen.

Wien, 28. Febr. 1866.
C. v. Littrow.
13, OIdenbwrn

Nachdem das Centralbüreau in Erfahrung gebracht hatte, dass Oldenburg im Besitz
von Hauptdreiecken sei, die zum Theil von Gauss selbst gemessen sind und der Freiherr

v. Schrenck dieselben bereits unter dem 6. Mai mitgetheilt hatte, musste es höchst wünschens-

 
