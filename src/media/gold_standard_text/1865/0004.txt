 

|
|

Be

Die permanente Commission sprach sich in der Sitzung am 4. Sept. auf die betreffende
Vorlage des Generallieut. z. D. Baeyer dahin aus, dass über die fraglichen Punkte, ohne die
Kenntniss der vorhandenen Materialien, ein begründetes Urtheil nicht abgegeben wer-
den könne, und beschloss, an die Grossherzogliche Regierung die Bitte zu richten, ihr die
Details der in Baden vorhandenen Triangulationen mittheilen zu wollen. Dieser Bitte wurde
vom Grossherzoglichen Ministerium des Innern unter dem 16. Jan. 1866 durch Uebersendung
der Abstände der Dreieckspunkte vom Mannheimer Meridian und Perpendikel nebst einer
Dreieckskarte bereitwillig entsprochen. Die permanente Commission überschickte dies Ma-
terial unter dem 6, Febr. dem Centralbüreau mit der Aufforderung, ihr bei der nächsten Zu-

sammenkunft am 6. April in Neuenburg darüber Bericht zu erstatten.

2. BA Ie rn.

Dem Centralbiireau ist durch das diesseitige Königl. Ministerium der auswärtigen
Angelegenheiten die erfreuliche Benachrichtigung zugekommen, dass die Königlich Baierische
Regierung, nachdem sie nicht in der Lage gewesen sei die 1864 in Berlin stattgefundene
allgemeine Conferenz der Bevollmächtigten für die mitteleuropäische Gradmessung beschicken
zu können, in Anerkennung der hohen wissenschaftlichen Bedeutung des Unternehmens, ihren
Bevollmächtigten, den Prof. und Direktor der Münchener Sternwarte Dr. Lamont beauftra-

sen werde sich an der nächsten allgemeinen Conferenz zu betheiligen.

> belsıcn.

Anstatt eines erwarteten Berichtes ist leider durch den Oberstlieutenant a. D. Die-
denhoven unter dem 4. März dem Centralbüreau die betrübende Nachricht zugegangen, dass
sein Bruder, der Oberst im Generalstabe Diedenhoven, Dirigent der Belgischen Triangu-
lation, seit Monaten an einer schweren Krankheit darniedergelegen habe. Er sei zwar ge-
genwärtig in der Reconvalescenz begriffen, allein seine Gesundheit sei noch so geschwächt,
dass er sich mit keiner ernstlichen Arbeit beschäftigen dürfe. — Wünschen wir, dass dieser
ausgezeichnete Belgische Geodät‘ recht bald wieder in den Stand gesetzt werden möge, seine

bisher so erfolgreiche Thätigkeit wieder aufnehmen zu können.

2. Danemark

Bericht des Geheimen Etatsrath Andrae.

Die dänischen Hauptdreiecke,

welche Kopenhagen mit den schwedischen und preussischen Dreiecken verbinden.

Die dänischen Hauptdreiecke, durch welche Kopenhagen einerseits mit den schwe-

dischen, andererseits mit den preussischen Dreiecken verbunden wird, bilden für die mittel-

europäische Gradmessung das unenkbehrliehe Glied zur Verbindung sämmtlicher Triangula-

 

=
x
>

hun ae Muh

FT

lc chen as 0 Äh Md lie da man an

D LAURE hat NT

 
