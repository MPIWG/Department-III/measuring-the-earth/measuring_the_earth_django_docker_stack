 

ss

Zwischenpunkte, z. B. solche an Chaussdehäusern, an Haltestellen von Eisenbahnen
u. s. w. werden durch schwächere Stahlbolzen fixirt.

Der Nivellirungsapparat, welcher zu den Nivellirungsarbeiten angewendet wird, be-
steht aus einem guten Luftblasenniveau mit Elevationsschraube und einer Röhrenlibelle zum
Umsetzen; ferner aus dem hierzu nöthigen Stativ und aus zwei Nivellirstangen mit der nö-
thigen Ausrüstung, wozu gewölbte schmiedeeiserne Fussplatten mit Fussspitzen und Dosen-
libellen mit ihren an die Stangen anzuschraubenden eisernen Postamenten gehören.

Diese Stangen sind 4,2 Meter lang, und die Eintheilung derselben geht bis auf Cen-
timeter, wobei sich oft noch Millimeter abschätzen lassen.

Ausserdem gehört zu dem Apparat noch eine mechanische Vorrichtung, wodurch die
kurzen Nivellements von den Nivellirstangen nach den durch Messing- oder Stahlbolzen fixir-
ten Punkten vollzogen werden.

Das Nivelliren wird, so weit es ein blosses Ausschreiten zulässt, stets aus der Mitte
vollzogen; auch ist jede Ablesung an der Nivellirstange nach Umsetzung der Libelle zu
wiederholen, sowie jede Linie zwei Mal, ein Mal hin und ein Mal zurück, zu nivelliren.

Aus den Nivellirungsarbeiten, welche im Spätsommer 1865 von zwei Assistenten voll-
zogen worden sind und welche die vollständigen Nivellements der Linien: Dresden-Altenberg
und Dresden-Freiberg sowie theilweise die der Linien Freiberg-Altenberg und Freiberg-Sayda
gegeben haben, sind in Betreff der Ausführung der Nivellirungsarbeiten folgende Erfahrungen
gemacht worden. /

Ein Assistent nivellirt hin und zurück, je nach der Länge und Beschaffenheit des
Terrains, monatlich 1 bis 12 Linie von je 3 bis 5 Meilen Länge in grader Linie, liefert daher
in einem Jahre mit 6 Arbeitsmonaten im Durchschnitt 3 Linien. Durch zwei Assistenten
werden folglich jährlich 16 Nivellirungslinien geliefert, wonach in 3 Jahren mindestens das
ganze aus 46 Linien bestehende, auch in praktischer Hinsicht sehr wichtige Hauptnivellirungs-
netz von Sachsen vollendet sein möchte.

Die durch diese Nivellirungsarbeiten erlangten Ergebnisse sind schliesslich durch die
Ausgleichungsrechnung zu einem Ganzen zu vereinigen, dessen Angaben bei den Nebenni-
vellements zu Grunde gelegt werden.

Der Anschluss des sächsischen Nivellirungsnetzes an die Nivellements der benachbar-
ten Staaten ist durch die Eisenbahnen leicht zu bewerkstelligen.

Die Genauigkeit, welche durch derartige Nivellements zu erlangen ist, ist aus folgen-
den Thatsachen zu ersehen.

Die Höhe des festen Punktes im Bahnhof zu Freiberg über dem im böhmischen Bahn-

hof zu Dresden ist gefunden worden:
| Min |. 297,514 Meter;
nor. un. 2.990530. 1-
also ımı Mittel 4 : 294,522 N -

 

{

 
