 

 

PUNTPTE TAT

rt

RT A A

Beilage A. I.

BERICHT

der für die Vorbereitung einer internationalen Organisation der
Breitenbestimmungen eingesetzten Specialkommission.

Die Commission bestehend aus den Herren Schiaparelli, Tisserand und Feerster hat
sich zunächst dureh Correspondenz über die Form zu einigen gesucht, in welcher der ihr
zugewiesene Schritt bei der Internationalen Astronomischen Gesellschaft, nämlich die Befra-
gung derselben über die Zweckmässigkeit einer internationalen Organisation der Breiten-
bestimmungen ausgeführt werden sollte. Zu diesem Zwecke wurde von Herrn Feerster der Ent-
wurf eines Schreibens an den Vorstand der Astronomischen Gesellschaft den beiden andern
Mitgliedern der Commission zur Genehmigung oder Abänderung übersandt. Derselbe lautete
folgendermassen :

Entwurf eines Schreibens an den Vorstand der Astronomischen Gesellschaft.

In der letzten Versammlung der Permanenten Kommission der internationalen Erd-
messung in Genf (September 1893) sind die Unterzeichneien damit betraut worden, hinsicht-
lich der künftigen Organisation der Messungen zur Bestimmung der Lagenänderungen der
Erdaxe sich zunächst mit der Astronomischen Gesellschaft und sodann auch mit den leitenden
Organen der geologischen Forschung in Verbindung zu setzen, um womöglich gemeinsam die
internationale Dotation eines solchen für die Astronomie, die Geodäsie und die Geologie
wichtigen Messungsdienstes zu Stande zu bringen,

Es wird sich künftig darum handeln, für die zahlreichen Messungen, bei denen die

ASSOCIATION GEODESIQUE — 16

 
