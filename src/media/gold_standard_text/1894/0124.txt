 

ur 118

M. le Président ne veut pas terminer les travaux de la Conférence sans exprimer, au
nom de l’Assemblée tout entière, les plus vifs remerciements aux Autorités politiques, uni-
versitaires et communales du pays, pour l’accueil bienveillant et hospitalier qu’elles ont bien
voulu accorder à la réunion internationale des géodésiens. Îl témoigne sa reconnaissance d’une
manière toute particulière à Son Excellence le comte Merveldt, Gouverneur du Tyrol, qui,
après avoir ouvert la session par un discours éloquent, a reçu les délégués de la mamière la
plus gracieuse dans son palais; à M. le baron von Reden qui, au nom de M. le Gouverneur,
a guidé les membres de la réunion de la manière la plus aimable dans l’excursion à l’Achen-
see, ce joyau parmi les beaux environs de la splendide ville d'Innsbruck; à M. le recteur de
l'Université, Professeur Ehrendorfer, qui a mis à la disposition de la Conférence la belle salle
de ce vénérable bâtiment, et enfin à M. le bourgmestre Dr Môrz, qui a bien voulu assister
aussi à la séance d'ouverture.

M. von Kalmar croit être l’organe de tous ses collègues en remerciant M. le Président
et le bureau de la manière impartiale et distinguée avec laquelle ils ont dirigé les délibéra-
tions de l’Assemblée.

La séance est levée à 3 1/, heures et la session déclarée close.

sb a ana

j
i
4
1
=

 

 
