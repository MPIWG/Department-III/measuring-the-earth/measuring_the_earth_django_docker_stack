 

Annexe B. XII.

ESPAGNE

Rapport sur les travaux céodesiques executes par l'Institut géographique
et statistique d’Espagne en 1894.

[. — TRAVAUX GEODESIQUES

Le Capitaine d’État-Major M. Escribano s’est rendu cet été sur la partie nord-ouest
de la péninsule, près de la frontière du Portugal, pour stationner à certains sommets d’un
quadrilatère de notre réseau de 4e ordre, qui sont devenus indispensables pour compléter les
calculs des coordonnées nécessaires pour notre carte topographique.

IL. — TRAVAUX ASTRONOMIQUES

Afin de former un polygone de longitude le long du contour de la Péninsule, dont
les sommets puissent ultérieurement être reliés à la capitale du Royaume, point fondamental
du réseau géodésique, on fait exécuter cet automne les observations de longitude à Vigo et
à Saint-Sébastien par MM. Borrès et Estéban.

Les deux points forment des sommets de notre triangulation de premier ordre, avec
latitudes et azimuts directement déterminés depuis longtemps.

r

MM. Borrès et Estéban se sont occupés cet hiver de réduire leurs observations de

longitude Madrid-Desierto, et MM. Defforges et Estéban se sont communiqué les résultats de

celles de Desierto-Rivesaltes.
M. Borrès calcule la latitude de Castellén et relie la position de Vigo à celle de

Gastrove.
Les calculs de la différence Monjouich-Vigo (1200 kilomètres de ligne télégraphique
et 43 minutes de longitude à peu près) sont aussi (très avancés.

eta Madd ada a ch ann

 

 
