 

éloiles, exigent des manipulateurs el observateurs très exercés, el compromeltent même
pour ceux-ci souvent la réussite parfaite, à cause de la précipitation du travail.

« Dans l'intérêt de eette transformation photographique de la méthode, qui promet
ainsi de grands avantages pour son emploi géodésique et astronomique, on propose mainte-
nant de construire et d'essayer un instrument qui coûlerait environ 7000 M. (8250 fr.), y
compris l'appareil auxiliaire servant au relevé des photographies.

« Les mesures mêmes seraient confiées à M. le D' Marcuse, sous la direction de
M. le prof. Foerster.

« Les indemnités pour l'observateur seraient à la charge de l'Observatoire de Berlin.

« Le constructeur Wanschaff, 4 Berlin, qui a fait ses preuves dans cette spécialité,
est prêt à construire l'instrument à ses risques et périls, sans même ètre assuré d'un acqué-
reur définitif. Il compte qu'après l'épreuve favorable de la méthode, l'instrument sera acheté
soit par l'Observatoire de Berlin, soit par l’Institut géodésique de Potsdam, soit par un autre
établissement, et qu'alors la commande d’un certain nombre de ces instruments lui permettra
de rentrer dans ses frais.

« Toutefois, pour se charger de ce risque, la maison Wanschaff demande une avance
de 3000 M. (3750 fr.), garantie : par l'instrument méme ; aussitol après son placement, celte
avance serait remboursée.

« Or, comme dans ce moment les établissements cités plus haut ne sauraient, pour
des raisons de forme déjà, fournir une pareille avance, tandis qu'il serail désirable, dans
l'intérêt de l'Association géodésique internationale, de pouvoir expérimenter instrument
encore dans le courant de cet été, je propose d'accorder la somme de 3000 M. dans le but
exposé, sur le crédit de 4000 fr. (3200 M.) voté en 1892 par la Commission permanente en
faveur des travaux de latitude, et non encore employé, sous la condition que cette somme
sera restituée à l'administration de l’Association géodésique, aussitôt que l'instrument deviendra
la propriété d'un autre établissement, ou bien qu'elle figurera comme premier versement
payé d'avance pour le prix de instrument, dans le cas où la Commission permanente deei-
derait d'acquérir elle-même l'instrument pour Pune des stations de latitude dont on étudie

la création en ce moment.
« (Signé) W. FŒRSTER. »

« En soumettant, par circulaire du 23 avril, la demande de M. Foerster au vote de
la Commission permanente, le bureau l’a appuyée, en faisant valoir la considération que,
bien qu’on ne puisse pas encore invoquer l’expérience directe en faveur de cel emploi du
procédé photographique, les raisons indiquées par M. Fœrster paraissent assez fondées pour
justifier l'essai de remplacer, dans la méthode Horrebow, l'observation optique des étoiles
par leur enregistrement photographique, et d'autre part le motif que les sommes consacrées
par l'Association géodésique spécialement aux études des varialions de la hauteur du pôle
étaient loin d’être absorbées.

« Aussi, le bureau a pu informer avec plaisir la Commission, par circulaire du 9 juin,
que la proposition de M. Foerster avait été votée à l'unanimité des membres de la Commis-

 

ä
3
a
i
i

babe haine din er

22 La Li huh LA

ans; sésame bib

 
