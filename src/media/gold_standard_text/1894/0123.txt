ee nn

 

a

THU TAT

HN

mp TN É

He TE

Ihe

 

117

observées dans la région de Moscou, est mise en circulation auprès des membres de las-
semblée !.
M. Hirsch présente ensuite le rapport sur les travaux de la Commission géodésique

suisse. (Voir Annexe B. XI.)
ll distribue des exemplaires du Procès-verbal de la dernière séance de cette (om-

mission et il fait remarquer d’une manière particulière les résultats d’un travail que M. L.
Du Pasquier a exécuté sur la demande de la Commission pour calculer l'attraction des mon-
tagnes sur la direction de la verticale dans un certain nombre de stations situées près du
méridien de Neuchâtel. Il en ressort le résultat important que, sauf pour une des stations
(Middes), l’action des masses visibles rend parfaitement compte des déviations observées dans
les limites des erreurs d'observation et de calcul.

Pour clore la série des rapports, M. d’Arrillaga communique celui de l’Institut géo-
graphique et statistique d'Espagne. (Voir Annexe B. XII.)

M. von Sterneck demande la parole au sujet d’une remarque contenue dans le rapport
lu par M. Albrecht, où il a signalé les bons résultats du micromètre enregistreur de Repsold
pour l'observation des passages des étoiles ; il désire constater que la première idée de l’em-
ploi de cette excellente méthode est due a M. le Dr Carl Braun, P. S. I., ancien directeur de
l'Observatoire de Kalocsa, dont il tient à rappeler le mérite.

M. ven Kalmar appuie chaleureusement cette réclamation de priorité.

M. Helmert, sans contester la priorité du principe, voudrait cependant faire remar-
quer que l’idée de Braun est restée longtemps sans profit pour la science, jusqu'à ce que
Repsold ait réussi à l’exécuter d'une manière pratique.

M. le Président croit qu’il ne reste plus à l’ordre du jour que la fixation du lieu et
de l’époque de la prochaine Conference cénérale et il estime que, d’après les précédents de
l'Association, lorsqu'il s’agit d’en modifier ou renouveler l'organisation, il va de soi que la
Conférence se réunisse à Berlin.

Quant à l’époque, M. Hirsch croit que les conditions climatériques, aussi bien que
les vacances, imposent le mois de septembre; mais pour fixer le jour précis, il convient de
charger, comme les autres fois, le bureau de la Commission permanente de faire au moment
voulu des démarches auprés des collégues et de les consulter en dernier lieu par circulaire.

La Commission permanente adopte à l'unanimité Berlin pour lieu de La prochaine
Conférence générale, et pour époque le mots de septembre.

1 Cette carte sera publiée dans les prochains Comptes-Rendus, après avoir subi quelques corrections
que M. le Gal Stebnitski envisage comme nécessaires. À. H.

 
