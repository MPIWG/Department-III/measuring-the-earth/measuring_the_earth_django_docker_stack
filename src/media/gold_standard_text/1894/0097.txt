 

 

TP TET

sat ia

rer

PRET BRET TT LI

SURE TT

Le procès-verbal est adopté sans observation.

M. le Président ouvre ensuite la discussion sur la question de la détermination de la

pesanteur el prie M. Hirsch de présenter le rapport de la sous-commission nommée dans la

première séance.

M. Hirsch rapporte qu'après une entente personnelle avec M. le professeur Weiss,
président des délégués académiques réunis à Innsbruck, la sous-commission a été invitée à
prendre part à la séance de ces derniers, qui a eu lieu hier à 11 heures. Après que MM. les
délégués des Académies avaient déjà pris quelques résolutions préalables, que M. le professeur
Weiss aura la bonté de communiquer, M. Hirsch, au nom de la sous-commission, dans le but

de donner satisfaction aux désirs des Académies associées et d’éviter tout danger de scission,
a proposé la résolution suivante, qui doit encore être ratifiée par la Commission permanente :

« La Commission permanente est disposée, à l’occasion du renouvellement de la
Convention géodésique internationale, en 1895, à faire la proposition de constituer dans son
sein une section pour l'étude de la pesanteur, de son intensité aussi bien que de sa direction,
et, par augmentation du nombre de ses membres, de rendre possible une représentation
convenable des intérêts géologiques et géophysiques. »

Après un échange d'opinions, une entente a été facilement obtenue sur la base de
celte proposition, ainsi que M. le professeur Weiss va le confirmer.

A la demande du Président, M. Weiss fait la communication suivante :

Rapport de M. E. Weiss sur la séance des délégués des Académies el sur la séance commune
de ceux-ci avec les représentants de la Commission permanente.

« Les délégués des Académies associées, qui avaient pris l'initiative d'assurer une
extension plus grande et une exécution systématique aux mesures de la pesanteur, se sont
réunis dans la matinée du 6 septembre pour une délibération qui a eu pour résultat deux
résolutions, dont la première, qui doit être considérée comme un simple communiqué à leurs
Académies, est conçue dans les termes suivants :

« Les Académies associées nomment une Commission permanente, dans laquelle la
oéologie sera représentée. Cette Commission est chargée de faire avancer les études de la
pesanteur, d'accord avec l’Association géodésique, dans les pays qui en font partie, et d'une
manière indépendante dans les autres pays. »

La seconde résolution se résume dans la demande suivante, à l’adresse de l’Asso-
ciation géodésique internationale :

« Les Académies intéressées expriment le désir que, lors de la réorganisation de
l’Association géodésique internationale, celle-ci veuille bien constituer une sous-commission
chargée de délibérer en commun avec les délégués des Académies. »

 
