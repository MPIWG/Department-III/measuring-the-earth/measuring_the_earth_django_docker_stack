 

 

AAA mt

OTT

RP QT

irre

lio

Et même, à supposer que la construction du médimarémétre ou de tout autre ap-
pareil atteindrait une perfection telle qu’on n’aurait plus à se préoccuper du réglage ou du
démontage de l'appareil, et qu’en outre, dans chaque pays, on réussirait par une grande
série d'observations longtemps continuées, à trouver une hauteur correspondant avec la
hauteur moyenne du niveau de la mer sur un certain point de la côte, il resterait toujours
la plus grande difficulté à résoudre,

En effet, on disposerait alors d'autant de plans différents qu’il existerait de fpoints
d'observation sans qu’on fût en possession d’aucun moyeh pour signaler celui de ces plans
qui s’approcherait le plus du niveau moyen général de la mer.

La condition dans laquelle on se trouverait aurait ainsi beaucoup de ressemblance
avec la situation actuelle, sauf encore cette différence que les nouveaux plans de niveau
n'auraient pas linvariabilité des plans actuels.

On n’a pas jusqu'ici fait voir comment, dans ce cas, l'unification cherchée se trou-
verait réalisée d'elle-même sans l’intervention arbitraire d’une compensation générale, ni
prouvé que celle compensation serait pratiquement nécessaire pour transporter d’un pays à
l’autre une hauteur choisie, au moven d’un nivellement.

La Haye, 16 août 1894.
VAN DI

(es)
wa
vd

N:

 
