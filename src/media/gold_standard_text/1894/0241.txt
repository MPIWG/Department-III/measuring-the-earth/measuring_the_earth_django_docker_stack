 

WW

i (ia nn Sn en

AH

fear

am | linet

Ly CRI re 0 Am

Gi

239

prototype international par M. le vice-directeur de l'Observatoire de Poulkova, A. Socoloff.
Ainsi, l’étalon N a été trouvé égal à 37898162 416 95 C, c’est-à-dire plus grand qu’autrefois

de =. Si l'on introduit cette correction dans la mesure de la base de Poulkova avec l’ap-
IHM

pareil de Struve, il en résulte la valeur de 2269170, c’est-à-dire une différence avec la

x r an 2 1 ® cs im
mesure d’après la méthode de Jäderin, de ==. La plus grande partie de cette différence

63000
s'explique par des changements de place des points aux extrémités de cette base, ainsi que
par une certaine variation du metre de Turrettini après sa comparaison à Breteuil.
La construction des signaux géodésiques a été commencée en 1888 dans la partie occi-

dentale de la triangulation et continuée en 1893 jusqu’au méridien de Reval. Les îles du

 

«olfe de Finlande Hochland (Mäkki Päalis) Lavensaari et Grand-Tutters et les églises de Ma-

holm et de Halljal en Esthonie au bord du golfe, font aussi partie de ce réseau. Les trois
derniers points entrent dans la mesure de l'arc du méridien de W. Struve.

Les angles horizontaux ont été mesurés par le Lieutenant-Colonel Witkovsky dans la
partie orientale de la triangulation, entre Toxovo et Jambowrv, et de là jusqu’à Hochland et
Halljal par le capitaine Lorentz. Ces mesures ont été faites à l’aide d’un instrument universel
nouveau de Bamberg, dont le limbe avait sept pouces de diametre et la lunette de contrôle
portait un micrométre; la lecture se faisait à l’aide de deux microscopes avec une exactitude
de T7. La lunette principale avait une ouverture de 4,4 pouce. Les angles ont été mesurés
partout à plusieurs reprises, en changeant la position du cerele de 15°. En 1889, le Lt-Colo-
nel Witkovsky a determine la latitude et l’azimut aux deux extrémités de la base de Molos-
kovitzy, ainsi que l’azimut au point A de la petite base de Poulkova. Les latitudes ont été
observées à l’aide d’un petit cercle vertical de Brauer, les azimuts avec un grand instrument
universel de Repsold. La latitude de Moloskovitzy a été déduite de six couples d’étoiles, celle
d'Osertilzy de douze couples. L’azimut a été déduil partout d’observations de Vétoile polaire,
à douze reprises de 15° en 15°, aux deux extrémités de la base de Moloskovitzy et à huit
reprises au point À à Poulkova. Le calcul du réseau de triangles depuis Poulkova jusqu’à
Halljal était fondé sur la base de Moloskovitzy et les nombres de Clarke, relativement à la
figure de la Terre. Les coordonnées polaires ont été calculées relativement au point À à Poul-
kova. Le calcul à donné les résultats suivants :

 

Latit. astronomique Latit. géodésique
PORN 8 cocon 59 46 16,00
Moloskowitage 28 20-020 JE cg ipgiqen Meter og ou
D 17,68 ag 29 10,01
Azimut a2 onomıle Azimut géodésique
Poutkova (My ree Alla | —
Moloskovitzy-Osertitzy . . . . 330 42 31.90 330 42 29,95
Osertitzy-Moloskoviizy ©. | | 5028 20 150 38 6,3

ge

PR

 
