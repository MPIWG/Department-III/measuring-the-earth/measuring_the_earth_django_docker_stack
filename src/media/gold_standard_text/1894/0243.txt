 

|
|

E
J
;

Lai put

Il

fet

Bring om || Vit wht

z
i

237

Azimut. La direction du couvent Donskoy, observée du clocher d’Iwan Weliki par
M. Miontchinsky en 1892, pour Vorientation de la triangulation de Moscou, est de

n

195 46 0,62 + 0,80

Le même azimut, observé en 1832 de l’autre point et réduit au clocher d’Ewan
Weliki, est de

WY

195 46 4,19

Cette différence de 3° 50 comprend la déviation de la verticale qui peut avoir lieu, car
la distance des points d'observation dépasse plusieurs kilomètres.

La latitude du clocher d’Iwan Weliki, d’après les observations de M. Iveronoff en
1899, qui a profité de l'instrument de passage en l’installant au premier vertical, est de

55 44 52,93 + 0,19

La même latitude, déterminée par feu le professeur Schweizer en 1845-46, est de

a 7 A 1

D0 44 93,20 Æ O4

b) En Crimée, le colonel Miontchinsky et M. Kortazzi ont déterminé les différences
de longitude : Théodosie-Soudak et Thédosie-Rostow. La première des longitudes repose sur
six nuits d'observations et l’autre sur huit. L’équation personnelle à été éliminée par la
méthode ordinaire: en outre, elle a été déterminée par les observations directes pendant
six nuits. Les corrections des chronomèêtres ont été déterminées par la méthode des hauteurs
correspondantes des étoiles. Les résultats des observations faites en 1892 et 1893 sont les
suivants :

 

 

 

 

 

 

ä Ban |
Latitudes Différences | Longitudes de Nicolaiew | Différences
astronomiques | géodésiq. astronomiques | géodésiq.
o if H 1 " WY o ! N I " "
Simféropol (eathédraleht #44) 44.57 46,5. SE AQ 38 02 7 2782 ay 30,4) + 236
Kekeneise (station de la poste). | 44.23 59,6 | 24 33,9 | +2 34,3 || 1 58 14,30 | 58 42,7 | — 1,6
Soudak (eglise) , 2. |. 44 50 95,4)-60 408002 16% 9 02277 0 7,8 | — 16,9

 

 

 

 

La détermination des longitudes faites en 1893 permet de fermer le polygone suivant:

ASSOCIATION GÉODÉSIQUE — 32

 
