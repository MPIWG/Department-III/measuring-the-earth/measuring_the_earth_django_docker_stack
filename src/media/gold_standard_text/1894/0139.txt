 

 

Sere RT Ye À

Lt en

im

Tier pen im

IK}

133

der letzten Ableitung in No. 322 des Astronomical Journal weniger störend hervor, so dass
man den von Chandler endgültig angenommenen Ausdruck (52), welcher von der Form:

Po = P + ri cos [A + (4 — T) 8] + r2 cos [A + © — G]

ausgeht und annimmt, dass die Grüssen 71, », 7, 8 und @ veränderlich sind und einer

66 jährigen Periode unterliegen, als eine brauchbare Näherung zur Berechnung der
Reduction der beobachteten Polhöhen auf den mittleren Werth derselben ansehen kann.

Diese Formel, deren specielle Zahlenwerthe für die Periode 1889—94:

ie 0 ri Vo G
1888 Oct. 31 0: 848 0:174 0:147 244
1889 Dec. 29 0-849 0-170 0-149 347
1891 Febr. 26 0-849 0-166 0.151 350°
1892 April 25 0-850 0-161 0-152 383°
2893 Juli 23 0-851 0-156 0-153 356°
1894 Aug. 20 0-851 0-152 0-154 358°

betragen, ist im Gegensatz zu den früheren Ableitungen Chandler’s nur auf die Europäischen
Beobachtungsreihen begründet, um nicht im Voraus in betreff der Bahn des Poles
allzu weitgehende Annahmen zu machen. Sie wird daher zunächst auch nur zur
Vergleichung mit den Resultaten der Europäischen Beobachtungen herangezogen werden
können, wird aber immerhin auch für die anderen Beobachtungsreihen erste Näherungen
ergeben.

Es stellt sich nun aber aus einer Vergleichung des Beobachtungsmateriales in
Anhang 2 mit den der Chandler’schen Formel (52) entsprechenden Zahlenwerthen heraus,
dass der obige numerische Werth des Coefficienten 7, für die gegenwärtige Zeit zu gross
angenommen worden ist. Die sämmtlichen Beobachtungsreihen erfordern eine Verkleinerung
dieses Coefficienten, welche für Kasan 56%, Pulkowa Wansch 24°, Kosrinsky (bis
Aug. 1893) 58 %*), Wien 97%, Prag 37°, Berlin 22°/.*), Potsdam 36°, Karlsruhe 55 °/,
Strassburg 17°/.*), Bethlehem (1. und 2. Reihe) 63°, Rockville 67°, San Franeisco 71°
und Honolulu 16°% des von Chandler angenommenen Zahlenwerthes beträgt.

Bei dieser Sachlage habe ich zu dem Auskunftsmittel gegriffen, zunächst in
Columne 6 für alle Beohachtungsreihen die Reduction auf die mittlere Polhöhe auf Grund
der Chandler’schen Formel (52) unter Verminderung des numerischen Werthes von rs auf ?|ı
des von Chandler angenommenen Betrages zu berechnen und diese Reductionen an die in

*) Die Ergebnisse der mit den leistungsfähigsten Instrumenten ausgeführten Beobachtungs-
reihen sind durch den Druck hervorgehoben.

 
