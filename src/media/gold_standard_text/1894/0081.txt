 

ee

bbb ab ud
ip aL Dil]

tae

LA a LL

Fil

 

45

« En procédant ainsi, on affirme expressément, dans la lettre de la Société de
Göttingen, aussi bien que dans le mémoire qui l’accompagne, qu'il ne s'agirait pas de prendre
à Innsbruck des résolutions formelles, réservées à pius tard, sur l’organisation de l’entreprise,
mais simplement de s’y procurer des informations réciproques et de préparer une entente
amicale sur les buts poursuivis. |

« Enfin, Messieurs les Secrétaires de la Société de Gôttingen nous prient de porter

[eur lettre et la notice ci-incluse à la connaissance des membres de la Commission per-

manenle.

« Nous le faisons d’autant plus volontiers que l’Association internationale s’occupe déjà
depuis nombre d’années, non seulement des déterminations de l’intensité de la pesanteur au
moyen des observations de pendule, mais — ce qui est tout aussi important — de l'étude des
anomalies constatées dans sa direction, au moyen des mesures de déviation de la verticale,
tout cela, il est vrai, essentiellement dans lintérêt de nos connaissances sur la figure du
Géoide, mais en ayant égard aussi — dans quelques pays du moins — à la structure géolo-
gique de la surface.

« En outre, pour rendre les mesures de la pesanteur, faites dans les différents pays
avec des appareils différents, aussi comparables que possible, l’Association géodésique,
d'accord avec le Comité international des Poids et Mesures, a décidé la création à Breteuil d’une
station centrale de comparaison de pendules, qu’on organise dans ce moment.

€ Donner aux observations de pendule une extension plus vaste et une répartition plus
systématique, et accorder à leurs rapports avec la configuration géologique des couches super-
ficielles une importance plus grande, ainsi que le proposent les Académies, ne pourra qu'être
favorable au progrès de la science. |

« De notre cöte, l’Associalion internationale peut facilement prêter les mains à déve-
lopper son programme dans cetle direction, puisque, la Convention du Métre devant être
renouvelée l'année prochaine, les travaux préparatoires pour élargir le nouveau programme,
p. ex. quant à l'étude des variations de latitude, figuraient déjà à Pordre du jour de la
session d’Innsbruck, avant que nous eussions connaissance de l’initiative, du reste très bien
venue, des Académies de Vienne et de Gottingen.

« En raison de ces circonstances et du fait que notre Association géodésique com-
prend déjà non seulement les États de l'Europe, à quelques exceptions près, mais aussi
les principaux pays d’Amérique, et le Japon a l’extrême Orient, l'opinion pourra être
défendue à Innsbruck, que le but envisagé par nos très honorés collègues des quatre
Académies serait atteint le plus facilement et pratiquement, si l’on donnait aux recher-
ches sur la pesanteur un plus grand développement au sein de l'Association, peut-être en les
remettant à une section spéciale, à laquelle on adjoindrait quelques géologues compétents.
De cette manière, on éviterait la création, qui pourrait rencontrer assez de difficultés, d’une
organisation spéciale et indépendante exclusivement pour les observations de pendule.

« Toutes ces considérations seront sans doute élucidées et les projets rapprochés
de leur solution par la rencontre proposée à Innsbruck avec les délégués des Académies.

Aussi, nous ne doutons pas d’être d'accord avec vous, Messieurs, en remerciant chaleureuse-

 
