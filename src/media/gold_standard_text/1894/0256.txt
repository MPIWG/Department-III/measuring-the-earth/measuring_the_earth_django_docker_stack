 

N
fé
bi
'
I
E
i
I.
|
ig
i

Procès-verbaux des séances de la

internationale, réunie à

Première séance. 5 septembre 1894

Liste des membres de la Commission per-
manente, des délégués, des représen-
tants du pays et de la ville d’Innsbruck,
ainsi que des invités oo

Discours d'ouverture prononcé par S. E.
le comte Merveldt, gouverneur du Ty-
rol et du Vorarlberg a

Paroles de bienvenue adressées a l’assem-
blée par M. le professeur Ehrendorfer
au nom de l’Université, et par M. le Dr
Mörz, SE de la ville d’Inns-
bEuek .. ... a

Réponse de M. Fe pré un) de |’Asso-
ciation géodésique oo .

Rapport du Secrétaire ee sur la
gestion du bureau de la Commission
permanente depuis la session de 1893 .

Rapport sur l’activité du Bureau central
depuis la Conférence de Genève, par M.
le professeur Helmert . :

Nomination d'une Commission de 5 mem-
bres, chargée de s'entendre avec les
délégués des Académies et Sociétés
scientifiques, réunis à Innsbruck, sur
l'extension à donner aux études de la
pesanteur . :

Rapport de la Chance is Tan
présenté par M. le professeur Foerster
(voir Annexe A. I, p. 121)

Nomination dela Commission des comptes

Deuxième séance, 7 septembre 1894

Liste des délégués des Académies et So-
ciétés scientifiques, invités à la séance

Lecture el adoption du a de
la première séance : a

Rapport deMM. Hirsch el Woe sur les
résolutions prises, ensuite d’entente
entre les délégués des Académies et de
la Commission permanente

250

Commission permanente de l'Association géodésique
Innsbruck du 5 au 42 Septembre 1894.

Pag.

65-89

65-66

66-67

67-68

68

69-76

76 88

88

89
89

90-97

90

90-91

91-92

 

Ces résolutions sont oes a l’una-
nimite . on

Premiere discussion sur a suction des
latitudes :

Rapport de M. le ee. Mpreoh sur
l’état actuel des recherches relatives
à la variabilité des latitudes {voir An-
nexe A. II, p. 131 et planches I et I).

Compte:rendu deM. le professeur Helmert,
d’un travail entrepris par M. A. Mar-
cuse sur le mouvement du pôle nord
de la Terre (voir Annexe A. Ill, p. 157,
et planche I)

M. le professeur Helmert Die une
comparaison des deux séries d’obser-
vations de Honolulu, exécutée par M.
A. Marcuse {voir Annexe A. V, p. 167,
et planche IV) HSE

Remarques de M. Fœrster sur la le
de la création d’un service special des
latitudes He :

M. Bakhuyzen communique Le |.
de ses calculs sur la variation des lati-
tudes et ee

M. Tisserand présente dieu remar-
ques sur la création d’un service inter-
national des latitudes a.

Reponse de M. Foerster a MM. Bakhuy zen
et Tisserand

Troisieme séance, |0 septembre 1894

M. le Président salue l’arrivée de MM.
von Kalmär et Bischoffsheim et lit une
lettre de M. Derrécagaia expliquant
l'absence de plusieurs délégués français

Lecture et adoption du proces: verbal de
la deuxième séance à

M. Helmert présente quelques remarques
au sujet de la communication faite par
M. Bakhuyzen dans la séance précé-
dente sur la variation des latitudes .

Pag.
92
92-97
92
93
93
93-94
94-96
96
96-97
98-110
98
98
98-99

a see

Dub ali Lada su dl au au.

casa it à OR

PPT es

 

 
