 

190

Hieraus folet :

Polhöhe des Aufstellungspunktes des Instrumentes : o =
Reduction auf das Centrum der Station

Polhohe des Uemipmma (er sialon. . . . 2. =

Es weicht dieses aus den I. Verticalbeobachtungen erhaltene Resultat der Polhöhe
von jenem aus Zenith-Distanzmessungen erhaltenen um nur 0 245 ab; die Uebereinstimmung

ist demnach zufriedenstellénd.

Vereinigt man beide Resultate mit Rücksicht auf die wahrscheinlichen Fehler zum

Mittel, so folgt:

50 47 35 650

+ 0,761

 

50 47 36'441 + 0'065

Polhöhe des Gentrums (Thurmaxe) der Station « Hoher Schneeberg » :

9 — 50° 47 36.468 + 0'069.
B) Azimut.

Auf der Station « Hoher Schneeberg » wurde das Azimut der Richtungen :
Schneeberg-Bösig und Hoher Schneeberg-Donnersberg gemessen, und zwar ersteres in

12 Sätzen zu je 4 Beobachtungen, letzteres in 8 Sätzen zu je 4 Beobachtungen.

Die Resultate für die einzelnen Sätze sind:
N.-O. Azimut der Richtung « Hoher Schneeberg-Bösig » :

1. Satz a
25 )
D. D
A)
D à
0.9
i >
S. D)
9.»
10 >
1 >
49, >) a

Mittel a
Reduction auf das Centrum der Station
Reduction auf das Centrum von Bosig
Convergenz der Meridiane

N.-O. Azimut der Richtung « Hoher Schneeberg-Bosig »
(Centrum der Stationen) .

 

N.-O. Azimut der Richtung « Hoher Schneeberg-Donnersberg » :

>

90,307
20,130
18,578

122 54 18.210

192° 5% 19/163
2190
£275 10.555
dz 60,554

o ¢. "
10250 477

H

Hoher

=
—
Lo
GO

0198.

u nun Bh

 
