 

Ri
be
I
ve

laborieux travail n’est pas encore entièrement terminé; mais, d’après le rapport présenté par
M. Du Pasquier à la Commission dans sa dernière session, il est déjà permis d'indiquer des
résultats approximatifs pour cing de ces stations. |

En adoptant, après de nombreuses expériences et calculs, la valeur 2,66 pour la
densité moyenne des roches qui forment les massifs des montagnes, et après s'être convaincu
que l’influence de l'inégale répartition des densités des masses superficielles est minime,
M. Du Pasquier trouve, pour les déviations théoriques de latitude causées par les attractions
des masses visibles, les valeurs suivantes qui, comparées aux dévialions observées, donnent
les différences inscrites à côté : |

 

Re. Deviations
Stations sae Différences
théoriques

 

WY

Tete-de-Ran. . 10,94 — 0.23

Chaumont . . 18,02 — (),17
Neuchatel... 15,43 + 0.10
Portalban— >. 4.78 + 0,52
Mid@es 00 4015 0,03 eat 43

 

 

Il en ressort pour les quatre premières stations, situées encore sous l'influence pré-
dominante du Jura, un accord remarquable des déviations théoriques et observées, puisque
les écarts restent dans les limites d'incertitude des observations et calculs. Pour la station de
Middes seule, l'écart plus fort paraît indiquer une attraction du massif alpin plus forte qu’elle
ne devrait l'être. Toutefois M. Du Pasquier ne croit pas qu’on puisse en conclure à une den-

sité moyenne du massif alpin plus grande que celle introduite dans le calcul, car en admeltant ~

pour cette densité un chiffre sûrement supérieur à ce que permet la structure eéologique
apparente, on ne parvient qu'à diminuer un peu l'écart sans le faire disparaître. On ne
pourra s’en rendre compte définitivement que lorsqu'on possédera encore les données pour
quelques autres stations voisines des Alpes.

En tout cas, ce premier essai d’une pareille étude détaillée et consciencieuse doil
nous encourager à continuer dans cette voie, surtout en tenant compte aussi des intensilés
de la pesanteur, mesurées au moyen du pendule.

Quant 4 ces derniéres, on a fait, dans le courant de celte année, des mesures relatives
au moyen du pendule de Sterneck, soigneusement vérifié et comparé à Munich; pour con-
naître l’heure et surtout la marche pendant la durée des observations, avec l’exactitude voulue,
la Commission vient d'acquérir un second chronomètre de marine enregistreur de Nardin,
du Locle, d’une rare perfection.

Je ne mentionne ici que quelques résultats interessants :

Si l’on déduit, par les observations comparatives faites à Munich, la longueur du
pendule simple et la valeur de la pesanteur pour Zurich, on trouve un accord très satisfaisant

Liu ha Lai dl aim. «

Tnt

 

 
