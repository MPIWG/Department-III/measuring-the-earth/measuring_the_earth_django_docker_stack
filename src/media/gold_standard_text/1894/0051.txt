 

Tes yy

|

LA men

ran

Weyer ae

CNT ni

mi CUITE

mereryi

 

AD
Breteuil ebenfalls die Verbindung mit der Sternwarte erhalten hat. Es ist dazu nur nöthig,
eine kurze Linie von einigen hundert Meter Länge zwischen dem Maass- und Gewichts-Bureau
in Breteuil and der Telegraphen-Station in Sèvres zu errichten. Das internationale Gomite
zweifelt nicht, bei der französischen Verwaltung auch zu diesem Zwecke die gewohnte Bereil-
willigkeit zu finden.

Der Herr Präsident schlägt vor, die Sitzung bis Nachmittag zu unterbrechen. Die-
selbe wird um 23/, Uhr wieder aufgenommen, und beginnt mit der Verlesung der Berichte
über die Arbeiten in den einzelnen Erdmessungsstaaten.

Der Herr Präsident ertheilt zuerst das Wort den Gommissaren von Oesterreich -
Ungarn:

a) Der Herr Professor Tinter verliest einen Bericht über die seiner Zeit von Professor
Heer gemachten Beobachtungen. (Siehe Beilage B. IA.)

b) Herr Professor‘ Weiss berichtet über die Thätigkeit des Gradmessungsbureau’s.
(Siehe Beilage B. IP.)

c) Herr von Kalmar, über die Schwere-Messungen, welche von den Offizieren der
österreichisch-ungarischen Marine, im Auftrage des k. u. k. Kriegs-Ministeriums ausgeführt
worden sind. (Siehe Beilage B. Ie.)

d) Herr Oberst-Lieutenant À. von Sterneck wird dem ständigen Seeretär zur rechten
Zeit den ausführlichen Bericht über die Arbeiten des militär-geographischen Instituts zu-
senden.

Einstweilen giebt Herr von Sterneck folgenden kurzen Auszug :

Im leitenden Personale des Institutes ist eine Veränderung eingetreten, indem Ilerr
Linienschiffs-Kapitän Ritter von Kalmar in Folge allerhochster Entschliessung zum Director
des hydrographischen Amtes von Pola ernannt wurde und demgemäss aus dem Verbande des
Institutes geschieden ist.

Die Untersuchung der Mess-Stangen des österreichischen Basis-Apparates ist laut
brieflicher Mittheilung des Herrn Directors Benoit bereits vollendet; es wird beantragt wer-
den, dass der Apparat wieder durch einen Offizier von Paris abgeholt und der Rücktranspor!
nach Wien überwacht werde.

Herr von Sterneck theilt sodann die verschiedenen Berichte über die Gradmessungs-
arbeiten des militär-geographischen Institutes mit, und zwar: a) über das Präcisions-Nivelle-
ment, b) über die astronomischen Arbeiten, von Herrn von Sterneck, c) über die trigono-
metrischen Arbeiten, von Herrn Hartl, und d) über die Schwerbestimmungen, von Herrn
von Sterneck. (Siehe Beilage B. 14.)

Bezüglich der astronomischen Arbeiten wird hervorgehoben, dass der aus sehr gulen
Bestimmungen sich ergebende Längenunterschied Däblie-Schneekoppe mit den Ergebnissen
der Ausgleichung des Herrn Bakhuyzen im Widerspruche steht. Es scheinen demnach dieser
Längenbestimmung, und vielleicht derartig ausgeführten Längenbestimmungen überhaupt,

Fehler systematischer Art anzuhaften.

 
