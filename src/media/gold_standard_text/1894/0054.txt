 

AS

 

Fehler einiger Grundlinien und der daraus abgeleiteten Nelz-Seiten. Er spricht dem Gentral-
bureau seinen Dank aus fur die Gefalligkeit, mit welcher dasselbe die ihm fehlenden An-
vaben verschafft hat. Er vertheilt diese Arbeit, sowie mehrere andere Publikationen der
italienischen Gradmessungs-Commission unter die Mitglieder der Versammlung. Diese Notiz

wird übrigens einen Theil des Berichtes über die Triangulationen bilden, welchen Herr
Ferrero der nächsten Generalconferenz vorlegen wird.

Herr Lorenzoni macht eine Mittheilung über die relativen Schwere-Bestimmungen,
sowie über seine Untersuchungen betreff des Mitsehwingens der Stalive, welche er, im Jahre
4893, in Padua, Mailand und Rom, mit Hilfe des Sterneck’schen Pendels ausgeführt hat.

(Siehe Beilage B. VII.)

Der Herr Präsident dankt den Herrn Delegirten für ihre interessanten Berichte und
schlägt vor die Reihe der Landes-Berichte jetzt za unterbrechen und die Frage der heutigen
Tages-Ordnung aufzunehmen, welche sich auf die für das Programm der Erdmessung, bei

Gelegenheit der Erneuerung der Uebereinkunft, vorzuschlagenden Aenderungen bezieht. Er

giebt zunächst dem ständigen Secretär das Wort.

Herr Hirsch schlägt vor eine Spezial-Commission von fünf Miteliedern zu ernennen,
öntwurf für einen Vorschlag auszuarbeiten hätte, welcher

welche ohne Verzug einen ersten E
alsdann vom Bureau sämmtlichen Mitgliedern der Permanenten Commission mitgetheilt

würde ; die letztere hätte schliesslich das definitive Projekt aufzustellen, welches der General-

Conferenz im nächsten Jahre zu unterbreiten ist.

Herr Ferrero drückt sich über diesen Gegenstand in foleender Weise aus:

« Im Jahre 1885, nach dem Tode des berühmten und hochverehrten Generals Baeyer,
wurde die Frage der Reorganisation der Gradmessung aufgeworfen. Es ist bekannt, dass,
angesichts des Grabes ihres Stifters, unsere Institution sich erweitert hat, indem sie einen
universellen Charakter erhielt, während sie bis dahin auf Europa beschränkt gewesen war.

« Darin liegt ein erfreulicher Beweis für die Fruchtbarkeit gewisser Schöpfungen,
welehe nicht nur ihre Gründer überleben, sondern mit der Zeit eine immer grössere Bedeu-
tung gewinnen.

« Eine Reihe von brieflichen Mittheilungen und Besprechungen zwischen einfluss-
reichen Mitgliedern der Gradmessung, sowie der Ideen-Austausch zwischen dem Vorstand
der Gradmessung und den Commissionen verschiedener Länder, haben dazu geführt, die
Grundlagen der jetzigen Vebereinkunft aufzustellen, welche im Jahre 1886 in Berlin zu
Stande kam.

« Unser College, Herr Foerster, erinnert sich ohne Zweifel
z\ entschwundenen von Helmholtz und mir ausgetauschten Corres-

berühmten und leider jet

pondenz über die
Preussischen Geodätischen Institut zu verbinden, zu des

Regierung unseren hervorragenden Collegen und Freun

sen Leitung die Königlich Preussische
d Herrn Helmert berufen hatte.

der damals zwischen dem .

Möglichkeit, das Central-Bureau der Gradmessung auch ferner mit dem |

id de

dote ai

au he ld Ail AA hab ae

shall ts Lab) deihesessidtt lien

fay pmmm za, au Mu LA nu un,

 

 
