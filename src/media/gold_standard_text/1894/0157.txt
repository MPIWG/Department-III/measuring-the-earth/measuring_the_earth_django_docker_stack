 

bt

Abweichung Reduction

Datum. Jahr. Polhöhe. Paare. von Mie Gin Ay

1890 Juli 16 1890.54 52 30 mas 96 0718 0 20 — 0102

| Aug. 10 61 17.49 a1) +o.16 —0.25 —0.09
| Sept. 5 68 17.56 40 0.23 —0.27 —0.04
ib Oct. 15 79 17.49 48 40.16 zo —0.05
i Noy. 18> 88 17.36 40 0.03 —0.09 —0.06
Dec. 26 98 17127 4I —0.06 +0.07 40.01

1891 Jan. 19 1891.05 17.04 56 —0.19 40.17 —0.02

Febr. ı5 13 17-05 162 —o.18 +0.24 —+-0.06

März 19 21 17.06 101 —0.27 +0.28 --o.or

April 18 30 17.18 155 —0.15 +0.25 —-o.10

Mai ı5 37 17.25 209 —0.08 40.17 0.09

Juni 14 45 177,331 21T —0.02 --0.06 —+0.04

Juli 18 LE 17,40 104 +o.15 —0.09 —+-0.06

Aug. 21 64 17.59 168 40.26 — 0.21 40.05

Sept. 16 71 271.04: 179 40.31 —0.26 +0.05

Oct 15 79 17.61 121 +o.28 —0.27 0.01

Nov. 9 86 17.60 75 40.27 —0.24 0.03

Dec. 9 94 17.66 98 —o.23 —0.14 —-0.09

1892 Jan. 10 1892.03 1747 40 +0.04 —0.01I 40.03

Febr. 16 13 En 1 155 — 0.16 —-o.14 — 0.02

März 17 27 17.08 223 —0.25 —o.23 — 0.02

April 14 29 17.06 164 — 0.27 —o.26 —0.01I

Mai ı8 38 17.08 297 —0.28 40.23 —0.05

Juli ıı 53 17.28 80 —0.05 --0.07 0.02

17.33 4529

Bis incl. April 1891 Monatsmittel abgeleitet aus den Tagesresultaten in No. 3010 und 3055 der
Astronomischen Nachrichten. Von da ab näherungsweise Monatsmittel abgeleitet aus den in No. 3131
der Astronomischen Nachrichten graphisch eingetragenen Mittelwerthen,

en em À

 

 

Potsdam. A= —13° 4
= . i ichung Re i
is Datum. Jahr. Polhôühe. Paare. 5 ee Ch. Bi Ap
1889 Jan. 10 1889.03 BoP 22 30.25 33 —0"03 40716 —+o0!13
= Febr. 13 12 56.24 53 —0.04 0.13 40.09
= März 13 20 50.22 7 —0.06 40.08 40.02
3 April 19 30 56.35 89 40.07 —0.02 +0.05
= Man us 27 66.315 229 —o.o7 — 0.09 —0.02
E Jung 17 46 56.43 238 40.15 —0.16 —0.01
F Jul 18 55 56.50 aS —-o.22 — 0.20 40.02
Aug. 19 63 56.46 167 40.18 —0.19 —0.0I
Sept. 10 69 56.41 84 +0.13 | —0.16 | —0.03
Oct u 78 56.28 79 G.00 À —0.08 —0.08
r Dec. 26 os | 56.02 58 —0.26 —o.17 —0.09
1890 Jan. 14 1890.04 56.02 150 — 0.26 0.21 —0.05
Febr. 13 12 55.98 137 —0.30 40.24 —0.06
März ı5 20 56.03 99 —0.25 +0.22 —0.03
J April 8 27 56.16 103 — 0,12 0.16 40.04
56.28 1671

Monatsmittel abgeleitet aus den Tagesresultaten in No. 3010 der Astronomischen Nachrichten,

CEERI LAON CU we TTT

NTM]

 
