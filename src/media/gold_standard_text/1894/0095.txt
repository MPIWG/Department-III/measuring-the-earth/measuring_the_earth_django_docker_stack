 

89

Cette commission est invitée à assister si possible à la réunion de demain des délé-
gués des Académies, d’y exposer la solution indiquée dans la circulaire du bureau, approuvée
par la Commission permanente, el de faire rapport au plus tôt. Elle est en outre autorisée à
inviter les délégués des Académies à assister aux séances de la Conférence.

M. le Président prie M. Fœrster de présenter le rapport de la Commission des lati-
tudes, nommée à Genève.

M. Ferster rend compte en détail des démarches faites par la Commission auprès de
la Société astronomique et du résultat obtenu. Il a d’abord fait circuler auprès de ses collé-
oues le projet d’une lettre dans lequel se trouve exposée lutilité d’un service spécial pour
l'observation des latitudes dans quatre stations réparties sous un même parallèle, à des diffé-
rences d'environ 90° de longitude. M. Schiaparelli s'étant déclaré pleinement d’accord avec
ce projet, tandis que M. Tisserand s’est opposé à l’organisation d’un service spécial, pour les
raisons qu’il avait déjà avancées à Genève, la lettre a été envoyée, au nom de la majorité de
la Commission, au bureau de la Société astronomique, avec prière d'y répondre le plus tôt
possible.

Le Président, M. Gyldén, a promis d’en nantir l’Assemblée de la Société astrono-
mique, qui devait se réunir au mois d'août à Utrecht. M. Fœrster a assisté à cette Assemblée,
tandis que M. Tisserand, membre aussi du Comité central, a été empêché de s’y rendre.

Après délibération, il a été décidé à Utrecht que le Comité central de la Société astro-
nomique se prononce en faveur de Putilité de l’organisation d’un service régulier pour la
détermination des changements de position de l’axe terrestre ; 1l se déclare en outre prêt à se
prononcer, si on le désire, sur Pélaboration d’un tel projet, mais il décline une coopération
continue aux travaux de calcul et aux publications.

M. Foerster termine son rapport, qui parailra in-extenso dans les Comptes-Rendus,
en proposant que la Commission permanente autorise son bureau à élaborer, avec laide du
jureau central, pour la Conférence générale de l’année prochaine, un projet d'articles à
ajouter à la Convention internationale, par lesquels on assurerait le fonctionnement d’un ser-
vice spécial des latitudes pour la durée de la nouvelle Convention. (Voir Annexe A. I.)

La discussion sur ce sujet est renvoyée à la prochaine séance.

Le bureau propose de nommer également aujourd’hui la Commission des comptes,
afin qu’elle puisse dès maintenant s'occuper de l’examen de la comptabilité du Bureau central,
contenue dans le rapport de M. Helmert. Le bureau croit indiqué de confirmer la Commis-
sion de l’année dernière, en y remplaçant M. von Kalmär absent, par M. von Zachariæ.

Gette proposition est adoptée et MM. Foerster, Ferrero et von Zachari sont priés de
présenter leur rapport dans une prochaine réunion.

La seconde séance est fixée au vendredi 7 septembre, à 2 heures.

La séance est levée à 4!/, heures.

ASSOCIATION GEODÉSIQUE — 1?

 
