 

Annexe B. V2.

FRANCE

Rapport sur les travaux exécutés par le Service Géographique de France
(oetobre 1893 — septembre 1894).

Les travaux exécutés par le Service Géographique de France, depuis la réunion de
Genève, comprennent :

- A. — EN GEODESIE:

4° La suite des opérations entreprises en 1893 pour reviser le réseau des Alpes-
Maritimes ; les observations ont été conduites cetle année jusqu’a la station de l’Enchastraye.

90 L'établissement, en Algérie, d’une chaine de premier ordre, entre Biskra et
Laghouat, pour former le troisième et dernier segment du parallèle du Sud Algérien; les
opérations ont été exécutées par M. le Capitaine. de Fonlongue.
: Le parallèle du Sud-Algérien, dans son entier développement, s'étend de la fron-
tière du Maroc jusqu'à Gabès et embrasse 8 degrés de longitude à la latitude moyenne de 54
degrés.

Par l’achèvenient de ce réseau, la triangulation primordiale de l'Algérie et de la
Tunisie qui comprend deux chaînes parallèles et quatre chaînes méridiennes, dans le sys-
tème dit à gril, se trouve définitivement terminée. Nous avons déjà fait connaître que l’en-

semble de ces chaines s’appuie sur huit bases ; de ces huit bases, trois ont été déjà détermi-.

nées ; les cinq autres seront incessamment mesurées.

3° La triangulation des hauts plateaux algériens entre les parallèles du nord et du
sud et les chaînes méridiennes de Laghouat et de Biskra par MM. les capitaines Dumézil et
Meunier.

 

abaisse itunes cata

soil u dl runs sore

 
