 

 

D

DT

Hs

À 0 2 an pq LL à fie HiT

logiques et sont seuls employés dans les nivellements. La peinture à Vhuile correspond aussi
bien aux besoins pratiques qu’à la nécessité de rendre le bois insensible aux influences

extérieures.
« Jai l'honneur de présenter à Votre Excellence l’expression de ma plus haute con-
sidération.
« (Signé) HELMERT. »
INSTITUT PHYSICO-TECHNIQUE Charlottenbourg, le 30 novembre 1893.

DE L’ EMPIRE

« À l’Institut géodésique de Potsdam.

€ J'ai l'honneur de répondre à votre lettre du 9 courant, que je ne crois pas pouvoir
entreprendre dans l’Institut physico-technique l’étude de la question de l'influence de la tem-
perature et de l'humidité sur la longueur des mires de nivellement en bois de sapin ou de
pin, et cela pour les raisons suivantes :

« 1° Vu l'incertitude de la détermination de la température et de l'humidité dans une
matière aussi peu favorable que le bois, on n’est nullement assuré d'arriver, par des expé-
riences, à des résultats quelque peu certains. Le travail de M. le colonel Goulier ne pourrait
pas être cité en faveur d’une telle opinion, puisque le rapport sur ce travail, que j'ai sous les
yeux, ne contient pas les détails nécessaires pour qu’on puisse le soumettre à une critique
serrée.

« 20 Même dans le cas le plus favorable, le résultat présenterait toujours un certain
caractère spécial qui empêcherait d’en généraliser les conclusions. La preuve évidente en est
fournie par l’indication contenue dans le rapport de M. von Kalmar (p. 103), d’aprés laquelle
les mires employées dans les Pays-Bas auraient été rendues, par leur peinture à lhuile,
presque insensibles à l'humidité, de sorte que les petits changements dans leur longueur s’ex-
pliqueraient presque complètement par Peffet de la température. Mais, même en faisant abs-
traction de cela, les résultats n’auraient toujours qu’une valeur purement théorique. Gar ils ne
pourraient pas servir à corriger après coup les anciens nivellements, puisque les observations
météorologiques, nécessaires pour établir l’état momentané des mires, font défaut; et pour
les nivellements futurs il parait au moins risqué d'apporter des corrections calculées d’après
l’état momentané des mires, dont la connaissance est sans doute bien plus incertaine dans
leur emploi en campagne que dans les expériences de laboratoire où lon peut s’entourer bien
mieux de toutes les précautions. En aucun cas on ne pourrait ainsi se dispenser des compa-
raisons fréquentes des mires en bois avec des étalons métalliques, comparaisons sans doute
très compliquées et exigeant beaucoup de temps, mais qui permettent d'éliminer, dans la
mesure voulue, les influences perturbatrices de la température et de l'humidité.

« Cette opinion parait être partagée aussi par le colonel Goulier, puisque ses longues
et patientes recherches Pont conduit à la conclusion que, pour les nivellements de précision,
il serait indispensable de se servir de mires dites de compensation.

 
