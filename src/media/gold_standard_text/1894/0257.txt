 

UM AM

it

i
18
=
z
E
Ë

Discussion A ce sujet entre MM. Bak-
huyzen, Foerster et Hirsch .

Observations et propositions faites par M.
Ferrero concernant Ja question des la-
titudes . 0.

Apres une discussion a laquelle prennent
part MM. Faye, Bakhuyzen, Ferrero,
Foerster, Helmert, Hirsch et Tisse-
rand, les propositions de M. Ferrero
sont adoptées à l'unanimité

Communication de M. Hirsch sur la sta-
tion centrale de pendule à installer à
Breteuil Die.

Rapports sur les travaux exécutés en Au-
triche-Hongrie, présentés par MM.
Tinter, Weiss, von Kalmär et von
Sterneck (voir Annexes B. M:-Id, p.
187-204) :

Rapport sur les travaux dans le Grand-
Duché de Bade, par M. le professeur
Haid (voir Annexe B. II, page 205)

Rapport sur les travaux belges, par M. le
colonel Hennequin (voir Annexe B. FFF.
D. 200) . :

Rapport sur les travaux en Danemark, par
M. le Celvon Zachariae (voir Annexe
BLY, p. 207)

Rapport de M. le général Derrécagaix sur
les travaux du service géographique de
l’armée et de M. Ch. Lallemand sur le
Nivellement général de la France (voir
Annexes B. V2 et Vb, p. 210 et 212)

M. von Sterneck rend compte des travaux
exécutés en Grèce sous la direction de
M. le colonel Hart! {voir Annexe B. VI.
p. 214)  .

Remarque de M. Fœrster sur les cala-
logues des étoiles fondamentales du
Berliner-Jahrbuch

Rapports sur lestravaux en Italie, par M.
le general Ferrero et M. Lorenzoni
(voir Annexes B. Vlla et cartes V, Vlet
VI, et VII, p. 215 et 219)

M. Ferrero présente son travail sur les
erreurs moyennes de quelques bases
mesurées et des côtés correspondants
du réseau

100-101

101-103:

103

103-105

105

105

105-106

106

106

291

 

 

Discussion concernant le renouvellemen!
de la Convention geodesique .

La proposition de MM. Hirsch et Ferrero
d'instituer une Commission spéciale
chargée de préparer un projet des chan-
gements à apporter à la Convention,
est adopiée el MM. Bassot, Ferrero,
Forster, Hirsch et von Kalmar sont
désignés pour en faire partie .

A la nouvelle de la mort de M. von Helm-
holtz, l'assemblée décide l'envoi à Ber-
lin d’un télégramme de condoléance

h

Quatriéme séance, 12septembre 1894

Lecture et adoption du procès-verbal de
la troisième séance . ve

La proposition de M. Ferrero de charger
le bureau de représenter lAssociation
internationale à la fête séculaire du gé-
néral Bæyer est adoptée

Observations de M. Zallemand au sujet
de la lettre de M. von Helmholtz sur les
recherches concernant les mires de ni-
vellement in

Remarques de MM. Hirsch, Helmert et
Bakhuyzen sur le méme sujet.

Rapport de la Commission des comptes ct
finances présenté par M. Fœrster

Après discussion, les comptes de 1893 sont
approuvés et les crédits proposés sont
adoptés : ee

Rapport sur les travaux exécutés cans les
Pays-Bas, par M. le professeur Schols
(voir Annexe B. VIII, p. 225)

M. Bakhuyzen communique une notice
de M. van Diesen sur le zéro interna-
tional des altitudes (voir Annexe A. VI,
D 172.

Rapports de M. le professeur Helmert sur
les travaux de Institut géodésique
prussien et de M. le colonel von Schmidt
sur ceux de la Landes-Aufnahme (voir
Annexes DB. IXa et IXD, p. 227 et 234).

M. le professeur Foerster desire que les
observations de latitude faites a Hono-
lulu soient publiées complètement etque
les géodésiens et les asironomes veuil-

ge

Pag.

106-109

109

110

111-118

aaa

114-143
143

114-145
115

116

116

116

 
