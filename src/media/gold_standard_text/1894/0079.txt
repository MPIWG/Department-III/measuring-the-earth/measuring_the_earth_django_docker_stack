 

B
if

pement

nin

EU Ny

RN ms | | Lit à pr TE

dd trier 1

1 jf)

 

73

sion, sans difficulté et même avec empressement ; loutefois, M. van de Sande Bakhuyzen, après
s’y être d’abord opposé, a fini par se déclarer favorable à la demande de M. Fœrster, attendu
que l'avance de 3000 M. sera certainement remboursée après quelque temps, si l'Association
n’achete pas l'instrument.

« En conséquence, le 11 juin, M. le Président a autorisé M. le Directeur du Bureau
central a verser a M. le professeur Foerster la somme de 3000 M. sur les fonds disponibles

de l’Association géodésique internationale, à titre d’avance pour la construction d’une lunette

zénithale de Wanschaff, avec appareil photographique, et à condition que cette somme serai
remboursée plus tard à la caisse du Bureau central, dans le cas où l’Association géodésique
ne ferait pas l'acquisition du dit instrument pour son propre compte.

€ IT parait que l’habile artiste, auquel a été confiée la construction de l'instrument,
n’a pas pu le terminer assez tôt pour qu’il fût possible jusqu’à ce moment de l’experimenter.

« Le Secrétaire donne enfin les renseignements nécessaires sur une question dont la
Conférence aura à s'occuper :

«Au commencement de juillet, M. le Président a reçu de la Société royale des sciences
de Gôttingen la lettre suivante :

SOCIÉTÉ ROYALE DES SCIENCES « Göttingen, le 30 juin 1894.
A GŒTTINGEN

« Monsieur Faye, Président de la Commission permanente de l’ Association géodésique

« inlernationale, à Paris.
« Monsieur le President,

€ Les Académies de Munich et de Vienne, ainsi que les Sociétés des sciences de Göt-
tingen et de Leipzig, qui depuis une année se sont entendues pour coopérer à des entreprises
scientifiques générales, nous ont chargés de vous donner connaissance de certains projels qui
ont été soumis par l'Académie de Vienne à la Conférence des délégués des Académies, réunis
les 15 et 16 mai dernier à Gôttingen.

€ Il s’agit de l'étude des rapports entre les intensités de la pesanteur à la surface de
la Terre et la structure géologique de l'écorce terrestre, rapports qui ont été rendus si
probables par les remarquables travaux de MM. Defforges et von Sterneck.

€ Nous avons la conviction que la solution d’un problème aussi vaste et compliqué
ne sera possible que par les efforts réunis des nations, et nous serions heureux de provoquer
une initiative dans celle direction. Nous savons également que le but indiqué ne saurait être
atteint sans l’appui de l'Association géodésique internationale, et il nous importe par consé-
quent, avant tout, d'entrer en relation personnelle avec Messieurs les membres de la Commission
permanente géodésique.

« Animes de ce désir, nous avons résolu de convoquer pour le 5 septembre à Inns-
bruck une Conférence des délégués des Académies réunies, dans l'espoir de trouver ainsi
l’occasion d'arriver à une entente avec Messieurs les membres de la Commission permanente,

ASSOCIATION GEODESIQUE — 10

 
