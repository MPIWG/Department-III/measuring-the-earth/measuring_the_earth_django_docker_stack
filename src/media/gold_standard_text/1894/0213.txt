 

 

 

Vii del

ee

Pe 1) RHYL

LF HRT

Beilage B. IV.

DANEMARK

Bericht über die geodätischen Arbeiten im Jahre 1894.

1. TRIGONOMETRISCHE STATIONEN

Die in früheren Berichten erwähnte Revision der !rigonometrischen Stationen ist
fortgesetzt worden und so weit gebracht, dass sie voraussichtlich nächstes Jahr beendet
sein wird.”

Im dritten Bande der Dänischen Gradmessung findet sich S. 402-409 eine Note über
die Bewegung der Dreieckspunkte, welche S. 407 eine Zusammenstellung von drei verschiede-
nen Messungen der Winkel des Dreiecks Troldemosebanks-Dyret-Ejersbaonehöj enthält. Diese
Messungen sind beziehungsweise in den Jahren 1847, 1866 und 1867 ausgeführt. Während die
beiden letzteren mit einander in guter Uebereinstimmung sind, weichen sie von den Ergeb-
nissen des Jahres 1847 so viel ab, dass man, in Betracht der era der Granitpostamente,
auf welchen die Punkte be zeichnel sind, En der Genauigkeit der Beobachtungen, die Er-
scheinung nur durch Bewegung des ade hat erklären können. Obgleich eine in 1884-85
ausgeführte Neumessung der betreffenden Winkel keine merkbare e Abweichung von den Er;eb-
nissen des Jahres 1867 zeigte, hat man es doch für richtig gehalten, die Messung im laufenden
Jahre zu wiederholen. Wie es aus der umstehenden Zusammenstellung hervo rgeht, stimmen
die Resultate von 1894 vorzüglich mit denen von 1867 und es darf als höchst schen L

bezeichnet werden, dass die Stationen sich seit 1867 nich! geändert haben.

 
