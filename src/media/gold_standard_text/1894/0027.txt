 

rn AT

my nee >

ere

PORTE Wee) CUTE pepe Ce

I Trier

a

24

erade und von der Feuchtigkeit abhängt. Ueber die Art der Abhängigkeit bestehen aber
gewisse Zweifel, welche durch eine sehr eingehende Untersuchung des französischen Obersten
Goulier, die in Brüssel mitgetheilt wurde, noch vermehrt worden sind. Während man näm-
ich als Ausdehnungskoeffizient « des Tannenholzes bisher 0,000004 annahm, fand Goulier
bei konstantem Feuchtigkeitsgehalt im Mittel 0,000009, ausserdem aber eine Veränderlichkeit
von « mit der relativen Feuchtigkeit, wonach bei trockener Luft und Sättigung « etwa gleich
0,000005 ist, bei mittlerem Feuchtigkeitsgehalte aber 0,000012.

« Ausserdem fand Goulier die Langenanderung bei konstanter Temperatur ungefahr
proportional der relativen Feuchtigkeit, so lange die Sattigung < ?/, bleibt; bei grosserer Sat-
tigung zeigte sich keine Aenderung mehr. Diese Ergebnisse erklaren aber die im Sommer
beobachteten Zunahmen der Lattenlangen nicht, weil die relative Feuchtigkeit im Laufe der
Campagne zwar schwankt, aber nicht im Grossen und Ganzen zunimmt. Die Temperatur-
zunahme kann aber allein auch nicht zur Erklarung dienen. Man muss vielmehr nach Oertels
Ergebnissen bei dem bayrischen Nivellement annehmen, dass die Lattenlänge mit der absoluten
Feuchtigkeit der Luft wächst.

« Der Antrag des Herrn von Kalmar lautet nun:

« In Anbetracht des in diesem Bericht (Goulier) erwähnten, zwischen 35 und 9° pro
Meter variirenden Ausdehnungs-Coefficienten von Nadelholz für 1° C. und des Umstandes,
dass auch eine Veranderlichkeit dieses Ausdehnungs-Coefficienten mit der Veranderung der
Luftfeuchtigkeit gefunden wurde; ferner zur Constatirung, ob die in Folge Luftfeuchtigkeit
statthabende Ausdehnung des Nadelholzes proportional dem relativen Feuchtigkeitsgehalte der
Luft in Procenten, oder dem absoluten Dunstdrucke ist, wird das Gentralbureau angewiesen,
eingehende Studien dieser für die im Gebrauche befindlichen Nivellirlatten sehr wichtigen Ver-
änderlichkeiten entweder selbst ausführen zu lassen, oder die Ausführung zu veranlassen. »

« Die Versammlung nahm einstimmig diesen Antrag an, wobei man allerdings gleich
unter der Hand die Hoffnung aussprach, dass die Ew. Excellenz unterstellte Reichsanstalt
sich der Untersuchung annehmen möchte. Wie aus dem Bericht über Gouliers Arbeiten,
sowie aus einer in den «Annalen der Physik und Chemie», 1888, Neue Folge Band 34,
S. 364 u. f. enthaltenen, hierher gehörigen Arbeit von Hildebrand hervorgeht, ist die Unter-
suchung nicht ohne Schwierigkeiten, zu deren Ueberwindung Einrichtungen gehören, die
wohl ein physikalisches Laboratorium bietet, welche aber im geodätischen Institut mangeln.
Da nun beim gegenwärtigen Stand der Sache auch ein rein physikalisches Interesse vorliegt,
hoffe ich um so mehr, dass Ew. Excellenz die Reichsanstalt mit der Untersuchung derselben
betrauen werden.

« Ich beehre mich, ein Exemplar der Brüsseler Verhandlungen, sowie die Oertel’sche
Arbeit beizufügen.

«In den Brüsseler Verhandlungen findet sich S. 100 der Antrag des Herrn von Kal-
mar. 8. 165 u. f. sein Nivellementsbericht vergl. besonders ($. 176 u. f.), endlich S. 664 der
Bericht von Goulier. |

« Bei Oertel ist S. 6 u. f., sowie Tafel I zu vergleichen.

« Es sei mir gestattet, darauf hinzuweisen, dass die Untersuchungen auf Stäbe von

 

 
