 

en

setae Mh i Ahn eka EN

ET YET WER REP He

Immer

af

quelle ces formules se vérifient lorsqu’on les applique, par voie d’extrapolation, aux observa-
tions dont elles ne sont pas déduites.

Il ne lui semble donc pas justifié de soutenir actuellement, avec quelque proba-
bilité, qu'il existe des formules dont on puisse, en extrapolant pour des époques un peu
étendues, déduire les mouvements du pôle sans avoir recours à des observations ultérieures
continues. Ce sont précisément les séries d'observations parfaitement homogènes et continues,

que nous proposons d'organiser, qui seules pourront résoudre cette question. Mais avant

tout, ces observations devront décider sur l'existence de mouvements progressifs, bien que
plus ou moins irréguliers du pôle, mouvements que MM. de Helmholtz et Schiaparelli envisagent
comme nullement invraisemblables, et dont on ne pourrait pas, de longtemps, établir la
preuve, en raison de l’incertitude de nos connaissances sur les mouvements des éloiles, si
l’on ne recourt pas à des séries d'observations telles que nous les proposons.

Pour ces raisons, M. Foerster estime que non seulement on peut, en toute bonne
conscience, recommander l’organisation internationale d’un service special pour l'étude des
mouvements de l'axe terrestre, mais qu’on y est obligé, dans Pintérêt de l’économie et de la
productivité du travail scientifique.

Quant aux craintes exprimées par M. Tisserand, M. Foerster désire faire observer
que les ressources nécessaires pour une semblable organisation ne seraient en tout cas
demandées aux budgets des États contractants qu'à partir de 1897, et que les Gouvernements
n'auraient à prendre une décision définitive sur cette question qu’en 1896. Jusqu’alors, les
astronomes français seront amenés, par leurs propres observations, à abandonner toute espèce
de doute sur l'évidence fournie, aujourd’hui déjà, par plus de trente mille observations de
latitude, exécutées dans douze observaltoires.

En général, on peut se demander s’il ne serait pas opportun d'obtenir d’abord, en
principe, l'extension du programme de l'Association géodésique, dans le sens des nouveaux
problèmes dont on lui demande de poursuivre la solution, y compris les propositions con-
cernant la géologie et la physique du globe, et ensuite seulement de demander les crédits
nécessaires, au fur et à mesure des besoins.

M. Hirsch croit qu'après toutes ces intéressantes communications il ne reste pas suf-
fisamment de temps pour liquider aujourd’hui ce sujet et qu’il sera De not nécessaire
d'y revenir dans une des prochaines séances.

M. Ferrero est du même avis, d'autant plus que la question du service international
des latitudes fait nécessairement partie du sujet plus général des modifications du programme
à proposer lors du renouvellement de la Convention, sujet qu’on a voulu mettre déjà à l’ordre
du jour de la troisième séance.

La Commission permanente ayant partagé cet avis, M. le Président consulte PAs-
semblée sur le jour et l'heure de la prochaine séance, qui, sur le désir de la majorité, est
fixée au lundi 10 septembre à 10 heures.

La séance est levée à 5 heures et quart.
: ASSOCIATION GÉODÉSIQUE — 13

 
