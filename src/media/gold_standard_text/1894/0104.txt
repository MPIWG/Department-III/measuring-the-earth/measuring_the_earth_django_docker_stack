 

TROISIEME SEANCE

Lundi, 10 septembre 1894.

Présidence de M. H. Faye.
Sont présents :

1. Les membres de la Commission permanente : NM. Ferrero, Forster, Helmert,
Hennequin, Hirsch, von Kalmar, van de Sande Bakhuyzen, von Zacharie.

IH. Les delegues : MM. Albrecht, d’Arrillaga, Guarducci, Haid, Karlinski, Lalle-
mand, Lorenzoni, Rajna, von Schmidt, Schols, von Sterneck, Tinter, Tisserand, Weiss.

Ill. Les invites : MM. Bischoffsheim, Pattenhausen, Tripet.
La séance est ouverte à 10 !/, heures.

M. le Président souhaite la bienvenue à M. von Kalmär et à M. Bischoffsheim, qui ont
tenu à prendre part au moins aux dernières séances de la Gonlerence.

Il lit ensuite une lettre qu'il a reçue de M. le général Derrécagaix, qui explique
l'absence de plusieurs délégués français : M. le colonel Bassot est retenu chez lui pour des
raisons de santé; M. le général Derrécagaix et M. le commandant Deflorges devant assister
aux grandes manœuvres qui ont lieu en septembre, ne peuvent pas prendre part cette
année aux travaux de la Conférence géodésique.

Le Secrétaire fait lecture, en allemand, du procès-verbal de la deuxième séance, qui
est ensuite adopté à l'unanimité.

M. le Président donne la parole à M. le professeur Helmert, qui désire présenter
quelques remarques sur la communication faite par M. van de Sande Bakhuyzen dans la
dernière séance.

M. Helmert estime que les résultats de la recherche de M. van de Sande Bakhuyzen
ne se rapporte qu'à des valeurs moyennes. De pareilles formules à deux termes ne peuvent

lh is

astra tamed es ne Mu da

LM bi

48 bed de 0

 

 
