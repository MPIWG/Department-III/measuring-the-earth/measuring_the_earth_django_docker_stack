 

 

a ee men

CE PME erent

ete

merite

D OO PAT UT TT OE

159

getheilt. Zur Ableitung der Ao aus den Beobachtungen wurden sogenannte Normaleurven für
die Breitenänderungen auf jeder Station benutzt, welche den mittleren Verlauf der btobach-
teten Polhöhe darstellen. Aus diesen Gurven wurden sowohl für den Anfangspunkt der Zeit
als auch für die übrigen in Monatsintervallen abstehenden Zeitpunkte die entsprechenden
Werthe der Polhöhe direet entnommen. Auf diese Weise wurden die Ao, gewissermassen
Normalwerthe darstellend, freier von den zufälligen Fehlerquellen und weniger abhängig von
gewissen systematischen Störungen, welche an bestimmten Tagen z. B. durch Refractions-
anomalien einzelne Tageswerthe der Polhöhe erfahrungsgemäss entstellt haben könnten.
Hierbei sind die Gewichte der Ag sowohl für die einzelnen Stationen als auch für die ver-
schiedenen Zeilpunkte gleich angenommen worden.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Iste Näherung IIte Näheruns
|| |) | i
STATION Kasan Strassburg | Bethlehem Peer der | Kasan Strassburg | Bethlehem |coordinaten der|f
ou or we Poleurve in |
Re erster Näherung | zweiter Näher. ||
; 4 4 4 | De | a 4 ee Be
Epoche Ao! (0,01) |} Awf (0,01) Ao’ (0,01) || a! | y! | Ao” (0,01) | Ao” (0,04) | Ao” 0,01) | # y
| |
Br R. B. Re B Rule i | B. Bar IB, BR, B. TR || ii HM
1892 Nov. 1 |-— 2 \ Bj+ 44 4)/— 4— 4/4 0,03) 4002) 04 3/4 24 4)/— 5 — 4/4 0,03) + 0,02 |
Dec. 1 |+ 7+ 6)/— 3— 3/—18 ~13/4+ 6/4 2/45+6— 2— 3-14 13/4 7° 6/4 121)
1898 Jan 1 DE Seb 6 nn -2 91 — ool dl 2. ZA. 0 22,8 ee À
Febr. 1 0— 21-8 -— 41-28 -—27\— 24 8-2 —- 3-95 —25)-27 -27|— 3|+ 3
Me see Oe OOO) Oe man 42 | — 10 9 33 34 30 30 || 9|+ 42
Apnil dol 16:46 58.40): 098 — 28 ||—-=16 1 44 45) 48 © ig og 840 | eo > ig a
Mai 1 | —18 —18 40 41 24 25 || 18 | + 42 20 18 39 — 41} —25 — 24 — 18)4+ 421
Juni 4 18 22 42 42 15 19 || 22 891 — 20 — 24 | — 44 — 417) — 19 I | 2ij+ 351
Juli 4 He 50) er) a ao, | i 23) 3
Aug. dy) 18 2 20.) a5. 2 34 6 — 8] 2|-=26|2 2 al 2 5 7 7| 2) 2
Sept.1 |— 17 —16|— 15 —20!)— 1—-1/— 16[+ 1211-19 —16|—17 —19|— 2 O— 16|+ 11
Oct, el er
Novo t (nou ei ee ee PE ene eee ee “he
Dec 4 Vth 6h 6 6 ee leg Baa, 1G!) 1
1894 Jan. 1 Adel eg ee — iain 8 || 3 4 9 8 B= = 3 || — 4 a ra.
Febr. i a ee 3 dl a ee ed |
März 4 Ae ees 240 5 RO EST ES we Gl 1
Aprild | + 3 — 5 21 15 > {4 | 53/+ 4174 1 61— 20 — 13 | — 6 —10| — G|+ 16 |
Mai 1 |— 2 — 6] — 23 — 204 12 15 || 6 23 | 4 7 22 20 | 13 —14| — 7\+ 2
um 1-8 6125, 25.255. 20), 62, So NS een
Jul A 14 14 26 24 21 20 || 11 | + de 16 -- 12 | — 25 — 28 er 22 — Meer 12|+ 29
i | | |

 

 

 

 

Die Coordinaten der Polcurve in erster Näherung, welche in vorstehender Tabelle
unter @, y' angegeben sind, wurden durch Auflösung,des obigen Gleichungssystems A), zu-
nächst unter Vernachlässigung der Nullpunktscorrectionen v®, erhalten. Aus diesen à’, y’
wurden für alle Stationen die jeweiligen Ag’ einzeln berechnet und neben den aus den Breiten-
messungen selbst abgeleiteten, unter B stehenden, Ag’ unter der Columne R aufgeführt. Die
Differenzen B—R ergaben dann die folgenden Zahlenwerthe in erster Näherung linker Hand :

 
