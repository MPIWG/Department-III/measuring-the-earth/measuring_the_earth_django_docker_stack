BL LR Fm TILT

 
 

 

 

5

 

ii

NUT AR (HN

ii

“=
RE
m
=
=

Rapport sur les travaux du Bureau central depuis la Conférence de Fribourg.

Messieurs,

Dans la derniére Conférence, le Bureau central a eu ’honneur de vous présenter les
résultats des observations de latitude faites, sur la demande de la Commission permanente,
à Berlin, Potsdam, Prague et Strasbourg, du 1°" janvier 1889 a la fin d’avril 1890. Les eir-
constances ne permettaient pas alors de dresser un tableau détaillé de toutes les conditions
et particularités concernant ces observations. Cependant les résultats obtenus ont paru si
importants ä la Commission permanente que le Bureau central fut chargé de lui présenter
dans l'espace de trois mois un rapport détaillé sur ces observations. À ma demande, M. le
professeur Albrecht se chargea de ce travail d ‘apres des points de vue établis d’un commun
accord entre lui et nous. Ge rapport fut autographié et parut à la fin de 1890 sous le titre:
« Résultats provisoires des séries d'observations à Berlin, Potsdam et Prague concernant la
variabilité des latitudes ». Tôt après, le n° 3010 des Astr. Nachr. en publia un extrait par
le même auteur.

Je crois pouvoir admettre que ce travail démontre suffisamment la qualité et l’exac-
titude des trois séries d'observations de Berlin, Potsdam et Prague.

Il montre en particulier très clairement qu’on ne doit redouter que fort peu les
influences locales des réfractions. C’est à cette circonstance et aux indications venues de
différents côtés sur les variations apparentes des latitudes et leurs causes, qu'il faut sans
doute attribuer le fait qu'aucune objection n’a été produite contre lPenvoi définitif d’une
expédition à Honolulu, qui avait été projetée à Fribourg, comme moyen approprié pour
reconnaître la cause des variations remarquées. Deux notices importantes de notre honorable
collègue M. Schiaparelli, sur ce sujet, ont été envoyées par le Bureau central à MM. les
membres de la Commission permanente le 25 janvier 1891. Elles seront fort utiles pour
l’organisalion des recherches ultérieures.

Quant à la possibilité qu’Honolulu, a cause du voisinage de volcans et du caractère
géologique de ces régions, serait exposé à de plus fortes varialions locales qui en feraient
un lieu d'observation peu convenable pour ces recherches, une étude sérieuse de tous les
ouvrages sur la question, par M. le Dr Marcuse, et un simple examen théorique de ma
part ont, à mon avis, dissipé toutes les craintes à ce sujet. Ge résultat a élé porté à la con-
naissance de ces Messieurs par une circulaire du Bureau central, datée du 24 janvier 1891.

Conformément aux décisions prises à Fribourg, je pus, aussitôt après la Conférence tenue
en cette ville, exprimer au Directeur du Coast and Geodetic Survey des États-Unis de l’Amé-
rique du Nord, M. Mendenball, le désir de la Commission permanente d’obtenir la coopera-
lion qui m'avait déjà été promise pour l'expédition d'Honolulu et pour les recherches sur
les latitudes en général. Par une lettre du 17 décembre 1890, que j'ai fait connaitre le 2
janvier à Messieurs les délégués de l'Association géodésique, M. Mendenhall me fit savoir
que la coopération à l’expédition d’Honolulu était assurée, grâce à la complaisance des admi-
