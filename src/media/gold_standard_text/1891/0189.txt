 

AOL ANT DRE RH UT

HALLE

OV NN LE RN mir

om qe

 

Annexe B. V2.

FRANCE

Rapport sur les travaux géodésiques executés par le Service géeographique
de l’armée (Section de géodésie), Octobre 1890-Octobre 1891.

4. TRAVAUX DE FRANCE

Le Service géographique a exécuté, cette année, à l'extrémité sud de la nouvelle Meri-
dienne de France, une série d'opérations qui sont destinées à fournir des vérilications aux
mesures effectuées sur cet important réseau. Ges opérations comprennent la mesure d’une
base, la détermination astronomique des trois coordonnées en une station et la détermination
de l'intensité de la pesanteur.

A) Base de vérification.

On sait que la nouvelle Méridienne s'appuie sur trois bases, celles de Paris, de Per.
pignan et de Cassel. La base de Paris, qui sert de base fondamentale, a déjà été déterminée
en 4890 : celle année, on a mesuré celle de Perpignan.

La base de Perpignan est précisément celle que Delambre a mesurée en 1799
avec l'appareil de Borda; a ce litre, les opérations qui viennent d’être exéculées présen-
sent un intérêt particulier, et elles donnent satisfaction au vœu souvent exprimé par le
Comité international des Poids et Mesures, de voir répéter une mesure, faite avec un système
de règles dérivant de la toise du Pérou, au moyen d’une règle étalonnée sur le metre ın-
ternational.

Les termes ont été établis par Méchain, puis successivement découverts par Goraboent
en 4825, et par Perrier en 1870, pour y appuyer, l’un la chaine des Pyrénées, l'autre la
nouvelle Méridienne : à chacune de ces opérations ils ont été restaurés, mais sans que les
repères primitifs fussent déplacés, ni même ébranlés : ils ont été retrouvés, en 1891, en par-
fait état de conservation.

 
