 

Rapport de la Commission des finances,
presente par M. erster

Les propositions essentielles de ce enn
sont adoptées après discussion.

M. Andonowits fournit quelques explica-
tions relatives aux travaux géodésiques
préparatoires exécutés en Serbie .

M. Ferrero invite, au nom de M. le prof.
Meucci, les membres de la Conférence à
visiter la collection des instruments de
Galilée

Troisième séance, 13 octobre 1891 .

Lecture et adoption du procès-verbal de la
deuxième séançg, après une observation
faite par M. Derrécagaix er

M. Betocchi annonce qu’il est N de re-
tourner a Rome oe 8

M. Schiaparelli, qui assiste a I séance,
se déclare accord avec les projets au
sujet de l'étude des variations de latitude
et remercie ses collègues de l’avoir dési-
gné comme membre de la Commission
spéciale ie

Commencement des rapports des date
sur les travaux exécutés dans leurs pays

Rapports de l’Institut militaire géographl-
que de Vienne, lus par le Secrétaire :

19 Sur les nivellements de précision, par M.
von Kalmar; 2° Sur les travaux astro-
nomiques, par M. von Sterneck ; 30 Sur
les triangulations, par M. Hartl; 40 Sur
les déterminations de la pesanteur, par
N vom Sterneck. (Mow Annexe BL Ip.
160-167). Re HOUR HUE à

Rapport de M. le colonel Hennequin sur
les travaux exécutés en Belgique. (Voir
Annexe B. II, p. 171-176)

Rapport de M. le colonel Zachariæ sur les
travaux géodésiques danois en 1894. (Voir
Annexe B. Ill, p. 477-478)

Rapport de Moi “dees Au ape sur ies
travaux géodésiques exécutés en 1891
par l'Institut géographique et statistique
d’Espagne. (Voir Annexe B. IV, p. 179-
ROO 0

Rapport de M. le:
les travaux géodésiques

en
général Derrécagaix sur
du Service géo-

30

3A

NO
bo

 

graphique de l’armee française. (Voir
Annexe B. Va, p. 181-186).

Etude de M. le Commandant Defforges sur
l'influence du glissement dans la rotation
du couteau sur la durée d’oscillation du
pendule réversible. IR Aunexe x I
p. 154-159). ae

M. Defforges propose la création, aux fae
de PAssociation géodésique, d’une station
normale de pesanteur, au Bureau inter-
national des poids et mesures, à Breteuil.

M. Hirsch communique les principaux ré-
sultats d’une étude de M. le D' Benoît,
Directeur du Bureau international des
poids et mesures, sur la comparaison des
principales toises géodésiques. La publi-
cation de cette étude comme Annexe
aux Comptes-Rendus est décidée. (Voir
Annexe A. If, p. 140-447) :

Observations de M. le colonel à il
soumet deux propositions à l'examen de
la Commission permanente . :

Rapport de M. Lallemand sur les travaux
du Nivellement général de la France en
1894 (Voir Annexe B. Vb, p. 187-189)

Invitation de M. le Marquis Torrigiani de
visiter la manufacture de porcelaine de
Doc 000... =e

Quatrieme seance. 15 octobre 1891

Lecture du proces-verbal de la troisieme
séance, qui est adopté Are

Rapport de M. le général Ferrero sur les
travaux exécutés en ftalie. {Voir Annexe
B. VII, p. 194-493) ‘ :

Rapport de M. Schols sur les De er
Pays-Bas en 1891. . oir Annexe B. VIE,
p. 194) :

Communication de M. van re i ae oe
huysen sur les travaux géodésiques exé-
cutés par les Hollandais dans les îles de
Java et de Sumatra. (Voir Annexes B.
VIII et B. VILLE, p. 195-210)

Rapport de M. le professeur Helmert sur
ies travaux de l’Institut géodésique prus-
sien. (Voir Annexe B° IX2, p. 211-212).

Rapport de M. le colonel Morsbach sur les
travaux de la Landesaufnabme de Prusse
en 4894 (voir Annexe B. IX), p. 213-216)

Pag.

31

31-32

32-33

33-35

di

38-39

39

40

  

 

 

aren eC Ce MN. du did ul

al lt

Lie a Hails i 1

dau do du |

 
