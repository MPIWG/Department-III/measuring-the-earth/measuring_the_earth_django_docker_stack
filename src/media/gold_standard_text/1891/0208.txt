 

a UES

Ingenieuren beforderten Herren Soeters und Woldringh wurden durch die Herren W. G.
Tennissen! und A. de Vletter besetzt.

Als ich mich, in Folge meiner Zurückberufung nach Utrecht, Ende August 1875
nach Holland einschiffte, hatte der Ingenieur Soeters eben mit der Messung der Basıs ın
Demak angefangen, und später wurde von ihm noch die Basismessung in Ost-Java geleitet.

Von. der Basis-Messung in West-Java, bei dem Dorfe Simplak, ist schon in der zwei-
ten Abtheilung des obengenannten Berichts (Haag, M. Nyhoff, 1879) Kenntniss gegeben ?. Die
dritte Abtheilung, welche neulich abgedruckt und theilweise schon verbreitet worden Ist,
erhält, ausser einigen Ergänzungen zu den beiden ersten Abtheilungen, die genaue Bestimmung
des Verhältnisses zwischen dem Normalmeter und dem Metre des Archives, das Basisnelz
von Simplak, die Basismessungen bei Logantong (Nittel-Java) und bei Tangsil (Ost-Java) und
die beiden dazu gehörenden Basisnetze.

Bei dem jetzigen Zustande der Geodesie heisst das Anwenden einer Triangulation für
Gradmessung nicht, wie früher, ausschliesslich das Bestreben die Länge eines Breiten- oder
Längengrades abzuleiten, sondern die Abweichungen zu studiren, welehe übrigbleiben, wenn
eine mittlere ellipsoidische Fläche den Berechnungen zu Grunde gelegt wird. Die Breiten-,
Azimut- und Längenunterschied-Bestimmungen sollen dazu so viel möglich vervielfältigt
werden.

üs war erst verordnet worden, dass an sieben verschiedenen Meridianen auf einem
nördlichen und auf einem südlichen Punkte Breitenbestimmungen stattzufinden hätten, später
aber, els der verstorbene Ingenieur Metzger einen grossen Unterschied zwischen einer geodäti-
schen und einer astronomischen Polhöhen-Differenz zweier einander nahe liegender Punkte
gefunden halte, verdoppelte ich diese Anzahl. Wo die Breite bestimmt wurde, sollte auch
immer zu gleicher Zeit eine Azimutbestimmung stattfinden. Die genaue Bestimmung der
Längen- unterschiede wurden bis zum Ende verschoben.

Leider ist das Programm, welches ich auf Bitte des Admirals, Chefs des Marine-
Departements, F. L. Geerling, vor meiner Abfahrt nach Holland ausarbeitete, weil das ganze
Personal nach und nach wegen Krankheit um Urlaub einzukommen genöthigt war (siehe
3. Abth., S. 126 und 127,) nicht ganz erledigt worden. Doch sind zahlreiche Breiten- und
Azimutbestimmungen ausgeführt und meistens schon auf Java mit den Sternörtern aus dem
Nautical Almanac reducirt worden; auch verschiedene, sowohl von den späteren Ingenieuren,
als auch von Herrn de Lange, welche ausserhalb des Programmes lagen. Ueberhaupt wurde
schon währen! der Terrainbeobachtungen auf Java gefunden, «dass die astronomischen Azi-
mutbestimmungen bis auf wenige Secunden mit den geodätischen stimmten.

Als ich, auf die Bitte des Ministeriums der Kolonien, die definitive Reduction der Trian-

2 Lieutenant bei der topographischen Aufnahme.

» Die erste Abtheilung war wenige Tage vor meiner Abreise nach Holland, 25. August 1875, abge-
druckt; von der zweiten Abtheilung war das Manuscript vollendet. Nachher wurde ich vom Ministerium der
Kolonien beauftragt, den Druck desselben hier in Holland zu besorgen ; nnd einige Jahre später, als das Personal
der Triangulation nahezu ganz ausgestorben und der geographische Dienst aufgehoben worden war, wurde ich
ersucht, die definitive Berechnung der Triangulation von Java zu übernehmen.

wll

{dl

1) ed (a Aa Mt ch |

Li Ladi

Jal ni |

il

its, und pis na bb a

ihe

 

 
