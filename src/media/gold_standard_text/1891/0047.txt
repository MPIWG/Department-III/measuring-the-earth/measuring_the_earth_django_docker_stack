 

IN Am

my ec iPr A 2 TE

i

 

 

39

sont contenus dans deux mémoires publiés en 1875 et 1879 et dans un troisième qui vient
de paraître et que les honorés collègues ont reçu probablement ces derniers jours.

Ce troisième mémoire contient entre autres la discussion des deux dernières mesures
de base et la comparaison exacte du mêtre normal avec le mêtre des Archives.

Au reste, la réduction de toutes les observations astronomiques est terminée, de
même que la compensation du réseau. Hl aurait fallu trop de temps pour venir à bout d’une
compensation compléte de tout le réseau avec ses trois bases, ce qui aurait conduit à environ
245 équations normales. M. Oudemans a préféré employer une compensation partielle et la
preuve qu'il a réussi ressort de la faible valeur des corrections qu'il a fallu ajouter aux lon-
gueurs mesurées des trois bases afin d'arriver à un accord complet. Ces corrections sont de
9, 13 et 4 unités de la septième décimale dans les logarithmes des longueurs.

Afin de donner une idée de l'exactitude de la mesure des angles, M. Oudemans a
calculé, conformément à la proposition faite par M. le général Ferrero, l'erreur moyenne
d’une direction d’après les écarts qui existent dans la fermeture des triangles. Il a trouvé
pour cette erreur moyenne + 0.81.

Afin de se procurer un raccordement avec la triangulation à faire ultérieurement
sur lile de Sumatra, on a rattaché deux points de Sumatra au canevas de Pile de Java.

La triangulation de Sumatra a été commencée en 1883 et poursuivie régulièrement
avec un personnel composé de cinq officiers, douze sous-officiers et un certain nombre de
malais comme aides et heliotropistes. La triangulation entreprise dans le même but que
celle de Java comprend, outre la triangulation primaire, aussi des triangulations secondaires.
Pour la triangulation primaire, on a employé dès l’origine des instruments et des méthodes
d'observation propres à donner l'exactitude nécessaire pour la mesure des degrés, de sorte
que dans la suite on aura, à côté des degrés mesurés dans le Pérou, une seconde triangula-
tion exécutée sous l'équateur.

Pour le moment, on donne une courte notice sur l’organisation de cette triangu-
lation, qu’on pourra joindre aux Comptes-Rendus de cette Conférence; à l’avenir on espère
pouvoir donner chaque année un aperçu des progrès de cette entreprise.

(Voir Travaux de triangulation de Java, Annexe B. VIII> et Travaux exécutés sur
Pile de Sumatra, Annexe B. VIII¢.)

. Sur Vinvitation de M. le Président, M. le Prof. Helmert lit le Rapport sur les travaux
exécutés l’année dernière par l’Institut géodésique prussien, qu’il demande la permission de
compléter jusqu'à la fin de l’année pour la publication des Comptes-Rendus. Son Rapport
constate entre autres l’achèévement des recherches sur les déviations de la verticale dans la
region du Harz, qui permettent maintenant d’essayer la construction de cette partie du
geolde.

Il rend compte en outre de quelques observations destinées aux déterminations de
la différence de longitude entre Berlin et Potsdam, ot: la méthode d’observer les passages
des étoiles au moyen du micromeétre de Repsold a donné les meilleurs résultats, en faisant
disparaître presque entièrement les équations personnelles. (Voir Annexe B. IX2.)
