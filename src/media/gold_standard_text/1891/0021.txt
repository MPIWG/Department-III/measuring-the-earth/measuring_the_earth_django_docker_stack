 

| A di dE

ILE M

 

=
15
=
=
m
an
iz

 

13

l’'empèchent de se rendre à la Conférence de la Commission permanente dont il fait partie.
M. le général Arbser ajoute dans sa lettre que les rapports sur les travaux géodésiques,
exécutés en Autriche pendant l’année dernière, seront envoyés à temps pour être commu-
niqués à la Conférence.

M. Hirsch fait remarquer qu'aucun autre délégué de PAutriche-Hongrie n’a annoncé
son arrivée, de sorte que, pour la première fois depuis l’existence de l'Association, l'empire
d'Autriche-Hongrie n’est pas représenté dans la Conférence.

Enfin, le Secrétaire annonce que M. le général Stebnitzhki s'est excusé d’être empêché
de venir à Florence, et qu'il lui a envoyé son rapport qui sera lu dans une des séances
ultérieures.

La Commission permanente procède ensuite à l’élection du Président. Comme M. von
Kalmar a transmis par lettre son droit de vote 4 M. le général Ferrero, neuf bulletins sont
délivrés et rentrés; leur dépouillement par le bureau donne le résultat suivant :

M. Faye obtient 8 voix.
M. van de Sande Bakhuyzen 1 voix.

En conséquence, M. Faye est nommé président de lu Commission permanente.

Il prend place au fauteuil présidentiel et prononce les paroles suivantes !
« Messieurs et chers collègues,

«Je suis infiniment honoré de la présidence que vous avez bien voulu me conlérer.
Je ferai tous mes efforts pour me rendre digne de votre confiance, et pour ne pas rester au-
dessous de mes prédécesseurs parmi lesquels je dois citer l’illustre général Baeyer, le londa-
teur de notre Association, et M. le général Ibañez. De ce dernier on ne peut oublier qu'il a
donné un grand essor à la géodésie espagnole et qu'il à contribué, avec les officiers français
et le général Perrier, à la jonction de l'Afrique à PEspagne au moyen des plus grands
triangles qui aient jamais été mesurés.

« Profitant du privilège qui m'est conféré par nos règlements, de designer le vice-
président, je prie M. le général Ferrero d’accepter cette honorable fonction. Je me fais un
grand plaisir de rendre hommage à l’homme qui a présidé avec tant de succes a la géodésie
italienne et qui a fermé l'immense polygone géodésique qui entoure le bassin occidental de
la Méditerranée, en jetant des triangles italiens par dessus la mer depuis la Sicile jusqu'à
la Tunisie. »

Le bureau ayant été chargé de nommer la Commission des comptes et des finances,
il la compose de MM. Frerster, van de Sande Bakhuyzen et von Zachariae. M. le President
prie la Commission de présenter son rapport aussitôt que possible.

I fixe la prochaine réunion au samedi 40 octobre à 2 heures.

La séance est levée à 9 heures.
