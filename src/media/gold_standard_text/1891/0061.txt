 

HE TT

na An Iran

Wifi

ik
=
=
=
Im
im
=
=

 

 

Oo

staunenswerth. Anfanglich schien es sich nur um Gradmessungen zu handeln; bald aber
erweiterte sich der Kreis Ihrer Arbeiten, Längen- und Breiten-Bestimmungen, die Höhen-
messungen und die Bestimmung der mittleren Meeresoberfläche traten hinzu, mittelst des
Pendels haben Sie an zahlreichen Punkten die Intensität der Schwerkraft bestimmt. So hat
sich Ihr Arbeitsfeld steis erweitert. Sie haben enge Bande geknüpft nicht nur mit der Astro-
nomie, Ihrem natürlichen Verbündeten, sondern auch mit der Geographie, der Geologie und
selbst der Physik; denn die Direktoren der physikalischen Laboratorien wünschen die
Resultate Ihrer Untersuchungen über Richtung und Intensität der Schwerkraft zu kennen,
um der Genauigkeit ihrer Experimente sicher zu sein.

« Sie haben ebenfalls die Bestrebungen für Vereinheitlichung der Maasse und Gewichte
hervorgerufen und den Gedanken einer Einheitszeit oder Weltzeit angeregt. Ihre Wissen-
schaft hat direkte Anwendungen gefunden in den grossen Kanalbauten, bei Erstellung von
Verkehrslinien und bei der Schifffahrt. Sie ist also einer der wichtigsten Zweige des mensch-
lichen Wissens geworden, welches die Gesetze der Natur zu erkennen sich bestrebt.

« Angesichts solcher Thatsachen, scheint es kaum erlaubt zu behaupten, dass diese
Forschungen für die litterarischen und moralischen Wissenschaften bedeutungslos oder
unnütz seien. Ist wohl anzunehmen, dass die Erforschung der Erde für die Kenntniss des
Menschen, welcher dieselbe bewohnt, werthlos sei? Hat die experimentale Methode ihren
wichtigen Einfluss nicht auch auf die philosophischen Wissenschaften fühlbar gemacht, indem
sie dieselben zwang in der geschichtlichen Methode einen weit sicherern und wissenschaft-
licheren Führer zu suchen? Sind die Anatomie, die Physiologie des Gehirns nicht die Grund-
lagen einer neuen Psychologie geworden ?

« Andrerseits, wären die schönen Wissenschaften, die Poesie, die Philosophie, welche
alle ebenfalls von der Natur beeinflusst werden, welche die Seele erheben, den Erfindungs-
geist anregen und die Einbildungskraft leiten, für die exakten und Naturwissenschaften etwa
hinderlich oder gar werthlos? Können die Gelehrten etwa den Erfindungssinn, den Scharf-
sinn, die Inspiration entbehren ?

« Desshalb, meine Herren, sind wir in Wirklichkeit weder Besiegte noch Sieger,
sondern vielmehr Verbündete, welche auf verschiedenen Wegen nach demselben Ziele, nach
der Wahrheit streben. Bald stehen die einen, bald die andern voran, stets aber schreiten
sie vereinigt vorwärts.

« Darum danke ich Ihnen als Minister für die Ehre, welche Sie uns dadurch erwiesen,
dass Sie Italien und speziell Florenz zum Versammlungsort wählten. Mögen Ihre Berathungen
von neuen Erfolgen gekrönt werden !

« Ihre Entdeckungen auf dem Gebiete der Naturgesetze werden uns auch förderlich

sein, die Geselze des Geistes zu ergründen, welcher ja nicht als ausserhalb der Natur zu
denken ist. »

Herr Oberst, Ritter Luigi Dainelli, Vice-Bürgermeister von Florenz, richtet folgende
Begrüssungsworte an die Conferenz :
