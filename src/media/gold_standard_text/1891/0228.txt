 

220

Deux chaines méridiennes, l’une occidentale, allant d'Alexandrowsk à Orel, et l'autre
orientale, entre Sarepla et Saratow, relient les parallèles de 52° et de 47 5.

Toutes les observations concernant Pare du parallèle de 475, ainsi que le calcul des
opérations astronomiques sont achevées. Quant aux travaux géodésiques, je suis à mème de
constater que le calcul de compensation du réseau est aussi terminé. I ne reste ainsi qu'à
calculer les longueurs des lignes géodésiques, les projeter sur le parallèle et, enfin, à pré-
parer définitivement la publication de ce travail.

6. OBSERVATIONS FAITES AVEC LE PENDULE A REVERSION DE REPSOLD

appartenant a la Societe impériale russe de Géographie, exécutées par M. Alexis Socoloff,
astronome à Poulkow«.

 

 

 

 

 

 

se | —— m m
| : SR Fae AR?
| | M NN 4. LE | = 2.1] DIMFÉRENCR, PaULKOvA | ns
| NOM = = HART de = = — licux d'observations, DIRERERENCE | PUR
inane ogoeraphi- de = de là longueur du pendule jobservée du
ler = Den se réduite au niveau de la mer | pendule
des statious. Que Greenwich. = a „„ obs. —cale.|
| [Ones ae | Öbservee. | Calculée. | à secondes.
N 5 i De cor nea Ss 2 Y mm N mm ne orn |
1883 | Varsoye = 22 13.0 1 247 109,4 | + 0,6443 | + 00414 | + 0,0029 70,994 194
| n bob 39 500 5227205021 | — 0,600, + 0.092. 0. 6
| 1890 | Moscou. yo 20 2 30) | / 0 ale), = 03320 | = Ons ee ace
= Samara . 5) 3202 89,1.) 0,520 0 5 0,5509 2001007 0.002 292
D  Ocenbure . ol 19 09 >47 108.3 | + 0,694 | + 0,6822 | +- 0,0112 | 0,994 125

 

 

 

 

 

Toules ces stations se trouvent le long du grand reseau Lrigonometrique du parallele
de 52°, el sont en méme temps les points astronomiques de ce réseau.

Opal
Hike N
et le calcul de la différence de la longueur du pendule à secondes, d’après la formule
L = 0990918 |1 +- 0.005310 Sin? BJ. (Voir Helmert, Die mathem. und phys. Theorieen
der höheren Geodäsie.)

Les longueurs observées du pendule à secondes sont calculées, en attribuant à celle
du pendule à Poulkowa, d'après le général Stebnitzki, la valeur O° 9948384.

La réduction au niveau de la mer est faite d'après la formule A L =

J. STEBNITZKI, Lieutenant-General.

au ea MMT MM GF TI. =

uk toll

DL

juan dad A idl pay

sk

 

 
