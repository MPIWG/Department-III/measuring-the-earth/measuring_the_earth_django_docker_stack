 

DIA AN IB

IDE QU

À AN D AR Fe TL

hi

 

 

Beilage B. IX.

PREUSSEN

Bericht des Kel. Preussischen Geodätischen Instituts

Das Institut hat im Sommer d. J. (1891) die Lothabweichungsbestimmungen im
Harze vorläufig durch Aufnahme von 6 a und Azimutstationen und 5 Stationen fiir
Polhöhe allein abgeschlossen. Es wird nunmehr der Versuch gemacht werden, für den Harz
und seine Umgebung aus den Lothabweichungen das Geoid zu konstruiren.

Ferner wurden auf 2 Stationen im centralen Theile von Norddeutschland die Polhöhe
und Azimut gemessen.

Endlich sind Beobachtungen zur Ermittelung des Längenunterschiedes zwischen der
Berliner Sternwarte und dem Neubau des Geodätischen Instituts, der im Frühjahr 1892 be-
zogen werden wird, angestellt worden, wobei zwei Passageninstrumente zur Anwendung
gelangten, die mit beweglichem Mikrometerfaden zur Beobachtung der Sternpassagen nach
Repsolds Methode versehen waren. Von dieser Methode erwarten wir das Beste. Nicht nur
hatten neue Beobachtungsreihen im Frühjahr bereits das Verschwinden der persönlichen Glei-
chung bei vier Beobachtern bis auf + 0°01 angezeigt, sondern es lassen die Zeitbestimmun-
sen auch erkennen, dass der Einste stlungste hler jetzt nicht grösser ist, als der Beobachtungs-
fehler bei der alten Methode. Von der Längenbestimmung selbst liegen allerdings zur Zeit
Ergebnisse noch nicht vor. Es mag aber bemerkt werden, dass ftir Kontrollen verschiedener
Art gesoret ist. Die Instrumente een an den 21 Beobachtungsabenden zwei Mal, Beob-
achter wirkten vier an Zahl. Endlich ergiebt sich eine Schlusskontrolle für das Fee ie,
indem der geodätische Werth des Längenunterschiedes und die Lothabweichungen bereits
anderweit ermittelt sind.

Der Mareograph in Swinemünde, welcher vor mehreren Jahren bei einem Brande
verloren ging, ist nunmehr in vorzüglicher Weise durch einen Universal-Apparat ersetzt, der
nach Prinzipien von Prof. Seibt durch den Mechaniker Fuess erbaut worden ist. Ueber die
Einrichtung dieses Apparates verbreitet sich eine Schrift des Prof. Seibt, die den Herren
Delegirten in diesen Tagen übersandt werden wird.

 
