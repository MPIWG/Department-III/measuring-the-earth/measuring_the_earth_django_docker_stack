 

ERSTE SITZUNG

Donnerstag den 8. Oktober 1891.

Die Sitzung wird um 2 Uhr 15 Minuten im « Saale der Zweihundert » im « Palazzo

Vecchio » eröffnet.

 

DEN

IDUL NT

On

eT Ta MRR PRIVEE WET TT aT ft TI

i

Anwesend sind :
I. Die Mitglieder der Permanenten Commission :

Herr Prof. R. Helmert, Divektor des Köniel. Preuss. Geodätischen Institutes und des
Gentralburean’s der Internationalen Erdmessung, in Berlin.

Herr Prof. Dr. Ad. Hirsch, Direktor der Sternwarte, in Neuenburg, ständiger Sekretär.

Herr H. G. van de Sande-Bakhuyzen, Direktor der Sternwarte in Leyden.

Herr Hf. Faye, Mitglied der Akademie der Wissenschaften und Präsident des Längen-
bureau’s, in Paris.

Herr General-Lieutenant A. Ferrero, Direktor des militär-geographischen Instituts, in
Florenz.

Herr Prof. Dr. W. Feerster, Direktor der Sternwarte, in Berlin.

Herr Oberst Hennequin, Direktor des Militir-Kartographischen Instituts, in Brüssel.

Herr Oberst von Zachariae, Direktor der Geodätischen Arbeiten, in Aarhus (Dänemark).

ll. Die Abgeordneten :

Herr F. de P. Arrillaga, Generaldirektor des Geographischen und statistischen Instituts,
in Madrid. |

Herr Oberst-Lieutenant Bassot, Chef der geodälischen Sektion im geographischen Amt
der Armee, und Correspondirendes Mitglieder des Langenbureau’s, in Paris.

Herr Prof. A. Betocchi, Sektions-Prasident des Generalraths im Ministerium der 6ffent-
lichen Arbeiten, in Rom.

Herr Bouquet de la Grye, Mitglied der Akademie der Wissenschaften und des Langen-
bureau's und Chefingenieur-Hydrograph der Marine, in Paris.

Herr Constantin Carusso, aus Athen.

ASSOC. GEOD. INTERN. — 7

 
