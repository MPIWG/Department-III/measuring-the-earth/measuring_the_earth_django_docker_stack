 

 

230

 

Protokolle der Permanenten Commission der internationalen Erdmessung, vereinigt in Flo-

renz vom 8. bis 47. October 1894

Erste Sitzung, 8. October 1891

Verzeichniss der anwesenden Mitglieder der
Gommission, der Abgeordneten und der
Eingeladenen on.

Eröffnungsrede S. E. des Herrn Unter-
richtsministers Villari a

Begrüssungsrede des Herrn Oberst Luigi
Dainelli, Vice-Biirgermeister yon Flo-
renz . ie he:

Ansprache des Herrn General Z'errero,
Präsidenten der italiän. geodät. Commis-
sion . ehren:

Dankrede des Herrn Professor Hirsch .

Necrolog des verstorbenen Präsidenten.
General Ibanez, Marquis von Mulha-
cen, vom ständigen Sekretär (siehe An-
nexe A..I, p. 4101-409).

Herr d'Arrillaga fügt der verlesenen Lob-
rede einige Worte hinzu

Geschäftliche Mittheilungen des Sekretärs

Circular an die Regierungen betreff Er-
höhung auf 12 der Mitgliederzahl der
Permanenten Commission.

Zustimmung der Regierungen a

Kinladungsschreiben des Sekretärs an die
zwei neuen Mitglieder Davidson und
Hennequin. Ce es

Die Herren Davidson und General Steb-
nitzki entschuldigen brieflich ihre Ab-
wesenheit und Generalmajor Arbser zeigt
an, dass Herr von Kalmar durch Arbei-
ten in Oesterreich verhindert sei, der
Permanenten Commissions-Versammlung
beizuwohnen ee

Wahl des Herrn Faye zum Präsidenten

Anrede des Herrn Faye, welcher Herrn
General Ferrero zum Vice-Präsidenten
bezeichnet oe

Wahl der Finanz- und Rechnungs-Com-
mission durch das Bureau

Pag.

49-60

49-51

92-53

35
56-57

58

58-59

39-60

60

60

 

ee ei len a

Zweite Sitzung, 10. October 1891

Verlesung und Genehmigung des Proto-
kolls der ersten Sitzung oo.

Herr v. Arrillaga entschuldigt die Abwe-
senheit des Herrn Merino aus Gesund-
heitsrücksichten

Der Präsident übermittelt die Einladung
des Herrn Dr. Giovannozzi zum Besuche
des Ximenischen Observatoriums

Bericht des Centralbureau’s von seinem Di-
rektor Herrn Helmert

Liste der vom Gentralbureau versandten
Erdmessungs-Publicationen

Besprechung dieses Berichtes

Ernennung einer Spezial-Commission für
die Frage der Breitenänderungen

Bemerkungen des Herrn Oberst Bassot

Herr Defforges wünscht dass die Commis-
sion die Anschaffung einer Normalstange
aus Platin-Iridium für die Vergleichungen
der. Basis-Stangen, und die Gründung
einer Normal-Station für Pendelmessun-
sen im Auge behalte pe

Erwiderungen der Herren Færster, Hirsch
und Ferrero

Bericht der Finanz - Commission, von

Herrn Foerster ee
Die wesentlichen Anträge dieses Berichtes
werden besprochen und angenommen
Herr Andonowits macht Mittheilungen
über die geodätischen Vorarbeiten in

Serbien .

Herr Z’errero theilt die Einladung des Herrn
Prof. Meucci an die Mitglieder der Con-
ferenz mil, die Sammlung der Galilei-
schen Instrumente zu besuchen

Dritte Sitzung, 13. October 1891

Verlesung des Protokolls der zweiten
Sitzung, welches nach einer Bemerkung

47-97

Pag.
61-76

61

 

rd al ER hha il Halal Li | dl ul

Las 1 ul i 1

(FAT TRI

ll

Mu ho

i. si aa pada

Yi

 
