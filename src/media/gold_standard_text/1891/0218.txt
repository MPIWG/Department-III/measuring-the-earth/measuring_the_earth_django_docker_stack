 

210

für Vermessungswesen, Band VID. Sur les stations de deuxième et de troisième ordre, on à
pris des tours d'horizon. L'altitude des stations est déterminée au moyen de distances
zenithales.

Comme la plus grande partie du terrain sur lequel la triangulation s'étend jusqu'à
présent est un pays montagneux, on à pu choisir presque toujours des sommels de mon-
tagne ou des collines pour y établir les stations trigonométriques.

Les sommets ont été déboisés pour autant que cela a été nécessaire et pourvus de
piliers en héton, d’une hauteur de 1,5 mètre pour les points de premier et de deuxième
ordre et de 0,3 mêtre pour ceux de troisième ordre. Dans les stations de premier et de
deuxième ordre, l'instrument repose directement sur le pilier; pour les stations de troisième
ordre, l'instrument est mis sur un trépied.

A la fin de 1890, la construction des piliers était achevée sur 64 points de premier
ordre, 84 de deuxième ordre et 911 de troisième ordre.

Le chef de la section lrigonomélrique,

(Signé) 11.-D.-U. BOSBOOM.

 

ll: — à

aus ad ra DURE BA IE Matin à he |

Li ts hui

a bi |

 

 
