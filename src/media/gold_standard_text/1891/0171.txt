 

163

Uebereinstimmung, wegen der grösseren Veränderung der Lattenlänge vom Frühjahr bis
September.

Rechnet man aber mit den für die Beobachtungszeiten aus den Sommervergleichen
abgeleiteten Meterlängen, so ergeben alle drei nenen Messungen eine vollkommene Ueberein-
stimmung, wie dies die in der zweiten Zeile stehenden Zahlen zeigen.

| Diese Resultate weichen aber von jenen der ersten Messung bedeutend ab, und da es
i auf anderen älteren Nivellement-Linien zu ganz ähnlichen Ergebnissen gekommen ist, so tritt

die Nothwendigkeit heran, jenen Theil unserer Nivellements, welcher über höhere Gebirgs-
züge führt, einer Neumessung zu unterziehen.

Unsere letzte (7.) Nivellir-Lalte wurde im verflossenen Winter ebenfalls mit Stahl-
stäben (zum Vergleiche während der Feldarbeit) versehen.

Die Resultate der im verflossenen Sommer durchgeführten Vergleiche unserer 7
Latten zeigt die nachstehende Tabelle.

Die Zahlen dieser Tabelle sind p. (Mikron) und geben an, um wieviel der Lattenmeter
in jedem Monate bei einer mittleren Temperatur von 16 bis 18° Celsius zu gross war.

 

I Latte A’ | atte: B: | Latte D’

| Latte EB | Latte F’ | Latte G’ | an |

 

 

 

 

| Beobachtungs- Mittlere Länge der Lattenmeter abgeleitet aus den |
Periode. absolu-| relati- fabsolu-| relati- fabsolu-| relati- fabsolu-| relati- @ absolu-} relati- absolu-| relati- # absolu- A
ten. ven. ten. ven. ten. ven. ten. ven ten. ven. ten. ven. ten. ven. |

 

 

 

 

 

 

 

 

Lattenvergleichungen.

 

1.000 000 À 1.000 000 5 1.000 000 # 41.000 000 | 1.000 000 | 1.000 000 1.000 000

 

 

  
  

 

 

 

 

 

 

Mitte April 1891 |4503 1482 us 4.447 ue + 440 |
|e Mat 1.» | 519 +596 + 439 + 466 + 460!
» Juni >» |+ 558 + 627 +454 +501 + 486)
> duh 11648 1.670 + 484 gi +528
| » August» +646 454814657 + 684 +. 468 +548 + O44)
| ee +624 + 568 +644 484 +563 + 494)
| » Octob.» i+ 646 4.5761-+ 705|+ 683 or Or +569 + 488)
= |» Nov. » [+624 +592 + 630|- 6321+. 517|+ 4671+ 613)+ 586] 496| 4 481)
= D, Dee » | + 683|+ 621
= Differenzen. 29 32 22 2 50 27 15
5 Anmerkung. — Latte B’ ist in den Monaten Mai, Juni, Juli und November nicht verglichen worden, |

| und erst im December von der Feldarbeit zurückgekehrt. |
| Latte D’ war im April in der Ausstellung des IX. deutschen Geographen-Tages. dann, behufs Adapti-
rung, beim Mechaniker und nur von Mitte August bis Mitte September in Verwendung.

Latte F’ ist von Mitte Juli bis Mitte August nicht verwendet worden.

ii

 

 

 

 

Aus den vorstehenden Differenzen erhält man den wahrscheinlichen Fehler der im
Sommer bestimmten Längen des nominellen Latlenmeters mil & Wu.
Nach der Art unserer Latienvergleiche drücken die Differenzen dieser Zahlen

À Ge mi Le

a

7

 
