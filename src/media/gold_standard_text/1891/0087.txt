 

für die Freundlichkeit zu danken, mit welcher er Herrn Prof. Lorenzoni während den Pen-
delbeobachtungen, die er im Laufe dieses Sommers in llalien ausgeführt, in alle Einzelheiten
seiner Instrumente und seiner Methoden eingeweiht hat.

Der Herr Präsident ertheilt, der alphabetischen Reihenfolge gemäss, dem Abgeord-
4 neten des Gro®sherzogthums Baden das Wort, welcher erklärt keinen besondern Bericht
5 vorlegen zu können.

Herr Oberst Hennequin erhält hierauf das Wort zum Berichte über die in Belgien
ausgeführten Arbeiten." Er gedenkt zunächst der grossen Verdienste des Generals Liagre, um
die Geodäsie in Belgien, sowie derjenigen der Hauptleute Delporte und Vermersch; alle drei
sind ihrem Vaterlande und der Wissenschaft durch den Tod entrissen worden.

Der Bericht beschäftigt sich hauptsächlich mit den Nivellementsarbeiten, den Ver-
bindungen des belgischen Höhen-Netzes mit denjenigen der Nachbarstaaten, und mit den
Mareographen. (Siehe Beilage B. II.)

Der Herr Präsident dankt Herrn Hennequin für seinen Bericht und spricht sein
Bedauern über den Verlust der hervorragenden, belgischen Geodäten aus, besonders erwähnt
er noch der Verdienste des gelehrten Generals Liagre, dessen Namen weit über die Grenzen
seines Vaterlandes bekannt geworden.

Hierauf ertheil er dem Ilerrn Oberst Zacharive das Wort zur Berichterstattung über
die geodälischen Arbeiten ın Dänemark während des Jahres 1891. Diesem Berichte sind
Zeichnungen von den Normal-Fixpunkten des Nivellements beigegehen. (Siehe Beilage B. 111.)

 

Hierauf folgt der Bericht des Herrn d’Arrillaga über die geodätischen Arbeiten,
welche im Jahre 1891 durch das geographische und statistische Institut in Spanien aus-
geführt worden. (Siehe Beilage B. IV.)

Nachdem der Herr Präsident die Mittheilungen der Herren Zachariae und d’Arrillaga
bestens verdankt hat, ertheilt er das Wort den Delegirten Frankreichs.

Herr General Derrécayaix erstattet Bericht über die geodätischen Arbeiten, welche
durch das geographische Bureau der Armee ausgeführt worden. Dieser Bericht, von welchem
gedruckte Exemplare unter die Mitglieder der Conferenz vertheilt wurden, betrifft, unter
den Arbeiten des letzten Jahres, die neugemessenen Verifications-Grundlinien von Paris,
is Melun und Perpignan, aus deren Resultaten der Herr General die Folge zieht, dass die
- bisher angenommenen Gleichungen der wichtigsten fremden Normalmaasse mit dem interna-
tionalen Meter um on zu schwach sein müssen. Als astronomische Arbeiten sind angeführt
die Bestimmungen von Längendifferenzen zwischen Rivesaltes (in der Nähe von Perpignan)
£ und Paris-Montsouris, zwischen Rivesaltes und Desierto (in Spanien), und die Breitenmes-
: sungen zu Paris und Rivesaltes. Ueberdiess erwähnt der Bericht die Schwerebestimmungen,

welche zu Rivesaltes und in vier benachbarten Stationen, sowie in Desierto ausgeführt worden
| sind. Ebenso wird über die in Algier ausgeführten Arbeiten berichtet. (Siehe Beilage B. Va.)

a AR

NE PART

A

 
