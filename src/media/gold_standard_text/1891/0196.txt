 

188

vée, il y a vingt-cinq ans, par les opérateurs suisses. Il est donc probable que l’un au moins
de ces repères a subi un déplacement. Pour que l’on puisse tirer de ce nouveau rattache-
ment des résultats utiles, la Commission géodésique suisse voudra sans doute poursuivre sur
son territoire les opérations jusqu’à ce qu’on ait atteint un repère ancien d’une stabilité
démontrée.

Enfin, sur notre frontière sud et à la demande de M. de Arillaga, Directeur Général de
l'Institut géographique de Madrid, nous avons préparé à Cerbère un nouveau rattachement
de notre réseau avec le réseau espagnol. Gelle opération sera terminée dans la prochaine
campagne.

Les nivellements que nous venons d’énumérer représentent un développement total
de 1540 kilomètres qui, ajoutés aux 9240 kilomètres précédemment effectués, portent à
10780 kilomètres l'étendue totale des lignes du réseau fondamental actuellement nivelées.
Les 1520 kilomètres restants seront terminés l’année prochaine, de sorte que, selon toute pro-
babilité, le réseau entier pourra être compensé en 1893 et les altitudes définitives publiées,

En attendant, nous continuons l’impression du répertoire graphique des repères,
dont la deuxième livraison, embrassant, comme la première, 3000 kilomètres du réseau
fondamental, vient de paraitre.

Au mois d'août dernier, notre élalon des mires a été comparé avec le metre proto-
type, au Bureau international des Poids et Mesures de Breteuil. Son équation absolue a ainsi
été déterminée; on en tiendra compte dans le calcul des allitudes définitives du réseau fon-
damental.

NIVELLEMENTS DE SECOND ORDRE

Le réseau fondamental doit servir de base 4 un réseau de second ordre, long de
27000 kilomètres environ, v compris tes 15000 kilomètres de l’ancien réseau de Bourdaloue,
qui y seront incorporés après rectification.
Les nouveaux nivellements de second ordre à exécuter présentent donc environ
12000 kilomètres de développement. Dès cette année, nous avons commencé lexécution de ces
nivellements, dont 180 kilomètres ont été effectués par une brigade spéciale dans la région
du Nord. Pour ces nivellements, on a adopté la même méthode et les mêmes instruments
‘que pour le réseau fondamental. La seule différence réside dans la suppression des retourne-
ments de la lunette et de la nivelle, dans l'augmentation de la longueur des nivelées, dans
l'élargissement de la tolérance fixée pour l’équidistance des mires au niveau et dans laccrois-
sement correspondant des tolérances admises pour les discordances systématiques et acciden-
telles entre les deux opérations d'aller el de retour; ces tolérances sont à peu près doublées.
Ces diverses simplifications permettront d’accroitre d’un tiers environ la vitesse du travail et
d’abaisser en conséquence le prix de revient.
Les coefficients kilométriques d'erreur probable admis pour ces nivellements de
second ordre sont respectivement de 2m pour la partie accidentelle et 0°"3 pour la partie
systématique, c’est-à-dire à peu près ceux du nivellement de Bourdalouë.

 

(idl ul —

zu ea Ta U U N À

ab lad

ty in |

ih

mal ie i MAMMAL Hdl ti 4

 
