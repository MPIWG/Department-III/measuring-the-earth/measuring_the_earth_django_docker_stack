 

Zu den Zeitbestimmungen verwendete ich das grosse 13zöllige Universale, welches
auch zu den Breiten- und Azimutbestimmungen auf den trigonometrischen Punkten I. Ord-
nung dient. Auf jeder Station wurde auch eine relativ genaue Breitenbestimmung ausge-
führt. Zar Vergleichung der Schwingungszeiten der benützten 4 invariablen Pendel dienten

| 4 Chronometer.
i Die beobachteten Stationen ausser Wien sind folgende: 1. München, 2. Grafing,
3. Ostermünchen, %. Rosenheim, 5. Fischbach, 6. Kufstein, 7. Wörgl, 8. Jenbach, 9. Fritzens,

10. Brixen, 11. Branzol, 12. Neumarkt, 13. Salurn, 14. S. Michele, 15. Lavis, 16. Trient,
47. Mattarello, 18. Culliano, 19. Mori, 20. Riva, 21. Ala, 22. Avio, 23. Peri, 24, Ceraino,
95. Pescantina, 26. Dossobuono, 27. Mozzecane, 28. Mantua, 29. Borgoforte, 30. Padua,
ol Venedig.

Die ganz ausserordentliche Unterstiitzung, welche diesem Unternehmen seitens der
hohen Regierungen von Oesterreich, Bayern und Italien zutheil wurde, ermöglichte es, diese
Bestimmungen einheitlich, mit ein und demselben Instrumente und durch denselben Beob-
‚achter durchzuführen, und es erscheint hiedurch der wahrhaft internationale Charakter
dieses Unternehmens schön gekennzeichnet.

Die in mehrfacher Hinsicht interessanten Ergebnisse dieser Arbeit werden in dem

. XI. Bande der Mittheilungen des k. und k. militär-geographischen Institutes veröffentlicht
werden.
he von SERRNECK" m’ pe

k. und k. Oberstlieutenant.

 

NT RT

PE UH Bm | iG HF TT

ci

ih

 
