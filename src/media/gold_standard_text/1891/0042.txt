 

34

lorsque ces étalons auront été soumis à des comparaisons directes avec le mêtre inter-
national.

Il est à remarquer d’ailleurs que la toise de Bessel est pour ainsi dire l’étalon fonda-
mental de la géodésie; c'est d’elle que derivent la toise de Struve, la toise d’Ertel, la toise
de Spano, etc. C’est à elle, en partie, qu'ont été comparés les étalons anglais et américains.
Son changement d’équation va done produire une véritable révolution dans la eéodésie con-
temporaine. Mais, dés que tous les étalons auront été ramenés a la méme unilé, il est permis
d'espérer que l'Association pourra enfin aborder le problème pour lequel elle a été fondée,
c'est-à-dire souder les uns aux autres les réseaux des différents pays, puis constituer de
grands arcs de méridien et de parallèle et les appliquer à la recherche de la forme et de la
grandeur de la Terre.

Du côté de l'Espagne, la nouvelle méridienne offre une divergence de m avec la
petite chaine qui vient de la base de Vich. Il y a lieu d’attirer lattention de nos collègues
d'Espagne sur ce point. Il semble qu’il serait nécessaire de reviser cette base de Vich. Elle
a été, en effet, mesuree avec l'appareil monométallique du Général Ibanez, dont l'équation à
été établie par rapport à celle de l'appareil bimétallique de Pnstitut d'Espagne. Or l'appareil
bimétallique espagnol a été étalonné avec la règle module de Borda, mais n'a jamais été
comparé à Breteuil. Il en résulte que l'équation de la règle monométallique employée dans
la mesure de la base de Vich ne dérive nullement du mêtre international.

On sait cependant que cette même règle monométallique à servi à la ınesure des
bases suisses, qu'elle à été ensuite étalonnée à Breteuil, et que là son équation a été sensi-
blement modifiée ; le coefficient de dilatation a été trouvé plus fort. Cette modification dans
la valeur du coefficient de dilatation a été attribuée à un changement d'état moléculaire du
métal de la règle, par suite des secousses occasionnées par un transport en chemin de fer
exceptionnellement rapide. On n’a done pas tenu compte du nouvel étalonnage pour les bases
antérieurement mesurées en Espagne. Mais le critérium auquel se trouve soumise aujour-
Whui la base de Vich, par suite de son voisinage avec la base de Perpignan, montre bien la
nécessité, pour l'Institut d'Espagne, de la soumettre à un nouveau contrôle, soit avec son

appareil monométallique, soit avec son appareil bimétallique, mais avec leurs équations
établies à Breteuil.

On voit, par cet exemple, l'intérêt qui s'attache à comparer entre elles les bases les
plus voisines des divers pays limitrophes. Cette étude est des plus importantes pour la
oéodésie internationale. Seule, elle pourra conduire à la fusion des réseaux. Elle est parti-
culièrement intéressante là où on relie une base mesurée avec un appareil étalonné à Breteuil,
à une autre base mesurée avec un appareil qui n’a pas encore été comparé au metre ınler-
national. C'est ce qui vient d’être fait en France. C'est ce que l'on pourrait faire très utile-
ment en Suisse, où existent trois bases mesurées avec une remarquable précision et ramenées
au mètre international, que l’on peut comparer aux bases voisines de l'Italie, de l'Autriche,
de l'Allemagne et de la France.

Comme conclusion aux observations qui précèdent, je soumels & l'examen de la
Commission permanente les deux propositions suivantes :

 

 

a a A TT

‘II

Len tu in

roh

un:

wu tamed dis na Mobilit arty by
