FT ln EI DARAN DL MON M8 LL à

Kind

 

LH

IMC QE

 

207

bedingung angenommen werden, was aber keine Veranlassung zu besonderen Schwierig-
keiten gab.

Nachdem also ein Haupt-Höhen-Neiz festgestellt worden war, wurden die Höhen der
secundären Punkte, mit Rücksicht auf die Gewichte der Höhen-Unterschiede an die Haupt-
punkte angeschlossen.

War auf einer Station das Meer sichtbar, so wurde auch stets die Kimmtiefe beob-
achtet. Man sieht aber in dem Ost-Indischen Archipel, zumal des Morgens, fast immer eine
starke Luftspiegelung, eine Folge des Umstandes, dass das Meer dann eine höhere Temperatur
als die Luft hat, also die unteren Luftschichten specifisch leichter sind als die etwas höheren.
Entfernte Inseln oder Schiffe scheinen dann immer etwas höher über die Kimm erhoben,
und unten von einem etwas zusammengedrängten Spiegelbilde begleitet. Diese Luftspiegelung
verursacht, dass man nicht ohne Weiteres durch die bekannte Formel die Höhe eines Gipfels
durch die Kimmtiefe bestimmen kann, da in dieser Formel zwar die Refraction beachtet
wird, dem Lichtstrahle aber die Gestalt eines Kreisbogens gegeben wird, der die Erdkugel

berührt, einen Halbmesser — + hat (R = dem Haibmesser der Erde) und durch die Beob-

achtungsstation geht.

Bei einigem Nachdenken sieht man aber rasch ein, dass der Kintluss der sogenann-
ten Luftspiegelung ', bei welcher der Lichtstrahl, vom Auge ausgehend, erst, bis nahe an
der Meeresoberfläche, nach oben convex ist, dann aber einen Biegpunkt bekommt, und end-
lich nach oben concav wird, bei der Bestimmung einer Höhe durch Kimmtiefe in einem
constanten Fehler bestehen muss. Ob dieser Fehler aber die Höhe zu gross oder zu klein
angiebt, ist nicht leicht a priori zu sagen. Die Discussion von 47 Kimmtiefen, auf Punkten
gemessen, von 0 bis 833 Meter Höhe, ergab dass mit ziemlicher Genauigkeit gesetzt werden
kann, bei normaler Luft sei

R

2 m
H = (6,5655) tg? d — I tg* d — 2,5,

wo H die Höhe über der Meeresoberfläche, und d die Kimmtiefe bedeutet. Das zweite Glied

9
a tg d kann in der Praxis wohl immer vernachlässigt werden. Die eingeklammerte
)

R 1 : 1
Zahl ist der Log. von ya up so dass aus ihr der Werth von —%k abgeleitet werden
_— TOR eo
kann; man findet 0,06776, ziemlich nahe mit dem oben genannten Werthe übereinstimmend,
Selbstverständlich ist immer die Genauigkeit dieser Höhenbestimmung für Gipfel im
Innern geringer als diejenige, welche erreicht wird, wenn man zwischen dem Meer und dem

Gipfel einige Zwischenpunkte annimmt; auch die mitunter vorkommenden ganz anormalen

1 Der Name ist deshalb nicht zweckmässig gewählt, weil von einer eigentlichen Spiegelung, d.h. Re-
flexion der Lichtstrahlen, wie Monge die Sache erklärte, keine Rede ist, vielmehr von einer Refraction, wie
Biot meinte.

 
