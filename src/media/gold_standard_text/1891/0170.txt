 

162
Zablotce-Krasne | (Bahn).
Bochnia- Krakau oO
Jaroslau- Rawaruska-Sokal »
Satoralya Ujhely-Miskolez-Banreve  »
Miskolez-Budapest »
Sillein- Petrowilz »
Kremnitz- Turcsek | »

Die meisten dieser Strecken sind wenigstens theilweise ein drittes Mal gemessen
worden, insbesondere dort, wo seit der ersten Messung Fixpunkte verloren gegangen waren,
und nun neue errichtet werden mussten.

Das Nivellement auf der Strecke Banreve-Poprad trägt vollkommen den Charakter
eines Gebirgs-Nivellement, infolge der grossen Höhenunterschiede auf kurzen Distanzen, wie
7, B. zyaschen dem En Csuntawa und Hammer (Distanz 10 km., Höhenunter-
schied 600 m.).

Hier ist daher die Wichtigkeit der Sommervergleiche unserer Nivellir-Latten in
eminenter Weise zulage getreten, wie aus der folgenden Tabelle zu ersehen ist, welche alle
auf dieser Strecke gemachten Messungen enthält.

In der ersten Zeile sind jene Daten, welche mit der im Frühjahre in Wien ermit-
telten Laltenlänge gerechnet wurden, während die Zahlen der zweiten Zeile mit der durch
die Sommervergleiche ermittelten zur Zeit der Messung stattgehabten Lattenlänge erhalten

worden sind.

a |

 

 

 

 

Alte Messung Neue Messungen vom Jahre 1891.
vom Jahre 1883. | mat ibd dous S06 Oe m N
1 2 3.
Instrument No 2985, Instrument N° 3572, Instrument No 2984,
| Latte.: H’. Latte: A’. Latte : G’.
602.2927 602.3019 | 602.3075 | 602.3232
— 602.3744 602.3767 602.3727

 

 

aa.
es

Da hier die zwei ersten Messungen recht gut übereinstimmen, wäre, ohne Adap-
tirung unserer Latten und Vergleichung derselben während der Arbeit, eine Control-Messung
auf dieser Strecke gar nicht ausgeführt worden.

Die auffallende Uebereinstimmung der drei ersten Daten in der ersten Zeile stammt
daher, dass die bezüglichen Messungen auf dieselbe Jahreszeit (Juni-Juli) fielen, weshalb die
Veränderungen, welche die Lattenlängen in diesen Fällen seit dem Frühjahrvergleiche
erlitten hatten, nahezu gleich gewesen sein werden. Aber schon die vierte Messung, welche
im September durch einen andern Beobachter vorgenommen wurde, zeigt eine geringere

 

 

ii el a TU RM NEN AT ddl. "À

Lake uit |

wt

ia ma pu aa Lu La idl peat 4

Ï il
