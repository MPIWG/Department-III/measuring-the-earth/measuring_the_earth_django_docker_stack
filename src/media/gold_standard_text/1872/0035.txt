 

BETT RETTET TTT TAT reset

ronan

ee En:

Für Breiten- und Azimuth- Bestimmungen würden ausser auf den Längenstationen

nothig sei

c

n circa 25 Punkte, von welchen 11 in Preussen etc., 8 in Bayern, 3 in Sachsen,
1 in Wiirttemberg, 2 in Baden liegen.

Pendel-Beobachtungen werden an 14 Orten und zwar an 9 in Preussen, 1 in Bayern,
3 in Sachsen, 1 in Baden gewünscht.

Die astronomischen Beobachtungen würden kosten für 36 Längenlinien zu 1000 Thaler
zusammen 36,000 Thaler; für 25 Breiten und Azimuthbestimmungen zu 500 Thaler zu-
sammen 12,500 Thaler; für 14 Pendelbestimmungen zu 200 Thaler zusammen 2800 Thaler.

Um sowohl die geodätischen als auch die astronomischen Bestimmungen auszuführen,
würde auch noch eine grosse Anzahl von Instrumenten nöthig sein, welche noch zu eirca
50,000 Thalern zu verrechnen sind.

Wenn man die Summen addirt, hat man:

für trigonometrische Arbeiten . 398,000 Thaler

 

für astronomische Arbeiten . . 951,300 ,
für Instrumente . js os POU ea
Summa: 499,300 Thaler
rund © x. 00.000 raie

Nach den Mittheilungen der Commissare ist in den meisten Staaten z. B. in Meck-
lenburg, Hessen ete. die Arbeit ganz vollendet, in anderen Staaten grösstentheils fertig.

Es fehlen etwa noch an trigonometrischen Arbeiten

in Preussen. ....1,..7100 Punkte
MORI AV CTI ul a Ly
ia SACHS! verte. We Og ec
me Wurttembers. 7 0 |
1A Ba MSI. i+ es) cree ED wen

Summa: 146 Punkte.

An Messung von Grundlinien:

7 ing Preussent 00 270 1913
10 Bayern ... 0...
IHM ASS 1.0 | ge a
inpaadleni-icue ed al

Summa: 6
An astronomischen Bestimmungen:

in Preussen 10 Längenverbindungen

in Sachsen... 2 5
in Bayern, u,» à
in Baden: | .

 

Summa: 18 Längenverbindungen.
General-Bericht f. 18%. ~

 
