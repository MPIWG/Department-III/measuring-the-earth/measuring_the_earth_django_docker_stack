  

MO oe

Auf besondere Einladung anwesend: Generalmajor v. Morozowicz, Chef der Konigl. Preuss.
Landestriangulation. Entschuldigt: Dr. Hansen und Herr Paschen.

Der Präsident eröffnet die Sitzung um 11 Uhr. Nachdem die geschäftlichen Mit-
theilungen beendigt sind, stellt Herr Bauernfeind den Antrag, es möchte der Berathung
ein Programm zu Grunde gelegt werden, und macht sofort folgenden Vorschlag hierzu:

1. Berathung über die Einrichtnng des Büreaus der ersten Conferenz und Wahl
dieses Büreaus.

2. Berathung und Beschlussfassung über die Organisation der Reichscommission
für die deutsche Gradmessung.

3. Berathung und Beschlussfassung über die in Deutschland nothwendigen Dreiecks-
ketten und astronomischen Stationen.

4. Berathung und Beschlussfassung über die geodätischen und astronomischen Ar-
beiten, welche für die Deutsche Gradmessung benutzt werden können.

Aufstellung eines Kostenanschlags über sämmtliche geodätischen und astrono-

CON

mischen Arbeiten, welche auszuführen sind, und Angabe des jährlichen Etats
der Reichscommission.

6. Berathung und Beschlussfassung über die Herstellung und Kosten von Präcisions-
Nivellements.

Dieses Programm wird angenommen und entsprechend No. 1. werden durch Accla-
mation die Herren Bruhns und Bauernfeind als diejenigen Mitglieder gewählt, welche in
Gemeinschaft mit dem Präsidenten Herrn Baeyer das Büreau zu bilden haben. Zu No. 2
bringt Herr Bauernfeind einen in 8 Paragraphen formulirten Entwurf vor, welcher sodann
im Einzelnen berathen werden soll.

Herr Baeyer betont, dass die Commission nicht beduftragt sei, in die Berathung
der in obigem. Entwurf enthaltenen Vorschläge einzugehen, sondern nur die beiden vom
Reichskanzler-Amt gestellten Fragen zu beantworten habe.

Die Herren Sadebeck, Hiigel und von Morozovicz legen ihre Auffassung der Stellung
der Deutschen Gradmessungs-Commission zu der permanenten Commission der Europäischen
Gradmessung dar, worauf die Commission die Ueberzeugung ausspricht, dass sie nicht im
Mindesten in die Rechte der permanenten Commission einzugreifen, sondern deren Arbeiten
zu fördern habe. N

Herr Seidel hält dagegen diese Berathung der Organisation für nothwendig als Vor-
bedingung der Beantwortung der vom Reichskanzler-Amt gestellten Fragen.

Nachdem Herr von Morozowiez die Organisations-Berathung als speciell erforderlich
für die Aufstellung eines Kostenanschlags durch Beispiele nachgewiesen hatte, wird die
Generaldebatte über den Bauernfeind’schen Entwurf geschlossen und in die Berathung der

einzelnen Paragraphen desselben eingegangen.

di uhr

+s ts

Joh

esas chek ui bad aid cl a ha i

 

 
