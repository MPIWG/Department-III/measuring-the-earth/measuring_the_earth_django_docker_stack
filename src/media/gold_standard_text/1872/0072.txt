 

RER

ee ee

 

 

m m T mm mm
0,05 — 2,95 = Eisenstab — 9,07 = 2901,058 — 0,702

a
i
3
3
i
i

0,06 — 2,96 (bei 130,2) — 8,80 — 0,681
0,07 — 2,97 G08 — 0,722
0,08 — 2,98 — 9,00 0,697
0,09 — 2,99 0 — 0,544
0,10 — 3,00 en 7

— 0,642,4 = 8 v? = 0,0666
0,0074
0,0004

290m der Hessischen Latte No. I. = 2900™™,416 + 0™™,027 =
0070

1™ der Hessischen Latte No. I. — 1", 000148 + 02,009

Mittlerer Theilfehler eines Intervalls von ? Strichen + 0",084

Mittlerer Theilfehler eines Striches a 000

Die Hessische Latte No. I, der vorigen ganz gleich gearbeitet, hat eine fast :
doppelt so starke Correction (1” : 1™,000279), die Theilfehler hingegen sind fast identisch
(+ 0,064). |

Leider hatten diese vortreflichen Miren keinen Sporn und Senkelvorrichtung, so |
dass es nicht anging, sie auch in Neuenburg in vertikaler Lage zu vergleichen.

Die Sächsische Latte, welcher die von Berlin eingesandte vollständig ähnlich ist,
hat 4 Meter Länge und ist in Doppel-Centimeter getheilt. Die Theilung, abwechselnd aus
Strichen und Punkten und nicht, wie bei den übrigen, aus abwechselnd schwarzen und
weissen Feldern bestehend, lässt viel zu wünschen übrig, die Theilstriche sind |
diek und schlecht begrenzt. Die Latten, ohne Verstärkungsrippe, waren stark verw orfen;
bei der Sächsischen betrug die Durchbiegung, senkrecht auf die Theilflache 9,5, ‚bei der
Berliner 6™", letztere hatte auch eine seitliche Biegung von 9™™. Folgendes sind die an
der sächsischen Latte von Deeimeter zu Deeimeter angestellen Messungen:

|

Sächsische Latte.

087 15,0 = 0,1 — 3,0 = Eisenstab — 0,46 = 2901,029 — 0,03 6

010 — 15,5 = 0,2 —3,1— (bei 12,2) — 8,80 — 0,681
01,5 — 16,0 = 0,3 — 3,2 9 79 — 0,288
0,2,0 — 1,6,5 = 0,4 — 3,3 so — 0,216
02,5 — 7,0 = 0,5 — 3,4 — 4,96 — 0,384
0,3,0 —1,7,5 = 0,6 — 3,5 710 — 0,286
0,3,5 — 18,0 = 0,7 — 3,6 nb — 0,581
O10 15520 "87 + 4,90 + 0,325

m —— 2

— 0,268 3 v2 = 0,6829
14:25 0,0976

0,0064

0,0912

 
