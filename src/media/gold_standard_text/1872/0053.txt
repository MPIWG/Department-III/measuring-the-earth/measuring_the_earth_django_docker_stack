aul Dir

Um die constante Höhe h des durch Multiplication der Ordinate mit der Constanten
| der Verjüngung erhöht gedachten Curvenstiftes tiber der Oberfläche des Wassers zu be-
stimmen, hat man, wenn y die an der Trommel abgegriffene und bereits mit 4,3995 multiplieirte
Ordinate der Curve, w den gleichzeitig am neuen Pegel im Bauhofe beobachteten Wasser-
stand bedeutet, h™ = 3,9214 + (y— w); um aber den mittleren Wasserstand eines Tages
auf den neuen Pegel zu übertragen, hat man (w — y) zu der betreffenden, vom Reeistrirungs-
bogen entnommenen, mit 4,3995 multiplieirten, mittleren täglichen Ordinate zu addiren.
Aus 26 Beobachtungen wurde h = 3%,9214 + 0m,2685 = 4,1899 mit einem wahrscheinlichen
Fehler = 0%,0023 gefunden. |

Der selbstregistrirende Pegel wurde anfangs Juni 1870 in Thätigkeit gesetzt und
ergab nach einem zweijährigen Gange, also bis Juni 1872, als Mittelwasser der Ostsee
über Null des neuen Pegels im Bauhofe = 1%,2442 — 0™,2685 = 0.9767, während nach
{rüheren 10jahrigen Beobachtungen des alten Pegels am Bollwerke (G.-B. v. 1870 pag. 57)
statt dieser Zahl 09,994 gesetzt, und vorerst auch so lange beibehalten werden muss, bis
die registrirenden Pegelbeobachtungen endgültig abgeschlossen sind.

Die beigefügte Tafel giebt in Intervallen von fünf Tagen die zweijährige Wasser-

standscurve, bezogen auf Null des neuen Pegels, in einer Ordinatenverjüngung von —

LO, i

SET PTT PONT WIRY TTT AT TTI tes te

4. Bericht des Herrn Professor Dr. Bruhns über die für das Königlich
Preussische geodälische Institut im Jahre 1872 ausgeführten astronomischen
Arbeiten.

Im Winter 1871—72 wurden zunächst die im Jahre 1571 ausgeführten Beobachtungen
von den Herren Dr. Albrecht und Dr. Löw reducirt und hat sich als definitives Resultat
ergeben, dass die Längendifferenz zwischen der Sternwarte Leipzig und dem Dreieckspunkt
auf der Sternwarte Mannheim

15™ 435,481 + 08,009
ist. Die Längendifferenz zwischen dem Dreieckspunkte auf der Sternwarte Mannheim und
dem Centrum des Hauptpfeilers der Sternwarte Bonn findet sich zu

5m 27,100 22 0,009.

Aus beiden Bestimmungen ergiebt sich die Leipziger Sternwarte östlich von der
Bonner Sternwarte zu |

21> 10 vol,

Herr General von Forsch giebt in dem vorigjährigen General-Bericht aus seinen
Längenbestimmungen einschliesslich der Differenz der persönlichen Gleichung der Beobachter
im Mittel:

/

Leipzig — Berlin — 4™ 08,74
Bonn — Berlin 2211792

also Leipzig — Bonn — 21™ 108,785,

 
