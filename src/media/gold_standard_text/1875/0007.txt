|

 

FRRRFRPTETTTTT

MT ETE ETRE

WOT

PTT TOUTE

NT TITTEN

u

Inhalt.

Table des Matieres.

Proces-verbaux des séances de la Commission permanente tenues 4 Paris
du 20 au 29 Septembre 1872.

Première Séance.

Liste des membres de la Commission per-
manente, des délegués, et des invités .
Discours d’ouverture de MMrs. Jourdan,

Ibañez, Baeyer et Faye . ;
Programme pour la réunion de la Gomme

sion permanente a Paris, en 1875
Rapport de la Commision permanente
Rapport du Bureau Central et de l’Institut

géodésique de Berlin . BE

Seconde Seance.
Rapports des MMrs. d’Oppolzer, de Bauern-
feind, Adan, Ibafiez, Perrier et Villarceau
Mr. Breton de Champ, communication sur la
difference de niveau entre l’Ocean et la
Méditerranée, résultant des Nivellements
en France, et discussion sur cette question

Troisieme Séance.

Rapport de Mr. de Vecchi.
Stamkart

Détails donnés par Mr. oti sur Pétude
des déviations de la verticale dans le Harz
et les environs

Rapports de MMrs. Bacar Peer pane
de Forsch, Bruhns et Hirsch . :

Quatriéme Séance.
Rapport de Mr. Mahmoud Bey sur les tra-
vaux en Egypte ;
Rapport de Mr. Hirsch sur Bonseitidn de:
décisions prises l’année dernière, concer-

Lettre de Mr.

pag:

10

11

14

15

15

16

 

nant la liste des publications géodésiques
et le tableau des déterminations astrono-
miquen , :

Discussion sur ce capa ;

Décisions prises & ce sujet

Cinquième Séance.

Communication d’une lettre de Mr. Fearnley
sur les travaux géodésiques en Norvège
Rapport de Mr. d’Oppolzer, rapporteur de

la Commission spéciale du pendule .
Résolutions adoptées sur la question du pendule
Rapport de Mr. le commandant Perrier, rap-

porteur de la Commission spéciale pour

l'appareil des bases KR
Propositions de la Commission a pour

appareil des bases, adoptées par la Com-

mission permanente 3
Discussion sur les questions du Sone rane

concernant les maréographes . . . . 4

Propositions de Mr. Hirsch adoptées

Communication de Mr. Hirsch sur l’établisse-
ment international des poids et mesures .

Discussion sur l'horizon fondamental pour
les altitudes :

Résolution a cet ni propenden par Mr.
d’Oppolzer "on, ;

Discussion sur la quéstion des attractions ee
cales et des déviations de la verticale

Discours de Mr. le Ministre de I’ Instruction

qui assiste à la séance
*

pag.

16
18
19

: 20

20
22

23

25
26

27

29

30

30

31

 
