En HERE So Ter IF

IR LEUR

al ART

Bi je 0

=
is
®
x
=
=
à
à

 

91

source d’erreurs, parce que le pendule en oscillation communique toujours un mouve-
ment oscillatoire au trépied auquel il est fixé et qu'il en résulte une perturbation
dans la marche du pendule. Il est vrai qu’on peut atténuer cette action perturbatrice
en raccoureissant le pendule et en diminuant son trépied; mais il reste toujours
douteux que l’influence soit entièrement éliminée.

Puis le degré d’exactitude du résultat final est aussi fortement influencé par
la mobilité des couteaux, à laquelle on peut attribuer la cause de variations de dif-
férentes sortes.

Enfin, le thermomètre métallique change son zéro par le fait du transport, et
par suite ne donne plus des résultats dignes de confiance. La cause de cette variation
doit être probablement cherchée dans l’ébranlement produit dans l’appareil par les trans-
ports en chemin de fer.

20 Conditions qu'il est à désirer que remplisse le nouvel appareil de pendule.

Les déterminations d'intensité de la pesanteur devront, à l’avenir, être exécutées
beaucoup plus fréquemment qu’elles ne l’ont été jusqu’à présent, pour rechercher les
causes des déviations de la verticale.

Quand une déviation de la verticale est produite par des couches voisines plus
denses, elle est accompagnée d’une augmentation de l’intensité de la pesanteur. Quand
elle est produite par des couches moins denses elle correspond à une intensité moindre
de la pesanteur. Il est donc possible, lorsqu'une déviation de la verticale est con-
statée, de décider, par des observations de pendule, lequel des deux cas se présente.
Mais comme il s’agit de la détermination de très petites quantités, il faut se servir,
pour la construction du nouveau pendule, de toutes les ressources que fournissent la
science et la technique actuelles. Il faudra en particulier que l’appareil remplisse les
conditions suivantes :

10 Il doit être aussi simple et aussi solide que possible, de manière à ne pas
risquer d’être détérioré par l’usage et par le transport.

20 Son installation ne devra pas être trop difficile et Vobservation devra être
aisée et ne pas exiger trop de temps. |

30 Il devra être construit de telle manière que le degré d’exactitude fourni jus-
qu'ici par les appareils de même espèce, soit surpassé le plus possible.

30 Mesures proposées éventuellement pour satisfaire aux conditions énoncées ci-dessus.

Les avantages que présente un pendule à réversion sont universellement re-
connus, mais il n’en reste pas moins vrai que la mesure de la distance des couteaux et
celle de la position du centre de gravité sont des opérations très délicates qui exigent
des appareils d’une grande précision. Il est donc évident que l’on ne pourra, dans des

stations passagères, exécuter ces opérations avec les mêmes soins que dans des observatoires
12*
