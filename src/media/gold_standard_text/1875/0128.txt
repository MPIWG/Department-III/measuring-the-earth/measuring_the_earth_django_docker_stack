 

 

 

118

triompher; les grandes opérations effectuées depuis quelques années, tant en France
qu’à l'étranger, ont donné les résultats les plus satisfaisants. Nos voisins de la Con-
fédération helvétique n’ayant pas été arrêtés par les difficultés que présente chez eux
la configuration du sol, on peut admettre que le nivellement géométrique sera exécu-
table partout où il sera nécessaire.

Le principal obstacle semblerait provenir de la difficulté d'obtenir de bonnes
mesures des distances zénithales des objets terrestres: l’exacte évaluation des réfrac-
tions rencontre, en effet, des obstacles considérables. Les discordances qui se sont
produites dans les nivellements géodésiques, à l’époque de Delambre et longtemps en-
core après lui, tiennent à l’emploi d’un certain coefficient à appliquer à lPangle au centre,
pour en déduire la réfraction, coefficient que l’on faisait varier suivant les saisons, du
jour à la nuit, sans règle bien précise. On a reconnu ensuite qu'il fallait abandonner
l'emploi d’un coefficient impossible à déterminer autrement que par les observations
elles-mêmes: alors on a conseillé la méthode des distances zénithales réciproques, qui
semblait devoir éliminer les effets de la réfraction. (Cette méthode n’a cependant pas
atteint son but; la théorie indiquait d’ailleurs son insuffisance. Parmi Îles tentatives
faites pour triompher de ces difficultés, il faut citer la belle opération exécutée par les
astronomes de Pulkowa, et sous la direction de W. Struve, pour déterminer la diffé-
rence de niveau entre la mer Noire et la mer Caspienne. Qu’on me permette ici de
citer mes propres recherches sur cette matière difficile.

En 1860, j'ai présenté au Bureau des Longitudes*) le résultat de recherches
théoriques sur les réfractions terrestres: j'avais pris pour base l'emploi d’une expression
de la densité de l'air, mise sous la forme d’un polynôme ordonné suivant les puis-
sances des hauteurs et limité au terme du deuxième degré inclusivement, polynôme
dont les coefficients restaient à déterminer par l’observation; les conditions de l’obser-
vation étaient celles de la méthode des distances zénithales réciproques , complétées
par l'observation du baromètre et du thermomètre aux deux stations. Cette methode
wa reçu chez nous aucune application; mais la même méthode a été appliquée, en
1864, par Mr. Ibanez, Membre de l’Académie de Madrid, qui l’avait imaginee de son
côté (voir Estudios sobre nivelacion geodesica, par don Carlos Ibanez € Ibanez. Ma-
drid, 1864). Le succès obtenu par Mr. Ibanez permet d’augurer favorablement de l’emploi
de la nouvelle méthode.

Voici, ce nous semble, le principal obstacle à l'application que nous venons
de présenter du nouveau théorème. Par l'exposé qui précède, on a vu que Valtitude
h d’un point B de la ligne de nivellement exige que l’on connaisse les - variations
(L'—L) et (g—£) ou (Z—Z), produites par les attractions locales sur les coordonnées
ou azimuts de la station d’où le point B a été observé. Le point B devient, à son
tour, le lieu de station d’où un troisième point doit être également observé, et ainsi de

*) Séances des 12 et 19 décembre.

 

+

/

fe) et All Ma:

La onde

ko |

wih

u hu hes jn, ddA Aci li pau pue à

stat vase VA |
