 

i
u
iy
;
i
Er
|
i
|

216

Die Winkelbeobachtungen sind somit auf 29 Punkten — darunter 1 Punkt
zweiter Classe — beendigt und nur noch auf 5 Punkten erster Classe, nämlich auf den
Stationen Leipzig, Kuhberg, Stelzen, Kapellenberg und Aschberg zu bewirken. Die-
selben sollen im Sommer 1876 ausgeführt und womöglich auch noch die Verbindung
des sächsischen Netzes mit den Punkten Ochsenkopf und Döbraberg des bayerischen
Netzes hergestellt werden, im Fall die königl. bayerische Gradmessungscommission die
hierzu nöthigen Pfeiler auf letztgenannten Punkten, wie gehofft werden darf, aus-
führen lässt.

Die Nivellementsarbeiten in Sachsen sind als abgeschlossen zu betrachten
und es wird gegenwärtig an der Ausgleichung gearbeitet.

Leipzig und Dresden, am 30. Januar 1876.
C. Bruhns. C. A. Nagel.

 

Schweden.

Aus der neuesten Dreieckskarte des Topographencorps, welche ich nächstens
übersende, wird man einen Ueberblick über die schon vollendeten Triangulationen er-
halten. Es war ursprünglich die Absicht, dass unser Dreiecksnetz, insofern es für die
europäische Gradmessung angewandt werden sollte, sich gegen Norden nur bis Getle
(an der Ostküste Schwedens) erstrecken würde. Indessen werden die Winkel-
messungen gegenwärtig weiter gegen Norden fortgesetzt, so dass das östliche Netz

binnen Kurzem bis nach Tornea reichen und somit sich an das scandinavisch-russische
Gradmessungsnetz anschliessen wird. Wir werden auf diese Weise ein zusammen-
hängendes Netz bekommen, welches von Hammerfest (Fuglenaes), am Eismeere, ausgeht
und durch Lappland, ferner längs den schwedischen Ost- und Südküsten sich erstreckt
und schliesslich über Öresund mit der dänischen Gradmessung in Verbindung steht.
Mit diesem Netze zusammenhängend, geht ein zweites Netz längs der schwedischen
Westküste nach Norden, bis es sich an die norwegischen Dreiecke anschliesst und
durch diese weiter gegen Norden fortgeführt wird. Schliesslich geht in beinahe ost-
westlicher Richtung quer durch das Land noch ein für die Gradmessung abgesehenes
Dreiecksnetz, welches, auf norwegischer Seite fortgesetzt, Stockholm mit Christiania und
Bergen (an der Westküste Norwegens) verbindet.

Längs den Dreiecksreihen sind sechs endgültige Grundlinien gemessen worden,
von denen zwei den Dreiecken nördlich von Gefle angehören.

Telegraphische Längenbestimmungen sind ausgeführt zwischen Stockholm und
Copenhagen, Stockholm und Christiania, Stockholm und Abo. Hierzu kommt die
zwischen den Sternwarten in Berlin und Lund ausgeführte Längenbestimmung.

Endgiiltige Azimuthe sind bis jetzt nur in Stockholm. und Lund ausgeführt.

 

=
=
a
a
>

>
=

[77

al

jas hah ake, a Mb LA al dt pus mms à

2. s0=ustttæs name DA |

 
