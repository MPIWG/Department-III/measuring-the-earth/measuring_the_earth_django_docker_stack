 

|
4

oe

Les valeurs de C et y que fournissent les données et conditions précédentes, ont pour
logarithmes
1.0 = 8.65758, 1. y = 3,40578.

À l’aide de ces données, on obtient les résultats suivants:

 

Peo Ay Ke. Den ee: MER
0° 0! 0.0 000 10! on 11999:
0.10 190) RTE, 1.10 16.1 19087
0.20 D 235 1.20 ou 0021400
0.30 2 1.30 115 using
0.40 Dug 1 SE 7208 1.40 ga u
0.50 DPA 109, 43 1.50 Shan 17340
140 19:4 11.99 2.0 718 I

(N. B, L‘’ — L change de signe avec L — L,; À — A, conserve le signe —.)

Les valeurs de L’ — L montrent comment les effets des attractions locales sont
supposés se succéder; les valeurs négatives de À — A, expriment la quantité dont la
surface +de niveau s’abaisse au-dessous de la surface menée par le sommet de la
protubérance liquide, parallèlement au sphéroïde de révolution.

Bien que ces résultats reposent en partie sur des données conjecturales, il ne
paraît pas douteux que les variations d’altitude de la surface de niveau n’atteignent
de 10 à 20 mètres dans le Caucase.

Voici un autre résultat bien plus extraordinaire, qui a été obtenu par
Mr. de Benazet, ingénieur des constructions navales, et communiqué récemment au
Bureau des Longitudes par cet ingénieur.

Mr. de Benazet, voulant connaître la déviation du pendule au Callao, dans

l'Amérique australe, s’est trouvé obligé, en l’absence de chaînes de triangles reliant les

deux versants des Andes, de calculer directement les attractions produites par le
continent entier de l’Amérique du Sud. A cet effet, il s’est procuré des renseignements
approximatifs sur le relief du sol et les densités, sur la variation de profondeur de la
mer. Mr. de Benazet ne s’est pas contenté de calculer la déviation du pendule au
Callao, déviation qu'il à trouvée dépasser plusieurs minutes; il a calculé les ordonnées
du profil de la surface de la mer dans la direction perpendiculaire à la côte, au Callao.
Or il a trouvé que la mer s’abaisse progressivement, à partir de la côte, d’une quantité
qui finit par rester constante, et atteint alors 137 mètres! La plupart des données du
calcul de Mr. de Benazet sont affectées d’incertitudes; mais il paraît cependant admissible
que le résultat ne soit pas affecté d'erreurs dépassant le 1}, ou le '/, du chiffre auquel
il est parvenu: on peut ainsi regarder comme très probable que, dans une région
donnée du Pacifique, les attractions du continent de l'Amérique du Sud produisent,
dans le voisinage des côtes, un exhaussement atteignant 100 mètres ou plus encore.

a aa À: dd Li

O1

I

tt

(abet tui |) il

I a |

ju no Le a Mat a LA ba ld agai yg in à:

 

 
