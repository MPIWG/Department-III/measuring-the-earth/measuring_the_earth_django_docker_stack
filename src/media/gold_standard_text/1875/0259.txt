Fc

|
|

|
IN

 

Gene

 

}

|
Î

N

N

 

 
 

Ke mesures du pendule.

 

ur

 

 

 

  

  
 

 

 

 

 

 

 

 

Jahr und Monat. Apparat. 2 lz Veröffentlicht. Titel der Publication. Bemerkungen.
Année et Mois. Appareil. = Ss» Publie. Titre de la publication. _ Remarques.
ES SS ES om
74, Mai Id: SB. : >
En ime Se : B. A Ver ee
1874, Septbr., Octbr. | desgl. IB. ots dem Bone, a Note
1874, Septbr., Octbr. | desgl. - |B. malmeter Nr. 1605, dessen
1874, October . desgl. . |B.| Correction gegen das
1875, Mai desgl. . |B. Meter provisorisch mit
1875, Juni . . |. desel. abe -+0""0401 angenommen
1875, November . . | desgl. SB. ist. Die Vergleichung
wurde 1874 im März und
November durch Prof.
Förster ausgeführt.
1826—1828 . ‚Bessel’scher Pendel-| . Abhandlungen der Akademie der Wissen-
apparat. schaften zu Berlin pro 1826. Berlin 1829.
1829—1830 . desgl. Astronomische Nachrichten Nr. 937— 945.
1835 | desel. Abhandlungen der Akademie der Wissen-
| schaften zu Berlin pro 1835. Berlin 1837.
1870, August, Septbr. | desel. Publication des geoditischen Instituts:
| Beobachtungen mit dem Bessel’schen
| Pendelapparate in Königsberg und
Güldenstein. Hamburg 1874; sowie
Astron. Nachrichten, Bd. 86, Nr. 2056.
1871, Mai, Juni desgl. > à a
1869, Juni . . | Repsold’sches Re- Publication des geodätischen Instituts:
|  versionspendel. Astronom.-geodätische Arbeiten in den
| Jahren 1872, 1869 u. 1867. Leipzig 1874.
isd, Auosst . . . | desgl. 5 ee Ss 5
1869, September . desgl. >: ee > 4
1869, October = desgl. 29 99 99 29 29 DD: 92)
1870, Mai desgl. Publication des geodätischen Instituts:
| Astronomisch-geodätische Arbeiten im
| Jahre 1870. Leipzig 1871.
870, Jun: . desgl. le 3 = 5 Se = =
1870, Juli desgl. ie 5 5 is ; = .
1869, Juni, Juli Lohmeier’s Rever- |B. > = > = > » | Noch nicht berechnet.
sionspendel.
1869, Novbr., Decbr. | desel. ne = ss > ; . 5 dto.
1870, Juni . : desgi. ee 5 5: > 5 n = =... dee,
D desgl. SB. = > “ = > „ .| Projectirt.

Herr Sawitzsch hat zahl-
reiche Pendellängen be-
stimmt.

1869, Juli—Novbr. . | Repsold’sches Re- Noch nicht definitiv be-
versionspendel. rechnet.
SS .: desgl. dto.
1871, Juni— August . desgl. an dto.
1865, 1866, 1871. Pendule & réversion.| . Expériences et Nouvelles expériences faites
avec le pendule aréversion. 1866 et 1872.
1867, Juillet, Aoüt . dto. Nouvelles expériences faites avec le pen-
dule à réversion. 1872.
1868, Juillet, Août . dto. . |\Observations faites dans les stations astro-
1869, Juillet, Août . dto. . |. |f nomiques suisses. 1873.
oe : |
a:
IB.
.ı+B:

 

 

 

 

 

 
