er ger

 

Ham

WORT ren

PIF

WTP

TTT

POPPI TR RT TT aT Perr rere

TRF

V

Berathung der Frage über Lokalanziehungen |
und Lothablenkungen” © 2220 00 20 |
Ansprache des Herrn Unterrichtsministers, |
der der Sitzung beiwohnt . .:. “1.70

Sechste Sitzung.

Berathung über einen Azimutalkreis des fran-
zösischen Depöt de la Guerre, construirt
von den Gebriidern Brunner in Paris,. . 72

Bericht des Herrn Perrier über die Resultate,
welche in Frankreich durch die Ver-
gleichung von Tag- und Nachtbeobachtun-

 

gen gewonnen worden ind + . . . .. 73 |
Anträge des Herrn Adan in Betreff der vor- |
laufig zu gebrauchenden Elemente des
Sphäroids, betreff der bei der Generalkarte
zu wählenden Projection, und über die
Wahl eines Ausgangspunktes für die geo-
dätischen Rechnungen. Discussion dieser
AREAS „2. N.
Beschluss des Bureaus über die Publikation
der Protokoille #1... 76
Verschiebung der Wahl des Ortes für die
nächste Versammlung auf nächsten Früh-
Re rn
Schlussworte des Präsidenten und Dankes-
votum an die französische Regierung . . 77
Siebente Sitzung.
Unterzeichnung.der Protokolle .. .. 7%
Anhang. 1. Gutachten der Pendel-
commission.
1. Gutachten des Herm, baeyr 0, . u83
2. Gutachten des Herrn Bruhne ....... sı
3. Gutachten der Schweizerischen Commis-
sion See ee
4. Gutachten des Herrn von Oppolzer . . 85

5. Gutachten des Herrn C. A F. Peters . 88 |

Annexe I. Rapport de la Commission

du Pendule. pag.

1. Circulaire de Mr. Baeyer, président . . 90

2, Kapvort dé Mr brune 2: 93
3. Rapport de la Commission un de

là Suisse 0 à ee

4. Rapport de Mr. dGepdner ii VET OE

5, Rapport de Mr CO: AH Peters; =. m»

Annexe II. Convention du Metre.

Artiles 4 11... 2. 2.2.2.2... 102

Reglement . . ... 2... 20. 301
Annexe III.*) Dispositions transitoires.

Articles. 1-6 %. . 22. 2.2.0... 32000024103

Annexe IV.

Nouveaux théorèmes sur les attractions lo-
cales et applications à la détermination de
la vraie figure de la Terre, par Mr. Yvon
Nillarceau” votre al
Second théoréme sur id ae fa
et premiére détermination de la vraie figure
de la Terre, fondée sur la comparaison
des nivellements oe et géomé-
triques 7. „218
Troisiéme em sur ies nn le
et seconde détermination de la vraie figure
de la Terre obtenu sans le concours des
nivellements proprement dits . . . . . 120
Nouveau mode d'application du troisième
théorème sur les attractions locales au
contrôle des réseaux géodésiques et à la
détermination de la vraie figure de la Terre 133
Remarques concernant l’emploi des series
trigonometriques dans la representation des
effets des attractions, et l’integration de
l’Equation differentielle des surfaces de
niveau... ren ee en

| Annexe V.

Etude comparative des observations de jour
et de nuit, par Mr. le commandant Perrier 144

*) Hier ist das Versehen begangen, die zur Meter- Comvenies gehörigen „Dispositions tran-

sitoires” als besonderes Annexe III zu geben.

Par erreur, on a séparé et publié a part,

qui font partie de la Convention du Metre.

comme Annexe III, les „Dispositions transitoires“

 
