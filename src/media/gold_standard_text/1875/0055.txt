a TY WETTER. TYTN nn

R
£
È
F
8
k
t
=
S
5
t
t

 

45
Die besonderen Commissionen, denen man in Dresden aufgetragen hat, die
Fragen wegen des Pendels und der Basis-Apparate zu studiren, werden sich verständigen
um uns Vorschläge zur Beschlussfassung vorzulegen.
In Betreff der Thätigkeit des Central-Büreaus wird Herr General Baeyer seinen
Bericht erstatten.“
Herr General Baeyer bittet Herrn Hirsch, in seinem Namen folgenden Bericht
über die Thätigkeit des Central-Büreaus und des geodätischen Institus in Berlin vorzulesen:

Bericht

des Central-Büreaus und des geodätischen Instituts in Berlin über die
im Sommer 1875 ausgeführten Arbeiten.

A. Geodätische Arbeiten.

{. Die Dreieckskette, welche von Berlin durch Thüringen und Hessen bis an
den Rhein führt, wo sie sich in der Seite Taufstein-Dünsberg mit der Rheinischen
Kette vereinigt.

Auf einigen Punkten der seiner Zeit von Gerling gemessenen Dreiecke sind in den
Winkeln Abweichungen gefunden, welche sich nur dadurch erklären lassen, dass die
später errichteten Beobachtungs-Pfeiler mit den alten Stations-Punkten nicht identisch
sind. Da die älteren Centrirungs- Elemente nicht bekannt sind, so blieb nichts übrig,
als die Winkel auf der Mehrzahl der Stationen neu zu messen. Im laufenden Jahre
hat Professor Sudebeck mit seinem Assistenten Dr. Lamp die Beobachtungen vollendet,
welche noch auf dem Taufstein auszuführen waren, ferner die Stationen Knüll und
Milseburg erledigt und auf dem Meisner die von ihm früher ausgeführten Beobachtungen
durch die Festlegung der Richtung nach dem Taufstein vervollständigt. Dergestalt sind
die Winkelmessungen in dieser Kette so weit vollendet, dass nunmehr die definitive
Rechnung unternommen werden kann.

2) Die Rheinische Kette, welche den Rhein entlang von der Grenze zwischen
Belgien und Holland bis zum Grossherzogthum Baden aufsteigt, wo sie sich zunächst
mit der Triangulation von Württemberg und darauf mit dem Schweizer Netze vereinigt.
Professor Bremiker und seine Assistenten Dr. Fischer und Dr. Winterberg, welche an
dieser Kette arbeiten, haben zunächst die Beobachtungen beendigt, welche noch auf
dem Erbeskopf und dem Donnersberge auszuführen waren und es ist wahrscheinlich,
dass es ihnen noch in diesem Jahre gelingen wird, die neuen Stationen Ketterich und
Hörnisgründe zu vollenden. Ausserdem ist die Vereinigung mit der neuen Triangulation
von Württemberg in der Art festgestellt worden, dass mit den dem Schweizer Netze
gemeinsamen Seiten ein grosses Polygon um den Feldberg herum gebildet wird.

 
