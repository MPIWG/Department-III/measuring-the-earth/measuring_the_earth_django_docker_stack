 

De

Au moyen de ces valeurs, il vient

f Gece
de = — “ya” sin Lcos £ dL — acosisin £ df,
2 22
iy es. dy = — — sin L sin dL + à cos À cos £ d£ ,
De v3 5
2 2
dg = + — cos Ld i.
Transportons ces quantités dans l’&quation (5), nous aurons d’abord
Dane
iL, = — =. [sin L‘ cos L — sin L cos L’ cos (£ — £ )]dL
— acosacos Lisin (€'’— £)dk,
a Bee. ;
(18) | dA=— ya em > E) 2 sn L cos L’ sin® 4 (— 2 )dL
| — a cos À cos L'sin (£ — £ )d£.

Cette équation est rigoureuse; mais on peut la simplifier, tout en lui conser-
vant une exactitude plus que suffisante. En effet, les différences L'— L et ! — £
sont généralement égales à un petit nombre de secondes, et, dans les cas les plus
extrêmes, elles ne paraissent guère dépasser 1 ou 2 minutes d’arc: cela permet d’en
négliger les carrés et les puissances supérieures. Afin de pouvoir conserver les diffé-
rences L' — L et £' — £ exprimées en secondes d’arc, nous mettrons, à la place de
leurs sinus, leurs produits par sinl”. Posons, en conséquence,

k= — asin",

F (L, oe De F0, &)— 100$ 2,008 L'(L— L),

l'expression (18) deviendra

(19)

aA
FDA eE
et coincidera avec l’expression (1) qu’il s’agissait de former.
On remarquera que, a cause du faible aplatissement du sphéroide, L et A

2
° , ac e r .,7 3 . .
differeront peu, et le facteur ys sera peu différent de l’unité: il s’ensuit que la fonction

F (L, £) sera sensiblement égale à L'— L. Quant a la fonction f (L, £), on voit
que, étant divisée par cos À, elle représentera la différence de longitude (¢/ — ¢ )
réduite en arc de grand cercle. On devra avoir égard à cette circonstance, dans le
calcul du développement de la fonction f (L, £ ).

De ces remarques il résulte que l’équation de condition (2) revient sensiblement à

a... A a +,

*) P. S. L’équation (20 bis) constitue le troisième théorème sur les attractions locales. Dans.

le Mémoire suivant, le même théorème sera présenté sous une autre forme et ses applications reposeront

 

à
a
a
ü
=
>
=
x

pub 10 ed 1

ak ini

Lu chi hs ad dan db pau pus à

2. s4ttittéaienns, misst DEA |

 
