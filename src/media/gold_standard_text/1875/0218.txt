 

an
tember zwei der in den Jahren 1874 und 1875 benützten Reversionslatten*) — Thei-
lung auf beiden Breitseiten — auf ihre absolute Länge geprüft. Diese Prüfung ergab

für beide Skalen der Latte IP:
1™ = 17000082 + 0™=016 und 1” = 17000066 + 02007
und als mittleren Fehler eines Theilpunktes: + 0™™064 und + 0™™028; fiir beide
Skalen der Latte 2P:
1” — 17000678 + O™™009 und 1” = 1"000642 + 0™™009
und als mittleren Fehler eines Theilpunktes: + O™™037 und + 0039.

Die neuen Latten sind hiernach mit einer Genauigkeit getheilt, die allen
Anforderungen genügt.

Schliesslich folgen noch die Seehöhen einiger neuen Festpunkte des Nivelle-
ments von 1875 bezogen auf das Mittelwasser der Ostsee bei Swinemünde (2"4258 unter
der als Fixpunkt dienenden Plinte am Hause Nr. 21, früher Nr. 2, an der Ecke der
Königsstrasse in Swinemünde).

Höhenmarke am Stationsgebäude der Cöln-Mindener Eisenbahn zu Düsseldorf 39679

: = je „ 1 ks so VOMIOOe 2.7 22.102
Nullpunkt des Meterpegels an der Schiffbrücke zu Côln . . . . . . . . 36.115
eee ee ee’ Sicinwarte zu Bonn . .» : 2... 2 ... . . . . OL91I1
Nullpunkt des Meterpegels zu Bonn . . . m; _ wy 40-809

à = is am Krahn zu ion a ce 01.022
a = Kuspesek an der Schitibricke zu Coblenz .… .. … . . « 597.772
ss oF 2 u eee ke ie. A 09.512
2 re a Bad. . . .. . ©. +. «2 . . . Ol.721
es ss eu 638.862
ss >> 5 oa 0 zen, 0... 66.358
Li 5 ss cn ern vin: 68.645
a 3 ” peeimavem ey tl. i Bee ac ach 10298
oo is > „ Manz . . ol Mor)
hs » alten Meterpegels zu nat a. M saut sut OO
Ss » neuen is = ee 0015
» » Meterpegels zu La er a. orte)
Höhenmarke am östlichen Portal der Rheinbrücke zu donc Pa ee 104666

is - Hopiporlal des Munsters zu Strassburg . . . . . . . 145.021

53 = tationscebiude zu Kehl . . . . P ....... 142.182
Französische Höhenmarke am Stationsgebäude zu lese sn 5 240.099
Schweizer Höhenmarke Nr. 46 am Üentralbahnhofsgebäude zu Dci ee
Höhenmarke am Badischen Bahnhofsgebäude zu Basel. . . . . . . . . 258.345

*) Bezüglich der Construction dieser Latten wird auf die bereits im Druck befindliche Publi-
cation der diesseitigen Nivellements verwiesen.

 

à
a

II TEL

141

ie am db ha ba a ba nm

 
