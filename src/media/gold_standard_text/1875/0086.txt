 

 

16

hin, welche die Ausführung einer solchen Karte mit sich bringt; und findet es für
jetzt hinreichend,. wenn. die, verschiedenen Staaten ihre Dreiecksketten nach den Mes-
sungen stets vervollständigten. Später wenn die Arbeiten sich: ihrem Abschlusse nahen
werden, wird eine Generalkarte in Sectionen angefertigt werden können.

Herr. Hirsch tritt ‘der Ansicht .des’ Herrn de Vecchi bei, weil es sich gegen-

wärtig nicht um eine genaue Karte, sondern nur um ein allgemeines Bild handelt und
am vortheilhaftesten würde es jetzt. sein, an dem über diesen Gegenstand angenommenen
Antrage festzuhalten.
Da diese Ansicht überwiegend ist, so: wird .die Discussion ‚über diesen Punkt
geschlossen.
Herr Villarceau. kommt nochmals auf das von Herrn Adan Gesagte zurück und
frägt, ob es nicht zweckmässig ‘sei, die Frage der Wahl eines Centralpunktes, welcher
als Ausgangspunkt für die geodätischen Rechnungen dienen soll, einer Specialcommission
zu überweisen ?

Herr. General Baeyer. glaubt, dass es. bei dem gegenwärtigen Stande der Arbeiten,
da’ die Ketten noch nicht unter einander verbunden sind und die Mehrzahl noch nicht
endgiltig. berechnet, unmöglich. ist, -den Centralpunkt auszuwählen.

Herr Villarceaw ist mit dieser Erklärung zufrieden gestellt und bittet, dass
die Arbeiten beschleunigt werden möchten, um .zur Verwirklichung .des Wunsches zu
gelangen.
. 1 Herr Perrier, theilt mit, dass Herr Gldssener aus ‚Brüssel einen Apparat, nach
Paris gesendet hat, welcher zum Registriren yon Sterndurchgingen dienen soll. Dieser
Chronograph ist. bei Herrn Rumkorf aufgestellt, wo. man ihn in Augenschein nehmen kann.

Der Präsident zeigt.an, dass die Protokolle ‚dieser Sitzungen in extenso zugleich
mit dem Generalbericht veröffentlicht ‚werden‘ sollen. : Die abwesenden Commissare
werden ersucht werden, ihre Berichte bis Ende- des. Jahres einzusenden, und die anwesen-
den Mitglieder werden ‚gebeten, ihre hier vorgetragenen Berichte durch Hinzufügung der
in den noch übrigen drei Monaten dieses Jahres ausgeführten Arbeiten zu vervollständigen.

In Betreft der Wahl des Ortes, wo sich die permanente Commission: im kom-
menden Jahre versammeln werde, frägt der Präsident an; ob die Entscheidung erfolgen
oder bis auf nächstes Frühjahr aufgeschoben werden solle; im letzteren Falle :würde eine
schriftliche Abstimmung eintreten.

Mit 3 gegen 1 Stimme wird die Entscheidung bis nächstes Frühjahr aufgeschoben.

Da das Programm der jetzigen. Session ‚erschöpft ist, spricht der Präsident. vor

dem Schlusse ‚der. Sitzung im Namen der Commission folgende Schlussworte:
Es. sei; mir. gestattet,: den. Mitgliedern, dieser Versammlung. für. ihre Berichte,
Mittheilungen und Vorschläge, so wie auch, fir, die Betheiligung an unseren, wissen-
schaftlichen Debatten und die auf diese Weise: bewirkte Förderung: unseres gemeinsamen
‚Werkes meinen’ Dank auszusprechen. ‚Ich. danke gleichfalls (allen  eingeladenen Mit-
gliedern, welche. unsere Arbeiten mit ihrer Gegenwart beehrt und an unseren Berathungen
theilgenommen haben.

 

x

a A hak

ne

Ih nl i

co Ill

à us ame A u hd nn 0
