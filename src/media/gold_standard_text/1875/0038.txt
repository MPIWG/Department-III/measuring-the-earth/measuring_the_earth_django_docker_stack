 

 

28

réunis en Commission spéciale, ont été chargés d’élaborer un projet de Convention.
Après avoir discuté, dans une série de séances, deux projets différant essentiellement
au point de vue de l’organisation à donner et de la durée à assigner au Bureau des
poids et mesures, la Commission spéciale à soumis à la Conférence ces deux projets,
un de majorité, l’autre de minorité; c’est le premier qui à été adopté par la grande
majorité de la Conférence dans sa séance du 15 Avril, où l’on a parafé la Convention,
qui a été signée définitivement dans une dernière séance, du 20 Mai, par les représen-
tants des 17 Etats suivants: Allemagne, Autriche- Hongrie, Belgique, Brésil, Confé-
dération Argentine, Danemark, Espagne, Etats-Unis, France, Italie, Pérou, Portugal,
Russie, Suède et Norvège, Suisse, Turquie et Venezuela. L’échange des ratifications
a été fixé au 20 Novembre 1875 et la mise en vigueur de la convention au 1% Janvier
1876. — Toutefois le Comité international des poids et mesures, chargé par la Con-
vention de la Direction supérieure du Bureau international, a été autorisé à se con-
stituer des le 15 Avril et & faire des études préparatoires. |

Usant de ce droit, le Comité international des poids. et mesures, qui était
composé des 14 membres suivants: MM. Förster, Herr, Stas, Ibanez, Hilgard, Morin,
Chisholm, Govi, Bosscha, Wild, Wrede, Broch, Hirsch et Husny-Bey*), s’est con-
stitué le 19 Avil, en nommant Mr. Ibanez Président et Mr. Hirsch Secrétaire. Mr.
Govt a été désigné provisoirement pour remplir les fonctions de Directeur du Bureau.
Dans d’autres séances, le Comité s’est occupé du choix de l’emplacement pour le Bu-
reau international; il a donné la préférence au Pavillon de Bréteuil, dans le Parc de
St Cloud, et à chargé son Bureau de traiter avec le Gouvernement français de l’acqui-
sition de ce local. Enfin le Comité s’est livré, dans plusieurs séances, à une première
discussion des instruments et appareils qu’il faudra faire construire pour le Bureau;
apres avoir fixé en principe la construction de 5 comparateurs et d’un certain nombre
de balances, l’étude spéciale de chacun de ces instruments a été confiée à plusieurs
membres du Comité qui doivent faire des propositions plus tard. Peu après la mise en
vigueur de la Convention, le Comité se réunira pour arrêter les plans de construction
du Bureau et pour commander les instruments et appareils nécessaires: de sorte qu’on

x

peut prévoir que le Bureau international pourra commencer à fonctionner au prin-
temps de 1877.

Après ce court exposé historique, il nous reste à relever, dans la Convention
des poids et mesures, — dont il serait utile de publier le texte comme annexe du pro-
chain Rapport général de l’Association **), — les articles qui interessent plus parti-
culièrement la géodésie. L’art. 6 de cette Convention, qui definit les fonctions dont
le Bureau international sera chargé, comprend parmi celles-ci: ,1’étalonnage et la
comparaison des regles geodesiques“, et en outre „la comparaison des étalons et échelles

 

*) Deux parmi les membres désignés, MM. Chisholm et Bosscha ont déclaré plus tard ne pas
accepter leur nomination, de sorte que le Comité se compose actuellement de douze membres.

**) Voir la Convention, le Réglement et les dispositions transitoires dans l'annexe No. 2 et 3.

 

~
=
=
=
=
a
=
È
=
=
x
ü

ak nik il

by to

wid

À as dh hn lad dada ll pau sn à

 
