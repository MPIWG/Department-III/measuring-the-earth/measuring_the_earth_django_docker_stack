Oy

Le chiffre de la population exprimé en millions, sera multiplié wi
par le coeffieient 3 pour les Etats dans lesquels le — mé on
obligatoire ; See aap.
par le coefficient 2 pour ceux dans lesquels il n’est que - facultatits,
par le coefficient 1 pour les autres Etats.
La somme des produits ainsi obtenus fournira le nombre ie par Ya
la dépense totale devra étre divisée. Le quotient donnera le montant de ur
depense.

is DL.

Les frais de confection des prototypes internationaux, ainsi que des étalons et
témoins destinés à les accompagner, seront supportés par les Hautes Parties contrac-
tantes d’après l’échelle établie à l’article précédent. |

Les frais de comparaison et de vérification des étalons demandés par des États
qui ne participeraient pas à la présente Convention, seront réglés par le Comité con-
formément aux taxes fixées en vertu de l’article 15 du Règlement.

ARD. 29:

Le présent Règlement aura même force et a que la Convention. à | ad
il est annexé.

 
