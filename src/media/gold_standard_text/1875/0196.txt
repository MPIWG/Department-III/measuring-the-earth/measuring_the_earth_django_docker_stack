 

x
=
a
a
a
=

a

186

11a bi)

tat

den Herren Baron Hans von Rühling und Johann Strobl gewonnen, die im gegen-
wärtigen Augenblicke fast schon das grosse Beobachtungsmaterial der letzten Campagne
bewältigt haben. |

Die Reduction der Längenbestimmung Paris— Wien ist vollendet und der Druck
der französischen Ausgabe in den Annalen der Pariser Sternwarte begonnen; ich benutze
hier die Gelegenheit, um die Resultate der einzelnen Abende nach Anbringung der
persönlichen Gleichung nebst den zugehörigen Gewichten hier mitzutheilen.

 

 

À Gewicht
1873 August 23. D6% 0514 0.4 Oppolzer in Wien, Loewy in Paris. =
i ADS EOIOS 0.4 3 re MANU,
a 2 00 0.4 à BR, ME
I DOI END 0.5 33 ey fe ONU]
eset. 927 55:59:99 0.5 5 ip as ee, -
i is Le OO 10306 0.2 Loewy ae Oppolzer’s; >), =
ae en 110$ 5 Wi); Be I 5 -
a Hi 19. 80-005 0.5 5 he PE ran), Ce E
5 5 1D E56 O07 0.6 ii No ante 28), :
3 20s, 6. 0-08 0.5 a a DDR
l : 2. 28. 00,020 0» à ne Ei eh
: a ty 06 007 0.6 55 Se eet as
| ; | 020,790, 0:02 0.6 5 Beer N
u mi eer 56 .0.01 0.6 is sale oi
| .. Ockbr...:8. 06, O12 0.6 Oppalzer, „ HO...
| .- 1.8 085». o7 n ae 0,
| . es 15 » 012 0.7 . ns ee
| ° dE 50 0.04 0.7 = „m ae à
| 567 ,0:07,,25,0:010
| Reduction auf den Meridian
| von Frankreich 015
EL Wien (Türkenschanze) — Paris 56% 0:22 + 0501.

Auch die Reduction der Linge Pfander—dZiirich ist so gut wie vollendet. Das
Resultat derselben ist provisorisch mit 4753879 +0803 bestimmt worden, die definitiven
Zahlen werden sich nicht wesentlich von den vorstehenden unterscheiden.

Die Anlage der Art der Reduction unserer Längenbestimmungen ist eine
derartige, dass dieselbe gleichmässig für alle Beobachtungen vorschreitet; die Resultate
werden daher nahe gleichzeitig fertig werden, bis zu diesem Abschlusse werden aber
noch 2—3 Jahre Arbeit erforderlich sein; doch sowohl bei den Längenbestimmungen, |
; als auch bei den übrigen Operationen (Breite-, Azimuth- und Pendelbeobachtungen) |

 
