 

178
Ponza, Ventotene, Anzio, Circeo, Petrella, Cornacchia, Semprevisa, ed i punti
intesi a legare la base di Napoli alla grande rete, cioe:

Camaldoli, Ufficio Topografico, Osservatorio di Capodimonte — Granili, Candele,

 

Somma Cancello. —

Il quadro*) esprime a colpo d’occhio lo stato della Geodesia Italiana per quello
che riguarda la misura de’ gradi. Non vi manca se non la base di Udine, operazione
per adesso isolata; e quella di Napoli non ha potuto esservi inserita per la piccolezza
della scala.

Pubblicazioni.

8° Per conto della Commissione italiana si attende alla pubblicazione in
Fascicoli distinti per parte geodetica e parte astronomica dei lavori eseguiti in Italia
relativi alla misura dei Gradi. :

E uscito il 1° Fascicolo della parte geodetica ed & in corso di stampa il
1° Fascicolo della parte astronomica, pubblicati per cura dell’ Istituto Topografico
Militare.

Vennero altresi pubblicati e distribuiti in Fascicoli a parte per cura dei
Professori Schiaparelli e Lorenzoni i lavori di longitudine e latitudine da loro eseguiti
negli scorsi anni.

Il Prof. Betocchi sta preparando una pubblicazione relativa alle osservazioni
mareografiche eseguite dal Ministero dei Lavori Pubblici.

Progetti per l’anno venturo.

Salvo le modificazioni che potranno essere suggerite nella presente riunione
della Commissione permanente, ed in quella della Commissione italiana che avrà luogo
in Novembre a Milano, sarebbe mia intenzione che nel prossimo anno si procedesse
ai lavori geodetici per il rattacco della Sicilia con l’Africa all’ esecuzione di una o due
stazioni astronomiche per la determinazione di latitudini ed azimut; e ad alcune livellazioni
di precisione per collegare i mari italiani col mar Baltico e col mare del Nord,
rattaccandosi alle livellazioni Svizzere ed alle Austriache, ed approfittando delle
osservazioni flussometriche, di cui il Commissario Prof. Betocchi assumerä la direzione.

Inoltre si continuerä regolarmente la pubblicazione dei lavori già eseguiti. —
Per questi lavori sono assegnate sul bilancio del 1876 circa Lire 30000. —

Comnussione Italiana per la misura del Grado Ewropeo.

*) Vedi carta Generalbericht 1874.

 

x
=
à
x
a
=

11411 88

| el

jt Di |

À à mel Mas Ah edd lh hy

2. ssemtssauæs mins di |

 
