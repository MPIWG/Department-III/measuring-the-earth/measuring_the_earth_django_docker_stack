“1 = RET PPPOE PPP Tyme Tf

regt

mern

ame DETTE BTS

k
è
=
=
=
F

5

x

DD

Herr Hirsch: Es handelt sich um drei wohl zu unterscheidende Dinge:

1. Der Catalog der geodätischen Publicationen ;

2. das Verzeichniss der gut bestimmten astronomischen Coordinaten ;

3. Herstellung einer allgemeinen Dreieckskarte und einer Nivellementskarte.

Das Central-Büreau hat im Monat November 1874 an die Commissare ein
Circular geschickt, um sie zu bitten, die nothwendigen Documente einzuliefern: Nur drei
Staaten haben auf das Circular geantwortet, Herr Hirsch hat daher Anfang August die
Bitte wiederholt, und fast alle Staaten haben geantwortet.

l. In Betreff der ersten Frage ist das Tableau der Publicationen ziemlich voll-
ständig; es umfasst in diesem Augenblick ohngefähr 220 Nummern. Wenn dieses Ver-
zeichniss gedruckt wäre, würde es schon grosse Dienste leisten können.

Herr Hirsch schlägt vor, dem Central-Büreau die Veröffentlichung aufzutragen..

2. In Betreff der zweiten Frage sind die von den verschiedenen Ländern ein-
gereichten Materialien noch nicht vollständig genug; überdies haben einige Länder noch
keine Daten geliefert. Herr Hirsch hat das von Herrn Bruhns vorläufig aufgestellte
Tableau erweitert, aber es ist nicht hinreichend vollständig, um veröffentlicht zu werden.

Herr Hirsch schlägt vor, diese Frage dem Central-Büreau zu überreichen, da-
nit es den Commissaren Formulare zur Ausfüllung übermittelt, für welche er folgende
Schemata in Vorschlag bringt.

 

 

 

 

 

 

 

 

 

Längen.
m a | ee ae 4 ue em
| 3 | | | |
= Längen- > | | Verôffent |
S| Unterschied | ro cdalay A à Ne bBes | | |
2 | : | | = licht. | |Beob- Pro- | Bemer-
| Stationen, ==... pes | Beobachter | , und ni til rech} Ce)
= | Werth [Wan | Monat | _Fitel der |, |achtet/jectirt! kungen
Sı is : | | ) . . | | |
= | in scheinl. | | Publication | | |
: = | | |
© Zeit Fehler | | LA |
Polhöhen
2 Ls | Bice eae eb es
se | = mt 2 | Bd | |
& | Polhdhe | 3 es 820g | Veröfent- © ©. |
> | on. Ei eee ae eee dodo de Te
SO eee. | | Beobachter = 28 |338a| TODD I es (ene emer-
= AAT | obachter |Z | So ; | s | ©
= | Station Wahr- | e A | 22 ‘Bs os | Titel der | 2 4 = kungen
& | NT erheni:! [RUE | £ | . . | g
| Werth ‚scheinl- = SS | S.à ep | Publication | S &
S| ‚Fehler | Le) à ER "ri | | |
| | el | ee | |
Azimuthe.
frein 2 \ = : | |
- = à | |
a ie Azimuth S| a) Veréffent- (8 | 2)
= ‚Benennung Zu ; Erlen ne
n ol vee = ra > licht = = > B
=| Statio der |Wahr- | Beobachter | 2 ib ee
| Station ay Vahr- | ailes le S| Oh]
= Richtung | Werth .scheinl BR a a | ac | 3 er
3 5 erth scheinl. 5 \@\\ 'Publieaätion a a "a
© | | Fehler = | |

 

 
