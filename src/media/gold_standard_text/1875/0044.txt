 

 

34

Mr. le Col. Laussedat est heureux de constater les bons résultats des observations
de nuit. — Il s’était aperçu de l’excellence des images de nuit en faisant ses expériences
de télégraphie optique. — Il offre aux membres de l'Association une brochure qui donne
l'historique des signaux télégraphiques.

Il présente son appareil, servant aux signaux de nuit pour la télégraphie optique.

Mr. d’'Oppolzer a pris grand intérêt au rapport de Mr. Perrier, d’autant plus que
lui-même a recommandé les observations de nuit, il y a 4 ans, en Autriche. — Il avait
proposé, pour signal de nuit, une appareil à lentilles de Fresnel, qui offre l’avantage
de ne pas demander à être pointé dans une direction déterminée, et qui se voit
très-bien à l’oeil nu à 30 Kilom. :

Tl demande a Mr. Perrier si des mesures ont été faites pour déterminer les
variations zénithales des signaux de nuit.

Enfin, puisqu'il a la parole, Mr. d’Oppolzer se permet de présenter une ob-
servation sur la manière de partager la périphérie du cercle pour les origines des
différentes séries d'observations: 1l pense qu'il serait préférable de faire ce partage en
nombre impair; les erreurs périodiques s’éliminent aussi bien, et, comme avec cette
méthode, les microscopes correspondent à beaucoup plus de divisions du limbe, on
éliminerait beaucoup mieux les erreurs accidentelles de division.

Mr. Perrier répond à Mr. d'Oppolzer que les variations dans les angles zénithaux
des signaux de nuit seront étudiées, et que la vis micrométrique de l'instrument qu’on
peut tourner dans le sens vertical, permettra précisément cette étude.

Mr. Villarcoeau demande à compléter l'historique de Mr. Perrier sur les ob-
servations de nuit, en rappelant que ce sont les observations qu’il a faites entre St. Martin
du Tertre et le Panthéon qui l’ont convaincu de l'excellence des résultats qu’on peut
obtenir. — |

Mr. le Major Adan a la parole pour faire une proposition; il s’exprime en
ces termes:

Si J'entends bien la question de la déviation de la verticale en un lieu quelcon-
que, il me semble que trois causes influent sur cette déviation.

La premiére est due aux attractions locales du terrain avoisinant la station

et ces attractions, que Newton déjà avait indiquées, se manifestent tant à cause du relief

que de la constitution géologique du sol. Des opérations de nivellement et la con-
naissance du sol et du sous-sol doivent donc permettre de calculer cette attraction ainsi
que le font déjà depuis douze ans les savants anglais et russes.

La deuxième cause réside dans la forme de la surface de la terre, forme qui,
ft-elle même géométrique, ne permettrait pas l'identification de la verticale avec la
direction des forces attirantes.

Déjà Laplace avait recherché une formule propre à représenter le degré de
méridien sous une latitude quelconque et il ne put arriver qu’à une formule approximative,
impropre à donner l’inclinaison de la normale sur le diamètre.

Les recherches de James entreprises plus tard ne l’amenèrent pas à un résultat

 

=
=
=
=
a
=
N
=
=
x
U

ak mul

ko m 1

 

2.) sos masses A |
