ee TYE |

COTE 7

an UAT

 

 

Beobacht.
Polhöhe.

Ang. mittl.
Decl. 1864, 0-+-Corr. 0.

Biegung. | Gewicht.

 

 

 

 

 

 

a Urs. min. | 88°35! 3°77 50° 47! 36696 | —0/6424b| 60
8 Urs. min. | 74° 42’ 39/99 L0"53 35'762 |—O0"4058b| 27
8 Cephei 69° 57! 4983 + 020 35948 |—0"3296b| 18
8 Gemin. 28° 217 575--0"11 35/177 | --0"3835b| 30
& Coronae | 27° 18' 28/82 — ("70 351178 | 1 0"4033b| 31
æ Aquilae 8° 80! 42/20 — 029 34"955 |-1-0"6735b | 26
a Orionis 7° 22' 4254 4068 35"776 | +0"6885b| 20
a Can. min. | 5°34! 15"00—0"37 34"721 |--0"7101b | 13

225

Behandelt man diese Beobachtungen nach der Methode der kleinsten Quadrate, so

erhält man als wahrscheinlichste Werthe:
Polhöhe = 50°47 35759; Gewicht 223,16; w. F. # 07057
Biegungscoeff. b = 1"0908; Gewicht 68,07; w. F. + 07060

Als wahrscheinlicher Fehler einer auf je einer Einstellung in jeder Kreislage beruhen-
den Beobachtung folgt + 05592.

Der Pfeiler des Universalinstrumentes stand 13,532 W. Klafter = 0"831 südlich von
der Thurmaxe, somit ist die Polhöhe des Dreieckspunktes aus den beobachteten
Circummeridianhöhen:

50° 47’ 36"590; Gewicht 223,76; wahrscheinlicher Fehler + 0"037.

Behufs der Verbindung der an beiden Instrumenten beobachteten Sterne wäre noch
das Verhältniss der Einheiten, auf welchen die oben angesetzten Gewichte beruhen, festzu-
stellen. Ich habe zu diesem Behufe die einzelnen aus jedem Sterme folgenden Resultate mit dem
zugehörigen Mittel verglichen und es ergab sich der wahrscheinliche Fehler einer Beobachtung

am Passageinstrumente —-+ ("5150 aus 118 Beobachtungen

am Universalinstrumente = +0"5135 - 225 -
wonach sich das Verhältniss der Gewichte wie 1: 1,006 stellt; der Unterschied ist so gering,
dass es nicht der Mühe lohnt, denselben zu beachten. Aus den einzelnen Sternen ergeben
sich daher, mit Berücksichtigung der oben erhaltenen Biegung des Fernrohres des Universal-
instrumentes, folgende Werthe der Polhöhe des Dreieckspunktes, mit den beigesetzten Gewichten:

a Persei 50! 4755/1885 Gewicht 56
a Aurigae 37040 21
n Urs. maj. 36631 17
a Oygni 36/7869 24
a Urs. min. 36326 60
& Urs.. min. 36/1190 27
6 Cephei 36'419 18
6 Gemin. 36426 30
a Coronae 36/449 31
a Aquilae 36520 26
a Orionis 37/5598 20
æ Can. min. 36327 13

343
