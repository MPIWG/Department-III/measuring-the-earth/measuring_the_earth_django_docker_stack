Te IV |

a de ee

mich aber der Hoffnung hin, dass das alte Sprüchwort: „Keine Nachrichten sind gute Nach-
richten,* auch hier zutrifft. Meine eigene Gesundheit ist in den letzten zwei Jahren recht
unbeständig gewesen. Die gewöhnlichen Sporteln, welche von Gradmessungsarbeiten einge-
erntet werden, nämlich rheumatische Leiden, sind auch mir reichlich zu Theil geworden.
Stockholm, den 28. Januar 1867.
D. G. Lindhagen.

18. Se h woe 17

Auszug aus dem Protokoll der Schweizerischen geodätischen Commissions- Sitzung
vom 4. April 1866. Präsident General Dufour.

Herr Denzler spricht die Hoffnung aus, dass die Triangulation, mit Ausnahme der
Azimuthal-Bestimmung, im Laufe des Sommers beendigt werden könne, zu der Centralkette
fehlten nur noch die Winkelmessungen auf den Stationen Titlis, Hangendhorn und Basodino
und auf dem Hörnli seien noch einige Winkel nachzumessen. Hierzu kämen dann noch die
7 Stationen der westlichen Kette, Suchet, Berra, Rochers de Naye, Dole, Colone, Trelod und
Colombier. Die Kosten schlägt er zu 4000 frs. an.

Herr Hirsch schlägt vor das Nivellement der westlichen Schweiz ım Laufe des
Sommers zu beendigen. Der Präsident fordert ihn und Herrn Plantamour auf, den Arbeits-
plan specieller festzustellen. Beide Herren halten für nothwendig, dass folgende Strecken
nivellirt werden müssten:

1. auf der Strecke Genf-Morges, die erst einfach nivellirt sei und keinem Polygon ange-
höre, wäre das Nivellement zu wiederholen.

2. Von Morges über Lausanne nach Freiburg längs der Eisenbahn.

3. Von Freiburg über Morat und Sugy (Endpunkt der Basis) nach Neuchätel, um das
grosse Polygon zu schliessen.

Diese drei Strecken haben eine Länge von 190 Kilometer. Dann sei längs der Eisen-
bahn von Freiburg nach Bern, Biel, Neuchätel vorzugehen, um mit dieser Strecke von 100
Kilometer das Polygon zu schliessen. Endlich sei noch das Nivellement von Biel nach
St. Imier (35 Kilometer) hinzuzufügen, um das Polygon Biel- St. Imier-Neuchätel zu bilden.

Die ganze Ausdehnung des Nivellements pro 1866 betrage hiernach 325 Kilometer.
Nach den Erfahrungen des vorhergehenden Jahres würden zur Ausführung desselben 200
Arbeitstage und an Geldmitteln etwa 5000 frs. erforderlich sein.

Die Commission beschliesst hierauf: im Sommer 1866 die Dreiecksmessungen been-
digen und das Nivellement der westlichen Schweiz ausführen zu lassen.

Für die Berechnung der Dreieckskette und der Nivellements stellt die Commission
den Herren Hirsch und Plantamour 3000 frs. zur Disposition und ermächtigt sie ihren

Assistenten für die bereits ausgeführten Rechnungen eine angemessene Gratification zu zahlen,

und erforderlichen Falles auch Hülfsrechner anzunehmen.

 
