HR ee a

KT TTP NE |

(EAGT hL

ART

vw

hu

ILE

lo 4

point donné, au moyen des valeurs fournies par la résolution des équations de condition, on
calculera de proche en proche les coordonnées et la direction du méridien pour les divers
points qui conduisent au lieu (RL). Alors il arrivera que si la figure de la Terre est telle
qu'on l'a supposée, la direction australe du méridien, fournie par le calcul, coïncidera, au moins
approximativement, avec celle du méridien tracé sur le sphéroide; dans le cas contraire, la
direction calculée fera un certain angle u avec ce méridien. Supposons cet angle compté
du sud vers l'ouest: un azimut, rapporté à notre méridien auxiliaire, se trouvera être égal à
Yazimut calculé, augmenté de l'angle u. Convenons, pour plus de simplicité, que Z désigne
désormais l’azimut calculé, nous devrons changer, dans l'équation (2.), Z en Z+u. Alors
cette équation deviendra
(4)  Z—Z+sinL'(Q—R) = u.

„On en conclut que si le sphéroïde peut, dans son ensemble, être assimilé à un
ellipsoïde de révolution, on aura, aux erreurs près des observations, et quelles que soient les
attractions locales,

5.) Z!—Z+smL'(8—R = 0.
S'il en est autrement, la figure de la Terre n’est pas celle d'un ellipsoïde de révolution.

„Les mêmes considérations s’appliqueraient au cas d’un ellipsoïde à trois axes inégaux,
et le résultat « 0 en détruirait la possibilité.

„U est à peine nécessaire d'ajouter que les différences Z/—7Z et ©'—R n'auront pas
besoin d’être effectivement calculées après la résolution des équations de condition, car ces
quantités exprimeront précisément les erreurs de ces équations.

On peut remarquer que si l’exactitude des azimuts géodésiques pouvait permettre de
les substituer aux latitudes *), au lieu de poser des équations de condition de la forme
L'—L—0 ou L—L—0, auxquelles il est généralement impossible de satisfaire, il serait
préférable d'employer la forme unique (5.), puisque cette équation devrait être satisfaite,
quelles que fussent les attractions locales.*

(26 mars 1866.)

Neue Bestimmung des Haupt-Azimuthes zur allgemeinen Orientation der Karte
von Frankreich. Von M. Yvon Villarceau.

Delambre hatte 1799 und 1800 in seinem Observatorium Rue de Paradis au Marais
zu Paris das Azimuth des Pantheons aus 396 Beobachtungen der Sonne am Morgen und am
Abend bestimmt. Verschiedene Verbesserungen, welche in der Delambre’schen Gradmessung
nothwendig geworden waren, deuteten auf eine Abweichung von 19” in dem Haupt-Azimuth

in Paris hin und machten eine neue Messung wiinschenswerth. Das Kaiserliche Observatorium

*) La triangulation anglaise de l’Inde s’y prétera sans doute, des que la détermination astronomique
des longitudes sera effectuée. Cette détermination est déjà commencée.

D *%

 
