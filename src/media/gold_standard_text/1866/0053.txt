Is were

nn |

M ho ART

Nacht rag.

Vorbemerkungen. 1. Der vorhergehende Theil des Generalberichtes ist von dem
Präsidenten des Centralbüreaus der permanenten Commission in ihrer diesjährigen Conferenz
in Wien vorgelegt worden und hat die Commission bestimmt, dass der Generalbericht mit
dem 15. Mai abzuschliessen sei und dass nur die bis zu diesem Termin eingegangenen Be-
merkungen, Berichtigungen u. s. w. in demselben aufgenommen werden können.

>, Das Centralbüreau hatte der im Jahr 1866 in Neuenburg versammelten Conferenz
der permanenten Commission die von Herrn Dr. Börsch und Herrn Kaupert in ihrem Bericht
vom 26. März 1866 ausgesprochenen Bedenken, in Bezug auf die Bestimmung des mittleren
Fehlers in einem ausgeglichenen Dreiecknetz, vorgelegt. Die permanente Commission behielt
sich die Entscheidung vor, und das Centralbüreau versprach, im Nachtrage zu dem General-
bericht pro 1865 (Seite 46. Anmerk.) eine erschöpfende Darlegung des Sachverhaltes bei der
permanenten Commission in weitere Anregung zu bringen. Diesem Versprechen ist dadurch
genügt worden, dass eine unter dem 28. Februar d. J. an das Centralbüreau eingereichte
Abhandlung des Herrn Dr. Börsch, über den erwähnten Gegenstand, der permanenten Com-
mission bei der diesjährigen Conferenz in Wien zur weiteren Veranlassung und Erledigung

übergeben wurde.

1. © e€s tery ei ¢

a. Bericht über die für die mitteleuropäische Gradmessung im Jahre 1866 von
Seite Oesterreichs ausgeführten astronomisch geodätischen Arbeiten.

Die kriegerischen Verhältnisse des Sommers 1866 haben die im grösseren Umfange
auszuführen beabsichtigten geodätischen Arbeiten auf ein sehr beschränktes Mass zurückge-
führt, indem die Verbindung des wallachischen Dreiecksnetzes mit dem grossen russischen
Gradbogen bei Ismail, wozu bereits Alles vorbereitet gewesen, theils wegen Mangel an
Arbeitskräften theils wegen der politischen Verhältnisse in den Donaufürstenthümern ganz
unterbleiben musste, während in der von Böhmen durch Mähren nach Wien führenden Drei-
eckskette nur die Beobachtungen auf den Stationen Rappotitz, Hora und Spitzberg, so wie
die Messung von gleichzeitigen Zenithdistanzen zwischen diesen Punkten und Ambossung,

dann die Bestimmung der Polhöhe und des Azimuthes auf Rappotitz noch vor Ausbruch des

Krieges zur Ausführung gelangten.

 
