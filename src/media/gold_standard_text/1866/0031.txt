ee cel à

a Oh aL

greifen zu stéren. Dagegen musste aber auf die Bestimmung der Gewichte fiir die auf ver-
schiedene Arten abgeleiteten Höhendifferenzen ein besonderes Augenmerk gerichtet werden,
was bei der Ausgleichung geschehen ist.

Ursprünglich war das Höhennetz auf die Meereshöhe der Göttinger Sternwarte
und des Hannoverschen Dreieckspunktes Hohehagen basirt, ist aber nunmehr auf die vom
Königlich Preussischen Generalstabe bestimmte Meereshöhe des Inselberges = 470,000 Toisen
(über dem Mittelwasser der Ostsee bei Swinemünde = 0,5636 Toisen über dem Nullpunkte
des dortigen Pegels) bezogen worden.

Für die Bestimmung der Meereshöhen in der Grafschaft Schaumburg ist der Höhen-
unterschied des Martinsthurmes in Kassel und des Wittekindstein-Thurmes bei Minden
— 54,1806 Toisen (so viel liegt letzterer höher, als ersterer) zu Grunde gelegt worden. In-
dessen kann die Genauigkeit dieses Höhenunterschiedes nicht verbürgt werden, weil der-
selbe aus anderweitigen nivellitischen Arbeiten entnommen werden musste. Er wird durch ein

besonderes Nivellement geprüft und verbessert werden müssen.
Das Verzeichniss der Höhenpunkte, die auf der Uebersichtskarte Taf. III. dargestellt
sind, befindet sich auf der folgenden Seite.

 
