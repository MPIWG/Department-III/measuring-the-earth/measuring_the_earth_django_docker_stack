 

eee

und hieraus folgt als wahrscheinlichster Werth:
Polhöhe von Hohe Schneeberg A = 50° 47’ 36"524
mit dem wahrscheinlichen Fehler +0"035.

Azimuth.
Auf dem hohen Schneeberge wurde das Azimuth zweier Richtungen:
nach dem Dreieckspunkte Bösig (Entfernung 27240,6 W. Klafter)
und - - - Donnersberg (Entfernung 15458,5 W. Klafter)
unmittelbar gemessen. Beide Punkte waren durch Heliotrope signalisirt. Die Resultate sind

folgende:
Bösig. Donnersberg.
September: 10. 302° 54! 1976 September 10. 25° 20! 5278
- - 1991 - - 52"48
- - 19196 - 13. 54''93
- 13. IT - - 56/20
: - 20/51 - - 5332
- 14. 1749 - 28. 5541
5 17. 18/11 - - 5288
- = 18/55 - - 5319
- 21. 20/24
- - 20113
- - 18"57
- - 15713
Mittel 302° 54! 19256 25° 20' 531899
+ 0196 25 07900
Reduction auf die Dreieckspunkte:
Bösig. Donnersberg.
Beobachtetes Azimuth . . . . . . 302° 54! 19256 25° 20! 53899
Reduction auf die Axe des Thurmes . + 1561312 + 117487
Reduction auf das Signal . . . . . — 12555 + 107002
Convergenz der Meridiane . . . . . + 0554 + 0554
302256" 3567 25° 21' 15'942

Somit ist

Azimuth der Richtung:
Hohe Schneeberg A — Bösig A = 302° 56’ 3"567 + 0"198,
Hohe Schneeberg A — Donnersberg A = 25° 21! 15942 + 0'335.
Die Differenz dieser Azimuthe beträgt 82° 25' 12"375 + 0"389. Ich selbst hatte nicht
Gelegenheit, den Winkel Bösig-Schneeberg-Donnersberg unmittelbar zu messen, da ich bei
der äusserst ungünstigen Witterung Mühe hatte, nur die obigen Azimuthbeobachtungen zu

 

|

er mbmllil luase

 
