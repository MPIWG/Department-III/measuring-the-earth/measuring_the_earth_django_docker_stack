[YIN we mein ei

N Te ST TT re PTE INN |

À EAU

il

N

solcher Genauigkeit ausgeführt, dass in dieser Beziehung keine weitere Messung nothwendig
sein wird.

Gleichfalls können die auf den Stationen Dangast und Varel ausgeführten terrestrischen
Winkelmessungen als abgeschlossen und als ausreichend genau angesehen werden. Weniger
lässt sich dies von den Messungen auf dem Schlossthurm zu Jever behaupten, da die hier
gefundenen Resultate gegen die in frühern Jahren von andern Geodäten angestellten Messun-
gen grössere Abweichungen zeigen, als nach der grossen Uebereinstimmung der Resultate
einzelner Tage untereinander erwartet werden durfte. Es liegt die Vermuthung nahe, dass
der obere Theil des Schlossthurms, in welchem das Instrument aufgestellt war, wegen seiner
eigenthümlichen Construction aus Holz bei dem oft herschenden starken Winde manchen
Schwankungen ausgesetzt gewesen ist, und dadurch jene Abweichungen veranlasst worden
sind. Mit Herrn Dr. Tietjen ist die Verabredung getroffen, in diesem Frühjahr, sobald die
Witterung sich günstig gestaltet, die Winkelmessung in Jever zu wiederholen und zur Ver-
meidung des muthmasslichen Uebelstandes auf der Schlossthurmmauer einen soliden Beobach-
tungspfeiler herzurichten.

Es liegt in der Absicht, auch auf dem Thurm zu Langwarden, an welchem vor mehren
Jahren bauliche Veränderungen vorgenommen sind, noch einige Winkelmessungen auszuführen,
theils zur Verstärkung des Dreiecknetzes, theils zur Erlangung einer Gewissheit darüber, ob
und in wie weit das Centrum des Thurms sich durch jenen Bau geändert habe.

Nach Abschluss dieser noch rückständigen Messungen werde ich nicht ermangeln, dem
verehrlichen Central-Büreau sämmtliche Beobachtungen und Messungen nebst den aus denselben
hergeleiteten Resultaten in extenso mitzutheilen und, soweit nöthig, mit meinen Bemerkungen
begleiten.

Alsdann würde nur noch die Ermittelung des Längenunterschiedes zwischen
Dangast und Göttingen erübrigen und zu diesem Ende die Herstellung einer unmittelbaren
telegraphischen Verbindung erforderlich werden.

Da zwischen Göttingen und Varel bereits eine solche Verbindung besteht, so bedarf
es nur der Weiterführung des Telegraphendrahts von letzterem Punkte bis zur Station Dangast,
somit der Anlegung einer Telegraphenlinie von circa 1 Meile Länge. Die hierfür erforder-
lichen Anordnungen habe ich bereits getroffen und hoffe, dass die fragliche telegraphische
Verbindung zwischen Göttingen und Station Dangast schon um die Mitte des nächsten Monats
völlig hergestellt sein wird, so dass nach diesem Zeitpunkt die von Herrn Dr. Tietjen mit
den betreffenden Herren Astronomen in Göttingen näher zu verabredenden correspondirenden
Beobachtungen jeder Zeit werden in Angriff genommen werden können.

In Uebereinstimmung mit Sr. Excellenz Herrn General Baeyer würde ich es für sehr
wünschenswerth halten, wenn etwa gleichzeitig auch eine Längenbestimmung zwischen Station
Dangast und der Sternwarte zu Leiden Statt finden könnte. Ich habe mich zu diesem Ende

unter dem 31. vorigen Monats mit einem Schreiben an den Herrn Professor Kaiser gewandt

und zu meiner Freude schon einige Tage darauf das in Abschrift angelegte gefällige Antwort-
General - Bericht f. 1866. 4

 
