 

u

Die englischen und amerikanischen Astronomen waren die ersten, welche die elektrische
Telegraphie zu Längenbestimmungen benutzten. Das Pariser Observatorium folgte 1854 mit
der Längenbestimmung zwischen Paris und Greenwich nach, die in Gemeinschaft mit den
englischen Astronomen ausgeführt wurde, sobald die unterseeische Leitung fertig war. Später
1856 unternahm es, unter Mitwirkung des Depöt de la Guerre, die Längenbestimmung eines
Centralpunktes, wozu Bourges auserschen wurde. Von da ab ruhten diese Arbeiten mehrere
Jahre und erst 1861 konnte das Observatorium dieselben mit der Längenbestimmung von
Havre wieder aufnehmen. Seitdem wurden sie bis 1865 ununterbrochen fortgesetzt und die
Längen und Breiten auf folgenden Stationen bestimmt: Dünkirchen, Strasburg, Brest, Talmay
(Côte d'Or), Biarritz, Madrid, Marennes, Rodez, Carcassonne, Lyon (Fourvières). Auf vier
dieser Stationen wurden auch die Azimuthe gemessen, und ausserdem Polhöhe und Azimuth
zu Saligny-le-Vif (Cher).

Die Beobachtungen zur Bestimmung der Polhöhen und Azimuthe, und die trigonome-
trische Verbindung der astronomischen Stationen mit den geodätischen finden sich in den
Annalen des Pariser Observatoriums (M&moires) im 8! und 9" Band. Die Meridianbeob-
achtungen der Sterne für die Bestimmung der Längen und Azimuthe finden sich ebendaselbst
unter Observations im 18'", 19ter und 20°ter Bande 1862, 63, 64, 69.

Ein Memoire über die Vergleichung der neuen Längen-, Breiten- und Azimuthalbe-
stimmungen, mit denen im Memorial du De&pöt de la Guerre aufgeführten, wurde der Akademie
in der Sitzung vom 2. März 1866 vorgelegt.

Die nachstehende Uebersicht enthält die Vergleichung zwischen den astronomischen

und vorläufig verbesserten geodätischen Bestimmungen.

=

La shh ht a Mh i)

ova

‘us sat imdb, i, ileal Ad eis lh nih ie x

 

Ä
|
|

 
