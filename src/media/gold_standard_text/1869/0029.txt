| en mess done

KT TORTE PPT |

URI

ivi 0

u 90. 4

1) Beobachtungen von Sternen am Himmel:

V—B
Kr. West = —0.005 + 0.010 42 Sterne
Kr. Ost = —0.010 + 0.020 42 -
Mittel = —0.007 + 0.009,
2) aus Beobachtungen am Gleichungs-Apparat
V--B
Kr. West = 0.005
Kr. Ost = —0.013
Mittel = —0.004 + 0.007.

Angenommen wurde daher für die persönliche Gleichung
Ss
VER = = 0003;

s
Die sogenannte Stromzeit ist schon im vorigen Generalbericht zu 0.039 + 0.0023 angegeben.

Die ausführliche Begründung dieser Zahlen wird in einer Publication tiber die Längen-
bestimmung Berlin-Lund mit Genehmigung des Herrn Generallieutenant Dr. Baeyer in den
Annalen der Lunder Sternwarte publieirt und noch in diesem Sommer jedem der Herren Com-
missare ein Exemplar dieser Publication zugesandt werden.

Die Pendelbeobachtungen. .

Das Pendel ist ein Reversionspendel, angefertigt von den Herren Repsold & Söhne in
Hamburg und ist im Allgemeinen dem vor einigen Jahren für die Schweiz angefertigten, von
Herrn Plantamour in seinen ,,Expériences faites avec le pendule de reversion A Geneve“ ähn-
lich. Die wesentlichen Abänderungen sind die folgenden: 1) die Länge des Pendels von
Schneide zu Schneide ist 1 Meter; 2) das Pendel und der Maassstab sind aus derselben Me-
tallplatte (Messing) hergestellt, um möglichst nahe denselben Ausdehnungseoeffieienten für
beide zu erhalten; 3) der Maassstab hat 2 Stäbe (1 aus Zink, 1 aus Messing), als Metallther-
mometer, um aus dem Unterschied der Ausdehnung auf die Temperatur des Maassstabes
schliessen zu können.

Der Pendelapparat kam hier Ende Mai an. Er wurde sofort aufgestellt und zunächst
eine Bestimmung der Pendellänge in Leipzig in den Tagen von Mai 29 bis Juni 5 ausgeführt,
und zwar in nur 2 Lagen die vier Combinationen, während später (von November 24 bis De-
cember 10) in allen 4 Lagen und jeder der 4 Combinationen beobachtet wurde. In Gotha
wurden von Juni 18—23, auf dem Seeberge von Juli 31 bis August 5, auf dem Inselsberge
von September 4—9 und in Berlin (in demselben Lokale, in welchem Bessel seine Pendel-
beobachtungen angestellt hat) von October 24—30 Pendelbeobachtungen angestellt.

 
