 

à
N
N
i
i
|
h
t

A.

Einstellungen sich unmittelbar die Runcorrection ergiebt. Um die Biegung zu eiiminiren
wurde der Polarstern nach Norden hin beobachtet, dagegen drei oder vier hellere Sterne zu
vier verschiedenen Zeiten nach Süden, deren Zenithdistanz im Mittel nahe der des Polarsterns
gleich war.

Zu jeder vollständigen Breitenbestimmung wurden 48 Höhenmessungen des Polarsterns
zu vier verschiedenen Zeiten angestellt und die Zeiten womöglich so gewählt, dass die Hälfte
der Beobachtungen in einem 12 Stunden grössern Stundenwinkel des Polarsterns angestellt
wurde, als die andern. An jedem Tage wurden gewöhnlich ausgeführt:

3 Einstellungen, wenn das Ocular nach West oder Ost zeigte,
6 : ; è 5 - Ost West ‘©
3 = - - - Se est, cae OS ..-
Jeden Abend wurden ausserdem 3 oder 4 Siidsterne genommen und zwar:
Stern I. 2 Einstellungen Kreis Ost,

2 - - West,
Stern 11. 2 - - West,
2 - = Ost:

u. 8. W.
Ferner sollten zur Breitenbestimmung an einem Passagen-Instrument mit gebrochenem Fern-
rohr von 30 Lin. Oeffnung an 3 Abenden an jedem Abende 4 Sterne im ersten Vertikal in
beiden Kreislagen beobachtet werden.

Bei Bestimmung des Azimuths, ermittelt durch Beobachtung des Polarsterns und
eines terrestrischen Objects mit dem oben genannten Universal-Instrumente, wurde nach fol-
gendem Schema verfahren:

Beobachtung des Objects in der Mitte oder an beiden Mittelfaden Ocular West oder Ost,

- - Polarsterns - - se E ; = 5 = -

Fernrohr durchgeschlagen,

- - Polarsterns in der Mitte oder an beiden Mittelfäden Ocular Ost oder West,

: - Objects ii - = - - : Fate -

: - Objects - = = ey ie - - - He -

- = Polarsterns - - - BAT GLS RTS - - Rene

= Fernrobr durchgeschlagen,

- - Polarsterns in der Mitte oder an beiden Mittelfäden Ocular West oder Ost,

- - Objects - = = - = - - - us -
Die Messungen wurden an 5 je 36° von einander entfernten Stellungen des Kreises wiederholt

und auf etwa 3 Tage vertheilt.

Da mit dem Passagen-Instrument Zeitbestimmungen gemacht wurden, war es leicht
möglich, eine Marke mitzubeobachten, welche im Meridian aufgestellt war, und deren Azimuth
zu bestimmen. Festgestellt wurde, 3 obere und 3 untere Culminationen des Polarsterns, nö-
thigenfalls auch von d Ursae minoris zu beobachten und bei jeder Culmination 8 Fäden, wozu

 

3
i
|
|
i
]

 
