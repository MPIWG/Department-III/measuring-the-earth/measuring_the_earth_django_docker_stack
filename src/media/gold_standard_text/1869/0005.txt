de TOT PT

Li EME GE ol]

a m

à
2
=
3
x
®

A

Les dépenses causées par le travail géodésique exécuté pendant la campagne de 1869,
se décomposent comme suit:

10. Indemuités accordées aux officiers employés sur le terrain . . . 11243,50 fres.

20, Frais d'installation aux elochers; frais de construction des signaux;

indemnités aux heliotropistes . . . . 2... a a
Total de la depense 26911,33

Pendant l’année 1870, le nombre des officiers observateurs se trouvera réduit à 5 et
la dépense quabsorbera le travail de cette année peut s’évaluer approximativement à 15000 jres.

Veuillez agréer, mon Général ...

Le Général, Directeur du Dépôt de la guerre,
Chef du Corps d'Etat major,
Bruxelles, le 10 février 1870. Simon.

4, Dinemark

Bericht über die in Dänemark ausgeführten Arbeiten von dem Herrn Geheimen
Etatsrath Andrae.

Im letztvergangenen Jahre hat man auf verschiedenen Stationen der nördlichen, bis
nach Skagen gehenden Dreieckskette die Winkelmessungen ausgeführt, und unabhängig von
diesen Arbeiten im Felde auch die Rechnungen für den zweiten Band der Gradmessung fortgesetzt.
Da die Ausgleichungen der in diesem Bande behandelten Hauptdreiecke vollständig durchgeführt
sind, gebe ich unten die hauptsächlichsten dadureh erhaltenen Resultate. Die erste Abthei-
lung enthält die Dreieckskette zwischen Refsn®s-Klöveshöi und Lysabbel-Fakkebjerg
(siehe die angebogene Dreiceksskizze) und schliesst sich somit dureh die erste Seite genau
an die Hauptdreiecke des ersten Bandes. Die erste Verticalcolonne giebt die Richtungen, so
wie sie aus den einzelnen Horizont-Ausgleichungen hervorgehen, die zweite aber die definitiven,
durch die Ausgleichungen des ganzen Dreiecksnetzes bestimmten, die dritte endlich die Loga-
vithmen der in Toisen ausgedrückten Dreiecksseiten. In der zweiten Abtheilung, welche die
älteren von Schumacher zur Bestimmung des Meridianbogens zwischen Lauenburg und
Lysabbel gemessenen Hauptdreiecke umfasst, ist die erste Colonne weggeblieben, da hier keine
Horizont-Ausgleichungen vorkommen, indem nicht die Richtungen satzweise, sondern die ein-
zelnen Winkel dureh die Repetitionsmethode bestimmt sind. Auch in dieser Abtheilung sind

- sämmtliche Dreiecksseiten aus der Kopenhagener Grundlinie abgeleitet, und die Braaker Ba-

sis, deren Genauigkeit nicht verbürgt werden kann, vorläufig ignorirt worden.

Erste Abtheilumg.

Klöveshöi.
0 I II 0 | ft
Böschjergihin dr ana ; 0 00 00,00 0 00 00,00 4,3281088
Refsnes ese ahora) PRS. i 37 3939,03 37, 39 38,60 4,2318079

 
