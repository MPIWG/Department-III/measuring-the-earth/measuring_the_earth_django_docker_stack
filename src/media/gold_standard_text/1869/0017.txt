re

u RT FOR PY PRP TTT |

iL AR Jam

it

li 1

ill

:
x
=
x
3
x
x
=
=

 

ae ho

dische Regierung unmittelbar die Geldsummen bewilligt hat, die zum Behuf der Arbeiten fiir
die europäische Gradmessung erbeten sind. Herr Prof. Cohen Stuart, der, seiner Ernen-
nung als Direetor der polytechnischen Schule in Delft wegen, die Leitung der geodätischen
Arbeiten hat ablehnen müssen, hat ursprünglich dazu um 15,090 Gulden angefragt. Diese
Summe wird wohl nicht ausreichen, indem die wiederholte Basis-Messung im Harlemermeere
hinzugekommen ist, aber ich bezweifele es nicht, dass das noch fehlende hinzugefügt werden
wird. Für die astronomischen Arbeiten habe ich um 5000 Gulden nachgesucht. Dafür sind die
Registrir-Apparate u. s. w. angekauft, ist das Häuschen neben der Sternwarte gebaut und wer-
den die Reisekosten vergütet. Diese Summe wird um so mehr ausreichen, da durch die tüch-
tige Hülfsleistung unserer löblichen Telegraphen-Direetion beträchtliche Geldsummen erspart
sind. Uebrigens sind die Arbeiten für die Astronomie im Allgemeinen und für die Grad-
messung in’s Besondere, so wie auch die zu beiden erforderten Geldsummen, dergestalt ver-
mischt, dass sie sich nicht von einander trennen lassen. So haben die Arbeiten für die
Gradmessungs-Sterne seit fast zwei Jahren alle Kräfte der Leidner Sternwarte in Anspruch
genommen, und diess konnte geschehen, indem diese Arbeiten auch für die Astronomie selbst
ihre Wichtigkeit hatten. Die Niederländische Regierung wird es nicht an den Geldmitteln
fehlen lassen, welche für wichtige wissenschaftliche Arbeiten erforderlich sind, und ich würde
mich sehr glücklich schätzen, wenn ich behaupten könnte, dass die Früchte dieser Arbeiten
immer der Munificenz unserer Regierung entsprechen.

Leiden, 28. Februar 1870.
F. Kaiser.

2. Bericht des Herrn Prof. Dr. Stamkart.

Auf Veranlassung des Circulaire vom 14. Januar dieses Jahres habe ich die Ehre
dem Central-Büreau der europäischen Gradmessung den folgenden Bericht mitzutheilen.

Die Arbeit, welche im abgelaufenen Jahre, was den geodätischen Theil betrifft, in
Holland verrichtet worden ist, besteht nur in einer zweiten Messung der Basis, welche ein Jahr
vorher im Harlemermeer gemessen wurde, und in einer Verlängerung der Basis um 881 Meter
in der Riehtung nach NW, nach einer Seite, und um 100 Meter nach SO nach der anderen
Seite. Diese beiden Verlängerungen sind auch zweimal gemessen.

Am Ende der Verlängerung von 881 Meter ist nun auch ein fester Endpunkt bezeich-
net auf eine unter den Boden gemauerte stumpfe Pyramide. Das Ende ist — ebenso wie der
Anfangspunkt im SO — auf eine messingene Platte von 5 Centimeter im Quadrat, durch eine
Oeffnung von 2 Millimeter, angedeutet. Concentrisch mit dieser Oeffnung sind auch zwei
Kreise von 10 und 20 Millimeter Durchmesser gezogen und die Inschrift:

Graadmeting
1869
gravirt. Ein Stein von 9 Decimeter in jeder Seite und 1 Decimeter Dieke verdeckt das Ganze,

General-Bericht f. 1869. 3
