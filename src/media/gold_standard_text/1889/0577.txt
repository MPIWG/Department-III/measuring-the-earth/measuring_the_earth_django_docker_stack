 

 

 

 

\
|
Î

u ra i a ‘ Russe: En on

ne rire) nennen als sina Tana

iii insiste

Annexe B XVII.

ETATS-UNIS

Mr. President and Delegates of the International Geodetic Association :

An Act of Congress at its last Session authorized the President of the United States of
America to appoint a delegate to this-Association; and on the 7' of September Profes-
sor Mendenhall, the recently appointed Superintendent of the United States Coast and Geo-
detic Survey gave me, afler two conferences, the necessary instructions to appear as the re-
presentative of that great national work.

I bring from him kindly greeting and good will to you all, a hearty sympathy with
the objects and labors of the Association, and a conviction that its views will grow with the
breadth of the new world before it.

For myself and my colleagues in the Coast and Geodetic Survey I repeat the senti-
ments of the Superintendent; and I feel that we shall learn much by the free interchange of
experiences and opinions with you all, personally and officially.

The Superintendent directed me to bring such outline schemes of the later progress
of the work of the Survey in its different branches, as would enable me to make explicit expla-
nations of the objects, scope, methods, means, rate of progress and prospective views of the
Coast and Geodetic Survey.

I did not expect to give more than a verbal description of the work, but in order to
conform to your system in the Conference, | feel compelled even at such short notice to put
my remarks upon paper.

Before | take up the subject matters referred to in my instructions, it will be welt
for us to bear in mind the extent of the territory of the United States, excluding Alaska. Il
stretches from the 67 nearly to the 125t degree of Longitude, and from the A9th to the 26%
degree of Latitude. The lengths of the diagonals from the northern part of Maine to San Diego,
and from Key West in Florida to Cape Flattery at the Strait of Juan de Fuca, are nearly
equal at 2880 statute miles, or 4640 kilometres. The line along the parallel of 30° is 2628
statule miles in length or 4250 kilometres.

 

 

 

 

 

 
