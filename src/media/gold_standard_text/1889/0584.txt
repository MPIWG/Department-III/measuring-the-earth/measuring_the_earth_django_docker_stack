 

 

|
i
|
i
'
N
|
|
|
|

 

 

scheme was inaugurated lo check and control the immediate triangulation of the coast that
had much shorter sides, and was much impeded by the peculiarities of the rocky or forest
covered coast ranges which lie very close to each other.

As early as 1859 I made some observations upon the Sierra Nevada from the station
Mount Tamalpais in the westernmost of the coast ranges. The distance was about 165 miles
or 260 kilometres ; and as 1 was already familiar with the relation of the coast ranges with
the Cascade Range and the Sierra Nevada, I foresaw the development of a great scheme of
triangulation to the north and south between these mountain ranges, with extraordinarily
favorable opportunities for measuring baselines of almost any desired length in the nearly
level plains of the Great Valley of California. Since then this scheme of triangulation has been
carried out, and that of the Transcontinental are of the parallel includes and starts nearly

from this Tamalpais station.
5. The Oblique Arc of the Alluntic Coast has been executed for a length of 17° 24

7

and will be continued southwestward to the shore of the Gulf of Mexico, and northeast-
ward to Cape Breton in the Dominion of Canada. The total length will be 20 3/,° equal to
1202 miles or 1920 kilometres, and that small part to the northeast of Maine will necessa-
rily be executed by Canada.

6. A number of sub ares have been executed on different parts of our coasts. There
are nineteen of these, and they have an average length of 138 miles or 224 kilometres.

7. The great central are of longitude on the parallel of 39°. It extends from Point
Arena on the coast of California Lo Cape May on the Atlantic coast, and has a total length of
48 3/,° of longitude, equal to 2628 statute miles or 4230 kilometers. Thirtg-four degrees of this
are have been executed in four sections. | have already stated that this great Transcontinental
are was projected to connect the schemes of the triangulation on the Atlantic and Pacific coasts.

Along this line the telegraphic longitude observations are nearly completed; the geo-
detic levellings have progressed from the Atlantic to beyond the Missouri River; and the gra-
vity determinations will be made systematically from ocean to ocean.

| need say little about the character of the triangulation upon this parallel. In some
parts of the country it is made with diffieully on account of the heavy forests and the ab-
sence of prominent heights. There, and in other parts, the lines are necessarily shorter than
is desirable, but baselines will be more numerous, and astronomical observations will be
frequently repeated. From the eastern line of the Rocky Mountains to the Pacific Ocean the
size of the triangles, quadrilaterals, and pentagons is sufficiently large for the highest cha-
racter of work. Asa general rule the almospheric conditions are favorable, but the difficul-
ties to be overcome in such an arid region, very sparsely populated, are extreme. It requires
the utmost devotion and exertion to carry forward the work, and especially when a party is
caught ata station from 10,000 to 13,000 feet elevation (3050 to 3965 metres) in the cold
weather of the early winter.

The Association can learn the character of the work near the western extremity of
this arc from the published accounts of the measurement of the Yolo base line and the treat-
ment of the geometrical figures comprising « The Davidson Quadrilaterals. » As observers

 
