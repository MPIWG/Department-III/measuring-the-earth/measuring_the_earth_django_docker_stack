 

 

nee,

IV. — Danemark.
NL 00007

   

 

 

 

 

 

 

 

 

 

 

N° de LATITUDE. |LONGITUDE. ÉPOQUE. Sn | INSTRUMENTS. REMARQUES.
DES POINTS. ET OBSERVATEURS.
een à |
1 | Copenhague .....:. 55°40'43"| 80° 14’ 48”! 1837-38-40 | Nyegaard, Petersen, Ne- | Théod. Ertel de 15 p. | Publiés dansle «Den
Tour St. Nicolas. hus. et théod. Reichen- ey
bach de 12p. dr, Vol. 1, 2 et 3.
PoP ase oo 2. Do se 7 0b 14 1838 Nyegaard, Petersen.
Terme Nord.
biases ss 55.37 41: 30.1450 1838 id. Théod. Ertel de 15 p.
Terme Sud.
lc Frederiksholm..... 59:38 56 | 20. 11° 89 1838 id. Der Dom ders
ld; Copenhague........ 55 40 48 | 30 14 9 1837 id. Theod. Reichenb. de Base.
| Kgl. St. Pierre. | jop.
le, Frydenhöi......... 55 aie | 830. 26.50 1838 id.
Ei Valhol iS. ok ss. nd) 40) 30 | SO 022 1838 id.
lg| Kongelunden ...... 5 35 40. | 30.13 26 , 1888.39 id.
2 | Snoldelev.......... 55, 34 lo 2947 49 1838 id.
3’ Julianehöi......... 55,46 54, 23.18 I en
4 | Mörkemosebjerg ...| 55 35 7 | 29 20 52 1842-4 egaard. ,
an 55 47.36 29 340 | 184243 | id. Théod. Ertel de 15 p.
6 | Klôveshôi . ........ 55:34 11429 055 1843 id.
qe) Metsnis 2 oc ss 2h... DD ARMOR 28 34 31. 1843 id.
8 | Bégebjerg:........ 55 32.40 128 21 33 1844 id.
9 | Dyrebanke......... 5 2 BQ pi a 1844-45 id.
10 | SKamlingsbanke ...| 55 25 8 | 27 13 49 1846 id, Prusse, B. N. 276
11 | Troldemosebanke ..| 55 43 45 | 27 36 9 | 1846-67 | Nyegaard, Meldahl.
PD Dynes tS. 55 50 355 | 28 15.41 1847-67 Thalbitzer, Meldahl.
13 | Eiersbavnehöi ..... 55.58.38 | 27 29-44 1867 Meldahl.
14 | Agri Bavnehöi..... 6 IS48 | 2830. 5 1868 id.
> | BYSMet .......:... 502227 | 20.38 14 1868 id.
16 | Hegedal Bavnehöi . | 56 30 19 | 28 13 49 1868 id. à
mi |; Hovhor oe 2. 56 38 Alk |, 270.39: 56 1868 ide
18 | Rold Bavnehöi..... 56 46 3 2029 15 1868 id. Théod. Ertel de 15 p.
19 |-Muldbjerg.......:. 56.34 Si 2 D5. 30 1870 id.
20 | Jägerdalshöi....... 56 55 33 | 27 28 49 1869 id.
21 | Storskoven ........ 019 97075439 1869 id.
Se nt: 8 57 1722| 27.28 52 1869 id.
Peslhdi-........: 57 298922 7271,48 12 1869 id.
DI Bade os. 50.95.21) 28.7.5021, 1869 id.
= SRasen .......:..>, 57 45 46 2816 ae ee 3 : id. ‘ : en
Knivsbjerg ........ Do 0 27 02 1845-4 yegaard. 4 russe, B. N. 274.
| Leerbjerg ......... 55 7 30 | 27 55 58 1845 id ne En : - pe
28 | Lysabbel 54 54 13 | 2740 9 | 1818-46 | Caroc, Nyegaard. een
29 | Fakkebjerg........ 54 44 93 | 98 21 53 | 1818-41-44 id. hoch de 1
30  Sprenge.. -...:... 54 27 14 | 27 46 9 || 1817-18-19 | Schumacher, Caroc. ,
31 | Hohenhorst. 54 15 1.927 50 53 | 1817-18 id. | Théod. de 19p.
32 | Bungsberg........ 54 12 39 | 28 23 19 || 1817-18-41 no Caroc, Nye- a a a D: , | Prusse, B. N, 80.
aar éod. Reichenb. de
. 12 p.etthéodde8 p.
83 | Segeberg.......... 53.56 Tol 21 Do De 1817-18 Schumacher, Caroc.
D Eds =... 53 52, 4.98 20:58 1817 Schumacher. ee
39, Bichede. .......... bp 43. 228 4 17 1819 Caroc. ide ni
36 | Base de Braack....| 53 38 27 | 27 52 30 1821-22 Schumacher. > Théod. de 12 p.
Terme Nord.
97 nn de Braack.....53 35 44 | 27 55 15 1822 Schumacher, Caroc.
Lerme Sud.
DO Si oe 53 38 2.|.27 57 44 1818-22 Schumacher, Caroc, Ne- | Théod. de 8 et 12p. a
hus. aa
38a} Bornbeck.......... 533811 128 017 1822 Schumacher, Caroc. a de 12 p. et de
39 | Hohenhorn......... 53 28 32 | 28 153 | 1818-24-25 | Caroe. Inch de 8, ot, de Mer eos
p . 39.

 

 

  

  
