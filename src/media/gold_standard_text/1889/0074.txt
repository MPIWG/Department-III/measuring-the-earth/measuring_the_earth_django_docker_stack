(

 

68

Herr Ulysse Granchi, Chef am militär.-geographischen Institut zu Florenz.
Ilerr Perrotin, Director der Sternwarte von Nizza.
Herr J. Stas, Mitglied der Akademie von Brüssel.

Ferner die von der französischen Commission eingeladenen Herren :

Herr Eginitis, Astronom an der Sternwarte zu Paris.

Herr Constantin Gogou, Professor in Bukarest.

Herr Cruls, Director der Sternwarte von Rio de Janeiro.

Herr Beuf, Direetor der Sternwarte von La Plata.

Herr Wada, von Japan.

Herr d’Abbadie, Mitglied der Akademie der Wissenschaften.

Herr Cornu, Mitglied der Akademie der Wissenschaften.

Herr Daubree, Mitglied der Akademie der Wissenschaften.

Herr Mascart, Mitglied der Akademie der Wissenschaften, Director des meteorologischen
Centralbüreau’s.

Herr de Saint-Arroman, Bureauchef im Unterrichtsministerium in Paris.

Herr Oberst Laussedat, Director am «Conservatoire des Arts et Métiers », Paris.

Herr Dr. Rene Benoit, Director am internationalen Maass- und Gewichtsbureau, in Sevres.

Herr Gaspari, Ingenieur-Hydrographe der Marine.

Ilerr Germain, Ingenieur-Hydrographe der Marine.

Herr Hatt, Ingenieur-Hydrographe der Marine.

Herr Cheysson, Oberingenieur der Brücken und Wege.

llerr Marx, Vizepräsident der allgemeinen Nivellements-Gommission von Frankreich.

Herr Durand Claye, Oberingenieur der Brücken und Wege.

Herr Trepied, Director der Sternwarte in Algier.

Herr Teisserenc de Bort, vom meteorologischen Centralbureau, in Paris.

Herr Callandreau, an der polytechnischen Schule.

Als Hülfssekretäre :

Herr Brullard, Geniehauptmann.
Ilerr Bourgeois, Arülleriehauptmann.

Seine Excellenz Herr Spuller, Minister der Auswärtigen Angelegenheiten, in Beglei-
tung Seiner Excellenz des Kriegsministers Herrn von Freycinet und Seiner Excellenz des
Unterrichtsministers, Herrn Fallieres, eröffnet um 2!/, Uhr die Sitzung mit folgender
Ansprache :

« Hochgeehrte Herren!

«Ich erfülle heute eine meiner Amtspflichten der ich den höchsten Werth bei-

T

messe : es ist mir vergonnt, im Namen des Präsidenten der Republik und im Namen meiner

 

nee
ELLIO EE EE

 

 

 
