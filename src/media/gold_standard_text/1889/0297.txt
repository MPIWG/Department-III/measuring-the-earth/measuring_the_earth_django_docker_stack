 

VI. — France et Algerie.

je

  

D ER EE TANI

INDICATION

 

DIRECTEURS

 

 

 

 

Ne LATITUDE. LONGITUDE.| EPOQUR. INSTRUMENTS. REMARQUES.
DES POINTS. ET OBSERVATEURS.
Triangulation de Jonction de la Base de Bordeaux au Parallèle Moyen.
259 | Chantillac......... | 45°19' 30") 17°24 58" Mémorial du Dépôt
2 neun ea : Déjà mentionnés. ne vr =
961 | Soubran........... 45) Où Ge © 1
969 | St. Savin: ....-... Au 9 922 7 10,55 :
263 | Le Gibault ........ 45 10 54 | 17 39 44 =
964 | La Pouiade........ 45 5 99 | 17 22 45 a
265 | St. André de Cub- a
wesen AA 59 57 | 17 12 36 >
266 | Montagne ......... 44 55 53 | 17 31 59 me a
967 fa SANTE...) #461) | 1791 2 5 © 3
268 | Bordeaux.......... 445019 | 17.5 4 : ae 2
969 | Blanquefort ....... 44 54 38 | 17 1 39 a Bo ©
970 | Leugean.......-..- 44 58 39 | 16 53 11 ob ES $
O71 | Captienux .......... 44 51 38 | 16 48 58 D aa =
Do | led... : 45 41 34 | 23 51 36 = oe 3
273 | Bellachat.......... 45.32 30 | 24 4 9 os 2
A| Brene............. 45.91. 9.| 23 51.49 Se 35
975 | Encombres......... AD (a Dl 24 GC SI =
276 | Mont Jouvet....... 45 29 42 | 24.18 11 3
277 | Roche Chevriére...| 45 17 37 | 24 23 8 ©
278 | Mont Tabor ....... A 056 124 13939 2
979. -Ambin..  ...2:.... 45 93925 | 94 3253 =
980 | Chaberton......... 44 57 54 | 24 24 48
981 | Albergian ......... 15 Or) M2
282 | Rochemeion ....... 45 12 13 | 24 44 28
Triangulation du Paralléle de Rodez. (Partie Orientale.)
283 | Puech de Monsei- Id.
Ener tr... 42195821 20351331
62663) Rodez, La Gaste...| Déjà mentionnés.
284 | Cougouille......... AS oi ol | 20 Ay Di
285 | Puech d’Aluech. ...| 44 20 9 | 21 4 58
L 286 | W’Hort de Dieu. ...| 44 7 19 21 14 40 es
_ 287 | Roc de Malpertus..| 44 24 5 | 21 30 33 e
288 | Le Guidon de Bou- -® a
guet So 44.659 |, 21.56 56 ci 5
289 | Dent de Rez....... AAG AG | 22. I 57 SB 2
290 | Montmon.......... AEN 24292225 30 18 Du. 3
291 | Grand Montagné...| 43 58 34 | 22 25 46 Gi ES eS |
292 | Mont Ventoux..... 44-30 97 | 22 56 51 x 22 2 |
293 | Les Houpies....... AB AD AN | 29888 45 = we 2
294 | Le Leberon........| 43 48 56 | 23 7 50 = 3
2 De 0... 44 7 23 | 23 27 58 Ss ©
DGSE Jüllen. 43 41 27 | 23 34 18 2 =
297 | Mourré de Chenier. .| 43 50 30 | 24 0 52 5
298 | Les Monges........ 44 15 46 | 23 51 28 nn
299 | Le Grand Coyer ...| 44 6 1 | 24 21 12 |
300 | Le Grand Berard. .| 44 26 57 | 24 19 25 |
301 | La Chains ......... 43 44 51 | 24 19 31 |
Cheiron ........... 43 48 53 | 24 37 59

         

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
