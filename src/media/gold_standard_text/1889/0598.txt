 

 

b) Operations d’ Algerie.

Le Service Géographique a commencé la mesure d’une chaine parallele, au Sud de
l'Algérie et de la Tunisie, qui s’étendra de la frontière du Maroc, pres d’Ain Sefra, aux rives
de la Méditerranée, près de Gabès, en se raccordant aux méridiennes de Laghouat, de Biskra
et de Gabes et aura un développement de près de douze degrés. Le travail a été divisé en
trois segments, limités par les chaînes méridiennes.
| Le premier segment, compris entre la méridienne de Laghouat et la frontière du
Maroc, est en voie d'exécution ; les opérations de 1889, confiées à M. le Capitaine Barisien,
comportent un réseau de huit triangles entre Laghouat et Géryville; la reconnaissance a été
faite au printemps et les mesures d’angles se poursuivent en ce moment.

c) Opérations de Tunisie.

La chaîne méridienne de Tunis à Gabès s'étend le long du méridien situé à 7° de
longitude Est de Paris, sur un développement de trois degrés environ; elle part des côtés
Saguaguid-Rihan-Zaghouan du paralléle de Bône à Tunis déjà mesuré, et aboutit au côté
Tébaga-Zémelet situé au Sud du seuil qui sépare le golfe de Gabès du Chote Fedjedj; une
base de vérification à été préparée dans les environs de Gabès; deux stations astronomiques
sont prévues, l’une à Kairouan, l’autre à Gabès. Cette chaîne pourra ultérieurement être
prolongée plus au Sud d’environ un degré.

La première partie du réseau à été mesurée dans l'automne de 1888 ; les observa-
lions géodésiques se poursuivent en ce moment sur la deuxième partie et seront achevées
dans le cours de l’année.

Les travaux sont exécutés par MM. les Capitaines Tracou et Dumay.

NIVELLEMENT DE PRECISION

Ainsi que nous l’avons annoncé a la Conférence de Salzbourg, le Service Géogra-
phique à mis à exécution le projet qu’il formait depuis longtemps de faire entrer le nivelle-
ment de precision dans le programme de ses travaux. Le nivellement général de la France
étant du domaine du Ministère des travaux publics, il trouva un autre champ d’action dans
nos possessions algériennes et tunisiennes dont il est important, dans l'intérêt de la coloni-
sation, de compléter la description géométrique.

L'insuffisance des voies de communication ne permet pas d’établir dans ces contrées
un projet de réseau par polygones fermés embrassant tout le pays; on doit se borner pour
le moment a des lignes de nivellement qui se fermeront plus tard les unes sur les autres au
fur et à mesure du développement des routes et des voies ferrées.

Dans une première campagne de six mois, d'octobre A888 A avril 1889, une brigade
d'opérateurs, composée de trois adjoints du génie, sous la direction de M. le Capitaine Brul-

 

 

 
