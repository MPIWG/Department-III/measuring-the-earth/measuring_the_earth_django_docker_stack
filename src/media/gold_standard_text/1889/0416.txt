A

XVIL — Suede.

INDICATION | DIRECTEURS

LATITUDE. |LONGITUDE.||  HPOQUE. INSTRUMENTS.
_ DES POINTS. ET OBSERVATEURS.

Brattbacken OPV’ HB) RIB Be 1851 Arosenius.
Prensnipan 59.37 7 30:45 8 1852-53 : 1
Foritomme 59 35 242 30.26 35 1852
Blabarskull 59,50 16 1 30. 31 50 1852-53
59.50.5230 58 7 1853-54
Oster Sattra....... 60 3 571.31. 031 1854

 

 

Gesundaberg. ....-- 19.20 | 92,11729 1881 Rosen. | Repsold I.

oe eel id. id.

15 20 | 32 45 58 1881 id. | id.

13 6 | 32 12 32 | 1881-82 | Rosen, Heden. | Repsold I, IT.
Nyslogberg 2 OG | 32.10.28 1882 Heden. Repsold EL.

Anjosvarden 25 45 | 31 56 47 1882 id. id.
Korpmek 26) 34) | 32 a8, 3 1882 id. | id.
Frundeklint 33,50, 92, 341 1882 id. | id.
Pilkolamanopi 35 384 | 32 24 32 1882 id. id.
Garpkoln 51 52 20 652 28.383 1882 Rosén. Repsold I.
Brickan 55 88 Bl ao) ol 1882 -1d. Repsold IT.
Ronasen 46.14} 31 18 35 1882 id. id.
Knatten Bk 10. 44 | 31 56 40 | 1882 id. Repsold I.
Sonfjell 18 27 | 31 18 36 1882-83 Heden. Repsold IT
Skurfdalsfjeli 25 55.2931 40.09 1882 Rosén. Repsold I:
Oxsjovalan 2392.30 |.31.98.29 1882 id. id.
Serffjell 1262) 38. bol 0.21 1882 ids id.
Hoverberg. ........ 249 48 | 32 6 0 1880 id. id.
Hundshogen Py OU) Ol 2a 30 1882 Heden. Repsold ie
Sol a2 0 1880 | Rosen. id.
Froson >|]: 6 Sle loon 1880 id. Repsold I.

48 40 | 31 35 45 1840 Häggbladh. Littman.
Bjekhult 46 16° 131 ] 1839 id. j
Ysnabjer co lo | 31 29226 1839 1
Gronhult 5 26 50 | 31 89 30 I
Graflunda 40 6 | 31 49 16 1839
Kaseberga 23 21 | 31 44 16 1839
Gladsax : 8 a3 A | 9). 50 52 1839
AUS un 58 al DL 8% 1839
Maltesholm 53; 6231.50 3 189)
2252 31 3938 1832
5 u ol 2 3 . 1832
0 32 30 44 1832
Se 32 32 1832
6 33. 4.52 1832
c 58 6 1832
> 10 83 30 1832
Utlangau l DU Aa 1831 :
Dufverum Bol: 5D 28 14 1832 id. id. \
Oland sodra fyr....| 56 11 34 3 54 1832-41 Cronstrand, Selander. Reichenbach, Littman
Majo é 33 42.35 1832 Cronstrand. Reichenbach.
Garpo 33 47 42 1832-41 Cronstrand, Selander. Reichenbach, Littman
Getlinee = + 6 5 | 34 6 0 SA Selander. Littman.
Hagbyudd 5G SIDA AS 1841 id. id.
Mysingshog 6 26 1841 id. id.
Hulterstad 56 Ad 2 1841 id. | id
Algutsrum ë | S4 1140 1841 id. id.
Ryssby 56 >10 1841 ad,
Getstaas 34 18 3: 1841 id
Kafvershall 5 34 2. 1841 id.
Borgholm 652 2% 27 54 1841 id.

|

 

 

 

 

 

 

 

 

 

 

 

 
