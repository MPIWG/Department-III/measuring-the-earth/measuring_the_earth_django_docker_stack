 

ENT

nimes

 

 

 

 

ÉTALONS
DE
COMPARAISON.

ÉQUATION DES RÈGLES
DE

L'APPAREIL,

COEFFICIENTS DE DILATATION.

FAITS MERITANT D’ETRE RELATES.

 

| Klafternormale de Vien-

ne, comparée en 1850
avec l’étalon Nde Pul-
kowa par le conseiller
d'Etat W. Struwe.
(Sitzungs-Berichte der
K. Akademie der Wis-
senschaften).

XLIV. Band, Wien, 1861.

»

Voir note 2°.

 

Les bases de Josefstadt, de Skutari et de Sign ont été publiées
dans les « Astronomisch-geodätische Arbeiten des K. K. mi-
litär-geographischen Institutes ». Volumes I, II et IM.

Portion de la base de Grossenhain mesurée pour comparer
les régles autrichiennes avec celles de Bessel.

 

 

 

 
