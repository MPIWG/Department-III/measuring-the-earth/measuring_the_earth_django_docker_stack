5

 

  
 

And probably it will be as well to introduce in this place the different operations
which have been made at all the stations on the Pacilie Coast, and in the adjacent schemes,
The stations so occupied number twenty-three from the Sierra Nevada westward, and fifteen
or more to the eastward.

I present to the Association several «progress sketches » which are also regularly
published in the annual reports of the Superintendent. On these are exhibited the lines obser-
ved, the base figures adjusted, the parts already reconnoitred, and the spaces yet unfinished.
One of these sketches exhibits not only the western part of the chain of triangles, but the
extension of the triangulation for the Pacific are to the south and to the north. At all the sta-
tions of the main triangulation on the Pacific Coast and extending eastward therefrom along
t | the arc of the parallel of 39°, the observations comprise the following:

HoRiZONTAL DIRECTIONS. These are made under all conditions of the atmosphere,

when the signals can be seen with the telescope. No more than two sets of observations are

| made in any one morning or in an afternoon. cach set consists of an observation with the

( telescope and circle direct, and one with the telescope and circle reverse. For lines under

| 100 miles in length (160 kilometres), 46 observations have been made in twenty-three posi-

| tions ; and for lines over 100 miles in length, 69 sets of observations were made at the last

| slations. Two sets have heretofore been made in one position of the horizontal circle, but my

| | experience dictates that hereafter only one set shall be made in one position, and the num-
| ber of positions will necessarily be nearly doubled. In 1876 the method of using the ocular
micrometer was introduced at Mt. Diablo station, and the observations have been improved

| thereby. The theodolite circle is 20 inches or 50,8 centimetres in diameter, and is read by
; | three microscope micrometers. The objective is 7,5 centimetres in diameter. The instrument
I | rests upon a specially devised position circle which is cemented upon the stone or brick pier.

AZIMUTH OBSERVATIONS. The theodolite which is employed for the Horizontal di-
rections is also used for the determination of the azimuth of one of the lines of the trian-
culation, or upon areference station that is usually six to ten miles distant. The stars usually
observed upon are + Ursæ Minoris and 4165 B. A.C. at opposite elongations, and as near
elongalion as practicable. The series gene rally includes observations before and observations
after elongation. | have made the same number of observations at each baseline station as

at all the other stations.

 

LATITUDE OBSERVATIONS. These are made with the Zenith telescope or with the Da-
vidson Meridian Instrument according to the Talcott method, and generally embrace obser-
vations upon 25 to 30 pairs of stars through 7 to 10 nights. I have made the same number of

| observations at each baseline station as at all the others. The north polar distances of the
' stars are obtained from about thirty original authorities.

 

OnsERVATIONS FoR isn. Double Zenith distances are measured upon the signals
at all the other stations between the hours from noon to one or two o’ clock in the after-

 

 
