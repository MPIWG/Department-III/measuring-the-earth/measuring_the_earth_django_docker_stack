 

 

 

bei Memel : Preuss.-Russ. = — 39.0
und bei Augustowo : Preuss.-Russ. = — 121.1.

Die Resultate dieser Rechnungen in Verbindung mit denen der astronomischen Be-
stimmungen sind nachfolgend zusammengestellt :

 

Astr. Länge | Astr. Azimut

gegen Greenwich. nordöstlich.

 

Station. Astr. Breite.

 

 

/ v
en eel 55, 9 26,28 Memel
Goldaperberg . . . || 54 16 58,33 2 17 34,495 | 76 56 43,87 | Nemexch
| 71 49 2.79 : | Jakobstadt
| 154 4 45,48 Goldaperberg

Memel . ...™ .\ of £3 4061 2 5 49,965 |

be. | 189 36 5.53 Nemesch
i ( 259) 24 32,97 Goldaperberg
}

Nemesch «©. . . «|| oF 38 3186 9 9 44,45 | Jakobstadt

 

 

 

 

 

m

Seite Goldaperberg-Memel == 178296,1
» Memel-Jakobstadt = 308343,5

» Jakobstadt-Nemesch = 208865,2

» Nemesch-Goldaperberg = 200322,5

Die drei Bedingungsgleichungen, welchen die geoditischen Ergebnisse dieses Vierecks
zu genügen haben, sind mit nachstehenden Schlussfehlern behaftet :

UY
5,15 in den Winkeln,
0,55 in der geogr. Breite,
0,69 in der geogr. Länge.

Behufs widerspruchsloser Ableitung der relativen Lothabweichungen gegen Goldaper-
berg wurden diese drei Gleichungen unter Hinzunahme der Laplace’schen Gleichung für
Goldaperberg-Memel (mit einem Fehler von 5.06) einer Ausgleichung unterworfen, und dıe
hieraus folgende Lothabweichung in Breite für Jakobstadt gegen Goldaperberg den weiteren
Rechnungen zu Grunde gelegt. Unter Benutzung der schon vorhandenen Rechnungen für
den Zug Paris-Rauenberg-Königsberg-Goldaperberg leitete man sodann nachstehenden Werth
der Lothstörung in Breite von Jakobstadt gegen Paris fiir das Clurke’sche Ellipsoid von
1880 ab:

6. — + 0,24 + 0,944 €, — 0,210 x, -+ 18320 a — 7976 da, (1)

1 Diese geogr. Längen sind astronomisch nicht bestimmt.

 

 

iittala

À
|

 
