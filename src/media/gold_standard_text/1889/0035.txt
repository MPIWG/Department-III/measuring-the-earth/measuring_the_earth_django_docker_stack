een te

 

 

29
vibrante étaient synchronisées électriquement par l'horloge directrice : le synchronisme était
parfait, car le tracé graphique était d'une régularité complète : une inégalité constante ou
systématique dans la période de la lame vibrante aurait entrainé l'arrêt de ses oscillations, ou
Lout au moins donné au tracé des signaux une allure périodique immédiatement reconnais-
sable sur les feuilles du chronographe. Il ne pouvait donc subsister que des inégalités
fortuites.

Le relevé de 75 groupes de signaux, répartis uniformément sur toute la durée des
expériences, à fourni 75 valeurs de la durée de 10 secondes, en fonction de la durée d’une
oscillation de la lame: la moyenne reproduit exactement le chiffre 10000, prévu par la
construction des appareils.

u Ces valeurs offrent un avantage qu’on rencontre rarement : elles représentent
€ un nombre connu a priori et que leur moyenne a reproduit exactement ; il en résulte que
« les divergences des résultats partiels avec la moyenne représentent les écarts absolus, au
« lieu de donner, comme dans les cas ordinaires, les différences avec une moyenne d’une ap-
« proximation incertaine en elle-méme. On a donc les éléments nécessaires pour étudier
« exactement la loi des écarts absolus. » (Loc. cit., p. A. 219.)

Le résultat de cette étude a été de prouver, comme on l'a dit plus haul, que les
erreurs suivent exactement la loi de Gauss. Bien que le nombre des valeurs soit petit (79,
représentant toutefois 7900 oscillations), le criterium pratique est vérifié : le double du
quotient de la moyenne du carré de ces erreurs par le carré de l’écart moyen reproduit la
valeur théorique (7) avec une approximation trés grande (3,1401 au lieu de 3,1416).

Ces vérifications expérimentales de la loi des erreurs sont loin d’être superflues : elles
justifient Pusage du calcul des prohabilites pour la determination ou la compensalion des
erreurs fortuites.

(est ace titre que étude entreprise par M. le général Ferrero est d’un haut intérêt,
non seulement pour la géodésie, mais encore pour la métaphysique des sciences d’obser-
vation.

M. d’Abbadie demande à M. le rapporteur s’il a distingué dans ses recherches si inté-
ressantes, les signes des erreurs.

M. le général Ferrero répond qu’il a tenu compte naturellement des signes pour les
données détaillées d'Italie, dont il disposait ; mais comme les autres pays ne lui ont envoyé
que les erreurs moyennes de leurs triangulations, il n'était pas possible de rechercher la dis-
tribution des signes dans les erreurs individuelles. C’est précisément pour cette raison que
M. le général Ferrero à exprimé le désir qu’on lui fournisse les erreurs de clôture de chaque
triangle ; il va même jusqu’à demander ces erreurs aussi pour les triangles de second ordre,
afin de disposer d’un nombre beaucoup plus grand de données sur lesquelles il puisse étendre
ces recherches.

M. le colonel Bassot a la parole pour lire son Rapport sur les Mensurations des
bases, lequel sera comme les autres reproduit sous forme d’Annexe dans les Comptes Rendus
de la Conférence. (Voir Annexe A. V.)

 
