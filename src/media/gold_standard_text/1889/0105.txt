 

 

99
die Präeisionsnivellements übertragen werde. Diese beiden Fragen, welche so nahe mitein-
ander zusammenhängen, würden auf diese Weise ıın nämlichen Bericht behandelt werden.

Dieser Vorschlag wird ebenfalls ohne Einwendung angenommen.

Der Herr Präsident geht alsdann zum dritten Kapitel des Programms über, welches
die Berichte der Herren Delegirten über den Fortschritt der Arbeiten in ihren betreffenden
Ländern enthält.

Nach der alphabetischen Reihenfolge kommt zuerst Öesterreich-Ungarn an die Reihe,
und das Wort wird dem Herrn Dr. Tinter, Präsidenten der österreichischen Gradmessungs-
Commission, ertheilt. Derselbe berichtet in deutscher Sprache über die Arbeiten dieser Gom-
mission.

Hierauf resumiren die Vertreter des geographisch-militärischen Institutes in Wien,
Herr von Kalmar und Herr von Sterneck, die von der geodälischen und astronomischen
Section dieser Anstalt ausgeführten Arbeiten.

Aus dem Bericht des Herrn von Kalmär geht unter Anderm hervor, dass auf das
Ersuchen der griechischen Regierung der Herr Oberst Hartl, in Begleitung von zwei andern
Offizieren, letzten Sommer mit dem Basisapparat sich nach Griechenland begeben hat, um
die Erdmessungsarbeiten dieses Landes zu organisiren. Die Präcisionsnivellements wurden
im Laufe des Jahres um ungefähr 100 Kilometer erweitert, und die 7 fundamentalen Fix-
punkte sind.gegenwärtig alle gesetzt. |

Herr Oberstlieutenant v. Slerneck constatirt, dass man im Jahre 1889 zwischen der
Schneekoppe und Dablic eine Längenbestimmung, sowie auf sechs Stationen des Gradmes-
sungsnelzes von Böhmen Breiten- und Azimutbestimmungen ausgeführt hat. Der Ilerr Be-
richterstatter stellt in Aussicht, dass durch diese Länge, sowie durch die auf AD Stationen in
Böhmen sorgfältig ausgeführten Breiten-, Azimut- und Schwere-Bestimmungen man in Balde
alle Elemente besitzen werde, um daraus die Geoidoberfläche des böhmischen Netzes her-
leiten zu können.

Ferner theilt Herr v. Sterneck, im Namen seines in Griechenland weilenden Kolle-
gen, Herrn Obersten Hartl, einen Bericht über die von Letzterem geleiteten trigonometrischen
Arbeiten mil, welche sowohl den trigonometrischen Anschluss der Universilätssternwarte
von Wien, als auch die Fortsetzung des Netzes erster Ordnung in Siebenbürgen umlasst.

(Siehe diese verschiedenen Berichte Oesterreichs in den Beilagen B. XXI.)

Der Herr Präsident dankt den Herren Delegirten von Oesterreich-Ungarn und ladet
darauf Herrn Obersten Hennequin ein, seinen Bericht über Belgien zu verlesen.

Der Herr Hennequin theilt mit, dass die Arbeiten des cartographisch-militärischen
Institutes in Bezug auf die Triangulationen seit der letzten Generalconferenz sich wesentlich
auf die Recognoscierungen der geodätischen Punkte erster Ordnung beschränkt haben.

Die auf die belgische Triangulation angewandte Genauigkeitsrechnung nach der von
General Ferrero angegebenen Formel 'hat den Werth m = + 089 ergeben, während
die Ausgleichungn ach der Enke’schen Formel für e den Werth e = + 0.993 gegeben hatte.

 
