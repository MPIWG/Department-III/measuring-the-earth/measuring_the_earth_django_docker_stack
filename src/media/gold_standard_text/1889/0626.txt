 

2

B. Die Triangulirung I., IH. und Ill. Ordnung in Siebenbürgen und dem angren-
zenden Theile Ungarns behufs vollständiger Dotirung der Reambulirung mit derartig be-
stimmten Punkten.

C. Die Erginzungsbeobachtungen ım Netze I. Ordnung im Bereiche der früheren
Rayons.

D. Die Fortsetzung des Präcisions-Nivellements in Nordost- Ungarn und Ost-Galizien
zur Herstellung doppelt nivellirter Schleifenschlüsse, mit Einbeziehung von an den Nivelle-
mentslinien liegenden Triangulirungspunkten, meteorologischen Stationen und Flusspegeln.
Endlich die Herstellung der zwei letzten Nivellements-Hauptfixpunkte bei Budweis und Fran-
zensfeste.

Im Laufe des letzten Winters hat die königlich griechische Regierung durch unsern
werthen Herrn Gollegen, G. Carusso, als speziellen Bevollmächtigten in Gataster-Angelegen-
heiten, das Ansuchen an die österreichisch-ungarische Regierung gestellt, für die Trianguli-
rung in Griechenland einen leitenden und noch zwei andere in der astronomisch-geodätischen
Gruppe ausgebildete österreichisch-ungarische Offiziere zu commandiren, sowie zu gestat-
ten, dass der Basisapparat des militär-geographischen Institutes an Griechenland leihweise
überlassen werde, und dass auch zwei griechische Offiziere in Oesterreich-Ungarn in der
Geodäsie ausgebildet werden.

Diesem Ansuchen wurde bereitwilligst entsprochen und es ist unser werther Herr
College Oberstlieutenant Hartl, sowie Hauptmann Franz Lehrl und Linienschiffs-Lieutenant
Julius Lohr, mit dem Basis-Apparate Mitte August nach Athen abgegangen.

Die Arbeiten des Jahres 1889 sind :

A. PRÆCISIONS-NIVELLEMENT.

Dem vorangeführien Erlasse des k. u. k. Reichs-Kriegsministeriums entsprechend,
wurde das Pracisionsnivellement in Ost-Galizien und Nordosi-Ungarn fortgesetzt; da es jedoch
grösstentheils zweite Messungen waren, welche im heurigen Rayon vorgenommen worden
sind, so wurde die Gesammtlänge der theils doppelt theils einfach nivellirten Linien mil
Knde des Jahres 1889 etwa um 100 Kilometer vermehrt.

Ausgeführt wurden :

a) Zweite Nivellements auf den Linien :

Stanislau-Stryj-Chyrow (Bahn-Nivellement);

Lanezyn-Kolomea (Strasse) ;

Gsap-Ungvar-Uszokpass-Chyrow-Przemysl (Strasse) ;

Przemysl-Lemberg (Swasse);

Kniaze-Krasne-Lemberg (Bahn), und

Corlkow-Trembowla (Strasse).

Auf den vier letzten Strecken wurde die Mehrzahl der Hohenmarken, die bei der

 

 

 
