 

 

47

 

publies, qui est parvenu à reproduire d'anciens documents et imprimés, en revivifiant
l'encre par des moyens chimiques et en s’en servant pour la réimpression, procédé qui,
dans des cas particuliers, peut rivaliser avec la reproduction photographique, employée par
M. le Général Ferrero.

Dans ce procédé on commence par décaper et par dégraisser le papier à l’aide d’un
acide; puis on le charge d’encre avec un rouleau. L’encre ne prend pas sur le fond et
adhére seulement A l’encre ancienne. On fait un décalque de cette feuille sur pierre ou sur
zinc, puis on procède au tirage par le mode lithographique.

M. Cheysson a fait reproduire ainsi à peu de frais, un volume nécessaire pou
compléter une collection dépareillée. Il a de même pu reproduire des documents remon-
lant A plus d’un siècle et demi. 11 peut rendre de véritables services dans des cas particuliers.

La séance est levée à 6 heures.

 
