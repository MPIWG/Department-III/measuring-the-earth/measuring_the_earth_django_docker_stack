 

Beilage B. XXIIIe.

BERICHT

des Oberstlieutenant von Sterneck über die von ihm ausgeführten
Schwerebestimmungen für das Jahr 1889.

Im Laufe des heurigen Jahres wurden mit dem neuen Pendelapparate auf nachfol-
genden Punkten I. Ordnung des böhmischen Dreiecksnetzes Schwerebestimmungen ausge-
führt

1. Svidnik, 2. Mezivratach, 3. Pecney, 4. Schneekoppe, 5. Dablic, 6. Donnersberg,
7. Sadska, 8. Visoka.

Im Frühjahre wurden in Wien die Constanten des Apparates neuerdings bestimmt.

Auf drei Oertlichkeiten in Böhmen, nämlich auf den Bergen Jeretin, Hasenburg
und Rip wurden im Monate August specielle Untersuchungen über das Verhalten der Schwere
ausgeführt. Diese Berge stehen vollkommen isolirt in der weiten Ebene, haben eine sehr
regelmässige geometrische Form und bestehen aus Basalt.

Es sei mir gestattet, hier einige Resultate dieser Untersuchungen mit wenigen
Worten zu besprechen, da dieselben einestheils die Leistungsfähigkeit des neuen Pendelapp: \-
rates zeigen, andererseits ein, wie ich glaube, nicht unwichtiges Resultat ergeben.

Auf dem Berge Rip wurde nämlich die relative Grösse der Schwere unter anderem
auch auf zwei Stationen auf seinem oberen Plateau in der Weise bestimmt, dass aus vollkom-
men gleichzeitigen Beobachtungen die Unterschiede der Schwingungszeiten von vier invariablen
Pendeln zwischen einer etwa Akm entfernten, in der weiten Ebene 250™ tiefer liegenden Station
im Dorfe Netes, und den Stationen auf dem Bouepidtenn ermittelt wurden, zu welchem Zwecke
diese Stationen telegraphisch verbunden wurden.

Das obere Plateau des sonst sehr regelmässig en Berges ist ziemlich uneben,
dla stellenweise Basaltfelsen zu Tage stehen und Erhe ebungen verursachen, während die da-
zwischen liegenden Partien, welche mit Gesteinstrümmern in Humus ausgefüllt sind, einige
Meter tiefer liegen, so dass das Plateau ein hügeliges, unebenes Aussehen hat.

Kine der ausgewählten Stationen befand sich direet anf dem anstehenden Basaltfelsen

 

 

 

 

 
