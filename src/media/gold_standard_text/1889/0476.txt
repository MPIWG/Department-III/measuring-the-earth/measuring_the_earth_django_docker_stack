|
|

 

 

AQ

 

 

NOMS DES PAYs.

DESIGNATION DE LA BASE
ET ANNEE DE LA MESURE
SIMPLE OU DOUBLE.

 

6. Base de Tachbunar.
1852.

7. Base d’Elima.
1844.

8. Base d’Uleaborg.
1845.

9, Base de Oefver Tornea.
1851.

Mesurée deux fois.

10. Base d’Alten.

POLOGNE.

11. Base de Varsovie.
1846.

 

12. Base de Tarnograd.
1847.

13. Base de Czenstochau.
1848.
BUSS LE.
PARALLELE DE 52°

14. Base de Varsovie.
1846.

 

 

 

LONGUEURS

ERREURS |

 

 

 

NOMS PROBABLES DÉSIGNATION
CALCULÉES :
DE ET RÉDUITES = ee
OBSERVATEURS. a © | ERREU RS APPAREILS DES BASES EMPLOYÉS.
AU NIVEAU DE LA MER. RELATIVES.
. - t . .
Prazmowski, 2770,2461 1 Appareil ancien de Struve
Wagner. 1240000 nor
Sabler, 1348,7457 1 >
Woldstedt. 1240000
» 1505,3175 ) »
{|
Lindhagen, 1519,8386 » Appareil de Struve n° 2 a])
Selander, levier de touche mobile
Skogwan, perfectionné.
Wagner.
Lindhagen, 1154,7439 » »
Klouman.
Tenner et Slobin. 2710,4847 1 Appareil de Tenner
335000 à languettes.
Iwanow Ier. 2529,0572 » »
» 2048,4375 » »
Terner , Slobin. 2966,9702 1 | Appareil de ‘Tenner.

 

 

 

~ 825000

 

 

see

——

——

ee inane

nm

|
|

I

=

 

 

 
