Ze

I. — Autriche-Hongrie. a
we u Te Ve 4

DIRECTEURS
un LATITUDE. |LONGITUDE.| POQUE. WO SU

DES POINTS.

ET OBSERVATEURS.

REMARQUES, _

 

 

 

Pecen

Saualpe...........-
St. Peter

Golica

Gerlitzen

Eisenhut

Staffberg

Thorkofi

Kosuta

Plegas

Radica

Udine
Mrzavec

Urasica

— Grintotie
Gurivreh
Donati

Wurmberg
Buchberg
Base de Kranichs-

Terme Nord.
Base de Kranichs-
feld
Terme Sud.
Ivancica
ee

Petrivrh

Brzevopolje

Kueerina

DubDiCa 0 5.00.

Base de Dubiea....
Terme Nord.

Base de Dubica . .
Terme Sud.

Alje

Vristik I

Hunka

Priseka

Plesivica

Petrovac

Ivanich (Observ.)
Oklmak.. 2...
Privis :

Biela-lasica........
Treskovac

Tuchovie
Zerkh

Krimberg
Schneeberg

 

46° 30' 44"

46 51 16
46 37. 30
46 29 32
46 41 44
46 57 10
46 44 12
46 42 9
46 26 17
46 9 54
46 13 42

46 3 55
45 58 44

46 8 26

46 21 28
46 15 29

46 15 48

46 28 29
46 26 6

46 28 15

25 45

10 55
54 1

10

23 08
20 22

12 20

14 49
13 14
15 15

259
36 53

HA

12; 16
44 16
19 2
44 21
43
23
8
16

0
20
32
46 3
Dora
55 45

> 35 21

 

32° DA 5S"

32 19.2
31 59.29
31 43 14
31 34 49
öl 95.9)
ol 0 LE
30 44 32
32 0 36
31 46 46
31. 91.52

30 54
al 28 18

32 10 41

3212 sd
32 28 57
33 24 31

33 28 46
33 16 42

33 21 4

33 23 37

33 47 37
341 47 36

34 58 44
a0 075
34 46 2
34 27 53
34 30 10

34 30 10

34 22 34
40 20

4 25 12
56 14

Oo Olea
320 4
3 28 16
9

ol

20

 

 

1861-77

1877
1876-77
1861-77
1877-79
1879-80

1879

‚1879

1877

1861
1861-84

1861
1861-84

1861-77

1861-77
1861
1877-80

1877
1877

1877

1877

1877
1877-78

1878
1878
1878-79
1878
1879

1878-79

1879
1879
1877-79
LST
1879
1877
ieee

1878
1877-78
1877-78-79
1855-61-72
78
1874-78
1861-78
1861-78
1861-78
1861-77
1861-77
1861

Vergeiner, Hartl.
Hartl.

id.
Breymann, Rehm.
Rehm, Hartl.
Hartl.
id.
id.
Rehm.
Breymann.

Merkl, Fiala.

Breymann.
Breymann, Lehrl.

Vergeiner, Breymann,
Rehm.

Vergeiner, Rehm.

Vergeiner.

Rehm, Corti.

Hartl.
id.

Ganahl, Rehm.

Hartl.

Molnär, Rehm.

Podstawski, Molnär, Ster-
neck.

Sterneck.

Randhartinger.

Randhartinger, Rehm.

Molnär, Nahlik.

Kalmar.

id.
Lehrl.

Kalmars =
Podstawski, Rehm.
Podstawski, Kalmär.
Rehm.

Podstawski.
Podstawski, Schwarz.

Nahlik.

Kalmär.

Kalmar, Schwarz.

Breymann, Schwarz, Nah-
lik.

Hartl, Kalmar.

Breymann, Kalmar.

Vergeiner, Kalmar.

Breymann, Kalmar.

Merkl, Rehm.

Breymann, Rehm.

Vergeiner.

 

 

Starke n° 3

10 p.
Starke ae de 10 p.

id.
Id. n° 1 et 5 de 10 p. |
Id. 20 1eb3 de 109

Starke n° 3de 10 p.
id.
id.
Starke n° 1 de 10 p.
Starke n° D» de 10 D.

Starke n° 1 del0 p: ||

et n° L de 8 p-
Starke n° 5 de 10 p.

nat de 8p.

Starke n° 1, 4 et 5 |

de 10 p:

Id. n° 1 et 4de10>p. ||
Starke n° 4 de 10 p. |
Starke n° 1 de 8 p. |

ot 10°p:-
Starke n° 3 de 10 p.

id.

Starke Univ. de 13 p.

Starke n° 8 de 10 p. |
Tdsne jet 5 de 10)

Starke ne I de Sp.
n° 4 et b de 10:p.
Starke n° 4 de 10 p.

Starke n° 2 de 8 p. |
Id. n° 2 et 4 de 10 p. |
Starke n° 5 de 10 p. |
Starke n°4 de 10 p. |

Id. n° 2 et 4 de 10 p.

Starke n° 2 de 10 p. |
id.

Starke n° 4 de 10 p.

Id. n° 2 et 4 de 10p. |
Starke n° 4 de 10p. |
id

id.
Id. n° 1 et 4 de 10 p.

Starke n° 4 de 10 p.'|
Starke n° 2 de 10 p. |
Id. n° 1 et 2 de.10 p. |
Starke n° 2, 4 et 5 de |

Ion.
Id. n° 2 et 3 de 10 p:
Id. n° 2 et 5 de 10 p.
Id.

Starke n° 4 de 10 p.

et 4 de |

 

 

 

n° 2 et 4 de 10 p. |
Id. n° 2 ebb. dead pe
Starke n° 1 de 10 p. |
| Id not et.) de lO py

litalies
Id. m 5 de 10 pet}

 
