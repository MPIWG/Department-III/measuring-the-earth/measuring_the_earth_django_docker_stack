°
t

es

 

nn eee

 

 

a

LocaL DEFLECTION OF THE PLUMB LINE. In these operations of the triangulation the
observations of the Latitude and Azimuth have exhibited great irregularities in the local
deflection of the plumb line at the various stations. In the meridian the range of deflection
from the mean which has been adopted amounts to nearly seven seconds of are plus and
minus : and in the prime vertical to nearly the same amount. This has led to the adop-
tion of provisionally standard geodetic data for a given station and for a given line; that is, a
certain station has assigned to it a given latitude and longitude, which is very closely the
mean of all the stations reduced thereto, and the azimuth from that station to another given
station 1s adopted as the true azimuth thereof. The direction of these deflections ofthe plumb
line can sometimes be predicted from surrounding geological and orographical conditions, but
in some cases, as on the Plains of Los Angeles, large deflection was found where none had
been anticipated. In this connection | may say that the Superintendent has directed me to
present to the Association twenty or more photographs which exhibit some of the physical
features of the trigonometrical stations which have been occupied in the line of the Sierra
Nevada by the party under my direction. Besides this they give proof of the frequent clearness
of the atmosphere in showing sharply mountain outlines that were at least sixty miles (96 ki-
lometres) distant. One of these exhibils a summer view of the Yosemite Valley through which
the triangulation party will pass to Mount Conness.

Il. — THE PRINCIPAL ARCS OF THE MERIDIAN AND OF THE PARALLEL

THAT HAVE BEEN, OR WILL BE, DEVELOPED FROM THE COAST TRIANGULATIONS.

I shall enumerate these very briefly.

1. The Nantucket arc of the Meridian : 5 /,° have been finished ; and it will be ex-
tended to 6°. It lies between 41° 15’ and 470 30".

2. The Pamplico-Chesupeake are of the Meridian : 4 1/,° have been finished, and it
will be extended to 11 !/,° between the latitudes 35° and A6 !/,°.

3. The Lake Superior and Gulf of Mexico are of Ihe Meridian : 10° 12 of this arc
were executed before 1878 by the Corps of United States Engineers, and it will be extended
by the Coast and Geodetic Survey to the Gulf of Mexico, making a total length of 18° 35”, be-
tween the latitudes 30° 10° and 48° 45”.

4. The Pacific arc of the Meridian ; the triangulation has been executed from about
latitude 39° to latitude 34°, and it will be extended through California, Oregon, and Washington
to the boundary between the United States and British Columbia in latitude 49°; making a
total of 15° within the limits of the United States. Northward of the United States it can be
carried through British North America to the Arctic Ocean.

Within the limits of the United States this main triangulation is carried on be-
tween the Sierra Nevada or the Cascade Range on the east, and the Coast Range on the west,
and involves lines of great length. The longest line yet observed has been 192 miles or
307 kilometres ; and the longest reconnoitred is 244 miles or 390 kilometres. This great

ANNEXE B. XVII — 2

 

 

 
