 

 

 

6

s. Die Einstellungen des beweglichen Fadens auf den Stern sind an bestimmten

Stellen des festen Fadennetzes vorzunehmen, und zwar wegen etwaiger Faden-
schiefe genau symmetrisch zur Mitte. Die Fadenschiefe ist gelegentlich zu messen.

An aufeinanderfolgenden Abenden sind überdies für ein jedes Sternpaar
entgegengesetzte Fernrohrlagen anzuwenden.

4 bis 5 Pointirungen in Abständen von etwa 1o Aequatorialsekunden mit
jedesmaliger Ablesung der Schraube folgen hintereinander, wobei immer einige
Sekunden lang die Bisektion zu prüfen ist.

Die Schraube muss vorsichtig gedreht werden, um nicht durch ein Verdrücken
des Fernrohres Veränderungen im Stande der Libellen herbeizuführen. Um dieses
sicher erzielen zu können, ist 2" bis 3" vor Beginn der Beobachtung der beweg-
liche Faden bis auf # 1" in den Parallel des Sternes zu bringen.

Die Einstellungen der Schraube sind thunlichst in der Richtung des An-
spannens der Feder zu bewirken.

. Die Blasenlinge der Libellen soll '/; bis ‘/2 der Theilungslänge, der: letzteren

betragen.

Bei beiden Komponenten des Sternpaares muss nahezu dieselbe Stellung der
Blase vorhanden sein und zwar immer nahezu in der Mitte der Theilung.

Die Ablesung erfolgt je ein Mal vor und nach der Sternbeobachtung, wobei
darauf zu achten ist, dass die Blase einen festen Stand erlangt hat. Bei der Ab-
lesung bedient man. sich eventuell eines kleinen Ablesefernrohres aus freier Hand.

Der Unterschied der beiden Niveauablesungen vor und nach den, Pointirun-
gen soll im Mittel o’ıs, im Maximum. 0’5o nicht überschreiten.

. Der Zustand des Himmels und die Güte der. Bilder ist unter Anwendung von

Zahlwerthen zu notiren.

. An der Okularröhre ist eine Theilung oder mindestens ı Strich anzubringen,

wodurch die Stellung dieser Röhre und damit diejenige des Fadenkreuzes zum
Objectiv fixirt wird. Bei dieser Stellung ist der Schraubenwerth im Laufe der
Zeit mehrfach, insbesondere bei verschiedenen Temperaturen, auf 1/sooo seines
Werthes zu bestimmen.

Die fortschreitenden und periodischen Fehler sind am Beginne und am
Schlusse der ganzen Beobachtungsreihe zu untersuchen, womöglich auch bei ver-
schiedenen Temperaturen.

. Der Theilwerth der Libellen, welcher erfahrungsmässig nicht über ı” betragen

soll, ist für Ausschläge nahe der Mitte bei verschiedenen Temperaturen und
jedenfalls am Anfang und Schluss der Messungsreihe in Bruchtheilen der Schrauben-
umdrehung zu ermitteln.

 

|
|
|
|
|

 

!
j
j

 
