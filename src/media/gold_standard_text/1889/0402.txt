eg

XV. — Russie.

INDICATION : DIRECTEURS : >
LATITUDE. |LONGITUDE. EPOQUE, INSTRUMENTS. | REMARQUES. .
DES POINTS. ET OBSERVATEURS. |

 

 

—— ——

Triangulation de Lithuanie. Jonction à Mémél. (Swite.)
16595225) 40°13. 22°
Déjà mentionnés. |

56% int) 49 40 7
: De oo: 4, 239 ||
Couvent Polaven...| 55 48 14 | 42 32 48 |

Domiany
Butninuy

Poschtschutaty ...

Grusche
Kemiany

Lanksaitzi........

Schipili
Seliany
Lopaitzi
Eitentaitzi

5d 58 26 | 42
06. 7° 8.| 42
5d 55 54 | 41
50 118141
56 15 10 | 41
15 0 | 4l
& 8,4]
56 13 | 41
7 41 | 41
56 1 | 40
5 31 | 40
1 7 440
45 26 | 39
20,39

 

 

Zwirblastzy. ....... 581839
Zwaschni 44 35 | 39
Chweidany 342 3959
Medweigola........ DS An 40
Lepaitzi 59,38 1939

estre & répétition de Reichenbach de 12 p.

Lieut.-Général de Tenner.

~

étition de Troughton.

| N. 142, Prusse. —

Jakubowo
Tarwidsi
Grabschitzi

55 49 52 | 38
55 58 14 | 38
>91 383.39

 

56 38:
Polangen 505.3

 

 

 

Cercle à répétition de Baumann de 13 p.

Cercle à rép
Théodolite terr

 

 

 

 

Triangulation du Parallèle 52°.

Tarassowzi * ba : ;
Paks Guiischi | Déjà mentionnés. |
Mirotetchy So ie Age 38 \
Mitskewitchy 16 43 25 |
Juchkewitchy 14 43 55
Roskocha 32 44
Kroutoi-beregue ... 16 44
Kouchtchitza 3 44
Koukowitchy D 44
Jodtchilsy 02: 59,1 44
Lutowitchy 2 54 44
Boulatniki 4 44
Kondratowitchy ... 13 44
5 45
Bototchitsa 56 45
Mirgora l 45
Mordwitowitchy ... 50 45
Bassıka. ©... 55 45
47 45
54 46

4 46

5 46

vil 46
52 46
59 47

Lieut.-Général
de Tenner

Travaux
supplémentaires
Colonel Zylinski

 

 

 

 

 

 

 
