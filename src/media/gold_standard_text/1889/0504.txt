 

2

Italien. Herr Professor Lorenzoni hat seine Messungen der Schwerkraft zu Padua
aus den Jahren 1885 und 1886 reducirt und ein grösseres Werk darüber publieirt, welches
auch eine vollständige Darstellung der bezüglichen Theorien giebt. Aus der Vergleichung mit
fünf Stationen der Nachbarländer findet sich nach diesen Messungen die Intensität der Schwer-
kraft zu Padua nahezu normal, während die älteren Messungen von Biot vom Jahre 1824
einen lokalen Ueberschuss von !/aoooo andeuteten. Vergl. übrigens Nr. 1 der Schlusstabelle.

Spanien. Die von Herrn Colonel Barraquer 1882 beobachtete Pendellänge des Obser-
vatoriums zu Madrid bildet den Gegenstand einer ausführlichen Abhandlung des jetzt erschie-
nenen VII. Bandes der Memorias del Instituto geografico y estadistico. Zugleich sind daselbst
die Ergebnisse der Vorversuche von 1877 auf dem Instituto geografico mitgetheilt. Vergl.
Nr. 2 und 3 der Schlusstabelle.

Oesterreich. Herr Oberstlieutenant von Sterneck hat 1887/88 in Tyrol auf zwei Nivel-
lementslinien, welche in Innsbruck beginnen und nach Ueberschreitung der Alpen in Bozen
zusammenlaufen, den Gang der Schwerkraft durch relative Messungen auf 37 Stationen im
Anschluss an Wien ermittelt. Ausserdem wurde auf vier Hochstationen von 1044 bis 2760»
Höhe beobachtet. Der angewendete Apparat, die Methoden und ein Theil der Resultate sind
im Vil. nnd VIII. Bande der Mitiheilungen des k. k. militär-geographischen Instituts publi-
eirt. Die Ergebnisse dieser Messungen bilden zudem den Gegenstand einer Mittheilung des
Herrn von Sterneck auf dieser Konferenz, so dass ich sie nicht aufzuführen nöthig habe. Sie
gestalten u. À. eine interessante Anwendung auf die Berechnung des Einflusses der Ano-
malien der Schwerkraft auf die Ergebnisse der Nivellements im Hochgebirge.

Nach den Verhandlungen in Rom, 1883, p. 269, ist in der obenerwähnten Nivelle-
mentsschleife in Richtung N. W. S. E. das

f äh 04180

e

praktisch gefunden worden. Der theoretische Werth aus der Gleichung

i (a m Î (9— go) ah

wo g, der Mittelwerth 9,804 der Intensität der Schwerkraft innerhalb der Sehleife ist, ergab
sich mir durch graphische Behandlung gleich — 0"025 Hiervon kommen auf die Aende-
rung der Schwere mit der Breite 0"007 wie schon Herr von Sterneck angegeben hat.

Im Sommer 4889 wurde begonnen, die Intensität der Schwerkraft auf den sämm-
lichen 40 Hauptdreieckspunkten Böhmens mit dem neuen Pendelapparat zu bestimmen, und
zwar kamen acht Stationen zur Erledigung.

In Russland sind ebenfalls umfangreiche Operationen im Gange (vergl. Gonptes
39m f) o | Q VIN - dr pes > , . ry . ?
rendus 1889, CIX, p. 357, Observations de pendule effectuées en. Russie. Extrait d'une lettre
adressée à M. Faye, par M. le general Stebnitzki). Die kaiserliche Gesellschaft für Geographie

 

|
|
|
|
|
i
|

 
