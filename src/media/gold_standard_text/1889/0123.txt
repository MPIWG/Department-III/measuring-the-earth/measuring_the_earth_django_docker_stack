 

 

 

147

Als der Herr Präsident die Frage aufwirft, welchen Weg man zu betreten habe, um
diese Abänderung eines Artikels der Gonvention zu erreichen, wird nach einer kurzen Be-
sprechung einstimmig beschlossen, dem Bureau der Permanenten Commission diese Sorge
zu überlassen.

Die Herren Hennequin und Davidson fühlen sich durch dieses Zeichen des Ver-
trauens, welches die Conferenz ihnen bezeugt hat, sehr geehrt und erklären, dass sie keine
Anstrengung scheuen werden, um zum Gedeihen des gemeinschaftlichen Werkes beizu-
tragen, wenn die Regierungen auf den Wunsch der Conferenz eingehen.

Der Tagesordnung gemäss beschäftigt sich die Versammlung mit dem Bericht der
Finanz-Commission über das Budget und die Feststellung der Beiträge.

llerr Fwrster verliest als Berichterstatter folgendes Schriftstiick :

«Nach Art. 9 der Uebereinkunft soll eine neue Vertheilung des Budgets unter den
betheiligten Staaten stattfinden, sobald der Beitritt anderer Staaten zur internationalen Krd-
messung, unter Beibehaltung der bisherigen Ansetzung der Beiträge, eine Ueberschreitung
der auf 16000 M. — 20000 Fr. angesetzten Grenze der Dotation der Permanenten Commission
zur Folge haben würde. ;

Offenbar gehört es zu den Obliegenheiten der Generalconferenz als des obersten
konstituirten Organs dieser internationalen Verwaltung, über eine solche neue Vertheilung
der Beiträge Beschluss zu fassen.

Die Commission hat in dieser Beziehung gegenwärtig folgende Sachlage vorgefunden:

Im Jahre 1887 hatte die Gesammtheit der Beiträge, welche von den bis dahin bethei-
listen zwanzig Staaten bestiimmungsimässig zu entrichten waren, mit Binschluss von Chile
(welches bald nach seinem Eintritt die für den zehnjährigen mit 1887 beginnenden Zeitraum
kapitalisirte Einzahlung geleistet hatte) nur den Betrag von. . . 13640 M. = fr. 17050
erreicht.

Alle diese Beiträge sind für das Geschäftsjahr 1887 entweder während dieses oder
im Laufe des folgenden Jahres wirklich bezahlt worden.

Im Jahre 4888 hat die Gesammtheit der Beiträge, welche
von den bis zu Ende dieses Jahres beigetretenen vierundzwanzig
Staaten zu entrichten waren, bestimmungsmässig den Betrag von 16320 M. = Ir. 20400
erreicht. Aber bis zum heutigen Tage fehlt noch die wirkliche Zah-
lung der Beiträge von dreien dieser Staaten, nämlich von Japan,

Maske md Serbien, zusammen me ¢ 3 2... 8 . . . 9440 M. = fr. 3050
so dass die Gesammtheit der vertragsmassigen Einnahmen für das
Ge ch sah 1888 nr beiradenhalu ni. Agee MS ir. 17350

Was das Jahr 1889 betrifft, so wird die Summe der Bei-
träge, welche von den bis jetzt beigetretenen sechsundzwanzig
Stadien! zu’zahlen: sind, "sieh aufs > a0 2 \ Je Sooo Wi tk. 22000
erheben, aber bis jetzt sind hierauf nur eirklicn Irre ihrem 3436er. 14200

 
