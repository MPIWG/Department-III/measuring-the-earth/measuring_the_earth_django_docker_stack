 

a

 

 

« Monsieur le Ministre,

« Je saisis avee empressement Poccasion qui se présente à moi, pendant les minutes
durant lesquelles je dois encore occuper ce fauteuil, comme Président de la Commission
permanente, pour prononcer quelques paroles de réponse au discours de Votre Excellence et
remercier le gouvernement français de l'accueil sympathique et bienveillant qu'il vient de faire
à l'Association géodésique internationale.

«Ge n’est pas la première fois que celle-ci siège à Paris, comme Votre Excellence
vient de le dire. Déjà en 1875, peu de temps après l'entrée de la France dans cette vaste
Association qui embrasse maintenant le globe entier, son organe permanent, que J'avais comme
aujourd’hui l'honneur de présider, grâce à la bienveillance de mes collègues, à tenu ici-méme
sa session annuelle ; mais, dans l'occasion présente, c’est la Conférence générale, composée
de délégués de tous les Gouvernements, qui se réunit périodiquement, d’après une disposition
réglementaire prescrivant des sessions triennales de cette Conférence.

«Le Gouvernement français, ainsi que les savants géodésiens et les astronomes de
ce pays, continuant la tradition de leurs illustres devanciers, ont puissamment protégé el
encouragé notre vaste institution, à laquelle on est redevable, non seulement d'innombrables :
déterminations scientifiques dans les domaines de l'astronomie, de la géodésie et de la phy-
sique, mais aussi de laccomplissement de grands travaux de jonction astronomique el géo-
désique réalisés d’observatoire à observatoire d’un même pays, de nation à nation, de conti-
nent à continent. Qu'ils reçoivent tous, Gouvernement et savants, les remerciements les plus
chalenreux de la part de l'Association, et qu'hommage soit rendu à la mémoire de Vinfatigable
ododésien et académicien francais, le général Perrier, dont la voix ne résonnera plus dans nos
\ssemblées, et qui a disparu peu de temps après le savant astronome autrichien von Oppolzer,
Vice-Président de la Commission permanente, mort quelques semaines seulement après la
dernière Conférence générale.

«Je suis heureux de pouvoir saluer l'achèvement des observations de la nouvelle
méridienne de France, qui permettra d'entreprendre prochainement le calcul du plus grand
are de méridien qui ait été jusqu’à présent tracé sur le globe, embrassant 28 degres environ,

“ depuis le nord de l’Ecosse jusqu’au Sahara algerien, grande operation que lon doit aux géo-

désiens anglais, francais el espagnols, et a laquelle a collaboré, dans tous ses grades, depuis
celui de lieutenant jusqu’à celui de général inclusivement, linoubliable et regretté Perrier.

« Le Gouvernement français et, en son nom, le même général, ont également apporté
à la géodésie la mesure d’un are de parallèle de dix degrés d'amplitude, en Algérie, qui
pourra être utilisé pour la future détermination de la forme et des dimensions terrestres.

« Le rôle prépondérant joué en géodésie par la mesure de l'intensité de la pesanteur
n’a pas été négligé dans le programme des travaux du Service oéographique de Parmée fran-
caise. Au contraire, ces sortes de déterminations ont été vigoureusement poussées en y ap-
portant de notables perfectionnements, aussi bien dans les instruments dont il a fallu se ser-
vir que dans les méthodes de mesure de l'intensité absolue et de l'intensité relative de la
pesanteur.

PROCÈS-VERBAUX — 2

 
