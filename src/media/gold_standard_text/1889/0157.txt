 

 

|
Ih
;
{
|
*
b
£

Annexe A. II.

RAPPORT

sur l’état des travaux faits pour la détermination du niveau des mers
de l’Europe continentale,

PAR

M. LE GÉNÉRAL DE Diviston, Marquis DE MULHACEN

r r

L’Association géodésique internationale, dans la Conférence générale tenue A Berlin
au mois d@octobre 1886, m’a de nouveau confié la mission de rédiger un Rapport sur l'état
des travaux faits depuis 1885 pour la détermination du niveau moyen des mers en Europe,
lequel devait être présenté l’année suivante à la Commission permanente dans sa session de
Nice. Je me mis en mesure de m'acquitter de cette mission, et à cel effet J’adressai aux délé-
oués des nations maritimes du continent européen, par l'intermédiaire de notre Bureau cen-
tral à Berlin, un questionnaire destiné à recevoir tous les renseignements nécessaires à l’éla-
boration de mon nouveau Rapport. Plusieurs nations ne purent cependant, pour des causes
diverses, me faire parvenir à temps les renseignements demandés, et la Commission perma-
nente, dans sa session de 1887, vouiut bien renvoyer la présentation du Rapport en question
à la prochaine Conférence générale, devant avoir lieu à Paris en 1889.

Dans le but d'offrir dans ce nouveau Rapport les renseignements les plus complets
et les plus récents possible, j'ai laissé à dessein passer toute une année, et ce n’est qu’au
commencement de l’année actuelle que je me suis de nouveau adressé à nos honorés Collègues,
lesquels ont tous répondu à mes démarches avec le plus gracieux empressement, ce dont je
les remercie bien vivement.

La Conférence générale de Rome en 1883, a, sur ma proposition, prié les Etats
maritimes faisant partie de l'Association géodésique internationale, entre autres de bien
vouloir faire relier entre eux par des nivellements de précision tous les maréographes de
chaque pays, puis de faire rattacher ces lignes, dans le plus bref délai possible, à celles
des pays voisins ; elle leur a de même recommandé, comme étant d’une grande utilité pour
les études intéressant la physique des mers, de faire instiluer, à proximité des maréographes,
des observations météorologiques. Je dois done signaler à la Conférence générale de 1889
les progrès réalisés dans ce sens depuis 1883. A cet effet jai ajouté, dans mon nouveau
questionnaire, deux colonnes destinées 4 recevoir ces renseignements et jai été heureux de

RAPPORT SUR L'ÉTAT DES TRAVAUX. ETC. — 4

 
