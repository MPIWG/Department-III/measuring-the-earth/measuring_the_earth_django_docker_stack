A =

Xi = Portugal:

oo ee ee
0

Ces observations ont été faites par un personnel très restreint.

Dans la détermination des distances zénithales on à employé la méthode des obser-
vations croisées, accompagnées successivement par des lectures barométriques et ther-
mométriques près de l'instrument.

Le tableau ci-joint embrasse uniquement les stations œéodésiques fondamentales, et
le mot incomplète, dans la colonne des remarques, indique celles de ces stations où on

n’a pas encore conclu les observations d’après les modernes procédés. La numération

des stations est faite, autant que possible, selon la derivation des triangles.
Dans la colonne des longitudes on trouve ces valeurs rapportées au meridien de l'Ile
de Fer, déja correctes de la difference dernièrement observée entre Lisbonne et Greenwich.
Sous le titre de Directeurs et observateurs on a seulement fait mention de ceux qui
ont exécuté les observations, et nullement de ceux qui ont été les directeurs généraux
des travaux géodésiques portugais. Pour ceux-ci, on doit mentionner : le feu général
Philippe Folque, jusqu’à 1874; M: le contre-amiral Pereira da Silva, depuis 1874 jus-
qua 1880, ME. le général de division Arbués Moreira, depuis 1880 jusqu'a présent.
Dans les stations déjà complètes selon les modernes procédés on à supprimé Pindi-
cation des anciennes observations, qui ne pourront servir que pour l’histoire de notre

géodésie, eb qui, en outre, ont déjà, paru dans les rélations antérieures.

Lisbonne, 20 Mai 1887.

Brito Lipo
Chef de la °° section de la Direction générale
des travaux géodésiques. ;

 
