 

  
   

hy |

N | r \

| ETALONS | EQUATION DES REGLES

| DE | DE L'APPAREIL, FATTS MERMTANT D’ETREIKDLATES:
1 COMPARAISON. | COEFFICIENTS DE DILATATION.

 

iy)| Etalon de campagne de | Les deux règles sont | Cette base a élé mesurée avec le même appareil que la base
N 5 metres, en acier, |- 4 compensation acier de Yolo en Californie.

 

 

 

 

 

 

 

 

 

il rapporté lui-même au | etzinc,etont 5 mètres Les languettes de contact ont été redivisées; Îles règles ont
| metre en fer du co- | chacune.Lecoeflicient élé couvertes avec de petites planches et enveloppées de
ee nu : a ec rR feutre a chaudiére 9 centimètres d’épaisseur, u
te de 1799. | de dilatation differen- eutre a ee de 2 centimètres d'épaisseur, ie
| tiel n’a pas encore été recouvert de toile. Le poids de chaque régle est de 72 , 5.
alculé. Il a été fait une comparaison des règles de mesures avec
| l’etalon chaque matin. La mesure de la base a été faite à
| | l'ombre : la vitesse maximum d'exécution a atteint 400 règles
| | en 400 minutes, et 2 kilomètres en un jour.
tas | = RB : 1 ON
hf] Etalon en acier de 6™, | Longueur moyenne des | Les 4/5 de la base sur terrain naturel et commode, la dernière
Sy | rapporté lui-même au | regles 4-0 degré cent. ue es tae ote accidenté, avec des pauls
a ty Pago Ain) (RTA -m : variant de 2° 4 5°.—La double mesure, exécutée sur cette
lad metre enfer du comité | — 5 999984. Une com- SR hr Cue MESHES
tt de 1799 fe 7 : partie, a donne les résultats suivants :
Lil en |.  paraison à basse tem- m [ à an
| | on a fas 4" mesure : 1079 032016; température moyenne : 66° Fahr.
tie 1, 4 Derature, sera, faitect aus, „ 10797 033086. Ge pi. Se
ai ? 4 2 “ J.VIE )
i | l'hiver prochain. Y
a | Coefficient de dilatation
| | approché = 0,000011.
au
A
I |
| |
iia)
in
|
x | Toise du Pérou. Delambre estime à 18 lignes l'erreur maxima totale qui a pu
il | | être commise sur la mesure; c’est de là qu'on a déduit
' | erreur relative des deux bases de Melun et de Perpignan.
11
à |
t | »
il]
‘|| |
Bi |
|| |
i} i » | L’erreur relative des 5 bases mesurées par les Ingénieurs
|; #1 | Géographes n’a pas élé calculée par eux; on peut admettre
7 | toutefois qu’elle n’est pas supéricure a lerreur attribuée par
#1 Delambre aux bases de Melun et de Perpignan. (Les bases
ia | françaises n’ont été mesurées qu'une fois.)
v |
| |
|

  

 

 

ANNEXE A. V — 4

i
|

 
