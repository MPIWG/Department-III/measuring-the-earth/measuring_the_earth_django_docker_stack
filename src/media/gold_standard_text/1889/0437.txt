 

7

  
    

\ Le Général Perrier, en communiquant son dernier rapport à Nice, avait annoncé
Ny qu'il présenterait à la prochaine Conférence générale un résumé complet de toutes les bases
un mesurées. Dans ce but, il avait demandé à ses collègues de l'Association, en Janvier 1888,
| de compléter et de rectifier au besoin les indications contenues dans les tableaux qui avaient

Yi été successivement insérés dans les Comptes Rendus.
la |: Reprenant son œuvre, j'ai rassemblé tous les documents qui lui avaient été adressés
Ih k par l'entremise du Bureau central. Ces matériaux m'ont permis d'établir la situation actuelle
Ki. des bases mesurées, et je pense répondre au vœu de l'Association en proposant de l’imprimer

à la suite de ce rapport.

ki :
Ce tableau général n’a pas été produit depuis 1877.

At. Bl
NY t

 

 

Us) |
En rösumant, par Etat, le nombre des bases mesurées, on arrive au chiffre de 129 ;
nl) ces bases sont ainsi réparties : |
|
ley Matiche'Mongrie. 4. cal) |
Nun Baviére 3
Belgique. 2
Danemark. . 2
ak: À Espagne. 9
— Etats-Unis. . 2
ui France et Alcéme. 2 ba wer oo 1
NR | Hesse-Darmstadt 1
san! Hollande. 2
i Italie. 9
| Portugal. 1
mie Prusse 8
Saxe. 1
erat | Suede 6
; Momere §0 . |
Brea à Sue... end
| Wi 2... .22..
| Allen |
cg! | \ Royaume-Uni. 7
ple el ee, Indes. .. „N
si | Cle Gap. 4,02
129

Je serais reconnaissant à mes collègues de l'Association de vouloir bien me signaler
les erreurs ou omissions qui pourraient se trouver dans ces tableaux.

BASSOT.

 

 

 
