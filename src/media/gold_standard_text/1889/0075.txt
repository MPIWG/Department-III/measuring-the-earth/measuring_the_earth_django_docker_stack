 

À
à
ie
È
€
ß
à

 

ram Trot

 

Herren Kollegen, den Gruss Frankreichs den Mitgliedern der internalionalen Krdmessung
darzubringen, welche nach Paris zusammenberufen worden sind, um inmitten des Glanzes
der an die Säcularfeier des Jahres 1789 erinnernden Weltausstellung ihre neunte allgemeine
Conferenz abzuhalten und um die Untersuchungen und die Arbeiten fortzuselzen, welche sich
an ihr schönes und grosses Unternehmen knüpfen.

« Schon im Jahre 1875, meine Herren, haben Sie sich hier vereinigt, und dieses
Haus ist Ihnen nicht fremd. Ueberall, wo die internationale Erdmessung ihre Versammlungen
abgehalten hat, ist ihr ein ehrerbietiger Empfang zu Theil geworden, welchen sie vor allem
dem hervorragenden Verdienste und den Leistungen der berühmten Gelehrten verdankt, die
sie in ihrem Schosse zu besitzen sich rühmen darf. Das seit den ältesten Zeiten wegen sei-
ner. herzlichen und treuen Gastfreundschaft berühmte Frankreich konnte Ihnen gegenüber
seine Traditionen, auf die es stolz ist, nicht aufgeben, und da Sie selbst unsere herrliche
Stadt in diesem denkwürdigen Jahre zum Ort Ihrer weiteren Studien und Ihrer friedli-
chen Diseussionen gewählt haben, so ist es an uns, Ihnen für diese Huldigung zu danken,
welche Sie auf diese Weise dem den Gelehrten stets so theuren Wahlprincip. auf welchem
künftighin die nothwendigen und endgültigen Einrichtungen der französischen Nation
beruhen, freiwillig dargebracht haben.

«Ihre Vereinigung, meine Herren, wächst und entwickelt sich. Wir befinden uns
den Vertretern von zweiundzwanzig Staaten der alten und neuen Welt gegenüber. In der
kurzen Zeit von drei Jahren hat sich der Umfang Ihrer Organisation so erweitert, dass sie die
ürenzen des europäischen Kontinentes überschritten hat, und Sie haben mit besonderer
Genugthuung die Repräsentanten kleiner und grosser überseeischer Nationen aufgenommen.
Die Wichtigkeit Ihrer Arbeiten und das allgemeine Interesse welches dieselben hervorrufen,
ist dadurch klar bewiesen. Nicht nur wünschen die Gelehrten aller Länder daran Theil zu
nehme,:sondern unter denselben sieht man sowohl Givil- als Militärpersonen vereinigt und
sie alle wetteifern, mit Aufbietung aller ihrer Kräfte, um die Grundlagen einer Wissenschaft
zu erweitern, welche so viele Gegenstände umfasst, welche sowohl die Hülfe der höchsten
Speculationen der Astronomie wie die unscheinbaren Operationen des Feldmessers benutzt
und welche sowohl der Physik, der Chemie, der Geographie und der Meteorologie als der
furchtbaren Kriegskunst dient, um unsere allgemeinen Kenntnisse und besonders diejenigen
von unserm Planeten zu erweitern, welcher zwar nur zu oft der Mittelpunkt unserer Leiden
und bejammernswürdigen Streitigkeiten ist, der jedoch auch der glorreiche Schauplatz der
menschlichen Thätigkeit, unserer Arbeiten, Entdeckungen und unserer Triumphe ist, und
auf welchem es fiir die Volker und fiir die Menschen so erwiinscht ware, als Bruder neben-
einander zu leben.

«Sie, meine Herren, wissen es am besten, wie ausgezeichnet Ihr Werk ist und wel-
ches die edlen Gründe sind, welche Sie zu Ihrer Hingabe begeistern. Dasselbe ist zwar,
nach seinen höchsten Zielen beurtheilt, ein rein wissenschaftliches Werk; wer aber gesteht
heute nicht zu, dass nach so vielen Beobachtungen und eben so glücklichen wie mannig-
fachen Anwendungen, die Wahrheiten der Wissenschaft früher oder später selbst der priva-
ten und allgemeinen Industrie, den praktischen Bedürfnissen und dem täglichen Leben zu

 
