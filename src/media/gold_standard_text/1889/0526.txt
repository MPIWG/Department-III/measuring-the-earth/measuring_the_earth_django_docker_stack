 

4

Folge eines allmählichen Rückganges in der Herstellung guter Glassorten zur Zeit kaum
feine Niveaus zu beschaffen sind, welche ganz einwandsfrei funktioniren.

Was die Beobachtungen selbst anlangt, so konnte an der Bedingung, während
des ganzen Jahres dieselben Sterne zu beobachten, aus dem Grunde nicht festgehalten
werden, weil die Instrumente nicht lichtstark genug waren, um die erforderliche Anzahl
von Sternen am Tage sehen zu können. Es wurde deshalb für die Beobachtungen aus-
schliesslich die Zeit von 4 Stunde nach Sonnenuntergang bis 4 Stunde vor Sonnenaufgang
gewählt, dadurch aber ein wiederholter Wechsel der Sternpaare im Laufe des Jahres
unvermeidlich gemacht. Der hierdurch bedingten Verminderung des Genauigkeitsgrades
wurde auf die Weise vorzubeugen gesucht, dass die einzelnen Sterngruppen hinreichend
über einander übergriffen, um die Fehler der Deklinationen der Sterne aus dem Resultat
der Polhöhe eliminiren zu können. Bei Ausnutzung der ganzen Nachtzeit hätte man mit
4 bis 5 Sterngruppen — jede auf 2 bis 3 Stunden der Rektascension ausgedehnt — aus-
reichen können. Da dies aber für die Beobachter grosse Unbequemlichkeit im Gefolge
gehabt hätte, wurde die Beobachtungszeit im Allgemeinen auf die Stunden vor 2 Uhr
Nachts eingeschränkt. Nur in den Herbstmonaten ist eine Ausnahme von dieser Regel
gemacht worden, weil es der wesentlich veränderten meteorologischen Zustände wegen
wünschenswerth erschien, sobald als möglich wieder auf die während des Winters beo-
bachteten Sternpaare überzugehen.

Demsemäss wurden für jede der Stationen 9 Sterngruppen von je 8 bis 9 Stern-
paaren ausgewählt, welche die Sternzeitstunden bezw. Ce
ze 158, 15°—17", 17"—19", 19°—21" und 21" 23" umfassten und an jedem klaren
Abende je zwei dieser Sterngruppen nach dem folgenden Beobachtungsprogramm beobachtet:

Gruppe Sternzeit
1. Janr.—31. Janr. 31 Tage Lund N 2"— 4® und 54? — 73°
ı. Febr.— 3. März ot. „ =. Bt Jo OE
4. März — 3. April 21 Le. LV ge ET 5, Il. 03
4. April — ı. Mai 28, TVG Ti — 18 D 14, 5
2. Mai —27. Mai 20: „ Ve, VE 13-715. oy 2S ee
28. Mai —.2o. Juni 24; Vi, VIT DS — 1) 2 wild. eho
21. Juni —30. Juli 1e VI „WEI LT 10; 5 JO nai
31. Juli — 8. Sept. 10 „ Vill, EX 19. — 21. 5, 25, =e
9. Sept. —27. Nov. 89.4 IX. al 2123 a
28. Nov. —31. Janr. 65.2, oo 2 Asp Samak

Eine graphische Darstellung dieses Beobachtungsprogrammes, aus der unmittelbar
zu ersehen ist, wie sich die Beobachtungen auf die verschiedenen Stunden mittlerer Zeit
vertheilen und wie viel die einzelnen Sterngruppen über einander übergreifen, ist auf
der diesem Bericht beigegebenen Tafel enthalten.

 

 

 
