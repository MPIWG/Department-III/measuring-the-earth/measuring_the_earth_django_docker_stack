 

géodésique autrichienne et par l'Institut
géographique-militaire de Vienne (voir
Annexes B. XXIII+ & XXI)

Rapport de M. Hennequin sur les travaux
de la Belgique (voir Annexe B. XIV)
Rapport de M. Zachariae sur les travaux
de Danemark (voir Annexe B. XY) .
Rapport de M. le marquis de Mulhacen
sur les travaux de l’Institut géographique
et statistique d'Espagne (voir Annexe B.

XVI)

Rapports des ns naeh MM. Dawe:
cagaix, Bouquet de la Grye et Lalle-
mand, sur les travaux exécutés en
France (voir Annexes B. XVII, XVI,
XVIIIe) . ES

Note de M. Defforges sur l'influence du
milieu ambiant sur l’oscillation du pen-
dule (voir Annexe A. VIII)

Note de M. Bouquet de la Grye sur ie
choix d’un plan fondamental pour les
altitudes (voir Annexe B. X VIII¢)

Mémoire de M. Tisserand sur |’aplatisse-
ment de la Terre (voir Annexe A. X.)

Quatrième séance, 11 octobre 1889.

Lecture et adoption des procès-verbaux
des deuxième et troisième séances . .

Observations de M. @’Abbadie sur les ni-
veaux

Remarque de M. Lallemand sur la meade
bution des erreurs dans le nivellement
général de la France .

Rapport de M. v. Sterneck sur ses ere
minalions de la pesanteur en 4889 (voir
Annexe B. XXIIIc) ;

Rapport de M. Carusso sur les Rava +
la Grèce (voir Annexe B. XIX) .

Rapport de M. Ned sur les nivellements
exéculés dans la Hesse ee Annexe B.
XX).

Rapport de M. eig ero sur es ‘aging de
la Commission italienne (voir Annexe B.
XXI

Rapports de MM. ae et chats
sur les travaux des Pays-Bas (voir An-
nexe B. XXIVa et XXIV)

Rapport de M. Morsbach sur les travaux

 

3

 

Pag. Pag.
de la « Landesaufnahme» de la Prusse
(voir Annexe B. XVID) 27>. 42
35 Rapport de M. Helmert sur les dr de
l’Institut géodésique de Prusse (voir An-
35 nexe B. X 2 42
Rapport de M. Stebnitzki sur iG ara
36 exécutés en Russie dans les années 1886-
1888, communiqué par M. Hirsch (voir
Annexe D XXVI °° 43
Rapport de M. Rosén sur les a Le
36 cutés en Suède, de 1887-1889 (voir An-
nexo;B. XVI) . aya). Gila? 43
Rapport de M. Hirsch sur les Bee exé-
culés en Suisse (voir Annexe B. XXIX) 43, 44
Rapport de M, Davidson sur les travaux
36-39 du «Coast and Geodetic Survey » des
Etats-Unis (voir Annexe B. XVI. . AA
Discussion entre MM, Ferrero et Davidson
37 au, sujet de Ce rapporm = 4h
M. Andonowits annonce le commencement
des travaux en Serbie pour l’année sui-
38 yanle vn ern er ee 45
Rapport de M. Térao sur l'organisation
38 des travaux géodésiques au Japon (voir
Annexe BY X XID 45
Réponse de M. Foerster aux obere
de M. d’Abbadie au sujet du niveau. . 46
40 Premiere discussion sur le Reglement de la
Commission permanente . ... . . 46
AO Communication de M. Faye sur un nouveau
procédé de M. Cheysson pour la repro-
duction d’anciens documents et imprimés 46, 47
40, 44 Cinquième séance, 12 octobre 1889.
Suite de la discussion sur le Règlement . 48, 49
AA Le Réglement pour la Commission perma-
nente estadopté avec deux modifications. 50
AA Le Reglement pour le Bureau central est
adopie .. 3... a 50
La nomination de M. a, ee comme
4A membre de la Commission a
est ratifiée par la Conférence. . . 50
MM. Bakhuyzen, Ferrero, Fœrster et
41, 49 Mulhacén sont réélus membres de la
Commission permanente . . . 50, 54
MM. Davidson et Hennequin ayant on
42 égalité de voix pour le cinquième siège

seront tous les deux considérés comme

han RE Scag

Fi mc ee sn Ge

était anis ni

   

chat tn oh géné

 

 
