 

 

Annexe A I.

RAPPORT

sur l’état actuet des travaux de nivellement de précision exécutés dans les
differents pays de l'Association

présenté à la Conférence générale de Paris, le 2 Octobre 1889

PAR

A. von KALMAR.

Messieurs,

Chargé par notre très honoré Président de présenter, à cette Conférence, le rapport
sur les travaux de. nivellement en Europe, j'ai cru pouvoir suivre le plan adopté par notre
collègue, M. le D' Hirsch, et je prends comme base de mon travail le dernier rapport que
celui-ci à lu à Rome en 1883. :

Pour vous donner un aperçu général des progrés des nivellements, il me sulfira donc
de vous exposer, sous forme de tableau, la situation actuelle de l’entreprise et d’en tirer les
conséquences.

Afin de pouvoir établir ces tableaux, j'ai envoyé au commencement d'août à nos
collègues des divers Etats de l'Association la circulaire suivante, ainsi qu'un questionnaire
pareil à celui de mon prédécesseur, et une carte de nivellement dans laquelle J'ai prié de

faire figurer les nouvelles données.

ASSOCIATION GÉODÉSIQUE
INTERNATIONALE Vienne, le 4er août 1889.

CIRCULAIRE

Monsieur et très honoré Collègue,

Désigné par notre Président comme rapporteur sur les nivellements de précision
pour la prochaine Conférence générale, je prends la liberté de vous envoyer le questionnaire
que notre Secrétaire perpétuel vous a adressé il y a six ans, en vous priant de bien vouloir
y répondre, si possible, avant le 1er septembre.

Il s’agit naturellement de compléter les données pour les six années écoulées, d’in-

RAPPORT SUR L'ÉTAT ACTUEL DES TRAVAUX. ETC. — |

 
