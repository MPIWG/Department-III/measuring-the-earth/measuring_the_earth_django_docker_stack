 

6

 

moyenne et sur un pic élevé. La réduction au vide, dans ces différents cas, ne peut pas se
calculer avec le même coefficient numérique. C’est une difficulté sérieuse, qu’il faut signaler,
dans l'emploi du pendule invariable.

Dans nos expériences, les séries à la pression 760mm ont été faites en vase clos. Les
nombres obtenus diffèrent à peine des valeurs obtenues dans l'air pour la durée de l'oscilla-
tion. Notamment la différence T'—T, des durées observées dans les deux positions, poids lourd
en bas et poids lourd en haut, qui est, pour ainsi dire, caractéristique à la fois du pendule
et de la pression du milieu ambiant, reste rigoureusement la même. On en doit conclure que
l'effet des parois, à la distance (19 centimètres environ) de l'expérience et pour d'aussi fai-
bles vitesses (moins de un degré d'amplitude) est négligeable. Les analyses pourraient donc,
dans une première approximation, négliger l'effet de l'enveloppe sur la durée de l'oscillation
d’un pendule à vase clos.

Les expériences de Breteuil ont été refaites sur un plan plus large et complétées,
en ce qui concerne l'air, à Paris et à Dunkerque, cette année même. Nous nous proposons,
pour vider la question, d'étendre ces recherches à différents gaz, notamment à l'acide carbo-
nique et à l'hydrogène, afin d'étudier comment le coefficient p. varie avec la densité des gaz.
serait intéressant en outre d’étudier expérimentalement la variation du coefficient p. avec la
température. Singulière propriété du pendule qui, après avoir emprunté à la physique, pour
la mesure de g, les résultats de ses plus délicates recherches, lui rend, de son propre fonds,
autant, sinon plus qu’il ne lui a emprunte.

Pendule court de Brunner.

 
 
 

EN IEEE EE

 

 
 

    

 

 

 

 

 

| Observation. Calcul. A (o-c)
| us bd = a Ss : Sos Ss
| T + 735 + BZ, — 0.7115004 | 0.7115016 | — 0.0000014
297 #0 4111026 23
bon » Sl 19,4 0. en 4236 00
> 185 1356: Size :0.7113812 3806 | + 06
| Snpepeanyy 9,6 = 0,7113598 3589 | + 11
eet 8 Peeves 077118862 3369 | — 05
|
/ ia al voz b OY C N 90% 1 = | 5
| I + 7: iv ot ae 0.7113372 | 0.7118357 | + 0.0000025
DC o% Br DER ¢ 2 i
Poids lourd en haut . rt Di ME re 2169 | + :
| 207 4 = 1582 1590 | — 8
108 104) = 1219 1241 | — 2
8 28 0858 0818 | + 04

Dans chaque groupe on a éliminé d’abord, par une moyenne, et en soustrayant

 

messiah seca elisa

{
i

 
