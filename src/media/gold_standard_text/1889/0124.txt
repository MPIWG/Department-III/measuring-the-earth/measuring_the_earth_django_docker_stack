  

Es bleiben für das laufende Jahr noch zu zahlen die Bei-
träge von Griechenland, Norwegen, Oesterreich-Ungarn, Serbien,
Hamburg, Mexiko, Japan, und von den beiden soeben erst beigetre-
tenen Staaten, nämlich von der Argentinischen Republik und den
Vereinigten Staaten von Nordamerika.
Zusammenfassend finden wir als die Summe der Beiträge,
welche nach der in Art. 9 der Uebereinkunft aufgestellten Skala
für die seit der vorangegangenen Generalconferenz verflossene
dreijährige Periode zu zahlen waren, den Betrag von . . . . 48320 M. = fr. 60400
an Stelle des Betrages von. . . HONG IO EAN = 100000
welcher die obere Grenze des in Ant. 7 der Uebereinkunft festge-
setzten Budgets für einen dreijährigen Zeitraum darstellt.

Für die seit der letzten Generalconferenz abgelaufenen drei Jahre ist hiernach der
Unterschied zwischen dem Gesammtbetrage der für die Permanente Commission durch die
Uebereinkunft festgesetzten Einnahmen und dem Gesammtbetrage der von den Staaten gezahl-
ten Beiträge so äusserst gering, dass es zulässig erscheint, diese letzteren Beiträge lediglich
als den Bedingungen der Uebereinkunft entsprechend anzunehmen, mit dem einzigen Vor-
behalte, dass man den kleinen Ueberschuss von 320 M.= 400 Fr. über den dreijährigen Be-
trag der vertragsmässigen Dotation bis zur nächstfolgenden Gonferenz überträgt.

Andererseits wird die Summe der seit dem Beginn des Jahres 1887 bis jetzt gemach-
ten Ausgaben, einschliesslich der in dem laufenden Kalenderjahre noch zu erwartenden Aus-
vaben 32000 M. = 40000 Fr. nicht übersteigen, so dass zum Schlusse dieses Jahres ein ver-
fügbärer Bestand von nahezu 16000 M. = 20000 Fr. vorhanden sein wird, welcher nach
der in Art. 7 der Uebereinkunft ertheilten Ermächtigung reservirt werden darf, um in geeig-
neten Zeitpunkten wissenschaftliche Untersuchungen zu unterstützen, welche von allgemeiner
Wichtigkeit für die internationale Erdmessung sind. In diesen verfügbaren Bestand sind aber
die Rückstände der Beiträge mit einbegriffen, welche im gegenwärtigen Zeitpunkte 9440 M.
= 11800 Fr. betragen.

Was die Beiträge für die nächstfolgenden Jahre betrifft, so würden die sechs-
undzwanzig Staaten, welche bis jetzt der internationalen Erdmessung beigetreten sind, nach
der in Art. 9 aufgestellten Skala einen Gesammtbetrag von. . . 55080 M. = fr. 68850
zu entrichten haben.

Wenn man den vorerwähnten Veberschuss . . . 320. M. fr. 400
hinzufügt, welcher aus den Beiträgen der vorangegangenen a
Jahre sich ergeben hat, findet man, dass ohne Herabsetzung der
Beitrags-Skala die der Ben Commission nach der Ueber-
einkunft gewährte Dotation bis zur nächsten Generalconferenz
überschritten sen würde um den.Betrae yon 0... .1..:.n 1400 Mase fr. 9250
d. h. um etwa 13,3 °/, der Summe der Beiträge.

Demgemäss schlagen wir der Conferenz vor, dass vom Beginne des Jahres 1890 ab

 

i
f

m ER

ee

 

|
i
