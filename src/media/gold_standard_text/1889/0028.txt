  
  
    
     
  
    
 
  
      
   
   
   
    
    
   
   
   
   
    
        
       
   
   
   
   
   
   
   
   
    
    
   

 

22

 

Ei
&

ation de la Commission permanente a été administré comme prece-

Le Fonds de dot | Ei
demment. Jaurai ’honneur de remettre à M. le Président l’état des comptes pour Van- | |
née 1888 et un aperçu de la situation actuelle. u |

HELMERT.

Ce Rapport est accompagné de la liste suivante des expéditions faites pendant lexei

cice 1888/89.

Résumé des expeditions de publications de l'Association géodésique faites par le
Bureau central.

  

Depuis la Conférence de Salzbourg, en 1888, jusqu’à la fin de seplembre 1889, le
Bureau central a recu de MM. les délégués de l'Association eéodésique internationale, pour
être distribuées, les publications suivantes :

1. De M. le général Schreiber, à Berlin : Abrisse, Koordinaten und Hôhen sämmt-
licher von der Landes-Aufnahme bestimmten Punkte. 8ter Theil. 79 exemplaires.
9. Du même : Nivellement der Trigonometrischen Abtheilung der Landesaufnahme.

er

Tier Band, 58 exemplaires.

3. De la Commission géodésique suisse : Le Röseau de Triangulation suisse. Vol. II.
Mensuration des bases suisses par MM. le Dr Hirsch et le colonel Dumur.

4. De la même : Das Schweizerische Dreiecksnetz. Band IV. 95 exemplaires.

5. De M. le colonel de Stefanis, à Florence : Relazione sulle esperienze istitute nel
R. Osservatorio Astronomico di Padova in Agosto 1885 e Febbraio 1886. 100 exemplaires.

6. Du même : Triangolazione di 1° ordine dell’ isola Sardegna. Vol. 1. 100 exem- |
plaires.

7. De VInstitut militaire-géographique, 4 Vienne : Mittheilungen. VUE Band.

O

80 exemplaires. :

8. Die M. le conseiller intime, professeur Nagel à Dresde : Das Trigonometrische Netz
| Ordnung, I. Heft. 57 exemplaires.

9. De M le général marquis de Mulhacén : Memorias del Instituto Geographico y
Estadistico. Tomo VIE. 67 exemplaires.

10. La Commission permanente a publié les ouvrages suivants :

a) Bibliographie géodésique 875 exemplaires dont il à été distribué en mai et juin :

72 exemplaires aux Gouvernements des Etats faisant partie de l'Association inter-
nalionale,

309 exemplaires aux délégués, autorités, instituts, savants, sociétés, etc., suivant la
liste de distribution établie par la Commission,

300 exemplaires ont été déposés en commission à la librairie G. Reimer, à Berlin et
154 exemplaires sont restés en dépôt au Bureau central.
