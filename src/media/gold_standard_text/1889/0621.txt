 

}
{
6
t
|
|

 

 

 

 

HR

RSR e

 

 

 

Annexe B. XXII.

COMMUNICATION

sur les travaux géodésiques au Japon.

Messieurs,

Ce m'est pas un rapport que je vais lire. Nos travaux géodésiques ne sont pas encore
assez avancés et ils n'ont pas encore un caractère assez scientifique pour que nous puissions
dès à présent vous présenter un rapport qui vous soit vraiment utile. Du reste, je sais mieux
que personne que le but de mon Gouvernement, quand il s’est décidé & m’envoyer ici comme
Délécué, a été moins d'apporter quelques contributions à vos travaux communs, que de pro-
filer de vos expériences et de vos lamiéres pour faire progresser nos propres travaux; et
j'ose dire que votre bienveillance envers notre pays, dont nons connaissons tant d'exemples,
justifiait cette espérance. Cependant, même pour pouvoir profiter de vos renseignements, je
dois commencer par vous mettre en quelques mots au courant de ce que nous avons fait et
de ce que nous espérons faire.

Depuis longtemps déjà, la nécessité d’avoir des cartes exactes du pays nous avait
conduits à faire quelques essais de travaux géodésiques. Mais c’est surtout depuis six ou sept
ans que notre Etat-Major à entrepris sérieusement le levé général du pays, fondé sur des
triangulations. Sous la direction du Commandant Tasaka, qui a fait ses études en Allemagne,
on a poussé très activement les travaux de triangulalion ; et déjà aujourd’hui, les deux plus
orandes villes du Japon, Tokyo et Osaka, se trouvent reliées par un réseau de triangles du
premier ordre, mesurés avec des instruments exactement pareils à ceux employés en Prusse.
En même temps, des nivellements de précision ont été exécutés entre les mêmes villes et
au delà, également au moyen d'appareils allemands. Dans ces deux ordres d'opérations, les
triangulations et les nivellements, le Commandant Tasaka nous assure qu’on a obtenu des
résultats présentant toute l’exactitude que comportent ces excellents instruments de mesure.
Mais en ce qui concerne la mesure des bases et la détermination du niveau moyen de la mer,
on constate avec regret qu’elles sont loin de comporter le même degré d’exaclitude que nos
mesures d’angles et nos déterminations de différences d'altitude. Ensuite de circonstances
administratives qu’il serait trop long d'indiquer ici, on a persisté à employer, pour la mesure

 

 

 

 

 

 

 

 

 

 

 
