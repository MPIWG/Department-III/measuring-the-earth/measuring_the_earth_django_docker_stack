 

|

 

 

En

Hoffnung, Dr. Gill, hat die unter seiner Leitung von Major Morris ausgeführte Vermessung
im bone n Theile von Südafrika gute Fortschritte gemacht. Von Port Elizabeth im Süden
geht eine 550 englische Miles lange Dreieckskette bis zur Nordspitze von Natal. Sie stellt
eine Breitengradmessung von 8° dar. Die geographischen Positionen wurden mit Clarke’s
Dimensionen des Erdellipsoids von 1880 ausgehend von Zwart Kop in Natal berechnet und
passen im allgemeinen gut mit den astronomischen. Grössere Differenzen treten auf in
Berlin, 300 miles südöstlich von Zwartkop und 130 ostnordöstlich von Port Elizabeth. Ferner
in Port Elizabeth und den beiden, 13 und 50 miles nördlich davon gelegenen Punkten (oega
Kop und Zuurberg :

Berm | oe. 0,86
Autirbere; . +... 7... 001
Goran... Senn
Port Elizabeth, . - 8,38.

Hier hat man es mit lokalen Wirkungen zu (hun, die weiter verfolgt werden sollen.

Preston erwähnt in seinem Bericht über die Pendelmessungen auf den Sandwichinseln
auch einer neuerdings bewirkten Triangulation, welche in Verbindung mit astronomischen
Messungen zur Kenntniss bedeutender ‘Lothabweichungen auf diesen Inseln geführt hat.
14 Punkte sind auf den vier grössten Inseln so angeordnet, dass die Abweichungen nördlich
und südlich der hohen Berge bekannt wurden. Sie gehen bis zu + '/, Minute. Umfassenderen
Mittheilungen muss mit grossem Interesse entgegengesehen werden, denn sie versprechen
Aufschlüsse über die Dichtigkeit der Vulkane in verschiedenen Entwickelungsstadien. Die
erloschenen Vulkane, wie der Haleakala äusserten auf Pendel und Loth eine ihrer äusseren
Form entsprechende Anziehung, während die noch thätigen einen Massendefect zu besitzen
scheinen !.

HELMERT.

! Verbesserungen zum Bericht über Lothabweichungen von F. R. Helmert in den Verhandlungen
von Nizza :
Seite 9, Tabelle Ib, in der Spalte Beobachtete geogr. Breite, ist bei Tarqui vor 3° 4 32.07 das Zeichen
— hinzuzufügen.
» 43, Tabelle Ha, in der Spalte Radius in Km., ist bei 47, Burleigh Moor, 77 statt: 17 zu lesen.
» 34, Zeile 14, 148 und 19 lies: § 4 statt: § 3.
» 3h, Tabelle IV, in der Spalte Beobachtete Linge etc., Zeile 55, Genua, lies: 8 88 20.385
statt: 8° 55° 20 538.
>» 36: Zene 2, inter Lothabweichungsanomalie lies: in Tiflis statt: Daselbst.
» 44, Tabelle VIa, in der Spalte Landstrich lies: New-Hampshire statt: New-Hamphire.

 

|
i
|
‘
4
}

ne

 
