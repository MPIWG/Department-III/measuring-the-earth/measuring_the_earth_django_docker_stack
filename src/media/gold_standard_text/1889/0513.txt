5

 

Il est à remarquer que de ces mesures obtenues à Maui, aussi bien que de celles
exécutées dans les Alpes-Maritimes, près de Nice, il résulte une variation de la pesanteur
LL avec la hauteur qui ne correspond point a la variation dans lair libre, mais qui est modifiée
rat encore par toute lattraction des masses de montagnes. À Maui, on ne peut pas non plus
reconnaître dans les valeurs absolues de la pesanteur une compensation générale de l'effet des
montagnes par celui de vides souterrains (stations d’ile). Pour les Alpes-Maritimes, la valeur
absolue de la pesanteur, indispensable pour apprécier une pareille compensation, n’est pas
encore connue. Quant aux Alpes du Tyrol, voir le Rapport de M. von Sterneck.

 

 

   
  
    
 
  
  
   
   
  
  
  
  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

HELMERT.
- a du an = Longueur du pendule
| à seconde, = _ a seconde, Année
= reduile au niveau de la mer ae
Le au niveau de la mer. = © et a la latitude de 48°. de
| D'après la | D’après la = = D’apres la D’apres la ee l’observa- nenn
formule formule De formule formule
SH 5H Ss 2H 5H tion.
R ER = R ER
9 10 44 12 13 1% 15 16
i Wi | 0,993 554 0.333 551 | — 37 | 0,99 517 | 0,995 514 Lorenzoni 1885/86
Ba 1 0,993 174 | 0,993 096 | + 419 593 alo Barraquer 1887 res HS géologi- |
a les, la densite du sous-sol est/i
KEN 4 0,993 170 | 0,993 092 | + 420 590 512 » ikea a! ae 2 |
= 0,995 699 | — 2149 a 550 Wilkitzky 1887
= 0.995 201 | —1667 = 33% » 1887
“i 0,994 596 | 0,994 579 | — 965 | 0,993 631 614 Bredichin 1880/81
009% 339 | 0,994 312 | — 796 536 516 Sternberg 1888
0,994 204 | 0,994 186 | — 60% 600 582 » 1888
[0,992 993 ]| [0,992 992]| + 557 |[0,993 550] | [0,993 549] | Preston et Baylor | 1887
5 0,992 881 | 0,992 868 | + 655 4. 536 523 Preston et Hill 1887
0,992 940 | 0,992 789 | + 696 636 485 | Preston et Keeler | 1887
|; | 4 2 Q 9B ; ar} m reo. \ à j m (
5 | 0,991 836 | 0,991 836 | 11937 773 773 | Preston, Dodgl 1887 1 nal sur In) ast si
i | 0 7 1 1 7° = 195€ rap 75 N hl a ca0a0 est orme 6 asaltes, ont
i | ‚391 811 | 0,991 797 | 11959 770 756 | et 1887 — 16 — 97 et en moyenne
nl 0,992 074 | 0,991 721 | 41973 | 0,994 017 694 Wall 1887 am |
j = 0,991 7 725 | 0,991 725 | +1963 | 0,993 688 688 | 1883
m : = \ Preston et Brown Lahaina est la station de Freycinet.
| ‚0,991 230 | 0,991 229 | 41-2472 702 701 1883
EM On n'a pas tenu compte, dans les
reductions au niveau de la mer, de
valeurs particulières de ©.

 

 

|
|

 
