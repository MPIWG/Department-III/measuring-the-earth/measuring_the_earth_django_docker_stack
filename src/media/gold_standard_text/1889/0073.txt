 

ar

O/

Herr Professor Helmert, Director des königl. Preussischen geodätischen Institutes in Berlin,
Director des Gentralbürcau’s der Erdmessung, Mitglied der Permanenten Commission.

Herr Generalmajor Schreeber, Vorsteher der Landesaufnahme, in Berlin.

Herr Oberstlieutenant Morsbach, Ghef der Triangulation bei der Landesaufnahme.

Herr Professor Albrecht, Sektionschef am königl.-geodätischen Institut in Berlin.

RUM/ENIEN.

Ilerr General Fulcoiano, Generaladjudant S. M. des Königs von Rumänien, Ghef des grossen
Generalstabs, in Bukarest.
SERBIEN.

Ilerr Professor Andonowiis, Divector des Serbischen geodätischen Institutes.
SCHWEIZ.
Herr Professor Hirsch, Director der Sternwarte in Neuenburg, ständiger Sekretär der inter-
nationalen Erdmessung. |
B. AMERIKA.
VEREINIGTE STAATEN.

Herr Professor G@. Davidson, Assistent der «U. S. Coast and Geodetic Survey ».

MEXIKO.
Herr Professor Mendizabal Tamborrel, Geograph-Ingenieur, Director der Sternwarte von
Thapultepee.
llerr Angel Anguyano, Givilingenieur und Director der Sternwarte zu Tacubaya.

C. ASIEN.

JAPAN.

llerr Terao, Professor an der kaiserlichen Universität und Director der Sternwarte zu
Tokyo.
Ausserdem wohnten der Sitzung bei :
Die von der Permanenten Commission eingeladenen Herren :

Graf Albert Amadei, Rath beim Ministerium des Aeusseren zu Wien.
Herr Backlund, Mitglied der Akademie von St-Petersburg.

Herr R. Bischoffsheim, von Paris.

Herr Dr. B. A. Gould, von Cambridge (Vereinigte Staaten).

 
