de

XII. — Prusse, À:
Re D me a ee
Bureau der Europäischen Gradmessung, bezw. dem Königl. Preussischen Geodätischen
Institut übertrug. In den Jahren 1867-77 wurden sodann die Winkelbeobachtungen
für die ganze Dreieckskette unter Leitung der Herren Prof. Bremiker und Prof. Fischer
vollendet.

Beobachtungs-und Rechnungs-Methoden stimmen mit den in der Gradmessung von
Ost-Preussen und in der Küstenvermessung benutzten überein. Die Resultate sind veröf-
fentlicht unter dem Titel: Das Rheinische Dreiecksnetz. I. Heft. Die Bonner Basis.
Berlin 1876. —U. Heft. Die Richtungsbeobachtungen. Berlin. 1878. -- Ill. Heft. Die Netz-
Ausgleichung. Berlin 1882.

Hessisches Dreiecksnetz.

Als die Winkelmessungen für -das Rheinische Dreiecksnetz im Jahre 1869 bis zu
den Anschlusspunkten an die Kurhessischen Dreiecke Gerling’s fortgeschritten waren,
zeigte es sich, dass deren Identität mit den alten Punkten sehr zweifelhaft war. Da
überdies noch einige andere Punkte der Gerling’schen Dreiecke als verloren betrachtet
werden mussten, wurde beschlossen, ein ganz neues Dreiecksnetz durch die Provinz
Hessen-Nassau bis zur Seite Inselsberg-Brocken zu legen, wärend von hier bis zum
Anschluss an die Dreiecke der Küstenvermessung und an die Berliner Grundlinie schon
seit mehreren Jahren Winkelmessungen für das sogenannte Märkisch-Thüringische Drei-
ecksnetz im Gange waren. Die Winkelbeobachtungen wurden in den Jahren 1873-76 mit
dem zehnzölligen Universal-Instrument N. 1 von Pistor und Martins und unter Leitung
des Herrn Prof. Sadebeck ausgeführt. Auf den Anschluss-Stationen Brocken und Insels-
berg wurden die älteren, für das Märkisch-Thüringische Dreiecksnetz in den Jahren 1865,
1869, 1871 zum Theil mit einem 13-zölligen Universal-Instrument von Pistor und Mar-
tins (Siehe: Prusse, B. Instrument N. 3) angestellten Beobachtungen mit aufgenommen.

Beobachtungs-und Rechnungs-Methoden sind die nämlichen wie für das Rheinische
Dreiecksnetz. Die Resultate sind publicirt in dem Werke: Das Hessische Dreiecks-
netz. Berlin 1882.

Anschluss der Sternwarte in Breslau und des Punktes Rosenthal

an das Schlesische Dreiecksnetz.

Die Sternwarte in Breslau war zu einem Hauptpunkt der Längengradmessung unter
dem 52. Parallel ausersehen. Zu dem Ende wurden 1862 auf der Sternwarte von General
Baeyer Polhöhe und Azimut bestimmt; zur Bestimmung der geographischen Länge
eignete sich jedoch die Sternwarte in Folge ihrer ungünstigen Lage nicht. Als Län-

 
