wen

Iv. — Danemark.
a ne a al ee DIE
a détermité la difference de longitude entre Copenhague et Altona au moyen du télé-
oraphe électrique, ce qui à rendu superflue la partie du parellèle située à l ouest de
la méridienne, et en même temps on à renoncé à la base de Skagen où le terrain
n’offrait pas des conditions assez favorables. Aux opérations qui, de cette maniere, ne
comprenaient que la prolongation de la chaine méridienne jusqu'à Skagen, on a ajouté
des observations d’ azimut en trois stations, une de la Seeland, une de la Fionie et une

du Jutland. Les observations ont été finies en 1870.
Le rapport complet des observations et des résultats qu’on en à tirés, se trouve
dans l'ouvrage < Den danske Gradmaaling > vol. I-IV, publiés par M. Andræ en 1867,

1872, 1878 et 1884.

Par ordre du directeur
AXEL OLUFSEN

capitaine.

Erreurs moyennes.

Par rapport aux observations des angles pour les triangulations de la mesure des
degrés en Danemark il faut faire distinction entre trois époques bien marquées, savoir
celle de 1817 à 1824, celle de 1837 à 1847 et celle de 1867 à 1870.

1. Pendant l’époque de 1817 à 1824 on a mesuré dans les duchés de Holstein
et de Slesvig les triangles qui lient la base de Braack près de Hambourg au cote
Lysabbel-Fakkebjerg. La méthode employée pour la mesure des angles des vingt trian-
gles de cette époque est celle dite de répétition, et de ces mesures on déduit pour
l’erreur moyenne d’un angle

u
my to ‚788.

2. Pendant la deuxiéme époque, celle de 1837 & 1847, on a mesuré les quatre
séries de triangles qui, entourant les iles danoises, joignent d’une part la base de Co-
penhague au côté Lysabbel-Fakkebjerg et de l’autre la même base aux triangles sue-
dois et prussiens de la côte baltique. Pour ces observations on a employé le théodolite

d’Ertel de 15 pouces et la méthode dite de rétération. Par les 51 triangles possibles
de ces quatre séries on obtint

ma A 152187 gg
a wer

 
