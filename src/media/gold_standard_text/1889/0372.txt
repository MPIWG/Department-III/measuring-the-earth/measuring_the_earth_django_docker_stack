1e

XIII. — Prusse, B.

a ee 4
| Te er ae

No

| Strauch

3 | Keulenberg

| Schneekoppe

 

| Schneeberg

| Springberg

 

INDICATION
DES POINTS.

Krugberg

Colberg

Glienicke. .....-...
Eichberg
Ziegelberg

Boosen

Gulmberg
Brautberg

102 21
52.0

| 51 41

 

Grossberg

Brandberg
Hochstein

| Riickenberg
| Meiseberg

| Dalkau

| Gréditzberg

Todtenberg
Tschelentnig

Gay et u.

| 50
50 12

Krostkowo
Dembogora
Olesnitz

Sienno
Prieske Berg
Dobrojewe

Tarnowo
Chmilinko

Moszin

Schroda

Guzdzin

Kobelnik
Josephsberg
Schwarze Berg ...
Trebehen..........
Kröben |
Meiseberg

Dalkau ........x
Todtenberg
Tschelentnig

Schneeberg
nn B
Hermsdorf.

 

| 51
| 5]
| 51
| 51
| 51
| 51
| 51
| 51

Si
| DL
| 51
| 50

LATITUDE. a)

|
52 14 24 |
52 16 12
52 18 56
52 15 11
3
DI
7

52 6

52 1
57
40
23
18
13
35
steal
36
5D
39
10
31

17

45

0
17
Al
49

Dl

| 32
Blk
3 |

9 |
10 |

43 |
48 |

X0) |
#39

| 33
| 34

 

17
44
55
51

39
50 26
55
ol

Sa i

Ba
53
52
52
52

50°55'.26”
50,51 95
50 12 31
50 46 33
50 45,55

 

| 34
14 |
| 34
| 34 22 35
| 34 30 59

Mirkisch-Schlesische Drei
47

31° 45"
31.298
ol 25)
30 46 44
31 49 39
82 5.28

al
31
31
ol
30
ol
32
32
32
33

33 0
0 59

40 44
37
138
30
47
u
32
25
17
9226
24 31
54 29

54
42

33

12 19 |
39 13 |
0 41
14 39 ||
28 ||
|
53 |

41 |
0 |

| ; DIRECTEURS
ÉPOQUE. INSTRUMENTS.

| RE ;
ET OBSERVATEURS. | MARQUES. 2
|

|

eckskette.

| Publikation N. 6, À

 

von Morozowicz.
Ne. uud. >:

 

 

|

Schlesisch-Posensche Dreieckskette.

34°17'
34 54 31
35. 922
Aal 0
4.12 11
34 5] 59
3108 >
34 4 40
34 40 42
SOS
oo Ol
34 30
34 56
33 56
34 14
33 52
34 47
34 18
34 39
op
33 92
34 17
34 52

34054
34 29
34 30
34 51

29"
35
59
38

177 |

19 |

|| Publikation N. 6. {

|

von Morozowicz.

 

Schlesische Dreieckskette.
| Publikation N: i

|
|

1854
und 1878.
d

Baeyer
un

 

34 53 46 |

 

Schreiber.

 
