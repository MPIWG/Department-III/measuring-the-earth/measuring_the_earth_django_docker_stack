 

61

are

IT. Compte des contributions capitalisées pour 1888.

EEE Le PEINE PP AE OY

RECHTTES. | Marks. | Pf.) Marks.

Pf. Francs. | C. Francs.

a. os su

Solde au 31/12 1887 des contributions capi- | os
pee ES Ge 4 pte 80 | 18474 | 75 |

Rentrée d’une contribution capitalisée . . | 2040 | |. 2550 | —

Eon dlunlssötssoitssiene' oa swrlssnd dû |

21706

 

DEPENSES.

 

Déduction de la contribution annuelle du | |
Cukpom= 1881 ..-. 240 | —

 

 

 

 

 

 

 

 

 

 

 

Déduction des contributions annuelles des 6 a | | |
BE Be 2... 2 — ı 2100 —
Contribution de la Norvège pour 1888, payée | | | Poel
Pawnee misst. U EL. EL (+ 210 | 800 —
Frais d'achats de 12000 Marks de consolidés | | | | |
Ban. ur 2: er | 368 90! 3008 ‚90, 461 112) 3761 | 12
Solde disponible au 31/12 1888 eee D Oe
| | 17364 90) | ore

 

Le rapport ayant constaté que la comptabilité du Bureau central est tenue dans un
ordre parfait et que toutes les dépenses sont justifiées par des pièces à l’appui, la Commis-
sion permanente donne décharge pleine et entière à M. le professeur Helmert, Directeur du
Bureau central, pour l'exercice de 1888.

| Le rapport propose en outre d’allouer au Bureau central un nouveau crédit de 2000
I francs, pour la continuation des observations sur la variabilité de l'axe terrestre.

 

Cette proposition est également adoptée à l'unanimilé.

M. Helmert donne connaissance du programme suivant pour les travaux scientifiques
du Bureau central pendant l’année 1890 :

Jo Continuation des recherches préparatoires sur le mouvement de lPaxe terrestre.
9° Continuation des. calculs de la partie géodésique de la mesure des longitudes sui-
vant le 52me parallèle.

3 Continuation des calculs concernant les déviations de la verticale.

 
