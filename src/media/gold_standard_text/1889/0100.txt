 

a

Herr Hirsch hebt ebenfalls den günstigen Zeitpunkt und die Nothwendigkeit hervor,

um endlich vermittelst des internationalen Bureau’s der Maasse und Gewichte zu der Ver-
gleichung und zur Kenntniss der Gleichungen aller Einheiten zu gelangen, deren man sich
inden verschiedenen Ländern bei den Triangulationen bedient hat, welche zum allgemeinen
Studium der Erdform beitragen sollen. Herr Hirsch ruft in Erinnerung dass besonders die
Bedürfnisse der Geodäsie im Schoosse der ersten Gradmessung von Gentraleuropa den Gedan-
ken der Gründung eines internationalen Bureau’s für Maasse und Gewichte wachgerufen
haben. Jetzt wo dieses Institut vorhanden ist und den Beweis seiner Tüchtigkeit geliefert hat,
wo anderseits das Bureau von Breteuil die nöthige Musse hat, um einen grossen Theil
seiner Zeit und seiner Hülfsmittel der Vergleichung der geodätischen Maassstäbe zu widmen,
wäre es zu bedauern, wenn aus reiner Nachlässigkeit ein grosser Theil der Geodäten auf das
einzige Mittel verzichten würden, das ihnen zur Verfügung steht, um eine für die Erdmes-
sung schädlichste Unsicherheit zu beseitigen. Zum Schlusse drückt Herr Hirsch den Wunsch
aus, die Permanente Commission möchte, gestützt auf die Autorität der Gonferenz, welche
dem Schlussantrag des Berichtes des Herrn Obersten Bassot zugestimmt hätte, an die Erd-
messungscommissionen der verschiedenen Länder, welche ihre geodätische Maassstäbe noch
nicht an das internationale Bureau für Maasse und Gewichte gesandt haben, wiederum eine

Einladung ergehen lassen, um dieselben zu bitten, mil dieser Zusendung nicht mehr lange
zu warten.

Der Herr Präsident ist der Meinung, es wäre angemessen, um die Zusendung der
Maassstäbe aus grossen Entfernungen zu erleichtern, dass das internationale Bureau die zum
Transport nöthigen Vorsichtsmassregeln angebe.

Herr Bassol bemerkt, dass diese Apparate, um ihrer Bestimmung zu genügen, noth-
wendiger Weise so construirt und verpackt sind, dass sie einen langen Transport aushalten
können.

Der Herr Präsident frägl an, ob jemand in Bezug auf die Anträge des Herrn Obers-
ten Bassot weitere Bemerkungen zu machen habe. Da Niemand das Wort ergreift, so sieht
der Herr Präsident dieselben als angenommen an.

Er bittet hierauf den Herrn Prof. Helmert die Berichte, deren Ausarbeitung er über-
nommen habe, mitzutheilen.

Herr Helmert verliesst in französischer Sprache seinen Bericht über die Bestimmung
der Schwere vermillelst des Pendels. Obgleich man seit dem letzten diesen Gegenstand behan-
delten Berichte in Nizza, im Jahre 1887, zahlreiche Messungen ausgeführt hat, muss sich der
Berichterstatter wesentlich noch darauf beschränken, die neuen Stationen zu erwähnen, da
man bis jetzt nur einen kleinen Theil der Resultate veröffentlicht hat. Eine grosse Anzahl von
Stalionen hat man besonders in Frankreich und Oesterreich ausgeführt; auch in Russland
werden die Pendelbeobachtungen im grossen Maassstabe fortgesetzt; endlich wurde eine
Bestimmung in Padua und andere auf den Sandwichsinseln ausgeführt.

Ein grosser Theil dieser Messungen haben zum Zweck, den Einfluss der Höhe auf
die Intensität der Schwere zu studieren. Im allgemeinen scheint die früher schon constatirte

 

iy
i

1.
17

en PR PT RSR IE EEE r
Ge 4 DE SER

 
