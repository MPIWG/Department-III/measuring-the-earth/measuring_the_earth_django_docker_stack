INDICATION
LATITUDE.

DES POINTS.

Krasnogorsk
Aleksandrowskoié. .
Wozdwijenskoié. ...
Bich-Agatch
Guirialskala.......
Jettinskaia
Werkhnie-Ösero.... |
Kantchourina
Nikolskaia.
Mametiééwa
Ssarwaiéwa
Mouil-Agatch
Alitchinbaiéwa
Jsperdina
Del-el-Tubé
Polowinnaia
Banwaia
Tchouwar-Tubé....

(bout septentr. de la
base)

(bout mérid. de la!
base)

 

 

 

Valeurs de l'erreur moyenne des

|
]
|

LONGITUDE. EPOQUR.

Jo

XV. — Russie.

DIRECTEURS

| ET OBSERVATEURS.
||

 

Triangulation du Parallèle 5%. (Suite.)
73° 44'

73 où
AN A
15
14:9
74 24
ES
74 45
74 48
|
Ton ot
79
19226
75
10.2
12,98
76
76
76
76

Travaux supplémentaires 1861-72.
Colonel Zylinski.

76

 

angles, calculées d'après la formule

2

>

m
37

he +4/

pour les séries de triangles, faisant la partie russe de la triangulation du 52me parallele.

Nombre
des

triangles.

1) Série de triangles entre la base d’Orsk et celle de Bousoulouk

»

»

»

»

» de Bousoulouk et celle de Wolsk

> de Wolsk et celle d'Eletz

» d'Eletz et celle de Rogatscher

» de Rogatscher et celle de Varsovie

INSTRUMENTS.

 

 

Erreurs

moyennes.

+ 07,87
+0 ,95
+1,15
+] ,29
+1 ,00

REMARQUES.

 
