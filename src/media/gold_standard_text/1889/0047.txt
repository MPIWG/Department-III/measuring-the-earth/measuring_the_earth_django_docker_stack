 

ener ee

 

j
!
i
Ë
F
&
!
f

11

Depuis 1884, les discordances par nivelées sont groupées, pour chaque section, d’après
leur nombre et leur grandeur en un diagramme que Pon compare ensuite au diagramme
théorique donné par la loi des probabilités, et répondant & la méme erreur accidentelle
moyenne. La concordance a toujours été remarquable.

On obtient, d’autre part, la portion systématique — toujours trés faible de ces
discordances, en annulant celles-ci progressivement depuis lorigine de la section et en pre-
nant le coefficient moyen d’inclinaison sur l’axe des abscisses, de la courbe représentative de

 

ces discordances cumulées.
Un album donnant les résultats de ces deux genres de recherches est actuellement
en cours d'impression et sera présenté à l'Association dans Pune de ses prochaines réunions.

M. Ferrero est heureux de voir qu’on s'occupe de la question des erreurs; mais a
mesure que les données augmentent, il devient plus nécessaire qu’on les étudie dans chaque
pays même où elles sont recueillies, et qu'on lui fasse connaître seulement les résultats.

M. le Président, avant de passer à l’ordre du jour, communique des excuses qu'il a
reçues de plusieurs invités, qui n'ont pas pu prendre part aux séances, tels que M. le Minis-
tre de Belgique, le prince de Monaco, M. Goulier et M. Tcheng Ki Tong.

Il donne ensuite la parole à M. v. Sterneck pour compléter son rapport, lu dans la
dernière séance, par une note sur les déterminations de la pesanteur, qu'il a exécutées
en 1889. (Voir Annexe B. XXIII°.) :

M. Helmert se félicite des nombreuses expériences qui ont été faites en Autriche
et en France sur les variations de la pesanteur.

M. le Président donne ensuite la parole à M. Carusso, qui lit le rapport de la
Grèce, (Voir Annexe B. XIX.)

M. Rümker donne quelques informations sur le nivellement du terrain de la ville de
Hambourg, et M. Nell parle également des nivellements exécutés dans la Hesse.

(Voir Rapport de la Hesse, Annexe B. XX.)

M. le Général Ferrero lit le rapport sur les travaux géodésiques italiens. La triangu-
lation, qui s’est accrue pendant la dernière année de 14 stations de premier ordre, sera com-
plètement terminée dans deux ou trois ans; le réseau contient sept bases mesurées, auxquelles
il faudra en ajouter encore une dans les maremmes de Toscane, dans le voisinage de Grosseto.
Les calculs de compensation sont poussés vigoureusement; on les a divisés en 18 groupes
de 45 à 50 équations de condition chacun, dont 11 sont déjà terminés.

Les nivellements de précision progressent également d’une manière continue; le rat-
tachement à la frontière française a été exécuté sur quatre points. Sur l’un d’entre eux, à Vinti-
mille, les cotes absolues provisoires, dont les françaises sont basées sur Marseille et les
laliennes sur Gênes, se sont rencontrées avec une différence seulement de cinq centimetres.

Quant aux maréographes, 12 sont en fonction et leurs courbes en voie de dépouille-
ment; avant la fin de l’année, il y en aura un nouveau d’installe a Ancona el probablement un
autre à Porto-Mauricio.

PROCÈS-VERBAUX — 6

 
