 

À

le système des déviations de la verticale dans le système des deux grands ares de latitude,
mais sans succès réel. Un ellipsoide de trois axes s’adapterait peut-être mieux; cependant
des calculs de ce genre ont actuellement peu de valeur, attendu que l’achèvement des mesures
des degrés de longitude en Europe, suivant le 52e parallèle, est imminent. En général, je suis
de l'avis qu’on peut le mieux se rendre compte des anomalies de la figure de la Terre si l’on
emploie désormais toujours l'ellipsoïde de Clarke de 1880 pour les recherches au sujet des
déviations de la verticale, aussi longtemps que nous ne possédons pas un autre ellipsoide
s’adaptant mieux à toutes les mesures existantes.

D'après une communication obligeante de la part du Directeur de l'Observatoire du
Cap de Bonne-Espérance, M. le Dr Gill, les triangulations exécutées sous sa direction par le
Major Morris dans la partie orientale de l'Afrique du Sud ont fait de réels progres. Depuis
Port Elisabeth au Sud, une chaîne de triangles s'étend, sur une longueur de 500 milles anglais,
jusqu’à la pointe Nord de Natal. Elle représente une mesure d’arc de latitude de 8°. Les
positions géographiques ont été calculées avec les dimensions de Clarke de l’ellipsoide ter-
restre de 1880, en partant de Zwartkop dans le Natal, et elles s'accordent en général bien
avec les déterminations astronomiques. Des différences plus notables se présentent à Berlin,
situé à 300 milles au Sud-Est de Zwartkop et à 130 Est-Nord-Est de Port Elizabeth. Puis, à
Port Elizabeth et aux deux points de Coega Kop et de Zuurberg, situés à 13 et 90 milles au
Nord de la précédente localité, on trouve les déviations suivantes :

Benlane +.» sten 20 6
Zaurbere;.. a 110.01
Coega Kop . .....at „8,47

Port Elizabeth. . + 8,5

On a à faire ici à des influences locales dont l'étude sera poursuivie.

M. Preston, dans son Rapport sur les mesures de pendule faites aux Iles Sandwich,
mentionne aussi une triangulation effectuée récemment, laquelle, combinée avec des mesures
astronomiques, a conduit à la connaissance de déviations importantes de la verticale sur ces
iles. Quatorze points sont groupés sur les quatre plus grandes îles, de telle façon qu'on a
reconnu les déviations au Nord et au Sud des hautes montagnes. Ces déviations vont jusqu'à
+ 1/2 minute. C’est avec un vif intérêt qu’on doit attendre de plus amples détails, car ils
promeltent de fournir des renseignements sur les densités des volcans, à diverses périodes de
leur développement. Les volcans éteints, comme le Haleakala, ont montré sur le pendule et le
fil à plomb une attraction correspondant à leur forme extérieure, tandis que ceux qui sont
encore en activité semblent posséder un défaut de masse.

HELMERT.

 

 

 
