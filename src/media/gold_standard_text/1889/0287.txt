 

 

TEE EUTIN, ST en

En

Sg

MI France ét Algérie.

 

Note sur les triangulations de France,
Gé E ci de Juris.

La triangulation primordiale de la France a été exécutée de 1792 à 1843.

Elle s’appuie sur sept grandes bases: Melun, Perpignan, Brest, Einsisheim, Bordeaux,
Gourbera, Aix, et comprend environ 740 triangles formant les trois chaînes méridiennes
de Paris, Sédan et Bayeux, les six chaînes dirigées suivant les parallèles d'Amiens, Paris,
Bourges, Lyon (moyen), Rodez, Pyrénées, la petite méridienne de Fontainebleau, la mé-
ridienne de Strasbourg et enfin la chaîne littorale du Sud-Est. | :

Ces chaînes méridiennes et parallèles forment des quadrilateres de 200 kilométres
environ de côté, remplis de triangles du 1° ordre déterminés avec la méme exactitude
que ceux des chaînes principales ; — on peut donc dire que la surface de la France
est couverte d’ un réseau continu de triangles du 1% ordre.

Les angles ont été mesurés par la méthode de la répétition à l’aide de 3 series au
moins de 20 duplications chacune, soit 60 fois. — Les cercles répétiteurs employés, con-
struits par Lenoir, Gambey, etc., étaient gradués suivant la division centésimale du
quadrant; le diamétre des cercles divisés était de 13 pouces ou 35 centimètres.

Les distances zénithales qui ont servi au calcul des différences de niveau géodé-
siques ont été obtenues aussi par voie de répétition, aux heures favorables de la journée,
par 3 séries de duplications chacune.

Les observations astronomiques de latitude, longitude et azimut ont été exécutées
à Paris (station primordiale) et en un grand nombre de points du réseau, soit par les
astronomes de | Observatoire, soit par les ingénieurs géographes, soit par les officiers
d’ Etat-Major du Service Geographique. .

C’est la méridienne de Paris, calculée par Delambre, appuyée sur les deux bases de
Melun et de Perpignan, et supposée parfaite, qui a fourni les longueurs des côtés, les
altitudes et les coordonnées géographiques de départ de tout le réseau français.

Dans le calcul des coordonnées géographiques on a adopté pour le demi grand
axe la valeur

a— 6 3576989 mètres

   
 

2

BE
=

ee
aa
4

   
 
 
 
 
 
 
 
 
   
   
 
  
   
