  

Q

Sitzungsberichte der Permanenten Commission. . , . Pag. 124-128

Pag. Pag.

Erste Sitzung, am 7. October 1889. Acciamation zum Präsidenten wieder-

_ Antrag des Herrn von Mulhacen der Con- erwähll. a: u 125
ferenz die Vertagung der Wahl ' des Bericht des Finanz- ee von Mer i
Höhennullpunktes auf drei Jahre vorzu- Hoenster,, u. . 125-127 |:
Schlagen. . . . 123 Die Commission eee ene we

Discussion und ame ib a 123,124 Helmert Entlastung für das Jahr. 1888. . 127

Die Commission billigt ebenfalls den Wunsch Die Commission votirt dem Centralbureau
des Herrn von Mulhacén, den Bericht einen Credit von 2000 Franken zur Fort-
über die Mareographen künftig dem setzung der Breiten-Beobachtungen . . 127
Herrn v. Kalmär zugleich mit dem Be- Das von Herrn Helmert für die Arbeiten
richte über die Nivellements zu über- des Centralbureau’s im Jahre 1890 vor-
WOM ee eee ge 124 geschlagene Programm wird genehmigt. 127,128

Freiburg i. Br. wird zum Vereinigungsort
der Permanenten Commission im Jahre
Herrn Marquis von Mulhacen wird. durch 1890 sowabt is ke eg ee ee 128

Zweite Sitzung, am 12 October 1889.

 

ANNEXES — BEILAGEN

A. Rapports et mémoires spéciaux. — Berichte der Spezial-Referenten.

Pag.
Annexe I. Rapport sur les nivellements, par M. v. Kalmar (avec une carte) . . . . . 4-49
Annexe II. Rapport sur les maréographes, par M. lue Mulhacen ss. ; 4-44
Annexe III. Rapport sur les longitudes, latitudes et azimuts, par M. van ae De oa
(avec deux cartes)». + - wre A
Annexe IV. Rapport sur les triangulations, par M. irre 0 26 ee avec une A
Annewe V. Rapport sur la mesure des bases, par M. Beowotigns aa Anuanie cer aa 1-61
Beilage VIA. Bericht über die Lothabweichungen, von Herrn Helmert . . . . . . . . 1-A
Annexe VI Rapport sur les déviations de la verticale, par M. Helmert.. . . . . + a. 1-5 L
Beilage VITA Bericht über die Pendelmessungen, von Herrn Helmert. 1-5 |
Annexe VII! Rapport sur les mesures de pendule, Pat M Hebeps es se 1-5
Annexe VIII. De l'influence de la pression du fluide ambiant sur le pendule + par M. Def.
forges: ose RE oon ES 1-7
Beilage IX. Bericht über die Thaligkeit ile Centrales in don Frage über die Veränderlich-
keit der Lage der Erdaxe, von Herrn Albrecht (mit einer Tafel Ve 1-14
anexe X.  : Mémoire sur les méthodes employées pour déterminer l’aplatissement terrestre, par
MeTossorana MS le es Herre (es Bee ee 1-9

! Ce rapport a été imprimé en Italie et intercalé ons le présent volume.
Dieser Bericht ist in Berlin gedruckt. worden.

   

 
