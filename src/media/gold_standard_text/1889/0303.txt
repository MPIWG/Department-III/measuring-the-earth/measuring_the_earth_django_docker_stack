  

249

VI.— France et Algérie.

hl CL tC

 

 

 

 

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Br INDICATION | DIRECTEURS
N° LATITUDE. |LONGITUDE. ÉPOQUE. | INSTRUMENTS, REMARQUES.
ak DES POINTS. | ET OBSERVATEURS.
|
hy i. Triangulation Primordiale Algérienne. (Partie Orientale — Suite.)
nt F Schal. 2.2... 36° 16'34"| 20°58' 21") 1861 | Mémorial du Dé-
NE Ga Adet bon Souifa. 36 2 3|21 048 1861 | ne à
- Piha oo cis. SS 8055-37) 21 18 | 1861
e Ree Afoal......... 35 53 18 | 2113 .9| 1861
è Ben Abdallah. ..... 35 58 40 | 21 28 14 | 1861 ) Cercle répét. n° 13:
We Dra el Djoua...... 56 12 42) 21 82 55 | 1861
El Afroun......... 36 2 24) 01 49 00 | 1861-62
| Azeroun Said...... 36 11 28 | 21 51 33 || 1861-62 |
| Lalla Kredidja. . 36 26 55 | 21 58 35 || - 1861-62 /
E Mansourah......... 36.2 5.22.65 1862
L | Tirghreunt........ S07 23 |. 22 99 38 1862
I Meuricane......... 36 9.20.92 36 7 1862
£ | Azeroun Beni Our-
| Blane. no... 36 26 27 | 22 29 25 1862
Wi BE eS 36 18 8 | 22 46 42 1862
Ë Sn... 36 124,92 50 24 1862
| Mesriss...........- 56 19 5471-25, 118 1862-63
I Mnäguer .......... 36 17.16 | 23-756 | 1862-68
| Grae .....: 36 424: 58 16 51 | 1869-63 |
I Schouf Aissa Ben : Data Versi
L Wie ks 36 17 46 | 23 24 50 1863 hs es:
D Tababourt......... 36 32019" 128: 77 2 1863
| le a 36 34 9 - 25 15 1863
E Schouf Melouk..... 36 20 15 | 23 46 30 1863 ale. vous We
| Mouleh el Meid....| 36 34 54 | 235223 | 1863 Kann
Ë Zonaoui +... 36 19 10 | 24 63 1863
teil SIdr Driss. .......: 36 35 20 | 2413 34 || 1863
on |) Quasch............ 36 28 40 | 24 27 19 | 1863-65
pi - Wore: <2. os 36 39294 993] 1863
Li Guebel Mzära de
F Bir Setel........ 36 810] 24 41 5 | 1863265
. Thaya........... 36 30 24 | 24 46 54 1865
Mahouna .......... 0 22 3.25.9024 1865
Aotara. 86 39 55 | 95 14 9 1865
LE. Ouenkel........... 36 14 3 | 25 16 18 1865
| MGs 86 23 574 25 43°91 1865
’ Bou Aboed........ 36.47 0 3541 29 1865
E R’hourra.......... 36 35 56 | 26 2 45 1865
i Bone (Phare). ..... 36 54 31 | 25 26 22 1865 |
j | | a
E
Triangulation Primordiale Algérienne. (Partie Occidentale.)
Nador des Soumata. | 36°25' 1”| 20°10! 20" 1864 Id.
Sidi Ali des Che-
; ne an, 36 35 > 20-1 38 1864
accar Gharbi..... 36.1955, 719, 5241 1864 bye Da say
1% Ce Din. 5644 4 0 6 8] 1864. | Capliaines Rorrior ot Row
pl Lari Taourirt ..... 36 25 45 | 19 36 55 1864 ne
we Amrouna.......... 35 56 29 | 19 40.29 1864 ad
a} ee 36 12 17 | 19 97 13 | 1864 Ti à
if Ouarsenis.......... 35 52 30 | 19 17 50 1864 nn
il Sala... 35 54.20 119 023 | 1864-65
atte Ouled Kocéir...... BO 29 Ml iO 6 1 1864-65 |
LE Djebel Bioid....... 36 10 37 | 18 40 51 1865 Oanitälee R
if Miralow. 2. |; 35 55 16 | 18 34 36 || > 1865 a
| Sidi Sald.......... 30 830. 189319 1865
Keloub Tsour...... 85 52 44 | 18 7 49 1865
WE |
\ À

 
