 

 

Beilage XXIII”.

 

b. ASTRONOMISCHE ARBEITEN.

Im Sommer 1889 wurde die Längenbestimmung Schneekoppe-Dublie, sowie Polhöhen-
und Azimuth-Bestimmungen auf 6 Stationen des böhmischen Gradmessungsnelzes, und zwar
auf den trigonometrischen Punkten 1. Ordnung Swednik, Mezivratäch, Pecnej, Donnersbery,
Sadska und Wysoka ausgeführt.

Zu der Längenbestimmung wurde das auf der Schneekoppe bereits bestandene königl.
preussische Observatorium verwendet, und es waren daselbst nur unwesentliche Adapti-
rungen auszuführen, um es zur Aufstellung unserer Instrumente und Apparate geeignet
zu machen.

Auf Dablic wurde ein neues Observatorium erbaut, und stand daselbst das Instru-
ment im Centrum der Station, unmittelbar auf dem Marksteine. Die Verbindung mit dem
Staats-Telegraphen-Netze wurde durch eine 4,5 km. lange Zweiglinie hergestellt.

Die Beobachtungen begannen am 21. Juni und waren am 15. Juli vollendet. Nach
/ gelungenen Abenden, nämlich am 1. Juli, fand der Beobachterwechsel statt; die Instru-
mente wurden nicht gewechselt.

Der Vorgang bei den Beobachtungen war der gleiche wie bei allen übrigen 10,
seitens des militär-geographischen Institutes bereits ausgeführten Längenbestimmungen,
und zwar genau nach Hofrath v. Oppolzer’s Vorschriften; auch waren die gleichen Instru-
mente, Uhren ete., wie sonst in Verwendung.

Mit den Beobachtungen der Breite und des Azimuthes auf den früher genannten 6
Stationen wurde begonnen, um einem immer mehr und mehr gefühlten Bedürfnisse nachzu-
kommen, nämlich in einem grösseren, geschlossenen, geodälischen Netze, wie es das böh-
mische ist, alle Elemente zu bestimmen, die zur Ableitung der wahren Form der Geoidfläche
nöthig sind.

Durch die heuer ausgeführte Längenbestimmung zwischen zwei Punkten dieses
Netzes und die auf jeder der 40 Stationen desselben mit grosser Sorgfalt bestimmte Breite,
Azimuth und Schwere werden hoffentlich binnen kurzer Zeit in diesem ausgedehnten Gebiete
alle Elemente zu dem gedachten Zwecke gegeben sein.

Um diese Arbeit durchführen zu können, ist jedoch die Beschränkung der Breiten-
bestimmungen auf nur eine Methode, nämlich auf die Methode der Zenith-Distanz-Beobach-
tungen unerlässlich, doch werden, als theilweiser Ersatz für eine zweite Methode, auf jeder
Station auch Meridian-Zenith-Distanzen nördlicher und südlicher Sterne gemessen, welche
Methode der Breitenbestimmung viele Vortheile bezüglich der Theilungsfehler der Kreise,
Biegung des Fernrohres, Refraction und Ungenauigkeit der Declinationen bietet, da hiebei

 

 
