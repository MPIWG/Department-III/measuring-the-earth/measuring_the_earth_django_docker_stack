    

1 Angle entre le signal et une
; mire méridienne, dont l’a- |
zimuta été déterminé par un
cercle méridien de Brunner. |

efor:

Vien} Azimut d’une mire installée
sur le Panthéon mesurée
directement au moyen du
cercle méridien n° ? de Ri-

Année et mois.

1884 avril
et mai.

»

1884 septemb.

et octobre.
1884 aoüt.

1885 juin et
juillet.

1886 juillet et
aoüt.

et octobre.

et octobre.

 

| gaud.

  

| Angle entre la Polaire et le

jm” _ Signal, mesuré par un ins-
‚| trument universel de Rep-
A sold.

In,

| ju {

|

 

1888 juin et
juillet.

1884 septemb.'

1887 septemb.

1866 septemb.

 

 

TITRE DE LA PUBLICATION.

Pas encore publie.

Annales de l’Observatoire
de Paris. (Tome IX.)

Pas encore publié.

RAPPORT SUR LES AZIMUTS,

   

 

REMARQUES.

Resultat definitif.

»

Resultat definitif.

Resultat provisoire.

Cette valeur differe de celle

donnée par M. Y. Villarceau
u

de — 0,5, correction quipro-
vient de ce qu’on a ramené
le catalogue de M. Villar-
ceau au catalogue de la con-
naissance des Temps et a
celui de M. Lewy.

Sous presse.

 

 

 

ENG >

 
