 

 

 

 

 

 

 

 

 

 

 

16

States, -of which a copy is presented. The practical value of the deductions in these investi-
gations is seen in the numerous and important calls made for the variation of the magnetic
needle at given epochs when the early surveys of the country were made almost wholly by

compass courses and distances.

Except where special expeditions have been fitted out, the magnetic observations are
made by the triangulation parties during the occupation of a station, and never interfere
with or prolong the regular work. In the transportation of a party for long distances, as in
the earlier geographical expeditions to Alaska, and in the present movement of the parties
to the Yukon and Porcupine Rivers, advantage is taken of every stoppage to obtain magnetic
as well as astronomical observations. Two magnetic observatories have been maintained by
the Survey where the records of the different instruments are made by photography.

| present herewith a chart exhibiting the Isogonic Lines for the epoch January 1885 within
the limits of the United States.

XIl. — THE HYDROGRAPHIC WORK ON THE SEABOARDS
AND THE DEEP SEA INVESTIGATIONS

For the use of the Navigator, the depths of the water along the coast are usually car-
ried to moderately deep soundings. The distances from shore are increased as the immediate
demands of close shore work are filled. The Atlantic and Gulf sea coast depths differ wholly
from those along the Pacific coast at the same distances. A broad submarine plateau exists
off the eastern coast; on the Pacific coast there is no such plateau. The depths reach 2000
fathoms in a distance of fifty or sixty miles off the California Coast, and the Coast mountains
attain an elevation of 5000 feet within less than three miles of the sea. On the Atlantic and
Gulf seaboards the hydrographic work is well advanced, and more attention is permitted to
the investigation of currents and the study of bar formations.

As far back as 1845 the Superintendent of the Survey instituted investigations in the
off shore waters of the Atlantic seaboard, in order to embrace the whole of the course of the
Gulf stream. The work has been continuously carried on, and has led to the examination and
study of the waters of the Gulf of Mexico. The methods and means of deep sea investigation
have been thereby thoroughly developed by the officers of the United States Navy who execute
this work under the direction of the Superintendent. The breadth, velocity, and direction of
the currents of the Gulf Stream and the temperature of the water are determined for the sur-
face and for different depths ; they are also measured for different times of the year and for
different years. The temperature and movement of the adjacent colder waters are determined
with equal care. In connection with these practical problems the Survey has materially aided
the investigation and study of the deep sea flora and fauna, as shown in the earlier work of
the elder Agassiz and Pourtales; and in the later work of Alexander Agassiz detailed in his
« Three Cruises of the Blake. »

On the Pacific Coast the hydrographic surveys have developed some curious features

 

 

it nina a nits

i cis Malis

 
