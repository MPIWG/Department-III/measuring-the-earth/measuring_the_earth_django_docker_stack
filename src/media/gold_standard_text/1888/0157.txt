 

61

  
   
   
   
    
   
    
   

a, = + 0,420 + (1) — (3) Gew. 1 % = — 0,064 — u + (1) Gew. 2
er ee an ar 000 le
ee OO a» | „ OTS a 0 u.
à, — + 0,106 + yb) ia ol Ne DO Cd D
a = 211080 Oh ea) hy Sep 10,092, 4 (2) (8). 7 |
Be a QUT A) ar ai er)

best, |

Die Gewichte der einzelnen Bestimmungen sind für jede Linie gleich 1 angenom-
men; demgemäss erhielten die Gleichungen 7, 8 und 9 das Gewicht 2, damit ihre Differen-
zen, welche beobachteten Linien entsprechen, das Gewicht 4 haben. Der Ansatz dieser Feh-
lergleichungen entspricht dem Umstande, dass die Längenbestimmungen Berlin-Breslau und
Breslau-Königsberg wesentlich auf denselben Zeitbestimmungen in Breslau beruhen, sowie
der Erfahrung, dass der Fehler der telegraphischen Uebertragung der Zeit, welche aller-
dings nur auf den genannten beiden Linien und nicht direkt zwischen Berlin und Königsberg

 

 

 

  

 

| erfolgte, als gering gegenüber der Gesammtheit der Fehler der Zeitbestimmungen betrachtet

| werden kann, x ist eine Unbekannte.

| Die Normalgleichungen sind die nachstehenden :

| ENDWERTE

F Poa. - EN ee ie

0 © D © IE @ 0 97 eee

& .— 1)432— 8)— (4 = 136 2 a

| À 8 2) 33) + 11,198 0) ease

|: = a © in. © . = + 0,006 |(4) = — 0,007

[1 19y 0 Fa) „9 ls

| 1 —2u - . + 2 (6) 0 16) = — 0,075

| .— () . — (9) an) Ha) 00%
Hierin musste (wie von vornherein klar) eine der Unbekannten () . . . (7)

gleich null angenommen werden, da die Summe der Gleichungen sich identisch null er-

giebt.
Der mittlere Fehler für’s Gewicht 1 folgt gleich

0,004299

Ss
oo md on \c
m AG + 0,029

 

Berücksichtigt man auch noch den Schlussfehler des Längendreiecks Königsberg-
Memel-Goldap von 1887, so wird

    

/0,005840

5
G — + 0,031,

==

   
