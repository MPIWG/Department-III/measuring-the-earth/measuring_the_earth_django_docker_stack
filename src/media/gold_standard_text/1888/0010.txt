|
|
|
WW

 

 

 

 

4

80 Herr Professor F. Karlinski, Direktor der Sternwarte, in Krakau.

9° Herr Ch. Lallemand, Secretär des französischen General-Nivellements, in Paris.
10° Herr @. Rümker, Direktor der Sternwarte, zu Hamburg.

11° Herr Dt Schols, Professor am Polytechnikum, zu Delft.
12° Herr General-Major Schreiber, Chef der « Landesaufnahme », zu Berlin.
130 Herr Professor Tinter, Präsident der K. K. Gradmessungs-Gommission, zu Wien.
14°. Herr F. Tisserand, Mitglied der Akademie der Wissenschaften, in Paris.
15° Herr Professor E. Weiss, Direktor der Universitäts-Sternwarle, zu Wien.

Il. E ingeladene ;

1° S. E. Graf Thun-Hohenstein, K. K. Statthalter, zu Salzburg.
90 S, E. Feldm.-Lieutn. Baron Wimpffen, zu Salzburg.

30 Herr General von Pohl, zu Salzburg.

4° Herr Bischoffsheim, zu Paris.

50 Herr Perrotin, Direktor der Sternwarte, in Nizza.

6° S. E. Herr Baron von Te/fé, aus Brasilien, in Paris.

7° Herr Dr Schumacher, Bürgermeister von Salzburg.

80 Herr Dr Poschacher, 1. Bürgermeister-Stellvertreter, zu Salzburg.
99 Herr Dr Spengler, 2. Bürgermeister-Stellvertreter, zu Salzburg.
10° Herr Professor Lukas, Gemeinderath von Salzburg.

11° Herr K. Petter, Gemeinderath von Salzburg.

12° Herr A. Newmitller, Rechtsrath, zu Salzburg.
13° Herr Dauscher, Ober-Ingenieur, zu Salzburg.

©

S. E. der Herr Statthalter Graf Thun begriisst die Versammlung im Namen der k. k.
Staats-Regierung in der wohlwollendsten Weise mit folgenden Worten:

« Hochansehnliche Versammlung !

« Als Chef der politischen Verwaltung des Landes, in dessen Hauptstadt der Congress
der Bevollmächtigten für internationale Erdmessung heute tagt, wurde mir der Auftrag zu
Theil, Sie, meine hochgeehrten Herren, namens der österreichischen Regierung zu begrüs-
sen. Ich komme diesem ehrenvollen Auftrage mit aufrichtiger Freude nach, indem ich Sie,
meine hochverehrten Herren auf österreichischem Boden herzlichst willkommen heisse. Ein
wissenschaftliches Unternehmen, welches sich, wie die internationale Erdmessung, cine so
umfassende Aufgabe zu lösen bestrebt, trägt den Character des Grossartigen, Universellen an
sich und mit Bewunderung blickt sowohl der Laie als der Mann der Wissenschaft auf die
Männer, welche um die Lösung so schwieriger Aufgaben sich bemühen.

« Wenn solche Männer sich zusammenfinden, um im gegenseitigen Verkehr und

Oo ©

a | Vee al » =
Gedankenaustausch ihr grosses Werk zu fördern, aber auch der Erholung auf einem der
c LAS a

nn

QI creo

Men

op

 

‘SE NON CN AMAR

ERBE STE

 

 

 
