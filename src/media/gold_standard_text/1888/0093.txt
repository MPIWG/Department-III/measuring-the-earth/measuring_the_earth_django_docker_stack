 

SSE FRB APPE

2

N dk
R de
sie
ence
rave
eral
„En
dro-

‘i
Iqué
ek

|
|
|
8
k
Ê
|
|
|
|
I

FPE RER SERRES

EE RENE ÉTEND ERNEST DENON

Al
nière, à la cinquième séance de la réunion de Nice (voir p. 92 du texte français des Comptes-
rendus de Nice), la Commission permanente a adopté une résolution, proposée par M. de
Kalmär et Hirsch, qui demandait, dans tous les pays maritimes, des nivellements de préci-
sion le long des côtes, réunissant directement tous les maréographes et échelles. L’exécution
de cette résolution faciliterait beaucoup Vidée de M. Faye.

M. Fœrster demande de clore la discussion qui, dans ce moment ne saurait être bien
utile pour l'avancement de la question, et comme la rédaction présentée par le Bureau ne
prétend pas impliquer une solution définitive, il propose de l’accepter sans autre.

M. le Président ayant ouvert le scrutin, la proposition du Bureau est votée par lu
Commission permanente a Vunanimitée. M. Nagel, quia du partir avant la fin des débats,
avail délégué sa voix a M. Foerster.

M. le Président revient à la lecture des rapports des différents pays et donne en pre-
mier lieu la parole à M. le colonel Capitaneanu, délégué de la Roumanie.

M. Capitaneanu rend compte sommairement des travaux géodésiques peu nombreux.
exécutés pendant les deux derniéres années en Roumanie, où il a fallu consacrer en pre-
mier lieu toutes les forces dont on dispose à la confection de la carte du royaume. (Voir le
rapport de la Roumanie 4 l’Annexe VIII.)

Ensuite, M. le Président prie M. le général Schreiber de communiquer son rapport
sur les travaux accomplis l’année dernière par la section trigonométrique de l'état-major
prussien.

M. le général Schreiber rend compte en détail de l’état d'avancement de ces travaux
jusqu’au moment actuel et il promet de compléter, par l'exposé de ceux qui sont encore en
cours d’exéculion, son rapport qu'il enverra au Secrétaire à la fin de l’année. (Voir Annexe
VIP.)

M. le Président invite M. Hirsch à présenter son rapport sur les travaux exécutés
en Suisse, puis il le prie de bien vouloir lire le rapport espagnol.

M. Hirsch rend compte brièvement des travaux de la Commission géodésique suisse.
(Voir Annexe X.)

Il fait ensuite lecture du rapport de M. le général [banez sur les travaux d'Espagne.
(Voir Annexe I.)

M. le Président donne la parole à M. l'amiral baron de Teffe, qui explique que l’'Em-
pereur Don Pedro a été malheureusement empêché, par la grave maladie dont il a souffert,
de réaliser la promesse donnée à la Conférence de Nice de faire entrer l'empire du Brésil
dans l'Association géodésique internationale. Toutefois, ayant été autorisé par Sa Majesté
l'Empereur et gracieusement invité par M. le Président à assister déjà à la Conférence de
cette année, il se permet de déposer sur le Bureau plusieurs cartes brésiliennes, ainsi que
deux publications, dont lune concerne les déterminations de longitudes, et l’autre l’obser-
vation du passage de Vénus en 1882. Ensuite, M. le baron de Teffé rend compte des travaux.

géodésiques proprement dits exécutés dans son pays.

PROCÈS-VERBAUX — 6

 
