este
Il de

p
te

od

i
|
i
I
j
i
t
f
;
|
È
L
i
’
;

 

POPC

a

|
|
À
È
|

 

93

 

IV. — RESUME DES OBSERVATIONS

Les tableaux suivants contiennent le résumé des mesures de la durée d’oseillation,
exéculées aux quatre stations.

Les observations sont réduites à 10° et à l'amplitude 0 par les formules habituelles.

Le coefficient de dilatation admis pour le bronze des deux pendules est 00000175
par metre et par degré.

Les résultats sont ramenés à la latitude du Mont Gros par la formule de M. Helmert
di = 0. 00526 (sin 9 — sin ? o)

ou 7 est la longueur du pendule qui bat la seconde a la latitnde o.
LE ®
a nm s à
On en conclut, pour un pendule reversible de 0"50 de distance entre les couteaux :

dT == — 0,0037 sin % de

On trouve ainsi:

Réduction de Peïracava au Mont Gros -}+ 0,000014
» du Barbonnet » — 0,000008
» de Nice (génie) » — 0,000002

Les abréviations PLB (poids lourd en bas), PLH (poids lourd en haut) indiquent à
laquelle des deux positions du centre de gravité du pendule correspondent les séries consi-
dérées.

Les indications : masse ., masse :, correspondent aux deux positions que peut oc-
cuper la masse pesante aux extrémités du pendule.

Les indications : droite, gauche, marquent la position de la signature du construc-'

teur, gravée sur une face de la lame du pendule, par rapport à l’observateur.

 
