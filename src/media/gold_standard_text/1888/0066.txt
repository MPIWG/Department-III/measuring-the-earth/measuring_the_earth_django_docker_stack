 

|
|

14

 

bérations et au Secrétaire celui de rédiger les procès-verbaux. Il fixe en conséquence la
prochaine séance à mercredi, à 2 heures précises.

M. le professeur Tinéer annonce à l’Assemblée que S. E. Monsieur le Gouverneur de
la province désire recevoir chez lui MM. les membres de la Conférence, jeudi à 8 heures
du soir, et que les Autorités de la ville de Salzbourg invitent, pour demain mardi, MM. les
membres de la Conférence et leurs familles, à une excursion sur le Gaisberg.

La séance est levée A 4 heures environ.

 

 

T—

4

 

 
