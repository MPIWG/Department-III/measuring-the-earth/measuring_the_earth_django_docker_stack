Is et
erie.

au
IMd-

 

E
E
x
I
&
|
Ë
È

 

|
k
|
|
!
\
'
L

 

 

que la jonction de la Gorse avec [Italie puisse étre entreprise le plus tôt possible et il com-
munique un projet de réseau de triangles, qu'il à élaboré a ve bu.

En l’absence de M. le commandant Defforges, M. Bassot donne encore un court ré-
sumé d’un rapport sur les mesures relatives de la pesanteur à Nice et dans trois stations des
Alpes-Maritimes, exécutées en octobre-novembre 1887. Ce rapport fait suite a la communica-
tion de M. Defforges à la Conférence de Nice et sera publié in-extenso comme annexe. Nous
relevons seulement ici comme résultats généraux que, dans les Alpes-Maritimes, la pesan-
teur diminue régulièrement avec là hauteur, que les observations s'accordent d'une manière
remarquable avec la formule de Bouguer et avec le coefficient de Baily, et que la densité des
couches de la surface est environ de #8/,,, de la densité moyenne du globe. (Voir Annexe Ile.)

M. Bouquet de la Grye désire renvoyer sa communication à la prochaine séance.

M. le Président donne ensuite la parole à M. Lallemand, qui expose les résultats ob-
tenus par le Service du nivellement général depuis la dernière réunion de la Commission
permanente.

Ces résultats se trouvent figurés sur des cartes, dont un exemplaire a élé distribué
à chacun des membres présents.

M. Lallemand annonce que les raccordements du nouveau réseau avec PEspagne sont
préparés, pour ce qui regarde la France, et que ceux avec ltalie seront terminés prochaine-
ment. Le nouveau réseau fondamental sera achevé dans quatre ans, c’est-à-dire en 1892.

_ La comparaison du nivellement actuel avec celui exécuté par Bourdalouë, il y a
vingt-cinq ans en moyenne, fait ressortir une discordance qui va en croissant du Sud au
Nord, depuis Marseille jusqu’à Lille, où elle atteint près de 80 centimètres. En raison de
son importance et de son caractère systématique, cette discordance parait difficilement at-
tribuable à des erreurs de l’une ou de l’autre opération; elle est due vraisemblablement à
un affaissement du sol, au moins pour la plus grande part.

Pour reconnaitre la nature de ce mouvement, et savoir s’il est progressif où pério-
dique, la Commission du nivellement général a décidé d'entreprendre, dès 1893, un troisième
réseau fondamental, dont l'exécution suivra ainsi à dix années de distance, en moyenne,
celle du réseau actuellement en cours. (Voir Annexe II.)

Cette communication soulève une longue discussion, à laquelle prennent part MM.
Hirsch, Faye, Bakhuyzen et Fœrster. M. Hirsch mentionne le fait qu'il a soupçonné éga-
lement de pareils mouvements du sol en Suisse, lorsqu'il a comparé quelques nivelle-
ments partiels de contrôle aux anciennes opérations, effectuées dix-huit à vingt ans aupara-

vant. Cependant ces mouvements étaient trop faibles et pas suffisamment caractérisés pour
qu’on osat en tirer des conclusions générales. A plus forte raison les faits signalés en France
lui paraissent avoir un grand intérêt, s'ils devaient se confirmer par des observations subsé-

quentes.
M. Faye n’est nullement surpris des mouvements du sol résultant des deux nivelle-
ments; ils sont conformes à d’autres faits et à l'opinion répandue que les continents s’abais-
