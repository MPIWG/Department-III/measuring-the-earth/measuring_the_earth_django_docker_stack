 

 

€

le

tay

à
les
en

In,

i:
ale

 

|
|

i
|
|
|
|
®

 

d'Helsoland et de son maréographe, qui ont déjà été rattachés autrefois à Wangeroog. Bien
que des mesures trigonométriques de hauteur ne permettent pas une exactitude suffisante
pour le but proposé, M. Helmert croit cependant obtenir, par ce procédé, des matériaux
intéressants pour des études.

La réduction des mesures antérieures est en majeure partie terminée et les publica-
tions, qui paraitront dans le courant de l'hiver prochain, contiendront toutes les observations
astronomiques obtenues avant 1887. (Voir Annexe VIT à).

M. le Président donne ensuite la parole 4 M. Foerster pour faire la communication
suivante :

M. Ferster, en s’appuyant sur le travail de M. Mylius, de la « Reichsanstalt » de
Berlin, dont il a pris la liberté d'offrir des exemplaires à MM. les membres de la Conférence,
résume les résultats des études qu’on a faites à Berlin dans ces derniers temps pour obvier
aux graves perturbations remarquées dans les indications des niveaux.

D'après ces études, l’eau contenue dans l’éther remplissant les niveaux exerce une
action élective sur l'excès des alcalins qui caractérise la composition des verres les plus usi-
tés dans la technique moderne; cette action produit peu à peu de très petites exulcérations
des surfaces de ces verres et trouble désagréablement la marche et les indications des bulles
de niveau.

Le remède pourra, selon M. Mylius, être trouvé dans la diminution de la part exces-
sive que les alcalis occupent dans la composition des verres de précision, par exemple dans
le remplacement partiel des alcalis par le plomb, et en même temps il conviendrait de déli-
vrer plus complètement l’éther de l'eau, ou de choisir un autre liquide encore plus ap-
proprié.

La continuation très active de toutes ces études est assurée el les institutions
scientifiques de Berlin seront toujours prêtes à fournir à ce sujet des informations aux sa-
vants et aux constructeurs des autres pays.

Comme Pheure est déja passablement avancée, M. le Président declare vouloir ren-
voyer à la prochaine séance, qui aura lieu dimanche à 1 heure, la lecture des quelques rap-
ports de pays, qui n'ont pas encore été présentés.

Toutefois, avant de prononcer la clôture, il est d'avis qu'il faut, comme il l’a an-
noncé au début de la séance, s'occuper encore du choix du lieu dans lequel se réunira la
Conférence générale de l’année prochaine. M. le Président rappelle que lan dernier, à
Nice, l’idée de tenir la prochaine Conférence generale A Paris a rencontré un assentiment a
peu pres complet. En conséquence, le Bureau prend la liberte de proposer maintenant a
l’Assemblée de choisir définitivement Paris, et cela d’autant plus que, parmi les capitales
des grands Etats faisant partie de l’Association géodésique internationale, le tour revient
actuellement à Paris.

Cette proposition ne soulevant aucune objection, est mise aux voix et acceptée à
l'unanimité des neuf membres de la Commission permanente présents à la séance. Paris est
