 

93
begleitet, beehre ich mich vorzulegen. In derselben sind auch die Laplace’schen Punkte
eingetragen.

gezeichnet: HELMERT.

Anlage ll zum Bericht des Centralbüreaus.

Uebersicht der Versendung von Erdmessungs-Publicationen durch das
Centralbureau.

Seit der Nizzaer Konferenz im Jahre 1887 hat das Centralbüreau von den Herren
kommissaren der Internationalen Erdmessung folgende Publikationen zur Vertheilung er-
halten:

1. Von Herrn General Schreiber in Berlin: Die Königlich Preussische Landestriangu-
lation. Hauptdreiecke, IV. Theil, die Elbkette, 1. Abtheilung. 74 Exemplare.

2. Von demselben: Die Königlich-Preussische Landestriangulation.

I. Verzeichniss der Druckwerke der Trigonometrischen Abtheilung der Landes-
aufnahme.
II. Die Dreiecksmessungen I. Ordnung 1876-1887.
III. Die Ergebnisse der Hauptdreiecksmessungen 1876-1885. 74 Exemplare.

3. Von der Königlich Bayerischen Gradmessungs-Commission in München: Telegra-
phische Längenbestimmungen für die Königliche Sternwarte zu Bogenhausen, I. Theil, von
G. von Orff. 103 Exemplare.

4. Vom K. K. Militär-geographischen Institut in Wien: Mittheilungen desselben.
VII. Band. 73 Exemplare.

>. Von der Königlich Bayerischen Gradmessungs-Commission in München: Ergeb-
nisse aus Beobachtungen der terrestrischen Refraction von C. M. von Bauernfeind. 3. Mit-
theilung. 103 Exemplare.

6. Von der Niederländischen Gradmessungs-Commission in Leiden : Uitkomsten der
Rijkswaterpassing 1875-1885. 186 Exemplare.

7. Von der Königlich Bayerischen Gradmessungs-Commission in München : Das
Bayerische Präeisions-Nivellement. VII. Mittheilung von C. M. von Bauernfeind. 100 Exem-
plare.

Ausserdem ist folgendes zu erwähnen :

|
I
}
|
i

 
