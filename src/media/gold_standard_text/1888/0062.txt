  

 

  

 

U

Vous serez d'accord avec nous, Messieurs, pour regretter l'absence de ces trois sa-

vants officiers, empêchés de prendre part à nos délibérations.

Nous sommes certains également d'avoir été l'organe des sentiments de la Commis-
sion Permanente, lorsque nous avons invilé, d’une maniére spéciale, à assister à notre
réunion, M. Bischoffsheim, qui nous à reçus Si brillamment l’année derniére dans son observa-
t M. Perrotin, qui en a fait les honneurs avec une si grande amabilité. Nous avons

loire, €
. Perrotin déjà aujourd’hui parmi nous, tandis que M. Bischoffsheim est

la joie de voir M

attendu pour ce soir.
Nous avons enfin le plaisir de vous annoncer que M. le Lieutenant-Colonel Capila-

neanu, notre ancien collègue de la Roumanie, à tenu à assister à cette réunion.

I. Depuis la dernière Conférence, le nombre des commissaires officiels s’est accru

de plusieurs délégations nouvelles :

1° En premier lieu, la nomination de M. le professeur Weiss, directeur de l’Obser-
vatoire de Vienne, en qualité de commissaire d'Autriche, en remplacement de notre regretté
collègue défunt, M. v. Oppolzer, à été confirmée par une dépêche de l'Ambassade dAutri-
che-Hongrie au Ministère des affaires étrangères de Berlin, lequel nous en a donné con-
naissance par l'intermédiaire du Bureau central.

9o M. Francisco Diaz Covarrubias, Consul oeneral du Mexique à Paris, a été nommé

délégué du Mexique, ainsi que cela résulte de la dépêche suivante :

SECRÉTARIAT
DES

AFFAIRES ÉTRANGÈRES Mexico, 23 Mars 1888.

Monsieur le Directeur général,

J'ai l'honneur de m'adresser à Votre Excellence dans le but de porter à sa connais-
sance que le Gouvernement de cette République, ayant été invité par celui de l'Empire alle-
mand à prendre part à l'Association géodésique internationale, organisée à Berlin, et de la-
quelle Votre Excellence est le digne Président, le premier Magistrat de la Nation à accepté
celte courtoise invitation et a désigné Monsieur Francisco Diaz Covarrubias, Consul général
du Mexique à Paris, comme délégué du Mexique auprès de l'Association dont il s'agit.

En faisant celte communication à Votre Excellence, je suis heureux de pouvoir lai
présenter les assurances de ma considération distinguée.

(Signé)  MARISCAL.

1 i c NA! S À AI A A ae eee = r S x = =
A Son Eve) lense Monsiew le Général Ibanez, Président de la Commission Permanente de
l'Association géodésique internationale, Calle de Jorge Juan, 8, Madrid.

Do (7 a x pee 2 Ree ae i ars . .
a Vous êtes déjà informés, Messieurs, par notre circulaire du 1° juin 1888, que
M. le Ministre de la guerre de la République française a fait savoir au Président que M. le

 
