 

}
i)
il
H

i
it
i)

i
i
i
i
|

30

 

V. DES ERREURS

L'erreur moyenne d’une détermination isolée de la durée d’oscillation est égale a
PLB + 0,0000032

PL = 0,0000050

Ces deux erreurs moyennes sont bien, comme l'indique la théorie, dans le rapport
inverse de ha h’.

Chaque valeur de T et de T’ qui entre dans le calcul de + provient en moyenne de
huit déterminations.

L'erreur moyenne de T sera donc :

+ 0.000001!
Celle de F sera :
+ 0.0000017

Mais on a, par une formule bien connue, pour lerreur moyenne de 7

 

 

4 3 :
C= Se 007 2 (AT)? 2 (AT = = J
ge...) I WAN eh (AT Ae) 000002

 

et, par suite, pour lerreur probable de T :

+ 0,000003

soit 97000 °2Yiron de la durée théorique observée.

L'erreur probable qui en résulte dans une mesure d'intensité relative de la pesan-

 

 

 

Yo

teur, sur la valeur du rapport — sera:
gi
Jo se An „(Ara 2 |
it Lu à 5 + 4(—2) = = 0, 0000012 1
Ja Ti, Ta |
|
ail ao dela valeur |
anna, Ge a valeur de cevrapport, |
80000 a
Ces erreurs probables, notablement plus fortes, mais assurément plus vraies que |
les erreurs probables données par le pendule invariable, nous paraissent représenter assez |
exactement l’erreur ä craindre dans la mesure expérimentale de la durée d’oscillation théori- |

 

 
