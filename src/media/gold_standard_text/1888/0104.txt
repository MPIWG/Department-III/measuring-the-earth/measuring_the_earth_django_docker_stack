 

b) Jonction des triangulations francaise et italienne dans les Alpes-Maritimes.

Le raccordement des réseaux frangais et italien, qui n’avail jamais été effectué dans
la région sud de la frontière franco-italienne, s’établit trés simplement en soudant lun a
l’autre le côté Cheiron-Grand Cover du parallèle de Rodez et le côté Meunier-Tournairet de
l’ancienne chaine des Alpes; les quatre sommets sont réciproquement visibles.

Le travail, entrepris l'an dernier, avait été interrompu à la suite d’une tempête qui
avait renversé les mires. Il a été repris et terminé celte année par M. le capitaine Tracou.
La jonction comporte un quadrilatère dont on a mesuré les deux diagonales.

Le voisinage immédiat de l'Observatoire de Nice, dont le rattachement au parallèle
de Rodez a été effectué en 1887, donne une importance particulière à ce raccordement des
deux triangulations voisines.

c) Rattachement de l'Observatoire de Marseille.

L'Observatoire de Marseille forme une station astronomique très heureusement si-
tuée à l'intersection de la méridienne de Sedan et de la chaîne du littoral méditerranéen. On
le rattache en ce moment au réseau de 1° ordre par le côté Carpiagne-Sainte Victoire. Le
travail est exécuté par M. le capitaine Durand.

d) Opérations de Tunisie.

Le Service géographique a entrepris, dès cette année, la triangulation générale du
territoire de la Tunisie : les opérations comprendront l'exécution des réseaux de 4er, de 2e
et 3° ordre, le nivellement géodésique de tous les sommets et le nivellement géométrique
des lignes principales de communication. Deux chaînes primordiales serviront de base à la
triangulation secondaire : d’abord la chaîne parallèle de Bône à lextrémité de la presqu’ile 4
du cap Bon, formant le prolongement du parallele algerien; en second lieu une chaine mé-
ridienne s’etendant de Tunis a Gabes. La premiere est déjà exécutée, la seconde a été com-
mencée cette année; la reconnaissance est faite et les signaux sont construits entre Tunis et
Kairouan ; les observations seront poursuivies cet automne par M. le capitaine Tracou. |

 

 

OPÉRATIONS ASTRONOMIQUES.

 

 

 

Longitude Paris-Greenwich.

La différence de longitude entre Paris et Greenwich, bien qu’elle ait été déterminée
déjà deux fois, ne parait pas avoir été obtenue avec la précision que comportent les mé- 4

 
