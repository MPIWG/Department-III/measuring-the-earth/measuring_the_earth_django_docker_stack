 

38

 

— Id. — Lois provisoires de l’affaissement du sol de la France (Comptes-rendus de l’Aca-
démie des Sciences, 1888, tome GVIT).

Ch. Lallemand : Note sur la détermination du niveau moyen de la mer à l’aide d’un nou-
vel appareil : le médimarémètre (Comptes-rendus de PAssociation géodésique in-
ternationale, Conférence de Nice, 1887, et Comptes-rendus de l’Académie des Scien-
ces, Séances des 28 mai et 11 juin 1888).

— Id. — Traité de Nivellement de haute précision. (Paris, Baudry et (le, 1889.)
Nivellement général de la France. — Instructions préparées par le Comité du nivelle-

ment pour les opérations sur Île terrain. (Paris, Baudry et Cie, 1889.)

 

 

se

 
