 

wir ihn noch das Vergnügen hatten zu sehen, General Perrier, einer der Abgeordneten

Frankreichs bei unserer Vereinigung, zu Montpellier gestorben ist.

« Diejenigen unter uns, welche den tüchtigen Kollegen mit dem festen, energischen
Charakter, mit dem unermüdlichen Fleiss und dem liebenswürdigen, fröhlichen Humor
näher zu kennen die Ehre gehabt haben, hatten letztes Jahr in Nizza eine leichte Verände-
rung in den Gesichtszügen und in der Haltung des Generals Perrier wohl bemerkt, aber sie
schrieben dies einer vorübergehenden Mattigkeit zu, welche bisweilen die unermüdlichsten
Arbeiter überfällt. Keiner von uns, meine Herren, hatte vermuthet, dass wir Gefahr liefen
einen hervorragenden und allgemein hochgeschätzten Mitarbeiter so bald zu verlieren.

«Es kommt uns in dieser wissenschaftlichen, internationalen Versammlung nicht
zu, auf seine Eigenschaften als Militär und Staatsmann einzugehen, welche die Mitbürger
des Generals beim Begräbnisse unseres tiefbedauerten Kollegen hervorgehoben haben. Es
möge jedoch auch einem Fremden erlaubt sein, seine llochachtung auszudrücken vor der
festen und zugleich gemissigten Ueberzeugungstreue, vor dem patriotischen Muth und den
bürgerlichen wie militärischen Tugenden, durch welche General Perrier in seinem Vater-
lande sogar bei den Gegenparteien sich die allgemeine Sympathie zu erringen gewusst hat.

« Es kommt uns ebenso wenig zu die grossen Dienste, wie sie es verdienen, zu
würdigen, welche Perrier sowohl der französischen Geodäsie, der er einen neuen Aufschwung
zu verleihen gewusst hat, um sie auf das Niveau aller neueren Fortschritte zu erheben, so
wie auch dem geographischen Bureau der Armee geleistet hat, welches er durch sein be-
merkenswerthes Organisationstalent, sowie durhh das Beispiel seiner unermüdlichen Thätig-
keit entwickelt und vervollkommet hat. Nach der Aussage seines Mitarbeiters und Freundes,
des Kommandanten Bassot, unseres Kollegen, hat er sogar noch auf dem Sterbebette einen
letzten rührenden Beweis seiner unverwüstlichen Arbeitslust gegeben. Die wissenschaftlichen
und militärischen Behörden Frankreichs haben übrigens diese Verdienste ihres Kollegen und
Kameraden, dessen vorzeitiges Verschwinden sie mit Recht tief beklagen, erkannt und wür-
dig belohnt.

« Es ist nur natürlich, wenn wir hier besonders die Arbeiten unseres verstorbenen
Kollegen hervorheben, welche sich mehr oder weniger unmittelbar auf das Ziel beziehen,
welches die Vereinigung der internationalen Erdmessung verfolgt. Auch auf diesem Felde
bedaure_ich es, durch die Umstände an derjenigen Ausführlichkeit gehindert zu sein, welche
mir aus Freundschafts- und Dankbarkeitsrücksichten erwünscht gewesen wäre.

« Es sei mir vor Allem erlaubt, zu konstatiren, dass wir es zum grossen Theile den
hohen Gefühlen und dem vorurtheilslosen Geiste Perrier’s, so wie dem Einflusse des
gelehrten Präsidenten des Längen-Bureau’s, den wir mit Vergnügen in unserer Mitte sehen,
verdanken, Frankreich an unserer geodätischen Vereinigung Theil nehmen zu sehen. Ich
glaube das Recht zu haben, dies zu behaupten ; denn im Jahre 1872 hatte ich die Ehre, mit
unserem damaligen Präsidenten, dem Feldmarschall Fligely, eine Audienz bei Thiers zu er-
langen, um ihm die Vortheile auseinander zu setzen, welche durch den Eintritt Frankreichs
in die zur Entwicklung der Erdmessung gegründete wissenschaftliche Vereinigung der civi-

lisirten Länder, sowohl für diesen Staat selbst als auch für die Wissenschaft entstehen wür-

 

 
