IN,
lh

lon
I

0"
Ink
les

N

 

 

d'uue autre station près de Barcelone, en raison du fait que la pente abrupte de la côte y fait
présumer une perturbation considérable. La situation isolée et avancée des iles Baléares fait
entrevoir des résultats très intéressants pour l'étude approfondie des déviations de la verticale.

Actuellement, il existe des déterminations de longitude pour quatre stations, qui sont
en même temps des points de Laplace. L’établissement dun autre point semblable serait dé-
sirable dans la région nord-ouest du pays (Santiago). Quant au point de Laplace de la station
de Tética, qui jusqu’à présent n’est rattaché qu'avec l'Algérie, M. le général Ibanez vient de
m'annoncer qu'on va le relier avec Madrid.

Portugal. L'œuvre principale à entreprendre nous parait être la mesure de la diffé-
rence de longitude entre Lisbonne et une station espagnole, attendu que le Portugal ne se
trouve actuellement rattaché au réseau des longitudes de l’Europe que par la détermination
Lisbonne-Greenwich, dans laquelle les équations personnelles n’ont pas été éliminées.

Algérie et Tunisie. Les 9 points astronomiques sont tous des points de Laplace. L’a-
vancement réjouissant des travaux géodésiques dans ces pays correspond entièrement au
grand intérêt qu'ils présentent, aussi bien pour la déduction de la figure générale de la
Terre que pour l'étude des formes géodésiques spéciales propres au littoral méditerranéen.

Italie. Les déviations considérables et singulières qu’on a signalées, surtout au sud
des Alpes el au sud des Apennins, où la déviation en latitude présente à Florence et à Pise
un signe contraire à celui de Pattraction des masses de montagne, demandent des recherches
étendues. Il est indiqué également de continuer l'étude de la marche des déviations en lati-
tude le long de la méridienne de Florence, 4 partir des Alpes jusqu’à la côte. Déjà cette
meridienne, ou du moins celles très voisines, qui passent par Munich et par Christiania, ont
été l’objet d’études approfondies en Allemagne et le seront sous peu également en Danemark
et en Norvège.

En outre, si l’on trouvait des différences notables de déviation pour des points astro-
nomiques voisins, il y aurait lieu d'y intercaler encore d'autres stations, par exemple entre
Nice et Gênes, et entre Turin et Andrate.

Le nombre des points de Laplace s'élève actuellement à 19; il fournit un contrôle
suffisant pour le réseau astronomique, et une base solide pour des études spéciales.

Autriche-Hongrie. Le magnifique réseau qui couvre ce pays est déjà entièrement
observé, mais non encore calculé; il contient 13 points de Laplace, la Schneekoppe com-
prise, el de nombreuses stalions astronomiques. On pourrait tout au plus désirer encore
l'établissement de quelques stations dans le Tyrol, pour faire suite aux recherches approfon-
dies exécutées dans les pays voisins, le long de la méridienne Florence-Munich-Chrisliania.

Suisse. Elle présente dès maintenant plusieurs stations de déviation trés intéressantes.
Le réseau astronomique contient 8 points de Laplace. En continuant les observations, il con-
viendrait peut-être de s'occuper du méridien du Simplon!, qui passe en Italie par Andrate,

1 C'est déjà fait. : A, H,
