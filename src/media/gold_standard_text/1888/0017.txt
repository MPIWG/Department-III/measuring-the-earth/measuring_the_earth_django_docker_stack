SRE

 

11

diese Ernennung dem Herrn Präsidenten durch eine Depesche mitgetheilt worden, welche
in der Uebersetzung hier folgt:

SEKRETARIAT DER
AUSWERTIGEN ANGELEGENHEITEN. Mexico, 23. Marz 1888.

« Herr Generaldirektor !

«Ich habe die Ehre Eure Excellenz zu benachrichtigen, dass die Regierung der mexi-
kanischen Republik von der deutschen Reichsregierung einge laden worden ist, sich an der in
Berlin reorganisirten internationalen Erdmessung, welche Eure Excellenz zu ihrem würdi-
een Präsidenten ernannt hat, zu betheiligen, und dass der Präsident der Republik dieser
(reundlichen Einladung Folge gegeben hat. Derselbe hat Herrn Generalconsul Diaz Govarru-
bias, in Paris, zum Delegirten Mexiko’s bei der internationalen Erdimessung ernannt.

« Indem ich dieses zur Kenntniss Eurer Excellenz bringe, habe ich die Ehre die-
selbe von Neuem meiner ausgezeichneten Hochachtuug zu versichern.
« (gezeichnet) MarıscAL. »

«AnS.E. Herrn General Ibanez, Präsidenten der Permanenten Commission der in-
ternalionalen Erdmessung, zu Madrid. »

3) Wie wir den Herren Delegirten bereits durch Circular vom 1. Juni 1888 mitgetheilt,
hat der Kriegsminister der französischen Republik dem Pri isidenten zur Kenntniss gebracht,
dass der Herr Oberst Derrécagaix, Nachfolger des verstorbenen Generals Perrier, zum Dele-
sirten bei der internationalen Erdmessung ernannt worden ist, um bei dieser, zugleich mit
en Commandanten Bassot, das französische Kriegsministerium zu vertreten.

A) Vor Kurzem, am 10. September, hat der Präsident eine Depesche erhalten, worin
demselben die Ernennung des Herrn Bouquet de la Grye, Ghef-Ingenieur beim hydrogra-
phischen Amt der Marine, zum Delegirten des französischen Marine-Ministeriums bei der
abe Erdmessung mitgetheilt wird.

5) Ausserdem bringen wir zur a er Versammlung, dass Herr Lallemand
dem ee lurch Schreiben vom 97. August, seine Ernennung dureh den Minister der
öffentlichen Bauten zum Delegirten ike General-Nivellements von Frankreich bei der Erd-
messung mitgetheilt hat. Wir bezweifeln nicht, dass diese Ernennung demnächst offiziell von
der französischen Regierung dem Präsidenten der internationalen Erdmessung zur Kennl-
niss gebracht werden wird'; einstweilen gereicht es uns zum Vergnügen, Herrn Lallemand
an der hiesigen Versammlung theilnehmen zu sehen

6) Ebenso haben wir mit Befriedigung tod das Centralbureau erfahren, dass Herr
Professor Andonowits in Belgrad das Königreich Serbien, welches ein geodätisches Institut
gegründet hat, bei der internationalen Erdmessung vertreten wird. Wir hoffen dass die

1Djese Formalität ist in der That seither erfüllt worden. x DR

 
