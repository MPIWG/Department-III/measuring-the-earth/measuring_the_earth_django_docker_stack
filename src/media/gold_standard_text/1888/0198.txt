 

 

Motion de M. Helmert au sujet de nouvelles
lignes de pesanteur dans le Tyrol
Rapport de MM. Tinter et Weiss sur les
travaux de calculs exécutés dans les der-
nières années par la Commission géodé-
sique autrichienne [voir Annexe IVC)
Rapport de MM. Bakhuyzen et Schols sur
les travaux des Pays-Bas (v. Annexe V).
Rapport de M. d’Avila sur les travaux exé-
tes en Portugal (voir Annexe VI)
Rapport de M. Helmert sur les opérations
de l’Institut géodésique prussien (voir
Annexe VII) i
Communication de M. Foerster relative aux
resultats obtenus par M. Mylius pour
ameliorer les niveaux par une composi-
tion de verre moins riche en alcalis .
Sur la proposition de son bureau, la Con-
férence choisit, a l’unanimite, Paris com-
me lieu de réunion de la Conference
generale de 1889... ... :
M. Bassot est nommé rapporteur sur les
esures de bases, à la place de feu le
général Perrier

Quatriéme séance, 23 septembre 1888.

Le proces-verbal de la troisiéme séance est
lu et adopte oe

Le Secrétaire fait lecture d’une dépéche
télégraphique de Mme veuve v. Oppolzer.

Proposition du Bureau concernant le choix
du zéro international pour les altitudes en
Europe oo eee ae

Discussion sur cette proposition, a panels
prennent part MM. Lallemand, Hirsch,
Bouquet de la Grye,
et Foerster

Ferrero, Faye

102

 

Pag.

33

34

35

36

 

La proposition du Bureau est votée a |’ una-
nimité par la Commission permanente .
Rapport de M. Capitaneanu sur les tra-
vaux exécutés en Roumanie oe An-
nexe VID 2. = ee 2
Rapport de M. le Ga! Sohneiber sur les
travaux accomplis par la section trigono-
métrique de ee) prussien {voir
Annexe VD), 1° :
Rapport de M. Hirsch sur ie tira de
la Commission géodésique suisse we
Annexe X)... _ ee ;
Rapport de M. Ibanez sur i travaux exé-
cutés en Espagne {Voir Annexe |
M. le baron T'ejfé fait espérer l'entrée pro-
chaine du Brésil dans l'Association in-
ternationale ; il dépose sur le bureau de
la Conférence des cartes brésiliennes,
ainsi que deux publications sur des deter-
minations de longitude et l’observation
du passage de Venus en 1882; il rend
compte des travaux géodésiques exécu-
tesausbeesl 2x 2, .
M. Foerster lit un second ep. de la
Commission des finances
La Commission permauente adopte ie ae
positions contenues dans le rapport pré-
cédent >. =, : - s
Programme des sein: de Bureau cen-
tral pour 1888-89, depose par M. Helmert.
MM. Nagel, Backhuysen, Feerster, Ibanez
et Ferrero sont désignés par le sort
comme membres sortants de la Commis-
sion permanente en 1889 :
Remerciements adresses, au nom de Fa

semblée, par M. le Président au repré

sentant de l'Empire et aux autorités de
Salzbourg pour Paimable accueil qu'ils
ont accordé a la Conférence

Pag.

41

4A

He
no

43

 

 

 
