Tr

A
L

les

IN k |

i
|
à
|
%
i

5
E
q

 

Se

iy |

 

DEUXIEME SEANCE
19 septembre 1888.

La séance est ouverte à 2 heures 15 minutes.
Présidence de M. le général /bañez.
Sont présents :

1. Les membres de la Commission permanente : MM. van de Sande Bakhuyzen, Faye,
Ferrero, Forster, Helmert, Hirsch, von Kulmar, Nagel.

Il. Les délégués : MM. d’Arila, Bassot, Bouquet de la Grye, Capitaneanu, Diaz
Covarrubias, Derrécagaix, Hartl, Karlinski, Lallemand, Rümker, Schols, Schreiber, Tinter,
Tisserand, Weiss.

II. Les invités: MM. Bischoffsheim, Perrotin.

M. le Président souhaite la bienvenue aux membres de la Conférence récemment
arrivés, MM. Bischoffsheim, Diaz Covarrubias et le général Schreiber, qui assistent a la
séance de ce jour.

En ce qui concerne la langue dans laquelle les procès-verbaux devront être rédigés,
M. le Président pense que la Conférence sera d'accord que, pour suivre au mode usité jus-
qu'ici, ces documents devront être écrits, d'une séance à l’autre, dans la langue du pays
dans lequel se tient la session, car il est matériellement impossible au Secrétaire de les ré-
diger, dans l’espace d’un jour, en allemand et en français. Toutefois, le Secrétaire est prêt à
donner de vive voix un résumé en français du procès-verbal rédigé en allemand. [1 va de soi
que les Comptes-rendus paraîtront plus tard au complet dans les deux langues.

Aucune observation n'étant faite, le Président déelare que cette manière de procéder
est approuvée.

M. le Président donne la parole au Secrétaire pour la lecture du procès-verbal de la
première séance.

Le Secrélaire annonce d’abord à la Conférence qu’il a reçu une lettre hier de M. le
major Hennequin, de Bruxelles, et pendant la matinée un télégramme de M. le professeur
