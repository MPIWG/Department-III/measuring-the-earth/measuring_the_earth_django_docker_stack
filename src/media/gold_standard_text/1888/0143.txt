 

Dey
Vive.
ors.
sagen
cken
1, I:
(Jes

RE ER RS

 

Annex N» IVe.

BERICHT

des Professor Dr. Wilhelm Tinter über die im Jahre 1558 ausgeführten
Rechnunesarbeiten. |

Stationen Krakau und Jauerling.

Die definitiven Rechnungen zur Ermittlung der Polhöhe und des Azimuthes auf den
genannten beiden Stationen sind nunmehr zu Ende geführt.

A. Station Krakau.

1. Die Bestimmung der Polhöhe des Aufstellungspunktes des Universal-Instrumentes
aus gemessenen Zenithdistanzen der nachstehenden Sterne hat folgende wegen der Biegung des
Fernrohres schon verbesserte Werte ergeben :

| Polaris ji Reihe ©. 0. o à ur p= 90
| > 22 Reikteig UC. 52,118 87
% Ursae minoris 52,620 94
6 Cepher 92.109 99
a Coronae 082,124 28
x Boolis 92,701 11
2 Ophiuchi | 59,050 oA
a Orionis 02,28 vo
a Canis minoris or = S08 O20 AO

 

Hieraus folet : Polhöhe des Aufstellungspunktes des Universal-Instrumentes aus 962
Doppelbeobachtungen von Zenithdistanzen nördlicher und südlicher Sterne

Oo /

9 = 50 3 59,346 + 0,064

2. Aus den beobachteten Sterndurchgängen im I. Vertical sind für die Polhöhe des
Aufstellungspunktes des Passage-Instrumentes folgende Werte erhalten worden !

 
