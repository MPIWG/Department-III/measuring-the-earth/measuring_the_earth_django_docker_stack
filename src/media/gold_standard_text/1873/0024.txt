 

 

wegen der genannten Baulichkeiten die uns ausgesetzten Geldmittel in unerwarteter

Weise aufgezehrt worden waren.
Sadebeck.

4. Bericht des Herrn Professor Dr. Bremiker.

Als Fortsetzung der Rheindreiecke, wovon der Theil zwischen Roermonde und
Löwenburg bereits ausgeglichen und in dem General- Bericht pro 1871 abgedruckt ist,
kann das Netz über Baden bis an die Schweiz angesehen werden. Die Beobachtungs-
pfeiler, so weit sie im Lande liegen, sind von Baden gebaut, die Winkelmessung ist
dagegen von der Grossherzoglichen Regierung dem geodätischen Institut, und von letz-
terem dem Unterzeichneten übertragen. Der Sommer 1873 war dazu bestimmt, vorzugs-
weise auf Badischen Punkten die Winkelmessung auszuführen. Zu dem Ende begab. ich
mich im Juni zuerst nach Mannheim, wo der auf der Gallerie der Sternwarte errichtete
Beobachtungspfeiler, welcher von Herrn Dr. Albrecht zur Messung des Azimuths und
der Polhöhe benutzt ist, zugleich als Dreieckspunkt dient. Die Beobachtungen konnten
hier, obgleich vielfach durch Rheinnebel verhindert, im Laufe des Juni Ausgeführt werden.
Der nächste Punkt war Durlach. Auch hier waren, da der Dreieckspunkt ebenfalls
nur eine geringe Erhöhung über der Rheinebene hat, die Linien Strassburg und Mann-
heim öfter durch Nebel gestört, so dass der ganze Monat Juli für die Beobachtungen
nöthig war. In den ersten Tagen des August konnten auf dem Punkte Königsstuhl bei
Heidelberg die nöthigen Einrichtungen für die Winkelmessung getroffen werden. Das
Wetter war indess im Laufe dieses Monats den Beobachtungen wenig günstig, so dass
ein vollständiger Abschluss nicht erreicht ist. Die Fortsetzung dieser Arbeiten sowohl
hier als auf den folgenden Punkten in Baden und dessen Nachbarschaft, namentlich auf
Melibocus, Katzenbuckel, Calmit, Hornisgrinde etc., ist fiir den nächsten Sommer in Aus-
sicht genommen.

Als Winterarbeit wurde neben den Stationsausgleichungen auch eine Netzaus-
gleichung von der Seite Löwenburg—Nürburg des früher ausgeglichenen und im General-
Bericht pro 1871 mitgetheilten Netzes bis zur Seite Feldberg — Donnersberg vorge-
nommen. Diese Ausgleichung kann aber erst dann als definitiv angesehen werden, wenn
noch einige Dreiecke, die damit im Zusammenhange stehen und deren Beobachtung noch
rückständig ist, darin aufgenommen sind, weshalb die Mittheilung vorläufig unterbleiben
muss. Dagegen können die aus diesen Rechnungen hervorgegangenen geographischen
Positionen, da sie kaum eine Aenderung erleiden werden, als definitive gelten. Diese
Positionen sind von Bonn aus gerechnet, mit der dort stattfindenden Polhöhe und dem

astronomischen Azimuth, und ihre Vergleichung mit den astronomischen Bestimmungen

giebt folgende Zusammenstellung.

 

|
|
4
|
