Cm rh mr

TE PPT TRITT mn NT

ton ppt

i
a3
1]
z=
ik
if
ee

R
13

=

:

r

è

General-Bericht

über die
Europäische eradmessung
fiir das Jahr 1873.

Zusammengestellt im Centralbürean.

iD ade m

Von den Beobachtungspfeilern der in Baden liegenden Dreieckspunkte der
Europäischen Gradmessung war noch der auf Königstuhl unvollendet.. Um der Winkel-
messung, welche auf den Wunsch der Grossherzoglichen Regierung vom geodätischen
Institut ausgeführt wird, ungestörten Fortgang zu geben, ist im Frühjahr dieses Jahres
von Herrn Professor Jordan, welcher den Pfeilerbau auf allen. Punkten. geleitet hat,
auch dieser Punkt ausgebaut. Die Winkelmessung ist auf einigen Punkten bereits
ausgeführt, worüber unter „Preussen“ im Zusammenhange mit den Winkelmessungen
auf benachbarten Punkten berichtet wird.

Bremiker.

Zadar ern,

Bericht über die von der Königl. Bayerischen Kommission für die Europäische

Gradmessung im Jahre 1873 ausgeführten Arbeiten.

Die geodätischen Arbeiten bestanden zunächst in der Berechnung und
Zusammenstellung der im vorausgegangenen Jahre 1872 für das Präeisionsnivellement
gemachten Beobachtungen, welche die Strecken München — Grafing — Rosenheim und
Mtinchen—Landshut—Regensburg in einer Länge von 218 Kilometer umfassten. An

General-Bericht f. 1874. :

 
