 

aus dem Bediirfniss der topographischen Aufnahmen, entstanden sind. Zum Theil noch
nach den zwanziger Jahren stammend, sind sie nach alten Methoden berechnet, die
einzelnen Gruppen sind oft schlecht mit einander verbunden, nie mit einander ausge-
glichen. Wir haben angefangen mit den westlich von der Breitengradmessung bis zur
preussischen Grenze gelegenen Triangulationen, und haben bis jetzt die in den Gou-
vernements Wilna, Kowno und Kurland liegenden Ketten durchgerechnet. — Die Rech-
nung hat dargethan, dass diese, noch von General Tenner ausgeführte, Triangulation
eine für Gradmessungszwecke genügende Genauigkeit besitzt, und wenn an geeigneten
Punkten astronomische Bestimmungen hinzugefügt würden, einen recht brauchbaren
Beitrag zur Europäischen Gradmessung liefern könnte.

Das Präcisionsnivellement zwischen Petersburg und Moskau, das im vorigen
Jahre begonnen wurde, soll in diesem beendigt werden, so dass die ganze Strecke
doppelt, das heisst einmal hin, einmal zurück, nivellirt sein wird. — Im nächsten Jahre
denken wir es weiter nach Süden fortzusetzen.

Herr v. Séruve hat mir aufgetragen, folgende Mittheilung über Pendelbeobach-
tungen zu machen:

Der von den Herren Repsold 1863. für die kaiserliche Akademie der Wissen-
schaften in St. Petersburg gearbeitete Pendelapparat wurde von Herrn Prof. Sawitsch
benutzt, um in den Jahren 1865—68 auf 12 Hauptpunkten der russischen Breitengrad-
messung die Länge des einfachen Secundenpendels zu bestimmen. Auf den Wunsch von
Colonel Walker, Superintendent of the Great Triangulation of India, wurde dieser Apparat

1869 nach Indien gesandt, wo derselbe auf verschiedenen Punkten, gleichzeitig mit einem.
, 8

für die indische Vermessung in England gearbeiteten Apparate, beobachtet wurde.
Diese Operationen, durch den leider in Verfolgung derselben den Anstrengungen er-
legenen Capitain Basevi begonnen, sind im vergangenen Jahre durch Capitain Heaviside
zu Ende geführt, welcher darauf beide Apparate nach Europa gebracht hat. Leider
waren auf dem ersten Transporte des russischen Apparats nach Indien die Schneiden
stark gerostet und wurden bei ihrer Ankunft daselbst als nicht genügend rectificirt be-
funden. Da aus diesem Grunde der ganze Apparat auseinander genommen und von
neuem berichtigt werden musste, waren die in Russland gewonnenen Resultate nicht
mehr unmittelbar mit den indischen vergleichbar. Um nun diesem Uebelstande abzuhelfen,
hat Capitain Heaviside im Frühling dieses Jahres beide Apparate gleichzeitig in Kew
beobachtet und auch daselbst das von Kater 1818 benutzte Reversionspendel mit hin-
zugezogen, für welches neuerdings durch Colonel Olarke die Abstände der Schneiden aufs
schärfste bestimmt sind. Unmittelbar nach Beendigung seiner Beobachtungsreise hat
Capitain Heaviside den russischen Apparat, aufs sorgfältigste verpackt, nach Pulkowa
gesandt, wo sogleich eine neue Reihe von Versuchen durch Obristlieutenant Zinger
ausgeführt ist. Auf solche Weise wurden die russischen Bestimmungen unmittelbar für
die indischen und englischen vergleichbar, unabhängig von den absoluten Bestimmungen,
welche für jeden der getrennten Apparate ausführbar und ausgeführt sind, welche
aber offenbar erst durch Umwege und cömplieirtere Operationen mit geringerer

 

Ha hs A | Mn |

de sah

ine he ha 1e D ha a um ue

 
