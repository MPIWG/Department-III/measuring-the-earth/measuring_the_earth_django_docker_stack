 

 

-

Stk
welche nicht Gegenstände des in der ersten Plenarsitzung beschlossenen
auch mit diesem nicht im Zusammenhange stehen, sowie etwaige
bringende Mittheilungen solcher Art sind vorher bei
dem Bureau einzureichen. Dasselbe entscheidet über deren Zulässigkeit in der laufenden
Sitzungsperiode. Bezüglich solcher Anträge und Mittheilungen kann jederzeit der Antrag
auf Uebergang zum Programm für die laufende Sitzungsperiode gestellt werden.

58:
Bei Eröffnung jeder Plenarsitzung der Conferenz bringt das Bureau die in-
zwischen überreichten Vorlagen, welche sich auf die Sache beziehen, zur Kenntniss der
Versammlung. Dergleichen Vorlagen können auf Beschluss der Versammlung, wie auch

n dem gedruckten Rechenschaftsberichte mehr oder weniger vollständig
schliesslich dem Archiv

Anträge,
Programms betrefien,
schriftliche vor die Conferenz zu

des Bureaus i
erwähnt, oder ganz in denselben aufgenommen werden. Sie sind

der Europäischen Gradmessung einzuverleiben.

8

Die Redaction der Verhandlungen der Conferenz übernimmt die permanente

Commission und sorgt für den Druck und die Vertheilung.

8.10.
Die Wahlen für die ausscheidenden Mitglieder aus der permanenten Commission
der letzten Plenarsitzungen als erster Gegenstand der

werden von dem Bureau in einer
t hierauf in der vorhergehenden Sitzung aufmerk-

Tagesordnung vorgenommen, jedoch is
sam zu machen.

Herr von Bauernfeind: Wir haben zunächst den 8. 1 der Geschäftsordnung in

Ausführung zu bringen und ich frage an, ob Jemand aus der Versammlung das Wort

wünscht?
Herr Hügel: Meine Herren, gestatten Sie, dass ich das Wort ergreife, um Ihnen

den Vorschlag zu machen, den Begründer unseres Unternehmens, Herrn Dr. Baeyer,
welchem zu unser Freude es vergönnt gewesen, allen unseren allgemeinen: Conferenzen
beizuwohnen, durch Acclamation zu unserm Ehrenpräsidenten zu ernennen.

Der Vorschlag wird einstimmig angenommen.

Herr Baeyer: Meine Herren, ich danke Ihnen für die Ehre, die Sie mir wieder
erwiesen haben. Bei unseren früheren Versammlungen ist es Gebrauch gewesen, dass
einer der Herren Commissare, welcher dem Staate angehörte, in welchem wir unsere
Conferenz abhielten, präsidirte und erlaube ich mir, Ihnen Herrn Director Bruhns zum
Präsidenten vorzuschlagen.

Herr Bruhns : Ew. Excellenz danke ich fiir das
Sie mir soeben durch den Vorschlag geschenkt haben, aber im Interesse der
ich zu bitten, mir, da ich bisher allen Conferenzen als Secretär genützt habe, auch

persönliche Wohlwollen, welches
Sache wage

 

 

she had
