 

18

thatsächlich festzustellen, den die Lothablenkungen im Harz auf das Nivellement aus-
geübt haben.
Die Länge der seit 1871 nivellirten Strecken beträgt etwa . 750 Kılom.
Di dam ren a wellire 3. eat Ph ne 1800: .£,
Gesammtlänge des Nivellements . ... : . . . . . 2550 Kilom.

E. Maassvergleichungen.

In dem Bericht den das Centralbureau in der dritten allgemeinen Conferenz 1871
in Wien erstattete, ist das Unzureichende des engen Miethlokals, wo der Steinheil’sche
Fühlspiegel-Comparator in Ermangelung eines besseren aufgestellt werden musste, hin-
reichend hervorgehoben werden, woraus denn auch folgt, dass alle ausgeführten Ver-
gleichungen nur den Charakter von Vorversuchen haben können.

Die Genauigkeit im Princip des Apparates zur Vergleichung von Eindmaass-
stäben hat sich fortgesetzt bewährt, obgleich einzelne Vorrichtungen dabei erheblicher
Verbesserungen fähig sind. Anders verhält es sich aber hinsichtlich der Massnahmen
zur Bestimmung der absoluten Ausdehnungen, und namentlich hat die Gummi-Dichtung,
welche den Zwischenraum um die im Erdboden fundamentirten Fixpunkte (die durch
den Boden des Troges hindurch gehen) gegen das Durchdringen der Flüssigkeit
im Troge schützt, sich völlig unzureichend erwiesen: weniger in Bezug auf die Dichtung
selbst, als vielmehr in Bezug auf die Unveränderlichkeit der Fixpunkte. Es hat sich
nämlich herausgestellt, dafs die Fixpunkte bei den verschiedenen Temperaturen dem
elastischen Zuge des Gummis keinen ausreichenden Widerstand geleistet haben.

Um der Conferenz ein übersichtliches Bild von dem Gange der Beobachtungen
zu geben, übergehe ich die einzelnen Vorversuche und die mannigfachen kleinen
Schwierigkeiten nebst deren Beseitigung, wie sie sich bei jedem neuen Apparat vor-

- finden und wende mich gleich zu der Darstellung des Hauptversuches, den ich Anfangs

1872 angeordnet hatte.

Die allgemeine Einrichtung des Comparators ist im Generalbericht pro 1869
von Steinheil selbst beschrieben. Zum Apparat gehören eine Anzahl Vorlege-Üylinder
von Glas, deren Länge so gewählt ist, dass 38 gleich einer Toise und 39 gleich zwei
Meter sind. Am genauesten entsprach der Vorlage-Cylinder No. III der Ausgleichung
der Toise mit dem Meter, so dass (2 M—IIl) sehr nahe gleich der Toise ist. Die
beiden Meter, die angewendet wurden, sind von Steinheil angefertigt und von Glas,
und zwar von demselben Glase als die Vorlege- Cylinder.

Folgende Stäbe kamen der Reihe nach zur Vergleichung
Doppelmeter 2 M—II.
Toise Bessel

5, SNOW 119)
Unie RON
» Lenoir.

ea da RER

ablenken

is sous ae ia Mi a ans

 
