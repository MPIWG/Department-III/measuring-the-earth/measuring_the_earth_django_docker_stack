 

118
Sternes, die Unvollkommenheit der Mikrometerapparate , die Theilungsfehler der Kreise
und die ungenaue Kenntniss des Refractionsbetrages hinzukämen. Letzterer Umstand
sei als eine Hauptunsicherheit des gegenwärtigen Verfahrens zur Bestimmung der Decli-
nationen zu betrachten und wäre diesem Uebelstande nur dadurch vorzubeugen, dass
man sich bei der Beobachtung am Meridiankreise auf Zenithdistanzen von 0-50),
innerhalb deren die Refraction nur kleine Beträge erreicht, beschränke. Er schlägt vor,
dass seitens zweier Beobachter mittelst Meridianinstrumenten von mittlerer Grösse an
zwei Stationen resp. im 60° nördlicher Breite und wenig nördlich vom Aequator Decli-
nationsbestimmungen aller Sterne bis zu 30% Zenithdistanz ausgeführt werden und seien
alsdann die gemeinschaftlich beobachteten Sterne mit grossem Vortheil zur Ermittlung
des Breitenunterschiedes der "beiden Stationen zu verwenden. Auch empfiehlt derselbe
vor allem die Methode der Azimuthbeobachtungen in der Nähe der grössten Digression,
weil man auf diesem Wege die Declinationen frei von den Unsicherheiten der Refraction
und der Biegung erhalte.

Herr Faye unterstiitzt diesen Vorschlag, discutirt den Einfluss der Refraction
auf die Declinationsbestimmungen und gelangt zu dem Resultat, dass die Hauptunsicher-
heit des Refractionsbetrages auf die Schwierigkeit der 'Temperaturbestimmung der Luft
zurückzuführen sei. |

Die Herren Hirsch und Oppolzer weisen auf die Schwierigkeit der Ausführung
dieses Vorschlages und besonders auch darauf hin, dass die Bestimmung der Declina-
tionen der Sterne nicht eigentlich in das Gebiet der Gradmessung gehöre, um so mehr,
da die einzelnen Methoden zur Längen-, Breiten- und Azimuthbestimmung in vielen
Fällen eine directe Elimination der Rectascensions- und Declinationsunsicherheiten zu-
liessen. Die Versammlung beschliesst auf Antrag des Herrn Hürsch, Herrn Villarceau
für Vorlegung und Ueberlassung des Memoire zu danken ‚und dasselbe im Bericht über
die vierte Generalconferenz mit abzudrucken, um es auf diesem Wege zur Kenntniss
derjenigen Astronomen zu bringen, welche sich mit der Bestimmung der Declinationen
der Fundamentalsterne beschäftigen.

Herr Bruhns empfiehlt noch, bei den Publieationen jedesmal eine Angabe über
die bei der Rechnung in Anwendung gekommenen’ Deeclinationen zu machen und wo-
möglich auch die numerischen Werthe der Differentialquotienten hinzuzufügen.

Die Versammlung geht nunmehr auf die Berathung des dritten Punktes des
Programms über und beschliesst zum Zwecke der Beantwortung der Frage: Welche
Pendelapparate sind für die Bestimmung recht vieler Punkte die vortheilhaftesten?

eine besondere Commission niederzusetzen, welche unter dem
Vorsitz des Herrn Baeyer aus den Herren Bruhns, Hirsch, Oppol-
zer, Peters und Albrecht bestehen soll.

In Bezug auf den zweiten Punkt: Die Ausführung, von Pendelbeobachtungen im
Gotthardtunnel und auf der Höhe des Gebirges erkennt die Versammlung den hohen
Werth einer solchen Untersuchung an und spricht einstimmig den Wunsch aus, dass
dieselbe seiner Zeit zur Ausführung gebracht werden möchte.- Herr Hirsch hatte zuvor

=

 

4
i
i
d
À

 
