PT PPT PT PONT PTT re UN

a

compensare il tratto che copre la Basilicata, e questo lavoro, compiuto nell’ anno cor-
rente, verra a suo tempo consegnato all’ Ufficio centrale.
Della rete parallela tra l’Albania e Visola di Ponza, al principio dell’ anno

-corrente. mancayano ancora diverse stazioni le quali poi vennero eseguite, ad eccezione

di poche, cui si sta lavorando.

Del resto, esaminando l’annesso graficio (vedi Carta 1), si vede lo stato dei
nostri lavori nelle provincie meridionali.

Si scorge da esso che la rete meridiana, la quale dall’ isola di Ponza, per
Roma, dovrebbe proseguire verso Rimini, é€ stata anch’ essa oggetto di studio, essen-
dosi infatti in quest’ anno compiuta la riconoscenza per riattaccare la rete dei dintorni
di Roma con quella delle provincie meridionali.

b) Base di Udine.

Nel programma dei nostri lavori (vedi General-Bericht 1873, pag. 5) era
compresa la misura di una base nelle vicinanze di Udine, operazione da eseguirsi di
conserva con Ufficiali dell’ I. R. Istituto geografico di Vienna, impiegando i due rispet-
tivi apparati di misura.

Questa operazione venne eseguita nel mese di Maggio e nei primi giorni di
Giugno.

La lunghezza dell’ intera base e di circa 1667 tese; fu misurata due volte,
dividendola in cinque tratti di lunghezza tra loro pressoché uguale.

Non & ancora ultimato il calcolo della base di cui si tratta, ma la seguente
tabella puod essere utile ad apprezzare la precisione della misura.

Tabella A.
1874.
Lunghezza della base di Udine
In tese: In 2m 6 tir:

1° Quinto Andata 332.461317 } à. 1° Quinto Andata 647,979178] ..
Ritorno 3321460824 | Üfferenza 0,000995 Ritorno 647,977243 differenza 0,001935

Medio 332,460821 - Medio 647,978210
9° Quinto Andata | id 0,000825 2° Quinto Andata ee id.

 

 

 

 

 

Ritorno 332,448108 Bitorno 647,953433 0,001609
Medio 332,448521 Medio 647,954237
3° Quinto Andata 332,505954 : 3° Quinto Andata 648,066177)  :
: Ritomo 8820066101 7... O00 Ritorno 6481067846, Mh: 1u0M04669
Medio 332,506382 Medio 648,067011
4° Quinto Andata 332,435466/ . 4° Quinto Andata 647,928794/ .
Ritorno et a Ritorno, 647,927940) 1, = 2000804
Medio 332,435247 Medio 647,928367
5° Quinto Andata 336,908500 . 5° Quinto Andata 656,646900 à
? Ritorno ea ide UNE Ritorno 656.649655| id: 0002756

Medio 336,909207 Medio 656,648277

 
