 

en ER

Fra

RR

IR ee

SE EEE

ns:

ee RD ACCES OEE OTE

ee ee

RE TS

SR
ee

10

3. Ueber die Beobachtungen zur Bestimmung der Intensität der Schwere.

Welche Pendelapparate sind für die Bestimmung recht vieler Punkte die
vortheilhaftesten ?

Auf Wunsch des Herrn Hirsch wird der Conferenz vorgeschlagen, im
Interesse der Wissenschaft den Wunsch auszusprechen, dass die Durchbohrung
der Alpen benutzt werde, um durch Pendelbeobachtungen, welche an geeigneten
Punkten im Innern des Gotthard-Tunnels und auf der Höhe des Gebirges an-
gestellt würden, eine Neubestimmung der Erddichtigkeit zu erhalten.

4. Ueber Maassvergleichungen.

Auf den Wunsch der Herren Ibavez und Hirsch schlagt das permanente
Comité folgende Resolution vor:

Die Gradmessungsconferenz möge den Wunsch wiederholt aussprechen, dass
die Herstellung des internationalen Meterprototyps soviel als möglich beschleu-
nigt und zu diesem Zwecke die vorgeschlagene und von den meisten Staaten
angenommene diplomatische Conferenz ohne Verzug einberufen werde, damit
dieselbe diesem wichtigen wissenschaftlichen Unternehmen die nöthige inter-
nationale Organisation sichere.

5. Ueber Messung von Grundlinien.
Welches ist die beste Construction eines Basisapparates und wie ist selbiger

zu beschaffen ?

6. Ueber geodätische Richtungsbeobachtungen.

Herr Perrier stellt die Frage: ob es vortheilhaft ist, bewegliche Fäden
anzuwenden?

Herr Villarceau: ob Tag- oder Nachtbeobachtungen vortheilhafter ?

7. Ueber die Praecisions- Nivellements.

Die Herren Baeyer und Bruhns stellen die Frage: auf welche Weise ist die
mittlere Höhe der Meere am schnellsten und zuverlässigsten zu bestimmen und
welche registrirende Pegel sind zu empfehlen? Ist die Festsetzung eines inter-
nationalen Nullpunktes schon möglich? Hat jedes Land einige unzerstörbare
und dauerhaft versicherte Niveaumarken? Die Angabe derselben ist erwünscht.

8. Ueber die Publication der Gradmessungsarbeiten.

Herr Bruhns fragt: Ist eine Zusammenstellung der Literatur aller Grad-
messungsarbeiten wünschenswerth? Ist eine Zusammenstellung der fertigen
astronomischen, geodätischen und nivellitischen Coordinaten wünschenswerth und
in welchem Umfange?

Herr Bruhns: Es dürfte wohl ebenso wie früher am einfachsten sein, wenn sich
zwei Sectionen bildeten, eine für die astronomischen Fragen (Frage 1 bis 3), eine für
die geodätischen Fragen (4 bis 7), während Frage 8 in einer Plenarsitzung direct ver-
handelt werden kaun. \

cou ah i i ish he a a tae

 

 
