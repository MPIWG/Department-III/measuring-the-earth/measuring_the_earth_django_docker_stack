 

 

 

nn.

Herr Ganahl: Zuerst will ich die Bemerkung vorausschicken, dass die Thätigkeit
des militär-geographischen Instituts sich über Oesterreich-Ungarn erstreckt. Die Mittel
werden von beiden Delegationen bewilligt, da das Institut ein gemeinsames und nicht
ein speciell österreichisches ist.

Von den von Seite des militär-geographischen Institutes in Wien für Zwecke
der Gradmessung im Sommer 1874 ausgeführten astronomisch-geodätischen und nivelli-
tischen Arbeiten bespreche ich zuerst die astronomischen Arbeiten.

Auf dem Triangulationspunkt 1. Ordnung Haunsberg, 2 Stunden nördlich von
Salzburg, und auf dem westlichen Basis-Endpunkt der Radautzer Grundlinie wurde die
Polhöhe und das Azimuth gemessen. Erstere sowohl aus Zenithdistanzen nördlicher
und südlicher Sterne, sowie aus Sterndurchgängen im ersten Vertical, letzteres aus
Horizontaldistanzen des Polarsternes. Auf Haunsberg wurde auch zur Polhöhenbe-
stimmung die Methode der gleichen Höhen angewandt.

Die hierzu benutzten Instrumente waren das 13“ Universale mit gebrochenem
Fernrohr und das 10“ Universale mit dem Fernrohr in der Achse, dann zwei Passagen-
rohre, eines mit 30, das zweite mit 21‘ Oeffnung. Alle vier Instrumente sind in
früheren Jahren schon verwendet und beschrieben worden.

Ueber die geodätischen Arbeiten habe ich zu bemerken, dass zur Vergleichung
des österreichischen Basis-Apparates mit dem italienischen gemeinschaftlich von Italien
und Oesterreich eine Grundlinie nächst Udine gemessen worden ist, in ähnlicher Weise
wie im Jahre 1872 bei Grossenhain ein Theil der dort mit dem Bessel’schen Apparat
gemessenenen Grundlinien auch mit dem österreichischen Apparat gemessen wurde.

Die Resultate sind aus provisorischer Rechnung folgende:

Italien bekömmt für die Länge der Grundlinien
Hinmessung 16667 76017
Riickmessung 1666. 76018
Oesterreich Hinmessung 16667 75288
is Riickmessung 1666. 75463
daher Oesterreich die Grundlinie um OT 00642 oder 5U547 kleiner findet.

Noch unbedeutender waren die Differenzen beim Vergleiche mit dem Bessel’-
schen Apparat im Jahre 1872. Bevor jedoch diese Resultate endgiltig festgestellt wer-
(den können, ist eine neue Bestimmung der Ausdehnungs-Coefficienten unserer Messstangen
nöthig; der hierzu erforderliche Apparat dürfte im nächsten Monat fertig und diese
Arbeit dann vorgenommen werden.

Eine zweite Basis wird so eben bei Radautz in der beiläufigen Länge von 2000
Klaftern gemessen. Es wird hiezu der westliche Endpunkt der alten 4000 Klafter langen
Grundlinie, welche im Jahre 1818 jedoch nur einmal gemessen wurde, benutzt; dann
zunächst der alten Basismitte, von welcher leider der Markstein nicht mehr aufzufinden
war, der zweite Endpunkt gewählt. Die alte Basis selbst noch einmal zu messen, ist
aus dem Grunde nicht möglich gewesen, weil in der Linie mittlerweile Gebäude errichtet
worden sind.

 

Didi ul Lilas lh ar

 
