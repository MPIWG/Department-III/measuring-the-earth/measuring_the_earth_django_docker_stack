DUR mes ee

“tel ot - eT TNT TYE |

TOW Ory

FT

DL TTT

R
=
8
æ
=
5

2 ER
diplomatische Conferenz ohne Verzug einberufen werden,
damit dieselbe diesem wichtigen wissenschaftlichen Unter-
nehmen die nöthige internationale Organisation sichere.

Die permanente Commission wird beauftragt, ein dahin gehen-
des Schreiben an die französische Regierung zu richten.

Dieser Antrag wird einstimmig zum Beschluss erhoben.

Präs. von Forsch:- Nach dem Berichte des Präsidenten des Centralbureaus fehlen
dem letzteren zu Massvergleichungen die Localitäten, ich frage daher, ob die Conferenz
folgenden Antrag annehmen will:

An die königlich Preussische Regierung ein Schreiben zu rich-
ten mit der Bitte, dem Generallieutenant Dr. Baeyer ein
besonderes für Massvergleichungen brauchbares Gebäude
zu gewähren.

Der Antrag wird einstimmig angenommen.

Präs. von Forsch: Ferner liegt zu Frage 5 der Antrag vor:

Eine Commission, bestehend aus den Herren von Forsch, Ganahl,
Ibanez, Perrier und de Vecchi dem Herrn Baeyer zur Seite zu
stellen, um die Frage über die beste Construction eines
Basisapparates zu studiren und recht bald der permanenten
Commission die erforderlichen Vorschläge vorzulegen.

Der Antrag wird angenommen.

Herr Bruhns: Als erster Punkt der Tagesordnung für die nächste Sitzung habe
ich die Wahlen in die permanente Commission zu verkünden; ferner wird Geschäft-
liches vorliegen, schliesslich die Vorschläge der astronomischen und geodätischen Section
und die Berathung über Frage 8 des Programms.

Präs. von Forsch: Die nächste Sitzung beraume ich auf Montag, den 28. Sep-
tember, um 10 Uhr an.

Schluss der Sitzung: 1 Uhr 15 Minuten.

12

 
