 

 

A.

Punkt 5: Basismessung.

Nachdem die Herren Ibanez, de Vecchi, Perrier, Villarceau einige allgemeine
Mittheilungen über die von ihnen benutzten Basisapparate gemacht, und der Herr Vor-
sitzende betont hatte, dass die Entscheidung der in 5. gestellten Frage nur auf experimen-
tellem Wege geschehen könne, legen die Herren Daeyer und Hirsch die Umstände dar,
welche zu der Aufstellung der Frage geführt haben. Es ist früher die Anschaffung eines
Basismessungsapparates beschlossen worden, welcher vom Centralbureau dazu benutzt
würde, die Grundlinien in den einzelnen Ländern nachzumessen und zugleich einzelnen
Ländern auf ihr Verlangen zum Behufe von Basismessungen zur Verfügung gestellt
werden könnte. Es handelt sich nun für den Herrn Präsidenten des Centralbureau

‘darum, die Ansichten einer Commission von Männern kennen zu lernen, welche sich

speciell mit Basismessungsapparaten beschäftigt haben.

Herr Bruhns schlägt vor, dass eine Commission gewählt werde, in welcher die
verschiedenen im Gebrauche befindlichen Basisapparate vertreten sein sollen, und welche
innerhalb einer gegebenen Zeit an die permanente Commission zu berichten hätte.

Herr Bruhns giebt zunächst näheren Aufschluss darüber, was zu der Aufstellung
der Frage (7) nach der besten Construction von selbstregistrirenden Pegeln Veranlassung
gegeben habe, und schlägt vor, dem Herrn General Baeyer eine ähnliche Commission
beizugeben, wie für die Ermittlung des besten Basismessungsapparates.

Herr Hirsch wünscht die Anzahl der Commissionen nicht zu vermehren und
schlägt vor, das Centralbureau zu beauftragen, dass es in denjenigen Ländern, wo
selbstregistrirende Pegel aufgestellt seien, die gemachten Erfahrungen sammeln und die
Resultate den Mitgliedern der Conferenz mittheilen möge.

Herr Bruhns ist hiermit einverstanden und die Section erhebt den Antrag des
Herrn Hirsch zum Beschluss.

Herr Hügel fragt an, ob von den Beobachtungen am selbstregistrirenden Pegel

zu Swinemünde schon Resultate vorliegen.

Herr Bruhns antwortet hierauf, dass die Ableitung der Mittel aus den vom
Apparate gelieferten Curven in thunlichster Bälde mittelst des Amsler’schen Planimeters
erfolgen werde.

Herr Hirsch macht noch auf die Wichtigkeit der Errichtung von selbst-
registrirenden Pegeln an den französischen Häfen des Mittelmeeres und des Oceanes
aufmerksam, worauf Herr Faye deren Errichtung in Aussicht stellt.

Eine längere zwischen den Herren Bauernfeind, Bruhns, Faye, Hirsch, Hügel,
Oppolzer geführte Debatte entspann sich über den weiteren Inhalt von Punkt 7.

Herr Bruhns schlug vor, dass diejenigen Staaten, welche Gelegenheit haben,
ihre Höhen auf ein Meer zu beziehen, dieses schon jetzt thun sollen, wogegen Herr
Bauernfeind auf das Gefährliche aufmerksam machte, jetzt die Höhen auf einen pro-
visorischen Meereshorizont zu beziehen und alsdann, wenn der internationale Nullpunkt
gewählt sein wird, die Höhen aufs Neue abzuändern. 2

 

;
i
4
4
i
4
a
3
i
