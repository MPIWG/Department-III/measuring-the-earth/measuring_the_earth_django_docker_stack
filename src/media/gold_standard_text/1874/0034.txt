0 en |

35

Nell’ intervallo totale di 27 giorni abbracciati dalle operazioni si pote ogni sera
comunicare col Sempione. In 23 di queste sere a noi riusci una sufficiente determina-
zione del tempo: ma nei giorni 25. e 30. Giugno, 2. 4. 8. 11. 12. e 16. Luglio il tempo
fu coperto al Sempione: per modo che il numero delle sere atte a fornire una longi-
tudine precisa al Sempione si riduce a 17. Con Neuchatel lo scambio dei segnali ebbe
luogo nove volte; in una di queste sere il tempo fu coperto a Neuchatel; in due altre
sere lo scambio ebbe luogo in un senso solo e non nel senso opposto. In tutto si hanno
sei sere intieramente buone e due altre imperfette, le quali perd alla prova diedero
risultati intieramente attendibih.

5° Risultati.

La tabella seguente contiene nelle colonne segnate L le differenze di longitudini
ottenute in eiascuna sera; nelle colonne segnate T i tempi impiegati dai segnali a per-
correre le linee. Le differenze di longitudine si riferiscono, per Milano e pel Sempione,
al punto doy’ era collocato lo strumento dei passaggi: per Neuchatel all’ Osservatorio.
Dapertutto & già messa in calcolo l’equazione personale.

 

 

 

Differenze  Milano—Sempione Differenze Milano —Neuchâtel

 

 

 

Data 1870 | L ro Data 1870 | L | 22
i{ | 1
Giugno 21. | 4™ 39s 255 | + 08 019 Giugno 23. \ gm 55s 930 + 0s 066
22. | 222 0.008 26. | 55.963
23: | 109 | 0.003 Luglio 3. | 56.066 | 0.081
26. | 136 0.027 5. | 56.081 | 0.107
27: | 348 0.020 6; | 56,983 | 0.070
28. || 298 |: 6.021 ai.
29. | 338 | 0.032 idl; : 86416 0.105
Luglio 1. | 285 | 0.012 19.1) 56.020 —
Q || D: | Ô © © © © © ——— N
e: | a ane Medio | 8" 56: 043 | 05 089
6. | 160 | 0.014 |
7. || 264 | 0.007 |
9. || 316 0.017
10. | 349 | 0016
13. | 259 | . 0.012 | |
18. | 4921 0.020 |
15. | 151 0.037 | |
Medio | 4" 395249 | Os 018 | |

I nostri amici di Svizzera hanno avuto la gentilezza di comunicare il risul-
tamento della determinazione Sempione- Neuchatel; onde si pud formare il quadro
seguente :

Milano—Sempione 4m 398 249 & 0% 015
Neuchâtel 4 16 843 - 0.021
Milano 8 56 043 + 0.020

 

Sempione
Neuchatel

 

 

Errore di chiusura 08 049 + O8 033.

 
