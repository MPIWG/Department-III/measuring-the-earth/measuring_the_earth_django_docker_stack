 

die hir A Mn

78.

this

anderer Dienstabhaltungen nur auf zwei Punkten die Richtungsbeobachtungen habe vor-  ;
nehmen können. Es sind dies die Station Hohburg zwischen dem Colmberg und Leipzig, |
und unser höchster Punkt in Sachsen, der Fichtelberg. Dieser war insofern als ein
schwieriger Punkt zu betrachten, weil er sehr oft in Nebel gehüllt ist, und ich musste
daher zu den Beobachtungen eine Zeit wählen, wo die Anzahl der Nebeltage die ge-
ringste war, den Monat August. Es ist mir gelungen, diesen Punkt in vier Wochen
zu erledigen, und ich versuchte dann noch nach einem Punkte in der Ebene, Röden
bei Zeitz, zu gehen, um insbesondere das grösste Dreieck, Fichtelberg—Colm—Röden,
welches wir in unserm sächsischen Netz haben und welches beziehentlich 13'/,, 11!
und 9 Meilen Seitenlänge hat, zum Schluss zu bringen. Leider ist mir dies nicht ge-
lungen, da die Jahreszeit für derartige Arbeiten im flachen Lande schon zu weit vor-
geschritten, indem die nöthige Ruhe der Bilder, die zur Beobachtung nothwendig ist,
nur kurze Zeit des Tages, etwa '/, Stunde, vorhanden war.

Ich habe daher die Station verlassen müssen, ohne die Beobachtungen vollenden
zu können und gedenke im nfichsten Frühjahre diese Arbeit in kurzer Zeit zum Ab-
schluss zu bringen. Von dem sächsischen Haupt- und Verbindungsnetz zwischen der
Basis mit den Hauptpunkten, deren Anzahl bekanntlich früher auf 33 angegeben worden
ist, bleiben dann noch 9 Punkte zu bestimmen, und werden, da diese 9 Punkte verhält-
nissmässig wenige Richtungen in sich enthalten, etwa zwei Sommer nothwendig sein,
um diese Beobachtungen zu vollenden, da, wie Sie bereits aus früheren Berichten
wissen, die Beobachtungen auf den Hauptpunkten von mir allein gemacht werden. Die
Berechnungen schreiten langsam vor, insbesondere wegen Mangels an Arbeitskräften.
In neuerer Zeit haben die Eisenbahnbauten so viel Kräfte in Anspruch genommen,
dass es sehr schwer war, entsprechende Assistenten zu bekommen. Ich hoffe trotz-
dem, dass ich mit der Publication der trigonometrischen Arbeiten mit dem nächsten

 

Jahre beginnen kann.

Was das Nivellement anbelangt, so hat selbiges im vorigen Jahre ebenfalls
wegen Mangels der nöthigen Assistenz ausgesetzt werden müssen. Im Laufe dieses
| Jahres sind die Arbeiten wieder aufgenommen worden und es ist insbesondere durch
die Königl. Wasserbaudirection zunächst gewünscht worden, ın Verbindung mit dem
i Gradmessungsnivellement, ein Elbnivellement ausgeführt zu erhalten vom Einfluss der
7 Elbe aus Böhmen bis zu ihrem Ausfluss aus Sachsen. Es sind daher am Ufer der
I Elbe von 500 zu 500 Meter grosse Steine eingemauert, in welche Metallbolzen mit
4 Kugelcalotten eingelassen wurden. Nach Beendigung des Nivellements werden diese
Bolzen durch Decksteine verwahrt werden. Wo derartige Steine nicht anzubringen
waren, wie z. B. hier in Dresden, sind Höhenmarken in derselben Distanz angebracht.
Ausserdem habe ich in der sächsisch - böhmischen Schweiz einen Anschluss an
Oesterreich herbeizuführen gesucht, indem ich von Königstein die Elbe hinauf über

unsere Grenze bis Bodenbach, von da über den hohen Schneeberg nach Rosenthal, der
- Schweizermühle durch den Bielagrund nach Königstein zurück habe nivelliren lassen.
Nach Beendigung dieses Polygons habe ich noch ein Polygon bei Leipzig zum Schluss

ocean un 1e du ahd a us nu

SSS SSS ae

 
