 

36

+

L’errore di chiusura dunque appena supera di una piccola quantità il suo
importare dedotto dagli errori probabili, e. quindi l’accordo delle tre determinazioni
indipendenti fra loro pud dirsi sufficiente. Compensando le tre differenze si trovano
1 valori definitivi |

M — 8. = 4” 398 ROUE
De N, —=4.:16. 8242 0: 016
M—N.=8 56. 062 5 0. 016

Nel far questa relazione sommaria, è mio dovere di esprimere quanto 10
sono grato al Sig! Üeloria, secondo astronomo di questo Osservatorio, pel valido
concorso prestatomi nelle osservazioni e nei calcoli; né devo pretermettere di no-
tare, che l’esperienza gia acquistata dai Signori Direttori Plantamour ed Hirsch in
questo genere di lavori, e la benevolenza con cui essi ci furono larghi dei loro consigli,
hanno essenzialmente contribuito a facilitare la buona riuscita delle operazioni anche
per la nostra parte.

Milano, Osservatorio di Brera, 17. Agosto 1874.
Il Direttore

(firmato) Schiaparelli.

Sulla latitudine della Stazione geodetica Barberini a Monte Mario.
Nota del Prof L. Respighi.

La stazione geodetica Barberini a Monte Mario trovasi in un parallelo distante
meno di 4 chilometri dagli Osservatorj del Collegio Romano e del val e percid
la sua latitudine- avrebbe potuto dedursi geodeticamente da quella gia determinata
per gli indicati Osservatorj, ritenendo perd le direzioni delle verticali in queste localita
non soggette ad alcuna irregolaritä od anomalia.

Percid la misura diretta della latitudine di Monte Mario doveva essere piuttosto
diretta allo scopo di verificare se esiste una qualche differenza fra la latitudine ricavata
dalle osservazioni astronomiche e quella dedotta geodeticamente dai paralleli di quei
due Osservatorj, anziché alla determinazione del valore assoluto di questo elemento.

Prescindendo anche da cause occulte, le quali avessero potuto produrre una
qualche anomalia od irregolaritä nella inclinazione relativa delle verticali nelle due
diverse localitä, si aveva ragione di sospettere che una qualche leggera deviazione
potesse verificarsi in causa della irregolarita dell’ andamento del suolo fra le indicate
stazioni: poiché mentre i due Osservatorj giacciono nella vallata del Tevere in un suolo
leggermente ondulato, la stazione di Monte Mario trovasi quasi sul ciglio meridionale di

una collina o montagna, che protendesi a notevole distanza verso settentrione per

un’ altezza di oltre 100 metri dal sottoposto piano.

de a. A

sesh 2 ah aa le tae

 

 
