 

264

Daraus ergibt sich zunächst für die persönliche Gleichung:
1905 Wien—Pola: p=  — 0.016

Die persönliche Gleichung war daher jedenfalls sehr klein. Für die Länge selbst
findet sich: m, 8
Wien—Pola: 9 58.369

Die Länge bezieht sich in Wien auf das Zentrum der grossen Kuppel der Stern-
warte, und in Pola auf den Meridiankreis der Marinesternwarte.

Zur Registrirung der Kontakte wurden Strichkontakte verwendet, und es zeigte sich
bei der Ablesung zuweilen ein bis auf ein Zehntel Sekunde ansteigender Unterschied
zwischen Anzug und Abfall.‘ Abgelesen und publiciert sind beide Momente: die Längen
wurden indess nur aus den Anzügen berechnet, die dem Karakter der Zeichen zu Folge
als die sicheren erscheinen. Uebrigens weichen, wie eine nachträgliche Berechnung zeigte,
die Resultate nur unerheblich von den angeführten ab, wenn man der Rechnung statt des
Anzuges den Abfall zu Grunde legt.

Das Detail dieser Längenbestimmung enthält der eben fertig gestellte XV. Band
der Publicationen des k.k. Gradmessungsbureau.

Zur Vervollständigung des von Wien ausgehenden Längennetzes wurde im vorigen
Jahre (1908) eine Längenbestimmung zwischen Wien und dem geodätischen Institute in
Potsdam ausgeführt. Bei derselben wurde das für die eben besprochenen drei Längenbe-
stimmungen ausgearbeitete Beobachtungsprogramın mit einigen geringfügigen Modificationen
beibehalten, von denen die wichtigste darin besteht, dass beim Beobachterwechsel auch die
Instrumente gewechselt wurden. Die Reduction der Beobachtungen konnte wegen mancherlei
Schwierigkeiten die sich dabei ergaben noch nicht vollständig durchgeführt werden, ist aber
bereits so weit gediehen, dass sie jedenfalls noch vor Jahresschluss beendet werden wird.

Im nächsten Frühjahre (1910) ist eine Längenverbindung von Athen mit Wien in
Aussicht genommen, und zwar eine direkte, oder wenn dies auf allzugrosse Schwierigkeiten
stossen sollte, eine indirekte mit einer Zwischenstation, vermuthlich Salonichi.

Ich benütze diese Gelegenheit, um im Anschlusse an die hier kurz skizzirten Arbeiten
des k.k. Gradmessungsbureau mit ein paar Worten eine Notiz von einem umfassenden Unter-
nehmen zu geben, welches während des Baues der Tauern-Bahn von der kaiserl. Akademie
der Wissenschaften durchgeführt wurde. Ausser geologischen Aufnahmen in den tieferen
Einschnitten und Tunnels wurden während des Baues systematische Temperatur-Beobachtungen
in allen Tunnels, namentlich in dem grossen Tauerntunnel vorgenommen, und auf der Sohle
des letzteren am Eingange, in der Mitte und am Ausgange Schwerebestimmungen aus-
geführt. Ueberdies sollen im kommenden Jahre auf dem Stocke des Sonnblickes, welchen
der Tunnel durchbricht, noch Schweremessungen in verschiedenen Höhen an geeigneten
Punkten vorgenommen und durch eine auf dem Gipfel in einer Seehöhe von rund 3000 m.
ergänzt werden. Die Resultate aller dieser Untersuchungen, die einen nicht unwichtigen
Beitrag zur Physik der Erde liefern dürften, werden in einem eigenen Bande der Denk-
schriften der Kaiserl, Akademie veröffentlicht werden.

 

 

 

TOILE Rem kd aii Uh

ren vied

a

ak ind

 

 
