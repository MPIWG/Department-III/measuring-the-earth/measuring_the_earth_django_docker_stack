249

the so-called instrumental equations, the results would have coincided within a much smaller
amount. This seems to be clearly indicated by the results of the other experiments repeated
then in many cloudy nights.

The minute details of all these experiments will perhaps be published hereafter, and
here I give only their outlines. I further may add that we are prepairing for executing
the actual determination of longitude between two spots far more remote from each other

T. NAKANO,

 
