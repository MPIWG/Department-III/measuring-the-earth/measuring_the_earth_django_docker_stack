2h ba RAT PESTE SN vum 1 TT |

175

nn

LIL T 21)

x
=
=
à

 

ANNEXE A. XVII.

SUISSE.

Rapport sur les travaux exécutés en Suisse depuis la Conférence
de Budapest.

PAR

M. R. GAUTIER.

PERSONNEL.

La Commission géodésique suisse a été très éprouvée pendant ces dernières années ;
elle à perdu deux de ses membres les plus actifs: M. le professeur J. J. ReBsrTeIN, le 14
mars 1907 et M. le professeur Max RosenMunr, le 18 août 1908. Le rapport de M. le
Secrétaire perpétuel a retracé les traits principaux de la carrière de ces deux hommes dévoués
et distingués. Leur mort a été très douloureusement ressentie en Suisse, et je tenais à rap-
peler, moi aussi, leur mémoire au début de mon rapport.

Pour combler, partiellement au moins, ces vides, M. le Lt. Colonel L. HE xp, directeur
du Service topographique fédéral est entré cette année dans notre Commission. Nous espérons
beaucoup de sa collaboration.

TRAVAUX GÉODÉSIQUES EN GÉNÉRAL.

Le rapport que j’ai eu l'honneur de présenter à la XVme Conférence de l’Association
géodésique internationale à Budapest rendait compte, d’une façon abrégée, des principaux
travaux exécutés en Suisse de 1903 à 1906. Ce rapport-ci exposera brièvement les résultats
des travaux des années de 1906 à 1909.

Les procès verbaux des séances de la Commission géodésique suisse des 23 mars 1907
et 2 mai 1908, qui vous ont été envoyés, et celui de la dernière séance, du 8 mai 1909,
qui vient de vous être distribué, vous ont renseignés au fur et à mesure sur notre activité.

Vous avez aussi reçu les volumes X et XI de nos publications ,, Travaux astronomiques
el yéodésiques exécutés en Suisse”, consacrés: le premier à , Relative Lotabweichungen gegen
Bern und telegraphische Uhrvergleichungen am Simplon” volume rédigé par M. le Dr. Nier-
