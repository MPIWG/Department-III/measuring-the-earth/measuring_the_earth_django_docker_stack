2 |

Im un

il

Ba m

Para |

343

Dans les derniers temps nous avons reçu des nouvelles qui nous autorisent à espérer,
de la part de l'Observatoire de Turin, une coopération qui, par la valeur des observations
peut être certainement d’une très grande portée.

Quand on pose la question, savoir quelle latitude géographique, en vue de la distribution
de belles étoiles visibles aussi pendant le jour, est la plus favorable aux observations continues
pour la déterminätion de la latitude au moyen d'étoiles tout près du zénit, on trouve qu'à cet
égard la latitude d'environ + 45° 0’ jusqu’à +- 45° 5’ surpasse énormément toutes les autres
latitudes. Pour cette latitude on trouve dans le Berl. Jahrbuch les quatre étoiles suivantes :

Grandeur Ase. dr. Deel.
N°, 227 6 Bouvier 1,9 5h 58m +. 44° 56’
420 Ww Grande Ourse 980: 11 © 44 59
742 3 Cygne 2,8 19 42 44 55
777 « Oygne 18 00 88 44 58,

qui toutes y culminent, à quelques minutes près, au zénit et sont reparties tant soit peu
regulièrement en ascension droite. Une série continue d’observations de ces quatre étoiles
pendant les 24 heures de la journée nous fournirait certainement des résultats d’une grande
valeur pour la variation de la latitude.

A cette latitude favorable se trouve l'observatoire de Turin (® — 1450 4"), qui
cependant, par sa posision au milieu de la ville, n’offrait pas, jusqu’à present, de bonnes
conditions pour de telles observations. Puisque l’observatoire va être transféré à Pino
Torinese (9 — + 45° 2’, H = 630 m) & une distance de 10 kilométres de la ville, ces mau-
vaises conditions n’existeront plus, et on ne peut qu’applaudir au projet du directeur de cet
observatoire, M, le Prof. Dr. Grovannı Boccarpı, d’entreprendre de telles series d’observa-

tions systématiques,
Tx. ALBRECHT.

4, MESURES RELATIVES DE LA PESANTEUR AU MOYEN DE PENDULES.

Le grand rapport de M. le Prof. Borrass, dans lequel il donne un aperçu de toutes
les déterminations relatives de la pesanteur au moyen de pendules, est sous presse et paraîtra,
d'accord avec le secrétaire M. H. G. van DE SANDE BAKHUYZEN, comme vol. III des Comptes
rendus de la 16e Conférence générale,

M. le Prof, HAASEMANN s’est occupé plusieurs fois, dans l’intérêt de notre Association,
de la détermination des constantes de différents appareïls et de rattachements à l’Institut
de Potsdam:

Au mois de février M. le Dr. E. FacerxoLmM d’Upsal a executé a Potsdam des ob-
servations de pendule pour obtenir un nouveau rattachement d’Upsal à Potsdam; dans ce
travail il fut aidé par M. le Prof, HAASEMANN,

M. HaAsEMANN a préparé ensuite, à l’aide des ressources qu offrait l’Institut géodési-
que royal de Prusse, un appareil qui, à la demande de M, Gxorcg C. Simpson, fut cédé au

a

 
