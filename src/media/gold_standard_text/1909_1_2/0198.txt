 

182

D’autre part, les &carts de fermeture des polygones situés au nord du Col de Larche
sont tous négatifs, tandis que ceux des deux polygones méridionaux sont positifs. Entre le
Col du Petit St. Bernard et le Col de Larche, l'écart total atteint — 872 millimètres; il
est de 4273 millimètres entre le Col de Larche et le Pont St. Louis, pres Vintimille.
Le rapprochement de ces chiffres tendrait a faire craindre l’existence de quelque anomalie
dans la ligne traversant le Col de Larche. La réitération du trongon frangais de cette ligne
n’a que trés peu modifié les résultats primitifs; une vérification analogue, exécutée du

N;

côté italien, lèverait toute incertitude à cet égard.

Il. — NIVELLEMENTS DE DETAIL.

Hn 1907, 1908 et 1909, on a exécuté environ 18.200 kilométres de nivellements

géométriques (voir la Carte, page ci-contre) savoir:
1°. — 7.100 kilomètres de 3ème ordre, dans le Midi de la France;
20, — 6,100 kilomètres de 4ème ordre, dans diverses régions.
Au 31 Décembre 1909, la situation générale des travaux du Nivellement général

de la France sera la suivante:

Opérations effectuées avant 1884 et non réitérées depuis:

Longueurs
Miyellememie divers. =. | . . .,. .°. , 9.000 km.
Mivclicmen; de Bourdaloue. . . . . . . . 11.000 ,

Ensemble 16.000 km.

Opérations effectuées, de 1884 à 1909 inclus,
par le Service du Nivellement général de la France.

ee SO .  ,  , , , 11,800 km.
Sein sen 14800 ,
De Ola er oe
Aue Orden 19200 ,

Ensemble 81.900 km.

Nivellements géométriques restant à effectuer.

Se... en nie.  G.400 km.
me... ee. 137.200 ,

Ensemble 146.100 km.

 

Total général 244.000 km.

 

à
x
=
=
=
a
È
=
=
=

118411)

soa

Libs tail

aaa mann

inch odd abs ha

 
