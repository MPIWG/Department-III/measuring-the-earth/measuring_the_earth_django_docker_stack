Tr tn

FT ITNT |

na er

[LA

is

a
=
=
=
x
x
=
à

367

Figure 2 represents the case in which the elementary mass, dm, is higher than the

station, the difference of elevation being h. In the triangle SAB, from the law of propor-
tional sines, 2

heos 5
Sin Be — D eu +. 8 (4)

 

also in this triangle

D* = Dj +b? +2D,h sing . (8)

Combining (4), (5) and (2) and observing that 6 = 6, —£., there is obtained for

the vertical component of the attraction at the station of an elementary mass which is
higher than the station,

heos 5

sin | Sins.

 

 

D; ++ 2D,h sin à
kdm —kdmu .:. ©

Di +h? + 2D, hsing

 

in which E, is that part of the formula which depends simply upon the direction and
distance of the elementary mass from the station.
Figure 3 represents the case in which the elementary mass, dm, is lower than the

station, the difference of elevation being h. By the same process as that used above it may
be shown that

 

 

 

be heos 5
sin [a+ —
| V/ Di+n—2D as?
kdm —kimb,... . . G
Di 4 he 9) heim!

2

in which E, is that part of the formula which depends simply upon the direction and
distance of the elementary mass from the station.

The only approximation made in deriving formulae (3), (6) and (7) is that due to
the assumption that the distance from the center of the earth to the station is for every
station equal to 637000000 centimeters, the assumed mean value for the radius of the
earth. This assumption is in error since the earth is an ellipsoid of revolution not a sphere
and the station is ordinarily above sea level. But the error is not greater than 1/400 part

for any gravity station.
*

47

 
