 

ia ae

COR en ni

Wipro pe

339

les valeurs trouvées par l’Institut de Vienne, tandis que en passant du groupe XXIX ala
base de Szatmär les valeurs de l’Institut de Vienne surpassent de 87,5 unités celles du
Bureau central (Voir le Rapport de 1909 pe ©.

Pendant l’annee 1910, les caleuls des lignes géodésiques et des déviations relatives
de la verticale pour la mesure de l’are de parallèle de 48° ont été tellement avancés que les
résultats de ces calculs qui se rapportent à la partie orientale de l’arc depuis Budapest jusqu’à
Astrachan, ainsi pour 29 degrés de longitude, sont terminés. Pendant l’année 1909 on avait
déjà calculé les lignes Strazsahalom—Base de Roman, et Base de Roman—Kischinew. Pen-
dant l’année 1910 on y a ajouté les lignes: Széchényi hegy—Strazsahalom, Strazsahalom—
Czernowitz, Czernowitz— Base de Roman, Kischinew—Nikolajew, Nikalojew—Alexandrowsk,
Kischinew—-Alexandrowsk, Alexandrowsk—Rostow s. D., Rostow s. D.—Sarepta et Sarepta—
Astrachan.

Puisqu’on ignorait les valeurs de la latitude et de l’azimut, déterminées auparavant
à Széchényi hegy ainsi que les résultats des déterminations de longitude, de latitude et d’azimut
faites, en 1909, à Strazsahalom, on ne pouvait déterminer les déviations de la verticale pour
la ligne Széchényi hegy—Strazsahalom. Les calculs des lignes Strazsahalom—Czernowitz et
Uzernowitz—Base de Roman sont controlés par les calculs de la ligne Strazsahalom — Base
de Roman qui ont été exécutés, l’année passée: on avait adopté provisoirement comme point
de départ des valeurs convenables pour la latitude, la longitude et l’azimut à Strazsahalom.

L’Institut géographique militaire de Bucharest a trouvé la cause de la grosse erreur d’en-
viron 11}, de l’azimut à l'extrémité nord de la base roumaine de Roman signalée dans le rapport
de 1909 p. 8, de sorte que cet azimut s'accorde maintenant fort bien avec les azimuts voisins
déterminés à Czernowitz et à Kischinew. Pour les mesures à l’est de Kischinew ona employé,
autant que possible, les résultats obtenus par les géodésiens russes pour leur part de l’are
de parallèle, résultats qui ont été publiés dans les volumes XLIX et L des »Sapiski (Me-
moires) de la section topographique militaire de l'État major general & Si. Petersbourg” (St.
Petersbourg 1893). Plusieurs rattachements de stations astronomiques au réseau principal
résultant de ces mesures ont donné lieu à des recherches spéciales et à plusieurs calculs.

Un tableau contenant les déviations relatives de la verticale en latitude et en longi-
tude, et les erreurs de clôture des équations de Laprace depuis Czernowitz jusqu’à Astrachan,
accompagné de quelques autres données et d'observations critiques, se trouve dans mon rapport
»Bericht über Lotabweichungen (1909)? (Comptes rendus de la 16e Conf. de 0 Assoc. géod. int.
Londres. Cambridge II. Vol. Berlin 1911 p. 259); il suffit done de renvoyer à cette publication.

Aux calculs mentionnés dans ce rapport ont pris part: l’adjoint au Bureau’ central,
M. G. Fôrsrer, jusqu'à mi-mai, le géomètre, M. Hınonar, jusqu’au fin de juillet et M. E.
Hower candidat en mathématiques depuis mi-juin jusqu’à la fin de l’année,

Pendant l’année 1911 on continuera les caleuls relatifs à l'arc de parallèle de 48°,
Quand nous connaîtrons les résultats des observations astronomiques a Széchényi hegy et à
Strazsahalom, qui nous manquent encore, on s’occupera d’abord des calculs définitifs qui sont
nécessaires à la détermination de deux lignes géodésiques, puis on commencera des travaux
analogues pour la partie à l’ouest de Budapest. En premier lieu viennent les lignes Széchényi

 
