 

 

RS

 

 

306

latitudes astronomiques, comme il est naturel de le supposer avec un arc aussi court. Avec

l’ellipsoïde de Bzssez, l'écart moyen entre la latitude géodésique et la latitude astronomique,

est de 3/.70, avec celui de CLARKE, de 3”.62, et avec celui de M. Hezmerr, de 3”.66.
La plus grande déviation de la verticale dans le sens du méridien est au cap Fanshawe ;

elle atteint 10”.1.
OBSERVATIONS GEODESIQUES ET TOPOGRAPHIQUES.

Il importe de savoir si les déviations signalées peuvent être imputées à l'attraction
des massifs apparents, et pour cela de calculer l'attraction due aux massifs apparents les

plus voisins des stations d'observation.
A cet effet, on a fait un lever topographique de ces massifs à l'échelle 1 : 100 000,

afin d'évaluer leur attraction, et une étude géologique des roches qui les composent, afin

de connaître leur densité.
Ce lever comprend une large zône, enveloppant les sommets de la chaîne jusqu’à

90km de distance au moins. Les isohypses y sont tracées de 100m en 100m d’après les ob-

servations photogrammétriques.
Les déterminations de la densité ont été faites sous la direction de M. le baron

Dre Gæer et comprennent 50 échantillons appartenant aux schistes cristallins, aux roches
éruptives anciennes, au silurien, à l'étage du houiller, du trias et du jurassique.

CALCUL DE L’ATTRACTION DES MASSIFS APPARENTS.

Le calcul des attractions des massifs apparents, dont j'ai été chargé, est à peine
abordé, parce que les données hypsométriques sont encore beaucoup trop incomplètes.

Je me suis servi d’une méthode d'intégration qui consiste à faire un changement
de variables dans Vintégrale triple qui donne la composante de l'attraction suivant une di-
rection donnée. Au lieu des variables semipolaires, 7, &, z (rayon vecteur, azimut, différence
d'altitude), j’introduis trois variables nouvelles, p, , ¢, liées aux premiéres par des formules

simples:
r
p— lon 7, ¢ == sin @, tale 2,

en sorte que l’élément de l'intégrale triple :

6r2cosa dr da dz

(7? + 2)

 

prend la forme:
6 dp do dé,

où les trois variables sont séparées.
L'application numérique de cette formule est très commode. L'intégration par rapport

 

 

vd MAN LE bte 4 Rd A a A hi

se me bn a db A a lh baie te
