 

 

 

290

$ 3. Dre WINKELMESSUNGEN.

‘

Hs war urspriinglich beabsichtigt auf jeder Station nach Möglichkeit vollständige

Richtungssätze zu beobachten. Doch so günstige Umstände, dass alle zu beobachtenden Rich-

tungen gleichzeitig sichtbar waren, sind nur ganz ausnahmsweise eingetreten und es wurde
sehr bald den Beobachtern freigestellt, in jedem Falle den Umständen entsprechend zu ver-
fahren. In Folge der auf Spitzbergen so schnell wechselnden atmosphärischen Bedingungen,
der häufigen Nebel und der oft schlechten Bilder haben auch die Beobachtungsmethoden
sehr variirt. Neben vollständigen Richtungssätzen kommen zahlreiche unvollständige vor,
ferner Winkelmessungen zwischen je zwei Signalen oder auch zwischen einer nahen, dem
Netz nicht angehörenden Mire und je einer Richtung. Das letztere Verfahren gestattet ja
jeden Augenblick zu benutzen, selbst wenn auch nur eine Richtung des Netzes sichtbar ist,
verlangt aber andrerseits mehr Arbeit und Zeit und man opfert dabei an Genauigkeit.
Wollte man jedoch 1901 die ganze Arbeit abschliessen, so war grösste Eile geboten und
selbst ungünstige Umstände und schlechte Bilder mussten mit in den Kauf genommen
werden, um nur die notwendigste Zahl von Messungen zusammenzubringen. Gute Bilder
sind ja vorgekommen, namentlich bei relativ nahen Signalen, aber im Allgemeinen haben
die Beobachter fortwährend mit Schwierigkeiten zu kämpfen gehabt, klagen häufig über
schlechte Sichtbarkeit der Signale und haben sehr oft wegen Nebel oder heftiger Stürme,
die Arbeit unterbrechen müssen. Es hat mitunter grosse Mühe gekostet, ferne Signale
überhaupt zu entdecken und es sind Fälle vorgekommen, wo ein Signal eine Woche und
mehr vergebens gesucht wurde, ehe es bei etwas besseren Bildern dennoch entdeckt und
beobachtet wurde.

Es handelte sich nun zunächst darum, aus dem ganzen gesammelten Material für
jede Station die plausibelsten Resultate abzuleiten. Den einzelnen Beobachtungen jeder Rich-
tung ist hierbei meist dasselbe Gewicht erteilt worden, da eine individuellere Bewertung
der unter verschiedenen Umständen erlangten Beobachtungen nicht ohne ganz willkührliche
Interpretationen der von den Beobachtern hinzugefügten Bemerkungen möglich gewesen wäre.
Ueberall, wo es anging, wurde dann der Versuch gemacht, die Genauigkeit, mit der die
einzelnen Richtungen bestimmt werden, für jede besonders zu ermitteln. Wo mehr oder
minder vollständige Richtungsbeobachtungen vorlagen, wurde zunächst eine strenge Stations-
ausgleichung vorgenommen, um sodann das elegante Verfahren von HELMERT anzuwenden,
welches die Gewichte der einzelnen Richtungen so zu bestimmen erlaubt, dass das Stations-
resultat als ein Satz von unabhängigen Richtungen aufgefasst werden kann. Bei Winkel-
messungen, wo jede Netzrichtung zusammen mit einer Mire beobachtet wurde, müssen dann
die mittleren Fehler der Winkel direkt den Richtungen zugeschrieben werden.

Laut des ursprünglichen Programms sollte der mittlere (wahrscheinliche) Fehler eines
auf der Station gemessenen Winkels nicht +0”.9 (+0”.6) überschreiten. Diese Genauigkeit
ist in allen Fällen nicht nur erreicht, sondern meist ganz erheblich überschritten worden,
wenigstens was die zufälligen Beobachtungsfehler anlangt.

vr ae sh 8 dé dd À dd ele

a mA te a ed a add ana un

 

 
