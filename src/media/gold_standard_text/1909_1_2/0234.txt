 
 
 
 
  
 
 
 
 
 
 
 
 
  
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
  

214

Connection will be made with the Lake Survey of the United States near River
St Clair, and on the Niagara Peninsula, and with the Coast and Geodetic Survey in New
York State or Vermont.

Within the last two months observations have been begun on the triangulation of
the Bay of Fundy. Two points of this are points at the extremity of the Oblique Arc of the
United States Coast and Geodetic Survey.

Reconnaissance has been made to the extremity of Cape Breton Island and the
topography has been found suitable for a strong triangulation.

Extension of the triangulation is intended at an early date along the St Lawrence
River northeast from Quebec, and, in the Province of Ontario, northward along the east
shore of Georgian Bay.

Precise levelling has been carried on in Quebec and Ontario to the extent of about
1100 kilometres, twice levelled. The instrument used is of the same pattern as that of
the United States Coast and Geodetic Survey. The two levellings of a section are required
to agree within a limit, in feet, 01.0,017 x |/ distance in miles, or 4mm. x distance in km.

The initial point of the levelling is a bench mark at Rouse’s Point, N. Y., the
elevation of which above mean sea-level at New York City has been determined by the
United States Coast and Geodetic Survey. Connection has also been made with a bench
mark of the Deep Waterways Commission at the International Bridge, near Buffalo, N. Y.

Reference may be made also to astronomical determinations of latitudes and longi-
tudes, though these have not been made primarily for the purposes of the Geodetic Survey,
but to assist in the correction of the existing geographical maps.

In the observations transit instruments of about 7.5 centimetres aperture and 85 cm.
focal length have been used. The eye piece micrometer is arranged so that it can be used
either for azimuths or for zenith distances. A sensitive level is attached to the telescope when
zenith distances are to be measured enabling latitudes to be determined by Talcott’s method.

The micrometer, when used in transit observations, registers electrically upon a
drum chronograph, with which is connected a break circuit sidereal chronometer.

The longitudes are determined by telegraphic exchange of signals with a point of
known longitude, usually the Dominion Observatory at Ottawa. The observing hut is
erected near a telegraph office, and the wires are brought into it. The railway and tele-
graph companies have always been ready to give us every assistance in this regard.

In general, personal equation is determined by comparison of the results obtained
by two observers side by side, each observing with the instrument used by him throughout
the longitude campaign. This comparison is made before and after the observer goes to
the outside stations.

The longitude of the Ottawa Observatory has been determined by connection with
the longitude net of the United States at three points, and adjustment of the differences

of longitude.
Up to the present time 96 stations have been determined in latitude and longitude.

 

 

 

1084111188 MA u | ua

ni

ak ml

mi!

Inc vd As a, All a Hm)
