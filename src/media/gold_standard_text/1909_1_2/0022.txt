 

16

A Budapest, la Conférence avait aussi adopté une résolution tendant à exprimer sa
satisfaction de l’entrée de la République Argentine dans notre Association, et à indiquer
les travaux géodésiques qui, dans ce pays, auraient le plus de valeur. Toutefois la partici-
pation de la République Argentine à nos travaux ne nous avait pas été notifiée officiellement
par le représentant diplomatique de cette République, mais nous avait été communiquée par
M. le Professeur Porro DB SoMenzr; c'est done à lui que j'ai transmis notre résolution.

Jusqu'à présent cette notification officielle nous fait encore défaut, mais, par une
publication de l’observatoire astronomique de l’Université nationale de la Plata, nous avons
appris que le Congrès national a voté le 10 Septembre 1908 une loi approuvant 1° la par-
ticipation de la République Argentine à notre Convention, 2° l’achat de l'observatoire de
latitude à Oncativo, appartenant à notre Association, moyennant une somme de 20000
Marks, fixée par une convention entre M. Hutmurr et M. Porro. D’aprés une lettre de la
légation de la République Argentine & M. Hetmurr, cette somme a été versée par le Mi-
nistère des finances à l’Université de la Plata; à ma prière, M. le Président de cette Uni-
versite a bien voulu faire verser cette somme à la Caisse des légations à Berlin.

Au mois de Novembre 1906, le Bureau a expédié la lettre suivante aux représentants
diplomatiques à Berlin des États contractants qui avaient notifié leur adhésion à la proro-
gation de la Convention pour une nouvelle période de dix ans.

Association géodésique

normaonale, Nice—Leype, Novembre 1906,

MoxsiEur L' AMBASSADEUR,

Dans la lettre que nous avons eu l'honneur d'adresser à Votre Excellence en date
du 27 Avril 1905, nous vous avons informé que, d’après l'art. 7 de la Convention de
l'Association géodésique internationale qui est entrée en vigueur le let Janvier 1897, les
États contractants n’avaient pris l’engagement de payer leurs contributions que pour une
période de dix ans, finissant le 31 Décembre 1906.

En insistant sur l'importance de la continuation des travaux de l’Association, nous
nous sommes adresses aux hauts gouvernements contractants, pour leur demander de bien
vouloir donner leur adhésion à une prorogation de la Convention pour une nouvelle période
de dix ans, à dater du ler Janvier 1907, sans aucune modification des articles, et par con-
séquent de continuer pendant la méme période leurs contributions annuelles indiquées dans
l’article 9.

Par lettre du...... 1905, Votre Excellence a bien voulu nous faire connaître que
son Gouvernement donnait son adhésion à une telle prorogation.

D’autre part, nous avons ee des déclarations analogues de la presque unanimité
des 21 Etats contractants; un seul n’a pas envoyé de réponse,

 

=

Me Ah |

we (iam cra

La a

to |

jt ahmed be a did dual db a punis à
