 

346
M. le Prof. Dr. GALLE a aussi construit une carte des triangles dans l’Amörique
du Nord près du méridien 98° à l’ouest de Greenwich, ainsi qu'une carte des nouvelles
triangulations en France; ces deux cartes seront publiées avec son rapport.
M. le Prof. Dr. AzBreouT a envoyé à M. le Secrétaire un rapport sur les déter-
minations de latitude, de longitude et d’azimut exécutées pendant les dernières années qui
paraîtra aussi dans le 2e Vol. des , Comptes rendus”,

HELMERT,

B. Gestion administrative.

Le fonds des dotations a été géré comme d'habitude. En nous réservant le
dépôt conventionel des comptes exacts et des dépenses, nous donnons ci-dessous un aperçu
du mouvement des fonds pendant l’année 1910.

Recettes.
Suite eu des fonds a la ün de 1909, . .. . : . . .._ M 68 505,92
Contributions pour les années précédentes . . . . + +s. +s » 9 999,60
Donmbumons pour hannee [90 . .. rn ne nen 68 276,00
Vente des publications . . . on 95,20
Intérêts: du , Kur- und N nc Pee alien ace
kasse’ A Berlin. . , 5 343,70
4 du „Königliche Seohandlung (Prousische Staaisbank)”
berlin 5 ; eier ; 1 269,65
Ce M... 142 490,37
Dépenses.
Indemnité au secrétaire perpétuel . . . . M. 5 000,00
Pour le service international des latitudes (au de ie R ipiatonk) 5 51 600,51
ty, 3 ; 5 @u sud. ,, i ; 10 336,00
Pour les calculs relatifs aux déterminations de la pesanteur, des
déviations de la verticale et de la figure de la terre . . „ 4 934,00
Pour l'installation de la station de Pribram . : . ; 1 610,42
Frais d'impression (déterminations de la pesanteur à la mer et tres)? if 3 484,10
Frais de transport, port de lettres, frais d’expédition . . ‘i 1 793,67

To M. 78 758,70
Par conséquent à la fin de 1910 le solde àctif était. . . . M. 63 731,67

 
