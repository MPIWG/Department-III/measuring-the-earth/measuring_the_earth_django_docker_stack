 

q
j
a

 

186

I. Renseignements généraux non donnés dans les Rapports antérieurs.

1. ALLEMAGNE.
g. Prusse. (Landesaufnahme).

Piste d’eaperienee. — En 1908, ä Freienwalde, dans la vallée de l’Oder, on a installe
une piste d’essais et d’experiences pour les nivellements. |

Cette piste, longue de 2 kilométres, présente des déclivités variables et une dénivel-
lation totale de 45 métres entre ses deux extrémités. 9 reperes souterrains ont été établis,
savoir: 3 4 chaque extrémité et 3 sur le parcours, subdivisé ainsi en 4 troncons de chacun
900 mètres de longueur.

Ces repères se composent d’un pilier de granit, haut de 0m,90, fixé au ciment sur
une dalle quadrangulaire de 0,60 de côté et portant à son sommet un cylindre de bronze
couronné par une bille d’agate.

Le couvercle est formé d’une dalle de granit, dont la face supérieure est à 0m,30
au-dessous du sol.

Sur cette piste, chaque printemps, on éduque les manoeuvres et N nouveaux opé-
rateurs. On y essaie de méme les niveaux et les mires au début et a la fin de chaque
campagne.

Conservation des repères. — Chaque année, par suite de dégradations, de démolitions
d'édifices, ou d’affaissements du A il disparait une centaine (soit 8°/,,) des 13.000 repéres
que comporte le réseau nivelé. Ces repères sont soigneusement reconstitués.

À. Prusse. (Ministère des Travaux publics).

Sur le bras de mer, large de 12 km., qui sépare du continent l’île de Sylt, on a
effectué, en 1900, un nivellement de précision.

En 1904, pour mesurer l’importance d’affaissements constatés sur les murs de la
cathédrale de Konigsberg, on a, dans l’intérieur de celle-ci, exécuté un nivellement hydro-
statique au moyen d’un appareil Sergr-FuEss.

12. ROUMANIE.

Médimarémètres. — Le médimarémètre de Constantza, dont l’observation, pendant 8
années consécutives, a permis de fixer le zéro fondamental roumain, n’existe plus; mais on
a l'intention de le rétablir ultérieurement et d’en installer un second à Mangalia.

Réseau de 2me ordre. — Kn vue de l'établissement d’une carte générale du pays, on
a commencé, sans plan préalable d’ensemble, l’exécution d’un réseau de 2e ordre, dont les
lignes sont choisies et nivelées au fur et à mesure des besoins.

sank nn aan u
