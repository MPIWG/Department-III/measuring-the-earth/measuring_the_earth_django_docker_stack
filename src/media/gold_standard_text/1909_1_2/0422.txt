 

|
|

358

with the Lake Survey triangulation at Duluth, and to utilize this connection to strengthen
the ninety-eighth meridian triangulation. The least square adjustment involved was of unusual
magnitude. The whole chain of triangulation, from Page, Nebraska, to Duluth, Minnesota,
900 kilometers measured along the axis of the triangulation, involving 92 triangulation
stations, and containing two measured bases, was adjusted at one time. There were 199
condition equations involved in the adjustment. Of these 143 were angle equations referring
to closures of triangles, 50 were side equations to remove discrepancies in lengths as com-
puted through different triangles, 3 were length equations to make the computed lengths
agree with the measured bases, and the remaining 3 equations insured that the latitude,
longitude, and azimuth, as computed from Page upon the United States Standard Datum,
would agree at Duluth with the values already computed upon that datum through trian-
gulation from the eastward.

The last observations on the primary triangulation northward from central California
to the State of Washington were made on July 26, 1906. The positions of the principal
stations on this whole triangulation, from Marysville, California, to Tacoma, Washington,
have been computed on the United States Standard Datum. This standard datum has also
been extended northward and westward from Tacoma, by utilizing old triangulation, 1o points
on the Canadian boundary in latitude 49°, and to Cape Flattery at the western end of Juan
de Fuca Strait. The total length of this triangulation, measured along its axis, from Marys-
ville to Cape Flattery and the forty-ninth parallel, is 1300 kilometers.

Triangulation extending from latitude 35°, near the boundary between South Carolina
and Georgia, southward to Port Royal on the coast, and thence following the coast of South
Carolina and Georgia and completely around Florida to Mobile Bay, Alabama, has been
reduced to the United States Standard Datum. The total length of this triangulation, measured
along its axis, is 2500 kilometers.

The triangulation in these areas, being now computed upon the United States Standard
Datum, is available in the best form for use in connection with a new determination of the
figure and size of the earth.

ASTRONOMIC OBSERVATIONS.

Between August 25, 1905, and November 6, 1908, astronomic observations of latitude
were made at 76 stations, of azimuth at 67 stations, and of longitude at 31 new stations.
The corresponding computations are now all complete. To secure the greatest efficiency in
determining the figure and size of the earth, it is evidently more important to make astro-
nomic observations, and so to determine the deflections of the vertical, at many stations
than to secure an extremely high degree of accuracy in the astronomic observations at a
few stations. Hence, after fixing upon a sufficiently high standard of accuracy for the pur-
pose, the endeavor was made to use such methods of observation as to secure with a
minimum expenditure of time and money results conforming to this adopted standard
of accuracy. The observers were required to observe the astronomic latitude in such a manner

 

4
3

inl 11d oedema heen 1.

suit,

ja heal Le a Hi A Mb na nn

{
i
j
|

 
