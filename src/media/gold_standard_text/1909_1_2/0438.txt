 

374

Zone 18. Inner radius 1° 29 58”. Outer radius 1° 41’ 13”.

One compartment.

Unit of elevation is 100 feet (27.1 fathoms for depths), and the principal correction
is .0001 dyne for each unit, minus for land and plus for water.

 

Correction for elevation

Correction | Correction for ‘
of station.

 

as read | departure from

 

 

: : Station at | Station at | Station at
from map \proportionality. 49 feet | 10000 feet | 15000 feet
+ 150 ge I ——) —— al)

-- 100 . 1 == == 3 =,
= 50 0 | 9 =)
u 06 0 0 | 1
: 0 0 0 0 0
—_., 98 0 0 ara ae
— 90 0 +1 a) a
— 75 0 +1 +2 a
= 100 0 +9 13 +5

 

 

one 13. Inner radius 3° 3 5”. Outer radius 4° 19’ 13”.

Sixteen compartments.

Unit of elevation is 1000 feet (271 fathoms for depths), and the correction is .0001
dyne for each unit, minus for land and plus for water. |

There is no correetion for departure from proportionality.

There is no correction for elevation of station.

fone 7. Inner radius 20° 41’. Outer radius 26° 41’. Two compartments.

Unit of elevation is 1000 feet (271 fathoms for depths), and the correction is .0001
dyne for each unit, minus for land and plus for water.

There is no correction for departure from proportionality. —

There is no correction for elevation of station.

Zone 6. Inner radius 26° 41’. Outer radius 85° 58’. Highteen compartments.

Unit of elevation is 10000 feet (2710 fathoms for depths), and the correction
is .0001 dyne for each unit, minus for land and plus for water,

There is no correction for departure from proportionality.

There is no correction for elevation of station.

Zone 1. Inner radius 150° 56’. Outer radius 180°. One compartment.

Unit of elevation is 10000 feet (2710 fathoms for depths), and the correction
is .0001 dyne for each unit, minus for land and plus for water.

There is no correction for departure from proportionality.

There is no correction for elevation of station.

uud

HAMMEL MEME sau À

au ed, 1488 7}

 

E
|
|

 

 
