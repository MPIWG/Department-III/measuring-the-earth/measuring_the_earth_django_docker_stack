HMPPTEETRE

fondés sur ceux des hommes les plus illustres que le monde ait connus, et ils auraient été
impossibles sans là collaboration de toutes les nations civilisées. Ce sont les grands génies,
les Newrox, les Gauss, les LapLace qui ont posé les fondements de ces travaux, ce sont ces
grands mathématiciens, dont la gloire rejaillit sur les nations dont ils sont issus, qui ont
contribué puissamment à élucider la structure de tout ce qui nous entoure. Presque chaque
peuple est représenté dans la grande série des hommes illustres qui, par leurs travaux, ont
fait avancer les grandes sciences abstraites qui forment le fondement des études dont cette
conférence s’occupe. Voilà pourquoi à cette occasion l’accueil du gouvernement est un ac-
cueil si sincère, voilà pourquoi nous sommes coinvaineus que les sujets dont vous allez vous
occuper amèneront les différentes nations, dont les délégués sont réunis ici, à des points
de vue communs et contribueront à une meilleure appréciation mutuelle,
Au nom du gouvernement, je vous souhaite cordialement la bienvenue.

Sir ARCHIBALD CikiEe, Président du »Royal Society”, étant empêché par une in-
disposition d'assister à la séance, Sir Gore H. Darwin adresse, au nom du »Royal So-
ciety”, à l’assemblée les paroles suivantes.

M. Haldane, M. le Président, Mesdames, Messieurs.

Sir ARCHIBALD Gutkiz m’a prié d'exprimer son grand regret de ne pouvoir assister
aujourd’hui à cette séance. Il a eu un petit accident qui le retient loin de Londres. Dans
ces circonstances c’est moi qui ai l'honneur de représenter ici le Royal Society, une des plus
anciennes Académies des sciences de l’Europe, dont Sir ARCHIBALD GEIKIE occupe si dignement
le siège présidentiel, siège occupé jadis par Nawroxw, le fondateur, pour ainsi dire, de notre
science, et commémoré déjà par M. Hazpane. Comme vous le saver, Sir ARCH1BALD lui-même
n’est pas géodésien, mais il est aux premiers rangs dans une science apparentée, c’est à dire la
géographie, dont le nombre des points de contact avec la géodésie augmente de plus en plus.

Vous avez appris de M. HaLpans, parlant au nom du ministre président et du
gouvernement, que le gouvernement de sa Majesté considère comme un devoir et comme
un privilège de souhaiter ici la bienvenue à MM. les délégués de la Conférence. Qu'il me
soit permis, comme représentant du Royal Society, de vous exprimer la satisfaction des
hommes de science de la Grande Bretagne, parmi lesquels nous comptons ceux qui sont
venus des différentes parties de l'Empire, de rencontrer, réumis ici, la plupart des géodésiens
les plus distingués du monde. -

Depuis l'entrée de la Grande Bretagne dans l'Association géodésique, j'ai l'honneur
d’être le représentant du Gouvernement de Sa Majesté auprès de cette Association, comme
membre de Ia commission permanente. Il se peut que d’abord il y ait eu, de la part des
fonctionnaires du gouvernement, un peu de scepticisme quant aux avantages qui, pour le
service géodésique de la Grande Bretagne, pourraient résulter de notre entrée dans l’Asso-
ciation. D’après une expérience d’environ 11 années, je puis vous assurer que pour nos
géodésiens dans les différentes parties du monde ces avantages ont été énormes. De toutes
parts on pourrait obtenir des témoignages en faveur de cette opinion, ainsi que J'ai eu

 
