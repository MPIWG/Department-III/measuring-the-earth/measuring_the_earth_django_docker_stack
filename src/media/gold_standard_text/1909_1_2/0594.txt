 

 

ee en

\
Ib
N,
|
|
Ht

oe.

Quant aux résidus accidentels, il nous parait qu’on doit les attribuer a deux causes
principales:
1°, Les difficultés qu'on avait, par suite de la nature du terrain, d'installer les
piquets tenseurs de manière à éviter tout frottement latéral des cordes.

90, Trincertitude sur la véritable valeur de la correction à apporter à l'étalonnage
du fl, obtenue avec les divisions des réglettes tournées vers le haut, pour son emploi sur
le terrain. Les conditions variables de l'éclairage obligent, en effet, pour faire de bonnes
lectures, à donner aux réglettes des inclinaisons diverses et il n’est pas rigoureux d’ad-
mettre que les réglettes soient toujours dirigées vers le bas.

Aussi les écarts accidentels maxima de 0™™,15 environ que nous avons constatés n’ont
ils rien d’excessif; ils suffisent à faire proscrire de manière absolue l'emploi isolé du fil Ac.

    

Echelle des crdonnées: 1 pour 0,2.

    

a SAC eae 2 3 \
ae a x
m
490 200 je 20
\

#0 265
A
= x

Nec

 

N

us 7
160 ip 786
\

Ne

Puisque nous sommes parvenus 4 représenter les écarts constatés par une loi ne
laissant que des résidus d’allure accidentelle, il est bien évident que nous pourrons modifier
les résultats du calcul des mesures de la base au moyen de A,, de maniére 4 obtenir une
concordance satisfaisante, Nous aurons à ajouter au chiffre de la mesure d’aller la valeur

de l'intégrale
263
Î (Omm,3 -L Omm,27e—0018x) dx — 98mm,988

et au chiffre de la mesure de retour

263
Î (mn 045 — DOD0 x) dx —. Ilum, 143.

©

 

 

ve

um hb lA I dah

AM dec ib ded sone dits

Lbs dd do

a dh he Iie di ld dlp
