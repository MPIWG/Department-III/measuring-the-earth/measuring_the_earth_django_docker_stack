wuation au ler Janvier 1909.

repéres. — Precision des résultats.

        

Nombre den reperes
ee ce j Erreur accidentelle

 

 

 

 

 

 

 

créés de 1906 à 1909 exclus existant au ler janvier 1909 n moyenne quadratique Discordance
= == le À S ey | systématique
epères de ie ondre Repéres de ler ordre Bee resultant moyenne i
Se aos rena a Br ae Observations
| | : None Nombre | = 3 2 des de la com en i i
ES Zn Hal]: ses "IT entre l’aller
souter- | métalli- de a souter- métalli- de tot m 52 |fermetures | paraison a ir
| rains ques | 2€ ordre ARS ques | 2€ ordre ae as des de deux
E | a polygones eee
\ km een a m m/m m/m
aim de az
avant
1906: 0,9
» 83 391 4714 » 434 846 1280 1,4 » en (1) Pays dont I question-
; 1906: 0,7 naire n’est pas rentré; les ren-
> 89 S 826 3 193 4 019 1,4 1,82 0,77 0,26 seignements indiques sont ceux
16 m 48 a 24 760 1 280 2 064 0,5 0,8 0,5 i publiés en 1906 ou trouvés
» > 5 = 111 1 090 1201 1,8 1,4 à 4,0 | 1,0 à 5,0 5 dans les publications.
» » » » » 652 652 1,0 1,2 2,6 > (2) On a réitéré 145km d’an-
» » » 4 409 2 305 2714 | 2,0 = 11 ciennes lignes.
9 To i 160 és 13 000 = 13 000 1,5 1,81 0,47 0, 60 (3) Outre 100km de nivelle-
ments de revision.
3 3 500 e 3 500 5 20 000 5 20 000 0,6 1,14 0,67 0,044
» 5 = a 1 720 1 720 156 ic 2,4 >
» » » = 213 1 600 1 813 Wei , al
E ee 5,1(*) 1,4(*) (4) Avant 1899
Ë a |
2 275 Br a 7. 3080 | 11798 |. 15,66 ” 1 1.8¢)| 126) ast ) (5) Depuis 1899
L 2 » » » 626 1 487 2113 0,6 » 0,83 0; 16
BE, a 990 205 1 365 2 560 Pi I 1,4 0,6
En 232 745 977 5 1815 13 169 | 14984 0,9 5, 1,25 0,51
» » » 22 1,20 0, 36
» 3 = A 30 000 15 000 | 45 000 0,6 5 1,50 0,78
I 2 » 29 5 3,40 2, sl
Ë » 5 - 23 | 24192 | 24215 | 0,9 1,0 i
= , i 100 1 100 5 8614 8 614 0,9 1,0 1,4 2,2 2 (6) Outre 151km de nivelle-
, 62 50 112 & 229 121 350 3,8 . 2,07 a ments de revision.
À x 461 432 893 2,8 0,5 0,5 = (7) Outre 32km de retour
> 133 109 242 5 1 642 1114 2 756 0,9 5 eae sur un ancieu itinéraire nivelé
; I) 213 na. | 0,9 une seule fois.
» 749 392 1141 5 1 836 392 2 228 1,9 A ’ >
» 267 » 2 368 2 368 | 13,9 » na 1,0 (8) Y compris l’Asie russe
=. 5 a : 2 440 195 | 2635 | 2,5 4,4 2,24 1,37
+. » 5 5 3 900 2 100 6 000 0,6 1,08 0,85 0,85
= 28 219 2 45 401 : 446 |. 1,5 : 0,37 5
ne 201 645 = 818 2 273 3 091 0,72 £ 5d,1 à gd | 0,24 & 7a
» » » » 102 255 357 0, 804 » » »
els eB 1 284 1 447 1 560 13 415 | 14975 2,1 5 i (9) Non compris 1865km de
» Es , sy 7 399 5 7 399 2,0 2,71 : 1,13 nivellements réitérés en vue
d'éliminer de forts écarts de:
fermeture,
» | >» 2 3 392 » 392 3,4 » 1,78 »
> 1 300 on 35 10 100 10 100 4122) 0,67 Onn 0,02 a 0,74 (10) 2K,8 pendant la période
» 17 a a 31 14 45 | 3,5 4 1,2 à 1,5 10,4 & 1,1 | 1906 -09.
» » » » » 367 a il » 0,98 0,4
i à + 1 (11) Nivele au ler Juin 1909.
a
è ” ” » » » 3 3 700 | 3 700 0,8 » » »
15 216 5826 | 7327 | 18869 | 2626 | 98723 | 23867 | 219 716) 1549

 

 

 

 

 

   
