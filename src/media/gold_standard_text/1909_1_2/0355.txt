DR es nee se ove

M nn. Dates en URL |

m

x
x
=
=
à

BEILAGE A. XVI, b.

SCHWEDEN.

Bericht über die Gradmessungsarbeiten der Kgl. Akademie
der Wissenschaften.

In dem ursprünglichen Plane für die Theilnahme Schwedens an der mittel-europäischen
Gradmessung beabsichtigte man, die schon ausgeführten Triangulationen des Kgl. Schwe-
dischen topographischen Corps in Verbindung mit den Dreiecksnetzen der respektiven Nach-
barländer anzuwenden für die Bestimmung von Entfernungen und gegenseitigen Richtungen
zwischen Punkten, wo die astronomischen Bestimmungen von Polhöhe, Länge und Azimuthe
entweder schon vorhanden waren, oder an denselben ausgeführt werden konnten.

Demzufolge stellte die Kgl. Akademie der Wissenschaften einen Plan und Vorschlag,
theils zur Revision und Completirung derjenigen Dreiecksnetze die schon vorhanden waren
und die als Theile in die wissenschaftliche Arbeit eingehen sollten, und theils zur Aus-
führung erforderlicher astronomischer Bestimmungen an passenden Punkten.

In den Jahren 1843—1869 wurden auch folgende Arbeiten bewerkstelligt, nämlich:

1°. Messungen von Grundlinien auf Ladugärdsgärde in der Nähe von Stockholm, an
Axewalla Hed in Westergötland und an der Laholmbuchte in Halland.

2°. Horizontale Winkelmessungen zur Verbindung der Grundlinien mit den Haupt-
seiten der resp. Dreiecksnetze.

3°. Neue Horizontalwinkelmessungen an 26 Dreieckspunkten der westlichen Küste
Schwedens zur Revision des älteren Netzes.

4°, Telegraphische Längenbestimmungen zwischen Stockholm, Kopenhagen und
Kristiania.

5% Azimutbestimmung der Dreiecksseite Lund—Romeleklint.

‘Von diesen Arbeiten sind nur die astronomischen veröffentlicht worden und zwar in
„Kgl. Svenska Vetenskaps Akademiens Handlingar och Bihang”.

Die eben erwähnten geodätischen Messungen an der westlichen Küste Schwedens
bildeten den grössten Theil der in Schweden ausgeführten Arbeiten, und beabsichtigten zu-
nächst einen Beitrag zur Bestimmung der Krümmungsverhältnissen des grossen Meridian-
bogens etwa zwischen Palermo und Kristiania.

 
