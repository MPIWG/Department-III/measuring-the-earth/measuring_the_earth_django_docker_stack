 

238

The heights are in feet and are referred to the staffs used at the several stations.

ete se eh bl bk bh ie dia Mi Ma ieh

ee

 

 

 

 

 

Colonial Beach, Weeks, Galveston San Diego, Tloilo, Panay I.,
Virginia Louisiana (Ft. Point), Texas California Bil.

De mn m nn
Year HTL MSL HTL MSL HTL MSL HTL MSL HTL MSL
1904 4,66 4,62 4 AA
1905 398 3022) 4,69 4,65 4,28
1906 6,40 6,41 1) 304 301 4,66 4,65 6,56 6,503) 4,71

; 1907 6,34 6,35 6.62 6,56 4,56

; 1908 6,36 6,52 6,46

Means 6,37 6,38 200 302 4,66 4,64 Gol 6,01 4,49

 

i a

 

 

Fort Hamilton, Philadelphia, Baltimore, Fernandina, 3
New York Pennsylvania | Maryland Florida

| mm LI

Year HTL MSL HTL MSL HTL MSL Eu MSE

| 1893 5,81 5,87

\ 1894 9.89 9,89

6 1895 Da 38

N 1896 289 54 |
| 1897 ASE Ur ï
| 1898 »,90 06.00 6,08 6,14 |
| 139, 532 59 6,20 6,25

i 1900 Doo 5,89 5 6,02 6,06

| , 1901 6,03 6,08 6306 6,49 : 6,09 6,14

: 1902 6,07 6,10 6,6425 6,77 | 6,09 6,14

| 1903 | 6.01 005 6.30. 6,69 445 415 6,25 082 i
j 1904 sen SU 6,27 6,42 390 2 397 619 020 1
| 1905 5,82 5,86 6,22: 6,37 400 402 | 6,08 6,20 |
h 1906 Do SON 6,37 6,54 4,06 4,08 6,11 6,25
| 1907 9 539 00 oi 4,00 4,02 5,91: 6,02

I 1908 934389 6,25 6,40 4,04 4,05 611105

| Means 5,90 5,94 6,38 6,52 4,04 4,05 610 6,18

 

 

 

 

All the above series are for calendar years.

i 1) Series of 1906 at Colonial Beach, Virginia, consists of 1 year commencing July 1, 1906. Mean has been
; weighted accordingly.

2) Series of 1905 at Weeks, Louisiana, cousists of 1 year commencing Feb. 25, 1905.

3) Series of 1906 at San Diego, California, consists of 1 year commencing Jan. 21, 1906.

All other series are for calendar years.

TS

ee ee

A see

RT

ji
i
:
i
|

 
