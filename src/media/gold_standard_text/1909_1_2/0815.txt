 

ma AT

ai

Be

OTR AT

mip

2 329

hingewiesen hatte, Rechnung getragen wurde. Das Werk hat den Titel: „Destimmung
der Schwerkraft auf dem Schwarzen Meere und an dessen Küste sowie neue
Ausgleichung der Schwerkraftsmessungen auf dem Atlantischen, Indischen und
Großen Ozean.“

An dem früheren Gesamtergebnis hat sich nichts geändert: es entspricht im
großen und ganzen die Intensität der Schwerkraft auf der Tiefsee der Ozeane bis auf
wenige Tausendstel-Zentimeter meiner Formel von 1901, die — abgesehen von der
Nähe der Küsten — für die Festländer gültig ist.

Besonders bemerkenswert ist, daß auch über den Tiefen des Schwarzen Meeres
die Schwerkraft angenähert normal ist. —

In der Enzyklopädie der Mathematischen Wissenschaften, Bd. VI, habe ich
einen Artikel: „Die Schwerkraft und die Massenverteilung der Erde“ erscheinen lassen,
worin auch die Anomalien der Schwerkraft mit den Forderungen der Prarr’schen Hypo-
these verglichen werden.

Nächstdem habe ich mich mit der isostatischen Reduktion der auf dem Meere
beobachteten Schwerkraftswerte beschäftigt. Die Anwendung auf die Hecker’sche
Station auf dem Schelf in der Nähe von San Francisco ist im Gange. He LMERT.

6.

Beobachtungen zur Bestimmung der Bewegung des Lotes unter dem Einfluß von
Mond und Sonne.

Nach Abschluß der Beobachtungsreihe in der Seitenkammer des Brunnenschachtes
der Potsdamer Observatorien, deren Resultate in Kürze gedruckt vorliegen werden, sollte
zur Vermeidung gewisser Fehlerquellen eine Erweiterung des Beobachtungsraumes her-
gestellt werden, eine Absicht, die wegen baulicher Schwierigkeiten aufgegeben werden
mußte. Dank dem Entgegenkommen der Kel. Sächsischen Regierung konnte aber das
Geodatische Institut die Beobachtungen an einem anderen sehr viel geeigneteren Orte
fortführen, nämlich in einem Schachte des Reichezecher Bergreviers in Freiberg i. Sa.
Wesentlich erleichtert wird die Durchführung dieser Beobachtungen durch die freund-
liche Unterstützung der Kgl. Bergbehörde und der Kgl. Bergakademie in Freiberg.

Auf dieser Station wurde nun im Juli, August und September des Berichtsjahres,
dem auf der Konferenz der I. Erdmessung in London und Cambridge angenommenen
Beschlusse entsprechend, eine Vergleichung der zwei für solche Beobachtungen in Be-
tracht kommenden Arten von Horizontalpendeln durchgeführt, nämlich des früher in
Potsdam benutzten Apparates mit Aufhängung der Pendel auf Spitzen und eines
nach dem Prinzip von ZöLLner gebauten Apparates mit Aufhängung der Pendel an
feinen Drähten.

Als Resultat der Vergleichung ergab sich, daß die Pendel mit Drahtaufhängung
bei entsprechender Beschaffenheit des Drahtes für die Beobachtung der Bewegung der
Lotlinie vorzuziehen sind. Die Station der Internationalen Erdmessung in Pribram,

 
