Bremer ee en

43

M. le Président remercie M. Schmidt et donne la parole à M. Gautier.

M. Gautier présente un rapport détaillé sur les travaux géodésiques exécutés en
Suisse, et indique les résultats obtenus par les observations de pendule et par les calculs
concernant l'attraction des masses jusqu'à une assez grande distance. (Voir le Rapport, An-
nexe A. XVII).

M. le Président remercie M. Gautier et donne la parole à M. Titimann, qui présente
un résumé de son rapport volumineux sur les opérations géodésiques exécutées dans les
États-Unis et sur la détermination des dimensions et de la figure de l’ellipsoïde terrestre
déduites des triangulations dans ces États, après les avoir corrigées pour l'attraction des
masses, comme M. Hayford l’a indiqué dans notre réunion à Budapest. D’après toutes les
observations faites jusqu’à l’année 1909, la profondeur de la compensation au-dessous du sol
fut trouvée égale à 122,2 kilomètres. (Voir le rapport, Anuexe A. XX a).

M. Helmert rappelle que par les travaux aux Htats-Unis de MM. Titémann et Hay ford
la géodésie est entrée dans une nouvelle période, signalée par l'application de la théorie
de Pratt aux calculs concernant la figure de la terre. Il félicite ses collèœues d'Amérique
d’avoir inauguré cette nouvelle période. (Applaudissements).

M. le Président remercie vivement M. Tifémann et donne la parole à M. Terao, qui
présente des rapports sur les travaux exécutés au Japon. (Voir les rapports, Annexe A. X a,
A. X6 et A. Xe). M. Nakano présentera plus tard un rapport sur les déterminations de
longitude.

M. le Président remercie M. Terao et donne la parole 4 M. Gillis, qui présente le
rapport sur les travaux géodésiques faits en Belgique. (Voir le rapport, Annexe A. D.

M. le Président remercie M. Gilis et donne la parole à M. Xiso Patron, délégué du
Chili, qui, pour la première fois, assiste à la Conférence. (Applaudissements).

M. Rıso Patron donne le résumé suivant de son rapport.

Dans la partie septentrionale du Chili une superficie de 80 000 km. carrés a été reconnue
et un réseau a été projeté entre les paralléles de 17° et 23°; dans 7 stations de premier ordre
les mesures ont été faites; dans les ports les plus voisins un marégraphe et deux médi-
marémétres ont été installés, et 74 kilomètres ont été nivelés en double. Dans la partie
centrale, une chaîne de triangles, dont les mesures ont été faites à 27 stations, s’appuie sur
une base de 7666 mètres, mesurée sur le rail du chemin de fer avec un ruban en acier
de 49,60 m. Au sud du parallèle de 37°, une chaîne de 13 triangles vient d’être projetée;
on installe un marégraphe à Talcaguano et 49 kilomètres ont été nivelés en double. (Voir
le rapport, Annexe A. Il).

M. le Président en remerciant M. 2250 Patron, félicite la Conférence de l’entrée du
Chili dans notre Association, qui pourra être d’un grand intérêt pour l’avancement de la
géodésie.

Personne ne demandant plus la parole, la séance est levée à 1 heure.

 
