ua han a el Min À

sal da

44 18

un ea  d.

9, Zweites astronomisches Nivellement durch Württemberg im Meridian
8° 33’ östl. von Greenwich. Bestimmung der Polhöhe und der meri-
dionalen Lotabweichungskomponente auf den acht Stationen: Schwen-
ningen, Horgen, Oberndorf, Schopfloch, Durrweiler, Ettmannsweiler,
Wildbad, Schwann. (Im Anhang: Polhöhen in Stuttgart und in Tübingen.)
Im Auftrage des K. Württembergischen Ministeriums des Kirchen- und
ee Cabot on de anmer oo  , , .... . . nn 102 Ex.
10. Untersuchung des Domes in Königsberg 1./Pr. auf Senkungserscheinungen.
Mit einer Tafel. Bureau für die Hauptnivellements und Wasserstands-
beobachtungen im Ministerium der öffentlichen Arbeiten. (Geh. Reg.-Rat
roe Dr Dr sug. Sewr) ... : 0 >
11. Differenza delle longitudini fra lan) Pas dav à ‘i Bene
e Crea punto trigonometrico di 1° ordine della rete geodetica italiana.
Osservaziont di G. Cecorra e M. Rasna. Caucont pi L. GaBBa . . . . . D,
12. Resultate des Internationalen Breitendienstes. Band Ill. Von Te. ALgrzonr i
ind 1B Wissen. Mic 2 Fafeliio. 4e 1, : i 41-5669. , :
13. Relative Schweremessungen ausgeführt im Huftrag Nes Kal. inigfeziums
des Kirchen- und Schulwesens. V. Messungen auf den Linien: 1. Schwen-
ningen bis Erolzheim. 2. Heilbronn bis Crailsheim. 3. Mergentheim bis
Weikersheim. 4. Altshausen bis Wurzach. 6. Fischbach a. B. bis >
: Von Be Koma se. ö ; ; 102
14. Den Danske Gradmaaling. Ny Raekke. Hette N a Sllenish over
Bredere Vandarealer. Udgivet af den Danske En. efter afdode
General Zacnarıaes Manuskript .. . ea sont 100
15. Den Danske Gradmaaling. Ny Babe. Hefte Nr. 5. 6 Bredde-
bestemmelser udforte i Aarene 1890—1892 efter Horrenows Metode

i
a

SS

= at =

 

a 4 tn bin a, ide deb aa ma um

N. tilligemed Resultaterne af Gradmaalings senere. Breddebestemmelser.

: | Udgivet af Generalmajor V. H. OÖ. Mapsen, Direkter for den Danske

R Gradmaaling.. Endelig redigeret af Oberstlojtnant M.J.Sanp . . . . 100 ,
q 16. Mitteilungen des k. u. k. Militär-geographischen Institutes. Heraus-

À gegeben auf Befehl des k. u. k. In XXV IEL. 3

I Bande: 1008.02. IGOR
| 175 Die nisch ähtisähen Einheiten abe L u. i Militär non

Institutes in Wien. XXI. Band. Astronomische Arbeiten. Polhöhen-

 

N und Azimuth-Messungen auf den Stationen: Brasso, Castei Dubica, Hum,

N Kloster, Kloster Ivani¢é, Krimberg, Lagerdorf, Opcina, Peterwardein,

4

iq Seo eccenulounioibenva 1.4.) aula wlan eek à les 96

iM

q 18. Nederlandsche Rijksdrichoeksmeting. Rechthoekige codrdinaten. I. Hoofd-

1 ee Niere sen Man . dan. wi. ulsambesti sans 421105

a 19. Procès-verbal de Ia 55™° séance de la commission geodesique suisse, tenue |
| au Palais fédéral à Berne le 8 mai 1909 avec une annexe (Supplement |
i au Procesverbal’de ta of™’-cséancee div 2 mai 1908) CC, as 96 |
‘| |
L

i

N

 
