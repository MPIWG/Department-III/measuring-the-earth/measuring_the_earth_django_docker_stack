 

|
|

|
|
|

274

wurden, um die Tabelle mit der im „Lotabw.-Ber, 1906” (Verh. in Budapest 1906, II. Teil.
S. 151) enthaltenen in Übereinstimmung zu bringen, aus anderen Quellen ergänzt ').

Lotabweichungen in Breite für Indien,

 

 

   

 

 

 

 

 

 

a | Geogr. | Meeres-
; : : ah: eoD.- xeogr. Länge |höhe in 2
Station Triangulations-Gebiet er Breite Kan eng]. £
Greenwich | Fuss
ra Duin (Hai rva- a0 105 17816 9940 | — 36,6
a (Haig Observa Cuca APE Monidtonal | 30,195 18 6 2240 36,68
Dangarvadi 90 43,0 | 70 59 96 | — 8,52
Kunkavar | Kathiawar Meridional 21 39,2 10759 21, Sal | „ 21,68
Dungarpur oe 1 0 nn = 269
Chamardi Kathiawar Minor Longi-; © (21492) m 58 a
tudinal { | | |
Ingrodi | Ce Pe ee 199 574 Ms | 118 — 508
Paldi ee rom 3 | og 2 547
Zi | ; re c QAC
Ghorarao | | 22 52,2 18240016 6828 0 — ae
Pavagad DO 19, 33 27214 | — 4,
Sidhpur ) | Singi Meridional | 92 23 | 713 831 169 | 9344
Alamvadi | | | a 326 | n | — eee
Tarbhan | 22 0606| 8 6 14 — 5,77
ve a ee
amanva “a: en | co 123 32,1 | cos | — 9,14
ail Abu Meridional Series | = (93 51,4 ie 1. | nn aus a
Kardo Do i 20 280m | > 1,80
Lakarwäs | Verbindung der Karachi| mm «| D4 31,8 Lye 73:02 | 2574s | —-...6,94
Tiki Longitudinal und a) 2 | 24 55,6 13 53 | 2369 | — 3,72
SingiMeridional Series) | | | |

Ferner wird berichtet (Extracts ete. 1906—07, S. 134—136), dass mit der Berech-
nung des Einflusses der Attraktion der sichtbaren Massen auf die & begonnen worden ist.
Es sollen dabei die Massen bis zu 3000 engl. Meilen im Umkreis von Kaliänpur berück-
sichtigt werden. Für 7 Breitenstationen sind bereits vorläufige Werte der durch die ozeani-
schen Teile des Gebiets verursachten Ablenkungen berechnet worden (a. a. O., 8. 1385). Für
4 von diesen Punkten waren auch schon im „Professional Paper Nr. 5°?) die Attraktions-
wirkungen berechnet worden; die hierbei aus den Ozeanen allein sich ergebenden Beiträge
zu den Ablenkungen in Breite, im Betrage von 18” bis 30”, weichen von den neu ermit-
telten bis zu 8” ab, wobei jedoch zu beachten ist, dass damals der Einfluss der Ozeane nur
angrenähert berücksichtigt wurde.

Eine Zusammenstellung sämtlicher bisher in Indien bestimmten Lotabweichungen in
Breite ist von Herrn Oberst $S. G. Burrarn 1906 im XVII. Bande der „Great Trigono-

1) Extracts from Narrative Reports of Officers of the Survey of India for the season 1906—07. Calcutta 1909.
S. 132— 137; dasselbe für 1907—08. Calcutta 1910. S. 98--103.

2) S. G. Burrarp, The Attraction of the Himalaya Mountains upon the Plumb-line in India. Dehra Din 1901.

(Survey of India Department. Professional Paper Nr. 5).

ve he UD EE

hehe aa

 
