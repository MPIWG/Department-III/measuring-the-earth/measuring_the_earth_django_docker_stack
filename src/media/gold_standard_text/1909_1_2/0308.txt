 

3 es

272

En déplaçant une croisée le long de l’autre à l’aide de la vis micrométrique, on
peut exactement superposer la surface plane de [a molette à la surface plane verticale du
comparateur et par la rotation de l’écrou à quatre manches, qui se trouve en haut de la
croisée, on peut lever ou abaisser la molette et ainsi établir exactement dans les foyers des
microscopes extrêmes, l’échelle du fil suspendu sur elle.

La partie essentielle de la console est la molette, qui se tourne sur des globules.
Le frottement de cette molette doit être aussi faible que possible, parce qu'il peut provoquer
une tension inégale du fil sous l'influence du même poids, et une augmentation de la tension
d’une gramme allonge le fil de 1.28 microns. On a prêté une grande attention à ce
sujet et les molettes, commandées à l'atelier de PÈque à St. Petersbourg, ont un frotte-
ment 11}, fois moindre que celles qui étaient envoyées par l'ingénieur CARPENTIER avec
les fils, confectionnés par lui-même.

Par-dessus la mortaise de la molette est lancé un cordon; à l’une de ses extrémités
est attachée une carabine pour le fixer aux fils, et à l’autre un gond pour suspendre le

poids de 10 kilogrammes. Le gond est attaché au fil de manière qu’au moment où la

division moyenne de l'échelle se trouve sous le microscope, le poids est éloigné du plan-
cher d’environ © centimètres.

La qualité du cordon a une grande influence sur la commodité et l'exactitude des
travaux du comparateur; le cordon doit être suffisamment souple et d’une épaisseur égale
sur toute sa longueur, parce que le manque de souplesse augmente le frottement pendant
la rotation de la molette et l’épaisseur inégale du cordon donne des résultats encore moins
désirables.

Des expériences faites sur des fils divers ont constaté, qu’un fil en cuivre rouge,
ayant un diamètre d'au moins un millimètre, répond le mieux aux exigences. Un fil avec
un diamètre de {mm est suffisamment solide et souple; pendant l’étalonnage il ne provoque
(sur le comparateur) aucun déplacement de l'échelle sous les microscopes.

La perfection du comparateur de Tachkent s’est manifestée aussi dans l’amélioration
de l’eclairage électrique du champ des microscopes. Au lieu des lampes transportables
qu'on à employées jusqu'ici, on obtient l'éclairage par une batterie immobile, de la raison
danoise d’ÆELLESENS-ENKE, composée de six éléments secs de 11/, volts chacun, qui sont
connus par leur grande durée.

Les éléments sont assemblés en série et établis sur un rayon au-dessus du milieu du
comparateur; partant de la batterie sont placés le long du mur trois principaux fils conducteurs
isolés, dont le fil supérieur est uni avec la pince droite de la batterie, le fil du milieu avec
celle du milieu et le fil inférieur avec la pince gauche. De ces principaux fils conducteurs
on a pris des branches vers les lampes, qui sont installées sous les consoles de chacun des
neuf microscopes (fig. N°. 2).

Du coté droit de chaque console est attachée au mur une pièce pour faire passer le
courant de telle façon, que la pince droite de la pièce est unie avec le fil conducteur d’en
haut, que la pince du milieu, sur laquelle se trouve l’axe de la manche, est unie A travers
la lampe avec le fil du milieu et enfin que la pince gauche est unie avec le fil conducteur

 

ch

vn ra Le Me Bu | ui

NT

ah ind mr

u

Ih

|

    

1 kann nmi ah a

2.4 dia man MA |

 
