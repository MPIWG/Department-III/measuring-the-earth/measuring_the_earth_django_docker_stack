 

334

ra aM A a Na

gegenüber seien. Eine besondere Veranlassung zur Besprechung dieser Frage gibt mir auch
eine sehr beachtenswerte Arbeit des Herrn Professors A. Venturı!) der auf Grundlage
kritischer Behandlung meiner Methode auch seine Meinung über die Leistungsfähigkeit
derselben ausspricht. Ich hoffe, dass Herr Professor VENTURI, wenn er zur Kenntnis dessen
gelangt, was ich hier schon gesagt und noch zu sagen habe, sich überzeugen wird, dass
unsere Ansichten nicht weit von einander abstehen.

Die zwei vorangehenden Kapittel dieses Berichtes beschäftigten sich schon mit diesem
) Gegenstande. Als Ergänzung wollen wir aber jetzt die neuen und die älteren Methoden
l noch besonders in Hinsicht auf die Aufklärungen beurteilen, die sie über Massenverteilungen
zu geben berufen sind, welche dem Auge unsichtbar sich nur durch ihre Anziehung er-

 

kennen lassen.
Solche Aufklärungen können nur auf Grundlage „Synthetischer Untersuchungen

über den Einfluss gegebener Massen” erzielt werden, wie sie Herr Geheimrat F. R. HELMERT

im zweiten Bande seiner „Theorien der höheren Geodesie’” ausführt.

ji Im engen Rahmen dieses Berichtes will ich von allen möglichen Fällen der Massen- i
N verteilung nur einen einzigen, zum karakteristischem Beispiele geeigneten besprechen. Daran 1
u sollen aber auch Betrachtungen von mehr allgemeiner Bedeutung geknüpft werden.

| Ich nehme an, dass unter den lockeren und wenig dichten Aufschüttungen einer
i Ebene (wie das ungarische Tiefland) sich ein dichter Gesteinsboden und ein von diesem
| gebildeter langer gerader Gebirgszug nebst parallel mit diesem fortlaufenden Tale befinde.

i Die Figur 3 stellt das Querprofil dieser angenommenen Massenverteilung dar.

In dieser Zeichnung ist die der ebenen Oberfläche entsprechende Gerade in aequi-
distante Teile zerlegt, die an den Teilpunkten angeschriebenen Zahlen bezeichnen ihre vom
Punkte 0 in Kilometern gemessenen Entfernungen. Der Verlauf und die Tiefenlage des

Profils der dichteren Gesteinsmassen sei der folgende:

re er

==

von a bis 5 in 500 Meter Tiefe, horizontal
eo, oum 250 ‘„ ansteigend

ju 00 am is de D dh a a sn

D TR RO mt

ee
Le
Ss

\ ee du 200 „  abtallend

N ae im 300, Trek, horizontal

k 2, am 000 À ablallend

4 | , 3 um 900 , ansteigend

N DU, 7 in o00 „ Miele horizontal

I

q Die Dichte der den Boden bildenden Gesteine sei 2,6, die der darüber gelagerten
4 Erdmassen 1,8. Unter Annahme des in der Richtung s sich bis ins Unendliche erstreckenden

4 Gebirgszuges und Tales, können die auf s normalen Schwerestörungen leicht berechnet
q werden, Die Resultate der Berechnung sind in der beigedruckten Tabelle zusammengestellt.

 

N 1) A. Venturt. Teoria della bilancia di Torsione di Eörvös. Palermo, 1908.

 
