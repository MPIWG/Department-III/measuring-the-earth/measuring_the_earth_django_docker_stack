 

78

Erreurs de clôture des triangles, dont les trois angles ont été mesurés.

 

 

 

N°.

. d’ordre

Auteurs des instruments

employés
et
diamétre du cercle
horizontal

2 a
n Be eye
Lecture en | & oy ESS
secondes sa Sen
du vernier | #2 |8 9.8 © 73
= SS he
ou du iS 223283
. CE © © & © a
microscope | 2 8 on
D
3°. fo

Erreurs de
clôture de
chaque triangle

At

Remarques

& CO 2 OT SE WD

10
41
12
13
14
15
16
17
18
Ig
20

al

29
93
24
25
26
27
28
29
30
31
32
33
34
35
36
Sl
38
39
40
41
49
43
44,
450
4.5

 

 

Julius Wanschaff
a Berlin
35 centimètres

 

 

roi

Lecture di- 9 94

recte de 1”

Les parties

décimales ont

été estimées
id. 2 24
id. 2 94
id. 9 24
id. 2 94
id. 2 94
id. 2 24
id. 2 24
id. 9 94
id. 2 94
id. 2 24
id. 2 94
id. 9 24
id. 2 24
id. 2 94
id. 2 94
id. 9 24
id. 2 94
id. 9 24
id. 9 94
id. 2 94
id. 2 94
id. 2 94
id. 9 24
id. 9 24
id. 9 24
id. 9 94
id. 2 94
id. 2 94,
id. 9 24
1d. 9 24
id. 9 24
id. 9 94
id. 9 24
1d. 9 94
id. 2 24
id. 2 24
id. 9 24
1d. 2 94
id, 9 24
id. 9 24
id, 2 24
id, 2 24
id, 2 24
id. Q 24

 

Troisième groupe: [A?] — 21,9438 n= 45

Les trois groupes combinés 63,2704 n= 119

 

isième groupe.

0,010

0,115
0,553

0,419
1,003

0,527
0,685

0,088
0.143
0.361

0,086

0,316
0,586

0,090
0,097
0,781

0,724
0,364

1,057
0,215

1,089

1,756
1,004

 

 

 

S Ai
m= |/=4 LC

 

 

0,0001 ° L’erreur de clôture du
quadrilatere N°. 45 ayant
le poids 1/,, tandis que
les erreurs de clôture des

triangles ont le poids '/s,

0,0132 le carré de cette erreur

0,3058 a été multiplié par 3/,.

0,520 0,2704
0,134 0,0180
0,433 0,1875
0,1756
1,0060
0,839 0,7039
0,2777
0,4692
0,843 0,7106
1,085 1,1772

0,0077

0,0204

0.1303

0,683 | 0,4665
0,489 0,2391
0,0074

0,538 0,2894
1,166 1,3596
0,575 0,3306
0,291 0,0847
0,0999

0,3434

0,376 0,1414
0,409 0,1673
1,792. 3.2113
0,0081

0,495 0,2450
' 0,0094

0,6100

0,5242

0,1325

0,035 0,0012
0,110 0,0121 :
1,184 1,4019
1,1172
0,0462
0,235 0,0552
1,1859
0,189 0,0357
CU 0,5055
3,0835
0,7560
[A*] == 21,9438
21,9438 an /0.1695 — ()".403
en 135 = V 0,1625 ,40
63,2704

a

 

== Y0,1772 == 0,421

bd ld hdl A a Nk he

sats

au hell hs au

 

 
