 

 

24

La moyenne des différeuces des Ag, aller et retour, est pour les 6 stations en mer
— 0,135 em.; d’après le premier calcul sans correction cette moyenne était — 0,007 cm.

Ces nombres indiquent que la théorie est encore incomplète, et qu’il doit exister une
compensation de l'influence du mouvement horizontal du vaisseau.

La moyenne de Ag de toutes les observations est:

Mouvement du Ag
bateau avec correction sans correction
u. vers Vest . . + 0,083em. + 0,048 cm.
» Pacifique: Sydney—San Francisco. » » | . -+ 0,060 — 0,005
» » San Francisco— Yokohama » l’ouest. . — 0,074 + 0,009.

Ces résultats ne justifient non plus la correction pour le mouvement horizontal du
vaisseau.

6.
DIVERS.

M. le lieutenant Awnastasiu de Bucharest, qui était venu au Bureau central en
1907, y est resté en 1908 pour continuer ses études astronomiques; il y a profité de
l'instruction de M. le Prof. Dr. Gare et de M. le Prof. ScHNauDer.

M. le Dr. Luctan Grasowskı de Cracovie etait pendant la période de Janvier jusqu’à
Mai temporairement au Bureau pour s’instruire; il travaillait surtout avec M. le Prof.

Dr. Hecker.
Depuis la fin d’Avril M. le Prof. Jorvan D. Kovaronerr de Sofia prenait aussi part

aux travaux de M. Hucxer.

_ Après que, pendant la période de Janvier a Avril, M. l'ingénieur diplomé K. OrrTay avait
appris sous la direction de M. le Prof. HaasemMann a faire des observations avec un ap-
pareil à quatre pendules de Srückrarx, dont il avait aussi déterminé les constantes, il a
exécuté au Bureau central pendant la dernière moitié du mois d’Août des observations dans
le but d'obtenir un raccordement avec Budapest. [Il a aussi déterminé avec M. le Prof.
HaAAsEMANN, au moyen d'un examinateur de Wanscuarr, les erreurs de division de 4 à 4
degrés du cercle vertical de 13 pouces d’un altazimut appartenant à la commission géo-
désique hongroise.

Depuis la fin du mois de Juin jusqu’à la fin du mois d’Août, M. le Dr. Prey et en-
suite M. le. Dr. Jascuxe se trouvaient à l’Institut géodésique pour y déterminer, de la part
de la commission géodésique autrichienne, la différence de longitude Vienne— Potsdam.

 

te she Ae AA LS a eh a haider

sins nnn a Adal ob dl cn

ameter, ana di |

 
