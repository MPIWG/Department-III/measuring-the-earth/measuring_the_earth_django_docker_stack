 

152

frei gegeben wird. An seine Stelle wird der Telegrafenberg bei Potsdam treten, auf den
die alten Positionen mit möglichster Schärfe übertragen werden sollen. Die endgültige
Orientierung der Preussischen Netze erfolgt alsdann durch eine breite Verbindungskette,
welche bereits erkundet ist und mit deren Beobachtung im nächsten Sommer begonnen wird.

Als Ausgangswert für die Seitenlängen des Polygons um Berlin ist im vorigen Jahr
in unmittelbarer Nähe von Berlin eine Grundlinie gemessen und ihre Länge auf eine Seite
des Polygons übertragen worden. Die Grundlinie ist rund 8113 m. lang; sie wurde nach
Analogie der früheren Messungen mit den Basser’schen Stangen je einmal hin uud zurück-
gemessen, wobei sich eine Uebereinstimmung auf 5,2 mm. ergab. Die gesammte Messung
dauerte 8 Tage bei einer durchschnittlichen Tagesleistung von 2028 m.; die grösste Tages-
leistung betrug 2497 m. Als mittlerer Fehler ergab sich:

für die Grundlinie selbst aus den Strecken berechnet 1,68 mm. oder 1:4830000 der Länge,
für die abgeleitete Dreiecksseite 88 mm. oder 1: 637000 der Länge.

Letztere Genauigkeit steht hinter der der meisten Basisnetze etwas zurück; zurück-
zuführen ist dies auf die Beobachtungen der Station Müggelsberg, deren Ergebnisse durch
örtliche Einflüsse die nicht zu umgehen waren herabgedruckt wurden.

Diese neue Berliner Grundlinie wurde durch Winkelmessungen in Verbindung ge-
bracht mit der alten Berliner Grundlinie von 1846 sowie auch mit derjenigen des Geodä-
tischen Instituts vom Jahre 1880. Der Unterschied Grundlinie 1846, abgeleitet aus Grund-
linie 1908, minus ursprüngliche Messung 1846 beträgt — 5,6 mm. Bekanntlich fügte s. Zt.
General Bazyer dem ursprünglichen Wert, da er dem Vergleich der Messstangen im Jahre
1846 nicht recht traute, eine Korrection von 31,7 Einheiten der 7. Stelle des log. hinzu.
Er näherte sich damit wieder dem alten Kénigsberger Stangenvergleiche. Ferner erreichte
er damit eine sehr gute Uebereinstimmung der aus Berliner und Königsberger Grundlinie
abgeleiteten Seite Trunz-Wildenhoff, nämlich auf 4 Einheiten der 7. Stelle des log., Linge
ausgedrückt in Toisen (4,4789086 gegen 4,4789090). Legt man diesen so veränderten Wert
der alten Berliner Basis zu, Grunde, so ergibt sich: 1908 ermittelte Länge minus Länge
nach Baryer = — 22,6 mm. Herr Geheimrat Hzımerr schlug später einen Mittelweg vor,
indem er einschl. Verwandlung in internationale Meter eine Korrektion von + 70,4 Ein-
heiten der 7. Stelle des log. empfahl, hierbei aber betonte, dass er es für das richtigste
halle, für die Länge der Grundlinie 1846 nur die 1846 bestimmten Konstanten zu verwenden.
Mit der erwähnten Henmenrv’schen Korrektion beträgt Grundlinie 1846, abgleitet aus 1908,
minus Linge nach Hetmert = — 12,3. Der 1908 ermittelte Wert für die alte Berliner Basis

stimmt also am wenigsten gut mit dem Wert von Banver, wesentlich besser mit dem

Wert von HELMERT, am besten — und zwar innerhalb des aus den Beobachtungen zu er-
wartenden mittleren Fehlers — mit dem unter Zugrundelegung des Stangenvergleichs von
1846 ermittelten Werte. Die Hutmert’sche Ansicht vom Beibehalt dieses ursprünglichen
Wertes wird also durch die Ergebnisse der neuesten Messungen bestätigt. Eine weitere
Stütze für die Richtigkeit dieser Annahme gibt folgendes: Die schon erwähnte Seite Trunz-
Wildenhoff ist gelegentlich der Neumessungen in Ost- und Westpreussen erneut bestimmt

A ee ul

1101 tel

LT

 

 
