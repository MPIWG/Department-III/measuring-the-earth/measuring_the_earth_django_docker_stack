Ak Mn hauen dé

ANNEXE B. I.

van shel de 1.148 441.188

 

RAPPORT

SUR

LES TRAVAUX DU BUREAU CENTRAL DE L'ASSOCIATION GÉODÉSIQUE INTERNATIONALE

en 1908

| ET PROGRAMME DES TRAVAUX POUR L'EXERCICE DE 1909 !)..

| A. Travaux scientifiques. i
1. Calculs relatifs au système des déviations de la verticale en Europe. i
7 7» + 3° N 3
2, Recherches sur la courbure du géoide le long des méridiens et des parallèles. ;
| 3. Service international des latitudes.

L 4, Déterminations relatives de la pesanteur au moyen de pendules.

sy 5. Determination de la pesanteur en mer.

| 6. Divers.

| 1.

| . RAPPORT SPÉCIAL SUR LES CALCULS RELATIFS AU SYSTÈME DES DÉVIATIONS

DE LA VERTICALE EN EUROPE.

juni ha a Me Ah amas um

| Les travaux et les caleuls qu’on a l’intention de publier dans le 4° cahier des
déviations de la verticale (Heft IV der » Lotabweichungen’”’) furent termines en 1908; sauf
| quelques petits détails, le manuscrit est prêt à imprimer. On a calculé non seulement les
I lignes géodésiques et les polygones indiqués dans le dernier rapport, mais aussi la ligne
Springberg—Todtenberg—Breslau, divisant en deux parties le grand polygone s’eten-
| dant de Leipzig jusqu’à Varsovie, qui fait partie de l'arc du parallèle de 52°. On pouvait
| examiner ainsi d’une maniére plus efficace l’exactitude des résultats obtenus pour les dévia-
tions de la verticale aux stations qui se trouvent dans la direction de Varsovie. Quant
| aux erreurs de clôture, le résultat de ces calculs a été fort satisfaisant, puisque l'erreur
| de l'équation de Laprace pour la ligne Springberg—Todtenberg—Breslau n’a qu’une valeur
moyenne et que les trois erreurs de clôture assez fortes du grand polygone ont été à

RES

1) De chaque compte rendu des travaux accomplis en 1908 résulte le programme des différents travaux qu’on

se propose d'exécuter dans le même domaine en 1909.

{
i
|

 
