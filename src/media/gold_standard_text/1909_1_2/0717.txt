PP En EE |

(090 RER en

man dl

ya la

lie

=
x
z
x
=

241

England, Dover (Rh)
Eastbourne (R)
Fleetwood (misprinted Heetwood in 1903)
Hartlepool
Holyhead
Hull
Liverpool (St. George’s Pier, end Helbre Island) (R)
Middlesbrough
Newhaven
Newcastle on Tyne
North Shields (R)
Ramsgate
Sheerness (A)
Southampton
River Thames (mostly R), Southend, Gravesend, Crossness, North Wool-
wich, Deptford, Blackfriars Railway Bridge, Chelsea-Bridge, Hammer-
smith-Bridge, Richmond Lock.

Scotland, Glasgow (five gauges under Clyde Trust) (R)
Grangemouth

| Leith

Ireland, Cork
Dublin
Kingstown

It is not known with certainty that all these gauges are still in operation.

With respect to the ,,Crown Colonies” a circular letter was drawn up and was sent out by
His Majesty’s Secretary of State for the Colonies; the following answers have been received.

Nassau, Bahama Islands. — Tide-gauge in Colonial Hotel Basin, one eighth of a
mile east of the Navy Yard, Nassau; installed June 1908, but not still in operation. The
observations were made by Mr. W. ©. Townsunp, for Dr. O. L. Fassie of the Geographical
Society of Baltimore, U.S. A.

The observations for the years 1903 and 1904 were reduced, and results are given
in „The Bahama Islands” edited by Professor. G B. Suarruck, and published by the Geo-
graphical Society of Baltimore.

British Honduras. — Tide-gauge at the mouth of the Haulover Branch of the
Belize River. Installed June 1908, and still in operation.
The observations are made by the Public Works Department, but the records are not kept.

Trinidad and Tobago. — A tide-gauge was in operation from early in 1896 to the
end of 1898 at the Light House Jetty, King’s Wharf, Port of Spain.

 
