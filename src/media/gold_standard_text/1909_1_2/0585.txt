 

in!

TOTO ATR DR LET Jim Ti

mp

109

Avant de décrire les opérations de mesure effectuées par la mission, nous allons in-
diquer quels étaient les éléments déduits de la triangulation du Serv. géogr. de l’Indo-Chine,
et qu'il s'agissait pour nous de vérifier. Ces éléments nous ont été communiqués sur place,
eu Décembre 1905 par M. le Capitaine du génie Sonrprn chef de la Section de géodésie,

En prenant pour point de départ la longueur de la base de Thanh Hoa 6662™,193308
(log.: 3.8236172) le calcul des triangles de la chaîne möridienne de Thanh Ho
au côté Thanh Shaoc-Hanoï sur lequel s'appuie la chaîne du parallèle de H
gueur: (43866209). Le calcul de cette dernière cha
Haiphong les valeurs suivantes : ei

a-Sontay donné
aiphong la lon-
ine fournit alors dans le voisinage de

Nui Son Dao (Nui Deo) — Nui Con Voi. ..... . (4.1584431)
Nui Son Dao — Nui Do Son (Ga Mirador). (4.4415977) |

C'est au premier de ces côtés qu'a été rattachée la base de Haïphong. Sa longueur
a été déduite de deux triangles: Terme Nord-Keou Vien(morne R)-Terme Sud et Terme.
Nord-Nui Deo-l'erme Sud qui donnent respectivement

. 6308,51 (log.: 3.7999270)
6308,33 (log.: 3.7992145).

Le premier de ces triangles étant beaucoup mieux conformé que le second nous avons
donné un poids double à la détermination qui en résulte et nous ayons admis par consé-
quent, que la longueur de la base de Haiphong déduite de celle de Thanh Hoa par la
triangulation du Service géographique de l’Indo-Chine était de 6308,45 m.

Telle était la valeur qu’il s'agissait de vérifier par une mesure directe.

Emplacement de la base. — La base de Haiphong s’étend dans les rizières
comprises entre la digue du Cua Cam et celle du Cua Lach Tray. Son terme Nord se trouve-
à environ 4500 mètres à l’est de la gare de Haïphong, un peu à l’ouest de la cabane du
câble télégraphique. Il est constitué par un pilier de briques maçonnées dont la face supé-
rieure effleure le sol de la digue. Contrairement aux indications, d’ailleurs un peu dubita-
tives, du Service géographique, aucun repère métallique n’a été retrouvé à l’intérieur de
ce pilier. Nous avons alors admis comme terme Nord de la base le centre même du pilier :
la construction étant fort régulière, l'incertitude résultant de ce fait ne pouvait êlre que
peu appréciable. Ce centre a été alors matérialisé au sein du pilier par une croisée de traits
gravée sur la face d’un cylindre de cuivre encastré dans une borne en granit qui a été
cimentée dans la fondation. |

Le terme Sud de la base est constitué par un pilier analogue à celui du terme.
Nord, mais émergeant de la digue du Cua Lach Tray, à 4000 mètres environ du pare du
Lach Tray, en descendant le fleuve. On a retrouvé dans les fondations de ce pilier un culot
de cartouche solidement cimenté, qui a été respecté. jus

Les rizières que traverse la base sont presque toutes consacrées à la culture dite du
dizième mois, les circonstances les plus favorables à la mesure se sont rencontrées au mois

 
