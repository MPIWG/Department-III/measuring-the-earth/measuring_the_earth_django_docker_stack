tram en were PAR PTT pr on

PE PERTE ovine

rn

Te pe ten

4
1

i
|

 

167

§ 3.

Aus diesen Unterlagen erhielt ich die folgenden 28 y-Gleichungen; sie werden weiter-
hin auch als ,,Fehlergleichungen” bezeichnet.

Die Unterschiede zwischen den absoluten Gliedern der Fehlergleichungen der drei
Paare von Nachbarstationen: Nieuport-Rosendaël, Rauenberg-Berlin und Breslau-Rosenthal
betragen nur wenige 0’.1; um nicht einzelne lokale Abweichungen doppelt vertreten zu
haben, wurden zur weiteren Ausgleichung gliedweise gemittelte Gleichungen benutzt.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Nm 5
Station Index Fehlergleichungen Darstellung

Feaghmain 1 | +098994, —-0.93819u = 3663 "0100 7 3598 00e
Killorglin a + 0.9899 — 0.2188 + 2.266 —0.1334 = 4a + 9.494 — 0.0950
Haverford west 2 + 0.9975 — 0.1112 + 0.699 —0.0678 =» + 1.672 — 0.0701
Greenwich 0 —- 1.0000 0.0000 0.000 0.000 = + 1.808 — 0.0441
. + 0.9994 +0.0580 -— 3.876 005] a. | tar 0
Bruxelles b -+ 0.9979 -- 0.0980 — 3.144 1200591 = — 0.609 — 0.0218
Ubagsberg € + 0.9966 +01335 .— 7.191 + 0.0806 = #e — 4.395 — 0.0134
Bonn 3 + 0.9952 + 0.1590 — 7.457 10059 = — 4.472 — 0.0077
Brocken 6 —- 0.9893 -1 0.2372 — 1.280 +0.1481 = 4 + 2.278 +.0.0104
Göttingen i + 0.9908 + 0.2224 — 6.408 +0.13842 = 4, — 2.957 + 0.0070
Leipzig 8 + 0.9858 +. 0.2762 — 1.050 0.1669 = 4 + 2.793 + 0.0197
Grossenhain 9) + 0.9829 + 0.3028 — 6.583 +0.1828 = m — 9,552 + 0.0259
Schneekoppe 10 —- 0.9770 + 0.3504 — 4221 +02117 = #0 + 0.158 + 0.0371
D ; ; +0.9736 +0,3788  — 0.995 + 0.9994 = sun | ono) 0
Trockenberg 15 +. 0.9672 + 0.4188 — 5.187 + 0.2530 = 93 — 0.315 + 0.0531
Mirow 14 | +09672 +04961. — 9458 - 09608 2, ok oe
Ravens + 0.9891 4.0.2981 = 2196 10.1790 = | dl 808 on
Springberg N -- 0.974] + 0.8695 — 8469 40224] = 7 +1.048 -+ 0.0424
Schönsee 18 | 09607 10491 — 7.359 10940 m. | 200 00
Warschau 19 | 409587 +0.4651 — 2.898. 10.9895 = 4, | 200 vue
Grodno 20 + 0.9464 + 0.5247 — 1.898 +03198 = woo — 9.974 + 0.0810
Bobrysk 21 0.9198 -- 0.6370 — 5.158 102895 = 45, +1255 + 0.1098
Orel 92 -- 0.9217 + 0.7870 — 9.618 + 0.3904 = y9 +4.919 + 0.0542
Lipetzk 28 + 0.9245 + 0.8668 = 7004 ae a. + 0.482 + 0.0254
Ssaratov 24 1. 0.9538 OT — 9804 7023958 = yoy -- 6.476 — 0.0280
Ssamara 25 +. 0.9198 + 1.0915 — 195253 1702895 = 7. — 2.720 — 0.0609
Orenburg 26 == 0:93 15 + 1.2165 2 0980 109946 7, 1.378 — 0.1032
Orsk mall + 0.9358 — 1.2993 — 19.861 +039%4 = 4, — 8.478 — 0.1326

Die Unbekannten „, einerseits, u und £, andererseits gehen hier in verschiedener

Weise ein; während die Koefficienten von y, nur zwischen + 0.9 und + 1.0 schwanken,
wachsen die von u und &, allmählich in fast gleicher Weise an und zwar ist dieses Ver-
halten begründet in der Form der Koefficienten von u und & in den ursprünglichen Lot-

*

22
