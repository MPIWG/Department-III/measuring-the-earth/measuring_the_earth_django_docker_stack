SER

a

nia

 

eS iS SS aes

168

abweichungsgleichungen zwischen benachbarten Stationen. Der Koeflicient von &, (q, in der
Bezeichnung der „Lotabweichungen”) enthält den Faktor sin], der Koefficient von u (q;)
dagegen 1 selbst, wo 1 die Längendifferenz zweier benachbarter Stationen ist; die übrigen
Faktoren ändern sich längs einem Gradmessungsstreifen auf einem Parallel nur wenig. Um
die entfernten Stationen auf die Referenzstation zu beziehen, müssen bekanntlich die Glei-
chungen zwischen den Nachbarstationen aufsummiert werden, und da 1 der Konvergenz
wegen nur wenige Grade gross genommen werden’ darf, so bleiben die Faktoren von da
(oder von u) und &, auch nach der Summation einander nahe proportional; diese beiden
Unbekannten bleiben demnach miteinander verquickt. Um dem zu begegnen, müssten andere
Gleichungsysteme zwischen ihnen, etwa aus „Breitengradmessungen” oder aus „Gradmes-
sungen schräg zum Meridian”, allgemein Messungen an Stationsfolgen mit wechselnden
Breiten zugezogen werden.

Ferner bleiben in obigem System die Koeflicienten von & von Bobrysk ab nahe
konstant, während die von u weiter wachsen; dies hängt damit zusammen, dass von dieser
Station ab die £-Glieder der y-Gleichungen ebensowenig wie die &-Gleichungen selbst ein-
geführt worden sind, s. o. 8. 162.

Bei einer Reihe früherer Ausgleichungen, z.B. auch bei jener, deren Ergebnisse in
der Abhandlung: „Die Grösse der Erde’’ benutzt sind, wurde auf zweierlei Weise versucht,
&, zu eliminieren: einmal wurde, wie schon oben am Schluss des & 1 erwähnt, aus den
gegebenen &-Gleichungen des Längenbogens zwischen Feaghmain und Bobrysk eine Normal-
gleichung gebildet und damit &, eliminiert; zweitens wurde das der Ausgleichung des west-
europäischen Meridianbogens !) entstammende &, mit benutzt. Sämtliche bisher abgeleitete
Werte von &, schwanken zwischen — 2” und — 7” ?), was darin begründet sein dürfte, dass
die dazu benutzten zwei Gradmessungsbögen in dieser Hinsicht noch unzulänglich sind.
Herr Hetmert schlug deshalb vor, &, bis zum Ende der Ausgleichung unbestimmt zu lassen,
so dass y, und u als Funktionen von &, erscheinen. Nach Heranziehung genügend vieler
anderer, unter einander verbundener Bögen kann dann ausser definitiven Werten von &, und
4, auch der definitive Wert für das da des 52. Parallels (oder für einzelne Teile desselben)
berechnet werden.

Damit erhielt ich folgende Normalgleichungen nebst Lösungen und Kontrolle.

En, 10e = 128.47 1523478, = 0
anal 2 9128 37.72 63.9057. = 0
eo 84.624 4 17402 2 — 0

u — + 7.4600. — 0.87538 £,

Mo = + 1.8077 — 0.044147 8,, Gewicht 13.356
ee eee See, OL 0109498>
BL Biss, 10.1094 >
1) Der neue westeuropäische Meridianbogen. Sitzungsberichte der XV. Generalkonferenz der Internationalen Rrd-

messung, Budapest 1906; Leiden 1907. S. 19.
2) Längengradmessung II. S. 205.

Ha hau | u aid Li

1188411188

wit oad

 

i
|
|

 

 
