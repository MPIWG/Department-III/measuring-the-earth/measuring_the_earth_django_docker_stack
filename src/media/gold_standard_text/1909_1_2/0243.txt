Feen 4

TE PTR DUCTION er Tee rare

221

 

The measurement of the base itself was deferred pending the arrival of a 2'/, metre
standard, obtained on loan from the Russian Government at the request of the Royal Society
of London ‘).

Accordingly after a certain amount of line-clearing and beaconing had been done
and astronomical observations had been made at Msambansovu for latitude and at Kawira
for latitude and azimuth, Dr. Rusın was instructed to demarcate the Portuguese boundary
running due south from the Zambesi, near Zambo, and to fix by astronomical observation
the point where the 15th parallel of latitude crosses the river Loangwa. These operations
were undertaken to facilitate the work of the Anglo-Portuguese boundary commission and
were not completed till April 1904 — meanwhile Mr. G. Tyreeır Mc Caw had been ap-
pointed Chief Assistant to Dr. Rusin and left for Feira, via Salisbury on October 5, 1903,
having spent a month at the Cape observatory for farther training in astronomical work.
He took with him the spare theodolite and nickel-steel wires for the Jipmrin apparatus.
It is unnecessary here to trace the historic progress of the work in farther detail, it is
sufficient to say that it was pressed forward steadily, - often in circumstances of great
difficulty, on the one hand from the smoke of grass fires in dry weather and on the other
from difficulty of transport after heavy rain. In 1906 it became necessary for various reasons
to bring the work to a close. The available funds were exhausted and the staff was ordered
to return February 1906. Dr. Rupin persevered however for some time at considerable self-
sacrifice, carrying on the work unaided to Mpange, latitude — 9° 40° 57”, when he was
perimptorily recalled.

The triangulation accomplished is shewn on the general map, N° I.

One base line has been measured in the Luangwa valley (latitude — 15° 10’), and
it was intended to measure another about latitude — 10°, a project which had to be given
up for want of time.

Astronomical azimuths and latitudes were determined at Masombansovu and Lovusi,
and latitudes only at Kapsuku, Koulashishi, Machecheti, Mkokomo, Chifikunya, Kweshi,
Ulungu, Mtsense, Mtsengulu, Mabylo, Moyenze, Lovusi, Iwangwe, Chipala, Makowonshi,
Kongawakadi and Mpange.

In the report of the Geodetic Survey of Southern Rhode: (Vol. III) the coordi-
nates of all the stations were referred to the point Salisbury, as origin, the coordinates of
Salisbury having been preliminarily fixed by a telegraphic determination of its astrono-
mical difference of longitude from the Cape observatory, and astronomical determinations
of its latitude and azimuth. The subsequent completion by Captain Gorpon R.E. of the
chain of triangles connecting the points Standaus-Wedza of the Southern Rhodesian chain
with the Northern points, Pont and Dongola, of the Transvaal system, renders it possible
to unite the whole of the geodetic triangulation south of Tanganyika with the homogeneous
system previously used throughout Cape Colony, Natal, the Transvaal, Orange River Colony

1) It is a matter of great regret that the 21 meter Standard received some injury at the Russian frontier on
its return journey to Russia.

 
