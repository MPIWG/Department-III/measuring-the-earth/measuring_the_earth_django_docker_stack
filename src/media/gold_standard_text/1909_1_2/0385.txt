er

 

7

1,5

ae

x
x
x
x
=
x

325

noch so heftige Bewegung geratene Balken sich wieder beruhige. Da die hierzu bemessene
Zeit beim früheren Gebrauche des Instrumentes nicht kleiner als 1 Stunde und 40 Minuten
war, erreichten wir durch diese neue Dämpfungsvorrichtung den Vorteil von nahezu doppelt
so vielen Beobachtungen in gleicher Zeit.

Die Anregung zum Suchen eines Mittels, das ein schnelleres Beobachten ermöglicht,
habe ich Herrn Professor O, Hucker zu verdanken, der die Herstellung eines dem meinen
ähnlichen Instrumentes besorgend, dasselbe auch in diesem Sinne zu verbessern trachtete.
Professor HECKER versah überdies sein Instrument auch mit Vorrichtungen für automatische
Einstellung und für photographische Registrierung. Diese letztere Art der Beobachtung
bringt nebst ihrem dokumentarischen Wert noch den grossen Vorteil mit sich, dass sie
eine stetige Verfolgung des Balkenstandes ermöglicht und so die Zufälligkeiten einer Moment-
anablesung ausschliesst,

Dass ich mich bisher doch nicht dazu entschliessen konnte, photographische Registrier-
apparate auch an meinem Instrumenten anzubringen, erklärt sich dadurch, dass ich einer-
seits mich scheute am Instrumente neue Vorrichtungen anzubringen, welche Quellen neuer
Störungen werden könnten, andererseits aber den Vorteil nicht aufgeben wollte, den die
Ablesung mit dem Fernrohre dadurch bietet, dass ihre Angaben einen unmittelbaren Auf-
schluss über die am Beobachtungsorte vorhandenen Schwereverhältnisse geben und es so
ermöglichen, sogleich nach Abschluss der Beobachtungen an einer Station die Lage der
zunächst folgenden zweckentsprechend zu wählen.

Il. BEOBACHTUNGEN MIT DER DREHWAGE, RELATIVR PRNDELMESSUNGEN UND BESTIMMUNGEN
DER RELATIVEN LOTABWEICHUNG, AUSGEFÜHRT IN DEMSELBEN GEBIETE.

Die Ausführung dieser drei Arten der Untersuchung von Schwerestörungen in dem-
selben Gebiete erschien mir darum von grösster Wichtigkeit, weil damit ein Beweis dafür
erbracht werden sollte, dass die Drehwage tatsächlich fähig ist räumliche Veränderungen
der Schwerkraft d.i. derselben Kraft anzugeben, die dem Lote seine Richtung, dem Pendel
seine Bewegung vorschreibt.

Ein älterer derartiger Versuch misslang. In meinem Berichte von 1906 machte ich
Erwähnung davon, wie bedeutend der aus meinen Drehwagenbeobachtungen abgeleitete
Wert der Differenz der Schwerebeschleunigung zwischen den am Balatonsee gelegenen Sta-
tionen Boglär und Fonyöd von jenem Werte abweicht, den General v. STERNECK für die-
selbe Differenz als Resultat relativer Pendelmessungen angiebt. Ich vermute, dass dieser
Misserfolg der Inkonstanz der benützten Pendel zuzuschreiben sei.

Umso grosser ward aber das Bedürfnis nach einer neuen derartigen Vergleichung
mit einem hierzu ausreichenden Materiale von Beobachtungen, die mit möglichster Sorgfalt
unter einheitlicher Leitung angestellt werden sollten.

Eine derartige vergleichende Untersuchung führten wir in dem von uns eingehend
bearbeiteten Arader Gebiete aus und zwar nicht allein die Beschleunigungsdifferenzen be-
treffend, sondern auch bezüglich der relativen Lotablenkungen.

 
