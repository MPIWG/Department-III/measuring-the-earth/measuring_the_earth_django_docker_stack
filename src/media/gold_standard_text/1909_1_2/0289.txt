 

“Ir

TL TR TE aT Te

ANNEXE A, xb,

PAYS-BAS.

Rapport sur la triangulation de l’île de Sumatra
Janvier 1906—Décembre 1908.

PAR

M. l’ingénieur S. BLOK,
Chef de la Brigade de triangulation.

Avec deux cartes.

1. RÉSEAU TRIGONOMÉTRIQUE.

Des piliers ont été érigés aux stations de la nouvelle chaîne, qui rattache la triangu-
lation de la partie méridionale de Sumatra au réseau de ,Sumatra’s Westkust”, et qui est
représentée sur la carte A. Le pilier érigé en Décembre 1885 sur le Goenoeng Korintji au
bord du cratère à une altitude de 8805 M. étant devenue presque inaccessible par suite
des éboulements et tout à fait impraticable comme station pour la mesure des angles, on
a dû construire un nouveau pilier à quelque centaines de mètres au dessous du sommet.

En 1907 on a commencé les reconnaissances pour l’extension de la triangulation dans
la résidence ,0ostkust van Sumatra”. On a établi une chaîne de triangles, se rattachant
au sud à la coté Dolok Martimbang—Dolok Paoeng du réseau de ,Sumatra’s Westkust”, qui
passe par le lac de Toba dans la partie septentrionale du pays des Bataks, depuis quelques
années sous la domination néerlandaise; cette chaîne, qui s'étend jusqu'aux confins du Gouver-
nement d’Atjèh, est représentée sur la carte B; elle comprend 14 points dont, au mois de
décembre 1908, 9 étaient pourvus de piliers.

Dans la plaine au nord du lac de Toba on à trouvé un terrain approprié à la mesure
d’une base, qui pourra être rattachée sans difficulté au coté Dolok T'andoek Benoea—Dolok
Sibajak par un réseau rhomboïque comprenant deux points intermédiaires. Pour la mesure
de la base on se servira d’un appareil à fl d’invar de MM. Benorr et GUILLAUME.

II. MESURE DES ANGLES,

Sur six stations de la chaîne de jonction méridionale les angles ont été mesurés. Sur

 
