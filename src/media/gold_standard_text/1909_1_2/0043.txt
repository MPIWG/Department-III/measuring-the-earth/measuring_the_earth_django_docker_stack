FETTE TEPER PY TT TTT TAT NTI NT

art

CET Pre YT ee ppm ooo

37

nant autour d’un axe, même quand il est homogéne. J’espére qu'on trouvera plus tard la
solution de ce probléme et qu’on sera alors, mieux qu’aujourd’hui, en état de juger si mon
explication a quelque valeur.

Je regrette que les recherches préliminaires de M. Hecker sur les effets de la pression
atmosphérique sur la position de la verticale n’aient pas conduit, jusqu’à présent, à des ré-
sultats définitifs. Je crois que plus tard peut-être de telles recherches auront du succès, J ai
reçu, il y a quelque temps, une lettre de M. Napier Denison, du service météorologique au
Canada, qui s’occupe des observations de pendules horizontaux, dont pourtant la sensibilité
n’est pas aussi grande que celle des pendules de M. fecter. Avec ces pendules il a déter-
miné la position moyenne de la verticale dans les différentes saisons de l’année, pendant
lesquelles la pression atmosphérique varie d’une manière assez considérable. La grande région
des fortes pressions barométriques, qui s'étend sur l'Océan Pacifique, se déplace en certaines
saisons vers Alaska et dans d’autres saisons vers le sud. Quoique je ne connaisse pas les lois
de ces déplacements, je crois que M. Denison a trouvé que les variations de la verticale
s'accordent avec les déplacements de la région des fortes pressions. Üe n’est qu'une re-
cherche provisoire, mais il ne me paraît pas impossible que le pendule horizontal prenne
place parmi les instruments de météorologie, car la déformation de la terre se transmettra
certainement avec une rapidité plus grande que celle des nouvelles envoyées par le télégraphe
sans fil. Elle pourra nous renseigner assez vite sur les variations des pressions atmosphériques.

Autrefois je me suis occupé du problème de la pression exercée par les marées de
l'Océan sur la croûte terrestre, et je suis arrivé au résultat que les déformations résultant de
cette pression pourraient être observées à une distance assez grande des côtes. M. v. Xebeur
Paschwitz, dont nous déplorons la mort prématurée, a dit que mes recherches ne pouvaient
se rapporter aux stations situées dans le nord de l’Europe, maïs je crois pourtant qu'il sera
intéressant de comparer les résultats obtenus au moyen des pendules de M. Hecker avec
ceux qui seront déduits des observations dans l’Europe occidentale. Je crois que, pour le
moment, les seuls résultats qui se rapportent à ce sujet sont ceux qui ont été obtenus par
M. Æhlert à Strassbourg et par M. Kortazzi a Nicolaieff.

Il est certain que de telles recherches rentrent dans le domaine de notre Association.
Elles se rapprochent aussi des recherches dont s'occupe l'Association sismologique, et je crois
qu'il serait fort utile d'obtenir la coopération de ces deux associations. Je propose donc que
notre Association nommera une commission qui s’occupera des travaux de M. Hecker, et s’il
m’est permis, je voudrais proposer comme membres de cette commission: MM. Hecker,
Lallemand, Haid, Eötvös, Backlund, Weiss et Poincaré; je serais heureux si la Conférence
voudrait m’associer 4 cette Commission.

M. le Président donne la parole & M. Haxd.

M. Haid dit que, il y a quatre ans, il a fait installer en deux erdroits, 4 Durlach
pres de Karlsruhe et à Fribourg des pendules horizontaux, construits à Potsdam d’aprés
les indications de M. Hecker. Ces instruments sont installés directement sur la roche. D’après
les résultats obtenus pendant les années 1906, 1907 et 1908, M. Haid a pu constater pro-
visoirement que les amplitudes et les phases des oscillations dans ces deux stations sont

 
