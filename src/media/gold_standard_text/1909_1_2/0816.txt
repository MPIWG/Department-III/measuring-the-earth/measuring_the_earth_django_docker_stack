 

 

 

 

330 14

deren Errichtung von der K. K. österreichischen Regierung bereitwilligst gestattet worden
war, wurde daher mit einem Horizontalpendelapparat dieser Art ausgerüstet. a

Die Einrichtung der Station sowie die Beaufsichtigung der Apparate wurde in
dankenswertester Weise von Herrn Prof. Dr. Kouuer, Professor der Geodäsie und Mark-
scheidekunst an der K. K. montanistischen Hochschule in Pribram, übernommen.

Die verschiedenen Schwierigkeiten bei der Aufstellung der Instrumente hat Herr
Prof. Köster jetzt überwunden, so daß von Mitte Januar 1911 ab ein gutes Funktionieren
der Station erwartet werden kann. PR

Die Kammer liegt abseits von den durch Bergbau gestörten Strecken 1110 m unter
Tage. Die Temperatur in derselben beträgt 27.5 und die Feuchtigkeit (Anfang Januar)
95%; ein Beschlagen der Linsen des Instrumentes, ein Übelstand, mit dem stets in
Schächten zu rechnen ist und der störende Lücken in den Registrierungen hervorruft,
wird umsoweniger zu befürchten sein, als in dem abgeschlossenen Beobachtungsraum
grössere Gefässe mit Chlorkalzium aufgestellt sind. O. Hacker.

Gj
Verschiedenes.

Schon erwähnt ist die Anwesenheit des Herrn Dr. Facernoum aus Stockholm,
sowie die Ausrüstung der neuen Scorr’schen Südpolarexpedition mit Apparaten für
Pendelmessungen.

Der Bericht über die Triangulationen (1903—1909) wurde von Herrn Prof.
Dr. Gazze unter meiner Mitwirkung fertiggestellt und ist als Bestandteil der , Ver-
handlungen der 16. Allgemeinen Konferenz“ gedruckt. Er enthält u. a. die vervoll-

" ständigte Karte der europäischen Dreiecksnetze, die mit Benutzung der früheren Aus-

gaben der Karte nach den von uns gesammelten Daten vom militär-geographischen Insti-
tute in Florenz in entgegenkommender Weise hergestellt wurde, wofür wir diesem

Institut, insbesondere seinem Direktor, Herrn General Griamas, zu großem Danke ver-

pflichtet sind.

Herr Prof. Dr. Gautz hat auch eine Karte der nordamerikanischen Dreiecke in
der Nähe des Meridians von 98° westl. Länge, sowie eine Karte der neuen Dreiecks-
messungen in Frankreich gezeichnet, welche Karten dem Bericht beigegeben sind.

Von Herrn Geheimrat Arskechr wurde der Bericht über die in letzten Jahren
ausgeführten Breiten-, Längen- und Azimutmessungen abgeschlossen und dem beständigen
Herrn Sekretär der I. E. zur Aufnahme in Teil II der „Verhandlungen“ übersandt.

HeLuerr.

B. Geschäftliche Tätigkeit.
1,

Der Dotationsionds wurde wie bisher verwaltet. Seine Bewegung im
Jahre 1910 stellt sich, vorbehaltlich der Kkonventionsmäßigen genauen Nachweisung
der Einnahmen und Ausgaben, wie folgt:

sd
