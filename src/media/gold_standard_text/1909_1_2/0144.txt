 

ee N ee -
— =~ . = rs en

136 no.

Jusqu’au mois de Mai dernier, vingt-sept stations avaient été exécutées, en observant
aussi les distances zénitales pour effectuer le nivellement trigonométrique entre les différents
points.

Un nivellement de précision a été effectué depuis un point de la côte du Pacifique (port
de Cartajena) et la station géodésique la plus voisine (cerro de Cartajena), laquelle sera bientôt
réunie pareillement avec un autre point de la même côte (port de San Antonio).

A Valparaiso, les marées seront d’abord observées et un nivellement sera fait entre ce
dernier point et la station la plus voisine (Alto del Puerto).

La tolérance admise dans ces opérations est de un millimètre par kilomètre de ligne
nivelée (communication du 29 Mai dernier).

En même temps que’ l'Etat-Major commençait ces travaux, un nombreux corps d'ingé-
nieurs chiliens effectuait un relevé de la Cordillière des Andes, pour servir à la démarcation des
limites avec la République Argentine et avec la Bolivie. Pendant dix ans, ces opérations se
poursuivirent au moyen de lignes polygonales suivant généralement le fond des vallées et
traversant les passes, jusqu'à couvrir une étendue, du Nord au Sud, de près de 4500 kilomètres
(depuis le 17° jusqu’au 55° parallèle), avec un développement total de 30.000 kilomètres. Sur
ce parcours, des centaines de points furent en plus déterminés astronomiquement par leur
latitude et quelques points de départ aussi par leur longitude au moyen du telegraphe.
D’innombrables altitudes furent determindes par le nivellement trigonometrique. L’ensemble
de cestravaux sera suffisant pendant longtemps pour satisfaire aux besoins geographiques du
pays dans la région la plus élevée des Cordilliéres, et de plus, constituent une base de
reconnaissance pour l’extension vers l’Est des travaux géodésiques qui se bornent pour le
moment à la région centrale.

En profitant des services d’une partie de ce personnel, on constitua, dans l'année 1907,

“un nouveau bureau, dénommé de Mensura de Tierras (mesure des terres).

Une des sections de ce Bureau, la section Géodésique, sous les ordres de l'ingénieur,
M. Ernesto Greve, doit entreprendre le relevé du plan topographique des provinces du Sud
et du Nord.

Après les opérations de reconnaissance, il à été possible de projeter dans la partie nord
du pays un réseau de soixante-dix triangles, dont les côtés varient de 24 à 74 kilomètres ; il y à
seulement six côtés dans toute cette triangulation dont les longueurs dépassent 60 kilomètres.

On étudia d’ailleurs l'emplacement et le développement des cing bases dont trois ont été
adoptées : celle de Chuta, d’environ 6585 mètres, située le plus au nord, qui servira à
calculer le côté Meseta-Arunta; celle de Pintados, d'environ 8348 metres, avec laquelle on
pourra calculer le côté Puquio de Nunez — Rabo de Chancho ; et la troisième, dans la partie sud
de cette région, la base de Paciencia, d'environ 4691 mètres {avec une extension possible d’un
kilomètre), et avec laquelle on calculera le côté Pedregoso — Manchado.

1 tdi hk

ns Ml Al

ET

ial

ne t l)

A Mh mann 11

4 tam en a

an

2: ssmtététemt. anse nid |

 
