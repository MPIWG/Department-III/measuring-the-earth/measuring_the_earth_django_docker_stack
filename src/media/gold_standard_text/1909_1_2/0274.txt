 

É
iF
|
ie
\
|

244

37° strömen die Flüsse parallel dem Ufer des Ozeans wie bei Abukuma und Kitakami der
Fall ist. Dieser Verlauf der Flüsse deutet einigermassen auf die Bildungsweise der nörd-
lichen Hälfte des Honshü hin. Es ist wahrscheinlich dass diese Landstrecke durch starkes
Zusammendrücken von der Seite des Ozeans und davon resultirende Erhebung ausgebildet
worden ist, wie es einerseits der Flussverlauf und der starke Küstenabfall, anderseits die
Schwerkraftsanomalien und die magnetischen Störungen vermuten lassen.

Die von Otani, Shinjo und von mir vor mehreren Jahren ausgeführten absoluten
Messungen der Intensität der Schwerkraft an vier Stationen lassen sich mit den relativ

gemessenen Werten vergleichen.

 

 

 

 

Station g (absolut) g(relativ) | g (abs.) — 7 (tel).
EE  —
Tokyo 979.810 979.814 = 0004
_ Kyoto 07025 979.734 -- 0.009
Kanazawa 979.884 979.891 0,007
Mizusawa 980.167 980.172 - 0.005

 

Mittel — 0.006

Die Differenzen zwischen den relativ und absolut gemessenen Werten zeigen dass sie
von einem systematischen Fehler begleitet sind, welcher zweifellos der Unsicherheit der
Biegungskorrektion des Sekundenpendels zuzuschreiben ist. Abgesehen von diesem konstanten
Fehler zeigen die absoluten Werte ziemlich gute Uebereinstimmung mit den relativen wenn
man die komplizierten Operationen der absoluten Messung in Betracht zieht.

Gleichzeitig mit der Schwerkraftsmessung haben wir die Breitenbestimmung fast an
jeder Station ausgeführt; es steht in Aussicht diese Stationen mit dem geodätischen Dreiecks-
netze zu verbinden, damit die Schwere- und Breiten-störungen zu gleicher Zeit studirt

werden können.
H. NAGAOKA.

Physikalisches Institut der Professor der Physik,
Universität, Tokyo Mitglied der Japanischen
den 3ten Juni, 1909. Geodätischen Kommission.

au hu in a Di au Land ana ue er

 

q
i
3
I
3
1
1

 
