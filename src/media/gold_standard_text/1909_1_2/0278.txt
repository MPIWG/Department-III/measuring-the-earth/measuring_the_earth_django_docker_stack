 

248

aerial be at anchor or drifting about as close as possible to the station. To compare the
chronometers, signal waves are sent out from the aerial of the ship, and are received both
by the small aerial of the near station and by the large aerial at the distant station, and
thus the chronometer comparison can at once be accomplished.

As almost every isolated island in Japan, of which the longitude determination is
desirable, lies within the limit of reach of correspondence from at least one of those aerials
which haye been erected for the purpose of correspondence over all the parts of the country,
the chronometer comparison can in this way be accomplished.

In last winter, we have experimentally determined the difference of longitude between
Tokyo, astronomical observatory, and Yokosuga, naval instrument depot, by means of wireless
telegraphy. The distance between them is only 45 kilometres, but it may perhaps serve
us at least as a model of the actual determinations. To make the case the most complex,
we had no high aerial at the two stations, so that we took two intermediate stations, one
being the naval college three kilometres from Tokyo astronomical observatory, and the
other being the naval torpedo college four kilometres from Yokosuga naval instrument
depot. The chronometer comparisons were done three times a night.

Further the two longitude stations being connected by a telegraphic line, common
telegraphic determinations of longitude were made at the same time. It was so arranged
that the errors of astronomical observations and the errors arising from the irregularities
of the rates of the chronometers were introduced wholly in common to the final results of
the longitude determined by both methods.

As the weather was then extremely unfavourable, we had only four nights in which
a more or less sufficient number of stars was observed. The results are the following:

Yok. nay. inst. depot west
from Tokyo ast. obs.

Date 1909 By wireless tel. By line tel. : Diff.

 

Jan. 6 2 185.173 + 185,175 — 08,002
16 143 165 29

24 216 217 A

26 174 178 a À
Mean: -+ 185.178 4 188.185. 08,007

The difference on January 16 is exceptionally large and can probably be due to the
fact that in that night, the relay in Yokosuga was not sensible enough. The results are
not so satisfactory as we could have expected from our former experiments, yet the above
differences are generally so small that their amounts are within the probable errors of ob-
servations. If we could take a greater number of nights, and if we could exchange the
apparatus of both wireless and line telegraphies between the two stations, in order to eliminate

 

ven ea ur een Al ne be ca mede

so pean anamnestic le he

 
