 

 

en

SSCS SST GLE TS EIS Es

Serra en

ET

ER

Bee een

 

54
la réduction ordinaire des observations ont été faits de la même manière que les calculs
des observations sous le parallèle nord; pour la plus grande partie ils ont été exécutés aussi
par les mêmes personnes.

Au courant de l’année 1910, une nouvelle station dans l'hémisphère austral, celle
de Johannesburg (Transvaal), s’occupera des observations de latitude; à cause des conditions
climatologiques fort favorables cette station fournira probablement une grande quantité
d’observations. Le Bureau central élabore un programme d’étoiles pour cette station qui est
attachée au »Government observatory” (9 — — 26° 11”.

Pendant l’année 1909, seul l'observatoire de Poulkovo a coopéré aux travaux du
service des latitudes, par l'observation des étoiles au télescope zénital et à l'instrument
des passages au premier vertical, ainsi que par l’observation continue de l'étoile à Cassiopée,
de la même manière que pendant les années précédentes. On peut voir les résultats de
ces observations dans le Vol. XVIII de la Serie II des Publications de l'Observatoire Central
Nicolas sous la direction de O. BackLunn.

Prochainement nous pourrons attendre une augmentation fort importante des con-
tributions cooperatives. M. le Directeur BackLunn a declare à la Conférence générale de
Londres qu'il voulait faire ex&cuter des observations continues d’etoiles zenitales a la suc-
cursale de l’observatoire de Poulkovo a Odessa, et le contre-amiral de CamPos-RoDRIGUEZ,
directeur de l’observatoire de Lisbonne, a aussi déclaré vouloir faire entrer dans le pro-
gramme de l'observatoire les observations continues de l’étoile x Lyre qui passe près du zénit
de Lisbonne, tant au télescope zénital qu’à l'instrument des passages au premier vertical.
Sous peu les observatoires de Christiania et d’Upsal qui se trouvent à peu près sous le même
parallèle que Poulkovo prendront probablement part aux observations continues de l'étoile

à Cassiopée.
Tu, ALBRECHT.

4, DÉTERMINATIONS RELATIVES DE LA PESANTEUR AU MOYEN DE PENDULES.

M. le Prof. Borrass a préparé et à peu près terminé pour les Comptes rendus de
la 16e Conférence générale de Londres et de Cambridge un tableau général de toutes les
déterminations relatives de la pesanteur depuis une période d'environ 100 ans, qui pour les
observations du 19e siècle se base sur le rapport que j'ai présenté en 1900 à Paris.

En me servant des perturbations Ag, par rapport à ma formule de 1901, observées
en 51 stations se trouvant aux côtes raides de la mer, j’ai déterminé la profondeur de la
couche où, d’après l'hypothèse de Pratt, la pression par unité de surface est partout la
même, j'ai trouvé 118 km. (Voir les Comptes rendus des séances de l’Académie de Berlin de
1909 p. 1192—1198 et aussi les Comptes rendus de la 16e Conférence générale de l’Asso-

ciation géodésique Vol. I p. 46).

a8 hdd ss Aides tds al

ehr

sv 2000 cab ii u bs dd dad dh ech un.

 
