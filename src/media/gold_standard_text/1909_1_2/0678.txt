 

202

canadien se trouverait 4 1™,68 (5,52 pieds) au-dessous du niveau moyen de l’Atlantique

dans le port de New-York.

Les altitudes calculées depuis 1903 sont rapportées au niveau moyen de la mer à
New-York, en attendant que les nivellements canadiens soient reliés au marégraphe installé,
il y à une dizaine d'années, par le Ministère de la Marine, à Halifax (Nouvelle Ecosse),
en un point où les courbes de marée ne seront influencées par aucun apport d’eau douce.

20. Erats-Unis DE L’AMERIQUE DU NoRrn.

Les altitudes sont rapportées au niveau moyen de la mer, défini par sa cote sur les
échelles de marée, dans les divers ports.

21. MEXIQUE.

Le zéro normal n’est pas encore fixé.

22, Équareur (Mission du Service Géographique de l'Armée française).

Le zéro adopté pour le calcul des altitudes du nouvel are de méridien est le niveau
moyen du Pacifique, déduit des indications des médimarémètres installés à Salinas et à Payta
et observés, le premier, du 14 février 1904 au 81 mars 1907, et le second, du 7 mai
1906 à 1907.

Qo, Orne

Le zéro normal n’est pas encore fixé.

AUSTRALIE.

-24, ETATS DE VICTORIA.

Pas de renseignements.

b. Altitudes des repères frontières et leurs discordances.

La nomenclature des repères frontières est fournie par les tableaux ci-après, se rap-
portant chacun à deux pays contigus.

Chaque tableau fait connaître:

1°, les localités où se trouvent les repères de jonction (col. 1);

20, les numéros, dans les deux pays, de ces repères (col. 2 et 3);

30, la désignation de leurs emplacements (col. 4);

4°, leurs altitudes dans les deux pays (col. 5 et 6) avec la désignation de leur mode

a eh sa a

 
