  

h
il
a

IT SE

RE

SS

2
if
|

342

 

Zwischen die Schichten des krystallinischen Schiefers sind Lager von Diorit, Am-
phibolit und Serpentin, stellenweise auch Glaukophan eingekeilt, die nächst dem Kamme
des waldbedeckten Gebirges aus den reichen Humusschichten hie und da zu Tage treten.

Aufmerksam gemacht durch die magnetischen Störungen, die von Kreın und von
Scomenzt im Rahmen ihrer Landesaufnahmen an zwei Punkten in der Nähe dieses Gebirges
(Ujvidek und Karlöcza) entdeckt wurden, wählte ich die umliegende Ebene zum ersten Felde
meiner derartigen Untersuchungen, auch aus dem Grunde, weil der einfache, inselartige
Aufbau des Gebirges die Möglichkeit eines leichteren Überblicks seiner Massenverteilung
in Aussicht stellte.

Die Arbeit dreier Jahre, von 1902 bis 1904 verwendeten wir zur Erforschung dieses
Gebietes, an 109 Stationen Drehwagenbeobachtungen, an mehr als 1300 Stationen magne-
tische Beobachtungen ausführend. Gewissermassen waren es unsere Lehrjahre, in denen wir
unsere Beobachtungsmethoden ausprobierten und sie zu verbessern lernten. Im ersten Jahre
hatten wir zum Beispiel nur eine einfache Drehwage, die an jeder Station fünf Einstellungen
erforderte und wegen ihrer zeitraubenden Behandlung nur ein langsames Vorwärtsschreiten
ermöglichte. Das während diese drei Jahre gewonnene Beobachtungsmaterial bedarf eben
deshalb noch einiger Ergänzungen, 50 namentlich auch durch Pendelbeobachtungen und

Bestimmungen der Lotablenkung.

Die Hoffnung, die unsere Schritte in dieses Gebiet lenkte, täuschte uns in keiner

Hinsicht. Wir fanden dort ein sich lings. und um das ganze Gebirge weit erstreckendes
magnetisches Störungsfeld von seltener Regelmässigkeit. Auch die Beobachtungen mit der
Drehwage ermöglichten eine klare Einsicht in die vorhandene Massenverteilung, indem sie
Gradienten der Schwerestörung zu erkennen gaben, welche innerhalb 10 Kilometer betra-
genden Entfernungen vom sichtbaren Fusse des Gebirges von Süden wie von Norden nach

dessen Kamme hin gerichtet sind.
Schwerestörungen und magnetisc
Grösse und Richtung nach von einander unabhängig,

gestellten IIl-ten Störungstypus.
Das ganze magnetische Störungsfeld beherrscht hier eine im Norden des Gebirges

diesem parallel verlaufende magnetische Kammlinie, welche vom Fusse des Gebirges um
ungefähr fünf Kilometer abstehend, sich ihm in seiner ganzen ostwestlichen Erstreckung
anschmiegt. Diese Kammlinie gibt sich in zweifacher Weise zu erkennen: erstens durch die
längs ihr nachweisbaren maximalen Werte der Vertikalkomponente, zweitens dadurch, dass
nördlich von ihr Minimalwerte, südlich von ihr aber Maximalwerte der Horizontalkompo-
nente der erdmagnetischen Kraft aufzuflnden sind. Maxima und Minima stehen ungefähr
um fünf Kilometer von einander ab und erreichen Wertdifferenzen gleich dem fünfzehnten
Meile der ganzen Horizontalintensität. Neben dieser, die ganze Störung beherrschenden

Hauptkammlinie finden wir, hauptsächlich im Gebirge selbst noch andere dieser parallel

verlaufende ähnliche Linien wohl von geringerer Hrstreckung, aber einige unter diesen, die

noch um vieles grössere Unterschiede zwischen den nördlichen Minimalwerten und den

südlichen Maximalwerten aufweisen.

he Störungen sind aber hier wohl parallel, doch ihrer
entsprechend dem in der Fig. 6 dar-

na HALLE Ab dé à Bi A Laie

oh un a Ml al Land ea ne ne

 

 
