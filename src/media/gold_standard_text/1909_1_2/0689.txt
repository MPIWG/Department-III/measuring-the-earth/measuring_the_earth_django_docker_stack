A I a

 

ir

TE

TR rh a 10

x

 

 

 

 

 

 

213

Tableau n°, 183. — Danemark et Prusse.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Numéros matricules ' an. Differences
: Altitudes definitives
des reperes D-P
Noms des 0 | Designation de l’empla- Non ortho- =
localites | cement des reperes d inpbyaguee Moyennes
| D: ask Nee = Partiellk
| Danemark Prusse Danemiarkı Pace artlelles par
| D P localité
4 | 2 a 4 5 6 7 8
| SNe I NUN:
8 3m,9 N. £. ty&k Milepel| 35,8 | 0,249 :
oc 56 » m9 N. f. tysk Milepel| 35,837 35,895 | +0,
um. | » 8609 Grensepille, lige over} 35,795 35.553 | + 0,242 + 0,242
… | for tysk Milepæl
Ved alas: | 865 » Sur N. f. den danske 9,181 8,939 | + 0,242 a
Station | & a ro
| » 8588 Grensepille ved den 9,495 9,252 | + 0,243
danske Græn$e
Rae 62 » Lige over for Foldingbro | 20,525 20,282 | + 0,243
Holgi: > 8632 | Grensepille ved Told- | 20,837 | 20,602 | + 0.235 + 0,239
‘ kammerbygningen
‚IN { 108 » Lige over for 8637 29,030 21,188 | + 0,242
u | >» 8637 | Grensepille ved Bavne-| 21,988 | 94747 | o.o41 y | + 92415
gaard = a
. Différence moyenne générale: D-P = + 0,241
Tableau n°. 14. — Danemark et Suède.
Numéros matricul Différence
uméros nn ricules A den ee ifférences
des repères D-S
Noms des | Désignation de l’empla-|
localités cement des repères i : Moyennes
Danemark | Suede Danemark| Suède Partielles par
| D S 1) localité
+ ZN. nd
| | m m m m
1341, B » Kronborg. Fr. III. Bastion) 10,973 10,881 + 0,092
Helsingör D » » Düetaarnet | 26,238 26,145 | + 0,093 | + 0,093
N » » Telegraftaarnet 31,256 31,161 +- 0,095
> 118; D Vikingsberg 24,860 24,168 | + 0,092
Helsingborg » r  11&,M Palo tee 10,756 10,664 | + 0,092 | + 0,093
| D À » Pälsjö Klint 30,574 30,479 | - 0,095

 

 

 

 

 

 

 

 

Différence moyenne générale: D-S = + 0,093

1) Altitudes suédoises prises par le Rapporteur dans le questionnaire retourné par le Danemark. Ces altitudes ne
sont pas rapportées au repère fondamental suédois, mais au niveau moyen de la mer,

 
