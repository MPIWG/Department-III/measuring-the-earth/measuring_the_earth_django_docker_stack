 

216

 

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

4 . i
| Tableau n°. 19. — France et Suisse. |
Numeros matricules Altitudes definitives

i DE Differences

| des repères orthométriques

ere -

! made os | Désignation de | Tonics

| 0 lemplacement des |.

localités os Altitudes | Altitudes

| France Suisse : officielles | Tation- Suisse F-S F,-S

. nelles!)

F Br | S

4 4 2 3 4 5 oe 6 2 He

( ; Be = ZN. + PN. +

fi m m m m m

i Boncourt ‘| Seg. o-I » Batiment de la dou-| 370,840 370,870 | —2,752 | + 373,592) + 373,622

ane fédérale

| Le Locle - Sce. ®-I » Bâtiment des Pos-| 922,401 922,436 548,928 | + 373,473 | + 373,508

| tes et Télégraphes

I Les Verrieres Sbe. 86 » Maison de garde 925,598 995,628 | 551,992 | + 373,606 | + 373,636

i La Cure Sab. #-I » Bureau des Douanes| 1148,695 | 1148,721 11503. | + 373,592 | + 373,618

| Mollesullaz, IS. ® » Pont sur le Foron | 415,463 415,476 44 843 | + 373,620 | + 373,633

i (pres Anne- (après Pinondation

| masse) de 1888)

I St. Gingolph lab. 99 » Pont sur la Morge | 393,553 393,966 19,905 | + 373,648 | + 373,661

I Chätelard Ibd. 56 » Pont sur VIsle 44125,227 | 1125,234 751,508 | + 373,749 | + 373,726

| RANGER RIRE Fe ee à ER

ii ;

| Différences moyennes générales : F-S = + 373,607, Fr-S = + 373,629

| : :

| l'ableau n°. 20. — Italie et Suisse.

1

Numeros matricules : oe.

i : Altitudes definitives

| des reperes
Noms des | : Designation de l’empla- : 5
i S Orthomé- |Non ortho-| Differences 3
i localites cement des reperes Dit hu 1-5 i
2 i | I = 4
i ous oo Italie Suisse 2) j
| I S |
| 1 2 3 4 eh 7

I ZN. + PN. +
y é m m m

| Gran S. Ber- 341 » Presso la croce a levante | 2474,023 2 ?

nardo dell’ Ospizio

I Galleria del NH O1 » Portale Sud 630,436 ic ?

i Sempione NF. 100 » Portale Nord 682,798 ? t

| Gondo NK. 119 » Torre dell Castillo Stock | 855,502 ev ?

| alpes

Passo dello NF. 243 2109 Roccia 21414,908 | 1741,195 + 373,713

i - Spluga

i _ Spiga

| Castasegna NF. 218 2034 Ponte sul Lovero 686,022 | 312,318 + 373,704

|

| 1) Les altitudes rationnelles résultent de la compensation simultanée des réseaux de ler et 2e ordres du Nivelle-

N ment général de la France. (Voir Comptes-rendus de la Conférence de l’Association Géodésique Internationale tenue à

} Paris, en 1900, ler Volume, page 184).

a

2) Cotes prises par le Rapporteur dans le Catalogue des hauteurs Suisses (Nivellement de précision, — 10e

livraison. 2e volume 1891).

|
i
,
L

 
