 

Fe

394

Observations de M. Darwin au sujet des communications de MM. Hecker et Lallemand .
Communication de M. Haid sur les observations exécutées au moyen de pendules horizontaux.
Proposition de M. Backlund : - : s
Nomination d’une Commission pour les oe oe sur ie te de oe terrestre
Communication de M. Haid au sujet d’un instrument de nivellement de M. Zeiss : é
Rapport de M. Gil sur la mesure d’un grand arc le long du 30¢ méridien en Afrique et en Europe.
Communication de MM. Close et Mc Caw sur les mesures de l’arc du 30e méridien dans l,, Uganda

protectorate” : ; . 2
Communication fle M. Keelind sur la mesure de are du 300 bicnidien en Fieypte
Nomination d’une Commission pour la mesure de l’arc le long du 30e méridien.
Rapport de M. Darwin sur les marégraphes .

Quatrième séance, Vendredi, 24 Septembre

Lecture du procès-verbal . 5 ‘ :
Nomination d’un membre de la Commission fe tare an 30% San
Communications administratives. : :

Rapport de M. Börsch sur les deviations de la ce
Observations de M. Helmert au sujet de ce rapport. ‘
Rapport de M. Backlund sur les travaux de l’expédition russe au Saber D

Rapport de M. Carlheim Gyllenshüld sur les travaux de l’expédition suédoise au mere
Communications administratives. . :

Rapport de M. Artamonoff sur les travaux E en Russie
Rapport de M. Schmidt sur les travaux exécutés en Bavière .
Rapport de M. Gautier sur les travaux exécutés en Suisse
Rapport de M. Tittmann sur les opérations exécutées aux Etats-Unis
Observation de M. Helmert au sujet du précédent rapport

Rapport de M. Terao sur les travaux exécutés au Japon.

Rapport de M. Gillis sur les travaux exécutés en Belgique

Rapport de M. Riso Patron sur les travaux exécutés en Chili

.

Cinquième séance, Samedi, 25 Septembre .

Lecture du procès-verbal : . : : ‘ 3 ; :
Rapport de M. Helmert sur les dd de a ac la pesanteur : :

Rapport de M. Hayford sur Vinfluence de la correction topographique et de la compensation
isostatique sur la pesanteur. :

Rapport de M. Hecker sur les on de tone de ia ar a ie Mer Noire
Observations de MM. Darwin et Helmert au sujet de ce rapport .

Rapport de M. Kétvés sur ses observations avec la balance de torsion .

Remerciement de M. le Président à l’Institut des ingénieurs civils

Sixième séance, Cambridge, Mardi, 28 Septembre

Paroles de bienvenue prononcées par M. Buttler au nom de l’Université de Cambridge
Remerciement de M. le Président de l’ Association

Lecture du procès-verbal . : , ; ï . : : ‘ 5 ‘ :
Communication de M. Hecker sur la Balance de lo . : :
Observations de MM. Darwin, Eötvös, et Gill au sujet de cette Oman : i :
Rapport de M. Bourgeois sur les travaux du service géographique de Parmée à Paris
Rapport de M. Lallemand sur les nivellements de précision effectués en France.
Remarque de M. Celoria relative a ce rapport ;
Rapport de M. Celoria sur les travaux exécutés en Italie

Seite, Page
35—37
37—38

38
38
38
38

38—39
39
39
39

40—43

40
40
40
44
4A
41—42
42
42

42

42
AS

is
7

me ee

aa OO oo =!

N

ze
Me)

v

51—52
52
Bo

59

40 14 A Ae A AM Ad MN A hi NM ited tabi le

su

Lulu is

JA

iii

Adi mn in

{sk susan ben ha

 

 
