 

i
|
|

OS

pn

SS Se eee

Le
i

 

 

 

 

 

 

 

 

Numéros matricules R ae
: Altitudes définitives
. des reperes
Noms des Doro le le :
Désignation de l’empla Mimerences
loealites cement des reperes E-C
Etats-Unis| Canada Etats-Unis) Canada
E C
1 2 3 4 5 6 7
nl ni ni
housesPt.,N.Y., houses » Chapman Building 32,903 % ?
Point 1882
Valleyfield,Que.|Valleyfield » Soesc n°. 14, Beauhar- | 47,143 ? R
nois canal |

 

 

 

c. Relations de hauteur entre les zéros hypsométriques.

L’examen des tableaux qui précédent fait ressortir un certain nombre de lacunes qui
ne permettent pas d’établir, d’une maniére certaine, les relations de hauteur existant entre
les zéros hypsométriques des divers pays.

D’autre part, les altitudes attribuées 4 une méme repére dans deux pays voisins ne
sont pas toujours comparables: l’un de ces pays, par exemple, ayant fait subir à ses cotes
la correction orthométrique, liée au défaut de parallélisme des surfaces de niveau du globe
terrestre, tandis que, dans l’autre, on a négligé cette correction.

Dès lors, j'ai dû, cette fois encore, renoncer à calculer les relations de hauteur en

question.
B. RATTACHEMENTS DES STATIONS MAREMETRIQUES.

Altitude des repéres des stations marémétriques et hauteur du
niveau moyen de la mer dans ces stations.

Les tableaux qui suivent contiennent les renseignements fournis, pour chaque pays,
au sujet-des marégraphes, médimarémétres et échelles de marées.

Ces renseignements complètent les indications analogues figurant dans le rapport de 1906.

a 4 she de ur ha A teed

Ja,

sa hon he a lab a hd ant um un +

 
