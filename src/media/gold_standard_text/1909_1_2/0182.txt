166

Hobie tj) Mien At wis tad lh

} a ee (07 = ae — i
l oo = 050 «== 0.00832,  — 0.0007 a, =
N sire Hi -_ A nae
i SL, 2.051 «0.0001 -+ 0.0008 + 0.0004 u
; bi = = 0.05  — 0.0008 + 0.0001 + 0.0004
i ye = 2008 © 2-0,0007 + 0.0008 + 0,0008
| ne ego +52 0:0000 10.0004 + 0.0010
| ie = = 060 00049 +. 0.0005 + 0.0009
| > — — 010 — 0.0008 + 0.00038 + 0.0011
DD — 039 — 0.0026 + 0.0017 + 0.0081
| = 005 00009 + 0.0003 +- 0.0020
i mie 010 + 0.0016 + 0.0007 + 0.0040
| sl, = = a ar =
| u, 0.03 . —-0.0058 + 0.0013 + 0.0096
| ol, = m = — =
L = rs. 00050 — 0.0022 = 0137 :
| a —0.04 . +.0.0001 0.0000 0.0000 :
| = Did | :0.0015 — 0.0015 — 0.0070
i D = 0G. 0.0028 — 0.0030 0.0140
| D 905 1.0001 — 0.0003 — 0.0009
Für die Bögen zwischen Bobrysk und Orsk erhält man nach dem oben auf 8. 161
auseinandergesetzten Verfahren die folgenden y-Gleichungen zwischen Nachbarstationen:
Orel-Bobrysk = 1 100204, - 01488% i
Lipetzk-Orel 13 = — 5.08 4-1.00381y,, + 0.0772 u
Ssaratov-Lipetzk Mo, = + 4.92 +4-1.0094y,, + 0.1481 u
Ssamara-Ssaratov Yo, = — 9.76 + 0.9855y,, ° + 0.0883 u ;
Orenburg-Ssamara 4,, == + 3.30 + 1.0128y%,, + 0.1109 u 1
Orsk-Orenburg Moy —= — 10.44 +1.0046y,, + 0.0776u :

 

Die Busszu’sche Abplattung wurde beibehalten, da demnach gleich O gesetzt, da es
aussichtslos ist, aus einem Längenbogen allein eine verlässliche Verbesserung der Abplattung
ableiten zu wollen; erst die Verbindung geeignet über die Erde verteilter Bögen verspricht
hier Erfolg.

Wie in früheren Ausgleichungen ist zur rechnerischen Bequemlichkeit gesetzt worden:
100 000 en — u; wie früher, so ist auch hier mehrfach vom Rechenschieber Gebrauch

gemacht worden.

 

2 += aitemt naeh BAR |

 
