asa ee
TTT TY a

EE

CODEC

we

bedient hat; bald veröffentlichte ZacHarıae jedoch zwei grössere Werke, das erste seine
»Methode der kleinsten Quadrate”, deren zweite vermehrte Auflage 1887 erschien, das
zweite »Die geodätischen Hauptpunkte und ihre Ooordinaten”, herausgegeben im Jahre 1876.
Beide hatten den Zweck den Schülern der Militärschule einen guten Leitfaden bei seinem
Unterricht und bei ihren fortgesetzten Studien zu verschaffen. Durch die Klarheit der Dar-
stellung der verschiedenen Methoden, und durch die grosse Strenge in der Ableitung der
zugrunde liegenden Formeln erreichen sie diese Absicht im vollsten Maasse.

Als ZacHarıaE die Leitung der dänischen Gradmessung anvertraut wurde, waren
die Triangulation und die darauf bezüglichen Rechnungen beendet, mit dem Präcisions-
nivellement und den Bestimmungen der Schwerkraft war jedoch noch kein Anfang ge-
macht worden.

Zuerst hat sich nun ZacHArıan mit dem Präcisionsnivellement beschäftigt. An ver-
schiedenen Stellen war diese recht schwierig durch die manchmal mehrere Kilometer breiten
Wasserstrecken zwischen den Punkten deren Höhenunterschied zu bestimmen war, aber
durch eine gute Einrichtung der Beobachtungen und durch eine gründliche Diskussion der
Resultate hat er versucht diese Schwierigkeiten zu überwinden, und dass ihm dies vortrefflich
gelungen, zeigen die grösseren und kleineren Abhandlungen, welche er über diesen Gegen-
stand publicirt hat.

Im Jahre 1894 hat er auch die Schwerebestimmungen in Angriff genommen, Mit
einem Pendelapparat von Srerneck hat er in mebr als hundert Stationen genaue Schwere-
bestimmungen anstellen lassen, und daraus schöne Resultate in Bezug auf die Vertheilung
der Schwereintensität in der Insel Fünen abgeleitet; zu gleicher Zeit gab die Bearbeitung
dieser Beobachtungen ihm Veranlassung zu interessanten Betrachtungen über die bei den
Schwerebestimmungen vorkommenden Fehler, und über die beste Methode dieselben abzuleiten.

Wie aus dem mitgetheilten hervor geht, hat ZACHARIAE, während seiner langjährigen
wissenschaftlichen Thätigkeit, in allen verschiedenen Richtungen der Geodäsie gearbeitet;
niemals hat er sich begnügt bloss aus den Beobachtungen gute Resultate herzuleiten, sondern
immer hat er bei der Einrichtung und Berechnung der Beobachtungen neue Methoden oder
neue Theoreme eingeführt, welche die Wissenschaft gefördert haben, und den Namen
ZACHARIAE’S in der. Geschichte der Geodäsie werden fortleben lassen.

Dem Mann der Wissenschaft bringen wir einen ehrfurchtsvollen Gruss, aber wir
vergessen nicht den tapferen Krieger, den Mann mit dem ebenso hohen als einfachen und
liebenswürdigen Karakter, der besonders während der Kopenhagener Conferenz uns alle
entzückt hat. Wir werden sein Gedächtniss in Ehren halten.

Da die Mitglieder des Präsidiums verhindert waren an der Bestattung unseres Vice-
Präsidenten Theil zu nehmen, haben wir Herrn Oberstleutnant Prrersen gebeten dabei die
sympathischen Gefühle unserer Assoication zum Ausdruck bringen zu wollen.

Der Tod des Herrn ZAGHARIAE ist den Herren Delegirten durch ein Cirkular seitens
des Präsidiums mitgetheilt worden.

Endlich beklagen wir den Tod zweier Delegirten, Mitglieder der schweizerischen
geodätischen Commission, der Herren J. REBSTEIN und Max ROSENMUND.

 
