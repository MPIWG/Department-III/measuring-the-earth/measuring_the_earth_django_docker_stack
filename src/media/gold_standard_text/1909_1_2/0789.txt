 

iR

oh.

Tai

:
=
=
§
3
2
=
>
=

303

IV. EVALUATION DE LA RIGIDITÉ MOYENNE DE LA TERRE.

Admettant l'hypothèse de Rocxe-Wrecuerr sur la constitution interne du globe,
ScHWEYDAR a trouvé, entre le rapport = et le coefficient moyen p de rigidité du globe,
une relation qui peut s’écrire ainsi:

m 1,73 -+ 2 (7
: Lo En ae

et que j'ai traduite graphiquement dans le diagramme ci-dessous:

 

Fig. 6.

Coefficients e

Résistance élastique moyenne du globe

©

 

Marée océanique et déviation du pendule (2+)

Relation entre le coefficient de rigidité du globe et le facteur de reduction
des marées océaniques et des mouvements de la verticale.

Rigidité correspondant au facteur 2 de réduction des marées.

Si, dans cette relation, l’on fait:

m2
a FB
on a
pi = 6,3,
c’est-à-dire une valeur correspondant à une rigidité intermédiaire entre celle du cuivre
p= 4,7
et celle de l’acier
pS 14% ;

ce qui, somme toute, apporte une remarquable confirmation 4 la géniale intuition de Lord
KeLvin, en 1877. -

40

 
