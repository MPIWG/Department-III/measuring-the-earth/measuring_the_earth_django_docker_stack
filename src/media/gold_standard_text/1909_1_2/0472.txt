a

Rücksicht auf die nahe bevorstehende Veröffentlichung dieser Arbeiten können
ausführlichere, numerische Angaben über die im Berichtsjahre hierbei ermittelten
Ergebnisse unterbleiben.

In ähnlicher Weise wie im Hefte III der „Lotabweichungen“ wurden
auch für die hier neu auftretenden Punkte bereits vorläufige Werte der
Lotabweichungen in Breite und in Länge abgeleitet, und zwar sowohl unter
Annahme der Bzsser’schen Erdelemente und für Rauenberg als Ausgangs- und
Nullpunkt, als auch für die in letzter Zeit öfters benutzten neueren Elemente
@ 2 I Io), Oa Gee 5S = +O, 4 = +4 für Rauenberg).
Unter Zuhilfenahme früherer Rechnungen von Herrn Prof. Dr. R. Schumann
in Aachen wurden dann ebenfalls für diese neueren Elemente die plausibelsten
Lotabweichungen in Breite (5) für die astronomischen Punkte der russisch-
skandinavischen Breitengradmessung, wenn man diese für sich allein behandelt,
abgeleitet. Hieraus folgte, dab sich für die vier, mit dem jetzt bearbeiteten
astronomisch-geodätischen Netz gemeinsamen Punkte: Dorpat, Jakobstadt,
Nemesch und Belin die von Rauenberg her mit den neueren Elementen
berechneten € um 2.5” bis 2.4’ größer ergeben, als die entsprechenden aus
der Breitengradmessung allein. Es entspricht dies dem schon früher vom
Zentralbureau festgestellten besonderen Verhalten dieser Gradmessung (,, Verh.
d. 1. E. ım Paris 1859”, Annexe A XI, S. 1-5; „Tätigkeitsbericht. des
Zentralbureaus d. 1. E. für 1899, S.3—6). Dagegen hat sich der Anschluß
an den westeuropäischen Meridianbogen, der jetzt nach Afrika hinein bis zur

Breite von 83° 48° verlängert ist und dadurch eine Amplitude von 27° erhalten
hat, auf Grund der neuerlichen Bearbeitung dieses Bogens durch Herrn Prof.
DI 2 com in Aachen („Verh, d. LI. HE. in. Budapest 1906“, I. Teil,
S. 244—261) vortrefflich gestaltet. Hiernach wird unter Annahme der Besser’
schen Abplattung und bei Vergrößerung der Besser’schen halben großen Achse
um da—=--538 m, was diesem Bogen am besten entspricht, für Greenwich
Ég— — 2.47". Mit den neueren Elementen (also fiir da — + 688 m) folet
aber von Rauenberg her: & = — 2.31”.*) Der früher hier vorhandene größere
Unterschied von etwa 2” war hauptsächlich dadurch bewirkt worden, daß
nach Orarke’s Ellipsoid von 1880 damals für Greenwich && = — 0.78” gefunden
worden war.

Bei den genannten Rechnungen unterstüzte mich in den letzten beiden
Monaten des Jahres 1908 der Kandidat des höheren Lehramts, Herr G. Rumr.

Für die Längengradmessung in etwa 48° Breite zwischen Brest und
Astrachan wurde im Berichtsjahre die folgende Arbeit in Angriff genommen.
Vom militär-geographischen Institut in Bukarest wurden dem Zentralbureau
die Winkelmessungen und ihre Ausgleichung auf den Stationen für die im

 

 

*) Gibt man dem westeuropäischen Meridianbogen auch noch seine günstigste Lage auf
Grund der neueren Elemente, so wird für Greenwich && = — 2.17"
abgeleiteten Wert: — 2.31” sogar noch etwas mehr nähert.

, was sich dem vom Rauenberg her

 

vun rs de 4 AM A bh A ed A Mi id da À

Jat 4s dah

nt msi an a Mh a dt Ah tm mn
