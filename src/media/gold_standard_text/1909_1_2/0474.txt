AAA AoA MON A hh tk BN At se

6

vr.

Im Jahre 1909 sollen diese Untersuchungen gefördert und vielleicht eine
Zusammenstellung der bisher erzielten Ergebnisse für die Krümmungsberechnungen als
Beitrag zu den „Verhandlungen“ der nächsten Allgemeinen Konferenz vorbereitet werden.

3.

Sonderberieht über den Internationalen Breitendienst.

Der Internationale Breitendienst auf dem Nordparallel in
+ 39° 8° Breite hat auch während des Jahres 1908 gut funktioniert. 1

Im ganzen sind im Laufe des Berichtsjahres

 

in Mizusawa 2063 Sternpaare

„ Tschardjui 2342 fi |
, Carloforte 2894 |
, Gaithersburg 1877 . i
, Cincinnati 1382 x |
» Cl 1776 ‘ :

beobachtet worden.

Als Beobachter waren während des Jahres 1908 die Herren tätig:

in Mizusawa: Proc Dr. HH. Kimora und Dr. M. Hasaumoro;
„ Tschardjui: Oberstleutnant A. Ausax;
„ Carloforte: Dr. L. Vozra bis zum September und Dr. G. Siva bis

zum Oktober, Prof. Dr. L. Carnera vom November und
Dr. F. Catonio vom Oktober ab;

» Gaithersburg: Dr. Franx E. Ross;
; » Cincinnati: Dr. De Liste Srewarr;
+ Ukiah: res. 1) MApRILL:

Die laufende Reduktion der Beobachtungen wurde gleichwie in den
Vorjahren unmittelbar nach Eingang der Original-Beobachtungsbücher von
dem Observator im Geodätischen Institut: Herrn Prof. Wanach, unter Mit-
hilfe der Herren Rechner: W. Hexse, Ingenieur F. Jagroxskı und Lehrer
A. Wısanowskı ausgeführt.

. Die Reduktionen der mittleren Deklinationen der Sternpaare aut
den scheinbaren Ort sind im wesentlichen von den Herren Rechnungsrat
E. Menperson und Ingenieur F. JazLosskı, sowie von Fraulein Cu. Jaquer
berechnet und als mittlere Örter dieselben Werte angenommen worden, von
denen in den Jahren 1906 und 1907 Gebrauch gemacht worden ist.

Die Verzeichnisse der scheinbaren Deklinationen vom 7. Dezember
1908 bis 7. Dezember 1909, für die Zeiten der Greenwicher Kulmination
interpoliert, wurden autographiert und unter dem 19. Dezember 1908 den

VIERTE SAL |

 
