a

PUT BREE Fier men

Term nern Oe

238

de la difference de longitude entre l’observatoire de Padoue et la station de M. Mario,
détermination faite sous la direction de M. Lorgnzont.

2°, M. Favaro de l’observatoire de Padoue a collaboré aux opérations de longitude
entre Brera et M. Mario.

3°, M. Argssio a, pendant l'année 1907, fait à Padoue une série d’observations
pendulaires en relation avec des observations analogues faites à Potsdam dans le but de
déterminer la gravité relative entre les deux stations ainsi que les constantes de l'appareil
tripendulaire. — Les résultats ont été publiés dans le Vol. VI des , Annales hydrographiques”.

4°. M. ALEssio en collaboration avec M. Smva a aussi fait, pendant les mois de

juillet et d’août de l’année courante, des observations gravimétriques à Gènes et à Padoue;

les résultats en seront prochainement publiés.

TRAVAUX EXÉCUTÉS PAR L'OBSERVATOIRE DU COLLÈGE ROMAIN.

19 M. Brancnt a collaboré en 1907 à la détermination susdite de la difference de
longitude entre M. Mario et Brera.

2°, M. Mrrrosevicn, en collaboration avec M. Brancui, a déterminé, en 1906, au
moyen d'observations lunaires, la différence de longitude entre Tripoli di Barberia et Rome
(Collège Romain). — Les résultats en sont publiés dans les Comptes rendus de la ,R.
Accademia dei Lincei” (CCCIIL 1906).

A la méme occasion, M. Brancut a determine la latitude de Tripoli di Barberia (Phare).

TRAVAUX EXÉCUTÉS PAR LES OBSERVATOIRES DE ROME (COLLèGE ROMAIN)
ET DE NAPLES (CAPODIMONTE).

Sous la direction de MM. FerGoza et MiLLosevion, pendant les mois de juillet et |
d'août 1909, MM. Branch et Contarino d’une part et MM. Nosize et Zappa d’autre part
ont determine la difference de longitude entre la station de M. Mario et l’observatoire de
Naples. — On est en train de procéder à la réduction des observations et les résultats en
seront publiés le plus tôt possible.

> \
DETERMINATION DE LATITUDE ET D'AZIMUT A M. Mario.

Dans le but de compléter, avec toute la précision possible, les déterminations astro-
nomiques absolues à M. Mario, on y a déterminé, pendant les années 1904—05, la latitude
par différentes méthodes et pendant les années 1905, 1906 et 1909 (de même par différentes
méthodes) l’azimut de M. Soratte. Les observations de latitude ont été exécutées par MM.
E. Brancut, A. Di Leaez, A. LoPerrino, BE. Mornosevich et V. Reina. — Les résultats
en sont publiés dans un volume spécial qui vient de paraître et qui sera distribué à MM.
les délégués.

 
