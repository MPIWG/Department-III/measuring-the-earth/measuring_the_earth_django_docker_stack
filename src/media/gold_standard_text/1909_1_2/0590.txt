 

Nous aurons done:

oe ne 6:

La base entière ayant 263 portées, l'erreur moyenne quadratique de la moyenne des
deux mesures de cette base sera alors:

«1 28 —sun,35,
2

environ de la longueur.

 

1
1.180.000

On voit que ce chiffre est de l’ordre du millionième et vient à l’appui des conclu-
sions de MM. Benorr et GuizrauMg. Le terrain était cependant loin d’être favorable. Aussi,
cette précision n’a-t-elle pu être obtenue qu’en procédant lentement et avec des précautions
toutes particulières.

soit

Comparaison de la longueur mesurée avec la longueur calculée.

Si nous rapprochons la longueur 6308m,449 fournie par notre mesure de la valeur
6308m,45 obtenue par la triangulation, en partant de la base de Thanh Hoa, nous voyons
que la concordance est absolue.

Bien qu’il soit impossible de voir dans une coincidence aussi rigoureuse autre chose
qu'un effet purement accidentel, la comparaison de ces chiffres montre avec quelle précision
a été effectuée la triangulation de premier ordre du Service géographique de l’Indo-Chine.

Cette constatation est du plus haut intérét en ce qui concerne le lever hydrographique
des côtes de l’Indo-Chine; en quelque endroit qu’on soit amené à opérer, il sera toujours
possible de s'appuyer sur un réseau de triangulation parfaitement lié, sans qu’il soit désor-
mais nécessaire de mesurer des bases particulières.

Nous devons signaler que le Compte-rendu annuel des travaux du Service géogra-
phique de l’Indo-Chine (année 1908) donne, au sujet de la base de Haiphong, des chiffres
qui diffèrent légèrement des nôtres. On a adopté comme longueur calculée 6308m,417, c’est
à-dire la simple moyenne des deux valeurs fournies par les triangles Terme Nord-Keou
Vien-Terme Sud et Terme Nord-Nui Deo-Terme Sud, sans attribuer de poids prépondérant
au premier de ces triangles, malgré la conformation défectueuse du second. Quant au ré-
sultat de notre mesure, on lui attribue la valeur 6808m,411. Ce chiffre est celui que nous
avions communiqué à Hanoï comme provenant d’un calcul provisoire effectué avec une valeur
de l’étalonnage lévèrement différente de celle qui a été définitivement adoptée.

En admettant pour la valeur calculée le chiffre 6308,417, on aurait avec la véritable
valeur mesurée 6308,449 un écart de Om,032 tout aussi satisfaisant et ne modifiant en rien
nos conclusions.

Étude des variations du fil À, — En février, mars 1905, l’étalonnage fait
au Pavillon de Breteuil, à la température de 15° et sous la tension de 10 kgrs, avait donné

‘la valeur:

mr mm dt po mb mei bene nn nennen u

DAC TENTE CES TT Aisch

a

je

didnt

Jat d sii ted

bstucdtnedibd ode

jm a a, dbl da aie aa un

 

 
