 

190

3
3
ä
à
=
3
3
u

Dans la grande majorité des cas, (69 sur 86) c’est la méthode des angles qui a été
employée. (La méthode des directions, pratiquée en observant des séries complètes, eût été
inapplicable aux hautes altitudes de la Cordillére ou les signaux n'étaient jamais tous
simultanément découverts). Il convient de remarquer que l’on n’a pas retiré d’avantages de
l'emploi de la méthode des angles au point de vue de la rapidité des observations. En
beaucoup de stations élevées, l’invisibilité presque continuelle de certains signaux a obligé,
pour observer un petit nombre d’angles très difficiles, à des prolongations de séjour consi-
dérables que l’on eut peut-être evitées par l’emploi de séries partielles avec référence bien choisie.

La méthode des directions avec emploi d’une référence a été pratiquée quelquefois,
particulièrement aux stations basses du Pérou. On observait des séries soit complètes, si
c'était possible, soit partielles, qui toutes comprenaient la référence, jusqu'à ce que l’on
eût associé 20 fois chaque direction à la référence dans 20 calages équidistants de 5 grades. 1

ua sl:

L’altitude des 70 stations du réseau définitif varie de 2m (Machala) 4 4537™ (Yana-Urcu) ;
leur altitude moyenne est 2992m. Sept officiers observateurs: MM. de FoNLONGUE, MAURAIN,
LACOMBE, LALLEMAND, PEYRONNEL, DuraxD, PERRIER, ont été occupés aux mesures d’angles
pendant les 4 ans et 7 mois qu’elles ont duré. Si l’on fait le décompte pour toutes les
stations du nombre de jours compris entre la première et la dernière observation d’angle 5
faites en la station, on trouve au total 1638 jours. Voici les stations oü les observations

azimutales ont duré le plus longtemps:

Lu li |

Altitudes provisoires

mem RASE Los
EEE

El Pelado (occupée 2 fois) . . 4149m 142 jours
i Guachanama . ee 3086 eb, :
| Man 2... .% 4515 8, i
i Nirador 0. lou à 3830 au,
i Pinllar 1. 0 . . . 2875 OS 7 ;,
i 1
| Six stations ont &t& oceupees deux fois par suite de modifications reconnues indispen- i

sables dans l’enchainement primitivement projeté. On trouvera dans le fascicule consacré
aux angles azimutaux, qui va paraitre prochainement, les explications les plus complètes à
ce sujet. Ces explications sont d'autant plus nettes et détaillées que la réoccupation des
stations en question à malheureusement donné lieu à des commentaires écrits, malveillants,
de la part de personnes incompétentes en géodésie. Nous rappellerons en passant que le
nombre de destructions de signaux par la foudre ou la tempête a été de 4 et celui des

destructions par les indigènes de 17.

ne ro

 

Vu l'altitude considérable de la plupart des stations, et pour diminuer l'influence
de la correction à apporter à la longueur de la base fondamentale de Riobamba (altitude
moyenne 2786m) pour la ramener au niveau de l’ellipsoïde de référence, on a choisi pour

2: e=uaaens nme Bd |

Br Rene

Da

 
