 

miei

we;

x
=
x

 

317

Pendant l’été de 1906 Mr. Gazris, Commandant d'État-Major, fit avec ce même
appareil et les mêmes pendules des observations au Cap Peñas, à Santander, au Faro de
Igueldo et à Izarru. L'année suivante, en été, il continua les observations à Arbas, Vireno,
Camposamos, Villagarcia et la Coruña utilisant le même appareil avec les pendules 46, 47,
65, 66, 104, 105, 106 et 107. ie

Les constantes de la température des quatre derniers pendules furent déterminées 4
Potsdam par le Dr. Prof. HAASEMANN, et celles de la densité de l'air à Madrid par M. Gazmis.

En 1907, en été, le Lieutenant Colonel d'État-Major Mr. Mrrsur fit des observations
avec l'appareil déjà mentionné et les pendules 104, 105, 106, 107 à Huelva, Tarifa, San
Fernando, Llansä, Ripoll et Jaca et actuellement il continue avec les mêmes instruments
les observations à Tudela, Logroño, Pamplona, Ronceveaux (sommet Burguete), Reinosa, Burgos
et Palencia.

Toutes les observations antérieures sont réduites avec la plus grande activité possible,
et on en rendra compte dès qu’on en connaîtra les résultats.

II]. NIVELLEMENTS DE PRÉCISION.

Les travaux de nivellement de précision exécutés dernièrement sous les ordres de
M. l'Ingénieur Mier ont été ceux des lignes suivantes:

Terminaison de la ligne de Malaga à Almeria par la grande route et nivellement
d’une ligne au sommet géodésique ,Baños” ayant une longueur de 74km. avec 10 bornes
de bronze dont 5 du premier, 5 du second ordre.

Terminaison du nivellement de la ligne de la voie ferrée de Séville à Merida d’une
longueur totale de 240 km., dans laquelle on a placé 17 bornes de ler et 39 de 2e ordre.

Répétitions des lignes de Lerida à Vich et de Vich à Figueras par la grande route
nivelées en deux directions.

Ligne de Alicante à Murcia par la voie ferrée, longueur 80 km. avec 11 bornes de
ler et 15 de 2e ordre.

Ligne de Huercal Overa à Granada par la voie ferrée, longueur 227 km. avec 22
bornes de ler et 33 de 2e ordre.

Ligne de la voie ferrée de Granada à Bobadilla, La Roda, Utrera et Cadiz ayant
une longueur de 380 km. avec 36 bornes de ler et 41 de 2% ordre.

Pendant la campagne de l’année dernière on a placé 77 poteaux indicateurs de l'altitude
au dessus du niveau de la mer dans toutes les gares de la voie ferrée de Madrid à Zaragoza,
de Zaragoza à Alsasua et en divers édifices publics de Madrid.

Par ces travaux les lignes de nivellement de précision ont été augmentées de 1001 km.
avec 232 bornes de bronze, dont 101 de 1er ordre et les autres de 2e ordre.

IV. MARÉGRAPHES.

On a continué le calcul des courbes enregistrées par les marégraphes établis dans
les ports de Cadiz, Alicante et Santander,
