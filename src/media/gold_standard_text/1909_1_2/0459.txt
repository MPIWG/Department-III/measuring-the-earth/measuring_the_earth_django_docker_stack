 

ANT

TT i Dal ja

x
F

 

TABLE DES MATIÈRES — INHALTSVERZEICHNISS.

Procès verbaux des séances de la seizième Enns le
générale de l’Association géodésique internationale réunie à Londres
et à Cambridge du 21 au 29 Septembre 1909.

Séance d'ouverture, Londres, Mardi, 21 Septembre 1909 .

Liste des délégués et des invités
Discours d'ouverture de M. Haldane .
Discours de Sir George H. Darwin
Réponse de M. le Général Bussot
Rapport de M. le Secrétaire perpétuel i ; . - :
Élection du vice-président; M. Darwin est nommé; il accepte sa nomination
Nomination de la Commission des finances

Rapport de M. Helmert sur les travaux du Br caer al
Rapport de M. Albrecht sur le service international des latitudes.
Nomination d’une Commission des latitudes

Deuxième séance, Mercredi, 22 Septembre.

Lecture du procès-verbal
Communications administratives
Rapport de M. Madsen sur les travaux Exton en Danner
Lettre de M. Sanchez . &
tapport de M. Helmert sur les anale
Observation de M. Darwin relative aux triangulations a au danada
tapport de M. Bourgeois sur les mesures de base
Discussion de ce rapport par MM. Bassot, Gautier, Ditmar Gill, Hay ford Bate 4, Keeling,
Me Caw et Bourgeois À :
Proposition de MM. Bourgeois et Can melee a 6e
Rapport de M. Albrecht sur les déterminations de longitude, latitude et a
Rapport de M. Lallemand sur les nivellements. :
Rapport de M. Hecker sur ses observations avec le pendule Kor izontalı de Dale
Communication de M. Lallemand sur la déformation de l’écorce terrestre par l’attraction de i

lune et du soleil 5

Troisieme séance, Jeudi, 23 Septembre

Lecture du procès-verbal
Continuation de la communication die M. tolls

Page, Seite

3—28
Sl
6—7
no

310
10—21
21

21

22 —25

26—28

28

29-—34

29

29

30

30

30

30
30—31
31—33
33

99

99

33
33—34
30—39
30

30
