ah did)
Std

Ton OI RYE Bw tere pe

83

von Herrn Darwin in seinem Berichte erwähnten Berichts des Herrn Eérvés zukommen
lassen, mit der Bitte diese unter die assoziirten Akademien verteilen zu wollen,

Endlich erhielt ich noch von Seiten der Akademie der Wissenschaften in Paris, durch
Vermittelung ihres ständigen Seeretärs, unseres Kollegen Herrn DARBoUX, eine grosse Anzahl
Exemplare einer Abhandlung des Herrn Marcez BRILLOUIN »Sur l'ellipticité du Géoïde dans
le tunnel du Simplon”, welche durch die Güte des Zentralbureaus an die Herren Delegirten
versandt worden sind.

Bei der Wahl des Zeitpunktes und des Ortes der 16en Generalkonferenz, welche nach
Art. 5 dem Präsidium obliegt, hat es sich natürlich in Verbindung gesetzt mit Sir GEORGE
DARwIN, der in der 7en Sitzung der Budapester Konferenz erklärt hatte, dass, obwohl er
von seiner Regierung nicht ermächtigt sei eine officielle Hinladung an die Konferenz zu
richten, er sich glücklich schätzen würde, wenn die nächste Konferenz ihre Sitzungen in
England abhalten wollte. Herr Darwin hatte dle Güte uns mitzuteilen, dass im Falle diese
Sitzungen in England statt finden sollten, er vollkommen bereit wäre die nötigen Vorbe-
reitungen für die Generalkonferenz zu treffen, und es ist auf seinen Antrag dass das Prä-
sidium den Beschluss gefasst hat die ersten Sitzungen in London, die letzten in Cambridge
abzuhalten. In dieser Weise werden die Delegirten das Vorrecht geniessen, einerseits mit
verschiedenen Männern der Wissenschaft zusammenzutreffen, welche bis jetzt noch nicht an
unseren Konferenzen teilgenommen haben, andererseits die schöne Stadt -mit ihren alten
der Wissenschaft gewidmeten Palästen zu besuchen, wo NzwToN lebte und seine unsterb-
lichen Principia schrieb.

Das Präsidium hat den Herren Delegirten diesen Beschluss im folgenden Zircular
zugehen lassen.

Internationale Erdmessung. Nızza—Leiden. 9. März 1909.

HOCHGEEHRTER HERR KoLLEGE,

In der Sitzung vom 28en September 1906 der XV. Allgemeinen Konferenz der Inter-
nationalen Erdmessung in Budapest machte Herr G. H. Darwın den Vorschlag, die nächste
Allgemeine Konferenz in England abzuhalten.

Im Anschluss an diesen Vorschlag und nach weiteren Verhandlungen mit Herrn
Darwin, hat das Präsidium beschlossen, dass die Sitzungen der XVI. Allgemeinen Konferenz
in 1909 teils in London, teils in Cambridge stattfinden sollen,

Die Eröffnungssitzung ist festgesetzt auf:

Dienstag, den 2]. September 1909, II Une yorm aa

in den Salen der » Institution of Civil Engineers”, Great Georgestreet, Westminster, London.
Nach dem 26ten September werden die Sitzungen in Cambridge fortgesetzt werden.

 
