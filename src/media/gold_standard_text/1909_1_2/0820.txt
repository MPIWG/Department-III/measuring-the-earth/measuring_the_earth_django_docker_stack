334 |

C. Inventar der bei dem Zentralbureau befindlichen
Instrumente und Gegenstände der Internationalen Erdmessung.

    

Vergl. die Berichte von 1901, 1904, 1905 und 1909.

Das im vorigen Bericht erwähnte Fernrohr ist an Herrn Prof. Dr. Hucksx,
Direktor des Zentralbureaus der Internationalen Seismischen Assoziation in Straßburg i. E.,
ausgeliehen worden.

Angeschafft wurde 1 Zöruner’sches Horizontalpendel für Pribram. Dazu u. a.
1 Spaltlampe und 1 Handlampe.

Ferner wurde für den Breitendienst beschafft eine Mercedes-Rechenmaschine.

Die Bibliothek zählt 689 Nummern.

Potsdam, Februar 1911.

F. R. Helmert.

PS BETRETEN EO ST OUR ARE Ts SSS aS SRS EERE SRNR ET

ue

   

be eel tu Mand ad ed reraass

RES eS OE er

 

Se ET METRE

SE TS a ERO

 
