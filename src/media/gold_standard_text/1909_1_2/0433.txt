 

=

et

"|

x
x
x
=

369

 

 

Designation} Inner radius of | Outer radius of Ei of
of zone | zone in degrees | zone in degrees a .
18 1 29 58 Lars 1
17 1 41 13 1 54 52 1
16 1 54 52 2 11 53 1
15 211 53 2 33 46 1
14 2 33 46 3: os. 1
13 3 3. 4 19 13 16
12 4 19 13 5 46 34 10
11 5 46 34 7 51 30 8
10 7 51 30 10 44 6
9 10 44 9 4
8 14 9 20 41 4
7 20 41 26 41 9
6 26 41 35 58 18
5 35 58 51 4 16
4 bl 4 2, 1 12
3 HAS 105 48 10
2 105 48 150 56 6
1 150 56 180 1

 

 

 

For the numbered zones it was found to be more convenient to use the radius of the
zone in degrees and minutes of a great circle than in meters. The inner radius of zone 18
is the same as the outer radius of zone O. As zone A commences at the station and zone 1
ends at the antipodes of the station all the zones together cover the earth completely.

For each zone a special reduction table was prepared giving the relation between
the mean elevation of the surface of the earth in each compartment of that zone and the
effect of the topography and the isostatic compensation in that compartment upon the
intensity. of gravity at the station.

In making the arbitrary selection of radii of zones and of the number of compart-
ments in each zone, it was necessary to consider the effect of the size and shape of the
compartment; first, upon the time required to complete the computations, second, upon the
accuracy of the computations in so far as it depends upon the accuracy of the estimates
made by the computer of the mean elevation within each compartment and, third, upon
the accuracy of certain necessary assumptions in the computation. It is believed that the
size and shape of each compartment is so fixed that the error of computation for any com-
partment is ordinarily less than 0,0002 dyne and is of the accidental class. It is known
that success has been obtained in securing rapid computation.

COMPUTATION OF REDUCTION TABLES.

For zone A, comprising the surface of the earth in a circle, around the station with
a radius of two meters, the reduction table was computed by formula (8).

The effect of the topography in this zone is the effect of a cylinder of material
having a density, 3, assumed to be the mean surface density of the earth, namely, 2,67,
having a radius c= 200 centimeters, and a length equal to the elevation of the station.
In the formula h = zero for this case, as the station is at the end of the cylinder in question.

 
