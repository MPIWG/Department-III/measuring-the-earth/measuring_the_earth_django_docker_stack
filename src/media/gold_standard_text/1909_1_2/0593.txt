HR oe

 

Ian RT

LUE PEUR DU D EE

x

 

117

On peut alors faire un graphique représentatif de ces écarts (voir p. 118) en prenant
des abscisses proportionnelles au rang des diverses portées mesurées et portant les écarts
en ordonnées. Nous avons obtenu ainsi les deux courbes discontinues tracées en pointillé et
correspondant, l’une à la mesure d’aller, l’autre A la mesure de retour.

Nous avons alors cherché si la loi des écarts ne pouvait être représentée par une
courbe continue. Cette loi doit exister si le fil a réellement subi une contraction progressive
fonction du temps ou plutôt du nombre des épreuves de tension auxquelles il a été soumis
au cours des opérations de mesure, c’est-à-dire du nombre des portées mesurées.

Les résidus
entre les valeurs fournies par cette loi et les écarts r

éellement constatés devront alors pré-
senter l'allure des erreurs accidentelles, et par suite notre courbe continue devra être tracée

de telle sorte que l’aire limitée par la courbe discontinue ne soit pas changée.

On satisfait très sensiblement à cette condition en prenant pour la mesure d’aller la
courbe exponentielle

y = Onn,3 + Omm,27 e— 0,013 x

et pour la mesure de retour, la droite
y = 0mm,045 — 0,0002 x

presque paralléle a l’axe des x.

Si le fil mavait subi que Vinfluence des contractions provenant de manipulations
répétées, ces deux courbes devraient être le prolongement l’une de l’autre; la discontinuité
très nette qu'elles présentent, semble indiquer que c’est entre les deux mesures de la base
que le fil a éprouvé les légères avaries qui ont été ultérieurement constatées à Breteuil.

La première courbe est asymptote à la droite y — + Omm,3, qu'elle atteint très
sensiblement pour x — 263. A la fin de la mesure d'aller, le fil avait done presque atteint
son état stable; il l’a conservé pendant la mesure de retour, mais il avait subi dans l’inter-
valle, par suite d'accident, une petite déformation permanente.

Les opérations terminées, l’action du éemps a très certainement continué dans le même
sens que celle des manipulations, mais beaucoup plus lentement, et pendant la mesure, cet
effet a pu être négligé devant l’autre.

On a: Ayre dur = Riot duo:
d’où Xiot = Anite + Ou1—910) = Auris FAO.
D’autre part, en désignant par y,,, 74, les corrections de température

X,ot=X,,+%0;3 Ait = À, + Vo:

2

donc:
Lio = A1 + (War -Yao) FO. 0,0) — À, + A(y + 5).
Enfin, nous avons adopté une valeur A,, telle que
A, = AG
On a donc finalement:
Lio ie = A(y+è+e)
et le second membre représente précisément la différence des valeurs de la portée mesurée à t° au moyendeA,, etdeA,,.
