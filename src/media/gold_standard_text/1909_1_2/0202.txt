 

N et m mm a an m ——
2 oo eee es Se

 

 

 

 

 

 

 

184 :
3
:
| ger Qu le Janvier 1909, du niveau moyen de la mer, :
d’apres les médimarémétres,
| avant eb après correction de l’erreur systématique.
Nombre Altitudes rationnelles | Cotes rationnelles rapportées
É Date d’années rapportées au niveau moyen
| Postes d'entrée de au zero normal du médimarémétre N°, 2
N fonctionne alle
i d’observations oe , |Altitude| Altitude) Oor- |
| fonctions er brute | corrigee | rection Cote, | Coe Correction 4
normal A B B-A |brute | corrigée 1
| C D D-C
i 1 2 3 4 5 6 7 8 9
q cm. cm. cm. cm. cm. cm. 3
i I. Manche. 3
i |
; Obecbouree 2 0, . | 1891 18 — 12 — 8 +4 |—10 | —10 0 à
| II. Ocean. 7
| Curt  : . . .| 1800 19 — 8 0, 13, 3,9 0 =
hi Bulbeom 1 2 2... 1889 20 Beal ad Ho 2729|, 4.1 —]
| Menebles,  . . |. . | 1892 14 HG =. Lo E 4) 45 — 1
a In males 0... |: 1891 10 + 6 + 12 a6 7 + 9 + 2
N ee) 17 Ha +48 | 42 2151| 414 — 1
| SUR de Du |. .| 1890 19 0) 5 le
: IH Méditerranée.
i Dito |) Jess 21 — cl hen. ge 1 1
i Me Oita. 2 | 1898 16 Peat D) RE ==
i Port Vieux (Marseille) . .| 1890 19 a al 0 a
j Marseille (Anse Calvo).
I Médimarémètre N°. 1 j
| (1885 à 1905) . .| 1885 24 a al ay n n n 1
d Médimarémètre N°. 2
(depuis 1800), | 1890 19 — 1 + 2 + 3 0 0 0
Médimarémétre N°. 3, double:
Tube N°. 1 (depuis 1906) . 1906 3 — 3 + 1 +4 " 7
id N°2 id. ) 2. 1906 3 — 6 0 + 6 7 1 7
Médimarémètres Nos 1, 2 et 3
combinés 1) (depuis 1885) .| 1885 24 — 1 + 9 +3 n 1 1
Manigues = 5 0. .. «| 1894 15 — 2 + 2 + 4 0 0 0
Bontzde-Bouc : : , :.. 1894 14 — 5 — 1 +4 |— A| —3 +1
Be 0... 0.02 1888 21 — 4 — ] +3 |— 3] — 4 —1
Bor Nendees. . ....| 1888 21 — 8 — 1 +2 |— 2] — 8 — 1

 

 

 

 

 

 

 

1) Les résultats de l'appareil le plus sensible, pour chaque période, étant seuls utilisés,

 

2 idiot haut Sid |

RE

 
