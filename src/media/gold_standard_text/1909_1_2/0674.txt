 

198

des divers réseaux, je crois utile de rappeler la definition des zéros hypsométriques natio-
naux. J’indiquerai ensuite les relations provisoires de hauteur qui existent entre ces différents
zeros, tout au moins pour les pays d'Europe.

a) Definition des zéros hypsométriques adoptés dans les divers pays ').

EUROPE.
1. ALLEMAGNE.

a. Bade: Normal Null prussien.

b. Baviere: Normal Null prussien. Les lignes qui le relient aux quatre localites
Elm, Kahl, Coburg et Obersiemau, oti se fait le rattachement des deux réseaux
bavarois et prussien, ont toutefois reçu auparavant les corrections orthométriques
nécessaires, comprises entre 0™,028 et 0™,037,

c. Hambourg: Hamburger Null: Zéro du fluviographe principal. Un trait horizontal
gravé dans une grosse pierre du quai de l’Elbe, a Valtitude 10™.317 sert de
point de départ. Le zéro de Hambourg est situé à 82,538 au-dessus du zéro
normal prussien (Normal Null).

d. Hesse: niveau moyen de la Baltique & Swinemunde; mais on a l’intention d’adopter
comme zéro définitif le Normal Null prussien ?).

e. Mecklembourg: Normal Null prussien.

‚8, h. Prusse (Geodätisches Institut, Landesaufnahme, ete.): Le zero normal prussien
(Normal Null) établi, en 1879, pour servir obligatoirement d’origine aux alti-
tudes de tous les repères du royaume de Prusse, a été placé, d’après les ni-
vellements de la , Landesaufnahme’””, au niveau du zéro d'Amsterdam. D’après un
travail du Dr. BôrsCH ?), cette concordance se trouverait à peu près exactement
realisée.

En fait et par definition, il se trouve à 37 mètres au-dessous d’un «repère
fondamental» fixé à l'Observatoire de Berlin; mais son utilisation devenant chaque
jour plus difficile, à cause de l’intensité de la circulation, son transfert, avec celui
de l’observatoire lui-même, dans une zone mieux appropriée, a été décidé et doit
avoir lieu avant que les rapides progrès de la construction dans la région sud
de la capitale n'aient rendu à peu près impossible l'exécution du nivellement très
précis qui sera nécessaire à cet effet. :

i, Saxe: Normal Null prussien.

k. Wurtemberg: Normal Null prussien.

=

aa rn ce

rer

1) Dans l’énumération qui suit, les pays ont été classés, pour chaque partie du monde, d’après l’ordre alphabétique»
les états de l’empire allemand étant groupés sous le titre , Allemagne”.
2) A. Börsch et F. Kütunen. Vergleichung der Mittelwasser der Ostsee und Nordsee, etc. ... Berlin, 1891.

 
