py emy area)

Tan ae Lae

 

it
à
2
a8
3E
i
a5
iR
4

=
=
+=
it

Annexe I.

NOTE

SUR

LA POSSIBILITÉ DE CALCULER A PRIORI LE POIDS ET LA PRÉCISION DES
RESULTATS D'UNE TRIANGULATION PAR LA SIMPLE CONNAISSANCE DE SON
CANEVAS.

PAR

M'A, FERRER.

I discussion renouvelée entre le général Ibanez et le colonel Perrier sur les avantages
relatifs des bases longues et des bases courtes et sur leur distribution convenable dans
un réseau trigonométrique, me donne une occasion favorable pour soumettre à mes col-
lègues une nouvelle manière de considérer la question dont il s’agit. La facon
d'envisager le problème de la distribution la plus convenable des bases sur l'étendue d’un
réseau, qui est proposée dans ce mémoire, supprime, pour ainsi dire, l'arbitraire et dépend
de l’application de la méthode des moindres carrés à la recherche des poids des côtés

d'une triangulation et de toute autre fonction qui en dépend, sans avoir encore exé-
cuté les observations définitives. _

I. La première opération que l’on exécute sur le terrain pour le couvrir d’un
réseau trigonométrique, est une reconnaissance pour choisir les sommets des triangles
y construire les signaux et faire un projet de canevas sur lequel les points trigonomé-
triques soient placés avec une exactitude suffisante. (Cette opération est faite d’après
des principes scientifiques et des règles pratiques qu'il est inutile de développer ici;
mais il y a une circonstance qui mérite d'être remarquée et c’est que dans la recherche
des points trigonométriques l’on procède déjà à une mesure approximative des angles
moyennant un petit instrument goniométrique, tel qu’un sextant de poche ou un théodolite
de petites dimensions. Sans cette mesure provisoire des angles, il serait impossible de
tracer le canevas avec une approximation suffisante. L’expérience prouve que dans cette
premiere mesure on ne s’écarte pas de 10’ de la valeur definitive des angles; rarement
l’ecart dépasse 5’. L’examen des travaux faits en Italie prouve que les différences entre

 
