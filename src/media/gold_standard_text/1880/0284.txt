 

La confusion qui règne ainsi sous ce rapport en Allemagne est l’image de l’état
général des choses et on ne peut espérer de la voir cesser que par le choix définitif
d’un horizon général. Pour faciliter plus tard l'entente à ce sujet et la transition au
nouveau régime uniforme, il serait à désirer qu’on s’abstienne dans les différents pays,
d'adopter provisoirement de nouveaux horizons fondamentaux qu'il faudrait cependant
abandonner dans quelques annés d'ici, et qu'on rapporte les cotes provisoires à un repère

fondamental matériel situé dans le pays, et non pas à un plan imaginaire déterminé par
une cote incertaine au dessus d’un niveau incertain aussi d’une mer quelconque.

Ainsi en Autriche on a pris pour plan de comparaison provisoire le niveau
moyen de l'Adriatique à Trieste, fixé par le repère du maréographe au Molo Sartorio,
qui, d’après les observations d’un an et demi, se trouve à 37392 au dessus de ce niveau
moyen. Mais il paraît qu'on a l'intention de le remplacer par le repère fondamental
qu’on à établi avec beaucoup de soins dans la vallée de la Drau entre Maria Rast et Faal
et qu'on suppose se trouver & 295756 au dessus du niveau moyen de lAdriatique
à Trieste.

En Belgique, afin d'éviter les cotes négatives, on a pris jusqu’à aujourd’hui
pour plan de comparaison, le niveau des basses mers à vives eaux ordinaires; il est
coté 126465 à l'échelle du pilotage fixée à l’écluse des bassins de commerce à Ostende.
L'établissement dans ce port d’un maréographe permettra de rapporter dans l'avenir ce
plan de comparaison à tout autre que l’on jugera utile de choisir. Le repère fonda-
mental jusqu'en 1877 a été la tablette de bajoyer de l’écluse susdite; sa cote était
622835. Depuis cette date on a choisi un astérisque entaillé dans la pierre au som-
met de l'échelle de pilotage et coté 622905.

En Espagne on a pris pour horizon fondamental le miveau moyen de la Médi-
terrannée a Alicante qui a été déterminé par des observations continuées pendant 2 ans,
au moyen d’une échelle de port.

En France c’est toujours le niveau de la mer moyenne à Marseille qui sert de
plan de comparaison, sans que ce niveau moyen fut déterminé jusqu’à présent au moyen
d'un maréographe ou d’une longue série d'observations directes. Par décret ministériel
de 1860 le niveau a été fixé arbitrairement à 04 au dessus du zéro de l'échelle des
marées placée à l’entrée du bassin de radoub dans l’ancien port de Marseille.

Aux Pays-Bas les cotes sont rapportées à l’étiage d'Amsterdam (Amsterdam’sche
Peil); le repère fondamental se trouve, à 5156 au dessus du Amsterdam’sche Peil, dans
une ancienne tour (Foren van de Sint Anthonie waag) à Amsterdam.

En Italie les nivellements exécutés par l'Institut topographique militaire sont
rapportés à un repére fondamental placé sur le monument de Colombo a Génes et coté
provisoirement à 22186. Les opérations conduites par M. Oberholtzer ont eu pour repère
fondamental provisoire le repère N. F. 90 que les ingénieurs suisses avaient placé à
Domodossola.

 

uf
”
i
4
i
3
3
}
!
1

esas etl a a has dan ss

 
