ae

Kern pn pe EHER

Amer

verriet

EL ETT RRR AE DE OL HR tt

t
la surface de niveau confondue avec une surface idéale géométrique. Celle-ci, sim-
plement auxiliaire, s’écartera vraisemblablement de la surface de niveau rigoureuse.
Dès lors la note annoncée devait viser cette distinction entre la surface de niveau et
la surface géométrique auxiliaire. Je demande, en conséquence, l'autorisation de la
présenter dans ces conditions.

Munich le 12 Juillet 1880. E. Adan.

Note sur la figure de la terre.

L'année dernière, & Genéve, M.M. Peters, Y. Villarceau et le Chevalier d Oppolzer
ont eu l’obligeance, après avoir bien voulu examiner les questions présentées à A
tion, de donner sur elles une appréciation bienveillante.

Permettez-moi, Messieurs, en raison de l’intérét de ces questions pour les géo-
désiens de tous les pays et & cause de mon désir d’apporter à leurs importants travaux
la part la plus grande et la plus dévouée, d’attirer de nouveau un instant votre attention
sur le résultat auquel j'ai voulu atteindre.

Les relations entre les déviations en latitude, longitude et azimut, déviations
provenant de la noncoïncidence de la verticale et de la normale géodésique, ont été
obtenues par un calcul simple, tenant compte cependant des termes du second ordre
dans la différence des azimuts. Les équations qui lient ces déviations peuvent servir
à calculer l’une d’elles quand on a les deux autres, à les vérifier ou à les corriger si
toutes les trois ont été obtenues par les différences des observations astronomiques et
des calculs géodésiques. C’est là, nous croyons l'avoir fait voir, une condition essen-
tielle pour que les trois différences en latitude, en longitude et en azimut, soient les
composantes de la déviation totale existant entre la verticale et la normale géodésique.

Le premier mémoire indique les formules qui serviront à calculer les éléments
d’un ellipsoïde osculateur convenant le mieux possible à toute la région où les observa-
tions sont circonscrites. Certainement on pourrait introduire dans les équations aux-
quelles nous avons été conduits, des corrections éventuelles pour les trois coordonnées
géodésiques des extrémités d’une chaîne, mais cette adjonction amènerait inévitablement,
croyons nous, une complication fâcheuse dans la résolution.

Les écarts résultant de la comparaison des coordonnées astronomiques avec les
coordonnées géodésiques calculées sur l’ellipsoïde dont les éléments sont corrigés,
pourront réprésenter les effets des attractions locales d’ une façon très-approchée. C’est,
me semble-t-il, un résultat important.

Mais, Messieurs, l’écart entre la verticale et la normale géodésique ne peut pas
être attribué entièrement et exclusivement à des attractions locales, à une déviation du
fil à plomb. En effet, si même la verticale en un point était rigoureusement confondue
avec la normale à un ellipsoïde de révolution autour de l’axe du monde, il faudrait avoir

 
