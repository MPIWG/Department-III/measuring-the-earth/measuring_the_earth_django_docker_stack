 

42

Bases.

yar indiqué dans mon précédent rapport, adressé & votre Commission perma-
nente, que toutes les observations angulaires étaient faites aux 9 sommets du petit ré-
seau destiné à relier la Base de Cartagena au côté Columbares-Sancti-Spiritus du réseau
général. Les 49 équations, dont 28 d’angles et 21 de côtés, résultant des 9 points et
des 36 lignes, toutes avec des visées réciproques, viennent d’être résolues avec plein succès.

L'observation des angles sur les 9 sommets compris dans le petit réseau spé-
cial de rattachement de la Base d’Olite au côté Higa-Vigas, dans la Navarre, a donné
lieu à 39 équations de condition dont on a commencé la résolution.

Toutes les jonctions des bases au grand réseau sont représentées sur une

planche que vous avez sous les yeux.

Compensation du grand réseau espagnol.

Pour deux parmi les dix groupes dans lesquels il a été partagé, les équations
sont choisies et complètement établies. Le premier comprend 63 équations et le second 68.
Aussitôt que la résolution du groupe d'équations provenant du réseau spécial de ratta-

chement de la base d’Olite au grand réseau aura été terminée, celle de l’un de ces grou- :

pes sera commencée.
Nivellements de précision.

Le calcul des lignes nivelées en 1879 qui comprennent une étendue de 772
kilomètres nivelés à double, a occupé pendant une partie de cette année le personnel
affecté à cette grande entreprise. Six gros volumes im folio viennent d’être reçus à
l'archive géodésique de l’Institut, contenant toutes les études des constantes des instru-
ments, ainsi que les calculs définitifs des lignes.

La carte qui vient d’être distribuée, donne le tracé général des lignes et des

polygones.
Maréographes.

Ceux d’Alicante et de Santander ont continué à fonctionner régulièrement, ainsi
que les stations météorologiques qui leur sont adjointes.

l Je serai en mesure de présenter à l'Association, sitôt qu’une période de 5 an-
nées se sera écoulée pour le maréographe de Santander, un premier résultat sur la dif-
férence des niveaux moyens de POcéan et de la Méditerranée à la proximité des villes
en Santander et d’Alicante, en employant les courbes tracées par les maréographes en-
régistreurs qui portent le nom de ces villes et la ligne de nivellement de précision,
doublement nivelée, qui traverse la Péninsule de l’un à l’autre de ces deux maréogra-
phes en passant par Madrid. La période d'observations pendant laquelle le maréographe
de Santander a fonctionné est maintenant de 4 ans; celle d’Alicante dépasse 6 ans. Le
maréographe de Cadiz et la station météorologique sont enfin installés et fonctionnent

 

4
4
4
i
3
i

shame tab ibe

 
