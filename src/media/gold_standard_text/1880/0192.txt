 

. Die nach meinen Formeln berechneten terrestrischen Refractionen bewegen
sich zwischen den beobachteten und die letzteren sind im Allgemeinen
während des Tages (6 Uhr Morgens bis 6 Uhr Abends) kleiner, während
der Nacht aber grösser als die ersteren.

‚ Alle beobachteten Refractionen sind auf dem Döbraberge etwas kleiner
als auf dem Kapellenberge, was wohl mit der örtlichen Beschaffenheit
dieser Stationen zusammenhängt. Daraus erklärt sich dann von selbst,
dass die trigonometrisch bestimmten Höhenunterschiede zwischen Döbra-
und Kapellenberg auf ersterem stets grösser als auf letzterem sefunden
wurden.

Die barometrisch bestimmten Höhenunterschiede bestätigen aufs Neue das
in meinen obengenannten „Beobachtungen und Untersuchungen“ zuerst
aufgestellte Gesetz, dass in Folge der Wärmestrahlung des Bodens am
Tage (wo der Boden wärmer ist als die Luftschicht zwischen den Sta-
tionen) zu grosse und in der Nacht (wo das Gegentheil stattfindet) zu
kleine Höhenunterschiede gefunden werden, gegenüber denen, die durch
genaues geometrisches Nivellement festgestellt worden sind.

‚ Aus unserer Tafel folgt, dass die Barometerbeobachtungen auf dem Döbra-
und Kapellenberge den Höhenunterschied dieser zwei Stationen genauer
angeben, als die trigonometrischen Messungen. Da dieses letztere Re-
sultat wahrscheinlich mit dem Umstande zusammenhängt, dass die Luft-
linie Döbra-Kapellenberg beinahe wagrecht ist, so habe ich vor, im näch-
sten Jahre noch eine Reihe von Messungen anzustellen zwischen drei
Punkten in und am Bayerischen Hochgebirge, welche grosse Höhenunter-
schiede gewähren und gegenseitige Beobachtungen gestatten. Bis dahin
werde ich auch die nähere Discussion der im Fichtelgebirge und Säch-
sischen Voigtlande festgestellten und in der Abhandlung „Ergebnisse aus
Beobachtungen der terrestrischen Refraction“ in allen Einzelheiten mitge-
theilten Messungsresultate verschieben. Ich hoffe, dass diese Abhandlung
längstens in vier Wochen in den Händen der Herren Commissäre der
Europäischen Gradmessung sich befinden und von ihnen wohiwollend beur-
theilt werden wird.

C. v. Bauernfeind.

- 1)

 

 

j
|

 
