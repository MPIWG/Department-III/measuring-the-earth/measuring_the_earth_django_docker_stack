aan ee

CL

|
| 4
hi
i
t

 

52
Foerster; er zieht aus der zufälligen Uebereinstimmung seiner Vergleichungen mit der
von 1852 folgende Schlüsse:
: 1. die zweifellose Zuverlässigkeit seiner und der Vergleichungen von 1852,
2. die Unveränderlichkeit der Eisenstäbe während 26 Jahren.

Dass Eisen und Zink ihre Ausdehnungs-Ooefficienten ändern, wird anderweitig
bestätigt durch die Angabe (Maassvergleichungen des geodätischen Institutes, Heft I,
Seite 44), dass die Toise No. 10 und der Zinkstab gleiche Länge hatten

im Jahre 1852 bei der Temp. von 6°591 C. im Spiritusbade
[06 , , > „ 3160, , „ Petroleumbade
” ” 1872 ” ” 39 ” 10.161 2 D ”

Diejenigen, welche sich orientiren wollen, bitte ich, die letzte Folgerung zu ver-
gleichen mit:

Compte-rendu des opérations de la Commission instituée par le ministre
de la guerre pour étalonner les règles . . . Bruxelles 1855.

Im Nachstehenden lege ich der permanenten Commission meine auf fast vierzig-
jährige Erfahrung gestützte Ansicht über die Differenz vor, die zwischen den Vergleichun-
gen der Herren Schreiber und Foerster von 1878 und den meinigen von 1872 besteht.

In den „Verbindungen preussischer und russischer Dreiecksketten,

Berlin 1857‘ habe ich auf Seite 63 nachgewiesen, dass bei den Vergleichungen der

Stäbe mit der Bessel’schen Toise zwischen den Stäben und dem Spiritus eine Tempe-
ratur-Differenz von = 0:1 Cent. besteht. Diese Differenz ist bei Vergleichungen in der
Luft sicherlich noch grösser, allein die angegebene reicht vollständig aus, um die Unter-
schiede zwischen den Vergleichungen von 1872 und 1878 zu erklären. Die Ausdehnungs-
Coefficienten in Linien sind für Centigrade (Verbindungen der preussischen und russischen
Dreiecksketten auf Seite 65 und 66) in runden Zahlen: )

Bur esses und Lenoir . ::  . — 06010 ...t
NO ee, == 0,009. 22.1
Oe lc = 0.009 : . .t
Daraus folgen die Verbesserungen für Æ 0.1 Centigr.
jur No 9 und No. 10. . ..... = # 000009
bar .. . . : — 040010.

Werden die nachstehenden Beobachtungen
(Verhandlungen der permanenten Commission von 1879)

 

 

1872 1877/78 Differenz 1878 | Differenz

Baeyer. Schreiber. | Schreiber— Baeyer. Foerster. Foerster-- Baeyer.

 

 

 

Toise No.9... | 86440010 | 86440025 + 010015 86440026 + 010016
Poise No, 10 . | 863.9979 | 863.9999 + 0.0020 863-9990 + 0.0011
Toise Lenoir . | 863-9926 | 863.9955 + 0.0029 863-9953 + 0.0027

 

 

 

 

 

 

3
3
3
3
q
I

Ah mu on 111

int add Ant ns,

 
