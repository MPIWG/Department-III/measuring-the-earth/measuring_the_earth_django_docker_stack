 

le:
ty
a
i
he
i

H

i

ie

SAE RSE

SLRS CO RER NR RE OT ERT ORTOP

EEE

me

SERS TS

eS ENTE

Sa ee

Se

10

zu kommen. Der Präsident fügt hinzu, dass er ebenfalls sehr beklage, die Coast Survey
nieht vertreten zu sehen, er begrüsse aber hier ein früheres Mitglied derselben, den
gegenwärtigen Director der Argentinischen Sternwarte Herrn Dr. Gould aus Cordoba.
Verlesen wird ein Schreiben des Herrn Hügel aus Darmstadt, worin selbiger bedauert,
dass er wegen vorgeschrittenen Alters sein Commissariat habe niederlegen müssen, er
bittet, ihm ein freundliches Andenken zu bewahren.

Der Präsident fordert die Schriftführer der permanenten Commission auf, den
Bericht derselben vorzutragen. Herr Bruhns verliest ihn in deutscher, Herr Hirsch in

französischer Sprache.

Bericht der permanenten Commission.

Nach der Geschäftsordnung hat die permanente Commission der segenwärtigen
allgemeinen 6. Conferenz Bericht über ihre Thätigkeit seit der allgemeinen 5. Conferenz
und über die Fortschritte der Europäischen Gradmessung im Allgemeinen zu erstatten.

Die permanente Commission hat sich seit dem Schlusse der letzten allgemeinen
Conferenz viermal versammelt. Unmittelbar nach der Stuttgarter Conferenz tagte sie daselbst
am 3. October 1877, nachdem sie sich am 1. October bereits dahin constituirt hatte, dass
Herr Ibanez zum Präsidenten, Herr von Dauernfeind zum Vicepräsidenten und
die Herren Bruhns und Hirsch zu Secretären gewählt waren. Sie vollzog in der
letzten Conferenz die Protocolle, beauftragte die Secretäre mit der Veröffentlichung der
Verhandlungen und erledigte einige geschäftliche Angelegenheiten. Durch Circular
wurde im Frühjahr 1873 in Folge einer freundlichen Einladung des Senats der Stadt
Hamburg für die nächste Conferenz der permanenten Commission Hamburg gewählt, wo
die Sitzungen vom 4.—8. September 1878 stattfanden. Im Jahre 1879 versammelte
sich die permanente Commission, einer zuvorkommenden Einladung des Staatsraths des
Gantons Genf entsprechend, zum dritten Male vom 16.—20. September in Genf, wo nach
Erledigung der wissenschaftlichen Gegenstände als Versammlungsort für die gegenwärtige
allgemeine Conferenz München gewünscht wurde, welcher Ort, nachdem die k. bayerische
Regierung in diesem Frühjahr ihre Bereitwilligkeit, die Conferenz in München zu
empfangen, ausgesprochen hatte, einstimmig gewählt worden ist. Zum vierten Male
endlich hat die permanente Commission gestern eine Sitzung gehalten, um die Vorberei-
tungen für die allgemeine Conferenz zu treifen, sowie die Geschäftsordnung und das
Programm nochmals einer nachträglichen Berathung zu unterziehen.

Die Verhandlungen der permanenten Commission in Hamburg und Genf sind in
deutscher und französischer Sprache mit den Generalberichten über die Fortschritte der
Europäischen Gradmessung im Drucke erschienen, so dass über dieselben kein beson-

derer Bericht nöthig ist.

 

PAIE de nah a a a Aal
