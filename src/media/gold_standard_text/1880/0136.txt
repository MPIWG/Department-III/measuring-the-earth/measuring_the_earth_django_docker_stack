 

18 RAPPORT SUR LA QUESTION DU PENDULE,

x

On n’aurait pas & s’inquieter de l’influence, et il serait inutile de mettre l’appareil
en mouvement avec une grande précision. Mais on devrait mesurer à part T,, T
mer 3

| À ÿ ; 2 à en
pour connaitre —, ou >, et faire en sorte que la fraction a1, fût négligeable.

2?

 

L
Pour cela il conviendrait que y ne fût pas trop faible; aussi M. Pierce qui ne
s’est occupé que de ce mode d’observation admet que le support doit être très peu

. , . : , , Te
ferme. Ce serait nécessaire pour une autre raison: la durée du cycle étant or LÉ

nombre d’oscillations qu’il contient est

 

a tii & peu pres, N= — =
en Ne: PER DEEE UD Vi EDR

En supposant le trépied analogue à celui de Repsold et prenant en nombres ronds
h 2 1

Dow
deux modes de suspension N = 15000, N = 30000, de sorte qu’on ne pourrait observer
un cycle entier.

1 = 079; p — 31; & — 30" par kilogramme, il en résulte pour les

2° Emploi du mouvement troublé.

Il n’y a plus dans ce cas à terminer l'expérience à un passage simultané, mais
à un instant quelconque. A chaque oscillation, comme on l’a vu, les erreurs d'influence,
d'amplitude, de l'air et du frottement, s'ajoutent sans se modifier. Il faut, d’après le
principe démontré ci-dessus, que la première soit toujours négligeable, et dans ce cas
la valeur de g se déduira de lobservation de l’un des pendules, que nous appellerons
le premier, comme s’il était isolé et sans balancement. C’est la méthode qui a été
appelée directe.

&

278 See . 4
Tout se réduit ainsi à annuller l'erreur d'influence -Z

27 donnée par la formule

N fi D . . Q . ,
(16), exacte à chaque instant. On évaluera trop faible la fraction 27 qui avec les données
et l’on en déduira le maximum numérique < que s ne

,

. , s . , . . a . .
doit pas depasser. Il faut remarquer que si le mouvement était simple, — Varierait

ci-dessus serait ou

SL ue
30000 60000
dans le même sens pendant un cycle, v pendant un demi-cycle; le support étant ferme,
l'expérience durerait beaucoup moins qu'un demi-cycle, et s varierait toujours dans le
même sens; à moins de circonstances fortuites, il en sera en général ainsi dans le
mouvement troublé; il n'y à aucune raison en effet pour qu'il amène des variations

périodiques de s.

Il est inutile de mesurer à part T, et T,, mais d’après ce qui precede on
devra mettre l’appareil en mouvement d’une manière aussi parfaite que possible, de sorte
qu’on ait au commencement ao’ = à, v = 0, et par suite s = 0. On observera de temps

,

x v t 2 , ,
a autre a, a’, v, en remarquant que — = T > étant le temps écoulé entre les passages
T

 

 
