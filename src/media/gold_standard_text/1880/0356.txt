 

EEE LEE

f
5
i
il
|
q
|
i
|
4
u
a
|

48

Fergola et A. Secchi. Sulla differenza di longitudine fra Napoli e Roma. Napoli 1871.
Nota. Si fanno le operazioni per trovare telegraficamente la differenza
di longitudine fra l’Osservatorio di Capodimonte a Napoli e quello del Collegio
Romano a Roma. Tale differenza risulta di 7" 65247 Æ 05027 essendo Roma
più occidentale. Dagli Astronomi Capocei, Nobile e Vico mediante osserva-
zioni lunarie, occultazioni e stelle cadenti erasi precedentemente trovata tale
differenza di longitudine di 7" 5556. (Mem. dell’Osservatorio del Collegio
Romano.) Il Capitano Fergola con operazioni geodetiche (1840—42)
aveva pure ricavato il seguente valore 72 651. (Conn. d. temps 1843,
pas. 92.)
Ferrero, A. Eposizione del Metodo dei minimi quadrati. Firenze 1876.

— Note sur un procédé pratique pour établir l’accord entre plusieures bases d’une
triangulation (A. N. 2068, 1880).

Frisi, P. Disquisitio mathematica in caussam physicam figurae et magnitudinis telluris
nostrae. Milan, 1751.

Giletta, L. Studio sul triangolo geodetico. Roma 1880.

Govi, G. Metodo per determinare la lunghezza del pendolo. ‘Torino 1866. (Mem. Accad.
Torino.)

Giulio, C. L. Recherches expérimentales sur la résistance de l’air au mouvement des
pendules. (Turin, Ac. Mém. XIII, 1853, pp. 299—357.)

— Sur la détermination de la densité moyenne de la terre etc. (Turin, Acad.
Mém. 1840, pp. 379—84.)

Inghirami, G. Di una base trigonometrica misurata in Toscana nell’autunno 1817.
Firenze 1818.
Nota. Misura di una base di 4488.41 tese da S. Pietro in Grade
a Stagno fra Pisa e Livorno per opera di G. Inghirami, il quale vi apoggid
una rete trigonometr. della Toscana.

Instituto topografico militare. Parte I. Geodetica.

Fascicolo I. Descrizione dell’apparato di Bessel. Primi studii ese-
guitivi. Misura delle basi di Foggia e Napoli. Napoli 1875.

Fascicolo II. Nuovi studii sull’apparato di Bessel. Misura delle basi
di Catania, Crati e Lecce. 1876.

Fascicolo III. Cenni preliminari sulla triangolazione de 1° ordine
eseguita lungo la zona meridiana da Capo Passero a Lissa. Osser-
vazioni della rete di Capitanata e suo collegamento con la trian-
golazione austriaca sulle coste Dalmate. 1877—78.

Misura della base di Udine eseguita nel 1874. 1877.

 

|

 
