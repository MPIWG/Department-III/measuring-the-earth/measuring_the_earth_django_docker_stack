 

Alec
Bir
i
fi
i
IE
I:
i
Ib
N
if
| tf

EEE

 

|

 

2

Pour faciliter le travail de coordination, il serait utile de résumer les données
sous forme de réponses aux questions suivantes:

1.

Quelle est la longueur en kilomètres des lignes nivelées jusqu’a présent?
Combien de repères a-t-on fixés jusqu'à présent?

Avez-vous des repères de différents ordres et de quelle nature sont-ils?
Quels sont les points de jonction de votre réseau avec ceux des pays limi-
trophes? et éventuellement: dans quels points aboutit-il a la mer? dans ces
points existent-il des maréographes ?

Avez-vous rapporté les cotes de votre réseau hypsométrique à un plan de
comparaison provisoire? quel est le repère fondamental qui le détermine ?
et quelle cote provisoire attribue-t-on à ce repère fondamental ?

Description sommaire des appareils employés:
a) Ouverture et grossissement de la lunette; nombre et distances des fils
du réticule ;
b) Valeur d’une partie du niveau;
c) Division des mires; de quelle façon reposent-elles sur le sol, pendant
l'opération? par quel moyen s’assure-t-on de leur verticalité?

Description sommaire de la méthode d'opération:

a) Toutes les lignes sont-elles nivelées à double? et dans ce cas est-ce
que les deux opérations sont toujours faites en sens inverse?

b) Est-ce qu’on observe exactement dans l'horizontale, où seulement
approximativement à quelques parties du niveau près, dont on fait la
lecture à chaque visée, pour en tenir compte par une correction?

c) S’astreint-on à rendre les distances exactement égales dans les visées
en arrière et en avant, de façon à éliminer l'effet des erreurs instru-
mentales? ou se contente-t-on d’une égalité approximative (en faisant
p. ex. compter les pas au portemire) et tient-on compte ensuite du
reste d'influence des erreurs instrumentales ?

d) Est-ce que les erreurs instrumentales sont déterminées périodiquement,
et à quels intervalles ? a

e) Est-ce que les équations des mires sont déterminées périodiquement ?
par quel moyens et par rapport a quel étalon?

7. Calculs de réduction et de compensation:

a) De quelle manière applique-t-on éventuellement aux lectures directes
les corrections nécessaires pour tenir compte de l'inclinaison de la
lunette, des erreurs instrumentales et des équations des mires?

b) Quelle est la méthode suivie dans la compensation des polygones et
du réseau?

 

i

reed drg-sa0t dde) 4e

on co he ie idl don ana
