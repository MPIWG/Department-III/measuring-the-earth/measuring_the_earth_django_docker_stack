 

BE

(Eee

ER

 

i
3
;
+
i
|
i
3
I
5
il
1

nn nenn
ee aE

Désignation Année Noms Erreurs
Noms des Pays. de de la des ‘Longueurs. probables et
la base. mesure. observateurs. relatives.

 

 

 

Nous avons enfin & compléter deux lacunes du rapport de 1877 en formant les
deux tableaux suivants relatifs aux bases mesurées par Tenner en Pologne, de 1846 a
1848 et à celles qui ont été mesurées en Amérique, antérieurement à l’année 1875.
Les bases de la Pologne, au nombre de 3, servent de fondement à la Triangulation du
royaume, au moyen de laquelle est réalisée la jonction des triangulations de la Russie,

de l'Autriche et de l’Allemagne.

Les données relatives aux bases Américaines, au nombre de 13, ne nous sont
pas encore parvenues; elles seront transmises au bureau central avant le 1° Décembre,
pour être imprimées dans le rapport définitif.*)

 

 

 

 

Base de Varsovie | 1846 Tenmer et Slobin 2710-4847 |
Admis par
Struve pour
Russie les bases de
(Pologne) | er |
. Base de Tarnograd | 1847 Iwanow 1” | 2522°0572 erreur rela-:
tive
a
Base de Czenstochau | 1848 Iwanow 1” 2048°4375 any |
|
|
| |

*) Die oben genannten Angaben sind bis jetzt dem Centralbureau nicht zugegangen. B.

 
