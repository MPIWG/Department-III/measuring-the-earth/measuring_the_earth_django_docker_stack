 

SE AER SETS Se ee aI

EBEN

AN SEEN ee REND

SE SE I TS

iy

!

i

ie
=

i

X

N

i

|

i

|

/

SY TORS Sea REEL EN

SPER a REESE SESE ERTS

92
Klostermann. Recherches sur le degrés du méridien entre Paris et Amiens et sur la
jonction de l'observatoire de Greenwich à celui de Paris. St. Petersb.,
1789.
Khandrikoff. Détermination de la différence de longitude entre Kiew et Varsovie.
| (Annal. de l’Observatoire de Kiew. Vol. I, Kiew, 1880.)
Kortazzi, J. Bestimmung der Längen-Diff. zwischen Pulkowa, Helsingfors, Abo, Lowisa
und Wiborg. (Mém. de l’Acad. de St. Petersb. Série VII, Tome XVII.)
Se Petersb, 1871. (S. auch Band XXXII, p. 90.)
Krafft, G. W. De figura terrae. (St. Petersb. Acad. Sc. Comm. VIII).
Krafft, W. I. (Sohn des Vorigen). Analyse des expériences faites en Russie sur la
longueur du pendule à secondes. (St. Petersb. 1789 etc. Ac. Sc. Nova
acta VII, pp. 215—28).
— De tempore oscillationis pendulorum, dum arcus datae amplitudinis cujus-
cunque describunt. St. Petersb. 1792. (Ac. Sc. nova acta, X, pp. 225—42).
Lenz, E. Physikalische Beobachtungen, angestellt auf einer Reise um die Welt unter
dem Commando des Capt. von Kotzebue in den Jahren 1823—26. St.
Petersb, 1831. (Ac. Sc. Mém. I, pp. 221—44).
Lütke, ©. F. P. Observations du pendule invariable exécutées dans une voyage autour
du monde 1826—29. St. Petersb. 1831. (Ac. Sc. Mém. I, pp. 11—15.
Phil Mas D 1882 pp. 420 4 Roy. Inst. Journ. I, 1831, pp. 602—4.
St. Petersb. 1837. Mém. Sav. Étrang. II, pp. 1—242).
Marine-Ministerium. Tpuronomerpuuecxaa CHémxka Oeperogs Barriñckaro MopA, mpo-
HSBeNeHHAA TONB HAYANECTBOMB Ten.-Teür. IMydepra cs 1829-ro mo
1838-o% rogs. C.-Ilerep6yprs. Macrs I-aa 1867 r., uacre Il-aa 1872
r., vacts Ill-aa 1878 r. Msranie Innporpasmueckaro ‚Iemapramenra.

(Trigonometrische Vermessung der Küsten des Baltischen Meeres, unter der
Leitung des Gen.-Lieut. Schubert in den Jahren 1829 —1838 ausgeführt.
St Betersburs Band I, 1867; Band Il, 1872; Band III, 1878. He-
rausgegeben von dem Hydrographischen Departement des Marine-Mini-
steriums).
Moritz, A. Ueber die Anwendung des Pistorschen Reflexionskreises zum Messen von
Angulardistanzen zwischen terrestrischen Objecten. Tiflis, 1859.

Manganari, M. Cremxa mapmapnaro mopa. 180.
(Aufnahme des Marmara-Meeres. 1850).
Mallet, J. A. Observationes variae in Lapponia ad Ponoi institutae. Observ. Petropoli
et Poni institut. ad longitudinem penduli minuta secunda indicantis deter-
minandam. St. Petersb. 1769. (Ac. Sc. nov. comm. XIV, 2, pp. 14—5,
24—33).
— On the transit of Venus, the lengths of pendulums etc. (Trans. 1770.)
Napiersky. Die Polhöhe von Mitau. Mitau 1873.

 

i

dus lh a a al a

 
