 

 

19

20

1°

20

30
4.0
Le

: : , 18

À. Travaux géodésiques.

On continue avec une grande activité la reconnaissance géodésique et la
construction des signaux de manière à être assuré qu'elle sera terminée
dans la prochaine année 1881.

On continue les observations de premier ordre dans la région du Piémont
et plus particulièrement vers la frontière française et la frontière suisse.
Ces mêmes observations sont poursivies dans l'île de Sardaigne sous la
direction du commandant Magnaghi.

On travaille à compléter une petite construction à côté de l’observatoire
Arcetri près de Florence, qui doit servir à la comparaison de mesures.
Les nivellements de précision son continués assez activement et on a assigné

à ce travail pour cette année quinze mille livres.

On continue les observations maréographiques et on travaille à relever les

résultats des anciennes observations.

On travaille aux calculs et aux publications géodésiques.

B. Travaux astronomiques.

Le professeur Schiaparelli s'applique aux observations géodésiques et astro-
nomiques dans la région comprise entre Milan et Parme pour constater
et étudier les déviations locales signalées par les astronomes dans les tra-
vaux de la première moitié de ce siècle.

Par les soins du professeur Lorenzoni, qui dans ce moment est parmi nous,
on commencera les recherches avec le pendule à reversion.

On achetera de nouveaux instruments astronomiques.

On continue les calculs et on travaille aux publications.

canevas des travaux de la commission italienne qui est sous vos yeux ser-

vira à completer les renseignements que j'ai eu l’honneur de vous communiquer.

M.

Le Président de la commission italienne

E. Mayo.

eee

Pays- Bas.

Stamkart a continué la triangulation pendant les mois favorables de l’année.

L'œuvre qu’il s’est proposé d'exécuter touche à son accomplissement.
En effet étant déjà avancé jusque dans la province la plus au Nord-Est des

Pays-Bas,

il ne lui restait que les quatre dernières stations sur le territoire Neer-

landais, Uithuizermeden, Holwierde, Midwolde et Onstwedde, pour y faire des observations
complètes.

 

j
i
À

 
