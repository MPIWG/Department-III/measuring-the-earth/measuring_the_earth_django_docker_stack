 

4

Hilgard, J. E. Method of using the transit instrument for azimuth-obsery. (R. C.S.
f. 1856, pp. 208—209.)
— Theodolite-test. (R. C. S. f. 1856, pp. 310 —316.)

— Missisippi Sound, Triangulation. (R. C. 5. f. 1856, pp. 291—292.)
— Base apparatus for measuring subsidiary lines. (R. C. 8S. f. 1857, pp. 395 — 398.)
— Base-measuring apparatus. (R. C. 8. f. 1862, pp. 248—255.)

— Repeating-theodolite. Supplement to the method of testing (described in the
preceding paper). (R. C. S. f. 1860, pp: 357—361.)

— Results of experiments for determining the length of the six-metre standard-
bar and its rate of expansion by heat. (R.C.S. f. 1862, App. No. 26.)

— Railways, on the use of, for geodetic surveys. R,. C.S. f 1867, pp. 140—144.)
— Reflector. Description of a new form of geodetic sienalss (kh. C. S. £. 1867,
p. 145.)
— On the use of the zenith-telescope for observations of time. (R, ©. S. f. 1869,
pm 220 239)
— Description of two forms of portable apparatus for the determination of per-
sonal equation etc. (R: C. S. f. 1874. App. No. 17.)

— Transatlantic Longitudes. Final report on the U. S. Coast Survey determi-
mations, of 1872. (BR. C. S. 1874. App. No. 18.)

Hunt, E. Base-Measuring apparatus, description of, as used in the coast survey.
(RY ©. 8. 1. 1854, pp. 103—108.)

Jimenez, F. y Fernandez, L. Determinacion de la Longitud del Pendulo de Segundos,
y de la gravidad en Mexico a 2283" el Nivel del Mar. (Mexico, 1879.)

Kendall, E. C. Moon-culminations. Observed at High School observatory, Philadelphia.
(Re C5. © 1854 p. 120.)

Kummel, Chas H. Strenge Gleichungen zwischen den Seiten eines Dreiecksnetzes auf
irgend einer Oberfläche, insbesondere auf Bessels mittlerem Erdsphäroid.
A. N. No. 2116.

Lindenkohl, A. Solution of the ‚three point problem“. (R. C.S.f. 1869. App. No. 14.)

Lovering, J. On the determination of transatlantic longitudes by means of the telegra-
phic cables. 1873. (Mem. Amer. Acad.)

Mansfield, J. On the Figure of the Earth. (Connecticut Acad. Mem. I, 1810, pp. 111—8.)

Merriman, M. A List of writings relating to the method of least squares. (Transact.
of the Connecticut Acad. Vol. IV.) 1877.

Mitchel, 0. M. Moon-culminations etc. (R. C. S. f. 1858, p. 190 and 1859 p. 278.)

Newcomb, 8. On the possible variability of the Earths axial Rotation. (Amer. Journ.
of Sc. and Arts, Vol. VIII.) 1874.

Peirce, B. Note upon the conical pendulum. (Gould, astronom. Journ. II, 1852.)

 

1
i

el ue all a da lh a ee

 
