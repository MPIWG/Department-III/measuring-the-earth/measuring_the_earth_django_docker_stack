SO

aS ET EEE ENE IEE EI
ERSTES zz = DS EE
= 3

NE en
*

ea een

 

Triangulation du Parallele de Paris.

12

(Partie Occidentale.)

 

CNE PE

 

 

184

 

 

 

SBurfe . . |... .

 

 

 

46 59 53 | 22 48 35

 

 

 

 

 

 

 

 

 

 

0 Indication des . | Lonoi 1 2 Directeur
N° titude.' > : str Pad
ts Latitude.‘ ie Epoque one a Instrument. Remarques.
| | Î ji
1 | Panthéon . .. ... | |
3|Belle Assise .... Déjà mentionnés. |
4 | St. Martin du Tertre |
a yelzy.:...... 48°47! 7"|19°51' 14")
139 | Les Alluets le Roi . | 48 54 53 | 19 34 49 |
449 Le Perray ..... 49 41 4419 31 5)
141 Pyramide de Broue 48 45 8 | 19 10 57 | |
142 Gatelles ...... 48 32 40 |18 57 O | = N
143 | Granvilliers ..../48 48 55 | 18 43 27 | > |
144 La Ferté Vidame .|48 35 86 |18 32 14 = |
145 | Les Houlettes . 48 46 41 |18 12 44 | ® |
146 | Mortagne . . . . .. 48 42 20 | 18 12 33 | > |
147 |Ecouves ...... 43 33: 2 17 41 49 | = |
148 | Champ Haut ....148 43 0 1759 2| = |
149|Montabard..... 48 48 53 | 17 34 49 | \ 3 z |
150 | Charlemagne . . . .|48 38 58 | 17 13 29 | a De |
151 Les Bulleux . . . . | 48 20 17 | 17 11 40 | | Sys : |
152 La Hérouse . . . . | 48 26 23 | 16 46 28 | a Sen ä |
153 | St. Martin de Chau- | E38 3 |
lente 2 2 .. 48 44 12 |16 48 3 = AZo a |
154 | LesTroisCheminées | 48 46 36 |16 22 7 | oe = |
155|Montjoie ...... 48 82 0 | 16 22 19 | g == 2 |
156 | Cancale. . . . ... 48 40 40 15 48 44 | Ser 2 |
157 Becherel ...... 48 1745 1543 8 = re D |
158 | Saint Jean . . . .. 48 35 33 |15 17 4 | 28: à |
159 | Ménez Belair. . . .|48 19 27 15 4 43 2 Sr A
160 | Plouha . . .. . .. 48 40 37 | 14 44 1 ae Sn
161|Lanfains ...... 48 21 42 | 14 45 36 ais
162 | Ménez Bré . . . .. 4834 37 14 21 15 £ |
163 | Kergrist . . - . .. 48 18.27 | 14 22 8 | 2 |
164 | Toussaines . . . . . 48 22 93 | 13 42 32 À |
165|Plougaznon..... AS 41 49 | 13 52 11 | 2 |
166) Malllé . . . . . .. |. 14837 22 | 13 29 35 = |
167|Pencran ......- 48 26 16 | 13 25 35 = |
168 | Ménez-Home . . . .|48 13 13 | 13 25 36 = |
169 | Crozon . . . . - . . 48 14 50 | 13 10 18 |
Fo Brest... ....- 48 25 52 | 18 10 10 |
Observatoire. |
171 | Plouider  . . .. .. 48 36 35 | 13 21 48 |
172 | Base de Plouescat | 48 38 29 | 13 20 38 |
Terme Occidental.
173 | Base de Plouescat | 48 39 21 1329 6 |
Terme Oriental.
Triangulation du Parallele de Bourges. (Partie Orientale.)
A9 | Bourges. . . . . .. |
83 | Humbligny . . . .. | Déjà mentionnés. |
41 | Méry-ès-Bois . . . 8 |
174 | Pougnes. - . . . .|47 4 81 | 20 46 34 Ss |
175  Montenoison . A7 13 9 21 9 29 oS a
176 | Bois-Château . . .. 47 2 6 21 6 58 5 S =
177|Mourecon ......|47 16 42 | 21 32 5 | Ss ie =
178 | Toureau des Grands | 3 =
Bois Ae An 4 41 21 41 16 a | SE =
179|Le Grand Häbre. .|47 13 13 |21 48 52 2 à a 2
180 | Signal de Bard . . 47 8 43 | 21 59 46 Se Sg
181 | Rome-Chäteau . . . 46 54 12 22 16 35 | a 5
182 | Bessey en Chaume | 47 4 51 | 22 24 26 | oF ©
183 | Mont de Siege. . .| 47 16 12 | 22 35 0 S

ll sah u

rien

 
