 

A).

Der Präsident, Herr von Bawernfeind, übergibt das Präsidium an den Vice-
präsidenten Herrn Faye, um über die Fortschritte der Gradmessung in Bayern!) zu
berichten.

Herr Seidel veferirt über seine neuen theoretischen Arbeiten, deren Ergebnisse

er in einigen Monaten in einer gedruckten Abhandlung vorzulegen hofft. Dieselben
schliessen sich an die im Jahre 1874 von ihm veröffentlichte und auf der Dresdener
Gonferenz der europäischen Gradmessung vertheilte Abhandlung an über ein Verfahren,
die Gleichungen, auf welche die Methode der kleinsten Quadrate führt, durch successive
Annäherung aufzulösen, und haben den Zweck, die Vortheile, welche der Autor diesem
Verfahren glaubt zuschreiben zu dürfen, für die strenge Aussleichung grosser zusammen-
hängender Dreiecksnetze nutzbar zu machen mittelst der Einführung der für diesen
Zweck geeigneten Unbekannten des Problems in die Bedingungsgleichungen.

Herr Adan berichtet über die Fortschritte der Gradmessungsarbeiten in Belgien.?)

Der Präsident dankt dem Vortragenden und schliesst, indem er die nächste
Sitzung auf Dienstag um 10 Uhr festsetzt, die Sitzung um 1 Uhr 30 Minuten.

1) Siehe Generalbericht: Bayern.
2) Siehe Generalbericht: Belgien.

 

:
>
3
3
3
3
ı
q

ts us cn a nn llc db dh escalate sii 1

i
|
i
|

 
