en

mm

org

A

2
2

ik
ik
a
at
i
A
“EB
+
&
|

 

Und was endlich etwaige unvorhergesehene Aenderungen im Apparate betrifft,

so wird man dieselben stets erkennen können, wenn man vor der Abreise nach der

Station. und nach der Rückkehr von derselben die Constanten des Instrumentes bestimmt.
Immerhin wird zu einem sicheren Resultat wenigstens die Uebereinstimmung zweier
solcher Manometer-Apparate nöthig sein.

Andererseits ist Herr Villarceau zufällig auf den Gedanken gekommen, relative
Schwerebestimmungen mit Hilfe des unter dem Namen ,,isochronischer Regulator“ be-
kannten, von ihm vor 10 Jahren erfundenen und seither durch Herrn Breguet construirten
Instrumentes auszuführen. Die Verbesserungen, welche Herr Breguet an den Registrir-
Cylindern angebracht hat, haben zur Folge al dass man mit diesem Instrument den
Isochronismus innerhalb der Grenzen von 17000 bis 0, hat herstellen können; die
Geschwindigkeiten lassen sich damit bis auf —!-; genau messen.

Um die ganze Genauigkeit dieser Instrumente zu verwerthen, wird es freilich
nöthig sein, auf den Einfluss des Druckes und der Temperatur Rücksicht zu nehmen,
was die Herren Villarceau und Breguet zu thun sich vorgesetzt haben. Sie glauben, den
veränderlichen Gang des Instrumentes mit Hilfe des Taylor’schen Satzes darstellen
zu können. *

Wenn die Constanten dieses Instrumentes eine genügende Unveränderlichkeit
darbieten, so würde dieser neue Regulator ein sehr einfaches Mittel darbieten, um die
relative Schwere zu bestimmen. In der That, nach Berücksichtigung aller aus den er-
wähnten Einflüssen folgenden Correctionen, würde die Schwere offenbar von einem Orte
zum andern sich ändern, wie die Quadrate der Rotations-Geschwindigkeiten desselben
Regulators an den betreffenden Orten.

Herr Villarceau glaubt aus seinen Untersuchungen folgern zu sollen, dass man
das Programm der mit dem Studium der Schweremessungen betrauten Commission er-
weitern und dasselbe auch auf die zu relativen Schwerebestimmungen geeigneten Me-
thoden ausdehnen müsse.

Herr Villarceau übergiebt der Conferenz noch zwei Abhandlungen:

1) Mémoire sur les effets du roulement dans la théorie du pendule à réver-
sion. Extrait du Journal de l’École normale. 1879.

2) Application de la théorie des sinus des ordres supérieures à l'intégration
des équations linéaires. Extrait des Mémoires de la Société des Ingenieurs
civiles. 7. cahier, Juillet 1880. Paris.

Herr Frsch bemerkt zunächst zu dem in dem Referat des Herrn Plantamour
ausgesprochenen Wunsche, dass das internationale Maass- und Gewichtsbureau in weni-
gen Monaten im Besitz des schon vor Jahren bestellten Universalcomparators und daher
vom Jahre 1881 an im Stande sein werde, die Gleichungen der bei den verschiedenen
Pendelapparaten gebrauchten Maassstäbe zu bestimmen. Betreffs der Vorschläge des
Herrn Villarceau theilt Herr Hirsch dessen Ansicht, dass die relativen Schweremessungen
für die Geodäsie von grosser Bedeutung sind, spricht aber den Wunsch aus, dass es
Herrn Villarceau möglich werde, die verschiedenen Vorschläge und Entwürfe, von denen

 

f

 
