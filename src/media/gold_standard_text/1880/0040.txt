 

28

porn vorgeschlagen worden ist. Da er diesen Apparat in seinen Einzelheiten unter-
sucht und sich überzeugt hat, dass er den nothwendigen Genauigkeitsgrad zu erreichen
' gestattet, so glaubt Herr Villarceau, dass es wichtig sein würde, ihn zu versuchen.

Im Wesentlichen besteht dieser Apparat aus einem doppelten Glasrohr, von
denen das eine eine Barometerröhre, das andere ein mit Stickstoffgas gefülltes Reservoir
darstellt, welches durch eine Quecksilbersäule von dem Torricelli’schen Raume getrennt
ist. In der Nähe der oberen und unteren Quecksilberfläche muss das Rohr genau cali-
brirt sein. Ausserdem sind an beiden Enden zwei Stahlspitzen eingeschmolzen, welche
in Berührung mit den Quecksilberflächen gebracht zu werden bestimmt sind. Wenn
Temperatur und äusserer Druck constant erhalten werden, so wird das durch die hori-
zontale Quecksilberfläche, auf welche die eine Spitze eingestellt ist, begrenzte Volumen des
Stickstoffes und folglich auch seine Spannung unverändert bleiben; diese seine Spann-
kraft wird übrigens durch die Höhe der Quecksilbersäule bei einer gegebenen Intensität
der Schwere bestimmt.

Um die absolute Höhe dieser Quecksilbersäule nicht messen zu müssen, nehmen
wir an, es sei möglich, die in dem Barometerrohr enthaltene Quecksilbermenge und die
Neigung des Apparates in einer Weise zu modificiren, dass es möglich ist, die gleich-
zeitige Berührung der beiden Quecksilberflächen mit den beiden Spitzen herzustellen.
Alsdann wird die Spannung des Stickstoffes dargestellt durch das Produkt aus der
Entfernung dieser Spitzen in das Gewicht der Volumen-Einheit des Quecksilbers und in
den Cosinus der Neigung.

Wenn man nun zwei dieser an verschiedenen Orten erhaltenen Produkte ver-
gleicht, so wird offenbar, vorausgesetzt, dass die Entfernung der Spitzen sich nicht
geändert hat, das Verhältniss der Intensität der Schwere an beiden Orten, oder die
relative Schwere ausgedrückt sein durch das umgekehrte Verhältniss der Cosinus der
beiden Neigungen.

Herr Villarceau meint übrigens, man könne auf die gleichzeitige Berührung der
beiden Spitzen verzichten und sich mit einer successiven Berührung begnügen mit Zu-
hülfenahme von Reductionsformeln.

Um die grosse Genauigkeit, welche man bei der Einstellung solcher Stahlspitzen
auf Quecksilberflächen erhalten kann, auszunutzen, ist es freilich nöthig, dass man auf
die Unveränderlichkeit der Dimensionen des unter constantem Druck und constanter
Temperatur befindlichen Apparates zählen kann. Zu diesem Zwecke wird das Mano-
meterrohr in ein mit Eis gefülltes Gefäss eingeschlossen, in welchem künstlich ein con-
stanter Druck erhalten wird. Und um die plötzlichen Spannungsänderungen des Glases,
welche allein noch zu befürchten wären, zu eliminiren, wendet man das gleiche Mittel
an, wie bei dem Gebrauch der Marine-Chronometer, d. h. man verwendet mehrere solcher
Manometerröhren, welche sämmtlich auf ein und demselben, aus T-förmigem Eisen gebil-
deten Gestell befestigt sein können. Herr Villarceau hat sich übrigens versichert, dass
man auch eine Veränderung der Entfernung der Spitzen in Folge der Neigung nicht zu
befürchten hat, da letztere 15° nicht zu übersteigen braucht.

 

|

a ink hs

 
