 

|
l
I
|

 

L’expression du poids d’une fonction quelconque des observations, d’un côté, par exemple,
dépend de la forme de cette fonction et des coefficients des équations normales;
je peux donc déterminer ce poids P, et Verreur moyenne de la fonction donnée

sera —"—, Si donc cette fonction est un côté et que cette erreur moyenne —,

 

u

oe Ver
dépasse , de la longueur, il est évident qu'il faudra augmenter la précision et
le poids des observations, et si cette précision et ce poids ne peuvent plus être raison-
nablement augmentés, il faudra mesurer une nouvelle base pour éviter l'accumulation des
erreurs des observations angulaires. 3

On voit ainsi que le directeur dispose de plusieurs éléments pour régler d’avance
la conduite des travaux. Ces éléments sont:

a) L’erreur moyenne d’une observation simple.

b) Le poids des observations que l'ont peut régler par le nombre et la
combinaison des observations. Cependant ce poids ne peut pas être
augmenté indéfiniment, car il a des limites données par l'expérience.

c) Le nombre et la distribution des bases.!)

') Il suffit de faire attention à ces conclusions pour comprendre qu'il est, jusqu à un certain
point, possible d'atteindre dans une triangulation des résultats satisfaisants, même lorsque les instruments
coniométriques ne sont pas les plus parfaits. Tous ceux qui ont assez d'expérience dans cette matière
savent très bien que, malgré les principes théoriques, il n’est pas permis de croire à la possibilité d'un
degré indéfini de précision croissant en raison de la racine carrée du nombre des observations. C’est
qu'en effet chaque instrument peut donner par la réitération des observations un maximum de précision
au dela duquel il serait inutile de renouveler les mesures. La preuve la plus évidente de ceci, nous la
trouvons dans le fait que, malgré la précision théorique des observations, les équations de condition ne
sont jamais ou très rarement satisfaites par les observations dans une mesure qui soit en rapport avec
l'erreur moyenne de celles-ci. Cela étant admis, supposons qu’il s'agisse de rendre comparables les résul-
tats de deux instruments. Si ces instruments sont assez peu différents pour être rangés parmi ceux
d’une même famille, il suffit de déterminer, par un grand nombre d'observations, le poids relatif d’une
observation faite avec les deux instruments, et de régler en conséquence le nombre d'observations à faire
respectivement pour obtenir des résultats de même poids. Mais il est possible que faute de mieux l'on
doive se servir d'un instrument de type ancien et donnant, par exemple, pour l'erreur moyenne d’une obser-
vation une valeur triple de l'erreur moyenne usuelle dans les travaux modernes. Dans ce cas il ne faut
pas seulement chercher une compensation dans un plus grand nombre d'observations, mais il faut remédier
aux inconvénients en mesurant un nombre de bases convenable, en se guidant, pour les placer, avec les
principes indiqués dans ce mémoire.

Il peut encore se présenter une question très importante, c’est à dire de profiter d’une trian-
oulation ancienne, bien que ses angles ayent une précision inférieure à celle que nous prétendons actuelle-
ment. Dans ce cas, on comprend qu'il est possible de perfectionner la triangulation donnée soit par de
nouvelles observations angulaires, soit par la mesure de bases en nombre suffisant et convenablement
situées, soit par les deux moyens réunis. Les principes indiqués dans ce mémoire suffisent pour ré-
soudre sans tâtonnements la partie géométrique de la question, tandis que les données statistiques déduites
de travaux précédents nous indiqueront les dépenses inhérentes aux stations trigonométriques et aux mesures
de bases et nous feront décider, à égalité de précision, pour la solution la plus économique.

   

64h A AM eben Ada MB 4
>

in sah ab os bah A na nn
