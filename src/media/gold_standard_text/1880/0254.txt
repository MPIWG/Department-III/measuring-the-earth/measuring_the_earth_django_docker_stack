62

 

 

 

Triangulation de Lithuanie.

 

 

#
nel Indication des | patitude.| AS | Époque. Directeur Instrument, | Remarques. N
Points. | > tudena 1} et Observateurs.

106. Lola... - 54°18’33"|43°28’ 1”

Kor Deibissi. . .- .*- 54 31 34 | 43 35 56 | 5
108|Medniki....... 54.31 54 |43 17 35 | “
109 | Beresnaki . . . .. 5438 7143 5 35 2
110 Konradi... -- . 54 42 11 |43 25 48 =
111|Nemesch...... 54 39 3 | 42 58 45 | a
112|Naborowtschisna .| 54 42 17 | 42 27 44 | =
113 | Chornuschiski . . . 54 51 45 |43 17 38 | > à
114 | Meschkanzi . ...|54 55 54 | 42 58 55 | = Fe
115 ,Kongedi ...... 55 6 33 | 42 26 48 | 2 Se
116 Ambroschiski ...|55 8 27 | 42 58 18 | © =
7 Bani: ee. - 5517 0 49 49 8 | 2 a D ©
118 | Fschiwili . . . . . . DD 19 9148 8 7) a 3 ai”
119 | Stworanzi ..... 55 29 22 | 49 48 54 | + = acs
20 line a... 55 32 25 148 341 | S & ı das
121  Martintschuni . . .| 55 43 31 | 43 16 20 | = & = Se
102 .Kinderti- . -... 55 46 30 |42 46 24 | 5 es
123 |Karischki. ... . - 55 54 11 (43 5 7 | ak Toe
124|Jakschti ...... 55 55 58 | 42 48 42 | 5 a. ©
125|Ponedeli ...-. . 56 1 46 | 42 53 21 | = | te Bee
126 | Ganuschischki ..... 56 8 49 | 43 11 43 | | te ee 2
127 Pilkalna . 2. ou. 56 11 88 | 42 49 27 | | De 3
128 Unmen .... =. 56 15 25 438 1116 | oe.
129 | Dandsewass ... .|56 28 29 | 42 52 58 | | 8,8
130 | Arbidiani . . .. .. 56 29 O |43 17 40 | es
131 "Bristen . . . . 2.5 56 34 55 |43 1 32 | Ses
132 | Dabors-Kalns ...|56 35 5 | 43 21 29 | EN eee
133 | Jacohstadt . . . .. 56 30 7 |48 81 22 er.

 

 

 

 

 

 

 

Triangulation des Provinces Baltiques.

 

 

Dabors-Kalns .. .| | Dés ; 5 | | | Publié 3
A | | Déjà mentionnés. | | | IPubliés dans ,L'arc du
134 | Sestn-Kalns . . . .| 56 50 26 | 43 17 10 | | | lect, ca
| | | | | 2 |» Gradmessung in den
135 | Gaissa-Kalns . . . .| 56 52 14 483 37 32, | | E Russlands" due
136 Nessaule Kalns . .|56 57 89 43 50 4 | er ee
137 ElkasKalns .... 57 5 3 4815 5 | | | = |
138 Ramkan ...... 57 10 28 | 43 48 56 | | 2
139 | Kortenhof ..... 57 17 13 | 44 26 30 | S
0 Fabmar..... 57 27 35 |43 52 9 | È
1A Onpekoln -.... >. 57 33 2 |44 34 11 | 3
142 | Mario-Mäggi . . . .|57 38 20 | 44 3 39 © 3 | =
143 | Hummelshof . . . 157 53 1 |43 40 56 | & = | 2
144) Venartl yo as Ad 1 | 7 a | n
Diane, 57 59 46 | 43 39 59 | © : | >
146 Arrol......2. Be 8 2 | 4359 91. © = =
147| Annikatz...... 58 8 85 | 43 25 18 || = ca =
148 | Kolstfershofs . . . 58 17 39 | 43 23 21 | > |
119 Amon ©... 58 16 52 |44 227 | | 3 :
150 Dorpat (Observat.) 58 22 48 |44 23 7 | a |
151 | Oberpahien,. . ... 58 39 20 |43 38 3. 3
CS 58 39 87 | 44 9 52 | | s ;
A 58 56 19 |44 224 | E | 4
154 |Marien ....... 58 57 64 | 43 41 47 | | 4
155 | Tammik..... 0. 58 59 34 44 0 18 | A | à
Too Ehbafen., |. 59 6.18 |43 52 19 | | |
| A

 

 

 

 

 

 

 
