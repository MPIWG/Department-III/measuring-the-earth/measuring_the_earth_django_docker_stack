ET Zt

LR ee

im

m mor ft

"

ein

 

3

fahren von dem des Herrn Generals, wie ich seit dem Jahre 1877 schon mehr als ein-
mal bei Verhandlungen der Perinanenten Commission und in den Jahresberichten der
Europäischen Gradmessung zu erinnern veranlasst war. Dass dieser Unterschied sehr
wichtig ist, zeigen auch die von mir zu besprechenden Refractionsbeobachtungen, und
zwar dadurch, dass die aus ihnen berechneten einzelnen Höhenunterschiede sehr stark
von einander abweichen.

Die von mir angewendete Methode der Beobachtungen der terrestrischen Vertical-
refraction ist somit folgende: Zwei Beobachtungsorte D und K sind ihrer Lage nach
vollständig bekannt, d. h. man kennt sehr genau ihre horizontale Entfernung und das
Azimuth ihrer Verbindungslinie aus der Landestriangulation und ihre Meereshöhen durch
geometrisches Doppelnivellement. Bezeichnet C den Erdmittelpunkt, so lassen sich
aus dem Dreieck CDK die wahren Zenithdistanzen (Z) der Seite DK in ihren End-
punkten sehr genau berechnen und die scheinbaren Zenithdistanzen (z) ebendaselbst

messen, folglich auch ihre Unterschiede (Z—z) finden, welche die terrestrischen Re-
fractionen vorstellen.

Von dieser einfachen Untersuchungsmethode, der ich nichts Neues hinzugefügt
habe, als die Forderung, dass der Höhenunterschied der Beobachtungsorte durch genaues
geometrisches Nivellement gefunden werde, suchte ich sofort nach Vollendung meiner
Beobachtungen am Hohen Miesing eine Anwendung zu machen, indem ich von einem
seiner Lage nach bekannten Standorte bei Aibling die Refractionsbeträge 4 z — 7-7
mit Hilfe eines alten Reichenbach’schen Höhenkreises mass und mit den nach der da-
mals noch üblichen Refractionsformel 4 Z=k.C verglich. Ich verzichtete aber auf die
Veröffentlichung nicht nur jener Beobachtungen, sondern auch der später mit demselben
Instrumente angestellten Messungen, weil sich bei genauer Untersuchung herausstellte,
dass dessen Höhenkreis sehr bedenkliche Theilungsfehler hatte. Als ich hierauf im
Jahre 1873 der Bayerischen Commission für die Europäische Gradmessung behufs Ver-
bindung unseres und des Sächsischen Hauptdreiecksnetzes Entwürfe und Kostenanschläge
zur Erbauung massiver Beobachtungspfeiler auf dem Döbraberge und dem Ochsenkopfe
im Fichtelgebirge vorlegte, sprach ich auch den Gedanken aus, dass es zu den Auf-
gaben dieser Gradmessung gehöre, Refractionsbeobachtungen machen zu lassen und
empfahl sofort als eine günstige Gelegenheit für Bayern die Benutzung der bevorstehen-
den Messungen zwischen den genannten Bayerischen und den Sächsischen Punkten
Stelzen und Kapellenberg. Fünf Jahre später (1878, Generalbericht, S. 26), nachdem
ich schon über die ersten Erfolge der Bayerischen Refractionsbeobachtungen berichtet
hatte, gab die Permanente Commission meinem Gedanken auch einen officiellen Aus-
druck, indem sie es auf Antrag des Herrn Generals Baeyer für wünschenswerth erklärte,
an geeigneten Orten Observatorien zum Studium der Refraction zu errichten.

Da sich aus verschiedenen Gründen der Bau der massiven Beobachtungspfeiler
und die Durchlichtung der Waldungen auf dem Dobraberge und dem Ochsenkopfe bis
zum Jahr 1876 verzögerten, so konnten die von mir geleiteten Refractionsbeobachtungen
erst im Jahre 1877 beginnen und zwar nach einem unterdessen erweiterten Plane,

1*

 
