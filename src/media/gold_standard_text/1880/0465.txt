a ere esr

roe RR 1

TTT. TOP nee ge

TTT

 

TTT TTT RR TAT BATT TAT RT

49
sommet, mais Cet inconvénient était amplement racheté par la certitude d’obtenir 2% vo-
lonté les signaux les plus puissants et d’une intensité constante.

Dans les premiers jours du mois de Juillet, tous nos apparails étaient prêts
et nous avons pu faire, à Paris, dans les atéliers des constructeurs, des expériences
photométriques de jour et de nuit pour nous rendre compte de la puissance éclairante
des projecteurs. Les résultats ont dépassé toutes nos espérances.

Mais ce n'était pas tout; notre matériel ainsi préparé, il fallait le transporter
sur les quatre sommets du quadrilatère, et pour cela, il avait fallu préalablement ouvrir
des routes sur des montagnes désertes et escarpées, organiser des relais d’approvision-
nement d’eau et de charbon pour le service du moteur, des depöts de vivres pour un
personnel nombreux et c’est a cette besogne qu’ont été occupés pendant plusieurs
mois, en Espagne et en Algérie, plusieurs centaines de soldats travailleurs.

Le 20 Août, toutes les opérations préparatoires étaient terminées: on avait
hissé, pièce à pièce, les piliers en pierre de taille, les moteurs, les projecteurs, les
machines Gramme, les maisons en bois ou les tentes qui devaient servir dabri. Tout
était prêt enfin, malgré d’incroyables difficultés.

Les observateurs étaient à leur poste: en Espagne, au Mulhacen, le colonel
Barraquer assisté du Capitaine Borres et du Lieut. Cebrian; & Tetica, le Major Lopez
avec M. le Capitaine Pinal; en Algérie, au Filhaoussen le Capitaine Bassot avec le Ca-
pitaine Sever; & M’Sabiha, le Colonel Perrier avec les Capitaines Defforges et Derrien.

Aux quatre cimes le matérial était identique, la disposition seule des piliers
était différente d’aprés la nature du terrain. Ainsi, tandis qu'à M’Sabiha et au Mul-
hacen, on a pu s’etaler sur un large plateau, à Filhaoussen qui se présente sous la
forme d’une crête aigue, et à Tetica qui se termine en pointe escarpée, on a été réduit
à placer les appareils dans les unfructuosités du sol.

Au centre de chaque station se dresse le pilier central qui porte le cercle
azimutal entouré d’un pavillon-abri. Tout autour s'élèvent les piliers sur lesquels sont
installés les projecteurs de lumière. Un peu plus loin, c’est le moteur qui actionne les
machines Gramme d'où partent les fils conducteurs des courants induits qui aboutissent
aux pôles des lampes électriques; sur d’autres piliers sont dressés des héliotropes,
des lunettes-chercheurs; — sous la tente enfin se trouvent les pièces de rechange,

parmi lesquelles un foyer; nous n'avons eu garde de négliger cette précaution essentielle
pour parer aux accidents imprévus.

Ces stations exceptionelles ne ressemblent guère à celles de la géodésie ordinaire ;
aussi avons nous Crû devoir faire exécuter des photographies de l’une d’elles, M’Sabiha,
afin de les mettre sous les yeux de l’Association et de conserver le souvenir d’

un effort
que la science n’aura pas souvent l’occasion de renouveler.

Le temps était beau; mais les vapeurs qui montaient de la mer ne se laissaient
pas traverser par les faisceaux de rayons solaires dirigés sur nos instruments; la nuit,
les signaux électriques ne paraissaient pas davantage.

Gen.-Ber.

 
