i
N
:
i
iY
N
{

i
ie
|
|
|
N
i
;
i
ii
!
;

 

92

M. Bruhns ne peut partager opinion que la temperature s’eleve avec la hau-
teur; car, même pour une température constante, on obtient par la théorie de Newton
une valeur pour la réfraction horizontale bien plus grande que celles qui ont jamais
été observées. Il croit aussi que la réfraction astronomique doit être connue pour des

distances zénithales plus grandes que 893°, particulièrement lorsque, — comme cela
existe déjà en Amérique et comme il en est question en Italie pour PEtna — on

établit des observatoires astronomiques sur de hautes montagnes. On sait, que la ré-
fraction astronomique a une période annuelle ainsi qu'une période diurne, à cause des
divers degrés de diminution de la température dans l'atmosphère; il envisage qu'il est
nécessaire que les astronomes se servent aussi de thermomètres établis à certaines
hauteurs, ou bien qu’ils observent la direction d'objets terrestres éloignés pour en con-
clure quelle valeur de réfraction ils doivent employer. Du reste il est d'accord qu'il
existe une connexité complète entre les réfractions terrestres et les réfractions astrono-
miques. Les observations sur la température à différentes hauteurs ont été déjà fort
souvent exécutées, mais certainement jamais d’une manière suffisante; il serait d’avis,
qu'au point de vue des réfractions on procède à ces observations au moyen de ballons

captifs. Les observations faites à une hauteur de 20m. ne lui paraissent pas suffisantes.

M. von Oppolzer répond qu'il ne met nullement en doute la diminution de la
température avec la hauteur; il a seulement soutenu que, après le coucher du soleil,
et jusqu'à une certaine hauteur, il semble que la température tende à monter; sur les
autres points il est d'accord avec le préopinant, et il repète qu'il n'a nullement eu
l'intention de diminuer la valeur des observations tres méritoires de M. von Bauernfeind.

M. von Bauernfeind en renvoyant à son mémoire ,.Résultats d'observations de
réfraction‘, qui va paraître prochainement, fait observer que l’augmentatiôn anormale
de la température qu’on observe et que lui même à observée déjà dans certains cas
pour la première couche de 20 m., ne peut avoir aucune influence sur les faits dont
il s'agit, attendu que la ligne de visée entre Dobra et Kapellenberg passe en moyenne
3 200 m. au dessus du sol. Du reste il a déjà fait faire par M. le Professeur Schmadi,
des observations de température, au moyen de ballons captifs, jusqu'à la hauteur de
200 m., sur lesquelles l’observateur rendra compte lui même plus tard.

M. Ferrero a la parole pour faire rapport sur Pachèvement de la carte générale
des triangulations, et il regrette de ne pouvoir présenter cette carte complète, par suite
de ce que, sur beaucoup de points à propos desquels il priait qu’on lui donnat des
renseignements dans la circulaire qu’il à adressée à ce sujet aux délégués, il n’a reçu
que des réponses incomplètes. Il demande que son rapport, avant d’être imprimé, Soit
communiqué aux commissaires pour le compléter, et accepte avec remerciement l'offre
que lui a fait M. Bruhns de se charger de cette tache. Quant aux deux grandes cartes
de triangulation, qui sont exposées dans la salle, il propose que l’une soit remise à
l'Ecole polytechnique de Munich, et l’autre à M. Bruhns, pour être complétée. Il distri-
bue une carte de triangulation dressée sur une plus petite échelle.

Le Rapport de M. Ferrero se trouve aux annexes. (Voir annexe V.)

 

jo vou hm kn ll äh a a

 
