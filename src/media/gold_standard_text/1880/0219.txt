97 a |

Grossbritannien. :

 

 

 

 

 

 

 

 

 

 

ESS SE EC ER EE ED EC EE RS ME EN SO EE A” PP ES PC NSS TSA |
: Indication des 3 Longi- | É Directeur |
N Latitude. | = | Epoque. Instrument. Remarques,
Points. | tude. | Bi et Observateurs. 7
5 = LT ie — es — I TT = = = —— Län LE
| 176 | Mormonth. ..=. 7 |57°36" 9" 15°37:654| 1847 Serg, Donelan. Ramsden de3p.
177 | Mount Battock. . . | 56 56 57 | 14 55 91 | 1847 id, id,
178 | Mt. Leinster. ... .|52 37 5 |) 10 53 a
179 Mt: Sandy: NE" 55 10 55 | 10 48 54 | 1829 Colon, Dawson, Capit. Hen- | Troughton de
E | | derson, Lieut. Murphy. 2 pieds.
180 | Mowcopt . . . ... 53°46 58 | lo 27 0 1851 Serg. Jenkins. Ramsden de3p.
nel | Naseby ....... 52 23 48 |16 40 31 | 1842 Serg. Steel. Ramsden de18”.
182 | Naughton . . . ... 52 6 6 |18 36 56 | 1845 id. id,
183 Nephin . . 24°.) Sa °0 48 | 817 46 | 1828 Génér. Portlock. Ramsden de 3 p.
dita | Nive Hill. ..... 2% 60 47 33 | 16 53 49 | 1847 Serg. Jenkins. Théodol. de 7”.
184 | Nodes Beacon . . . 80 40 0/16 7 21 | 1845 Sergs. Winzer, Steel. Theod. de 18“.
185} Norih Rona ..../59 716 |11 50 58 | 1850 A
186 | North Ronaldsay. . 59 23 3 | 15 17 37 |
| (Phare) | |
fed Norwich <<... 52 37 54 11815756 | 1844 Serg. Steel. id.
#88 | Norwood . ...% 2°. 51 24 51 | 18 29 52 | 1844 id. id,
389 | Old Lodge 202. Staus 2 le. 0) 1849 Serg. Jenkins. Théodol. de 3 p.
190 | Old Sarun Castle . 51 5 35 | 15 51 32 | 1849 Serge. Donelan. id.
19%0a | Old Sarun Gun .. 51 5 44 |15 51 57 | 1849 id. id.
41191 | Orm’s Head . . . . 53 19 59 |13 48 39 |
4 1199 | Orford Castle . . . 52 5 39 | 19 11 49 | 1843 id. Ramsden de 3p.
CHATONS Le 52 8 55 |18 53 3. 1844 Sergs. Steel, Beaton. Ramsdende18”.
E Es: | Over Hi . . . . .. 57 15 19 115 3a 25 | 1817 Gardner. Ramsden de3p. Nahe bei 152.
ig 194 | Paddiesworth .... 51 a 1828 97 | 1844 ae ee a No. 447 in Frankreïch.
1 195 | Paracombe . . . .. | 51 10 32 |13 48 12 | 1845 | Caporal Stewart, id.
1196 | Pendie Hill. . . .. 53 52 6 |15 21 57 | 1841 Lieut. da Costa. Troughton de
| | 2 pieds.
2 197 | Penninis Windmill. | 49 54 29 11 21 33 1850 Caporal Wotherspoon. Ramsden de18”.
+ 198 | Pertinny ...... 150 6 24 12 je 841 1845 Serg. Donelan. Ramsden de sp. -
‘isp | Peterhead .... .|57 30 44 |15 52 [4 | 1850 | Serg. Steel. Theodol. de 7”.Nahe bei 115.
1593 | Pillesdon ..... .... 50 48 24 | 14 49 50 | 1845 Serg. Donelan. Ramsden de3 p.
200 | Plynlimmon. . ... 52 28 1 |13 52 53 | 1805 |
201 | Precelly ... .&: 51 56 46 |12 53 23 | 1842 Lieut, Luyken. id,
115c | Reform Momment .|57 29 31 |15 51 57 Nahe bei 115.
1 202 | Ronas. —-. 27.7) | 60.32: 1 11641829! 1821 Génér. Colby, Capits. Vetch, id,
4 | Drummond.
i 203 Base de Rhuddlan 53 17 12 | 14 11 32 | 1806 Wolcot. id.
5) (Terme Est) |
! 204 | Base de Rhuddlan | 53 17 41 | 14 4 51 | 1806 id. id.
À |: (Terme Ouest) |
1205| Bu RBea....... 572.50. 8 VIT 53 521 1848 Serg. Winzer. id.
= 206 | Ryder’s Hill . .. . 50 30 21 | 13 46 22 | 1845 Serg. Donelan, id. |
- 207) St. Agnes Beacon. | 50 18 24 | 12 26 49 | 1797 Génér. Mudge. id.
+ 208 | St.AgnesLighthouse | 49 53 33 | 11 19 7 | 1850 |
# 209 | St. Ann’s Hill . . .| 51 23 51 |17 8 23 | 1792 Génér. Mudge id.
} 210) St. Martin’s Head} 49 58 0 | 11 24 52) 1850 Caporal Wotherspoon, Ramsdende18”.
211 London St. Paul .|51 30 49 |17 33 57 | 1848
| 212 | St. Peters . . . .. 51 21 55 |19 458. 1844 Serg. Steel. id, |
#213) Sawell ..... .. 54 49 11 |10 37 30 | 1827 Gener. Portlock. Ramsden de3 p.'
| |
? 214 | Saxavord. ..... 60 49 39 | 16 49 25 | 1817—47 Gardner, Serg. Steel, à ei |
: | IL Theodol. de 7”.
| 215 | Sayrs Law. . . .. 55 50 49 | 14 59 39 | 1846 Serg. Winzer. Ramsden de3p.
i 216 | Sca Fell . . . . .. 54 27 15 142.97 10 | 1841 Capits. Pipon, Craigie. id.
ë | on 2 i 100 D + 4 | ye ee Robinson, Pipon. it
| 218; Scournalapich.../57 22 ¢ 36 | 18 erg. Donelan. oy) XG
à 219 | Severndroog . . . .| 51 27 59 | 17 43 27 | 1822—48 Génér. Colby, Capit. Kater, id.
| | | | Gardner, Serg. Donelan.
' 88c | Shanklin Down ..|50 37 6 |16 97 24 | 1846 Serg. Steel. Ramsden de18”.|Sehr nahe an 88.
: I 220 | Slieve Donard . . . 54 10 49 |11 44 36 | 1846 Sergs. Forsyth, Winzer, Ca- Ramsden de3p.
i | | poral Stewart.
221 | Slieve League . . . 54 39 4 | 8 57 26 | 1828 Génér. Portlock. id.

 

 

 

4%

EN

m

it

&
1
==
at
se
it
aS
ae
48
25
53
+
=

i
4

 
