3
Gould, B. A. Report on the latitude of Cloverdon station in Cambridge. (R. C. S.
1865, pp. 160—165.)
— On the longitude betw. America and Europe from signals through the atlantic
| cable. (R. C. S. f. 1867, pp. 57—133.)
i oo Telegr. Longit. of Charleston. (R. C. S. f. 1853, pp. 86—88.
# — On telegr. observations for the diff. of longit. betw. Raleigh N. C. and
Columbia 8. C. (R. C. S. f. 1854, pp. 128—131.)
— Rep. on telegr. operations for diff. of longit. betw. Columbia S. C. and Macon,
Ga. (R. C. S. f 1855, pp. 286—295.)
= er for diff. of longit. betw. Wilmington, N. C. and Montgomeri, Ala.
(R. GC. S..1..1856, pp. 163 106)
— On the progress made in the different campaigns. (R. C. S. £ 1857,
pp. 305—310.)
_ Abstract of a report on the determination by telegraph of the diff. of longit.
betw. New York City and Albany. (R. C. S. f. 1861, pp. 221— 222.)
— Longitudes in Maine, Alabama, and Florida. (R. C. 8. f. 1862, pp. 158—160.)
— On computations connected with the telegr. method. (R. ©. & 1. 1863, bp.
154— 156.)
On results by telegr. method. (R. ©. S. f. 1864, pp. 115—116.)
Rep. on the results of determining longit. by telegr. method. (R. C. S. f.
1865, pp. 150—151.)
— Transatlantic Longitude. (R. C. S. f. 1867, pp. 57—133.)
— Benjamin Peirce’s criterion for the rejection of doubtful observations. (R. C. S.
f. 1854, pp. 131—138.)

— The transatlantic longitude, as determined by the Coast Survey expedition of
1866. A report to the superintendent of the U. $. Coast Survey. Washing-
ton 1869. (Smiths Contr. to Kowl.)

~~ Remarques sur les attractions locales. (Paris Ac. Sc. C. R. LXIX, 1869,
pp. 814—5.)

Hassler, F. R. Papers connected with the survey of the coast of the United States.
Philadelphia, 1824—26.

— Comparisons of wights and measures of length and capacity. Washington, 1832.

— Documents relating to the survey of the coast of the U.S. New York, 1834.

— Documents relating to the construction of uniform standards of wights and
measures. New York, 1835—1836.

— Report showing the progress of the coast survey up to the present time.
Washington, 1842. oh

Hilgard, J. E. Discussion of probable error of observation at Wiirdemann’s portable
transit; from observations by G. Davidson in 1853. (R. C. 8. f. 1854,
p. 121.)

A pt D NP em mr NIT

PAPER

1*

kA RAVAN, ET TE TE
ET RR NUIT EL DIEU

2
ae

 
