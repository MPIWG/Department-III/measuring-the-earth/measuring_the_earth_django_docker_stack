 

Disons tout de suite que les signaux solaires ont complètement échoué; pas un
seul n’a été vu du 20 Août au 20 Octobre, ni en Espagne ni en Algérie et nous
aurions éprouvé un échec complet et désastreux, si nous n’avions pas eu recours à la
lumière électrique.

Pendant vingt jours, nous avons connu l'anxiété profonde qu'éprouvèrent Biot
et Arago en pointant en vain pendant trois mois leurs lunettes sur les reverbères
d’Ivica, et ce qui augmentait nos angoisses, c’est que nous étions sûrs de la bonne
orientation de nos projecteurs.

Enfin, le 9 Septembre, après vingt jours d'attente fièvreuse, la lumière électrique
de Tetica apparaissait dans le champ de la lunette de M’Sabiha; puis, le lendemain,
celle de Mulhacen se montrait aussi; les observateurs des autres stations apercevaient
aussi les feux dirigés sur eux et nous entrions dans la période si impatiemment attendue
des observations définitives.

En Algérie, le mauvais temps, la brume, les brouillards; en Espagne, les grands
vents, la neige, des tempêtes effroyables, rien ne put ébranler la patience ni le zèle des
observateurs, sans cesse à l'affût d’une éclaircie et dès qu’elle se produisait, le périmètre
et les diagonales du quadrilatere Hispano-Algerien s’illuminaient comme par enchantement.

Les observations commencées le 9 Septembre ont été terminées le 2 Octobre.
A chaque Station on a mesuré au moins 40 tours d’horizon.

Nos prévisions étaient ainsi justifiées. A des distances moyennes de 250
kilomètres (de 225 à 270) nos signaux électriques étaient aussi perceptibles que les
signaux lumineux obtenus avec des collimateurs à lumière de pétrole à des distances
de 30 kilomètres.

Les images obtenues au foyer de nos instruments nous apparaissaient sous la
forme de disques rougeàtres, à contours bien limités, de teinte uniforme, fixes et réduits
offrant ainsi des pointés d’une précision facile et sûre. |

La plupart du temps les signaux ont été visibles a l’œil nu, comme des globes
de feu dont l'éclat était parfois comparable à celui d’une belle étoile de première grandeur.

Nous donnons ci-après les résultats obtenus en mettant en évidence les excès
sphériques et les erreurs de nos triangles.

Directions les plus probables observées (sans réduction)

à Mulhacen a Tetica a MSabiha
Tetica 0° 0 07000 Mulhacen Oo 0. 07000." Tetica 0° 00’ 00000
Filhaoussen 287 30 31-315 Filhaoussen 89 39 9.803 Mulhacen Ne 14 54.757
M’Sabiha 309 59 22.894 M’Sabiha fio 2013-960 “Nilhaoussen 105 (1 71-535

a Filhaoussen
M’Sabiha 0° 00" 00000
Tetica 61 61 65.189
Mulhacen 87 47 63-301

 

4

ak a Md edd aed

 
