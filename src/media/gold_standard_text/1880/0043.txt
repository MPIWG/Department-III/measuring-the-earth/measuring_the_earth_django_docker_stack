Mais tout moyen d'éviter l’étude des influences atmosphériques implique des
combinaisons des quantités observées multipliant les erreurs. Je crois donc que l'étude
des influences atmosphériques vaut bien la peine qu'on y donne. L'expérience m'a
également montré qu’il n’y avait pas le moindre inconvénient à employer la chambre pneu-
matique dans les stations de campagne. Si même l'appareil fonctionne mal dans quelque
station les coëflicients des influences atmosphériques auront déjà été donnés par les
| expériences faites à d’autres stations. Je donnerais au pendule une forme cylindrique

reguliere de façon à pouvoir calculer aisément les effets de la pression et de la vis-
cosité de l’atmosphère.

Ce nest pas, que je sache, un trait particulier aux pendules invariables, qu'il
soit necessaire de les comparer entre eux; car les résultats obtenus au moyen des
instruments différents employés pour obtenir la pesanteur absolue doivent aussi être
comparés entre eux, et comme chaqu’un sait, ils présentent par fois des écarts considérables.
Le danger d'accidents pouvant modifier les pendules invariables est une difficulté sérieuse.
Mais jy obvie en faisant mon pendule à la fois invariable et à réversion. Toute alté-
ration du pendule serait immédiatement révélée par le changement dans la différence

= des deux périodes d’oscillation dans les deux positions. Une fois découverte, il en serait
tenu compte au moyen de nouvelles mesures de la distance entre les soutiens, Il
faudrait peut-être prendre ces mesures à toutes les dix stations. En resumé, il me
semble que si le pendule à réversion n’est peut-être pas le meilleur instrument pour
déterminer la pesanteur absolue, c’est, à la condition au moins qu'il soit réellement inva-
riable, le meilleur pour déterminer la pesanteur relative. Je voudrais qu’il fût formé
d’un tube de laiton tiré de 003 de diamètre avec des bouchons lourds de laiton égale-
ment tiré. Le cylindre serait terminé par deux hémisphères, les couteaux seraient
attachés à des bagues fixés près des extrémités du cylindre. Le centre de gravité doit
être cinq fois plus éloigné d’un couteau que de l’autre.

0

Kar TREFFER STETS on "

J’offrirai quelques remarques sur les details speciaux touchant la construction
d’un appareil de pendule. /
M. Vllarceau et moi, nous avons séparément conseillé de faire tourner le pen-
dule sur des cylindres de 07005 de diamètre. J’ai eu quelques cylindres de ce genre
merveilleusement construits, mais qui m'ont entièrement désappointé. Le pendule
| suspendu de cette façon s'arrête trés-promptement et le cylindre perd vite son poli au
1 point de contact. Mes expériences m'ont fait rejeter absolument cette idée si séduisante
: à première vue.
Je persiste à croire que le couteau doit faire partie intégrante des supports et
que les plans doivent faire partie du pendule.

Tous les modes que j'ai essayés d'observer la période d’oscillation laissent
quelque chose à désirer. J’ai fait un grand nombre d'expériences sur différentes ma-
nieres d’observer les coincidences, et les résultats ont été dans tous les cas défavorables.
On ne peut en effet observer avec précision une coïncidence qu’à l’aide de dispositions
plus ou moins compliquées qui exigent la construction de nouveaux piliers. L’éclairage

PAT TE

ir

ppt

ET Aube ni Lai À
RENE DORE UPPER UT BL COEUR QE NET ee

£

 
