ya we

  

10. RFP EPR] VT TTT Wey:

lA
FEIN ID NET TTI ATED INTERPRETIERT

arrete

betty

AAA Ba i

ah

i
ir
i
3

A
ë
=
7:
ik

Dans la dernière réunion de l'association géodésique M. Plantamour et moi avons été

chargés d'étudier la question du pendule double, et en général les améliorations à apporter
au pendule à réversion.

Ce même sujet a donné lieu depuis à des travaux importans. M. Yvon Villar -
ceau, dans un savant mémoire, a calculé les effets du roulement: En supposant le pen-
dule suspendu par des rouleaux de grand rayon, il a déterminé la loi de variation des
amplitudes, et les corrections de la durée qui donnent la valeur exacte de g. Si le
coefficient du frottement peut être rendu assez faible pour que l’expérience dure un
temps suffisant, cette disposition aura sans doute l'avantage de donner une grande

stabilité au mode de suspension, en empêchant la détérioration des couteaux et régu-
larisant le frottement.

M. Faye, pour remédier au mouvement des supports, avait déja propose le pendule
double; il a récemment indiqué une autre disposition de cet appareil, et a de plus entre-
pris de supprimer Verreur par un tout autre moyen, savoir en faisant osciller dans le
vide un pendule léger sur un support trés ferme.

Les diverses dispositions du pendule double seront bientöt analysées; mais comme
on devra lui appliquer les mémes corrections qu’au pendule de Bessel, je dois d’abord
en rappeler la nature, ou préciser l’état actuel de la question,

Corrections du pendule de Bessel.

On ne peut observer que deux élémens avec une exactitude complete, ou de
premier ordre; ce sont la distance des couteaux, désignée par À, et la durée d’oscillation.
Pour cette dernière, il faut que l’expérience dure un temps considérable, afin que l’erreur
des signaux au commencement et & la fin soit répartie sur un grand nombre d’oscillations ;
quant .a la distance A, sa mesure nécessite des précautions minutieuses, savoir: une
étude comparée des coefficiens de dilatation de la tige et de la régle, des modifications
que le voisinage de l’expérimentateur apporte aux lignes de visée des micrometres, la
mesure de l’écrasement des couteaux, etc.

14%

 
