 

PERS FT OE

Æ

en

 

D

PRES D EPP LR RP SP TE SERVIS SEE

 

50

Vereinigung der Preussischen und Russischen Ketten, B. über Thorn.

 

 

 

 

 

 

 

 

 

 

 

 

 

No Indication des aitude Longi- Bo Directeur t
Points. tude. Re et Observateurs. N pee anes:
88 | Lopatken . . .... 53°20'52"| 36°39'57" 1853 Juillet, Août, von Hesse, von Gottberg. |Cercle de 15 p.
d’Ertel.

89 Gum . .....088 53 20 54 |36 5 8 | 1853 Septembre id. id.
90|Blendowo...... 53 21 8 36 25 24 1853 Aoüt |. id. id.

91 |Culmsee ...... 53 11 13 |36 16 40 1853 Juin, Juillet id. id.

92 | Glascejewo..... 53 10 54 |386 3 1 1853 Juillet id. id.

98 6Getan..- ..... 58 2 50 |36 2 37 1853 Juin id. id.

94 Thom ....... 53 029 s616 7 1853 Juin id. id.

95 | Dohrezejewice....\53 1 6 | 36 30 37 | 1853 Mai, Juin id. id.
96|Kowalewo...... 53 9 24 |36 33 42 | 1853 Juillet id. id.

Yo Bulsk........ 53 3 24 | 36 47 12 1853 Mai id. id.
98|Racionzek ..... 52 51 25 | 36 27 58 1853 Juin id. id.

Vereinigung der Preussischen

 

 

 

1854 Juillet, Août

1854 Août
1854 Juillet
1854 Juillet

1862{Juillet, Août
1854 Juin

1862 Août

1854 Juin
1862 Août
1865 Mai
1854 Juillet
1854 Juillet
1854 Septembre
1854 Juillet

1854 Juillet

1854 Juillet

1854 Septembre
1854 Septembre

 

 

 

von Hesse, von Gottberg.

id.
id

‘| von Hesse.

Habelmann.
von Hesse.

Löwe, Stavenhagen.

Baeyer, Stiehle.

Löwe, Stavenhagen.

Baeyer, Sadebeck.

Baeyer, Stiehle.
id.

von Hesse, Stiehle.
id.

Baeyer, Stiehle.

von Hesse, Stiehle.

id.
id.

Cercle de 15 p.|
d’Ertel.

id.
Instr. Univ. de
13 p. de Pistor
et Martins.
id: de 8 p.
Cercle de 15 p.
d'Ertel.
Instr. Univ. de
8 p. de Pistor|
et Martins. |
id. „we 13 p.|

| id. de sp. |

id. de 13 p.|
id: |
id. de‘8 p. |
Cercle de 13 p.
d Ertel. |
Instr. Univ. de;
13 p. de Pistor)
et Martins. |
Cercle de 13 p.
d’Ertel.

id.
id. |

 

 

 

| Anschluss des Punktes Rosenthal an das Schlesische Netz.

 

108 Hundsfeld
108. Wüstendorf

ee einai

99 | Bischofskoppe . . .| 50 15 31 |35 5 39
100 | Schneeberg . .| 50 12 33 | 34 30 48
101 | Lossen . . . . . .. 50 47 39 | 385 13 21
102) Zobten ...-.... 50 51 57 | 34 22 24
103 | Rummeisberg .| 50 42 15 | 34 46 34
102 G0Y :. ...:...,. | 50 55 28 | 34 54 18
105 | Leupusch . . . . . .| 50 43 44 | 34 59 39
106 | Ziegenberg . . . .. | 50 45 23 | 34 44 36
106a | Ruppersdorf . . . .| 50 46 41 | 34 49 12
107 | Köchendorf .| 50 47 27 |34 55 29
10% | Obereck . . . ... 50 44 0 | 34 54 40
107) |Eisenberg ..... 50 44 37 | 34 50 44
107c | Base (Terme Ouest) 50 46 35 | 34 51 28
+074 | Base (Terme Est) 50 45 55 | 34 52 56
108| Breslau . . . . ... :51 6 56 | 34 41 59
|
108a| Rosenthal. ..... 51 8 12 | 34 41 59

51 8 50 | 34 46 37
51 se Ate 32

| 1862 Mai, Août | Baeyer, Sadebeck,Habelmann.

1865 Juillet
1865 Mai, Juin
1865 Juin
1855 Juin

Sadebeck.

Baeyer, Sadebeck.
id.
id.

instr. Univ. del
13 p. de Pistor|
et Martins.
id de sp.
\ id. de 8 p.

|
ug

 

und Russischen Ketten, C. Anschluss an das Schlesische Basisnetz.

id. INo. 11, Oesterreich.

reel ee Äh hu aaa à.

|

 
