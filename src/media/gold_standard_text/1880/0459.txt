TN menus vpn ir ns

am ann) 44

ar m!

POTTER VATA BATU ITE TT FRET TTT

Dur

43
depuis un mois. Il m'a été jusqu’à présent impossible, absorbé comme je Vai été par
(autres travaux, de faire une étude spéciale sur les premiers résultats fournis par ce
nouveau maréographe du systeme de Vingénieur Reitz, construit A Altona par Mrs. Den-
nert & Pape, et donnant, en méme temps que les courbes ordinaires, le niveau moyen
des eaux. Je me propose de m’en occuper prochainement et de rendre compte à l’Asso-
ciation de mes remarques.

Travaux métrologiques.

Le service général des poids et mesures de l'Espagne ayant été confié tout
récemment à la Direction générale de l'Institut Géographique et Statistique, un des
premiers travaux qui a été fait dans l'intérêt de cette branche importante a été la
comparaison du mètre en platine appartenant à l’ancienne Commission des Poids et
Mesures avec la règle géodésique, également en platine, étalon linéaire de l'Institut.

Ce travail fondamental fait par notre collègue, M. le Colonel Barraquer, a
compris en outre la comparaison, avec le méme étalon, de la regle du grand pendule à

réversion d’un mètre de longueur que possède l’Institut.

Intensité de la pesanteur.

Les études préparatoires pour arriver à la détermination de la pesanteur à
Madrid et pour l’étendre après à plusieurs points du territoire espagnol n’ont pas cessé
depuis que l’Institut possède, d’abord un grand pendule à réversion de Repsold, d’un
mètre de longueur, et postérieurement un autre pendule à réversion, du même construc-
teur, battant les trois quarts de seconde. Notre collègue, M. le Colonel Barraquer,
qui est chargé de l’éxécution de ces travaux, s’est occupé sans relâche aux recherches
nécessaires pour atteindre un but si important, sauf pendant le temps qu’il a voué à.
la jonction hispano-algérienne. Des milliers d'observations concernant les différentes
parties qui constituent la determination de lintensité de la pesanteur, y comprises celles
qui se rapportent à la determination de la correction provenant du mouvement du
trépied, ont été faites à l’Institut espagnol. Mais le local où ces travaux ont été exé-
cutés, dans l'édifice provisoire occupé par l’Institut, n’offrant pas toutes les garanties
nécessaires pour obtenir un résultat définitif, nous n’envisageons ce travail considérable
que comme un essai précieux et indispensable pour entreprendre, comme nous avons
l'intention de le faire prochainement, les observations définitives que M. le Colonel
Barraquer fera dans les conditions éxigées par des déterminations aussi délicates.

Stations astronomiques.

M. le Colonel Perrier vous parlera de la différence de longitude entre le sommet
espagnol Tetica et le sommet l’algérien M’Sabiha. La latitude et un azimut ont été

également déterminés à Tetica par M. Esteban, Ingénieur des Mines et géodésien de
6*

 
