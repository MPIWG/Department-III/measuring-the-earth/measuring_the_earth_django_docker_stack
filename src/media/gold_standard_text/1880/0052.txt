 

40

Der Antrag am Schlusse dieses Berichtes:

wonach sämmtliche bisher benutzten Basismessstäbe mit
einem internationalen Etalon, welcher im internationalen

Bureau für Maass und Gewicht in Breteuil aufbewahrt wird,

verglichen werden möchten,

wird angenommen, nachdem Herr Hirsch die Erklärung abgegeben hat, dass das Bureau

in Breteuil im Laufe der nächsten Jahre im Stande sein werde, die Arbeit auszuführen.
Herr Eürsch als Berichterstatter über den Fortschritt der Nivellementsarbeiten

trägt seinen Bericht vor. (Siehe Anhang VI.)

Die folgenden fünf Anträge:

1.

Or

um die Wirkung der zwischen Vor- und Rückblick stattfin-
denden Senkung der Latte und ihrer Bodenplatte zu elimi-
niren, wird empfohlen, die beiden Nivellementsoperationen
derselben Strecke stets in entgegengesetzter Richtung aus-

Zulia en,

bei der Ausgleichung der Nivellementsnetze sollte nicht nur
auf die Länge der nivellirten Strecken, sondern auch auf die
dabei überwundenen Höhendifferenzen Rücksicht genommen
werden;

es ist wünschenswerth, die Gleichungen der angewandten
Nivellirlatten, wo dies noch nicht geschehen ist, genau zu
bestimmen, sei es durch Vergleichung mit dem eisernen Drei-
meter-Stab der Eichstätte in Bern, sei es durch Einsendung
an das internationale Maass- und Gewichts-Bureau in
Breteuil ber Sèvres (Paris),

die Nivellements-Directionen in den verschiedenen Ländern
werden ersucht, so bald als möglich die Verbindung ihres
Netzes mit denjenigen aller benachbarten Länder auszu-
führen; -der Anschluss sollte an jeder Grenze womöglich
wenigstens in drei Punkten erfolgen;

das Centralbureau wird gebeten, nächstens eine Gesammt-
karte in kleinem Maassstabe, wenn nöthig in mehreren Blät-
tern zu publiciren, welche die Netze der. verschiedenen
Länder, oder doch wenigstens die Hauptnivellementszüge
enthält,

werden einstimmig angenommen, und ebenso der von Herrn Perrier ausgesprochene
Wunsch, dass bei jedem Nivellement die Zeit der Ausführung angegeben
werden möchte.

Herr Ibanez erstattet seinen Bericht über die Mareographen. (Siehe Anh. VII.)

   

4 41e ih em ds Bik a,

wre ut d

hun

a a ch a ns, ha nn

 
