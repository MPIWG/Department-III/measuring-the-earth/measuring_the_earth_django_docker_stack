ji
i
|
|
i

 
 
 

renee

A USA RUTH TTP PTT TH

t HT

M ET
EL CORDON DORA CORRE ET TIER Ta 14

 

 

PRÉSENTÉ À L'ASSOCIATION GÉODÉSIQUE PAR C. CELLÉRIER. 19
des deux pendules par la verticale en sens contraire; on en conclura la valeur de s,
et si elle dépasse e on arrêtera l’expérience. Il faudra modifier dans ce cas la longueur
l, du second pendule, en la diminuant un peu si s a crû positivement, en l’augmentant.
sis a crû négativement. En effet dans le mouvement simple, s varie de facon que la
durée se rapproche de T,; ainsi le changement précédent de l, eb par suite de D,

tend à faire varier s dans un sens contraire à celui qui a été observé.

On continuera
ces essais Jusqu'à ce qu’on obtienne une marche convenable.
J

Il ne reste qu'à examiner si l’on y parviendra. Les causes troublantes modifient
S principalement en faisant décroître inégalement a et a’; cet effet n’est guères attribuable
à la correction d'amplitude négligée, ou à l’action de l'air, qu'on peut égaliser, mais au
frottement; l’action de celui-ci sur les amplitudes, moindre que celle de l'air, peut être
inégale pour les deux pendules. S’il en est ainsi, les corrections indiquées ci-dessus
pour /, pourront l’amener à être, non égale à l,, mais un peu différente, de façon à
corriger l'inégalité du frottement. Toutefois si cette dernière était considérable, la
compensation précédente pourrait n'être pas constamment exacte,

et il ny aurait guères
alors d'autre ressource que de remanier un des couteaux. ‘

I y aura un grand avantage à ce que le support soit le plus ferme possible,
peut-être le poids du pendule inférieur à 3 kil; de la sorte, y Sera diminué, e augmenté :
le danger précédent sera peut-être écarté, et en outre l'observation de a, a’ pendant
lexpérience n’aura pas besoin d’étre faite avec la méme précision.

A la fin de chaque expérience, après avoir calculé s, on pourra pour plus
d'exactitude faire sur la durée totale une correction correspondant à la moyenne des
erreurs d'influence, évaluée approximativement. ,

Autres moyens de faire disparaître l'erreur du balancement.

S'il s'agissait de supprimer le balancement lui-même, je ne pourrais guère, ce
semble, que mentionner de nouveau l’ingénieux procédé dû à M. Faye, et qui consiste
à faire osciller dans le vide, sur un support très ferme, un pendule à réversion très-
léger, auquel dans ce cas la symétrie de forme ne serait pas nécessaire.

Mais pour compléter la théorie du pendule. il convient d'indiquer un autre
I I p )

moyen, consistant à éliminer l'erreur; cela se peut par divers procédés, entre autres
par les deux suivants:

1° Procédé. Il consiste à faire osciller successivement sur un même support
deux pendules différens. En nommant pour le premier T, T’ les durées, corrigées de

2 °
Yerreur d’amplitude ao A la distance des couteaux; A, h’ leurs distances au centre de

gravité; p le poids, on aura

ie Re y kph ve n° y LL hoe
Tara), rer

Qk

 

 
