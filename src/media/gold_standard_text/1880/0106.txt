ENTE

SEES TEN
“

ii
u

i:
y
i
t
i
Ht
1%
i
i
li
i
i
fa
i
j
I
IE

SoS SPIT TETE ER

 

94

Les cing propositions suivantes de ce rapport:

1. pour éliminer l'effet de tassement, on recommande de faire le double ni-
vellement toujours en sens inverse;

9. dans les calculs de compensation il importe de tenir compte, non seulement
des longueurs des lignes parcourues, mais aussi des différences de hauteurs
qu'on à rencontrées;

3. il est à désirer que les pays qui ne l'ont pas encore fait, établissent les
équations et les corrections de leurs mires, soit en les faisant comparer
à l’étalon de Berne, soit en les envoyant au Bureau international des poids
et mesures à Bréteuil, près de Sèvres (Paris);

À Jes directeurs des nivellements dans les différents pays sont invités à
faire - opérer le plus tôt possible la jonction de leur réseau avec ceux de
tous les pays voisins, si possible au moins en trois points;
le Bureau central est prié de publier prochainement une carte d'ensemble,
en plusieurs feuilles s'il le faut, contenant les réseaux ou du moins les
lignes de premier ordre nivelées dans les différents pays,
sont votées 2 Vunanimité, apres que M. Perrier eut exprimé le désir qu’on y ajoutat
la demande qu’à chaque nivellement on indique le temps consacré à l'exécution.

M. Ibanez présente son rapport sur les maréographes. (Voir annexe VIII.)

RL

Les deux propositions suivantes sont adoptees:

1. Tous les Etats maritimes qui ont des maréographes sont priés de bien
vouloir faire relever les résuitats numériques donnés par les courbes gra-
phiques, afin qu'il puisse être répondu le plus tôt possible au question-
paire, qu'ensuite de l'autorisation de la Conférence, j'aurai l'honneur
d'adresser ultérieurement aux différents délégués des gouvernements ;

2. afin de combiner entre eux tous les maréographes du Continent Européen,
les États sont priés de bien vouloir faire relier entre eux, par des lignes
de nivellement de précision, tous Îles maréographes de chaque pays, puis
de faire rattacher ces lignes, dans le plus bref délai possible, à celles des
pays voisins.

M. Sadebeck, en sa qualité de rapporteur sur les publications des ouvrages de
Géodésie, présente un nouveau catalogue des publications, et la Conférence décide que
ce catalogue sera publié comme annexe au Procès-verbaux. (Voir annexe IX.)

Le Président annonce que M. M. les Dr. Dr. S. Merz et A. Steinheil sont dis-
posés & faire visiter le lendemain leurs établissements à M. M. les membres de la Con-
férence, invitation qui est acceptée avec remerciement.

M. Plantamour émet le voeu, qu’à lavenir, les rapports spéciaux, aussi bien que
ceux des délégués et des rapporteurs, soient imprimés avant la Conférence, afin qu'il
soit possible de les mieux étudier, et d'en mieux discuter, ce qu’on ne peut obtenir que
lorsque les délégués en auront reçu communication avant la réunion.

 

j
|

 
