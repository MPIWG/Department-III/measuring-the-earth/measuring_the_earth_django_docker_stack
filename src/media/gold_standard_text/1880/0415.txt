u AT ren gegen He

Hmm

reanınn

rent

OL EEL TART TI gm Lu

 

107
Instituto geogräfico y estadistico. Memorias. Tome IL Madrid 1878.
1. Red geodésica de 1% örden de Espana. Parte segunda. Cadena de costa
Sur. Parallelo de Badajoz. Parallelo de Madrid. Cadena de costa Norte.
Parallelo de Palencia.
2. Nivelaciones de precision de España. Parte tercera. Lineas de Madrid
à Aranda de Duero, por Guadalajara, Sigüenza, Séria y el Burgo de Osma;
de Madridejos ä& Segövia por Toledo y Avila; de Albacete 4 Baïlén ; de Sanchi-
drian & Lugo; de Madridejos 4 Cädiz; de Sigüenza 4 Canfranc y Le Som-
port, y de Zaragoza ä la Junquera y Le Perthus.
Determinacion de latitudes y azimutes. Parte segunda. Estaciones de
San Fernando, Peuas y Palo.
—_ Experiencas hechas con el apparato de medir bases, pertene ciente 4 la Co-
mision del Mapa de Espana, por los Coroneles Don Cärlos Ibanez y Don
Frutos Saavedra. Madrid 1859. (Traduction en francais par Laussedat.
Paris 1860.)
— Base central de la triangulacion geodésica de España, por los Coroneles Don
Carlos Ibanes y Don Frutos Saavedra; y los Comandantes Don Fernando
Monet y Don Cesäreo Quiroga. Madrid, 1859. Traduction en français par
Laussedat. Madrid, 1865.
Ismail y Ibanez. Comparacion de la regla geodésica perteniciente al gobernio de
S. À. el Virey de Egipto etc. Madrid, 1863.
Ismaïl. Recherches des coefficients de dilatation et étalonnage de l’appareil à mesurer
les bases géodés. appart. au gouvernement égyptien. Paris, 1864.
Malaspina. Astronom. Observations made during Malaspinas voyage in 1789—95.
Madrid, 1809.
Merino M. [On the Figure of the Earth.] Madrid Observatory annuaire. (Alexander,
C. A. [Transl] Smiths. Inst. Report, 1863, p. 306.)
Nunez (Nunes, Nunnius, Nonius), P. De crepusculis etc. Olysipone, 1542. (Darin Be-
schreib. der Vorrichtung, kleine Bogentheile zu messen, die aber keine
Aehnlichkeit hat mit der, welche man fälschlich nach ihm Nonius nennt.)
Saavedra Meneses, D. Frutos. Discorso sobre los progresos de la geodésica. Madrid, 1863.
Ulloa, A. Observaciones astronémicas y fisicas hechas de örden de S. M. en los reinos
del Peru; de las cuales se deduce la figura y magnitud de la Tierra ete.
Madrid, 1748; 22 edicion Madrid, 1773.
— Voyage historique de l'Amérique méridionale. ‘Traduit de l'espagnol par de
Mouvillon. Paris, 1752:
— Relacion histérica del viaje & la América Meridional, hecho de érden de
S. M. para medir algunos grados de meridiano terrestre. Madrid, 1748.

©

' 14%

 
