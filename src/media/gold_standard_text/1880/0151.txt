 

 

TTT

 

À da à

2s
a
È
a
=

Re CARRE L RUN
PATATE UN

9 “
prototype du Bureau international à Paris, et que leur équation et leur coefficient de
dilatation n'auront pas été déterminés avec l'exactitude que comportent les appareils per-
fectionnés du pavillon de Breteuil. Il importe en effet, si l’on veut arriver à une uni-
formite dans les mesures de longueur, qui permette de rendre les résultats obtenus dans
les différents pays comparables entre eux, d’écarter les comparaisons indirectes des éta-
lons, à l'aide d’un ou plusieurs intermédiaires, auxquelles on a dû avoir recours
jusqu’à présent, et de se baser uniquement sur les comparaisons directes faites avec le
prototype du pavillon de Breteuil. Ce desideratum s'applique aussi bien, en ce qui
concerne la détermination de la pesanteur, à l'échelle servant à mesurer l'intervalle entre
les couteaux du pendule à réversion, que dans la mesure des bases géodésiques, aux
règles employées, et dans les nivellements de précision, aux étalons servant à déterminer
la longueur des mires. Nous ne pouvons ainsi que faire des voeux pour que les in-
stallations et les travaux préparatoires du pavillon de Breteuil permettent de commencer
prochainement ces comparaisons. On peut ajouter que le résultat obtenu par Bessel pour
la longueur du pendule simple à Berlin, ne pourra être comparé aux résultats obtenus
ces dernières années par d’autres observateurs à l’aide du pendule à réversion, que
lorsque les uns et les autres pourront être exprimés dans la même unité métrique
internationale, et après la comparaison de la toise de Bessel avec le prototype du bureau
international.

Une autre cause, qui a pu motiver un moment d'arrêt dans les expériences
faites avec le pendule à réversion, est le doute qui s’est élevé sur la convenance d’em-
ployer cet appareil, par suite de l'erreur introduite par le mouvement des supports.
M. Peirce a constaté, le premier, par une expérience directe que la déviation du plan de sus-
pension résultant de la flexion du trépied de Repsold par l'application d’un poids de 1 kg
pouvait non seulement être rendue visible, mais mesurée à l’aide d’un fort microscope, et
les recherches de M. M. Peirce et Cellérier ont montré la possibilité d'éliminer cette
cause d'erreur et de calculer la correction à apporter à la longueur du pendule simple
pour tenir compte du balancement des supports. Si l’on a déterminé par des expériences
directes la constante de balancement d’un instrument, c. & d. la déviation produite par
l'application d’un poids de 1 kg, on peut en déduire la déviation produite par la com-
posante horizontale du poids du pendule pendant qu’il oscille de part et d’autre de la
verticale. Une série de recherches expérimentales a été faite plus tard par l’un de
nous et publiée en 1878, en vue de déterminer cette déviation: il en résulte que la
déviation peut être obtenue avec une exactitude largement suffisante, par un petit nombre
d'expériences, et cela à l’aide d’un appareil peu compliqué, mais donnant un grossisse-
ment considérable. Ces expériences montrent l'importance de tenir compte du mouve-
ment du snpport, sur lequel le trépied métallique est placé, et du fait que suivant la
nature de ce support, la valeur de la déviation obtenue dans une expérience statique,
par l’application d’une force horizontale, peut s’écarter plus ou moins de celle qui se
produit dans une expérience dynamique par les oscillations mêmes du pendule. De la,
suivant nous, la nécessité de prendre dans le calcul de la correction du pendule simple

 
