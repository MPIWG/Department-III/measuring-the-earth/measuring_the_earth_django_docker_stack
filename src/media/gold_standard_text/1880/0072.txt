 

 

EEE

FE

RTE 2 UPS

SES

RARE

See

RR,

SE

 

pr
Mi
fF
i
i
i
j

60

de l’esprit, au point de vue des soins apportés au- développement des
sciences et des arts, la Bavière se permet d’entrer en lice avec les plus
grandes nations.
À tous ces points de vue, nous n’avons nullement lieu de nous effacer
devant les grands empires, simplement parce qu’ils sont plus forts que nous.
C'est dans tous ces domaines que se trouvent implantées les racines

les plus puissantes de notre force et de notre amour propre; pour ces

domaines — pourrait-il en être autrement? — nous éprouvons les plus
vives sympathies!

J’aime & espérer que vous disposerez du temps nécessaire pour voir
de plus près notre ville et pour vous persuader, par vous mêmes, qu'il ny à
pas d’exag6ration de ma part à dire que chez nous, Princes et peuple
considèrent, comme partie intégrante de leurs devoirs, la culture des arts
et des sciences, et que c’est dans la poursuite de cette tâche et dans les
efforts qu’elle réclame, qu’ils rencontrent les plus nobles jouissances.

Jugez par là, Messieurs, des sentiments de joie profonde qui doivent
animer le peuple et le gouvernement Bavaroïis en voyant arriver au milieu
d'eux un nombre aussi considérable des coryphées les plus distingués de
la Science, que j'ai l'honneur d’avoir présentement sous les yeux; surtout
lorsque peuple et gouvernement jettent un regard sur la grandeur de
l'œuvre à laquelle vous vous êtes consacrés et qu'ils caressent l’espe-
rance que la conférence réunie actuellement sera le point de départ de
nouveaux progrès de la Science.

Le Gouvernement Bavarois porte un tres grand intérêt à vos travaux;
il ne m’appartient pas de traiter à fond les sujets qui vous rassemblent
(la possibilité d'apporter quelque lumière dans ces questions est uniquement
de la compétence des hommes spéciaux); mais je puis dire, cependant,
que le gouvernement Bavarois apprécie vos efforts a leur juste valeur et
prend la plus grande part aux succès qui les couronnent.

Puissent vos travaux produire les plus heureux résultats: puissiez
vous tous, Messieurs, emporter dans vos foyers une impression assez
favorable de votre séjour à Munich et de vos relations avec ses habi-
tants, — qui, certainement, vous reçoivent à cœur ouvert et avec bon-
heur, — pour que, tous, vous gardiez un bon souvenir de la 6™° conférence
générale et que vous désiriez retourner bientôt dans la capitale de la
Bavière.“

Monsieur Jbanez remercie le gouvernement de S. M. le Roi de Bavière, dans
les termes suivants:

Monsieur le Président du Conseil des Ministres,
Ma position de Président de la Commission permanente me pro-
cure l'honneur de répondre par quelques mots, au discours que Votre

 
  

FA Af fod LMR dd i sa Al

à pu +4 400 d

1414s amd a ns, ad bi Al ad mu nn 1:

 
