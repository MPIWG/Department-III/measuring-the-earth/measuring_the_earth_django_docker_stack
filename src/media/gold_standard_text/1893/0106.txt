   

98

für die nächstjährige Versammlung zu besprechen ist. Da die Gradmessung in den letzten
Jahren in mehreren bedeutenden Ländern, in Deutschland, Frankreich, Italien, und Belgien
versammelt war, und sie jetzt in der Schweiz tagt, und da andererseits die General-Conferenz
von 1895, wo die geodälische Uebereinkunft zu erneuern ist, nothwendie in Berlin stattfinden
muss, so scheint es Herrn Hirsch natürlich, für die nächste Vereinigung an Oesterreich-Ungarn
zu denken, in erster Linie an Wien, wo die Erdmessung schon einmal so glänzend empfangen
worden ist, oder wenn diese Wahl, aus ihm unbekannten Gründen, nicht angehen sollte, an
die schöne Haupstadt Tyrols, Innsbruck, den Sitz einer blühenden Universität.

Herr Hirsch meint man könne vorgehen, wie diesmal bei der Schweiz, das heisst,

das Präsidium beauftragen, sich mit Herrn von Kalmär über die vorbereitenden Seh tte beı
den esterreichischen Behörden zu verständigen, um alsdann der Permanenten Commission bei
Zeiten Vorschläge machen zu können.

Herr von Kalmar, wenn auch ohne Vollmacht bindende Versprechungen zu machen,
ist gern bereit in seinem Lande die betreffenden Schritte zu thun, welche ohne Frage die
wohlwollendste Aufnahme finden werden.

Herr Ferster meint, wenn die nächste Versammlung nicht in Oesterreich stattfinden
könnte, auch an Hamburg zu denken wäre, welches den Beweis geliefert hat, dass es gerne
wissenschaftliche Versammlungen empfängt.

Herr Rümker zweifelt keinen Augenblick, dass seine Vaterstadt mit grösster Freude
zum zweiten Male die Permanente Gommission aufnehmen würde.

Ohne sich für den einen oder anderen der gemachten Vorschläge aussprechen zu

wollen, ertheilt die Permanente Commission ihrem Präsidium den Auftrag und bittet Herrn
von Kalmar, die einleitenden Schritte in dem angedeuteten Sinne rechtzeitig zu thun.

Der Herr Präsident spricht den Schluss der Versammlung aus, und hebt die Sitzung
m 3 Uhr 20 m. auf.

 

id

i

eer Te Se MY ee ea

taken iii

ln]

A am ab aa Ma a ab da 4 à

STON NNT TY See ART

 
