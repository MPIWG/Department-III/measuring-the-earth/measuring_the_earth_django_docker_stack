 

66

sammenzufassen, und daraus die den Astronomen unentbehrlichen Reductions-Constanten zu
entnehmen, wäre es praktischer, einfach graphisch zu verfahren, anstatt Formeln anzu-
wenden.

Herr Tisserand, ohne jetzt auf die complieirte Frage der zwei übereinander greifen-
den Perioden eingehen zu wollen, glaubt aussprechen zu dürfen, dass die Vergleichung
zwischen den Beobachtungen und den Resultaten der Chandler’schen Formel noch Vieles zu
wünschen lässt. In der That, wenn man die von Ilerrn Helmert soeben auf der Tafel einge-
schriebenen Zahlen aufmerksam betrachtet, so erkennt man alsbald für Strassburg sowohl
als, wenn auch in geringerem Grade, für Potsdam, verglichen mit den theoretischen Zahlen,
systematische Unterschiede, deren Grösse derjenigen der Breitenänderungen selbst gleich-
kommt.

Herr Bakhunyzen giebt zu, dass die Chandler’sche Formel noch Manches zu wünschen
übrigt lässt; indessen, wenn man die ungefähr sieben Jahre umfassenden Greenwicher
Beobachtungen zusammen stellt, findet man in denselben die Periode von 432 Tagen nahezu
wieder.

Herr Helmert hat keineswegs behaupten wollen, dass die Chandler’sche Formel und
ihre Constanten einen definitiven und unbestreittbaren Character darbieten. Uebrigens kann
er, in der jetzigen Phase dieser Untersuchungen, der grösseren oder geringeren Ueberein-
stimmung zwischen den Beobachtungen und der nothwendiger Weise noch unvollkommenen
Theorie nur geringen Werth beilegen.

Der Herr Präsident dankt den Kollegen für die interessanten Elemente, welche sie für
die Beurtheilung der Frage beigebracht haben, und hält die Discussion derselben vorläufig
für erschöpft.

Herr Hirsch glaubt, dass es nützlich, sogar nothwendig sein dürfte, in einer weileren
Sitzung auf diesen wichtigen Gegenstand zurückzukommen, namentlich um verschiedene
Projekte, unter anderem dasjenige des Herrn Marcuse, zu behandeln, welche sich auf die
definitive Einrichtung von vier Breiten-Observatorien beziehen, die auf dem gleichen Parallel,
mit ungefähr 90° Längenunterschied zu gründen wären. Herr Tlirsch erinnert daran, dass
eine solche Spezial-Organisation von der Brüsseler General-Öonferenz im Princip als noth-
wendig erkannt worden ist, und dass daselbst die nähere Untersuchung derselben der Perma-
nenten Commission empfohlen wurde. Obwohl schon wegen der nothwendigen finanziellen
Mittel die definitive Entscheidung der nächsten General-CGonferenz von 1895 vorbehalten
werden muss, so hält Herr Hirsch die Permanente Commission für verpflichtet diese Ent-
scheidung dadurch vorzubereiten, dass sie bereits jetzt die verschiedenen auftretenden
Projekte näher untersucht.

Der Herr Präsident ist einverstanden den Gegenstand auf die Tagesordnung einer

 

 

Adi add

eu et de eA Ck late a ke

Walkun.iulkll

 
