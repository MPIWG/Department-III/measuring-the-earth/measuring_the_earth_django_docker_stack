 

TE ee tee

RR Te |

aa am Korea

I

leiter da ehe :
PAT po AN PM M LU pt

ad
mischen Gesenschaft und der Carle photographique du ciel, die Sternwarten noch immer
gefallen, vorübergehend und zusammenhangslos die Arbeit für gewisse Zwecke unkrilisch
häufend und andere Gebiete ganz liegen lassend.

Prof. Ferster schlägt vor, für die energische weitere Verfolgung der Verhandlungen
betreffend eine rationnelle Organisation der Breiten-Beobachtungen die in Florenz 1891 ein-
gesetzte Gommission wieder in Thätigkeit treten zu lassen.

Herr Ferrero theilt zunächst einen Brief des Herrn Pergola mit, worin der Herr Di-
rektor der Sternwarte von Gapodimonte ıhn benachrichtigt, dass er sich mit Herrn Harold
Jacoby in New-York verständigt hat, um gleichzeitige Breiten-Beobachtungen nach der Tal-
cotl’schen Methode, mit zwei gleichen Zenith-Telescopen von Wanschaff auszuführen, und
dabei «dieselben Stern-Gruppen zu verwenden, da der Breiten-Unterschied der beiden Stern-
warten klein genug ist, um dies zu ermöglichen.

Herr Ferrero giebt darauf die Uebersetzung eines Briefes des Herrn Schiaparelli,
velehem er von dem Plane des Herrn Mareuse, vier besondere Beobachtungs-Stalionen aul
dem gleichen Parallel zu gründen, Kenntniss gegeben hatte.

Dieser Brief lautet folgendermaassen :

« Mailand, den 5. September 1893.
«Herrn General A. Ferrero, Prasident der italienischen geoditischen Commission.
« Herr Präsident !

« Ich sende Ihnen hiermit das Projekt des Herrn Marcuse zum Studium der Breiten-
Aenderungen zurück, dessen Grundgedanken ich vollständig billige. Schon mehrere Male
habe ich Ihnen die Meinung ausgesprochen, dass es nöthig ist, eine dauernde Controlle dieses
Phänomens zu begründen, und dass es der Erdmessung obliegt, sich damit zu befassen.

« Alles deutet darauf hin, dass die Ursache dieser Variationen tellurischen Ursprungs
ist. In dem Falle ist es nicht wahrscheinlich, dass ihre Gesetze sich durch eine einfache ma-
thematische Theorie mit wenigen Gonstanten genau darstellen lassen, wie dies z. B. für die
Aberration und die Nulation möglich ist. Es giebt hier sicher Perioden, die aber unregelmäs-
sig veränderlich sind, und welche sich vielleicht mit Säeular-Variationen verbinden, deren
Gesetz erst nach Verlauf längerer Zeit zu ermitteln ist. Es genügt offenbar nicht, das Phäno-
men während eines Jahres oder auch während einer kleinen Zahl von Jahren zu verfolgen ;
man muss sich darauf gefasst machen, die Untersuchung auf eine unbestimmte Zeit auszu-
dehnen, wie dies auch z. B. bei den periodischen und säcularen Variationen des Erd-Magne-
tismus der Fall ist.

« Nun ist offenbar die Erdmessung, durch ihren internationalen Character, durch die
bedeutenden Mittel über welche sie verfügt, durch ihre ununterbrochene Existenz und Tra-
ditionen, mehr als irgend eine andere Institution geeignet, die nothwendige Dauer und

 
