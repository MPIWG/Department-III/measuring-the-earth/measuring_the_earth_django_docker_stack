 

Annexe B. X.

Dana

Les opérations pour la triangulation des Pays-Bas ont été poursuivies cette année par
deux brigades d’ingénieurs, dont une était chargée des reconnaissances et de la construction
des installations sur les stations, la seconde, de la mesure des angles.

Après avoir préparé les installations 4 Ubagsberg pour les mesures astronomiques,
la premiére brigade a poursuivi les reconnaissances dans la partie occidentale de notre pays,
savoir dans les provinces de la Hollande du Sud, de la Zélande et les parties occidentales de la
Gueldre et du Brabant du Nord, y compris les reconnaissances pour la jonction avec le réseau
de la Belgique, le long des frontières du Brabant du Nord et de la Zélande.

La seconde brigade a commencé par compléter les observations sur la station de Ru-
remonde, qu'on n'avait pu achever l’année passée, et a fait ensuite les mesures sur les stations
belges de Lommel, de Peer et de Tongres pour compléter la jonction avec la Belgique le long
de la partie orientale de la frontière. Par ces mesures, les observations de premier ordre
dans la partie méridionale du Limbourg ont été achevées; elles ont été complétées ensuite
par les observations sur les stations secondaires de Reywerstok, de Venlo et de Sombeck.
Après ces observations, on a entrepris celles de la station de premier ordre de Bech, que
nous espérons pouvoir finir encore dans le courant de cet automne.

Pendant cet été on a fait une détermination de la différence de longitude entre Leyde
et Ubagsberg, un des points du réseau néerlandais de premier ordre, tout près des frontières
de la Prusse et de la Belgique et rattaché aux réseaux de ces deux pays. Ges observations
viennent d’être terminées il y a quelques jours. Actuellement, on s'occupe des déterminations
dazimut et de latitude. L’éminent directeur de l’Institut géodésique à Potsdam nous a facilité
beaucoup notre travail en nous prêtant pour ces observations un instrument de passage de
Pistor et Martins, muni de deux niveaux pour la détermination de la latitude d’après la
méthode Horrebow-Talcott. |

Le calcul de la hauteur des mers, d’après les différents maréographes, a été continué
comme les années passées.

Après la séance de Bruxelles, M. Defforges a déterminé avec son appareil la longueur
du pendule à secondes à Leyde. Le résultat de ces observations ne nous à pas encore été
communiqué.

GH.-M. SCHOLS.

did ul

ee a Meni eac 1

1

 

 

 
