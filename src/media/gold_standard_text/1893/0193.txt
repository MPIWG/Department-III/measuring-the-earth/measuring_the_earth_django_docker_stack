 

 

ATV à ml

DO RAID DAMON RL LUE pe di

mt per

185

L’attraction a donc lieu ici presque perpendiculairement à la direction du Jura.
Dans la station de Wiesenberg (1000), on a mesuré les déviations suivantes :

»
en latitude 710
m un. LL 20
enlonentul 2, no put D D 0
d’où résulte une déviation du zénith de 8,0, correspondant à l’azimut 151,8 (SE), ce qui

 

prouve de nouveau une attraction perpendiculaire à la chaine du Jura.

Station de Naye (20400).

Deviation en laude 9. a .. . — 06
» Vague Ni 2... — 106
» Dionamde 4... 29

» du zenilh .... 15,8, correspondant a Pazimut 2076 (NN), ce qui

indique l'attraction des Alpes situées à PEsL.

Pour la station de Fribourg (Gollège Saint-Michel), on a reconnu provisoirement une
déviation en latitude de + 1”.

A Lausanne (Université), la valeur provisoire de la déviation en latitude est de
Fe OR

En faisant ces d&terminalions astronomiques, notre ingenieur, M. Messerschmilt, a
exécuté en mème temps des observations de pendule avec l'instrument de M. von Sterneck,
dont la Commission géodésique a fait l'acquisition. Voici les résultats de neuf stations :

 

 

 

 

 

 

 

 

| 3
Stations | Hauteur | ae a

| observée théorique i i

| ım m m | m
Zurich | A66 9.80667 | 9,80670 | — 0,00003
Lägern | 0 589 5360| 29
Wettingen | 380 il 686 702 | — 16
Wiesen . ee LOU | 640 506 | + 134
Berne D 610 599 |) 11
Fribourg . .| 630 595 rel
Naye | 1987 DS HD ee 119
Lausanne 530 615 oy. | 45
Genève | 405 605 581 | + 24

 

Les mesures exécutées dans le temps par Plantamour avec lancien pendule a réver-

sion de Repsold, fournissent les valeurs suivantes:
ASSOCIATION GÉODÉSIQUE — 24

 
