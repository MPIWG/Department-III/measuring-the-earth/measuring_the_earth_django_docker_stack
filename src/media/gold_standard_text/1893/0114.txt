 

wll

Aid

106

cru qu'il était préférable de faire une nouvelle compensation du réseau avec une valeur de
la longitude Greenwich-Paris 9"20°83 s’approchant de celle que les Anglais avaient trouvée.

Les résultats des deux compensations sont réunis dans le tableau IV; les valeurs
d'après la € première hypothèse » sont calculées avec la valeur Paris-Greenwich 9°21°03,
celles d’après la « deuxième hypothèse » avec la valeur 92085. Comme la différence entre
ces valeurs est 020, on pourra aisément déterminer par une simple interpolation les lon-
oitudes définitives, aussitôt que l’on sera fixé sur la véritable valeur de la longitnde Paris-
Greenwich.

Les différences des valeurs dans les deux hypothèses, que l’on pourra regarder
‘ comme des hypothèses extrêmes, sont, à l’exception de celles de Greenwich qui dans les
deux cas est 0"0°00 et de Nieuport, comprises entre 0°04 et 0°07, en moyenne environ 0°06.
Dans la seconde hypothése, toutes ces stations se trouvent done transportées en moyenne
de 0°06 vers louest, mais leurs positions relatives sont restées à peu près les mêmes.

Je n'ai pas calculé les poids des différentes longitudes, cependant J'ai indiqué dans
la seconde colonne du tableau par À les quarante-quatre stations du réseau compensé, qui
sont liées au moins à trois autres stations, par 2 les stations rattachées à deux autres stations
et par 3 les stations qui ne sont rattachées au réseau que par un seul point.

arms 1188111: 118 ICT CE CE el

til

il

tal tnt

{

RER

ae

 
