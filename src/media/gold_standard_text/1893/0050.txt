 

AQ

donné connaissance du projet de M. le D' Marcuse, sur le système des quatre stations à créer
sous la même latitude. Voici cette lettre :
« Milan, le 5 septembre 1895.

« A M. le Général A. Ferrero, Président de la Commission géodésique italienne.

« Monsieur le Président,

« Je vous renvoie le projet de M. Marcuse pour l’étude des variations des latitudes,
dont j’approuve pleinement l’idée fondamentale. Déjà plusieurs fois je vous avais exprimé
l’opinion qu’il est nécessaire d'établir un contrôle permanent sur ce phénomène et que c’est
à l'Association géodésique internationale à s’en charger.

« Tout porte à croire que la cause de ces variations est d’origine tellurique. S'il en
est ainsi, il n’est pas probable que ses lois puissent exprimer rigoureusement par une
théorie mathématique simple, appuyée sur un petit nombre de paramètres, telle par exemple
que la loi de Paberration ou de la nutation. Il y aura des périodes, mais irrégulièrement
variables, superposées peut-être à des variations séculaires dont il ne sera possible de décou-
vrir la loi qu'après un laps de temps assez long. Il ne suffira pas d'étudier le phénomène
pendant une seule année, ni un petit nombre d'années : il faut se préparer à prolonger la
recherche pendant un temps indéfini, comme on fait, par exemple, pour les variations pério-
diques et séculaires du magnétisme terrestre.

« Or l'Association géodésique, par son caractère international, par les moyens puis-
sants dont elle peut disposer, par la continuité de son existence el de ses traditions, est sans
doute Vinstitution capable, plus que toute autre, d’assurer la durée et Vuniformité nécessaire
dans une œuvre aussi considérable et aussi importante. Le problème est un des plus remar-
quables de la géodésie générale ; de sa résolution dépendent plusieurs théories de la géodésie,
et la pratique des déterminations géographiques. Ce problème fait donc évidemment partie
du programme de l'Association. Non seulement il est convenable pour elle de s’en occuper,
mais j'ose mème dire que c’est pour elle un devoir de première urgence.

« On a dit, et on répétera encore, qu’il n’est pas nécessaire d'établir des stations
spéciales dans ce but; qu’on pourra tirer des observatoires déjà existants tout le matériel
nécessaire pour arriver à une parfaite connaissance du phénomène pour un temps à venir
quelconque. — Sans doute, il sera possible de déduire, à l’aide d’observatoires placés sous
des longitudes différentes (qui malheureusement aussi sont placés sous des latitudes diffé-
rentes) une connaissance approchée de ces faits; nous en avons même quelque expérience
tirée des travaux exécutés dernièrement dans quelques observatoires d'Europe, Pulkowa,
Berlin, Potsdam, Prague... Les discussions très importantes publiées par M. Chandler le
montrent aussi. Cependant on ne peut nier que celte méthode de recherche ne soit sujette
à des difficultés considérables. En premier lieu la difficulté inhérente à la diversité inévitable
des méthodes et des instruments. Ensuite, le peu de probabilité de maintenir un accord
durable entre plusieurs observatoires pendant une longue série d'années. Enfin, Pimpossi-

 

CN CONTE Te

  
 

a
a

WN

Lave nike nat

Web Tay bach

À A am ai pe Du au at A put pu

 
