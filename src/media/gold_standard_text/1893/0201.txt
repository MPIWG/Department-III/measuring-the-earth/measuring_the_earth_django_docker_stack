 

NM qui

iQ A

TR CTI wii vite pe iti

ip

ANNEXES — BEILAGEN

A. Rapports speciaux — Berichte der Spezial-Referenten

Annexe A, I, Compensation du réseau des longitudes, par M. H.-G. van de Sande-Bakhuyzen

Beilage A. II. Bericht über die Präcisions-Nivellements in vorgelegt der Permanenten
Commission bei der Versammlung in Genf, 1893, von A. von Kalmar

Annexe A, III. Rapport présenté au nom de la Commission du zéro international des altitudes,
par M. Ch. Lallemand

B. Rapports des délégués sur les travaux dans leurs pays — Berichte der
Delegirten über die Arbeiten in ihren Ländern

Beilage B. TI, Gesterreich-Ungarn. Bericht über die Thätigkeit des k. k.
bureaus, vom Oberleiter desselben, Prof. Dr E. Weiss :
Beilage B. II. Oesterreich-Ungarn. a) Bericht über die Gradmessungs- hellen der en
nomisch-geodätischen Gruppe des k. u. k. militär-
geographischen Institutes im Jahre 1893, von Herrn

Linienschiffs-Capitän von Kalmär
b) Astronomische Beobachtungen, von nen Ghent

lleutenanbpon Sternen i ge
c) Trigonometrische Arbeiten, von Herrn Oberstlieut.
Hartl.

d) Bericht des Herrn Ober lientenans R. von See
über die ausgeführten Schwerebestimmungen für 1893
Annexe B. III. Belgique. Rapport sur les travaux belges, par M. le colonel Hennequin .
Annese B. IV. Danemark. Rapport sur Jes travaux a exécutés en 1893, par M. le
colonel Zachariæ . . : de :
Annexe BP. V. Espagne. Rapport sur les i avaux ue exécutés par r all dos
et statistique d'Espagne en 1893, par M. F. de P. Arrillaga No,
Annexe B. VI. France. a) Rapport sur les oe exécutés par le Service géographique de
l'Armée (octobre 1892 octobre 1893), par M. le Général Derrécagaix
b) Note sur les travaux du Service du Nivellement de la France
en 1893, par M CA. Lallemand . en,
Beilage B. VII. Griechenland. Bericht über die Arbeiten in Eee kind, von Herrn Öberst-
lieutenant Hartl. :
Beilage B. VIII. Hessen-Darmstadt. Bericht lie. ee tisane Ni lancent and den
gleichung in Hessen-Darmstadt, von Prof. Dr Nell,
Annex» B. IX. Italie. Rapport sur les travaux exécutés par la Commission on ein Fe
en 1892-1893, par M. le général Ferrero es
ASSOCIATION GEODESIQUE —

Pag.

101-134

101-114

115-123

A 2k-1 3%

137-186

137-440

141-143

145-146

147-148
149-150

151-152

153-155

156-157

158-159

160

161-167

168-171

25

 
