 

|

ne SEE TF |

nl

way im CHR

mi

NL

i
a
=
IE

00

actuel, il ne reste, après les distributions officielles, pour la vente en librairie, qu'environ 80
exemplaires, chiffre tout à fait insuffisant d’après l’experience des années passées.

M. Hirsch envisage qu’il serait peut-être justifié de proportionner le prix de vente
des volumes des Comptes-Rendus à leur étendue, en se gardant bien d'augmenter en général
le prix de nos publications et de tomber dans les travers regrettables des éditeurs allemands
qui, depuis une vingtaine d’années, exigent pour des ouvrages scientifiques des prix exagérés
dépassant notablement ceux des libraires français et anglais.

M. von Kalmar appuie cette manière de voir, mais il voudrait qu’on y donnat suite
en cédant les gros volumes, comme celui de la Conférence générale de Bruxelles, au prix
qu'on à exigé jusqu’à présent pour les Comptes-Rendus moins étendus des sessions de la
Commission Permanente et en abaissant le prix de ces derniers.

M. Ferrero reprend la parole pour exprimer le vœu que les membres de l'Association
aient la faculté de demander un certain nombre d'exemplaires tirés à part, soit des rapports
spéciaux, soit du volume complet des Comptes-Rendus, en offrant d'en payer le prix de
revient.

M. Hirsch est parfaitement d'accord avec ce vœu, pourvu que les demandes de ce
genre soient adressées au Secrétaire à temps pour qu'il puisse fixer le chiffre du tirage.
Quant aux rapports spéciaux, il rappelle qu’actuellement déjà il est d’usage d’accorder à
leurs auteurs, gratuitement, un nombre d'exemplaires jusqu’à concurrence de cinquante.

M. Fœrster fait l'observation qu’en examinant les frais d'impression pendant les
deux dernières périodes triennales, on constate que, loin d’être augmentés, ils ont sensible-
ment diminué depuis la publication des Comptes-Rendus à Neuchâtel. Il estime du reste que
le chiffre budgétaire prévu pourra suffire à couvrir les frais occasionnés par une augmentation
du tirage.

Aprés cette discussion, la Commission Permanente décide de porter le chiffre total du
lirage des Comptes-Rendus à mille exemplaires, dont 195 sont destinés, comme jusqu’à pré-
sent, a Ulmstilut géodésique prussien, el se déclare d'accord avec les vœux exprimés quant au
priæ de vente el au droit des membres de demander un certain nombre d'exemplaires au prix
de revient, en payant la valeur du papier et du tirage.

Enfin, la Commission Permanente approuve, conformément à la conclusion du rapport
de la Commission financière, les comptes de l'exercice de 1892 et donne décharge pleine et
entière à M. le Directeur du Bureau central pour sa gestion.

Continuant l’ordre du jour, M. le Président donne la parole à M. Lallemand pour
lire le rapport de la Commission spéciale chargée d'étudier la question du niveau fondamental
des altitudes.

 
