 

1%
en examinant, avant leur livraison, la bienfacture de deux télescopes 2énithaux de Wanschaff
commandés pour M. le professeur Jacobi à New-York et pour M. le général Stebnitzki, et en
faisant remédier, le cas échéant, aux défauts constatés.

«Je ne connais jusqu'ici que deux séries d'observations sur la variabilité des latitudes
pendant l'année dernière: la continuation de la série de Strasbourg, que je dois à Pobli-
seance de M. le Directeur Becker, et une nouvelle série de Potsdam; toutes deux ne sont
pas encore définitivement réduites. Les résultats fournis par les observations ont été donnés
de telle manière que les écarts positifs el négatifs, par rapport à une valeur zéro prise comme
point de départ, se compensent à peu près. À côté, on trouve des valeurs corrigées, d'apres
la formule de Chandler, tirée du tome XII de PAstronomaical Journal :

ei lo 205 DL
— (0.047 + 0,003 + +.0,00095 +?) cos (© + 10° — L).
|

T, = 2406193 jours de la période julienne — 127 novembre 1879.

1 J

+ == nombre d’années depuis 1875.

L — longitude Est, estimée à 10° en moyenne pour Strasbourg et Potsdam.

POTSDAM

 

 

OBSERVAT.

OBSER VAT ||

ANNÉE OBSER VAT. ; ANNÉE DATE OBSERVAT. ;
CORRIGÉES || CORRIGEES|

 

 

Avril 14 0.07
Mai 20 14
Juin 46
Juillet 46
Août 16
Septembre 18
Octobre
Novembre 29
1893 [Janvier 48
Fevrier 24
Mars 26
Avril 12

Mai 13

Avril 22
Mai 20
Juin 45.
Juillet 28
Aout 19
Septembre 20
Novembre 6
Decembre 18
Janvier 12
Fevrier 3
Mars 8

20)
Avril 13
Mai 27

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

« En général, la formule de Chandler est donc confirmée ; cependant les deux séries
d'observations montrent encore dans les mois de mars et avril 1893 des écarts plus sensibles
dans le même sens. (Comme cela à été constaté plus tard, des réfractions locales à Potsdam y
sont intervenues.) En faisant abstraction des erreurs d'observation, on ne peut du reste
absolument pas s'attendre à ce que la variabilité de la latitude, pour des localités situées

 

 

ae

Hun pied Om Rd Bd adit

tab ind

ln)

a ua ob ail 4 à

hostname ph aa
