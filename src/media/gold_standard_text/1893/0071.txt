 

vu en Ha

nr

et mE

FETT TTT PRA BERUFT TT

63

 

 

STRASSBURG

        
        
      
   
 
    
   
   
   
   
     
     
     
   
   
 

 

CORRIG. CORRIG.

JAHR DATUM BEOB. JAHR _ DATUM BEOB.

BEOB. BEOB.

 

 

1899 April 22 — 0,29 — 0,0% April 44
Mai 20 nu 2 Mai 20
Juni 45 — 48 0 Juni 46
Juli 28 ua ee 9 Juli16
August 19 12.00 We 6 August 16

September 48
October 20

September 0 | + 2 | + 4
November 6 Mo ei

 

 

 

 

Dezember 18 || -- 47 + 4 November 29
1893 Januar 12 -|- 3 — 2 Januar 18
Februar 3 + 4 | 4 Februar 24
März 8 0 - 12 März 26
» 20 da, April 12
April 43 — 8 + 42 Mai 43
Mai 27 6

 

 

 

 

 

 

 

 

 

 

 

« Im Allgemeinen wird also die Formel Chandlers bestatigt, nur zeigen beide Beo-
bachtungsreihen in den Monaten März und April 1893 nach derselben Seite grossere Ab-
weichungen. (Wie sich später zeigte, spielten in Potsdam lokale Refraktionen hierbei eine
Rolle.) Von Beobachtungsfehlern abgesehen, kann man übrigens gar nicht erwarten, dass die
Veränderung der Breite für Orte verschiedener Länge durch einen zweigliederigen Ausdruck
von ea. 430 tägiger und von jährlicher Periode darstellbar ist. Denn selb wenn die muth-
masslichen Ursachen der letzteren, die Massenbewegungen auf der Erdoberfläche, ganz regel-
mässig wirkten, so ist es doch sehr unwahrscheinlich dass sie gerade eine kreisförmige
jewegung des Trägheitspoles ( ergeben. Wie aus meinem Aufsatz in den Astr. Nachr. 3014
hervorgeht, nähert sich nun zwar die entsprechende Bewegung des Momentanpoles M in
der Regel weit mehr der Kreisform, als die von C, aber Abweichungen werden immerhin zu
erwarten sein.

« Für die Zukunft muss man danach streben, die Untersuchungen über die Verän-
derung der Breite in mehreren, wesentlich verschiedenen Meridianen so zu führen, dass für

jedes Jahr einzeln die Bewegung der Erdachse mit Sicherheit hervorgeht, und michterst für das

Mittel mehrjähriger Perioden, denn dies würde eine der interessantesten Thatsachen der
Erscheinung verdunkeln und die Forschung hemmen.

«Die geschäftliche Thätigkeit des Gentralbureau’s betraf die Verwaltung des Dotations-
fonds der Permanenten Commission, sowie die Versendung einer grösseren Anzahl von Publi-
cationen, deren Liste hier unten folgt.

« HELMERT. »

 
