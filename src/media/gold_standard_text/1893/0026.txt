 

IR)

cussion différents projets, entre autres celui de M. Marcuse concernant l’organisation defini-
tive de quatre observatoires de latitudes, situés sous le méme paralléle a environ 90° de
longitude. M. Hirsch rappelle qu’une organisation spéciale de ce genre a été reconnue néces-
saire en principe par la Conférence générale de Bruxelles, qui a recommandé son étude à la
Commission Permanente. Or, bien que, déjà en vue des demandes de nouvelles ressources
budgétaires qui seront indispensables, la décision définitive soit nécessairement réservée à
la prochaine Conférence générale en 1895, M. Hirsch croit que la Commission Permanente
a l'obligation de préparer cette décision en étudiant dès à présent dans ses réunions les pro-
jets qui se produisent dans lPintervaile.

M. le Président est d'accord pour mettre ce sujet à l’ordre du jour d’une des pro-
chaines séances ; pour aujourd’hui, il croit l'heure trop avancée pour passer encore à la
lecture du Rapport de M. von Kalmär sur les nivellements et le renvoie à la prochaine séance,
qui aura lieu jeudi à 10 h. du matin. Après avoir accordé la parole à M. Gautier qui désire
donner quelques renseignements sur le programme général de la session, M. le Président
lève la séance à midi et quart.

Tdi

OO RI Me 1

a

Hay ay tee hy

wt

di in

\

|
i

 

 
