 

Mickie See

tua meer en rae |

yaa

iV CAB

LOFT FAIRE D RO Le

mir

I

24

 

nations relatives de la pesanteur aux Observatoires de Vienne, Paris et Padoue, avec les appa-
reils de M. le colonel von Sterneck et de M. le commandant Defforges.

Le rapport de M. Ferrero contient une liste de six stations nouvelles de premier
ordre; on a ainsi achevé la triangulation de premier ordre dans ltalie continentale et dans
l’île de Sardaigne; il reste essentiellement à opérer la jonction du continent avec la Sar-
daigne en utilisant quelques stalions de la Corse, mais une entente dans ce but avec les
collègues français n’a pas encore abouti. Le rattachement à PAlbanie, dont la triangulation a
été exécutée par les oéodésiens d’Autriche-Hongrie, donne une différence de onze centimètres
entre les deux valeurs provisoires des côtés de jonction. Ce rapport fournit également des
détails sur les nivellements de précision, en particulier sur ceux qui réunissent les différents
maréographes et qui permettent de comparer les niveaux moyens de la Méditerranée et de
VAdriatique. (Voir Annexe B. IX.)

M. le Président propose une interruption de séance jusqu'à une heure.

À la reprise, et pour revenir à l’ordre alphabétique des pays, M. le Président invite
M. Hennequin à présenter lerapport sur les travaux de la Belgique. Ge rapport rend compte des
opérations de jonction du réseau belge avec le réseau prussien et celui des Pays-Bas. L’adminis-
tration des Ponts et Chaussées a fait installer définitivement le maréographe d’Ostende auprès
duquel on a établi un médimarémètre, système de M. Lallemand, dont les résultats sont
identiques à ceux donnés par le maréographe. D’après les raccordements du nivellement de
précision belge avec les réseaux voisins, on trouve que le niveau moyen de la mer à
Ostende est à 0" 151 en-dessous du niveau moyen de la mer à Marseille, à 0° 339 en-dessous
du Normal-Null et 4 0" 320 en-dessous de l’Amsterdamsche-Peil. (Voir Annexe B. III.)

M. Zachariæ n'ayant pu venir à Genève enverra au secrétaire son rapport sur les
travaux en Danemark, qu'on trouvera parmi les Annexes. (Voir Annexe B. IV.)

La parole est donnée à M. d’'Arrillaga pour présenter le rapport sur les travaux
d'Espagne. Ce rapport comprend d’abord les travaux astronomiques, parmi lesquels la
détermination de la difference de longitude entre Barcelone et Vigo, pour ce dernier point
on a également mesuré la latitude et l’azimut.

L’intensite de la pesanteur à été calculée pour la station de Pampelune, où elle a été
observée l’année derniére. Le résultat diffère du nombre théorique, suivant la formule Hel-
mert, pour la longueur du pendule simple, de 0°000.3546.

On vient de terminer les observations à Coruña et on les poursuit dans ce moment à
Jarcelone.

Quant aux nivellements de précision, on a achevé les lignes de Logrofio 4 Taca et de
Soria 4 Tudela, et dans cette campagne on exécute le nivellement entre Gijon (port de PAtlan- :
tique) à Santander (station maréographique).

infin le rapport de M. d’Arrillaga contient des résultats intéressants fournis par les
maréographes d’Alicante, de Cadix et de Santander. (Voir Annexe B. V.)

 
