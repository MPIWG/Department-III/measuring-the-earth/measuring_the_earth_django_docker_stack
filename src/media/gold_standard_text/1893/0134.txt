 

7

4 G

(

b

 

avec celle de 066, déduite du nivellement espagnol entre Alicante et Santander, et il en con-
clut que désormais la réalité des différences notables signalées dans les niveaux des mers ne
saurait être mise en doute. En terminant, il exprime le vœu que les lacunes existant dans
les jonctions des réseaux de nivellement soient rapidement comblées, et que le niveau moyen
de la mer et ses variations soient exactement déterminés dans les principaux ports, afin que,
dans la prochaine Conférence générale, on puisse définitivement choisir le zéro international
des altitudes,

Il. — Conférence de Nice, 1887.

Dans la Conférence générale de Berlin, en 1886, M. Lallemand avait rappelé Valten-
lion sur les erreurs que peuvent introduire, dans les résultats d’un nivellement le défaut de
parallélisme des surfaces de niveau du globe terrestre et la variation de longueur des mires,
au cours des opérations sur le terrain, sous l’action de la température et de lhumidité.
Il avait indiqué les moyens pratiques employés dans le nouveau Nivellement général de la
France pour calculer et corriger ces erreurs.

En vue de permettre une comparaison du niveau des mers, pratiquement affranchie
de l'influence de ces deux causes d'erreurs, MM. von Kalmar et Hirsch font adopter l'année
suivante, par la Conférence de Nice, un vœu tendant à ce que les appareils d'observation du
niveau de la mer soient, autant que possible, reliés entre eux par des lignes de nivellement
longeant les côtes et ne s’élevant pas à de grandes altitudes.

IV. — Conference de Salzbourg, 1888.
we)

Dans la réunion de Salzbourg, en 1888, M. le professeur Hirsch soumet, au nom
du bureau de la Commission Permanente, un projet de résolution d’après lequel la question
du zéro international, maintenant considérée comme müre, devra être définitivement Lran-
chée par la Conférence générale de 1889.

Pour des raisons pratiques, ce zéro doit être choisi de préférence dans un des
ports de la mer du Nord, n’appartenant pas à un grand Etat.

M. Lallemand fait observer qu’avant de prendre une décision de cette importance, il
serait peut-être prudent de poursuivre, pendant quelques années encore, les études à ce
sujet, la réalité de notables différences entre les niveaux moyens des diverses mers ne
paraissant pas encore absolument démontrée.

M. Bouquet de la Grye opine aussi pour lajournement et ajoute qu'en tous cas le
zéro international devrait être choisi sur le littoral d’une mer à faible marée, soustraite à l’ac-
tion des cyclones, comme la Méditerranée.

 

 

du sd an Äh bet uud u

Lah

 
