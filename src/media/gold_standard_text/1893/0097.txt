 

Im

LIL

RO VPN OR LU PR TI

Auflage der Verhandlungen auf 1000 zu erhöhen, wovon, wie bisher, 125 für das K. preus-
sische geodätische Institut bestimmt sind, und erklärt sich einverstanden mil den ausge-
sprochenen Wünschen, betreff des Verkaufspreises und des Rechtes für die Mitglieder, eine
gewisse Anzahl Exemplare zum Herstellungs-Preise zu verlangen, gegen Zahlung der Kosten
für Papier und Druck.

Ausserdem billigt die Permanente Commission, dem Antrage der Finanz-Commis-
sion gemäss, die Rechnungs-Ablaye fir das Jahr 1892, und ertheilt dem Herrn Direktor des
Centralbureau’s volle und unbeschränkte Entlastung für seine Verwaltung.

Im Verfolg der Tagesordnung, giebt der Herr Präsident Herrn Lallemand das Wort
zur Verlesung des Berichtes der Spezial-Commission, welche mit dem Studium der Frage
des allgemeinen Fundamental-Niveau’s der Höhen betraut ist.

Dieser Bericht enthält zunächst Vorschläge zu einer übereinstimmenden Termino-
logie für die bei dieser Frage zu verwendenden Ausdrücke. Da die Commission wegen dieser
Terminologie nicht zu Rathe gezogen ist, so giebt Herr Lallemand seine Vorschläge nur als
persönliche.

Der zweite, bei weitem wichtigste Theil liefert einen historischen Rückblick über
die verschiedenen Phasen, welche diese Frage im Schosse der Erdmessung durchgemacht
hat. (Siehe Beilage A. 111.)

Herr Hirsch erinnert daran, dass nach den in Brüssel gefassten Beschlüssen es sich
heute nicht darum handeln kann die Frage selbst zu erörtern, oder definitive Beschlüsse zu
fassen. Herr Lallemand sagt übrigens selbst im Beginne seiner Auseinandersetzung, dass die
Commission, welche in der Zwischenzeit der beiden Gonferenzen sich nicht versammelt hat,
heute keine Vorschläge unterbreiten kann, sondern nur die Permanente Commission über
den jetzigen Stand der Frage auf dem Laufenden zu halten wünscht. Es ıst zu hoffen, dass
in der nächsten Versammlung diese Untersuchung weiter gefördert sein wird, so dass diese
wichtige Frage endlich in der General-Conferenz von 1895 endgültig erledigt werden kann.

Herr Ferrero meint ebenfalls, dass der Bericht des Herr Lallemand zu keiner weiteren
Discussion Veranlassung giebt.

Die Sitzung wird um Mittag unterbrochen, und um I Uhr 50 Minuten wieder auf-
genommen.

Der Herr Präsident ertheilt dem Ilerrn Forster das Wort, um auf die Frage der
definitiven Organisation der Breiten-Beobachtungen zurückzukommen.

Prof. Ferster verliest zunächst den von der vorjährigen General-Gonferenz angenom-
menen Bericht der Permanenten Commission über die demnächstige Behandlung der Pol-
höhen-Frage.

Durch den Ausfall der diesjährigen Versammlung der internationalen astronomischen

ASSOCIATION GEODESIQUE — 12

 
