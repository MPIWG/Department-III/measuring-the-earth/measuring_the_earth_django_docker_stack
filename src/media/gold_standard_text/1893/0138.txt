 

 

 

pays, du zéro international, supposé en coincidence avec le niveau moyen de la mer sur un
point des côtes européennes, s’opérerait, par la mer elle-méme, avec une exactitude plus
grande, qu’au moyen des nivellements actuels les plus précis.

La proposition de M. Hirsch est adoptée par la Commission Permanente.

VIII. — Conference generale de Bruxelles, 1892.

Les réponses faites à Bruxelles, en 1892, par le Bureau central de lAssociation !,
aux lrois questions qui lui avaient été posées, peuvent se résumer ainsi :

4o Les hauteurs des deux seuls zéros normaux non situés sur le rivage d’une mer,
savoir, la Pierre du Niton et le zéro normal prussien, peuvent être fixées respectivement à
6 centimètres et à 4 centimètres près, par rapport au niveau moyen des mers les plus voisines.

90 Les différences de hauteur du niveau moyen de la mer, entre des points appar-
tenant à un même littoral et reliés par des nivellements directs, atteignent :

Sem dans la Baltique ;

48cm dans la mer du Nord;
97em dans la Manche ;

33cm dans l'Océan Atlantique ;
42cm dans la Méditerranée ;
17m dans |’ Adriatique.

30 D’aprés ces chiffres, aucune des mers en question ne présenterait un niveau uni-
forme, les dénivellations le long d’un méme littoral atteignent les mémes grandeurs que les
dénivellations des mers entre elles.

Relativement à la stabilité relative des côtes et des mers, les observations faites
jusqu’à ce jour sont encore insuffisantes pour permeltre de formuler à cet égard une opinion
certaine.

Dans ces conditions, le Bureau central ne peut que maintenir sa précédente con-
clusion, tendant 4 abandon du zéro international des altitudes.

Si chaque pays prenait pour zéro normal le niveau moyen de la mer en un point de
ses côtes, le désaccord des altitudes aux frontières ne dépasserait pas 9 à 3 décimètres ; or
l'adoption d’un zéro unique n’empêcherait pas l'existence de pareilles differences aux points
de jonction non englobés dans la compensation générale.

Vouloir à tout prix éliminer ces discordances une fois pour toutes, serait mettre une
barrière à la marche du progrès.

1 Note de M. le D: Bôrsch, présentée par M. Helmert (Comptes-rendus de la Conférence de Bruxelles
en 1892, Annexe A YI, page 547).

=

(CIN TEEN

ee hi

Lou

 

 
