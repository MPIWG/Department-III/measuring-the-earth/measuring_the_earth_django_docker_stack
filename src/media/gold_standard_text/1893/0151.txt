 

|
|
i

LLC Am RTE

il

DE A D DR A de

mi lpr

 

143
welche zusammen eine Arbeitsleistung von 893 km repräsentiren, von denen etwa der
4. Theil auf Strassen-Nivellemenis entfällt. Durch diese Messungen sind die im vorigen
Jahre gemachten ersten Nivellements controllirt, zugleich aber — was die beiden Linien Laa-
Jedlersee betrifft — die zweiten Messungen für solche ältere Linien nachgetragen worden,
die, nach den damaligen Absichten, nur einmal gemessen und daher lediglich durch den
Polygonschluss verificırt werden sollten.

Auf diesen Linien sind die astronomischen Punkte Dablitz (Linie Turnau-Prag) und
Buschberg (Linie Laa-Jedlersee) berührt und mit Nivellement-CGoten versehen worden.

Die Gesammt-Arbeitsleistung in der diesjährigen Arbeits-Garmpagne beträgt demnach
rund 1460 km.

Das Ergebniss der im Sommer gemachten relativen Lattenvergleichungen und die Re-
sultate der absoluten Massbestimmungen unserer Latten, die vor dem Abgange zur Feldarbeit
und nach dem Eintreffen von derselben ausgeführt wurden, sind in der nachfolgenden Tabelle

übersichtlich zusammengestellt.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

| Latte A’ | Latte BY’ | Lette D Latte E’ Latte F’ | Latte G’ Latte H’
| Mittlere Lauge des Lattenmeters, abgeleitet aus den
Beobachtungs- A , 2 a iy
Periode abso- | rela— abso- rela- abso- rela- abso- rela- abso- rela- abso- rela- abso- rela-
| luten tiven luten tiven luten tiven luten tiven luten tiven luten tiven luten tiven
Iuattenvergleichungen
1000000 1000000 1000000 1000000 4000000 4000000 4000000
[Mitte April 18934 438] | 448 : or el + 464 1. 394
» Ma, > ed. res In A dE Lo. 11 998
> Jom > [+ 512 a 80 + -869| : als) + 384
» Juli » |-- 528 + 483 + 578 + 494 + 432
» August » -- 559 — 510 — 596 + 526 + 467
» Sept. | » e567 uns + 599 + 544 I 465
» October » . | 584 - + 579 : - + 606 - . - + 591 5 + 535
» Novem.» |+ 604|4: 575) - {+ 576)+ 624/+ 632/+ 643|-+ 6927| 425! 4941/4 64314 626|+ 599/-+ 572
Differenzen . . ap 20 + 24 0 + 16 2 mn = 20

 

 

 

 

 

ANMERKUNG. — Die beiden Latten D’ und F’ sind bei der diesjährigen Feldarbeit nicht verwendet worden.

Mit Ausscheidung der Latte F’, bei deren Vergleichung ein Fehler unterlaufen sein
dürfte, stellt sich, zufolge der oben angeführten Differenzen, der wahrscheinliche Fehler einer
relativen Vergleichung auf + 14 Mikron.

von KALMAR, m. p.

kn. k. Linienschiffs-Capitän.

 
