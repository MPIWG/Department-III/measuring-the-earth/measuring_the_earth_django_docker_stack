 

I
E
|
|

 

dhgeveunt
PRRBARTETT

ATV ETUI

pe AN RON RL NFO Fa

aE
13

toujours, la précision nécessaire pour ces observations délicates. Que le Bureau central
remplisse la tâche qui lui incombe, de fournir prochainement aux géodésiens et aux astro-
nomes pour les deux dernières années et pour celles qui vont suivre des tables de réduction
qui donnent, à quelques centièmes de seconde près, les réductions de la position momen-
tanée de l’axe terrestre à une position initiale, convenablement choisie, et on sera frappé
bientôt des difficultés qu’il y a à déduire ces nombres, avec une certitude convenable, des
travaux libres de quelques observatoires isolés, et surtout à obtenir ainsi les matériaux
nécessaires dans un délai rapproché. Or, le retard que subira ainsi l'établissement de ces
tables de réduction fera naître bientôt un mécontentement général et intense, parce que de
nombreux travaux délicats se trouveront arrêtés par ce défaut des réductions nécessaires :
car ce que lon a täche jusqu'à présent d'éliminer autant que possible comme provenant
d'erreurs accidentelles, ne saura désormais être mis en compte explicitement que sur la
base de mesures directes ; et aussi longtemps qu’on ne disposera pas pour cela de tables de
réduction, la poursuite de nombreuses recherches délicates sera arrêtée.

Mais, même en supposant qu’on réussisse à combiner dans ce but une coopération
libre et suffisamment complète de plusieurs observatoires et à obtenir la communication assez
rapide et régulière de leurs résultats, on verra, après un petit nombre d'années, que ces
résultats ne seront pas assez homogènes, si l’on ne réussit pas à éliminer complétement les
mouvements propres des étoiles employées. EL cela n’est possible, avec la méthode la plus
exacte, qu'à la condition que trois observatoires au moins, qui y concourent, soient situés, à
quelques kilomètres près, sous le même parallèle. Or, une telle combinaison n'existe pas, ii
faut donc la créer ad hoc. (est là notre projet, qu’on se plait à désigner comme inutile et
exagéré, el qui est cependant exigé par la logique des faits et par un jugement réfléchi.

Une pareille organisation sera en outre aussi au point de vue économique plus avan-
tageuse que l'accumulation de travaux hétérogènes et isolés, qu’on continue à prôner pour les
observatoires, abstraction faites des deux grandes entreprises internationales : les Zones « de
la Société astronomique » et la « Carte photographique du Ciel »; on risque ainsi soit d’in-
terrompre certaines recherches, soit d’accumuler inutilement des matériaux incohérents, et
d'autre part de laisser tout à fait de côté d’autres branches d’études.

Afin de poursuivre énergiquement les efforts pour une organisation rationnelle des

observations de latitude, M. Foerster propose de reconstituer la Commission nommée en
1891 à Florence.

M. Ferrero donne d’abord connaissance d’une lettre de M. Pergola, dans laquelle
M. le Directeur de l'observatoire de Capodimonte lui apprend qu’il s’est entendu avec M.
Harold Jacoby, 4 New-York, pour entreprendre des observations simultanées de latitude,
d'après la méthode de Talcolt, avec deux lunettes zénithales identiques de Wanschaff, et
enfin au moyen des mémes groupes d’étoiles, puisque la différence de latitude des deux
observatoires est assez faible pour le permettre.

© M. Ferrero traduit ensuite une lettre qu’il a reçue de M. Schiaparelli, auquel il avait
ASSOCIATION GÉODÉSIQUE — 6

 
