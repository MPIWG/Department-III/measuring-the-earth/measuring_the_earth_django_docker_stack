 

Te Peg

 

LT UP

D RUN DDR M LUN pt tir

=

 

Beilage B. IT d.
BERICHT

des Oberstlieutenant R. von Sterneck über die ausgeführten
Schwerebestimmungen fiir 1893.

Die Resultate der vorjährigen relativen Schwerebestimmungen in Berlin, Potsdam
und Hamburg, dann jene zur Untersuchung der Schwereverhältnisse auf den Linien Graz-
Wien, über die Karpathen, und in der ungarischen Tiefebene, sind im XII. Bande der Mit-
theilungen des k. u. k. militär-geographischen Institutes veröffentlicht.

Die relativen Bestimmungen an den drei genannten Orten gewähren in Verbindung
mit den schon früher erhaltenen derartigen Resultaten einen Einblick in die Debereinstimmung
einiger bisher ausgeführten absoluten Bestimmungen der Sekundenpendellänge, welche auf
Wien redueirt, gleiche Werthe geben müsste. Die Uebereinstimmung ist jedoch eine geringe,
da die Differenzen 100 Mikron der Sekundenpendellänge übersteigen. Es dürften demnach
den meisten absoluten Bestimmungen Fehler systematischer Art anhaften.

Durch die ausgeführten Untersuchungen über Schwereverhältnisse auf den oben
genannten Linien wurde unsere Kenntniss über das Verhalten der Schwere in verschiedenen
Gegenden und geologischen Formationen wieder etwas vermehrt, doch ist das bisher gewon-
nene Material noch zu gering, um daraus allgemeine Regeln aufstellen zu können. Indessen
scheint jetzt schon festzustehen, dass weniger die sichtbaren Formen des Terrains, als viel-
mehr die geologischen Beschaffenheiten desselben auf die Grösse der Schwere von Ein-
fluss sind.

Mit Bewilligung des k. u. k. Reichs-Kriegs-Ministeriums wurden im Laufe des
heurigen Sommers beide Arten von relativen Schwerebestimmungen fortgeseizt.

Zur Vergleichung der Ergebnisse der absoluten Schwerebestiimmung unter sich,
sowie behufs Uebertragung derselben auf Wien, wurden relative Bestimmungen in Paris,
London (Greenwich und Kew Observatory), Strassburg und Budapest ausgeführt.

Die Untersuchungen über das Verhalten der Schwere im Anschlusse an die früheren
Arbeiten werden auf den Linien : Püspök Ladany-Budapest-Graz-Rlagenfurt bis Franzens-
feste und Landeck-Bregenz fortgesetzt. Hiedurch werden einzelne Strecken früherer Messungen
mit einander in Verbindung gebracht, und es wird eine ununterbrochene Ost-West-Linie der
Untersuchung zur Verfügung stehen, welche von Bregenz bis Maros-Vasarhely sich über
14 Längengrade erstreckt und die verschiedensten Terrainverhältnisse durchzieht.

Meine bisherigen derartigen Untersuchungen, welche nur als eine vorläufige Durch-
forschung zur Gewinnung eines allgemeinen Ueberblickes zu betrachten sind und demgemäss

 
