 

Messungen durch Herrn Professor Aımoxerrı entnommen worden.

Catania
Palermo
Milazzo
Castellamare

31.2 3022

p

38

6.9
13.1
40 41.6

14

 

4
i
À
|
;
i

9 (Potsd. Syst.)

980.065 = 0.005 em sek”?
980.069 = 0.004
980.198 Æ 0.004

980.318 Æ 0.004

”

Für die Referenz-Stationen Catania und Palermo stimmt das Ergebnis der Nä-
herungsrechnung auf S. 201 mit dem der Ausgleichung genau überein, während die für
die beiden Zwischenstationen Milazzo und Castellamare abgeleiteten Näherungswerte
durch die Ausgleichung eine Korrektion von je + 0.001 cm sek”? erfahren.

Gruppe 4. Messungen in Oberitalien (Piemont).

(Ref. Stat. Turin, Pal. Mad.)

Das Material unsrer Tabelle ist den Berichten 1900, 1903 und 1906, Tab. XVd:

In der Tabelle zum

Bericht 1900 werden 2 g-Werte fiir Turin mitgeteilt, von denen der eine (Turin 1896)
aus einem Anschluß an Wien Stw., der andere (Turin 1395) aus dem Anschluß an Padua

hery

orgegangen ist.

Der Wiener Anschluß ist zwar im erläuternden Text (S. 293), nicht

aber in der Tabelle zum Ausdruck gebracht, was ich hier zur Erklärung der nachträg-
lichen Aufnahme der Station Wien Stw. in den vorliegenden Bericht 1909 erwähne.

Jahr

1893

1896

1896

1897

1898

1901

Syst

Für die Ableitung von g(Turin) im Potsd. Syst. liegen folgende Messungen vor:

Station

Wien, Stw.

Turin, Pal. Mad.

Turin, Pal. Mad.

Paris, Obsery.

Turin, Pal. Mad.

Padua, Stw.

Turin, Pal. Mad.

Padua, Stw.

Turin, Pal. Mad,

Padua, Stw.

p

1e 1329

45

45
48

cll

bo

PX

u) I
102 20.7 237m
HAUTS 233
lene 233
2 207% 61
TS 233
Il 25223 19
te 8 233
lee Oto 19
TAB: 933
I 92.8 19

Aus diesen Daten lassen sich

. herleiten:

g (beob.)
980.866

rm

o11
980.588
981.000
980.565

980.570
377

980.572
: a

Beobachter
vy. STERNECK

Amonerrr J

Cozrer |

J

”

BAGLIONE |

AIMONETTI |

» J

AIMONEDTI |

ah

Quelle
Ber. 1900,
Tab. XVd u. 8.293
Ber, 11903,
S. 195
Ber. 1900,
Tab. XVb

Ber, 1900,
Tab. XVd

Ber: 1905,
ao, XVIe

die nachstehenden Werte für g(Turin) im Potsd.

 
