 

74

rarwsche Halbsekundenpendel beigegeben, für deren Konstanten Herr Prof. L. Haasemann
im Frühjahr 1901 nachstehende, in 1077 Sternzeitsekunden angegebene Werte fand:

Temperaturkoeffizient Luftdichtekoeffizient

Pendel Nr. 13 47.81 0.10 655.85 6.2
ed 48.76 + 0.13 642.0 + 10.6
i, 26 48.95 + 0.15 640.4 3.9

Dr 48.41 + 0.14 650.1 + 8.8

Auf allen 3 Stationen der Expedition fanden die Pendelbeobachtungen im luft-
verdünnten Raum bei etwa 60 mm Druck statt, wobei die Pendel ca. 12 Stunden
schwangen. Zur Bestimmung der Schwingungsdauer stand ein Narpiy’scher MZ-Chro-
nometer (55/8555) und eine Halbsekunden-Pendeluhr von Strasser & Ronpe (Nr. 231),
beide mit elektrischer Kontakteinrichtung versehen, zur Verfügung; außerdem hatte die
Expedition 4 weitere Chronometer (2 SZ, 2 MZ) an Bord, die gelegentlich zu Ver-
gleichungen herangezogen wurden.

Die Anschlußmessungen in Potsdam haben folgende, auf den Normalort der
Schwerebestimmungen (H — 87 m) bezogene Werte der reduzierten Schwingungsdauern
ergeben:

Pendel 13 Pendel 14 Pendel 26 Pendel 27
Potsdam, 1901, Frühjahr: 05076010  0°5077935 05099080  0:5097910
oy ee 6023 7939 9088 7927
Anderung in 3 Jahren: 48 al +8 ir

die eine genügende Konstanz der Pendel erkennen lassen.

Die erste Station, welche die Expedition auf dem Wege zum Südpolargebiet
erledigte, war Porto Grande (genauer: Am Porto Grande, dem Hafen von Mindello)
auf der Kapverdeninsel Säo Vicente. Hier beobachtete Prof. von Drysazskı 2 Pendel
(Nr. 13 u. 14) über je 12 Stunden. Leider konnten infolge Ungunst der Witterung
keine Zeitbestimmungen ausgeführt werden; der Gang des zu den Pendelbeobachtungen
allein verwendeten Chronometers Narpın mußte durch Vergleichung mit den 4 Bord-
Chronometern, zwischen deren Standbestimmungen ein Zeitraum von rund 3 Monaten
lag, abgeleitet werden. Aus der eingangs genannten Publikation ist nicht ersichtlich,
weshalb die Nardinstiinde nur auf ganze Sekunden abgeleitet wurden; es wird jedoch
mitgeteilt (S. 326), daß die so ermittelten Nardingänge auf etwa 2 Sekunden unsicher
sind, während an einer andern Stelle (8. 360) die Unsicherheit dieser Gänge ohne nähere
Begründung zu 0.75 Sekunden angenommen wird.*) Beiden Angaben würden Unsicher-
heiten im Stationsmittel der Schwingungsdauern von rund 120 bezw. 45 . 10°7 sek oder
in g von etwa 47 bezw. 18. 10”° cm sek? entsprechen. Berücksichtigt man noch, daß

 

*) Die erste Angabe rührt von Herrn Regierungsrat Dr. Doms, der die Zeitbestimmungen,
die zweite von Herrn Prof. Hassemann, der die Schweremessungen der Expedition bearbeitet hat, her.

 

en ae

dh ane,

hu

 
