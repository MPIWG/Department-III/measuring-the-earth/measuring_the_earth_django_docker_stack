 

oo

T

; Die neuen Messungen, hier auf die Standorte der alten reduziert, sind an den
folgenden Stellen nachgewiesen:

Mannheim Bericht 1909, Tab. I, Gr. 7, B
Gotha n 1909, 1 I, MH 4, B
Leipzig 1009,20 2, AB
Leiden 7 190%. 1, 03
Berlin 1909, „m ,„4B;

ihre durchschnittliche mittlere Unsicherheit beträgt etwa =+ 0.002 cm sek”?, sodaß die
obigen Reduktionsbeträge nahezu die wirklichen Fehler der alten Messungen darstellen.
Die Ursache dieser bedeutenden Fehlerbeträge ist wohl hauptsächlich in der Veränder-
lichkeit des Mitschwingens (Pfeiler +4 Untergrund), das nirgends ermittelt wurde, zu
suchen.*) Besonders fehlerhaft erscheint der absolute g-Wert fiir Gotha (Hermerr, 1. c.
S. 195); schließt man ihn aus, so würde sich als Reduktion auf das Potsd. Syst.
der Mittelwert — 0,017 cm sek”? ergeben, den wir angewendet haben. Die mittlere Un-
sicherheit einer älteren Messung würde danach im Durchschnitt etwa = 0.011 cm sek”?
betragen; sie kann aber für einzelne Stationen erheblich größer sein, wie das Beispiel
von Gotha zeigt.

Im Vergleich mit den neueren Arbeiten können die vorliegenden, sowie auch
die unter A und B dieser Gruppe aufgeführten, nur noch historisches Interesse bieten.

Gruppe 2. Messungen durch deutsche Expeditionen.
(Ref, Stat. Potsdam, G. L)

Mit Ausnahme der Stationen der Deutschen Südpolar-Expedition und der im
Nachtrag zur Gruppe 2 (8. 77 dies. Ber.) erwähnten Arbeiten ist das Material unsrer
Tabelle dem Bericht 1900, Tab. Ve: Messungen durch Herrn Prof. v. Drxcausxr ent-
nommen worden.

Die neu hinzutretenden Arbeiten der Deutschen Südpolar-Expedition
sind veröffentlicht in dem Werke: Deutsche Stidpolar-Expedition 1901—1903; im Auf-
trage des Reichsamtes des Innern herausgegeben von dem Leiter der Expedition
Erich von Drycauseı, Bd. I, Geographie. Von diesem Werke lag uns ein unter dem
Titel: Schwerkrafts-Bestimmungen der Deutschen Siidpolar-Expedition 1901—1903 auf
Säo Vicente (Kapverden), auf Kerguelen und in der Antarktis, von Kirton von Drvsanskı
und 1. Haasemann erschienener Sonderabdruck vor, dem wir die nachstehenden Angaben
über die Messungen entnommen haben.

Zu den Schweremessungen wurde ein luftdicht verschließbarer Zweipendel-Apparat
verwendet, den der Mechaniker des Geod. Inst. in Potsdam, M. Frcunxer, nach Angaben
des Herrn Prof. Hsımerr besonders hergestellt hatte.**) Dem Apparat waren 4 Srück-

*) Vergl. hierzu die Erläuterungen von Herımerr im Bericht 1900, S. 194 u. 195.
**) Eine Beschreibung dieses Apparates ist in der Zeitschrift für Instrumentenkunde 1902,
8. 97 ff. erschienen.

10

 
