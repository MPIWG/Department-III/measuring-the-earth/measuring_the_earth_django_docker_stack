 

30

 

größer ist sie nur bei ein paar älteren, meist aus absoluten Messungen gebildeten Reihen,
die gegenwärtig kaum noch ein anderes als historisches Interesse bieten.

Bei der Übertragung der y-Werte ins Potsdamer System wurden zunächst die
Angaben für g, 9, A, H, © und g’— g = topogr. Red. in den früheren Schwereberichten,
soweit es möglich war, mit den ihnen zugrunde liegenden Originaldaten verglichen, wobei
einige Druckfehler im Bericht 1900 verbessert werden konnten. Nachdem so die Grund-
lagen für unsere Tabellen sichergestellt und alle g-Werte ins Potsdamer System über-
tragen waren, wurden die abgeleiteten Größen g, und g;, sowie auch die Größen Y,,
9% m und 90), sämtlich neu berechnet und die erhaltenen Werte ebenfalls mit
den entsprechenden Angaben der früheren Schwereberichte verglichen.

Zur Berechnung der erwähnten Größen wurden die Formeln

9, 91 1027-3086 7

" 30
RI hm re 9)
7, = 978.080 {1 -+ 0.005302 sin? 9 — 0.000007 sin? 2 y}

verwendet, worin g die beobachtete Schwerkraft im Potsdamer System, y die geographische
Breite der Station, H ihre Meereshöhe in Metern und © die Dichte der unter ihr liegenden
Erdmasse bezeichnen. Von den abgeleiteten Grössen y, und 9, bedeutet die erste den
Wert von g, reduziert aufs Meeresniveau wie in freier Luft; die zweite denselben
Wert, vermindert um die Anziehung der Erdmasse zwischen der Erdoberfläche in einer
gewissen Umgebung der Station und dem darunter liegenden Meereshorizont. Diese An-
ziehung setzt sich nach der zweiten Formel aus 2 Teilen zusammen: aus der Anziehung
3 O (g — go) /4-5-52 einer massiven Platte zwischen dem Stations- und dem Meeres-
horizont und aus einer durch die Abweichung des Erdreliefs vom Stationshorizont bedingten
Korrektion (g’— g) der Plattenanziehung, der sogen. topographischen Korrektion.

‘ Der obige Ausdruck Y für die normale Schwerkraft im Meeresniveau ist aus
der Hrunerr’schen Formel von 1901, die sich auf das Wiener System bezieht, abgeleitet,
indem an die Äquatorkonstante dieser Formel die Reduktion vom Wiener auf das Pots-
damer g-System nach 8. 26 angebracht worden ist; es besteht also zwischen beiden
Formeln die Beziehung:

7909) — 70 (120) 0.016 em sek

Wie in den früheren Berichten wurden auch hier zweifelhafte Höhenangaben
in den Tabellen mit einem ? versehen und vorläufig angenommene Werte der Erdboden-
dichte in () gesetzt; außerdem sind zweifellos fehlerhafte g-Werte, deren weitere Ver-
wendung nicht zu empfehlen ist, durch ein hinter den Angaben go — 7% in der letzten
Spalte der Tabellen stehendes (4) gekennzeichnet worden.

Es folgen nun die tabellarischen Darstellungen und Erläuterungen über die
Arbeiten der einzelnen Staaten in der auf 8. 29 vorgezeichneten Reihenfolge. Ein alpha-
betisches Verzeichnis der Schwerestationen am Schlusse des Berichts erleichtert wesentlich

den Gebrauch der Tabellen.

 

 
