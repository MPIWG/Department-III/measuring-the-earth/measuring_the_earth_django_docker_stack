 

139

Jahr = Station pa À H g Beobachter Quelle: Ber. 1909,
1890 1. Orenburg Bile Aa 55° 6.8 100m 981.205 SoxoLow |
2, Samara Howls) 5082523 85 356 n Tab. VI, Gr. 2 A
3. Pulkowo 59 46.3 80 19.7 75 888 i |
1902 4. Samara 53 11.0 DD Ro 65 981.365 Baranow
2 = | Map. VI Gu: Al
5. Kasan 55 47.4 49.7.3 70 512
1902 6. Taschkent Ate Oia 69 KT 478 980.071 ZALESSKI
ee ab, Ve Gre G8
7. Tiflis 41 43.1 44 47.8 412 175
1905 8. Taschkent A 19,6 COTE 478 980.086 ZALESSKI |
9. Orenburg b1 45.1 55 62 100 981.192 ab vl Ce GC
10. Samara 53 10.8 502 952 65 354 : J

Anmerkung. Der Soxonow’sche g-Wert fiir Orenburg (1890) ist hier durch Anbringen der
Reduktion d‘g (Orenb. 1890) = + 0.002 cm sek"? auf den Zurzsskr’schen Standort (1905) bezogen worden
(8. à. a. O.).

ll Vergleichen wir diese Beobachtungsreihen mit den Annahmen:

iF
I}

| p A H g Potsd. Syst.)

| Taschkent 41° 19/5 69° 17/7 478m 980.088 + (1)

| Tiflis 43.1 44 47.8 412 980.175

|; Orenburg Se Aa 55 649 100 981.205 +2)

il Samara 53 10.8 30. Bo) 65 981.363 + (8)
Kasan 55 47.4 19 75 71 981.572
Pulkowo 59 46.3 30 19.7 75 981.898 ;

worin die g-Werte der Hauptstationen Tiflis, Kasan und Pulkowo im Potsd. Syst. durch
die Netzausgleichung S. 25 gegeben sind, so gelangen wir zu folgenden Fehler- und

i reduzierten Fehlergleichungen:
Jahr Station Fehlergleichungen Reduzierte Fehlergleichungen
1890 1. Orenburg u, +2) = Oo+ 2/8 (2) — 1/8 (8) = +17/38 + «,
2. Samara u+(8)=— 7T+8 — 1/3 Q) + 2/3 (8) = —4/3 + &
3. Pulkowo u — —10+8, — 1/3 (2) — 1/8 (8) = —13/3+ 8,
1902 4, Samara uy +(83) = + 2+e 1/2(8) = +1+a
5. Kasan Us = 0+8 — 1/26) = —1+E6
1902 6. Taschkent Ux +) = —17 +6, 1120) = —172 + &
7. Tiflis u, = ote —1/2(1) = + 17/2 +6
1905 8. Taschkent „+l)= — 2+8% 2/3 (1) — 1/8 (2) — 1/3(8) = +6+e&
9. Orenburg u + @) = —13+ 6 — 1/3 (1) + 2/8 2) — 1/86) = —5 + &
10. Samara u+(3)=— 9+.4, SU) 130) HAB) str ie

18"

if

if

ewe rey

 
