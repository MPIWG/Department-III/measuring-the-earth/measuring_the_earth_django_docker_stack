 

82
Zur Ableitung der mittleren Anderung À eines Pendels während der Kampagne
eignen sich am besten die Stationsmittel P seiner Schwingungsdauern, die man mit den
Gesamtmitten M—=-—[P] vergleichen kann. Um die Unterschiede P—M für verschiedene
Stationen streng vergleichbar zu machen, muß man sie auf eine bestimmte Schwerkraft,
z. B. auf g (Potsdam), reduzieren. Bezeichnet man daher mit P, — Mo die analogen
Größen für Potsdam, so folgt aus der bekannten Gleichung: 9 P? = const.:

DUMP M. in (ner)
0
oder auch
M-M=rP-PR+ ne er)
0

Im ersten Fall stellt (M — Mo) (Mo — Po)/My die Reduktion von P—M-aut den Pois-
damer Wert P)— ™, dar: im zweiten Fall führt dieselbe Reduktion den Stations-
unterschied P— P, eines bestimmten Pendels auf den Stationsunterschied M — M, des
mittleren Pendels zurück. In der auf S. 98 seiner Publikation gegebenen Tabelle der
Stationsmittel der Schwingungsdauern hat Herr Prof. Hecxer die in Rede stehende Re-
duktion bereits an die P angebracht, und es konnten daher dieser Tabelle die nach-
stehenden, unter sich streng vergleichbaren Werte P-—- M entnommen werden:

 

 

P,—M P,—M P,—M P,—M m

1, Potsdam + 29548 — 30007 + 29304 — 98847 7

2. Odessa 554 001 292 846 7

3. Tiflis 558 O11 306 855 i

4, Bukarest 548 006 310 853 6

5. Potsdam 545 4 002 a 1 9000, op 849 10
Einf. Mittel: + 29551 — 30005 + 29803 — 98850

Durch Vergleichung der Einzelwerte P--M mit den entsprechenden einfachen
Mitteln ergeben sich nachstehende Abweichungen v in 107 sek:

Ds De 07 Ve =
1. Potsdam +3 +2 — 1 — 3 0.143
2, Odessa — 3 — 4 + 11 — À 0.143
3. Tiflis — 7 + 6 — 8 +5 0.143
4. Bukarest nn sbi — 7 +3 0.167
5. Potsdam +6 — 3 — 2 — 1 0.100

aus denen folgt:

[vv] = 422 - 107 sek?, u.

ill yn

 

 
