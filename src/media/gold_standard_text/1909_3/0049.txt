Sony

> mer

 

37

— 10.10-1:sek? an. Dieser Betrag entspricht erfahrungsgemäß den Umständen,
nae denen die Messungen stattfanden und ist jedenfalls nicht zu klein bemessen. Wir
erhalten hiermit für den m. F. Ms des Gesamtmittels der auf jeder Station beobachteten
Schwingungsdauern S den Wert

ME — m° + m° = 14.2. 10714 sek?, Mm _ -393-10.!sek,
oder, in Einheiten von 9 ausgedrückt:

M, - +2. MM +14.]0 em sc.

Ss

Der m. F. My, =Y2:M, der relativen Bestimmung 9» — gr würde somit sein ‘

My, +20 10, omsek.

Aus den Stationsmitteln der Schwingungsdauer
O Oo

S (Potsdam) = 0°5011889 (H= 83m)
S (Budapest) = 0.5012420 CH = 108 m)

folgt mit g (Potsdam) = 981.275 cm sek-? (fir A = 83 m):

1908/09 p 1 H g (Potsd. Syst.)
Budapest (Ref. Stat.) 47°29:5 19°3:6 {108m 980.852 + 0.002 cm sek ”.

Für Budapest liegt eine ältere Bestimmung vor (Bericht 1909, Tab. I, Gruppe 1, A),
die nachstehenden Wert ergeben hat:

p A H g Potsd. Syst.) Jabr Beobachter
Budapest 47° 29:7 19° 4” 122 m 980.844 1893 Krırka
Reduktion auf die Meereshéhe 108 m ......------ + 0.004: = =
Budapest 47 29.7 19 4 108 980.848 (Ber. 1909, 1. c.).

Da diese Bestimmung mit einem Apparate älterer Konstruktion, unter Ver-
wendung eines Chronometers und ohne Berücksichtigung des Mitschwingens ausgeführt
worden ist, so nehmen wir für „gy (Budapest) im Potsd. Syst. “ den neuen Wert von
1908/09 an, dessen Kontrolle durch einen weiteren Anschluß an das Hauptnetz Se. Ex-
zellenz Herr Baron Eörvös bereits in Aussicht gestellt hat.

 
