 

196

Mit Ausnahme von Zürich gehören sämtliche Anschlußstationen dem ausgeglichenen Netze
an. Aus den vorstehenden Beobachtungen ergeben sich für y (Padua) im Potsd. Syst.
zunächst folgende Werte:

Ag(beob.) g(R.St,P.S.) g(Padua,P.S.) Beobachter

1891 Padua — Miinchen [— 0.064] 980.732 [980.668] V. STERNECK
» — Wien, M.-G.I. == 205 860 655 n
wa OÙ — W195 853 658 4
1892 Padua — Wien, Stw. = 02195 980.853 980.658 Lorenzonr
» — Paris — 0.278 943 665 5
1894 Padua — Wien, M.-G. I. [— 0.191] 980.860 [980.669] v. Terurzı
1896/97 Padua — Wien, M.-G.1. [— 0: 197] 980.860 [980.663] Baarione wu, a.
1898 Padua — Ziirich — 0.015 980.673 980.658 Messerscumirr
1900 Padua — München OUT 980.733 280.660 Hai, Birarm
» — Wien, M.-G. I. —- 0.200 860 660 9
De, SW, ==. 19i 853 662 ;
— Straßburg — 0.243 904 661 nn
». — Paris — 0.283 943 660 ie
» — Karlsruhe — 0.307 967 660 ;
1907 Padua — Potsdam — 0.618 980.274 980.656 Anzssıo.

in der Beobachtungsreihe von 1891 ist München mit einem größeren Fehler
behaftet (Bericht 1909, Tab. I, Gruppe 1A); bei den Tsurzı’schen Messungen (1894)
waren die Pendel sehr veränderlich (Bericht 1900, 8. 168, And. = 80- 10-7 sek), und
der Baauione’sche Anschluf von 1896/97 ist nur em einseitiger mit großer Zwischenzeit.
Diese in | ] gesetzten Ergebnisse schließen wir von vornherein aus. Von den übrig-
bleibenden Resultaten können streng genommen nur die Messungen von Ham (1900)
und Atrssio (1907) als vollwertig gelten, da bei allen andern das Mitschwingen nicht
berücksichtigt worden ist. Dieser Mangel fällt hier aber insofern weniger ins Gewicht,
als die Pendelapparate überall sehr solide aufgestellt waren und folglich der Einfluß
des Mitschwingens auf allen Stationen wohl nahezu derselbe war. Stellen wir die obigen
Ergebnisse für die einzelnen Beobachter zu Mittelwerten zusammen, so folgt:

Jahr g (Padua, P.S.) Beobachter

1891 980.656 V. STERNECK
1892 661 Lorenzont
1898 658 Messerscamirr
1900 660 Hai, Biirary

1907 656 ALESSIO.

 

i
|
|

aa

 
