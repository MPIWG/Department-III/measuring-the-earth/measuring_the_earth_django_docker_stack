 

 

 

76
erhaltenen Messungen, von denen jede einzelne über rund 12 Stunden ausgedehnt wurde.
Zeitbestimmungen wurden hier in großer Zahl erhalten, und zwar durch Messung von
Sternazimuten mit dem Universalinstrument in der Nähe des Meridians. Die den Epochen
der Pendelbeobachtungen entsprechenden Uhrgänge hätte man vielleicht etwas strenger
ableiten können, als es Herr Prof. Hassemann getan; doch ist es zweifelhaft, ob der
formale Gewinn an Präzision die aufgewandte Mühe, die recht ‘beträchtlich gewesen
wäre, gelohnt haben würde.

In der folgenden Tabelle habe ich für sämtliche Stationen die zur Beurteilung der
relativen Genauigkeit der Messungen geeigneten Unterschiede der Stationsmittel der
Schwingungsdauern (48) in allen Pendelkombinationen zusammengestellt. Die unter
den 45 der Außenstationen stehenden Zahlen bedeuten ihre Reduktion auf die Schwer-
kraft in Potsdam; nach Anbringung dieser Reduktion sind die / 8 innerhalb einer Pendel-

kombination direkt vergleichbar.

Stationswerte 4S in 1077 sek.

Pendel Nr.: din) a a ei) (26—27)
Potsdam (1901) — 1925 — 23070 —21900 — 21145 — 19975 + 1170
Porto Grande — 1715*) == = = — —

u
Kerguelen — 1935 —— 23147 He za) — 19887 + 1323
+0 + 2 + 2 + 2 2 20
Winterlager (a) — 1880 — 22953 — 21799 — 21073 — 19919 + 1154
1 = =e Bee En el
fi (b) — 1871 — 22979 — 21823 — 21108 — 19952 + 1156
er en eae — 19 ii +1
Potsdam (1904) — 1916 — 23065 — 21904 — 21149 — 19988 —+ 1161

Von den beiden Angaben (a) und (b) für das Winterlager wurde die erste mit
der Pendeluhr, die zweite mit dem Chronometer Narpın erhalten. Die den einzelnen
AS zukommenden Gewichte lassen sich mit Hilfe der folgenden Tabelle, welche die
Beobachtungszahlen für jedes Pendel angibt, leicht berechnen.

Zahl der Beobachtungen.
Pend 1B Pend 141: Pend, 20 Pend. 27

Potsdam (1901) 9 9 1, 9
Porto Grande i 1 — —
Kerguelen fl 1 iL 1
Winterlager (a) 2 2 4 6
" (b) 2 2 4 4

3 3 4 4

Potsdam (1904)

'*) In der eingangs genannten Publikation (8. 356) steht irrtümlich — 1725; siehe die An-

merkung auf 8. 75 dieses Berichts. In y bewirkt dieser Irrtum eine Änderung dg= — 0.002 cm sek ?.

ah sans

hs

mé dt dar Mn A td,

 

 
