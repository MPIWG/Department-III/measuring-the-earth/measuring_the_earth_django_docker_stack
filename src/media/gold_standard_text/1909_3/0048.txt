 

Der m. F. mı des Gesamtmittels aller auf einer Station beobachteten Schwingungs-
lauern würde sich dann aus der Gleichung

u? Dar = ‘ ' E
m? — oe 1071: sek? —= 4.2. 107? sek?

zu &2.1.1077sek ergeben.

Um die mittlere Änderung A eines Pendels während der Messungen aufzusuchen,
bilden wir die Unterschiede 4 zwischen den Stationsmitteln der einzelnen Pendel und
dem Gesamtmittel, und vergleichen die 4 mit ihren Mitteln. Auf diese Weise erhalten
wir aus der Zusammenstellung der Schwingungsdauern folgende Werte:

 

 

 

 

| | |
A 12 | Aus | Asa | Ais | Wy | Uno, Ds | Dia | Vite | Ball ae
| | | | Il
a | De
ges | u wa, ei Oe 24 26 | 05
+ 640 | 22000 070 | — 90 | 2 | +2 | ©) 0 2 28 | 0.5
1642 | —278 | —270 | — 94 | | | | "az | 1.0

Die Z[vo] ist mit w? und 4 durch die Gleichung*)
„ rel il ;
So sur <: SS + 3 (r — 1) 2°

verbunden, worin r die Zahl der Stationen, » die Zahl der Sätze zu je 4 Pendeln auf
jeder Station bedeutet. Wir erhalten somit zur Bestimmung von A? die Gleichung:

47 . 1071: sek? — > pw? 84,

die mit u? — 83.4. 10-1* sek? den Wert 12 —= — 1. 10-14 sek? ergibt, woraus wir schließen,
daß eine Veränderlichkeit der Pendel aus dem vorliegenden Material nicht nachweisbar ist.

Der im vorstehenden ermittelte Fehler mı stellt nur die relative Genauigkeit
der Messungen dar, wie sie sich durch Vergleichung und Kombination der reduzierten
Schwingungsdauern ergibt. Es bleibt noch übrig, den Einfluß der konstanten Fehler, die
den angewandten Werten des Uhrganges, des Mitschwingens, der Pendeltemperatur, der
Pendelkonstanten usw. anhaften, und alle Schwingungsdauern auf einer Station gleich-
mäßig beeinflussen, zu ermitteln. Da die eingangs erwähnte Veröffentlichung hierfür
keine Daten gibt, so nehme ich den gesamten Einfluß dieser Fehler schätzungsweise zu

 

+ 1 c. S. 62.

 

AA nm

 
