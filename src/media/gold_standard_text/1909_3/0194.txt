 

102
Dementsprechend habe ich — ungeachtet einer kleinen Unsicherheit in der Höhen-
angabe — zur Reduktion der Hansxı’schen Beobachtungen auf das Potsd. Syst.
einstweilen nur den Wert für Meudon:

— 0.071 cm sek?

benutzt. Ein genauerer Wert kann erst ermittelt werden, wenn weitere Anschlüsse an
die Haxsxrschen Stationen vorliegen.

C. Messungen mit invariabeln Reversionspendeln des Service géographique.
(Ref, Stat. Paris.)

Unsre Tabelle zerfällt in 2 Teile. Der erste, die Messungen von Bıcourpan und
Corner (1893—99) enthaltende Teil ist dem Bericht 1900, Tab. XIle: Messungen durch
verschiedene Beobachter mit Pendein des Service göographigwe entnommen, wobei jedoch
einige Verbesserungen (Bericht 1903, S. 195 u. 196) berücksichtigt wurden.

Der zweite Teil enthält neue Messungen, die der Direktor der Sternwarte in
Bordeaux, Herr Pıcarr, im Sommer 1909 durch die Herren Escrancon und Sraprer im
Departement de la Gironde ausführen ließ. Die Ergebnisse dieser Arbeiten hat Herr
Escrangon unter dem Titel: Sur l'intensité de la pesanteur et ses anomalies à Bordeaux
ct dans la région in den Comptes rendus 1910, tome 150, $. 189, mitgeteilt. Wir ent-
nehmen daraus folgende Einzelheiten.

Die Messungen sind relative mit der Referenz-Station Paris. Obgleich der An-
schluß an Paris nur einseitig ist, so liegen doch die Epochen der Beobachtungen:

Floirac 1909, Juli 24, Sept. 22, Nov. 4;

Cavignac > PAU 0:

Coutras 2 Aus ls

Langon = 7 Auer 255

Créon „ RE 24

Arcachon ©. Sept lo)

Paris » Now 1971;
so nahe beieinander, daß eine. Veränderung des Pendels — es scheint nur ein Pendel
verwendet zu sein — wohl kaum zu befürchten ist. - Außerdem bieten die Messungen

in Floirac, die jedoch nicht im einzelnen mitgeteilt worden sind, hinlängliche Kontrollen
für die Konstanz des Pendels. In Paris fanden 16, in Floirac im ganzen 24 Bestimmungen
der Schwingungsdauer statt. Der Gang der Koinzidenzuhr wurde durch Vergleichung
mit den Normaluhren der Observatorien ermittelt; fiir die Außenstationen fanden am
Beginn und am Schluß der Messungen telephonische Uhrvergleichungen mit dem Obser-
vatorium in Bordeaux statt. Als Beobachtungsräume dienten meist Keller mit. konstanter

 

|

ad

 
