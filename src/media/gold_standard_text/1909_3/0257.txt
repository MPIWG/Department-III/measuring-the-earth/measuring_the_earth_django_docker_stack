 

245

Gruppe 3. Messungen durch das Observatory zu Ottawa (Brit. N. A.).
(Ref. Stat, Ottawa).

Die y-Messungen der Sternwarte zu Ottawa begannen 1902 mit dem Anschluß
der kanadischen Ref. Station Ottawa an die nordamerikanische Hauptstation Washington,
©. a. G. S. Office, Pendelpfeiler. Dieser Anschluß wurde von den Herren Epwın SMITH
und Dr. Krorz, Observator an der Sternwarte zu Ottawa, ausgeführt und von dem letzt-
genannten in dem Report of the Chief astronomer for the year 1905, Ottawa 1906,
eingehend veröffentlicht. Obgleich nur einseitig angeschlossen wurde, so bieten doch die
nachstehend mitgeteilten Schwingungsdauern für diese und zwei spätere Stationen einige
Sicherheit, daß keine größeren Änderungen der Pendel in der kurzen Zwischenzeit vor-
gekommen sein werden.

 

Washington, 1902, Mai 21 —25.

 

 

Position Pendel 1 Pendel 2 Bendelar | Mittel | MP, M—P, M—P,

| d 0°5014339 0°5015802 025015530 | 075015224 | -+ 885 — 578 — 806

i Te 4344 5780 Hasler as Sale 84 — 562 DS

ii 7 | 4343 5785 Ba >26 |, .2...808 — 569 — 305

I 2... 2 088 5537 | 024 hr Ol — 589 = ole

| Mittel 0.5014337 0.5015795 0.5015530 | O-.5015221 | 858 — 514 — 809
li Ottawa, 1902, Aug. 6—10.

d 025013069 0°5014538 025014270 | 075013959 — 890 © — 519) ol

r 3081 4527 4280 | 3963 ++ 882 — 564 — 817

” 3073 4518 4242 | 3944 + 871 — 574 — 298

da 008 en 2 ee en — 590 za

Mittel | 0.5013073 0.5014531 0.5014959  0.5013954 — 881 ont — 305

Suva, 1903, Jul 12. fe,

 

d | 025018124. 0°5019583 075019276 Ro a — I) — 282 4
i 8122 9562 9338 900% | 4885 — 555 — doll
d 8146 9530 9319 8998 + 852 — 532 == Bill

nee 8116 9518 932100) SOC | 4p SOY — 533 — 336

Mittel 0.5018127 0.5019548 0.5019314 0.5018996 + 869 mo — 98

Doubtless Bay, 5 De, 2 22

 

d 025015042 — 0°5016160 . | 0°5015601 + 559 — — 559
an mM. a
Mittel 0.5015060 = 0.5016204  0.5015632 | + 572 — — 572

i

1k

 
