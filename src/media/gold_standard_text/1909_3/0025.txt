 

à Dy Pa (0, uN [pr] a
(1 ta) Ve Oe va Bi As 2
Py À Po) ke = aa an IL

a (1 =) Das. me

und die ihnen entsprechenden Normalgleichungen:

u (1 = a (1) on QO) 7 (wi = een mA
nn Va [pw]
HE) 4m ft). né Pen

eek) ek © fee a ee tee eo + 0 ee es) 1e eee ele) ones ie ete eee ent

Die Determinanten beider Systeme sind null. Den vorstehenden Ausdrücken
lassen sich leicht folgende praktische Regeln zur Bildung der reduzierten Fehler- und
Normalgleichungen entnehmen:

1. Die reduzierten Fehlergleichungen einer Beobachtungsreihe ergeben sich aus

den ursprünglichen (Tabelle C), indem man diese mit ihren Gewichten zum

E Mittel vereinigt und dieses Mittel von den einzelnen Gleichungen subtrahiert,
F wobei |pe] gleich null zu setzen ist;

m

die entsprechenden Normalgleichungen gehen aus den reduzierten Fehler-
gleichungen durch Multiplikation der letzteren mit ihren Gewichten hervor,
wobei die Produkte pe fortzulassen sind.

Terran

Mit Rücksicht auf den einfachen Zusammenhang der beiden Gleichungssysteme
genügt es, eins derselben aufzustellen. Wir geben in der folgenden Tabelle D die aus
Tabelle C nach den vorstehenden Formeln abgeleiteten reduzierten. Fehlersleichungen.

D. Reduzierte Fehlergleichungen.

 

 

 

 

 

 

 

Reine en
[eee a | 5 6 7 8 9 io ı 0.00
8 gp se 17 0 8 or 4
| pe | Sel Hol: À
a : i a to 1
je le Te 18,198
ea) pee 25 25 2 "a, Soe is | ia
70,10, : a : - :
15 i) eal
| —2/5 | + 3/5 | — 2/5
| |
| | |
—1/2| +1/2]| + 2/5 | — 8/5 | +12/5 | 0 Gl | Par
4 Ae A le A 2 6 À 6 2 la

 

 

 

 

 

 

 

 

 

 

 

 

 
