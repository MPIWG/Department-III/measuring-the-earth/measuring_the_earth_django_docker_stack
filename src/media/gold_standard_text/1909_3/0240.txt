 

228

Gruppe 3. Messungen durch englische Expeditionen.

Messungen der National Antarctic Expedition 1901—1904.
(Ref. Stat. Kew.)

Die Angaben unsrer Tabelle sind dem Werke: National Antarctic Expedition
1901—1904; Physical observations with discussion by various authors, part IL: Pen-
dulum observations by L. C. Bernacom, F. R. G. S. Prepared under the superinten-
dence of the Royal Society, published by the Royal Society, London 1908, entnommen.
Das ziemlich umfangreiche Werk stand mir bei Abfassung dieses Berichts nur ganz kurze
Zeit zur Verfügung, so daB ich mich hier im wesentlichen auf eine kurze Inhaltsangabe
beschränken mußte.

Zur instrumentellen Ausrüstung der Expedition gehörte ein Srückrarn’scher
Dreipendelapparat des South Kensington Museums mit 3 Pendeln (Nr. 37, 38, 39), sowie
eine Halbsekunden-Pendeluhr und ein guter SZ-Chronometer (Kuntzers Nr. 6711), der
auf den Außenstationen als Koinzidenzuhr gedient hat (siehe hierüber auch Bericht 1903,
S. 206). Vor der Abreise der Expedition (1901) fand im National Physical Laboratory in
Kew eine Bestimmung der Pendelkonstanten statt, welche für den Luftdichtekoeffizienten
den Wert 675 X 10-7 sek und fiir den Temperaturkoeffizienten den Wert 46.4 X 10° sek
in Sternzeit ergab.

Die Messungen begannen 1901 in Kew und endeten daselbst 1904; sie wurden
von zwei Mitgliedern der Expedition, den Herren Bernaccnı und SRKELTON, ausgeführt und
erstreckten sich auf 3 Stationen: Melbourne (Australien), Christchurch (Neu-Seeland) und
Winter Quarters auf Ross-Island in der Antarktis (Vietoria-Land).

In Kew wurden an derselben Stelle, wo Mr. Pursan 1900 beobachtet hatte,
nachstehende, ohne Riicksicht auf das Mitschwingen gebildete Anschlußwerte der Pendel
ermittelt:

 

Pendel 36*) Pendel 37 Pendel 38 Pendel 39
Kew 1901 0.5087654 0.5087801 0.5087745 0.5088202
„ 1908 7692 7702 7719 7925
Änderung: =2 = Ao 26 OT.

Auf dem Wege zur Antarktis lief das Expeditionsschiff „Discovery“ zunächst
Melbourne an, wo im dortigen Observatorium 2 Pendel (Nr. 87 u. 39) an zwei Tagen
(1901, Nov. 11—12) beobachtet wurden. Der Gang des Koinzidenzen-Chronometers K.6711
wurde durch Vergleichung mit den Uhren des Observatoriums ermittelt; das Mitschwingen
aber weder hier noch auf den beiden andern Stationen bestimmt.

Auf der zweiten Station, Christehurch (Neu-Seeland), wurden die 3 Pendel an
zwei Tagen (1901, Nov. 26—27) einmal beobachtet, wobei der Pendelapparat in dem
„Absolute Magnetic House“, einem kleinen Gebäude mit beträchtlichen Temperatur-

 

*) Das in Kew mitbeobachtete Pendel Nr. 36 wird im weiteren Verlaufe der Expedition nicht
mehr erwähnt; vielleicht war es als Standard in Kew zurückgeblieben.

 

da a

 

i
|
|
j
