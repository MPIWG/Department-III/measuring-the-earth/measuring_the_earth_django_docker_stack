an

nv ;

TNT

 

Tabelle VII. Schweizerische Messungen.

Gruppe 1. Messungen der schweizerischen Gradmessungs-Kommission
vor dem Jahre 1900.
(Ref. Stat. Zürich.)

Das Material zu dieser Tabelle ist dem Bericht 1900, Tab. XI: Messungen durch
die schweizerische Gradmessungs-Kommission entnommen worden. Einen Teil der dort
aufgeführten Arbeiten, die Beobachtungen von Messerscumirr aus den Jahren 1897
(10 Stationen) und 1898 (8 Stationen), hat Herr Dr. Nırruamver nachträglich einer Neu-
reduktion unterworfen, wobei er die neuesten Werte der Pendelkonstanten (Bericht 1906,
S. 208 u. 213) verwendete, die Änderungen der Pendel strenger berücksichtigte und eine
Anzahl von Rechenfehlern verbesserte. Die Ergebnisse der Neureduktion sind im Proces-
verbal von 1906, 8. 6, mitgeteilt und in unsrer Tabelle berücksichtigt worden.

Für „g(Zürich) im Potsd. Syst.“ habe ich weiterhin (S. 165 u.f.) den Wert

p À H g(Potsd. Syst.)
Zürich 47° 22/7 8°33/2 463m 980.673 cm sek”?

abgeleitet, dem eine mittl. Unsicherheit von etwa = 0.004 cm sek"? anhaftet.

Die frühere Höhenangabe für die Ref. Stat. Zürich: 467 m, ist nach einem
Schreiben des Herrn Prof. Dr. Riacensacn yom 20, Juni 1908 durch die obige Angabe,
die sich auf den neuen schweizerischen Landeshorizont (Pierre du Niton, H — 873.6 m
über dem mittell. Meer) bezieht, zu ersetzen. Nach derselben Quelle ist für den Beob-
achtungsort in Basel (1893) 7 — 269m (statt 267 m) anzunehmen; ob auch die übrigen
an Zürich angeschlossenen schweizerischen Stationen einer Höhenkorrektion bedürfen,
ist noch festzustellen.

Gruppe 2. Messungen der schweizerischen Gradmessungs-Kommission
nach dem Jahre 1900.
(Ref, Stat, Basel.)

Das bis zum Jahre 1906 vorliegende Beobachtungsmaterial ist in den Berichten
von 1903 und 1906, Tab. XI: Messungen durch die schweizerische Gradmessungs-

21

 
