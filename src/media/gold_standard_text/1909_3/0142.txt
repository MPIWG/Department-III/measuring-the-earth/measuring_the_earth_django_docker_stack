 

130

Gruppe 4. Messungen der Kaiser]. Universitäts-Sternwarte zu Kasan.
(Ref,-Stat. Kasan, altes Observ.)

Zur Herstellung unsrer Tabelle haben wir folgende Publikationen und hand-
schriftliche Mitteilungen benutzt:

1) Bericht 1900, Tab. Xi: Messungen durch die Herren Prof. Dusraco und
Prof. Krassnow im Anschluß am Kasan, enthaltend die Arbeiten von 1896,
10 Stationen;

2) Berichte 1903 und 1906, Tab. Xi: Messungen der Kaiserl. Universitäts-
Sternwarte in Kasan, enthaltend die Ergebnisse der Arbeiten aus den Jahren
1899, 1900, 1902 und 1903 in vorläufiger (Bericht 1903) und definitiver Form
(Bericht 1906), im ganzen 21 Stationen;

3) eine tabellarische Zusammenstellung der Ergebnisse aus den Arbeiten von
1907 und 1909 (zusammen 16 Stationen), die uns der Direktor der Kasaner
Sternwarte, Seine Exzellenz Herr Prof. Dr. Dunraco, mittels Schreibens vom
24. Mai 1910 übersandt hat;

4) eine tabellarische Zusammenstellung der unter 3) aufgeführten Arbeiten von
Seiner Exzellenz dem Herrn Generalleutnant von Srusenvorrr vom ©. J uli 1910:

Die unter 2) genannten Arbeiten sind inzwischen in russischer Sprache unter
dem Titel: Bestimmungen der Intensität der Schwerkraft im Ural und an der Wolga
in den Jahren 1899, 1900, 1902 und 1903 von W. A. Baranow, Kasan 1907, publiziert.
Einige Abweichungen, die sich zwischen den Angaben dieser Publikation und den auf
Mitteilungen des Herrn Generals von Srupenporrr beruhenden Angaben des Berichts 1906
noch zeigten, wurden berücksichtigt. So erhielten die geogr. Längen der Stationen
durchweg eine Korrektion von — 0:1; die geogr. Breiten von Werchni-Kyschtym und
Perm wurden um —0/1 und + 0:2 und die Meereshöhen von Kamyschlow und Kuschwa
auf Grund einer Mitteilung des Herrn von Srusenporrr vom 29. April 1909 um +12
und 3m geändert. Die Mitteilungen 3) und 4) stehen bis auf die geogr. Länge
von Rybinsk, für die beide Autoren zwei um 10’ differierende Angaben machen, im
Einklang; ich habe den in unsre Tabelle aufgenommenen Wert A (Rybinsk) vorläufig mit
einem ? versehen.

Die neuen Stationen von 1907 und 1909 gehören dem Wolgagebiete an; sie

wurden von den Herren Dusiaco und Baranow mit dem im Bericht 1903, 8. 177, be- .

schriebenen Scunerper’schen Pendelapparate bearbeitet.*) Im Jahre 1908 wurde mit dem-
selben Apparat durch die Herren Baraxow (Kasan) und Prof. Haasemann (Potsdam) auch

*) Während der Drucklegung unsers Berichts sind die Resultate unter dem Titel: Arbeiten
des astron. Observ. der Kaiserl. Universität Kasan, No. XXII; Bestimmungen der Schwerkraft längs
der Flüsse Wolga, Kama und Wjatka etc. in den Jahren 1907 und 1909, Kasan 1910, in russischer
Sprache erschienen.

at death à A ada iid

 

 
