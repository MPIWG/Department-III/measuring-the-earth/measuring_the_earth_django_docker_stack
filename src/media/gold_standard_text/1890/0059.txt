 

LE
F
|
|
|

Te le

ml] AR

i

im
5
73
=
=
=
iz

 

PREMIERE SEANCE

Lundi 15 septembre 1890.

La séance est ouverte à 2 heures 40 minutes.
Sont présents :
I. Les membres de la Commission permanente :

lo S. E. M. le général Marquis de Mulhacen, de Madrid, President.

90 M. le professeur Ad. Hirsch, Directeur de l'Observatoire de Neuchâtel, Secrélaire
perpétuel. |

30 M. le professeur R. Helmert, Directeur de l'Institut géodésique de Prusse et
Directeur du Bureau central de l'Association géodésique internationale, àBerlin.

4oM. H.-G. van de Sande-Bakhuyzen, Directeur de l'Observatoire de Leyde.

50 M. Faye, membre de l’Institut et Président du Bureau des Longitudes, a Paris.

Go M. le général À. Ferrero, Directeur de l’Institut géographique militaire de Florence.

7° M. le professeur W. Foerster, Directeur de l'Observatoire de Berlin.

8° M. le capitaine de vaisseau von Kalmar, Directeur des triangulations a l'Institut

I.-R. géographique militaire de Vienne.
9o M. le colonel von Zacharide, Directeur du Service géodésique, 4 Aarhus.

Il. Les délégués :

10 M. le professeur Albrecht, chef de section à l’Institut géodésique royal, à Berlin.

90M. Antonio José d’Avila, major à l'État-major, pair du royaume de Portugal, à
Lisbonne.

30 M. le commandant Bassot, chef de la section géodésique du Service géographi-
que de l'Armée, à Paris.

49 M. Bouquet de la Grye, membre de l’Institut, à Paris.

50 M. Constantin Carusso, à Athènes.

60 M. le commandant G. Defforges, chef de bataillon, du Service géographique de

l'armée, à Paris.
To M. Haid, professeur à l'École polytechnique de Karlsruhe.
