 

 

betheiligten Staaten nur etwa noch ein oder zwei mit ihrer Beitragszahlung im Rück-
stande sind,

Mit Bezug auf die Veroffentlichungen der Permanenten Commission hat der Secretar
zu erwähnen, dass der ziemlich umfangreiche Band der Verhandlungen der Pariser General-
eonferenz mil seinen zahlreichen Beilagen von zwölf Spezialberichten, siebenzehn Landesbe-
richten und zwölf verschiedenen Karten und Tafeln, bereits im Frühling dieses Jahres hat
erscheinen können. Derselbe wurde am 24. Mai 1890 an die Gesandtschaften der Erdmes-
sungs-Staalen in Berlin zur Vertheilung gebracht und ungefähr zur gleichen Zeit an die
Commissäre der Gradmessung in den verschiedenen Ländern versandt. Der Secretär ist selbst-
verständlich bereit, jede etwa gewünschte Auskunft über einzelne Punkte dieser Veröffent-
lichung zu geben, sowie alllällige Bemerkungen und Wünsche zur Berücksichtigung für dic
künftigen Veröffentlichungen dieser Art enlgegenzunehmen.

Die Commission erinnert sich, dass sie von der Generalconferenz im vorigen Jahre
beauftragt worden ist, den hohen Regierungen der Erdmessung die Vermehrung der Mit-
eliederzahl der Permanenten Commission um ein Mitglied, zur Erwägung zu unterbreiten.
Nachdem das Bureau sich durch confidentielle Schritte bei den maassgebenden Persönlich-
keiten der verschiedenen Länder überzeugt hatte, dass ein solcher Antrag im Allgemeinen
eine günstige Aufnahme erwarten dürfte, hat dasselbe sich des Auftrages durch eine Cireular-
Depesche vom 20. Juni an dic Gesandten der betheiligten Staaten entledigt. |

Obwohl diese Depesche seiner Zeit den Mitgliedern der Commission zar Kenntniss
vebracht worden ist, hält der Secretär es für angezeigt, dieselbe im Protocoll der jetzigen
Vereinigung zu veröffentlichen. Dieselbe lautet

INTERNATIONALE ERDMESSUNG. Nizza und Neuchätel, 20. Juni 1890.
« Eurer Excellenz

« Haben wir die Ehre gehabt kürzlich die Verhandlungen der neunten Generaleonferenz
der internationalen Erdmessung zustellen zu lassen. Ihre Hohe Regierung wird daraus ersehen
haben, dass die Conferenz, gemäss den Artikeln 5 und 11 der « Uebereinkunit vom October
1886 », sowie in Folge des Artikels 3 des Reglements der Permanenten Commission, die
vorschriftsmässige Erneuerung der Mitglieder dieser letzteren vorgenommen hat.

« Bei der Wahl der neuen Mitglieder ist der Fall eingetreten, dass trotz wiederholter
Abstimmung, für die fünfte freie Stelle zwei Gommissäre die gleiche Anzahl Stimmen erhalten
haben. In dieser Lage, und in Erwägung dass der Eintritt von fünf nenen Staaten in die Erd-
messung eine Vermehrung der Mitgliederzahl der Permanenten Commission rechtfertigen
dürfte, hat die Conferenz beschlossen, die oben erwähnten zwei Gommissare als gewählt zu
betrachten, unter der Bedingung, dass die hohen Regierungen ihre Zusimmung zu einer
Abänderung des Artikels 5 der Uebereinkunft in dem Sinne ertheilen würden, die Mitolieder-
zahl der Permanenten Commission von elf auf zwölf zu erhöhen.

«Der unterzeichnete Vorstand der Commission, welcher mit den dazu nothigen

u san Um a A aE a

Peery

Wore niente

Yer tii

Wek Lun)

sauna in Malan ay a

in

A

 

 
