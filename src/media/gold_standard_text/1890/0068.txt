 

 

62

celle série revêt une très grande importance, attendu qu’elle enlève aux résultats de Berlin
et de Postdam leur caractère local. Je ne doute pas que la Commission permanente n’appré-
cie à leur juste valeur les efforts de l'Observatoire de Prague.

Afin d'apprendre s'il existait peut-être dans d’autres Observatoires et pour la même
période de temps des matériaux pouvant fournir un appoint à nos recherches, j'ai inséré clans
lo no 9963 des « Astr. Nachr. » une courte notice sur les variations observées des latitudes,
et j'y ai ajouté la prière de publier ces données ou ce les envoyer au Bureau central. Je me
suis adressé dans le même but, par écrit, à quelques astronomes.

On a répondu de différents côtés à ma demande de la manière la plus obligeante.
MM. Nyrén, Herz et Porro ont publié leurs observations ; j'ai reçu en outre des communica-
lions écrites de MM. Gillet Nobile. Toutefois, l’ensemble des données résultant de ces infor-
mations est trop faible pour qu’on puisse en tirer un appoint important à l'étude de la ques-
Lion qui nous occupe.

La plus complète de ces communications est celle que M. Herz à publiée et qui
comprend une série de distances zénithales de la polaire, qu’il a observées A Ottakring pres
de Vienne.

Si l’on possédait de pareilles séries provenant de plusieurs Observatoires, elles au-
raient de l’importance ; mais isolée, le poids de cette série est trop faible, car si l’on forme
de ces mesures deux groupes, embrassant, l’un la période allant du 7 juillet au 2 septembre,
l'autre l'intervalle compris entre le 11 janvier et le 15 mars, périodes qui, d’après les ob-
servations de Berlin, Postdam et Prague, comprennent les valeurs maxima et minima de la
latitude, on obtient comme différence des latitudes de la première et de la deuxieme période :

0,06 + 0,40 (erreur moyenne)

L’incertitude probable est donc de 0 3 environ, de sorte que la différence pourrait
être de plusieurs dixièmes de seconde au lieu de 0.06.

Ensuite, les mesures fournies par MM. Nyrén, Porro et Gill ne contredisent pas non
plus la diminution de latitude constatée par les trois Observaloires, de juillet 1889 à janvier
1890. M. Nobile communique des mesures faites dans les années 1886 à 1888 et en conclut
que la latitude de Naples a aussi varié dans cel intervalle.

M. le Dr Küstner a démontré récemment (voir le n° 2993 des « Astron. Nachr. »)
une variabilité de la latitude identique à Poulkowa et à Berlin, dans les années 1 884-1885,
s’élevant 4 0-4.

J'espère ne pas rencontrer de contradiction en affirmant que les résultats récents
obtenus pour la question de la variabilité des latitudes nous obligent à continuer énergique -
nent les observations dans ce domaine. Comme particularité de la méthode suivie dans les
trois Observatoires, on doit remarquer que la latitude calculée dépend dans une forte mesure
des réductions des positions moyennes aux positions apparentes des étoiles. Si l'on veut
conserver celte méthode, ce qui est désirable en raison de sa grande précision, 1l faudra
bien revenir aux propositions de MM. Fergola et Foerster. Gar, dans la supposition que la

ill

syne id cam WIRT MT ER 1. di

bab ny

Wk ana ih

jam ha a Lau ua à

i

 

 
