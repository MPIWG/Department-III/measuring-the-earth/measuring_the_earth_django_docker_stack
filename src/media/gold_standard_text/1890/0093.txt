 

A THT HA

AE AR DRM | HU

|

aah

We |) CRE YT RTPA werent Foe (ili!

87
sur les récents progres faits dans les observations de la Lune et leur utilisation dans l'intérêt
des déterminations géodésiques. M. Foerster montre, en outre, un petit appareil, imaginé
par M.le D Wellmann, au moyen duquel des lignes tracées sur un micrometre en verre
apparaissent, avec un éclairage par le bord latéral, sous forme de lignes claires dans le
champ de la lunette. (Voir Annexe B. IX%.)

Ensuite, M. le professeur Helmert lit un rapport sur les travaux de P’Institut geo-
désique. Ce rapport rend compte des travaux de campagne et de bureau, exécutés par
l'institut en 1889/90. Ils comprennent entre autres des mesures de latitude et d’azimut en
trois points de premier ordre et plusieurs déterminations de longitude. Ils concernent
aussi des recherches sur l'influence de la torsion de l’axe horizontal des instruments uni-
versels sur les mesures des distances zénithales. (Voir Annexe B. IX».) :

Enfin, M. le colonel Morsbuch communique le rapport de la section trigonométrique
de la « Landesaufnahme » prussienne sur les travaux de 1890. D’après ce rapport, les me-
sures ont été continuées dans la chaine rhéno-hessoise; on à organisé les stations dans le
réseau de la base de Bonn et établi les signaux sur le réseau rhénan inférieur. On à muni
complétement de repères et de marques de contrôle les lignes de nivellement dans le Schles-
wig-Holstein, et on a commencé le même travail dans le Meklembourg; enfin, on à exécuté,
le long de la Warthe, un nivellement de précision d’une longueur totale de 375 kilometres.
(Voir Annexe B. IXC.)

M. le Président remercie les trois délégués de la Prusse pour leurs intéressantes
communications et prie M. le Secrétaire de donner lecture du rapport de M. le général
Stebnitzki sur les travaux russes exécutés en 1889.

Il résulte de ce rapport, en ce qui concerne d’abord les observations astronomiques,
que les cinq déterminations de longitude exécutées en 1887 et 1888 permettent la clôture de
deux polygones, dont le premier : Kiew-Moscou-Kazan-Ekaterinbourg-Orenbourg-Saratow-
Astrakhan-Rostow-Kiew, se ferme avec une erreur de + 0178, et le second polygone :
Tiflis-Rostow-Astrakhan-Bakou-Tiflis, se ferme avec une erreur de — 0°08.

En 1889, on a mesuré en Crimée les deux différences de longitude Jalta-Balaklawa
el Theodosia-Balaklawa.

Dans les années 1887 et 1888, on a déterminé, par des observations au 1° ver-
tical, les latitudes d’Orenbourg, Saratow, Bakou, Astrakhan, Rostow, Kowel, et dans l’an-
née 1889 la latitude des deux points extrémes de la base de Moloskowitzy. La différence
astronomique de ces deux stations est de 436,89 et la différence géodésique, calculée avec
les éléments de Clarke, de 43672, donc accord dans la limite des erreurs.

On a de même mesuré des azimuts pour ces deux points de la base.

Quant aux triangulations de premier ordre, on mentionne la jonction de Wilna à la
chaine de Marianpol au moyen de 14 triangles.

Les nivellements de précision ont été exécutés le long de deux lignes de chemin de
fer, sur une étendue totale de 660 kilomètres.

 
