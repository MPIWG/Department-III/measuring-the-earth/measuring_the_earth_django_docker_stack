 

167

Outre les difficultés qui ont retardé le commencement de ce travail, projeté depuis
très longtemps, l'installation par l’Institut d’une ligne télégraphique entre Baza el le sommet
de la Sierra de los Filabres, où se trouve Tetica, absorba un temps considérable, non seule-
ment à cause des rigueurs de la saison, mais aussi à cause des difficultés qu'on ne pouvail

manquer de rencontrer sur un parcours de 90 kilomètres à travers des montagnes.

| L'installation ne fut done achevée que vers la fin d'octobre ; les premières observa-
tions combinées entre les deux stations eurent lieu le 31 octobre et elles ont été terminées
le 23 novembre. Pendant ce temps on ne put profiter que de huit soirées complètes de
travail. Le séjour à Tetica fut très pénible, car la neige commença à tomber et couvril
bientôt la montagne, et le froid rendit le séjour impossible dans ces parages; le thermomètre
descendit à 8° au-dessous de zéro et, à Madrid il fut aussi constamment au-dessous de zéro.
Dans ces conditions, il ne fut point possible de faire l'échange des observateurs afin de
répéter l'opération et d'éliminer du résultat l'erreur de l'équation personnelle; celle-ci fut
déterminée à Madrid par des observations spéciales avant et après les observations de
longitude proprement dites.

Le nombre d’étoiles observées, tant à Madrid qu'à Tetica, fut en moyenne de quarante
par soirée, divisées en quatre séries, avec deux retournements de la lunette, et le nombre
de séries de signaux télégraphiques échangés pour la comparaison des pendules fut de deux,
: l’une entre la première et la deuxième série, l'autre entre la troisième et la quatrième.

Les calculs n’ont pu être activement entrepris jusqu’au mois de novembre 1890 ;
mais à l'heure présente ils sont suffisamment avancés pour que l'on puisse affirmer qu'ils
seront bientôt achevés et que l’on commencera impression des Mémoires correspondants,
au courant de l'été prochain.

Madrid, janvier 1891.

Talal IH TEE ee en

Le Directeur general,
F. de P. ARRILLAGA.

UNNA Ih HN

I ARD

TUE JURA EL OL LU

mp)

 
