 

 

 

SO
Cette concordance dans les résultats de recherches poursuivies d’une façon indépen-
dante permet peut-être d’inférer que la loi est générale et que les détails du relief dans les
massifs montagneux sont pour peu de chose dans les anomalies de la pesanteur, les causes
de ces anomalies devant être recherchées plutôt dans des variations de la densité au-dessous
el au voisinage de la surface de niveau zéro.

M. le professeur Helmert a la parole pour proposer à l’Assemblée, conformément à
son rapport sur les mesures de pesanteur, lu dans la dernière séance, la résolution sui-
Nate

«La Commission permanente, en reconnaissant limportance des observations de
pendule de M. de Sterneck en Tyrol, exprime le désir que ces observations soient continuées
vers le Sud jusqu'à Padoue, et vers le Nord jusqu'à Munich, et autant que possible par
M. de Sterneck lui-même et avec ses instruments. »

M. Hirsch croit que la proposition de M. Helmert se justifie d'elle-même au point
de vue scientifique. Pour l’appuyer, il se borne à faire observer qu’une pareille initiative de
la Commission permanente, par laquelle on faciliterait la continuation dans les pays voisins
de certains travaux géodésiques importants commencés dans l'un des Etats de Association,
est parfaitement conforme à esprit et a la lettre de la Convention géodésique.

La proposition de M. Helmert est ensuile adoptée a Punanimite.

M. le Président, à qui on a demandé de divers côtés de fixer la date et l'heure
de la prochaine séance au début de celle-ci, désire consulter l’Assemblée sur les deux
propositions qui lui sont parvenues, savoir s'il faut siéger dimanche à partir de 1 heure,
ou bien s’il est préférable de renvoyer cette séance au lundi à 10 heures. Il met aux voix
cette alternative, sur laquelle tous les membres présents à l’Assemblée ont le droit d'émettre
leur vote; 14 voix s’étant prononcées en faveur de la séance du dimanche et 10 voix pour celle
de lundi, M. le Président fixe la prochaine séance au dimanche 21 septembre, 4 1 heure.
ll croit, sans en être complétement sûr, que tous les objets encore à l’ordre du jour pour-
ront être liquidés dans cette séance, qui serait ainsi la dernière de la session. Il réserve
cependant ce point à l’Assemblée elle-même qui en décidera suivant les circonstances.

M. le Président donne la parole à M. von Kalmür pour lire son rapport spécial sur
les nivellements de précision exécutés en Europe pendant l’année dernière. Ce rapport sera
publié in-extenso comme Annexe aux Comptes-rendus. (Voir Annexe À.)

Il en résulte entre autres le fait réjouissant que le réseau des nivellements de l’Eu-
rope avait déja atteint, en octobre 1889, un développement de 118000 km. et qu’en
moyenne des dernières années Paccroissement annuel peut être évalué à 6 600 km. environ.

Le rapport de M. von Kalmar contient en outre d’intéressants résultats concernant
des recherches faites en Autriche-Hongrie et en Allemagne sur la longueur des mires et leur
variabilité. Le rapporteur exprime le vœu, partagé par la Commission permanente, que les
données qui existent sur ce mème sujet dans d’autres pays lui soient communiquées le plus

tôt possible.

Al u

2 a RUE MT TC 1

Lib a il

a ee

mama hs autel nn duo

 

 
