 

Annexe B. III ¢.

Note sur les travaux du Service du

NIVELLEMENT GÉNÉRAL DE LA FRANCE
en 1590

Pır M. (Cu. LALLEMAND

. ZONIVELLEMBNTS DE PRECISION

Pendant la campagne de 1890, on a prolongé le long de la Manche, comme le montre
la carte ci-annexée, la chaîne de polygones qui, dès 1887, avait été poussée de Marseille vers
l'Océan, en vue d'atteindre le plus tôt possible les marégraphes et médimarémètres établis sur
ce littoral et d'obtenir ainsi une exacte détermination de la difference de niveau entre les
deux mers. Cette seconde chaine se trouve maintenant reliée par le Nord à la premiere
chaîne méridienne qui s’étend de la Méditerranée à la frontière belge.

En outre de ce travail, on a exécuté deux nouveaux rattachements avec le réseau
italien : l’un au Mont-Genévre, près de Briançon, l’autre à travers la galerie du Fréjus, sur
le chemin de fer de Modane à Turin. Le nivellement du tunnel du Mont-Cenis, long de 13°"7,
a élé exécuté avec un plein succés, moitié par l’Institut géographique militaire de Florence,
moitié par le Service du Nivellement général de la France. (est la première fois, il n'est
peut-être pas sans intérêt de le faire remarquer, qu'on exécute une opération oéodésique
souterraine aussi importante.

L'ensemble de ces lignes nouvelles forme un développement total de 1550 km. qui,
ajoutés aux 7890 km. antérieurement nivelés, portent à 9220 km., représentant les lrois
quarts du nouveau Réseau fondamental, la portion actuellement terminée de ce Réseau.

L'hypothèse que nous avions émise dans notre précédent rapport, touchant l'égalité
probable de niveau de la Méditerranée et de l'Océan, a trouvé cette année une nouvelle
confirmation dans ce fait que les cotes du niveau moyen a Cherbourg et au Havre, par rapport

A Marseille, se sont trouvées respectivement ramenées A 0"09 et a O"04, au lieu de 0"90 et

"36, qui étaient les altitudes anciennement admises.

(idl lh

are ra A IE |

Lib: nk

bt al

iu sumo pd Mla laa fey

 

 
