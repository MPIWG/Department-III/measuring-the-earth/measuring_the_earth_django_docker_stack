 

98

M. le professeur Færster, pour le cas où le projet de se réunir à Florence n’abou-
tirait pas, désire recommander Varsovie comme lieu de la prochaine session.

Finalement, la Commission permanente charge son Bureau de suivre à l'affaire EL
en tenant compte des renseignements officieux qu’il obtiendra sur les dispositions éventuelles
des gouvernements, de fixer en temps utile le lieu et l'époque de la re Sunion de 1891.

Le Président, M. le marquis de Mulhacen exprime, au nom de la Commission per-
manente, sa profonde reconnaissance au gouvernement grand-ducal, aux autorités de la
ville et de l’université de Fribourg, el en particulier à M. le professeur Lüroth et à M. le
premier bourgmestre Winterer, pour Paimable accueil que l'Association géodésique interna-
tionale a recu dans cette agréable ville.

Il adresse de même les plus vifs remerciements de la Commission permanente à la
Société du Musée et à son Comité pour la généreuse hospitalité avec laquelle ils ont mis leurs
splendides locaux à la disposition de l'Assemblée.

Le Président déclare ensuite close la session actuelle de la Commission permanente.

La séance est levée à 6 heures.

 

UE

2 M M À

Lil: WAM th 1

Nato ill)

 
