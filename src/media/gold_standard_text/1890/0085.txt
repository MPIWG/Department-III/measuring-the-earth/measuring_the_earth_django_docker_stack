 

eq |

vi

STA

WAMU AWE

[UVF fied Wi it

  
 

im
m
\®
=
i=

TROISIEME SEANGE

Vendredi 19 septembre 1890.

La séance est ouverte à 2 1/, heures.

€

Président : M. le général Marquis de Mulhacén.
Sont présents :

|. Les membres de la Commission permanente: MM. Bakhuyzen, Faye, Forster,
Helmert, Hirsch, von Kalmar et von Zacharvae.

Il. Les délégués: MM. Albrecht, d’ Avila. Bassot, Bouquet de la Grye, Carusso,
Defforges, Haid, Hennequin, Lallemand, Morsbach, Nell, Rümker, Schols et Tisserand.

III. Les invitöes: MM. Lüroth, Jordan, Koch, Meyer, Schneyder, Steinmann et Stie-
kelberger.

M. le Président donne la parole au Secrétaire pour lire le procés-verbal de la
deuxième séance. Ce procès-verbal, resume ensuite en langue française, est adopté à Vuna-
nimité après une courte observation de M. Lallemand, d’après laquelle la notice qu'il a lue
dans la dernière séance n’est pas la même que celle qui à paru dans la « Revue scientifique »
sous le titre: Le niveau des mers en Europe et l'unification des altitudes, et qui a été dis-
tribuée aux membres de l’Assemblée.

M. Defjorges, à propos du procès-verbal et du rapport de M. Helmert sur la pesan-
teur, demande a la Conférence la permission d’insister sur la concordance des résultats
obtenus séparément, par M. de Sterneck, dans le Tyrol, par M. Erasmus Preston, du Coast
Survey, aux îles Hawaï, et par lui-même dans les Alpes maritimes.

A peu près à la même époque, ces trois observateurs, avec des instruments diffé-
rents et des méthodes différentes, ont abordé le même problème en Autriche, en Océanie et
en France. Ils ont obtenu un résultat identique. Ils ont tous trois constaté que la formule
de Bouguer, en v introduisant la densité géologique moyenne de la montagne, dans les
régions où ils ont observé, représente d’une manière assez satisfaisante la variation de la
gravité avec l'altitude.

 
