 

oraduées du vide à la pression d’une atmosphère, nous avons obtenu, avec les pendules de
Brunner du Service géographique, des résultats inattendus qui nous ont paru dignes d’être
communiqués à l’ Association.

Le calcul assez long, dont nous venons d’esquisser la marche, a été exécuté par
approximations successives. Nous allons le résumer brièvement pour en donner une idée
aussi précise que possible.

Nous prendrons comme unités la minute sexagésimale d’are, la minute de temps et
le millimètre de mercure.

A la station de Rosendaël (1888), on a observé les décroissements suivants . (Pendule
long de Brunner, Pi Bb)

Pression Aa t Amplitude initiale
240 mm. 234 en 420 min. 200
385 » 10,3 en 164 » 15,3
766 » ig en 199) 26,4

)

Ces observations ont été faites à dessein à de faibles amplitudes pour atténuer autant
que possible l'effet du rayon de courbure; nos premières études à Breteuil nous ayant montré
que cet effet, pour nos couteaux, est presque négligeable aux faibles amplitudes.

Posant

d x r
aU A

a.

 

c’est-à-dire négligeant pour une première approximation le premier et le troisième terme de
la formulé complète, on à :

r lon à == loos
— lore= SEROMA aS
2 l

h

 

 

On trouve ainsi :

r log e = 0.00226 a la pression 210mm.
l

D 000207 » 389

ye 0.0000 ) 766

Ces nombres croissent comme la racine carrée de la pression correspondante.

C’est un premier résultat de l'expérience; dans la loi du déeroissement de amplitude,
le coefficient du deuxième terme, celui qui exprime leffet de la résistance de Pair considérée
comme proportionnelle à la vitesse, est lui-même proportionnel à la racine carrée de la
pression.

 

qu

et RM ME a

ab nnd

 

 
