 

|

el

AM eee

LT im

DE DOI WOE) TTF irn

im

90

Bestimmung Paris-Mailand wird im XIV. Bande des Memorial erscheinen. (S. Beilage b. I11®.)

Herr Bouquet de la Grye bervichtet, dass das hydrographische Bureau im Jahre [890
auf Corsika ein kleines Dreiecksnetz zur Verbindung des Nord-Kaps mit der die Insel
umkreisenden Kette vollendet hat. Herr Hatt, welcher mit dieser Aufgabe betraut war, hat
im letzten Frühling auf der Nordseite der Insel auch ein Azimut gemessen.

In Madagascar setzen die Herren Mion und Fichot die Triangulation und die Auf-
nahme der Westküste fort. Ferner ist an der Mündung und dem äusseren Theile des Gabon
ebenfalls eine Dreiecksmessung ausgeführt. Alle diese Arbeiten sind fast vollständig berech-
net. Die Triangulation der Nordküste von Tunis, welche von den Ingenieuren des hydrogra-
phischen Amtes ausgeführt ist, befindet sich unter der Presse.

Herr Lallemand verliest einen Bericht über die im Jahre 1890 vom französischen
Generalnivellement ausgeführten Arbeiten. Man hat 1560 km Doppellinien gemessen, und
von den 12300 km des neuen Netzes erster Ordnung sind 9500 km, also ungefähr drei
Viertel gegenwärtig vollendet. Die mittleren Meereshöhen am Canal in Cherbourg und Havre,
welche nach dem alten Nivellement von Bourdaloué um 90 und resp. 36 cm über dem Niveau
des Mittelmeeres in Marseille lagen, sind nunmehr nach den neueren Messungen auf 9 und
resp. 4 cm herabgegangen. Drei neue Médimarémétres sind seither in La Pallice (bei La
Rochelle), am Ocean und in Cherbourg und Havre am Canal aufgestellt worden, wodurch die
Anzahl dieser Instrumente an den französischen Küsten auf 14 gestiegen ist. (Siehe Beilage
Bi lites)

Herr Carusso verliest einen Bericht des k. k. Oberstlieutenant Hart! über die unter
seiner Leitung in den Jahren 1889 und 1890 in Griechenland ausgeführten Erdmessungs-
Arbeiten. (Siehe Beilage B. IV.)

Herr General Ferrero erstattet seinen Bericht über die geodätischen und.astronomi-
schen Arbeiten der italienischen Commission. Zum leichteren Verständniss desselben vertheilt
General Ferrero eine Anzahl Karten und Netze. Er lenkt die Aufmerksamkeit seiner Collegen
besonders auf das Projekt einer trigonometrischen Verbindung zwischen Sicilien und der
Insel Malta, und vertheilt auch hierfür das in Aussicht genommene Netz.

Der Berichterstatter hält es für seine Pflicht, der Versammlung anzukündigen, dass
nunmehr fast sämmtliche Erdmessungs-Staaten auf sein Circular geantwortet haben, worin
er um Mittheilung der Schlussfehler der Dreiecke erster Ordnung gebeten hatte. Diese
gesamte Statistik bestätigt vollkommen die in der Generalconferenz letztes Jahr mitgetheil-
ten Schlussfolgerungen.

Herr Ferrero legt ferner eine gewisse Anzahl wissenschaftlicher und kartographi-
scher Werke vor, welche von dem militär-geographischen Institut in Florenz publicirt sind,
und welche er für die Bibliothek der Universität Freiburg bestimmt, deren Professoren die
Gradmessungsversammlung so freundlich aufgenommen haben.

Endlich bedauert der General Ferrero seinen Bericht mit der Erwähnung des Hin-
scheides seines berühmten Collegen Respighi zu schliessen. Die Geodäten sämmtlicher

 
