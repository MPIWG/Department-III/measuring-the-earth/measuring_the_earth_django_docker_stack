 

 

 

 

 

 

 

 

 

 

 

 

178
PLB PL
et TS ua a
Aby 1 ASy 1
ee Fey a Te J Per
Aut ve h J Ons AY h Ks
D'ailleurs, si on a observé les durées en même temps que le décroissement de
’amplitude :
PLB PLH
2 Iv 0 3 19 Tr? i
Tes À (1 a Top = N (1 ue i
q h g h
—2 ea, 19 rn? On,
Té a iv ) 4 By I, = Ta (1 Fe dite
En q | ß y 0 h'
Fo PUR ®, (2 rn? 0.
TS a Fu À ik = Ty5 fe ir (1 a nn
q I i h

Soit douze équations pour huit inconnues, en y comprenant fet g. Elles sont done
toutes déterminées.

Si f est connu (c’est le coefficient de roulement agate sur agate), on voit que l’obser-
vation du décroissement de l'amplitude fournira à l’observation de la durée, série par série,

. « By .
la correction due à la courbure moyenne du couteau, la valeur de “is Jour chacune des six
h

dernières équations venant de l’équation correspondante en f.

Nous nous proposons, dans les réductions de l’ensemble de nos observations de
pendule, de corriger ainsi loutes les durées observées de la courbure, après les avoir d’abord
corrigées de l’éffet de l'air et du support. Il ne restera plus alors qu’à fixer, par la différence
des durées d’oscillation P LB et P LH, l'erreur de la position du centre d’oscillation pour
pouvoir ramener toutes les observations à un pendule idéal de longueur donnée.

La constance que cette différence doit présenter à toutes les stations sera à la fois un
criterium de la stabilité du pendule et une garantie de la valeur des mesures.

Il ya lieu maintenant, ayant fixé la loi du décroissement de amplitude, de se pre-
occuper de la réduction à l'arc infiniment petit. -

Les formules de Borda et de Pasevi, généralement employées, supposent que la
courbe de l'amplitude considérée comme fonction du temps est une exponentielle de la forme.

ml

A= A, 6

Nous savons que la loi est plus compliquée.

 

 

DE cn me nae Mea La

kml

 
