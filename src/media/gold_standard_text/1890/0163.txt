 

 

Et fn

EE NON RUN D MO HL ALL

Tor

c) Azimuts.

La mesure des azimuts a été effectuée par M. Witkowsky aux deux extrémités de la
base de Moloskowitzy au moyen de l'instrument universel de Repsold. Les résultats sont :

 

Azimut de Moloskowitzy, mesuré à Oseritzy. . 150 38 7,00 + 0,94
Azimut d’Oseritzy, mesuré à Moloskowitzy . . 330 42 6120 =. Uae
/ u
Diffiäreneen 0 mon)
Différence géodésique (éléments de Clarke). + . 4 99,94

11. OPÉRATIONS GÉODÉSIQUES
a) Triangulations de premier ordre.

Le rattachement de la triangulation de Wilna à la chaine des triangles de Marianpol a
été accompli au moyen de quatorze triangles. Cette chaine part du côté Soroktotar- Berezniaki
et aboutit au coté Nowinki-Kléminé. Les triangulations du Gouvernement de Saint-Pétersbourg
et celle de la Crimée ont été continuces.

b) Nivellements de précision.

L'année passée, les nivellements de précision ont suivi deux lignes de chemin de fer :
1° depuis Libawa jusqu’à la station Kochedary, d’une longueur de 314 km. et 2 depuis la
station Raiowelz, en passant par Zwangorod jusqu’à la ville de Kieltzy, d’une longueur de
966 km. Le long de ces lignes on a installé trente-deux repéres. En outre, les huit points de
la triangulation de M. le Général Tenner, situes près de ces lignes, y ont été rattachés. La
longueur des lignes de raccordement, nivelées à double, est de 80 kim.

III. OBSERVATIONS DE PENDULE

En 1889, M. le professeur Bredichin et M. Sternberg ont continué leurs observations
de pendule. Le tableau suivant contient les résultats de leurs travaux :

 
