 

Der Herr Präsident dankt dem Grafen d’Avila und ertheilt das Wort den preussischen
Commissaren, und zwar in erster Linie Herrn Prof. Forster, welcher über einige neue
Arbeiten berichtet, welche auf den Sternwarten zu Königsberg und Berlin im Sinne der
künftigen Verwerthung der Mondbeachtungen für geodälische und geologische Untersuchun-
ven ausgeführt worden sind. Ferner zeigt Herr Förster eine von Herrn Dr. Wellmann
erdachte Beleuchtungsvorrichtung für Glasmierometer, wodurch die Striche desselben als
helle Linien im Felde erscheinen. (Siehe Beilage B. IX.)

Ferner verliest Herr Prof. Helmert einen Bericht über die Feld- und Bureauarbeiten
des geodätischen Instituts. Dieselben betreffen unter Anderem Breiten- und Azimut-Messungen
an drei Punkten erster Ordnung, sowie einige Längenbestimmungen; ferner eine Unter-
suchung über den Einfluss der Torsion der llorizontalachse der Universal-Instrumente aul
die Messungen von Zenithdistanzen. (Siehe Beilage B. IX®.)

Ebenso verliest Oberst Morsbach den Bericht der trigonometrischen Abtheilung der
preussischen Landesaufnahme über die Arbeiten des Jahres 1890. Nach demselben wurden
die Beobachtungen in der rheinisch-hessischen Kette fortgesetzt; das Bonner Basisnetz mit
Beobachtungsvorrichtungen versehen und das niederrheinische Dreiecksnetz mit Signalen
ausgestaltet. Die Verfestigung der älteren Nivellementslinien mit besonderen Hühenmarken
und Mauerbolzen ist in Schleswig-Holstein beendet und in Meklenburg begonnen worden,
und ein Pricisions-Nivellement des Wartheflusses in einer Gesammtlänge von 375 km aus-
eeführt. (Siehe Beilage B. IX®.)

Der Herr Präsident dankt den drei Commissaren für ihre Berichte und ersucht den
Secrelär, in Abwesenheit und im Namen des Ilerrn General Stebnitzki, den Bericht über die
russischen Arbeiten im Jahre 1889 mitzutheilen.

Aus demselben geht hervor, was zunächst die astronomischen Bestimmungen
betrifft, dass die in den Jahren 1887 und 1888 ausgeführten 5 Längenbestimmungen den
Abschluss zweier Polygone gestatten, wonach das erste Polygon Kiew-Moskau-Kazan-
Ekaterinburg-Orenburg-Saratow - Astrakhan - Rostow - Kiew, mit dem Fehler + 0°178 und
das zweite Polygon Tiflis-Rostow-Astrakhan-Bakou-Tillis mit einem Fehler von — 0, 08 ab-
schliesst.

Im Jahre 1889 wurden in der Krimm die beiden Längen-Differenzen Jalta-Balaklawa
und Theodosia-Balaklawa ausgeführt. |

In den Jahren 1887 und 1888 wurden durch Beobachtungen im ersten Vertical die
Breiten für Orenburg, Saratow, Bakou, Astrakhan, Rostow, Kowel, und im Jahre 1889 für
die beiden E adpunkte der Basis von Moloskowitzy die Breite bestimmt, deren astro nomische
Differenz 4 36.89 und die geodätische mit den Elementen von Clarke berechnete 4 36 79
ergiebt, also Uebereinstimmung innerhalb der Fehlergrenzen. Ebenso sind an den beiden
Basisendpunkten Azimute gemessen worden.

But Ma |. Lidl.

   

a
ij

Ill

bal m)

uy tin ty

1 erleben +

 

 
