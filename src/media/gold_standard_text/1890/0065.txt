 

lan Semen Sn

til

ALU LE RNA AE

M CE MOUV UE NN

99

auprès des Gouvernements, en vue de ce changement d’un article de la Convention, nous
avons l'honneur de soumettre, par votre intermédiaire, cette question à lattention de votre
Haut gouvernement et de prier Votre Excellence de nous faire connaître la décision qu'il
aura prise à cet égard, si possible à temps pour qu’elle puisse être communiquée à la session
de la Commission permanente qui s’ouvrira le 15 septembre prochain.

Veuillez agréer, Monsieur le Ministre, l'assurance de notre très haute considération.

Le Secrétaire, Le Président,

(signé) Dr Ad. HIRSCH. (signé) Gal Marquis de MULHACEN.

Le Bureau a la satisfaction de pouvoir annoncer à la Conférence que les dix-sept
États de la Convention ! dont les noms suivent ont déjà fait parvenir leur vote affirmatif et
sans restriction : la Saxe, Hambourg, la Bavière, la Belgique, la Suisse, la Prusse, l'ftalie,
les États-Unis de l'Amérique du Nord, le Wurtemberg, les Pays-Bas, l'Espagne, la Républi-
que-Argentine, la France, la Russie, le Chili, la Serbie et la Hesse. Comme la grande
majorité des États de PAxsociation ont déjà signifié leur consentement, et que jusqu'à ce jour
il n'est parvenu de refus d'aucun des Hauts gouvernements, on peut avec raison espérer
que d'ici à peu de temps l'unanimité des Hauts gouvernements exigée pour la revision
d’un article de la Convention sera atteinte.

Dans ce cas, le Bureau ne manquera pas d'informer M. le professeur Davidson et
M. le colonel Hennequin de leur nomination définitive comme membres de la Commission
permanente, ainsi que de communiquer aux autres membres Pheureuse solution de cette
démarche.

Une des questions les plus importantes dont la Commission se soit occupée dans le
courant du dernier exercice, el qui concerne la variabilité périodique des latitudes géogra-
phiques, observée en plusieurs points, a élé renvoyée au Bureau central pour étude ulte-
rieure. Le Directeur de celui-ci, M. le professeur Helmert, fournira dans son rapport à
l’Assemblée des renseignements détaillés sur l'avancement de cette recherche, dont les ré-
sultats paraissent suffisamment favorables pour engager la Commission permanente à pour-
suivre l'étude de cette question.

Enfin, avec le consentement de M. le Président, le Secrétaire se permet de revenir
dans celte réunion sur lPimportante question concernant le choix d’un méridien initial com-
mun pour les longitudes géographiques et l'opportunité de lintroduetion d’une heure uni-
verselle, question dont l'Association géodésique internationale s’est déjà occupée sérieuse-
ment il y a quelques années. Après avoir subi un temps d'arrêt assez long, ce sujet a été
récemment soulevé de nouveau el cela dans un sens qui ne correspond nullement au point
de vue scientifique soutenu par l'Association et qui justifie la crainte de voir cet important
problème dévier dans une voie fâcheuse. M. le professeur Hirsch demande lautorisation de
faire une proposition à ce sujet dans l’une des prochaines séances.

! Au moment de la publication de ces pages, le nombre s'élève à vingt. AE

 
