 

1)

zéro normal un point situé à 37 metres au-dessous d’un repère fondamental placé à PObser-
vatoire de Berlin; ce zéro parait en fait se trouver à deux décimètres environ au-dessus du
niveau moyen de la Baltique à Swinemünde.

Lorsque les réseaux de nivellement des divers pays, après avoir atteint les frontières,
eurent été reliés les uns aux autres, on constata, aux points de jonction, l'existence de
désaccords notables entre les altitudes respectivement obtenues de part el d'autre.

Des dénivellations importantes paraissaient ainsi exister entre les diverses mers, et
souvent même, le long d’un littoral, la cote du niveau moyen variait notablement d’un lieu a
un autre. Ainsi, la Méditerranée se trouvait de 110 environ en contre-bas de l'Océan, d’après
le nivellement de Bourdalouë entre Marseille et Brest; la dépression était de 064, entre
Alicante et Santander, d’après le nivellement espagnol. La mer du Nord, à Amsterdam, se
trouvait à 0732 au-dessus de l’Adriatique, à Trieste, ete. (Voir dans le tableau ci-après et
sur la carte annexée à cette note les démivellations résultant des anciennes observations.)

Ces écarts paraissaient dépasser de beaucoup les erreurs possibles des nivellements,
combinées avec celles des cotes locales des niveaux moyens; aussi ne mit-on pas en doute
la réalité des discordances constatées. L'existence d'importantes dénivellations entre les divers
bassins maritimes devint un fait admis. Dès lors, pour faire disparaître les inconvénients
résultant du désaccord des horizons fondamentaux, il était naturel de proposer l'adoption
d’une même origine d’altitudes pour toute l'Europe continentale.

La raison d’être du projet d’unification des altitudes reposant exclusivement sur la
grandeur des dénivellations constatées entre les mers européennes, il y a tout d’abord un
intérêt capital à rechercher si réellement ces dénivellations sont aussi importantes qu’on la
cru jusqu'alors.

À cet égard, le nouveau nivellement général de la France, en cours d'exécution
depuis 1884, fournit des résultats singulièrement instructifs. Ce nivellement, en effet, se
trouve aujourd'hui relié, d’une part, au réseau italien, qui est raccordé lui-même avec les
lignes autrichiennes ; d'autre part, ilest rattaché aux nouvelles lignes belges, et par celles-ci
aux nivellements de précision de la Hollande et de l'Allemagne du Nord; enfin, le nouveau
réseau français touche à la Méditerranée et à l'Océan, en de nombreux points où le niveau
moyen de la mer est déterminé depuis quelques années avec des appareils spéciaux, appelés
médimarémetres, dans des conditions offrant toutes garanties d’exactitude.

Le tableau ci-après donne les dénivellations entre les mêmes points de quelques mers
européennes, respectivement d'après les anciennes et d’après les nouvelles opérations.

 

ll

ara ca Ue ae ME AE Ta

Yaa tiki il

tray cy li

Wh

ve baal ty

 
