 

102

Mecklenburg hat, sowie Belgien, bis jetzt keine metallenen Fixpunkte, mit Ausnahme
jener, welche von der Preussischen Landes-Aufnahme und vom Geodätischen Institute
gesetzt wurden.

Im Ganzen sind in Mecklenburg 652 Fixpunklte bestimmt, woraus sich die mittlere
Entfernung zweier solcher Fixpunkte mit circa 10 km. ergibt.

Anschlüsse an die Nachbarstaaten wurden ausgeführt :

Von Baden,

an Preussen (Geodätisches Institut) auf der Linie Friedrichsfeld-Basel-Gonstanz ;
we) (Landes-Aufnahme) an 56 Bolzen derselben ;

» Württemberg an 7 Glas- und Höhenmarken ;

» die Schweiz in Basel, Säckingen, Stein, Albbruck, Schaffhausen und Constanz ,

Von Mecklenburg,

an Preussen (Geodätisches Institut) in Rostock, Warnemünde, Wismar, Lauenburg und
Dönnitz ;
» » (Landes-Aufnahme) an 7 Bolzen derselben.

Beziiglich der Ausgangs-Niveau-Flachen ist zu erwähnen, dass in Baden und Meck-
lenburg die Höhen über « Normal-Null » dem in Berlin errichteten Normal-Hohenpunkt fur
Preussen, welcher (nach neueren Messungen) nahezu mit dem Mittelwasser der Nordsee
übereinfällt, publiziert sind. Mecklenburg hat aber überdies auch die Höhen über den Null-
strich des Pegels zu Wismar veröffentlicht.

Auch bei diesen beiden Staaten waren die Instrumente und Nivellier-Methoden ge-
nügend conform mit den von den Conferenzen ausgesprochenen Wünschen, und ich erwahne
nur noch, dass in Baden vom Jahre 1878 angefangen auch die Veranderungen des nominellen
Meters der Nivellier-Latten, durch Vergleiche im Sommer, ermittelt worden sind, und dass
die Mecklenburgischen Nivellier-Instrumente keine Aufsatz-Libellen hatten, sowie dass deren
Fernrohr nicht rotiert werden konnte.

Die mittleren Fehler der Nivellements dieser beiden Staaten erreichen nicht die
Grösse, welche von den Conferenzen noch als statthaft erklärt worden ist.

Die Nivellements-Publicationen dieser beiden Staaten führen die Titel :

Baden : Die Grossherzoglich Badischen laupt-Nivellements mit den Anschlüssen an die Nach-

barstaaten. Bearbeitet von Prof. Dr. Jordan..... Karlsruhe, 1885.
Mecklenburg : Grossherzoglich Mecklenburgische Landesvermessung, IV. Theil. Die geome-
trischen Nivellements..... Schwerin, 1882.

Als Ergänzung meines vorjährigen allgemeinen Rapportes muss ich hier noch
bemerken, dass mir, nach den über den numerischen Betrag der sphäroidischen und anderer
Nivellements-Correctionen angestellten Untersuchungen, schon jetzt ganz zweifellos zu sein
scheint, dass die Europa umspülenden Meere ein und derselben Niveaufläche (Geoïdfläche)
angehôren.

LU

(idl

thi I

aT ELT TTT

Lil lat

ll)

matt ve babe da se

 

 
