 

D FREER yeah

il

ii

CT

PMT TANT

LIT

ME

IE
=
m
=
=
=
>

2

43

und die Dokumente dieser Untersuchungen veröffentlichen und die Arbeit, sowie das Ver-
dienst, aus dem vorhandenen Material die bedeutendsten Folgerungen zu ziehen, den Stern-
warten oder anderweitigen wissenschaftlichen Institutionen überlassen ? Oder soll sie, in
ihrer Eigenschaft als Centrum einer grossen wissenschaftllichen Organisation, nicht vielmehr
diese kostbaren Elemente, welche sie aus eigener Initiative erworben hat, selber so weit ver-
folgen, als ihre finanziellen und wissenschaftlichen Mittel es erlauben?

Selbst vorausgesetzt, die Fortsetzung und Vervollständigung dieser Untersuchungen
lieferten den sichern Beweis, dass die betreffenden Variationen einen ausschliesslich regio-
nalen Charakter hätten, und demnach ein Zusammenhang mit der Bewegung der Rotations-
achse im Innern des Erdkörpers ausgeschlossen wäre, so würde diese Entdeckung für die
Geodäsie und die Geologie vielleicht noch wichtiger sein, als der Beweis einer Bewegung
der Erdachse, welch’ letztere Idee ja keineswegs neu ist, da sie schon früher aus einigen ver-
einzelten Beobachtungsreihen vermuthet und eine Theorie derselben versucht worden ist.

Um aber den regionalen Charakter dieser beobachteten Erscheinung zu beweisen,
sollten in den verschiedensten Gegenden gleichzeitige und gleichwerthige Beobachtungen
ausgeführt werden. Anderntheils müsste man aber auch in den eniferntesten Gegenden, und
wo möglich unter den Europa enigegengesetzien Längengraden, ähnliche Beobachtungen
anstellen, um die andere Alternative, welche von einigen kompetenten Astronomen aufrecht
erhalten wird, beweisen zu können, wonach die den drei Sternwarten gemeinsamen Varia-
tionen der Zenithlage gegen die Sterne durch Bewegungen der Erdachse im Raume verur-
sacht würden, die bis jetzt unbekannt geblieben wären. Diese zweite Alternative nimmt also
an, dass es sich bei dieser Erscheinung um gemeinschaftliche Bewegungen des Poles und
der Zenithe am Sternen himmel handelt, ohne periodische Breitenänderungen und ohne
merkliche Bewegung der Rotationsachse im Erdkörper.

Herr Tisserand ist von der Wichtigkeit der im Jahre 1889/90 auf den Sternwarten
zu Berlin, Potsdam und Prag gemachten Erhebungen überzeugt. Nichtsdestoweniger scheint
es ihm, um mit Recht behaupten zu können, dass die beobachteten Veränderungen von
einer Bewegung der Rotationsachse im Erdinnern herrühren, wäre es angezeigt, im Laule
eines Jahres noch auf andern europäischen Sternwarten nach verschiedenen Methoden Breiten-
Beobachtungen vorzunehmen, bevor man daran gehe eine Expedition nach den Antipoden
auszurüsten. Variationen im Betrage von 0 5 könnten einem Astronomen, welcher z. B. im
Laufe eines Jahres die Zenith-Distanz eines in der Nähe des Zenithes culminirenden
Sternes beobachten würde, nicht entgehen; ein Einfluss der Theilungsfehler und der Biegung
wäre dabei vollständig ausgeschlossen. Die Beobachtungen, welche Herr Perigaud seit zwei
Jahren auf dem Observatorium von Paris an dem Gerele von Gambey ausführt, zeigen die
Möglichkeit, das fragliche Ziel zu erreichen. Sollten sich bei völligem Wechsel von Methode,
Beobachter und Station dieselben Breitenänderungen constatiren lassen, dann wäre aller-
dings kein Zweifel mehr erlaubt, und dann wäre es auch angezeigt, Zahl und Ort der entfern-
ten Stationen zu bestimmen, an welchen in einer gegebenen Zeit Beobachtungen vorgenom-
men werden sollten, um mit der grössten Genauigkeit die Elemente der Bewegung der
Eirdachse bestimmen zu können.

 
