IV. NIVELLEMENT.

Le réseau général du nivellement de précision de la Suisse a été terminé, il y a
déjà plusieurs années; il n’y a plus à publier dans la neuvième livraison que quelques
opérations de contrôle, et quelques lignes destinées à rattacher au réseau les stations astro-
nomiques de Weissenstein, Righi et Gæbris. Cette neuvième et dernière livraison aurait
paru depuis longtemps, si elle ne devait pas comprendre en même temps la compensation
du réseau et finalement, comme couronnement de notre œuvre de vingt-deux ans, le Tableau
hypsométrique de la Suisse.

Le travail de la compensation du réseau a été exécuté par M. Scheiblauer, sous ma
direction, en 1884 et 1885. Ce travail est particulièrement long et compliqué pour un nivelle-
ment dans un pays de montagnes, où la variabilité des mires, qui, dans les pays de plaine,
est un élément presque insignifiant, acquiert une influence considérable sur lexactitude
des résultats. Après de longues recherches préalables, nous avons décidé que, dans la com-
pensalion du réseau, nous tiendrions compte, pour la détermination des poids, de trois
éléments, savoir de l’erreur d’observation kilométrique, du tassement et de erreur de la
mire. Sans vouloir entrer dans les détails, qui seront publiés sous peu dans notre neuvième
livraison, je veux citer seulement quelques résultats principaux de la compensation, qui
prouvent que nous avons lieu d’être très satisfaits de la précision de notre nivellement, sur-
tout si l’on tient compte des difficultés spéciales et exceptionnelles auxquelles ces opérations
sont exposées dans un pays où les differences de niveau atteignent 2000™ et dont le réseau
comprend un assez grand nombre de passages des Alpes.

Ainsi l'erreur fortuite d'observation à été trouvée — + 1°°5 par kilomètre, ce qui
est certainement très acceptable dans nos conditions; le tassement moyen a été également
assez faible, savoir 055, grâce à l’excellent état de la plupart des routes en Suisse; enfin
la correction provenant des équations admises pour les mires est de 0""035 par mètre de
hauteur. L'erreur moyenne de clôture d’un polygone est, d’après les observations, + 549
et, d’apers le caleul, + 57°"#4. La correction que la compensation apporte à une sous-
section, C'est-à-dire à la différence de niveau entre deux repères secondaires, dépasse rare-
ment 1mm, Enfin, les corrections des altitudes de nos repères principaux, assez éloignés, in-
diquées par la compensation, sont comprises entre + 2™™ et 74mm, ce maximum dans un
terrain exceptionnellement difficile.

La compensation étant achevée, il fallait encore déterminer définitivement l'unité des
altitudes. Vous savez que nos mires, ainsi que celles de la plupart des autres pays, ont été
comparées à un étalon en fer de 3" appartenant au Bureau fédéral des poids et mesures à
Berne et étalonné dans le temps (il y a une vingtaine d'années) par M. le professeur Wild, au
moyen du mêtre normal suisse. Pour pouvoir exprimer nos allitudes en véritable unité mé-
trique, il fallait encore déterminer à nouveau cette règle de Berne au Bureau international

 
