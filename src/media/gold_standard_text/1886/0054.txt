 

 

30

Der Bericht über Pendelmessungen zur Bestimmung der Schwere wurde zuerst Herrn
von Oppolzer anvertraut; dieser jedoch trat unter Zustimmung der Commission das Referat an
Herrn Stebnitzky ab, nachdem der letztere in der Commission die Ansicht geäussert hatte, die
Pendelbeobachtungen möchten nicht blos in den gewöhnlichen Gradmessungsgebieten ausge-
führt, sondern noch mehr als bisher auch über die Küsten und Inseln der gesammten Erd-
oberfläche ausgedehnt werden, was für die Ausmittlung der Gestalt der Erde von grösstem
Nutzen sein würde. Die Discussion über diesen Punkt im Schoosse der Permanenten Com-
mission ergab, dass von Herrn Faye schon früher ein ähnlicher Antrag eingebracht worden
ist, welcher auch damals schon volle Billigung erfahren hatte, und ferner, dass diese plan-
mässigen Pendelbeobachtungen besonders gute Resultate erziehlen würden, wenn dieselben
mit vergleichbaren Instrumenten ausgeführt werden könnten. Die Commission beschloss,
Herrn Stebnitzky, sowohl um den Bericht über die in den letzten drei Jahren ausgeführten
Schwerebestimmungen, als auch um die Ausarbeitung eines vollständigen Programms für
diese planmässigen Pendeluntersuchungen bis zur nächsten Versammlung zu ersuchen.

Ausserdem theilt der Sekretär aus dem Protokolle über die Commissionssitzung mit,
dass Herr Förster den Antrag gestellt hat, das Gentralbureau möge von der Conferenz mit
der Zusammenstellung und Discussion der bei den geodätischen Untersuchungen mit kleinen
Instrumenten gemachten zahlreichen astronomischen Beobachtungen beauftragt werden,
um dieselben auch für die Astronomie nutzbar zu machen, da sie durch ihre grosse Anzahl
eine sehr werthvolle Gontrolle der weniger zahlreichen, mit grossen Instrumenten angestellten
Beobachtungen darbieten könnten. — Die hieran sich knüpfende Discussion ergab das Re-
sultat, dass Herr Bakhuyzen, natürlich ohne ihn irgendwie in Betreff der Zeit für diese Ar-
beit beschränken zu wollen, um die Sammlung dieses astronomischen Materials ersucht
wurde, wobei ihm das Gentralbureau durch Uebermittlung der betreffenden Publicationen
nützlich an die Hand gehen solle.

Sämmtliche die Specialberichte betreffenden Anträge der Commission werden von
der Generalconferenz genehmigt, mit Ausnahme desjenigen über die Pendelbeobachtungen ;
da der Herr General Stebnitzky ablehnt, auch die Ausarbeitung des Berichtes über die be-
reits geleisteten Arbeiten zu übernehmen, wird dieser Gegenstand an die Permanente Com-
mission zur nochmaligen Berathung und Antragstellung zurückgewiesen.

Herr von Struve spricht den Wunsch aus, dass auch diesmal die terrestriche Re-
fraction zum Gegenstande eines Specialberichtes gemacht, und Herr von Bauernfeind um
Ausarbeitung desselben gebeten werde.

Dieser Antrag wird unter der Voraussetzung, dass der im Augenblicke abwesende
Herr von Bauernfeind sich damit einverstanden erklärt, von der Versammlung gutgeheissen,

setreff der Schwerebestimmungen bemerkt Herr von Struve ausserdem, dass syste-
matische Untersuchungen dieser Art an den Küsten der Meere und auf den Inseln keineswegs,
wie man es in der Permanenten Commission befürchtet zu haben scheine, zu den frommen
Wünschen gehören. Nicht nur seien in der Vergangenheit nicht unbedeutende Arbeiten
dieser Art ausgeführt worden, wie z. B. durch Admiral Lütke, der bereits vor fünfzig Jahren

a

 

m

 

 

 

|
f
i
|
|
|
|
|
if
|

 
