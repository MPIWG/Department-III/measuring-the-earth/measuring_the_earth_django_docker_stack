213

kehren sie auch noch später zu kürzerem oder längerem Aufenthalte zu uns zurück, beson-
ders wenn sie nach längerer Theilnahme an praktischen Arbeiten das Bedürfniss fühlen sich
noch über einen oder den andern Punkt näher informiren zu lassen. Zu solchen Besuchen
bieten namentlich die Vorbereitungen zu besonderen astronomisch-geodätischen Operationen
und deren Bearbeitung, sowie das Prüfen der anzuwendenden Instrumente vielfach Veran-
lassung. Die Pläne zu solchen Arbeiten werden immer einer eingehenderen Kritik seitens
eines der Astronomen der Sternwarte, der zugleich das Amt eines berathenden Astronomen
des Generalstabs und der Flotte bekleidet (seit 22 Jahren W. Döllen), unterzogen. Demselben
ist durch das genannte Amt zugleich Gelegenheit geboten sich mit der Ausführung der
Arbeiten nach Bedürfniss bekannt zu machen und eventuell an den Director der Sternwarte
zur Mitwirkung in besonderen Fällen zu recurriren.

Nach dieser Charakteristik der vornehmlichsten Bestrebungen Pulkowa’s auf geodä-
tischem Gebiete, erübrigt es nur über die hier am Orte unmittelbar von Döllen oder unter
seiner Leitung ausgeführten, die weitere Ausbildung der Geodäsie bezweckenden Studien
Mittheilung zu machen. Zunächst wäre seiner mehrjährigen Bestrebungen Erwähnung zu
thun ein Instrument herzustellen, welches dem Beobachter gestatten würde auf ein und dem-
selben Standpunkte alle für die höhere Geodäsie erforderlichen astronomischen Bestimmun-
gen (Zeit, Polhöhe, Azimuth) und trigonometrischen Messungen (Horizontal- und Vertikal-
winkel) mit grösster Schärfe auszuführen. Gelingt das, so wäre damit die Möglichkeit geboten
jeden Hauptdreieckspunkt ohne erheblichen Zeit- und Kraftaufwand zu einem sogenannien
astronomischen Punkt zu erheben und eine Masse unmittelbar zu verwerthenden Materials
zu beschaffen, gegen welches das nach den gewöhnlichen Operationsmethoden erreichbare,
verschwindend klein wäre. Wir hoffen um so mehr auf das Gelingen, als ein solches Instru-
ment bereits in der mechanischen Werkstatt unserer Sternwarte nach Döllen’s Angaben
construirt ist, bei dem nur noch ein fein getheilter Höhenkreis anzubringen wäre, um es zu
einem wahrhaft universellen in dem angegebenen Sinne zu machen. Ueber die Construction
und Leistungsfähigkeit dieses Instruments bietet eine im vergangenen Jahre erschienene
Schrift eines der jüngsten Schüler Pulkowa’s, des Capitän Witkowsky, ausführliche Auskunft.

Ferner hat Herr Döllen in den letzten Jahren den von dem schwedischen Geodäten
Jäderin vorgeschlagenen Basisapparat einer eingehenden Prüfung unterworfen, durch welche
die Hoffnung erweckt ist, dass in der That dieser Apparat in Zukunft eine bedeutende Rolle
in der Geodäsie zu spielen berufen sein dürfte. Bei der Schnelligkeit des Operirens mit dem-
selben würden Basismessungen, wie es gewiss zu wünschen ist, viel zahlreicher und in viel
kleineren Abständen von einander ohne viel Mühe ausgeführt werden können, als wie das
mit den gegenwärtig gebräuchlichen, complicirten und schwerfälligen Apparaten geschehen
kann, und, wie es scheint, auch ohne Einbusse an der mit Recht an Basismessungen zu stel-
lenden Anforderungen an Genauigkeit. Ueberdiess würde die Leichtigkeit des Transports die
Anwendung jenes Apparats auch in Gegenden gestatten, die für die gebräuchlichen Basis-
apparate als unzugänglich bezeichnet werden müssten. Es sind jedoch Herrn Döllen’s Prü-
fungen des Jäderinschen Apparats noch nicht abgeschlossen. Namentlich mangelt es noch
an einem Nachweis seiner Constanz, sowie an einem streng begründeten Urtheil über die

 
