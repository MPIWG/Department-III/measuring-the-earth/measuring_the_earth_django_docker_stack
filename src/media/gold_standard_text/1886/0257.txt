PR A

933

mais aussi dans les quatre stations tessinoises de Cadenazzo, Gubiasco, Tiglio et Mognone; elles
seront réduites et publiées prochainement. Un premier calcul a montré déjà que les dévia-
tions de la verticale dans ces stations, sans atteindre les 50”, dont il a été question derniere-
ment dans une Académie, dépassent cependant sensiblement la valeur de 10”.

Je désire attirer à cette occasion Pattention de la Conférence sur les réfractions laté-
rales qui ne me semblent pas encore suffisamment étudiées et qui, d’après les observations
extrêmement nombreuses que J'ai faites depuis vingt-sept ans sur une des mires méridiennes
de l'Observatoire de Neuchâtel, peuvent atteindre des valeurs beaucoup plus considérables
qu’on ne les a admises jusqu’à présent, d’après les opinions des grands géodésiens, tels que
Struve et Bessel; car 4 Neuchatel, le rayon visuel de la mire du Sud, long de 7km, hori-
zontal et passant dans tout son parcours 4 60™ environ au-dessus du lac, et qui, par
conséquent, se trouve dans des conditions optiques exceptionnellement favorables, est
sujet à des variations, par suite de la réfraction laterale, qui, en moyenne, vont 4 + 1.11,
et qui, dans des cas exceptionnels où le rayon traverse plusieurs couches juxtaposées de tem-
pératures très différentes qui ont pour cause des vents différents, atteignent jusqu’à 469,
lorsque le rayon visuel rencontre les surfaces limites de ces couches assez obliquement.

Dans les conditions optiques incomparablement plus défavorables du réseau du Tessin, on

pourrait, de cette seule source, rendre compte des erreurs de clôture des triangles jusqu’à 3”
au moins. Il me semble desirable que dans d’autres Observatoires, qui sont pourvus égale-
ment de mires méridiennes lointaines, on reléve de méme les variations de l’azimut de ces
mires, déduit de celui de la lunette, déterminé par les observations astronomiques faites a
quelques heures de distance; car il serait certainement trés utile pour la géodésie d’obtenir, |
par ce moyen, des données precieuses sur la réfraction latérale.

Enfin, je veux encore mentionner le fait qu'ayant employé dans toutes ces triangula-
tions concurremment les observations nocturnes avec celles de jour, on a trouvé en général
une si faible difference entre ces deux genres d'observation, qu’on pouvait leur donner le
même poids.

Hl. TRAVAUX ASTRONOMIQUES.

Abstraction faite des déterminations de latitudes et d’azimuts que nous venons de
mentionner el que notre Commission géodésique a décidé de multiplier encore au Nord
des Alpes, nous avons à rendre compte des déterminations de différences de longitude
Genève-Vienne et des longitudes Franco-Suisses.

Quant à la première, elle a été calculée à double, à Vienne, par M. von Oppolzer,
qui a indiqué pour résultat 40°44 617  0*019, et par M. le Colonel Gautier, à Genève,
qui a trouvé 40"44°676 + 0°022. Ces deux résultats sont asssez concordants pour qu'il
soil permis d'en prendre la moyenne probable, pour laquelle on trouve Vienne-
Geneve = 40°44563 + 0°03. En ce qui regarde la publication de ce travail, notre cher
collègue défunt, M. von Oppolzer, m’a donné l’assurance, pendant la Conférence, qu’elle
serait faite par les soins et aux frais de la Commission géodésique autrichienne.

30

 
