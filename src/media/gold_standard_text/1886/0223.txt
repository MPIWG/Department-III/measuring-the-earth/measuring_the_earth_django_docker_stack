199

Um so erfreulicher ist die Thatsache, dass Aussicht auf Abhülfe vorhanden ist und dem
Geodätischen Institute also nach einigen Jahren Gelegenheit gegeben sein wird, in den fried-
lichen internationalen Wettstreit zur Erforschung der Gestalts- und Massenverhältnisse des
Erdkörpers voll und ganz einzutreten.

HELMERT.

Beilage XI».
BERICHT

der Köniel. Preussischen Landesaufnahme.

Der Trigonometrischen Abtheilung der Landesaufnahme sind die Triangulirungen
und Nivellements in Preussen übertragen.

Die Hauptnetze sind nahezu fertig: das Hauptdreiecksnetz wird 1892, das Haupt-
nivellementsnetz 1888 vollendet werden.

Die Veröffentlichung der Hauptdreiecksmessungen ist in den letzten zehn Jahren
gegen andere dringlichere Arbeiten zurückgestellt, nunmehr aber wieder in Angriff genom-
men worden, und zwar mit den Resultaten beginnend. Diese werden — soweit sie noch nicht
publizirt sind — binnen Jahresfrist in den Händen der Herren Delegirten sein. Das Uebrige
wird weiterhin folgen.

Die Publikation der Hauptnivellements hat mit der Messung ziemlich gleichen Schritt
gehalten : sechs Bände sind bereits erschienen, zwei folgen noch.

Uebrigens sind diese Messungen — nämlich die Nivellements und die Hauptdreiecke
— nur ein kleiner Theil der Gesammtmessungen der Landesaufnahme; es werden jährlich
etwa zweitausend Punkte bestimmt und alle nach der Methode der kleinsten Quadrate aus-
geglichen. Die Gesammtdreiecks- und Héhenmessungen werden publizirt in dem Werke :

Abrisse, Koordinaten und Höhen sämmtlicher von der Trigonometrischen Abtheilung
der Landesuufnahme bestimmten Punkte.

Von diesem Werke sind bis jetzt sieben Bände erschienen. Von dem letzten Bande
habe ich mir erlaubt, einige Exemplare auf den Tisch der Conferenz niederzulegen.

ÖBERST SCHREIBER.

 
