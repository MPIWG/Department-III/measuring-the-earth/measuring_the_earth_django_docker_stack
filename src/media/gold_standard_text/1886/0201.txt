 

 

 

 

 

ar

 

4. An die württembergischen Nivellements in den 2 Glasmarken desselben zu Non-
nenhorn und Kressbronn.

5. An die Präcisions-Nivellements der Schweiz in 7 schweizerischen Fixpunkten
wovon 3 Messingmarken sind.

Mit dem bereits fertig bearbeiteten Anschlusse bei Martinsbruck gibt diess 4 Anschluss-
marken I. Ordnung.

6. An die italienischen Nivellements in Strassoldo und daher, mit den bereits früher
hergestellten 2 Anschlüssen im ganzen an 3 Punkten 1. Ordnung.

‚7. Für Rumänien sind 4 Anschlussmarken vorbereitet und zwar bei Verciorova,
südlich von Hermannstadt, südlich von Kronstadt und bei Itzkani.

8. Die russischen Nivellements sind bis jetzt bloss in Szezakowa angeschlossen, doch
wird von uns noch ein Anschluss in Radziwilow und die Vorbereitung zu weiteren zwei
Anschlüssen in Nowoselica und Podowoloezyska in den nächsten Jahren fertig gestellt sein.

HARTL, R. von STERNECK, von KALMAR,
k. k. Major. Rok. Major. R. R. Linienschiffs-Capitän.

Beilage VIIIe.

BERICHT

des k. k. Major von Sterneck über die von ihm bisher auseeführten
Schwerebestimmungen.

Die von Seite des k. k. militär-geographischen Institutes, beziehungsweise durch mich
bisher ausgeführten Schwerebestimmungen sind im allgemeinen bloss als provisorisch zu
betrachten, indem es sich bei ihnen in erster Linie darum handelte, einestheils jene Appa-
rate und Beobachtungs-Methoden ausfindig zu machen, mittelst welcher man im Stande ist,
selbst auf schwer zugänglichen Orten relative Schwerebestimmungen leicht und verlässlich
auszuführen — anderseits jedoch um über die Ergebnisse derartiger Bestimmungen Erfah-
rungen zu sammeln bezüglich ihrer Uebereinstimmung mit den bisherigen Annahmen über
das Verhalten der Schwere auf und in der Erde.

 

 
