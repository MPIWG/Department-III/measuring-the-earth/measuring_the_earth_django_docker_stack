165

 

 

4. An die wiirttembergischen Nivellements in den Glasmarken desselben zu Nonnen-
horn und Kressbronn, und sind dies die in obiger Zusammenstellung untern den Nummern
608 und 611 angeführten Fixpunkte, so beziffert im Verzeichnisse der Fixpunkte des baye-
rischen Pracisions-Nivellements '.

5. An die Pracisions-Nivellements der Schweiz in den schweizerischen Fixpunkten
N. F. 141, © 60, © 64, N. F. 142, dann © 59, N. F. 140 © 58 2,

Als vorläufige Meereshöhe über dem Mittelwasser der Adria bei Triest, abgeleitet
über Laibach—Mauthaus a. d. Save—Tarvis—Villach— Spital a. d. Drau—Lienz—Franzens-
feste—Bozen—Nauders— Landeck—Bludenz—Feldkirch—Bahnwächterhaus Nr. 15 nahe süd-
lich von Bregenz fand sich für die Schweizer Höhenmarke N. F. 141 in Fussach

396,99

Der Vergleich der Schweizer Nivellements und jener von Oesterreich-Ungarn auf
den gemeinsamen Nivellementstrecken N. F. 142 — © 60 —N. F.40— © 58 und © 60
— N. F. 141 — bayerischer Fixpunkt 586 stellt sich wie folgt:

 

 

 

 

 

 

 

 

 

Nach den Nach den
Schweizer Nivellements. Oesterr. Nivellements.
N.F. 142 a. ee
©. 64 — 2,1901 —- 2,1906
© 60 — 0,9545 — 0,9478
@ 59 — 1,5600 —- 1,5562
N. F. 140 | + 0,9014 ++ 0,9145
© 58 — 1,9142 — 1,9063
© 60! — —
Bayerischer Fixpunkt 585 + 5,0126 —- 5,0409
N° 14] — 0,6255 + 0,6262
Bayerischer Fixpunkt 586 —- 0,1790 3 + 0,1836

 

Die Nivellementstrecke zwischen den beiden bayerischen Fixpunkten 585 und 586
erscheint sonach sowohl von der Schweiz als auch von Bayern und von Oesterreich-Ungarn
nivellirt und findet sich der Höhenunterschied dieser Fixpunkte 585-586 mit

1Nach dem wäürttembergischen Nivellement wird die Meereshöhe der Glasmarke in Nonnenhorn
d. i. 608 (Bayern) mit 420,2726 über Normal-Null (ausgeglichen) gefunden, 1885 aber mit 420,380 publieirt.

2Man sehe: A. Hirsch et E. Plantamour, Nivellement de précision de la Suisse, ewecute, etc.
Sixième livraison. Genève, Bâle, Lyon, 1877, page 384, 385, 386.

8 Nach den Schweizer Nivellements nur einfach bearbeitet.

 
