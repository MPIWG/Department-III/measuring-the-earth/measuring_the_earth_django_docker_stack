Election de M. le Prof. Hirsch comme Se-
Brätame perpetieln nn... 02...
Election des neuf membres temporaires de
la Commission permanente . : +: .

Troisième séance, 30 octobre 1886.

Le procès-verbal de la deuxième séance,
est lu par le Secrétaire dans les deux
langues, et adopté. . . . ws

Communications d’affaires par M. le Prési-
Genk — ee Se oe

Rapport de M. le Colonel one sur les
travaux géodésiques en France pendant
les trois derniéres années, présenté par
M. Faye (voir Annexe V, page 128). .

Rapport de M. d’Avila sur les travaux géo-
désiques accomplis en Portugal (voir An-
Dexe À. pare 183). +. . ee.

Rapport de M. von Bauernfeind sur les
travaux en Bavière (voir Annexe I, page
We

Rapport du Secrétaire sur la constitution
de la Commission permanente et sur la
désignation par elle des rapporteurs spé-

Ce 0... :

Discussion sur ce rapport et en des
propositions de la Commission perma-
nenne on ne

Motion de M. von Struve d’eriger un buste
au General Baeyer dans le nouvel Institut
géodésique prussien; acceplé avec l'a-
mendement de M. Ferrero . . . -

Quatrième séance de relevée, 30
octobre, après midi.

Communications d’affaires de M. le Prési-
SUE ROG ibn fon ni es ie
Rapport de M. le Général Ferrero sur les
travaux exécutés en Italie pendant lestrois
dernières années (voir Annexe VI, page
a en. 2. 2er
Communication de M. le Gen. Falcoiano
sur l’état actuel des travaux géodésiques
ein ROWS 5 5 6 0 6 6 :
Rapportde MM. von Kalmar, von iz
et von Sterneck sur les travaux en Au-
triche-Hongrie (voir Annexes VIII, VIII,
NS pace 12) à 0. à ©: * -

 

Rapport de M. le Lieut.-Colonel Hennequin
sur les travaux en Belgique(voir Annexe If,
pase her se se

Rapport de M. Nel sur les travaux dans le
Grand-Duché de Hesse. . . 3

Rapport de M. le Prof. ea, sur Dies
travaux en Norvege (voir Annexe VII,
Wace MLO) i ee

Rapport de M. le Colonel Zachariae sur
les travaux en Danemark (voir Annexe III,
page 122). 0 ; :

Rapport de M. le ln Nha sur ies
travaux de triangulation exécutés, les
derniéres années, en Prusse, par la « Lan-
desaufnahme » (voir Annexe XI>, page
199, ee ee

Cinquième séance, Aer novembre 1886.

Le procès-verbal des deux dernières séances
est lu par le Secrétaire et adopté par
Assemblée = =, = ee

Discussion sur le len des mem-
bres absents de la Commission perma-
nente, question renvoyée à celle-ci. .

Communications d’affaires du Président .

Lettre de Son Exc. M. le ministre d'Etat
von Gossler, pour prendre a de la
Goniérente:. > 2

Explications de M. le general alesıano
sur sa communication dans la séance
précédente; dépôt de la carte de trian-
gulation roumaine. . + + :

Rapport de M. von Struve sur les oo
eéographiques en Russie (voir Annexe
XID; p2004) ee

Discussion provoquée par la critique du
systéme actuel de rapports géodésiques,
par M. von Struve - .  . .

Sixieme séance de relevée, 1°" no-
vembre 1886.

Discussion et resolution sur le transfert de
la voix d’un membre absent de la Com-
mission permanente sur un autre mem—
press À ee .

Rapport de M. ee sur les travaux
exécutés dans les Pays-Bas pendant les
trois derniers ans (v. Annexe IX, p. 178)

 
