 

 

   
 

16

Beendigung derselben den Doctorgrad der Philosophie, und besuchte zugleich die Vorlesun-
oen auf dem Polytechnieum in Wien; nach deren

Absolvirung er beim Bau der ungarischen

Centralbahn, bei der Donau-Aufnahme und bei der Semmeringbahn beschäftigt war. Im
Jahre 1850 wurde er Assistent der praktischen Geometrie am Wiener Polytechnicum unter
859 Professor der höheren Mathematik in Prag. Im Jahre 1856

Simon Stampfer, und 4
wurde er zum Professor der praktischen Geometrie an das Wiener Polytechnicum berufen,

und 1866 daselbst Professor der Astronomie und hoheren Geodäsie.

In Anerkennung seiner Verdienste bei der Reorganisation dieses Instituls, wurde er
4866 zum ersten Rector der nunmehrigen technischen Hochschule gewählt. Seit 1860 war
er Mitglied der österreichischen Gradmessungs-Commission, die ihn im Jahre 1881 zu ihrem
Präsidenten wählte.

Als Gradmessungs-Commissar hat er thatig an der Ausführung der Gradmessungs-

arbeiten mitgearbeitet; es wären hier unter Anderen zu nennen seine Betheiligung an den
Längenbestimmungen Wien-Fiume und Wien-Kremsmünster; seine Breiten- und Azimuth-
Bestimmung auf dem Spieglitzer und Petschner Schneeberg nnd auf Wetrnik.

Im Jahre 1872 wurde er zum Director der k. k. Normal-Aichungs-Commission und
zum Ministerialrath ernannt, und seiner unermüdlichen und hervorragenden organisatori-
schen Thätigkeit war es zu verdanken, dass die Einführung des metrischen Maasses und
Gewichtes in verhältnissmässig kurzer Zeit in Oesterreich ausgeführt wurde.

Von seinen literarischen Arbeiten ist ein im Jahre 1857 erchienenes Lehrbuch der
höheren Mathematik zu erwähnen, welches mehrfache Auflagen erlebt hat. Im Nachlasse des
Verstorbenen befindetsich ein fast vollendetes Lehrbuch der sphärischen Astronomie, welches
durch Herrn Prof. Tinter publicirt wird.

Am 30. September 1884 beschloss Herr sein thatenreiches Leben, nachdem er lange
mit einem schweren Leiden zu kämpfen gehabt hatte. Alle. diejenigen, welche unserm geehr-
ten Collegen näher gestanden sind, haben bei ihm, unter einem kalten und zurückhaltenden
Aeussern, einen Charakter von seltener Güte und hervorragender Energie erkannt und ge-
schätzt. Seine zahlreichen Freunde betrauern tief den Verlust dieses trefllichen Mannes, des
gewissenhaften und würdigen Gelehrten, des liebenswürdigen und zuverlässigen Collegen.

Es bleibt nur noch übrig, die weiteren Personal-Aenderungen kurz zu erwähnen :

Am 14. Juni wurde von der französischen Regierung, an die Stelle des verstorbenen
Villarceau, Herr Tisserand zum Delegirten erwählt, den wir das Vergnügen haben unserer .
hiesigen Versammlung beiwohnen zu sehen.

In Dänemark hat Herr Oberstlieutenant Zachariae, an Stelle Sr. Excellenz des
Herrn G. R. Andrae, der sich zurückgezogen, die Direction der Gradmessung übernommen,
und ist zugleich demselben als Commissar für die europäische Gradmessung beigegeben
worden.

Schliesslich hat in Rumänien Herr General Barozzi, nachdem derselbe zum Chef
des Generstaßs des Königs ernannt worden, am 16. Juni dieses Jahres seinen Rücktritt von
der europäischen Gradmessung angezeigt.

Dr. Ad. HırscH.

 

 

 

 
