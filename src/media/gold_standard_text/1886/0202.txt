 

 

178

Zu diesem Zwecke wurden im Laufe der letzten Jahre mit einem provisorisch herge-
stellten Apparate an 12 Orten mit 32 Stationen, welche bei verschiedener geologischer
Beschaffenheit des Bodens sich von 1000 Meter unter der Erde successive bis zu einer Meeres-
höhe von 3000 Meter erheben, Beobachtungen ausgeführt, und hiebei fast ausnahmslos mit
unter sogar sehr beträchtliche Abweichungen der Schwere von dem bisher als normal ange-
sehenen Verhalten derselben vorgefunden.

Gleichzeitig wurden auch an den zu diesen Bestimmungen benützten Apparaten und
Beobachtungs-Methoden nach und nach alle jene Verbesserungen angebracht, die sich unter
den verschiedenartigsten Verhältnissen als wünschenswerth erwiesen haben.

Die Ergebnisse dieser Bemühungen sind in den « Mittheilungen des k. k. militär-geo-
graphischen Institutes », Band II bis VI enthalten, und kann ich als wesentlichsten Erfolg
derselben anführen, dass in Folge der erhaltenen relativ guten Resultate die k. k. öster-
reichische Gradmessungs-Commission, in ihrer Sitzung am 19. December vorrigen Jahres
einstimmig den Wunsch ausgesprochen hat : es möge ein nach meinen Prinzipien konstruirter
Apparat durch einen Mechaniker hergestellt, und mit demselben an möglichst zahlreichen
Orten Schwerebestimmungen ausgeführt werden. Das k. k. Reichs-Kriegs-Ministerium hat
diesen Beschluss durch Bewilligung der zur Anschaffung dieses Apparates nöthigen Geld-
mittel zur Ausführung gelangen lassen, und es dürfte derselbe noch vor Ablauf dieses Jahres
fertig gestellt sein.

STERNECK,

Major.

Annexe IX.
PAYS-BAS

RESUME
des travaux géodésiques exécutés dans les Pays-Bas en 1884, 1885 et 1886.

TRIANGULATION.

Comme il a été dit dans notre rapport sur les travaux géodésiques exécutés en 1883,
(voir Rapport général pour l’année 1883, Berlin, 1884, p. 297), les travaux pour la trian-
gulation ont été suspendus après la mort de notre collègue, M. Stamkart, pour être repris

 

—_—

—

 

 

 

————

—

nn

 
