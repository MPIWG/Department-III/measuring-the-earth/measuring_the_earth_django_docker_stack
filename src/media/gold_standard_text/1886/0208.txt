 

LA

184

Pour l'observation des angles horizontaux, nous employons la méthode, bien connue,
de la réitération. On a tâché, au commencement, d’obtenir pour chaque point le même
nombre de visées, dans des conditions identiques. Par ce moyen, on rendait plus facile le
calcul des directions les plus probables. Dernièrement, néanmoins, à cause de quelques
difficultés pratiques survenues, nous avons adopté fidèlement le système usité dans les
triangulations de Prusse et d'Espagne.

Pour la mesure des distances zénithales, nous employons la méthode qui consiste à
observer dans les deux positions opposées du cercle vertical, en les accompagnant des lec-
tures barométriques et thermométriques nécessaires pour le calcul de la réfraction selon la
formule de Struve. Chaque distance zénithale est donc le résultat de deux visées, et des
lectures de ces visées sur le niveau et sur les microscopes micrométriques.

BASES GÉODÉSIQUES.

La base adoptée — Batel Montijo — qui était suffisante pour la triangulation de la
carte générale, n’atteint pas le degré de précision des bases modernes.

Nous possédons maintenant un nouvel appareil, construit dans les ateliers si renom-
més de M. Repsold, avec lequel on mesurera, au moins, une nouvelle base aussitôt que la
triangulation sera terminée. On est maintenant en train de choisir un terrain approprié à

cet usage.

POINTS ASTRONOMIQUES DE LA TRIANGULATION.

Le réseau fondamental est lié aux observatoires astronomiques de Lisbonne et de
Coïmbre : la liaison du premier a exigé une triangulation spéciale, qui vient d’être publiée,
et dont j'ai l'honneur d'offrir un exemplaire à chacun de mes honorés confrères, les mem-
bres de cette Conférence.

Ce travail, dont l'exécution a été confiée par l’illustre directeur des travaux géodé-
siques, M. le général de division Arbués Moreira, 4 M. le lieutenant-colonel du génie Brito
Limpo, dont le haut mérite est généralement reconnu en Portugal, et dont je constate avec
regret l'absence à ce Congrès, où des motifs de santé l'ont empêché de se rendre, ce travail,
dis-je, renferme la description de quelques-unes des méthodes d'observation et de calcul
suivies en Portugal.

L'Observatoire royal astronomique de Lisbonne, construit exprès à la Tapada d’Ajuda,

peut, à juste titre, mériter le nom de grand observatoire. Il est dirigé par M. Oom, élève du.

célèbre astronome de Poulkowa, feu M. W. de Struve. On y trouve les meilleurs instruments,
et un comparateur de règles géodésiques y est installé selon les principes de la science
actuelle. La latitude de cet observatoire inspire la plus grande confiance, et la différence de
la longitude par rapport à Greenwich a déjà été déterminée par les procédés électriques.

 

ARR

 

Se

 

 

 
