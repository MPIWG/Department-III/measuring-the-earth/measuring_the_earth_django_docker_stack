 

 

157

zwischen dem 45. und 46. Parallel wurden die Beobachtungen auf Nasgy-Kikinda, Hatzfeld
und Kis Torak ausgeführt.

C. DAS BASISNETZ BEI BUDAPEST
wurde Anfangs Juli recognoscirt.

Die Auffindung des geeigneten Terrains zur Messung einer 4 bis 4°"5 langen
Grundlinie war durch die im Frühjahre 1883 fertig gewordenen Militäraufnahms-Sectionen
dieser Gegend sehr erleichtert.

Das in Wien nach diesen Sectionen entworfene Project für die Grundlinie auf dem
Rakos und deren Entwicklung in eine Seite des Hauptnetzes erwies sich nach vorgenomme-
ner Recognoscirung als vollkommen durchführbar und zweckentsprechend.

Die Sichten vom Lebhegy nach dem Westlichen und nach dem Oestlichen Basisend-
punkte, welche durch sanfte Terrainwellen und durch zahlreiche Alleebäume behindert wa-
ren, konnten nur durch Errichtung von Gerüstpyramiden auf den genannten drei Punkten
hergestellt werden. Um die zu diesem Zwecke nothwendigen Höhen der Pyramiden er-
mitteln zu können, wurden längs der Linien Lebhegy-Westlicher Basisendpunkt und Lebhegy-
Oestlicher Basisendpunkt Nivellements ausgeführt und die nach denselben construirten Profile,
auch die Höhen der die Sichten verstellenden Bäume eingetragen. Danach erhielten die drei
Gerüstpyramiden folgende Dimensionen :

Höhe der Pyra- Höhe des Instru-

midenspitze mentenstandes

über dem natürlichen Boden
WesihchenBassniunt  . 1 . ... 11,00 7.20
Oestlicher » 0, a) 8.25
a re 7.69

Der Instrumentenstand auf jedem der beiden Basisendpunkte sollte einerseits die Auf-
stellung des Theodoliten in der Axe der Pyramide gestatten, um die bei so kurzen Dreiecks-
seiten die Genauigkeit der Arbeit beeinträchtigenden Reductionen zu vermeiden, durfte aber
mit seinem unteren Ende die Aufstellung des Absenklungseylinders ! über dem durch einen
Messingkonus markirten Endpunkte, sowie die Messung der Grundlinie mit den Messtangen
nicht hindern. Der Baumstamm, welcher als Instrumentenstand zu dienen hatte, wurde des-
halb nicht vertical gestellt, sondern unter einem Winkel von 15 bis 20° gegen die Verticale ge-
neigt und entsprechend gestuzt. oo

Auf dem Entwicklungspunkte Kalvarienberg, dann auf den Punkten I. Ordnung
Johannesberg und Baj temetés wurden gewöhnliche Pyramiden gebaut und später auf allen
sechs Punkten des Basisnetzes die Winkelmessungen durchgeführt.

1 Die Beschreibung des Messapparates und des Vorganges bei der Messung findet man in den «Mit-
theilungen des k. k. militär-geographischen Institutes», Band III, Seite 13 bis 22, uud im I. Bande « Die
astronomisch-geodätischen Arbeiten des k. k. militär-geographischen Institutes in Wien ».

 
