239
étalonnée de nouveau au Bureau international des poids et mesures, ce qui a été fait au
commencement de 1886. Le résultat a été que la longueur de la régle espagnole est à 0° :
Meee ie
F= 4 — 309,84 = 1,9

r

et que sa dilatation est représentée par la formule :

1, = 1, (1 + 0,000011426 x ¢-+- 0,00000000815 x #) + 0, 6

Comme pour la premiére réduction nons nous étions servi de l’ancienne détermi-
nation que M. le Général Ibanez avait exécutée en 1865 dans les ateliers des constructeurs,
et qui avait donné pour la longueur de la règle :

F — 4,0006542 + 0, 043193 (4 — 21,935),

il y avait donc entre les constantes anciennes et nouvelles, des différences assez no-
tables pour nous obliger de refaire la réduction. D'autant plus que M. le Général Ibanez,
après son retour de Suisse à Madrid, avait déterminé à nouveau les coefficients de dilatation,
et avait trouvé en 1882 :

1 0. 0466358 >< « —- 0,0000068 >< 2,

et que MM. Plantamour et Dumur avaient déduit des bases mêmes une correction de l’ancien
coefficient de dilatation de + 3432, ce qui s'accorde assez avec le premier coefficient déter-
miné à Breteuil.

Comme les expériences de M. le Général Ibañez ont prouvé que la longueur absolue
de sa règle n'avait pas sensiblement changé entre 1865 et 1882, suivant les comparaisons
faites dans ces années avec la règle bimétallique restée à Madrid immobile depuis 1865, de
sorte que la différence indiquée ne peut s’expliquer que par l'erreur du métre employé en 1865
comme étalon et dont l'équation avec le mètre des Archives n’était pas suffisamment connue;
et que, d'autre part, la variation intervenue réellement dans la dilatation de la règle n’est
pas proportionnelle au temps, mais doit être attribuée essentiellement au voyage que lappa-
reil a fait en 1880 de Madrid A Aarberg, par train express, en cinquante-huit heures, notre
Commission géodésique, d’accord avec l’opinion de M. le Général Ibafiez, a décidé de réduire
à nouveau les mesures de nos trois bases en employant, pour la longueur aussi bien que
pour la dilatation, les valeurs déterminées au Bureau international des poids et mesures en
1886. Ce travail est en exécution et nous espérons pouvoir publier prochainement les mesures
de nos bases.

 
