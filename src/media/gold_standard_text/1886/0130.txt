 

I
l
|
|

   

 

a

M. Ferrero rappelle que la proposition de faire des observations de pendule, d’après
un plan plus vaste, a déjà été présentée par M. Faye dans une précédente Conférence, et que
des travaux de celte nature ont d’ailleurs été inscrits dès l’origine dans le programme de la
mesure des degrés; il ne lui paraît done pas nécessaire d’en faire l’objet d’une nouvelle
motion.

M. Ferster partage Popinion de M. Stebnilzky ; on pourrait faire dans cette direction
des progrès plus sérieux. M. le rapporteur devrait être invité à dresser, en vue des détermi-
nations de la pesanteur, un plan complet sur la manière d'entreprendre systématiquement
ces déterminations en différents lieux de la surface du globe, et à indiquer, en même temps,
quels seraient les instruments les plus convenables dans ce but, afin qu’on püt, dans la suite,
établir à ce sujet un programme complet.

M. von Oppolzer appuie M. Ferrero et craint que des points de vue généraux ne ser-
vent pas à grand’chose dans ce cas; il faudrait recommander des méthodes et des instru-
ments spéciaux. Il ne s’agit pas ici de déterminations absolues, mais bien de détermina-
tions relatives de la pesanteur, qui sont d’ailleurs plus faciles à exécuter; peut-être réussi-
rait-on mieux dans ce but avec un pendule invariable qu'avec le pendule à réversion, qui du
reste est certainement supérieur.

Îl appuie l'idée de M. Fœrster que le rapporteur spécial élabore en même temps le
plan de ce grand travail, et il désire, en conséquence, que M. Stebnitzky soit chargé de l'en-
semble du rapport, puisque c’est lui qui a mis en avant ces déterminations de la pesanteur,
qu'il propose d'étendre à la surface entière du globe.

M. Faye croit qu'un tel plan, embrassant toute la Terre, pour la détermination de
la pesanteur, serait aussi d’une grande utilité, en tant qu’on arriverait, par ce moyen, a ob-
tenir un appui efficace de la part des marines des différents pays. Aussi ne doute-t-il pas que
les officiers de marine français ne consentent volontiers à exécuter, dans ces conditions,

des déterminations de la pesanteur, conformément à un plan établi et selon les méthodes re-
commandées.

M. Ferrero croit que le moyen le plus simple d'obtenir des officiers de marine fran-
çais leur précieuse coopération, serait que la Commission française pour la mesure des de-
orés prit l'initiative de cette demande.

M. Stebnitzky dit qu'en Russie il existe un plan d’après lequel on fera sous peu des
observations de pendule depuis la Nouvelle-Zemble jusqu’à Arkangel.

M. Fœrster propose de charger, comme par le passé, M. von Oppolzer du rapport sur
les observations de pendule déjà terminées, et M. le genéral Stebnitzky de l'élaboration d’un
plan pour les observations ultérieures.

M. von Oppolzer est, au contraire, d'avis qu’un seul rapporteur, M. le général Steb-
nutzky, soit chargé des deux parties de ce travail.

À la demande du Président, la Commission se prononce à l’unanimité dans ce der-
nier sens.

 
