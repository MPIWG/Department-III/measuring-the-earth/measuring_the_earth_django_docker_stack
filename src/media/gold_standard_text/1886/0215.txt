 

 

|

|
|

 

 

191

Ganges der Lothabweichungen um Rauenberg-Berlin wurden behufs Breiten- und Azimuth-
bestimmung sechs Punkte ringsum in circa 20 Kilometer Abstand ausgewählt. Vier der
Punkte konnten im Laufe des Sommers absolvirt werden.

Um ferner den Gang der Lothabweichungen auf dem nahezu meridionalen Streifen
Christiania-Tunis, für welchen Herr Generalmajor von Orff auf der Strecke in Bayern und
von da bis Oberitalien bereits eine interessante Untersuchung anstellte (vergl. « Bestim-
mung der geographischen Breite der Königl. Sternwarte bei München. 1877,» S. 39-69),
und für welchen in neuerer Zeit sich in Bayern und Dänemark, sowie im Harz weiteres
Material ergeben hat, in Norddeutschland eingehend zu untersuchen, wurden zwischen dem
Harz und der dänischen Grenze in Schleswig vierzehn Breitenstationen eingeschaltet. Als
Beobachtungsinstrument gelangte das schon im Jahre 1875 und 1881 zu gleichem Zwecke
benutzte dreizehnzöllige Universalinstrument der Königl. Sächsischen Gradmessungskom-
mission, welches von Herrn Geheimen Regierungsrath Professor Nagel aufs Bereitwilligste
dem Institut zur Verfügung gestellt wurde, zur Verwendung. Die geodätische Lage dieser
Punkte war durch die Landesaufnahme in allen Fällen unmittelbar gegeben, mit Aus-
nahme von Braunschweig, wo eine grössere Centrirungsmessung erforderlich ist, welche
Herr Professor Dr. Koppe zu übernehmen die Güte hatte. Die Untersuchung des Harzgebietes
durch Breitenbestimmungen wurde ebenfalls auf sechs Stationen fortgesetzt; es wurden
aber auch zwei Punktreihen in ostwestlicher Richtung ausgewählt, auf welchen in den näch-
sten Jahren Azimuthmessungen angestellt werden sollen, in der Absicht, mittelst zweier
Ostwestprofile Verbindungsglieder für die im Harzgebiete aus den zahlreichen Breitenbe-
stimmungen ableitbaren Meridianprofile des Geoides zu erhalten. Eines der Ostwestprofile
konnte etwas nördlich vom Harze in eine flachere Gegend verlegt werden, wo eine rasche
Veränderlichkeit der Lothabweichung nicht zu erwarten ist, so dass für die Konstruktion
dieses Profiles des Geoids die Verhältnisse möglichst günstig werden. Zur Bestimmung der
geodätischen Lage der Punkte hat die Königl. Landesaufnahme ihre Mitwirkung zugesagt.

Die Messungen im Harzgebiete hatten sich der regsten Theilnahme und thatkräftig-
sten Unterstützung von Seiten der Herzoglich Braunschweig-Lüneburgschen Kammer und
der ıhr unterstellten Forstbeamten zu erfreuen.

Es mag hier noch erwähnt werden, dass die Horrebow-Taleott’sche Methode in die-
sem Sommer auf dem Rauenberge durch Oekonomie der Zeit beim Beobachten und Rechnen
sehr befriedigt hat, und dass daran gedacht wird, dieselbe mehr und mehr, namentlich für
Stationen, die nur in Breite bestimmt werden, ausschliesslich anzuwenden.

Besondere Aufmerksamkeit wurde 1886 der Konstruktion solcher Meridianmarken,
die auf freiem Felde an Holzgerüsten befestigt werden, zugewandt. Veranlassung hierzu gab
die ungewöhnlich grosse Differenz von 47, die sich 1884 auf dem Zobten zwischen Morgen-
und Abendbeobachtungen im Azimuth gezeigt hatte, wobei nicht ausgeschlossen war, dass
wenigstens ein Bruchtheil der Differenz durch eine Bewegung des die Marke tragenden,
1m hohen Holzstammes unter dem Einfluss der Sonnenbestrahlung entstanden sein könne.
Demgemäss wurde einerseits von Professor Albrecht auf Rauenberg die südlich gelegene
Meridianmarke mit einem nur nach Norden offenen Schutzhaus umgeben, während andrer-

 
