 

II

 

Arr. 5. — Die Permanente Commission besteht hinfort ausser den beiden standigen
Mitgliedern aus neun von der nächsten Allgemeinen Conferenz zu wählenden Mitgliedern. In
Betreff des Modus des Ausscheidens und der Wiederwahl dieser Mitglieder bestimmt die Con-
ferenz das Nähere (siehe Art. 11 und 12).

Art. 6. — Um der Permanenten Commission die ihr zugewiesene Oberleitung des
Centralbureaus (siehe Art. 2-4), sowie überhaupt die wissenschaftliche und geschäftliche
Förderung des Unternehmens in einer noch wirksameren Weise als bisher zu ermöglichen,
wird derselben zunächst für die Dauer von zehn Jahren eine Dotation ausgesetzt, welche
durch Beiträge sämmilicher betheiligten Staaten aufgebracht wird.

Ant. 7. — Diese Dotation soll den Jahresbetrag von 16 000 M. oder nahezu 20 000
franes nicht übersteigen.

Hiervon sind bestimmt :

a) 41 000 M. oder nahezu 13 750 frs. für Ausführung oder Unterstützung wissen-
schaftlicher Arbeiten, für die Publikationen derselben, sowie für den Druck der Verhandlun-
sen der Commission und für vermischte geschäftliche Unkosten ;

b) 5.000 M. oder nahezu 6 250 frs. für den ständigen Sekretär der Commission.

Wird obiger Jahresbetrag unter a) nicht verbraucht, so tritt die Esparniss zu dem für
die folgenden Jahre verfügbaren unveränderten Dotationsbetrage hinzu. — Die Verwendung
der Fonds unter a) im Einzelnen erfolgt auf Grund von Mehrheitsbeschlüssen der Perma-
nenten Commission.

Art. 8. — Die Beiträge der betheiligten Staaten können geleistet werden entweder
jährlich im Beeinne des Jahres oder auf einmal im Laufe des ersten Jahres (1887) in der
Höhe des auf nahezu zehn Jahre mit 4°/, kapitalisirten Betrages, welcher nahezu dem 81,
fachen der jährlichen Zahlung gleichkommt.

Die Einzahlungen erfolgen durch die Vertreter der betheiligten Staaten in Berlin an
die Legationskasse zu Berlin.

Arr. 9. — Die Vertheilung der Beiträge unter den zur Zeit an dem Unternehmen
betheiligten Staaten geschieht nach folgenden Abstufungen :

a) Staaten mit einer Bevölkerung bis zu 5 Millionen zahlen ungefähr 240 M. oder
nahezu 300 frs. jährlich; À

b) Staaten mit einer Bevölkerung von mehr als 5 bis zu 10 Millionen zahlen unge-
fähr 400 M. oder nahezu 500 frs. jährlich,

c) Staaten mit einer Bevölkerung von mehr als 10 bis zu 20 Millionen zahlen un-
gefahr 800 M. oder nahezu 1 000 frs. jährlich;

d) Staaten mit einer Bevölkerung von mehr als 20 Millionen zahlen ungefähr
1800 M. oder nahezu 2 250 frs. jährlich.

    

 

 

% t
Et ns rm ES an A Bai ge
7 né A D le ee ae 5 r a

 

 

 
