 

 

 

145

E d’ uopo notare chei calcoli fra i punti della Svizzera furono eseguiti adoperando
le osservazioni originali alle singole stazioni. Se invece si fa uso delle osservazioni svizzere
compensate, si otliene per

Limidario-Menone con la provenienza dalla base renana . . . log. 4,98417 96
e la differenza col nostro lato salirebbe a 65 della settima decimale.

Giova soggiungere che nel processo verbale della Commissione geodetica svizzera,
del 98 giugno 1885, sono riportate a pag. 10 le diverse provenienze del lato Ghiridone'-Menone,
che noi qui trascriviamo con l’aggiunta dei rispettivi logaritmi.

Partendo dalla base di Bellinzona . . . 38387,61 =, lo 12.00.9101
Deducendolo dalla base di Aarberg . . 3886,41 . . 18315
ld. id. di WemlteWden na ~ 2 =») 1799 9

Valore dedotto dal lato Chasseral-Röthi
adottato da Eschman per la rete geo-

denen suzera  ... soon 2 1760 3
Dall’ antica rete della Landesvermessung  38387,67 . . >» 1917 >
Walle rete renana (hase di Bonn) ...,. 3sos6,0) >» 1796 4

La Commissione svizzera non fu soddisfatta dei risultati ottenuti partendo dalle tre
basi recentemente misurate, ed attribuendo le discrepanze riscontrate ad errori sistematici
nelle osservazioni angolari, dipendenti dalla natura eccezionale del suolo svizzero, avvisd ai
mezzi opportuni di ricerca affine di eliminarli. In attesa intanto dei nuovi risultati svizzeri,
noi possiamo fin d’ora notare come il nostro lato Ghiridone-Menone, che risulta lungo-
38387"168, non si discosti gran fatto dalla media delle risultanze svizzere, con la differenza
sulla settima decimale del logaritmo a meno di 40 unila.

LIVELLAZIONE

Nel 1883 fu livellata a doppio la linea :

Lece -Bergamo-Brescia-Verona-Padova- Venezia, collegandola col caposaldo austriaco
a Deri.
Nel 1884 furono livellate a doppio le linee :
Mestre-Conegliano-Udine.
Udine-Strasoldo (caposaldo austriaco).
Udine-Pontebba (caposaldo austriaco).
Padova-Ferrara.
Casalpusterlengo-Bresciu.

1Tl punto Ghiridone corrisponde al nostro Limidario.
19

 
