 

 

 

88

à cette époque lentière approbation de la Commission; et en second lieu que ces observa-
tions de pendule, faites d'après un plan méthodique, produiraient surtout de bons résultats,
si elles pouvaient être exécutées avec des instruments comparables et comparés. La Commis-
sion a donc prié M. Stebnitzky de rédiger aussi bien le rapport sur les déterminations de la
pesanteur, exécutées dans les trois dernières années, que d'élaborer, jusqu’à la prochaine
Assemblée, un programme complet pour des recherches à l’aide du pendule, d’après un plan
méthodique.

En outre, M. le Secrétaire donne un extrait du procès-verbal de la Commission, d’après
lequel M. Færster à fait une proposition tendant à ce que le Bureau central fût chargé par
la Conférence de coordonner et de discuter les nombreuses observations astronomiques faites
avec des instruments de petites dimensions dans les recherches géodésiques. Vu leur grand
nombre, elles pourraient être utilisées aussi pour l'astronomie, en fournissant un contrôle
précieux des observations moins nombreuses faites avec de grands instruments. Il résulta de
la discussion qui s’ensuivit, que M. Bakhuyzen à été chargé de coordonner ces matériaux
astronomiques, à la condition toutefois qu'aucune limite de temps ne lui sera fixée pour ce
travail, et que le Bureau central lui viendra en aide par l'envoi des publications relatives à
ce sujet.

Toutes les propositions ayant trait aux rapports spéciaux sont adoptées par la Confé-
rence générale, à l'exception de celle concernant les observations de pendule; comme M. le
general Slebnilzky ne consent pas à se charger en même temps du rapport sur les travaux
exécutés jusqu'à ce jour, cet objet est renvoyé à la Commission permanente pour nouvel
examen en vue de propositions ultérieures.

M. de Struve exprime le désir qu’on présente aussi cette fois un rapport spécial sur
la réfraction terrestre, et que M. de Bauernfeind soit invité à se charger de ce travail.

Cette proposition est adoptée par l’Assemblée, dans la supposition que M. de Bauern-
feind, absent en ce moment, y donnera son consentement.

En ce qui concerne les déterminations de la pesanteur, M. de Struve fait en outre
lobservation que des recherches systématiques de cette nature, sur les côtes des mers et les
iles, ne restent pas à l’état de vœu pieux, comme on a eu l'air de le craindre dans la Com-
mission permanente. Non seulement le passé a vu s’exécuter des travaux importants de ce
genre, comme, par exemple, ceux qu'a entrepris consciencieusement et avec un grand succès
l'amiral Lütke, il y a déjà cinquante ans, sur trente points différents, dont douze à quinze
répartis sur des îles de l'Océan; mais on peut espérer, pour l'avenir, une riche collection
de matériaux de cette espèce, grâce au concours des marines des divers Etats.

M. Folie désire attirer l'attention des observateurs appelés à faire des recherches
dans ce domaine sur la question suivante, dont l’importance n'échappe à personne : Existe-t-il
réellement ou non, dans un même lieu, des variations périodiques de l'intensité de la pe-
santeur ? D’après Phypothèse généralement admise de nos jours, de l’existence d’un noyau
liquide au centre de la Terre, il ne serait pas invraisemblable a priori, que des phénomènes

 

 

 

 

  

RP AT FE TO PETE

 

 
