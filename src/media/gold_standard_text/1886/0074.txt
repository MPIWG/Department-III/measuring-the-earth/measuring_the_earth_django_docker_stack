 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

PER GENF EEE IS EET

er nochmals erinnern wolle, das internationale Mass- und Gewichts-Bureau in Breteuil bei
Paris in der Lage und stets bereit.

Herr Ferrero erinnert daran, dass ein ähnlicher Vorschlag, Pendelmessungen nach
einem ausgedehnten Plane auszuführen, bereits von Herrn Faye auf einer früheren Confe-
renz gemacht worden sei, und dass solche Arbeiten übrigens von Anfang an im Plane der
Gradmessung gelegen haben; es scheine ihm daher nicht nöthig, diese Anregung von Neuem
zu wiederholen.

Herr Förster theilt die Meinung von Herrn Stebnitzky : Man könne wohl in dieser
Beziehung ernsthaftere Fortschritte machen. Man solle den Herrn Berichterstatter über die
Schwere-Messungen bitten, einen vollständigen Plan auszuarbeiten, wie man die Beobach-
tungen der Schwere an verschiedenen Theilen der Erde systematisch ausführen könne, ebenso
anzugeben, welche Instrumenten am Besten anzuwenden seien, damit dann später ein ganzes
Programm vorliege.

Herr von Oppolzer stimmt Herrn Ferrero bei und fürchtet, dass mit allgemeinen
Gesichtspunkten hier wenig gedient sei; man müsse besondere Methoden und Instrumente
empfehlen. Hier handle es sich nicht um absolute, sondern um relative Schwerebestimmun-
gen, die ja viel leichter auszuführen seien; vielleicht dürfte man zu diesem Zwecke mit
einem invariabeln Pendel weiter kommen, als mit dem sonst jedenfalls überlegenen Rever-
sionspendel.

Er unterstützt Herrn Förster in der Ansicht, dass der Spezial-Berichterstatter zu-
gleich den Arbeitsplan ausarbeiten möge, und wünscht deshalb, dass Herr Stebnitzky, von
dem die Idee zu diesen über die Erde auszudehnenden Schwerebestimmungen ausgieng, mit
dem gesammten Berichte bstraut werde.

Herr Faye glaubt, dass ein solcher, die ganze Erde umfassender Plan für die Be-
stimmung der Schwere auch insofern von grossem Nutzen sein würde, als man damit die
thätige Unterstützung der Marinen der verschiedenen Länder erreichen würde. So zweifelt
er nicht, dass die französischen Marine-Öfficiere unter solchen Bedingungen sich gerne bereit
finden lassen würden, dem aufgestellten Plane gemäss und nach den empfohlenen Methoden
Schwerebestimmungen auszuführen.

Herr Ferrero glaubt, dass die Anregung zu solcher erwünschten Betheiligung von
Seiten der französischen Marine-Offieiere am natürlichsten von der französischen Gradmes-
sungskommission ausgehen werde.

Herr Stebnitzky erwähnt, dass in Russland ein Plan besteht, wonach binnen Kurzem
Pendelbeobachtungen von Nowaja Semlja bis Archangelsk ausgeführt werden sollen.

Herr Förster schlägt vor, Herrn von Oppolzer wie bisher mit dem Berichte über
die bereits ausgeführten Pendelbeobachtungen und Herrn General Stebnitzky mit der Aus-
arbeitung des Planes für die künftigen Beobachtungen zu betrauen.

Herr von Oppolzer dagegen ist der Ansicht, dass ein einziger Berichterstatter, und

   

EATERS WERL

ne

er EN CS ETE AR SLALOM CTS TAR tig tron

 

 

 

 

 

 

 

 

 

 

   
