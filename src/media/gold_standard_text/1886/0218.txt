 

 

D. BUREAU-ARBEITEN.

Die seit der letzten Allgemeinen Conferenz vom Geodalischen Institut herausgegebe-
nen Publikationen sind bereits erwähnt.

Von astronomischen Arbeiten wurden diejenigen des Jahres 1885 und einige aus
früheren Jahren verbliebene vereinzelte Breiten- und Azimuthbestimmungen reducirt und
druckfertig gestellt. Das Märkisch-Thüringische Dreiecksnetz, welches in den Jahren 1867
bis 1872 durch das Personal des Geodälischen Instituts beobachtet worden war, wurde
ebenfalls zum Drucke vorbereitet, dagegen konnten wegen anderweiter dringlicherer Arbei-
ten die Rechnungsarbeiten für das Ostpreussische Netz nicht bis zu Ende geführt und die-
jenigen für das neue Berliner Basisnetz nur begonnen werden. Die Reduktion der neuen
Grundlinienmessungen in Schlesien und bei Berlin hat eine weitere Förderung seit dem
letzten Bericht nicht erfahren; es wird aber sofort an die Vollendung gegangen werden,
sobald die jetzt eingeleitete Etalonnirung des Basisapparats in Breteuil durchgeführt sein
wird.

Das oben erwähnte Nivellement von Swinemünde bis Cuxhaven ist redueirt und harrt
der Veröffentlichung. Die Untersuchung des Mittelwassers bei Travemünde (vergl. die
Publikationen) ergab das wichtige Resultat, dass, ebenso wie bei Swinemünde aus einem
Zeitraum von vierundfünfzig Jahren, bei Travemünde aus einem solchen von dreissig Jahren
eine säkulare Verschiebung der Küste der Ostsee und ihres Mittelwasser-Standes gegenein-

ander nicht zu erkennen ist. Dagegen treten deutlich periodische Veränderungen hervor,
die denen in Swinemünde und an anderen Punkten der Ostsee annähernd parallel laufen.
Dieser Parallelismus leistete vorzügliche Dienste als Wegweiser bei der Revision des Wasser-
stands-Beobachtungsmaterials in Stralsund und Wieck; mit Hülfe desselben konnte Professor
Dr. Seibt in demselben eine Anzahl von Irrthümern zur Aufklärung bringen und die über
vierzig Jahre zurückreichenden Beobachtungsreihen zur wissenschaftlichen Verwerthung
geeignet machen. Im Laufe dieses Sommers hat auf meinen Antrag der Herr Minister der
öffentlichen Arbeiten sämmtliche Ostseepegel seines Ressorts der wissenschaftlichen Ueber-
wachung seitens des Geodätischen Instituts unterstellt und das z. Th. sehr werthvolle ältere
Beobachtungsmaterial dem Geodätischen Institute zugänglich gemacht.

Im Interesse der Europäischen Längengradmessung unter dem 52. Grad ‚der Breite
wurde in den letzten Monaten des vorigen Jahres und der ersten Hälfte dieses Jahres auf
Wunsch des Direktors der Sternwarte Pulkowa, Herrn Geheimen Raihes 0. von Struve, Exc.,
von dem in seinem Auftrage erschienenen Adjunkt-Astronomen ‚Herrn Dr. Th. Wittram und
dem Assistenten des Geodätischen Instituts Herrn Dr. Börsch eine Sammlung, kritische Un-
tersuchung und Berechnung des geodätischen und astronomischen Materials für die deutschen
Dreiecke unter dem 52. Grad der Breite mit den Anschlüssen an Belgien und Russland aus-
geführt, worüber aus einem bezüglichem Bericht des Herrn Dr. Börsch hier Nachstehendes
Platz finden möge :

 
