|
|

 

Dritte Sitzung,

31. October 1886.

Eröffnung der Sitzung um 11 '/, Uhr.
Vorsitzender : General Ibanez.
Anwesend : Sämmtliche Mitglieder der Commission.

Der Sekretär verliest das Protokoll der letzten Sitzung und resümirt dasselbe in
französischer Sprache. Dasselbe wird mit einer kleinen Richtigstellung von Seiten des Herrn
Förster angenommen.

Mit Bezug auf die an die Commission zurückgewiesene Frage des Berichtes über die
Schwerebestimmungen, schlägt Herr Hersch, nach Rücksprache mit den betheiligten Herren,
vor, Herrn von Oppolzer, wie von Anfang an beabsichtigt, mit dem eigentlichen Berichte
über die ausgeführten Arbeiten zu betrauen, und Herrn General Stebnilzky um die Aus-
arbeitung des Programms für allgemeine Schwerebestimmungen zu ersuchen. Die Commission
ist damit einverstanden.

Herr General Stebnitzky erklärt, dass er sich zunächst darauf beschränken werde,
dass Programm für die Schwerebestimmungen in Russland auszuarbeiten.

Herr Hirsch erwähnt, dass er vom Internationalen Mass- und Gewichts-Gomite beauf-
tragt sei, im Schoosse der Gradmessung zu beantragen, es möchten Schwerebestimmungen
auch in Breteuil und in dem früheren Laboraiorium von Regnault angestellt werden.

General Ibanez ist der Ansicht, dass dies eine Aufgabe der französischen Geodäten sei.

 
