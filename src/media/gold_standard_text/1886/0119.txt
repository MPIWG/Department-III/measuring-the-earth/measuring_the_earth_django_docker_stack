 

na

 

sion de sa visite aux Instituts du Brauhausberg pres Potsdam, et de prendre personnellement
congé de ses Membres.

«Je l’aurais fait d’autant plus volontiers, que les travaux de la Conférence et de
la Commission permanente ont intéressé 4 un haut degré le Gouvernement prussien et qu'ils
ont produit des résultats salués par lui avec une vive satisfaction et une sincère reconnais-
sance.

« La Conférence peut être assurée que le Gouvernement prussien et ses organes se
voueront avec empressement à l’exécution des vœux qu’elle leur a confiés.

« Je vous prie, M. le Président, de bien vouloir communiquer le contenu de cette
lettre à la Conférence et d'exprimer à ses Membres mon désir le plus vif qu’ils n’emportent
dans leurs foyers que des souvenirs agréables de leur séjour à Berlin.

€ (Signé) VON GOSSLER. »

Le M. Président, reprenant la série des rapports d’après l’ordre des pays, demande
d’abord au délégué roumain s'il désire présenter à l’Assemblée un rapport général sur les
travaux exécutés dans sa patrie.

M. le général Falcoïiano répond qu'il n’a pas préparé un rapport détaillé, puisqu'il
assiste pour la première fois à une Conférence générale et que, par conséquent, il n’a pas eu
connaissance des usages existants. Il n'aurait d’ailleurs que peu de choses à ajouter à la
communication qu'il a faite dans la dernière séance. Quant aux déterminations astronomi-
ques de coordonnées géographiques, on n’a exécuté en Roumanie que la différence de longi-
tude entre Kronstadt et Bucharest, avec le concours de PInstitut militaire géographique de
Vienne; M. de Kalmar a déjà mentionné cette détermination dans son rapport. En ce qui
concerne les travaux géodésiques proprement dits, 1l a déjà signalé le dépôt, sur le Bureau
de l'Assemblée, de la carte du réseau de triangles de premier ordre, terminé depuis la der-
niére Conférence.

M. de Struve prend la parole pour lire un rapport détaillé sur les travaux faits en
Russie. (Voir aux Annexes.)

Dans l'introduction de ce document, M. de Struve, en partie pour justifier la forme
un peu différente de celle admise jusqu'ici pour la rédaction des rapports, a exprimé l’opi-
nion que les méthodes suivies et le mode de rédaction usité jusqu'à maintenant devraient
être abandonnés et remplacés par des comptes-rendus répondant mieux au but poursuivi ac-
tuellement par l'Association. Ge but est caractérisé par le titre même: Association géodésique
internationale pour la mesure de la Terre; on tiendrait compte davantage des idées théo-
riques fondamentales, des principales méthodes nouvelles, ainsi que des résultats essentiels
obtenus; et, d'autre part, on éviterait, autant que possible, d'entrer dans de trop grands dé-
tails sur chacun des travaux et mesurages.

Après la lecture du rapport de M. de Struve, M. von Oppolzer demande la parole
pour répondre aux attaques contenues dans l'introduction du rapport du délégué russe
contre les méthodes employées jusqu’à présent dans la mesure des degrés. M. von Oppolzer

 
