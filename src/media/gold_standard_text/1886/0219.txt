 

 

 

 

 

 

195

« Die veröffentlichten Messungen der Triangulation des Königreichs Belgien, des
Rheinischen und Hessischen Netzes des Geodätischen Instituts, der Märkisch-Schlesischen
Kette der Königl. Preussischen Landesaufnahme, der Küstenvermessung und der Verbin-
dungen der preussischen und russischen Dreiecksketten bei Thorn und Tarnowitz, sowie
die von der Königl. Preussischen Landesaufnahme handschriftlich mitgetheilten Resultate
der Hannoversch-Sächsischen Kette und der 1878er Controlmessungen in der Schlesischen
Keite machten es möglich, in einer Kette von siebenundfünfzig gutgeformten und in Bezie-
hung auf das Beobachtungsmaterial möglichst gleichwerthigen Dreiecken von der Anschluss-
seite Peer-Montaigu an die belgische Grundlinie bei Lommel, unter Anschluss an die Grund-
linien von Bonn, Göttingen, Berlin und Strehlen, welche sämmtlich mit dem Bessel’schen
Basisapparat gemessen sind, bis zu den Anschlussseiten Lysiec-Markowice-Grodziec der rus-
sischen Dreiecke zu gelangen. Für die Güte der Beobachtungen geben die Schlussfehler der
Dreiecke einen Anhalt, von denen 40 zwischen 0” und 1”, 15 zwischen 1” und 2”, und 2
zwischen 2” und 25 liegen; die beiden letzteren erreichen die Werthe 22 und 24. Der
mittlere Fehler eines Winkels ergiebt sich aus diesen Schlussfehlern zu + 0.59.

«Um beim Anschluss an Belgien nicht nur auf die eine Seite Roermonde-Ubagsberg
angewiesen zu sein, wurden die preussischerseits 1860-1861 auf den Punkten Roermonde,
Ubagsberg, Erkelenz, Langschoss und Henri-Chapelle eigens zum Zwecke der Längengradmes-
sung mit einem achtzölligen Universalinstrument angestellten Beobachtungen den publieirten
preussischen und belgischen hinzugefügt, und das so um Ubagsberg als Centralpunkt entstan-
dene Polygon einer Ausgleichung unterworfen. Hierzu war es nothwendig aus den Original-
beobachtungsbüchern alle einzelnen Beobachtungen herauszuziehen, zu discutiren und
auszugleichen, sowie eine Menge von Centrirungen zu berechnen. Vorhandene ältere Rech-
nungen waren nicht zu verwerthen. Auf einigen weiteren Stationen, auf welchen Beobach-
tungen aus verschiedenen Zeiten und von verschiedener Seite zur Verfügung standen, wurden
plausible Mittelwerthe abgeleitet. Die darauf durchgeführte Berechnung der Dreiecksseiten
ergab eine sehr befriedigende Uebereinstimmung der oben erwähnten fünf Grundlinien unter
einander. Wenn man eine beliebige Seite der Kette aus allen fünf Grundlinien ableitet, so
beträgt die grösste Differenz der so erhaltenen Werthe kaum on der Länge, während
die grösste Abweichung vom Mittel nur nn der Länge erreicht, Werthe, welche aus der
Unsicherheit der Winkelmessungen allein erklärbar sind. Bei der Geringfügigkeit dieser
Abweichungen wird man von einer Ausgleichung der Grundlinien absehen können, und die
ganze Kette, von einem plausiblen Mittelwerth für irgend eine Dreiecksseite ausgehend,
berechnen dürfen. Durch die Messungen der Königl. preussischen Landesaufnahme von 1878
wurde in den Beobachtungen (der Centrirung) auf der Station Annaberg von 1854 ein Fehler
constatirt, dessen Elimination die früher so befriedigende Uebereinstimmung mit den Wer-
then der russischen Anschlussseiten leider auf etwa = der Länge herabgedrückt hat. Die
in nächster Zeit in Aussicht stehende Vollendung der Königl. Sächsischen Landesvermessung
wird vermittelst der Seiten Keulenberg-Strauch-Collm-Leipzig auch die Grossenhainer Grund-
linie zum Vergleich heranzuziehen gestatten.

« Der geodätische Anschluss der Dreieckskette an die drei ursprünglichen deutschen

 

 
