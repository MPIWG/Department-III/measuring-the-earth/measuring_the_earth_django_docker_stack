 

192

seits Professor Fischer auf Neuenhagen bei Berlin zum Zwecke einer bezüglichen Unter-
suchung eine freiliegende Marke mit in die Beobachtungsreihen aufnahm. Als Regel ist fest-
gesetzt, dass in Zukunft die Richtungen nach Marken mit solchen nach Dreieckspunkten
immer zu denselben Zeiten verbunden werden sollen, zu welchen Azimuthmessungen der

Marken angestellt werden.

3. Trigonometrische Messungen. In den Jahren 1884 und 1885 wurden die 1882
begonnenen Ergänzungsmessungen an den Dreiecksketten von 1858 und 1859 in Ostpreussen
zu Ende geführt; ausserdem wurde in diesen Jahren damit begonnen, die Verbindung der
schlesischen Grundlinie mit den Königl. Sächsischen Dreiecken neu herzustellen, da früher
hierbei nur ein minderwerthiges Instrument zur Anwendung gelangt war. Endlich ist im
Jahre 1885 die 1880 gemessene Berliner Basis mit dem Hauptdreiecksnetz in Zusammenhang
gebracht worden. Im Jahre 1886 sind Dreiecksmessungen nicht zur Ausführung gelangt.

Ueber das Verhalten der beiden Stäbe des Brunner’schen Basisapparates bei Tem-
peraturänderungen hat Herr Professor Fischer 1884 in Ergänzung früherer Beobachtungen
noch einige Versuche mittelst Thermo-Elementen angestellt, die die 1882 erhaltenen, in den
Astronomischen Nachrichten, Nr. 2451, veröffentlichten Ergebnisse bestätigen; doch musste
die Fortsetzung dieser Arbeiten bis zur Erlangung geeigneter Dienst-Lokalitäten verschoben
werden.

Die Etalonnirung des Apparates in Breteuil steht bevor; Professor Fischer hat den-
selben Ende September dieses Jahres nach Paris übergeführt. Entsprechend den von Herrn
General Ibanez mit. seinem Apparat gemachten Erfahrungen wurde Eilguttransport vermie-
den; der Apparat wurde vielmehr, in einem Güterwagen auf doppelten Polstern gelagert,
mit einem gewöhnlichen Güterzug Wansportirt.

4. Höhenmessungen, Fluthmesser und Pegel. Die Beobachtungen an dem registri-
renden Fluthmesser und dem Ablesepegel in Swinemünde sind regelmässig fortgesetzt wor-
den. Auf Ersuchen des Senates der Freien und Hansestadt Lübeck wurde ferner durch das
Geodätische Institut in den Jahren 1884 und 1885 ein registrirender Fluthmesser in Trave-
münde an der Ostsee aufgestellt; ebenso hat dasselbe seit dieser Zeit auf Antrag des Gross-
herzoglich Mecklenburgischen Statistischen Büreaus die wissenschaftliche Ueberwachung
der Pegel zu Wismar und Warnemünde übernommen und an diesen Orten neue Normalpegel
mit auswechselbarer Porzellanskala aufgestellt.

Zur Verbindung der genannten Östseestationen : Swinemünde, Travemünde, Wismar
und Warnemünde, sowie der Pegel zu Wieck und Stralsund unter einander und mit der
Nordseestation Cuxhaven, kam in den Jahren 1883 bis 1885 ein 523 Kilometer langes Präci-
sionsnivellement hin und zurück zur Ausführung.

Aus den Differenzen der beiden Messungen für die einzelnen Theilstrecken ergiebt
sich ein mittlerer Fehler der Endwerthe von + 1""3 auf den Kilometer. Jedoch ist diese
Rechnung insofern hypothetisch, als sich auch auf dieser Linie die schon früher bemerkte
Thatsache herausgestellt hal, dass die Resultate der in entgegengesetzter Richtung geführten
Nivellements in ziemlich regelmässiger Weise auseinandergehen; in ganz besonders auffäl-

 

 

ee

 
