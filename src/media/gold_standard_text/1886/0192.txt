 

 

 

 

 

 

 

An eg

 

Se

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

168

welche letztere, gleichwie im Jahre 1883, das österr. Gradmessungs-Bureau beigestellt hatte,
war um die Mitte des Monates Mai vollendet. Des ungünstigen Wetters wegen konnte jedoch
mit den Beobachtungen erst am 21. Mai begonnen werden.

Die Beobachtungen selbst sind, so wie alle früheren Längenunterschied-Messungen,
nach den Instructionen vorgenommen worden, welche Herr Hofrath von Oppolzer für diese
Arbeiten verfasst hat.

Zur Eliminirung des Personalfehlers wurde bei jeder Längenunterschied-Messung
nach der halben Anzahl gelungener Abende der Wechsel der Beobachter vorgenommen.

Nachdem am 21., 22., 24. und 28. Mai vier Beobachtungsabende in der ersten Lage
der Beobachter vollkommen gelungen waren, wurde an den Abenden des 4., 5., 6. und 7.
Juni mit der 2. Beobachterlage die Bestimmung der Meridian-Differenz Krakau-Kronstadt ab-
geschlossen.

Auf einen etwas längeren Zeitraum vertheilen sich, wegen des ungünstigen Wetters,
die Beobachtungen des Längenunterschiedes Czernowitz-Kronstadt.

Auf der ersteren dieser beiden Stationen war schon während der Beobachtungen auf
der Linie Krakau-Kronstadt ein Observatorium erbaut und eingerichtet worden und began-
nen die Beobachtungen am 13. Juli mit dem 1. gelungenen Abend in der ersten Beobachter-
Lage und wurden am 11. August mit dem A. Abend der 2. Lage beendet.

In Bukarest hatte der königl. rumänische grosse Generalstab für die Erbauung und
Einrichtung eines Observatoriums Sorge getragen. Da dasselbe aber nicht im Besitze eines
den österreichischen Instrumenten conformen Passagenrohres war, so musste die Ankunft des
von Czernowitz dahin abgesendeten Passagenrohres abgewartet werden.

Nach den Eintreffen und Aufstellen des österreichischen Instrumentes begannen so-
fort die Beobachtungen und dauerten, mit Beobachterwechsel, vom 3. September (1. gelun-
gener Abend) bis 17. September (8. gelungener Abend).

B. TRIGONOMETRISCHE ARBEITEN.

Bei Gelegenheit der Triangulirung zur besseren Dotirung des ehemaligen Grossfür-
stenthumes Siebenbürgen — welches noch keinen Cataster besitzt — für die Reambulirung
der Militär-Aufnahme, konnte auch dem Bedürfnisse nach einer Grundlinie im südliche Theile
dieses Landes Rechnung getragen werden und wurde deshalb die Messung einer solchen
in der Ebene nördlich von Kronstadt, ferner die Verbindung dieser neuen Basis mit jener
bei Radautz in der Bukowina durch ein Netz 1. Ordnung in das Arbeitsprogramm für den
Sommer 1885 aufgenommen.

Die für diese Arbeiten bestimmten drei Triangulirungs-Abtheilungen begannen die
Dotirung anfangs Mai.

Sowohl der Zeichenbau wie auch die darauf folgenden Winkelmessungen wurden
bedeutend verzögert durch die heftigen Stürme, häufigen Gewitter und Regen in den Mona-
ten Mai, Juni, Juli, theilweise auch im August; diese Verzögerungen wurden jedoch einiger-

 

 

 

rere ét

 

 

 
