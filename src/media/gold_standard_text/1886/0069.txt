ern ge een mern

 

45

Mittheilungen zu unterbreiten; bei der vorgeschrittenen Zeit zieht er es jedoch vor, die-
selben zu vertagen.

Hiermit erklärt der Herr Präsident die Tagesordnung der heutigen Sitzung und das
Programm der diesjährigen Conferenz für erledigt.

Der General Ibanez bittet ums Wort, um in den wärmsten Ausdrücken Sr. Excellenz
dem Herrn Staats-Minister von Gossler den Dank der General-Gonferenz auszusprechen, für das
von dem Herrn Minister für die Erdmessung bewiesene grosse Interesse, für die von Sr. Excel-
lenz dem Unternehmen während der überstandenen Krisis zu Theil gewordene wirksame
Unterstützung, sowie für die überaus freundliche Gastfreiheit, welche die Conferenz in Berlin
genossen, und endlich für das ehrenvolle und gütige Schreiben, womit Herr von Gossler von
der Conferenz Abschied genommen hat.

Sämmtliche Mitglieder der Versammlung erheben sich von ihren Sitzen, zum Zeichen
der Beistimmung zu diesem Dankesvotum.

Der Herr Präsident spricht zum Schlusse den lebhaften Wunsch aus, dass die Grad-
messung, nachdem sie die durch den Tod ihres Begründers unvermeidlich gewordene Krisis
glücklich überwunden, und eine strengere, internationale Organisation sich gegeben, auch
ferner sich in dieser nach den weitesten Zielen strebenden Richtung entwickeln und gedeihen
möge, zum Nutzen der Wissenschaft und zum leile der allgemeinen Kultur.

General Ferrero dankt, mit allgemeiner Zustimmung der Versammlung, dem Herrn
Präsidenten für seine ebenso gewandte als unparteiische Leitung der Geschäfte.

Die Sitzung, und’somit die Allgemeine Conferenz von 1886, wird um 5 Uhr geschlossen.

 
