 

 

190

Die drei Längenbestimmungen des Jahres 1884 wurden nach einander mit je ein-
maligem Beobachterwechsel an 14 bis 15 Abenden unter Erzielung eines Gewichtes von
42 bis 14 vollen Abenden erhalten. Die beiden Bestimmungen Berlin—Breslau und Berlin -
Königsberg wurden 1885 gleichzeitig mit Hülfe von drei Beobachtern, welche die Stationen
zwei Mal eyklisch wechselten, ausgeführt, während die Bestimmung Rugard-Königsberg für

sich allein erfolgte.
Bei diesen Bestimmungen von 1885 wurde das Gewicht von 12 bis 14 vollen Aben-

den bei 17 bis 18 Beobachtungsabenden erreicht.

Von den drei Längenbestimmungen des Jahres 1886 dienten die beiden ersten :
Kiel-Rugard und Kiel-Berlin zur Ergänzung des Längennetzes, die letzte: Rauenberg-Berlin
zur Verbindung des wenige Kilometer südlich von Berlin gelegenen Gentralpunktes der Königl.
Preussischen Landesaufnahme mit der Berliner Sternwarte. Während bei den ersten beiden
Linien ein Gewicht von 12 vollen Abenden mit ein bezw. zweimaligem Wechsel der Beob-
achter, entsprechend dem gewöhnlichen Verfahren, erzielt wurde, konnte bei der letzteren
Operation wegen der Gunst der Verhältnisse ausser mehrmaligem Wechsel der Beobachter
auch ein solcher der Instrumente stattfinden. Diese ungefähr 20 vollen Abenden äquivalente
Reihe wird somit wenigstens für einen Fall zeigen, welchen Einfluss die kleinen Verschie-
denheiten anscheinend übereinstimmender Instrumente auf die Längenbestimmungen aus-
üben.

Die oben angegebene Vergrösserung des wahrscheinlichen Fehlers der letzteren
durch die Polygonabschlüsse im Vergleich zu dem Werthe, der aus den Unterschieden der
Abendresultate hervorgeht, beruht jedenfalls theilweise auf der Veränderlichkeit der persön-
lichen Gleichung. Es wird möglich sein, über diese Veränderlichkeit bei einer Beobachter-
kombination des Geodätischen Instituts genauere Aufschlüsse zu erhalten, da für die per-
sönliche Gleichung der Herren Professor Albrecht und Richter seit 1874 eine grössere
Anzahl Beobachtungsreihen vorliegt.

Der weitere Ausbau des telegraphischen Längennetzes dürfte sich für Preussen
innerhalb des nächsten Decenniums vollziehen, wenn der der « Uebersicht der Arbeiten etc. »
beigefügte « Allgemeine Arbeitsplan etc. » zur Durchführung gelangt. Es sind dabei auch
sechs Verbindungen mit dem Auslande ins Auge gefasst (Seite 6), von denen an erster Stelle
stehen : Schneekoppe-Dablitz, Berlin-Kopenhagen, Berlin-Stockholm.

2. Breiten- und Azimuthbestimmungen. Im September und Oktober 1884 wurden
auf Wunsch der Königl. Sächsischen Regierung vom Königl. Preussischen Geodätischen
Institut Breite und Azimuth für den sächsischen Gradmessungspunkt Kapellenberg bei Fran-
zensbad ermittelt. In Preussen selbst wurden 1884 nur gelegentlich anderer Arbeiten auf
Zobten in Schlesien Breite und Azimuth, auf Tschelentnig ebenda die Breite beobachtet,

1885 aber keinerlei dergleichen Arbeiten vorgenommen. Dagegen sind 1886 in Ausführung
des « Allgemeinen Arbeitsplanes etc. » zahlreiche Bestimmungen dieser Art gemacht.

Auf dem Rauenberge bei Berlin wurde die Breite nach drei Methoden (1. Vertical,
Zenithdistanzen am Höhenkreise und Zenithdistanzen nach Horrebow-Talcott), das Azimuth
nach zwei Methoden (Universal- sowie Passageninstrument) ermittelt. Zur Untersuchung des

 

à

à
5
à

 
