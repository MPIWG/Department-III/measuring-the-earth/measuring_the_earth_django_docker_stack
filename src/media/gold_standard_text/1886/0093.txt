EEE TEE EEE

ee AE

 

 

69

cette surface, dans lesquels on déterminera la pesanteur, pourra devenir beau-
coup plus considérable sans qu’il résulte de ces travaux une augmentation de
dépenses.

Les nivellements de précision, dont l’exactitude rigoureuse, établie d’abord
par des experts français, était encore pour ainsi dire un sujet d’étonnement et
de doute chez la plupart des membres de la Conférence de 1864, ont poursuivi
leur marche triomphale à travers toute l'Europe, grace a l’influence vivifiante de
l’organisation de l'Association géodésique.

Non seulement toutes les déterminations de mesures et les moyens employés
ont fait des progrès importants sous le rapport de l’exactitude, mais les pro-
blèmes ont été approfondis et élargis, et sont devenus en même temps toujours
plus fertiles en résultats et en promesses pour d’autres domaines des recherches
scientifiques et pour la pratique.

Personne n'aura passé en revue l’exposition de tous ces résultats acquis et
de tous les problèmes posés, que notre collègue felmert à publiée dans ces
dernières années, sans être saisi de la grandeur et de la profondeur de la tâche
à laquelle notre Association voue ses efforts.

Tout récemment encore, des savants appartenant à des domaines d'étude qui
n'ont en apparence aucun rapport avec la géodésie, ont commencé à adresser à
l'Association géodésique des demandes que je mentionnerai brièvement, parce
qu’elles sont un exemple frappant de l’enchainement et de la fécondité de toutes
les recherches sérieuses. On a pu constater que les méthodes et moyens employés
dans la détermination de la tension des gaz, tels qu'ils se rencontrent dans la
thermométrie, la thermodynamique, la météorologie, ete., ainsi que dans de
nombreux problèmes du domaine de la chimie, ne peuvent plus longtemps être
comparés d’un lieu à un autre avec la précision qu’on a déjà obtenue dans ces
déterminations, si la géodésie ne fournit pas, pour chacun des Instituts de
physique et de chimie dans lesquels on exécute des mesures précises et absolues
de cette nature, une détermination directe de lintensité locale de la pesanteur.
La connaissance générale de la forme de la Terre et des variations de l'intensité
de la pesanteur avec la latitude géographique et l'altitude au-dessus du niveau
de la mer, ne suffit plus pour établir une comparaison suffisamment exacte
entre les déterminations barométriques absolues qui sont exécutées dans des
endroits différents.

Comme conséquence des offres faites aux Instituts physiques et chimiques,
et en particulier au Bureau international des poids et mesures, établi à Breteuil,
près Paris, en vue de déterminer l'intensité locale de la pesanteur, l'Association
séodésique s’acquittera d’une partie des obligations qu’elle a contractées envers
l’organisation internationale des poids et mesures, pour les dispositions prises par
elle et ayant pour but la comparaison fondamentale des règles géodésiques de tous
les systèmes avec le prototype international du mêtre.

 
