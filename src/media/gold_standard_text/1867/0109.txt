mi

À PPP TR PT YT Tr

Im u on

m RI

105

lichen Arbeit, welche dadurch hervorgerufen wird, würde es wohl zweckmässiger sein,
die Abweichungen, auf Grund welcher vorzugsweise solche Untersuchungen mit Nutzen
vorgenommen werden könnten, erst abhängig zu machen von jenen Indicien, welche durch
die Bearbeitung des vorliegenden Materials. von astronomischen und geodätischen Be-
stimmungen gewonnen werden können.

Ich erlaube mir weiter noch mitzutheilen, dass Herr Schering darauf hinwies,
dass die durch die Gawss’schen Schöpfungen in der Geodäsie gewonnenen richtigen Ge-
sichtspunkte für die Auswahl der Stationen, wo astronomische Beoachtungen anzustellen
sind, schon in den Verhandlungen der allgemeinen Conferenz ım Jahre 1864 sich an-
gegeben finden, dass aber seiner Meinung nach die vierte Frage des diesjährigen Pro-
gramms jenen Standpunkt zu verlassen scheine. Er glaubt nun die Imnehaltung jener
Vorschläge um so mehr empfehlen zu dürfen, da er gefunden hat, dass wenn die
Beobachtungen in der dort gewünschten Weise ausgeführt worden sind, die Vergleiehung
astronomischer Ortsbestimmungen mit den geodätischen ohne Schwierigkeit so herge-
stellt werden kann, dass keine zum Voraus bestimmte, sondern nur eine näherungsweise
bekannte Gestalt der geodätischen Fläche angenommen wird. Das Princip der Ver-
eleichung beruhte auf einigen allgemeinen, seit kurzem gefundenenen geometrischen
Lehrsätzen, welche solche Gleichungen zwischen geodätischen und astronomischen Be-
stimmungen geben, die ganz unabhängig sind, sowohl von der wirklichen Gestalt der
zu bestimmenden Fläche, als auch von der hypothetischen, den geodätischen Reductions-
Rechnungen zu Grunde gelegten Fläche.

Der eine dieser Sätze ist eine Verallgemeinerung des bekannten, von Laplace
aufgestellten Satzes, der die Azimuthe und Längenunterschiede für die Endpunkte einer
Linie betrifft, die auf einer von der Kugel wenig abweichenden Fläche liegt, eine Ver-
allgemeineruug, die auch Gauss bekannt gewesen zu sein scheint, wie aus seiner im
Jahre 1830 gegebenen Anzeige des Werkes über die am Südabhange der Alpen aus-
geführten astronomischen und geodätischen Operationen geschlossen werden darf.

Der andere Satz bezieht sich auf die durch astronomische Beobachtungen ge-
fundenen Längen und Polhöhen an drei und mehreren Orten der Fläche in ihrer Ver-
eleichung mit den aus geodätischen Messungen gewonnenen Resultaten.

Herr Schering glaubt auf diese Sätze so speciell hinweisen zu dürfen, da sie
zu den sonst schon bemerkten Gründen für die Wichtigkeit der Bestimmung aller drei
astronomischen Constanten des Azimuths, der Polhöhe und der Länge an demselben
Orte und für die Zweckdienlichkeit einer gleichmässigen Vertheilung der astronomischen
Stationen über das ganze trigonometrisch gemessene Gebiet einen neuen, wie ihm scheint,
nicht unwesentlichen Grund hinzufügen.

Die Commission hat diese Mittheilungen des Herrn Schering dankend entge-
gengenommen und glaubt in ihnen auch wohl ein Motiv erblicken zu müssen, welches
für den schon vor drei Jahren aufgestellten Wunsch, dass an mögliehst vielen, gleichmässig

vertheilten Punkten alle drei astronomischen Bestimmungen gemacht werden sollen, sprechen.
In einer weiteren Verhandlung über diesen Gegenstand konnte jedoch die Commission nicht
eingehen, weil eben die Verallgemeinerung der Sätze, von denen Herr Schering sprach,

14

 

 
