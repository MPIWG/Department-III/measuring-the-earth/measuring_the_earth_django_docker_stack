 

|
|
i
;
|
i

 

eT

 

a

von Localattractionen herrühren mögen. In Betracht dieser Sachlage, welche das Be-
dürfniss weiterer Erfahrungen über die mittlere Seehöhe fühlbar macht, beantragt die
Commission:

6. Der dritte Beschluss der Conferenz von 1864 wird dahin erneuert:

„dass die an das Meer grenzenden Staaten Oesterreich, Italien,
Preussen dringend ersucht werden, an möglichst vielen Punkten ihrer
Küsten, womöglich durch Registrirapparate die mittlere Höhe des
Meeres festzustellen“ und

7. „Der Beschluss 4 der Conferenz von 1864, über Höhenmessungen, soll fest-

gehalten werden.“

Neben der Besprechung der allgemeinen Grundsätze für die Behandlung der
Höhenfrage war die Commission in der Lage, der Conferenz noch besondere Wünsche
vorzulegen, durch welche die nivellistische Verbindung der Meere vorbereitet werden
soll. Nach dem Antrag des Herrn /Rirsch soll nämlich der Badischen Regierung, welche
diejenigen Arbeiten namhaft gemacht haben wollte, die ihrer Mitwirkung bei der Grad-
messung zufallen würden, als eine solche Arbeit ein Präcisionsnivellement durch ihr
Land bezeichnet werden, durch welches die Schweiz in Verbindung mit Hessen
gesetzt würde.

Ferner solle nach dem Wunsche des Herrn Weisbach die nivellistische Ver-
bindung der Schweiz mit Sachsen auf dem kürzesten Wege durch Bayern als Aufgabe
der Bayerischen Herren Commissare erklärt werden. Endlich wurde von Seiten des
Herrn [firsch der Wunsch einer Verbindung der Schweiz mit dem Mittel- und dem
Adriatischen Meere über Italien gesäussert.

Schliesslich bringt die Commission zur Kenntniss der Conferenz eine Erklärung,
welche Herr v. Morozowiez auf den Wunsch der Commission über die Ausgleichung der
Widersprüche zwischen den Ergebnissen geometrischer und trigonometrischer Nivelle-
ments formulirt hat. Sie lautet:

„Wo durch Kreuzung geometrischer und trigonometrischer, auf gegenseitig
gleichzeitigen Messungen von Zenithdistanzen beruhender Nivellements dop-
pelte Bestimmungen von Höhenunterschieden entstehen, sind bei der Aus-
gleichung des Höhennetzes die Gewichte ‘beider Bestimmungen in der Art
zu nehmen, wie sie sich aus denjenigen wahrscheinlichen Fehlern ergeben,
welche sich bei jeder von beiden Operationen aus den Abweichungen mehr-
facher Messungen eines und desselben Höhenunterschiedes vom Mittel oder
aus dem Abschluss geschlossener Polygone herausstellen.“

Es ist diese Fassung !zum Theil das Resultat einer kleinen Debatte, welche
zwischen einigen Mitgliedern der Commission stattgefunden hat. Die Commission hielt
es nicht für nothig, deren Details zur Kenntniss der Conferenz zu bringen.

Präsident Baeyer: Ich eröffne zunächst die Debatte über den ganzen Bericht.

Herr Schering: Meine Absicht ist nur, der Conferenz und dem Herrn Vor-

sitzenden der Commission den Dank dafür auszusprechen, dass Herr Sartorius von

 
  

vr sa ae

Pay (eile)

ko ml

1 si Ms hs äh u pus

 
