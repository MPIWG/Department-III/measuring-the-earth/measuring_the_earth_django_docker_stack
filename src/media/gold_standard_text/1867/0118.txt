 

 

Triangulationen Vorsorge treffen, und sobald die Arbeiten weiter fortge-
schritten sein werden, in grôsserem Maassstabe eine Karte, die ausser
den Triangulationen nur wenig Detail zu enthalten braucht, herstellen
lassen.

2. Die einzelnen Bevollmächtigten sind zu ersuchen, soweit es nicht schon ge-
schehen ist, an das Central-Bureau Mittheilung über die in den betreffenden
Ländern bereits ausgeführten oder definitiv projektirten Dreiecksketten zu
machen. Das Central-Bureau möge diese Mittheilungen zu Händen des
Herm Generals v. Fligely in Wien bringen und selbigen ersuchen, mittelst
dieses Materials die bereits gegebenen Exemplare der Scheda’schen Karte
durch Hinzufügung der Blätter für die neu beigetretenen Gebiete zu comple-
tiren, auch wo möglich noch eine Anzahl von Exemplaren des Ganzen für
die Commissare der letztern Länder zur Verfügung zu stellen.

3. Auf Anregung des Herrn Oberst-Lieutenant v. Ganahl wird es noch als
wünschenswerth erkannt, zur allgemeinen Uebersicht der Arbeiten in einer
aus einem einzigen Blatte bestehenden Karte von Europa, die übrigens nur
wenig geographische Details zu enthalten braucht, die von den Triangula-
tionen der Gradmessung bedeckten Gebiete mit Farbe zu bezeichnen, und
die astronomisch bestimmten Punkte, so wie die gemessenen Grundlinien
einzutragen.

Herr Oberst-Lieutenant v. Ganahl spricht die Hoffnung aus, dass Herr
General v. Fligely wohl auf Ersuchen der Conferenz eine solche Uebersichts-
karte würde herstellen lassen.

Präsident Baeyer: Ich eröffne die Debatte und ertheile dem Herrn Professor
Bauernfeind das Wort.

Herr Bauernfeind: Ich erlaube mir die Anfrage zu stellen, ob auch diejenigen
Triangulationen, von denen man nicht annimmt, dass sie für die Europäische Grad-
messung sich eignen, an den Herrn General v. Fligely mitgetheilt werden sollen.

Herr Bruhns: Vorerst noch nicht.

Herr Bauernfeind: Das müsste dann bestimmt ausgesprochen werden.

Herr v. Ganahl: Das ist schon in der früheren Conferenz ausgesprochen.

Präsident Baeyer: Da sich Niemand mehr zum Wort meldet, bitte ich die-
jJenigen Herren, welche gegen die Anträge der Commission sind, sich zu erheben.

Die Anträge der Commission werden einstimmig angenommen.

Herr Bruhns: Der Auftrag der Commission ist also erledigt und die heutige
Tagesordnung damit erschöpft. Es sind aber noch die zwei schon Anfangs gedachten
Anträge eingegangen, die sich auf die Aenderung der Statuten der permanenten Com-
mission beziehen. Ich erlaube mir, diese beiden Anträge heute schon zur Kenntniss
der Herren Conferenz-Mitglieder zu bringen, um morgen desto schneller über diese
Sache hinwegkommen zu können.

Herr Ober-Steuerdirektor Hügel stellt den Antrag, den betreffenden Paragraphen

tt

ak mil ll

to mil

he Abs ea nn à

|
i
{
|

    

TOUL EE
