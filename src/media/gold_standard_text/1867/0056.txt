 

 

|
Ri
i
|

SAS Shee A ENT

 

EEE

ee

Se

RES

SRE

EEE

=

1
rey

52

b) Continuation de la triangulation sur le meridien de Messine jusqu’au Monte
Gargano. \

c) Passage en Dalmatie.

d) Mesure d’une nouvelle base entre celle de Catane et celle de Foggia.

e) Travaux astronomiques dont le professeur Donati vous parlera tout à
l'heure.

Herr Hirsch: Herr Donati wird um seine Berichterstattung gebeten.

Herr Donati: Messieurs! Le Général Ricci vous a déjà dit quel est le plan
que la Commission italienne s’est proposée de suivre, dans les travaux astronomiques,
pour la mesure des Degrés en Europe. Ce plan d’ailleurs n'est autre chose que
l'application des principes qu’on a établis dans notre Conférence générale de 1864.

Mais quoique le plan astronomique soit depuis longtemps complètement arreté, C’est
toute autre chose pour ce qui a rapport à son exécution. Si le choléra et la guerre
ont pu interrompre chez nous les opération géodésiques, ces mêmes causes ont, à plus
forte raison, influencé les travaux astronomiques. Pour la partie géodésique nous avons,
en Italie, un corps déjà organisé depuis longtemps; c’est notre Etat-major; maïs ce n’est
pas la même chose pour cette partie de l’astronomie qui a rapport à la geodesie. Pour
cette partie, il fallait tout organiser. Vous comprenez sans peine, Messieurs, que dans
les dernières années, nous n'avons pas eu le loisir de nous occuper d’une telle organisation.
C’est pour cela que, je le dis avec regret, mais enfin il faut bien le dire, en Italie, nous
n’avons rien fait pour la partie astronomique, qui se rattache aux travaux géodésiques, objet
de notre réunion. Mais pourtant, j’ai pris la parole, pour vous assurer, Messieurs, qu'à
présent, même en Italie, on pourra bientôt faire les observations astronomiques, qui sont
nécessaires à notre entreprise.

Le professeur Schiaparelli, à Milan, a déjà pourvu son observatoire, d’un in-
strument portatif de passage d’Ærtel, et d’un chronographe éléctrique de Æipp, pour la
détermination des différences de longitude. Notre Etat-major est déjà en possession
de deux altazimuths à microscopes d’Ærtel: ces deux instruments sont trop grands et
trop lourds pour les observations géodésiques, et on pourra les employer utilement pour
les observations astronomiques.

Mais tout cela serait bien peu de chose, si je ne pouvais encore ajouter que
dernièrement, avant de partir de Florence, le Général Ricci et moi, nous avons eu une
entrevue au ministère de linstruction publique, et qu'on nous a assuré d’une manière
formelle, qu’on pourra bientôt acheter les instruments astronomiques qui nous sont né-
cessaires, et que je puis déjà prendre des engagements avec les constructeurs; et qu’en
même temps on nous fournira tous les autres moyens qui jusqu'à présent nous ont
fait défaut.

Ainsi je ne doute pas Messieurs, qu’en Italie aussi la partie astronomique
commencera à marcher aussi promptement, qu’on peut le desirer pour l’accomplissement
de cette grande et noble entreprise.

TA LL

Lab nike

jai tu mil

us pm he à ad Aa LA il nd maudit pus à:

sonate names sil |

 
