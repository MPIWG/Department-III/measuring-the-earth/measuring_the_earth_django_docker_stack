MRC

 

Ir

EN

Herr Bruhns: Herr Börsch hat das Wort.

Herr Börsch: Die geodätischen Arbeiten in dem #ormaligen Kurfürstenthum
Hessen sind bereits in den Generalberichten der Jahre 1864 —66 gröstentheils ent-
halten; sie beziehen sich auf die trigonometrischen Bestimmungen des Hauptdreiecks-
netzes sowohl in horizontalem als auch vertikalem Sinne. Nur in Bezug auf die im Ge-
neralberichte von 1866 aufgeführten trigonometrischen Höhenbestimmungen sei es mir
gestattet, noch einige Worte hinzuzufügen. Mein ausführlicher Bericht des genannten
Jahres enthält nicht allein die Theorie meiner Höhenausgleichung, sondern auch die voll-
ständige Berechnung selbst, derselbe konnte jedoch nach den Verhandlungen der ersten
allgemeinen Conferenz nur im Auszuge in den Greneralbericht aufgenommen werden.
In diesen Auszügen sind nun (G.-B. pag. 33) die vorläufigen Meereshöhen der Haupt-
höhenpunkte, ihre Verbesserungen und die definitiven Meereshöhen aufgeführt, es würde
aber ganz falsch sein, wenn man diese Verbesserungen als ein Maass der Genauigkeit
betrachten wollte, indem sie nur die kleinen Grössen bezeichnen, welche den willkür-
lich gewählten vorläufigen Meereshöhen zugefügt werden müssen, um die definitiven zu
geben; die eigentlichen Fehler sind die den einzelnen berechneten, in dem Auszuge
jedoch nicht mitgetheilten, Höhendifferenzen nach der Ausgleichung beigesetzten
Correctionen.

In dem vergangenen Sommer wurde den geodatischen Arbeiten noch ein, jedoch
erst theilweise vollendetes geometrisches Hauptnivellement hinzugefügt, welches, von Null
des Fuldapegels in Kassel ausgehend, in zwei Armen durch ganz Kurhessen sich hin-
ziehen und bei Hanau oder Frankfurt wieder vereinigen soll. Die Eisenbahnen boten
hierzu die passendsten Linien und sind die Arbeiten längst derselben bis Marburg be-
ziehungsweise Fulda bereits vorgeschritten. Von passenden Fixpunkten aus wird dann
durch trigonometrische Höhenmessungen an die trigonometrisch bestimmten Höhenpunkte
angeschlossen werden. Zu diesen Nivellements wurden zwei grosse Nivellirinstrumente
aus dem mechanischen Institute von F. W. Breithaupt & Sohm in Kassel benutzt;
dieselben haben 18 Linien Objectivöffnung mit 18 Zoll Brennweite, 40facher Ver-
grösserung des Oculars und eine Aufsatzlibelle von 10. Sekunden Angabe; ausserdem
gehört zu jedem Instrument noch ein zweites Ocular von 54facher Vergrösserung und
eine zweite Libelle von 5 Sekunden Angabe. Die Construction des Instrumentes ist
der Art, dass das Fernrohr, auf je einem Stahlkeile und einer stahlernen Corrections-
schraube ruhend, im Sinne seiner Längenaxe umgelegt, so wie die Libelle umgesetzt
und dadurch jeder Fehler des Instrumentes unschädlich gemacht werden kann. Das
Fadennetz ist auf Glas gezogen und enthält ein Kreuz und ein Paar Parallelstriche zum
Distanzmessen, bietet also ein einfaches Mittel, genau aus der Mitte nivelliren zu
können. Die Nivellirlatten sind 2,5 Toisen lang mit 0,002 unmittelbarer Ablesung und
leichter Schätzung bis zu 0,0005 Toisen; zur genauen senkrechten Stellung sind an den
Rückseiten Dosenlibellen angebracht.

Es wurde mit nur wenigen, durch die Oertlichkeit bedingten Ausnahmen, stets
genau aus der Mitte nivellirt, und dadurch, dass man durch Umlegen des Fernrohres
Umsetzen und der Libelle die Fehler des Instrumentes beseitigte und stets zwei Rück-

 

 
