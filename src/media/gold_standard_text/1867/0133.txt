Inn wen em or oe =>

 

RT

129

Herr Lindhagen: Vielleicht ware es geeignet, statt des Ausdrucks ,,vom rein
wissenschaftlichen Standpunkte“ ganz einfach zu sagen „für wissenschaftliche Zwecke“.

Präsident Baeyer: Ist Herr Seidel damit einverstanden?

Wird bejaht.

Herr /hirsch: Ich habe gegen diese Fassung nur das eine Bedenken, dass es
den Anschein gewinnt, als ob die Conferenz der Ansicht wäre, dass für alle andere als
wissenschaftliche Zwecke das Meter nichts tauge; ich möchte in gleicher Absicht, wie
Herr Lindhagen, vorschlagen zu sagen:

„Die Conferenz spricht sich im Interesse der Wissenschaft für das Meter aus.“

Der Antrag würde danach lauten:

„Da unter den möglicherweise in Betracht kommenden Maassen das Meter
die grösste Wahrscheinlichkeit der Annahme für sich hat, so spricht sich
die Conferenz im Interesse der Wissenschaft für die Wahl des metrischen
Systems aus.“

Herr Hansen: Da möchte ich mir doch eine kurze Bemerkung erlauben. Die
Conferenz soll hier also erklären, sie spreche sich für das Meter aus, welches die grösste
Wahrscheinlichkeit der Annahme für sich hat. Wenn nun aber das Meter nicht diese
Wahrscheinlichkeit für sich hätte, würde sich dann die Conferenz nicht für dasselbe
aussprechen? Es ist dies ein Sinn, der zwar nicht in dem Antrag liegt, der aber doch
leicht hineingelegt werden kann. Es handelt sich hier aber um einen Wunsch, der
auch im Interesse der Wissenschaft ausgesprochen werden soll. Wir müssen hier er-
klären, dass in jedem Falle, wie auch die Wahl des allgemeinen Maasses für den
Verkehr ausfällt, das Meter in wissenschaftlicher Beziehung das wünschenswerthere sei.

Herr Hirsch: Ich hoffe, die Befürchtungen des Herrn Vorredners heben zu
können. Die Mitglieder der Commission, welche die Geschichte dieser Proposition
und ihrer Fassung kennen, wissen, wie es gekommen, dass gerade diese Fas-
sung beliebt worden. Die Absicht ist nämlich folgende gewesen: Zunächst wollte
man sagen, ein einheitliches Maass ist jedenfalls im Interesse der Wissenschaft
nothwendig; dann ferner, welches Maass gewählt würde, sei gleichgültig; und um
der Ansicht derjenigen Mitelieder unserer Vereinigung, welche meinen, dass das
Meter an sich keinen Vortheil vor den übrigen Maasssystemen, vor der Toise oder dem
Enelischen Fuss u. s. w. habe, Rechnung zu tragen, beschränkte sich die Commission
darauf, bei der vorausgesetzten Gleichwerthigkeit sämmtlicher Maasssysteme nur an das
Patan zu erinnern, dass das Metersystem allein als dasjenige zu erachten sei, welches
die Wahrscheinlichkeit der allgemeinen Annahme für sich habe, und die Commission
wünschte dies als Hauptgrund ihrer Empfehlung hervorzuheben. Um aber auch ferner
— und das ist gerade auf den Vorschlag unseres Präsidenten, des Herrn v. Struve, ge-
schehen — uns nicht den Anschein zu geben, als ob wir unser Mandat überschritten,
und allgemein giiltige Behauptungen teen zu denen wir nicht berechtigt sind,
hat die Commission diesen Zusatz „von rein wissenschaftlichem Standpunkte‘ vorge-
schlagen oder, wie er jetzt, um den Bedenken des Herrn Seidel und des Herrn Zindhagen

17

 

 
