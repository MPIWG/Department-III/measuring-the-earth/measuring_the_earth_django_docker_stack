 

Te VSS RTE TIE Toes ot

1: NET TS gem HT ns

PTET

ve?

   

al

in der ersten Generalconferenz der verehrlichen Versammlung hierbei das „Memoire*
über die von Eckhardt und Schleiermacher im Jahre 1808*) ausgeführte Messung der der
Triangulirung des Grossherzogthums zu Grund gelegten Basis: „Darmstadt-Griesheim“
ergebenst zu überreichen. Dasselbe ist eine wortgetreue Abschrift des von Schleiermacher
(+ 1844) hinterlassenen, in französischer Sprache abgefassten Manuscripts und ist so
ausführlich, dass es wohl keiner weiteren Erläuterung mehr bedarf. Nur bemerke ich,
dass die erste Rechnung die Länge der Basis zu 3976,088 Toisen ergab, während bei
einer späteren Revision der Rechnung, welche Schleiermacher mit Rücksicht auf spätere
drfahrungen über den Ausdehnungs-Coefficienten noch selbst vorgenommen hat, die
Länge der Basis um 0,001 Toise geringer, also zu 3976,087 Toisen resultirte.

Ueber die zur Ausführung der Beschlüsse der ersten Generalversammlung in
den letzten 3 Jahren im Grossherzogthum vorgenommenen Arbeiten habe ich vorläufig
nur zu berichten, dass in Folge des Beschlusses wegen Ausführung von Nivellements
erster Ordnung im Grossherzogthum Hessen bereits im Jahre 1865 circa 16 Meilen
Eisenbahnen, und zwar die Main-Neckarbahn von Frankfurt bis zur Badischen Grenze
bei Heppenheim, die Main-Rheinbahn von Darmstadt bis an den Rhein bei Mainz und
die linksmainische Mainz-Frankfurter Bahn nivellirt worden sind. Leider konnten diese
Nivellements im Jahre 1866 wegen des Kriegs und im laufenden Jahre wegen anderer
unvorhergesehener Hindernisse nicht fortgesetzt werden, sollen aber im nächsten Jahre
mit um so grösserem Eifer wieder aufgenommen werden. Die angegebenen Strecken
wurden zweimal, einzelne a gone sogar dreimal nivellirt und u. die gemessenen

mesure de la base entre Darmstadt et Griesheim.“

Präsident Baeyer: Ich spreche Herrn Hügel den Dank der Versammlung aus.

Herr Hirsch: Zunächst hat Herr Rice? das Wort.

Herr Ricci: Je ne vous ferai pas, Messieurs, une exposition ‚detaillee de l’état
ott sont les travaux géodésiques en Italie: j'ai fait un exposé de ces travaux à la réu-
nion de la Conférence en 1864 et je me borne aujourd’hui à indiquer ce qu’on afait depuis.

Le gouvernement Italien ayant approuvé les engagements pris par la Com-
mission à la Conférence générale d'Octobre 18 ;4, nous avons cru convenable de nous
réunir à Turin au mois de Juin 1865 afin de fixer les bases des opérations que nous
avions à exécuter et combiner l’ordre de nos travaux.

Le procès-verbal de cette réunion a été communiqué à la Commission permanente
à Leipzic en Septembre 1865 par notre collègue le Professeur Schiaparelli lors de sa réu-
nion annuelle.

Je ne répéterai pas ici tout ce qui est consigné dans ce procès-verbal, mais je
pense qu'il soit utile de résumer brièvement les résolutions plus importantes et d'annoncer
la partis que les circonstances nous ont permis de réaliser.

*) In meinem in der Sitzung des ersten geodätischen Congresses vom 18. October 1864 erstatte-

ten, in dem Generalbericht vom Jahre 1864 Seite 15 und 16 abgedruckten kurzen Berichte beruht die
Angabe, dass die Basismessung im Jahre 1809 ausgeführt worden sei, auf einem Druckfehler.

6

  
