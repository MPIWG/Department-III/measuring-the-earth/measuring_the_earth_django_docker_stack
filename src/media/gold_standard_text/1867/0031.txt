 

rer ry

et“

27

auch dergleichen Differenzen an Stellen, wo nach Lage der Akten keine Signalände-
rungen vorkamen, und in solchen Fällen wird es unentschieden bleiben, woher die
Abweichungen rühren. :

Bei Errichtung der künstlichen Signale auf den Hauptdreieckspunkten bat man
leider auch bei uns in Bayern, wie in anderen Ländern, die Versicherung dieser Punkte
nicht immer so solid in Hausteinen ausgeführt, wie es für deren Erhaltung auf die Dauer eines
halben oder ganzen Jahrhunderts nöthig gewesen wäre. Daher rühren grösstentheils die
eben erwähnten Winkeldifferenzen, und es lässt sich voraussagen, dass bei der Ver-
wandlung der hölzernen Signale in massive, welche die königliche Steuerkataster-Com-
mission nunmehr vornehmen lassen will, die Identität der Punkte abermals beeinträchtigt
werden wird.

Das System der Dreiecksberechnung, welches in Bayern seit dem Jahre 1811
ausschiesslich zur Anwendung kam, rührt in allen seinen Theilen von Soldner her, der
darüber ein Manuscript hinterlassen hat, das in dem schon genannten Werke über die
Bayerische Landesvermessung vollständig abgedruckt werden wird. Dieses System hat
nur die Triangulation eines Landes von der Grösse Bayerns im Auge und entspricht,
von diesem Gesichtspunkte aus betrachtet, allen wissenschaftlichen Anforderungen. Zu
bedauern ist nur, dass es nicht auch eine Anleitung zur Ausgleichung der unvermeid-
lichen Beobachtungstehler enthält.

Soldner legt seiner Dreiecksberechnung eine Kugel zu Grunde, welche die
Meeresfläche in dem Centralpunkte München berührt und deren Halbmesser die Nor-
male dieses Punktes ist. Die Wahl dieses Halbmessers gewährt den Vortheil, dass,
bei der angegebenen Beschränkung des Netzes, die Ausdrücke für die Azimuthe der
Dreiecksseiten und die geographischen Längen der Punkte keiner Correction wegen der
Abplattung der Erde bedürfen. (Wo diese noch vorkommt, ist sie zu 7%,, ange
nommen.)

Als Soldner seine Theorie einer Landesvermessung entwarf, war die Delambre’sche
Methode der Dreiecksberechnung vorherrschend im Gebrauche und der Legendre’sche
Satz noch unbekannt. Soldner verkannte den Vorzug dieser Methode bei Gradmessungen,
nämlich die Distanzen ohne genaue Kenntniss der Abplattung der Erde richtig finden
zu lassen, nicht, hob aber mit Recht hervor, dass bei einer Landesvermessung sphärische
Coordinaten berechnet oder andere bestimmte Eintheilungen der krummen Erdoberfläche
getroffen werden müssen, um Ordnung in die Detailmessung zu bringen; eine Forderung,
welche die Sehnenmethode von Delambre nicht oder nur schwer erfüllen könne.

Da der Anwendung der sphärischen Trigonometrie auf die Berechnung terrestri-
scher Dreiecke bekanntlich nur der Umstand entgegensteht, dass die Logarithmentafeln
nicht genau genug sind, um aus dem Sinus eines Bogens den Bogen selbst im Längen-
maasse hinreichend scharf zu bestimmen, so fand sich Soldner veranlasst, eine Hılfstafel zu
entwerfen, welche für die Grösse und Lage des Bayerischen Dreiecksnetzes ohne alle
Mühe den Logarithmus des Bogens aus dem Logarithmus seines Sinus zu finden gestattet.
Somit wurden alle Bayerischen Dreiecke sphärisch berechnet.

Soldner hat nicht versäumt, den Einfluss zu untersuchen, welchen die Abplat-

At

 
