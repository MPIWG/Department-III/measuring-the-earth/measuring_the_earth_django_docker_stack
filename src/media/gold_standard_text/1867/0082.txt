 

18

Vierte Sitzung

zweiten Conferenz der Mitteieuropäischen Gradmessung.

Berlin, Donnerstag, den 3. October 1867.

Anfang der Sitzung: 12 Uhr 15 Minuten.

D der. Herr Baeyer, Schriftführer: die Herren Bruhns und Hirsch.

Präsident Baeyer: Ich erkläre hiermit die Sitzung für eröffnet und theile der Ver-
sammlung mit, dass der Geheime Regierungs-Rath Herr Briz uns die Ehre erweist, unserer
heutigen Sitzung beizuwohnen. Ehe wir zur Tagesordnung übergehen, bemerke ich, dass
yon der Direction des Berliner Clubs die Eintrittskarten mit einem Schreiben des
Herın Geheimen Raths Maclean in Bezug auf die Zeitdauer der Gültigkeit der Karten
eingegangen ist. Wir gehen jetzt zur Tagesordnung über und zwar zur Berichterstat-
tung. einiger Commissionen. Die zweite Commission hat zuerst ihren Bericht angemel-
det und ich bitte den Präsidenten derselben, Herrn Geheimen Rath Dove, die Güte zu
haben, die Berichterstattung zu veranlassen.

Herr Dove: Herr Herr wird als Referent dieser Commission den Bericht er-
statten. .

Herr Herr: Meine Herren! Die zweite Commission hat über zwei Fragen des
Programms Bericht zu erstatten und die erste derselben betrifft die Intensitätsbestim-
mungen der Schwere. In Bezug auf diesen Punkt wurde von der ersten allgemeinen
Conferenz vor drei Jahren der Beschluss gefasst, dass Bestimmungen der Intensität der
Schwere, beziehungsweise Messungen der Pendellängen, an möglichst vielen. astrono-
mischen Punkten auszuführen wünschenswerth sei, und dieser Aufforderung ist bisher
die Schweiz nachgekommen, wo an drei verschiedenen Punkten: in Genf, Neuenburg
und auf dem Rigi, Messungen der Pendellänge und zwar mit einem nach den Prin-
cipien von Bessel durch Repsold construirten Apparat ausgeführt worden sind. Von
diesen Messungen sind jene von Genf bereits publicirt und liegen die Resultate in einer
Abhandlung von Herrn Plantamour vor, welche an die N fehrzahl der Herren Mitglieder
der Conferenz gesandt worden ist. Es geht aus diesen Arbeiten der schweizerischen
Commission hervor, dass die Bestimmung der Pendellänge mit dem Repsold’schen Ap-
parate mit Leichtigkeit ausgeführt werden kann und die Commission ist daher um so
mehr bestärkt worden, den Beschluss der ersten Conferenz: „dass Messungen von Pendel-
längen an vielen astronomisch zu bestimmenden Punkten vorzunehmen, wünschenswerth
sei“, aufrecht zu erhalten. Die Mittheilungen, welche Herr Hirsch in der Commission

gemacht hat, sind in jeder Beziehung interessant und ich erlaube mir daher, der Con-
ferenz das Wesentlichste darüber zu referiren. Der Apparat leistet in Bezug auf Ge-

    

re emt Lem 1 HA

bu mi | al nl

Wit

À at hell dn in, Mdina nous pus à

2.1 setuatt ant, name DEA |

 
