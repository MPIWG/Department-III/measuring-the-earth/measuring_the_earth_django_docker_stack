PHARES

 

sam ann

Im ART

139

den wesentlichen Theilen nur dringend empfehlen können. Es hat auch bei dieser
Gelegenheit nicht an einer Stimme gefehlt, welche vor einer nicht nothwendig gebo-
tenen Ausdehnung der ohnedies umfangreichen Aufgabe der Gradmessung gewarnt
und auf die Gefahr hingewiesen hat, dass in Staaten, welche nicht über die nöthigen
Kräfte verfügen, die anderen Aufgaben der Gradmessung beeinträchtigt werden möchten.
Dabei wollte man nach eigenen Erfahrungen über die relative Genauigkeit des geome-
trischen und trigonometrischen Nivellements, den Nachdruck, welcher in Punkt 1. der
obigen Beschlüsse auf das erstere gelegt ist, nicht für gerechtfertigt erkennen. Dagegen
wurde andrerseits auf den Umstand aufmerksam gemacht, dass die Höhenfrage neben
der nahen Beziehung, in welcher sie zur Hauptaufgabe der Gradmessung steht, von
geographischer Seite Gesichtspunkte darbietet, welche geeignet sind, thr das allgemeine
Interesse zuzuwenden. Ferner wurde auf die in der Schweiz, Sachsen, Mecklenburg,
Hessen gewonnenen Ergebpisse hingewiesen, die für das geometrische Verfahren sprechen,
indem die Elimination der Instrumentalfehler und des Einflusses der Strahlenbrechung
bei der Operation aus der Mitte nach Herrn Zirschs Angabe die Genauigkeit des geo-
metrischen Nivellemens viermal grösser als die des trigonometrischen erscheinen lässt.
Demnach beantragt die Commission der Conferenz zur Annahme:

1. „Die in mehreren Ländern, namentlich in der Schweiz, Mecklenburg, Sachsen,
Hessen ausgeführten Nivellements haben so günstige Resultate geliefert, dass
die Conferenz ihren vor drei Jahren gefassten Beschluss wiederholt, die geo-
metrischen Nivellements mit Anwendung der Operationsmethode aus der
Mitte auf das Dringendste zu empfehlen und dieselben namentlich für die
Verbindung der verschiedenen Meere für unentbehrlich zu erklären.“

Da diesem Antrag die Ueberzeugung von der überwiegenden Genauigkeit des
geometrischen Nivellements zu Grunde liegt, so hielt es die Commission für geboten,
sich über gewisse Normen auszusprechen, welche für die Anwendung desselben bindend
sein sollen, und legt demnach der Conferenz folgende weitere Anträge zur Beschluss-
nahme vor:

2. „Die bei dieser Operation verwendeten Latten sollen nicht nur auf ihre Thei-
lungsfehler untersucht, sondern es soll auch entweder ihre absolute Correction
oder wenigstens ihre Gleichung genau ermittelt werden.  Dieselben sollen
mit Dosenlibellen und Senkblei zur Verticalstellung der Nivellirlatten versehen
sein, und die Unveränderlichkeit ihres Standes während der Drehung ist durch
eine besondere Vorrichtung zu garantiren.“

3. „Die Controle bei dieser Operation soll durch polygonalen Abschluss der
Stationen, wobei die Polygone nicht zu gross anzunehmen sind, und wo
möglich auch durch mehrfache Nivellirung derselben Linien erzielt werden.‘

4. „Die seither erzielten Resultate erlauben, die bei dem geometrischen Nivelle-
ment erreichbare Genauigkeit so zu definiren, dass der wahrscheinliche
Fehler der Höhendifferenz zweier um ein Kilometer entfernter Punkte
im Allgemeinen nicht drei Millimeter und in keinem Falle fünf Millimeter

überschreitet.“

18°

 

 
