 

4
a

EE

Sa

EE SER

Ep

FREE

EM AME LEST

:
i
:
;
4
:

jenigen Herren, welche gegen den Bericht stimmen, sich zu erheben. Es erhebt sich
Niemand, der Bericht ist daher von der Conferenz ohne Widerspruch angenommen.

Herr Peters: Ich bitte Herrn Förster über den Punkt 1 des Programms zu
berichten.

Herr Förster: Die Commission hat in ihrer Berathung über die Ausführung
und die Erfolge der in dem Berliner Programme (1864) festgesetzten Vorschriften bei
den Polhöhen-, Azimuth- und Längenbestimmungen an die Zusammenstellungen jener
Vorschriften sich angeschlossen, welche in meinem ersten Referat zur besseren Uebersicht
aufgestellt worden war. Dort findet sich unter „III. Astronomische Fragen“ pag. 6
zuerst unter der Rubrik A. folgender Passus:

„In einem Schreiben des Herrn Professor Argelander an Professor Förster,
welches der Conferenz vorgelegt wurde, sind für die Polhöhen- und Längenbestimmungen
die höchst bedeutsamen Vorschläge gemacht worden, dass womöglich alle Bestimmungen
dieser Art über das ganze zu untersuchende Areal der Mitteleuropäischen Gradmessung
von denselben Beobachtern, etwa vier an der Zahl, und mit möglichst identischen In-
strumenten ausgeführt werden möchten. Die Conferenz hat den mustergültigen Cha-
rakter eines solchen Verfahrens anerkannt und dasselbe in der ihr vorgelegten Fassung
in den Bericht aufzunehmen beschlossen. Sie verkennt aber die Schwierigkeiten und die
Verzögerungen nicht, welche die Wahl jenes Verfahrens mit sich bringen würde und
beschliesst zunächst nur die Erstrebung einer solchen Gleichmässigkeit innerhalb der
einzelnen Messungsgebiete dringend zu befürworten.“ Die Commission war in Bezug
hierauf der Ansicht, dass noch keine Veranlassung hervorgetreten sei, die Proposition von
Argelander in umfassenderer Weise für das ganze Arcal der Gradmessung zur Geltung
zu bringen, dass hingegen auf’s Neue angerathen werden müsse, innerhalb der grössern
nationalen Gemeinwesen möglichst nach den Vorschriften von Argelander zu verfahren
und jedenfalls für die verschiedenen Beobachter und Instrumente benachbarter Gebiete
besondere Vergleichsoperationen zur Sicherung der Uebergänge anzustellen.

Innerhalb der Commission wurde bezüglich der Argelander’schen Vorschläge
noch die Ansicht geäussert, dass dieselben wahrscheinlich erst in einem spätern Stadium
der Mitteleuropäischen Gradmessung für einzelne Meridiane und Parallele sich zur Con-
trole des Ganzen empfehlen möchten.

Der Abschnitt B. jenes Referats enthält die Vorschrift für Breiten-, Längen- und
Azimuthbestimmungen. Ich werde mir jetzt erlauben, bei jedem einzelnen Punkte
die gewünschten Zusätze gleich aufzuführen.

1. a) lautet folgendermaassen :
„Für Breitenbestimmungen ist es nothwendig, den Einfluss der Bie-
gungen durch Messung correspondirender nördlicher und südlicher Circum-
meridian-Zenithdistanzen gut bestimmter Sterne mit Anwendung von Uni-
versalinstrumenten oder Verticalkreisen aufzuheben. Um den Einfluss der
Fehler der Refractionsberechnung zu vermindern, wäre es rathsam, nicht über
20° Zenithdistanz hinauszugehen. Abweichungen davon werden durch die

  

Ta Ta RT

Io mi | ak nl

in)

u ind dn, hg dial data ll aly fy ie 19

ae aa sid |

 

 
