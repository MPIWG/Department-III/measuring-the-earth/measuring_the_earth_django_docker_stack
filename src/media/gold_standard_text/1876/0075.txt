In me mem ess don 24

 

WiLL WT

i
x
à
=
=
=
z
x
=
=
:
a

 

65

 

 

et peut étre retirée de temps en temps pour le méme motif. Mais comme elle est en
cuivre elle est beaucoup moins sujette aux incrustations. Cette partie inférieure se
termine par un cône dont le sommet ouvert constitue la seule voie pour l'entrée et la
sortie du liquide. La section de cette ouverture est calculée de telle sorte que, même
à l’époque où la.marée monte avec la plus grande rapidité, le niveau dans le tube ne
puisse jamais être en retard sur le niveau extérieur de plus d’un centimètre, limite de
précision qu'il est inutile de vouloir reculer d'avantage. De cette manière, les ondulations
des vagues sont amorties presque totalement. Il est d’ailleurs bon que le diagramme
garde quelque trace de l'agitation de la surface, cette agitation constituant un ren-
seignement utile pour la météorologie.

Maréographe enregistrant à distance.

Supposons qu'il s'agisse d'enregistrer les variations de niveau qui se produisent
en un endroit peu accessible. On coulera en cet endroit une petite boîte étanche ren-
fermant un manomètre, car ici je me propose de mesurer, non plus la hauteur, mais
la pression de l’eau à une profondeur donnée, quantités qui peuvent d’ailleurs se déduire
facilement l’une de l’autre. Cette boîte, outre le manomètre et son aiguille indica-
trice Z contient encore:

1. Une aiguille observatrice O, isolée électriquement de la première par son
axe et dont la position normale est le zéro du cadran du manomètre; |

2. Une roue & rochet Re, dont l’axe porte l'aiguille O;

3. Une fourche F' qui, agissant sur la roue à rochet, la mettra en mouvement
en même temps que l'aiguille O, toutes les fois qu’une succession de courants émanés de
la pile P, traversera l’électro-aimant A,, quatrième et dernier organe important du
mécanisme de la boîte étanche, coulée au fond de la mer en un endroit choisi.

À terre maintenant nous avons:

1. Un cylindre vertical e dont l’axe constitue l’un des mobiles d'un mouvement
d'horlogerie momentanément embrayé (et que je n'ai pas figuré sur le schema);

2. Un burin 2 placé devant ce cylindre, et qui, porté par l’armature dun
électro-aimant A+, marquera un point sur le cylindre toutes les fois qu'un courant
momentané, parti de la pile P, traversera le circuit de l’électro-aïmant 4. Mais ce
circuit est momentanément interrompu par l’écartement qui existe entre l'aiguille obser-
vatrice O et l’aiguille indicatrice Z; enfin

> A Vaxe du cylindre est attachée une roue Mm portant 2 fois plus de
dents que n’en possède la roue R,; puis 2 frotteurs F' et F’, dont l’un est toujours en
contact avec la roue 24, tandis que l’autre ne la touche qu’au passage d’une dent. Dans
la position actuelle le frotteur FÆ” maintient ouvert le circuit de Aı. Mais si la roue A,
tournait, une succession de courants serait lancée dans la bobine Aı et Vaiguille obser -
vatrice O serait poussée vers l’aiguille indicatrice 7.

Or, mettons que le mouvement d’horlogerie qui commande laxe du cylindre et

que tantôt nous avons supposé embrayé, mettons qu’il devienne libre (une montre se
9

Fig. IL
