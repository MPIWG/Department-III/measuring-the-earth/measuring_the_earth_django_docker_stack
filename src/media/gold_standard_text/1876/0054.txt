 

me
F
a
i
li
Bi
É

i
IN
Ä
i

SSS

oe

Zweite Sitzung.

i Brüssel, den 6. Oktober 1876.
Eröffnung der Sitzung um 1 Uhr 25 Min.

Anwesend sind von den Commissaren: die Herren Adan, Baeyer, von Bauern-
feind, Bruhns, Faye, Ferrero, von Forsch, Hirsch, Houzeau, Ibanez, Liagre, von Oppolzer,
de Vecchi, Villarceau, und von den Eingeladenen: : die Herren Oberst-Lieutenant Ayou,
stellvertretender Director im Kriegs-Ministerium, Brialmont, Folie, Hennequin, Henrionet,

~ Maus, Peny, Hauptmann Pilloy, y, Professor an der Kriegsschule , H. Sainte-Claire Deville,

Mitglied -des Instituts aus Paris, Schmit, Stas, Terlinden.

Präsident: Herr General Ibanez.

Schriftführer: die Herren Bruhns und Hirsch.

Das Protokoll der ersten Sitzung wird vorgelesen und genehmigt.

Auf Ersuchen des Priisidenten giebt Herr von Oppolzer zuerst einen ausführlichen
Bericht über seine astronomischen Beobachtungen, hierauf einen Auszug aus dem Berichte
des Herrn Obersten Ganahl über die von dem militär -geographischen Institute in Wien

‚ausgeführten Bestimmungen von Polhöhen und Azimuthen, sowie über die Triangulation

und das Nivellement; endlich trägt er noch den Bericht des Herın Tinter über dessen
astronomische Bestimmungen vor *). |
Herr von Bauernfeind erstattet hierauf Bericht über die in | Bayern ausgefiihrten

Arbeiten **).

Herr Major Adan erstattet Bericht über die in Belgien ausgeführten Rechnungen,
sowohl in Betreff des Dreiecksnetzes, als auch der Nivellements, und bespricht hierauf
die Resultate der belgischen Nivellements, welche auf die Bestimmung des Mittelwassers
der verschiedenen Meere einen Einfluss haben dürften ***).

Herr Hirsch fragt Herrn Adan, ob besondere Gründe vorhanden seien, von dem
in der ersten geodätischen Conferenz' gefassten Beschlusse abzuweichen, nach welchem
für die vorläufigen Rechnungen die Elemente, des Besselschen Sphäroids in Anwendung

wu ‚bringen sind. Seiner. Meinung nach würde es bedauerlich sein, wenn man nicht in

den verschiedenen Ländern für diese Rechnungen dieselben Piptients benutzen wollte.
‚ Herr Major Adan antwortet hierauf, er erblicke keine Schwierigkeit in Belgien

dem Beschlusse nachzukommen, an welchen Herr Hirsch so eben erinnert habe.

*) Siehe den General-Bericht unter Oesterreich.
**) Siehe den General-Bericht unter Bayern.
***) Siehe den General-Bericht unter Belgien.

  

118841) 1180 Hi d | At

rk.

ak mil!

 

2.4 snmmessahtennn mama Dh |

 
