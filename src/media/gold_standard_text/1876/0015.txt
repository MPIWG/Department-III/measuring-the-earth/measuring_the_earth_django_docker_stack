Inn we m mes ay

POMP Or Try err

IPN AT

year on

à
=
=
:
x
8

 

Mr. le President repond en ces termes:
Monsieur le Ministre ,

Je suis certain d’étre detre des sentiments de P Association geodesique
internationale en exprimant au Gouvernement belge notre profonde reconnaissance pour
l’aceueil bienveillant qu’il vient de faire à la Commission permanente.

Nous devions nous y attendre: la Belgique fut un des premiers pays qui repon-
dirent a Vappel de notre vénérable fondateur, ét l’un des premiers qui se mirent à
l’œuvre pour réaliser sa féconde idée. (C’est en Belgique qu’un remarquable travail
pour l'étalonnage des règles géodésiques a été fait sous la direction du savant et re-
gretté général Nerenburger, par une Commission dont je vois ici quelques illustres
membres, travail publié dans un livre qui est devenu classique pour ces sortes de re-
cherches. C’est encore à un savant militaire belge que nous devons le traité sur le calcul
des probabilités et sur la théorie des erreurs, qui exerça une si grande influence sur

la diffusion de ces connaissances appliquées à la géodésie, chez les nations ot . langue

allemande était peu répandue.

La mort et le changement de position dans l’armée nous avaient enlevé la
précieuse coopération des délégués belges; mais le Gouvernement, en prenant en con-
sidération les désirs de notre association, vient de s’y faire représenter tout récemment,
et cet acte nous fait espérer qu'il continuera, comme par le passé, à accorder toute sa
protection à l’œuvre de la nouvelle détermination de la forme et de la grandeur
de la Terre, — ni

Mr. le Ministre remercie Mr. le Président des sentiments qu’il vient d’exprimer.
La Belgique tiendra toujours à honneur de rester fidèle à ses traditions scientifiques, et
puisqu'elle ne peut s’étendre en largeur, elle tachera de s’élever en hauteur.

Mr. le Général PBaeyer prend la parole et prononce un discours que Mr. le
Professeur /irsch reproduit en français de la manière suivante:

Messieurs,
La Belgique a été le premier pays dans lequel les progrès que la Géodésie
doit aux grands travaux de Gauss et de Bessel ont été reconnus et mis en pratique. .
Bientôt après la publication de la „mesure d’un degré dans la Prusse orientale“ par
Bessel, le Dépôt de la guerre belge adopta la nouvelle méthode pour sa triangulation.
principale, qui s’exécutait alors sous la direction du general Nerenburger. En 1847,

la Belgique emprunta l’appareil de Bessel pour mesurer les bases de Lommel et

d’Ostende. Après avoir terminé cette opération, le ministre de la guerre belge nomma >
une commission spéciale, dans laquelle brillaient les noms de MM. Stas et Liagre, pour
exécuter une troisième comparaison des règles géodésiques de Bessel (comparées une
première fois, en 1834, par Bessel lui-même, et une seconde fois, en 1846, par moi)
et pour déterminer leurs coefficients de dilatation. Vous connaissez tous le rapport si.
clair et si substantiel du Général Nerenburger sur ces travaux terminés en 1854. On
doit à cette commission d’avoir constaté définitivement la diminution avec le temps des
coefficients de dilatation du fer et du zinc, diminution que j'avais soupçonnée dès 1846.

 
