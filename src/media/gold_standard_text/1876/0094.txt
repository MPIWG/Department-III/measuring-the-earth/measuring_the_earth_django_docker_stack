 

i
hs
Ki
bi
Li
Bi

84

Dänemark

Im Laufe des Jahres 1876 hat man den Druck des. 3. Bandes der Gradmessung
angefangen. Mit diesem Bande wird der geodätische Theil der dänischen Messungen
vollständig abgeschlossen. Nach Beendigung sämmtlicher Ausgleichungen sind alle Drei-
eckspunkte, durch scharfe Berechnung der ‚geodätischen Breiten und Längen, auf dem
für ‚die Karte von Dänemark“ adoptirten Sphäroid festgelegt worden. Das nachstehende
Verzeichniss giebt eine Zusammenstellung der dabei erhaltenen definitiven Resultate,
in dem für jeden Dreieckspunkt nicht nur die Breite A und die Länge Z (von der alten
Copenhagener ‚Sternwarte in westlicher Richtung p ositiv gerechnet), sondern auch die
Azimuthe sämmtlicher von dem Punkte ausgehenden Richtungen, so wie die Logarithmen
der entsprechenden, in Toisen ausgedrückten Entfernungen aufgeführt werden.

‘ Das angewendete Sphäroid wird bestimmt durch:

den mittleren Breitengrad gy = 57010 Toisen
und die Abplattung à = 345.

®

oe und Länge für den Ausgangspunkt „Nicolai“ so wie das Azimuth für

die Dreiecksseite ,,Nicolai—Store Modllehéi sind auch hier in Uebereinstimmung mit
* den vom Generalstabe bei der Bearbeitung der „Karte von Dänemark‘ angenommenen

Werthen festgesetzt.

8. Station Nicolai.

1 = 55° 40' 42"937 [= 9 00 19/005

7 | ee Dog dote
Monzeuneen 7 6 à, 5 48 57:16 3.8219748
Pb Me © … . « 4 19 95.18 4.3257767
mom  .. ....0.4l 51 38.29 3.355587
ME Di 52 43,26 3.6779474
Sree BT 17T 31.89 41979804
alla 97 O1 55.99 3.6019525
ee 2 di | {09:46 .00.14 2.5503951.
Ne. 5986, 83: 33.39 41528529
Boot. 32594 95:77 4.2667113
Hollenderby: . . . D. 339 20 53.66 3.1029006
Nordlige Endepunkt af Basis, . 351 2. 00,29). ‚811872944
Sydlige ‘ 6 359, 84 52.45 3.4612104

    

ah HA Al!

a

=

 

2. stats mama DA |

 
