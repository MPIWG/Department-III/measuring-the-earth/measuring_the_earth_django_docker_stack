ee

DIRE TETE NT

TNT ER

i
a
=
R
:
:
:

 

BU RP PMT PTT TTT TT!

T
côtés limitrophes, introduire comme équation de condition l'accord absolu
entre ces deux bases ou côtés?
b) Sur la compensation par groupes d’un réseau de triangles:
+ c) Sur l'emploi de la division centésimale des angles dans les travaux
géodésiques, et sur la regle à établir à cet égard pour les publications
de PAssociation.

6. Fixation du programme et choix du lieu pour la Ce nie générale de 1877.
Eventuellement propositions individuelles.

=]

Conformément à ce programme, Mr. le Président donne la parole à Mr. le
Secrétaire Bruhns, qui fait le rapport suivant sur l’activité de la Commission permanente
pendant l’année 1875.

Rapport de la Commission permanente.

Par suite de la décision prise dans la dernière session à Paris, de renvoyer au —
printemps le choix du lieu de la réunion pour cette année, la Commission permanente —
répondant à une gracieuse invitation du Gouvernement Belge — a décidé de siéger cette.
année à Bruxelles. La Commission s’empresse d'exprimer au Gouvernement Belge
toute sa reconnaissance pour lintérêt qu'il vient de témoigner à l’Association géodésique,
en nous offrant l'hospitalité et en nous accordant pour nos séances les salles de l'Académie

royale des sciences. *

Nous sommes heureux de n’avoir 2 pas à déplorer, cette année, la perte be membres
de notre Association. .

_ La Commission permanente avait chargé les Secrétaires de publier les comptes
rendus de la, session de Paris, en même temps que le Rapport Général pour l’année 1875.
Nous nous sommes acquittés de cette mission; les Procès-verbaux publiés dans les deux
langues, et les rapports des délégués publiés dans la langue où ils ont été écrits, -ont
paru avec des annexes importants, formant un fort volume de 249 pages, qui a été
envoyé à tous les délégués. — Le Bureau Central a entrepris, comme il en avait été chargé,
un catalogue des ouvrages et publications géodésiques des differents. pays. — Les Secrétaires
ont rassemblé les déterminations astronomiques et les observations de pendule, suivant
les formulaires convenus, et les ont également annexées aux Comptes Rendus de la dernière
session. Plusieurs délégués n’ayant pas fourni à temps toutes les données relatives à leur
pays, on aura soin de compléter ce catalogue.

Le Bureau Central a publié et distribué une carte des triangulations et nivellements
del’ Allemagne du Nord; il s “agit de savoir si l’on veut accepter ce modele et le nz
aux autres pays.

Nous avons à l’ordre du jour de nos séances: rapports et discussion sur l’acqui-
sition d’appareils de base, sur la comparaison des différents pendules et sur une liste
des maréographes.

 
