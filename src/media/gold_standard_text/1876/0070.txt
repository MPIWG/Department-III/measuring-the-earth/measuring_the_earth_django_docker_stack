 

Annexet.

LE MARÉOGRAPHE DE M® VAN RYSSELBERGHE.

La forme d’équilibre que prendrait, sous l'influence de la Lune une couche
aqueuse enveloppant toute la partie solide du globe terrestre, serait un ellipsoïde de
révolution, dont l’excentricité serait fonction de la distance des 2 astres, et dont le
grand axe serait dirigé suivant cette distance. Le soleil exercerait une influence ana-
logue sur la surface des mers idéales que nous avons supposées. Il en résulte que la
rotation de la terre autour de son axe, son mouvement autour du Soleil, et le mouve-
ment elliptique de la Lune autour de la Terre engendreraient dans la profondeur de
Veau en un endroit donné, plusieurs variations périodiques, dont la résultante consti-
tuerait le phénomène des marées. i

Ge phénoméne serait déja complique dang la supposition @une mer enveloppant
tout le globe et débarassée des obstacles qui pouvaient la géner dans la réalisation de
sa forme d'équilibre; mais il acquiert une complication excessive lorsqu'on passe à la

réalité de ce qui existe, c’est-à-dire aux océans plus ou moins étendus, aux mers inté-

rieures, aux détroits; tous ces bassins communiquant d’ailleurs les uns avec les autres,

et chacune de leurs parties subissant, non seulement l’action directe des astres, mais

encore, par dérivation, les ondulations qui ont pris naissance dans les mers’ environ-
nantes et qui se propagent au loin en conservant, sinon leurs amplitudes, du moins
leurs périodes initiales. Ainsi, que les marées de l'Atlantique soient directes, ou qu'elles
dérivent de celles de l'Océan Austral, toujours est-il qu'au large de nos rivages circulent
au moins deux marées fondamentales (résultante, chacune en particulier, de plusieurs
ondulations élémentaires) ayant d’ailleurs même période, mais différant par l'amplitude,
l'époque, l’âge, et le sens de leur propagation. L’une pénètre par la Manche en mar-
chant vers l'Est, l’autre descend du Nord de PEcosse; mais toutes deux proviennent
des pulsations de l’Atlantique.

Malgré cette grande complication, il est permis de considérer les variations du
niveau de la mer en un endroit donné, & Ostende par exemple, comme résultant de la
superposition de plusieurs . variations périodiques (pnisque les marées dérivées con-

al m

 

2. etes named NEA |

 
 

mise a Aa

 
