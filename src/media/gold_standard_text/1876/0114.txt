 

104

Lavori Astronomici.

Stazione Astronomica delli Foj.

Dall’Ingegnere geografo signor D’Atri venne completata questa stazione per la
determinazione di latitudine ed azimut, la quale era stata eseguita l’anno scorso. Per
cura dell’Istituto topografico militare (Sezione di Napoli) si eseguiscono i calcoli e le
riduzioni relative alla citata Stazione delli Foj e a quella di Castania eseguita l’anno
scorso dal Maggiore De-Vita.

Stazione di Pachino.

Nei mesi di Luglio ed Agosto venne eseguita per cura del Comandante Magnaghi
della Regia Marina, membro ‚della Associazione, una Stazione per determinazione di lati-
tudine ed azimut.

Per cura del Professore Nobile, Astronomo del Regio Osservatorio di Napoli e
del gia citato Comandante Magnaghi, coadiuvato da Ufficiali della Regia Marina, venne
determinata la differenza.di longitudine tra Pachino e Napoli.

Calcoli relativi alle differenze di longitudine misurate nel 1875.

Come venne riferito nella riunione dell’anno scorso, si erano determinate le dif-
ferenze di longitudine Milano, Padova, Monaco, Vienna e Milano, Padova, Genova, Napoli,

In quest’anno si eseguirono le riduzioni delle operazioni e si stanno ordinando
per la pubblicazione che avr&a luogo probabilmente nel prossimo anno.

Ormai la meridiana dal Capo Passero a Lissa & in istato da poter venire cal-
colata quando che sia.

La seconda tavola annessa presenta a colpo d’occhio le differenze di longitudine
misurate in Italia ed il loro collegamento diretto od indiretto con i principali Osserva-
torii d’Europa.

Determinazione di declinazione di stelle dalla prima alla sesta
grandezza.

Per cura del Professore Respighö si sono continuate nell’Osservatorio del Cam-
pidoglio con tutta la possibile assiduita ed accuratezza le osservazioni meridiane destinate
alla verifica delle declinazioni medie delle stelle dalla prima alla sesta grandezza inclu-
Sive, comprese fra i paralleli + 20° e + 63°. Egli assicura che questo gravoso lavoro
trovasi gia molto avanzato, specialmente per la parte di osservazione, essendosi gid fatto
piu dei tre quarti delle osservazioni richieste dal piano che si era prefisso.

Il numero delle stelle da osservarsi essendo circa 1460, ed essendosi stabilito di
fare almeno sopra ogni stella venti osservazioni assolute di distanza zenitale, e per
quelle convenientemente distanti dal zenit meta dirette e meta per riflessione, e cioè in
tanti distanti passaggi meridiani, il numero totale delle osservazioni poteva valutarsi non

  

Ju ed, 1188411188 LTT

{ae uit ll

Ju du Hi |

à au phil he, à Hi nd ta A mana ns

D: eetusstiomds mama M |

 
