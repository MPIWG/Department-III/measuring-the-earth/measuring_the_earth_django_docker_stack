= PE PRETTY TTT TTT

beh aan

Bt

Pap Al

WP A en

x

133

Russland.

Herr General von Forsch hat zur Berichterstattung an Herrn Generallieutenant
Dr. Baeyer eu Schreiben gesendet.

PETERSBURG, den -"/1o, Marz 1877.
Ew. Excellenz!

Dem Schreiben Ew. Excellenz vom 10. Januar ejusd. an. zufolge, habe ich der
geod. Abtheilung der mir anvertrauten militair-topographischen Section des General- Stabes
aufgetragen, alle in russischen Häfen des Baltischen und Schwarzen Meeres gemachten.
Pegelbeobachtungen in Ordnung zu bringen und dieselben zu berechnen. Diese Arbeit
wird gegen Ende dieses Jahres ausgeführt werden, und um diese Zeit wird es auch
möglich sein, ‘die Resultate dem Central- Bureau en Gegenwärtig. begnüge ich
mich mit der. allgemeinen. Uebersicht dessen , was von uns, ‚bezüglich des Lots sente
(Gegenstandes, gemacht worden ist.

Die Pegelbeobachtungen. im baltischen Man: werden angestellt in folzerlden

Punkten, in Cronstadt,. Reval, Riga (Dünamünde), Windau, Libau. und auf der Insel
decid, Diejenigen von Goal. Reval und Riga ‚eine Periode von mehr
als 30 Jahren;. aus diesen en haben wir schon den mittleren Stand des
Wassers für dae mit dem WF. 5 0.3 enel. Zoll erhalten. In.Libau und Windau
werden die Pegelbeobachtungen nur seit 6—8 Jahren gemacht, aber auch diese sind von
srössem Werthe Angesichts der von mir vorgenommenen Nivellirungen zwischen den
obenerwähnten Pegeln. Auf diese Weise werden unsere Pegel, in Verbindung mit den
längs der nördlichen Küste Preussens aufgestellten, viel zur Lösung der. wichtigen Frage
von der allmäligen Stauung des Wassers von W. gegen O©. in der Ost-See beitragen.

Die regelmässigen Pegelbeobachtungen im Schwarzen Meere werden nur seit
wenigen oe in Odessa, Otschakoff, Nikolaieff, Sebastopol, Kertsch, Noworossiisk und
Poti angestellt. Diese Beobachtungen werden uns zur Bestimmung der Niveau-Differenz
zwischen dem’ Baltischen und Schwarzen.Meere werthvolle Data liefern können.

Fast an allen unseren Beobachtungsorten in beiden Meeren werden gewöhnliche
Pegel gebraucht; die vorhandenen registrirenden Apparate zu Petersburg und Nikolaieff
Sind nur seit Kirn Zeit aufgestellt worden; desswegen hat die Vergleichung der Jähr-

lichen Mittel, die aus den Angaben der’ ersteren und der letzteren abgeleitet werden,
nicht stattfinden können. 11

Mit der vorzüglichsten Hochachtung
Ew. Excellenz

ganz ergebenster
Ed. Forsch.

 
