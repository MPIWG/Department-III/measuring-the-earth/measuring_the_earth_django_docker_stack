PRE

TP en M LENE |

 

an LA

à
à
=
=
=
=
æ
z
=
=
à
à

wünschen sollten, welcher, -inn Sinne der Conferenz vom Jahre 1875, Gemeingut or
verbündeten Länder hätte enduit sollen.

‘Herr General Baeyer erwidert, dass die von Amite befürchteten diplo-
matischen ‘Schwierigkeiten nicht eintreten werden, weil das Central-Bureau für sich allein
und ohne Mitwirkung einer. Regierung über den fraglichen Apparat zu bestimmen haben
werde. Es werde genügen, dass die Bevollmächtisten der betheiligten Länder sich an
das Central-Bureau wenden, um die Benutzung des Basis-Apparätes- zu erlangen.

Nachdem Herr Hirsch die Worte des General Baeyer in französischer Sprache
wieder gegeben hat, erinnert er daran, dass im Jahre 1875, als eine Special-Commission
über den Ankauf des in Rede stehenden Apparates Berathungen pflog, die französischen
Bevollmächtigten sich nicht für autorisirt hielten, für ihre Regierung in Bezug auf die
Beitrags-Frage eine Verpflichtung zu übernehmen, und dass von denselben Vorbehalte in
Bezug auf den Aufbewahrungsort des Apparates ausgesprochen worden sind.

Uebrigens wird nach der Auffassungsweise des Herrn Hirsch der Hauptzweck,
welchen man durch Erwerbung des Apparates erreichen wollte, nämlich die Bestimmung
der Gleichungen zwischen den geodätischen Einheiten der verschiedenen Länder, durch
die Thatsache erreicht werden, dass sich in dem internationalen Bureau für Maasse und
Gewichte in Paris ein geodätischer Urmaassstab und ein besonderer Comparator für die
Bestimmung aller geodätischen Maassstäbe, welche die Regierungen oder Gradmessungs-

 Commissionen dem Bureau zusenden werden, vorfinden wird.

. - Herr Hirsch schlägt also vor, zu beschliessen, dass es nicht erforderlich ist, der
nächsten General-Conferenz Maassnahmen in Bezug auf die Beiträge vorzuschlagen, welche
von den verschiedenen Staaten zu beanspruchen sein würden.

Dieser Vorschlag wird einstimmig angenommen.

Der Präsident ersucht Herrn Hirsch über Punkt 4a (Vereleichung der Pendel-
Apparate) zu berichten. | a

Herr Hirsch erinnert, dass, wie aus dem Berichte des Central-Bureaus hervor-
geht, Herr v. Oppolzer allein auf das Ersuchen geantwortet habe, welches das Central-
Bureau in Folge des in Paris gefassten Beschlusses an alle betheiligten Bevollmächtigten
gerichtet hatte, im Normal-Eichungsamte in Berlin ihre ver schiedenen Pendel-Apparate zu

vergleichen. Die von Herrn v. Oppolzer zum Frühjahre beabsichtigten Beobachtungen
mussten auf den Herbst verschoben werden.

Herr Peirce hat zwei Monate auf dem Bessel’schen Standorte gearbeitet, um
aufs Neue die Intensität der Schwere mittels seines Reversions-Pendels. zu bestimmen.
Die Beobachtungen des Herrn Peirce haben das merkwürdige und gewiss wenig’erwartete
Resultat geliefert, dass an dem Apparate, welchen er benutzt hat, der Dreifuss an den
Schwingungen des Pendels theilnimmt und dass man deshalb an die Länge des Pendels
eine Verbesserung von 0””25 anbringen muss.

Der Bericht des Herrn General Baeyer fügt hinzu, dass Herr Peörce von dem-
selben Gesichtspunkte aus den dem Central - Bureau gehörenden Apparat geprüft hat.
Herr Peirce hat aus seinen Untersuchungen geschlossen, dass die Differenz von 018
zwischen dem von Bessel früher erhaltenen Werthe und demjenigen, welcher neuerdings

 
