 

ma mt

an

=
=
=
5
à
x
x
=
=
5
à

45

Auf eine andere Frage des Herrn Hirsch, auf welche Weise das Mittelwasser
bei Ostende bestimmt worden sei, erwidert Herr Major Adan, dass die Feststellung
der mittleren Meereshöhe, welche von dem belgischen Depöt de la guerre angenommen :
worden sei, nur als eine vorläufige betrachtet werden könne. Diese Feststellung wird
wahrscheinlich nach einigen Jahren durch die Angaben des Mareographen abgeändert
werden, welchen das Departement für öffentliche Arbeiten kürzlich zu Ostende hat
errichten lassen. | ae oo |

Herr General von Forsch erstattet Bericht über die in Russland ausgeführten
Arbeiten *). a

Der Präsident dankt den Herren Bevollmächtigten, welche Bericht erstattet
haben, und setzt die Fortsetzung der ou) auf die yon. der
es Sitzung.

Herr General Liagre ladet im Namen des ‚Empfangs - Comte: die Herren
Bevollmächtigten ein, sich Sonntag den 8. Oktober nach Ostende zu begeben, um den
Mareographen des Herrn Ingenieur van Rysselberghe in Augenschein zu nehmen, welche
Einladung dankbar angenommen wird.

Die dritte Zusammenkunft der Commission wird auf den 7. Oktober um 1 Uhr
angesetzt.

Schluss der Sitzung um 3 Uhr.

Dritte Sitzung.

Brüssel, den 7. October 1876.

Eröffnung der Sitzung um 1 Uhr 20 Min. ©

Anwesend sind von den Bevollmächtigten: die Herren Adan, Baeyer, von Bauern-
feind, Bruhms, Faye, Ferrero, von Forsch, Hirsch, Houzeau, Ibanez, Liagre, von Oppolzer,
Perrier, de Vecchi, Villarceaw, und von den Kingeladenen: die Herren Ayou, Folie, Hennequm,
Henrionet, Peny, Pilloy, H. Sainte-Olaire Deville, Schmit, Stas, Terlinden.

Präsident: Herr General Ibanez.

Schriftführer: die Herren Bruhns und Hirsch.

Herr Hirsch liest das Protokoll der zweiten Sitzung vor, dessen Fassung angenommen
wird, nachdem einer Bemerkung des Herrn von Oppolzer Rechnung getragen worden ist.

Auf das Ersuchen des Präsidenten trägt Herr Hirsch einen: bei dem Präsidenten
so eben eingegangenen Bericht des Herrn Stamkart vom 4. Oktober vor, welcher die

*) Siehe den General-Bericht unter Russland.

 
