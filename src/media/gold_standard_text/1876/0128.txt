 

118

Herr Anton hat tiberdies Pendelbeobachtungen in Pola, Ragusa und Wien an-
gestellt, und ist.eben im Begriffe, in Krakau diese Arbeiten durchzuführen. Diese letztere
Bestimmung, deren Durchführung in den nächsten Tagen mit Sicherheit erwartet werden
darf, ist die Schlussarbeit auf den Punkten erster Ordnung in Oesterreich. Die für dieses
Frühjahr in Aussicht genommenen Pendelbeobachtungen mit unserem Repsold’schen Ap-
‚parate in Berlin konnten in Folge von Raummangel in Berlin noch nicht zur Durch-
führung gelangen, ich habe aber gegründete. Hoffnung, dass diese so wichtigen. Opera-
tionen, die in empirischer Weise die Vergleiehung der verschiedenen Apparate ermöglicht,
noch in diesem Jahre beendet werden können *).

Dass es mir gelungen ist, das grosse vorgesteckte Ziel in seinem ersten Abschnitte
in diesem Jahre zum Abschlusse zu bringen, verdanke ich wieder nur der opferwilligen
Unterstützung der mir im k. k. Gradmessungsbureau zugetheilten Herren, nämlich :

Herr Ferdinand Anton (1. Observator),
„ Robert Schramm (2. Observator),
, Frang Kühnert (Assistent),
„ Baron Hans von kühling (Hilfsassistent), :
„ Johann Strobl o .

und ausserdem den mir von Seite des k. k. militair-geografischen Institutes zu-
getheilten Herren:

Oberlieutenant Gustav von Steed,
4 Alois Nahltk.

. Leider wurde uns Oberlieutenant Gustav-R. v. Steeb in. diesem Jahre. durch den
Tod entrissen, und es war demselben nicht gestattet, das von ihm in so hervorragender
Weise geforderte Unternehmen seinem Abschlusse zuzuführen.. Ein in Ragusa denselben
‘befallendes Wechselfieber und eine durch die Errichtung eines Pyramidensignals in allzu-
srossem Eifer zugezogene Uebermüdung veranlassten eine Entzündung der Herzklappen,
die leider seinen Tod am 25. Juli v. J. herbeiführte. Seine letzte Arbeit war die Bethei-
ligung an der Längenbestimmung Ragusa—Pola—Wien. Die Gradmessung verliert an ihm
einen der hervorragendsten Mitarbeiter, ich verliere an.demselben einen wahren Freund;
es wird demselben, sobald seine Leistungen durch unsere Publikationen bekannt gemacht
werden, ein ehrenvoller Platz unter den Theilnehmern der europäischen Gradmessung
"gesichert sein. |

*) Sie wurden durch Herrn Anton im October und November ausgeführt, eine provisorische
Reduetion ergiebt für Berlin eine um 0.03 Millimeter kürzere Pendellänge als dieselbe durch Bruhns ge-
funden wurde, entfernt sich also noch mehr vom Bessel’schen Resultate.

ah 1 108 411.108 hie As

shunned ah a, td ob Ah a in

 

 
