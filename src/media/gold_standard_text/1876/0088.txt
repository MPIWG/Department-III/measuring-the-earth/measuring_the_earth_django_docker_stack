 

18

Elles donneront les valeurs des altitudes compensées d'après l'importance des
cheminements effectués.
* Afin de ne pas augmenter considérablement la longueur des calculs, on a formé
un groupe de tous les cheminements de base principaux, à savoir:

Ostende-Bruxelles

. Bruxelles-Anvers
Bruxelles-Quévy
Bruxelles-Pepinster
Bruxelles-Arlon

et de la succession des cheminements qui suivent la frontiere du territoire.

Les altitudes calculées sont celles des points les plus importants, dans les villes,
bourgs, etc., où aboutissent les cheminements de l'intérieur et où l’opérateur a changé.

On a eu ainsi pour ce premier groupe 83 points déterminés par 102 chemine-
ments. La résolution des 83 équations normales à fourni les valeurs des altitudes rap-
portées au point de départ d'Ostende et il à suffi d’ajouter 672835 & toutes ces cotes
pour les avoir par rapport au niveau moyen des basses mers adopté dès 1873 par le
Dépôt de la Guerre.

Les cotes provisoires qui ont servi a assoir le nivellement de detail, avaient
parfois été obtenues par d’autres cheminements que ceux employés dans ce premier
groupe; les différences de niveau ne concordent pas avec les différences des cotes, de
sorte que les cotes compensées ne peuvent être comparées rigoureusement aux cotes
provisoires, mais les différences de niveau résultantes sont comparables aux différences
de niveau trouvées sur le terrain par les officiers topographes.

Les écarts sont très-petits à l'exception de celui de lun des cheminements
d'Ostende à Bruges; cependant on n'a pas cru devoir le supprimer parceque les inscrip-
tions manuscrites ne lui attribuent que 8 millimètres d'erreur moyenne kilométrique. La
hauteur du repère à Bruges est d’ailleurs connue avec une précision suffisante, elle a
pour erreur moyenne:

 

 

ar 31 al

L’erreur moyenne de lensemble des déterminations est égale à la racine carrée
de la somme des carrés des écarts entre les différences de niveau primitives et les diffé-
rences de niveau compensées, divisée par l’excès du nombre des cheminements sur le
nombre de points déterminés:

pi 48605,38 __ om
ae joe = 00 018

(L'erreur moyenne serait réduite à un centimètre si le cheminement d’Ostende
a Bruges, dont il vient d’être question, était supprimé.)

  

wi ead md A

Il

al le

 

PENDFESRFFFFFFEEGBEFEREE UN |

 
