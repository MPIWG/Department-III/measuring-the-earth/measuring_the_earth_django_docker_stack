 

36

Während wir in der vorigen Sitzung den Verlust zweier Commissare durch den
Tod zu beklagen hatten, freuen wir uns: dieses Mal constatiren zu können, dass kein
- Verlust in dem letzten Jahre. eingetreten ist. an ‘

Die permanente Commission beauftragte ihre Schriftführer, das Protokoll der
vorigen Sitzung in Paris durch das Centralbureau drucken zu lassen. Selbige haben sich
dieses Auftrages erledigt, die Protokolle nebst den Generalberichten, erstere in 2 Sprachen,

letztere so wie sie eingesandt sind, sind verschickt und in Ihren Händen und bilden -

mit den Anhängen einen stattlichen Band von 249 Seiten.

Die in der vorigen Sitzung gefassten Beschlüsse, eine Zusammenstellung der

‘Literatur der geodätischen Arbeiten in den einzelnen Ländern zu geben, sind von dem
Centralbureau ausgeführt worden. Die Zusammenstellung der astronomischen und der Beob-
achtungen der Pendellängen ist nach dem berathenen Schema von den Schriftführern
gemacht und befindet sich vereinigt .mit den Protokollen und dem Generalbericht. -Da
von einzelnen Commissaren die gewünschten Daten nicht vollständig eingeliefert worden
sind, wird für die Fortsetzung, respective Ergänzung Sorge getragen werden.
Die gewünschte Uebersichtskarte der Triangulationen und: Nivellements ist für
das -nôrdliche Deutschland vom Centralbureau als Vorlage zu weiteren Karten angefertigt
und versandt; wir haben darüber Beschluss zu fassen, ob wir diese Form allen bethei-
listen Staaten empfehlen wollen. - N |

Ueber die gefassten Beschlüsse wegen Beschaffung des Basisapparates, wegen
. der Vergleichung der verschiedenen Pendelapparate in Berlin, wegen einer Zusammen-
stellung der Mareographen wird in den hiesigen Sitzungen weiter berichtet und ver-
handelt werden.

Mit Freuden können wir endlich constatiren, dass, wie auch aus dem General-
berichte hervorgeht, in allen betheiligten Ländern die Arbeiten rüstig vorwärts schreiten,
ja mehrfach der Beobächtungstheil schon fast vollendet ist, sodass nunmehr bald an
uns die Aufgabe tritt, zwischen den astronomisch bestimmten Oertern die geodätischen
Linien zu rechnen, um daraus zum Schlusse die Grösse und Figur der Erde zu finden,
wie sie aus den Messungen in Europa sich am wahrscheinlichsten ergiebt.

Der Präsident dankt Herın Bruhms und bittet Herrn General Baeyer den-Bericht
des Centralbureaus zu geben, welchen Herr Hirsch vorliest.

| | Bericht |
des Central-Bureaus und des geodätischen Instituts über die im Jahre 1876
ausgeführten Arbeiten.

| A. Geodätische Arbeiten.
1. Zur Ueberwachung des Druckes der Publicationen war Prof. Sadebeck in

Berlin zurückgeblieben und deshalb war seinen beiden Assistenten Dr. Lamp und Werner
die Ausführung der für das Dreiecksnetz zwischen Berlin und dem Taunus noch fehlen-

|e Hh) Bae

11841

1/1 (bet

Lia

 

 

 
 
