Ten Sere

FR en

my

en

embry

per trot

arten

=
&
a
ke
E
=
k

 

À PRP PPAF TT rm NW!

9

ou pluvieux la perte du courant n'était que de quelques degrés; deux fois seulement,
le 12 et le 13 juin, où le temps était exceptionellement mauvais, la perte est allée
jusqu’à 21° et 11°. C’est grâce à cette isolation parfaite des tienes que l’échange des
signaux à pu Se faire toujours parfaitement, même dans les plus mauvaises conditions
atmosphériques.

L’autre opération, entre Strasbourg et Genéve, n’a pas été aussi favorisée et il
n’a pas été possible d’obtenir un nombre suffisant de déterminations dans le temps prévu.
L’opération a été surtout entravée par le mauvais état de la ligne Geneve-Bäle. Par

suite de ces perturbations il a fallu, pour compléter les données, recommencer les

observations le 1 octobre.*)

En outre MM. Valentiner, Löw et Richter ont exécuté la determination combinée
des longitudes Strasbourg-Mannheim et Strasbourg-Bonn, du 2 juillet au 5 août. Ensuite
MM. Albrecht et Westphal ont mesuré la latitude et l’azimut. au Feldberg (dans la Forêt
Noire), du commencement de juillet au milieu d'août.

Dans l'opération de la détermination de longitude, on a éliminé les équations
personnelles en faisant observer les trois observateurs chacun successivement aux trois
stations ; en outre et pour contrôler, on a déterminé les équations personnelles par des obser-
vations faites & Mannheim du 7 au 10 août. En vingt jours d'observation on est arrivé
pour la longitude Strasbourg-Mannheim au poids 1, 6 et pour Strasbourg-Bonn au
poids 11,2.

Au Feldberg on à mesuré des distances zénithales circumméridiennes, observé
des passages au premier vertical, mesuré l’angle entre la Polaire et P’heliotrope sur le
Gebweiler-Belchen, determine l’azimut d’une mire méridienne au moyen de l'instrument
des passages et mesuré au théodolite l'angle entre cette mire et l’héliotrope sur le Belchen.

C. Travaux de nivellement.

Mr. le Professeur Dr. Börsch a d’abord nivele la ligne Eisenach-Cobourg, pour
obtenir la jonction avec la Bavière; ensuite il à fait la révision de lopération entre

Cassel et Northeim, enfin il à nivelé les lignes Rheine-Hamm et Hamm-Plettenberg. Son

aide, retenu jusqu'au commencement d'août à Berlin par la publication des travaux
antérieurs, a d’abord exécuté dans le Harz, entre la Victorshôhe et Friedrichsbrunn, un
nivellement important pour le relevé géologique du pays. Ensuite il à commencé le nivelle-
ment que l’Institut à entrepris le long de l’Elbe, sur la demande du ministre du commerce,
dans l'intérêt des travaux de correction de ce fleuve. Commencé à Riesa près de la frontière
saxonne, le nivellement à été poussé actuellement jusqu'à Magdebourg.

D. Comparaison d’etalons.

Le Bureau Central n’est pas encore en possession d’un local où il puisse installer
le comparateur de Steinheil. La Chambre des députés a bien voté une somme de

*) Elles ont été terminées le 10 octobre. A. H.
