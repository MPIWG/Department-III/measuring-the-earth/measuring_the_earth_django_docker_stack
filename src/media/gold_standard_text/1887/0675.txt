 

INDICATION

DES POINTS.

LATITUDE.

ae

Grande Bretagne.

LONGITUDE.

EPOQUE.

DIRECTEURS

ET OBSERVATEURS.

INSTRUMENTS.

REMARQUES.

 

+

Lundy Island
Lynn Old Tr..-..
Lyons Hill
Maker Ch. Tr.....
Malvern

Mamsuil

Merrick

Merrington Ch....|

HL LO:

52
53
50
52

57
51

ES

D4

Mickfield Ch. Tr...| 5

Milk Hill

Base de Misterton
Carr (Terme N.) |

Base de Misterton
Carr (Terme S.)

Moelfre Issa

Monach

Mordington....... |
Mormonth
Mount Battock

Mt. Leinster...... | 5

Mt. Sandy

Moweopt
Naseby Ch. Tr. ...

North Rona

North Ronaldsay .-
Phare |

Norwich Spire....

Old Lodge
Old Sarum Castle. .
Old Sarum Gun...

Orme’s Head
Orford Castle

 

Otley Ch. Tr.
Over Hill
Paddlesworth

Penninis Windmill.
Pertinny

 

Peterhead Old.
W. m. |
Pillesdon

Plyulimmon
Precelly |
Reform Momment. .

51

15
17
20

6

16
13

QU

4
25
50
I

48
7

21

39

; | 18
| 15
| 16

bo
1 © CO

| 50 48 24

52 28 1
51 56 46 |

51.29 91

N
Do

 

12950 201

Wy
jui
13
15
12
1

13

16

15 43

743 |

28
19

44

31

32
Ü

11 45
4

48 «

45 3

3 44.

15
14

13
12

ry

5D
21

52
49

52

13
50

36 ||

10

59 |
53 22
15 51 56 ||

1845
1843
1829
1846
1803-44

1848
1798-1844

1841-52

1854
1845
1850
1801 .

1801

1806
1839

1846
1814-47
1814-47

1829

len:
' 1800-42-43

| 2 1845,
1828
1847
1845
1850

1844
1844
1849
| 1849
| 1793-1849

1843
1844
1817
1844
1804-45
1841
1850
1796-1845

 

| 1850
| 1795-1845

1805
1803-42

 

|

 

Caporal Stewart.

Serg. Bay.

Gener. Portlock.

Serg. Donelan.

Woolcot, caporals Cosgro-
we, Stewart.

|Serg. Winzer.

Génér, Mudge, Woolcot,
capor. Stewart.

Capit. Hornby, serg. Jen-
kins.

Cahill.

Sere. Steel.

Serg. Donelan.

W oolcot.

id.

Génér. Colby, Woolcot.

Colonel Robinson,
Hornby.

Serg. Winzer.

Gardner, serg. Donelan.

Génér. Colby, Gardner,
serg. Donelan.

Col. Dawson, capit. Hen-
derson, lieut. Murphy.

Serg. Jenkins.

Wooleot, serg. Steel.

id.
Gener. Portlock.
Serg. Jenkins.
Sergs. Winzer, Steel.
Serg. Steel.

id.
id.
Sergs. Jenkins, Donelan.
Serg. Donelan.
Génér. Mudge, serg. Do-
nelan.

id.
Sergs. Steel, Beaton.
Gardner.

Serg. Donelan.

| Woolcot, capor. Stewart.

Lieut. Da Costa.

Caporal Wotherspoon.

Génér. Mudge, Woolcot,
serg. Donelan.

Serg. Steel.
Génér. Mudge, serg. Do-
nelan.

Générs. Colby, Mudge.

Woolcot, lieut. Luyken.

capit. |

 

Ramsden de 3 p.
Troughton de 2 p.
Ramsden de 3 p.

id.

id.

id.

id.

id.

Theod. de 12 p.

Ramsden de 18 p.

Ramsden de 3 p.
id.

id.

id.
id.

id.
id.
id.

Troughton de 2 p.

Ramsden de 3 p.

Ramsden de 3p. et

18.9:

ld,
Ramsden de 3 p.
Theod. de 7 p.
Theod. de 18 p.
Airy’s Zenith Sector.

Theod. de 18 p.
id

id.
Theod. de 3 p.
id.
id.

Ramsden de 3 p..
Ramsden de 18 p.
Ramsden de 3 p.
id.
id.
Troughton de

2 De
Ramsden de 18 p.

Ramsden de 3 P.

“ Theod. de 7 p.

Ramsden de 3 p.

id.
id.

|
|

 

 

 

| Nahe bei 152.

| Nahe bei 115.

Nahe bei 115.

 
