      
     
     
  
 

15
2° Lorsque la force synchronisante et le mouvement synchronisé sont représentés par
la loi pendulaire simple, il existe toujours une différence de phase entre la force et le mouve-
ment : cette différence de phase, conséquence de l'amortissement, correspond toujours à un
retard du mouvement synchronisé.

 
    
   
    
     
      
  
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
     
   

En d’autres termes, la synchronisation de deux horloges ne peut être réalisée par une
force unique; a la force synchronisante il faut opposer dans l'horloge synchronisée une force
amortissante antagoniste de la première. (’est seulement sous l’action combinée de ces deux
forces que l'horloge peut prendre un mouvement régulier, synchrone du premier et en re-
tard sur lui d’une différence de phase constante.

Pour réaliser ces conditions théoriques, M. Cornu a disposé sur le balancier de
l'horloge à conduire un aimant en are de cercle qui se meut pendant les oscillations du ba-
lancier dans deux bobines creuses, enveloppées de fil de cuivre recouvert de soie. Dans le fil
de l’une des bobines passe un courant périodique envoyé par l'horloge conductrice à un in-
stant toujours le méme de chaque oscillation double. Ce courant, passant dans le sens con-
venable, transforme la bobine en un solénoide qui attire l’aimant mobile. Voilà la force syn-
chronisante. Le mouvement de l’aimant mobile développe dans le fil de la seconde bobine un
courant induit dont on fait varier à volonté l'intensité à l’aide d’un appareil de résistance in-
troduit dans le circuit. C'est la force amortissante. En réglant convenablement l'intensité des
deux courants, on arrive à établir un synchronisme parfait. M. Cornu a amplement dé-
montré, à l’aide d'appareils très ingénieux, que la différence de phase des deux horloges, une
fois le régime régulier établi, reste absolument constante. Ce n’est pas ici le lieu d’entrer
dans le détail de ces remarquables expériences. Nous ne voulons retenir qu’un point, ex-
lrémement important au point de vue pratique. Pendant la période préparatoire de la syn-
chronisation, l'amplitude des oscillations du balancier de l'horloge synchronisée oscille autour
d’une valeur déterminée qu’elle atteint rapidement. Lorsque l'amplitude est constante, la
différence de phase l'est aussi et la synchronisation est parfaite. La constance de l'amplitude
est donc le critérium de la synchronisation parfaite.

Nous avons fait de nombreux essais de cette méthode. Nous avons opéré en intro-
duisant entre les deux horloges des résistances équivalant à 1000 kilomètres de ligne télé-
oraphique. Toujours le synchronisme a été établi et maintenu sans peine. Aussi n’avons-nous
pas hésité à appliquer ce procédé de synchronisation à la détermination télégraphique de l’in-
tensité de la pesanteur.

Nous passons à la seconde partie de la méthode proposée, le pendule réversible in-
versable.

Les craintes qu'inspire le pendule invariable, au point de vue de la conservation
de Vinvariabilité, sont surtout :

La crainte d’une modification moléculaire provenant d’un choc, du transport, du
temps et faisant varier le moment d'inertie du pendule.

La crainte d’une altération de larête des couteaux par un long usage et d’une modi-
fication correspondante de la durée d’oscillation.
