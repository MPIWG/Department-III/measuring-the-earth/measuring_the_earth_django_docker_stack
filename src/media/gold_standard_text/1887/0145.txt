 

N

ill
|.

l-

all

N-

TEEN TEILEN

 

71

denen man sich dort seit einigen Jahren beschäftigt, und dass Brasilien auch der Internatio-
nalen Erdmessung beitreten wird, die ich das Vergnügen habe, hier vereinigt zu sehen.

«Gez. : Don PEDRO D’ALCANTARA.

« Auswärtiges Mitglied der Akademie der Wissenschaften. »

Alle Mitglieder der Conferenz erheben sich von ihren Sitzen zum Zeichen des
Dankes für das Versprechen Seiner Majestät, (lass das Kaiserreich Brasilien der internationalen
Erdmessung beitreten wird, wodurch die Möglichkeit sich darbietet, die geodätischen Studien
auf den sudamerikanischen Continent auszudehnen.

Der Vorsilzende constatirt, dass das Programm der Gonferenz erschöpft ıst, und, da
niemand neue Vorschläge einbringt, erklärt er die Conferenz der Permanenten Commission
der internationalen Erdmessung im Jahre 1887 als geschlossen. Er dankt Seiner Majestät
dem Kaiser von Brasilien, für die Güte, einer Sitzung der Permanenten Commission bei-
gewohnt zu haben und für das Interesse, welches Dieselbe dem Unternehmen der Erdmes-
sung bezeugt.

Er drückt alsdann, im Namen der Permanenten Commission, Herrn Bischofsheim
den wärmsten Dank aus für die glänzende Gastfreundschaft, welche er der Conferenz hat zu
Theil werden lassen in seiner prachtvollen Sternwarte, rnit der er Nizza, Frankreich und die
ganze wissenschaftliche Welt beschenkt hat.

Der Vorsitzende macht sich ebenfalls ein Vergnügen daraus, dem sympathischen
Direktor der Sternwarte von Nizza, Herrn Perrotin, die grosse Dankbarkeit der Permanenten
Commission für die liebenswürdige und gastfreundliche Art auszusprechen, mit der er die
Conferenz auf der Sternwarte empfangen hat.

Der Vorsitzende dankt endlich dem Herrn Grafen von Malaussena, Maire von Nizza,
für den freundlichen Empfang, den er der geodätischen Gonferenz hat zu Theil werden
lassen, welcher er den Gemeinderathssaal für einige ihrer Sitzungen zur Verfügung ge-
stellt hat.

Schluss der Sitzung um 5 Uhr.

 
