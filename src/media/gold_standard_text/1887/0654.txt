a
France et Algérie.

INDICATION DIRECTEURS

LATITUDE. |LONGITUDE.| ÉPOQUE. INSTRUMENTS. REMARQUES.

DES POINTS. ET OBSERVATEURS. |

 

 

Triangulation de la Méridienne de Dunkerque. (Partie Sud — Suite.)

 

 

Meimac
Aubassin

La Bastide
Montsalvy.........
Rieupeyroux

Rodez

St. Georges
Cambatjou
Mont-Redon
Montalet

Carcassonne
Bugarach
Tauch
Forceral

Terme Boréal
Puy de la Estella..
Puy Camellas

Bois Commun

Chapelle la Reine .
Haut du Ture

Assigny
Humbligny
Montargis

Les Bezards.......
Montifaux

Bouy

La Charité
Saligny le Vif.

45° 58! 38”
46 12
45 51 49
45 44 41
45 45 14
45 24 0
45 34 14
45 11 4
4 DO
44 50 8
A4 42 53
44 19 2
44 91 5
44 7 57
44 042
43 51 55
48 43 1
43 41 0
43 31 34
43 25 29
43 854
43 12 55
42 51 51
42 54 38
49 43 38
42 49 35
42 43 17

42 49 28

42 30 54
42 26 27

 

 

20° 5417
19 48 15
19 46 53

19 54 42 ||

20 13 54
20.7 14
19 47 10
19 48 20
20-19 1
19 46 54
20° 9-58
19 53 38
20 14 15
20 18 10
19 56 16
20 13 25
19 58 16
20 24 19
20 23 40
20: 57 91
20 17 26
20 0 46
20. 2 29
20 20 31
20 21 44
20 27 47
20 32 55

20 34 46

20 12 57
20 29 26

 

 

1792 et suivante.

Delambre et Méchain.

 

2 Cercles répétiteurs de Lenoir.

 

Triangulation de la Méridienne de Fontainebleau à Bourges.

| Déjà mentionnés.

47°49' 40”

20°10' 59”

20 17 40 ||

20 25 51
20 18 4
20 23 27
20 24 50
20 35 45
20 49 43
20 40 48
20 25 50

 

 

 

Capitaine Delcros, lieute-
nants Roset et Stamati

7
a
J

Cercles répe

 

 

 

titeurs.

Triangulation du Parallèle d’Amiens. (Partie Orientale.)

Villers-Breton-
Sourdon Déja mentionnés.
Beauquéne
Lihons

Major
Corabœuf,
lieutenant

Testu.

Cercles
répétiteurs.

| 49° 49! = 20° 25' 41”
| |

Mémorial du Déyge
Général dela se a
re, Tome VI. 4

 
