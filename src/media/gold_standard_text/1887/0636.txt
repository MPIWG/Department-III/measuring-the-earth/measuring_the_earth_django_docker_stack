eraphe électrique, ce qui a rendu superflue la partie du parallèle située à l’ouest. ie
la méridienne, et en méme temps on a renoncé à la base de Skagen où le terrain
n’offrait pas des conditions assez favorables. Aux opérations qui, de cette manière, ne
comprenaient que la prolongation de la chaîne méridienne jusqu’à Skagen, on a ajouté |
des observations d’azimut en trois stations, une de la Seeland, une de la Fionie et une
du Jutland. Les observations ont été finies en 1870.

Le rapport complet des observations et des résultats qu’on en a tirés, se trouve
dans l’ouvrage « Den danske Gradmaaling » vol. I-IV, publiés par M. Andre en 1867,
1872, 1878 et 1884.

Par ordre du directeur

AAL OLUFSEN

capitaine.

 
