 

N
i
3

ya

las se.

 

 

i
y
|
j

supposer sans calcul en tenant ce > uni ie
compte uniquement des différences d’altitude. Lanserkopf a

une altitude de 930m, et la vallé
oe AS > d | à à > ë ONE
ni > vallée de VInn a Pradl, 575™; la différence est seulement de

5m KE Anand: Na :
et
la verticale dans le systäme du lan Ul ee ‘ cn thse ten a he . nn r
manière que Pattraction de toutes he Hee at N u 3 Hi
Ben a | edt sses dans la périphérie de 12 nr ou 40 Gar pour
Arad, no Sonim. _ ee dg y ne ae ae oe eae
kopf, selon la none de + 20"3 a ee 2 nn u at pes
) a 20 3 ou de + 178, comme ila été dit auparavant. Si

on ajoute — 179, il reste + 3°1 dans un cas, et dans l’autre + 06.
On peut faire ici l'expérience que l'emploi de stations de vallées pour la recherche

du cours de la déviation de la verticale est en somme peu propice.

§ 8. Recherches sous le 49° parallèle dans l’Amérique du Nord.

Le 49 parallele marque la frontière entre les États-Unis et le Canada. Entre les
deux monuments qui la fixent sur la rive ouest du lac des Forêts et au sommet des Montagnes-
Rocheuses, il y a une difference de longitude de 18254. Sur cette étendue, 41 points (les
points extrêmes compris) ont élé déterminés astronomiquement en latitude par les astrono-
mes des deux pays voisins; d’autres points intermédiaires ont été fixés geodesiquement, en
tirant une ligne perpendiculaire de l’un des points extrêmes de chacun des 40 espaces au
moyen d’un azimut mesuré, normalement au méridien, et en déduisant le parallèle par lin-
termédiaire de distances tangentielles.

D'une station astronomique jusqu’à l’autre, la déviation relative de la verticale en
latitude se déterminait ainsi sans une influence considérable des dimensions de lellipsoide
terrestre. Par l'addition successive on pouvait rapporter le tout a la premiere station el,
après l’application d’une constante proportionnelle, à un parallèle moyen normal: Abstraction

faite des détails, la série présente quatre groupes de signes : -F — -- —. Les deviations de
: : re 5 Sol

la verticale se groupent done par région. Les valeurs vont jusqu’à 8 , leur moyenne est

2.4

A Vexception des eing stations des hautes montagnes, ila été exécuté pour les sta-
tions astronomiques des calculs d’attraction avec 40 milles de rayon extrême des masses ;
les déviations de la verticale correspondant à ces attractions ont en partie des signes contrai-

12 points); cependant la décroissance est en moyenne

res ainsi que les deviations (pour
0.7, par consequent 1"4 ou les ?/, du montant de la déviation moyenne de la verticale res-

‘Local deflections of the plumb-line near the 49th parallel. Read before the Essayons club of the
1876. from Lieut. F. V. Greene, Corps of Engineers. (Also read before the
20th, 1876; and published with the approval of Major W. J. Twi-
hern Boundary Commission.)

Corps of Engineers, Novembre 12,
Philosophical Society of Washington, May
ning, U. S. Engineers, Chief Astronomer, Nort

 
