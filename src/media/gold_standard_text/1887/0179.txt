of

  
 

$ 5. Quelques déviations de la verticale en longitude pour le centre
et l’Ouest de l’Europe.

Le tableau IV donne pour 55 stations, groupées d’après divers points de vue, la dé-
vialion de la verticale vers l'est à, par rapport à Rauenberg pour les mêmes trois ellip-
soides qui ont éte employés dans le paragraphe précédent. A la suite des trois valeurs respec-
ctives de la déviation de la verticale, on a indiqué les coefficients m et n de l'expression
m 6r fn Ar qui doit être ajoutée à ces valeurs numériques, si l’on ne suppose pas égale
à zéro la déviation de la verticale vers le nord Er et celle vers Pest Ar, pour Rauenberg. Sous
ce rapport, le tableau IV se distingue du tableau II, où cette expression, par suite de
| la forme de la région considérée, a approximativement la valeur constante €,, dont l’indica-
1 tion répétée n'était pas nécessaire, tandis que dans le cas actuel ses coefficients m et n va-
| rient considérablement.

Le tableau a été dressé principalement d’après des calculs de l’Institut géodésique,
c’est-à-dire en partie d’après les communications des Lothabweichungen, fase. |, en partie
d’après des recherches inédites. Il faut en excepter le N° 11, Altona, pour lequel on a utilisé
la publication de C.-F.-W. Peters, citée dans le § 3, puis les Nos 18, 35-39, 42 et 54, Co-
bourg, Nuremberg, Wulzbourg, Munich, Peissenberg, Benediktbeuren, Tubingue et Milan,
qui ont été établis à l’aide des calculs de von Orff dans la Triangulalion du royaume de
Bavière et dans la publication de la latitude de Munich (comp. $ 3), ensuite N° 55,

gt salles r ? à r ® EG D $
ln | Gênes, rattaché d’après les données de Celoria (comp. $ 3) et le N° 34, Feaghmann, inter-
cet calé au moyen des données contenues dans le Ordnance Trigonometrical Survey of Great
eu Britain, elc., p. 694.

ce un pe

i Von Orff rapporte, dans les publications mentionnées, les déviations de la verticale à
passé ; Munich comme point de départ. Pour la transition au Rauenberg, on pouvait utiliser les huit
on poste | points suivants: Goettingue, Mannheim, Strasbourg, Pfaender, Zurich, Rigi, Gabris et
1 Berne, qi figurent aussi bien dans les publications de von Orff que dans les calculs de l’In-
slitut géodésique. On a employé la simple moyenne des huit déterminations qui ne sen
écartent au maximum que de 0 3.

Les déviations de la verticale contenues dans le tableau IV, partout où la longitude
géographique n’est pas arrondie aux minutes pres, sont déduites des plus récentes détermi-
nations de longitude au moyen du télégraphe électrique, ou bien conclues des mesures d’azi-
mut. Ce dernier procédé est, comme on sait, sujet en général à une incertitude d’aulant plus
grande qu’un point est plus éloigné du point de départ. La cerlitude n’est suffisante que
pour des points dont la distance au point de départ ne dépasse pas la longueur de quelques
côlés de triangles principaux. Le point de départ doit être dans ce cas le Rauenberg lui-

 

spé. : -
À même ou un point dont Vazimut est contrôlé soit par la compensation d’un réseau astronomo-
i es : ; ee 4 soeraphi l à 3

ee | géodésique, soit par une détermination de la longitude géographique. Conformément à cet

 

 

 

 

 

 

 

 

 

 

 

 
