  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

16

 

en sont données, pour la plupart des stations, d’après les mémoires de Villarceau sur les
Déterminations astronomiques des longitudes, latitudes, etc., dans les années 1862-1866, La
valeur pour la latitude de Paris est tirée des Mémoires de 1863, conformément au choix de
Villarceau lui-même parmi les différentes délerminations.

La latitude d'Amiens correspond à la détermination de Peytier, d’après le Mémorial
du dépôt général de la guerre, t. IX, p. 264 et celle de Saint-Martin de Ghaulieu à la déter-
mination de Bonne, d’après ce même Mémorial, t. VIE, p. 396.

Les chiffres romains suivis de chiffres arahes, qui figurent dans le tableau à côté des
points français, désignent le volume et la page dudit Mémorial auquel sont empruntées les
valeurs arrondies indiquées pour la longitude et la hauteur du terrain au-dessus du niveau
de la mer.

Les latitudes et les altitudes pour les points belges Lommel et Nieuport sont em-
pruntées à la triangulation du royaume de Belgique, soit au nivellement!.

Dans le tableau ci-dessus, on n’a pas utilisé les données de Germain sur les dévia-
tions de la verticale dans le Midi de la France. Je me borne à y renvoyer le lecteur?. Une
donnée ultérieure sur Nice sera exposée dans le tableau III, dans le prochain paragraphe,
en connexité avec un autre sujet.

Un coup d’œil jeté sur le tableau Ib met en évidence le fait remarquable que les
déviations de la verticale en latitude, de Lommel à Carcassonne, sont toutes positives, de
sorte qu'il existe en apparence pour la France, dans le système de l'arc méridien franco-an-
glais, d'après les calculs de Clarke, de 1880, une cause agissant dans un seul et même sens.

=

 
  
 
 
 
 

 
 

$ 4, Déviations de la verticale, suivant la latitude, pour le
Danemark, l’Allemagne, la Suisse et l'Italie.

 
   
 

Le tableau HI présente l’ensemble des déviations de la verticale pour cent dix-sept
points des États susmentionnés et des pays limitrophes; ces déviations ont élé déduites en
utilisant un assez grand nombre de publications. Les matériaux insérés dans la publication de
l’Institut géodésique, Déviations de la verticale, fase. ler, constituent la base du tableau 5. Les
déviations de la verticale sont rangées en premier lieu par rapport à Rauenberg près Berlin,
pris comme point de départ pour l’ellipsoïde de Bessel, 2) pour l’ellipsoïde de Clarke, de 1880
et 3) pour un ellipsoide ayant le demi-grand axe a de l’ellipsoïde de Clarke et l’aplatissement
a de Pellipsoide de Bessel, relativement à la première, à la troisième et à la deuxième place

  
 
 
  

    
   

* Triangulation du royaume de Belgique, 1867, t. I, |. I, p. 519, 526, 574.
Nivellement général du royaume de Belgique, 4879, p. 448.
? Germain. Observation de la déviation de la verticale sur les côtes sud de France. Comptes-rendus
1886, CII, p. 4400.
* Lothabweichungen, Heft I. Formeln und Tafeln, sowie einige numerische Ergebnisse fiir Nord-
deutschland. (Veröffentlichung des Königl. Preuss. Geodätischen Instituts). Berlin, 1886, S. 88.

 
   

    
