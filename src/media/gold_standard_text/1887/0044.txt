 

|
|
|

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

u
trer dans nos rapports. Le grand Bessel, dans la Gradmessung i Ostpreussen, ne donne pas
l'erreur moyenne des observations. Notre regretté maitre le général Baeyer, dans Ia Küslen-
vermessung, et d'autres après lui, ont donné une valeur de l'erreur moyenne d’après une
formule de Encke qui ne répond pas au cas en question. Dans les triangulations du Dane-
mark, de la Prusse et de l'Italie, on donne l'erreur moyenne d’une direction compensée et
de chacun des côtés les plus éloignés de la base, d’après les formules rigoureuses de Gauss.

Mais il faut remarquer que, pour combler la lacune du rapport général sur les trian-
oulations, il n’est pas nécessaire de donner une valeur rigoureuse de l'erreur moyenne des
angles et des côtés.

Il suffit d'adopter un procédé simple, unique el conforme au bon sens pour se faire
une idée du degré de précision des mesures angulaires. Cest pour cela que le général Fer-
rero propose de se contenter de déterminer l'erreur moyenne d'un angle d'après l'erreur
de clôture des triangles, c’est-à-dire par la formule

> À
Mm = :
3 N

dans laquelle les A indiquent, en tenant compte de l’exces spheroidique, les erreurs de clo-
ture des triangles et x le nombre des triangles du réseau.

Si l'on voulait au contraire une solution plus rigoureuse, lon serait dans la nécessité
de faire des calculs laborieux, sans avantage au point de vue d’un rapport de caractère histo-
rique et statistique.

 

M. le Président remercie M. Ferrero d’avoir soulevé cette question importante, mais
qui est assez compliquée pour qu’il vaille la peine de la soumettre à l'étude préalable d’une
sous-commission qu’il propose de composer de MM. Ferrero, Helmert et Tisserand.

Cette proposition est adoptée.

M. Perrier avoue ne pas être arrêté suffisamment sur le sens de l'erreur moyenne
d’une triangulation. Il croit que l’exactitude des angles seuls ne suffit pas pour la déterminer ;
mais que l'accord des bases, réunies entre elles par les chaines de triangles, est beaucoup
plus apte à la caractériser.

M. Ferrero répond que l'incertitude des longueurs des côtés augmente nécessaire-
ment avec leur éloignement de la base, même avec les triangulations les plus parfaites. II
s’agit avant tout de trouver un moyen rationnel de donner une mesure d’exactitude pour les
angles ou les directions. C’est pour cela que non seulement les Italiens, mais Andre de
Copenhague, la « Landesvermessung » de Berlin, le général Ibañez, se sont bornés à indiquer
l'erreur moyenne des angles ou des directions, et non pas des longueurs. Il est vrai qu'on
pourrait employer encore les valeurs des corrections résultant de la compensation, pour me-
spre de l’exactitude des réseaux; mais on entrerait ainsi dans une solution plus difficile, si
l’on ne veut pas prendre la valeur moyenne de toutes les corrections, comme M. Ferrero l’a
proposé dans le temps.

 

 

 

iR

 
