  

 

 

 

 

 

 

 

 

 

66

À x ; 5 a3 . . & fi Le
à MM. les délégués qu’ils veuillent bien, jusqu’a la session suivante, réfléchi

permanente et : se
sur la meilleure utilisation d’une partie du total de ces moyens disponibles.

D'après notre avis, il se présentera un assez orand nombre de travaux ou de

publications scientifiques d’un intérêt commun à nous tous, pour lesquels il sera très utile

voir di enti t nous croyons
d’avoir disponibles, au moment propice, quelques moyens de subvention, € \

qu'à cet égard l'avenir nous ouvrira encore des horizons plus étendus.

4» Nous devons enfin laisser à la session suivante le soin du contrôle et de la
décharge de la gestion financière pour l'exercice de 1887, qui n'est pas encore terminé. M. le
Directeur du Bureau central soumettra alors les comptes de 1887 et les pièces justificatives
a examen de la Commission permanente.

, Le Rapporteur,

FŒRSTER.

Le rapport constate, entre autres, que le solde disponible est actuellement de 6750
francs, et que la Commission propose de porter ce solde à compte nouveau.

Celle proposilion, ainsi que les autres, comprises dans le rapport, sont adoptées.

M. Hirsch est d'accord que les finances de la Commission ne se trouvent pas dans un état
trop mauvais. Toutefois il croit utile de constater qu’à la date du 1er octobre, quatre des
Etats associés, parmi lesquels trois grands pays, n’ont pas encore versé leur contribution
pour l’année courante. C’est une preuve nouvelle à l’appui de la nécessité, mentionnée dans
le rapport du Bureau de la Commission, lu dans la première séance, de proposer à la pro-
chaine Conférence générale de demander aux puissances signataires d'ajouter à la Conven-
tion une disposition qui permette au Bureau de la Commission de s'adresser aux Gou-
vernements de ces Etats, ainsi que cela est prévu, par exemple, dans la Convention du
metre,

M. von Kalmar lit une communication de son gouvernement, par laquelle il est auto-
risé 4 annoncer le paiement immédiat de la contribution autrichienne.

M. le President remercie la Sous-Commission et son rapporteur, M. Foerster.
M. le Président, passant au dernier sujet à l’ordre du jour, donne la parole à
M. Helmert, qui lit le projet de programme pour les travaux du Bureau central. Le voici :

Programme de travail pour le Bureau central en 4 887/88.

I. (Conformement à la proposition de M. Bakhuysen) : Etude de la distribution
aphique des points astronomiques, afin de préparer, d'accord avec les
différents Etats, de nouvelles déterminations astronomiques de

2. Etude et catalogue des déclinaisons d’étoiles qui résultent des déterminations de
latitudes faites jusqu’à présent par des observations au 1er vertical.

r

geogr:

À

délégués des
coordonnées géographiques.

   
 

 

 
  

 

   
    
     

   
  
   
   
