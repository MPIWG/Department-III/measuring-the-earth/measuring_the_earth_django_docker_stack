 

A
10

es

 

 

67

noch an den sehr unregelmässigen Mondrändern ausgeführt werden, könnte man für deren
Verwendung zu Gunsten der geodätischen Probleme fast nichts erhoffen.

« In der That geht jeder Fehler im Winkelwerthe, der in der Annahme der Lage des
Radiusvectors eines Beobachtungsortes begangen wird, mit beinahe !/,, in die Reduktion der
Mondbeobachtungen ein, so dass eine einzelne Positionsbestimmung des Mondes (welche nach
den bisherigen Erfahrungen mit einem wahrscheinlichen Fehler behaftet sein dürfte, welcher
1 Sekunde übersteigt) einer Unsicherheit in der Lage des Radiusvectors des Beobachtungs-
ortes von über 60 Sekunden entspricht. Und sicherlich scheinen Unsicherheiten von diesem
Betrage noch ziemlich selten zu sein.

«Es wird sich deshalb als nothwendig herausstellen, die Ränder aus allen Mond-
beobachtungen zu eliminiren und letztere auf eine Gesammtheit von ziemlich zahlreichen,
auf die ganze Mondoberfläche vertheilten Punkten zu beziehen. Bei der Wahl dieser Punkte
wäre nalürlich auch der Bestimmung der Umdrehungszeit und der Lage des Schwerpunktes
des Mondes Rechnung zu tragen. Auch müssten sie so gewählt werden, dass die Beleuch -
tungsphase keinen, nicht leicht zu berechnenden Einfluss auf ihre Lage ausüben könnte; und
endlich müsste es möglich sein, diese Punkte auch mit Ilülfe des Erdlichtes zu messen.

« Die Lage dieser Hauptpunkte der Mondoberfläche ist an benachbarte Sterne anzu-
schliessen; und um einige gemeinsame Fondamentalreihen ausführen zu können, welche
einigermassen die correspondirenden Beobachtungen der Bedeckungen gewisser Sterngruppen
ersetzen würden, muss ein gemeinsames System von Vergleichsternen für die Haupt-Mond-
punkte gewählt werden.

« Diese Beobachtungen wären am Heliometer auszuführen; auch kann man vielleicht
Spiegeleombinalionen anwenden, wie dieselben jüngst von Herrn Lewy mit neuen Vor-
schlägen und geschickten Vorrichtungen eingeführt worden sind.

« Der Stand der Frage, über die ich berichte, ist somit zur Zeit ein solcher, dass ich
nicht eigent'iche Vorschläge der Permanenten Commission zu unterbreiten habe; ich werde
mir indessen erlauben, auf die Frage später zurückzukommen. Für den Augenblick bitte ich
meine astronomischen und geodätischen Collegen, zu der weiteren Entwickelung der Mond-
beobachtungen auch ihrerseits beitragen zu wollen, denn sicherlich können dieselben nach
ernstlicher Vervollkommnung dazu beitragen, einige schwierige Punkte in der Erdmes-
sung aufzuklären.
« Der Berichterstatter :

« Gezeichnet : KŒRSTER. »

Herr von Kalmar liest auf französisch die neue Redaktion des Artikel 11° des Regle-
ments der Permanenten Commission und des Artikels 5 desjenigen des Centralbureau’s,
welche an die Specialeommission zurtickgewiesen worden waren. Diese Artikel werden in
ihrer neuen Forın ohne Opposition angenommen !.

! Diese Artikel sind bereits in dieser Form in den beiden Reglementen enthalten, welche sich unter
den Annexen dieses Bandes finden. A. i.

 
