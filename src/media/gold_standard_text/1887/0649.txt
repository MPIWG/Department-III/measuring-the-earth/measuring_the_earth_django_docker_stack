 

 

 

Note sur les triangulations de France,

d'Algérie et de Tunisie.

La triangulation primordiale de la France a été exécutée de 1792 à 1843.

Elle s'appuie sur sept grandes bases: Melun, Perpignan, Brest, Grisisheim, Bordeaux,
Gourbera, Aix, et comprend environ 740 triangles formant les trois chaines méridiennes
de Paris, Sedan et Bayeux, les six chaines dirigées suivant les parallèles d'Amiens, Paris,
Bourges, Lyon (moyen), Rodez, Pyrénées, la petite méridienne de Fontainebleau, la mé-
ridienne de Strasbourg et enfin la chaine littorale du Sud-Est. Ä

Ces chaines méridiennes et paralléles forment des quadrilateres de 200 kilométres
environ de côté, remplis de triangles du 1% ordre déterminés avec la méme exactitude
que ceux des chaînes principales ; — on peut donc dire que la surface de la France
est couverte d’un réseau continu de triangles du 1% ordre.

Les angles ont été mesurés par la méthode de la répétition à l’aide de 3 séries au
moins de 20 duplications chacune, soit 60 fois. — Les cercles répétiteurs employés, con-
struits par Lenoir, Gambery, etc., étaient gradués suivant la division centésimale du
quadrant ; le diamètre des cercles divisés était de 13 pouces ou 35 centimètres.

_ Les distances zénithales qui ont servi au calcul des différences de niveau géodési-
ques ont été obtenues aussi par voie de répétition, aux heures favorables de la journée,
par 3 séries de duplications chacune.

Les observations astronomiques de latitude, longitude et azimut ont été exécutées
à Paris (station primordiale) et en un grand nombre de points du réseau soit par les
astronomes de l’Observatoire, soit par les ingénieurs géographes, soit par les officiers
d'État-Major du Service Géographique. : |

C’est la méridienne de Paris, calculée par Delambre appuyee sur les deux bases de
Melun et de Perpignan, et supposée parfaite, qui a fourni les longueurs des côtés, Les
altitudes et les coordonnées géographiques de départ de tout le réseau français.

Dans le calcul des coordonnées géographiques on a adopté pour le demi grand axe

la valeur
a = 6 376 989 mètres

 
