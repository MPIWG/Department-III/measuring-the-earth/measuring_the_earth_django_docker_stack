 

 

Deviätidn He tah ver tie lieale.

 

 

   
   
   
   
 
 
 
 
 
 
 
  
 
  
  
  
  

 

 

 

 

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

ES ]
a | oc es | | oS
oS | r ap = 9 = ou Rauenberg wen | g
an Noms des Di (Se oe erg = 0 For Ort à
iS : an 2 foe las ea 15 2 -
3 stations. = 23/25|/ = en a REMARQUES
= © ES 2 à 2 een =e | Observateurs. a Ue déviations dont il est ici
= = | oP y 38 = = a Se se | = question se rapportent Due
+ = EL Ze < SR © jours à la {e colonne.)
= , ade
2 C © m || | Ike = =a
e un 19 59 39.0.8591 390: aA _ 37) 3" ’ = |
: eresse 2,.4,— 3,7— 3,6+ 4,4) Oc oe
R \ | ) + Ocrtel. Donnee pré- | 5
63) 5 Nüremberg 49 97 6.9011 3| 310 a > 2 ia pre- 14884 | Pays parcouru par are
Bastion de l'Ours | he — 6,3— 7,8 — 7,7)— 27x Ort rentes chaines de- collines,
64) Wiilzburg |i9 4 33,17j11 0] 630 a, an
| Petite tour on 301— 1,0 — 92,6— 2,5+ 225i] v. Ort ner nae fl ee
A . ea] 439|| x r ’ pe sridiona e(p atea 1 ©
= ee eure. £9 057,25 12 6 340 — Se 5 A 4.9 + () | 1875 > à de RE tn a
; : Emeran | | Dole la ea 0.4 Schieee lon des Alpes A ee
I se) DRS 18 53 0,6212 34) 330 — 0,5 2 23 9 | où) 85 1804 |pour Munich à environ 14"
‘= S| r de l’eveche ? S SE ae a ole 2, ++ 9 ‚9 S hi |
1 67 Ingolstadt P.A.aul48 46 47.92/44 26] 370 | 2 chiegg 1804
de garde Minucei. se 26! « — 1,0— 8,8— 8,7— 3 7 v. Orft
a 69) m at er 2 Schiegg
Ei | 99) Augsburg is 21 22,94. 1054| 500 | 2, | . een
© | Tour Pick | | 2,3 — 4,3— 41 ee aucune cause apparente d'at-|
. = 10 Munich h en | i 3 || Schiegg 1804 |traetion ee
EX | | Pointtrig. à l'Obs. is 845,800 35/5201 25 261 444 o6l | |
S| | de Een | 20) ; 3 ‚6 Lamont et v. Orff 1865/75 ou plateau de 30-40 km.,
ra Zaren |S | |} en cause apparente d’at-)
= 7A Holzkirchen ie 30: TI 42 600 | : if traction locale. |
> | | Point astronom. es 2| 690 + 1,6— 0,6— 0,4 + 4,6] v. Orff Se
a ae Hohensteig 47 5% 6.09/12 ; i ? 2 I 1873 , Dans un circuit de 14 kml
| Boi boue = 6,0212 9! 480 + 2,91 + 0.7 as 10+ 6,0 aucune cause apparente d’at-
| 73 Irschenberg |47 4 | À ‚0 Oertel 1884 tnachon locale.
49 55.94 1° | en j a
bal ‘Point de Bauernfeind AT 49 95,9411 55) 7505 5,64 3,44 3,714 8, 7) Oertel h een Aa a
| T2) Peissenberg 147 48 7.201414 1 | | | 188% |pres de Kampenwand, Be-
| Tour A 990 + Vo ARTE ae ‘ | nediktbeuren, Wendelstein,
73 Ranpenwand a | | = ’ 4 + 3,6! Schiegg et Lamont |4804/63 Mittenwald. Wahserkopt el
‚Point de Bauernfeind N 612 2211560 410,4 8,2 +- 8,4/+ 43,4) Oertel | SR te de fortes|
76, Benediktbeur 1 | | - E Sell DEE 284 owations, positives. Bonn
en |47 49 41.401114 24! 62 | | || Benediktbeuren l'attraction]
Point astronom. Sey + 620)4+ 6,8-- 4,54 4,84 9 gl I est de 23” Sa CuOn
| | i| | 2 ’ 7,8 Li . 23”, pour Lanserkopf
77, Wendelstein |¢7 42 27,4912 11840 41,7 9,4 pyr ane 1863 elle est ae 20. :
= É J| 14 ¢ + el & + |
| 78 Pyramide biel | + 9,44 9,7)+48,7| Schiegg 1804
[4 | Pfänder Sur la |47 30 28 74 | 9 47/1060 || | ; | DD 4 |
Cone [~ 1,2;— 3,6/— 3,4% 4,6| v. Oppolzer 1872 Df
79 Mittenwald [47 26 40,15/14 16) 920 | lee | Ro 2 An
| Point astronom 2 920 IF 8,94 6.5 —- 6,8414 3 : se reconnaitre sans le caleul
Ir 80 Lanserkopf [47 1% 56,90/14 2 | . 4 ayo Bi, où pourrait s'atten
7 4% 56,90)14 25 ga = re ä une faible deviation,
| fen (One Arigon, n | LAS 8,8 9,414 44,4 Institut milit.-geogr.| — positive. ae
| 84/ Bonn Obser., cere |50 43 44,50 7 6) 60] 0 5 jimp. et royal à Vienne
| | mer: dans la salle Est : D 08 1,4, 1,61 3,4) Foerster 54 | Pay : |
. | 82) Taufstein 50 34 A r | | S 185% a ays de collines. Ce qui)
IE | | Point trigo £ ‚88 9414| 770 |— 1,3 — 5,2 — 5,2i— 0 9 Sachen. et Weiner explique pour Bonn une de-
} 83|F n. 7 14 ,2)) Sadebeck et Werner 14874 viation positive de peu d’im-
& | 83 Feldberg danstetaums|50 13 54,37] 8 28 980 : | * |portance. ee
2 | 84/0 ales Pas I, 417.82) 8,1 3,1) Fischer 1871
a Pp . | 4 4
T ss pel Point trigon.|49 56 21,12) 740 650 — 4,2— 5,5 — 5,4 a
2 | 85) Mannheim 49 29 10,96) 8 28) 100 | le 5,3|— 9,4|— 0,4) Fischer 1872
| a Pilier trig. a = I— 4,71— 6,2— 6,0 — 4,0) Albrecht 1870
© | urlach . |48 89 37.05 - |) | se
= | Point trigon. er 57,02 8 29) 260 lee 4,214 2,5|- le 2,6 = 7 6) All x 2 a eon Oye
on || | Rz ? 70) brechtet Low 1871 Noire, on comprend a fai-
= | 87| Strasbourg N Le © | | Deren prend une fai
5 | |Observ 8 Nouv.|18 35 0,20| 7435| 120 — 2,0— 3,9— : all e déviation positive. |
3 a8 serv. Cercl. mer, : | | N 3,9 — 3,14 4,3) Schur A881 /86 | 4 Vallée du Rhin.des devia-
= | 88} Solitude 48 47 14,37) 9 5) 520/— 2 | | tions positives minimes peu-
| | Point trig DE 975, 520. 22) PO 3,6 9° MU IBEFDICNUS.
| 89) Tübi Ou | | a ' 1,2) Zech 1880 |
| | + ingue ze ol 1% 9 3, 200 | 9,5| h 1e 4.9 0 | en / Forêt-Noire. |
| 90! Feldb er vatoire ab i az 4,21 + 38 Bohnenberger 1895 | Pour la Solitude et Tubin-
> erg F.-Noire 47 52 24,18) 8 01500 — 4,3— 6,6! 64 | au peut s'attendre à une
oint trigon. | 2 | 2) + + A 4 Albrecht 1876 | attraction positive minime
| /
|

 

 

 

   
