 

ur là
Il,
il dy
td)
Veau

e de

u
res,
part
» les
s de
ns le
ope.

SES,

À

 

61

 

M. Perrier ayant répondu que non, la proposition est soumise par M. le Président
au vote et adoptée par la Commission permanente,

M. Ferrero a recu, par l’entrainement que M. Lallemand a mis dans son exposé,
Pimpression que, théoriquement, il ne doit pas étre bien éloigné de la vérité; cependant, au
point de vue pratique, il n’y a que l’expérience qui puisse décider sur le mérite de son
nouvel instrument. C’est pour cela qu’il se permet de demander à M. Lallemand de lui pro-
eurer un ou plusieurs exemplaires du médimarémètre, pour pouvoir l’expérimenter sur les
côtes d'Italie.

M. Belocchi s'étant associé à cette demande, M. Lallemand répond qu’il y fera droit
avec plaisir. Le prix d’un tel instrument, qui est actuellement de 80 à 85 francs, se réduira
plus tard, par la fabrication en grand, à 50 ou 60 francs.

M. Bakhuysen est heureux d’avoir entendu M. Lallemand parler de la jonction entre
la France et la Belgique; cela donne l'espoir de rattacher la mer du Nord à la Méditer-
ranée, pourvu qu’on fasse aussi la jonction entre les réseaux belge et hollandais, car il est
triste de dire que, jusqu’à présent, le port d'Amsterdam n’a pas encore pu être relié à celui
d'Ostende. Il fait donc la proposition que, par l'intermédiaire du Bureau central, M. le Major
Hennequin soit prié de joindre le réseau du nivellement belge à celui des Pays-Bas, comme
à celui de la France.

Cette proposition étant appuyée par M. Lallemand, la Commission décide d’y donner
suite.

La discussion sur les différents sujets se rattachant au rapport de M. Lallemand
élant close, M. Hirsch demande au Président la permission de rappeler aux membres de la
Conférence que, dans la seconde séance déjà, M. Helmert a remis à chacun d’eux une liste
imprimée, destinée à rassembler le nombre des exemplaires des publications de l'Association
demandé par chaque pays. Il fait circuler une pareille liste et prie ses collègues d’y inscrire
les changements qu’ils désirent voir apporter sous ce rapport.

À l'observation de M. von Kalmar, que quelques-uns parmi les délégués doivent
d’abord prendre l'avis de leur gouvernement, M. le Secrétaire répond qu'on n’a qu’à lui
envoyer, jusqu’à la fin de l’année, le chiffre d'exemplaires qui sera demandé définitivement.

M. Helmert exprime le vœu que les recherches sur l'influence de la pesanteur sur
les nivellements, commencées par M. le major de Sterneck, à Vienne, puissent être conti-
nuées.

La Commission est unanime pour appuyer ce vœu.

M. le Président donne la parole à M. Hirsch pour présenter le rapport sur les tra-
vaux exécutés en Suisse.

M. Hirsch, tenant compte du peu de temps qui reste à la Commission, se borne à

1 Par suite de la mort de notre regretté collègue, le mémoire du Colonel Gouillé n’est pas parvenu
au Secrétaire. A. H.

  

  
