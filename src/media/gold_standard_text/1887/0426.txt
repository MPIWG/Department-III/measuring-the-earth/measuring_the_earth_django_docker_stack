 

bs

|

À

|

I
=
j

I
i

5

A

m
Aarberg (valeur provisoire-définilive) = — 0), 040
Weinfelden id. == — 1), 059
Bellinzone id. — — 0,047

En Russie, des expériences intéressantes ont été faites par M. Dollen, astronome de
l’observatoire de Pulkowa, sur l'appareil des bases imaginé par M. Edio Jäderin. Get appareil
se compose de deux fils mötalliques, l’un en acier, l’autre en laiton, de 1,5 & 2” de dia-
metre et de 25 métres de longueur, dont l’ensemble forme un véritable thermomètre métal-
lique. Les fils sont soumis à une tension constante. — D’après M. Jäderin, son appareil
donnerait la longueur d’une base à Ane prés d’erreur relative, soit 4 25 millimétres
près pour une longueur de dix kilomètres, ce qui serait très satisfaisant, surtout si l’on con-
sidere que les opérations s’accomplissent sur le terrain avec une très grande rapidité. —
M. Dôüllen, toutefois, hâtons-nous de le dire, pense que lappareil précité ne comportera le
degré de précision qui lui est attribué par son auteur que lorsqu'il aura été lPobjet de per-
fectionnements jugés nécessaires. — Il y a là, toutefois, une tentative intéressante que j'ai
cru utile de signaler à l'Association.

Le nombre total des bases mesurées en Europe s’élève aujourd’hui à 90.

Ce nombre serait assurément suffisant pour asseoir sur des indications solides la
triangulation de l’Europe, mais à une condition toutefois, c’est que les lignes mesurées eus-
sent la même précision et fussent rapportées à une même unité de longueur. Peut-on dire
que celte condition soit tout à fait réalisée ? Nous n’hésitons pas à affirmer que non.

Si nous considérons, en effet, les appareils employés à la mesure des bases, nous y
reconnaissons la diversité la plus complète. Des règles bimétalliques, généralement au nombre
de 4, ont été employées en France (Borda), en Allemagne (Bessel), en Italie, en Belgique,
en Russie (Struve, Tenner). Parmi ces régles, les unes sont a languettes (Borda), d’autres
pourvues de coins en verre (Bessel), pour mesurer les intervalles consécutifs; d’autres
encore sont munies de leviers à touches mobiles (Russie) ou à contact (Etats Unis), ou même
sont formées d'appareils à compensation. En Autriche, l'appareil adopté se compose de quatre
règles en fer pourvues de languettes dont les températures sont données, pour chaque règle
en fer, par deux thermomètres à mercure logés dans les règles. Dans les opérations moder-
nes, on n'emploie plus qu’une seule règle, soit bimétallique formant thermomètre (Espagne,
Algérie), soit formée d’un seul métal et pourvue de thermomètres donnant la température,
el on mesure les distances successives comprises entre les axes verticaux de microscopes
dressés le long de la ligne à mesurer. Autant de pays, autant de systèmes, pourrait-on dire,
et par conséquent, autant de degrés de précision.

Si maintenant nous considérons les longueurs des régles géodésiques, nous consta-

CT

r ’ 3 LE . r . > Sr . r . r r
lerons qu’elles ont été exprimées en fonction d’unités différentes : toise du Pérou ou étalon

 
   
   
 
 
 
 
   
   
 
   
 
 
 
 
 
 
 
 
