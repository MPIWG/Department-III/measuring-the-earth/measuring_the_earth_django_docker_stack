 

 

8

  

nächstens ausgeführt werden. Wünschenswerth wäre auch die Bestimmung der Längen-
differenz zwischen Ragusa und einer Station im Süden Italiens, und dass die letzte Unsicher-
heit in den französisch-schweizerischen Längen zum Verschwinden gebracht werde.

Auch die Bestimmungen erster Ordnung der Breiten und Azimute sind beinahe ganz
vollendet ; in verschiedenen Ländern hat man sogar weit mehr gethan. Indessen giebt es
noch anderes auszuführen.

In dem gegenwärtigen Stande der Geodäsie genügt es nicht mehr, unter einem Me-
ridiane oder einem Breitenkreise, die Lage einiger Stationen, welche durch eine Dreiecks-
kette unter sich verbunden sind, zu bestimmen, um daraus die Längen der Grade unter
verschiedenen Breiten und die Dimensionen eines Kllipsoides abzuleiten, welches diesen
Messungen am besten entspricht. Man muss weiter gehen und in den verschiedenen Gegenden
die Abweichungen des Geoids von einem angenommenen Vergleichsellipsoid bestimmen, das
am besten die Gesammtheit der Längen der Grade darstellt.

Um diese Abweichungen zu ermitteln, bedürfen wir in jedem Lande einer grossen
Anzahl trigonometrischer Punkte, in denen die Differenzen für Breite und Länge oder auch
das Azimut mit Genauigkeit bestimmt sind. Die Zahl und die Lage dieser Punkte hängt von
den Dreiecksketten ab, die ınan gemessen hat, und ausserdem von der Gestaltung des Bodens,
so dass es sehr schwierig ist, die Lücken anzugeben, die in dieser Beziehung in unserem
Netze noch bestehen.

Will man in dem Studium über die Form der Erdoberfläche Fortschritte machen, so
muss diese Arbeit unbedingt ausgeführt werden, und da sie sich unmittelbar an die Unter-
suchung des gelehrten Direktors des Centralbureaus über die lokalen Lothabweichungen an-
schliesst, so schlage ich der Permanenten Commission vor, Herrn Helmert einzuladen, der
General-Gonferenz einen Bericht über diesen Gegenstand vorzulegen und in demselben die
Lücken anzugeben, die in den Längen, Breiten und Azimuten der verschiedenen trigonome-
trischen Punkte bestehen, damit wir zu einer vollständigen Bestimmung der Abweichungen
gelangen können, welche die Oberfläche des Geoids in den verschiedenen Ländern Europas,
in denen die Triangulationen ausgeführt sind, mit Bezug auf die Oberfläche eines Ver-

gleichsellipsoids darbietet.

'Gez.: H.-G. van pe SANDE BAKHUYZEN.

 

 

 
