 

15

 

| Punkte numerisch aufgestellt (selbstverständlich mit Benutzung einer für den vorliegenden
Zweck angemessenen Vereinfachung) und dabei

 

log ze log da = 6, 224 — 10

a

eesetzt. Unter Festhaltung des nach Tabelle I" für Paris angenommenen Werthes der Loth-
ou. fanden sich für Nr. 3 und 8, welche bereits durch die vorerwähnte Berechnung
bekannt geworden waren, wieder dieselben Werthe der Lothabweichungen. Auch stimmt
Nr. 3 mit der entsprechenden Angabe der Tabelle I” überein. Dagegen zeigt Nr. 13, der
südlichste Punkt des französischen Meridianbogens in Frankreich, Carcassonne, einen wesent-
lichen Unterschied mit Tabelle I”, nämlich 1°8. Dieses ist wohl darauf zurückzuführen, dass
Villarceau eine neuere ende e Berechnung von Paris bis Carcassonne benutzte, während
die Glarke’schen Annahmen für diesen Meridtanbogen noch von Puissant entlehnt sind. Die
kürzlich erfolgte Neumessung dieses Bogens durch General Perrier wird Gelegenheit geben,
einen neuen definitiven Werth seiner Länge einzuführen.

 

a Es sei noch erwahnt, dass die Angaben fiir Greenwich und Strassburg, welche in Vil-
larceau’s Arbeit vorkommen, nach gehöriger Reduktion genügend mit den entsprechenden
Werthen der Tabelle I” bezw. der weiterhin vorgeführten Tabelle Ill übereinstimmen. Die
Differenzen betragen nur ein paar Zehntelsekunden.

Die Lothabweichung für Nr. 5, Saint-Martin-du-Tertre, wurde aus den Angaben in
Villarceau’s bekannten Mémoire: De l'effet des attractions locules sur les longitudes et les
azimuls, applications d'un nouveau théorème, ete. ! entlehnt.

Die Angabe für Nr. 4, Amiens, ist den Levret’schen Mittheilungen entnommen ?. Da
dieselben ein Ellipsoid mit

Bene

a 6 918 933 on und 994, 31
>

    

voraussetzen, musste auf Clarke’s Ellipsoid von 1880 mittelst

da
a

 

log = 4, 444 — 10 und logda = 4, 978 — 10

 

redueirt werden. Zur Prüfung wurden auch die von Levret abgeleiteten Lothabweichungen
. von Dünkirchen und Greenwich umgerechnet, und bis auf 0° 0 bezw. 0. ‘9 übereinstimmend
ı mit Tabelle I” befunden

In der Rubilrk « Bemerkungen » finden sich die Angaben über die Namen der Beob-
achter und das Jahr der Beobachtung der geographischen Breite. Die Werthe derselben sind
für die meisten Stationen nach Villarceau’s Abhandlungen über die Déterminations astrono-

I i: 1 Journal de Mathématiques pures et appliquées. 2° série, t. XII, 1867.
2 Mémorial du Dépôt général de la Guerre. Supplément au t. IX. Paris, 1865; p. 68 et 75.

 

 
