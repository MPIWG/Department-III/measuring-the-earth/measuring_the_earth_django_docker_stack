 

|
i

 

 

 

 

 

8

 

plus solides et de rechercher si, par une disposition spéciale des appareils et des observa-
tions, il ne serait pas possible d’éliminer l’action du support.

Voici la méthode qui nous a paru propre à réaliser celte élimination :
Considérons deux pendules réversibles de longueurs différentes, disposés pour re-
cevoir les deux mêmes couteaux et pour osciller sur un même support d’élasticité &. Soit :

À, 4, les longueurs des deux pendules, mesurées entre les arêtes des couteaux.
/ 1 2 OR x
h, h,, h, h, les distances des centres de gravité des deux pendules aux arêtes des couteaux.
es’ les rayons de courbure des couteaux.

% % . . . . 1
T T,,T,T, les durées d’oscillation, poids lourd en bas et poids lourd en haut, des deux
pendules oscillant sur les mêmes couteaux, semblablement disposés et dans |
les mêmes limites d'amplitude. À

On a, entre ces quantités, l’intensité g de la pesanteur et le rapport + de la circon-
férence au diamètre, les relations connues :

 

 

Ri u oe ; | rp PE a )

 

ah gia cal hah! |
BEE pape Oey
h,—h, Gg! k  #— hh)

Retranchant membre 4 membre, il vient, en représentant par

7, et Tg les premiers
membres pour abréger l'écriture :

2

2 2 m . À |
Glee (es aise À À + DD, in a2 A (
en MORE. Ve ( ) ane re

 

ou, en remplaçant À el À par h + h, h + h, quantilés équivalentes :

‘ : 1 c 2 5 Die
NE ee jy ana ete)
J 9 g bh ba

x

I faut et il suffit
nuls, que :

» Pour que les deux derniers termes du deuxième membre soient

 

 

 

 
