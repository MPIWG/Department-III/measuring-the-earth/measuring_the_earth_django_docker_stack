 

‘ke

Pr SE

aprés la constatation d’une semblable déformation provenant d’une manipulation délicate,
ce qu'il adviendra pendant une mesure sur le terrain ou en voyage, où la régle sera néces-
sairement maniée plus rudement et subira des variations de température beaucoup plus
brusques.

Nous sommes fondés à conclure de ces faits que le principe de la construction de
notre régle en fer par Brunner n’est pas irréprochable, qu’il faut supprimer toute vis et
tout rivet, et qu'il y aurait avantage à faire la règle d’un seul bloc.

PB. Règle bimélallique.

L'étude de la règle bimétallique conduit à des résultats plus satisfaisants.

Les expériences sur la déformation apparente de la longueur des deux règles, pla-
tine et cuivre, ont permis de constater que le déplacement des supports de la règle, depuis
le milieu jusqu'aux extrémités, produit des variations apparentes de

7 microns sur la règle de platine,
92 » » laiton.

La petitesse de ces nombres, relativement à celui qui a été trouvé pour la régle en
fer, tient à ce que les règles de l’appareil bimétallique sont tracées très près du plan neutre.
On remarquera cependant que la variation est plus grande pour le laiton que pour le
platine; cela tient encore à ce que la règle de laiton est tracée sur deux appendices saillants
situés plus loin du plan neutre que dans la règle de platine. Cette différence dans la defor-
mation des deux règles ne peut-elle pas vicier les indications du thermomètre métallique? Il

est permis d'exprimer un doute à cet égard.

L’inegalite de déformation disparaitrait presque sûrement si l’on pouvait mettre les
deux règles platine et cuivre côte à côte, comme dans l'appareil Porro, au lieu de les super-
poser; il y a peut-être là un progrès à réaliser dans la construction de l’appareil bimétal-
lique.

La mesure des coefficients de dilatation a été effectuée par 60 séries de comparaison,
entre les températures de 4-39 à +375, de la régle bimétallique avec la règle internatio-
nale G,. Les calculs ne sont pas terminés, mais un premier examen des nombres laisse pré-
voir des résultats très concordants.

L’étalonnage de la règle de platine a été fait par comparaison directe de chacune des
4 sections de 1 métre avec le métre international provisoire 1, de même métal. Chaque sec-
tion a été comparée 4 fois.

Quant à l’ötalonnage de la règle de laiton, il a été obtenu en comparant cette rêgle
à la règle de platine, sur les traits extrêmes des deux règles.

Enfin, comme opération de contrôle, on a profité de la présence à Breteuil de la
règle bimétallique de l’Institut géodésique de Berlin pour faire avec cette règle les deux

 
