INDICATION
DES POINTS.

LATITUDE.

LONGITUDE.

N

Autriche-Hongrie.

. ÉPOQUE.

DIRECTEURS
ET OBSERVATEURS.

INSTRUMENTS.

 

 

 

 

 

 

 

Stirbee mare

_ Kukujova
Bayaniste

| Funtina faetje....
| Zichydorf

Ludwigsdorf

Fi Pancsova

Kelskatsch

2 | Veudvar

Beeskerek
Kis Torak
| ‚Csurug

Peterwardein

Ojezow

Koniusza

Sieborowice
- pee

Szyezkow (Chich-
kowo)

| Hohe Gehren.

a Wendelstein

Reisrachkopf

) | Gross Glockner.....
| Ziethenkopf

M. Paralba
_Gülbner Ioch
Rödt Spitz...
Schwarzenstein. .
Hinterthal Kogl..
Kömlö

Viniéni vrch
Iasz Ladäny
Sarhegy
N. Perkata
Erdöhegy
Perdy polje
Bajmok

Harterberg (Har-
dihegy)

Garabhegy

Czernieder

Petrovoszelo
Devecser Puszta...
Kula an der Hut-

 

44° 35' 34”
44 32 1
44 49 12
45 024
45° 13 50
45 21 23
45 9.7
44 52 22
45 417
45 ‘9:26
45 17 17
45 15 48
45 22 52
45 30 16
45 28 26

45 14 30

ov 12, 1

| 50 10 57

50 9 44
50 22 9
50 25° 56

50 22 0
47 45 7
47 42 16
47 27 31
AT 33 33
47 20 2

47 13 17

47 431
46 48 23
46 37 49
46 49 30

47 039

47 36 8
47 17 34
47 21 55
47 5:57
AT. 1 43
AT 8.0
45 10 28
45 58 4
46 47 59

46 45 10
46 46 44
46 32 12
46 30 41
46 37 13
45 41 46
45 36 14

45 37 54

 

 

39°56! 46”
39 46 57
38 34 3
38,31. 56
38 47 23
38 30 9
38 26 8
J0 LS
31 59.33
37 45 42
37 53 42
38 15 47
33 3 26
38 15:52
37 44 22

37 32 4

37 29 26

31.59 52

37 42 86
40 20 14
40 18 26

40 10 12
30 10 57
29 40 41
29 27 32
30 35 23
29 57 43

30 36 28

30.21 39
30 36 35
30 23 8
30 10 13

29 32

38 6
37 16
a7 49
35 59
36 30
37
37
37

36

+ ON
ND & Or © 1 OL OD C0
DU

 

 

1865
1865

1870
1870
‘1870
1870
1870
1870
1870
1870
1870
1870
1870
1870-80

1870-80

1848
1848
1848
1848
1848

1848
1852
1852
1852
1857
1852- 57-80

1857-80-81

1880-81-83
1880-81-84

1884
1882

1880
1880
1880
1880
1880
1881
1880
1880
1880-81

1880
“1880.
1880
1880
1880

 

Ettner.
Ettner.

Vergeiner.

Vergeiner, Rehm.

Vergeiner, Hoffer.

‘Toh. Marieni.

id.

ide
Pechmann.

id.

id.
Muszynski.

de

id.
Grüner.

Muszynski, Grüner, Corti.

Grüner, Corti.

Hartl.
Hartl, Ritter.

Ritter.
Rehm.

| Venus.

Corti.
Venus.

Hartl.
Hoffer.
Kalmar.
Venus, Hartl.
Venus.
id.
id.
id.
id.

Corti.

Kalmär.

 

Starke n° 1 de 10 p.

id.

Reich: n° 2 de 12%,
Reich, n°2 de 12h
id.

id.

id.

id.

id.

id.
id.
id.
id.

|
Reichenbach n° 2 de

12 p. et Starke n° 1
de 10p.

Reichenbach n° 2 de
12 p. et Starke n° 5
de 10 p.

| Reich. n° 1 de 12p.

id.
id.

Reich. n° 2 de 12 p.

id.

i &
Starke n° 3 de 10p.
id.
id.
Starke n° 1 de 10 p.
Starke n° 1 et 3 de

10 p. et n°2 de8p.-

Starke n° 1 de 10 p.
et n°2 de 8 p.

Starke n° 3 de 10 p.

Id, n°2 et 3-de 10 p:

Starke n° 2 de 10 p.
Starke n° 1 de 10 p.-

id.
Starke n° 4 de 10 p. :
Starke n° 2 de 8p.
Starke n° 4 de 10 p.
id.

Starke n° 3 de 10 p.
Starke n° 5 de 10 De |

Starke n° 2 de 8
Id. n° 3et4de 10 p.

Starke n° 4 de 10 p.
id.

Starke n° 2 de 8 p.
ide

 

 

Italie.

 
