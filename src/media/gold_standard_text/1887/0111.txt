 

  

C9
=]

  

DRITTE SITZUNG

24. October 1887.

Abgehalten im Gemeinderaths-Saale in Nizza.

Die Sitzung wird um 2 Uhr 20 Minuten eröffnet.
Präsident : Herr General Ibanez.
Es sind anwesend :

I. Die Mitglieder der Permanenten Commission. Die Herren : Van de Sande Bak-
huysen, Faye, Ferrero, F erster, Helmert, Hirsch, von Kalmar, Zacharie.

Il. Die Delegirten. Die Herren : @’Avila, Bassot, Betocchi, Cornu, Defforges, Loren-
zont, Perrier, de Stefanis.

III. Die Eingeladenen, Die Herren : Bischoffsheim, Cahours, Lallemand, Navarrete,
Stephan, Charles von Struve.

Der Schriftführer verliest auf den Wunsch des Vorsitzenden das Protokoll der letzten

Sitzung.
Herr General Perrier wünscht seinem Berichte, den er in der letzten Sitzung vor-
getragen hat, die Gesammttabelle der bis jetzt gemessenen Basen beizufügen.

Der Sekretär antwortet, dass natürlich der Bericht unverändert so gedruckt werden
wird, wie der Verfasser ihn einsendet!.

Herr Perrier bemerkt noch, dass, wenn Herr Hirsch auf dem Fragebogen die wich-
tige Thatsache bemerkt hatte, dass der Ausdehnungscoefficient der spanischen Messstange sich
geändert hat, ohne dass die Länge derselben eine Veränderung erfahren, so würde sein Be-
richt keinen Anlass zu der gestrigen Discussion gegeben haben.

Herr Hirsch antwortet, dass er einige Monate vorher Herrn Perrier das Protokoll der

‘ General Perrier hat den Bericht, ohne diese Tabelle an das Centralbureau eingesandt; leider hat
ihn der Tod überrascht, ehe er seiner Absicht, die Gesammtliste der bisher gemessenen Grundlinien aufzustel-

len, Folge geben konnte. Der Sekretär.

 

   
