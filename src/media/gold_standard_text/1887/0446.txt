 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

6

Sans parler de l’erreur de fermeture du polygone franco-suisse, la concordance des
deux valeurs de 1885 et de 1863 et d’autres indices, parmi lesquels en première ligne la
valeur inadmissible obtenue en 1877 pour l’azimut de deux mires lointaines, que nous avons
déterminé plusieurs années de suite à l’occasion d’opérations exécutées à Lyon, nous condui-
sent à rejeter d’une manière absolue la valeur obtenue en 1877.

Gelte dernière valeur ne saurait être affectée que du poids zéro. Cest pourquoi
nous ne saurions être d'accord avec notre collègue, M. Hirsch qui, en opérant la compen-
sation d’un certain polygone de longitudes en vue de découvrir l'erreur du quadrilatére
franco-suisse, a adopté pour Paris-Lyon une valeur résultant de la combinaison des observa-
tions de 1877 auxquelles il a donné le poids 1 et de celles de 1885 auxquelles il a attribué
le poids 2.

Les observations de 1877, pour Paris-Lyon, nous tenons à le répéter, doivent être
rejetées comme entachées d'erreurs systématiques et ne sauraient, par conséquent, entrer
dans le calcul d’un polygone à compenser.

PUBLICATIONS

Le Service géographique a publié et a fait distribuer aux membres de l’ Association,
d’abord le Mémoire que nous avons publié en commun avec le général [bañez sur la jonction
géodésique et astronomique de l’Algérie avec l'Espagne et ensuite le Tome XIII du Mémorial
du Dépôt de la Guerre, qui contient, avec le Mémoire précité, les opérations qui se rattachent
à la jonction des deux pays, et ayant spécialement pour objet, en Algérie, la mesure de la
différence de longitude entre M’Sabiha et Alger et la liaison du côté Filhaoussen-M’Sabiha
avec la triangulation primordiale de l'Algérie.

Ajoutons en terminant que le Service géographique poursuit l'impression de deux
tables de logarithmes, l’une à 8 décimales avec indication du dernier chiffre forcé, l’autre à
D décimales, pour les nombres naturels et pour les lignes trigonométriques, dans l'hypothèse
de la division décimale du quadrant. La table des logarithmes des nombres à 8 décimales
sera entiérement imprimée vers la fin de l’année courante.

Signé : Général PERRIER.

 
