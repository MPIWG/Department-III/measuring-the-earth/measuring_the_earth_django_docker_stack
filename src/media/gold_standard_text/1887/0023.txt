aT A MEANT

 

 

F7

pas trace de pouvoir pour imposer ses résolutions à un Etat ou à une corporation scientifique
quelconque contre leur gré. Je vous prie de bien vouloir fixer votre attention, quant à ce
point, sur Part. 2 de la Convention, qui maintient l’ancienne compétence de la Commission
permanente (Voir p. 1 du dernier volume) et Vart II, 4 des anciens statuts de 1864 (Voir
p. 18) qui démontrent avec évidence la compétence strictement consultative de cette
Commission.

« Str de vous avoir donné, suivant mon savoir et ma conscience, des réponses
satisfaisantes à tous vos doutes et questions, j’espère, Monsieur, que, comme vous avez eu
la bonté de me l'indiquer, l'affaire sera prise de nouveau en considération, et que vous-
même, ainsi que les géodésiens de votre Office, vous voudrez concourir en recommandant au
Gouvernement des Etats-Unis d’adhérer à la Convention, se basant sur la raison que vous
avez donnée dans une forme expressive et élevée, «que le Gouvernement américain ne
peut pas convenablement décliner sa participation à une entreprise d’un tel intérêt general
pour le monde scientifique ».

« J’ai Vhonneur, Monsieur, d’étre, avec la plus parfaite considération, votre dévoué.

«A. HIRSCH. »

Cette lettre à reçu la réponse suivante que nous donnons ici également en tra-
duction :

« U. S. Coast and Geodetic Survey Office.
« Washington, le 31 mai 1887.

« Monsieur le professeur D" Hirsch, directeur de l’Observatoire à Neuchâtel.

« Monsieur,

« J’ai l'honneur d’accuser réception de votre lettre en réponse à la mienne du 23
avril courant, qui contenait certaines questions concernant les fonctions et les buts de
l'Association Géodésique Internationale. Je vous remercie également de l’exemplaire de la
« Achte Allgemeine Conferenz der Internationalen Erdmessung, » que vous avez eu
Pobligeance d’envoyer 4 notre bureau; veuillez accepter mes remerciments pour cette aimable
attention.

« Quant au sujet principal de votre lettre, il sera sous peu pris en considération

avec toute l’attention qu’il mérite.

« Veuillez accepter l’assurance de ma considération et croyez-moi,

< Notwe fres respectueux
« F. Tuorn, Superintendent. »

J’ai encore écrit derniérement 4 M. Thorn dans les termes suivants :

PROCES-VERBAUX — 3

 
