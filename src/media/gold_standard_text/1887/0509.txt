|
|
J
|

 

hri

ce

fixe
(ur

MB

 

Annexe No VI.

RAPPORT
LES TRAVAUX DE L'ITARIE

PAR

Le Gener. A, EBRRERO

Depuis la derniére Conférence géodésique, qui a eu lieu a Berlin en octobre 1886,
voici les travaux exécutés par la Commission géodésique italienne.

A. TRIANGULATIONS.

Observations de premier ordre aux stations de Pratomagno,Alia di Sant’ Egidio,
Monte Peglia, Lucardo, Torre del Vignanone, Monte Amuata.

B. NIVELLEMENTS DE PRECISION.

Jo Ligne Piacenza-Fiorenzuola-Borgo-Sun Donnino-Parma-Reggio-Modena-Bologna.
90 Ligne Spezia-Pontremoli-Passo della Cisa-Berceto-Fornovo- Parma.

C. MARÉOGRAPHES.

Actuellement les maréographes rattachés aux lignes de nivellement sont ceux de

Gênes, Livourne, Venise et Porto Corsini.
RAPPORT FERRERO. — 4

 

 

 
