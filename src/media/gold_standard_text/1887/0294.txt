 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

12

 

 

Die erwähnten Beobachtungen sind publicirt :

a) Die in Tiflis angestellten im XXXVIII. Bande der Sapiski der Kaiserlichen Aku-
demie der Wissenschaften, Nr. 1 (in russischer Sprache) ;

b) Alle übrigen in den Astronomischen Nachrichten, Bd. 99, Nr. 2370; Bd. tot
Nr. 2416; Bd. 103, Nr. 9473, Bd. 111; Nr. 2653 und Bd. 113, Nr. 2689 ;

%. Der Einfluss der Flexibilität des Stativs unseres Apparates ist zweimal bestimmt;
im Jahre 1881 durch Vergleichung der Schwingungszeiten der Pendel an einer soliden
steineren Wand mit den Schwingungszeiten, welche Beobachtungen auf dem, auf einem
steinernen Fundamente ruhenden Stative ergaben und ferner im Jahre 1885 durch Anwen-
dung eines leichten Pendels.

Im ersten Falle ergab sich die Correction des Sekundenpendels für Mitschwingen des
Stativs = +- 0,0650 Pariser Linien, im zweiten Falle == + 0,0665 Pariser Linien. Da die
Beobachtungen in Tiflis, Duschett, Gudaur, Wladikawkas, Batum und Jelisawetopol vor der
Bestimmung dieser Correction publieirt wurden (Astr. Nachr., Nr. 2370), so konnte auf die-
selben für’s Erste nur die von Professor Plantamour für die Schweizer Reversionspendel
(welche an Dimensionen und Construction den unseren fast gleich sind), bestimmten Cor-
rectionen angewandt werden. Ausserdem ist zu berücksichtigen, dass bei der Bestimmung
unserer Correction fiir Mitschwingen des Stativs, letzteres auf einem steinernen Fundament
aufgestellt war, während die Pendelbeobachtungen an 2 Orten, in Duschett und Gudaur, auf
einer Holzdiele ausgeführt waren. In Ermangelung einer selbständigen Correction für Holz-
fundamente blieh uns nichts übrig, als unsere Correction fiir diesen Fall den Versuchen des
Professor Plantamour gemäss zu vergrössern. Aus diesen Gründen sind an die in den Astr.
Nachr., Nr. 2370 publieirten Beobachtungen folgende Correctionen für das Mitschwingen
des Stativs anzubringen :

Für die Stationen Tiflis, Wladikawkas, Batum und Jelisawetopol + 0,0650 Par. Lin.
Für die Stationen Duscheit und Gaudaur 9 3 >»

Die Schneiden der Pendel sind bei allen Beobachtungen im Kaukasus nicht umge-
legt, sondrrn genau in derselben Lage gelassen, in welcher im Jahre 1874 Capitain Heaviside
in Kew, in demselben Jahre Professor Zinger in Pulkowa und im Jahre 1876 General
Stebnitzki beobachtet haben. Die durch Verschiedenheit der Abstumpfung der Pendel-
schneiden bedingte und von Capitain Heaviside im Jahre 1874 bestimmte Correction ist in
gleichem Betrage an alle unsere Beobachtungen angebracht.

Bei der Reduktion auf die Meeresfläche ist die Anziehung des zwischen der Beobach-
lungsstation und dem Meeresnivean liegenden Continents in Betracht gezogen, wobei das
Verhältniss der Dichtigkeiten des Terrains und der ganzen Erde = 1/2 angenommen ist.

Wird von der Anziehung des Continents abgesehen und nur allein die Höhe über
dem Meeresnivean berücksichtigt, so nehmen die Resultate der Pendelbeobachtungen im
Kaukasus folgende Gestalt an! :

“Die letzte Rubrik beider Tabellen IN ist von mir eingefügt. H.

 

 

 

 

 

 

 

 
