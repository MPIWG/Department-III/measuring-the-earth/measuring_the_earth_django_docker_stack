 

É

Annexe N° Ve.
MÉMOIRE

LA MESURE DE L'INTENSITÉ DE LA PESANTEUR

Par LE CAPITAINE DÉEFFORGES

Dans un savant rapport présenté en 1883 au congrès de Rome, le regretté M. Op-
polzer à développé d’une façon magistrale les avantages et les inconvénients des diverses
méthodes employées pour la mesure de lintensité, soit absolue, soit relative, de la pesan-
teur.

Insistant sur la valeur théorique et pratique du pendule à réversion pour les me-
sures absolues, il s’est longuement étendu sur les précautions à prendre dans les observa-
tions. Il faut retenir surtout ces deux points très importants :

Nécessité absolue d’opérer, dans les deux positions du pendule réversible, poids
lourd en haut et poids lourd en bas, entre les mémes limites d’amplitude.

Nécessité de tenir compte du mouvement communiqué au support par le pendule
en mouvement, soit par une correction calculée à laide du coefficient d’élasticité, soit a
l’aide du pendule léger auxiliaire de M. Cellérier.

Dans la mesure de lintensité relative, M. Oppolzer a indiqué les déformations du
couteau et du pendule, causées par l’usure et le temps, comme une source derreurs redou-
table et dont il faut se préoccuper.

Depuis plusieurs années, le service géographique de l’armée française se préoccupait
de la question de la pesanteur. Nos recherches se sont portées surtout sur le mouvement du
support et sur l’invariabilité présumée des pendules. Elles nous ont conduit à proposer deux
méthodes nouvelles de mesure, l’une pour l'intensité absolue, Pautre pour l'intensité rela-
ve. Ce sont ces méthodes, toutes deux fondées sur le principe de la réversion, que nous
nous proposons d'exposer brièvement 1c1.

ASSOC. GHOD. INT. — AN. NO Vo. —— 4

 
