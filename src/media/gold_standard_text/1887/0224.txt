 

 

 

 

 

 

 

 

 

 

 

 

 

 

20

gangenen Fehler wenigstens für die Punkte von Dänemark bis Oberitalien kaum 1 bis 2
Zehntelsekunden; hier sind auch die Triangulationen von völlig ausreichender Güte und
zumeist ebenso die astronomischen Angaben für die geographischen Breiten, wiewohl die in
Bayern z. Th. benutzten Bestimmungen aus dem Anfang dieses Jahrhunderts weniger sicher
als die neueren Breitenbestimmungen sind und einen mittleren Fehler von rund + 1” be-
sitzen. Welche Sicherheit die Angaben für Wien und Fiume, Nr. 32 und 100, haben, und
desgleichen die für Lanserkopf und Kumenberg, Nr. 80 und 92, ist mir allerdings auch un-
bekannt. Ich glaube aber dass sie hinreicht.

Verhältnissmässig das mindeste Vertrauen verdient die Mehrzahl der Angaben von
Nr. 101 bis 117, sowohl wegen des geodälischen, wie wegen des astronomischen Theils
ihrer Entstehung. Es kann sein, dass eine sorgfältigere Bestimmung hier Aenderungen bis zu
2” geben wird, — mehr jedoch schwerlich. Was insbesondere die älteren astronomischen
Bestimmungen anbetrifft, so dürfte für sie die Annahme eines mittleren Fehlers von + 1” die
Sicherheit eher unter- als überschätzen, wie aus mehreren Kontrollen hervorgeht. Bei Flo-
renz, Nr. 113, besteht eine neue Bestimmung der geographischen Breite!, welche sie aber
nur etwa 1/,” kleiner giebt und wobei wegen der bezüglichen geodätischen Uebertragung
auch Lokalanziehung von Einfluss sein kann. Für Rom, Nr. 115, wird im Berliner astrono-
mischen Jahrbuch die Breite um 1'6 grösser angegeben; wie es aber hierbei mit der Punkt-
identität beschaffen ist, habe ich nicht ermittelt. Bei Nr. 116, Neapel, glaubte ich annehmen
zu dürfen, dass die neuen Angaben für die Breite sich auf denjenigen Punkt beziehen, dessen
Breite Marieni zu 40° 51° 47.0 angiebt, und führte daher den neueren Breitenwerth, der
15 kleiner ist, ein.

Im Allgemeinen bemerke ich hier noch, dass ich überall die Lothabweichung mög-
lichst genau auf den wirklichen Beobachtungsort bezogen, also thunlichst die « beobachtete »
geographische Breite uncentrirt in Tabelle III angesetzt habe. Die geographischen Längen
und Meereshöhen sind meist den betreffenden Werken über die Triangulirungen entlehnt; die
Höhen für Dänemark und Bayern theilten mir die Herren von Zachariæ und von Orff freund-
lichst auf Anfrage brieflich mit. Ich habe überall eine Abrundung eintreten lassen, da für
den Zweck des Ueberblicks Angaben auf 10 m genügen, in den meisten Fällen aber auch der
einzelne Meter ohne Weitlaufigkeit nicht zu konstatiren war.

Eine Betrachtung der in Tabelle IIl enthaltenen, in Bezug auf Rauenberg als Null-
punkt abgeleiteten Lothabweichungswerthe (also der drei ersten Spalten dieser Werthe) zeigt
ein Vorherrschen des Minuszeichens. Ordnet man insbesondere die Werthe der 3. Spalte, die
sich auf Glarke’s Ellipsoid von 1880 beziehen, nach der Breite, so folgt :

' Processo verbale delle sedute della Commissione geodetica italiana, tenute in Milano nei Giorni
27 e 28 Settembre 1886. Firenze, 1887, p. 36.

 
