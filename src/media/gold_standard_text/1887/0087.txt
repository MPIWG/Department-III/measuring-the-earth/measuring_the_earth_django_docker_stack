 

(his

sol
nge
ids.
al,

ner
der
hen
1550

13

« U.S. Coast and Geodetic Survey Office,
« Washington, April 23rd, 1887.

« Professor Dr. Hirsch, Director of Observatory, Neuchatel.
«Dir,

« On the Ist February last the Minister of Germany at this Capital, in behalf of the
Imperial government of Germany, addressed to the Honorable the Secretary of State a cour-
teous invitation to this government to adhere to the Convention (Oct. 1886) concerning the
International Geodetic Association for the measurement of the Earth, which invitation toge-
ther with a copy of the Convention and of the Results of the General Conference at Berlin in
Oct. 1886, was in due course referred to this Office for report.

« For certain reasons, which were mentioned with some detail in my report, the
gentlemen familiar with Geodetic work, who are engaged at this Office, concurred in my
opinion that it is inexpedient for this government to adhere to the Convention mentioned,
and my report to that effect having been duly forwarded, the Minister of Germany was advi-
sed accordingly. Chief among the reasons which led us to the conclusion mentioned, was the
impression, derived from the tenor of the Convention and the history of the Association, that
the Convention, notwithstanding its title, contemplates the consideration of geodetic opera-
tions with reference only to Europe. That impression received strong confirmation from the
announcement in paragraph IV, page 4 of the Results of the General Conference of the Asso-
ciation held at Berlin in October 1886, that the numerous reports whose preparation had
been assigned to various distinguished specialists were to be prepared « with a view to the
« advancement and extension of the scientific work of the measurement of degrees in Ku-
«rope ».

« It was thought also that the functions of the Association were so far supervisory and
authoritative that the methods, etc. of geodetic operations carried on by any nation during
its membership in the Association would, asa matter of honorable good faith, be, to some
extent, subject to the control of the Association, or suggestion of the General Conference.

« Having in my report declared that « Whenever an International Association shall
« disregard Continental areas and contemplate the entire planet as within the field of its
« measurements, experiments and observations, this government cannot with propriety de-
«cline to participate in an enterprise of such general interest to the scientific world, » |
stated that « the considerations above adverted to, » (among others the consideration of ter-
ritorial limitation and supervisory authority), « induce the conclusion that, while we should
« permit nothing to interrupt or to abate the useful intimacy of our relations with the scien-
« tific men, or the warmth of our interest in the scientific affairs of Europe, it is inexpedient
«for the United States Government to adhere to the Convention of October 1886 concerning
«the International Geodetic Association for the measurement of the Earth, in conformity
« with the invitation so courteously conveyed by the Minister of Germany. »

 
