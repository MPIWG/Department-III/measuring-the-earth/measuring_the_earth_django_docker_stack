 

 

ET

tS ENE SPT RIE AER

ee

nee

 

3

In den Vereinigten Stauten von Nordamerika sind 1879 und 1880 von Peirce mit dem
früher von ihm benutzten Reversionspendel weitere Messungen, nämlich in Allegheny,
Ebensburgh und York angestellt und bereits publieirt worden. Ausserdem sind mit den oben
genannten drei invariablen Pendeln Messungen von E. Smith in Washington, Auckland, Sidney,
Singapore, Tokio und San Francisco gelegentlich der Venusexpedition von 1882 ausgeführt
worden, so dass Ostasien und Australien mit Nordamerika, Europa und Indien in Ver-
bindung gebracht sind. Auch diese Messungen sind, wie die vorher erwihnten, in den Reports
der Coast and Geodetic Survey zur Veröffentlichung gebracht.

Nach gefälliger Mittheilung des Direktors der deutschen Seewarte, Herrn Neumayer,
liegen auch Messungen von deutscher Seite vor für Melbourne, Aucklands Inseln, Kerguelen
und Süd-Georgien. Die Beobachtungen am erstgenannten Orte wurden von ihm selbst mit
dem Lohmeier’schen Reversionspendel angestellt, welches später C. F. W. Peters 1869 auf
drei preussischen Stationen schwingen liess. Die Beobachtungen auf den Aucklands Inseln
und Kerguelen sind gelegentlich der Venusexpedition 1874 gewonnen, und diejenigen auf
Süd-Georgien bei dem Aufenthalt des Dr. Schrader für die Internationale Polarforschung
1882/83.

Schliesslich ist noch zu erwähnen, dass für Portugal und Norwegen Pendelbeobach-
tungen in Aussicht gestellt sind. Im erstgenannten Lande ist bereits ein Reversionspendel
angeschafft. Es macht sich sonach ein erfreuliches Leben auf dem Gebiete der Schwere-
messungen geltend; binnen Kurzem werden wir weit genauer als bisher wissen, wie die
Schwerkralt in begrenzten Gebieten verläuft, namentlich, ob sich neben lokalen Anomalien
auch regionale Anomalien zeigen, d.h. solche, die sich in gleichem Sinne über weitere Ge-
biete erstrecken. Für den gegenwärtigen Bericht musste ich mich allerdings auf nur wenige
specielle Mittheilungen beschränken, da von den neueren Messungen noch zu wenig fertig
redueirt vorliegt. Eine neue Zusammenstellung dessen, was zur Zeit überhaupt an alten und
neuen Messungen veröffentlicht ist, würde mich zu keinen andern Ergebnissen führen, als
ich im If. Bande meiner mathematischen und physikalischen Theorien der höheren Geodäsie,
3. Kapitel, p. 202-222, 1884, dargelegt habe.

Die speciellen Mittheilungen, welche ich im Nachstehenden geben kann, sind fol-
gende:

1. Ergebnisse der Untersuchungen über lokale Störungen der Schwerkraft aus den
Jahren 1883 und 1884 durch Herrn Major von Sterneck, Tabelle I.

2. Beobachtungen mit drei unveränderlichen Pendeln, durch Edwin Smith u. a. nach
den Publikationen der Coast Survey, Tabelle Il.
3. Kurzer Bericht über die im Kaukasus angestellten Beobachtungen, von Herrn Gene-

rallieutenant Stebnitzki gefälligst mitgetheilt als Antwort auf das noch auf Verlangen Oppolzer’s
versandte Circular des Centralbiireaus vom 25. November 1886, Tabelle IITA und III.

4. Zusammenstellung von 26 Pendellängen an 23 Orten, Tabelle IV, auf Grund folgen-
der Messungen :

 
