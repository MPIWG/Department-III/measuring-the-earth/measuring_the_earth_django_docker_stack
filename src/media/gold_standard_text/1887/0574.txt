 

 

 

2

  

5. Le Bureau central réunit toutes les données nécessaires pour pouvoir présenter,
à la demande de la Commission permanente, des rapports systématiques et disposés par
matières, sur l’état des travaux de l'Association. Ces rapports sont indépendants des rapports
spéciaux sur l’avancement triennal des différentes branches des travaux de l’Association, qui
— comme par le passé — seront présentés dans les Conférences générales par les rappor-
teurs désignés dans la Conférence précédente.

6. Sous le contrôle de la Commission permanente, le Bureau central exécute les tra-
vaux et conduitles négociations nécessaires afin d’obtenir l’uniformité voulue pour les mesures
géodésiques et astronomiques.

7. En outre, le Bureau central a l'obligation de se tenir au courant de toutes les
publications sur la géodésie, et de suivre les progrès théoriques et pratiques de la
science, de façon qu’il puisse remplir réellement sa mission d’organe exécutif de la Com-
mission permanente de l'Association géodésique internationale, et qu’il puisse, à côté des
fonctions définies dans les articles précédents, satisfaire à toutes les exigences que les Con-
férences générales et la Commission permanente seraient dans le cas de demander au
Bureau central, dans l'intérêt de leurs travaux géodésiques et astronomiques.
