a ee

Espagne.

Jen © RR SS ESS SSS SSS SSS SSS

INDICATION | DIRECTEURS

ODIDOR UN m

 

DES POINTS.

Peñas

Gamonal
Trigueiro
Braña-Caballo

Casas-Viejas

S. Vicente

_Campanario

Fuentes
Teso-Santo
Castillejo

Corral

4

Diego Gömez

Calvitero

Santa Barbara....

Miravete
Valdegamas
Pedro Gömez
Montanchez
Magacela

Santa Inés........
Bienvenida
Hamapega
Tentudia

Bujadillos

-Regatero

_ Gibalbin

Esparteros
Pinar

Aljibe

S. Fernando

 

LATITUDE. | LONGITUDE.

|
boa

Triangulation du Méridien de Salamanca.

43°39’ 16”| 11°49' 20”
43 13 43 | 11 43 29
43 16 36 | 12 10 21
25°0 18 12 145
43 152 | 12 28 53
42 41 -3:| 12 19 57.
42 38.20 |.11 54. 53
42 19 53 | 12 24
42 12 58 44 14
42,1 26 59
48 58 119
37 43 al
14 18 33
10 39 12

29

16 55

49 11
22 29

43 0

19 | 12 11 34
36 45 56 | 1215 1
36 30 37 |12 353
86 27. 55: 11128 3

36 15 10 | 11 42 5
36 6 54 | 12 7 40

 

 

Triangulation du
1862-70

43°29' 14”| 13°52' 10”

14 23 39
13 59 26
1433 3

45 23 19
43 8 45
48 2 23

 

ÉPOQUE.

1872

1872
1871
1871
1871
1868
1868
1868
1868
1868
1864
1864
1864
1863

1860

1860
1861
1861
1863

1863
1863
1863
1863
1862

1863
1863
1863
1864-65
1864
1864
1864-72

1864

1864-72

1863

1864
1863
1863
1863

1863
1863

1862
1861-71
1861

 

ET OBSERVATEURS.

Lieut. col. d'État-Major
Monet.

Col. d’E.-M. Carames.
id.
id.
id.
id.
Comm. d’E.-M. Sanchis.
id.
de
Cap. d’E.-M. Ahumada,
Comm. d’E.-M. Sanchis.
Comm. dart. Corcuera,
cap. du genie Barra-
quer.
id.
id.

id.
Comm. d’E.-M. Sanchis,
cap. d’E.-M. Ahumada.
id.

id.
Comm. du génie Ibarreta,
cap. dart. Solano.
id.
id.
id.
Comm. d’E.-M. Monet.
id.
id:
Comm. d’E.-M. Monet,
cap. d’art. Hernändez.
Comm. d’E.-M. Monet.
Comm. d'E.-M. Monet,
cap. d'art. Hernandez.
Cap. du génie Ruiz Mo-
reno (D. J.)
Comm. d’E.-M. Monet.
id.
ud:
Cap. du génie Ruiz Mo-
reno (D. J.)
Comm. d’E.-M. Monet.
id.

Meridien de Madrid.

Cap. d’E.-M. Quiroga,
comm. d’E.-M. Monet.
id.

id.

id.

 

INSTRUMENTS.

Pistor n° 2.

id.

Repsold A.

ld.

id.

id.

id.
Repsold n° 2.

Piston ne: 2.
Repsold n° 2.

id.

Brtel ne |:

de

‘id.

Mol.
Repsold ne 2.

id.
id.
id.
id.
Repsold (sans mar-
que).
id.

id.

: id.
Ertel n° 2.
id.

id.
Ertel n° 2, Pistor
me Be
Ertel n° 2.
Erte] n° 2, Pistor
nos
Ertel n° 2.

Brt.ne2, Pistorne:2.

Ertel n° 2.
Erb. n22, Bistorne2:
Ertel n°2.

 

 

REMARQUES.

Publiés dans les
« Memorias del In-
stituto Geografico y
Estadistico.» Vol. I.
Madrid, 1875.

 
