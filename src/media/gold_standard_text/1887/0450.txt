 

 

 

    

  

2

des distances symétriques comptées du milieu de la règle, la position intermédiaire coïnci-
dant à très peu près avec la position de flexion apparente nulle ou des points neutres.

L'ensemble des observations a montré que les déformations de la règle obéissent
d’une façon trés satisfaisante aux formules connues déduites des lois de lélasticité, et le
coefficient d’élasticité conclu de nos mesures est très voisin du coefficient admis dans
l'industrie.

La variation totale de longueur pour les 4 mêtres, entre les positions extrêmes des
rouleaux, à été de 168 microns. Dans le voisinage de la position normale des rouleaux, celle
qui correspond aux points neutres, un déplacement symétrique des rouleaux de 5 centimètres
seulement produit une variation de longueur apparente de 6 microns. Ce résultat démontre
combien il est essentiel que, pendant la mesure d’une base, la règle soit toujours supportée
sur les mêmes points, et, autant que possible, sur les points neutres.

9 La dilatation a été mesurée par 40 séries de comparaison entre notre règle,
portée successivement à différentes températures comprises entre + 55et +379 et la règle
géodésique internationale G,, maintenue à une température à très peu près constante de
146. |

Les séries d'observations ont été poursuivies par 3 observateurs, opérant successi-
vement par couple de deux, le premier aux microscopes, le second aux thermomètres. Il
résulte de l'examen des résultats qu’il n’y a pas d’équation personnelle appréciable entre les
observateurs; mais, si l’on ramène à zéro, à l’aide du coefficient de dilatation obtenu,
toutes les valeurs trouvées pour la longueur de la régle aux différentes températures, et
qu’on les compare a la longueur moyenne déduite de l’ensemble des mesures, les erreurs
résiduelles présentent parfois des écarts inusités et trop considérables pour être mises sur le
compte des erreurs d'observations; elles affectent une marche systématique suivant que la
température monte ou descend, et elles atteignent jusqu’à 7 microns. Ces anomalies nous
paraissent dues à une déformation permanente de la règle, sous l'influence de laccroisse-
ment ou du decroissement de température, et provenant de ce fait que la règle du Service
géographique est formée de deux parties ajustées et rivées ensemble, dont les retraits ou les
allongements, produits par les variations de température, sont probablement inégaux et
peuvent se contrarier : en d’autres termes, la règle semble ne pas obéir rigoureusement aux
lois de la dilatation comme le ferait un bloc unique et homogène.

3° L’étalonnage a été fait deux fois, avant et après la mesure de la dilatation. La
deuxième opération a donné pour longueur de la règle un résultat supérieur au premier de
3°93; cette différence né provient certainement pas d’une erreur d'observation, car les
nombres obtenus par les trois observateurs sont presque identiques et ne peuvent laisser
aucun doute à cet égard.

Du premier au deuxième étalonnage, la régle a done subi un allongement de plus
de 3 microns; c’est peu de chose sans doute, mais si l’on veut bien remarquer que toutes
ces’ opérations d'étalonnage ont été faites avec toutes les précautions possibles, que les
variations de température ont été produites lentement, on est en droit de se demander,
