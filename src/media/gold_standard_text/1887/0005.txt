   
  
   
  
  
    
  
  
  
    
  
 
 
 
 
   
 
  
    

   
 

VERHANDLUNGEN

[ 21. BIS ZUM 29. OCTOBER 1887 AUF DER STERNWARTE ZU NIZZA ABGEHALTENEN

7 ÉONFERENZ DER PERMANENTEN COMMISSION

DER

4p YERNATIONALEN ERDMESSUNG

Redigirt vom ständigen Secretär

A. HIRSCH.

Zugleich mit den Berichten mehrerer Special-Referenten über die Haupt-Fächer, und den
Berichten über die Fortschritte der Erdmessung in den einzelnen Ländern im letzten Jahre.

|
|
}

!
i
1
i

MIT ELF LITHOGRAPHISCHEN TAFELN

+ 0 0-2

CGOMPTES-RENDUS

DES SEANCES

DE LA COMMISSION PERMANENTE

L’ASSOCIATION GEODESIQUE INTERNATIONALE

RÉUNIE DU 21 AU 29 OCTOBRE 1887, A L'OBSERVATOIRE DE NICE
RÉDIGÉS PAR LE SECRÉTAIRE PERPÉTUEL

Suivis des Rapports spéciaux sur plusieurs branches principales, et des Rapports sur les
travaux géodésiques, accomplis dans les différents pays en 1887.

 

 

RER
AVEC ONZE PLANCHES . Pa as
SEAL
PAST NICE
I DIN In nn Wes
ER

 

1888 ‘=

VERLAG VON GEORG REIMER IN BERLIN

 

IMPRIME PAR ATTINGER FRERES, A NEUCHATEL
