 

 

Annexe No If.
RAPPORT
DRWIATIONS DE LA VERTICALE

Pan FO DHEEMERT

TABLE DES MATIERES
Introduction.

I. Recherches sur de vastes territoires pour lesquels la forme de l’ellipsoïde de compa-
raison a une certaine influence.

119

1. L’ellipsoïde terrestre général.

ro

2. Deviations de la verticale, suivant la latitude, dans la Grande-Bretagne et
l'Irlande.

4170

3. Déviations de la verticale, suivant la latitude, en France et en Belgique.
4. Déviations de la verticale, suivant la latitude, dans le Danemark, l'Allemagne,
la Suisse et l'Italie.

072)

we)

5. Quelques déviations de la verticale, dans le sens de la longitude, pour FEu-
rope centrale et occidentale.
6. Déviations de la verticale au Caucase et dans la Crimée.

§ 7. Déviations de la verticale dans les Etats-Unis de l'Amérique du Nord.

)

Il. Recherches sur lesquelles la forme de Vellipsoide de comparaison est sans influence.
I. La contrée de Moscou (Schweizer).

2. Deux petits territoires des Alpes (Pechmann).

3. Le 49% parallèle dans l'Amérique du Nord (Greene).

%. La contrée de Leipzig (Bruhns- Nagel).

5. Le Harz (Boeyer- Andree).

She

JO

LI) We >

(Avec 3 cartes. )

RAPPORT HELM. — 4

 

 
