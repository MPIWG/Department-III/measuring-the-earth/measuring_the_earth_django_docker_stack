14

  
   
   
  
   

 

Annex VII°.

 

 

 

BERICHT

 

 

des Professor Dr. Wilhelm Tinter iiber die im Jahre 1857 ausgeführten
Arbeiten.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Die Reduction der gemessenen Zenithdistanzen des Polarsternes und der Circum-
ineridianzenithdistanzen anderer Sterne zum Zwecke der E rmittelung der Polhöhe der Station
Krakau ist nunmehr definitiv zu Ende geführt worden. Es hat sich ergeben :

Polhöhe des Aufstellungspunktes des Universal-Instrumentes aus 562 Beobachtungen :

 

» = 503 52,342 + 0.067

 
  

 

Da die Reduction der Polhöhe vom Universal-Instrumente auf den Megichnnkgeis der
Sternwarte — 0-393 beträgt, so folgt aus diesen Beobachtungen :

Polhöhe des Meridiafikreises der Sternwarte in Krakau :

  
     
   

9 7,

¢= 003 51,949

i

    
 

Die Beobachtungen der Sterndurchgiinge im I. Vertical zur
mussten mit Rücksicht auf die Instrumentalfehler,
lichen Reduction unterzogen werden

Ermittlung der Polhöhe
Azımuth und Collimation einer neuer-
n, deren Gallons in nächster Zeit geschehen wird.
Die zweite Bean für die von mir auf den Stationen Jauerling und St. Peter bei 4
Klagenfurt ausgefiihrten Beobachtungen zum Zwecke der Bestimmung der Polhöhe und des” 4

Azimuths ist auch so weit vorgeschritten, dass ich deren Vollendung für das nächste Jahr in 4
sichere Aussicht stellen kann. \

 
     
 
      
     
       
 
  
