 

Ê
E

Te ee ee ia

Annex III’.

BERICHT

DIE LANGEN, BREITEN UND AZIMUTE

H.-G. van DE SANDE BAKHUYZEN

Auf meine Bitte hat das Centralbureau an meine verschiedenen Collegen cin Circular
versandt, um dieselben zu ersuchen, mir die nôthigen Mittheilungen über den Stand der Arbei-
ten über Längen-, Breiten- und Azimutbestimmungen in ihren Ländern zukommen zu lassen,
und es haben hierauf geantwortet : Oesterreich, Bayern, Dänemark, Spanien, Frankreich,
Italien, Preussen, Sachsen, Schweden und Würtemberg, während von Belgien, Hessen,
Norwegen, Portugal, Rumänien, Russland und der Schweiz keine Antwort eingegangen ist,
so dass für diese letzteren Staaten der Bericht vielleicht nicht so vollständig ausgefallen ist,
wie ich es gewünscht hätte. Seit dem letzten, in Rom vorgelegten Berichte hat sich die Zahl
der Längenbestimmungen um 28 vermehrt; ausserdem kann ich die Resultate von 13 Be-
stimmungen anführen, die theils als provisorische, theils als endgültige bezeichnet sind und
welche, obgleich die Operationen vor 1882 ausgeführt worden sind, zu jener Zeit noch nicht
berechnet oder publicirt waren.

Die Anzahl der neuen Breitenbestimmungen beträgt 68, wovon

7 auf Oesterreich,

6 » Bayern,
1» Spanien,

14 » Frankreich,
9.» Italien,

ol » Preussen,
1 » Sachsen,
1 » Schweden

entfallen.

 
