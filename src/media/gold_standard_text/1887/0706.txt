Noe

INDICATION

DES POINTS.

LATITUDE.

— 76 — .

Rheinisches Dreiecksnetz.

LONGITUDE.

EPOQUE. .

DIRECTEURS

ET OBSERVATEURS.

INSTRUMENTS.

REMARQUES,

 

 

 

 

 

Dreifaltigkeitsberg

Hehentwiel

.Feidberg i. 8.....

Sulzer Belchen....
Röthifluh
Wiesenberg
Lägern Hochwacht.
Nannheim
Königstutl
Durlacher Warte..
Strassburg

Kiihfeld. .

Feldberg im Tau-
MUS. este
Hasserod

Dünsberg
Taufstein

Punctd.Geodät.Inst.

| Milseburg

Fuselsberg
Punctd.Geodat. Inst.

Brocken
Punctd.Geodat. Inst.

 

 

48 8' 56”

47 45 56
AT 52 28

47 54 7

47 15 32
47 24 12
47 28 57
49 29 16
49 24 16
48 59 53
48 34 58

50° 41' 34”

50 14 1
50 56 21

50 39. 5
50 31 6
DL. 1.95
50 55 4
DLO 3
51 13 38

50 32 46
0.3 9

51 48 9

 

 

26° 25' 47”)

26 29 12
25 40 18

24 45 57
25 11 44
25 32 58
26 4 6
260°. 7288
26 23 47
26 :9 LE
25 25 5

25° 43' 22”

26-7 30
26 13 47

26 14 53
26 54 23
26 41 19
At) 24
21.341
27 31 26

27 33 58
28 8112

26 1108

 

1877

1877
1877

1877
1877
1877
1877
1873
1873
1873
1876

1869

1871
1876

1876

| 1874-75-76

1876

1875

1874
1873-75

1875
1869-71-74

1865-71-76

 

 

 

 

Werner, Lamp.

id.
Fischer, Westphal.

id.
id.
id.
id.
Bremiker, Fischer.
id.
de
Fischer, Weinterberg.

Hessisches Dreiecksnetz.

Bremiker, Fischer.

id.

Werner, Lamp.

id.
Sadebeck, Werner,Lamp.
Werner, Lamp.
Sadebeck, Lamp.
Sadebeck, Werner.
Sadebeck, Werner,Lamp.

Sadebeck, Lamp.
Albrecht,Sadebeck,Schur,
Werner.

Baeyer, Sadebeck, Schur,
Werner, Lamp.

 

 

10-zölliges Univ.-
Instr. von Pistor
und Martins Nek

ld.
10-zölliges Univ.-
Instr. von Pister

und Martins N2 |

10-zölliges Univ.-

Instr. von Pistor

und Martins N. 2.
id

-]0-zölliges Univ.-

Instr. von Pistor |

und Martins N. 1.
id.

id.
13-zölliges Univ.-
Instr. und 10-zöl-
liges Univ.-Instr.
N. 1 von Pistor und
Martins.

Anschluss der Sternwarte in Breslau und des Punctes Rosenthal
an das Schlesische Dreiecksnetz.

Tschelentnig
Breslau (Stern-
warte)

Wüstendorf

Hundsfeld
Rosenthal

 

50° 42! 13")

50 55 26
50 51 55
51 17 39
sl 694
51

51

"34 52 26

 

34° 46' 44"

34 54 29
34 22 35

 

1862

1862-65
1862-63

1862-65
1865

1865
1865

 

 

Löwe, Stavenhagen.

Löwe, Stavenhagen, Bae-
yer, Sadebeck.

Löwe, Stavenhagen, Ha-
belmann.

Baeyer, Galle,
mann, Sadebeck.

Sadebeck.

Habel-

8-zölliges Univ.-In-
str. von Pistor und
Martins.
id.

id.

Id. und 13-zölliges |

Univ.-Instr. von
Pistor und Martins.
8-zölliges Univ.-In-
str. von Pistor und
Martins.
id.
id.

| N.337, Prusse, B
| N. 8, Suisse,
| N. 1, Suisse.

 

 

 

 

 

N. 2, Württemberg,

W236 Suisse.
N. 22, Suisse.
N.3, Württemberg,

N. 2, Suisse,

N. 340, Prusse, B.
N. 187, France,

. 285, Prusse, B.

. 284, Prusse, B.

. 291, Prusse, B.

. 210, Prusse, Be

. 188, Prusso,B: |

| N. 184, Prusse, B. 4

. 181, Prusse, B.

 
