 

 

40

Basen, die unter sich durch die Dreiecksketien verbunden sind, ein weitaus geeigneteres
; . .
Mittel giebt, um diesen Fehler zu charakterisiren.

Herr Fervero antwortet, dass die Unsicherheit der Längen der Seiten nothwendiger-
weise mit der Entfernung derselben von der Basis wächst, und zwar auch für die vorzüglich-
sten Triangulationen. Es handelt sich vor allem darum, ein rationnelles Mittel zu finden, um
eine Angabe über die Genauigkeit der Winkel und Richtungen machen zu können. Aus diesem
Grunde haben sich nicht nur die Italiener, sondern auch Andr& in Kopenhagen, die Landes-
vermessung von Berlin, General Ibanez damit begnügt, den mittleren Fehler der Winkel und.
Richtungen anzugeben und nicht denjenigen der Längen. Es ist richtig, dass man auch die
Werthe der Correctionen, weiche sich aus der Ausgleichung ergeben, als Maass der Genauig-
keit der Netze angeben könnte, aber man würde so auf eine schwierigere Lösung stossen,
wenn man nicht, wie General Ferrero das seiner Zeit vorgeschlagen hat, den mittleren Werth
aller Gorrectionen nehmen will.

Herr Ibanez bestätigt, dass er bis jetzt in den Annalen seines Instituts nur den mitt-
leren Fehler einer nicht ausgeglichenen Richtung für jede Dreiecksgruppe gegeben hat.

Herr Ferrero wiederholt, dass er nicht die Absicht gehabt hat, eine genaue Lösung
dieses Problems vorzulegen, sondern dass er die Aufmerksamkeit der Commission auf die
Nützlichkeit eines geeigneten, durch den gesunden Menschenverstand empfohlenen Mittels
habe lenken wollen, das eine Abschätzung des Genauigkeitsgrades der verschiedenen Trian-
gulationen gestattet.

Herr Hirsch glaubt, dass die Verschiedenheit der Beurtheilung dieser Frage durch
die Herren Perrier und Ferrero mehr scheinbar als reell ist, da alle Netze immer auf
Winkel- und Längen-Gleichungen führen, deren Lösung in der Ausgleichungsrechnung die
Correctionen für beide und auch den Fehler dieser Correctionen liefert. Diese Elemente
werden stets die wirkliche Grundlage für die Beurtheilung der Genauigkeit der Netze ab-
geben. Vielleicht überschätzt Herr Ferrero die Schwierigkeit und die Länge dieser Arbeiten
etwas. In jedem Falle wird die Fortsetzung der Diskussion fruchtbarer sein, wenn man sich
auf die Vorschläge der mit dieser Frage betrauten Specialcommission wird beziehen können.

Der Vorsitzende ist mit dieser Ansicht einverstanden und glaubt, dass es angemessen
ist, dieser Commission noch die Herren Perrier und Forster beizugeben.

Dieser Vorschlag wird von der Permanenten Commission angenommen.

Der Vorsitzende bittet die Specialcommission, sich so bald wie möglich zu consti-
tuiren. Darauf ertheilt er das Wort dem Herrn van de Sande-Bakhuysen zur Berichterstat-
tung über die astronomischen Bestimmungen der Breiten, Längen und Azimute.

Herr Bakhuysen verliest seinen Bericht. (Siehe Annexe III‘ und III.)

Nachdem die Diskussion über diesen Gegenstand eröffnet ist, stellt Herr Fierster an
den Berichterstatter die Anfrage, ob die Commission hoffen kann, noch in dieser Conferenz
ebenfalls über den zweiten Theil des Berichtes, den er übernommen hat, Bericht zu erhalten,

 
