 

 

 

 

 

:
19

  

der Zeitschrift für Vermessungswesen, Band XIV (1885), veröffentlichten Aufsatze! ausführ-
lich dargelegt. Hier möge nur der eine, allerdings für sich allein schon zwingende wieder-
holt werden, dass die Gauss’sche Triangulirung zum Theil schon vor ihrer Beendigung, fünfzig
Jahre später aber so gut wie völlig vom Erdboden verschwunden war. Die Trigonometrische
Abtheilung hat es sich bei ihren Arbeiten auf diesem klassischen Boden angelegen sein lassen,
den Spuren der Gauss’schen Thätigkeit von Punkt zu Punkt zu folgen, und sie war hierzu
um so mehr im Stande, als sie sich im Besitz der amtlichen Berichte und sämmtlicher Be-
obachtungsprotokolle von Gauss befindet. Die Resultate ihrer Nachforschungen, soweit sie
die Wiederherstellung der Dreieckspunkte betreffen, sind folgende :

Von den 31 Punkten, auf denen Gauss selbst beobachtet hat (d. ı. allen, mit Aus-
nahme der Anschlusspunkte Inselsberg, Hamburg, Hohenhorn und Lauenburg), waren noch
drei in sichtharer Bezeichnung vorhanden, nämlich : Göttingen Sternwarte, Göttingen Meri-
dianzeichen und Zeven. Vier Punkte : Lüneburg, Bremen, Jever und Hohehagen sind durch
örtliche Messungen wiederhergestellt worden, und zwar die beiden ersten auf einige Millime-
ter, die beiden letzten auf einige Gentimeter genau?. Die übrigen 24 Punkte konnten theils
gar nicht, theils nur auf einige Decimeter genau wiederhergestellt werden; sie sind daher als
verloren anzusehen. Auf acht derselben wurden zwar Steinpfeiler vorgefunden, die aber weder
original noch zuverlässig sind. Auf allen Punkten, wo Gauss Winkel zwischen noch jetzt vor-
handenen umliegenden Thürmen oder sonstigen nahen Objekten gemessen hal, ist die Wie-
derherstellung durch Aufsuchung desjenigen Punktes versucht worden, wo die Gauss’schen
Winkel stattfinden; dieses leicht und bequem anzuwendende Verfahren hat aber, — mit
Ausnahme der bereits genannten Punkte Lüneburg, Bremen und Hohehagen, auf keinem
Punkte genauer als auf einige Dezimeter zum Ziele geführt, theils weil die Thürme, bezw.
Objekte, zu weit entfernt oder zu unsicher, theils weil die Winkel zwischen ihnen von Gauss
nur beiläufig und daher nicht genau genug gemessen waren.

Näheres wird demnächst mit fortschreitender Veröffentlichung des Werkes Haupt-
dreiecke mitgetheilt werden.

NIVELLEMENTS.

Im Sommer 1887 sind 900 Kilometer in Ostpreussen doppelt nivellirt worden. Es
sind fünf Schleifen zum Schluss gekommen, nämlich (vergl. Nivellements der Trigonometri-
schen Abtheilung der Landesaufnahme, Band VI, Tafel ID) :

‘ «Beiträge zur Kenntniss von Gauss’ praktisch-geodätischen Arbeiten. Nach Original-Materialien
bearbeitet von Gaede, Hauptmann A la suite des Generalstabes und Vermessungs-Dirigent bei der Trigono -
metrische Abtheilung der Landesaufnahme. »

2 Die Wiederherstellung von Lüneburg ist in der vor einigen Wochen veröffentlichten A. Abtheilung
des vierten Theils der Hauptdreiecke ausführlich beschrieben. Auf dieselbe Art ist Bremen rekonstruirt wor-
den. Bezüglich Hohehagen und Jever vergl. den in der Fussnote auf Seite 7 angeführten Bericht. Auch über
den Befund auf den Gauss’schen Stationen Litberg, Hilrede, Brocken, Falkenberg, Varel und Langwarden ist
an den beiden genannten Stellen bereits berichtet worden.

 

 
