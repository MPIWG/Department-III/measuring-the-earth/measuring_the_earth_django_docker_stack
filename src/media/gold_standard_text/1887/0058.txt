  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

02

emercie MM. Helmert et de Stefanis pour leurs communications. Il
4 MM. v. Kalmar et Hirsch pour donner connaissance de la rédaction, dans
concernant les nivellements le long

M. le President r

donne la parole
les deux langues, de la proposition de M. von Kalmär,

des cötes. er
Ces messieurs lisent la rédaction sur laquelle ils sont tombés d'accord.

M. le Président approuve cette rédaction et la soumet au vote de la Commission
permanente, qui l’accepte dans cette forme :

Rédaction convenue entre MM. v. Kalmar et Hirsch.

Afin d’obtenir la comparaison des niveaux moyens des différentes mers d'Europe
aussi indépendante que possible des variations de la pesanteur, M. v. Kalmär exprime le vœu
que, dans les pays maritimes, outre les jonctions déjà exécutées entre les mers qui les entou-
rent, on complete, là où la chose est possible, les lignes doubles qui existent déjà le long des
côtes, par des opérations polygonales, de telle manière que les différences de niveau entre les
maréographes et les échelles, placées le long de toutes les côtes, puissent être déduites aussi
sans passer sur des hauteurs considérables.

Toutefois il est bien entendu que, finalement, toutes les données de nivellement con-
tribueront, par voie de compensation, à la déduction des différences de niveau des mers.

Naturellement, il faut employer les plus grands soins aux passages des embouchures
des fleuves, de façon à rester, là aussi, dans la limite d’erreur de 3™™ par kilometre, fixée en
général pour les nivellements de précision.

Les résultats de pareils nivellements n’ont pas besoin de réductions sensibles a la
surface du niveau de la mer, et sont influencés aussi peu que possible par la variabilité des
mires; ils donneront directement les différences de niveau des mers et contribueront
essentiellement a la solution de cet important probleme.

(Signé) A. v. KALMAR.
(Signé) Dt Hirscu.

M. le President donne la parole a M. Zacharie, qui lit un rapport sur le nivellement
de précision au Danemark, en l’accompagnant d’un dessin en couleurs, qui représente la
construction des repères lesquels, au Danemark, sont placés souterrainement !.

M. le Président remercie M. Zachariæ, et ouvre la discussion sur son rapport :
> 0 ND A D lA à 1
M. Perrier trouve l'idée de ces repères souterrains excellente dans un petit pays,
mais la Juge impossible à réaliser dans les grands pays, à cause des frais trop considérables
| ay 11e 5 Ter =) > x Ros
nn M. Hirsch envisage l’idée comme d’autant plus précieuse, que les repères placés à
e 1 = a r a x A . 7 r r r 8 2 -
ur du sol sont exposés à être enlevés ou déplacés par suite du vandalisme des populations

1 Voir Annexe V bis,

 

 

   
 
 
  

    
