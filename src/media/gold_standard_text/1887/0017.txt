|
|

 

ee

 

14
mérite scientifique de Sir Wilson, mais aussi parce qu’elle renferme la preuve que l’ab-
sence de la Grande-Bretagne à la dernière Conférence générale de l'Association géodésique,
à Berlin, que nous avons tous regrettée profondément, n’a été que passagère, et que l’An-
eleterre, qui est entrée il y a quelques années dans cette organisation scientifique interna-
tionale, où elle est appelée à jouer un rôle important, est décidée d’y persévérer, en adhé-
rant à la Convention (octobre 1886) concernant l'Association géodésique internationale pour
la Mesure de la Terre, laquelle, proposée par le Gouvernement prussien, pour réorganiser
convenablement l’ancienne Mesure des degrés en Europe, après la mort de son fondateur, le
général Baeyer, a été acceptée par tous les États réunis autrefois pour cette œuvre scientifique.
« Veuillez agréer, Monsieur le Ministre, l’assurance de ma haute considération.
« Le Secrétaire perpétuel del’ Association géodésique internationale,
« Dr A. Hırscn. »
A Son Excellence Sir Francis Adams, Envoyé extraordinaire et Ministre plénipotentiaire de
Sa Majesté britannique près la Confédération suisse, à Berne.

Sur une démarche nouvelle, faite auprès du Gouvernement britannique par le Gou-
vernement de Prusse, qui a bien voulu tenir compte en cela d’un vœu émis par la Confé-
rence générale, le marquis de Salisbury a informé l'Ambassadeur d'Allemagne à Londres,
que le Gouvernement de Sa Majesté avait décidé de ne pas prendre part à l'Association géo-
désique internationale, ainsi que cela résulte de la note que j'ai reçue le 11 mai 1887 de
Sir Adams et que nous avons communiquée en traduction par circulaire à tous les membres
de la Commission. Il ne reste qu’à reproduire ici l'original de la note de Sir Adams :

« Berne, May 11, 1887.

« Monsieur le Professeur
7

« With reference to my letters to you of August 2, 1884, and December 27, 1886, I
have the honnor to state that the Marquis of Salisbury has informed the German Ambassa-
dor in London that Her Majesty’s Government have decided not to be one of the parties to
the proposed International Geodetic Association, and that the nomination of British Dele-
gates to it, which I had announced in my above-mentioned letters, have accordingly to be
withdrawn.

«In accordance with my instructions from Lord Salisbury, I beg to communicate this
information to you.

€} may add that the reason for this withdrawal is stated to the that be circumstances of
the case do not appear to be such as to bring the functions of the association within the range
of subjects to which Her Majesty’s Government can make a contribution from public funds.

«I have the honnor to be, Monsieur le Professeur, your most obedient humble servant,

«F. O. ADAMS. »

 
