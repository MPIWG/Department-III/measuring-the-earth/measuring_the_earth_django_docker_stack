   

 

|
f
|

 
   
 

SS
Or

§ 6. Lothabweichungen im Kaukasus, sowie in der Krym.

Der gefalligen Mittheilung des Herrn General Stebnitzki ist das Material zu der Ta-
belle V sowie die Karte zu verdanken, wodurch eine Uebersicht der wichtigen Untersuchungen
dieses unermüdlichen Geodäten im Kaukasus gegeben wird. 4

Für den ersten Theil der Tabelle sind die Meereshöhen direkt und die geographi-
schen Längen durch Interpolation aus der Karte von mir entnommen, für den zweiten Theil
ebenso die Breite von Rostow. Im übrigen folge ich dem Bericht über die betreffenden Ar-
beiten in den Mélanges mathématiques el astronomiques tirés du Bulletin de l’Académie im-
periale des sciences de Saint-Petersbourg, t. IV, livre 5, 1870. Gegenüber dieser Mittheilung
haben sich neuerdings die Zahlen für Nr. 12, Jelisawetopol, und Nr. 13, Schemacha, ver-
ändert. Den Abweichungen in Länge liegen Bestimmungen geographischer Längendifferenzen
zu Grunde, die mittelst des elektrischen Telegraphen seit 1882 erhalten worden sind.

Die geodätischen Berechnungen wurden unter Anwendung der Dimensionen des
Erdellipsoids von Walbeck (1819) ausgeführt. Der letztere findet in seiner Schrift !

 

302,146. undog= 52009, 76.8,
g die mittlere Gradlänge des Meridian-Quadranten. Hieraus folgt
a = 6 376 895 m.

Bei den geodätischen Breiten ist von dem westlichen Ende der Basis bei Jekaterinograd aus-
gegangen, wobei als Breite der um die aus der Attraktionsberechnung folgende Lothab-
weichung verbesserte Beobachtungswerth eingeführt wurde. Bei den Längen ist Rostow Aus-
gangspunkt. Was Jekaterinograd anbetriffi, so folgt nach der erwähnten Abhandlung von
1870 aus dem Vergleich mit südrussischen a im Mittel als Lothabw eichung 13/84,
welche Zabl bei Anwendung der Bessel’schen Erddimensionen auf 13” herabgeht. Diese Zah-
len stimmen annährend mit dem Ergebniss 12°62 der Attraktionsberechnung überein.

Bei der Berechnung der Lothabweichung aus den Gebirgsmassen wurde die Dichtig-
keit derselben gleich der halben mittleren Erddichte gesetzt. Der Radius des berücksichtig-
ten Umkreises beträgt u. a. bei den Breitenabweichungen 85 Kilometer für Jekaterinodar
(Minimum), 139 für Schemacha, 200 für Jekalerinograd, 261 für das russische Signal
(Maximum); er wurde überall sorgfältig mit Rücksicht auf die Lage der entfernteren Massen
ausgewählt. Die Unterschiede « Beobachtete — berechnete » Lothabweichung sind nördlich
vom Gebirge gering, während sie südlich, d. h. für die drei Stationen Tiflis, Jelisawetopol
und Schemacha, recht beträchtlich sind, de innen) mit dem vulkanischen Charakter

 

1H. J. Walbeck, De forma et magnitudine telluris, etc., Aboae, 1819, p. 16.

  

   
   
