dra
pi
I
ienl
gil

US
\Va-

von

nl,
ple-
le,

EEE TEE TEE TE NETTER ET TRETEN PR

 

ae EEE

TEE Teer

 

ol

 

rapport spécial sur les déterminations de la pesanteur, dont M. Helmert à bien voulu se
charger en place du regretté von Oppolzer.

M. Helmert donne lecture de ce rapport, accompagné de plusieurs tableaux statisti-
ques ; il regrette de n'avoir pas pu Paccompagner également d'une carte synoptique, dont
il espère cependant doter son rapport avant le moment où il sera livré à l'impression !.

M. le Président remercie M. Helmert de son rapport, et prie M. de Stefanis de lire
la note que M. Lorenzoni, qui malheureusement à déjà dû partir, a écrite sur ses travaux de
pendule.

M. de Stefanis donne lecture de cette notice, el la remet au Secrétaire. En voici la
teneur :

«Dans le mois d'août 1885 j'ai trouvé la durée d’une oscillation du pendule-mêtre
à réversion de Repsold — 10034395 avec une distance des couteaux = 10003798 à la
température moyenne de 23.47.

«En février 1886, mes expériences m’ont donné: durée de l’oscillation= 10032977 avec
la distance des couteaux == 1000"411°0 a la température moyenne de 8 90.

« Les deux valeurs pour la longueur du pendule à secondes, qui découlent de ces élé-
ments, coincident entre elles jusqu’au dixiéme de micron et avec le nombre

h = 993" SATA

«Quant 4 la correction pour les trépidations du support, elle doit étre, @aprés mes
expériences et pour l'appareil de Repsold dont j'ai fait usage, inférieure a un mucron.

«En 1824, M. Biot avait fait ses expériences à l’observatoire de Padoue, dans une salle
qui est placée à 12 mètres plus en haut que la chambre où j'ai fait les miennes. Le résultat
de Biot, réduit au niveau du pendule de Repsold, est

1 = 99360174

qui excède le mien de 54°. La presque coïncidence de cet excès avec la différence de 56” en
plus, trouvée par Biot entre la longueur du pendule observée à Padoue et celle calculée
d’après les observations faites dans d’autres stations, fait soupçonner, contre Pavis de Biot,
que cetle derniére différence n’exprime pas leffet de causes perturbatrices physiques rési-
dantes dans la région où Padoue se trouve placée.

Nice, 24 octobre 1887.
G. LORENZONI. »

1 Voir les Annexes Ilt et II".

 
