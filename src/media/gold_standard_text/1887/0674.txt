INDICATION

DES POINTS.

LATITUDE.

LONGITUDE.

de

Grande Bretagne.

EPOQUE.

DIRECTEURS

ET OBSERVATEURS.

INSTRUMENTS. REMARQUES

 

 

 

Gwaunysgaer
Hampton Poor-

Hanger Hill Tr. ..

Hanslope ‘Spire....
Happisburgh Ch.Tr.
Hart Fell

Hensbarrow

High Pert Cliff...
| High Wilhays
Hingham Ch. Tr. ..
Holme Moss
Horton’s Gazebo...
Howth Hill

Hungry Hill
Ingleborough
Inkpen Beacon....

Jura, North Pap..

Karnbenellis
Karn Galver......

Karnminnis

Keeper
Kellie Law

Keysoe Spire
King’s Arbour....

Knockalongy
Knockanaffrin
Knocklayd
ÆKnockmealdown .
Knocknadober
Knocknagante
Knocknaskeagh....
Lawshall Ch. Tr...
Lasfield Ch. Tr....
Layton Hill

Leith Hill Tr. ....| ?

Lincoln Minster...

Little Sterling....
Littledown Down...
Llanelian

_Basede Lough Foyle
' Terme Nord
Base de Lough Foyle

Terme Sud
Lumsden..... N

 

Sad) 19

51 25
DL OL

26
52 49
55,24

50 23

50 35
50 41
52 34
53 32
50 51
53 22

51 41
54 9
D1 21

55 54

50 10
b0,,.9

50 11
52.45

56 14
15
28
3 10
35
1
{7

9

13
59
53

6

9

18
16
10

14

>»
23

46
30
29

1

45

N
45
9
36
24

13
59
a

8
57
55
41

5
53

0
47

 

14° 16)

17
17

16
19
14

13

16
13
18
12
14
II

7
15
16

an

12
12
12

9
14

17
17
11
14

8
10

17 54

21 54 |

50 11
11 41
15 46

50 42

28.15
og Li
38 47
46 51
42 21
35 41

52
15
11

18
59
56
39 37
26

1
Au

1805

1787-92
1792-94

1822-48
1843
1816-47

1796-1845

1846
1845
1843
1841

1829-44

1832
1807
1797-1844

1822-47

1796
1850
1796-1845

1830-31
1813-47

1843
1792
1829
1814
1828
1829
1827

1840
1840
1831
1843-44
1845
1817
1792-94
1822-44

1842
1814-50
1846
1805-06
1843

1828

 

 

1829
1809-46

 

 

Woolcot.

Générs,Roy,and W.Mudge.
Génér. W. Mudge, capit.
Kater, serg. Donelan.

Serg. Donelan.
Génér.Colby,Gardner,serg.
Bay.

Génér. Mudge, capor. Ste- |

wart.
Serg. Steel.
Serg. Donelan.
Serg. Steel.
id.

Génér.. Portlock, Gordon. |

Génér. Portlock.

Gener. Colby.

Génér. Mudge, caporals
Stewart, Cosgrove.

Capits. Vetch,
serg. Donelan.

Génér. Mudge.

Caporal Wotherspoon.

Génér. Mudge, serg. Do-

nelan.
Génér. Portlock.
Génér. Colby,
serg. Winzer.
Serg. Steel.
Gener. Mudge.

| Génér. Portlock.

Génér. Colby, Gardner.
Lieut. Murphy.

| Gener. Portlock.

id.

Serg. Donelan.
Beale.

Gener. Portlock.
Serg. Donelan.

\Sers. Bay.

Gardner.

Générs. Mudge, Colby, ca-|
pit. Kater, Gardner, serg.

Donelan.
Lieut. Da Costa.

Gardner, serg. Steel.

Serg. Steel.

Génér. Colby.

Lieut. Luyken, capor. Ste-
wart.

Capit. Hendersob, lieuts.
Murphy, Mould.

Major Pringle, lieuts. Mur-
phy, Henderson, Mould.
Mr. J. Gardner, serg. Win-
zer.

| Ramsden de 18 p.
| Ramsden de:3 p.

Dawson,

Gardner,

| Ramsden de

| Ramsden de
| Ramsden de 3 p.

| Ramsden de 18 p.
| Ramsden de 3 p.

 

Ramsden de 3 p.

id.
id.

id. |
Ramsden de 3 p. et |
Troughton de 2 p. |
Ramsden de 3 p. |

| Nahe bei 88,

Ramsden de 18 p.
id.

Ramsden de 3 p.
Theod. de 7 p.
Ramsden de 3 p.
id.
id.

id.

id.
Ramsden de
Ramsden de

id.
id.

18 p.
Ramsden de 3 p.
id.
id.
le p:

id.

Theod. de 12 p.

Id.
Ramsden de 3 p.

id.
| Troughton de 2 p.

Ramsden de 3 p.
id.

| Troughton de 2 p. |

Ramsden de 3 p.
Théod. de 7 p.

| Nahe bei 88.

id.
Troughton de 2 p.
id.
Ramsden de 3 p.

 
