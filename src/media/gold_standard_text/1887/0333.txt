 

I

Mt

Ir

2
i
=

   

 

ose OS

jp it cat

se
he

se

és |

 

AZIMUTS.

 

Angle entre la Polaire et le
signal.

A, Angle entre la Polaire
et le signal, déterminé au
moyen d'un instrument
universel de Starke.

B. Angle entre le signal et
la mire méridienne dont
l’'azimut fut déterminé au
moyen d'un instrument de
passage de Pistor et Mar-
tins.

»

4 mele cuire la Polaire
et le signal, déterminé au
moyen d’un instrument
universel.

B. Angle entre le signal et
la mire méridienne, dont
Vazimut fut déterminé au
moyen d'un instrument
universel.

Année et mois.

1883 juin.

1883 juin et
juillet.
1884 mai.
1884 juin.
1886 mai.
1886 juin.
1886 juillet.

188% aout.

1884 août.
1885 juin.

1885 juin.

 

TITRE DE LA PUBLICATION.

Pas encore publié.

 

REMARQUE.

Les Azimuts dans la Bavière
sont tous comptés du Nord
en passant par l'Est.

 

 

 
