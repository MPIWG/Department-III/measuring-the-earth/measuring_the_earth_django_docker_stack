  

 

 

 

Herr Perrier fügt bei, dass die paar Zeilen, welche sich in seinem Berichte auf die
Pendelarbeiten beziehen, eine Vervollständigung in der Mittheilung des Herrn Defforges er-
halten werden.

Herr Perrier kann nicht umhin, im Namen des « Service géographique francais »
dem Internationalen Meterbureau fiir die zuvorkommende Freundlichkeit zu danken, mit der
es den Offizieren des « Service géographique » gestattet hat, in dem internationalen Bureau
in Breteuil den Bestimmungen beizuwohnen, welche mit den französischen Basisapparaten
ausgeführt worden sind. Herr Perrier glaubt, dass die Offiziere und Gelehrten, welche in den
Fall kommen, mit einem Basisapparat zu thun zu haben, nirgendwo eine bessere Lehre
durchmachen können, als in Breteuil.

Herr Hirsch legt Gewicht darauf, zu betonen, dass die Vollkommenheit, welche man
in diesem Institute für derartige Operationen erreicht hat, besonders dem grossen Ver-
dienste des französischen Gelehrten Herrn Dr Benoit, dem ersten Adjunkten des Bureaus, zu
danken ist.

Der Vorsitzende giebt die Versicherung, dass jedem Begehren, der Bestimmung
eines nach Breteuil gesandten Basisapparates beiwohnen zu dürfen, entsprochen werden
wird.

Da die Zeit schon vorgeschritten ist, müssen die übrigen Berichte auf die nächste
Sitzung verschoben werden, welche morgen, Freitag um 1'/, Uhr, auf der Sternwarte eröffnet
werden wird.

Schluss der Sitzung um 14 !/, Uhr.

 

     
