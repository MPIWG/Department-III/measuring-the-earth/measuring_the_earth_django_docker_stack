  

   
 
 
   

 

 

 

 

 

 

 

 

 

 

 

 

 

20

  

 

peut-être çà et là dépassent à peine 1 à 2 dixièmes de seconde, au moins pour les points
situés entre le Danemark et la Haute-ltalie; dans cette région, les triangulations aussi sont
suffisamment bien établies et on peut en dire autant des données astronomiques pour les la-
titudes géographiques, quoique les déterminations employées en partie en Bavière, et qui
datent du commencement de ce siècle, soient moins exactes que les déterminations plus ré-
centes de la latitude, et soient affectées d’une erreur moyenne de + 1” en chiffres ronds.
J'ignore également quel degré de certitude on peut accorder aux indications pour Vienne el
Fiume, Nes 32 et 100, de méme que pour les Nes 80 et 92; mais je crois toutefois qu'elle
est suffisante.

Toute proportion gardée, c’est à la majeure partie des indications comprises entre
les Nos 101 à 417 qu’on doit accorder le moins de confiance, aussi bien à cause de la partie
géodésique que de la partie astronomique à laquelle ces indications doivent leur origine. Il
se peul qu'une détermination plus rigoureuse donne ici des différences jusqu’à 2", — cette
quantité pourra être difficilement dépassée. Pour ce qui concerne en particulier les déter-
minations astronomiques plus anciennes, en supposant une erreur moyenne de + 1", l’exac-
itude serait plutôt évaluée trop basse que trop haute, comme cela résulte de plusieurs con-
trôles. Pour Florence, N° 113, il existe une nouvelle détermination de la latitude géogra-
phique!, mais qui la donne seulement d'environ !/," plus petite, et où une attraction locale
peut aussi avoir de l'influence à cause du transfert géodésique qui y figure. Pour Rome,
N°115, le Astronomische Ja'rbuch de Berlin indique une latitude de 16 plus grande; mais
je ne me suis pas rendu compte de Videntité des points dans cette détermination.

Pour le Ne 116, Naples, je croyais pouvoir admettre que les nouvelles données pour
la latitude se rapportaient & ce point, dont Marieni fixe la latitude à 40° 91° 47-0 et pour
celte raison J'ai introduit la valeur de latitude la plus récente, qui est inférieure de 15

En général, je fais encore ici la remarque que j'ai rapporté partout la déviation de
la verticale le plus exactement possible à l’endroit réel de l'observation, c’est-à-dire que j’ai
indiqué, autant que faire se peut, dans le tableau IF la latitude géographique observée et
sans correction pour centrage. Les longitudes géographiques et les altitudes au-dessus de la
mer sont empruntées en grande partie aux ouvrages relatifs sur les triangulations. À ma de-
mande, les altitudes pour le Danemark et la Bavière m'ont été obligeamment communiquées
par MM. Zachariæ et von Orff. J'ai partout arrondi les chiffres, attendu que pour embrasser
d'un coup d'œil rapide l’ensemble des données, une approximation de 10 m. est suffisante et
que, dans la plupart des cas, le mêtre ne pouvait être constaté sans difficulté.

Un examen des valeurs de déviations de la verticale, contenues dans le tableau IH,
el dérivées par rapport à Rauenberg comme zéro (c’est-à-dire des trois premières colonnes
de ces valeurs) montre une prédominance du signe négatif. Si l’on range en particulier les
valeurs de la 3° colonne, qui se rapportent à lellipsoïde de Clarke, de 1880, d'après la lati-
tude, il s’ensuit que :

/ Processo verbale delle sedute della Commissione geodetica italiana, tenute in Milano nei giorni
27 © 28 Settembre 1886. Firenze, 1887, p. 36.

 

 

 
