 

|

f
|
|
|
|
i

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

10

 

Pour la comparaison des résultats, on utilise encore, outre ce qui précède, la forma-
tion des sommes de carrés des différences (Observation-Calcul). Si, dans ce calcul, on lient
compte des centièmes de seconde de ces différences, il en résulte les valeurs ci-après des
sommes de carrés pour chacun des ares. (Clarke indique dans sa Geodesy, p. 318, le calcul
plus précis 285,763) :

Systeme des éléments

de Clarke. Quatrième.

Are franco-anglais,"em latitude . . .. © 68,1 70,4
Are russe » ee 5755
Arc indien » or TA) 184,1
Arc sud-africain » de 0,6 1,1
Arc sud-américain » ee 0,27 0,1
Aves de lonaïtude en Indes. ge . .- 609 56,6

284,7 379,4

À l’appui du systeme des éléments de Clarke, on peut donc avancer le fait que les
trois grandes mensurations de degrés de latitude montrent toutes ensemble, quand on le
prend pour base, de plus petites sommes de carrés qu’en employant le quatrième sys-
teme, ce qui, par rapport à l'arc indien, est tout à fait conforme à la nature, mais ne Vest
en aucune manière par rapport à l'arc franco-anglais et à l’are russe, parce qu'ici le qua-
trième système des éléments, abstraction faite de l’aplatissement, a été choisi de telle façon
que la somme des carrés des écarts est un minimum. Il existe encore alnsi en apparence
entre les résultats des mesures de la pesanteur et ceux de la mesure des degrés, pour l’apla-
tissement de l’ellipsoïde terrestre, une différence sensible non résolue, mais qui trouve aisé-
ment son explication dans des influences systématiques qui existent, comme il a été dit plus
haut, non seulement dans le calcul de l’aplatissement, résultant des mesures de la pesan-
teur, mais aussi dans la déduction des éléments de l’ellipsoïde terrestre des résultats des me-
sures de degrés. Les différences (Observ.-Calcul) signalées dans les tableaux ci-dessus ont
en effet çà et là un caractère systématique frappant.

Mais au point où en sont actuellement les choses, on sera obligé pour le moment
de donner la préférence sur tous les autres aux éléments de Clarke. C’est pour cette rai-
son que J'ai réduit, autant que possible, à ce système d'éléments les déviations de la verti-
cale qui sont cornmuniquées dans les résumés suivants.

®

 

 
