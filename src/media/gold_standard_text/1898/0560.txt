 

a a

506
ooservations nécessaires pour obtenir la jonction du nivellement en Seeland a celui de Vile
de Falster, dont la pointe méridionale se termine au port de Gedser sur la Baltique.

Les observations pour le passage du Grand Belt, entre les îles de Fionie et de
Seeland, ont été terminées en 1897 et les résultats de cette opération délicate ont été publiés
dans le bulletin de l'Académie des sciences de Copenhague (1898 n° 4, p. 163-207) dont
voici le résumé : >

L'article Ie" donne quelques remarques préliminaires sur le problème à résoudre et
montre que, pour neutraliser l'effet perturbateur de la réfraction, il faut des milliers d’ob-
servations disposées par groupes et réparties systématiquement sur telles et telles heures,
tant du matin que du soir. La petite île de Sprogô, située au milieu même du Beet, permet
de restreindre la distance à 8 kilomètres environ et d'employer deux méthodes différentes,
celle des observations réciproques et celle des observations d’une seule station qui tient le

milieu exact entre les deux repères. Dans l’article Il, on explique le procédé de M. le lieute-

nant-colonel Rasmussen pour la méthode réciproque. Ce procédé a ceci de particulier que,
pour obtenir un nivellement trigonométrique, on peut ge contenter d’instruments ordinaires
de nivellement géométrique, pourvu qu’ils aient des niveaux sensibles et des micrométres
oculaires.

Liarticle III contient les tableaux des groupes d’observations de la méthode réci-
proque dont on s’est servi pour réunir l’ile de Fionie a celle de Sprogé et cette île à celle de
Seeland, ainsi que les tableaux du nivellement géométrique entre les stations intermédiaires
de Sprogô.

Les articles IV et V sont consacrés aux erreurs constantes des deux instruments et
à l'influence de la courbure de la Terre et de la réfraction moyenne. La méthode réciproque

4

ayant été employée sur trois lignes, l’une à l’ouest, entre Fionie et Sprogü. les deux autres
€ 2 D D

à l'est, entre Sprogô et Seeland, les résultats obtenus présentent un moyen assez sûr de
constater l'invariabilité des instruments et en même temps l'admissibliité des hypothèses qui
servent de base à cette méthode. En désignant par (1) et (2) les valeurs kilométriques des
erreurs constantes des deux instruments, on déduit, des observations faites sur les trois
lignes, les valeurs suivantes :

(D) — @) = 67,2 + 0,9 millimètres.
(1) — (2) = 67,9 + 1,5 »
(1) — @) = 67,4 + 1,4 >
Moyenne — 67,5 + 0,7 »

L’accord parfait des trois résultats présente, ce me semble, un contrôle de quelque
valeur.

Dans Particle VI, on résout l’erreur de la réfraction en deux composantes, dont l’une,
f, est commune aux deux observations réciproques, tandis que l’autre, s, est particulière à

 

 
