 

 

. 188
Reval. Le résultat de cette opération est parfaitement d’accord avec la déduction théorique de
M. Rylke, car elle arrive 4 une correction de + 054, et elle donne en méme temps pour la
différence de niveau entre Kronstadt et Reval la valeur de + 012, au lieu de + 0755 qu'on
admettait autrefois.

M. de Stubendorff croit qu’on ne saurait nier la réalité de cette différence de + 0° 12,
attendu qu’il est naturel que la partie orientale du golfe de Finlande doit se trouver à un
niveau un peu supérieur à celui de la partie occidentale. L’explication devrait en être cherchée
essentiellement dans la prédominance des vents Sud-Ouest qui poussent les eaux dans le
golfe de Finlande.

M. Rosén dépose en outre sur le Bureau un exemplaire de son mémoire, commu-
niqué le 9 février 1898 à l’Académie suédoise, sous le titre : « Détermination de l'intensité
de la pesanteur dans les stations de Haparanda, Hernôsand, Upsala, Stockholm et Lund. »

M. Tinter obtient la parole pour une communication sur les travaux géodésiques en
Autriche. 11 donne d’abord lecture du rapport du Contre-Amiral M. A. von Kalmar sur les
mesures de pesanteur exécutées par des officiers de la marine austro-hongroise, dans les

années 1895-1898. (Voir Annexe B. X2.)

M. Tinter promet en outre d'envoyer à temps au Secrétaire les rapports de M. le
prof. Weiss et de l’Institut géographique-militaire.

M. Raoul Gautier présente le rapport sur les travaux géodésiques exécutés en Suisse,
de 1896-1898. (Voir Annexe B. XI.)

Le Président donne la parole à M. le Général de Stubendorff pour communiquer le
rapport sur les travaux exécutés en Russie dans les années 1896 et 1897. (Voir Annexe B. XII.)

M. de Stubendorff fait ensuite lecture des notices nécrologiques suivantes sur ses deux
prédécesseurs, E. Forsch et J. Stebnitzky :

Le 21 juin 1896 est mort le général d'infanterie Æ. Forsch, ancien chef de la section
topographique de lPÉtat-Major général et membre de la Commission permanente de PAsso-
ciation géodésique internationale.

Le défunt à commencé sa carrière géodésique en 1858, quand il fut attaché au
Service géographique de l’armée. En 1860 il à été nommé chef des travaux astronomiques et
géodésiques en Finlande. En 1863, quand la mesure internationale de l’arc du parallele de
52° de latitude fut décidée définitivement, c’est lui qui a été chargé de la direction de la
partie russe de la mesure, et peu de temps après, avec le consentement des gouverne-
ments étrangers qui prenaient part à cette opération, l'exécution des travaux astronomiques
sur toute l’étendue de l’arc lui a été confiée, en qualité de premier observateur. C’est ainsi

1 Le dernier de. ces rapports a été en effet envoyé par M. de Sterneck le 17 décembre dernier.

(Voir Annexe B. Xb.)
Le Secrétaire

 

 

À Meillth ir 1 di Li

a a HART!

Lal ml

ol!
