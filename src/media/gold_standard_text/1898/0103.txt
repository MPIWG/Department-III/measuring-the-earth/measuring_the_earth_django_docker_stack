 

 

|
I
(

Lui qi

en

I TTF

u

On s’occupe actuellement de la discussion des résultats obtenus par les voies photo-
graphique et optique. Mais on peut déja déclarer maintenant que l’erreur moyenne d’un couple
d’étoiles est la même pour les deux méthodes, savoir -- 0.16; toutefois il ne faut pas oublier
que les observations optiques ont été faites avec une lunette zénithale moins grande (68mm
d’ouverture) et moins stable, tandis que les résultats photographiques ont été obtenus avec
un télescope zénithal très stable et possédant une ouverture de 135™™.

La marche moyenne de la hauteur du pôle est la même pour les deux séries, à
quelques centièmes de seconde près, et pour toutes deux on a reconnu l’absence presque com
pléte d’écarts systématiques diurnes.

Enfin, les courbes de Potsdam s’accordent dans leurs formes, 4 0.02 prés, avec la
marche de la hauteur du pôle telle qu’elle résulte de la discussion générale de toutes les
données d’observation obtenues ailleurs (voir « Rapport sur l’état de l'étude des variations de
la hauteur polaire, en décembre 1897»), de sorte qu’abstraction faites d/influences cons-
tantes éventuelles, on peut conclure qu’il n’existe pas de perturbations variables de réfrac-
tion pendant cette période trimestrielle d’observations.

ALBRECHT.

Comme on le voit par le rapport spécial qui précéde, MM. Schnauder et Dt Hecker
ont continué également en 1897 les observations optiques à la lunette zénithale de l’Institut
géodésique, de sorte qu’on possède actuellement une série continue de quatre ans dont les
résultats sont dans ce moment soumis à une discussion complète. |

a

Quant aux observations de latitude à entreprendre dans quatre stations internatio-
nales situées sur le même parallèle, il m’a paru qu'avant tout il faudrait s'assurer s’il est
possible de trouver quatre stations semblables qui réunissent toutes les conditions voulues.
Dans ce cas seulement il serait possible de se décider définitivement pour ce projet. Parmi
les différents groupes de stations que M. Albrecht avait examinés, j'ai donné la préférence
au groupe Dover-Ukiah-Mizusawa-Cagliart, situé sous la latitude de 39°’.

Pour l'Amérique du Nord, M. Titimann m'avait déjà fourni le 31 mars de cette
année quelques informations préalables qui m'ont permis d'adresser le 5 mai un exposé
détaillé de la question à M. le général Duffield, Superintendent du Coast and Geodetic Survey
U. S. A., en le priant de bien vouloir ordonner des reconnaissances appropriées pour les
stations de Dover et d’Ukiah, et éventuellement pour d’autres points du voisinage.

De même, sur le conseil de M. le D' Omori, de Tokio, je me suis adressé le 3 juin à
M. le Ministre de l'Instruction publique du Japon, pour solliciter des renseignements sur
Mizusawa. Enfin, le 18 septembre, j’ai fait parvenir une semblable demande pour Caghari
à M. Celoria, à Milan, vice-président de la Commission géodésique italienne.

M. le général Duffield a fourni le 11 août et le 21 octobre des renseignements sur
les stations de Dover (Delaware) et d’Ukiah (Californie) ; pour le premier point il a proposé
éventuellement encore Round-Hell ou Leedsburg (Virginie), qui seraient plus faciles à attemdre

 
