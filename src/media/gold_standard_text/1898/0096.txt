|
|
‘i
|

 

88

Enfin, le Secrétaire se croit obligé d'ajouter qu’en agissant ainsi on se conforme aux
usages, trés justifiés du reste, de l'Association, mais on n’obéit pas en cela a une prescrip-
tion légale de la Convention qui ne prévoit nulle part ce caractère bilingue de nos publica-
tions. Du reste, les rapports spéciaux et nationaux annexés à nos Comptes-Rendus ne sont
publiés que dans une langue et, d’autre part, la correspondance officielle avec les gouverne-
ments par Vintermédiaire de leurs représentants diplomatiques n’a lieu gu’en frangais.

M. le Président invite le Secrétaire à lire la traduction en français du Rapport du
Bureau central pendant l’année 1897. Le rapport original, en allemand, que M. le professeur
Helmert a fait imprimer à Berlin, figurera aux annexes. (Voir Annexe A. I.)

Rapport sur l’activité du Bureau central de l'Association géodésique internationale
en 1897.

suivi du programme des travaux pour l'exercice de 1898.

D’après l’art. 3 de la « Nouvelle Convention de l'Association géodésique internationale »
d'octobre 1895, entrée en vigueur avec Île commencement de l’année 1897, le Directeur du
Bureau central présente chaque année au Bureau de l'Association un rapport sur l'activité
du Bureau central et lui soumet le programme pour les travaux pendant l’année suivante. Ce
rapport et le programme seront imprimés et envoyés à tous les délégués.

En conformité de ce qui précède, j'ai ’honneur de présenter le rapport suivant :
A. Activité scientifique.

Le programme de cette activité était tracé par les délibérations et les décisions des
deruières Conférences de Berlin et en particulier de Lausanne. Elle s’est en conséquence
exercée dans les domaines suivants :

4o Déviations systématiques de la verticale ;

90 Mouvement de l'axe terrestre à l’intérieur du globe, déduit des données fournies
par la coopération libre des observatoires ;

30 Travaux préparatoires pour le Service international des latitudes ;

4° Déterminations absolues de la pesanteur au moyen du pendule ;

50 Recherches sur les mires en bois.

4. J'ai chargé les chefs de section, MM. les professeurs Borsch et Kruger, des calculs
des déviations systématiques de la verticale ; on leur a adjoint, pour l'exécution des calculs
numériques, M. le D' Schendel, mathématicien, qui a pu être rémunéré au moyen du crédit
voté dans une des séances de la Conférence de Lausanne sur la dotation de 1887-1896.

Ces deux Messieurs ont rédigé sur leur travail le rapport spécial suivant, que je me

 

Utd 1

PP CO LINE a) Mark

ak tn

vl!

D. uses same |

 
