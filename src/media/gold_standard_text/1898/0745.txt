a RARE TE SSS

dpt

iy

ui

All

MN Om AL

NE DER

 

 

 

 

Do

II. — Bavière et Palatinat.

 

 

 

 

N° INDICATION | : DIRECTEURS : Mee
Do a LATITUDE |LONGITUDE] EPOQUE ns INSTRUMENTS REMARQUES
87 | Ochsenkopf........ 50° 1'52”, 29° 28'26”| 1812-20-21 | Soldner, Weiss, Mader,
43-50-51 Wieland.
88 OTD. ee 50 bl 22,27, 2 54 1822 Mader.
89 | Peissenberg ....... 47 48 4 | 28 40 37 | 1804-10-11 | Bonne, Mader, Rathmayer,
15-16-19-53 Thoma, v. Amman.
54-57
90 | Pöttmes........... 41834 5,2912 1806-10 Bonne, Glaser.
9) | Rachel. 48 5843.31 3 14 1811 Soldner.
92 | Radspitz -........ 50 14 8 | 29 5 59 || 1849-50-52 | Wieland, Fritz.
93 | Ranchwanne....... 48 46 0 | 28 10 18 | 1811-13-16 | V. Amman, Glaser, Mader. .
94 | Rauhe Culm....... 49 49 45 | 29 30 51 | 1812-20-21 | Soldner, Weiss, Mader, v. a .
22-25-42 Imsland. A
95 | Röttingen......... 3290. 2832 1822 Mader. en
06 "Rofanı.. 2... a 27 28 | 29°27 28 les Rathmayer. we à
97 | Roggenburg ....... 48 16 30 | 27 53 33 | 1810-11-16 | Glaser, v. Amman, Mader, o © ©
18-19 Hermann. Sn Ss |
98 | ROWE. 62-5, 355.53; 49 4770 | 29 8 20 1804-05 Bonne. ee va |
99 | Siuling ........... 41 32 6 | 2825 6| 1815-16 | Mader. Tee |
100 | Schafberg ..... + | 41 46 97 | 38 >51, ell Glaser. Sata |
101 | Sehnaitsee......... 48 417|30 2 9 | 1810-11-29 | Soldner, Glaser, Hermann, aa :
30-53-54 Wieland, Rathmayer. Oa se
102 | Schünegs 0. AS 628 | 2757 38| ls10lı |. Amman. 2 o A 3 |
103 | Schweitenkirchen ..| 48 30 21 | 29 16 17 Rte Bonne, Soldner, Fritz. a ae =
10-5 Sses
104 | Sodenberg......... 0 625.27 28) 2 1822-44 Mader, Wieland. noes
105 | Stauffersberg ...... 48 26 42 | 28 22 48 | 1810-11-16 | V. Amman, Mader, Her- 23353 |
18-19 mann. nal |
106 | Steinbach.......... 50 21 18 | 29 15 3 | 1833-49-50 | Mader, Wieland, Fritz. Oh &
107 | Tanfsten.. 0 2. 50.31 3126052 8| 1822 Mader.
108 | Teuchatz.. . 49 51 38 | 28 44 15 || 1820-21-22 | Mader, v. Imsland, Wie-
23-42 land.
109 | Watzmann......... 47. 33 3 » 30 35 19 1811-32 Glaser, Hermann.
110 | Wehringen ........ 48 15 1912825014 1805-10 Bonne, Glaser, v. Amman.
111 | Wendelstein ....... 47. 42 13 | 29 40 36 1852-54 Rathmayer.
112 WWolzburs.. | 49 1 31 | 28 40 8 | 1804-09-11 | Brousseau,v.Amman,Sold-
12-13-22 ner, Glaser, Mader.
113  Wirzbure .... 49 46 53 | 27 24 183 | 182225 Mader, Zobel.
2) Der Kreis links des Rheins (Pfalz).
114 | Biesingen ......... 49 12 41 | 24 51 49 | 1824-28 Messmer. : à à
Vis Calmit.. ... 22. A919 I 20 1710 | Mn Lämmle,Messmer,v.Klose. 12 X
25-2 © 5
116 | Derstenberg....... 49 5 5 |925 34 33| 1827 Messmer. 33 + |
117 | Donnersberg....... 49 37 24 | 25 35 43 || 1821-22-24 | Messmer, v. Klose, Her- wann
| 25-40 mann. ase
118 | Eschkopf . ee , 49 18.20, | 2551 2, ne Messmer, Leber. mes |
2 Boece ©
119 |Kandel. | 49° 4 59 | 25 SE 24 1827 Messmer, v. Klose. =o
120 |Ketterich.......... 49 8 22,20 io 23 1824-27 Messmer. _ Se ee
121 | Klobberg.......... Ag 449112552710 1825 id. 8 on =
122 | Lemberg .......... 49 46 52 | 25 25 52 | 1824-25 ji ass |
123 |Mannheim (Obser.).| 49 29 14 | 26 7 23 1820-29 Messmer, Lammle. 2285
124 | Chapelle $S. Michael. 49 5 18 | 26 13 29 Ve Klose. Seas
125 | Oggersheim........ 49.29 26 | 26 225, 181920 Lämmle. = a3» |
126 [Botzberg:........ | 49 31 18 | 25 8 37 || 1822-24-27 | Messmer, Leber. ee |
32 ODHHS |
127 Speyer 2... | 49 19.91.26 0242| en Lämmle,v.Klose,Messmer. ae |
| | | il

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 
