 

186 oo

Stationen erbeten; bis Jahresschluss war zunächst die Angabe für Gaithersbury
durch gütige Vermittelung von Herrn Superintendent Professor Prircuerr (Brief vom
21. December 1898) eingegangen.

Gleichzeitig machte der genannte Herr die höchst bedeutungsvolle Mittheilung,
dass die Coast and Geodetic Survey beabsichtige, den Beobachter für die Station
Gaithersburg aus ihrem Personal zu stellen, so dass der I. E. daraus keine Kosten
erwachsen würden. Diese wichtige Unterstützung, welche hiermit der internationale
Polhöhendienst erfährt, wird gewiss im Kreise der I. E. die grösste Anerkennung
finden, und Herr Professor Prircnerr hat sich durch die Herbeiführung dieses
Arrangements den wärmsten Dank verdient.

Zur Orientirung für weitere Kreise erschien nach vorheriger Vereinbarung
mit dem beständigen Secretär der I. E., Herrn Director Prof. Dr. Hirscs, ein von mir
und Herrn Geheimrath Arerecur verfasster Bericht in den Astron. Nachr. No. 3582,
betitelt: Der internationale Polhöhendienst, welcher Bericht an alle Delegirte versandt
wurde und auch in den Stuttgarter Verhandlungen von 1898 platzfinden soll.

‘° Hierin sind auch noch Mittheilungen über die Instrumente, das Beobachtungs-
programm und den Stationsdienst enthalten, die als Ergänzung zu dem, der Stuttgarter
Conferenz vorgelegten Bericht dienen können.

Zur weiteren Ergänzung möge im Folgenden eine kurze, zusammenfassende
(von Herrn Geheimrath Arsrecur aus den Akten zusammengestellte) Beschreibung der
Beobachtungsstationen gegeben werden, da es nicht möglich ist, alle die betreffenden
Verhandlungen in extenso zu drucken.

Mizusawa.

Die Station steht unter Leitung der japanischen Erdmessungs-Commission,
deren Vorsitzender, Herr Prof. Dr. H. Tirıo, Director des astronomischen Observa-
toriums in Tokio ist.

Ueber die speciellen Verhältnisse der Station liegt der: Bericht über die Wahl
der Station für den internationalen Polhöhendienst in Japan des Herın Professors
Tanakapare vor, welcher in den Verhandlungen abgedruckt wird. Aus demselben ist
zu entnehmen, dass in dem 5 bis 15 Kilometer breiten, von Nord nach Süd sich er-
streckenden Flussthal des Kitakami, 1 Kilometer südwestlich von der Stadt Mizusawa,
ein geeigneter Beobachtungspunkt aufgefunden worden ist. Indem in Betreff der
Details auf diesen Bericht verwiesen wird, sei hier nur noch darauf hingewiesen,
dass auf Tafel I eine Karte reprodueirt ist, aus der die allgemeine Lage der Station
zu dem Flusslauf des Kitakami und den Ausläufern der das Thal einschliessenden
Gebirgsketten zu ersehen ist. Diese Karte wird durch die speciellere in dem Bericht
von Herrn Tanaxapare ergänzt.

Die geographische Breite der Beobachtungsstation beträgt 39° 8° 07.

 

ddl ul

no ae A Melk |

kn Mk

 

 
