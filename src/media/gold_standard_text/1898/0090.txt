 

 

 

13.

14.

15.

16.

17.

18.

19

20

gart, ou invité

États-Unis.

Grande-Bretagne.

Grèce .

Hongrie
Italie
Japon .

. Russie.

. Suede .

M.

M.

M.

M.

82

E.-D. Preston, du U.S. Coast and Geodetic Survey 4 Washington,
suivant la lettre de M. Pritchett, superintendent du Survey, datée
du 6 juin 1898.

George Howard Darwin, professeur d'astronomie à l'Université de
Cambridge, suivant une dépêche du 9 mars 1898.

le Colonel Hartl, de l’Institut géographique militaire autrichien,
suivant lettre du 19 mai au Président, confirmée par dépêche du
16 juillet.

. L. Bodola de Zägan, professeur à l’École polytechnique de Buda-

pest, d’après dépêche du 20 avril 1898.

le Général Ferrero, Vice-Président du Bureau, Commandant de

corps d’armée & Alessandria, suivant dépéche du 19 février 1898.

. Térao, professeur à l’Université de Tokio, President de la Gom-

mission géodésique du Japon, d’après dépêche du 14 juillet 1898.

. le Général de Stubendorff, Chef de la section topographique de

l'État-Major à Saint-Pétersbourg, par dépêche du le? septembre
1898.

Rosén, professeur à l’école de l’État-Major à Stockholm, suivant
dépêche du 16 août 1897.

Outre ces 20 membres de la Commission permanente, nous donnons ci-dessous la
liste des autres délégués désignés par différents Gouvernements pour la Conférence de Stutt-

s suivant l'indication des représentants des États à la Commission permanente :

I. Allemagne. A. MM. le Dr Seeliger, directeur de l’Observatoire de Munich.

2.

=.

Or

10.
it,

»

»

»

»

»

le Dt Nagel, professeur 4 Dresde.
Ont reçu en outre la circulaire d'invitation, conformément à

la liste fournie par M. Fœærster :

le Général Oberhoffer, Chef de la Landesaufnahme, à Berlin.

le Colonel von Schmidt, Chef de la division trigonométrique de
la Landesaufnahme, à Berlin.

le professeur Albrecht, Chef de section à l’Institut géodésique
prussien, à Potsdam.

le professeur Westphal, Chef de section à l'Institut géodésique
prussien, à Potsdam.

le professeur Bærsch, Chef de section à l’Institut géodésique
prussien, à Potsdam.

le professeur Krüger, Chef de section & l’Institut géodésique
prussien, à Potsdam.

je Dr von Richthofen, professeur à l'Université de Berlin.

le Général D' von Orff, à Munich.

le Dr Max Schmidt, professeur, de l’Académie des sciences, à
Munich.

 

PP CO LL CELL ICONE LION! À

ll

Date tb

toh |

A
N
|
|
