 

|
|
i

 

iW |

Han

PODIUM UTP OR CUT ONT

73

 

Quant aux adhésions qui nous ont été notifiées aprés la distribution de notre dernier
rapport de 1898, il faut avant tout citer avec une vive satisfaction la rentrée de la Grande-
Bretagne dans |’ Association séodésique, dont nous avons été informés par une dépêche de
la Chancellerie allemande, en date du 6 janvier 4898. Par la même voie, la nomination de

M. George Howard Darwin comme re résentant du gouvernement anglais dans la Commission
©
permanente nous a été communiquée le 9 mars 1898.

Enfin, après avoir appris que le Parlement anglais avait voté les crédits pour la part
contributive de la Grande-Bretagne pendant la durée de la nouvelle Convention, le bureau

a porté cetle importante nouvelle à la connaissance des gouvernements par la lettre sui-
vante :

ASSOCIATION GÉODÉSIQUE Par; Na ta ‘1 1898
aris et Neuchatel, le : vrıl 1898.
INTERNATIONALE oe ? à

 

Monsieur I’ Ambassadeur,

Le bureau de l’Association a la satisfaction de faire connaitre aux Hauts Gouverne-
ments contractants que la Grande-Bretagne, sur l'initiative de la « Royal Society », a décidé
d’adhérer à la nouvelle Convention géodésique internationale conclue en 1895 pour l’époque
de 1897-1906 ; que le Parlement anglais a voté les crédits nécessaires pour le versement,
pendant ces dix ans, de la contribution annuelle de 7500 fr., et que M. George Howard
Darwin F. R. S., professeur d’astronomie au Trinity College 4 Cambridge, a été désigné par
le Gouvernement anglais comme délégué 4 la Commission permanente.

Cette rentrée de l’Angleterre dans l’organisation scientifique internationale, dont elle
avait déjà fait partie pendant quelques années, est certainement réjouissante à tous les égards,
non seulement à cause de l'importance et de l'étendue des grands travaux géodésiques
exécutés par les Anglais, mais aussi parce qu'elle achève de donner le caractère universel à
notre organisation scientifique qui embrasse désormais toute la terre.

Nous avons de plus l'honneur d'informer les Gouvernements adhérents à la Conven-
tion que la prochaine Conférence générale se réunira à Stuttgart, dans l’Aula de l'École poly-
technique, le lundi 3 octobre 1898, 43 heures. Nous prions en particulier ceux d’entre eux
qui n'ont pas encore désigné leurs Délégués, auxquels nous ne pouvons par conséquent
adresser directement la circulaire de convocation, de bien vouloir les aviser du Jour et du
lieu de la Conférence.

Nous serions en même temps très reconnaissants aux Gouvernements qui sont dans
ce Cas, savoir : l’Autriche-Hongrie, les États-Unis d'Amérique, le Japon, la Russie et la Serbie
de nous donner connaissance, aussitôt que possible, de la nomination de leurs Délégués.

Veuillez agréer, Monsieur l'Ambassadeur, l'assurance de notre plus haute consi-
dération.

Pour le Bureau de l'Association :
Le Secrétaire, Le Président,
Dr Ad. Hirscn. H. Faye.

 
