 

LENS eee ae de +

 

ma Rt

CT D ALLIE TILL

81
nécessaires, doit être envisagée comme définitive, puisque M. Pritchett, Superintendent of
the U. S. Coast and Geodetic Survey, a fait savoir A M. le Président, par lettre du 48 juin

1898, que le gouvernement des Etats-Unis sera représenté par M. Preston, Officier du
Coast Survey.

Enfin, nous avons le regret d'annoncer que, d’après une dépêche de la Chancellerie
allemande, en date du 18 janvier 1898, le gouvernement du Chili s’est retiré de l’Associa-
tion, parce que le budget de la République ne prévoit pas les crédits nécessaires.

Si l'on se rappelle que la République Argentine s’est également désistée, mais que
ces deux perles sont compensées par la rentrée de la Grande-Bretagne et par l’adhésion de
la Hongrie comme Etat indépendant, il en résulte que le nombre des Etats de l'Association

géodésique internationale qui ont accepté la nouvelle Convention est resté le même, c’est-à-
dire de 21.

Les modifications qui ont eu lieu dans la composition de l’Association géodésique
depuis 1895 ont, au point de vue financier, la conséquence heureuse que les ressources
annuelles de l'Association seront augmentées de 7400 M. — 9950 fr. En effet, l'entrée de la
Grande-Bretagne augmente les contributions annuelles de 6000 M. — 7500 fr. et celle de la
Hongrie de 3000 M. = 3750 fr., tandis que la retraite de la République Argentine et du
Chili comporte une diminution de 1600 M. — 2000 fr. Or, comme le dernier alinéa de l’ar-
ticle 9 statue : « Les parts contributives des États ne sont pas modifiées par l'accession d’un
nouvel État à la Convention. Ce dernier payera sa contribution selon l'échelle établie dans
cet article », il n’y a pas lieu de changer l’échelle des contributions, dans le but de ramener
la dotation exactement à 60 000 M. — 75 000 fr., somme qui, dans l’article 7, est expressé-
ment fixée comme ua minimum. Du reste, il ne faut pas oublier que les retards, malheureu-
sement trop fréquents, apportés au versement des parts contributives, peuvent avoir pour
conséquence une diminution génante des fonds disponibles, et en tout cas une perte d’inté-
réts assez sensible, de sorte qu’il serait désirable de pouvoir utiliser cet excédent de

7400 M. — 9250 fr., en partie du moins, à la création d’un fonds de réserve pour la gestion
financière de notre administration.

Passant à la composition de la Commission permanente, nous constatons avec plaisir
qu’elle est maintenant complète, sauf le membre représentant la Serbie, qui n’a pas encore
été nommé ; le gouvernement serbe a même avisé au mois de juin M. le Président, qu’il
regrelte de ne pouvoir pas désigner de délégué à la Conférence de Stuttgart.

Nous complétons la liste de 11 membres donnée à la fin du dernier rapport par les
noms suivants :

Nos ETATS DELEGUES
12. Autriche . . . M.le D' W. Tinter, professeur à l’École polytechnique de Vienne,
d’après dépêche du 20 avril 1898.
— 11

 
