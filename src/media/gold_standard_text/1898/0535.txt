 

|
|

 

Wi LT aan el)

but

1 pa id ik

HR COR a ee

Annexe B. XVIII.

JAPAN

Report upon the Progress of Geodetic Operations in Japan 1895-1898,
BY

A. TANAKADATE
(with 3 maps and 2 figures.)

The progress of geodetic operations in Japan down to the year 1898 is to be seen from
the Maps I. Il. II. It consists of:

One measurement of the base line in Sus
1896. Its length is 3291” 919 + 0" 00074.

Measurements of primary triangles ; Hosan, Kii, Hokurokudo and Goto ; the total
area being 1271 geographical square miles, or 4539 square ri.

Measurements of secondary triangles ; 610 geographical square miles, or 2177
square ri.

aka, in the province of Sinano, in the year

Measurements of tertiary and quarternary triangles; 2
or 1010 square ri;
Precision levelling; 2215 kilometres, or 566 ri :

Special topography in scale of I : 20 000 ; 82 geographical square miles, or 291
square ri ;

>

Special topography in scale of 1: 50 000: 124 geographical square miles, or 443
square ri.

83 geographical square miles,

These operations have been conducted by the Imperial Surveying Office of the War
Department.

The determinations of astronomical coordinates, conducted by the Imperial Hydro-
graphical Office, are as follows :

The observations of the variations of latitude in Tokyo, which for the last two years
have been carried on by D' Kimura under the superintendence of the Earthquake {nvesti-
gation Committee, will be continued. As Mizusawa lies almost due north of Tokyo, it will be
of great interest to compare the results obtained at these two places.

The Earthquake Investigation Committee has also the charge of the determination

of the force of gravity, under the direction of Prof. Nagaoka, who has already made several
observations which, however, have not yel been reduced.

— 61

 
