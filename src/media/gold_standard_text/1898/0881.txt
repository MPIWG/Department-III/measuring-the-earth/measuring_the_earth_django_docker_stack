Ei men À

DE EME u

dr nt

DRM Ou a dm du

HR |

 

vi — Grande Bretagne (Inde).

— 165 —

 

 

 

 

 

ma CAR PP ren aera spn atte pe * TE RARE SIE EEE ES th
|) INDICATION : ; DIRECTEURS
N D LATITUDE |LONGITUDE] EPOQUE Er one INSTRUMENTS REMARQUES
LE) Nelasat......2... 172 249, 028102 1871 Rogers, Barrow 24! no 2.
12 | Hudasi .. 0... 17 9/1 7m 4 8 1871 id: id.
F3 l'Impagat 7e 16 42 32 | 72 39.12 1871 id. id.
14 | Pirlagat.......... 16 51 35.77.18. 37 1871 id. id.
15 | Kottapalle........ 16.2819 1. 71 22 59 1871 id. id.
16-| Darur 2... I6 19.35 | 72 39.36 1871 id. id.
| Naikal 2... OS la Aer 1871 id. id.
18 | Chintalikunta..... 16 20 50 | 76 59 49 1871 id. id.
19 | Maliabad ......... IG S82 ie 2h 6 1871 id. id.
20 | Janikalı.......... 16 0 5%.,.76 57 25 al. id. id.
21 | Kere Balagal ..... 15 48 43 | 77 40 58 1871 id. id.
33: Adoni ee 15:38 50)| 77 16-46 1871 id. id.
23 | Pulikonda ........ 1528209 11.38 56 1871 id. id.
24 | Katomoraj........ 15 2920 | 7158 3 1871 id. id. ‘
25 | Gooty. <2 as I 6 AGH 70059 5 187] id. id.
26 | Gadekal .......... lo.7 1917 14 25 1871 id. id.
27 | Udarpidurga ...... 14:49:52: 772045 1871 id. id.
28 | Devarakonda...... 14 40 29 | 77 38 56 1870-71 id. id.
29 | Honnur........... 4 55.19 227 00 2 1871 id. id.
30 | Devädelabetta..... 143336 10% 730 1870 id. id.
31 | Kondapalli........ 14 31 49 71724 15 1870 id. id.
32 | Urakonda......... 1419 4277.30 29 1870 id. id.
33 | Pavagada. 2... 14,6 15) 77 1042 1870 id. id.
34 | Yerrakonda....... 13.5456 039. 1 1870 id. id.
35 | Innaglur.......... 14.222,78 120 1870 id. id.
36 | Ambäjidurga...... 13°23) 34) 7 24 1870 id. id.
37 | Dodnirmanga ..... 13 35 4977, 816 1870 id. id.
38 | Makalidurga...... 18 2640 80718 1870 id. id.
39 | Halasurbetta...... 13 938 | 77 37 13 | 1866-67-70 | Branfill, lieut. W.M. Camp- | T. et S. 24" no I et
bell, Rogers. Barrow 24" n° 2.
40 | Rämadevarabetta..| 13 19 19 | 77 956 1867-70 Campbell, Rogers, id.
41 | Savandurga....... 2550612721039 1867 Campbell. MT. et Ss. 24 n2
42 | Bannergatta ...... 12 2838 lie 34.36 1866-67 Branfill, Campbell. Tet. 246 peel et
Barrow 24" n° 2.
43 | Rangaswamibetta..| 13. 1 27 | 76 58 17 1867 Campbell. Tet Ss. 247 mele
44 | Hemagirt........- 124343 1 258 1867 id. id.
45 | Banatimaribetta...| 12 33 52 | 77 22 58 1867 id. id.
46 | Tirthapalli........ 13.2202 7058 lo 1866-68 | Branfill, Rogers. T. et Ss 22m 1 8
Barrow 24” n° 2.
47 | Nandigudi......... 13 12 8) 71053 48 1866 Branfill. vet Se 2A ne
48: Korn Re 13 SA Se 540 1866 id. id.
49 | Bhupatamma...... 12 3947,18 54l 1866 id. id.
50: Hour 12 43 32 | 77 50 23 | 1866-67-68 | Branfill, Campbell, Rogers. | T. et S. 24” n°1 et
Barrow 24" n° 2.
51 | Turukungutta ..... 12 5833| 71.42 44 1867 Campbell. Barrow 24" n° 2.
52 | Bangalore (Base Li-
ne-S. N: End)... 13 0 4177 3459. 186% id. id.
53 | Bangalore (Base Li-
ne N.E. End)... 15 460 775915 1867 id. id.
54 ı:Mandur: 2.00... 15 456 | 71.4959 1867 id. id.
55 | Devarabetta....... 237211 118037 1867 id. T. et S. 24" n° 1.
56 Anchettidurga Ines 12 38.18.) 77.03.20 1867 id. id.
97 | Mariyälam......... 12 2240: 71 42 20 1867 id. id.
58 | Koppabetta ....... 12 21 45 | 77 29 44 1867 id. id.
59 | Ponnäsibetta...... 12, 87 211 398.45 1867 id. id.
60 | Guttiräyan........ 12 15.251 77251 20 1867 id. id.
61 | Bandhallidurga....| 12 12 10 | 77 20 13 1867 id. id.
62 | Karadigutta....... 11 89,29 71.5236 1867 id. id.
63 -Bodamalai 1 54.51. 77.23 20 1867 id. id.
64.| Palamalal.. Fe, IL AL 281 7722 8 1869 Branfill. id.

 

 

 

 

 

 

 

 

 

 

 

 
