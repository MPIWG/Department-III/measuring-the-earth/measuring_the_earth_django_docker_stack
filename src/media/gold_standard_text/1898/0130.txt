 

 

122

l'Amérique du Nord sur une longueur de 55 degrés. Cette entreprise du Coast and geodetic
Survey fournira pour la mesure de la Terre un élément d’une importance capitale.

M. Sagasta lit le rapport sur les travaux exécutés en Espagne. (Voir Annexe B. VIL.)

M. le Président remercie MM. les délégués pour leurs précieuses communications
et il pense qu’on pourrait interrompre pour aujourd’hui la lecture des rapports nationaux,
et réserver pour les séances suivantes ceux de ces rapports qui n'ont pas encore été pré-
sentés. Dans ce but, le Secrétaire prie MM. les rapporteurs de bien vouloir s'inscrire sur la
liste préparée a cet effet.

Avant la clôture de la séance, les Présidents des deux Commissions spéciales nom-
mées hier convoquent leurs collègues pour des réunions qui auront lieu le lendemain. En
outre, M. Fœrster désire, en sa qualité de Président du Comité international des Poids et
Mesures, faire une communication qui intéressera particuliérement notre Conférence sur le
nouvel alliage en acier-nickel, découvert a Breteuil.

M. Fœrster s'exprime dans les termes suivants :

La plupart des membres de l'Assemblée savent, par les publications du Comite inter-
national des Poids et Mesures, que ce dernier a chargé le Bureau de Breteuil de chercher
des alliages appropriés pour la confection de régles de précision. Au cours de ces recherches,
MM. Benoît et Guillaume ont réussi à découvrir, parmi les alliages en acier-nickel, des com-
binaisons spéciales qui ont une dilatation particulièrement faible. Parmi ces dernières,
M. Guillaume a surtout constaté que celle qui contient environ 36 0/, de nickel sur 641/, d'acier,
lorsqu'elle a subi un traitement spécial, ne possède plus, à la température ordinaire, qu'un
coefficient de dilatation équivalant seulement à la 50% partie de celui de l'acier ou du fer.
Ce métal a naturellement la plus grande importance pour la science et la construction de nom-
breux instruments de précision, entre autres celle des règles de base. Toutefois, il ne faut pas
perdre de vue que, pour des recherches de la plus haute précision, cet alliage n'est pas
assez invariable, mais qu’il montre, plus même que d'autres métaux, certains changements
dont on doit conclure qu’il subit non seulement l'influence de la température, mais aussi du
temps.

M. Guillaume explique ce fait en supposant que la disparition presque complète de la
dilatation est un phénomène de compensation qui comprend deux éléments. D'abord, ce qui est
le cas chez presque tous les alliages, les composants des alliages se dilatent avec l'accroissement
de la température et se contraclent avec son abaissement. Toutefois, chez certains alliages on
a constaté des effets de retard dans ces mouvements moléculaires; par exemple les variations
d’une tige de laiton ne peuvent pas être considérées comme dépendant uniquement de la
température, mais aussi du temps. Ensuite, le second élément qui contrarie la dilatation du
volume du nouvel alliage, c’est que, d’après l’opinion de M. Guillaume, l'élévation de la
température produit dans l’acier-nickel aussi des combinaisons chimiques, de sorte qu'une
partie des molécules juxtaposées s’allient chimiquement lorsque la température s'élève ; avec
l'abaissement de la température, ces éléments se dissocient de nouveau. Or, on sait que les

 

Melk |. aid ul

ron dd ae

Jul: nit

Wb art!

;
1
|

 
