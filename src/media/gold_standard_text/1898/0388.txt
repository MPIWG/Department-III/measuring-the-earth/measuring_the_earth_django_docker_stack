 

lad ul

au ae ak eh ed

370

rait une surélévation du sol de près d’un millimètre par an. Mais si l’on remarque qu'en
1817 on avait précisément ce chiffre de 4458 et que les variations annuelles affectent une
forme sinusoidale, la conclusion à en tirer est que le granit de la Bretagne est stable et que
les variations annuelles doivent tenir à des causes non mises en équation, telles que la densité
de l’eau de la mer.

Quoi qu’il en soit, pour le niveau moyen, nous devons adopter la moyenne générale
de toutes les observations, soit 4°472, chiffre qui doit être encore augmenté de 7 millimètres
par suite de la hauteur de la cuvette du baromètre au-dessus de ce niveau moyen.

D'autre part, d’après le nouveau nivellement de M. Lallemand, le zéro du marégra-
phe est à — 4, 462 par rapport au zéro de Marseille, cela donne + 0017, pour la cote du
niveau moyen de la mer a Brest.

A Cherbourg nous avions trouvé pour le niveau moyen 3663 au-dessus du zéro du
maregraphe dont la cote nouvelle est — 37706, ce qui donne pour niveau moyen — 0"043.

Au Havre, nous avions trouvé un affaissement moyen annuel de 2 millimètres, chiffre
appréciable ; les nouvelles observations le réduisent à 0°"6. Jusqu'à nouvel ordre on doit le
considérer comme n'ayant aucune signification réelle. Le niveau moyen général est 4"801 ;
or le zéro du maréoraphe est à la cote — 4"968, ce qui fournit pour le niveau moyen celle
de — 0"167.

M. Hatt, pour le calcul des coefficients harmoniques de la marée, a analysé les courbes
de quelques années sans les corriger de l’influence du vent; ses résultats différent peu de
ceux que nous avons obtenus.

blu

I da nl

Enfin, les médimarémétres de M. Lallemand ont fourni toute une série d’éléments
du niveau moyen.

Nous plaçons dans un même tableau les résultats des niveaux moyens dépouillés
jusqu’à ce jour ; ils sont rapportés au zéro de la mer à Marseille.

mo ald aa 4 9

 

Médimarémètres. Maregraphes. Calcul de Ponts |
m M, Hatt. et Chaussées.
Niece |... 0,040 ;
Fü 2... + 0,030 |
Marseille. . ı... 0
Martigue.. ... — 0,020
Port-de-Bouc . . = 0,050
GENS. 5. | 6 0,030
Port-Vendres . . 0 3 .
Dueude 4. 1.0. = 0,160 + 0,034 0,02
| bDiazymizso. 2. 0,210 5
1 La Rochelle-La Palice +- 0,070 DO

Les Sables 0,030
I Sainl-Nazaire . . — 0,10
Î Doro, 222, -. 0,060
Ce... 1. 0.050

24 zummaiannsiinnnn, masses sil |

 
