     

     

 

in

V. — Espagne.

 

(Gees se ze

   

INDICATION |

DIRECTEURS

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

|
|
No x an | m | 1 : Ay
N | DES POINTS LATITUDE _ EPOQUE ET OBSERVATEURS INSTRUMENTS - REMARQUES
al
Triangulation de la Cote Sud. (Swite.)
89 | Aleornocosa ....... So A 102522 194 1872 Cap. d’art. Hernändez, Pistor n° 3.
90 | Asperillo. 3.358,10 050 1872 ick id.
gr Reales <3... scene 186.29 3,1227 51 1863 Comm. d'E.-M. Monet. Ertel n° 3.
92 | Torrecilla...... :.1:30 40/83 1 12,405 1863 id. id.
93 | Dadue.. 36,29 11 | 12 42 59 1863 id. id.
OA | Mages 0.0.0. 36 36 48 | 13 0 48 | 1863 id. id.
95 | Camorro Alto...... 30 98 164) 13; > 8 1863 id. id.
96 | Santopitar ........ 1 BO. AS dul | 3 22 a7 1863 de id.
7 | Sierra Gorda...... 32 3.0,1928 | 1863 Comm. d'E.-M. Monet, id.
cap. du génie Ruiz Mo-
teno (D. I).
98 | Cerrön...... | 36 49 40 | 14 33 44 | 1870 Lieuten. col. d’E.-M. Ahu- | Repsold C.
| | | mada.
99 Challos 2 37 542 14202] 1870 id. id. |
100: Baños @c259 7 ao 30642 7 A A955 1870 id. id.
OL BIO =.) 36 54 38: 15 0 49 1870 id. id. |
102 | Fotica +... 3710 9.15 5 > 1870 al. id.
103. Roldan. 2 | 36 56 42 | 15 45 41 1869 Comm. d'E.-M. Ahumada. id
104 |-Ténerife + 0.7 | aa 11, 5552 1869 id. id. |
Triangulation du Parallèle de Badajoz.
105 | Contenda.......... | 89° 1'52"| 10033’ 56" 1863 Comm. du genie Ibarreta, | Repsold (sans mar-
cap. d’art. Solano. que). |
+ 106.) ROO. 6 ea 38 50 23 | 10 24 50 1863 id. no |
= 107 | Reducton 222.2. 2839355 | LO 4 25 1863 id. id.
108 Alon 2 38 37 18 | 10.36 15 1863 id. D
F 109 Éobôn 2. Spb 2 ll 2,0, 1863 id. id. |
= 110 | Sierra Vieja. 88 2945 11 050 1863 Cap. dart. Solano. id.
= lll) Tires 38 41 24 | 12 19 36| 180 Comm. du génie Ibarreta, id. |
= | | cap. dart. Solano. |
À 112 | Bepics 39 233 | 12 12 42 1862 id. id.
2 lis Mc 0.5. 38.56 53 112) 36 fe 1862 id. a
a 114 Horcôn 3) 3: 39 12 7217» 1861 id. id.
= 1215. 2. Duranes . 85205519 1.9 1861 id. id.
116 | .Iudio.... .». . Vee dee 0401119680) 1861 id. id.
117 | Sierra Gorda.... .| 38 49 46 | 13 25 20 | 1861 id. id. |
118 | Juego de Bolos....| 38 52 27 | 14 34 25 1865 Comm. du génie Ibarreta. id.
= 119 | Cabeza de Buey ...| 38 37 47 | 14 28 9 1865 Comm. du génie Ibarreta, id.
: cap. d’E.-M. Jimenez Pe-
nacarrillo.
120 | Castellanos........ 38 36 41 | 14 47 53 | 1865 Comm. du génie Ibarreta. id.
121 | Barreros.......... 38 57 32 | 15 10 37 1865 id. id.
122 | Almenaras......... 3833244 9 5 1865 id. id. |
123. | Rople | 38 43 89 | 15 35 28! 1865 a Id id.
124 | Porrôn. #2 2 | 38 28 17 | 15 47 50 1866 Lieut. col. du génie Ibar- id.
reta, cap. dE.-M. Jime-
| nez Penacarrillo.
125 | Madron0 . . 53 30 01 6 3 8 1866 id. id. |
126 | Cabeza del Asno...| 88 19 39 | 16 753 1866 id. id. |
127. KW. Carche 2. ....2 38 25139) \ 16.30 31. || 1866 id. u id. |
198 Da Olive 5 38 44 16 | 16 38 32 | 1866-69 | Lieut. col. du génie: Ibar- | Repsold (sans mar-
= | reta, cap. d’E.-M. Jime- que), Repsold C.
i nez Peñacarrillo, comm.
i dart. Solano.
2 129 | Maïgmô .. 38 30. 7 | 20 1865 Comm. du génie Ibarreta. N (sans mar-
= que). |
= 130 Even) 38, 03 lo. ı In 2442 1200-09 Comm. du genie Ibarreta, | Repsold (sans mar- |
: | | | comm. d'art. Solano. que), Repsold C.

II

ME

 
