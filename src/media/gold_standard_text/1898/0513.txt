 

HR

 

ART

CT Wid

MRI | ARAB RIAA Ba LIU

matiques, M. Guedéonof a examiné 20 tours de |
de tour, en observant 18 digressions de la Pol

diagramme II).

471

 

DATE

MOYENNE

 

—- » 15
== » 20
_ » 20
= 30 6
— » 9
= > 28

u » 7
— » 12
FE » 1 5
= » 26

— octobre 5

sue » 10
— » 16
= » 30

— novemb. 20
décembre 8

— » i
— » 29

 

 

LAN TUDE

CS CY 89 Co Co Co eo
Co CS CE CS CO CO CO

su ©
X O0

io) co) ©
00

Ora CeO
ce )

=

5
©

CO

CO

 

| 1895 juillet  4/41° 19° 39740
38.

AA

49

AA
Ali
>>
AO

 

NOMBRE
DE COUPLES
D ÉTOILES

 

1896 janvier

MOYENNE

DATE

février

»

»
mars
avril

»
mal

»
juin

»

»

»

Juillet

MESURES DE PENDULE

 

29
15
18
29
23
8
29
11
20
6
15
19

5

30

 

a vis micrométrique par intervalles de 0,2
aire à l’est et à l’ouest du méridien (voir planche,

 

NOMBRE

LATITUDE |PE COUPLES

 

412 197 3817

38.
98.
38.
98.
38.
38.5
.20
38.
88.
38.
38.
38.
38.
38.
38.
98.
38.
38.

38

22
Ee
13
15
20
18

20

30
97
21
7

39
34
AA
29
42
38
45

 

D ETOILES

 

DI OL II OO OU EX
ve rr © À À À À Co À Co &

©9

M. le professeur Wittram, qui a pris part à l'expédition envoyée par l'Observatoire
de Poulcovo au pays de PAmur, pour l'observation de l’eclipse totale de Soleil, de 1896, a
mesuré la pesanteur à l’aide du pendule de M. de Sterneck A Orlovskoie (point d’observation

de l’éclipse, latitude + 50°41’) a Vladivostok et a Hong-Kong. Les résultats de ces observa-

tions ne sont pas encore connus.

M. le professeur Doubiago ayant pris part à l'expédition dirigée sur Novaia Zemlia
dans le méme but que la premiére, a aussi observé la pesanteur à l’aide d’un appareil de

M. de Sterneck et a obtenu, d’après un calcul provisoire, les résultats suivants :

 

 
