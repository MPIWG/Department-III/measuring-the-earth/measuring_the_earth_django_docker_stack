 

414

9° Le nombre total et la longueur moyenne des nivelées, laquelle, pour le Réseau
: entier, est de 129™.
| 3 L’erreur accidentelle probable, par nivelée et par kilomètre !, déduite de la compa-
raison des résultats des deux opérations normalement faites sur chaque ligne. Cette erreur
| | est de 0™"8 par kilomètre, pour la totalité du Réseau fondamental.
| 4° L'écart systématique total pour la section entière. Pour obtenir cet écart, on à
cumulé algébriquement, depuis l’origine de chaque section, les discordances de repère à
repère entre les deux opérations d’aller et de retour ; les résultats du calcul ont ensuite été
traduits en diagrammes ayant pour abscisses les distances 4 l’origine de la section et, pour or-
données, les discordances cumulées à partir de cette origine.

Sur ces diagrammes, on a recherché, pour chaque section, les tronçons de courbe
ayant à peu prés même allure et l’on a mené à vue, à travers chacun d’eux, une droite
moyenne 2. La différence des ordonnées extrêmes de cette droite mesurait, pour le tronçon
correspondant, l'écart systématique cherché. L'écart total pour la section a été pris égal à la
i racine carrée de la somme des carrés des écarts partiels.
5° La différence moyenne brute de niveau, pour la section entière, c'est-à-dire la
| moyenne des différences de niveau entre les deux repères extrêmes, respectivement obtenues

à l'aller et au retour.
| 6° La correction orthométrique totale pour la section, c’est-à-dire la correction à
| ajouter algébriquement 4 la différence brute de niveau pour da l'erreur due au défaut
de parallélisme des surfaces terrestres de niveau °.

7 L'erreur probable totale résultant des erreurs accidentelles et systématiques et de
l'incertitude métrique de l’étalonnage des mires +.

8° Les corrections de compensation calculées par la classique méthode des moindres

 

3

carrés.

eme ui au dh a da a anna à +

1 Pour le mode de calcul de ces erreurs, voir Nivellement de haute précision [no 91), par M.
Ch. Lallemand. — Paris, 1889. — Baudry, éditeur.
2 La longueur moyenne des tronçons de même discordance systématique a été trouvée de
56€". Pour plus de détails à ce sujet, voir Nivellement de haute précision, op. eit., no 80-1.
s Voir Nivellement de haute précision, op. cit., Cl, IE Sole
‘Dans mon Nivellement de haute précision, op. cit. no 96 A, j’ai montré que si l’on désigne
par
nr, Verreur accidentelle probable kilométrique ;
L, la longueur de la section ;
S, Pécart systématique total ;
à , Vincertitude métrique probable d’étalonnage des mires = + 2°";
D, la différence totale de niveau pour la section;
6 , Verreur probable totale cherchée ;
on a:

pace fn bt Fe:

|
4

 
