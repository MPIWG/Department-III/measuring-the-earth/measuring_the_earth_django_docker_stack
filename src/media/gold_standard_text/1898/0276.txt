 

 

258

 

DEUTSCHLAND

Durch das Königlich Preussische Geodätische Institut sind auf 22 Stationen des
Meridianbogens Kolberg-Schneekoppe die Lothabweichungen in Breite betsimmt und zur
Ableitung des entsprechenden Geoidprofils benutzt worden®; das Resultat ist in schöner
Uebereinstimmung mit den aus den gleichzeitig angestellten relativen Schweremessungen
sich ergebenden Folgerungen. Zu nachstehender Tabelle ist zu bemerken, dass die Breiten-
beobachtungen im Jahre 1894 ausgeführt worden sind, abgesehen von denen für die Schnee-
koppe, die schon aus dem Jahre 1888 stammen. Auf den Stationen Nr. 1-11 rühren die
Beobachtungen von Herrn Schnauder, auf den Stationen Nr. 12-21 von Herrn Galle und für

Lothabweichungen in Breite auf der Meridianlinie Kolberg-Schneekoppe.

   

 

 

 

 

 

 

 

 

 

L ae hache Lothabweichung
A ange gegen enaherte eobachtele Astr. —Geod. TET E AN
N STATION on Meereshôhe| geogr. Breite N a Me aco BEMERKUNGEN
m Rauenberg : +5”
© ! © , n "
i) Kolberg . 15 35,8 8 pe it Te + 1,4 Sand
2 | Bartin. 42,0 60 54 6 26,47 a. Jurakalk
3, Klorberg, . 48,0 1, 53 51 49,88 + 3,5 Sand
4 | Kleistberg . 29,6 180 03 28.19.15 4 >
5 | Arnswalde. Poe 60 3310 7,20 DD »
6 | Sehlsgrund 48,0 109 52 02 49,58 + 48 »
7| Goray I. ; 43,4 114 2 35.21.03 + 8,9 »
8 | Tirschtiegel . 02,4 38 222 540 + 9,4 >
SS Bomst:. au 518 > 2 949500 + 10,4 »
10 | Grünbergshöhe oY 200 Bil 56707; 16 + 7,4 »
11 | Neustädtel. . . 44,0 93 5d Al 8221 + 6,4 »
12 | Wolfersdorf I . 4922 189 51 27 56.33 + 4,7 Diluvialer Sand
13 | Gröditzberg | A9 992 D" t0 42:05 + 5,0 Basaltkegel
14 | Ludwigsdorf. 46,2 608 50 58 30,52 + 4,6 Silur. Thonschiefer
15 | Grunau . 45,2 308 50 55 38,00 do Diluvialer Lehm
16 | Cunersdorf 43,8 343 30 35 25,00 + 6,6 Granitit
17 | Stonsdorf . 44,2 389 8091 1505 + 9,0 »
18 | Seidorf ; 43,5 383 50 49 36,27 —- 12,9 Porphyr
19 | Giersdorf I AAA 185 us 21,12 —- 15,4 Granitit
20 | Querseifen. 2,11 128 50 46 55,80 H- 16,8 »
21 | Alter Bruch . 44,6 917 50 45 58,02 + 18,1 »
22 | Schneekoppe. . 44,6 1605 50 44 20,84 + 11,6 >

» Bestimmung der Polhöhe und der Intensität der Schwerkraft auf zweiundzwanzig Stationen von der
Ostsee bei Kolberg bis zur Schneekoppe. Mit vier Tafeln. Berlin 1896, Stankiewicz. (Veröffentlichung des
Königl. Preuss. Geodätischen Instituts). S. VI/IX, 39/40, 83/85. — F.-R. Helmert, Ergebnisse von Messungen
der Intensität der Schwerkraft auf der Linie Colberg-Schneekoppe. (Sitzungsberichte der Königl. Preussischen
Akademie der Wissenschaften zu Berlin. Jahrgang 1896. Erster Halbband. Berlin 1896, Reimer. S. 409/413.)

 

di à

nd a a) Melk |

ak tik

on!

|
|

 

 
