 

    

— 404 —

XVI. — Russie.

 

INDICATION
a | DES POINTS LATITUDE |LONGITUDE

EPOQUE ie a INSTRUMENTS REMARQUES
ET OBSERVATEURS

| a | al

oe du Parallèle 52°. (Suite.)
622 | Panchino.......... 590 57! Gé 8

 

 

 

 

 

 

 

 

 

 

675 | Orenbourg......... AS 2, 46
676 | Blagoslowennaia...
677 | Stoudenetz ........ |
678 | Ostrownaia........
679 | Tehernoostrojins-|

oe 2

 

623 | Zaborowka........ Ba is | es 56 {1858-59 | Colonel Maslou.
624 | Batraki..... A Bo 1 66 18 | 1872 id.
625 | Trubettchina ...... 53 16 65 57
626- | Ossinowka......... 5386 6560 iq
627 | Aktachka.......... 53 97 66 22 1858-59 id.
"028. | Kostytchi.........: 53 11 66 23
629 | Petcherskaia ...... 53 ji 66 29 |
630 | Nataliino.......... Bo Pi 66 33
631 | Percwoloki........ 53 14 66 51
- 632 | Wassiliewka....... 52 55 Ge |
633 | Ssewrukowa....... 53 11 67: 10
634 | Winowka.......... 53.18 67 22
6351| Mitowa........:... 52 59 67 50
636 | Woskresenkoie..... Bd 67 36
637 | Tornowaia ........ Be 17 67 36
6384 SSMara.. 62.2. .65.° 53 Il 67 4 |
639 | Doubowy-Qumiot...| 53 1] 71 57 |
640 Sssad. 2.2.2... 53 12 67 50
641 | Alekseiewski-Pry-
Bon ee 2. 52 15 68 7
642 | Spiridonowka...... 53.8.2268 183
643 | Baratchki......... 53 17 68 27
644 | Maloïe Malychewo .| 53 5 68 48
645 | Pustowalowa...... 59. 412 08 58
646.| Klutcht ........... 53 7 69 6 oO
647 | Ousmanka......... 52 50 69 28 I:
648 | Jelezny- Mar. la 52 50 69 5 &
649 | Podkolka.......... 52 48 69 35 eo
650 | Grichkina......... 52 36 69 22 ee =
e001 | Lipowka .......... 52 39 69 46 & a
652 | Trimikhailowka ..., 52 46 0 2 8 =
653 | Strielkowa........ 52 39 10 <2 = >
654 | Pogromnoïé ....... 52 33 F0 À a =
655 | Kamienna Ssarma..; 52 40 70, 16 D ©
656 | Totzkaïa (mérid.)..| 52 30 | 70 20 = 5
657 | Totzkaïa (sept.) ...| 52 35 50 Si Ss =
658 | Elchanka.......... 52 42 | 70 25 : =
659 | Pokrowskoié...... be 44 70 AS 5
660 | Jachkino.......... 52 40 7) 23 06% ©
661 | Ssorotchinskaïa....| 52 27 10 52 S
662 | Nadejdino ......... ÉD ol 110 Ex
663 | Bogolubowka...... 52 28 7 84
664 | Nowosserguiéwka ..| 52 13 11 283
6654 Baleika.. 1. %. 52 225 7: 36
666 | Chotowa........... 52 A) 71 3
667 | Kuwakbaiewa ..... Be 22 Wh BS
668 | Kroutodol......... De | 22 55
669 | Kutlumbetowa..... 52 19 210
670.) Repina............ DD A VT MB |
671 | Ssoudakow ...:.... 51 52 12416
672. Karsala........... el be Foie
673 | Maiatchnaia....... ol 47 72 42
674 | Ssakmarskaia...... ‚hl 553 72.56

 

 

 

 

ee 4h ds dv dd Ai san

here dlhmrmnnnnt nde

i644, adm As a
