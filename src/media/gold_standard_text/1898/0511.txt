 

|
|

 

ra

WIE” LURE RR |ARBRD 1H ARIA GT OO) | 168) Fe

même pour une partie de la Russie occidentale on a été force de calculer les éléments d’un
sphéroïde spécial. Le seul moyen de remédier à un pareil état de choses était de soumettre
tout notre réseau trigonométrique, composé de plus de 20000 points, à un nouveau calcul
de compensation. L’exécution de ce travail a été décidée au commencement de l’année 1897
et dans ce but il a été formé auprès de la Section Lopographique de l’État-Major un bureau
spécial sous la direction de M. le lieutenant-général Scharnhorst. Comme unique point astro-
nomique pour tout le réseau, on a adopté l'Observatoire de Poulcovo, et les positions géogra-
phiques seront calculées avec les éléments de l’ellipsoïde de Bessel.

OPÉRATIONS ASTRONOMIQUES

M. le colonel Guedéonof et M. le lieutenant-colonel Zalesski ont déterminé, en 1896,
par le télégraphe électrique, avec échange des observateurs, la différence de longitude
Aoulié-ata—Tachkent et ont obtenu le résultat suivant : Aoulié-ata (clocher de la nouvelle
église) —Tachkent (cercle méridien) — ++ 0" 8" 20° 598 + 0.012.

En 1881, M. le general Pomerantsef avait determine la longitude d’Aoulié-ata par
transport de chronométres pendant une expédition chronométrique entre Tachkent et Vérny.
Pour comparer les résultats des deux observations de 1881 et de 1896, obtenus par différentes
méthodes, M. Zalesski a opéré une jonction trigonométrique entre la station astronomique
de M. Pomerantsef, (l’ancienne église) et la nouvelle église, et a déduit pour la longitude de
ce point, des observations de M. Pomerantsef, la valeur de — (jh gm 20) Oo.

En 1897, les mémes observateurs ont déterminé par la méme méthode la différence
de longitude Pérovsk—Tachkent et ont obtenu :

Pérovsk (croix de la coupole principale de la cathédrale).

Tachkent (cercle méridien) = — 0" 15" 13° 358 + 0° 011.

Pour la même différence de longitude, M. Zalesski avait trouvé en 1891, par trans-
port de chronomètres, la valeur de — 0 15% 18° 49.

Pour satisfaire aux besoins du Service géographique, les géodésiens russes ont
continué pendant les années 1896-1897 les observations de longitude par transport de

chronomètres, ainsi que les observations de latitude dans différentes parties de la Sibérie et
du Turkestan.

VARIATIONS DE LATITUDE

Les recherches des variations de latitude ont été continuées aux Observatoires de
Poulcovo, de Moscou, de Varsovie et de Kazan.

A Poulcovo, les observations, au nombre de 230, ont été exécutées à l'instrument

des passages dans le premier vertical. Les résultats de ces observations, après leur réduction
définitive, seront envoyés au Bureau Central.

 
