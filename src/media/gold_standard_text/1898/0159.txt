 

ee ee

 

en]

À FAURE VU A à

mom |

151

M. le Président ouvre la discussion sur ce programme des travaux du Bureau central.

M. Ferster desire d’abord faire une observation de forme, qui pourrait avoir de
Vinfluence sur le mode de votation. Il lui semble qu’il s’agit d’abord de Vappréciation scien-

lifique des travaux proposés, et non pas du montant des crédits qui pourraient être néces-
saires pour les exécuter.

M. Hirsch, quant au point No 5 du programme, concernant la continuation des
études sur les mires de nivellement, est d’avis qu’a ’avenir les mires en bois seront proba-
blement remplacées, dans les nivellements de précision, par des mires métalliques en acier-
nickel, parce que ces dernières, sans être influencées par la température d’une manière ap-
préciable, sont complètement insensibles aux variations d'humidité. Il croit done que, abstrac-
tion faite de la nécessité d'achever les recherches antérieures par les réductions et la
discussion des résultats obtenus, il y aura, pour l’avenir, peu d'intérêt à continuer au Bureau

central les expériences de M. Stadthagen au sujet de l'influence de l'humidité sur les mires
en bois.

M. Bassot pense au contraire que ces recherches pourraient avoir de l'importance pour
les nivellements exécutés jusqu’à présent.

M. Fœrster croit également qu’il serait prématuré d'abandonner les études com- ©
mencées.

M. Lallemand appuie l'opinion de M. Fœrster et ajoute que, pour répondre à un
vœu plusieurs fois exprimé au sein de l'Association, il a fait, d’après les manuscrits origi-
naux du Colonel Goulier, une nouvelle étude, complète et détaillée, des expériences exécutées
avec des soins méticuleux et un remarquable sens critique par ce savant observateur, au
sujet des variations de poids et de longueur des règles en bois sous la double influence de la
chaleur et de l'humidité.

Entre autres choses intéressantes, cette étude à montré que, contrairement à une
opinion quelquefois émise, les variations de poids des règles, comme leurs variations de
longueur, sont liées aux changements d'état hygrométrique da milieu avec lequel elles sont
en équilibre d'humidité, mais qu’elles dépendent, au contraire, fort peu des changements
dans la tension absolue de vapeur de cette atmosphère.

On a même constaté, dans quelques essais spéciaux, que des règles, maintenues
dans un milieu saturé d'humidité, diminuent légèrement de poids quand la température et
par conséquent l'humidité absolue augmentent.

Les résultats de cette étude ont été, de la part de la Commission du Nivellement
général de la France, l’objet d’une publication restreinte à un petit nombre d'exemplaires,
à laquelle se trouvent annexées huit planches en couleur donnant, pour quatorze différentes
natures de bois et au moyen de surfaces topographiques définies par leurs courbes de ni-

 
