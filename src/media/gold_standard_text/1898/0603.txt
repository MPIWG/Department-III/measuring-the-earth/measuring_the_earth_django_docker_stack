 

|
I
if
!

 

ART

ll

OL TRUE

PUR LATIN LIN 881088)

947
TABLEAU V. — Influence de la température.

Coefficients thermiques de dilatation des bois dans divers états hygrométriques.

 

 

 

  
   
    
     
       
  
    
   
     
     
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

ALLONGEMENTS PAR METRE
pour 4° d’augmentation de la température, entre 0° et 58°
nn eee ii
Régles naturelles Regles peintes Règles huilées
ET Tr m | Te N TAN OOOO IT LS
ESSENCES DES BOIS Sd = = 5 zZ
ee. | a
tres | 25 | SS | més | BS | B= | ies | ee | —
ows ee oo eo 2 2 du
Sec. | SES 2e so pas ee | > ae
ied Es g d Ss SE 8 es Sg
\9") 2 = (Ol = (BL #2 3
< = =
de pd u u u B de u u
I. — Bors RESINEUX
Pitch-pin ordinaire sec 2,8 8a, E92, »0 Je 0, Mr 72 | 19
Sapin demi-sec . 0,8 | 81) 40) 62) 80 58 02, 7
Epicéa demi-sec. 3.0 | 10,6) 2,9 | 28 12 50) 0 Ga ae
Epicéa sec _ 46 | 18,4) 84 52 | 16 46 eo Ga 7
Moyennes 4,2 | 10,1) 3,5 | 5.5 Kor 22,23 10: 39
Il. — Boils NON RESINEUX |
Tulipier 9,2... 120) 80) 26 | 128) 20 72 ne 2,4
Charme 38 | 10,9) 40) 691 28 so 7
Idem 4,188) 58 0,0) wa enter Men PEN
Alisier. 4,0 )..78,1:4,6,1 8,6, 69, ze 91 9 2
Peuplier . 36 182 20, 06 102, © 80, 02 20
Acajou femelle 4,0.) 1,21 384 28 108 Sa en
Hétre . 3,6} 114,011 48111781" 1094 58) One
Buis 83 | 114) 61) 88) 182) 60) 90 0 7
Teck 8.0 | 16,0) 6.5 | 8.0) mo 74 | zo 2 7
Fréne 4,8 °° ONO 8a 109 GO| GL | 0) Gee
Moyennes 51 | 122| 45 | 71 In, se Os) 0 | aa
Moyennes générales . 453 | 11,0) 4.2) 06,6, 15.30 0 3 5,0)
|

 

 

 

 

 

 

 

 

 

 

  
      
 

NOTA. Les coefficients ci-dessus ont été déterminés en prenant dans les surfaces topographiques
(pl. I a VII) les deux coupes extrémes, qui répondent respectivement au minimum et au maximum de
poids des règles, et la coupe moyenne pour un état hygrométrique d’environ 604,

On a ensuite calculé le coefficient moyen d’inclinaison de chacune des sections, a peu prés rectilignes,
ainsi obtenues.

La figure 10 offre quelques spécimens de ces coupes.

 

 

 
