 

(ci ee een

 

ART

MRT TLD RI a en

539

Les résultats de ces mesures sont consignés dans le tableau II] ci-aprés. Ils ont servi
a construire diverses courbes (fig. 7, 8 et 9) donnant, pour une température voisine de 15°,
les variations de poids et de longueur de quelques règles en fonction de Pétat hygrométrique et
les variations moyennes correspondantes des bois résineux, d’une part, et des bois non
résineux, d'autre part.

Connaissant ainsi, pour chaque bois, la relation entre le poids des régles et leur
degré d’humidité, on a pu approximativement déterminer et representer (fig. 5, 111) la série
des états hygrométriques par lesquels est passé l’ensemble des règles pendant la première
période des essais, du 8 octobre 1883 au 11 février 1884. Isolant, dans cette période, toutes
les observations faites à une température voisine de 15°, et prenant ensuite la moyenne des
longueurs respectivement constatées pour les bois résineux et pour les bois non résineux,
d’abord à l'état naturel, puis à l’état peint et enfin à l’état huilé, on a obtenu les chiffres
consignés dans le tableau 1V ci-aprés et reportés graphiquement sur la figure 8-II, de maniere
à faciliter les rapprochements avec les données correspondantes du tableau III traduites sur
la même figure.

TABLEAU IV. — influence de l'humidité.

Variations moyennes de longueur des bois résineux et des bois non résineux, sous l'action

de l'humidité seule, à une température voisine de 15°. (Résultats obtenus dans la première
série d'essais.)

    
        
     
      
    
       
          
  
    

 

 

 

 

 

 

 

 

 

 

 

 

© Ei BOIS RESINEUX BOIS NON RESINEUX
| oe ©
| =.= | 5 | \ \
| 3.22! < ; | 2 5 Longueurs moyennes des 4 règles. Longueurs moyennes des 10 règles.
DATES ss: 24 | "2" a =
2 eu = En ie io ecle 8 Pole & ag
des observalions. en ee han. | | oe
= == En u rollen peintes huilées fees peintes huilées
= a m ff m. m.99923 2) Om 99% 2
TD = | gm 99980. [0.999767 | 0m, 99074 : om, 999082 | 099923 *| 0m,99912
| = + ug Fa mn +
jours. cmm. cmm. emm. cmm. cmm, cmm.
18: octobredl883.. 210%, 152 55. 70 59 61 83 95 80
24.octobre = | ANG oo >20 21 26 At 31 50 4
‚I6novembree ._ ...3 16 54 10 | — 6 22 18 4 il
HArdecembre 2 9 57 ar 9 400: 96 77 18 181 162 191
| I0 deeemure 63 | kon, god 89 70 13 143 150 139
Il janvier less | 16% 92 ol 83 88 170 142 175
tidevrier 2:5 22.04 bo, Pe 100 On 80 16 190 164 199
! Moyenne des longueurs des 4 règles de bois résineux (respectivement à l'état naturel, à l'état peint
| et à l’état huilé), pour l’état hygrométrique 34, obtenu par dessication, en présence d’une solution d'acide
| sulfurique monohydraté (voir tableau IL, longueurs Ly, Le, Ls). Es
? Moyenne des longueurs des 10 règles de bois non résineux, dans les mêmes conditions.

 

L'examen de ces diverses courbes a nettement confirmé les résultats déjà entrevus,
dans le principe, à l’inspection des surfaces topographiques et des premières courbes cons-
truites. On a pu, dès lors, formuler les conclusions suivantes.

 
