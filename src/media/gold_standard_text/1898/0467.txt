 

 

imma a

aii

ai

CORAIL MILITE IL

Annex B. VI

REPO RW

ON THE

2EODETIC OPERATIONS IN THE UNITED STATES.

The geodetic operations in the United States as executed by the Coast and Geodetic
Survey may be grouped into three distinct periods of time. The work was authorized by
Congress in 1807, but a quarter of a century elapsed before anything was done in the field
worthy of the name of geodesy. This closed the first period, which may be characterized as
the era of preparation and education of public sentiment. In 1832 operations were begun
with vigor, and the foundation was laid for a great national work. The Survey was con-
ducted on the same general lines of policy for eleven years, when the reorganization of
1843 established its permanent status. No great deviation has since been made from this
plan, which has now held for fifty-five years. If we eliminate the civil war period of five
years, during which work was suspended, and regard operations before the reorganization
as of a preliminary nature, we have half a century of geodesy. During its comparatively
Short existence the Survey has been three times under the control of the Treasury Depart-
ment, twice under the Navy, and once under law requiring its personnel to be army or
navy officers. The direction of the work has, however, remained throughout in the hands
of a civilian, and civilian methods have been applied in the administration. At the present
time it has been continuously under the Treasury for a period of sixty-two years. Although
Statistics do not always give an adequate conception of the work to which they are Supposed
to bear testimony, a general idea of the activity displayed may be had from the following
statement of work done:

Three hundred and fifty thousand square miles (906,500 square kilometers) of triangu-
lation, embracing 15,000 stations for horizontal measures and determining 28,500
geographical positions at which 1,000 astronomical coordinates have been observed.

Thirty-eight thousand five hundred square miles (99,710 square kilometers) of topog-
raphy, embracing 11,600 miles (18,670 kilometers) of general coast line and more than

100,000 miles (160,900 kilometers) of shore line (rivers, etc.), also including 51,000 miles

(82,080 kilometers) of roadways.

Five hundred and forty-five thousand miles (877,100 kilometers) of sounding, covering
164,000 square miles (424,800 square kilometers) of area, besides 93,000 miles (149,700
kilometers) of deep-sea sounding, in which 14,000 bottom specimens were obtained.

Four thousand six hundred original topographic and hydrographic sheets, from which
1,300,000 charts have been made and distributed; 30,000 original volumes of observations,
including magnetic records from 1,100 stations.

1-55

 
