 

|

|
|
|
|
|
|
|

 

494

ee ]

I. Réseau de deuxième ordre proprement dit.

Lignes nivelées avec les procédés du nivellement de premier ordre . . . . 705°
Lignes nivelées avec les procédés spéciaux au nivellement de second ordre . . 13924
Pomomenm tiledestignesmouyelles .. . . . . . . . . 2200120043946"
Lignes de l’ancien nivellement de Bourdaloué incorporées après discussion . . 3 222

Imserable. . . 11 16808

II. Mivellements exécutés avec la méthode de second ordre,

pour rattacher des points stables situés à proximité des lignes du Réseau de
ee .  . . 39

Total general :. "AT 506

RÉSEAU DE DEUXIÈME ORDRE.

Diagrammes des discordances systématiques entre l'aller et le retour,
classées d'après leur valeur kilométrique et d’après la longueur totale des cheminements de même erreur.

i0 Lignes nouvelles.
Discordances systématiques groupées par

 

 

 

 

 

 

 

 

 

 

   

 

 

 

ee Te — — a
A décimillimétres oes demi-millimétres
ongueurs ngueurs
correspondantes correspondantes
pour 1000 km pour 1000 km :
fe chemipement. de cheminement. Be |
u d rT 4
És 2LO ee km i
km | km ne ST =
100 Ni. 100 Do ee N | 300
m | NE
N | i
NV |
NN 200 200
NN
50 HAT 50
Een
N I
: ANS 100 100
N x 4
WN =
ee NA a Ds o en = ES = 0
nn 0 es 10, 0 240 108 +10 4e 0 0 010 120
Grandeur des discordances. Grandeur des discordances.
Fic. 7. Fic. 7bis

La compensation des mailles de deuxième ordre a été exécutée, par polygone ou zone
de premier ordre, au fur et à mesure de l’avancement des opérations; elle est aujourd’hui
complètement achevée, ainsi que le calcul des altitudes des repères.

 
