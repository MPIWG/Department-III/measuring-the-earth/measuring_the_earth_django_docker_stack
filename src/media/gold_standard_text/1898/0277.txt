 

{tts mews Ÿ

 

À Ha di

nal Wel

year

Dig Le à

MLE TARR A

Nr. 22 (Schneekoppe) von den Herren Albrecht und Richter her. Sämmtliche Stationen sind
trigonometrische Punkte der Königlich Preussischen Landesaufnahme, auf die auch die zumeist
excentrisch angestellten Beobachtungen reduzirt sind. Die Lothabweichungen beziehen sich
auf das im Nizzaer Bericht von 1887 angenommene mitteleuropaische System und auf Clarke’s
Ellipsoid von 1880. Ausgangspunkt ist die Schneekoppe, die als Nr. 30 auf S. 27 des Nizzaer
Berichts aufgeführt ist; da die Neubestimmung ihrer Breite einen um Q"9 grösseren Werth
als früher ergeben hatte, so war die Lothabweichung in Breite für die Schneekoppe von 10’7
auf 11°6 zu erhöhen.

Mit der kürzlich erfolgten Veröffentlichung der Resultate für 13 Azimutstationen und
für die Längenbestimmung Jerxheim-Kniel? sind die astronomischen Bestimmungen im Harz-
gebiet vorläufig abgeschlossen worden, so dass nunmehr zur Construction des Geoids in dieser
Gegend geschritten werden kann; bekannt ist, dass Andrae schon 1883, indem er nur Loth-
abweichungen in Breite benutzte, unter gewissen Voraussetzungen einen ersten Versuch
nach dieser Richtung hin unternommen hatte.

Eine einzelne Bestimmung der Lothabweichung in Breite ist 1895 für das Observa-
torium der Königlichen Navigationsschule in Danzig erfolgt; es wurde für sie gegen Rauen-
berg und für Bessel’s Ellipsoid — 4°0 gefunden, ein Werth, der sich dem allgemeinen Verlauf
der Lothabweichungen an der Ostsee-Küste gut anschliesst. Eine ältere telegraphische Be-
stimmung der Länge für diesen Punkt scheint zu einer Ableitung der Lothabweichung in
Länge nicht auszureichen.

In Bayern sind von Herrn Dr. Oertel auf der in der nachstehenden Tabelle aufge-

_ führten Stationen die beiden Componenten der Lothabweichung in Breite (&) und im Parallel

(n), die letzteren durch Azimutmessungen und nur für Bamberg durch eine astronomische
Längenbestimmung, theils neu bestimmt, theils neu berechnet worden’; hierunter befinden
sich auch wieder die drei Bauernfeind’schen Stationen für die Bestimmung der terrestrischen
Refraction. € und » sind hier im Sinne Astr. — Geod. aufgeführt, wobei die Längen nach Osten
wachsend angenommen wurden. Zu Grunde liegt Bessel’s Ellipsoid; Ausgangspunkt ist die
Sternwarte in München.

* Bestimmungen von Azimuten im Harzgebirge, ausgeführt in den Jahren 1887-1891. Bestimmung der
Längendifferenz Jerxheim-Kniel mittelst optischer Signale. Berlin 1898, Stankiewicz. (Veröffentlichung des

. Königl. Preuss. Geodätischen Instituts.)

5 Den Danske Gradmaaline. IV. Bd. Kopenhagen 1884, S. 414/41% und eine Tafel; oder auch: C.-G.
Andræ, Problèmes de haute géodésie, extraits de l’ouvrage danois: Den Danske Gradmaaling. 3e cahier. Avec
une planche. Copenhague 1883, p. 53/56.

5 Canin. Ueber die Lothablenkung in Danzig nebst Beobachtungen von Sternbedeckungen. A. N.
Nr. 3318, Bd. 139, 1896, S. 87/90.

7 Astronomisch-Geodatische Arbeiten. Verdffentlichung der Königlich Bayerischen Commission für
die Internationale Erdmessung. Heft 1, München 1896, S. 67/68; Heft 2, München 1897, S. 172/476; Heft 3,
München 1898, S. 146/421 und 238/240.

 
