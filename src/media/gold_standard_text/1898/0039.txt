 

a ee ee

 

vid | a

TER TI FIRE AR UE RT

Tl

rat

VIERTE SITZUNG

Freitag, den 7. Oktober 1898.”

Herr Faye, Präsident.
Gegenwärtig sind:

I. Die Herren Delegirten : Albrecht, Anguiano, van de Sande-Bakhuyzen, Bassot,
von Bodola, Bersch, Bouquet de la Grye, Bourgeois, Celoria, Darwin, Ferster, Gautier,
Guarducci, Haid, Hammer, Helmert, Hennequin, Hirsch, Kimura, Koch, Lallemand, Nagel,
Nell, Oberhoffer, von Orff, Preston, von Richthofen, Rosen, von Schmidt, M. Schmidt, von
Stubendorff, Tunakadate, Tinter, Westphal.

Il. Die Herren Eingeladenen : Deslandres, Haller, de Novales, Schlebach, Tripet,
Weitbrecht, von Zeller.

Eröffnung der Sitzung um 93), Uhr.

Der Herr Präsident ersucht den Secretär einige geschäftliche Mitthejlungen zu machen
und das Protokoll der 3. Sitzung zu verlesen.

Der Secretär theilt zunächst mit, dass Herr von Sterneck zu seinem lebhaften Be-
dauern durch Dienst-Geschäfte verhindert ist, an der Conferenz der internationalen Erdmes-
sung Theil zu nehmen.

Ferner hat Herr Graf d@ Avila dem Prasidenten bereits am 20. September geschrieben ,
dass, da diein den letzten Jahren in Portugal ausgeführten Nivellements-und Triangulations-Ar-
beiten auf den Azoren noch nicht völlig reducirt sind, er für diesmal den Besuch der Conferenz
unterlassen zu sollen glaubt. Uebrigens wird der Herr Graf d’Avila nicht versäumen den Bericht
über die Arbeiten auf den Azorischen Inseln dem Secretär für die Verhandlungen einzusen-
den. (Siehe Beilage B. VII.)

Ausserdem hat das Bureau die freundliche Mittheilung erhalten, dass Herr Ober-
bürgermeister von Rümelin, der schwer erkrankt ist, sein herzliches Bedauern ausdrückt,
durch die Erkrankung verhindert zu sein, die General-Gonferenz der internationalen Erd-
messung auch von Seiten der Stadt Stuttgart zu begrüssen. Er wünscht, dass die Versamm-

 
