pre

 

{rl

Ji lui

CR a a

 

VII." — Grande Bretagne—(Inde).

OS A SS SS
| | |

 

 

— 151 —

 

INDICATION DIRECTEURS
N° À LATITUDE [LONGITUDE É
DES POINTS Ei EPOQUE ET OBSERVATEURS
56. | Sullan, . 4556, 22 20° 8. 307 272° 10 134i 385 Lieut. M. W. Roge
ST Bil. 0.0 29 17 57 | 72 22 17 18 À ; Le ae
58 | Panchkot......... 2916 1,72 8.0 1875 id.
59 | Randa. 2.35.52... 23 1924 72151] 1875 id.
Kaimsir (Sutlej Se- a
Tes) a 20 2442 2 31 1876 id.
Kanda (Sutlej series) 29 27 42 | 72 19 44 1875 id.
Castern Sind Meridional.
Rojhra (Karachi
Lonple) 2.2. a, 24957 26% 70274 TE 1876 Capit. M. W. Rogers.
Sandohar (Karachi x“
Honslo) 2... 25 3 4 | 69 58 54 1876 id,
+ Pare 2... 24 52 56 © 3% 1876 id.
2:| Chiens, 2 @: 2458421. 695.2 2 18910 : | id,
> |Patatonk 7. 25 9 38 | 6945-22: 1876 id,
4 \=Narithal > =: 25.16.20. 695233 | 1876 id.
5 | Bhädi...... ne. 25 15 24 10 1158 1876 id.
6 | Hatodan., 25 29 35 | 69 49 45 .1876 id,
7\ Rupihar .. . ...... 23 27 283 10 219 1876 id.
8’: Kanakotri . ... 25 30. 16 | 1014739 1876 id.
9 | Mangtor.......... 25 3912 | 69 54 34 1877 id,
10. Bhitala 2... 25:38 A7 |-10 8 44 1877 id.
I Narhar 4. 25 50 58 | 69 54 20 1877 id,
12: Thakur 23 33700 3.2 2A 32,40 755 1877 id.
13 | Jeysulmere ....... 201 A5R | 69 51 24 1877 id.
14... Malar . ea ee 20 2.26 | 10.3 38 1877 id.
15 | Badhor.. .: 2... 25.59 49 70 17 3% 1877 id.
16: Ramsar.,....... .. 26 ld 11 | 69 5933 1877 id,
EP Sinaba.. 7 560 26-12 18 | 70 11 56 1877 id.
18 | Potanawäri ....... 26:23:31 , 00:08 32 1877 id.
19: | Joganali.......... 2625 870 350 1877 “14,
20.| Mande 22... 26 23 36 | 70 14 44 1877 id.
21] Samahu .. .....:.. 26 33 44 | 69 58 54 1877 id.
22.) Arrabhit.. £7.02. 26.34.41 10 9 32 1877 id.
23. Harna0. 2.0.2.2 26 43 55 | 69 56 35 1877 id.
24 | Dhanono.......... 26 45 12 | 70 1033 1877 :1d.
25 | Bandri.. 022.2. 26 54 44 | 69 49 59 1877-80 id.
26 | RAvilam 7.24 3. 26.52.2970. 220 1877-80 id.
27 MA ee, ı 27: 5 20 | 69 45 56 1880 id.
29. CA oe Sr + 57.120.008 1880 id.
20 MAEINSTA 2: 2659 517010 4 1880 id.
30. SIRSTA 0 0. 21.13 35 | 69583 32 1880 id.
SE | ASUS 0. cn. 27.10 32.10.10 59 1880 id.
32.1 Biirl. 2... 2... 8 27 22 47 69:00 20 1880 id.
33 | Parethal:. . >... ;.. 27.22 1% 10. 598 1880 id.
34) Kot... 223 21.25 61.10 4 20 1880 id.
35.) Chaukl ,,.. 27.34.18 | 09: 93 22 1880 Rogers, Lieut. Col. B. R.
Branfill.
364. Kharo....- =...... 2799 46 | 70 5.48 1880 Rogers. ;
37.1 Morgich: 2. 34. 97% 35 15. 1:10:14 29 1880 id.
38 | Trismigh.. ..- .. .: 27, 42, 24 | 0. op 1880 Rogers, Brantill.
39 | Thar Muhdri...... 27 41 33 1-69: 40 23 1880 Branfill. ;
40 | Kivariwaro,...-.~. 27 45 57 | 69 49 58 1880 id.
Al, Mark... 00.00 27 51 17-| 69.43 26 1880 id,
AS | NAT ee 27 54 35 | 69 48 50 1880 id,
42 | NUEDIR 6,2. . 1880 id,

 

 

 

 

 

 

 

 

 

27 54 36 | 69 59 31

 

 

 

 

 

   

INSTRUMENTS

 

 

REMARQUES

 

id.
id,
| ; id.

id.
id,

Barrow 24! n° 2.

id. :
Barrow. 24" n°:2 et
Ter 8. 2m >
Barrow. 24 ne 2

di res
Barrow 24” n° 2 et
Tot S; 24) Dee
Ty 60 Ss 24 nok:

id.

id.

id,

id,

ar RE

 

 

 

a oe ee

rs.

 
