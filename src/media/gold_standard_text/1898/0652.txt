 

 

eo

3 cartes), ainsi qu'un Rapport sur les mesures de pendule exécutées dans les
dernières années (ibidem, Annexe A. IT).

En 1888, à Salzburg, M. Helmert a fait des propositions pour compléter
le réseau des points astronomiques (voir C.-R. de 1888, p. 16 et 19-24) du réseau
géodésique européen et sur les déterminations de déclinaisons d'étoiles à tirer
des observations de latitude au 4er vertical 416 17).

Dans le rapport suivant, présenté à la Conférence générale de Paris en
1889, le Bureau central a rendu compte des recherches préparatoires qu'il a
entreprises sur le mouvement de l'axe terrestre (voir C.-R. de 1889, p. 20, 21) et
M. Albrecht y a communiqué un travail sur le méme sujet (voir ibidem, An-
mexe A. IX).

Le Directeur a lu dans cette méme Conférence un nouveau « Rapport
sur les déviations de la verticale » (ibid., Annexe A. VID), ainsi qu'une notice «Sur
les mesures de pendule » (Annexe VII).

On y a reçu communication d’un travail de M. Beersch sur la jonction
des mesures d’arc russo-scandinaves avec les mesures anglo-françaises. (Voir
Annexe A. XI.)

Dans la session de 1890 à Fribourg, le Bureau central a continué à rendre
compte des calculs de l'arc de longitude de Struve (voir C.-R. de 1890, p. 60, 61),
dont il avait déjà parlé l’année précédente à Paris (voir C.-R. de 1889, p. 21).

1] a soumis un rapport de M. Albrecht « Sur les observations de latitude
à Berlin, Potsdam et Prague ». (Voir C.-R. 1889, p. 61-68.)

La Commission permanente ayant décidé le 21 septembre 1890 à Fribourg
l'expédition à Honolulu, confiée à M. Marcuse, pour y entreprendre des observa-
tions de latitude conjointement avec plusieurs observatoires d'Europe, le Bureau
central a rendu compte à Florence, dans la session de 1891, des mesures prépa-
ratoires pour cette expédition qui, partie le der avril 4891 de Berlin, est arrivée
le 8 mai à Honolulu, après s'être entendue à Washington avec M. Preston du
Coast and geodetic Survey qui a bien voulu coopérer avec l'Association aux ob-
servations de Honolulu dans une station voisine et indépendante. M. Helmert a
pu donner déjà quelques premiers résultats généraux de cette expédition. (Voir
CEE dé 1801 p. 15-19)

M. Helmert à présenté à Florence un rapport sur le choix d’un zéro com-

 
