 

MER

 

pu qe

FERNER DRE LOU OL à

LI [AL

997

Les diagrammes ci-dessus (fig. 15) représentent respectivement les variations moyennes
de longueur d’une paire de règles à l’état naturel, et les changements correspondants de la
température, de l’état hygrométrique et de l'humidité absolue de l'air de la salle.

Les variations de longueur des

règles (diagr. [) présentent la même allure que celles
de ’humidite relativ

e (diagr. I). Ce parallélisme serait encore plus accusé si, par le calcul,
l’on corrigeait préalablement de l'influence de la température la lonyueur des règles

pour en
ramener les variations à ce qu’elles seraient dans

une atmosphère de température constante,
égale à 15° par exemple. Ces variations ne paraissent, au contraire, liées en rien à celles
de l’humidité absolue (diagr. IV).

On peut donc dire que les expériences en question n’ont fait que confirmer les lois
énoncées par le colonel Goulier.

Paris, le 31 décembre 1898.

 
