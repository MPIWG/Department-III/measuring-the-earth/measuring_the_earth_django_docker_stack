i

|
|

 

7 413
? 414

tu

IHM ER OB

ART

bul

 

 

 

Re

Vl.— France et Algérie.

 

 

 

 

INDICATION

 

 

 

 

 

 

 

 

 

 

 

 

 

 

|
N° re ON i DIRECTEURS ee |
| DES POINTS E In EPOQUE ET OBSERVATEURS INSTRUMENTS | REMARQUES
|
Triangulation de la Méridienne de Bayeux. (Partie Nord — Suite.)
406 | Mont Pinson ...... 482.58. 206.1 172 21 20: Cereles répétiteurs | Mémorial du Dépôt
407 | St. Jean des Bai-| 49 5 41 | 16 41 22 avec 4 verniers qui |; as
Sans. tre | 1818-19 Capitaine Delcros, donnent chacun la |
4082 Bayeux. = | 49.10.99.) 10 537.3 ; lieut.ts Fessard et Poudra. | lecture de 20" cen-
409) Rént. 0. Ag 782 sl : tésimales. — Pour |
chaqueangle furent |
| | exécutées 20 À |
titions.
Triangulation de la Méridienne de Bayeux. (Partie Moyenne.
215 | Bressuire. ....... Déjà mentionné. Id.
410 | St. Pierre du Che- | i
D 46° 41:48) 169% 57. AA |
214 | St. Michel Mont Mal-
huss 8... 46 49 56 | 16 46 46
All | St.AubindelaPlaine 46 30 31 | 16 41 34
412 | St. Michel le Cloucq.| 46 28 54 | 16 54 51
Courson :.::... 46 14 20 | 16 50 5 Capitaines Delahaye et Be-
La Grange St. Ge- » 1818-24, raud, lieuten. Peytier, Id. |
Ris Pr oc 40221 57 | 7 14 16 : Faulte et Hossard.
45 | Asure.. ea 29 8 0,1059 |
416 | Raimbault......... AG 10 32 | 1, 2 7
417 | Les Souverts....... 416 0730 | 12 746
418 | Les Educts ........ 46) 0) Woe | 10 2150 |
230 | Rouillae PA mentionnes,
| |
Triangulation de la Méridienne de Bayeux. (Partie Sud.)
252 | Chadenac .... \ Id.
200. | Epargne. à... Es
20! | Soubran =... =
259 | Chantillae = 5222 7.
202.8. Sam. 2.2.2... , Déjà mentionnés.
265 | St. André de Cub-
ZAG? er
269 | Blanquefort .......
260:| Montagne 25... | | 1823-24 Capitaine Mareuse, Id
419.) Bouliae. 2. | 44° 48 52"| 17° 9 30" 7 lieutenant Debuissy. :
ADO Crée | 44 46 30 | 17 1551
421.’ baunay.... .. | 44 43 19 | 17 40 58
4222| Biros. 2.02.22. A4 35,46 AT 89 7
423 | Escassefort ........ 44 32 58 | 1154 0
424 | Romestaing........ | 42.95.5117 3054
12 Xammtrailies nn.) Déjà mentionnés, |
| |
. Triangulation Méditerranéenne.
291 | Grand Montagné. | ld
293 | Les Houpies....... Déjà mentionnés. |
396 | Tabouret .......... ae oo. | oe à |
425 | Saintes Maries..... BOT el Sep somone Major Delcros,
426 | Bagnet....... | 4344.5 | 28 5.40 | lieut.® Rembault et Pissis. id. |
427 | Aignes Mortes ..... | 43 347 | 21 50 50
428 | Pic St. Loup......| 43 46 45 | 21 28 27 | |
|

RI IR Lem me lu

nr

 
