 

MV TS if |

MCE se oer =

 

a) LL

Nu RT

ea

Hwan

TUNER T

Wir yy

105

terminé que la latitude et l'azimut; les longueurs linéaires reposent provisoirement sur une
valeur moyenne des côtés, résultant de sept bases situées dans le voisinage de la chaîne
méridienne. Lorsqu'on connaîtra les valeurs définitives des longueurs et des cæfficients de

dilatation pour les règles de base de l'appareil autrichien, les longueurs de ces sept bases
recevront probablement encore de légères corrections.

Les erreurs de clôture des équations de Laplace sont satisfaisantes.

Enfin on a commencé les calculs pour rattacher la mesure de l’are de latitude Russo-
Scandinave à la mesure de l’arcde longitude sous le 52 parallèle. Dans ce but on a calculé à nou-
veau les deux lignes les plus rapprochées, de Varsovie à l'Est, de l'arc de parallèle mesuré parles
Russes, en utilisant autant que possible les résultats des calculs contenus dans le 47e volume
des « Sapiski de la Section topographique de l’État-Major russe ». Il s'agissait des lignes
Varsovie-Grodno et Grodno-Bobruisk, qu’il fallait encore traiter d’après notre méthode. On a
soumis à une critique serrée les déterminations astronomiques de Grodno et de Bobruisk, aussi
bien que les rattachements trigonométriques des points astronomiques au réseau principal
de triangles. Aux deux endroits, il est vrai, l’azimut a été observé dans un auire point que
ceux où la latitude et la longitude ont été déterminées ; cependant les stations astronomiques
sont toujours si rapprochées, qu’on pouvait rapporter les trois éléments astronomiques au

même point, sans avoir à craindre une influence sensible ou génante des déviations locales de
la verticale.

Les deux équations de Laplace pour Varsovie-Grodno et Grodno-Bobruisk montrent
malheureusement des erreurs de clôture de + 15 et de + 19”. Toutefois on pourra encore
réaliser assez exactement le transport de la latitude à partir de Varsovie jusqu’à Belin ou
Nemesch ; ce sont les deux points astronomiques les plus rapprochés de la mesure de l'arc
de latitude qui coupe l’arc de longitude de Grodno-Bobruisk presque au milieu et qui a avec
lui trois triangles en commun.

A. BarscH. L. KRüGER.

Dans l’année 1899 on continuera ces calculs pour le méridien de Vienne et en Russie ;
et en outre on fera la publication des résultats pour les points rattachés à Bonn, lorsqu’on
aura Obtenu la jonction des points astronomiques près de Paris, pour laquelle le Service
géographique a bien voulu promettre les données nécessaires.

2. Sur la déduction du mouvement de l'axe terrestre, par M. le professeur Albrecht,
le Bureau central a publié, au commencement de 1898, le « Rapport sur l'état des recher-
ches des variations de latitude au mois de décembre 1897, par M. Albrecht. Avec une plan-
che. Berlin, 1898 ». Ce rapport a été distribué dans la même mesure que les Comptes-Rendus.
Un extrait de ce rapport a paru dans les Astron. Nachr. No 3489, sous le titre: « Courbe
décrite par le Pôle nord de l’axe terrestre pendant la période 1890,0-1897,5. »

Vers la fin de l’année on a rassemblé les matériaux pour un nouveau rapport qui
doit être mis prochainement en ouvrage. Comme les années précédentes, l’Institut géodési-
— 14

 
