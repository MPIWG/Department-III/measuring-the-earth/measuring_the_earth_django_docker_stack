 

182

 

 

 

a
e x
Bonn + 08 — 573
Mannheim — 4.0 — 1.1
Strassburg — 1.6 — 2.4
Rigi + 14.5 — 42
Simplon + 9.1 — 5.2
Mailand — 19.6 + 3.5
Turin — 10.1 + 35.6
Genua ee 0. 0)
Nizza — 18.0 + 0.9
Gino, — 3.7 — 1.4
Gäbris + 4.7 + 0.2
i fander — 0.8 — 18.8
din — 9.1 + 4.0
Bern + 2.2 + 4.6
Neuenburg — 135.5 ar 13.8
Genf. — 2.8 + 0.0
Talmay — 1.2 — 5 : (
Paris — 2.5 + 1.7
Brest — 1.9 + 8.1.

Hervorzuheben sind aus dieser Zusammenstellung die grosse relative

Lothabweichung in Breite von Mailand gegen Simplon und die sehr grosse
relative Lothabweichung in Länge von Turin gegen Mailand, Genua und Nizza.

Hiermit sind die Rechnungen, die im Anschluss an den Punkt Bonn
der Europäischen Längengradmessung nach Süden und nach Südwesten hin
ausgeführt sind, vorläufig abgeschlossen worden. Es besteht die Absicht,
bevor zu einer Ausgleichung dieses Materials geschritten wird, die Ergeb-
nisse für die einzelnen Linien in der Form, wie sie in der Längengrad-

messung in 52° Breite angenommen ist, zu veröffentlichen. Auf diese Weise

würden dann Aenderungen, die sich noch nachträglich für die von uns

benutzten astronomischen und geodätischen Grundlagen ergeben sollten,

leicht berücksichtigt werden können.

Die Lothabweichungsrechnungen in Wiener Meridian südlich
vom Punkte Schneekoppe der Europäischen Längengradmessung in 52°
Breite sind gleichfalls gefördert worden. Hierbei wurden wir durch Herrn

Oberst von Srterneor wesentlich unterstützt, indem er uns besonders die

Winkelmessungen für den trigonometrischen Anschluss der Laplace’schen

Punkte Laaerberg bei Wien und Pola (Sternwarte) an die Dreiecke der

bereits ausgeglichenen und veröffentlichten Wiener Meridiankette mittheilte

aa MT Melk |. dd ul

ah iki

 

Deus sam hi |

 

 
