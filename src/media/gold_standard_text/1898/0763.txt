De So

5 Teen OS |

EMAIL a

ii iil iT

   

  

 

a

IV. — Danemark.

 

EPOQUE

DIRECTEURS
ET OBSERVATEURS

INSTRUMENTS REMARQUES

 

 

 

 

 

 

 

 

Ne INDICATION a. :
DES POINTS UDE |LONGITUDE
1 | Copenhague....... 50° 40’ 43”| 30°14’ 48”
Tour St. Nicolas.
la Bases ie 55829 2,50 109 4
Terme Nord.
1b | Base: 00 59 3741 530 14.50
Terme Sud.
le | Frederiksholm ....| 55 38 56 | 30 11 59
ld | Copenhague....... 55 40 48 | 30 14 9
Egl. St. Pierre.
le | Frydenhöi......... 55 87.971 | 07 50
JE Valois se. 58 0. BS) 40 SO) BO. 7 22
lg | Kongelunden...... 85 33 40) | 30 18, 58
2 | Snoldelev......... 55 84116 | 29 47 49
3 | Julianehôi ........ >46 31, 29 37018
4 | Mörkemosebjerg...| 55 38 7 | 29'20 52
SE Véirhoi 5d 47 36 | 29 3 40
6 | Klöveshöi......... bo 31 1112905
2. | Befsnäs..... 2... 59 44 10,1 28 34 37
8 | Bögebjerg ........ 5933240) 2821 33
9 | Dyrebanke........ 22 5275, 4
10 | Skamlingsbanke...| 5525 8 | 27 13 49
Al Troldemosebanke..| 55 43 45 | 27 36 9
122 | Dywrets. oo ee BD 30) 3 |) 28 13 au
13 | Eiersbavnehoi..... bo 58.38 | 27 29 44
14 | Agri Baynehéi....| 56 13 48 | 28 12 5
15. 2 Bysnet. 2.202 00% 50,22 2) | 2738 14
16 | Hegedal Bavnehöi.| 56 30 19 | 28 13 49
17 Howhöl.......... 56 38 4) | 277 39) 56
18 | Rold Baynehôi....| 56 46 3 | 27 29 15
19 Muldbjerg ...... 50.54 31 2055 30
20 | Jägerdalshöi...... 66 55 33 127 2849
21 Storskoven........ om 02 2.102094 39
22 sen anne Laon IT 22 27 28 52
23. Tegihol......:.. 12922, 273812
= 24 Blade. oe Di 2a 2 ee on ie on
29 | Skagen... ........ 21 43 46 | 28 10.18
26 | Knivsbjerg........ >> 836,21 025
27 | Leerbjerg......... oo 130 | 27 55.58
28. | EySabpel. 54 54 13 2740 3
29 | Fakkebjerg....... 54 44 23 | 28 21 53 ||
30 | Sprenge.......... 54 27 14, 2040 9
31 | Hohenhorst....... 5415 1.025053
32 | Bungsherg........ 54239) | 2825 1)
33 | Segeberg ......... 53.50. % 027 58 54
34 Lübeck... ........ 53.92 1.728 20.58
35 | Fichede. . ..... 33 48 22 aA lei
36 | Base de Braack...| 53 38 27 | 27 52 30
Terme Nord.
Bl Base de Braak....| 53 35 44 | 27 55 15
Terme Sud.
38. | SI€k. occ ees seen eke ones
38a | Bornbeck......... NES Se I 1283 0
39 Hohenhorn........ 53 28.232128 05

Tann (DRIN ELU Om

Nile) [ae

 

1837-38-40 | Nyegaard, Perersen, Ne- | Théod. Ertel de 15 p. | Publiés dans le «Den — 5,

1838
1838

1838
1837

1838
1838
1838-39
1838
1839
1842-43
1842-43
1843
1843
1844
1844-45
1846
1846-67
1847-67
1867
1868
1868
1868
1868
1868
1870
1869
1869
1869
1869
1869
1869
1845-46
1845

1818-46
1818-41-44

1817-18-19
1817-18
1817-18-41

1817-18
1817
1819

1821-22
1822

1818-22
1822

1818-24-25

hus. et théod.Reichen- | 7 Ga
bach de 12 p. dre, Vol. 1, 2 et 8.
Nyegaard, Petersen.
id. Théod. Ertel de 15 p.
id. Les points de la
id. Théod. Reichenb. de Base.
12 p.
id.
id.
id.
id.
Petersen.
Nyegaard.

Théod. Ertel de I5 p.

id.
id. Prusse, B. N. 276.
Nyegaard, Meldahl.
Thalbitzer, Meldahl.

Meldahl.

 

id.
id.
ide
id.
id.
id.
id.
id.
id.
id.
id.
id.

Theod. Ertel de 15p.

 

Nyegaard. ia Théod. Ertel de 15 p. |) Prusse, B. N. 274.
Theod. Ertel de 15p.
et théod. Reichen-

Caroc, Nyegaard.
id, bach de 12 p.

 

Schumacher, Caroc. Théod. de 12 p
id. : :
Schumacher, Caroc, Nye- | Theod.Erteldel5p., || Prusse, B. N. 80.
gaard. théod. Reichenb. de

12p. etthéod. de 8p.

Schumacher, Caroc.

Schumacher. Prusse, B. N. at. >
Caroc. ‘ ecklembourg, N. 40.
Schumacher. Theod. de 12 p.

Schumacher, Caroc.

Schumacher, Caroc, Ne- | Théod. de 8 et 12 p. | Mecklembourg, N.42..

hus.
Schumacher, Caroc. Théod. de 12 p. etde
sp.
Caroc. Theod. de 8 et de | Mecklembourg, N.39.

2 PD:

 

 

 

 
