 

 

Herstellung ein Mitglied dieser erlauchten Kommission die erste Anleitung gegeben hal, und
wovon eines der neuest erschienenen Blätter, das Blatt Cannstatt, hier auf dem Tische des
Saales niedergelegt ist, mit der Bemerkung, dass die Herrn Mitglieder nach Belieben davon
Gebrauch machen mögen. »

ee ——

INierauf erhebt sich der Präsident der internationalen Erdmessung, Herr Faye, und
antwortet auf diese freundliehen Begrüssungsreden :

« Zunächst erlaube ich mir den Herrn Kultusminister zu bitten Seiner Majestät dem
König den ergebensten Dank der Versammlung auszusprechen für das hohe Interesse, welches
derselbe an der Conferenz auszudrücken geruht hat. Sodann danke ich dem Herrn Minister
für die liebenswürdigen Worte, mit welchen er die Versammlung empfangen hat. Und endlich
gedenke ich mit Freude der Stadt Stuttgart, welche uns von unserem Aufenthalt vor 21
Jahren bei der damals hier tagenden Gradmessungs-Conferenz noch in angenehmster und
schonster Erinnerung geblieben ist. »

IF Auf Vorschlag des Prasidenten, werden zu Viceprasidenten der Conferenz prokla-
mir die Herren Professor Hammer aus Stuttgart, General von Stubendorff aus St. Peters-
burg und Professor G. H. Darwin aus Cambridge.

Die genannten Herrn nehmen mit Dank ihre Wahl an.

tei

Der Präsident ertheilt dem ständigen Secretär das Wort zur Verlesung seines
> int
jerichtes.

Der Secretär giebt zunächst folgende erklärende Einleitung:

Obwohl die heutige Sitzung wesentlich eine Festlichkeitssitzung ist, in welcher die
wissenschaftlichen Geschäfte den zweiten Rang einnehmen, so habe ich den pjerrn Präsi-
denten doch ersucht, wenigstens dem Sekretär für die Verlesung des Berichtes über die
Thätigkeit des Vorstandes der internationalen Erdmessung das Wort zu geben, hauptsäch-
lich in der Absicht, schon am ersten Tage unserer Vereinigung die nöthigen Maassregeln
ergreifen zu können, um die unentbehrlichen Kommissionen zu ernennen, und zugleich
= Sie zu bitten, hier auf zwei Listen, deren Formulare ich habe vorbereiten lassen, Ihre

Namen und Ihre Adressen in Stuttgart einschreiben zu wollen, damit das Bureau im Stande

ist, mit den Herren Mitgliedern hier in Stuttgart zu correspondiren. Während diese Liste
: zivkuliert, erlaube ich mir, den kurzen Bericht des Vorstandes zu verlesen und zugleich
Ihnen einige bisher eingetroffene, telegraphische oder briefliche Nachrichten mitzutheilen,

pearl) vil

mal ART

Es ist allerdings in unserer internationalen Gesellschaft Sitte, die Verhandlungen und
alle wesentlichen Aktenstücke in beiden Sprachen zu publizieren ;, da aber die französisch
verlesenen Dokumente von den übrigen Nationalitäten wohl begriffen werden, während das
Reciproke nicht in gleichem Maasse behauptet werden kann, habe ich, um den Meisten ver-
ständlich zu sein, den Bericht vorläufig französisch abgefasst, indem ich den deutschen
Text für den Druck der Verhandlungen vorbehalte.

RT

+ 2 EEE sal EE Re er ET
een a re eG

 
