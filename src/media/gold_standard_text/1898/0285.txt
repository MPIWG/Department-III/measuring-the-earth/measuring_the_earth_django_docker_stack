 

 

N

AMI

GH oa

PATENT TN

267
auf der Krimlandzunge) — der nach Beobachtungen aus dem Jahre 1837 im allgemeinen rus-
sischen System mit Walbeck’s Elementen eine Lothabweichung in Breite von nur — 40
hat — und in Länge gegen die Sternwarte in Nicolajef bestimmt wurde. Die Lothabwei-
chungen in Länge beruhen sämmtlich auf telegraphischen Bestimmungen; für die geodä-
lischen Positionen ist hier Clarke's Ellipsoid von A880 zu Grunde gelegt.

Im allgemeinen lassen sich diese Lothabweichungen, besonders die sehr beträcht-
lichen südlich des Gebirges, durch die gleichzeitige Wirkung des Gebirges und des schwarzen
Meeres erklären. Hiernach scheinen die Gebirge der Krim eine Ausnahme von den sonst ge-
machten Erfahrungen zu bilden, nach denen auf einen Massendefeet unter den Gebirgen
geschlossen wird. Indessen müsste dieser Schluss erst noch durch Altractionsberechnungen
bestätigt werden.

Die stärkste Aenderung in den Lothabweichungen, mit Rücksicht auf die Entfernung
der Punkte voneinander, findet für die in Länge zwischen Jalta und Kekenens statt, die im

Betrage von 29" bei nur 13/20” geodätischer Längendifferenz = der Amplitude ausmacht.

Zum Schluss leitet Herr Kuhlberg für dasselbe System noch die Lothabweichungen in
Länge und in Breite für Rostow am Don (Kathedrale) ab und findet für diesen Punkt

die Lothabweichung in Länge = — 0.6 oder — 0'5,
je nachdem von Nicolajef über Theodosia oder direkt nach Rostow übergegangen wird, und
die Lothabweichung in Breite = — 41 oder — 43,

je nachdem über Theodosia oder von dem östlichen Endpunkt der Jekaterinodarer Basis aus,
der schon geodätlisch fesigelegt war, gerechnet wird.

Eine sehr interessante und wichtige Arbeit ist ferner die des Herrn Generals Pomeran-
tzef «Ueber die Figur des Geoids im Ferganagebiet» in Centralasien2!. Das Ferganagebiet ist
ein Theil der tief eingeschnittenen Thäler der Flüsse Kara- und Syr-Darja und liegt zwischen
40°15’ und 41°15’ Breite und zwischen 39°30’ und 42°45’ östlicher Lange von Pulkowa. Dieses
Thal hat sehr nahe die Gestalt einer Ellipse, deren grosse Axe in der Richtung des Parallels ver-
läuft und 250 km. lang ist, während die kleine Axe eine Lange von 110 km. hat. Im Norden liegen
Gebirge mit einer mittleren Hohe von 2500 m. bis 3500 m., während sich im Süden die Alai
und Transalaiberge und die gewaltigen Hochplateaus des Pamir und des Hindukusch erheben.
Da hier ausserordentliche Lothstörungen zu erwarten waren, so wurde Herr Pomeruntzef,
der damals Director der Sternwarte in Taschkent war, beauftragt, Bestimmungen dieser Art
auszuführen und die Beziehungen ihrer Resultate zu der Orographie des Landes aufzusuchen.
Bei der Erledigung dieser Arbeit konnte von der schon vorhandenen Triangulation und von

21«Sapiski u. s. w.», Bd. LIV, St. Petersburg 1897. Ueber die Figur des Geoids im Fer-
ganagebiet von General-Major Pomerantzef. Mit einer Tafel und einer Karte. S. 79/120 (Russisch) ;
vergl. auch: Verhandlungen in Lausanne, 1896, S. 309/310, und Bulletin astronomique. T. XIV,
Paris 1897, p. 479/480,

 
