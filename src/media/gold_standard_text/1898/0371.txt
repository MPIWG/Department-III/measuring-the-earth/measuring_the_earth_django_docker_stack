 

353

fan paar ees

 

 

 

 

 

Méthode et instrument
Année et mois | TITRE DE LA PUBLICATION REMARQUES
employé
Horizontalwinkelzwischen Po- Die Messung dieser beiden Azi-
laris und dem Zielpunkt. 1897 i ae mute ist bei Gelegenheit des
Theodolit (Universalinstru- Aug.-Sept. Noch nicht publizirt. « Astronomischen . Nivelle-
ment) von Breithaupt, 21 cm. | ments auf dem Tübinger Me-

ridian » ausgeführt worden.
| Es sollte damit zunächst das

 

den.

1898 August

 

= |Polaris at various hour angles 1890 May Not published. Computed since last report.
BE 90 cm. dir’n theodolite No 115
» 1890 June > Computed since last report.
Polaris at various hour angles | 1894 August » Secondary azimuth
1 25 cm. repeating theodolite
3 No. 82.
Polaris at various hour angles 1895 June »
50 cm. dir’n theod. No. 5.
Polaris at upper and lower cul- » »
minations. Mer. Tel. No. 15.
Polaris at U. and L. culmina- 1895 July »

tions. Mer. Tel. No. 2.

 

lene

— 45

I RL AT

 
