 

368

d’entre eux à l’Institut géodésique, entretient ceux de Kiel, Wilhemshaven et Heligoland ; les
chiffres obtenus dans ces trois stations n’ont pas encore été publiés.

D'un autre côté, l'administration Royale Prussienne des travaux publics a placé 7
marégraphes sur la côte Ouest du Schleswig-Holstein et à l'embouchure de l’Elbe. Ils sont
tous du système Seibt-Fuess. La ville libre de Hambourg a également établi dans la basse
Elbe sept marégraphes, enfin à la demande d’une congrès géographique, le Gouvernement a
installé un repère fondamental dans la Baie de Jade sur une couche géologique stable. Ce
repère a été rattaché par un nivellement aux marégraphes de Wilhemshaven et de Bremer-
haven.

L'Institut géodésique royal de Prusse a publié les ouvrages suivants sur les marégraphes :

Niveau moyen de la mer à Swinemünde (Berlin 1881)

» a Travemünde » 1889)
» a Swinemünd » 1890)
Die neueren Deutschen Mareographen sind etc. . . (Berlin, Nov. 1891).

Der selbstregistrirenden Universalpegel zu Swinemünde (Berlin, Nov. 1891).
Untersuchungen über den selbstregistrirenden .. . 2u Swineminde (Berlin, juin 1895).
Der Curvenzeichnende Controlpegel etc. ... Berlin, février 1894).

FRANCE

Les ports où ont fonctionné des marégraphes appartenant au Service hydrographique
sont au nombre de onze en France.

On en a installé temporairement un à Alger et un autre en Cochinchine.

Le maréoraphe installé à Brest a marché pendant 49 années ; celui de Cherbourg,
pendant 48 ans; Saint-Malo-Saint-Servan et le Havre, pendant 45 ans. On a ensuite les chiffres
suivants : 35 années pour Rochefort ; 33 pour Saint-Nazaire; 21 pour le marégraphe ins-
tallé au fort Boyard, dans la rade de l’île d’Aix; 19 pour celui du Socoa, à Saint-Jean-de-
Luz; 15 4 Toulon; 14 au fort d'Enet, à l'embouchure de la Charente; 11 années à la Ro-
chelle ; 9 années à Alger el enfin 3 années à Marseille.

Le Service des Ponts et Chaussées, qui s’est chargé depuis l’année 1884 de l'entretien
et de la surveillance du marégraphe de Cherbourg, en a fait fonctionner d'autre part tempo-
rairement dans la Loire et la Gironde.

Enfin le Service du nivellement, que dirige M. Lallemand, a fait placer 11 médima-
remetres sur nos cotes.

Les marégraphes du Service hydrographique, qui sont tous du systéme Chazallon,
ont été installés en vue de la prédiction des hauteurs de la marée. Indirectement ils four-
nissent le niveau moyen de la mer et un contrôle de la stabilité du sol sur lequel ils ont été

 

Hbdi@i4 1 Maiiltds a do aca

1184011188

1111111884

Luk

to |

nehm a, blabla dtl arid eu

EIERN NN

 
