926
Dans ces conditions, j'ai pensé qu’il y aurait intérêt à revenir, pour la compléter, sur
ma communication de 1892 et à donner un compte-rendu plus détaillé des expériences de
l’'éminent colonel, avec la description des méthodes et des instruments employés, une discus-
sion complète des résultats numériques obtenus et une figuration graphique plus expressive
de ces mêmes résultats.

Tel est le but de la présente notice.

I. — CAUSES DES VARIATIONS DE LONGUEUR DU BOIS DES MIRES.

Trois causes principales paraissent influer sur les variations de longueur d’une mire,
ce sont :

1o Le temps écoulé depuis l’abatage et la mise en œuvre du bois;
90 Les changements de |’état hygrométrique de l’air ambiant ;
3° Les variations de la température.

Par suite de l’oxydation de la séve, un bois se rétracte lentement et progressivement
à mesure qu'il vieillit, jusqu'à ce qu'il ait atteint une limite que nous appellerons l’élat
normal pour une temperature et une humidité déterminées.

Quand le bois est parvenu à cet état, on dit qu’il est sec. Il ne parait plus alors étre
influencé que par la chaleur et par l'humidité ambiantes.

La contraction due à l'insuffisance du temps écoulé depuis labatage peut atteindre,
en quelques mois, plusieurs dixiémes de millimetre par metre.

En étudiant les opérations du nivellement de Bourdaloué, le colonel Goulier a con-
staté, par exemple, que la mire d’un opérateur avail diminué progressivement de longueur,
du mois de mai au mois de septembre de la même année, par rapport à la mire d'un autre
opérateur. En quatre mois, cette diminution avail atteint deux dirièmes de millimètre par
mètre. Les deux mires s’étant trouvées dans les mêmes conditions atmosphériques, ce résullat
ne pouvait tenir qu'à une dessiccation de la première mire, qui était sans doute en bois vert.

Un résultat analogue a été constaté pour la mire d’un troisième opérateur.

Les effets déterminés sur les bois par les variations de l'humidité et surtout de la
température se produisent plus rapidement.

Toutefois, c’est seulement au bout de deux à trois heures qu’une mire possède bien
la température de l'air ambiant.

Quant à l'humidité de l’atmosphère, il lui faut parfois plusieurs semaines pour faire
sentir son effet sur la partie centrale d’une mince règle en bois.

à. Minuit ana a

 

 
