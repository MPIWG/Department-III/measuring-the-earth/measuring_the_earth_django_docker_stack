   

   

a

AVI. — Russie

|

INDICATION 5 DIRECTEURS |
N° DES POINTS LATITUDE LONGITUDE| EPOQUE ET OBSERVATEURS INSTRUMENTS | REMARQUES

 

 

 

Triangulation de Bessarabie.

 

1 | Staro-Nekrassowka. | 45099! 9" 46°35! 37"! | Publiés dans « L'arc
2 Tabl 2... 45 20 24 | 46 28 40 | bee ae
ORDRE 0 45 22 20 | 46 25 33 | |Glaciale» Par J. G.
4 | Saphianowha ...... 45 24 36 | 46 32 25 | | W. Struve, St. Pe-
5 | Kairaklia.:........ 45 28 47 | 46 26 32 | ee Ae
6 | Katlabuch-Suchoi .. | 45 29 37 | 46 34 49 oe |
7 | Taschbunar II..... 45 33 59 | 46 30 20 | |
8 | Katlabuch......... 45 36 47 | 46 35 53 | |
9 Marakurt.......... 45 37 56 | 46 23 33 | |
10 | Pandaklia......... 45 47 0 | 46 34 45 |
1 | Bolerad........... 45 47 42 | 46 18 59 |
121 Tarsklia...:...... 45 55 2 | 46 25 29 | |
13. Kamboli.:......... 45 58 43 | 46 11 8 | | |
14 | Baurtschi......... 46 7 6 | 4622 7 = = |
15 | Malojaroslawetz- | co
kaja...... ao 46 5 25 | 46 38 33 ; on
16 | Kulmskaja......... 46 15 47 | 46 41 13 a So i
17 | Baschkalia........ 46 19 32 | 46 28 29 ao
18 | Nesselrode......... 46 27 55 | 46 46 32 5 | 8 |
19 | Nikolaiewka....... 46 20 40 | 47 4 52 m | AE |
20 | Mowo-Kauscani .. .. | 46 37 2] AT 7185 Ss + = |
21 | Dschamana. ...... 46 47 27 | 46 52 32 19 z 2 9
ee MOROI 7. 6 464 1,1632 4 = S HE |
23 | Ssurutscheni....... 47 4 1 | 46 19 23 © Ss eS |
24 | Wodolui........... 47 125 | 46 44 12 2 a |
Co APS .. ..... . 97413 47.18 41 ? Po
26 | Peressetschino..... 47 15 54 | 46 27 23 = go |
27 | Ziganeschti........ 47 19 8 | 46 12 48 2 aa |
28 | Bologan..:........ 47 27 41 | 46 29 58 | = a |
29 | Sagaikani......... 47 29 40 | 46 11 15 33
: 30 | Rospopeni......... 47 43 33 | 46 18 14 Zs |
31 | Tschutulesehti.. A7 45 19 | 45 59 25 =
32 | Unkiteschti........ | 47 532471 46. 9:40
By... 47 50 6 | 45 36 49
34 | Wodeni............ Al 99 26 | 45 52 42
| Rottos. ....... 4159,17, 45.29 17
36 | Boksano........... 28 72014544 11
31) IyEnoWwo.......... 418 10 0 | 45 18 22
Po BONY 4819 5, 153231
39 | Rotunda........... 48 14 30 | 44 59 5 ;
AO) pa 0 48 22 26 | 45 10 46 |
41 | Gwosdautzi........ 48 25 48 | 44 58 43 |
42 | Britschani......... 48 20 42 | 44 47 10

 

 

 

 

 

 

 

 

Triangulation de Volhinie et de Podolie.

 

 

e à Publiés dans « L'are
nn = Déjà mentionnés. Bee du Méridien » de
43 | Woltschenetz ...... 48028! 9"| 440351 0" z ag =
44 | Sagorjane......... 48 44 29 | 44 47 6 5 2 | sen = S
45 | Ssuprunkowzi..... 48,45 8 | Ad 2747 2 S® a aor =
46 | Karatschkonzi..... 48 53 47 | 44 12 46 7 oe ee | &
47 | Hanowka.......... 48 55 43 | 44 31 28 2 en Sa
Tschernowody ......49 8 3|44 1755 & 5 Dome ©
Baranowka........ 49 853 | 44 38 25 3 RE
Feltschin.......... 49 19 46 | 44 20 50 Boerne
Kriwotsehinzy...... 49 29 4144 8 28 Oe ee

 

 

 

 

Hall Md been done ab demerit

erh de rd

ine nme a a la A do aan un
