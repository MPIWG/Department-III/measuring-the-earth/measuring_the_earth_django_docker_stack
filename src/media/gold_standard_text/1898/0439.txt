 

|
|
!

 

AQU ap Emil QU

ll

N RER gi

409

 

Une troisième expérience est en ce moment en cour
et Mi-Amiata (115 kilomètres). On écrit que le m
d’apercevoir la lumière du projecteur, mais cependant
gaz et le fonctionnement de tous les appar

s d’exéculion entre Mt-Senario
auvais temps n'a pas encore permis

on à constaté que la production des
eils se font très régulièrement.

On est donc autorisé à espérer qu’on pour

ra, au printemps prochain, commencer les
opérations de rattachement.

Stuttgart, 7 octobre 1898.
F. Guarpucar.
Note supplémentaire du 25 novembre 1898.

Je viens compléter mon rapport en communiquant les résultats obtenus
rience quia eu lieu avec le nouveau projecteur entre Mt-Senario et Mt
qui était en cours d’exécution lorsque j’ai donné lecture du r

dans Pexpé-
-Amialo, expérience
apport même.

À Mont-Senario on avait placé le nouveau projecteur
d'une lunette à 0°10 d'ouverture ; à Mi-Amiata, une |
à laquelle on avait adapté une source lumineuse OXY-

a gaz oxy-acétylénique pourvu
ampe (collimateur systéme Lepante)
acétylénique pareille à la précédente.

Dans la soirée du 4 octobre, la station de Mt-Amiat
miere blanche, espacés de deux minutes, en brûlant, pour
de mélange (magnésium et chlorate de potasse).

aa produit deux éclairs à lu-
chacun de ceux-ci, 12 grammes

Au bout de 5 minutes, Mt-Senario a répondu à ces signaux par un éclair à lumière
rouge obtenu en brûlant 24 grammes de mélange (magnésium et chlorate de strontiane).

Tous ces signaux ont été aperçus à l'œil nu par les deux stations et, dans
ces éclairs paraissaient sous forme de flammes presque globulaires
une petite distance de l'objectif.

les lunettes,
d’une chandelle placée à

Quelques minutes après la production du dernier éclair, on a mis en fonction les

sources lumineuses, et les appareils ont été aperçus réciproquement par les deux stations,
de sorte qu’on a pu établir la correspondance télégraphique.

À M'-Amiata, la lumière envoyée par le grand projecteur de Mt-Senario se présentait,
a Peeil nu, comme un phare de premier ordre, de sorte que les télégraphistes ont pu rece-
voir les signaux sans l’auxiliaire de la lunette.

A la lunette du théodolite (47mm (ouverture et 35 de grossissement), cette lumiére
s’apercevait avec les dimensions apparentes de la planète Jupiter.

À M'-Senario la lumière du projecteur Lepante de Mt-Amiata n’était pas visible à l'œil
nu, mais dans la lunette du grand projecteur elle présentait l'apparence d’une étoile de 1re

grandeur et dans la lunette d’un théodolite analogue au précédent, d’une étoile de 3me où
Ame grandeur.

 
