 

   

— 808 —

XI. — Pays-Bas.
0000 nr

 

Depuis 1890 les observations ont été poursuivies d’abord par une section et plus
tard par deux sections d’ingénieurs, de sorte que vers la fin du mois de Mai 1898
les observations étaient terminées sur 50 stations primaires et 5 stations secondaires.

Les observations sont faites d’aprés la méthode du lieutenant général Schreiber au
moyen de deux théodolites de Wanschaff à Berlin avec des cercles de 35 centimètres de
diamètre. Les tambours des microscopes sont divisés en secondes et donnent par
taxation des dixièmes de secondes.

Dans la liste des erreurs de clôture des triangles on n’a tenu compte que des trian-
gles dont les trois angles ont été mesurés par nous. Les triangles dont les côtés tra-
versent la frontière prussienne sont mesurés en partie par la Landes-Aufnahme et font
partie des réseaux de raccordement avec la Prusse (voir XII, Prusse B).

31 Mai 1898.

H. J. Heuveumme.

Ah bio ah ia Aal dee

rasen
