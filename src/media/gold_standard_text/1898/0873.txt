   

 

 

— 157 —

VILs — Grande Bretagne (Inde).

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

INDICA TLLON 3 DIRECTEURS
N° A T m a \ ;
N DES POINTS LATITUDE LONGITUDE | EPOQUE ET OBSERVATEURS INSTRUMENTS REMARQUES
20 | Soman Devargutta.| 18° 7'46”| 80° 47' 16” 1870 | Shelverton. T..et 5.0.
2) Paluncha ! 2 17 34 31 | 80 43 39 1870 id. ; id.
22 | Bodänali.......... 17 59 29 | 80 57 27 1870 id. id.
23 | Singäwäram....... F2 45:10 80:66, 8 1870 id. id.
24 | Rajasutiıa 22952. 81 0383 1870 id. id.
25 | Näräkonda........ 708 hu sk 07 id. id.
265) Rapak 2 as I 3918 894 42 1870-71 Shelverton, Beverly. id.
27... Kaler..... 0.2. 17 48 16 | 812543 | 1871 Beverly. id.
28 | Päpikonda......... 17 3451 | 81 33 40 | 1871 id. : id.
29 | Singänama........ 18 422 81 3240 1871-72 Beverly, Rossenrode. id.
30 | Pedakonda........ 17 50 23 | 81 46 29 1871 Beverly. id.
31 PACA ES à I& 4 19°) 81 55581 1872 Rossenrode. id.
32, Munas 0... 18 20 27 | 81 5240 1872 id. id.
33.KBal2.8 0. 2... 825 8827 2 1872 id. id.
32. Chere. >... 5223 82 350 1872 id. id.
35 | Kaurälbiding......| 18 8 2 | 82 21 56 1872 ad: id.
36 | Kälingkonda ...... 17 49 42 | 82 18 40 1872 id: id.
37, | Pandivala 7 17.40 55.| 82.13 14 | 1872 id. id. =
38 Kap... 2... 1726 50,237 93 1860-72 Rosendrode, Strange. Barrows 24' n° 2.
oo Dhar 2 17 43 59 | 82 28 27 1860-71 id. T. et S. 36" et Bar-
row’s 24",
40 | Naläkonda ........ 17 44 8,8249 55 1860 Strange. Barrows 24". :
AE Sample: eo ee 2 31 19 8227272 1860 Strange, Basevi, Clarkson. id.
42 | Jarakonda........ e 15.249 8 1860 Strange. id.
+ 43 | Pinchdarla ©... 735. 2825753 1860 id. id.
- 44 | Pothkonda........ i AS 3.82 2% 1860 id. id. |
2 407 Var. 1740 3 805.17 1860 ; id. id:
= Märki (East Coast). | 18 259 | 83 4 14 | 1860 id. id. |
Gumru (East Coast).| 17 56 6 | 83 14 7| 1860 id. id.
: Jabalpur Meridional.
Kulumar (Calcutta
Longl. Series)....| 28027.027| 1904223 1865 | Shelverton. T.. et 8 807
Lora (Caleutta Lon-
gl. Series)... 23 29,42 | 80, 05% 1864 id. id.
3 | Karaundi . 3. 25 10 40 | 7959 23 1865 id. id.
= 2 | Sasan-Ki-Toria.... 23 11 93 ı AL 47 1865 id. id. |
3 | Ralangargar 2.77) 23 14 52 80 10 83 1865 id. id. |
A) Tapela:. 0.00 22 59 44 | 79:50 57 1865 id. id. |
5 halles 22 56 28 80:10 381 2.1805 id. id.
6 | Kusam Bara...... 22 AU AA 80: 119 1865 id. id.
7. Munda:s. 2.2... 2281 321710747670 1865 id. id.
8° KRotal 2 2. 227 AST DAU SOs eo 59 1865 id. id.
Oa) Tata... 22) 21 AVERY® One 20) 1865 | id. id. |
10: | Banott. 05 22 28 36 | 80 16 24 1865 id. id. |
11 | Sarandi Pat....... 22 13 19 | 80 3 5 1865 id. id. |
12 | Karidpahar 22 14 14.) 79 39 18 1865 id. id. |
13:-| Dhukrin 2.2 02 4.2.80 2603 1865 id. id. |
14. Sirrsjharti.  : 2153. 0.9920 1865 | id. | id. |
15 | Kharikona........ 21 58 6 | 80 12 23 | 1865-66 id. id. |
= 16 | Uingmära... ......1 2143 280 75 1866 id. id.
= 17| Bhajladand......:. 21 39 49 | 79 55 48 1865 ic) id. | id.
D SUR oe. |.21 48 11 | 80 21 40 | 1866 id. | id.
= 19 | Kara. <<. | 21 25 36 | 80 .5 Si} 1866 id. id.
Pt 00 | Sitéipir m... | 21 24 51 | 80 19 26 | 1866 | id. id. |
D 01 | Jet 5 | 21 12 21 | 80 153| 1866 | id. | id. |
2 22 | Chakälipät........ 21 9 19 | 80 19 15.| 1866 id. id.
E23 | Nano 0 | 20 58 52 | 80 12 2| 1866 | id. | id. |

 

 
