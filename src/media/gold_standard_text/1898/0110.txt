ï
id
i)
|
i
it
‚N
i

 

102

 

1.

Les calculs de déviations systématiques de la verticale en Europe ont été exécutés,
comme jusqu'ici, par les chefs de section, MM. les professeurs Bœrsch el Krüger, avec l’aide
de M. le Dr Schendel, qui a été rétribué sur les fonds internationaux. Vers la fin de l’année,
l'astronome M. Wanach a également exécuté quelques calculs.

Rapport spécial sur le calcul sysléma tique des dévialions de lu verticale,

en 1898.

Dans le but de contrôler les lignes géodésiques déduites déjà l’année dernière à
partir de Bonn, station de la mesure d’are de longitude en Europe, on a encore calculé les
lignes directes suivantes et les équations de déviation s’y rapportant :

Strasbourg-Paris (Panthéon),
Strasbourg-Talmay,
Paris (Panthéon)-Brest,
et Strasbourg-Rigi.
Ensuite on a achevé les calculs déjà préparés pour les lignes ci-après :
Simplon-Ghiridone (Limidario),
Ghiridone (Limidario)-Milan,
Milan-Turin,
Turin-Gênes,
Turin-Nice,
ainsi que les lignes de contrôle :
Simplon-Milan,
Milan-Gênes,
et Gênes-Nice.

Les résultats astronomiques pour l'Observatoire de Nice, de même que les données
sur la position réciproque des différents points d'observation et leur rattachement au réseau
trigonométrique nous ont été communiqués de la manière la plus obligeante par M. le Général
de la Noë.

Les doutes que nous avons déjà mentionnés à la page 90 du Rapport pour 1897,
quant à la jonction trigonométrique entre la Suisse el l'Italie, ont été confirmés par la com-
paraison des angles du quadrilatère commun Basodine-Cramosino-Ghiridone-Menone et par
l'erreur de clôture considérable de l'équation de Laplace pour la ligne Simplon-Milan, s’ele-
vant à — 11”, si l'on s’en tient au côté Ghiridone-Menone comme unique côté de jonction.
Toutefois ces doutes ont pu, à notre satisfaction, être levés, à la suite d’un échange de lettres
avec M. le D' Messerschmitt, à Zurich, auquel nous exprimons toute notre reconnaissance
pour les recherches approfondies qu'il a faites dans ce but. Il en résulte que le point suisse

A LL LICE COL ONE TRI

Jul: nik

 

ju abn A ha À L Lib Aus pu à D

Le rueaens mea hi |

 
