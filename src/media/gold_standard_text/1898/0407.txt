 

|
|
i

 

VL | AR

th
An

ni

PE TEL ELITE UT

 

 

389

Schweizerische Kommission, 44.0... 0.410, 82 (8) 99 69
FranzösischeiBeobachtersilo in. ut a lazeal 39 (16) 10 48
Geogr.-siat, Institut,in Madid. = =. _ 8 (1) 1 1
Italienische Beobachter 6. 22 (9) 18 24
Coast and Geodetie vun US ,ı 88 (22) 9 80
Japanische Beobachter. 2. 2 (2) N 1
Aeltere Beobachtongen = _ 120) À — 126

Diese Liste ergiebt 1385 Stalionen ; von diesen sind aber nur etwa 1298 verschieden.

Die Anzahl der Anschlussstationen beträgt etwa 73, wobei die mehrfachen Bestim-
mungen innerhalb derselben Gruppe, u. a. bei der des mil.-geogr. Instituts in Wien, der
österr.-ungar. Marine u. s. w. nicht gerechnet sind.

Höchst erfreulich ist es zu bemerken, wie überall das Interesse für relative Schwere-
messungen zunimmt und in weitere Kreise dringt. Steht daher für die nächste Zukunft
wieder eine rapide Steigerung der Ergebnisse in Aussicht, so ist doch bereits auch jetzt
schon das Erzielte von grosser Bedeutung, und zwar theils für die Erkenntniss der all-
gemeinen Veränderung der Schwerkraft mit der geographischen Lage der Stationen, theils

für die Erforschung der Beziehung der Schwerkraftsanomalien zu geotektischen und erd-
magnetischen Störungen.

Ilerr Oberst von STERNECK hat die vom militär-geogr. Institut auf 508 Stationen er-
zielten Ergebnisse zu einer Studie über die Abhängigkeit der Schwerkraft auf der physischen
Erdoberfläche von der Meereshöhe benutzt und ist zu dem bemerkenswerthen Resultate ge-

langt, dass im Durchschnitt die Abhängigkeit dieselbe ist, als befänden sich die Stationen in
freier Luft über der Meeresfläche.

Herr Ivanor, Astronom in Pulkowa, hat ferner aus 367 Bestimmungen auf ca. 320
verschiedenen Stationen im Anschluss an Oppolzers absolute Bestimmung in Wien eine neue
Formel für die Länge des Sekundenpendels im Meeresniveau abgeleitet, wobei auch eine
Kugelfunktion 3. Ranges mitgenommen ist. Er findet in Cenlimetern :

L = 99,0997 — 0,5240 sin? 9° — 0,0016 (sin @’ — 5/, sin’ 9’)
g ist die geocentrische Breite. Als Abplattungswerth leitet Ivanor den Betrag 4 : 297,2 her,
ein Werth, der auch in anderer Hinsicht plausibel erscheint. Die Schwerkraft am Aequator

folgt gleich 9,78075 m, also 0,00075 m grösser, als ich in meiner Formel von 1884 an-
gegeben habe.

Der geringe Betrag des Koeffizienten der Kugelfunktion 3. Ranges beweist, dass
man zur Zeit eine Ungleichheit der Nord- und Südhälfte der Erde nicht nachweisen kann.

Die neue Bestimmung der Pendelformel erleidet noch eine Einbusse der Sicherheit
in Folge des Widerspruchs in der Verbindung Amerikas und Europas nach den Bestimmungen
von englischer Seite und von französischer Seite. Wie ich aber kürzlich vernommen habe, ist

 
