  

 

 

| RUE

    
 

     
 

  
    

 

  
  
    
   
  
    
   
  
   

(4, 1915709. 5 (7)

 

4,3931067. 4

D

    

| 4, 8097497.

bo

, 0388050.

| 4,9511093,

©

5, 0409070.

OL

  

 

| 4, 9755350. 2

(17) |

4, 3376437, 8 (7)

 

ERREUR

MOYENNE
DE

| LA MESURE

|D’UN ANGLE

|

 

) 2", oe
0 |
Te

SOO

© es

, 998 (9)

(20)

1,

2)
=,

1,

 

788

| DE LA BASE

ee

 

|
ERREUR MOYENNE |
CALCULÉE ||

en
|

IE UNITÉS
EN PARTIES La

|
DU
a (7% ORDRE
NGUHU |
"| DU Boe. |

 

3m 132,9
En 24, 8
u | 30, 0
rn | 36.0.1
mes | 726 |
|
an | 80,4 |
an 1 85 |
on |: 98,2
: 28, 2

153 900 |

 

Se 5 (4)
598000 | 7°
ir :
RTS 0 0
271 000
(10) |
Ka an

240 000 |

tie 14,8
m | 17,3 ||
Es | 14, 6 |
| |
GE DT |
is 14,7

 

SH 000

ERREU

STINE

R

MOYENNE TOTALE

DE
LA BASE

nn

EN PARTIES

DE LA
LONGUEUR

1
> 500.

ee

143 000

a

130 000
1

5)
92

_ unes
121 000

>)
=
oO

148 000
ru.
285 000 |
be
266 000
ai)
235 000

1
217 391
a,
198 413
1
218 341
Ze
261 780 |
1

 

217 391

 

 

CALCULEE

——

DU

133, 0 42)

30, 2 (19)

il

|

| 2
EN UNITES

||
||
|

7™° ORDRE|
i DU LOG.

(19) eon +

(13)

für den Log.

l: ae

‘|| sung, 1.

3 LL ©:

Sapiski etc. B.
(14) Sapiski ete.

und 3,9841804.8.

B.

on
iD.

 

 
 
 

PA I Enns

REMARQUES

 
 

 

106.
en S. 11-37.

S. 70-77. Mittel aus den beiden Werten 3,9841711.8

Die L en giebt S. 261 nach 3 Ausgleichungen
der Seite die
(15) Mittlerer Fehler eines Winkels:
12.
Arien Fehler eines Winkels:
S.
Mittlerer Fehler eines Winkels

Werte: 3 98417: 28, 3,9841803 und 3,9841767.

I nach der ersten russischen Messung
II nach der zweiten russischen Mes-

: III nach der dritten Ausgleichung in

der Längengradmessung.

(16) Sapiski ete.,
(17) Sapiski
(15) Sapiski
(19) Sapiski
(20) Sapiski
(21) Sapiski
22) Sapiski
(23) Sapiski
(24) Sapiski

(

etc.,

etc.,
etc.,

(‘) « Da is Schweizerische Dreicksnetz » T. IV, p. 166-167. (N.B

B.
B. 47,
etc., B.
etc., B.
B.
B.
etc., B.
etc., B.
etc., B.

a Sr
le
. 68.
02%
bs,
ADD
47, 8. 48.
47, 8. 43.
LI. S. 162. Die Grundlinie ist selbst Dreiecksseite.

47,
47,
47,
47,

    

 
 

B. A la base de

Bellinzona on a tenu compte des déviations de la verticale, observées dans
le réseau de la base).

(2) « Das schweizerische Dreicksnetz » T. ILL, p. 94.
(3) Erreur moyenne de l’unite de poids. — Erreur moyenne de la moyenne

| uns angle: sh 0%;95. — i. c., I. Iy, p. 58.

Ole I. IV, p. 54.

@) rc Miles Se

(°) L. ¢., T. IV, p. 248. Erreur moyenne de Vunité de poids.

Gele, Ten, p. 246:

©) ve. wht ps 100:

(?) L. c., T. IV, p. 213. (N. B. L’erreur moyenne de l'unité de poids est

obtenue en tenant compte de l’attraction des masses visibles. Sans cela elle

|| serait de = 4’, 0).

 

(10) 18,

CHAT. TV, p. 213.

 

  

   

    
   
    
    
  
   
   
 
   
  
   
  
   
 
   
  
   
  
   
    
    
    
  
   
   
 
    
  
   
  
   
  
   
     
  
