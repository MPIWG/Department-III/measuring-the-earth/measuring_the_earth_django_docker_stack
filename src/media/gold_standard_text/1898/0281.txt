 

{VN ewes

 

iM ART

WE | TAMAR HAIGH ML à ee

263

Lothabweichung in Azimut bestimmt wart?. Es wurde als Lothabweichung in Breite fiir die
Martorana gegen Castania + 7°87 und gegen das nur circa 1 km. entfernte astronomische
Observatorium — 0 29 ermittelt, so dass also, ebenso wie beim Azimut, für das Observatorium
und die Martorana keine verschiedenen lokalen Einflüsse zu bestehen scheinen.

Ausserdem haben sich aber die Rechnungen des Gentralbureaus für das europäische
Lothabweichungssystem bereits bis nach Mailand, Turin und Genua ausgedehnt. Ueber die

bisher erhaltenen Resultate vergleiche man den diesen Verhandlungen beigefügten Thätig-
keilsbericht des Centralbureaus fiir 1898.

FRANKREICH

In der Nähe der Station Evaux des französischen Meridianbogens, wo schon Delambre
1797 eine Lothabweichung in Breite von 7” gefunden hatte, sind auf dem Punkt Arphenille-
Saint-Priest nördlich vom Puy-de-Döme 1895 Breiten- uud Azimutbestimmungen ausgeführt
worden, deren vorläufige Berechnung eine Lothabweichung in Breite von 6” ergeben hat, in
naher Uebereinstimmung mit Evaux, während schon früher auf dem Gipfel des Puy-de-
Döme 63 und in Opmer, in der Nahe von Clermont, 9” ermittelt worden waren. Diese Unter-
suchungen sollen im Interesse der Bestimmung der Attractionssphäre des Central-Massivs
des Gebirges noch fortgesetzt werden 4.

Durch die europäische Längengradmessung in 52° Breite ist für den nördlichen End-
punkt des neuen Meridians von Frankreich, Rosendaél-lés-Dunkerque, die Lothabweichung
in Breite und in Länge bestimmt worden ; ebenso sind in Breite und Länge neu bestimmt
oder berechnet worden die Lothabweichungen für die Punkte Talmay, Paris, Brest und
Nizza und in Breite allein die in St. Martin de Chaulieu (vergl. die Angaben über die Rech-

nungen des Centralbureaus am Ende dieses Berichts sowie den Thatigkeitsbericht des Central-
bureaus fiir 1898).

SCHWEDEN.

Die lokalen Lothabweichungs-Untersuchungen in der Nähe des Kustenpunktes Breds-
kär (vergl. den Lothabweichungsbericht in den Verhandlunsen zu Berlin, 1895, Il. Theil,

©

S. 184) sind nunmehr abgeschlossen worden, indem für 13 Punkte Breitenbestimmungen
vorliegen 12.

Angewandt ist Glarke’s Ellipsoid von 1880, und als Ausgangspunkt ist Bredskär an-
genommen.

Accademia dei Lincei. Anno CCXCIV, 1897, Serie quinta, Rendiconti, Vol. VI, 4° Semestre. Roma 1897,
pag. 327/334.

'" Verhandlungen in Berlin, 4895, II. Theil, S. 182.

11 Verhandlungen in Berlin, 1895, II. Theil, S. 202.

'2 Verhandlungen in Berlin, 1895, II. Theil, S. 273/274.

 
