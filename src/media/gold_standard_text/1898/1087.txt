eo

 

ce

FAA Oa LINE Hein

HAMA I

MIET,

 

 

— 371 —

 

  

 

 

 

XIV. — Prusse, B.—Erreurs de clôture des triangles desquels ont été mesurés les trois angles.

 

NOMBRE

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

B LECTURE DES ERREURS
EU a N
= a EN NL OBSERVATIONS | DE CLÔTURE
5 Poe SECONDES mus EXÉCUTÉES DE
5 ET oe ee VERNIERS POUR CHAQUE 4? REMARQUES
= DIAMÈTRE DU CERCLE n OU DES CHAQUE ANGLE | TRIANGLE
A HORIZONTAL oe MrORoscopns 0900 à
MICROSCOPE CHAQUE
DIRECTION nn

14 | Die 10 zülligen Theodolite N. I und| Ablesung |? Mikroskop- 24 07,049! 0,0024 | Veröffentlichung

II von Pistor und Martins (Thei- | mittelbar von | Mikrometer. N.8. :

lungsdurchmesser — 265"), '/„ sekunden.
15 à id. id. id. 24 0 ,641| 0,4109
16 id. id. id. 24 0,197, 2.046
17 id! id. id. 24 0,244 0,0595
18 id. id. id. 24 0 ,303| 0,0918
19 id. id. id. 24 0 ,549| 0,3014
20 id. id. id. 24 07,338 0, 1142
21 id, id, id. 24 1,190) 2.4161

XVIII.
HANNOVERSCH-SAECHSISCHE DREIECKSKETTE 1880-81.

1 |Die 10 zölligen Theodolite N. 1 und| Ablesung |2Mikroskop- 24 17,115] 1,2432 | Veröffentlichungen
II von Pistor und Martins (Thei- | mittelbarvon | Mikrometer. N.8 und N. 11,
lungsdurchmesser — 2652), '/ sekunden.

2 id. id. id. 24 02,140 0,0196

3 id. id. id. 24 | ,262 04172026

4 id. id. id. 24 0 ,065| 0,0042

5 id. id. id. 24 1,088) 1.062

6 id. id. id. 24 1.2580 2, 421]

7 id. id. id. 24 1,077) 1.159

8 id. id. id. 24 0 ,820 0, 6724

9 id. id. id. 24 0 ,909 0, 8263

10 id. id. id. 24 0 ,524| 0,2746
1] id. id. id. 24 0,232) 0,0538
12 id. id. id. 24 0 , 169 0, 0286
13 id. id. ld. 24 1,433 2.0885
14 id. id. id. 24 0 01» 0, 4556
15 id. id. id. 24 0 ,434 0, 1884
16 id. id. id. 24 0 , 866 0, 7500
17 id. id. id. 24 0 „061. | 0,0037
18 id. id. id. 24 0 ,490| 0,2401
19 id. id. id. 24 0 ,437| 0, 1910
20 id. id. id. 24 0 , 406 | 0, 1648
21 id. id. id. 24 0.,723| 0,5221
XIX.
HANNOVERSCHE DREIECKSKETTE 1882-1885.

1 |Die 10 zölligen Theodolite N. 1 und| Ablesung|2Mikroskop- 24 107,129] 0,0166 | Veröffentlichung
II von Pistor und Martins (Thei- | mittelbar von | Mikrometer. N.8.
lungsdurchmesser = 265™™). */,, Ssekunden.

2 id. id. id. 24 0 ,058} 0, 0034

3 id. ide id. 24 1 „8221. 89108 |

4 | id. id. id. 24 1”, 489 22171

5 id. id. id. 24 0 ‚266 0, 0708

6 id. id. id. 24 0,571 | 0; 3260

7 id. id. id. 24 1,109 | 2 2209

8 id. id. id. 24 Loi 2,3 AT

9 | id. id. id. 24 | 0 ,130| 0,0169

10 id. id. id. 24 0,115] 6012

 

 

 

 

 

 
