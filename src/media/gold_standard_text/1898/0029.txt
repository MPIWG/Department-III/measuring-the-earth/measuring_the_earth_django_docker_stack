 

 

IT AR

Tee iii

TR LORIE EL LUE

Wier

AWKITE SITZUNG

Dienstag, den 4. Oktober 1898.

Prasident: Herr H. Faye.
Gegenwartig sind :

I. Die Herren Delegirten: Albrecht, Anguiano, van de Sande-Bakhuyzen, Bassot,
von Bodola, Bersch, Bouquet de la Grye, Bourgeois, Celoria, Darwin, Forster, Gautier,
Guarducei, Haid, Hammer, Helmert, Hennequin, Hirsch, Kimura, Koch, Lallemand,
Nagel, Nell, Oberhoffer, von Orff, Preston, von Richthofen, Rosén, Sagasta, von Schmidt, M.
Schmidt, von Stubendorff, Tanakadate, Tinter, Westphal.

Il. Die Herren Eingeladenen : von Brill, Deslandres, Haller, Messerschmitt, de No-
vales, Tripet, Weitbrecht.

Die Sitzung wird um 3!/, Uhr eröffnet.
Der Secretär wünscht zunächst die folgenden geschäftlichen Mittheilungen zu machen :

Herr General Zachariae meldet leider aus Kopenhagen, in Antwort auf eine Depesche,
dass er definitif durch die dortigen Verhältnisse verhindert ist zur Conferenz zu kommen. Er
wird dem Secretär den Bericht über die dänischen Arbeiten für die Jahre 1897 und 1898
einsenden. (Siehe Beilage B. XX.)

Ferner schreibt Herr F. de P. Arrillaga, am 29. September, dass er zu seinem Be-

dauern abgehalten sei nach Stuttgart zu kommen und bittet die Conferenz seine Abwesenheit
zu entschuldigen.

General Bratianu telegraphirt aus Bucharest am 3. Oktober, dass dienstliche Ab-
haltungen ihn zu seinem Bedauern verhindern an der Conferenz Theil zu nehmen.

Nach einem Briefe des Herrn van Diesen an das Bureau, welcher von Herrn Bak-
huyzen dem Secretär übermittelt ist, erklärt derselbe, dass er durch verschiedene Gründe,

 
