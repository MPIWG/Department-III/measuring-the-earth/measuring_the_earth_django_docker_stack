   

a

VI. — France et Algérie.

INDICATION ; | DIRECTEURS

No LATITUDE

 

 

 

 

 

 

 

 

 

 

 

DES POINTS LONGITUDE EPOQUE | ET OBSERVATEURS INSTRUMENTS | REMARQUES
| = re un
os Triangulation Méditerranéenne. (Suite.)
peo Cette. on... se... 43° 24: 10"| 21° 20! 51! Cercles repetiteurs || Memorial du Depöt
430 | Cabreres........... 43 35 43 | 21 1 11 avec 4 verniers qui | Sénéral de la Guerre,
431 | Bezienn............ 43 20 31 | 20 52 23 donnent chacun la | *™
681281, bons ©... 2. Ki : ; oe lecture de 20" cen-
ol aie, | Déjà mentionnés. 1830-31-82. | er … | tésimales. — Pour |
~ 432 on es, 2311 8120 40 2.» innault eh Fissie. chaqueanglefurent |
auch. bss... ee : exécutées 20 répé-
72 | Bugarach.......... \ Déjà mentionnés. titions. |
Triangulation de Jonction avec l'Angleterre.
23 Dunkerque re 019 2. 8 20° 2! 98" | Mémorial du Dé-
21 ‘oCasselo 204/57 2098 | ae ge la Guerre,
443 | Gravelines......... 50 59 12 | 19 47 22 ek eae ieee
. 444 | Harlettes.......... 8043 0 | 19 38 15 |
445 | St. Inglevert...... 00 52 14 | 19 22 3 1861-62 Colonel Levret, Id
446 | Mont Lambert..... 90 43 1 | 19 18 53 ’ | capit.® Beaux et Perrier. :
447 | Coldham........... ol 610 185159
448 | Fairlight.......... 00 52 38 | 18.10 57 |
449 | St. Peters. ........ ol 21 55 | 19 4 55 =
/
Triangulation de Vile de Corse.
_ 450 | Monte Stello....... 420 47' 1'7"| 979 459" i Mémorial du Dé. |
451 | Monte Capanna....| 42 46 15 | 27 49 57 a]
452 | Monte Castello..... 43 2 57 | 27 28 45 a
453 | Monte S. Angelo ...| 42 27 46 | 27 4 22 1
454 | Monte Asto........ 42 31 27 | 26 52 26
455 | San Pietro ........ 42 23 45 | 26 59 28
Traunato.......... 42 25 13 | 26 44 41
Cine... 42 26 6 | 26 45 45 es
Paglia Orba....... 42 20 33 | 26 32 34\ 1868, en Thöodolite de 8 p.
Monte Artica......| 42 15 51 | 26 38 4. :
Linseinosa..... ee 42 18 9) 26-30 45
Monte Cervello.....| 42 8 26 | 26 33 4]
Saltelle.. ......... 42 7 57 | 26 25 20
Yılullo.....2.:..... a2 13 837 26 21 27
Carghése.......... 227260 | 26.15 14 |
DUO... 42 16 9 | 26 12 57 |

 

 

 

 

Triangulation Primordiale Algérienne. (Partie Orientale.)
Capit. Versigny et Vernet. | Théod. n° 13 avec 4 || Mémorial du Dépot

 

 

 

 

 

 

 

Base de Blidah....| 36°29' 18") 20°28' 21" 1860 Me
Terme Sud. verniers qui don- en doi Guerre,
nent chacun la lec- | 22° *
| ture de 20" centé-
simales.
Base de Blidah....| 36 34 35 | 20 26 58 1860-64 Capit. Versigny, Perrier | Cercle répét. n° 13.
Terme Nord. et Roudaire.
Aloın  ...... 36 27 44 | 20 17 13 1860 Capitaine Versieny. Id.
Meouzaia........... 36 22 35 | 20 23 13 | 1860-61-64 Capitaines Versieny, Per- | Cercles reper, ne 3,
| xier et Roudaire, 4 et 13,
Ferroukha......... 90 2158,20 5 24 1860-61
‘ 2 RS 5 ee
ce au : 2 nn 2 5 a 2 i 2 N Capitaine Versieny. Cercle répét. n° 15.
Saebkah Chergui...| 36 8 4 | 20 46 42 1861 |
| | |

 
