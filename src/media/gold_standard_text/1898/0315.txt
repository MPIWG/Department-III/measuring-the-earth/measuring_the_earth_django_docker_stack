 

|
i

am

oti

LE

ite

WIE P18 CRRA ARIA

 

  
 

[Elimination des

Année et mois ;
equations personnelles

 

1895
aoüt et
septembre.

Echange
des observateurs.

1896 >
septembre.

 

1897 »
Septembre et
octobre.

 

 

April 1895 With exchange

of observers.

May and June )
1895

June and July » |
1895

Aug. and Sept. »
1895

 

 

297

TITRE DE LA PUBLICATION

 

« Zapiski... » t. LIV, p. 9 (premiére
partie), résultat mentionné.

« Zapiski... » t. LV, p. 8 (premiére
partie), résultat mentionné.

Pas encore publié.

Not published

C. and G. 8. Report for 1897

 

REMARQUES

N.B. The superintendent’s annual
report for the fiscal year 1897!
contains in Appendix No. 2 the
finalresults of « The telegraphic
longitude net of the United states
and its connection with that of
Europe, as developed bythe Coast
and Geodetic Survey between
1866 and 1896.» By Charles A.
Schott, Assistant. For an abstract
of the paper see « The Astrono-
mical Journal No. 412», Boston.
1897, september 14. The tabular
results given here are the values
from direct observation, the ad-
justed values are contained in
the publication.

 

1 Not yet issued.

— 38

 
