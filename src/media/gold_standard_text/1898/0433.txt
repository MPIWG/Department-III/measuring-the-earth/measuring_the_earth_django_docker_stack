 

|
|
|

 

fay ti iil |

a a

ji li

a a

WR a

405

Travaux exécutés par l'Observatoire de Padoue (voir planche IN).

a) En 1897, différence de longitude entre les Observatoires astronomiques de Padoue
et de Bologne (point de Laplace), exécutée par MM. Lorenzoni et Ciscato au moyen des ins-
truments de passage Bamberg | et Il, déjà mentionnés à propos de la différence de longitude
Bologne-Florence.

L'opération, commencée le 9 mai, n’a été terminée, à cause du mauvais temps, que
le 14 juin, après neuf soirées utiles.

Après la quatrième soirée on a fait l'échange des observateurs pour l'élimination
de l’équation personnelle, laquelle a été aussi déterminée directement.

Les calculs de Popération sont en cours d’exécution à l'Observatoire de Padoue.

b) Latitude de Bologne. — Get élément a été déterminé dans le mois de juin par M.
Ciscato en suivant deux méthodes différentes. D'abord par la méthode Horrebow-Talcott au
moyen de linstrument de passage Bamberg n. |, pourvu de deux niveaux Reichel, en obser-
vant, en moyenne, quatre fois 20 couples formés de 37 étoiles, dont 14 fondamentales :
ensuite par la méthode de Struve au moyen de l’instrument de passage établi dans le premier
vertical, en observant quatre fois les passages de six étoiles, dont deux fondamentales. La valeur
définitive obtenue, réduite à l'axe de la tour de l'Observatoire, est la suivante :

Latitude astron. — 44 99 59,77 + 0,06.

c) Azimut du point géodésique Me Grande, vu de l'Observatoire astronomique de
Bologne.

Cette détermination a été faite également par M. Ciscato, dans les mois de juin-juillet
1897, par les deux méthodes suivantes :

En mesurant d’abord, avec l'instrument de passage, l’azimut « d’une mire méridienne
établie au Sud à la distance de 2750m et ensuite, avec l’altazimut Pistor, n° 14, l'angle ß entre
la mire et M° Grande ;

Puis en mesurant, au moyen de l’altazimut, la différence d’azimut entre la Polaire
et le signal géodésique de Me Grande.

La valeur définitive obtenue, réduite à l’axe de la tour, est:

Azimut de la direction vers M® Grande. Assır. = 146 41 95,29 + 0,11.

d) Déviation de la verticale. — La latitude ellipsoïdale (oen.) de l'axe de la tour de
l'Observatoire de Bologne et l’azimut ellipsoïdal de Me Grande (Aen.) vu du même axe, calculés
sur l’ellipsoïde de Bessel, en supposant la verticale de Gênes coïncidant avec la normale à
l’ellipsoide, sont, d’après les résultats de l’Institut géographique militaire :

 

RE RER ER RS ARR D

SAR RSR A ONS ap

OR EE

i
|

 
