i POL Hel) GUI

Ui WAIL AR

DA D FA 1 ale Hm!

|
I
|

 

 

Erreurs de clôture des triangles desquels ont ete mesurés les trois angles.

— 421 —

 

NOMBRE

  

 

 

 

AUT LECTURE DES ERREURS
s EURS DES En 5 NOMBRE ae ne
a EMPLOYES À
eS SECONDES Due EXÉCUTÉES DE a
© ET a VERNIERS POUR CHAQUE A? REMARQUES
2 DIAMÈTRE DU CERCLE a OU DES CHAQUE ANGLE am SER
OU POUR
. HORIZONTAL eo cote MICROSCOPES oem
DIRECTION ae a
: Richtungen.
1 | Universalinstrument von A. Repsold | Mikroskope. 2 40°... 1,042 2, 3778
2 und Söhne in Hamburg, 31,55 cm. id. 2 36 0,087 0, 0076
3 Durchmesser des Horizontalkrei- id. 2 36. 0 , 494 0, 2440
4 ses getheilt von 4 zu 4. id. 2 30 0,399 0, 1592
5 | Mittelst der Schrauben-Mikroskope id. 2 40 0 , 654 0, 4277
6 werden direct Doppelsecunden u. id. 2 40 0,536 0,2873
7 durch Schätzung bis zu 0".2 ab- id. 2 40 0,738 | 0,5446 |
8 gelesen. id. 2 . 40 1,346 1,8117
9 | Siehe: Astron. geodätische Arbeiten id. 2 40 ; OTL 0,005
10 im Königreiche Sachsen II Abth. id. 2 40 0,695. 0,4802
1) | 8.06 id. 2 40 1,489 2,2171
12 | Dreiecksschlussfehler: S. 484, 485 id. 2 40 1,282 1, 6435
5 u. 662, id. 2 40 | 0,556 023098
14 id. 2 36 0, 763 0, 5822
15 id. 2 36 0,380 | 0,1444
16 id. 2 40 0 , 602 0, 3624
17 id. 2 36 0,191 | 0,0365
18 id. a 36 0,219 0,0480
19 id. 2 36 0,001 0, 0000
20 id. 2 36 0,109 | 0.0119
21 id. 2 36 0,246 | 0, 0605
22 id. 2 36 0 , 484 0, 2343
23 id. 2 36 0,343 | 0, 1176
24 id. 2 36 0,107 | 0,0114
25 id. 2 ‚36 0,376 0,1414
26 id. 2 36 0,059 0, 0035
27 id. 2 36 0,656 | 0,4303
28 id. 2 36 0,293 0, 0858
29 id. 2 36 0,261 | 0,0681
30 id. 2 36 0,102 | 0,0104
at id. 2 36 0,097 | 0,0094
32 id. 2 36 0,121 | 0,0146
33 id. 2 .36 0, 048 | 0: 0023
34 id. 2 36 out : 0, 0445
3 id. 2 36 0,051 0, 0026
36 id. 2 36 0,134 0,0180
31 id. 2 36 0 , 008 0, 0001
38 id. 2 36 0,045 0,0020
39 id. 2 36 0,147 | 0,0216
40 id. 2 36 0, 837 0,7006
41 id. 2 36 0,965 | 0,9312
42 id. 2 36 0 ,964 0, 9293
43 id. 2 36 0,082 | 0,0087 |
44 id. 2 36 0,329 | 0,1082
45 id. 2 36 0 , 168 0, 0282 |
46 id. 2 ‚36 0 , 208 0, 0433
47 id. 2 36 1 , 620 2,6244
48 id. 2 28 0,143 | 0,0204
49 id. 2 = 0,471 0, 2218 |
50 id, 2 28 0,125 0, 0156
51 id. 2 .36 0,279 | 0,0778
52 id. 2 36 0,457 | 0,2088
53 id. 2 36 0,208 0, 0433
54 id. 2 36 0,038 | 0,0014
55 id. 2 36 0,223-| 0,0497

 

 

 

 

 

 

 

 

 

 

 

 
