LAS Ses ee

 

NL AR

ner TR IL la a

   

— 403 — : à

 

 

 

 

 

 

 

 

 

 

 

 

 

 

XVI. — Russie.
INDICATION DIRECTEURS
\ 0 | p : i 3 ENTS REMARQUES
; DES POINTS PTE INSEE FEU ET OBSERVATEURS : N a
= | = RIT
Triangulation du Parallèle 52°. (Suite.)
566 ı Tukowka.......... o 9! o 181
567 | Mikhailowka ...... in
568 Zasseietskaia .... . 5] 35 : 60 38
069 | Pestowka......... 51 2 60 47 | 2
970 | Kozlowka......... Bl 32 60. 58) | a
All Mikhailowka DR 5 39 60 53 S as
572 Meliss oe 2 Bile Om 61 2 = a =
DS Borki sical etre rete pease age 5] 28 61 5 & & s
574 Berezowka........ 5. 533 61 12 a oe
575 | Bezlessowka....... 5I 42 61 12 Rz I
576 | Olguinskoie ....... ol aa a D a
977 | Ssoukharewka ..... a ae 61 21 = oO
078 | Wedenapino ....... EI 4 61 39 = a
où Alekseïewka....... Shi ds 61 56 = e
580 | Oupornala......... 5 46 61 52 =
581 | Oblawka.......... Si 55 Gr aa
582 Birukowka........ 51 46 62 a |
583 Galakhowa........ | 5] 53 62 7 |
984 | Woiewodtchina....) 5) & 62 19 .
585 | Lapoukhowka.. ... 5] 44 62 27 | |
586 | Chtcherbinowa.. 51 50 62 32 | |
587 | Ssinelnikowa...... 5 55 62 25 | |
588 | Nieolaiewka....... SL 55 62 Al | |
589 | Prokaurorowka.... 5] 55 62 53
590 | Ossinowka......... 51 47 62 51 |
591 | Karakina.......... | 57 40 62 See
592 | Krukowka......... | 51 5 65 2
093 | Sleptsowa ......... 51 44 63 mn
594 | Lapoukhowka...... 51 32 63 18
595 | Chiroki-Bouérak... 51 42 63 24
096 | Mourawlew-Bouè-
Palko eee 5] 42 63) 377
997 | Klechtchewka...... 51 A] 63 48
598 | Jearinowa......... Bit 38 63 42
599 | Ssaratow.......... 51 32 63 43 5
600 | Korsakowka....... 51 54 63 25 ©
601 | Gremiatehka....... 5 7 93 26 | : | 2
602 | Nowy-Boursassy ...| 52 5 63 4 | 2 a
O03. RVie ae on say 64 2 \ a ) a
604 | Berezniki......... 5] 45 64 25 = | =
605 | Maksimowka....... 528 | 64.00 fe | cz
606 | Kiriakowa......... 51 57 64 40 Ù
607 | Chikhany.......... 529 64 50
608 | Bagai (bout sept.
de la base). 52 :6 64 39
609 | Bagaï (bout mérid.
de la base)..... 52.10 64 38
610 | Gawrilowka....... 02.20.1201. 09
611 >ELipowka., ..  , 52 20 64 A
612.) Legoche........... 5218 6522
613 | Ossinowka......... Hp 65:3
614 | Ssamodourowka....| 52 23 65 29
615 | Oust-Koulatka..... | 5S 86 | 65 201
616 | Sosnowaia-Masa.... 52 28 | 65 38 | |
617 | Elschanka......... | 52 32 69: 46 | |
618 | Zelenowka......... 1:52 43 1 05.31 | |
619 | Fedorowka........ | 52.238 63 51 | |
620 | Karagoujà......... 52440 65 54 | | |
* 621 | Beli-Kloutch....... | 52 59 265 44 |
| |

 
