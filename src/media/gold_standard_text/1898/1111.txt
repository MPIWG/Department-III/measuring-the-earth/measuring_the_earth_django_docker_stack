FAR ee

 

un NN AR

WF I rm ite lit

   

— 395 —

XVI. — Russie.

     

INDICATION | | . DIRECTEURS

 

 

| REMARQUES

 

 

 

 

 

 

 

 

 

|
\ | DES POINTS | LATITUDE | EPOQUE ET OBSERVATEURS INSTRUMENTS
Triangulation des Provinces Baltiques. (Suite.)
156 | Ebbafer ....... u...) 590 6/18") 43059! 197) \ |
lov | Racks | 59 9:33 | 44 2 16 | |
158: Devala, ae | 59 15 53 | 43 57 23 : i
5 Heol | Instr. Univ. |
159 | Warres-Mäggi ..... 59 9,4 2 r. U |
160 Hehe oo a ap 3g, 1810-28. F. W. Siruve, de Reichembach |
ma | 159 26 0 | 43 56 4 de 13 p. |
162 | Mäki-päälys .. 60 4 29 | 44 33 0 |
| / / |
Triangulation de Finlande.
Maky-päälys....... | Déjà mentionné. | Publies dans « L’are
163: Syartriea. . ... 2... | 60° 16" 38" | 4416 11" ay Meridien » per
164 | Ristisaari ......... | 60 18 52 | 44 28 49 | ne
1650 usa 45 | 60 22 59 | 44 13 11 | '
166 | Kokko-vuori....... | 60 27 49 | 44 27 4 cS
167 | Bovisa . 2... | 60 26 48 | 43 53 50 =
168 | Strömfors ......... | 60 31 20 | 44 5 49 Zi
169 | Korsmalm......... 1.60.35 14 | 49.51 46 ©
170 .Mustlla as 004333114 1% 5
171 1.Porlom TE. .. 60 42 4 | 42 40 12 &
Porlom I......... | 60 42 20 | 43 40: 10 =
172. Wallikala. 0. | 60 48 35 | 43.36 39 | ®
173 | Aemmänäuräs...... | 60 47 181 43 25 2| 2
174 | Perheniemi........ 6051 3 95103 = a
9, Huhtmar 35). 6055 7| 43 38 54 | œ os
16.» Messla. os Se, GE. 6 27- | 48° es : ay
177 | Wahteristo........ 61 450 | 43 3106| 2 2
178 | Wesivehmais....... 61 9 24: 43 21 23 ;
1795: Kurhilla. . . . : L 6 12.200) 4 400 = a
180 | Soitin-Kallio ...... | 61 27 37 | 43 22 24 = Ss '
181.) Warmala ......... | 6:27 2| 42 59 179 = ae |
182 | Wiljamin-Wuori ... | 61 35 23 | 43 29 27 a ae
183 | Kylmä Kangas.....| 6] 37 34 | 43 4 36 _ 8 ce |
184 | Kammio........... | 61 41 541 43 21 9 10 ‘A en |
185 | Rappu-Wuori......| 61 4840 | 43 5 38 a = 27 |
186 | Tammi Mäki....... | 6150 8 | 43 39 4 x a ne
187 | Puolakka.......... 61539 | 48 115 ‘3 2 ©
188 | Water-vuori... ...| 62 4 32 | 43 37 37 = Se
189 | Jywäskyla......... 62:12 51145002 0, Ss aS
190 | Ruuhi-Maki....... | 62 1251 | 4351 2 2 5.2
191. | Eaja-vuorni..-...... | 62 15 31 143 on 090 Ss LE
192 | Multa-Mäki........ | 62 27 48 | 43 21 4 2 25 |
193 | Ohi-Mäki.......... | 62 29 35 | 43 56 55 m a3
194 | Silmut-Maki.... ..| 6239 5 | 4259 0 | © BY N
195 | Kilpi-Mäki......... | 62 38 5 | 44 26 3 En PA |
196. Ma Mil aos oe | 62 42 40 | 43 27 53 3 Ss |
197 | Liston-Mäki........ | 62 51 33 | 43 45 49 | 3
198 | Wesa-Mäki ......., | 62 55 47 | 44 8 49 | x |
199 | Honka-Mäki....... | 62 57 30 | 44 46 42 | 2 |
209 | Lehto-Mäki........| 63 13 52 | 4 7% 0 A
201 | P6l6-Mäki........ | 63 22 48 | 44 47 44 | 2 |
202 | Pihiajan-Mäki..... | 63.20.15 1 24 435) ee |
203. Mali 220 8: | 63 937 47 | 44 44 30 a
204: | Kivi-Mild. . 19:56: 06 1 1 a
205 | Kulven-Mäki... ...| 63 46 5 | 45 18 29 A
206 | Salisän-Mäki ...... 63 49 21 | 44 56 57
207 , Naaras-Mäki....... | 63 55 49 | 45 29 33 |
| | | |

 

 

 

 
