 

160

Quant à son opinion personnelle, M. Hirsch avoue que le projet d’une Conférence
dans deux ans à Paris lui paraît offrir quelques inconvénients. Non seulement il sait par
expérience que le milieu d’une Exposition universelle, qui se prête siadmirablement aux réunions
de Congrès de tonte espèce, n’est pas précisément très favorable aux travaux d’une Conférence
officielle de délégués des Gouvernements, appelée à s'occuper de sujets d'un caractère très
spécial et essentiellememt scientifique , mais l’objection principale lui parait résulter de
l'impossibilité où nous nous trouverons probablement dans deux ans de soumettre à la
Conférence quelques résultats, même provisoires, des observations sur les mouvements du
Pôle, qu’on aura commencées au milieu ou vers la fin de l’année prochaine, dans les sta-
tions internationales.

En tout cas, il faudrait laisser au Bureau de l’Association le soin de décider, suivant
les circonstances et en temps opportun, l’année de la prochaine Conférence.

M. Helmerl, en vue de la possibilité de réunir la prochaine Conférence seulement
dans trois ans, exprime lé vœu, émis de différents côtés, que les Rapports nationaux soient
rassemblés et publiés chaque année. Ce serait trop long de ne rien apprendre pendant trois
ans sur les progrés des travaux dans les différents pays.

Le Vice-Président, M. le Genéral de Stubendorff, remplit un devoir agréable en
exprimant 4 Sa Majesté le Roi et au Gouvernement du Wurtemberg la plus profonde recon-
naissance de l’Assemblée pour l’accueil si gracieux que la Conférence a recu 4 Stuttgart. De
même, il est assuré d’être l’organe de tous les membres, en remerciant les deux collègues
wurtembergeois, MM. Hammer et Koch, de l’infatigable empressement dont ils ont fait

preuve pendant toute la durée de la session.

M. Helmert remercie également le Bureau et le Secrétaire perpétuel de leur travail
laborieux et impartial.

La séance est levée à midi et quart, et la session de la XIIe Conference generale
déclarée close.

À

ve Mel |

Il

Pb mike

ND (tour

À a a bas a M Aa a pv

Dire tas noms nil

 

 
