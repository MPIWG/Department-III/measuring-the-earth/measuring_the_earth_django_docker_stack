 

166

e

Hohentwiel -— Wiesenberg — Röthi — Lägern in die möglichst günstige Lage
zu einander gebracht, wobei die Schweizer Centrirungen auf dem Feld-
bergi.S. und auf dem Hohentwiel nach den gleichnamigen Punkten des
Rheinischen Netzes nachgerechnet und richtig gestellt wurden. Die Ueber-
einstimmung der Winkel ist gut; als plausibelste Differenz in den Seiten-
längen wurde ermittelt:

Schweizer Grundlinien winus Bonner Basis
== + 64 fink. d. ©. St. d. Log.

Der Anschluss der Schweiz an Italien giebt zu manchen Bedenken
Anlass. Beschränkt man sich auf die Seite Limidario (Ghiridone) — Menone,
die gewöhnlich als einzige Anschlussseite aufgeführt wird, so hat man:

Basis am Ticino minus Schweizer Grundlinien
= 1-5) Binh. d. 7. St. d. Log.

Ferner erhält man aus der Seite Strassburg — Donon (T. P. d. G. J.):

Oberhergheimer Basis minus Bonner Basis
= 24 fink, d. 7. St. Gd. Los,

und aus der Uebertragung der Oberhergheimer Basis auf die Ensisheimer
Basis:
Ensisheimer Basis minus Oberhergheimer Basis
— = 73 ion. d' 1.:ot. d. Log,

und endlich aus der Seite Strassburg — Donon (frz. T. P.):

Alte Basis von Melun minus Oberhergheimer Basis
= — (0 Einh. d. 7. St. d. Log.

Wenn man fiir die drei letzten Vergleichungen andere gemeinsame
Seiten benutzt, so andern sie sich zum Theil recht erheblich; jedoch sind die
aufgeführten Differenzen die sichersten. Ausserdem werden noch ver-
schiedene Reductionen, besonders die auf das internationale Meter, diese
Vergleichungen beeinflussen.

Aus der Zusammenstellung und Discussion der astronomischen
Bestimmungen mag unter anderm erwähnt werden, dass es gelungen ist,
das 1863 von Visrarceau bestimmte Azimut in Strassburg nach dem alten
französischen Punkt Donon in hinreichend genauer Weise (etwa auf 072
genau) auf den neuen Punkt Donon des Geodätischen Instituts und der
Königl. Preussischen Landesaufnahme zu reduciren, so dass Strassburg
nunmehr ein brauchbarer Laplace’scher Punkt geworden ist.

dd! 1

wp idem A ma A le a Mall ki |

ak ihe

uno ann |

 

2 ia main MN |

 

 
