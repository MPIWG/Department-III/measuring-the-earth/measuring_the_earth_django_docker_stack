 

Le marégraphe qui a fonctionné le premier en Italie est celui de Livourne (1858).
Viennent ensuite ceux de Venise (1872), de Porto-Corcini, prés de Ravenne (1872), de Naples
(1878). Les autres sont en activité depuis moins de 10 années !.

PAYS-BAS.

Nous devons 4 M. van de Sande Bakhuyzen les renseignements suivants sur les ma-
régraphes de son pays.

Le Service des Ponts et Chaussées qui les a sous sa direction en a placé 20 sur les
cötes de la Hollande ; plusieurs autres ont été installés sur les cours d’eau, mais ils n’inté-
ressent pas la géodésie.

La plupart de ces marégraphes sont enregistreurs, le contrôle de la hauteur des cour-
bes est fait tous les jours.

‘Les relevés aux marégraphes du Helder, d’Ymuiden et de Hock von Holland ont
été dépouillés en vue de déterminer les coefficients de l'analyse harmonique.

Au Helder, où les premières courbes ont été obtenues en 1852, on a calculé les coef-
ficients pour 16 années, réparties depuis cette date jusqu’à l’époque actuelle ; ils n’ont pas
montré de différences sensibles. En ce qui concerne les autres marégraphes, le dépouillement
a été fait suivant les anciennes méthodes.

Il ne semble pas résulter des déterminations du niveau moyen un mouvement appa-
rent du sol; cette égalité des chiffres du niveau moyen paraît aussi démontrée par des obser-
vations qui, pour Amsterdam, remontent à la fin du XVII siècle.

M. van de Sande Bakhuyzen a trouvé dans les courbes du marégraphe du Helder
une onde marée d’une période de 431 jours, correspondant au mouvement du pôle terrestre,
il a signalé ce résultat dans les «Astronomische Nachrichten ». Il paraît avoir été confirmé en
Amérique. On trouve dans l'annuaire de l’Institut des ingénieurs et dans les publications des
Ponts et Chaussées les chiffres annuels des hauteurs obtenues dans les marégraphes des
Pays-Bas.

NORVÈGE.

Les marégraphes installés sur les côtes de la Norvège sont au nombre de huit. Il n’y
a aucune modification dans leur situation depuis la présentation du rapport du Marquis de
Mulhacen.

Les résultats préliminaires ont été publiés par la Commission géodésique dans cinq
volumes du « Vandstands Observationer », 1882-1893.

A Vheure actuelle on dépouille les courbes de marées pour arriver à déterminer les
diverses ondes luni-solaires.

! Le délégué italien à la Conférence de Stuttgart a donné dans son rapport des renseignements
complémentaires sur les marégraphes italiens. Ceux-ci m'ont été fournis par le Colonel Coen, sous-
directeur à l’Institut géographique-militaire.

 

Mamba | didi oh

eed came aa

run

il

Ja

 

Dicota masses hi |

 
