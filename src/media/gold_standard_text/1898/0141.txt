 

een

 

dl) LUN

ALL Qi

AM ATI 1, AL A FR COTE

tout le travail doit étre dirigé par une seule personne. Toutefois, si une Commission devait

être nommée, il ne s’y opposerait pas pourvu qu’il ett Phonneur d’en faire partie afin de
pouvoir travailler avec elle.

M. Hirsch partage l'opinion de M. Helmert, d’après laquelle une commission,
composée de plusieurs membres appartenant à des pays différents, ne serait pas bien qualifiée
pour entreprendre des travaux de calculs d'ensemble très compliqués. C’est précisément pour
cette raison qu’il a été heureux que, lorsqu'on à fondé l’Association pour la « Mesure des
degrés en Europe », le gouvernement prussien a gracieusement mis à sa disposition son
Institut géodésique pour lui servir de Bureau central ; et puisque nous avons le bonheur de
posséder à la tête de cet établissement l’un des premiers géodésiens de notre époque, notre
organisation internationale a réellement peu à désirer sous ce rapport. Mais, comme la motion
de M. Preston contient cependant plusieurs points et désirs importants, il parait 4 M. Hirsch
que le mieux serait de prendre la résolution suivante : la seconde motion de M. Preston
est renvoyée au Bureau central, et ce dernier invité à en tenir compte autant que possible.

Celle proposition est adoptée à l'unanimité.

M. Bouquet de lu Grye fait lecture du rapport spécial sur les observations maréogra-

phiques exécutées dans les différents pays et sur les résultats qu'on peut en déduire.
(Voir Annexe A. V).

Puisque la Conférence dispose encore de quelques instants, le Président donne la

parole à M. Darwin pour présenter un court exposé des travaux récents, exécutés en Angle-
terre et aux Indes.

M. Darwin déclare que, n'ayant pas préparé un rapport complet sur la matière, il ne

lardera pas à le rédiger, après son retour en Angleterre, avec le concours de plusieurs de

ses collègues et l’enverra tout imprimé au Secrétaire pour la publication des Comptes-
Rendus. (Voir Annexe B. XXL.)

En outre, il a promis 4 M. Bouquet de la Grye de lui fournir une notice sur les
mesures maréographiques en Angleterre, dont il s’est occupé d’une maniére particuliére.

M. Darwin saisit cette occasion pour exprimer toute sa satisfaction de ce que l’Angle-
terre fasse partie de l’Association g&odesique internationale.

M. le prof. Hummer fait encore quelques communications aux membres de la Confé-
rence au sujet des excursions projetées.

Comme le lendemain a été réservé a d’importantes réunions de Commissions spé-
ciales, le Président fixe la prochaine séance de la Conférence à lundi 10 octobre, à 9 12h.

La séance est levée à 19 h. 95.

 
