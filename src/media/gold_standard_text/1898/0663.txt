 

ii ee

 

na art

TORRE a EI DIET RMI it

MIRE

Schwerkraft mit Pendelapparaten.
(Siehe Beilage A. VI.). site

Frage des Herrn Lallemand, inwie-
weit die Schwere-Anomalien durch
Unregelmässigkeiten des Geoids zu
erklären sindund Antwort des Herrn
Helmert CH PAF a ri 0

Discussion über die Verbindung der
Pendel - Stationen, und Beschluss
diese Verbindung durch Subventionen
zu fördern .

Bericht und Vorschläge der aa
Commission über die Neu-Messung
des Gradbogens von Peru, von Herrn
Preston .

Discussion und Bosahlligae har in
Vorschläge.

Bericht der Finanz- onen re
tet von Herrn Forster.

Durch Abstimmung nach ante
nehmigt die General-Conferenz die
Rechnungen der Jahre 1896 und 1897,
ertheilt dem Director des Centralbu-
reau’s für seine Verwaltung volle
Entlastung und nimmt die übrigen
Vorschläge der Finanz-Commission
einstimmig an

Siebente Sitzung, 11. October 1898

Mittheilung des Herrn Helmert über den
allgemeinen Arbeitsplan des Central-
bureau’s fürdienächsten beiden Jahre

Discussion und ia dieses
Programms ;

Beschluss der ( an. eine No des
Herrn Lallemand die Variatio-
nen der Nivellirlatten,nach den Unter-
suchungen des Oberst Goulier, in
die Verhandlungen aufzunehmen.
(Siehe Beilage C. I.)

Der Vorschlag a Herrn Bogda
den Wunsch nach einer Verbindung
der deutschen und französischen Drei-
ecks-Ketten, unter dem Parallel von
Paris, auszusprechen wird genehmigt.

Der Vorschlag des Herrn Bakhuyzen,
die Landes-Berichte, welche beim
Bureau niedergelegt sind, als gele-

Pas à

45

15-46

46-48

48

49

50-53

37-58

08

 

sen zu betrachten, wird, nach statt-
gefundener Discussion, vom Antrag-
steller zurückgezogen . :

Bericht von Herrn Rosen über die ae
beiten in Schweden. (Siehe Beilage
BC)

Hieran anschliessend , Bemerkungen
der Herren Helmert aa von Stuben-
dorff über die Niveau- Differenzen
der Ostsee. :

Herr Tinter verliest den Besteht es
Herrn von Kalmädr über die von öster-
reichischen Seeofficieren in den Jah-
ren 1895-1898 ausgeführten Schwere-
Messungen. (Siehe Beilage B. Xa).

Herr Tinter verspricht den Bericht des
Militär-geographischen Instituts dem
un einzusenden. (Siehe Beilage

DB. X.b)ri. u.

Bericht des Herrn Gain Ten die kee
beiten in der Schweiz während der
Jahre 1896-1898. (Siehe Beilage B.XI.)

Bericht des Herrn von Stubendorff über
die Arbeiten in Russland für die Jahre
1896 und 1897. (Siehe Beilage B. XID.

Nekrologien der Generäle Forsch und
Stebnitzky, von Herrn von Stuben-
dor ff. eee

Bericht des Herrn Hane: a ike N
beiten in Württemberg. (Siehe Bei-
lage B. XIII). eee

Herr Ferster dankt, im Namen der
Versammlung, dem Herrn Präsi-
denten Faye, welcher genöthigt ist
abzureisen, für seine unparteiische
Geschäftsführung

Achte Sitzung, 12. October 1898.

Bericht des Herrn von Bodola über die
Arbeiten in Ungarn. (Siehe Beilage
iB. XN):

Bericht des Herrn Bonn ren ‘dic
Arbeiten des französischen General-
Nivellements. (Siehe Beilage B. IV»).

Bericht des Herrn Henneguin, über die
ee in res (Siehe Beilage

> XV): .
Kur Mittheilung des ie rn Bruhn

Pag

98-59

59

60

60

62

63-66

63

63

64

 
