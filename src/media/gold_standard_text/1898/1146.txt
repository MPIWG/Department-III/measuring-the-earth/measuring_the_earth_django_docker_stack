   

 

 

a >

XVIII. — Suède.

urn a arena een

 

 

  
 
  
  
  
 
  
 
  
  
  
 
 
 
 
  
 
  
 
  
  
  

| | | |
INDICATION |. Seen | DIRECTEURS |
DES POINTS LATITUDE peel PUD Ee EPOQUE | ET OBSERVATEURS INSTRUMENT:
I
N | | =

Iggon Re one) oo bia <0 325 | 60° 52’ 29” DT 21 1871 | Elfoing. | Littman.
Skamningsberg ....| 61 0 9 | 34 53 50 | 1870 | Rosen. | id.
Stugutjarnsberg ... | 61 10 38 | 34 24 29 | 1864-71 | Stecksen, Rosen. | Littman, Repsold.
Sundmarnaset ..... mi © 3-| 3450521) 18% | Rosen. | Littman.
CSST 61 15 49 | 3447 1 1864-71 | Stecksen, Rosen. | Littman, Repsold.
Ranboberg ........ 61 19 37 | 34 28 22. 1865 | Stecksen. | Littman.
Prestgrund........ 61 20 47 |-35 0 2| 1864 id. | id.
Augaasen.......... 61 33 53 | 34 36 52 | 1864 | id. | id.
Husten. .........: 61 29 58 | 34 9 34 | 1864-74 | Stecksen, Rosen. | Littman, Repsold I.
Storasen .......... 61 30 59 | 34 31 46 | 1874 | Rosen. | Repsold I.
PDRCESIS 61 58 33 | 34 25 46 1874 | id. id.
Arderviksberg..... Oras 0) 35 4 1 | 1874 | id. Fe
Glarsberg ......... 61 52 26 | 34 4633| 1874 | id. ae
Jatteholmarne..... Opi 23 |35 819) 18% | id. | id.
Mipasen BI | 3424 38, 187% | id | =
Hoitoarne......... 61 59 26 | 35 10 2 | 1875 | id. | id.
Bolesklack ........ O27 12201 344648; 18%. :| id. se
Prend. 6213 0} 352059] 18% | ‘id. | id.
Brantbers......... 62 7213) 345925) 35 | ad. | id
Astholmen......... 62 24.18 | 35 2259) 185 | id. | Fe
Felemala.:......:. G2 38 21 | 35.36 36] 1875 | id. id.
fa  . 62 36 42 | 35 37 13 is | id. id.
Mocksjoberg....... 62 44 55 | 35 33 O | 1875 | id. id.
eee 62 42 2 | 35 44 54 1895 | id. a
Hvalshufoud....... | 62 47 43 | 35 46 12 1875 | id. | id.
Folkaberg......... 62 52 47 | 35 39 8 1875 id. id.
Ringkalleberg..... | 62 53 36 | 35 59 14 | 1875 | id. id.
Dalsberg.......... PSS SA) 85 | id. id.
Mjeliomsberg...... G2 709 16 | 36 3535| 1874 | Schartau. Littman.
DÉUMICDErS 22... .- | 63 4 38 | 36 1 6 1874 | id. id.
ie. 63 3 36 | 36 20 17 184 | id. se
Skaftedalsberg..... 63 7 32 | 36 9 16) 1874 | id. | id.
Faleberg .......... 63 13 15 | 36 20 18 | 1876 Rosen. Repsold I.
Alkaberz.........: | 63 13 49 | 36 37 57 | 1876 | id. | id.
NSbers 0. 63 18 16 | 36 19 45 | 1874 | Oldberg. | Littman.
Backeber2..-...... | 63 25 42 | 36 31 8 1874 | id. id.
Skalbadan......... 631515|365152| 1874 | id. | id.
Gabrielsberg....... 03 3 113856 29 | 1874 | id. | id.
Storbadan.......:.. 63 24 41 | 3115 31 1874 | id. id.
sh...) 63 29 10 | 37 33 48 ‘si | id. | id.
Degerbersg.......:. | 63 42 25 | 37 20 26 | 1874 | id. id.
Pad = 3. o. P5656 | 3741 91 1a | id. id.
Djeknebolsklint ...| 63 46 27 | 37 46 18 | 1872-73 | Rosén, Oldberg. Repsold I, Littman.
Bredskar. ....... 033944 | 3159 21 1873 | Oldberg. Littman.
UMSADErRS -......... 0849 253791 52 | 1872 | Rosen. Repsold 1.
&rubber?........... 68 50 55 | 57 52.41 | 1872 | id. id.
Base de Umea (Sud), 63 48 1 | 37 53 29 | 1872 | id. | id.
Base de Umea (Nord)| 63 49 25 | 37 51 13 | 1872 | id. | id.
Robacksberg....... 63 47 44 | 37 49 45 | 1872 | id. id.
Kiipbbere ......... 63 50 55 | 37 44 44 | 1872 es id. id.
Ersmarksberg...... 63 52 0.| 37 57 29 | 1873 | id. id.
Hayes ......:. bee 6 |) 637 1). 18% | id. | id.
Walberz........... 63 5955| 3741 28 | 1873 | id. | id.
Soderviksberg ..... 64 2 32 | 31 3322| 1873 | id. | id.
Orrberg..... a, “79 3721|: 188: | id. | id.
DODerS ee. 64.16 53:| 37.58 25 | 1873 | id. | id.
Vigerilint 2. |. Ged? 53 | 3821 44. 187. | id. | id.
Gexusalklanken ....| 64 22 46 | 38 21 57 | 1871-73 | Oldberg, Rosén. Littman, Repsold I.
Klintsjoberg oo 64 12 43 | 38 40 42 | 1873 | Oldberg. Littman.
TIDSDEIE.. .......... 6425 813831 57\ 1871 id. | id.

 

 

Bredberg.......... | 64212139 217 | 1871 id. | id.

sur tam Lis ha
