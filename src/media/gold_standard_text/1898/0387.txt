 

[Pits rem ee

 

Kia tet

Fun

Wij

DOTE NEC OUIIL EL LOL

369

 

placés. En 1888, nous avons publié dans les Comptes-Rendus de l'Académie des Sciences
(séance du 19 nov. 1888) les moyennes annuelles pour les ports de Brest, de Cherbourg et
du Havre.
Ce relevé qui, pour Brest, portait sur 35 années, avait d’abord été fait en prenant
la moyenne de toutes les hauteurs, de quart d'heure en quart d'heure, après les avoir cor-
rigées de la pression barométrique et de l'influence due à la direction et à la vitesse du vent ; ce
travail considérable à pu ensuite être simplifié en ajoutant un chiffre constant à la moyenne
des hautes et basses mers, auxquelles on avait fait subir les deux corrections précitées.

Ce chiffre constant varie beaucoup d’un port à un autre; à Brest où la sinusoïde est
presque régulière, il est de + 29 millimètres : à Cherbourg de 48 millimètres; au Havre de
176 millimètres.

Nous avons dû également corriger les résultats de l'influence d’une petite onde dont
la durée est de 18 années et à Brest nous avons trouvé que la hauteur de la pluie influait
sur le niveau de la mer. Le marégraphe placé à l'embouchure d’une petite rivière, la Pen-
feld, a ses courbes surélevées lorsque les eaux douces sont plus abondantes.

La formule de cette correction trouvée empiriquement es! (M, 0900) 2e 0 ie,
I étant la hauteur annuelle de la pluie.

Comme suite aux tableaux publiés en 1888, nous donnons, pour Brest, le Havre et le
Socoa, les chiffres obtenus pendant les onze dernières années.

       

 

       
  

 

 

 

   
     
    
       
      

 

 

 

     
   
            
        
            
  

  

 

 

 

 

 

 

 

 

  
 
  

 

BREST LE HAVRE LE SOCOA
Sera = a me Zr FERNE N EE RS Tr N
H. moy. | | H. moy. a H. moy.
corr. de la) Onde lk Vent Pluie corr. dela | Onde Weal ‘ | corr. dela} onge
Pression |Lunaire = soe Résultat | Pression |Lunaire Résultat | Pression i Resultat,
| paromé- | millim. | Millim. | millim. baromé— | millim, | Millim. baromé— | Lunaire |
| trique trique trique |
eS | i
iS Mpa | - S Le ma mes : A
1887 | 2,461 + 1|—. Ol 10] 4.4970 4,753 |+ 1|+ 8] 4°768 [27306 |-L 1 12,307)
| 88 HOT} = EE N 155 els) | | Ol 817 520 | 1 i, 32a
| 89 M49 | — 2|—- 104 2] 439 803 | — 2|-+ 18] 819 320 = 211 818.
90 44h | —Q)-- 181+ 9| 496 806 | — 2/4 12) 826 323 |— 2) 321)
oi 464 | — 8 |— 26 — 2 453 807 | — 3 + 18) 822 000 ol at
92 474|— 3|— 05|/— 3] 468 194) | 0122.86 306) |= 3, Bue |
8 499 |— &A|-—- 9)-—— 5) 181 829 |= MER | 692 920 | — Æ | 316
94 480 | — 5|— 21/— 8) 446 802 | — 5|+ 10) 807 311 | — 5 | 306
95 487 | — 5 OE 2 756 | — 5|-+ 28] 779 Sslaen 5 297
96 468) — 3 | Alt 5| 26 768 | — 8|+ 12) 771 825 |— 8) 399
+ 097 501|— 2|— I5l-E 3) 487 750 | 2480 778 | 83 ad
11892 ee Moy. 4,458 Moy. 4,803 Moy, 2,313

 

 

 

  

D'après les résultats obtenus à Brest, antérieurement à l’année 1887, on ne pouvait
en réalité admettre que des mouvements presque insensibles dans le sol (!/,; de millimètre
d’exhaussement par an). Si l’on compare la moyenne des hauteurs correspondant à l’année
1868(4/476), à celle de ce tableau (4458), une différence de 18 millimètres en 24 ans donne-

er

 
