fi Sees ere ere

 

CNT 1

no Fir I HAR ica ail ia 1) Wit

 

 

 

 

— 427 —

XV Suede.

 

 

| REMARQUES

 

 

 

 

N° en LATITUDE LONGITUDE| EPOQU De R b
DES POINTS QUE ET OBSERVATEURS | ne
1 | Falsterbo.......... 55023° 2") 30028'59"| 1839 Häggbladh. Littman.
2: Malmo cei 55.30 25 | 3019 1839 id. id.
3 | Romeleklint....... 5537.21 13. 70 1839 id. id.
&Bund. 0.2... 328, 8 80.5 ae 1839 id. id.
5 | Loberod........... by A 28) | Si © 67 1839 id. id.
6 |-Dagstorp.......... 5d 49 49 | 30 43 50 1839 id. id.
7 | Uranienborg....... 52.54.28 730 2 47 1839 id. id.
8 | Grifsvehacken..... 5p oF 30. 41 AS 1840 id. id.
9 | Helsingborg....... 56 250 | >30 21 ol 1840 id. id.
19, Hockull. ee 56 17 16 , 30) 11 36 1840 id. id.
11 | Hogalteknall....... 56 22.35 | 80 4225 1840 id. id.
12 | Knosen.. 56 27 48 | 30 24 20 1867 Lindhagen. Ertel.
13°3P Boa... | 56.37 459 | 30.45 3 1867 id. id.
14 | Wilsharad......... 56. 4] 28 | 30 23 55 1867 id. id.
19. Nyması: 2.2... 6 49. 6 2028 2 1841 Haggbladh. Littman.
16 | Hinnaklo.......... 56 46 13 | 30 48 26 1841 id. id.
17 | Fastumper......... 56 53 6 | 30 34 47 1841 14e id.
18 | Himmelskullen..... 56 53 292 7.30: 1928 1841 id. 10.
19 SSTanor ee 55 35 56 55 28 1,30 134 1881 id. id.
20 | Askomberg........ 57 040 30 22% 1841 id. id.
21 | Folkaredsklef...... 57.012 80 2 1 1841 id. \ id.
22  Elmebere. . .. 57 635 | 30 11 16 | 1841-67 | Häggbladh, Lindhagen. | Littman, Ertel.
23 | Apelviksas......... 52 539 12959 5 1841-67 id. id.
24 | Borstaras.......... 52.15. 2.20.09 1867 Lindhagen. | Ertel.
25 | Nidingen.......... 57 18 15 729349 1867 id. id.
26 | Fexleberg......... 57.2220 130. 320 1867 id. id.
it RON ee 57.29.14 129729753 1867 id. id.
28 | Hwalas 7 1 32293 72942759 1867 id. id.
29 | Ytter Fistlarne....| 57 30 40 | 29 23 33 1866 Stecksen. Littman.
20 WIN as. se 57.3759 2916 5 1866 id. id.
31 | Westerberg........ by 40) 2 | 29 BS 530 1867 id. id.
32 | Marstrand......... 57.53 12: 29 14 38 1885 Rosen. Repsold I.
oo | Aleklatf..........; 57 54 16 | 29 40 56 1867 Stecksén. Littman. |
34 | Boxviks sadel...... 58 121129 95 1867 id. : id.
35 | Brandaberg ....... 58 8 35,29 81.5 1867 id. id.
36 | Lejdeberg ......... 531838 | 2921749 1866 id. id.
D Salon es 59 20,22 | 28 52 17 1867 id. id.
38 | Ornekull.......... 58 307i) 129 4 2 1867 id. id.
39., Waderö 58 34 53 284219 1867 10. id.
40 | Amundshatt....... 58 47 0.29 7385| 1867 id. id.
41 | N* Kosteron....... 58 54 12 | 28 40 18 1866-67 id. id.
42 | Lofverasfjell....... bo 050,291 9 1866 ide id.
43 | Wagnarberg....... 59 2 3 28 48 20 1866 id. id.
44 | Dragonkull........ 59 6202858 7 1866 id. id.
45 | Hjorten ........... 58 38 25 | 5020 5 1844 Lilliehööck, Skogman. id.
46 | Aleskutan......... 58 39 47 | 30 20 28 1844 id. id.
47 | Wingen........... 5851 44 30,18 40 1844 id. id.
48 | Maken............ 5S al S | 30 20 46 1847 Arosenius. id.
49 | Busen... 1. 59 2 16 150 28 51 1847 id. id.
50 | Westanaberg ... ..| 58 57 22 | 30 15 3 1850 id. id.
Di Golekull 2 59 489930 10% 1850 id. id.
52 | Sjogeras........... 59 416 20 34 50 1850 id. id.
53 | Daltjarnshojden....| 59 14 35 2038 5 1850-51 id. id.
54 | Halneknatten...... 59 1358 | 30.1123 1850 id. id.
55 | Svarttjarnshojden..| 59 20 37 | 30 22 1851 id. id.
56 Elgdalshojden ee 59 26 18 | 30 39 52 1851 id. id.
57 | Saxehojden........ 59 24 171 3025 0 1851 id. : id.
58 | Stutekletven. .- — 59 25 22 | 30 49 20 1852 id. id.
ae | 59 30 48 | 30 48 16 1852 id. id

 

 

 

 

 

 

 

 

 

 

 

 
