 

FR ee

 

en]

i itl

gi

a 174
Die Platten sind inzwischen sämmtlich ausgemessen und die Resul-
tate dieser sowie der gleichzeitigen Beobachtungen der Herren Scunauper
und Dr. Hecker am optischen Zenitteleskop des Geodätischen Instituts
bereits abgeleitet worden.
Gegenwärtig ist die Discussion der auf photographischem und auf
optischem Wege erhaltenen Resultate im Gange. Es kann aber schon
jetzt mitgetheilt werden, dass sich der mittlere Fehler der Beobachtung
eines Sternpaares übereinstimmend bei beiden Beobachtungs-
methoden zu Æ 0'16 ergiebt, wobei allerdings zu beachten ist, dass die
Beobachtungen auf optischem Wege an einem kleineren und weniger stabilen
Zenitteleskop von nur 68™™ Oeffnung, die auf photographischem Wege
aber an einem sehr stabil gebauten Zenitteleskop von 185™™ Oeffnung aus-
geführt wurden. Der mittlere Verlauf der Polhöhe stimmt für beide
Beobachtungsreihen bis auf die Hundertstel-Secunde überein, und es
konnte auch für beide Reihen die nahezu völlige Abwesenheit von
Tagesfehlern nachgewiesen werden. Auch stimmen die Curven hin-
sichtlich ihrer Form innerhalb 0°02 mit demjenigen Verlauf der Polhöhe,
der aus der Gesammtdiscussion des anderweit vorhandenen Beobachtungs-
materiales folgt (vergl. den Bericht über den Stand der Erforschung der
Breitenvariation im December 1897), so dass man abgesehen von etwaigen
constanten Einflüssen auf die Abwesenheit variabeler Refractionsstörungen
während der dreimonatlichen Beobachtungsperiode schliessen kann.“

ALBRECHT.

Wie schon in vorstehendem Specialbericht angedeutet ist, haben auch im
Jahre 1897 die Herren Scuxauper und Dr. Hnoxzr die Breitenbeobachtungen am
visuellen Zenitteleskop des Geodätischen Instituts fortgesetzt, so dass gegen-
wärtig schon eine continuirliche 4-jährige Reihe vorliegt, deren Ergebnisse zur
Zeit einer zusammenfassenden Bearbeitung unterworfen werden.

In Bezug auf die localen Untersuchungen für vier internationale
Breiten-Stationen auf demselben Parallel leitete mich der Gedanke, dass es noth-
wendig sei, zunächst zu erkunden, ob sich vier solche, in jeder Hinsicht wohl
geeignete Stationen fänden. Erst dann würde es möglich sein, sich endgültig für
diesen Plan zu entscheiden. Von den verschiedenen Stationsgruppen, welche Herr
Professor Arsrecur discutirt hatte, wurde die Gruppe Dover — Ukiah — Midsusawa —
Cagliari in 39° 8’ Breite gewahlt.

Für Nordamerika gab mir Herr Trrrmann unter dem 31. Marz d. J. einige
vorlaufige Informationen, auf Grund deren ich mich am 5. Mai an den Super-
intendenten der Coast and Geodetic Survey U. S. A., General Durrizrp, mit einer
ausführlichen Darlegung der Verhältnisse wandte und die Bitte aussprach, dass für
Dover und Ukiah, sowie eventuell für benachbarte, geeignete Gegenden Erkundungen

angestellt werden möchten.
2 — 22

acceso

eee eet

1

 

pen

 

 
