Hemer ee en

TNT 1 PONT

in

ji

ili

TET HAA HF ERIE LL à

me |

SE em STH

 

 

 

El _

 XVIIL — Suède.

 

 

 

 

 

 

 

 

 

 

 

yo] INDICATION À cammmune LoNGITUDE] ÉPOQUE N I
DES POINTS : ERDE ET OBSERVATEURS N

239 | Hokmarksberg..... 64°26) 18") 38°56! 45” 1871 Oldberg. Littman.
240 | Biyberg........... 64 31 23 | 38 43 11 1871 id. id.
241 | Burealiden ........ | 64 35 52 | 38 54 57 | 1871-76 | Oldberg, Rosen. Littman, Repsold 1.

242 | Bensberg......... 1.64 35 30 | 88 29 14 1871 Oldberg. Littman.
243 | Noppelberg........ 64 46 10 | 38 38 41 1871 id. id.
244 | Romelshallan...... 64 54 16 | 39 0 34 1871-76 | Oldberg, Rosen. Littman, Repsold 1.
245 | Degerberg........- 65 4 59 | 38 38 14 1870 Oldberg. Littman.
246 | Ronnskar ......... 1:65.29 881345 1870 id. id.
247 | Vartraskberg...... 6510. 61539 10 59 1870-78 Oldberg, Rosen. Littman, Repsold I.
248 | Kalahatten ........ 65 19 52 | 38 43 41 1870-78 id. id.
249 | Falkberg.......... "69:23 10189 1.36 1878 Wolyn. | Repsold IT.
250 | Base de Pitea (Sud)! 65 20 34 | 38 53 56 1878 Rosen. | Repsold I.
251 | Base de Pitea (Nord)| 65 21 4 | 38 52 1 1878 id. cide
252 | Hogherg.......... 65 18 33 | 38 52 5 1878 id. id.
253 | Golaberg.......... 65.1223 | 3852 4 1878 id. id.
254 | Lappviksberg...... 65.20 21,39 16 14 1870-78 Oldberg, Rosén Littman, Repsold 1.
200 | Hyitberg.. ........ 65 37 49 | 39 142 1870 Oldberg. Littman.
256 | Mattsundsberg..... 65. 33.20 \ 39 30 13 1870 id. id.
257 | Timmerberg....... 65 4556, 2939 3 1870 id. id.
258 | Hertzoberg........ 65 36 39 | 39 51 38 1870 id. id.
259 | Blisberg........... 65 48 52 | 40 13 19° 1870 id. id.
260 | Hindersoharen..... 65 36 11 | 40 25 36 1870 id. id.
261 | Palangeberg....... 65 48 22 | 40 36 58 1870 id. id.
262 | bikskar 0.2.2 65 38 45 | 40 42 23 1869 id. id.
263 , Golihatten......... 65 48 12 | 41 3 53) 1869 id. id.
262. 1,.Halso 2.2... 65 39 38 | 41 13 32 1869 id. id
265 | Tornea furo....... 65 44 43 | 4] 43 33 1869-75 | Oldberg, Rosén Littman, Repsold I.
266 | Aijanpajanletto....| 65 35 32 | 41 37 13 1869 Oldberg. Littman.
27 Bulua....... 2. 65 41 40 | 41 56 56 1869 id. id.
268) orne 65 49 55 | 41 49 32 1869 id. id.
OO A108 -....0 6540 11 | 4213 18 1875 Rosen. ‚Repsold 1.
210 | Dragskog.......... 58 29 20 | 34 35 30 1820 Cronstrand. Reichenbach.
271 | Qvarseboklint ..... 58 38 19 34 1831 1820 id. id.
272 | Grindtorpsklint....| 58 28 9 | 34 14 56 1820 id. id.
273 | Svaltemo.......... | 58 39 6 | 33 44 41 1820 id. id.
274 | Muskedunder...... ı 58 30 39 | 33 51 48 1820 id. id.
275 | Brantebraten...... 58 31 AU | 3826 27 1820 id. id.
276 | Gostabacke........ 58 25.54 | 33 35 18 1820 id. id.
217 | Asdymlingen....... 58 22 3| 33 952 1820 id. id.
278 | Westerlosa ........ 58 24 22 23. 035 1820 id. id.
279 | Korpeberg......... | 58 31:13 | 33 10528 1820 id. id.
280 | Baleberg......... 58 86 17 | 824155 1820 id. id.
281 l'Ombers. 58 18 30 | 32 18 57 | 1819-20 id. id.
233 0b... 58 35 57 | 32 36 36 1819 id. id.
283 | Backudden ........ 5 31 7 32 IE 53 1819 id. id.
284 | Glattenas.......... 58 39 32 132 18715 1819 id. id.
280 | Eskeberga......... 58 36 29 | 32 36 56 1819 id. id,
286 | Wavere 0.5... 58 32.3122. 022 1819 1d. id.
287 | Lunnakulle........ 58 22 54 | 31059 22 1819 id. id.
288 | Hallasen . - 58 16 28) ol 2031 1819 id. id.
289 | ©. Billingen....... 5828 32.310 24 A 1819 id. id.
290 | Hoghult........... 58 31 43 | Bil Sl Bll 1819 id. id.
201) Ulan 58 40 19 | 31 31 46 1819 id. id.
292 Bergsgarde........ 58 30 22 ol 20.23 1819 id. id.
293 | W. Billingen ...... 58 27 59) 31 24 47 1819 id. id.
294 | Kinnekulle........ 58 36 0] 31 4 49 1818 id. id.
DOS ES KE Be) a NO [eal 6 19) 1818 id. ide
296 Langeberg......... 5S 2 80) 30 30) 4 1818 id. id.
297 | Mosseberg......... 58 12 53 1 31 8421! 1818 id. id.

 

 

 
