TI men me nn

 

DLR

Jami litt

VOR I MEL UIT 1

MIE LI,

   

 

 

 

 

— 810 —

XII, — Portisat,

 

Renseignements sur la triangulation de 1” ordre, |

La triangulation portugaise de 1” ordre comprend 132 signaux géodésiques, cons-
truits en maçonnerie et dont la plupart a la forme d’une pyramide rectangulaire à
base carrée. Dimensions moyennes: côté de la base, 3 mètres; hauteur, 9 mètres,

Il y a longtemps (1836-1863) que l’observation de ces sommets à été faite par la
méthode de la répétition des angles, avec des théodolites à deux lunettes. Ces travaux
avaient seulement pour but la recherche des éléments pour la levée de la carte.

Toute la triangulation s’appuie sur la base Batel-Montijo — 4787,941 brasses de
Ciera = 10823,894 mètres. Ce rapport a besoin d’être vérifié.

Le Portugal ayant adhéré au plan de l'Association géodésique internationale après
que l'Espagne a projeté son réseau fondamental, on à reconnu la nécessité de choisir
un certain nombre de triangles qui pussent compléter d’une manière uniforme le sys-
tème fondamental de la triangulation ibérique ; et après des combinaisons diverses on
a adopté le plan ci-joint, qui est simple et systématique.

En conséquence, le réseau géodésique fondamental fut réduit à 68 sommets, sans y
comprendre les extrémités B, M de la base. |

La direction de l’Institut Géographique du Portugal ayant considéré que les an-
ciennes observations d’angles, quoique exécutées avec tous les soins généralement en
usage dans la première moitié de ce siècle, ne pourraient point rivaliser ou même être
comparables avec celles qu’on obtient maintenant par des méthodes perfectionnées, a
acquis des instruments modernes et à procédé à de nouvelles observations dans les.
susdits 68 sommets et dans les extrémités de la base.

On a déterminé dans les stations 15 directions azimutales, à peu près, pour chaque
point géodésique. Chacune de ces directions est le résultat de 4 visées, dont les deux
premières sont obtenues avec le cercle vertical à gauche, et les deux autres avec le
cercle à droite. Pour chaque visée on a fait dans le cercle 4 lectures micrométriques.

 
