 

 

CINQUIEME SEANCE

Lundi, 10 octobre 1898.

Présidence de M. Faye.
Assistent à la séance :

I. Les délégués: MM. Albrecht, Anguiano, van de Sande-Bakhuyzen, Bassot, de
Bodola, Bœrsch, Bouquet de la Grye, Bourgeois, Celoria, Darwin, Ferrero, Færster, Gaulier,
Guarducci, Huid, Hammer, Helmert, Hennequin, Hirsch, Kimura, Koch, Lallemand, Nagel,
Nell, von Orff, Preston, Rosén, Sagasta, von Schmidt, M. Schmidt, von Stubendorff, Tana-
kadate, Tinter, Westphal.

II. Les invités : MM. Haller, de Novales, Schlebach, Tripet, Weitbrecht.
La séance est ouverte à 9 h. 50.

Avant l'ouverture de la séance, M. le Président fait connaître à MM. les délégués
que le Wurtemberg fête aujourd’hui le jour de naissance de Sa Majesté la Reine, et il invite
l'Assemblée à se lever en signe de respectueux hommage.

Tous les délégués se lèvent.

M. le Président salue avec plaisir le vice-président de l'Association géodésique inter-
nationale, S. E. Monsieur le Général Ferrero qui, malgré des empêchements nombreux, a
tenu à assister au moins à nos dernières séances, afin de témoigner de son intérêt immuable
pour nos travaux et pour déposer personnellement le rapport spécial sur les triangulations,
dont il s’est chargé comme d'habitude.

Il donne ensuite la parole au Secrétaire pour présenter quelques communications
d’affaires et pour faire lecture du procès-verbal de la 4 séance.

Le Secrétaire annonce que M. le Général Oberhoffer lui a écrit qu'il se voit obligé,
malgré lui, de retourner à Berlin avant la fin de la Conférence ; il regrette vivement de ne
pouvoir assister plus longtemps aux importantes délibérations de l’Assemblée et il demande
de prendre ainsi congé de M. le président et de ses collègues.

 

=

1) ad aan Lb Wek Th

Wy

ak mil

 

D nema Wil

 
