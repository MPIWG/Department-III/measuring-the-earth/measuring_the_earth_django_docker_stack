 

|
|
i

 

dpt qu

à QU

A APE DCR VU UN OLA fe

Annexe CI.

ETUDE
sur les variations de longueur des mires de nivellement,

d'aprés les expériences du colonel Goulier
PAR

M. CHARLES LALLEMAND

Directeur du Nivellement général de la France.

Note préliminaire.

Parmi les nombreuses et savantes études du re

gretté colonel Goulier pour la Com-
mission du Nivelle

ment général de la France dont il faisait partie, l’une des plus importantes
concerne la sensibilité des mires aux actions de la température et de l’humidité, ainsi que
la recherche des modes de préparation du bois les plus propres à atténuer cette double in-
fluence. En 1883-1884, le colonel a entrepris, à ce sujet, une longue série d'expériences,
dont la mort l’a empêché de publier lui-même la relation.

Des 1892, j'avais donné à l'Association
ce travail et des conclusions qui s’en dé
Autriche et en Bavière, ces conclusions
sociation fut inv

géodésique internationale! un court exposé de
gagent. Sur la foi de quelques observations faites en
ayant été mises en doute, le Bureau central de l’As-
ité à faire exécuter de nouvelles expériences pour trancher la question.

En examinant à nouveau les faits invoqués contre les

montrer déjà ? que, loin de les contredire, ces obser
confirmer.

lois du colonel Goulier, j'ai pu
valions ne font, au contraire, que les

(Juant aux nouveaux ess
exécutés à
de ces lois.

ais réclamés par l'Association géodésique et tout récemment
Berlin par le D' Stadthagen, ils ont apporté une éclatante preuve de l'exactitude

‘ Conférence générale de Bruxelles, en 1892. Comptes-rendus,

p. 664 et suivantes.
* Voir Conférence de Lausanne, en 1896. Comptes-

rendus, p. 230 et suivantes.
== G6"

 
