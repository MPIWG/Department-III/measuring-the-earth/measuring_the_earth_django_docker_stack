 

 

387

|
|
\

Cette machine est destinée spécialement aux stations qui ont une grande onde semi-
diurne.

Lorsque l’equation diurne est considerable, les résultats ne sont pas toujours satis-
faisants.

Ensuite de ce défaut, le Survey a entrepris la construction d’une machine ayant à
la fois les avantages de celle de Sir William Thomson et de Ferrel sans leurs inconvénients.
Elle comporte 37 composantes el ses indications seront données à une grande échelle.

ASIE. — JAPON.

Le Gouvernement Impérial du Japon à fait installer, sous la direction de l’Institut
géodésique, onze marégraphes-enregistreurs de divers systèmes.
Leurs emplacements sont, en partant du Nord et en suivant la côte Est :
Hanasaki (district de Nemoro),
Hakodate (district de Osuna),
Kinkuasan (district de Rikuzen),
Jokasima (district de Savani),
Tanabe (district de Ku),
Nobeka (district de Kyushui),

 

Cote Ouest :
Nisumi (district de Kyushu),
Hamada (district de Tzumo),
Minatsuki (district de Noto),
Swazaki (district de Mutsu).

Le marégraphe de Hakodate est du systéme de Lord Kelvin.

L'installation relativement récente de ces appareils ne permet pas encore de donner
des résultats ; mais leur dépouillement se fait d’après le système de l’analyse harmonique,
et notre collègue M. Tanakadate promet qu’à la prochaine Conférence il apportera des
determinalions du niveau moyen en différents points de l’Empire du Japon.

UL RT

Fl

m im

MIR LET ARAM II TREE Ta TF

 
