 

108
du Kitakami et aux ramifications des chaines de montagnes qui bordent cette vallée. Cette
carte est complétée par une autre carte plus détaillée donnée par M. Tanakadate.

La latitude géographique de la station est de 39° 8 0.
Tchardjut.

Cette station, proposée par M. le général de Stubendorif, chef de la Section topogra-
graphique de l'État-Major russe, sera probablement établie sur la rive gauche de PAmu-Darja,
à O kilomètres environ de la ville de Tchardjui, à la latitude de 39° 810". La reconnaissance
de cette région est encore en cours d’ex&cution.

San Pietro.

Pour la station italienne, on avait d’abord pensé aux environs de Cagliari, qui s’y
seraient prétés parfaitement aux points de vue social, seismique et météorologique. Mais
comme la latitude de Cagliari est de 39° 13, on ne pouvait songer, pour établir la station,
qu’à la région située au sud-ouest de Cagliari et au sud de Capoterra, large de 4 kilomètres,
comprise entre le versant esl de la chaîne de montagnes du sud de la Sardaigne et le bord
de la mer. Malgré les conditions favorables au point de vue astronomique que présente cette
région, il a fallu y renoncer, parce qu’elle est à un haut degré sujette aux fièvres pendant
toute l’année.

Les efforts de M. Celoria, vice-président de la Commission géodésique italienne,
pour trouver un autre emplacement, ont été couronnés de succès, car il a découvert sur la
côte orientale de l’île San Pietro, située à l’ouest de la Sardaigne, un point d'observation qui
paraît être très favorable. La station serait donc établie à proximité du point trigonométrique
Torre San Vittorio, édifice très solide d'une hauteur de 8 métres environ, non pas
sur la tour elle-méme.

 

Le Torre San Vittorio sert actuellement de magasin ; il est placé au milieu des
vignes, sur une colline de 43 mètres de haut et à 4 kilomètre au sud-ouest de Carloforte,
chef-lieu de Vile. Cette ville compte 8000 habitants et possède, après Cagliari, le port le
plus fréquenté de toute la Sardaigne. Elle est le siège de nombreux consulats, a un climat
très sain et offre tout le confort désirable. Deux fois par jour elle est en communication par
bateau à vapeur avec Porto Scuso, port de la côte ouest de ia Sardaigne, rattaché par une ligne
de chemin de fer à Cagliari, de sorte qu’on y parvient, de Carloforte, en 8 heures et demie.

runde ne HA Marla |. aid ul

Jalen Thy il

wih uw

;
|
|

 

 
