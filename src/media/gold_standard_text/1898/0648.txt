 

 

Si l’on retranche les remboursements ci-dessus du total des arriérés, il en
résulte que le total net des sommes encore à percevoir s'élève à . Fr. 14 702,49
ce qui porte l'actif total actuel à Fr. 28 494,61.

Get actif doit être employé d’abord à couvrir les dépenses suivantes :
1° les frais d'impression des Procès-Verbaux et Rapports de la dernière Confé-

rence de la Commission permanente à Lausanne, et quelques engagements

pour Pexercice de 1896, qui n’ont pas encore pu être liquidés définitive-

mem el adue en chiiiresronds. 0, Lo. Dr. 34052
20 les crédits accordés par la Commission permanente (voir Rap-

port financier, Conf. de Lausanne) et actuellement en

partie déjà engagés pour les entreprises scientifiques sui-

vantes, en cours d’exécution, soit :

a) pour études sur les mires de nivellement . . DEN, » 1045 —
b) pour établir à Breteuil une station destinée aux comparai-
pw dependue .. . 2... \ » 9000 —
c) pour des études sur les appareils et règles de base. . . nn).
d) pour subventionner des études concernant la mesure de
Da ies bord desnavires. il 2.5 » 7500
e) pour contribuer aux recherches sur les déviations de la
ee sn > 0 —-
He 20% —
init house en) 149 61

 

Hr. 28 194,61

 

Le contrôle de l'utilisation de ces crédits, ainsi que les modifications éven-
tuelles qui pourraient devenir nécessaires suivant le progrès des travaux dans la
distribution des crédits votés, appartiennent au bureau de l'Association institué
par la Convention de 1895. (Voir Comptes-Rendus de Lausanne.)

Nous terminons ce chapitre des comptes, en priant les Hauts Gouverne-
ments des 11 Ktats qui, d’aprés le tableau de la page 7, doivent encore une partie
de leurs contributions, de bien vouloir donner les ordres pour régler ces arriérés.

 
