 

 

 

 

   

— 114 —,

VU. —- Grande Bretagne.

 

 

 

 

Walpole, St. Peter’s

 

 

 

 

 

 

 

 

 

 

À due roncmuge + DIRECTEURS oo :
DES POINTS q EPOQUE ET OBSERVATEURS INSTRUMENTS REMARQUES
Ronas ..... a 60° 32... 16013 8% 1821 Génér. Colby, capits.Vetch, | Ramsden de 3 p.
Drummond.

Base de Rhuddlan.| 53 17 12 | 14 11 31 1806 Wooleot. id.
© Terme Est.
Base de Rhuddlan.| 53 17 41 | 14 451 1806 id. id.

Terme Ouest.
Ru Reno -...... 20 8 11 2322 1848 Serg. Winzer. id.
Ryder’s Hill ...... 00 30 21, | 15 46 22. 1845 Serg. Donelan. id.
St. Agnes Beacon..| 50 18 24 | 12 26 49 | 1797 Génér. Mudge. id. |
St.AgnesLighthouse 49 53 33 | 1119 7. 1850 Serg. Steel. Airy’s Zenith Sector.
St. Ann’s Hill..... St 25 17 822: 1792 Génér. Mudge. Ramsden de 3 p.
St. Martin’s Head..| 49 58 0 | 11 23 52, 1850 Caporal Wotherspoon. Ramsden de 18 p.
St. Paul’s, Tr. Lon-

don ........ Ol 2019 17 23 57 1848 Serg. Steel. id.
St. Peter’s Ch. Tr.. | 51 21 55 | 19 4 57 1844 id. id.
SaWOl sco. 04 49 11 | 10 37 29 1827 Génér. Portlock. Ramsden de 3 p.
Saxavord ......... 60°49 39 | 16 49 25 1817-47 Gardner, serg. Steel. Ramsden de 3 p. et |

Théod. de 7 p. |
Sayıs Law........ 99 90 49 | 14 59 38 1809-46 Gardner, serg. Winzer. Ramsden de 3 p.
SCA Hell... 42115 14 2710 1841 Capits. Pipon, Craigie. id.
Scarabin.......... be io 13 14 4 22 1839 Lieuts. Hornby, Robinson, id.
Pipon.
Scournalapich..... 5022 9 | 12°36 16 1846 Serg. Donelan. id.
Severndroog ...... ol 2059 14 43 27 1822-48 Gener. Colby, capit. Kater, id.
Gardner, serg. Donelan.
Shanklin Down ...| 50 37 6 | 16 27 24 1846 Sere. Steel. Ramsden de 18 p. | Sehr nahe an 88.
Slieve Donard..... 54 10 49 | 11 44 36 | 1826-45 Gener. Portlock, sergs. | Ramsden de 3 p.
Forsyh, Winzer, caporal
Stewart.

Slieve League..... 139 4, 35726) 1828 Génér. Portlock. id.
Slieve More....... 3 050, 758019) 1831 Serg. Doul. Theod. de 12».
Slieve Snaght..... 1147.10 1948| 1827 Génér. Portlock. Ramsden de 3 p.
Snowdon.......... > 426,135 17) 1842 Capit. Pipon, lieut. Stanley. id.
Southampton...... 50 54 49 | 16 15 39 | 1845 Soldat Scott. : Théod. de 10 p.
South Berule..... b4 828 | 12 59 40 1845 Sergs. Steel, Forsyh. Ramsden de 3 p.
South Lopham Ch.

DE 2 nn. 0225344182937 1844 Serg. Bay. Troughton de 2 p.
South Ronaldshay.| 58 46 55 | 14 43 15 1821 Capits. Vetch, Drummond. | Ramsden de 3 p.
Southwold Ch. Tr..| 52 19 41 | 19 20 33 1844 Serg. Bay. Troughton de 2 p.
Start Lighthouse..| 59 16 40 | 15 17 16
Stoke Ch. Tr...... 7092, 183318 1844 id. id.

Stoke Hill........ >10 19 1536 4 1849 Serg. Jenkins. Ramsden de 3 p.
SOL 2... 12025 112855 1847-48 Serg. Winzer. id.
Stronsay.......... 3 0838.15 71] 182] Capits. Vetch, Drummond. id.
Swaffham Ch. Sp..| 52 38 53 | 18 21 14 1843 Serg. Steel. Ramsden de 18 p.
Swyre Barrow....| 50 36 21 | 15 34 11 | 1845 Serg. Donelan. Ramsden de 3 p.
Da 24156 1126 47 1829 Génér. Portlock. id.
Farbathy.......... ou 12 42 1-15 35 39 1817 Gardner. id. Nahe bei 152.

VOU ek e214 18 | 832 17 1832 Génér. Portlock. id.
Tawnaghmore. .... 04 1740 | 8 358 1829 sere. Deul. Theod. de 12 p.
Telegraph Tower..| 49 55 44 | 11 21 37 1850 Caporal Wotherspoon. Theod. de 18 p.
Tharfield.......... 52:6 2.173145 1843 Serg. Donelan. Theod. de 3 p.
Thaxted Ch. Sp... 5157 15 | 18 0 21 1844 Serg. Steel. Theod. de 18 p.
DMO 7... 52 38 43 | 16 47 49 | 1800-43 | Woolcot, serg. Mulligan. | Théod. de 3 p.
Tofts Ch. Tr. ..... 25074 1974 14 1845 Serg. Bay. Troughton de 2 p.
Trevose Head..... 50 32 54 | 12 37 52 | 1797-1846 | Génér. Mudge, serg. Do- | Ramsden de 3 p.

nelan.

| Eröstan.. ......... on 241) 11 20 92. 1827 Génér. Portlock. id.

| Upcot Down ...... 0128 44) 19,51 24) 1850 Serge. Jenkins. id.
Viears Carn....... DA ld os 4-8 1827 Génér. Portlock. id.

oe 13437) 17 53 13 | 1843 Serg. Bay. | Troughton de 2 p.
