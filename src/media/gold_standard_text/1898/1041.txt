MES

 

NLA

ml

ui

D EAN DR NL LU 2 mt

MUR

 

de

XIE Prusse À

 

°

DREIECKSKETTEN DES GEODATISCHEN INSTITUTS

Vorbemerkungen. ;

Rheinisches Dreiecksnetz

Die Bonner Grundlinie und die Winkel zu ihrer Verbindung mit der Hauptdreiecks-
seite Siegburg-Michelsberg wurden unter General Baeyers Leitung bereits 1847 in der
Absicht gemessen, den älteren Tranchot’schen und Preussischen Dreiecken in den Rhein-
landen, :welche sich noch auf den Tranchot’schen Werth der Seite Niirburg-Fleckert
und damit auf die Französische Basis von Melun stiitzten, die nothwendige Selbstän-
digkeit zu geben. Die benutzten Winkelmess-Instrumente waren ein 15-zölliser Theodolit
von Ertel. und ein 12-zölliger Repetitionstheodolit von Pistor und Schiek (Siehe :
Prusse, B. Instrument N.1 und N. 2). | |

Später sollte für die Struve’sche Längengradmessung unter dem 52° der Breite preus-
sischerseits unter anderen eine Liicke zwischen den Kurhessichen Dreiecken von Gerling
und den Beleischen Dreiecken ausgefüllt werden. Zu dem Ende wurden bereits in den
Jahren. 1860-61 die Winkelmessungen von der Bonner Grundlinie bis zur Belgischen
Grenze mit einem 8-zölligen Universal-Instrument von Pistor und Martins ebenfalls unter
Baeyer’s Leitung ausgeführt. Als jedoch im Jahre 1862 die mitteleuropäische Gradmes-
sung von Baeyer ins Leben gerufen worden war, und es sich als wünschenswerth heraus-
stellte, auch rheinaufwärts eine Dreieckskette zum Anschluss an die Schweizerischen
und Italienischen Triangulationen zu legen, beschloss man, die Messungen von 1860-61
zu verwerfen und, unter Festhaltung des Anschlusses an die Kurhessichen Dreiecke,
die ganze Rheinische Dreieckskette mit grösseren Instrumenten (zwei 10-zölligen Uni-
versäl-Instrumenten von Pistor und Martins) aus einem Gusse herzustellen. Die Gross-
herzoglich Badische Regierung ermöglichte durch ihr liberales Entgegenkommen die
Ausführung dieses Programms, indem sie für die in Baden nothwendigen Arbeiten die
erforderlichen Geldmittel bewilligte und die Ausführung der Messungen dem Central-

 

 

 
