|
|
ë
IE

 

 

a AR!

ll

Ta nn a eT

Hie |

115
et en éliminant certaines sources d’erreur (surtout l’erreur constante dans la mesure des
distances des couteaux) : :

L = 994, 949.

D’autre part on obtient, par la combinaison de la valeur de L pour le pendule léger
autrichien avec la valeur moyenne fournie par les trois pendules lourds, qui ont à peu pres
un poids double de l’autre :

L — 994, 945.

Cette dernière valeur n'est pas aussi bonne que la précédente, quant à l'élimination
des erreurs constantes.

On sait que la détermination d'Oppolzer à Vienne, parfaitement transportée à
Potsdam, avait donné, en prenant — 07" 004 pour correction de flexion, L — 994™™ 959.
Gette valeur est déduite d’après la seconde méthode. La détermination de Defforges a Paris,
conformément à la seconde, donne L — 994" 964; toutefois le transport à Potsdam, ainsi
que la correction pour flexion ne sont pas trés sûrs. (Voir Helmert, « Contributions yp. ol)

Il n’y a donc pas de raison, pour le moment, d’abandonner la détermination d’Op-
polzer 4 Vienne. Du reste, on continuera les mesures 4 Potsdam, en apportant la modification
que les pendules oscilleront avec des plans sur un couteau fixe.

Ainsi que je l’ai déjà mentionné dans mes Contributions, p. 56, nous avons fourni,
dans le courant de l’année, par desexpériencesélectro-magnétiques, la preuve que l'induction du
magnétisme terrestre ne peut pas exercer d'influence sensible sur la durée d’oscillation des
pendules métalliques. Bien que la théorie parût déjà avoir décidé dans ce sens, la question
était cependant envisagée de certains côtés comme étant encore ouverte. (Voir p. ex. les
observations finales de Bruns au sujet des expériences de pendule de Bessel, dans les « Klas-

siker » d’Ostwald.)
6.

Rassemblement et discussion des matériaux pour différents rapports spéciaux.

La Conference generale a demandé l'élaboration de divers rapports et en particulier
le rassemblement des matériaux pour plusieurs rapports spéciaux réglementaires. Dans ce
but, nous avons en partie demandé la coopération de MM. les délégués, ou utilisé les publi-
cations déjà parues. Pour le Rapport spécial sur les triangulations, j’ai pu fournir person-
nellement une petite contribution ; mais j’ai été essentiellement occupé de l’élaboration d’un
rapport sur les déterminations relatives de la pesanteur au moyen du pendule et j'y travaille
encore actuellement. Le rapport sur les déviations de la verticale a été confié à M. le Dr
Beersch.

Pour compléter les indications sur son activité scientifique, j’ajoute encore que le
Bureau central examine en ce moment un appareil de pendule de Stuckrath pour le Japon et
que, dans plusieurs occasions, il a pu donner les renseignements qu’on lui demandait. Enfin,
M. le D' Schumann à Christiania a exécuté des mesures relatives de pendule avec une sub-
vention des Fonds internationaux.

 
