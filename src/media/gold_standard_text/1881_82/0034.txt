 

i
i

a

ns

SEELE EEE LEE EEE EEE En

ee

À

26

Les feuilles du maréographe d'Urk montrent un autre particularité, c’est à
dire une multitude d’oscillations de faible durée, de 12 à 13 minutes, qui ont quelque
ressemblance avec les ,,seiches‘ du lac de Genève.

M. le Président remercie M. van Diesen de sa communication et donne la parole
à M. le Major Capitaneanu, pour présenter le rapport de la Roumanie.*)

M. le Président, après avoir remercié M. Capitaneanu de sa communication, passe
au point 4 du programme, savoir à la nomination du 9% membre de la Commission
permanente, pour remplacer M. Bruhns décédé. La nomination se fait au scrutin secret.
Le dépouillement des bulletins de vote étant fait en séance, M. le Président proclame
M. le Professeur Nagel de Dresde nommé membre de la Commission permanente à l'unanimité
des voit.

M. le Président fixe la dernière séance pour Vendredi à 2 heures. L'ordre du
jour de cette séance comprend les rapports de la Norvège et de la Suisse, la désignation
des rapporteurs pour la Conférence générale, le remplacement de feu M. Plantamour
comme membre de la commission de pendule, enfin le choix de l’endroit pour la Con-
férence générale prochaine et le programme pour cette Conférence.

La séance est levée à 3°2f.

*) Voir au Rapport Général pour 1881;82, Roumanie.

amb om A eet) Meme Ra be doe

Lab toile

toy |

a ndash a mann 11

 
