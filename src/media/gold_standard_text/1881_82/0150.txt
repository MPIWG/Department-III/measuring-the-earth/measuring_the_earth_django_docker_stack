 

h

i

k

i
4
a
k
i
i
i
ir
i
i
i:
if
i

142

Höhe des Anschlusspunktes auf Alexanderschanze ergab sich zu 967983, während die
Landesaufnahme hat: 968”254. Es wäre also die württembergische Höhe noch zu ver-
mehren um 0”271, während für Bruchsal die entsprechenden Zahlen sind:

Württemberg: 116"125
Landesaufnahme: 116-224,

es wäre also in Bruchsal die württembergische Höhe blos zu vermehren um 0"099.

Es ergäbe sich hieraus für das Polygon Bruchsal—Appenweier—Alexanderschanze
—Horb— Bruchsal ein Anschlussfehler von 0"271 — 07099 = 0”172. Von Anfang
an waren gegen den Anschluss in Alexanderschanze von dem Unterzeichneten wegen der
nothwendig zu erwartenden Differenzen in Folge von Lattenungleichheit Bedenken aus-
gesprochen worden und zuletzt wurde denn auch dieser Anschluss fallen gelassen und
statt seiner mit Benutzung der bayrischen Höhendifferenz Würzburg— Kahl ein Anschluss
in Kahl hergestellt, der mit dem Bruchsaler gut stimmte.

Es betrug nämlich die Höhe von Kahl

Württemberg: 1122719
Landesaufnahme: 112.830
Differenz: o”111
07099 -+- O7111
2
um welche schliesslich die württembergischen 1879 mitgetheilten Höhenzahlen vermehrt
wurden, um die Höhen endgiltig über Normalnull zu erhalten.
Um sodann weiter die Höhen über dem Mittelwasser der Ostsee bei Swine-
münde zu erhalten, hat man nach der Publikation von 1879:

Das Mittel der Bruchsaler und Kahler Differenz ist — 0108

Württemb. Nivellem. Gradmess*)-Nivellem. Wirttemb.—Gradmess,-Nivellem.
Friedrichshafen 4057556 4057905 — 07349
Bruchsal 116.125 116.446 — 0.321
Kahl 112.719 113.001 — 0-282

Mittel: — 0-317

Damit erhält man nun folgende Höhen über

Normalnull. Mittelwasser der Ostsee bei Swinemünde.
Aulendorf HM. 549"062 5497274
Bietigheim HM. 222.231 222.443
Bruchsal HM. 116.230 116.442
Crailsheim HM. 411-413 411-625
Friedrichshafen HM. 405.661 405.873

*) Publication des Königl. preussischen geodätischen Institutes: Gradmessungs-Nivellement zwischen
Swinemünde und Konstanz. Berlin 1882.

 

i
i
i
3
3
3
1
j

Aa dsm vu

serai hu be

 
