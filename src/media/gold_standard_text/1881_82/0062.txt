 

4
bei geraden Instrumenten die symmetrische Anordnung der Lagerstützen gegenüber dem
Beobachter wohl den schädlichen Wärmeeinfluss grossentheils behebe.

Damit ist die Diseussion über diesen Gegenstand erschöpft und der Präsident
ertheilt Herrn Villarceau das Wort zu Punkt 7 des Programms. Die hierauf zur Ver-
lesung gelangende Mittheilung ist unter dem Titel „Sur la determination de la pesanteur
relative au moyen du regulateur Villarceau“, in den Beilagen aufgenommen.*)

Die für heute anberaumte Demonstration des betreffenden Apparates muss unter-
bleiben, weil der mit dem Transport betraute Capt. Perruchon an der Grenze zurück-
gehalten wurde.

Herr Villarceau erklärt noch, dass der Apparat in seiner gegenwärtigen Form
in Folge von gewissen Fehlern in der Ausführung nur eine Genauigkeit von 35400 für
die Bestimmung der Schwere gestatte, dass aber nach Behebung dieser Fehler eine Prä-
cision VON „00000 Sicher erwartet werden dürfe.

Herr Perrier fügt hinzu, dass es in Folge mangelhafter Construction der Luft-
pumpe noch nicht gelungen sei, den Villarceau’schen Regulator unter einem geringeren
Luftdruck als dem von 518”® wirken zu lassen.

Herr Oudemans frägt, ob mit dem Reversionspendel in seiner gegenwärtigen
Form bereits Versuche im luftleeren Raume gemacht worden seien?

Herr Hirsch weist auf die bezüglichen schon vor mehreren Jahren an ver-
schiedenen Orten, wie Genf, Berlin, Hoboken von Herrn Peirce angestellten Versuche
hin, welche die experimentelle Bestätigung des theoretischen Schlusses ergeben haben,
dass das Bessel’sche Reversionspendel den Einfluss der Luft vollständig eliminire. Dieser
Erfolg scheine die Nothwendigkeit weiterer Beobachtungen im luftleeren Raume um
so mehr aufzuheben, als solche stets mit beträchtlichen Schwierigkeiten verbunden seien.

Herr Ferrero fügt dem hinzu, dass die Erfahrungen, welche bei den in Italien
angestellten dergleichen Experimenten insbesondere Professor Pisati gesammelt, die Schlüsse
des Herrn Hirsch bestätigen.

Der Präsident dankt hierauf den Bevollmächtigten Frankreichs für ihre Mit-

_ theilungen und ertheilt Herrn General Baulina das Wort zur Berichterstattung über die

italienischen Arbeiten.**)
An diesen Bericht knüpft Herr Ferrero einige Erläuterungen und legt einige
mit Hilfe der Heliographen hergestellte Karten vor, welche seitens der Versammlung
die anerkennendste Aufnahme finden, und zwei Publicationen, betitelt:
1. Respighi-Celoria: Operazioni eseguite nell’anno 1879 per determinare la dif-
ferenza di longitudine fra gli osservatori astronomici del Campidoglio in
Roma e di Brera in Milano.

2. Rajna: Determinazione della latitudine dell’ Osservatorio di Brera in Milano
e dell’ Osservatorio della R. Universita in Parma.

*) Siehe Beilage 1.
**) Siehe den Generalbericht fiir 1881 und 1882, Italien.

van ya 4 A A hh 5 A Mi UB

nu ih at u, hd ed m in

|
|

 
