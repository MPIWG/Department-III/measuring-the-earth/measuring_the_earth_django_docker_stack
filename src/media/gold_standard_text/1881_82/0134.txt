 

|
|
|
|

Zr

Saray

|
|
|

126

aber den Einfluss des Mitschwingens des Stativs auf die Länge des Secunden-Pendels

an. Das dabei erhaltene Resultat stimmt fast vollkommen mit dem von Herrn Planta-
mour abgeleiteten. In Moskau sind von Herrn Bredichin, dem Director der Moskauer
Sternwarte, kürzlich ähnliche Experimente ausgeführt worden.

Die Präcisions-Nivellements sind auch nicht zurückgeblieben. Von den
14670™, der gesammten Linge unserer projectirten Nivellements-Linien, sind bis jetzt
4123 doppelt und 618™ einfach nivellirt worden. In diesem Jahre werden wir im
Stande sein, den Niveau-Unterschied zwischen dem Baltischen und dem Schwarzen
Meere aus doppelten Nivellirungen ableiten zu können. Die Verbindung unseres Nivelle-
ments-Netzes mit demjenigen von Preussen und Oesterreich-Ungarn ist in zwei Punkten:
Nimmersatt (Preussen) und Radziwilloff-Brody (Oesterreich) schon ausgeführt; die zwei
noch übrig bleibenden Verbindungen, bei Szczakowa und Thorn, werden in der nächsten
Zeit zu Stande gebracht.

Weniger befriedigend ist der Mareographendienst. Bis jetzt besitzen wir
nur einen den Wasserstand registrirenden Apparat, der vor drei Jahren in Dünamünde
bei Riga aufgestellt ist. Die gewöhnlichen Pegel, längs des russischen Theils der Ost-
see-Küste, sind vermittelst Nivellirungen mit einander verbunden worden, und in kurzer
Zeit hoffen wir eine eingehendere Einsicht in die Veränderung des Niveaus der Ostsee
zu gewinnen.

Schliesslich ist es mir angenehm, dem Central-Bureau anzeigen zu können, dass
der Druck des XXXVII. Bandes der ,,Memoiren“ unserer Topographischen Abthei-
lung des Generalstabes in den letzten Tagen beendigt worden ist.

Ed. von Forsch.

eee

Sachsen.

Leider ist der Unterzeichnete abermals, wie vor 12 Jahren, in die Lage ver-
setzt worden, seinen Bericht über die Sächsischen Gradmessungsarbeiten mit dem Hin-
weis auf das Hinscheiden eines treuen Mitarbeiters, eines liebenswürdigen Collegen zu
eröffnen. Den sämmtlichen Herren Commissaren der Europäischen Gradmessung ist be-
kannt, dass der Sächsische Mitcommissar und Secretair der permanenten Commission,
Herr Geheimer Hofrath Professor Dr. Carl Christian Bruhns, Director der Sternwarte
zu Leipzig, am 25. Juli 1881 im noch nicht vollendeten 51. Lebensjahr aus dem Leben
geschieden ist. Der Bericht über die Sächsischen Arbeiten, welche er während seiner
Krankheit im Frühjahr 1881 (28. April) in Gemeinschaft mit dem Unterzeichneten für den
vor zwei Jahren erschienenen Generalbericht geliefert, sollte seine letzte Arbeit für den
Sächsischen Theil der Gradmessung sein. Welchen grossen Verlust die astronomische
Wissenschaft und speciell die Europäische Gradmessung durch diesen Hingang erlitten,

 

i
|

 
