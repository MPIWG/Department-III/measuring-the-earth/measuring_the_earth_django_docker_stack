a Ze

PP TE nr |

|

TRAIT

Te

TOOT TR aT

k

63 à
Beide Zusätze zum Programm werden unter den von den Antragstellern aufge-
worfenen Bedingungen angenommen. |

Die anwesenden von dem Präsidenten in Vorschlag gebrachten Berichterstatter
erklären sich zur Uebernahme der Referate bereit und hiermit erscheint das für die
nächste allgemeine Conferenz beantragte Arbeitsprogramm in der permanenten Com-
mission gebilligt unter dem Vorbehalte, dieses Arbeitsprogramm im Falle der Nothwendig-
keit durch schriftliche Verständigung unter den Mitgliedern der permanenten Commission
zu erweitern.

Der Präsident bemerkt, es müsse als selbstverständlich betrachtet werden, dass
den Berichterstattern das Recht zustehe, an alle Bevollmächtigte Frageschreiben zu
richten und man dürfe erwarten, dieselben werden ihre Antworten rechtzeitig den Be-
richterstattern zukommen lassen; weiter wird auf den Vorschlag des Präsidenten Herr
Hirsch an Stelle des verstorbenen Herrn Plantamour zum Mitglied der Pendelcommission
ernannt und ausserdem dieser Commission ein fiinftes Mitglied, Herr Villarceau, beigegeben.

Der Präsident nimmt nun den 6. Punkt des Programmes auf, nämlich die Wahl
des Ortes für die nächste Generalversammlung.

Herr v. Oppolzer gibt dem Wunsche Ausdruck, dass Rom hierfür gewählt werde,
falls dies der italienischen Regierung genehm sei. Er begründet seinen Vorschlag
mit den vorzüglichen geodätischen Leistungen in diesem Lande, erwähnt der zahl-
reichen und hochgeschätzten Collegen der italienischen Commission, die persönlich kennen
zu lernen sich alle Mitglieder zur Ehre schätzen werden, ferner der Schönheiten und
Kunstschätze der ewigen Stadt. In Rücksicht auf die klimatischen Verhältnisse würde
sich als Zeit der Versammlung Mitte Oktober empfehlen. | :

Da kein anderer Vorschlag in dieser Richtung gemacht wird, erklärt der Präsident,
dass der Antrag, Rom als Versammlungsort zu wählen, allgemeinen Anklang findet.

Herr General Baulina antwortet, er wolle sofort die italienische Regierung von
diesem Wunsche in Kenntniss setzen und zweifele für seine Person nicht, dass dieselbe
dem für Italien so schmeichelhaften Vorschlag freundlich entgegenkommen werde.

Der Präsident meint, es sei der bei der Wahl des Versammlungsortes bisher
übliche Vorgang zu befolgen, nämlich dass das Bureau der permanenten Commission be-
auftragt werde, sobald ihm die Entschliessung der italienischen Regierung bekannt ge-
geben worden sei, die zur Entscheidung über Ort und Zeit der Zusammenkunft nöthigen
Maassnahmen zu treffen.

Die Versammlung stimmt diesem Vorschlage bei.

Der Präsident glaubt der Dolmetsch der Versammlung zu sein, wenn
er der königl. niederländischen Regierung für die der permanenten Commission gewährte
Gastfreundschaft den tiefstgefühlten Dank ausspreche.

Die Versammlung erhebt sich, um diesem Danke würdigen Ausdruck zu geben.

Im Namen aller fremden Bevollmächtigten dankt der Präsident unter allgemeinem

 

 
