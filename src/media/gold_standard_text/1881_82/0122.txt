 

+
i
a
i
3
!
i

114

 

Il. Zwischen Mittelmeer und Nordsee.

a
a
N
| a
i
a
]
'
:
|

Nach dem „Gradmessumgs-Nivellement des Geodätischen Instituts zwischen Swine-
miinde und Amsterdam. Berlin 1883“ liegt das Mittelwasser der Nordsee bei
oe als das den Ostsee ar. » »- - . . . . . . + 07098
a mr Odsee höher als Mittelmeer. - . . . . . . . . 0.664

daher Nordsee bei Amsterdam höher als Mittelmeer + 0°757

Senne

II. Zwischen Ostsee und Mittelmeer (über Ostende).

Quellen: a. Nivellement general du Royaume de Belgique. TIxelles-Bruxelles 1879
b. Uitkomsten van de in 1877 witgevoerde Waterpassing.
Nach II. liegt die Nordsee bei Amsterdam über der Ostsee . + 0093

Ron. hoc Wenlo höher als Amsterdam ,. : 4.11... + 22.017

| daher Venlo über der Ostsee + 22-110 = A
Nach a. liegt Ostende (Mittelwasser) über dem Mittelmeer . + 0.730
mi @ieno uber Ostende . . . . . . . ,.. . . + 22.038

daher Venlo über Mittelmeer -+ 22.768 = B
daraus folgt B— A d. h. Ostsee über Mittelmeer -- 07658

AAA dt verre

IV. Zwischen Mittelmeer und dem Atlantischen Ocean.

General Ibanez theilt im Spanischen Generalbericht pro 1881/82 mit, dass der
Ocean bei Santander’höher liegt als das Mittelmeer bei Alicante um + 076625

pe 00 them Lu ba

Zusammenstellung.

| Nach I: dem Nivellement von Swinemünde über die Schweiz nach

| | Marseille liegt die Ostsee über dem Mittelmeer . . . + 07664

| Nach III: dem Nivellement von Swinemünde über Amsterdam und

l Ostende nach dem Mittelmeer liegt die Ostsee über dem

| Mittelmeer . . + 0.658

| Nach IV: dem Spanischen A Hotiatibnd Het der rin hei aa alles

| | hôher als das Mittelmeer bei Alicante . . . . . . . “+ 0.6625

| Nach II: liest die Nordsee bei Amsterdam über dem Mittelmeer . + 07757 |

5
Nach III: liegt die Nordsee bei Ostende über dem Mittelmeer . . + 0.730 |

 
