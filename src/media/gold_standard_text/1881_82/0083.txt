 

TFT

paar Irre

&
R
=
3
te
=
t

75

Frankreich. France.
Note

sur les travaux géodésiques et astronomiques exécutés par le Dépot de la guerre pendant
les années 1881 et 1882.

Opérations Géodésiques.

France. La mesure des angles a été continuée en sept stations, le lone de
la méridienne de France, dans les environs de Melun.

La préparation des sites et la construction des signaux ont été exons entre
la base de Melun et la station de St. Martin du Tertre, au Nord de Paris.

La région comprise entre Melun et Paris présente de trés grandes difficultés
pour le choix des stations et la construction des signaux; c’est ainsi, qu’aux deux
extrémités de la base de Melun, on a été conduit à élever des charpentes de 30 et 20
mètres de hauteur.

Le terrain compris entre Melun et Lieusaint a été levé avec la plus grande.
exactitude: il résulte de ce levé que toute nouvelle mesure de l’ancienne base de De-
lambre sera extrêmement difficile.

Dans la prévision, où le remesurage avec les appareils modernes de Brunner
serait impossible, on a choisi l'emplacement et fixé les deux extrémités d’une base
nouvelle, à proximité de celle de Melun, dans l’alignement de l’ancienne base de Picard,
sur la route de Junsy à Villejuif. —

Un réseau de triangles reliera entr’elles les deux bases: la nouvelle base aura
7 kilomètres environ de longueur.

Actuellement, les deux bases de Melun et de Perpignan sont reliées entr’elles
par un réseau continu de triangles: les calculs ne sont pas encore terminés. Nous
espérons que nous pourrons en faire connaître ces résultats à notre prochaine réunion.

Algérie et Tunisie. Les signaux sont construits sur la méridienne d’Alger à
Laghouat: les perturbations survenues dans notre colonie ont empêché d'effectuer les
mesures d’angles.

En Tunisie, aidé des capitaines Defforges et Boulangier, j’ai complete la re-
connaissance géodésique des sommets du petit arc de paralléle qui doit prolonger le
parallèle algerien jusqu’à Tunis.

Opérations astronomiques.
France. Ces opérations comprennent:
19 la mesure de la différence de longitude entre Paris et Milan, par MM.
Perrier et Celoria.
20 entre Paris et Nice, par MM. Bassot et Perrotin.
10*

 

 
