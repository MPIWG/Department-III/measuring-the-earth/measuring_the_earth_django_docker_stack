 

ee

 

a u

En TS a eee SR D
= = = = a Ir EG SR ST >

SS D D DNS

nenne

110

Seehöhen

 

 

SS m ne

 

 

 

 

 

 

 

 

über Normal-Null | über Mittelwasser
hr den Nivellemeits- mitgetheilt von der| bei Triest abgeleitet | Lien

königl. preuss. Lan-| aus den österr. Ni-

Daum des-Aufnahme. | vellements.
(P) (6) (= 0)
W@711) bei Slupna. . . . 2597001 | 2597436 — 07435
(4720) , Zabrzes ....:. 233.095 | 233.529 oa
(5000) Oderberg (Annaberg) 198.514 | 198.956 | — 0.442
(7044) bei Peterwitz . . . 350.583 | ue =

(4884) ,, Bobischau. . . | 538.603 | 539.084 — 0.481
(4862) , Schlanei . . . | 351.790 | 352.338 — 0.543
(4801) ,, Liebau . | 511.110 | 511.608 | — 0.498

Mit den aus den bayerischen Nivellements abgeleiteten Seehohen über Normal-
Null, wie sie vom Herrn Director Dr. ©. M. v. Bauernfeind in:
„Das bayerische Präcisions-Nivellement und seine Beziehungen zur Europäischen

„Gradmessung. München 1880‘
bon werden, und den aus den Nivellements in Oesterreich-Ungarn abgeleiteten See-

höhen der bayerisch-üsterreichischen Anschlusspunkte über Mittelwasser bei Triest er-
gibt sich nachfolgender Vergleich, wobei die Daten des bayerischen Nivellements einem

bereits ausgeglichenen Netze entstammen:

Seehöhen

 

 

 

 

 

 

 

 

 

 

 

über Normal-Null | über Mittelwasser |
Höhenmarken- aus den bayerischen | bei Triest aus den | Differenz.

Anschlusspunkte. Nivellements. österr. Nivellements.
(b) | ©) | @—5

|

BO.) 466058 | 4662703 | — 0645
Scheerding © . . . (1898)| 316.067 | Ko | 008
Simbach © , - . . (1291) 350.110 | 350.767 | — 0.657
Salzpure.@). . . . (1352) 425.214 | 425.942 | — 0.728
Hangender Stein © . (1363) 461.103 | 461.838 | — 0.735
Russe @) . . . . (815) 483.462 | 484.366 | — 0.904

Es sind hierbei die Seehdhen tiber Mittelwasser bei Triest auf den Linien der
österreichischen Präcisions-Nivellements über Adelsberg, Laibach, Marburg, Bruck, Leoben,

 

3
i
a
i
3
3
3
|
1

se eau Le alll eh an

 
