 

EEE EEE

RP CO EN = SST ET

eu ee

Sate

SEE RTS

Fi
fi
il
a

iy
ay
i
i

|
ti
|
4
ii
i
ki
|
|
N
i
Fy
i
i,
i
|
N
1

 

ee
von den meteorologischen Verhältnissen darthun. Der Präsident ertheilt das Wort Herrn
v. Diesen zur Besprechung der holländischen Mareographen.

Die Einrichtung derselben fällt nicht.der Gradmessungscommission, sondern der
Verwaltung der Wasserbauten zu und untersteht speciell den Herren: Chefinspector Caland
und Ingenieur Vervey, welche, durch eine Amtsreise verhindert, der Sitzung beizuwohnen,
dem Bureau zahlreiche Mareographenblätter verschiedener Systeme übergeben haben.
Herr v. Diesen muss sich auf eine allgemeine Bemerkung über die Aufstellung und
'Thätigkeit der Mareographen beschränken, legt aber eine Karte der Niederlande, in
der die zahlreichen Aufstellungsorte von solchen Instrumenten verzeichnet sind und die
durch die letzteren erhaltenen Fluteurven, welche vermöge sehr interessanter Interferenz-
erscheinungen die verschiedenartigsten Formen darbieten, vor. Die Zahl der bereits
funetionirenden oder demnächst in Thätigkeit zu setzenden Mareographen beläuft sich
auf 64, unter welchen Herr v. Diesen 2 Kategorien begreift, nämlich die Mareographen
in strengem Wortsinne zur Bestimmung der Meereshöhe — dahin gehört die bei weitem
srössere Zahl — und die Fluviometer, welche dazu dienen, das Niveau der Flüsse und
Ströme dort zu bestimmen, wo ein Einfluss der Meeresströmung sich nicht mehr zeigt.
Er bezeichnet auch an der Hand der Karte die Grenzen, bis zu welchen dieser Ein-
fluss reicht.

Die Registrirung erfolgt seitens dieser Apparate nach verschiedenen Methoden
und in verschiedenen Maassverhältnissen; die einen ziehen eine fortlaufende Linie, andere
verzeichnen von 5 zu 5 Minuten einen Punkt; das Maassverhältniss schwankt zwischen
der natürlichen Grösse und '/,, derselben. An einzelnen Instrumenten müssen die Blätter
täglich erneuert werden, an anderen genügen sie einem Bedarfe von 8 Tagen. Herr
v. Diesen lenkt die Aufmerksamkeit der Versammlung auf ein Phänomen, Namens „agger‘‘,
welches an einem Theile der Küste, unter anderem in Katwijk, beobachtet wird und in
einer auffällig rückgängigen Bewegung der Gewässer besteht, die sich im Beginne der
Meeresflut zeigt; eine ähnliche Einbiegung zeigt die bei Helder beobachtete Fluteurve
bei Eintritt der Ebbe. Beide Thatsachen treten in den vorgelegten Registrirblättern
mit grosser Deutlichkeit hervor.

Die Mareographenblätter von Urk zeigen eine andere Eigenthümlichkeit, nämlich
eine Menge von Schwankungen kurzer Dauer (etwa 12—13”), welche einige Aehnlichkeit
init den Niveauschwankungen des Genfer Sees zeigen.

Der Präsident dankt Herr v. Diesen für seine Mittheilungen und ertheilt Herrn
Oberstliéutenant Capitaneanu*) das Wort zur Berichterstattung über die rumänischen
Gradmessungsarbeiten.

Nachdem der Präsident Herrn Capitaneanu gedankt, wird zum 4. Punkte des
Programms: der Wahl eines Mitgliedes der permanenten Commission an Stelle des

_ Herrn Bruhms übergegangen. Die Abstimmung wird geheim geführt; die Stimmenzählung

*) Siehe den Generalbericht für 1881 und 1832, Rumänien.

 

ee 2 hb Mb hid a Me ce

nen hc Ada a äh a nn
