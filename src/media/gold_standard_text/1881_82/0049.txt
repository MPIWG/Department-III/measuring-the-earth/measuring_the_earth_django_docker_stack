| In were werner ce os

 

iR!

Toy

a
=
=
=
z
x
=
=

41
Sie werden sich mit uns vereinigen in der Anerkennung der hervorragenden Ver-
dienste, welche Srukns sich um unser internationales geodatisches Unternehmen erwor-
ben hat, und in dem Ausdrucke der Trauer über den vorzeitigen Verlust unseres Collegen!

Italien und die Europäische Gradmessung haben im October des vergangenen
Jahres einen Veteranen der Geodäsie, Herrn General Marquis de Ricci, einen der hervor-
ragendsten Officiere des Generalstabes verloren. Er war einer der ersten, welcher Ge-
neral Daeyer’s Aufforderung: durch die vereinten Bemühungen der Geodäten vom Civil
und Militär innerhalb unseres Continents die Erdgestalt zu ermitteln, nachkam. Mar-
quis Ricci, der durch 15 Jahre dem italienischen Generalstab vorstand und als solcher
die grosse italienische Karte anregte, hat unserem internationalen Unternehmen wesent-
liche Förderung angedeihen lassen, sowohl durch seine wissenschaftliche Bedeutung als
durch die Liebenswürdigkeit seines Charakters und seinen zuvorkommenden Verkehr
mit allen Collegen unserer Vereinigung. Dieselben werden ihm stets ein ehrendes An-
denken bewahren.

Der Tod hat uns im Laufe des verflossenen Jahres einen ausgezeichneten Mit-
arbeiter, den gelehrten belgischen Officier Herrn Oberst Adan, entrissen; dieser war ein
praktischer Geodät von grosser Erfahrung, mit welcher sich noch tiefe theoretische,
insbesondere mathematische Kenntnisse verbanden.

Im Anfange dieses Jahres hatten die niederländische Gradmessungs-Commission
und unsere internationale Vereinigung den Verlust eines hervorragenden Geodäten Herrn
Professor Stamkart’s zu beklagen, der im Alter von 77 Jahren am 15. Januar 1882 ge-
storben ist. Früher Professor der Schifffahrtskunde an einer Marineschule, wurde er
später Eichungscommissar und 1867 Professor an der technischen Hochschule zu Delft.
Zur selben Zeit wurde er mit der Durchführung der Triangulation in den Niederlanden
betraut, welches Unternehmen er mit wahrhaft jugendlichem Feuereifer und seltener
Opferwilligkeit in Angriff nahm. Anfangs allein, später als Präsident der niederländi-
schen Gradmessungscommission hat er es an Bemühungen nicht fehlen lassen, die ihm
liebgewordene Arbeit dem Ende zuzuführen — leider war es ihm nicht vergönnt, deren
Abschluss zu erleben.

Von nicht geringerer Bedeutung sind seine Untersuchungen über die Flutmesser,
insbesondere der von ihm gelieferte Nachweis, dass das mittlere Niveau der Nordsee sich
innerhalb eines Zeitraumes von mehr als 150 Jahren in Bezug auf die Fixpunkte zu
Amsterdam nicht geändert habe. Schliesslich muss noch der mehrfachen interessanten
Abhandlungen Stamkart’s auf dem Gebiete der Maassvergleichung und seiner schon in das
Jahr 1839 zurückreichenden Erfindung eines Spiegelcomparators gedacht werden, mittelst
dessen er wiederholte Längenmaassbestimmungen vorgenommen hat,

Unsere Todtenliste schliesst mit dem Namen eines unserer hervorragendsten
Collegen, des Herrn Emil Plantamour aus Genf, welcher im Alter von 67 Jahren vor
wenigen Tagen einer mehrmonatlichen Krankheit erlegen ist. Der Sprecher steht noch
zu sehr unter dem Eindrucke dieser erschütternden Kunde — er erfuhr den Tod Plan

6

 
