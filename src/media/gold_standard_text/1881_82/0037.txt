|
|

 

TTT

a
=
=
s
z
x
=
=
>

29

M. Hirsch répond que ce moyen, en effet trés facile, pourrait peut-étre employé
pour la règle normale si l’on parvenait à laisser les traits intactes, mais qu’on ne saurait
y penser pour les règles de base, envoyées par les différents Etats et que le Bureau
international n’a pas le droit de modifier en quoique ce soit.

M. le Président remercie M. Hirsch et, le point 2 du Pu an étant épuisé,
il passe aux autres questions à l’ordre du jour.

Il soumet à la réunion le projet suivant du programme pour la Conférence
générale de 1883.

Projet de programme

pour la 7e Conférence générale de [ Association géodésique.

EL. Rapports annuels de la Commission permanente et du Bureau central.
IT. Rapports des Délégués sur l'avancement des travaux dans leurs pays.
III. Résumé de l’état actuel de la mesure des degrés en Europe:
1. Determinations astronomiques des longi-
tudes, latitudes et azimuts . . . . . . Rapporteur M. Packhuyzen.

2. Les triangulations . i M. Ferrero.
3. Les mesures des bases et oe aphaseil

qui y servent : a M. Perrier.
4. Les nivellements de précision . ie M. Hirsch,
©, Les mareosraphes 5 es M. Ibanez.
6. Les déterminations de la aes eeuE par les

différents appareils
7. Nouveaux travaux concernant a rn
8. Les nouvelles publications relatives a la
mesure des deeres 2... Bureau central.

» M. v. Oppolzer.
5 M. v. Bauernfeind.

M. Faye se demande s’il ne conviendrait pas que la prochaine conférence
s’occupat enfin de la possibilité de mesurer un grand arc de parallèle à travers
l’Europe, conformément au projet du Général Baeyer.

M. le Président serait d'accord, si M. Faye voulait se charger de présenter à
la Conférence le rapport sur cette question.

M. Faye accepte volontiers, à condition que M. le Général Baeyer, avec lequel
il s’entendra, l’approuve.

“M. Ferrero demande qu’on désigne également un rapporteur, à la place de feu
M. C. A. F. Peters, pour l’importante question des compensations; il reconnait qu’un
tel travail pourrait difficilement être fait par un seul homme dans le temps restreint
d’une année.

 

 
