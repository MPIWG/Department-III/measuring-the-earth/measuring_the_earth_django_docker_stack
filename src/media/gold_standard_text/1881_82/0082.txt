EEE EEE TE

Sr

ET

|
i
|
j

 

74
als früher beurtheilen zu können erwartete. Was er mit seinen Bemühungen für Resul-
tate in dieser Frage erreicht hat, ist in einem von ihm an Herrn Philipp Plantamour
in Genf geschriebenen und in den „Archives des sciences physiques et naturelles“ Nr.
12 (15. December 1882) abgedruckten Briefe vom 28. November 1882 unter dem Titel

Sur les mouvements périodiques du Sol“ enthalten, worauf wir hiermit zu verweisen
99

uns erlauben.

Ueber die Intensität der Schwere in Bogenhausen, |
welche Herr Oberst v. Orff vor einigen Jahren in Gemeinschaft mit Herrn Professor
v. Lamont mit einem Pendelapparat des Herrn Professors v. Oppolzer bestimmt hat,
ist zu bemerken, dass die hierüber in Aussicht gestellte Abhandlung hauptsächlich des-
halb noch nicht vollständig ausgearbeitet werden konnte, weil Herr v. Orff bis in die
letzte Zeit noch mit einer Spezialuntersuchung über das Auftreten und den Einfluss
einer Personalgleichung bei der Registrirung von Pendelschwingungen beschäftigt war,
die darin ihren Grund hat, dass sich bei der Registrirung von etwa hundert Anfangs-
schwingungen das Pendel dreimal schneller bewegt als bei der Registrirung von hundert
Endschwingungen einer Reihe von zweitausend Oscillationen. Eine solche Personalglei-

chung besteht selbstverständlich bei der von Herrn v. Orff ebenfalls angewendeten Methode

der Koinzidenzen nicht. Seine Beobachtungen nach beiden Methoden zeigen einen syste-
matischen Unterschied bis zu einem Hunderttausendstel der Vibrationsdauer (gleich
1,006 Secunden), und es könnte dieser Unterschied in der vorhin erwähnten Ursache
recht wohl seine Erklärung finden.

Dass diese Differenz noch nicht bemerkt wurde, kann nicht befremden, da beide
Methoden nebeneinander bisher noch nicht angewendet wurden. Als vorläufiges Resultat
seiner Bestimmung, wobei er eine von ihm aufgestellte Korrection der Stativschwingung
und die Clairaut’sche Formel über das Verhältniss der Schwere und die Abplattung der
Erde angewendet, giebt Herr v. Orff an, dass die Bogenhausener Pendellänge mit den
Bestimmungen der Berliner Länge, welche Bessel und Peters vorgenommen haben, inso-
fern übereinstimmt, als sie zwischen beide Ergebnisse hineinfallt. Unsere Commission
hofft, die Abhandlung des Herrn Oberst v. Orff ebenfalls bis zur nächsten Generalcon-
ferenz an die Herren Commissare der Europäischen Gradmessung gedruckt versenden

zu können.
C. v. Bauernfeind.

ARRETE YY

Dänemark.

In den beiden Jahren 1881 und 1882 ist man bei uns mit der Redaction und
mit dem Druck des 4. und letzten Bandes unserer Gradmessung beschäftigt gewesen.
Voraussichtlich wird dieser Band gegen Ende des Jahres 1883 publicirt werden.

Andrae.

eee ea

oe 99 <del ehe

ible md a 1

au va ahmed dis ha

 
