2 ot A PANNE ST mn

RUN

a AR

LIL RICE TT OIL. NL. RON

x

: 135
Le nombre définitif d’équations & résoudre pour la compensation de ce groupe
est de 61, savoir: 12 équations de cédté, 46 dangle et, & cause de la fermeture d’un
polygone intérieur, une équation d’angle et deux de projection en plus. Ce groupe est.
constitué par 42 points rattachés par 93 lignes, dont 86 de visées réciproques et 7 non
réciproques.

Ce travail considérable vient d’être terminé et je m’empresse d’en faire part à
la Commission permanente.

Jonction géodésique des Îles Baléares au Continent.

Ce travail qui a été interrompu lors de la grande opération hispano-algérienne,
a été repris en 1881, mais avec des moyens bien supérieurs à ceux qui avaient été
employés pour les premières observations. En effet, l'expérience acquise dans Vopé-
ration mentionnée et le matériel scientifique que l’Institut espagnol possède depuis cette
époque, ont permis d'employer la lumière électrique pour les observations, à la place
des héliotropes ordinaires. En conséquence j'ai modifié mon projet primitif en établissant
des côtés géodésiques qui, quoique plus longs, offrent des conditions plus favorables de
visibilité. C’est ainsi qu’un côté de 240 kilomètres, Desierto-Torrellas, (voir la
carte) a été, entre autres, déjà observé pendant la nuit avec ces moyens.

Les stations récemment terminées sont celles de Desierto et Mongé, et
celles qui restent à faire en employant la lumière électrique sont celles de Montsia,
Torrellas et Furnds, tandis que pour celles de Cabrera et La Mola de For-
mentera, qui était l’extremité Sud, au commencement du siècle, de la méridienne

célèbre de Dunkerque, on n’a besoin que d’observer pendant le jour, en employant
les moyens ordinaires de la géodésie.

Dans limpossibilité de monter les machines à vapeur à quelques uns des pics
abruptes qui servent de sommets à ce réseau de jonction, la lumière électrique y a été
produite au moyen des piles de 100 éléments de Bunsen, et sur les autres sommets
d'un accès plus facile, la machine à vapeur a été employée comme moteur.

Nwellements de précision.

L’intérét tout particulier de la ligne de nivellement de précision qui traverse
la péninsule du port d’Alicante à celui de Santander en passant par Madrid,
(voir la carte), puisqu'elle devait contribuer, avec les maréographes établis dans ces deux
ports, à la détermination de la différence de niveau entre l'Océan et la Méditerranée,
ma décidé à entreprendre sur le terrain un travail étendu qui devait retarder le com-
mencement de nouvelles lignes de nivellement, mais que je considérais nécessaire pour
pouvoir enfin donner la différence de niveau des deux mers, attendu que la période de
fonctionnement des maréographes qui servent d’extrémités à la ligne est déjà assez
considérable.

 
