EEE RE AE TROT TEE - «

a

coment

 

 

 

CINQUIÈME SÉANCE

Mercredi, le 26 Septembre.

Présidence de M. le Général Bassot.

La séance est ouverte à 9 heures 40 min.

Sont présents :

les délégués, Bassot, Bakhuyzen, Darwin, Guarducei, Crema, Bouquet de la Grye,
Poincaré, Anguiano, Darboux, Valle, Galarza, Mier, Hanusse, Lallemand, Bourgeois, Eütvôs,
Bodola, Bratiano, Cantea, Artamonofÿ, Backlund, Gautier, Foerster, Börsch, Helmert, Albrecht,
Gillis, Schiétz, Muller, Weiss, Schmidt, Heuvelink, v. Bertrab, Schorr, Borrass, Becker, Zachariae,
Hecker, Lehrl, Tinter, Kalmar, Tittmann, Hayford, Porro, Kimura, Tusaka, Rosen, Haid;

les invités, Déchy, Driencourt, Dobrovics, Antalffy, Andres, Guillaume, Harkänyi, Shinjo.

M. le Président donne la parole au Secrétaire pour la lecture du procès-verbal de

la quatrième séance.

Le procès-verbal est lu et adopté.

M. le Président annonce 1° au nom de M. Foerster, qu’aujourd’hui 4 2 heures et
demie il y aura une séance de la commission des finances ; 2° que le Directeur du musée des
arts décoratifs sera heureux de recevoir les délégués & trois heures pour leur faire visiter

le musée.

M. le Président propose, au nom du Bureau, de continuer d’abord la lecture des rap-
ports nationaux; après ces rapports M. Driencourt et M. Guillaume pourront faire leurs
communications. Cette proposition est adoptée.

M. le Président donne la parole à M. Galarza y Vidal, délégué de l'Espagne.

M. Galarza présente son rapport (voir A. XI).

M. le Président remercie M. Galarza et demande s’il y a un délégué chargé de pré-
senter un rapport sur les travaux géodésiques en Grèce. Comme personne ne se présente,

 

ait

arcadia LA ddA dak

JA sa hl

a Mu Da hal dh at

phe DOL

 
