 

214

 

Simplonhospiz, Berisal, Brig; Sitten, Martigny, St. Maurice. Neu sind von diesen Stationen
nur die 4 ersten; auf den nn an der Simplonstrasse fanden schon im Jahre 1900
Messungen statt us alleiniger Verwendung eines Chronometers und ohne Bestimmung
des Mitschwingens); die Ergebnisse der früheren Messungen auf den 3 letzten Stationen
sind im 7. Bande des Schweiz. Dreiecksnetzes, sowie auch im Heımerr’schen Schwerebericht
yon 1900, S. 253, veröffentlicht. Als Koinzidenzuhr wurde eine neue RırrLer’sche Pendeluhr
mit elektrischem Aufzug benutzt, deren Gang konstanter war, als bei der von 1902 bis
1904 verwendeten. Auch wurde der bis dahin befolgte Beobachtungsmodus insofern etwas
verändert, als unmittelbar nach der ersten Zeitbestimmung nur 2 Pendel, etwa 12 Stunden
später 4 Pendel und am Abend wieder 2 Pendel beobachtet wurden. Dies Verfahren wurde
bis zur Schlusszeitbestimmung fortgesetzt. Die in Basel vor und nach den Arbeiten ausge-
führten Anschlussmessungen ergaben für die Pendel 30, 31, 32 u. 64 Änderungen von
— 14,,4, 110 a, 0 Minh. d, 4 Dez, Hine Diskussion der Uni bide So — 8; U. S. W.
liess ausserdem noch sprungweise Änderungen auf den verschiedenen Stationen erkennen,
die bei Pendel 32 recht bedeutend waren. Herr Dr. Nreruasumer berechnet den durchschnitt-
lichen m. F. einer Ag-Bestimmung aus dieser Reihe zu + 0.0011 cm/sek?.

Zwischen den Proc.-verb. von 1906 und der uns durch Herrn Prof. Dr, A. Riccenpacu
in dem eingangs erwähnten Schreiben vom 21. Mai 1906 übermittelten Zusammenstellung
der Ag-Messungen von 1905 fanden sich einige systematische Abweichungen in den Höhen-
angaben; ich habe mich bei der Aufstellung unsrer Tabelle konsequent an die Rıcckn-
BACH chen Zahlen gehalten und die betreffenden Stellen durch ? kenntlich gemacht.

Schlussbemerkung. In den Proe.-verb. von 1906 giebt Herr Dr. NırruamMmer
die Resultate einer Neureduktion der Musserscoumirr’schen Ag-Messungen aus den Jahren
1897 u. 98 (10-+8 Stationen); hierzu bemerkt Herr Prof. Rısarngach in dem oben
erwähnten Schreiben, dass wahrscheinlich der grössere Teil dieser Stationen (etwa 12)
wegen verschiedener Mängel wird verworfen werden müssen. Ich habe deshalb vorläufig von
der Aufnahme dieser Resultate in unsern Bericht abgesehen.

ar hdd AL ad ddan dima

 

 
