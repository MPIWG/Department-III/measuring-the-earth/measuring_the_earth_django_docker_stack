 

 

ANNEXE A. IV®,

‘

ITALIE.

Rapport sur les travaux exécutés par l'Institut géographique militaire
italien dans les années 1903-1906.

TRIANGULATION.
Pendant le mois de Juin 1903, on a rattaché la station internationale de Carloforte
au réseau fondamental de l'ile de Sardaigne par des mesures angulaires aux stations de
Punta Sèbera et Isola del Toro. En même temps on a déterminé de nouveau la distance
entre la mire méridienne et l'axe du télescope zénital, et on à déduit l'azimut astronomique
de Punta Sèbera en fonction de celui de la mire susdite. On a ainsi obtenu tous les Elements
pour le caleul de la deviation de la verticale; en effet, en retenant pour la latitude

astronomique de Carloforte la valeur:
39° 8’ 8”,980 ')

et Vazimut astronomique de Punta Sèbera, vu du centre de la station étant:

102° 19’ 48”,385 ?)

on obtient:

2 7 A (deviation orientale)

— — 0,309 (déviation méridienne)

or de a Dell cv on ph que l’ellipsoide de Bessel coupe normalement la

verticale au niveau de la mer de Gönes.
La distance entre le zenith astronomique et le zenith ellipsoidique sera done a

Carloforte :
e= 1,148
et son azimut:
y = 267° 31
* *
x

Dans la méme année, et précisément du mois d’Aoüt au mois de Septembre, on a

1) Cfr. Resultate des Internationalen Breitendien
2, Oft. Appendice al collegamento geodetico dell

 

stes (Band I von Tu. ALBRECHT).

a Sardegna al Continente. Firenze 1904,

 

ét drop vd mode nome

 
