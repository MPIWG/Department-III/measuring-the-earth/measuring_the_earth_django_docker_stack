 

i:
|
|

128

AFRIQUE.

ALGÉRIE ET TUNISIE.

Altitudes des repères de contrôle voisins des medimarémétres.

nme nennen

Noms
des

mers

Repéres de contréle des médimarémétres

 

Désignation de
Vemplacement des

reperes

0

Méditerranée

 

Noms des Numéros
localités où sont| matricules
situés des
les appareils reperes
La Goulette —

Bône Ar 1152

Alger —

Oran —

 

Rivet, & 0m,25 de l’ouver-
ture du puits contenant le
médimarémètre

Sommet du goujon fixé,

près du médimarémètre, à
Vextrémité de la jetée

x

 

ASIE.

Rivet scelle sur le quai,
N N
à côté du médimarémètre |

 

 

 

. : Altitude ortho-

a ee metrique du Periode
be Gnneet niveau moyen, | d’observations
au zéro des Ponts EG ale on des
et Chaussées de aan d9 la médimarémètres

la Goulette Series
m m
+ 0,953 + 0,102 1890—1905
+ 1,863 + 0,112 1890—1905
| + 1,830 — 0,07 1904 et 1905
+ 1,707 — 0,023 1890—1905

Il existe, dans l’Inde proprement dite, 32 stations d'observations marémétriques,
dont 5 permanentes et 27 temporaires. Dés maintenant, 24 sont rattachées au réseau des
nivellements. Ces stations sont les suivantes:

Tuticorin

A. — Stations rattachées au réseau des nivellements.

1°, — Stations permanentes.

Karachi, Bombay I, Bombay II, Madras, Kidderpur.
2°, — Stations temporaires.

Okha Paumban Dublat

Port Albert Victor Négapatam Diamond Harbour

Bhavnagar Beypore Elephant Point

Marmogao Cocanada Rangoon

Karwar Vizagapatam Humstal

Cochin False Point Nowanar

Aa the lala

 

 
