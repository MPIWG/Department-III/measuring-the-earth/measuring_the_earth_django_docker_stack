 

D

 

166

rien either ee nombre

OBSERVATIONS ASTRONOMIQUES.

Dans mon rapport de 1903 j’ai parlé de la position géographique de neuf points et
j'en ai communiqué deux dont les calculs étaient déjà terminées. Maintenant, qu'on à
calculé les positions des sept autres, ainsi que celles d’autres stations où les observations
ont éte faites ensuite, je puis communiquer la liste de ces positions.

REO

 

eS a nk

 

 

 

Noms. Longitude. Latitude.

a

Huamantle ©. ae 5 son 19 18 2008

Fulancimee . 2... 0... - = 3 1107) 20 14%

Alpen, Gee. Ge. ya, u Aa, A

C. de San Juan (Puebla) 8.52.29: 19: 3 19.16

Tecamachaleo. 2. ..... - 5:51 18 922359

ee 08 18 54 18.16

Kom ee ea

Cerro, Verde 2. pas déterminée | 17 58 39.06

San, Lis POLOS . a Ds 0.90

©, Pothrio Diana u « a 5 AG 82128 Alot

Op Suen Gos et. Pe. 4+ 29 8.61 | pas déterminée |

Nosklage me ab een, 447 0.12 | 8119 49.90
Ces positions se rapportent aux piliers d’observation. |
Dans toutes ces déterminations on a employé pour les latitudes la méthode de Talcott i

et, pour les longitudes, celle des signaux télégraphiques. Quelques-unes de ces stations ap-
partiennent au réseau de la triangulation, comme celle de > Apam” qui correspond à l’ex-
trémité N.O. de la base de ce nom; celle du » Cerro de San Juan” prés de Puebla et celle
de »Cerro Verde”. Les trois dernières positions du tableau, c’est-à-dire, C. Porfirio Diaz,
C. Juérez et Nogales, furent déterminées surtout dans le but de rattacher ces points de notre
frontière au réseau des États-Unis, et de pouvoir de cette manière éclaircir certains doutes
que nous avions encore sur la vraie longitude de notre Observatoire national de Tacubaya.

On a fait aussi dernièrement des observations de longitude et de latitude à une des
extrémités de la base projetée pour la triangulation du Nazas, dont j'ai parlé plus haut.
On n’a pas encore eu le temps de terminer les différents calculs.

NIVELLEMENT DE PRÉCISION.

On a commencé, un peu tard il est vrai, les travaux du nivellement de précision,

 
