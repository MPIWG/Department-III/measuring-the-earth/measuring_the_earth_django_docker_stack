 

306

B. Gestion administrative.

1.

Le fonds des dotations a été géré comme d’habitude. En nous réservant le
dépôt conventionnel des comptes exacts des recettes et des dépenses, nous donnons ci-

dessous un aperçu du mouvement des fonds pendant l’année 1907.

Recettes.

Solde actif des fonds à la fin de 1906 .
Contributions pour 1906.
Contributions pour 1907.

Intérêts: du Kur- und No Eischo Ritterschaftliche Deren

kasse & Berlin .

> du Königliche as du dd

Berlin

Dépenses.

Indemnité au secrétaire perpétuel .

Total :

Pour le service international des latitudes (au ort de ly on

> > > > » > (au sud de l’équateur)
Pour d’autres travaux scientifiques (déterminations de la pesanteur

et détermination de la figure de la terre)
Pour l'achat et la réparation des instruments,
Pour frais d'impression

Frais de transport, port de ites fais a? ion

Total:

M.

>

>

>

>

52 759,74
10 600,00
59 900,82

533,70
1 171,40

M. 124 965,66

M.

>

»

»

» .

»

»

M.

5 000,00
44 339,08
17 114,75

1 211,00
185,42
265,25
996,00

69 111,50

Par conséquent & la fin de 1907 le solde actif était

Sur cette somme est déposé:
auprès du »Kur- und Neumärkische Ritter-
schaftliche Darlehnskasse”’ & Berlin .
aupres du »Königliche de (Preus-
sische Staatsbank)’’ a Berlin . :
dans la caisse du Bureau central pour les frais
de l’administration.

Total:

 

 

M. 15 262,00

» 39.092,16

» 1 500,00
M. 55 854,16

M.

55 854,16

 

 
