FETT

898
magnetische Störungen ausser von magnetisch wirkenden Gesteinen auch von Unregel-
mässigkeiten der Erdströme herrühren ; doch glaube ich dafür eine bessere Erklärung geben
zu können.

So lange nämlich, als das Wort Schwerestörung nur zur Bezeichnung jener Stö-
rungen benützt wird, welche mit dem Pendel und mit dem Lote beobachtet werden können,
ist es tatsächlich nicht möglich einen Zusammenhang festzustellen zwischen den beider-
artigen Störungen desselben Ortes.

Die von einer Masse ausgeübte magnetische Kraft ist nämlich nicht mit ihrer An-
ziehungskraft, sondern mit den Gradienten dieser Anziehungskraft proportional.

Wenn X,Y,Z die Komponenten der magnetischen Kraft bezeichnen, welche eine
Masse ausübt, für welche die Komponenten der Magnetisierung x, 6,7 constant sind, ferner
V das Potential der Massenanziehung, G die Gravitationsconstante und o die Dichte be-
deutet, dann bestehen die Gleichungen:

 

 

 

 

alu eV HL 6 ON ar
De Cie, On? Go dxoy | Ge Ox OZ |
ao B dv D ON À
a ie Pet le.
| Go d0x0y Gi Ge oy Go oy az )
= de 0 NI Bo JU OV
be a Ox 0g ur Go dy oz a Go 92°

Im Falle nicht homogener Magnetisierung gelten diese Gleichungen nur für unend-
lich kleine Raumelemente der Masse, und wenn diese in eine andere eingeschlossen ist,
dann sind die Grössen «, @,y und auch ¢ durch ihre Differenzen zu ersetzen, den beiderartigen
Massen entsprechend. Mit Benützung der Beziehung AV=0 erhalten wir dann:

 

 

 

 

 

xe_Y = one sis ae IV | By eV a iv |
er... Ge oe ae |
yo Ne co. 29% NM Po aß eV
0 == Sum 2 Sue mn rn 19.
u ae a x) Go oxow Ge a, Ge dy oz
a ae el) Zoe 07 Vi ae (27? ze): a
Ing ù x? Ho Go Oxdy ' Go Oxdz TGs Go dy dz

wodurch der Zusammenhang zwischen den magnetischen Wirkungen und jenen Grössen
ausgedrückt ist, welche mit Hülfe der Drehwage zu ermitteln sind.
Im Falle die magnetischen Massen längs einer Geraden, z.B. in der Richtung der
y gleichmässig verteilt sind, oder Stufen bilden, wie sie in den Figuren 16 und 17 des
vorangehenden Kapitels Herdentalkt sind, (magnetisch tektonische Linien) kann man diese
Gleichungen durch viel einfachere an:
I 50

 
