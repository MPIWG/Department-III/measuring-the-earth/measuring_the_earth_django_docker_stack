denne oh tama ay ot

ra rn

abated ane

va
oo pme me

 

145

de la voie, dans la ligne mesurée, donc à 245 millimètres à l’est de l'axe même du tunnel.
Ils sont situés:

Le n° 1 à l'endroit où finissait la mesure dans la galerie de direction nord et où
commençait la mesure sur rails du côté nord (Brigue).

Les n% 2 à 9 de 100 en 100 portées en général, soit environ de 2400 en 2400 mètres.

Le n° 10 à l’endroit où finissait la mesure sur rails du côté sud (selle) et où com-
mençait la mesure dans la galerie de direction sud.

II. ORGANISATION DU TRAVAIL.

La mesure de la base du Simplon a été scindée en deux parties: 1° Mesure de la
section extérieure au tunnel, entre les repéres I et III, qui devait se faire, aller et retour,
en dernier lieu; 2° la mesure du tunnel lui-même entre les repères III et V, à faire, aller
et retour, dans un temps donné.

La Direction des Chemins de fer fédéraux avait en effet mis avec la plus grande
obligeance le tunnel à la disposition de la Commission géodésique pendant cinq jours entiers,
du 18 mars à 6 heures du matin jusqu’au 23 mars à la même heure. Dans la deuxième
partie de la mesure, celle du tunnel, le travail devait donc être ininterrompu pendant le
temps dont disposait la Commission, et il a été exécuté par trois équipes toutes semblables,
travaillant chacune pendant huit heures consécutives. La Direction des Chemins de fer fédéraux
avait organisé pour cela un service de trains qui amenait au travail l’équipe montante et
ramenait à Brigue l’équipe descendante.

Composition d'une équipe de mesure.
Total.
Premier groupe, groupe des repères mobiles :
1 chef, polytechnicien (cand. ing.);
1 assistant, ouvrier intelligent;
LQ: porteurs de repères mobiles, ouvriers 0 oo . ., , 12

Deuxième groupe, groupe de mesure:

1 chef, secrétaire, en même temps chef de l’équipe;

2 observateurs, ingénieurs ;

2 auxiliaires, porteurs de piquets-tenseurs, polytechniciens (cand. ing.);

2201des Quer. dr d np e il: .

Troisième groupe, groupe du matériel:

1 lampiste;

2 .aides, ouvsiers 2.2.2... wa ee
Total d’une aupe . . . 22
I 19

 
