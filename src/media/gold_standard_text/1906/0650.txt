 

 

180

 

 

 

 

 

 

 

 

 

| Zahl der
Station Zeit Sätze zu je
3 Pendeln
München, Stw... 1897, Juli 8— Juli 29 8
Innsbruck 3... yn Aug. 30— Sept. ] 6
München, Stw. .. n Sept. 6— Sept. 7 4
Koburo u... n Okt. 19— Okt. 23 8
München, Stw. .. n Okt. 29— Dez. 28 10
Miinchen, Stw. .. 1898, Jan. 4— Marz 16 12
Potsdam, G. 1... „ März 22— April 13 14
München, Stw. . „ April 20— Mai 24 12
München, Stw. .. 1899, Febr. 23—März 13 8
Wien, M &1.... yn Marz 19—Marz 24 10
München, Stw. .. „ Marz 29—April 1 6
1 „ yn Mai 4—Juni 2 8
Ergebnisse.
D - —
Station À À H g Beobachter | Jahr Referenz-Stat.
Innsbruck Aq on 11. 9483 584 980,543 von Sterneck 1887 Wien, M. GI:
1 16,2 24,1 576 586 Anding 1897 München, Stw.
München, Stw..| 48 8,8 1186 529 980,735 von Sterneck 1891 Wien, M.G. I.
1! 8,8 36 529 750 Messerschmitt 1893
1 8,7 36,6 595 7148 Oertel, v. Filz | 1896 1
i 8,7 36,6 525 750 Anding 1898 Potsdam, G. I.
1 8,1 36,6 525 748 1 1899 Wien, M GT
1 8,7 36,5 524,4 749 Haid 1900 11
Koburg OU ne 290 9810361), Haasemann | 1895 | Potsdam, G.I.
1 15.8 58,1 298 032 Anding 1897 | München, Stw.

 

 

 

 

 

 

 

Als Referenzwerte g in Wien, Miinchen und Potsdam sind angenommen 980,876,
980,749 und 981,291 cm/sek’.

Ausser den veröffentlichen Ergebnissen sind im Jahre 1902 noch 13 Stationen im
westlichen Bayern gegen München festgelegt worden (siehe Schwerebericht für 1903, 8. 165),
so dass, abgesehen von den Verbindungen der Hauptstationen Wien, München und Potsdam,
gegenwärtig in Bayern 48 relative Schwerkraftsbestimmungen vorliegen.

1) Prof. Haasemann giebt in seiner Publikation: Bestimmung der Intensität der Schwerkraft auf 55 Stationen
von Hadersleben bis Koburg ete., Berlin 1999, S. 38, für Koburg drei g-Werte an, die, auf das Wiener System reduziert,
lauten: 9, = 981,036, g, = 981,036, g, = 981,032 cmysek? und auf verschiedenartiger Berücksichtigung der Pendel-
änderungen zwischen den Potsdamer Anschlussmessungen beruhen. Ich habe hier den Wert 9, benutzt, den Herr Prof.

Haasemann als den zuverlässigsten bezeichnet.

 

 
