 

fi)
hi
Bit
i
i

Sas es

 

 

 

 

te
ma
i
r

296
que les stations de Chilla-Cocha et Fierro-Ureu sont aujourd’hui terminées et que nos ob-
servateurs sont actuellement & Guacha-Urcu et Colambo (latitude 4° 20’ environ, latitude
de Payta 5°5). M. le capitaine Perrier à fait la reconnaissance et la construction des
signaux jusqu'à la frontière péruvienne. Les brigades se suivent maintenant parallèlement,
de sorte que les opérations sur les deux chaînes est et ouest sont simultanées à la même
hauteur, Deux points sont à signaler. Dans les quatre dernières stations, on a employé
l'héliostat concurremment d’ailleurs avec les mires, les signaux ayant été préalablement
construits par les reconnaissances. On a observé en effet que, dans cette région, malgré la
fréquence des brumes, le Soleil brille des que les nuages sont dissipés, en sorte que, a part
les jours oti la visibilité est nulle, l'emploi de l’héliostat est possible.

D'ailleurs, comme l’héliostat est doublé par la mire, comme nous venons de l’expli-
quer, on n’est pas exposé à perdre une journée favorable,

En second lieu, afin de rattraper autant que possible le temps perdu, le capitaine
Pevronnez, chef par intérim en 1904, a cru devoir remplacer les triangles de 50 km de
côté, qui avaient d’abord été prévus, par des triangles beaucoup plus grands de 100 km
environ: le nombre des stations se trouvera donc considérablement diminué, mais en revanche
on peut se demander si le nombre des jours de visibilité suffisante ne va pas diminuer dans
la même proportion. Toutefois les officiers ont observé qu’en dehors des jours, malheureuse-
ment trop fréquents, où les nuages couvrent les sommets les plus rapprochés et où aucune
opération n’est possible, la vue s'étend à de grandes distances. Nous ne pouvons que nous
en rapporter à leur expérience du pays.

Nous devons observer que les dimensions de ces triangles devront être progressive
ment réduites à mesure qu’on s’approchera de la nouvelle base à mesurer, afin de faciliter
le rattachement de cette base. D'un autre côté il va y avoir une assez brusque inflexion
de la chaîne vers l’Ouest afin de rejoindre la côte à Payta et un brusque changement
d'altitande au moment où l’on franchira la frontière péruvienne.

Astronomie. — Une station astronomique avait été installée à Cuenca. Les opé-
rations furent terminées au mois d'avril. La longitude fut déterminée par M. Maurain
à Cuenca et par M. Perrier à Quito; la latitude et l’azimut l'avaient été antérieurement.
Le nombre des déterminations à Cuenca est surabondant; à Quito le temps a été moins
favorable, mais les déterminations sont amplement suffisantes, la marche de la pendule étant
bien connue par les observations de M. GONNESSIAT.

Une station astronomique avait également été prévue vers le quatrième parallèle.

L'emplacement n’en est pas encore choisi; nous discuterons plus loin l'opportunité
de la création de cette station.

La station astronomique principale de Payta doit surtout attirer notre attention;
on y a déjà mesuré la latitude, il reste à y faire l’azimut et la longitude. Payta est relié
à Cuenca par Machala, Chacras et les lignes péruviennes. On pourra done mesurer, soit la
différence Payta-Cuenca, soit la différence Payta-Quito.

 

Ahad nis Le dl snes

|
i

 
