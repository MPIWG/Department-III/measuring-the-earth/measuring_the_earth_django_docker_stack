 

264 Ä in

Nach Fertigstellung der Druckhandschrift wurde mit dem Drucke
des Heftes III begonnen, der um die Mitte September 1906 vollendet war,
so dab diese Veröffentlichung bereits auf der Budapester Allgemeinen Kon-
ferenz vorgelegt werden konnte.

Im Jahre 1907 wird mit der endgültigen Bearbeitung der ver-
schiedenen Verbindungen der westeuropäischen Dreiecksnetze mit der russisch-
skandinavischen Breitengradmessung begonnen werden. Für mehrere von
ihnen liegen schon seit längerer Zeit ausgeführte Rechnungen vor. — Sobald
die Anschlußdreiecke ‚an die dänische Gradmessung.in Südschweden bekannt
sein werden, sollen auch sofort die Rechnungen von Kopenhagen aus nach
Norwegen und durch ganz Schweden hindurch nach dem nördlichen Teile der
russisch-skandinavischen Breitengradmessung: in Angriff genommen werden.“

A. Börsen.
2,

Die Untersuchung der Krümmung des Geoids in den Meridianen
und Parallelen hat Herr Prof. Dr. Scuumanx in Aachen durch Herstellung einer
Druckhandschrift für den westeuropäisch-afrikanischen Meridianstreifen. weitergeführt.
Diese Abhandlung soll in den „Verhandlungen in Budapest“ Aufnahme finden. Eine
zur Ergänzung dienende übersichtliche Darstellung der wichtigsten Dreiecksverbindungen
für diesen Meridianstreifen wurde im Zentralbureau unter Leitung von Herrn Professor
Dr. Börsch von Herrn G. Förster. gezeichnet.

Gegenwärtig führt Herr Prof. Dr. Scaumasx eine ergänzende ‚Berechnung für die
Europäische Längengradmessung in 52° Br. aus, die sich durch neue in ihrem Bereiche
ausgeführte geographische Längenbestimmungen erforderlich macht.

In den „Sitzungsberichten der Königl.. Preußischen Akademie der Wissen-
schaften“ von 1906, S. 525 u. f., wurde von F. R. Hermert eine Abhandlung über die
Größe der Erde auf Grund. der. in Europa. ‚gemessenen und.in rechnerischer.Bearbeitung
vorliegenden Meridian- und Parallelbogen veröffentlicht. Unter Annahme des Bsssezschen

Abplattungswertes 1:299,15 folet für die Äquatorialhalbachse a des Erdellipsoids in
internationalen Metern:

6878455 m # 127 m mittl. F., aus dem russisch-skandinavischen Meridianbogen,
Amplitude 25° 20’, Mittelbreite 58°. 0';

6.84.4998, (=:455 x aus : dem westeuropäisch-afrikanischen Meridian-
bogen, Amplitude 27° 2', Mittelbreite 47° 19’;
GS dors | ate 105 à aus der Längengradmessung in 52° Br., Amplitude

69° 5 in Länge;
h aus der Längengradmessung in 47'}° Br. Am-
plitüde 19° 12° in Länge.

6377350 = 650

Im Mittel ist

a = 6378150 m.

 

 
