 

 

8 10

Herr Professor Borrass informierte in der Zeit vom 17. bis 24. Oktober auch
den mexikanischen Ingenieur Herrn Romo, der im Auftrage der Erdmessungskommission
von Mexiko erschienen war, in der Ausführung von Pendelbeobachtungen.

Als Publikation des Centralbureaus, Neue Folge No. 10, erschien im Laufe des
Jahres die Arbeit des Herrn Geheimen Hofrats Prof. Dr. Han in Karlsruhe: ,,Bestimmung
der Intensität der Schwerkraft durch relative Pendelmessungen in Karlsruhe, Straßburg,
Leiden, Paris, Padua, Wien (Sternw.), Wien (Mil.-Geogr. Inst.) und München, ausgeführt
im Auftrage der Internationalen Erdmessung.“

Jetzt wird es auch möglich sein, das Netz der Haupt- und Verbindungs-
stationen der Schweremessungen zur Ausgleichung zu bringen.

a.

Bestimmung der Schwerkrait aui dem Indischen und dem Stillen
Ozean und an deren Küsten. Herr Prof. Dr. Hecker begann seine Reise am
98. März von Bremerhaven aus auf dem Dampfer „Weimar“ (5000 Tonnen) des Norddeutschen
Lloyds. Hier fand er alles wünschenswerte Entgegenkommen, insbesondere wurden ihm
2 Kabinen zugewiesen und das zahlreiche Gepäck günstig untergebracht. Sturm und schwere
See gestatteten den Beginn der Beobachtungen erst von Gibraltar ab. In Melbourne
eing Prof. H. anLand. Bis dahin wurden Beobachtungen mit 5 registrierenden Barometern
und mit 6 Thermometern an Bord erhalten März 28, April I, 2 3, 2 1 20 täglich,
Mai 1-8, 5, 6, 8-13 täglich; oftmals gelangen zwei volle Reihen an einem Tage.
Einige Beobachtungen werden wegen schlechter Films ausfallen.

In Melbourne und Sydney gelangen auch gute Pendelbeobachtungen auf den
Observatorien, wo Prof. H. sehr freundlich aufgenommen wurde.

Die Weiterfahrt erfolgte auf der „Sonoma“ (6000 T.). Beobachtungen an
Bord gelangen von Auckland ab bis zu Ende am Juni 26—30, Juli 1, 2 (Datumwechsel),
9-19, täglich meist 2 Reihen. An Bord sowohl wie in San Franeisco fand Prof. Bl.
großes Entgegenkommen. Von Pendelbeobachtungen wurden hier sehr ausgiebige Reihen
erhalten. Die Weiterreise verzögerte sich bis zum 30. August infolge des Krieges,
erfolgte dann aber auf einem sehr großen und schönen Schiff, der „Manchuria“
(14000 T.). Die Schwerkraftsmessungen an Bord gingen in durchaus normaler Weise
von statten, bis an den beiden letzten Tagen Sturm eintrat.

In Tokyo war den Pendelmessungen die Bodenunruhe sehr hinderlich; das Mit-
schwingen wurde ‚hier mittelst des neuerdings noch verbesserten Apparats von Herrn
Prof. Nacaora ermittelt. Die Abreise von Tokyo, wo Prof. H. sehr gut aufgenommen
worden war, erfolgte mittelst des Dampfers „Zieten“ Mitte Oktober.

Nach der Ankunft in Shanghai begab sich H. nach dem Observatorium zu
Zi-ka-wei und erhielt auch hier die Erlaubnis zu Schweremessungen mit jeglicher Unter-
stützung, die namentlich deshalb wichtig war, weil einige Apparate der Reparatur be-
durften. Es gelangen sowohl Pendelmessungen wie Barometer- und Thermometer-
beobachtungen. |

Dd asa nis bandas sie

 

t
i
;
}
|
4
}
|
}

 
