rw

 

u ml Il

il

ITU LME PORC MERS prit

ry

mr

FT

101

20, Cinq repères principaux (fig. 2) établis à Helsingborg, Storlien, Riksgränsen,
Gellivare et Kiruna, formés d’une tige de bronze scellée horizontalement dans le rocher et
terminée par un médaillon cylindrique formant saillie sur la paroi verticale et portant une
inscription.

39, Un grand nombre de repères secondaires, constitués par de simples goujons en
fer ou en bronze, de 0™,1 de longueur et 0,023 de diamètre (fig. 3). Ces goujons sont
enfoncés verticalement et presque entièrement dans la roche (fig. 4). Leur espacement moyen
est de 2 à 3 km.

Mire (fig. 5, 6 et 7). — La mire, construite par J. Kaver, de Berlin, affecte la
forme dune boite creuse, parallélipipédique, longue de 3™,02, large de 0™,11 et épaisse de
0m,037. La division, disposee dans l’axe de la mire, est formee de cases alternativement
rouges et blanches, peintes sur fond blanc et réunies par groupes de dix. En regard des
limites des groupes, et de part et d’autre de la division, sont peints en rouge deux nombres
dont la somme est toujours 100. La chiffraison de gauche croit ainsi de 0 a 60 a partir
du talon de la mire, tandis que celle de droite décroît de 100 à 40 à partir du même point.

Dans sa partie médiane, la mire porte, gravés sur deux petits disques métalliques
en bronze, deux repères distants d’un mètre l’un de l’autre.

En cours d'opérations, tous les deux jours, on détermine, à l’aide d’un mètre étalon
en acier, dont l'équation est connue, la distance exacte de ces deux repères, de manière à
pouvoir suivre les variations métriques de longueur de la mire avec le temps.

D'autre part, l'écart constant entre la distance des deux repères et le mètre moyen
de la division de la mire a été déterminé une fois pour toutes. On peut done, à tout moment,
calculer l'erreur moyenne métrique de la division de la mire et en tenir compte dans les
résultats du nivellement.

Le mètre étalon lui-même a été étalonné par la Normal-Aichungscommission de Berlin.

Niveau. (fig. 8). — Au début, on utilisait le niveau de BamBerG (modèle adopté
par la Landesaufnahme de Prusse) avec nivelle invariablement fixée à la lunette. En 1894,
le triangle à vis calantes a été remplacé par une genouillère permettant, avec une nivelle
sphérique, d'amener le pivot de l'instrument dans une position voisine de la verticale,

Le fin calage s’opère avec une vis micrométrique agissant sur la traverse qui porte

la lunette.
Les caractéristiques de la nivelle sont les suivantes :

Largeur des divisions de la fiole. . . . . . 2mm2,
Valeur angulaire correspondante. . . . . . 5Smm,2 a 6mm,3,
Rayon de courbure de la fol . , . 2 27m a 63m.
Méthode d'opérations. — Les mêmes règles ont été observées, presque sans change-

ments, de 1886 jusqu’à la fin des opérations. Ces règles sont les suivantes:

19, Le niveau est placé à égales distances des mires avec une approximation suffi-
sante pour qu’on n'ait pas à modifier la mise au point du réticule dans le passage du
coup d'arrière au coup d'avant. Cette distance, inférieure à 60 m, en principe, est mesurée
au pas sur les routes et par le nombre des rails sur les chemins de fer.

II 14

 
