un

ere

nr

TREE ee ts nes

 

49

 

par M. le Baron Eötvös, developpe les conclusions qui se trouvent dans le rapport provisoire
imprimé (voir le rapport avec les conclusions définitives B. Va).
Personne ne demandant la parole, les conclusions du rapport de M. Darwin sont

mises aux voix et adoptées.
M. le Président donne la parole à M. Zallemand pour présenter son rapport (voir le

rapport B. Vb).

Les conclusions du rapport de M. Lallemand sont mises aux voix et adoptées.

M. le Président invite les délégués à faire des propositions sur le lieu de la prochaine
réunion.

M. Darwin dit que, quoiqu'il n’ait reçu aucune autorisation de sor gouvernement pour
faire une invitation officielle, il serait fort heureux de voir la prochaine Conférence générale
se réunir en Angleterre. Aux mois d'Août et de Septembre un grand nombre de savants
étant absents de Londres, il serait peut-être peu opportun de se réunir dans cette ville;
il est d'avis qu'il vaudrait mieux se réunir à Üambridge avec son ancienne université, où il
se ferait un plaisir de recevoir les délégués.

M. le Président remercie M. Darwin et demande s'il y a d'autres propositions.
Puisque personne ne demande la parole, M. le Président déclare que le Bureau, tenant compte

de la proposition faite par M. Darwin, fixera en temps opportun, la date et le lieu de la

prochaine Conférence.

M. le Président invite les délégués qui ont à proposer des vœux à les lui remettre
bien rédigés afin qu’on puisse les voter.

Les vœux suivants sont proposés:

. I par M. Darboux.

L'Association géodésique émet le vœu que les gouvernements francais et italien
veuillent bien se concerter pour assurer dans le plus bref délai possible le rattachement
géodésique direct entre la Corse et le continent Italien.

II par M. Gausier.

L’Association géodésique internationale émet le vœu que les gouvernements suisse et
français se mettent d'accord pour profiter des opérations de triangulation exécutées actuelle-
ment par la France, dans le Jura, le long du méridien de Lyon, pour rattacher entre eux
les réseaux géodésiques français et suisse.

III par M. Lallemand.

A l’oceasion des nivellements de précision use dans le bassin de la Mer Rouge,
l'Association géodésique internationale émet le vœu que ces nivellements soient aie
au niveau moyen de la mer déterminé par des marégraphes ou des medimarémètres.

IV par M. Bratiano.

L’Association géodésique internationale émet les deux vœux suivants :

1° que les gouvernements francais et roumain se mettent d’accord pour faire deter-
miner par leurs services géographiques la mesure de la difference de longitude Paris-Bucarest,
afin de fermer le triangle de longitude Paris-Bucarest-Potsdam.

T -

 
