 

en nn

TEE

A ppt

rk
‘was tty pnp perpen

i
em ee nn

mir

IB

 

répété les mesures angulaires aux stations du réseau d’agrandissement de la Base de Foggia.
D’aprés les nouvelles données cette base, mesurée avec la toise de Bessel en 1859-60 sous
la direction de M. le Professeur FRéDéRIC ScHIAVONI, s’accorde encore mieux qu’auparavant
avec la longueur de la base de Sinj mesurée par les Autrichiens. En effet, avant la révision
des mesures angulaires, on avait entre les deux bases une différence de 112 unités du sep-
tième décimale du logarithme !), tandis qu’à present cette différence n’est que de trois de
ces unités ?).

Ce résultat a fait voir qu'il était intéressant de répéter les mesures à toutes les
stations des réseaux de l'Italie méridionale; on a pu alors éliminer un défaut d’origine,
car, dans les anciennes triangulations les angles furent mesurés avec des instruments répé-
titeurs d'après des méthodes qui ne permettaient pas d'obtenir une précision rendue nécessaire
par les progrés de la Géodésie moderne.

En même temps on pourra déterminer plus exactement les différences qui existent
entre la Base de Crati et la Base de Catane, à laquelle est rattachée la triangulation de
l’île de Sicile mesurée de nouveau pendant les années 1895-97.

La révision des mesures angulaires du réseau de la Base de Foggia a aussi donné
l’occasion de calculer les deviations de la verticale a Termoli (point de Laplace) et à l’île
de Lissa (ot il y a une station rattachant la triangulation italienne à la triangulation
autrichienne) par rapport à Gênes.

On obtient pour Termoli:

E = 12,25
y — — 8 ,48
a 14 ‚93
7 == olor 2c
et pour Lissa:
& S'S OCT
y — — 12 ,34
sue 12 ,36
D. 210 0

On voit done que la déviation en latitude que nous avons supposée nulle à Gênes,
ce qui n’est pas en contradiction avec les recherches de M. Hezmerr *), semble augmenter jusqu’à
Termoli et diminuer brusquement à l'île de Lissa, Ces calculs prouvent que l’ellipsoïde de
Bessel s’accorde bien avec les positions astronomiques de Gênes et de Lissa, et indiquent
en même temps qu'il existe probablement une cause souterraine d’action locale entre le

promontoire du Gargano et les îles Curzolaires.
# %
*

1) Cfr. Pubblicazione dell’ Istituto Topografico Militare. Parte I, Geodetica, fascicolo 3°. Napoli 1877-78.

2) Nuove misure angolari della rete di sviluppo della Base Geodetica di Foggia. Firenze 1904.

3) Cfr. Rapport sur les déviations de la verticale par M. HELMERT. Comptes-rendus des séances de la Commission
permanente etc., réunie à Nice 1887.

I 20

 
