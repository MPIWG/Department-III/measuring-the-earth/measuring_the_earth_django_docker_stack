 

|
H
iy
y
bit
\

ANNEXE A, XIIL®.

BADEN.

Mit einer Karte.

 

A., In den Herbstferien des J. 1905 wurden Pendelbeobachtungen auf 10 Stationen
ausgeführt. Mit den in 1897 und 1903 vorgenommenen Messungen gelangen hiedurch die
Beobachtungen im badischen Oberlande und im badischen Teil des Bodenseegebietes zu einem
gewissen Abschluss. Südlich der Linie Colmar-Messkirch sind bis zum Rhein bezüglich
Bodensee nunmehr auf 37 Stationen in mehr oder minder grossen Abständen Schweremes-
sungen gemacht worden. Über die Messungen von 1897 ist bereits in den Verhandlungen
der 12. Allgemeinen Konferenz in Stuttgart berichtet worden (s. 8. 397). Der Vollständig-
keit wegen sind diese Resultate nochmals in der folgenden Tabelle mit den Messungen von
1903 und 1905 aufgenommen worden.

Bei simmtlichen Beobachtungen wurde der gleiche Pendelapparat wie bei den im
J. 1900 im Auftrag der Intern. Erdm. ausgeführten Messungen benützt (s. Veröffentlichung
des Centralbureau, Neue Folge N° 10). In 1897 wurde als Beobachtungsuhr noch die
Hawetx Halbsekundenpendeluhr N° VI verwendet, und auf die zwischen den beiden Uhr-
vergleichungen liegenden 12 Stunden waren die Beobachtungen von 3 Reihen ä 4 Pendel
also von 12 Pendel gleichmässig verteilt. In 1903 und 1905 wurde die Risrrur Sekunden-
pendeluhr N° 48 mit Nickelstahlpendel als Beobachtungsuhr benützt, und waren die Anzahl
der Beobachtungen innerhalb des durch die Uhrvergleichungen begrenzten 12-stündigen
Zeitintervalls auf 2 Reihen & 4 Pendel also auf 8 Pendel pro Station beschränkt worden.
Die Ermittlung des Uhrgangs ist in 1903 und 1905 aus Zeitbestimmungen abgeleitet, die
am Observatorium der Techn. Hochschule in Karlsruhe gemacht wurden und die sich auf
die Rixrrær’sche Quecksilberpendeluhr N° 77 beziehen. Die Vergleichung von Risrcer 48 mit
Rınrıer 77 erfolgte automatisch. Es war hiefür an jedem Beobachtungstag morgens und
abends eine Telegraphenleitung zwischen den auswärtigen Stationen und Karlsruhe vom
Kaiserl. Reichspostamt kostenfrei zur Verfügung gestellt worden. Zum Gelingen dieser
Uhrvergleichungen haben die Oberpostdirektionen in Konstanz und Karlsruhe durch ihre
thatkräftige Unterstützung beigetragen, wofür hier bestens gedankt sei. Die Bearbeitung der
Beobachtungen, ihre Reduectionen bezüglich Temperatur, Luftdiehte, Mitschwingen und auf
kleinsten Bogen ist die bekannte und im allgemeinen die gleiche wie 1897. Jedoch werden
seit 1900 fünf Thermometer abgelesen, ein Pendelthermometer in der Mitte des Stativs,

A nas à oasis di asus

 

 
