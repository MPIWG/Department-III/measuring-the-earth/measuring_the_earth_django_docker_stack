 

ee Te EEE ee <

SR rer ep series

|

 

En comparant l'actif disponible à la fin de l'exercice de 1905 avec l'actif disponible
à la fin de l'exercice de 1902, on voit que, pendant les trois dernières années (1903— 1905),
cet actif a été diminué de 28,179.60 M. (28,974.50 Fr.).

Le petit Tableau suivant confirme et explique ce résultat:

Recettes. Dépenses. Recettes— Dépenses.

M. Br, M. Fr. M. Fr.
en 1903 69,307.63 86,634.54 59,741.10 74,676.37 + 9,566.53 | + 11,958.17
» 1904 72,076.68 90,095.85 78,759.30 92,199.18 = 1682.62. |= 2.108.285
1903 64,549.24 80,686 55 95,612.75 | 119,515.94 — $1,063.51 | — 88,829.39
Total. . 205,988.55 | 257,416.94 229,118.15 | 286,391.43 — 25,179.69 | — 28,974.50

L'accroissement des dépenses dans les deux années 1904 et 1905 s’explique par
l'extension du Service des latitudes dans l’hémisphére austral et par les expéditions pour la
détermination de la pesanteur en mer. En effet ces deux nouvelles entreprises de l’Association,
approuvées par la Conférence générale de Copenhague, ont demandé, dans les deux dernières
années, les dépenses suivantes (voir les tableaux précédents)

en 1904 pour la détermination de la pesanteur sur les océans 19,877.61 | 24,847.01
en 1905 pour le Service des latitudes (hémisphère austral) . 38,208.32 | 41,510.40

Total. .  53,085.93 | 66,357,41

 

Qur la base des résolutions de la dite Conférence, le Bureau de l'Association, dans
sa circulaire du 6 Mai 1905, avait même prévue pour le seul service des latitudes (hémisphère
austral) une dépense de 80,000 M. (100,000 Fr.), tandis que, jusqu'à la fin de l’exereice de 1905,
on a, pour ce but, depense 33,208.32 M. (41,510.40 Fr.) et on dépensera encore pour l’exercice
de 1906 à peu près 16,000 M. (20,000 Fr.). Malgré les grandes dépenses en question,
l'actif disponible à la fin de l'exercice de 1905, 72,414.77 M. (90,518.46 Fr.), est encore
resté considérablement au-dessus du fonds de réserve de 60,000 M. (75,000 Fr.) et d’après
une évaluation approximative de M. le Directeur du Bureau central, l’actif disponible à
la fin de l'exercice de 1906 pour l'exercice suivant atteindra à peu près 71,000 M.
(88,750 Fr.) en y comprenant la rentrée des arriérés actuels des contributions à l'exception
de la contribution arriérée de la Serbie. Le fonds de réserve, dont la conservation a été
recommandée par la Conférence générale de 1898, est donc encore intact et probablement
restera intact jusqu’à la fin de l'exercice de 1907.

En nous appuyant sur les résultats de la gestion dans les trois années 1903—1905,
nous nous proposons, comme d'usage dans les rapports de la Commission des Finances, de
donner une évaluation approximative des dépenses annuelles, pour ainsi dire, régulières
pour le prochain triennium :

avai bh

 

 
