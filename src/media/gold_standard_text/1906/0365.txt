 

321

Le centre du 3e signal, Stynka, a été retrouvé. Au point Radeni le centre avait
disparu, mais l’observateur, le lieutenant-colonel Poganovsky, y a trouvé un pilier, enfoncé

n (
ZAGAIKANY

w,

m

6
>
a

 

PEREPELTSA

 

    
   
 

FAMEGOURA

     

  
 

JSSARI

|

13 TT,

Hi LS TZ VORA

ADENIN A

| ; LE MTSIBARIKI
ik CHOLOK

6 "STYNKA NZ

 

IASSY 23

  

KICHINIOW

ui i
IRRE

47

!
1050000

ARE

en terre, qui servit ainsi à l'installation de l'instrument, Les observations ont été faites par
12 pointés avec un grand instrument universel de Hitpeprann.

Les erreurs des triangles, sont les suivantes: la plus grande, + 2”,299, a été con-
statée pour le triangle Tcholok-Megoura-Stynka, la plus petite, — 0,071, pour le triangle
Megoura-Stynka-Bogiany.

L'erreur moyenne d’un angle, évaluée suivant la formule de FERRERO, est égale à
+0”,474. Les calculs de compensation n’ont pas pu être effectués parce que nous ne con-
naissons pas encore la longueur du côté Radeni-Stynka.

À bd

Jde TEL TUT TREE NT TIEREN TON
RER PONT TT TREE +

“ab

 
