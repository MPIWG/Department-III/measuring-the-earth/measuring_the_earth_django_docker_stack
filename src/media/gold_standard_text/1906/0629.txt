VENEN NTT

me mt ma Le LOL 1 0
DE D LS

im

 

159
Die 4. Ausgleichung gibt demnach die plausibelsten Resultate, nämlich :

a— 6378282 m + 34m (wahrsch, Fehler)

l:= = 297.8 +09 > >

Diese Werte erhalten dadurch eine allgemeinere Bedeutung, dass sie mit anderen, in
neuerer Zeit abgeleiteten (vergl. S. 134) nahe übereinstimmen.

Die für die 4. Ausgleichung ermittelte Differenz 3T,—3T,, an 5,53 zeigt, dass in
der Tat in der oben angegebenen Gegend ein schwächerer Teil der À vorhanden
ist, der durch ergänzende Beobachtungen ermittelt und verbessert werden soll.

Für die 11 Larracr’schen Punkte bleiben die nachstehenden Reste im Sinne der
Lotabweichung im I. Vertikal übrig:

 

 

 

 

 

 

 

 

 

 

 

Westl. gEO8T. Most aus d. Rest aus | Differenz
Station Geogr. Breite Länge gegen 5 dem Lange—
Greenwich Linge Azimut Azimut

o / O° 4 di di 4
@ambxidse, Mass ss: 42 22,9 11 30,8 + 3,21 + 0,18 + 3,03
Ogdensburg N... .... 44 11,9 15 30,2 + 2,14 + 0,64 + 1,50
Monawanda IN Veet 43 0,1 78 53,3 — 0,84 — 4,06 + 3,22
Minnesota Point N. B., Minn. 46 45,5 9% 47 — 4,37 — 5,36 + 0,99
Sault Ste. Marie, Mich...... 46 30,1 84 20,9 — 5,92 — 5,13 — 0,79
Mord River 2, Mich. ....... 45 41,2 So — 4,42 — 3,18 ie 1,24
Willow: Springs, Il......... 41 43,6 87 51,1 + 0,84 + 0,37 + 0,47
Baukersbure, 1... ........ 38 34,9 88 1,8 — 0,70 + 0,19 — 0,89
Gumamsom, Valor ...2....0.. 38 32,8 106 55,5 + 1,74 + 3,41 — 1,67
Salt Lake City, Utah....... 40 46,2 111 5355 + 1,09 — 2,15 + 3,24
Ocden, Uah............. 4] 13,2 111 593,6 + 1,98 — 0,94 + 2,92

. Die Differenzen der hierbei übrigbleibenden Reste sind so klein, dass sie schon
allein durch die zufälligen Fehler der Triangulation und der astr. Bestimmungen erklärt
werden können; hauptsächlich würden die Unsicherheiten der Azimutmessungen in Betracht
kommen.

Sämtliche Ausgleichungen sind unter der Annahme gleichen Gewichts für alle
Fehlergleichungen durchgeführt worden. Die übrigbleibenden Fehler zeigten aber, dass, wie
es vorauszusehen war, die Azimutgleichungen im Durchschnitt grössere Fehler zurücklassen
und daher geringeres Gewicht als die anderen haben. Die 4. Ausgleichung wurde deshalb
wiederholt, indem den Azimutgleichungen für die östlich von Meades Ranch gelegenen
Stationen das Gewicht 0,7 und denen westlich von Meades Ranch das Gewicht 0,4 gegeben
wurde. Die Unterschiede der so erhaltenen Werte der Unbekannten gegen die zuerst er-
mittelten waren aber so gering, dass es im vorliegenden Falle unwesentlich ist, mit welchen
Gewichten die Azimutgleichungen in die Ausgleichung eingeführt werden.

Auf die Heranziehung der Ergebnisse der Schweremessungen wurde bei diesen Unter-
suchungen verzichtet. Dies war z. T. wohl schon deshalb notwendig, weil die Bestimmungen
der Schwerkraft in den U.S. A. fiir diesen Zweck bis jetzt noch zu wenig zahlreich sind

 
