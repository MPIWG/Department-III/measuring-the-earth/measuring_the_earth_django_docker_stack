 

 

269

 

dann 2 Sterneckthermometer und 2 Celsiusthermometer am Umfang desselben. Das schadhaft
gewordene ältere Pendelthermometer ist bei den Messungen in 1905 durch ein neues von
Furss bezogenes Pendelthermometer ersetzt worden. Für die Bestimmung des Mitschwingens
waren bei den Messungen in 1905 je die parallel schwingenden Pendel durch abgepasste
Auflagegewichte zu noch grösserer Übereinstimmung in ihrer Schwingungsdauer gebracht
worden. Als Referenzort dient Karlsruhe. Das Mittel der Schwingungsdauer der vier Pendel
N° 37, 38, 47, 48 war:
1903 vor der Reise 0,508 0883.4

nach » » 72.4
1905: vor (45 hs 69.8
nach >» » 2

Bezüglich der Genauigkeit der Resultate kann der mittl. Fehler in der Differenz der
Schwingungsdauer auf einer auswärtigen Station und Karlsruhe im J. 1897 zu & 6.107 im
J. 1903 und 1905 zu = 4.107 angenommen werden. Diesem Betrag entspricht ein mittl.
Fehler von + 0,0023 em. bzgl. 0,0016 em. in Ag. Wenn auch grösser, so doch von
gleicher Grössenordnung wird der mittl. F. in den Werthen der Schwerestörung zu
nehmen sein.

Die nachfolgende Tabelle enthält zunächst die Beobachtungsstationen in chronologischer
Folge, die geografische Position des Pendelstativs sowie die Angabe des Beobachters (Hap
oder Bürcın). Die Lage des Pendelstativs wurde für jede Station in den betreffenden
Katasterplan aufgenommen, und sind dann durch Rechnung die geografischen Koordinaten er-
mittelt, wobei für die geografische Lage des Nullpunkts in Mannheim die Breite — 49° 29’ 10,91
und die Länge — 8° 27'36”,80 östl. Gr. zu Grunde gelegt ist (s. Veröff. d. Centralbureau
N° 10. 1904 8. 4). Die Höhen wurden nivellitisch bestimmt und zwar in der Regel durch
Anschluss an Fixpunkte des badischen Hauptnivellements; auf den 6 Stationen Feldberg,
Schauinsland, Heiligenberg, Höchenschwand, Todtmoos und Marzell musste in Ermangelung
solcher Fixpunkte das Nivellement an geeignete Höhenpunkte der topographischen Aufnahme
oder an trigonometrische Signale angebunden werden. In der Tabelle sind weiter die beob-
achtete und die reduzierte Schwingungsdauer sowie die aus letzterer abgeleitete beobachtete
Schwerkraft angegeben. Die folgende Kolonne gibt die der Rechnuug zu Grunde gelegte
Dichte der Schicht zwischen Meeresniveau und Erdoberfläche. Für die Reduktion der Schwer-
kraft auf das Meeresniveau enthalten die folgenden 3 Kolonnen die einzelnen Werthe, aus
denen sich bekanntlich die Reduktion zusammensetzt. Hierzu ist zu bemerken, dass die
topographische Reduktion bis jetzt nur für einen Teil der Stationen berechnet ist; die mit
einem Stern versehenen Beträge sind geschätzt durch Vergleich mit ähnlich gelegenen
Orten, für welche diese topographische Reduktionen berechnet worden sind. Mit diesen 3
Reduktionen ergibt sich dann die Schwerkraft im Meeresniveau nach der Beobachtung ;
neben ihr steht in der Tabelle ihr theoretische Wert entsprechend

% = 978,046 (1 + 0,005302 sin? ® — 0,000007 sin? 29). ')

 

1) Hezwerr. Sitzungsbericht der Berliner Akademie 1901.

 

 
