i
i
|

trans

 

aan

em

OTEK IRE Tm BR Le

 

EEE

PRUSSE,

1°, Landesaufnahme.

A, — Altitudes des repéres de jonction avec les nivellements des pays limitrophes,

 

Noms des

Repéres de rattachement

 

a

 

 

Altitudes défi-

 

 

 

 

 

 

 

 

 

 

 

 

 

Définition du zéro

 

pays Noms des localités du Désignation ne auquel sont rapportées
limitrophes de = matt de l'emplacement des repères | dessus du les altitudes
sont situés cules zéro-normal
| prussien (1)
à nh
Niederlande) Nieuwe Schans 1856 Neben der Brücke, gegenüber dem| . 2,674 (1) Zéro établi en 1879,
Zollamt a Observatoire de Berlin,
Frensdorferhaar 5013 Neben der Brücke an der Grenze 24,031 our servir obligatoirement
Bentheim 5021 300m südl. des Bahnhofes 58,009 Sr en à
= 5 ; d’origine aux altitudes de
Elten 5870 An der Grenze 15,104 tous les repères du royaume
Dammerbruch 5873 ey „ 20,953 ne
Belgien Maldingen 8690 5m östl. der Grenze 514,893
Eupen 5881 An der Grenze 301,234
Frankreich Novéant 8661 An der Grenze 181,626
Avricourt 8652 |Am Garten des Zollamts SMa
Bi ) 8705 |An der östlichen Tunnelöffnung| 711,358
gg) m. B, Neben B. 8705 713,225
Altmünsterol 8647 |llm östl. vom Landesgrenzstein] 340,053
Schweiz Kiffis 6533 |Nahe der Grenze, beim Nummer-| 503,315
stein 19,5 ’
Basel 6534 |An der Grenze, beim Nummer-| 258,785
stein 48,1
Württemberg Bretten 6620 Neue Strasse nach Knittlingen,| 182,941
an der Grenze
Alexanderschanze | 6649 Auf dem Kniebis an der Grenze] 968,254
Bayern Kahl 6565 An der Grenze 112,184
Elm Bolzen Tunnel, nördl. Portal 312,840
Obersieman 6950 200 m östl. Obersieman 315,914
Mosbach 8761 An der Grenze 754,100
Henneberg 8765 Ne „ 489,940
Eicha 8767 iW 1 313,933
Hellingen 8770 Mm n 376,264
Neuhaus 8772 HAUT 1 356,637
Probstzella 877: mm 1 361,412
Kögelmütsle 8781 75m von der Brücke an der 495,449
Grenze

 

 

 

 
