at

Tun Ba EEE
ST TEEN TUT T À

 

BEILAGE A. XIII,

ELSASS-LOTHRINGEN.

Bericht über die in Elsass-Lothringen 1903—1905
ausgeführten Schweremessungen.

Seit dem Bericht von 1903 ist die Anzahl der Schwerestationen in Elsass-Lothringen
von 20 auf 44 gestiegen. Die Messungen sind durchweg mit demselben Apparat ausgeführt
worden, der bereits zu den Messungen im Frühjahr 1903 gedient hatte, ein Scunetpmr’sches
von M. Fsouwur in Potsdam verbessertes Wandstativ und ein Satz von 4, gleichfalls von
letzterem umgearbeiteten Pendeln. Das Stativ wurde meist in Kellerräumen in einer Höhe
von etwa 0.9m über dem Erdboden durch Keilschrauben befestigt und das Mitschwingen
mittelst eines Scuumann’schen Pendels durch mikrometrische Messung der Ausschlige des
getriebenen Pendels bestimmt. Uebrigens zeigte sich, dass bei dieser Befestigungsart das
Mitschwingen in allen Fällen ehr klein war und nur 3 bis 7 Einheiten der 7. Dezimale
der Schwingungsdauer erreichte, ein Betrag, der wohl wesentlich dem Stativ selbst zuge-
schrieben werden muss; nur bei zwei Sätzen auf der Referenzstation Strassburg, bei denen
das Stativ der Konizolle halber an einem frei stehenden und absichtlich schwach gebauten
Pfeiler in einer Höhe von 0.9m bez. 1.6m befestigt worden war, ergaben sich in guter
Uebereinstimmung mit den übrigen Messungen Beträge von 14 bez. 35 Einheiten. Auch
" sonst war das Verfahren, insbesondere die Ermittlung des stündlichen Ganges der Rrerter’schen
Stationsuhr durch automatische Uebertragung nach der Sternwarte in Strassburg dasselbe
wie in den früheren Jahren. Die Anzahl der beobachteten Pendel war an der Mehrzahl (15)
der Stationen 8, an 8 Stationen wurden 12 und an einer Station 16 Pendel beobachtet.
Was das Verhalten der Pendel angeht, so ergaben die Anschlussmessungen im Herbst 1903
eine Differenz des mittleren Pendels von -- 23 Einheiten der 7. Dezimale der Schwingungs-
dauer (im Sinne vorher—nachher), die im Frühjahr 1905 von — 8 Einheiten ; bei den
Herbstmessungen 1905 war die Uebereinstimmung eine vollständige. Im ersteren Falle ist
die Berechnung der Schwere doppelt ausgeführt, einmal unter Annahme einer der Zeit
proportionalen Aenderung der Pendel (obere Zahl in der nachfolgenden Tabelle), und
zweitens mit dem Mittelwert (untere Zahl).

Beträchtliche Abweichungen von der normalen Schwere sind auch diesmal nicht
gefunden worden. In der Rheinebene sind die Unterschiede Beob.-Theorie fast durchweg

I 34

 
