 

 

digen Messing-kästen und Röhren zusammengesetzt, von etwa 3 mm. Wandstärke. Das
Gehäuse selbst ist um eine vertikal stellbare Achse drehbar und auf festem Gestelle auf-
gesetzt, wobei zur Ablesung der Drehung des Gehäuses ein in drittel Grade geteilter

i horizontaler Kreis, zur Ablesung der Stellung des mit Spiegel versehenen Wagebalkens

 

 

 

a.) Hin einfaches Schwerevariometer,
und aus seinem fotografisch hergestellten Bilde (Fig. 4) ersichtlich ist, und
b.) Ein doppeltes Schwerevariometer (Fig. 5) aus der Verbindung zweier einfachen

 

 

i aber ein an dem Gehäuse befestigtes Fernrohr dient. Letzteres ist ein gebrochenes Fernrohr

um die Ablesungen auch in kleineren
Räumen zu ermöglichen. Die Länge des
das Fernrohr tragenden Armes wurde
auch aus demselben Grunde auf unge-
fihr 60 Centimeter herabgesetzt, wobei
bei der Spiegelablesung Skalen mit Halb-
millimeter Teilung erforderlich sind.

Bei Beobachtungen im Felde wird
das Instrument in einem transportablen
Häuschen von 22 Meter Grundfläche
aufgestellt, dessen Wände aus wasser-
dichter Leinwand bestehen.

So erhält die Drehwage gehörigen
Schutz um mit derselben bei Ausschluss der
Sonnenstrahlung, also während der Nacht-
stunden, brauchbare Resultate zu erzielen.
Bisher ist es mir nicht gelungen Appa-
rate herzustellen, welche bei Arbeiten
im Freien auch bei Tag benützt werden
könnten, doch hoffe ich auch dies errei-
chen zu können.

Was die weiteren Einzelnheiten der
Konstruktion betrifft, so war für diesel-
ben maassgebend, dass die Instrumente
Reiseinstrumente werden sollten. Dem-
entsprechend musste für eine sichere
Befestigung ihrer einzelnen Teile und
für die bequeme Verpackung des Ganzen
vorgesorgt werden.

Diese Erfordernisse ins Auge fassend
entstanden nun folgende zwei, von mir
und meinen Genossen bei den bisherigen
Beobaehtungen verwendete Instrumente:

dessen Einrichtung aus dem Querschnitte (Fig. 3)

hehe hu lennskabeinsas een

 

 
