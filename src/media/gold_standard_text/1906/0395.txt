Fr

i
N

 

 

 

 

349

Hieraus lassen sich aus den beobachtungen je eines Tages folgende Werte berechnen:

 

 

 

 

 

 

 

 

 

Da | 100, | 10 pale (5 — 5) Le a
u, 2.200 à = 66,88 | + 66,40 4 he
a + 20,85 00,8 0507 + 9,69
“a | 4220,85 == 67.06 + 65,67 | 2 01
Spa Ko) 2 6681 oa > 90
6lkm. za | 8 1 60.8 Po Po
Tixır. + 21,29 | — 66,92 | + 65,95 10,50
Slxır. 9109 | 66,92 65,95 10,50
9/xrr. A002 67.30 | +- 66,41 or
10/xır. + 20,68 — 66,83 os 10,19
a open | 067.00 + 67,88 1080

Mite 8 Spe | 6eeg + 66,08 10

 

 

Die Abweichungen der einzelnen Tageswerte von den Mittelwerten betragen also
nur Ausnahmsweise mehr als eine Einheit von der Ordnung 10-°, wodurch wir ein Maass
der erreichten Genauigkeit erhalten. Beobachtungen im Freien sind unter günstigen Ver-
hältnissen auch nicht mit grösseren Fehlern behaftet, wie dies an einem anderen Beispiele
gezeigt werden wird.

Bei solchen im Freien ausgeführten nr ı kann die störende Sonnenwirkug
bisher nur dadurch ausgeschlossen werden, dass die Beobachtungen zur Nachtzeit ausgeführt
werden. Eine Nacht reicht aber kaum aus um die volle Beobachtungsserie mit fünf Ein-
stellungen auszuführen, umsoweniger, da zur Sicherung der Resultate auch die Wiederholung
wenigstens einiger Einstellungen erwünscht ist. So lange uns nur einfache Apparate wie
N? II zu Gebote standen, mussten wir daher an jeder Station wenigstens zwei Nächte
verweilen, in jeder nur einen Teil der Einstellungen absolvierend.

Diesem Übelstande wird durch das doppelte Schwerevariometer abgeholfen (s. o.),
welches seit dem Jahre 1903 im Gebrauche ist und es ermöglicht in kürzerer Zeit mehr
zu leisten. Die zwei Wagebalken sind in diesem doppelten Instrumente parallel aufgehängt,
aber bezüglich des hängenden Gewichtes entgegengesetzt gerichtet. Gleichzeitig mit der

 
