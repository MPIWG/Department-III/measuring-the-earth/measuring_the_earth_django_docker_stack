en NN nn nl wich an. 8
OT PTT EN HY

sch
Pry iy

haut bn. À
WHEL TER

FO PPT NTI

PTT VAN L'UMF ENT

TUNA

re

rape apie

 

ANNEXE B. IX.

RAPPORT GENERAL
sur les nivellements de précision (Période de 1903 à 1906),

PAR

Mr. Ch. LALLEMAND.

Le présent Rapport fait suite à celui que j’ai eu l’honneur de présenter, en 1903,
à la Conférence de Copenhague !).

En vue de sa rédaction, j'ai adressé, en Mars 1906, au délégué qualifié de chacun
des pays associés, l’extrait correspondant de mon premier Rapport, avec prière de le rectifier
et de le compléter jusqu’à la date du 1er janvier 1906.

Pour la facilité des recherches et des comparaisons, les renseignements ainsi recueillis,
et ceux puisés dans diverses publications officielles, sont résumés ci-après, et classés dans
l’ordre même adopté pour le Rapport de 1903, savoir:

I. Renseignements généraux.
IT. Longueur des lignes nivelées, nombre des repères créés, précision des résultats
obtenus.

II. Repères, instruments et méthodes.

IV. Variations de longueur des mires.

V. Jonctions de réseaux limitrophes, Rattachements de stations marémétriques.
VI. Publications.

I. Renseignements généraux.
WURTEMBERG ï

En 1902, on a exécuté, aller et retour, sur la route de Boblingen à Lustnau, un
nivellement de précision de 26 km de longueur, dont les repères, au nombre de 67, doivent

être nivelés de nouveau tous les 5 ans, afin de mettre en évidence leurs changements pos-
sibles d’altitudes. |

 

1) Voir Comptes-rendus des séances de læ 14ème Conférence générale. Vol. I, (Annexe B. XV. — Page 303).

II 13

 
