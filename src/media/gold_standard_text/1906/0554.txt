   

Se ee

sn

 

sp pe - n

ne

94

4 + . ® > + . »
Ces repéres sont formés de boulons d’acier nickelé, scellés dans des massifs de béton
et disposés par groupes de deux, séparés par un intervalle de quelques métres. Les couples
successifs sont espacés de 750 métres en moyenne, les uns des autres ').

ROUMANIE.

Les altitudes des repéres sont rapportées au niveau moyen de la Mer Noire, déterminé
à l’aide du médimarémètre installé dans le port de Constanza.

SUISSE.

Depuis 1893, les travaux relatifs au nivellement de précision de la Suisse incombent
au service topographique du département militaire fédéral. Ce service à élaboré le projet et
commencé l'exécution d’un nouveau réseau de nivellements de précision qui comprendra
18 polygones fermés et 19 points de rattachement avec les réseaux des pays voisins,

EMPIRE DES INDES.

La longueur maxima des portées, qui, précédemment, pouvait atteindre 120 mètres,
a été réduite à 80 mètres.

Depuis quelques années, beaucoup de repères, particulièrement ceux situés le long
des voies ferrées, ont subi des variations de hauteur. En conséquence, les chemins de fer
ont été abandonnés comme étant impropres à l'exécution de nivellements de précision.

CANADA.

Les nivellements de précision ont débuté, en 1883, sous les auspices du Ministère
des Travaux publics.

Jusqu'en 1908, les altitudes des repéres étaient rapportées au niveau moyen approxi-
matif de l’Océan & Québec, déterminé en 1881—82. D’aprés un rattachement exécuté, en
1888, sur la frontiére du Canada et des États-Unis près du village de Rousse’s Point, entre
les nivellements des deux pays limitrophes, le zéro canadien se trouverait 4 1™,68 (5,52 pieds)
au-dessous du niveau moyen de l’Atlantique dans le port de New-York.

Les altitudes calculées depuis 1903 sont rapportées au niveau moyen de la mer à
New-York, en attendant que les nivellements canadiens soient reliés au marégraphe installé,
il y a une dizaine d'années, par le Ministère de la Marine, à Halifax (Nouvelle Ecosse),
en un point où les courbes de marée ne seront influencées par aucun apport d’eau douce.
Les altitudes, jusqu’ici calculées en pieds, seront alors réduites en mètres.

Le degré de précision que l’on s’est proposé de réaliser est caractérisé par les chiffres
suivants: l'erreur accidentelle moyenne quadratique de la différence de niveau de deux points
éloignés d’un kilomètre doit toujours être inférieure à 3 mm; entre deux déterminations de

 

1) Ces renseignements nous ont aimablement été fournis par M. le Prof. Dr. Hammer, bien que notre distingué
collègue ait, depuis 1905, résigné ses fonctions de délégué du Wurtemberg près de l’Association géodésique internationale.

 

'
|
|
i
;
|
|
|
