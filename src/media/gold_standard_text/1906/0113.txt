See eo

Sr nenn

ji

be

 

107

setzung der Uebereinkunft von 1905 für eine neue zehnjährige Periode zu genehmigen,
eingegangenen Antworten aller betheiligten Staaten, mit Ausnahme von Serbien, anwesend
sind. Alle diese Antworten sind bejahend. Er hat ausserdem einen Brief von Herrn Porro
erhalten, in welchem er mittheilt, dass er von der argentinischen Regierung zur Erklärung
bevollmächtigt sei, dass Argentinien am len Januar 1907 in die internationalen Erdmes-
sung eintrete.

Der Präsident schlägt vor folgenden Antrag zu genehmigen:

»Die 15. Generalkonferenz der internatiowalen Erdmessung erklärt, dass die am 31.
December 1906 endende Uebereinkunft von 1895, ohne etwaige Aenderung der Artikel,
während einer neuen zehnjährigen Periode, anfangend am len Januar 1907, fortgesetzt wird”.

Dieser Antrag wird genehmigt; er wird zur Kenntniss der betheiligten Staaten ge-
bracht werden. |

Der Präsident bemerkt, dass in Anschluss an diese Fortsetzung der Uebereinkunft,
Neuwahlen der Mitglieder des Präsidiums stattfinden müssen; auf Anfrage eines Delegirten
finden die Abstimmungen für den Präsidenten, den Vicepräsidenten und den ständigen
Sekretär gesondert statt. |

Die Abstimmungen geschehen nach Staaten.

Es wird gewählt:
als Präsident Herr General Bassot mit 19 Stimmen,
als Vicepräsident Herr General v. Zachariae mit 16 Stimmen, 3 Stimmen auf
Herrn Darwin,

als ständiger Sekretär Herr v. d. Sande Bakhuyzen mit 18 Stimmen; ein Stimm-

zettel blanco.

Herr General Bassot, Herr General v. Zachariae und Herr v. d. Sande Bakhuyzen,
dankbar für das Zutrauen der Delegirten, nehmen Ihre Ernennung an.

Da die Tagesordnung erschöpft ist, nimmt der Präsident das Wort und hält folgende
Anrede.

»Meine Herren ‚wir sind am Ende unserer Arbeiten, aber ehe ich die Sitzung schliesse,
bitte ich Sie mit mir der ungarischen Regierung unseren wärmsten Dank auszusprechen
für den.herrlichen und freundlichen Empfang, mit welchem sie uns beehrt hat. Diesen Dank
bringen wir in erster Linie dem erhabenen Souverän, ferner dem Vorsitzenden des Minister-
raths, und besonders dem Unterrichtsminister, der unsere Sitzungen eröffnet hat’”.

Die Delegirten erheben sich von ihren Sitzen. Lebhafter Beifall.

»Wir haben auch dem Herrn Präsidenten der Akademie der Wissenschaften, der die
schönen Räume in diesem Palaste zu unserer Verfügung gestellt hat unseren Dank zu bringen,
Ich bitte die Delegirten sich für dieses Dankesvotum von ihren Sitzen zu erheben’’. Leb-
hafter Beifall.

»Zum Schlusse bleibt mir noch die sehr angenehme Pflicht, unseren verehrten Herrn
Collegen Bodola~und Frau Bodola herzlich zu danken für die überaus liebenswürdige Weise
in der sie uns empfangen haben und uns fortwährend mit. Freundlichkeit entgegen ge-
kommen sind. Wir bringen auch unseren Dank den Herren, welche Herr Bodola zur Seite

 
