£
iE
i

rinnen

 

SIEBENTE SITZUNG

Herr General Bassot Präsident.
Die Sitzung wird 9 Uhr 40 Min. eröffnet.
Sind anwesend:

die Delegirten, Porro, Gillis, Zachariae, Hecker, Borrass, Albrecht, Börsch, Helmert,
Foerster, v. Bertrab, Schorr, Becker, Schmidt, Haid, Bassot, Poincare, Darboux, Bouquet de
la Grye, Bourgeois, Lallemand, Hanusse, Darwin, Crema, Guarduccit, Kimura, Tasaka, Valle,
Anguiano, Bakhuyzen, Heuvelink, Muller, Schidtz, Kalmar, Tinter, Weiss, Lehrl, Bratiano,
Cantea, Artamonoff, Backlund, Rosén, Gautier, Mier, Galarza, Bodola, Eötvös, Tittmann,
Hay ford ;

die Geladenen, Driencourt, Shinjo, Andres, Guillaume, Harkanyi, Dobrovies, Dechy,
Antalffy.

Der Präsident ertheilt dem Sekretär das Wort für die Verlesung des Protokolles.
Das Protokoll der 6en Sitzung wird gelesen und genehmigt.

Der Präsident theilt den Delegirten mit, dass der Staatssekretär eine Anzahl Eintritts-
karten für Urania zu ihrer Verfügung gestellt hat, und der Sekretär giebt im Namen des
Herrn Bodola einige Nachweise die von dem Herrn Industrieminister zu ihrer Verfügung
gestellten Eisenbahnkarten betreffend.

Der Präsident bringt in Discussion den Antrag der Association der Akademien und
erinnert daran, dass das Bureau zwei Delegirte, Herr Darwin und Herr Lallemand, eingeladen
hat über die zwei vom Geologenkongress gestellten Fragen Berichte abzustatten. Die Be-
richte sind vertheilt und der Präsident ertheilt Herrn Darwin das Wort um die von ihm an
das Ende des Berichts gestellten Anträge vorzulesen.

 

 
