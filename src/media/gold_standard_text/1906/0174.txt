 

 
  

NN ee .
= — - er = Sn TS RDS TE TES SL SES EE LE EN EE 4

154

Afin @obtenir un grand nombre d’elöments pour les recherches sur la déviation de
la verticale, on a rattaché, en 1904, l'île de Gorgona à la station du réseau géodésique M. del
Telegrafo située sur le promontoire de Camogli où, en 1902, par les soins de M. le Com-
mandant Carronica '), furent determinés, par des observations astronomiques, la latitude et
un azimut. Après avoir calculé définitivement le triangle M. del Telegrafo- Castellana-Gorgona,
on a déterminé la déviation locale à M, del Telegrafo et on a obtenu:

2. 10

2.2300

= 8 ,854

D. 20 40
x #

Ainsi que nous l'avons indiqué ci-dessus, on à fait, pendant les années 1904-1905, ja
révision des mesures angulaires relatives à la triangulation rattachée à la Base de Foggia,
à partir de la ligne brisée Pisarello-Semprevisa- Cornacchia- Majella- Torre Mucchia qui dans
la nouvelle réorganisation des réseaux italiens indique les limites entre la partie méridionale
et les parties centrale et septentrionale de l'Italie. Tout en faisant ces travaux on a pris
soin de rendre encore plus simple la figure du réseau en éliminant quelques stations et

par conséquent en construisant des triangles plus grands. Le nouveau réseau, dont les di-

mensions linéaires seront calculées avec la Base de Foggia, contient 36 triangles: pour la
ensation du réseau il faudra considérer seulement les conditions imposées par sa figure,

comp
des contradictions linéaires relatives aux réseaux limitrophes sera obtenue

car l'élimination
à part, c'est à dire après la compensation des mesures angulaires.

Au lieu de poursuivre, dans le courant de l’année 1906, le travail dans les provinces
voisines à la Capitanata, on à cru utile d’aller en Calabre pour compléter un projet de
travaux géodésiques, qui comprend le nivellement de précision, qu'on exécute à présent, et
des déterminations relatives de la pesanteur au point de vue de la sismologie.

NIVELLEMENT DE PRECISION.

On a exécuté, en 1903, le nivellement de la ville de Venise afin d’examiner le mouve-
ment du sol aprés la ruine du clocher de 8. Marco, et dans la même année on a nivellé
des lignes, d’une longueur totale de 140 km., dans la province de Ravenna. Ce dernier travail
a aussi servi pour tächer de découvrir le mouvement des terrains marécageux.

En 1904 on a relié le nivellement français au nivellement italien au Petit St. Bernard
à travers la vallée d’Aosta (sur une ligne de 157 km.), et le nivellement suisse au nivelle-

ment italien au Grand St. Bernard en partant de la ville d’Aosta et en parcourant 32 km.

sur la route de St. Rhémy.

A EN ER er

etiche eseguite negli anni 1901-1902. Genova 1904.

1) Operazioni Astronomico-Geod

 

ee

2 a La anna ene

DT

reread evaded eee
Ah ididedntorimBbettedlshdrreninennd

 
