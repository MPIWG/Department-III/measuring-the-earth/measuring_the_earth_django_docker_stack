A CARL hn A ee aie
Res A

NEU deen aes bes
POPE TOP RMIT OBEY ToT WEF pps tenes

as à paws
STI oe TO

oa

U U

ea ae 2
ae ve Ros 2a
2
Ce
ox dy 2

Diesen Formeln entsprechend können auch R und 2 für alle Wertgattungen berechnet
werden. In der die Arader Resultate enthaltenden Tabelle I sind in den letzten Columnen
nur die topografischen Werte von R und A mitgeteilt. Mit Hülfe dieser Grössen, welche
sich zur grafischen Darstellung eignen, lässt sich ein gut übersichtliches Bild der Krüm-
mungsverhältnisse herstellen. In der beiligenden Karte (Fig. 10, S. 370) sind diese Grössen für
jede Beobachtungsstation durch gerade Linienstücke dargestellt, deren Länge mit R propor-
tional ist und deren Richtung dem Werte von % entspricht. Die augenfällige Regelmässigkeit
in der Richtung dieser Linienstücke erlaubte auch eine Reihe von Hauptkrümmungslinien in
die Karte zu zeichnen. Zuerst jene, welche die Linienstücke R tangieren, also in der Rich-
tung des grösseren Hauptkrümmungsradius verlaufen, dann die darauf normalen.
| Auf diese Weise geschieht auch eine gewisse grafische Ausgleichung der Resultate
bezüglich der Werte von A. Eine solche grafische Ausgleichung kann für R auch durch
Linien geschehen, welche gleichen Werten dieser Grösse entsprechen. Diesem Zwecke ent-
sprechend entstand die Karte (Fig. 11, S. 372), welche die Werte von R im untersuchten
Gebiete durch Linien darstellt, deren Wertabstand 10 Einheiten der 10° Ordnung beträgt.

9, BesriMMunc DER Form EINER NIVEAUFLÄCHE.

Die Form der Niveaufliche ist durch die Grössen:

Sb eu Su)
a oon

 

bestimmt, zwischen welchen für die Schwerkraft folgende Beziehung besteht

au ou, ZU

 

a 902
Oke oy” 3 92° .
wo w die Winkelgeschwindigkeit der Erddrehung bedeutet.
Ff au QU
Die Kenntniss der durch die Drehwage bestimmbaren Grössen a — | und
OR,

U . : : 3 | :
ae st daher fiir sich allein noch unzureichend um diese Form zu ermitteln.
C
Wohl sind physikalische Methoden zu ersinnen, um diesem Mangel abzuhelfen. So

2

 

kann die JoLzy’sche Methode zur Bestimmung von dienen und es wäre möglich durch

Oz

Vergleichung der Schwingungen langer und kurzer Pendel zur Kenntniss derselben Grössen
zu gelangen. Auch könnte durch Beobachtungen an Pendeln, deren Trägheitsachsen zur

 

 

3
a
x
ig
+
i
3

TESTER >

 
