 

|

 

CRT

ALL LR

FU BAD LOU LE pn

|

259
soirement la marche) et en adoptant les positions des étoiles telles qu’elles sont données
dans le catalogue de M. Bosserr. Il y aura lieu de faire subir à toutes les droites un léger
déplacement parallèlement à Oz lorsqu'on connaîtra les marches moyennes réelles des
chronomètres dans le cours des séries qui leur correspondent. Pour les obtenir, nous com-
mencerons par tracer les courbes de marche des pendules durant la période des observations
à l'aide des états fournis par les graphiques provisoires et des comparaisons des chrono-
mètres avec les pendules. Nous basant ensuite sur ce que les marches des pendules, qui
étaient dans un local fermé et à l’abri des variations brusques de température et des chocs,
ont dû être plus régulières que celles des chronomètres qui, pendant les observations, étaient
portés à la main et exposés aux variations de la température extérieure, nous admettrons
la continuité des courbes de marche des pendules et, à l’aide des comparaisons, nous dé-
duirons celles des chronométres dans le cours des séries.

20 CoRRECTIONS DES POSITIONS D'ÉTOILES.

D’autre part, la comparaison des graphiques montrera que certaines étoiles s’écartent
toujours dans le même sens du cercle enveloppe des graphiques bruts. Cela ne peut être
attribué qu’à une erreur sur la position de l'étoile. Pour celles d’entre elles qui ont été
observées un nombre de fois suffisant — beaucoup ont été observées de 20 à 25 fois —
nous pourrons tirer de nos observations le déplacement moyen à faire Subir aux droites de
hauteur qui leur correspondent. |

Après ces corrections qui amélioreront notablement les graphiques sans nul doute,
nous pourrons relever sur chacun d’eux les trois éléments dont nous avons parlé plus haut
et déterminer le degré de précision avec lequel ils sont obtenus.

CALCUL FINAL DE LA LONGITUDE.

Il sera facile ensuite de calculer, pour l’heure moyenne des comparaisons Paris-Brest
de chaque soirée, les deux états du chronométre de chaque station ayant servi à la com-
paraison: les différences montreront comment varie la différence des équations personnelles
des observateurs conjugués. On passera de là au calcul des différences de longitude: les
valeurs obtenues avant et après permutation des observateurs donneront de même les varia-
tions de l'équation personnelle moyenne des deux groupes d’observateurs puisqu'il est établi
d'ores et déjà qu’il n’y a ni équation personnelle, ni retard de transmission appréciable
dans les comparaisons téléphoniques entre les deux stations. Il sera donc possible de calculer
l’approximation du résultat final d’une manière plus sûre que dans toutes les opérations
similaires effectuées jusqu'ici.

LamirTupess.

Les diverses valeurs obtenues en chaque point pour la latitude montreront avec
q p p
quelle précision cet élément est déterminé au moyen de l’astrolabe à prisme.

|
4
À
BI
i
|
i
|
|
4
i
4

 
