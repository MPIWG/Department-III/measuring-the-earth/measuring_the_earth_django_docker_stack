wein |

À rm

ae

rent TE

HTT TTT BRITA ILLIA OWT CTV TT

 

277

La valeur moyenne est:
a = 6 378 150 m.

Cette valeur est diminuée d’environ 10 m., quand on adopte pour l’aplatissement la
valeur 1:298,3, qu’on a déduite des mesures de la pesanteur.

Pour répondre à une demande qu’on nous avait adressée, nous avons recommandé,
pour la réduction de la triangulation de l'Égypte, l’ellipsoïde de révolution des dimensions
suivantes. En tenant compte des résultats des calculs récents du Coast and Geodetie Survey
des États-Unis de l'Amérique, on a adopté pour a une valeur qui est un peu supérieure

à la précédente.
a — 6 378 200 m., aplatissement 1 : 298.8.

3.

RAPPORT SPÉCIAL SUR LE SERVICE INTERNATIONAL DES LATITUDES AU
NORD DE L’EQUATEUR.

Le service international des latitudes sous le paralléle de 39° 8’ a de nouveau bien

fonctionné pendant l’année 1906.
Le nombre total des couples d'étoiles observées pendant cette année a été:

à Mizousawa . . 1685
à Tchardjoui . . 1876
a Carloforte . . 220
à Gaithersbourg . 1954

à Cincinnati. . 1209
a Ukiah. . . . 2880

Les observateurs pendant l’année 1906 étaient:

à Mizousawa : M. le Prof. Dr. H. Kimura: et M. le Dr. T. Naxano,

a Tehardjoui: M. le lieutenant-colonel Dawypow.

a Carloforte: Me le Dr,  Vonrs et Mo le Dr. G. Sinva,

a Gaithersbourg: M. le Dr. Frank E. Ross et M. Warrer N. Ross.

a Cincinnati: M. le Prof. Dr. J. G. Porter et M. le Dr. Ds LisLe StewArr.
a Ukiah: M. le Dr. S. D. Towntuny.

Ainsi que dans les années précédentes, les réductions ordinaires des observations ont
été faites immédiatement aprés la réception des cahiers originaux des observations par
M. le Prof. Wanacu, attaché à l’Institut géodésique, avec l’aide des calculateurs M. W. Hess,
M. F. Jasrowsxi, M. A. Wısanowsky et M. V. VocLkr.

Au commencement de l’annee 1906 on a remanie le programme des observations en
remplaçant les 24 couples d’étoiles de réfraction et 6 des anciens couples de latitude par
30 nouveaux couples de latitude. Ce remaniement était devenu nécessaire surtout par les
variations que, depuis l’année 1900, les déclinaisons des étoiles ont subies par la précession.

 
