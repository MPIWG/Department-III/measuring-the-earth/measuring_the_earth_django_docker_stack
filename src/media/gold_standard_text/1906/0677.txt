Te genen,

Lai RE REFERAT a. hd AAN 6 1
NET PTY PRINT TORR SFMT TEE Wo;

im

PoE

vier

IE
if

 

XI. MESSUNGEN DURCH DIE SCHWEIZERISCHE GRADMESSUNGS-KOMMISSION.

Über die in der Schweiz in den letzten Jahren ausgeführten Schweremessungen
liegen uns nachstehende Publikationen und Mitteilungen vor:

1) Relative Bestimmungen der Schwerkraft im Nikolaitale. Inaugural- Dissertation,
vorgelegt von Th. Niethammer, Basel 1904;

2) Procès-verbaux de la Commission Géodésique Suisse, 47— 52" séance, 1903—1900 ;

3) zwei handschriftliche Mitteilungen des Herrn Professors Dr. A. RIGGENBACH
vom 21. Mai 1906 und 8. Februar 1907.

Von diesen zur Herstellung unsrer Tabelle benutzten Grundlagen enthält die unter
1) genannte Publikation eine detaillierte Darstellung eines Teiles der Messungen aus dem
Jahre 1902, während in den Proces-verbaux nur vorläufige Ergebnisse und einige darauf
bezügliche sachliche Erläuterungen der Messungen aus den Jahren 1902, 1903, 1904 und 1905
gegeben werden. Eine einheitliche Bearbeitung sämmtlicher, seit dem Jahre 1900 durch die
schweizerische Gradmessungs-Kommission ausgeführten Schweremessungen ist, wie Herr
Prof. Rıceensach in dem zuletzt genannten Schreiben mitteilt, bereits in Angriff genommen,

Alle hier infrage kommenden Arbeiten wurden von dem Ingenieur Herrn Dr. Nıer-
HAMMER mit dem SrerNeck-Scuyerper’schen Apparat !) der Kommission ausgeführt. Zu dem
Apparat gehören 4 Pendel (N°. 30, 31, 32 mit Achatschneiden, N°. 64 mit Stahlschneide) ;
am Kopfe des Pendelstativs ist seit 1902 ein zweites Lager für ein schweres Wipp-Pendel
angebracht, das zur Bestimmung des Mitschwingens nach der Zweipendel-Methode dient.
Als Koinzidenzuhr für die Pendelbeobachtungen wurde ein Narpın’scher SZ-Chronometer
(N°. 34) verwendet; für die Zeit- und Gangbestimmung stand ausserdem eine mit Nickel-
stahlpendel, Rad- und Hebelkontakt versehene RırrLer’sche Sekundenpendeluhr zur Ver-
fügung. Diese sollte eigentlich direkt als Koinzidenzuhr zur Bestimmung der Schwingungs-
dauer der Pendel verwendet werden; allein der eigens für diesen Zweck angebrachte RıerLer'-
sche Hebelkontakt beeinflusste den Gang der Uhr so ungünstig, dass er ausgeschaltet werden
musste. Der Gang für Naroın wurde durch automatische Vergleichung mit Rrerrer mittels
eines Hrpp’schen Chronographen bestimmt.

Die Messungen von 1902 begannen mit der Bestimmung Potsdam — Basel ;
daran schlossen sich die Messungen im Nikolaitale und in Zürich. In Potsdam wurden
zunächst die Luftdichtekoeffizienten der Pendel ermittelt, wofür sich folgende Werte fanden:

1) Beschreibung und Abbildung im 7. Band des schweizerischen Dreiscknetzes.

 
