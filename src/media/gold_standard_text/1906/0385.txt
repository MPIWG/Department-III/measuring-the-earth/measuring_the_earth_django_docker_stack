339

dessen z-Achse in diesem Punkte vertikal abwärts gerichtet ist. Bezeichnen wir dann mit
K das Trägheitsmoment des Gehänges, mit & den Winkel zwischen der Achse des Stabes
und der x-Achse des Koordinatensystems, so ist das Drehungsmoment F für das Gehänge
erster Form:

U 2 sin 2a ol
= rag 2 ei Fer) i
F > +x K 5 Fin 0 & )

Fir das Gehiinge der zweiten Form erhalten wir dagegen,
wenn m die tiefer aufgehängte Masse, h ihren vertikalen Abstand == 7
vom Stabe und | ihren Drehungsarm bedeutet:

 

 

2) 2 1 2
fa ee sin 2 « IM ee à
Oy Ox 2 oxdy
"U aU)
— i = | u
sxaz mb! ne COS æ ) i

 

In der Gleichgewichtslage ist nun:

FETE

Sr

wo $ den Torsionswinkel, r die Torsionskonstante des Aufhänge-
drahtes bedeutet und daher für das Gehänge erster Form:

 

|

 

 

 

LK 707 =) Ko? U 5 it

ie CODE Zinn > a Da 0

: On ie OG ek T Day a )
Figur 2.

für das Gehänge zweiter Form:
1 DU 4 à KR au ml 0 0 |
= = 2 — —— 608 28 — —— —__—
I 9 a jaja en a & - Sy, net
mhl 97 U
+ Er sa 4.)

 

Indem man das ganze Instrument durch Drehung um eine vertikale Achse in ver-
schiedene Lagen bringt, verändert sich diese Drillung und es kann ihre Veränderung bei
beliebiger Veränderung des Azimuths « beobachtet werden. So kann man, wie ich weiter
unten ausfiihrlicher zeigen werde, durch eine entsprechende Anzahl von Beobachtungen in
verschiedenen Azimuthen die Werte folgender vier Grössen bestimmen:

 

AU FU ee a PU
dxoz’ dyoz \dy? 9x2)’ dxdy

Und zwar können mit einem Gehänge zweiter Form alle vier, mit einem solchen
der ersten Form aber nur die beiden letzten ermittelt werden. Die Bedeutung dieser Grössen

   
