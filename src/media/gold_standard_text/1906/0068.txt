 

 

 

62

sondern als die Erneuerung und Befestigung eines seit längerer Zeit bestehenden geistigen
Bandes. Einer unserer verdienten Kollegen hat als Vertreter Ungarns schon an mehreren
Versammlungen des internationalen Geodätenverbandes theilgenommen und die Herren werden
im -Verlaufe ihrer jetzigen Berathungen in der Lage sein, die Resultate zu beurtheilen, welche
unsere Gelehrten in Betreff der Lösung der in den Wirkungskreis der Erdmessung gehörenden

Probleme erreicht haben.

| Als ein weiteres, uns sehr angenehmes und werthvolles Moment des persönlichen
Bandes möchte ich noch die Thatsache anführen, dass es uns vergönnt ist, in unseren Reihen
zwei berühmte Männer auf dem Gebiete der Geodäsie und der mathemathischen Wissenschaft
überhaupt zu begrüssen, die beide unserer Akademie als auswärtige Mitglieder angehören
und die beide Mitglieder der um die Erdballforschungen so ruhmreich verdienten französischen
Académie des Sciences sind, der eine als jetziger Präsident, der andere als ständiger
Sekretiir: die Herren Henri Potnoart und Juan Gaston Darsoux. Unsere Akademie glaubte
dieses Band noch enger zu knüpfen, indem sie den zum Andenken des alten berühmten
Mathematikers Wourgane BöLyay gestifteten Preis zuerst Herrn Poincaré zuerkannte.

Ich begrüsse Sie, meine Herren, und wünsche von Herzen, dass Sie sich unter
unserem Obdach wohl fühlen, dass Ihre Thätigkeit auf ungarischem Boden, innerhalb der
Mauern der ungarischen Akademie der grossen, allen gebildeten Nationen gemeinsamen und
theuren Sache des wissenschaftlichen Fortschritts zu vielem Segen gereichen möge.

Herr General Bassor, Präsident der Internationalen Erdmessung antwortet mit fol-
gender Anrede: |

Herr Minister,

Die internationale geodätische Vereinigung ist der ungarischen Regierung sehr er-
kenntlich dafür, dass dieselbe sie eingeladen hat, die XV. allgemeine Konferenz in Budapest
abzuhalten. Die Worte des Willkommens, welche Sie soeben an uns gerichtet, vermehren unsere
Dankbarkeit. Wir sind sehr gerührt von Ihrer huldvollen Gastfreundlichkeit, doch sind wir
von derselben nicht überrascht, da wir die hochherzigen Gefühle des ungarischen Volkes,
seine Begeisterung für die liberalen Ideen, seine Pietät für Alles, was die Wissenschaft
angeht, kennen. Die glänzende Stadt, in welcher Sie uns empfangen, hat unvergleichliche
Reize. Vor Allem der majestätische Strom, diese reiche und abwechslungsreiche Natur, diese
schönen Monumente, die Urbanität ihrer Bewohner, alles vereinigt sich, um aus dieser Stadt
einen unvergleichlichen Aufenthaltsort zu machen und um eine unvergessliche Erinnerung
in uns zurückzulassen.

Herr Präsident der Akademie,

Als Sie uns in diesem Palaste empfingen, haben Sie die Einheit der Wissenschaft
bei allen Völkern bekräftigt. In der That, die Wissenschaft hat kein Vaterland, sie ist eins,

 

sil aise ara

a ot A AR a a A thai dens

 
