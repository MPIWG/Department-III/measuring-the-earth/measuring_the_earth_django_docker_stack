FE
|

 

 

307

Da aber die Gleichungen die Gesammtwirkungen bis zur Entfernung von 1000 Metern
ausdrücken, also solche Werte ergeben, welche, nach den eingeführten Benennungen, der
Terrainwirkung -+ der kartografischen Wirkung gleich sind, so erhalten wir die Terrain-
wirkung allein im allgemeinen nur nach Abzug der kartografischen Werte bis zu 1000 Metern.

Bei dieser Art der Berechnung bleiben manche störende Einflüsse, wie solche von
Gräben, Dämmen, Strassenkörper u.s. w. unberücksichtigt. Wenn solche durch passende
Wahl der Beobachtungsstation nicht ganz vermieden werden können, müssen sie für sich
berechnet werden.

3. KARTOGRAPHISCHE WIRKUNG.

Die Berechnung jener Wirkungen, welche den in der kartografischen Zeichnung
darstellbaren Bodenerhebungen und Senkungen entsprechen, kann viel einfacher ausgeführt
werden. Durch Vernachlässigung von h neben & und p vereinfachen sich die Gleichungen 9.),
wie folgt:

Ut el dpcos2ada ,

 

 

 

 

Se NO 0 a
dy? ax Ga p?
U 3 „desin2ada
ree.
ee 10.)
au 3 „dpcos2de
Be |
5 :
eu eee dp sin a da
oy OZ 2 p°

Die praktische Berechnung kann weiter auch dadurch besonders vereinfacht werden,
dass wir sie nach Höhenschichten ausführen, wobei nur Werte von £ ın Betracht kommen,
welche ganze Vielfache der Schichtenhöhe sind. Das sich um den Beobachtungsort weit er-
streckende Gebiet wird ähnlich wie bei der Terrainwirkungsberechnung in radiale Segmente
geteilt, aber in grösserer Zahl als dort. Bei unseren Berechnungen geschah diese Teilung
in 36 Segmente. Eine weitere Einteilung des Gebietes durch Kreise ist aber hier nicht
nötig, da die Werte von p als Entfernungen der Schichtenlinien abgemessen sich zur Be-
rechnung besser eignen.

Die Genauigkeit eines solchen Verfahrens richtet sich nach der Genauigkeit der Karte,
welche ihm zu Grunde liest. In kleineren Entfernungen sind Karten von grossem Maassstabe
und dichten Schichtenlinien erforderlich, während für grössere Entfernungen auch weniger
detaillierte ausreichen. So wurde z.B. für die Beobachtungen in der Gegend von Arad, die
weiter unten ausführlicher besprochen werden sollen, bis zur Entfernung von 12 Kilo-

metern östlich vom Rande des Gebirges, Karten im Maassstabe benützt, von dort

i
25000
: is i D
bis zur Entfernung von 30 Kilometern aber solche im Maassstabe von +55, und noch

900000
I 46

weiter eine Landkarte (om) Die Abstände der Schichtenlinien waren hierbei 40 m.,

 
