| 137

und der südliche von Rosendaël an, der mit dem ersten 3 Stationen (2° 12’ in Breite) ge-
meinsam hat: Aa=+ 145m + 382m.

In der nachstehenden Tabelle sind die zu diesen drei Werten von a gehörigen
Werte von & zusammengestellt, wozu viertens noch die bereits oben, S. 134 erwähnten,
sich aus dem Bogen Rosendaél-Nemours mit

Aa=-+ 293 m + 383m

ergebenden Lotabweichungen in Breite hinzugefügt sind.

 

Lotabweichungen in Breite für den westeuropäisch-afrikanischen Meridianstreifen.
(Bessersche Abplattung).

 

 

 

 

 

 

 

 

 

 

 

. | Geogr. Aa gegen BESSEL’s a
Geogr. re geg S
Station B nt Tomte
relte | gegen
Ferro + 538 m + 788m | + 145m | + 293 m
FH SAN AOL 2. 2. 60 49,6 | 16 49 — 4,0 — 9,6 — —
i alae te 2.0... 60. 45,0 16 53 — 6,1 — 47 — —
if Ben te 58 33,1 19.9 + 03 + 14 _ —
Le Comhythe te. 2, be Alia ine 0 oo + 82 — —
| Great’ Stirling.......... 57 O08 |b lee | oe | Be a a
N Kelle Baw.. ......... 7 56.124,91, 120593 — 3,7 — 9,9 = oe
| Gélton Hull 2 0 55 57,4 | 0400 + 8,5 + 43 == ==
{ Durham 0. Ge 54 46,1 | 16 5 — 0,9 09 a a
i Burleigh Moor......... Bd 34,3) 2G Ba a oh ui be
£ Cliton-beicon 4 158 975 0 lo 27, LE A — —
Arbuey EU. ye. 52 13,4 16 27 OÙ = 28 — —
Grecnwicue 0... 51 28,6 17 40 a — 2,4 — —
Nieuport te ok. 51 08 20 25 = Oe 0,4: —,, —,,
Rosendaël-lès-Dunkerque | 51 2,7 20 4 == 4059 — 0,9 — 3,4 3,0
WiNOUS eines ao. 49 49,9 20 25 = 5 + 0,4 — 17 — 1,4
Pants (Pantheon)... 48 50,8 2071 == 0,0 — 0,3 — 2,0 — 1,8
= Cheese 48: 0:5... 20 ky, sn 1252 — + 0,4 = 06
i= Balleuy le Vil 7. Al 2 20 26 ae ay — + 14 D
A Ampheuiller #0. 46 13,7 20 20 a 1080 — + 4,9 a
la Buy de Dome. ....... 45 46,5 | 20 38 + 1,0 — = 5,0 == BG
5 oder an... 44 21,4 | 90 14 + 1,7 — 00 06
= Carcassonne 2077 an. 48 13,3 20 al + 0,7 — — 0,0 — 0,3
Rivesaltes... ..... 42 45,9 20 32 — 0,7 — — 1,3 — 1,6
- NMontolan sere ae os ee 4] 385 ıı 1026 + 3,6 — + 32 + 2,8
Beada ee a. Ne — 02 — — 0,6 — 1,0
Javalon re Les 40. 15,8 | 16.5 — 0,2 — — 0,2 0,7
Deéslentor Loue ls 40° 5,0 102 — 45 — — A6 sl
@hunehnllay sc... oe ee 38 95,2% | 1056 + 2,2 — + 24 di AS
L Mola de Formentera....| 88 39,9 | 19 12 a 1e me — 0,9 — Lo
- Meilen. uen 827 152 ao, in 0,0 — + 4,0 1939
Roldan ri. 36 56,6 15 45 — 6,0 — — 5,3 ol
Conuros ed de 36 44,4 14 15 -— 19,6 — — 11,9 197
Bauen. ge. 35 39,6 | 16 49 + 65 = et 6
Newoussı 2.0.8, a JD Bus 15 49 Se (Rt — + 85 + 7,6
Bouzanea... 2.1.2... 36 48,0 20 42 + 9,9 — -+ 3,5 en
Algen (Volol)......... 36 45,1 20 42 — 91 — — 8,4 —
Gueltes Stel jeu. 20 ©, 35.1158 2041 7 — 10 — + 0,0 —
Lagon 26. 33480 | 90 38>) oi = 214 =

 

Pest

PEER Um a ee

Me ee

 
