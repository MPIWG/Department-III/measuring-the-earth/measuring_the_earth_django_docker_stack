EIERN a weht ka à

LT |

Lu à. man ne

I ART

fas iri

[UTERO UAT om NF

à

 

139

Infolge der neuen Bestimmung des Längenunterschiedes Potsdam--Greenwich und
durch die endgültige Feststellung des Längenunterschiedes Paris— Greenwich erleiden die
3 westlichen Stationen Feaghmain, Haverfordwest und Greenwich eine Annäherung an den
Kontinent von etwa 1,5 in Länge; ferner ist von den Engländern am westlichen Bogen-
ende eine neue Station Killorglin, 2m 13s östlich von Feaghmain, angelegt, wo weit weniger
lokale Lotabweichung als in Feaghmain zu erwarten ist. Nimmt man auf diese beiden
Umstände, zunächst nur nach einer Überschlagsrechnung, Rücksicht, so wird das Ergebnis
etwa

Aa=+ 660m + 105m.

Eine neue Bearbeitung des ganzen Längenbogens in 52° Breite, die diese Ände-
rungen scharf berücksichtigt, ist übrigens im Gange.

A. Der russische Teil der Längengradmessung in 47!/,° Breite, der von diesem
grossen europäischen Längenbogen bis jetzt allein fertig vorliegt, gibt bei einer Amplitude
von 19° 12’ in Länge und für 6 astronomische Stationen

Aa = — 47 m + 650 m.

Bei der Kürze des Bogens ist dieses Aa sehr unsicher, so dass sein Wert auf das
Ergebnis einer Zusammenfassung mit den Aa aus den ausgedehnteren Gradmessungen kaum

Einfluss haben wird.

Russische Längengradmessung in 47'/,° Breite.

 

 

 

 

Rüschinew.. a... 28 50,3 HO
Nikolajew...... 000 3] 58,5 — 2,7
Nlexandrowsk,.... 35 11,1 — 1,9
Rostow.an Don... 2... 39 42,9 — 9,1
Darepia...... ce 44 33,4 + 5,8
Astrachan.. ee 48 2,2 — 2,4

 

Aus diesen 4 Bogen wurde der oben, $. 134, aufgeführte Wert der halben grossen Achse

— 6378150 m
im Mittel abgeleitet.

DEUTSCHLAND.

Durch die im Jahre 1903 ausgeführte Bestimmung der Längendifferenz zwischen
