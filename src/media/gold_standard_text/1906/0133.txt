FT‘

ifs hie Re Adee hei ehe ak ea i8

il

 

FE

 

SOUTH AFRICA.

I. TRANSVAAL ann ORANGE RIVER COLONY,
by Sir Davin Giz, K.C.B.

With a plate.

The field operations of the Geodetic survey of the Transvaal and Orange River Colony
have been completed, and the reduction of the observations is well advanced. A report by
Colonel Morris is attached, giving a summary of the probable error of the triangulation.

The astronomical observations are not yet definitely reduced, as we are only now
completing, at the Cape Observatory, the observations of the stars employed in the deter-
mination of latitude.

The following report on the measurement of base lines is founded upon data furnished
by Mr. ALExANDER Simms, who was in charge of the base line measurement.

A map is attached shewing the triangulation.

BASE LINES.

Five base-lines have been measured with JÄDERIN wires of »invar’’ nickel-steel.

These wires were compared from time to time during measurement of the Base-Line
proper with short »Standard bases” measured on sites near the middle of each Base-Line
proper. The conditions aimed at in selecting the sites of the standard bases were 1° an
accessable rock-bottom for the foundations of the terminal piers and 2° proximity to a water-
supply, as the main camp was always established in the neighbourhood of the short
standard base.

The lengths of the short standard base-lines were determined by frequent measurement
with the Troughton and Simms base-apparatus, which is fully described in Vol. I of the
Geodetic Survey of South Africa. Details of the comparison of this apparatus with the Cape
Standard 10-foot bar, and of the latter with the metre at the International Bureau of weights
and measures are given in the volume in question. It seems unnecessary in the present
report to describe in detail the methods of measurement, because full details of these operations
will be given in a volume to be published in the course of 1907, together with a complete
account of the Geodetic Survey of the Transvaal and Orange River Colony, made under
the Direction of Colonel Morris.

 
