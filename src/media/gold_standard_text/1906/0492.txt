 

32

8
beitung des Beobachtungsmaterials vom Beginn der Beobachtungen bis zum
4. Januar 1902 ergeben hatten. Für die 30 neu hinzugetretenen Sternpaare
aber sind sie unmittelbar an das arithmetische Mittel der Deklinationen
ds a d, À : STE
er les neuen Sternprogramms angebracht worden.

Die Verzeichnisse der scheinbaren Deklinationen vom 1. November bis
31. Dezember 1905, sowie vom 1. Januar bis 6. Dezember 1906, für die
Zeiten der Greenwicher Kulmination interpoliert, wurden autographiert und
unter dem 25. Mai bezw. 9. Dezember 1905 den Stationen zugesandt, um den
Beobachtern die Möglichkeit zu bieten, sich über den Ausfall ihrer Beob-
achtungen durch Reduktion derselben selbst Rechenschaft geben zu Können.

Wie schon im vorjährigen Bericht angekündigt, ist die Bearbeitung
des II. Bandes der „Resultate des Internationalen Breitendienstes“, welcher
den dreijährigen Zeitraum vom d. Januar 1902 bis 4. Januar 1905 umfabt,
im Frühjahr 1905 von mir und Herrn B. Wanach in Angriff genommen worden.
Dieselbe ist gegenwärtig soweit gediehen, daß das Erscheinen des Il. Bandes
Mitte dieses Jahres in sicherer Aussicht steht.

Um aber schon vor dem Erscheinen dieses Bandes einen vorläufigen
Aufschluß über den weiteren Verlauf der Polhöhenbewegung zu erhalten, habe
ich auf Grund der abgeleiteten Verbesserungen der angenommenen mittleren
Deklinationen der Sternpaare für das Zeitintervall von 1904.0—1905.0 eine
provisorische Ableitung der Bahn des Poles ausgeführt, deren Resultate in
Nr. 4017 der Astronomischen Nachrichten veröffentlicht worden sind. Hier-
durch ist es ermöglicht, die im Verlauf des Jahres 1904 ausgeführten astro-
nomischen' Beobachtungen und astronomisch-geographischen Ortsbestimmungen
schon jetzt auf eine mittlere Lage des Poles reduzieren zu können. Eine
analoge Ableitung provisorischer Resultate für die Zeit von 1905.0—1906.0
ist für das Frühjahr 1906 in Aussicht senommen“.

 

Ta. ALBRECHT.

4.

Spezialbericht über den Internationalen Breitendienst auf dem Südparallel.

„Nachdem der Vorschlag des Präsidiums der Internationalen Erd-
messung: nach Maßgabe der zur Verfügung stehenden Mittel für 2 Jahre eine
Ausdehnung des Breitendienstes auf die Siidhalbkugel vorzunehmen, allgemeine
Billigung gefunden hatte, sind die Vorbereitungen seitens des Centralbureaus
soweit gefördert worden, daß dieser Breitendienst tatsächlich im Januar 1906
begonnen hat.

Dank der tatkräftigen Unterstützung der Direktoren der Observatorien
in Perth und Cordoba: Government Astronomer W. Ernest Cooxe bezw. Pro-
fessor Joun M. Tome, sowie des Chefs der Geodätischen Abteilung des Instituto
Geografico Militar im Buenos Aires Dr. Junio Lrperer war es gelungen, zwei

ianulhailtai

 

 
