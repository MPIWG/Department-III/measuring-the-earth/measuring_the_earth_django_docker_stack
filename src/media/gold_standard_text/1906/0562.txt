 

102

2°, Les observations sont suspendues lorsque les circonstances atmosphériques sont
defavorables: par exemple entre 9 h. du matin et 3 h. du soir, pendant les jours ensoleillés
de l’ete. L’instrument est abrité sous un parasol pendant la durée des observations.
3°. Dans les stations consécutives, le support en bois doit avoir une jambe dirigée
alternativement vers l’arriére et vers l’avant.
4°, L’ordre des lectures est le suivant:
Coup d’arriere: Lecture de la mire dans la partie gauche du champ de la lunette;
Lecture de la nivelle;
Lecture de la mire dans la partie droite du champ.
Coup d'avant: Même lectures que pour l’arriére et dans le même ordre.
Avant de déplacer l'instrument, on vérifie que les differences de niveau, calculées
séparément avec les lectures de mire faites à droite et à gauche du champ, ne diffèrent
pas de plus de 1 mm. Dans le cas contraire, on répète intégralement l'opération.

SUISSE.

Repères. — Depuis 1905, on a modifié les repères à sceller contre des paroïs verti-
cales (repères de 1er ordre). Les nouveaux repères sont constitués par des boulons horizontaux
complètement engagés dans la pierre au moment du scellement, Leur tête présente un trou
cylindrique où, lors du nivellement, on introduit une petite tige sur laquelle se pose la mire.
L'altitude obtenue se rapporte au centre du trou. Un couvercle cylindrique est vissé sur la
tête du repère pour le soustraire à toute dégradation.

Depuis 1903, les repères de 2e ordre sont aussi des repères métalliques.

Mires. — Pour les nouvelles opérations, on utilise les mires & compensation du
Colonel Govier, ainsi que des mires & reversion.

Depuis 1904, pour maintenir la mire en station, on utilise un trépied lui permettant
de tourner sur sa plaque support.

Depuis 1905, chaque brigade est pourvue d’une régle en metal invar, à laquelle on
compare, en campagne, la longueur de la mire.

Methode d'opération. — On observe exactement dans l’horizontale. Les observations
se font en double dans chaque station. Les deux mires d’arriere et d’avant sont placées
rigoureusement à la même distance de l'instrument, L’équation des mires est déterminée
par des observations spéciales.

AMÉRIQUE.
CANADA.

Repères. — Les repères métalliques permanents sont généralement constitués par

des boulons en cuivre rouge solidement enfoncés dans les murs, rochers, etc.; une marque
horizontale est faite, au ciseau, sur le milieu de la tête de chaque boulon; le matricule

aba

dis ullaihani bals

 

 
