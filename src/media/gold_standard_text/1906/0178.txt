 

 

 

      

N 2s
BEE TE EN. ER Sate ree rn eee

ANNEXE A, Iv,

Pik bat.

Rapport sur les travaux exécutés par la Commission
géodésique italienne.

eS ee

Avant de donner des renseignements sur les travaux exécutés par la Commission
géodésique italienne, son Président tient à déclarer que, dans les trois années passées, ces
travaux ont eu une grande extension, grâce à la cooperation de presque tous les professeurs
de géodésie et d'astronomie des universités du Royaume. :

Les travaux exécutés sont les suivants:

TRAVAUX. ASTRONOMIQUES.

I. Dans l’observatoire international de Carloforte on a continué les observations de
latitude pour l'étude des mouvements du pôle (Voir le Mémoire de MM. Cannena et VoLTa,
1905, et Comptes-rendus des séances de la Commission géodésique italienne tenues à Rome
en Avril 1906).

II. Continuation du nivellement astronomique suivant le méridien de Rome par
M. le Professeur Reina de l'Université de Rome (Voir Comptes-rendus des séances etc.,
Avril 1906. Annexe XIII).

III. Détermination de la latitude x M. Mario (Rome) par MM. les Professeurs Dr
Lecce, MiszosuvicH, REINA, Branch et par l'Institut géographique militaire (Voir Comptes-
rendus des séances etc., Avril 1906. Annexes IX, X, XI, XII et XIV D).

[V. Détermination des azimuts de M. Soratte et M. Cavo à M. Mario (Rome) par
MM. les Professeurs GUARDUCCI et Reina et par l'Institut géographique militaire (Voir Comptes-
rendus des séances etc., Avril 1906. Annexes VII, VIII et XIV DB).

V. Détermination de la différence de longitude entre l'Institut géographique militaire
et l'Observatoire d’Arcetri par l'Institut géographique militaire (Voir Comptes-rendus des
séances etc, Avril 1906. Annexe XIV O.).

VI. Différence de longitude entre l'Observatoire de Padoue et M. Mario (Rome) et
détermination des azimuts de M. Soratte et M. Cavo à M. Mario. Ces opérations exécutées

vtt path reed tnt tre dit d momie

 

 
