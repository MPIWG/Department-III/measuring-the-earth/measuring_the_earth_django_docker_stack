 

300

Beobachtung im Azimuthe & des einen Balkens ist daher auch eine Beobachtung am anderen
Balken im Azimuthe «+ r ermöglicht.

Die Wahl der Einstellungen kann auch bei der Benützung dieses Instrumentes in
mannigfacher Weise getroffen werden. Ich will aber hier nur eine Gruppierung der Ein-
stellungen behandeln, welche wir am meisten gebrauchten. Beobachtet wurde in drei Stellungen

Azimuth des Balkens I + = 0
» » il 2, - 180°
nn des Balkens I x, — 120°
> > I 2, 300°
ak des Balkens I se 240°
» » » IT 2% =060:

I. Stellung En
II. Stellung on

IIL Stellung

Wir setzen nun in Gleichung 6.) zur Abkürzung:

K mhl

 

 

 

 

Da, 2D = D,
7 is
und bentitzen die Gleichung:
21) 2 2 al)
nal — or) La + ta cos 24 — b sin æ + bs on

für den Balken I; eine zweite solche Gleichung mit n’, a’, b’, # diene für den Balken 11.
Dann ergibt die Rechnung für die Eleichgemichlstägen. in den drei Stellungen:

zu Où
u ya
DU. ow
ox doy oy oz

 

I. Stellung

 

chou al nn Ltée ailes

/ ’ 7
np] — + 2a

 

urn. où ae OU. b ou
er mn 2 dxoz 2dyoz

II. Stellung = =
| V3 i‘ a) U3 1 D D oÙ
== nr ee

 

 

 

io 2 Deon | 2 on oz

 

 

 

ax oy = ‘our D 350
oo V3 u EU

Il. Stellung

 

U u 20 ie ee b &U

ee ee | ee

Da wie ersichtlich :

(np — 24) + (29 — 22) + (Bo —n,)=9

und

(i a) ae (ui, no) +E (ni n') —0,

 

 
