TER SV TY

FRE TT RRR INET ITTY

Tv TT

PRIT

FT ER AE TON TEE MT TE ET

9 293

Wie bereits im vorigen Berichte erwähnt wurde, heben sich die
Beschleunigungen infolge der Auf- und Abwärtsbewegung des Schiffes nicht
immer auf, so daß also g nicht rein zurück bleibt. Für die Fahrt Sydney —

San Francisco und San Francisco—Yokohama wurde für jedes Barometer
‚gesondert eine neue Ausgleichung vorgenommen, in der diese Fehlerquelle
berücksichtigt ist. Es hat sich hierbei ergeben, daß die Mitnahme eines
von den Beschleunigungsdifferenzen infolge der Schiffsbewegung abhängigen
Gliedes keine erhebliche Verbesserung bedeutet. Die mittleren Fehler bleiben
angenähert dieselben.

In, letzter Zeit habe ich mich damit beschäftigt, eine Methode auszu-
arbeiten, die gestattet, in rascherer und jedenfalls auch genauerer Weise
Schwerkraftsmessungen auf dem Meere auszuführen. Es fallen hierbei die
Siedethermometerbeobachtungen weg. Die Beobachtung erfolgt im wesent-
lichen durch photographische Registrierung und kann auch durch wenig ge-
schulte Beobachter ausgeführt werden.

Die Vorversuche haben günstige Resultate ergeben.“

O. Hecker.
6.
Verschiedenes. Im Jahre 1907 wurde das Kgl. Preußische Geodätische
Institut und Zentralbureau d. I. E. mehrfach von wissenschaftlichen Gästen besucht.
‘Mitte Januar hielten sich die k. u. k. Hauptleute Herren Anpres und GAKSCH

vom militär-geographischen Institut in Wien einige Tage zur eingehenden Besichtigung
der Einrichtungen im Zentralbureau auf.

Herr Dr. Arzssıo, Schiffsleutnant der kel. italienischen Marine, brachte hier
durch Beobachtungen von Mitte März bis Mitte April seine in. den letzten zwei Jahren
ausgeführte Weltreise für relative Pendelmessungen zum Abschluß.

Während des Monats April waren die Herren E.A.J.H. Mopperman, Ingenieur
der kgl. niederländischen Gradmessungskommission, und Prof. Dr. Sumso aus Tokyo
anwesend.

Am 22. April trat Herr Oberleutnant Anwasrasın aus Bukarest behufs seiner
astronomischen Ausbildung ein.

Im letzten Vierteljahr waren ferner im Zentralbureau Ne die Herren

Dr. Irmarı Bonsporrr, Astronom der Sternwarte in Pulkowo, und Dr, Wessex aus
Helsingfors.

Endlich kam anfangs November aus Budapest Herr Dipl.-Ing. Karn Ouray,
Adjunkt des Herrn Professors L. v. Bovora, um sich in der Anstellung von Breiten-
beobachtungen und relativen Pendel Imessungen auszubilden.

Da das Zentralbureau nur wenige verfügbare Plätze hat, so as es im Interesse
der Reflektanten, möglichst frühzeitig an das Zentralbureau mit W ünschen um solche
heranzutreten, sonst kann es vorkommen, daß es unmöglich ist, den W ünschen zu ent-
sprechen, wie es in der Tat sich sehon ereignet hat.

2
u 40

BE a ele RRS roast eS

SSC RSE AACE

Kae

Eee

 

 
