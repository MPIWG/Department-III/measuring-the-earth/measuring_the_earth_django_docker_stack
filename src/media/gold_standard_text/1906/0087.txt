are

x

nn

ate wren

es

conenentes

E
8
=

 

[ETOP OMT IHL ewe op pepe tenes

 

81
Apparate, die für verschiedene ausländische Empfänger bestimmt wareu, sowie auch durch
Prüfung von Uhren gefördert, welche Arbeiten zum grössten Teile Herr Prof. Haasemann,
z. T. Herr Prof. Dr.. Hecknr und Herr Wanacu ausftihrten. Auch erhielten einige aus-
ländische Herren Unterweisung in der Anstellung von relativen Pendelmessungen.

Herr Prof. Borrass ist schon längere Zeit mit dem Studium derjenigen relativen
Pendelmessungen beschäftigt, die zur Ableitung eines Netzes von Hauptstationen dienen
sollen. Das gründliche Studium der betreffenden Veröffentlichungen erschien mit Rücksicht auf
die Wichtigkeit der Zusammenfassung der Einzelarbeiten unerlässlich. Denn etwaige Fehler in
der Annahme von g für die Hauptstationen pflanzen sich über weite Ländergebiete fort.

Die langjährigen Bemühungen der Herren Prof. Dr. Künnsn und Prof. Dr. Furr-
WÄNGLER um die Ableitung des absoluten Wertes der Intensität g der Schwerkraft in Potsdam
haben mit der nunmehr erfolgten Veröffentlichung der Beobachtungen an 5 Pendeln, sowie
des Gesamtergebnisses ihr Ende erreicht. Ich glaube nicht, dass der etwaige Fehler dieses
Ergebnisses 1/449 999 you g überschreitet; dies folgt einesteils aus der Uebereinstimmung der
verschiedenen Einzelbestimmungen in Potsdam, andererseits aus den Unterschieden mit ab-
soluten Bestimmungen, die an anderen Orten und von anderen Beobachtern erhalten worden
sind und auf Potsdam durch sehr gute relative Pendelbeobachtungen übertragen werden
konnten. Einige dieser absoluten Bestimmungen mussten allerdings neu reduziert werden,
wodurch grössere Beträge der Unterschiede beseitigt werden konnten.

Die systematischen Lotabweichungsberechnungen wurden durch Abfassung und Druck-
legung eines Heftes III der »Lotabweichungen” von Herrn Prof. Dr. Börsch gefördert.
Das System der Lotabweichungen für ein ausgewähltes Referenzellipsoid in Huropa ist noch
in Arbeit. | |

Sehr erwünscht wäre es für die Weiterführung dieser Arbeiten, wenn nunmehr
endlich die südschwedischen Dreiecke publiziert würden, um Zentraleuropa direkt mit den
in Schweden und Norwegen ausgeführten Gradmessungsarbeiten verbinden zu können.

Für die Ausfüllung der Lücken in der europäischen Längengradmessung in 47—
48° Br. sind einesteils trigonometrische Messungen in Südbayern und Tirol bewirkt worden,
andernteils in Rumänien (worüber die betr. Landesberichte Näheres bringen dürften).

Das Studium der Krümmungen des Geoids in den Meridianen und Parallelen ver-
schiedener Länder, wo ausgedehnte Gradmessungsarbeiten vorliegen, ist nahezu beendet.
Besonders hervorheben möchte ich, dass es dem Centralbureau dank giitiger Mitteilungen
des Service geogr. in Paris möglich wurde, eine erstmalige Durchrechnung der ganzen
Breitengradmessung, die im Laufe der Zeiten im Anschluss an die französische Meridian-
messung in Grossbritannien, Spanien und Algerien ausgeführt worden ist, zu bewirken. Ich
hoffe eine bezügliche Arbeit, die Herr Prof. Dr. Schumann in meinem Auftrage verfasste,
den Verhandlungen dieser Konferenz beifügen zu können.

Ueber die Verwaltung des Dotationsfonds habe ich die Ehre, Rechnungen mit Belegen
für die 3 Jahre 1903 bis 1905 vorzulegen. Ich füge einen Nachweis des Standes des Fonds
für den 1. September bei, mit einer Angabe des wahrscheinlichen Standes desselben am

Ende dieses Jahres.
I ; 11

 
