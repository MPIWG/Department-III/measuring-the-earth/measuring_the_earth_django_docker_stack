ns PAST

di + à aaa An 6ù 1 : un
TR ET gan

 

Dr 29

Die Summe der aus den Flächen berechneten: sphäroidischen Exzesse
aller 9 Polygone beträgt 18’ 215, während sich der Exzeß des großen Grenz-
polygons aus den beobachteten Winkeln zu 18° 3180 ergibt. Wenn man ferner
aus den Polygonwinkelschlußfehlern allein, in ähnlicher Weise wie nach der
internationalen Ferrrro’schen Formel für die Dreiecke, den mittleren Richtungs-
fehler für eine geodätische Linie ableitet, so erhält man aus den Werten ohne
vane. 0.92.

An den Rechnungen für Heft III der Lotabweichungen nahmen
im Anfang des Jahres, wie auch schon in den letzten Monaten des Jahres 1904,
Herr Dipl.-Ing. Fr. Köster aus Prag und Herr Dr. A. Semprap aus Wien,
außerdem Herr Landesvermessungsrat M. Sucıyama aus Tokyo zu ihrer Infor-
mation teil. Seit Oktober 1905 führte sodann noch Herr Oberleutnant
Bucawazpr aus Kopenhagen einige Rechnungen aus.

Im Jahre 1906 soll Heft III gedruckt und mit der Herstellung des
Druckmanuskriptes für die verschiedenen Verbindungen der westeuropäischen
Dreiecksnetze mit der russisch-skandinavischen Breitengradmessung begonnen
werden“. A. Börsen.

2.

Die Untersuchung der Krümmung des Geoids in den Meridianen
und Parallelen hat Herr Prof. Dr. Scaumayn in Aachen weitergeführt. Für einige
Längenstationen sandte Herr Oberst Burrarp in Dehra Dun die neuerdings astronomisch
ermittelten geogr. Breiten ein, so daß die Berechnung der Krümmung im Parallel von
Vorderindien sicher gestellt werden konnte.

Die Berechnung des neuen französischen Meridianbogens und seiner Fortsetzung
durch Spanien und Algier (vergl. Bericht 1905, .S. 4) wurde mit Benutzung genauer
astronomischer Daten, die der Service géographique in Paris gütigst zur Verfügung
stellte, bis Nemours erledigt. Der Anschluß der Küstenkette:von da bis Alger und der
Meridiankette von hier bis Laghouat ist auf Grund der ebenfalls vom Service géogr.
mitgeteilten Angaben im Gange.

Für den Linienzug von Rosendaäl bei Dünkirehen durch Frankreich und Spanien
bis Nemours wurden folgende Lotabweichungen in Breite erhalten, bezogen auf ein
Ellipsoid, das die Abplattung des Brssnr’schen Ellipsoids hat, dessen Äquatorialhalbachse
aber um nahe 300. m größer ist (wie es diesem Bogen am besten entspricht):

Rosendaél Ole: Rodez + 0.60 Chinchilla + 1°78
Lihons — 1.41 Carcassonne —0.27 Mola — 1.52
Panthéon — 1.79 Rivesaltes — 1.60 Tetica t +- 8.80
Chevry + 0.56 Montolar + 2.84 Roldan | — 6.07
Saligny + 1.48 Lerida = 0.98 Conjuros A
Arpheuillé + 4.89 Javalon — 0.73 M Sabiha + 6.538
Puy de Dôme + 5.61 Desierto — 5.09 Nemours + 7.58

 
