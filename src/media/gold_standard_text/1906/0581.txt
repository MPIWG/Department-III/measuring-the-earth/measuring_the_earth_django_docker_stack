121

DANEMARK.

Altitudes des repéres de jonction avec les nivellements des pays limitrophes.

TEST
pt ee En

 

 

Repéres de rattachement

 

Définition du zéro

 

 

 

 

 

 

Noms des Némér Altitudes définitives des
nas Noms des N repéres repeat ‚au zero auquel sont rapportées
er localités of ils) U nes BOT ee indes
limitrophes Oo eae danois et Cotes adoptées| Cotes adoptées
prussien en en
Danemark (!) Prusse
| m m
Han | Alan NN + 35,595 (1) Cotes deduites des nivel-
5 8609 + 35,558) + 35,553 lements danois, en partant de
Be + ‚8,939 la cote prussienne du repere
Prusse Ranhede | | gang + '9'253| + 9,082 |N° 8600
ut Alar + 20,282)
une 8632 + 20.595; + 20,602
ae ( pi» a 21788
|. | 3037 421,746; + 21,747

 

Altitudes des repères de contrôle voisins des marégraphes ou médimarémètres.

Se EE A md M LA 111 à

ki

MT

TU | a

   

(OC 1 |

Fir

m

 

 

 

Noms des

mers

a a a 4

Mer du Nord

»

Kattegat

>

Petit Belt

Grand Belt
Kattegat
Sund

Baltique

Reperes fe coe des maregraphes ou nme

 

 

Noms des
localites ou
sont situés
les appareils

Esbjerg
Hirtshals

Frederikshavn

Aarhus

Fredericia

Slipshayn
Korsor
Hernbæk
Kjôbenhavn

Gjedser

Numéros

repères

a

ß
© Krudttaarn

vestlige
Kontrolpunkt

® Ferge-
hallen 187-188

(0187—188
== Bro
213

©. Cr

 

 

242

 

matricules des|de l’emplacement|

Désignation

des repères

Sct. Olufs Anlæg

Korsör Bro

Toldbodens nor-
dre Gavl

 

 

repères
rapportées au
zero nn

Altitudes
définitives des
prussien (

2,529
3,746

N: N +
me 050
8, 826
2,742

0,490

(a) 0,861

Profondeur du

niveau moyen

de la mer au-
dessous

du repere du
controle

14,243
9,112

2,806

4,014

N

0,736

0,894
2,690
3,184

1,094

lements danois,
la cote prussienne du repere

n°. 8609

 

 

Définition du zéro
auquel sont rapportées
les altitudes

 

(1) Cotes déduites des nivel-
en partant de

(a) Cote provisoire

 
