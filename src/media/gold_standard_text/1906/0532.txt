   

epee eee eee een .

m a

12

»A mon avis leur importance ne s’arréte pas ici. Quand la sismologie et la géologie,
»par leur coopération mutuelle, auront atteint un développement tel qu’elles pourront fournir
>des résultats pratiques, ces sciences seront appelées à leur tour à rendre des services in-
vestimables aux intérêts matériels et à la sécurité et à la tranquillité des habitants de ces
»contrees et des contrées semblables. :

»Si, par exemple, on pourra démontrer plus tard, ce qui à mon avis est probable,
»que notre terre passe actuellement d’une période de déformation à une période de repos
»relatif, et que les actions à l'intérieur de notre globe, qui donnent lieu à ces catastrophes,
»diminueront, on aura contribué beaucoup à augmenter le bien-être de l'humanité.

» Actuellement le publie est en général déprimé par des craintes inutiles de grands
désastres qui pourraient nous menacer, même d'un catastrophe final et universel, craintes
»qui sont causées par des considérations étroites et pessimistes des temps passés. De mon
»point de vue, qui est naturellement partial, des considérations plus exactes sur l’avenir
»probable de la terre, résultant d’une étude poursuivie de la forme et de la constitution
>de notre globe, fourniront, au contraire, des données d’une importance capitale pour assurer
>le bonheur et le bien-être des peuples: et je pense que la tâche d'obtenir un tel résultat
»appartient en grande partie à la géodésie, dont les travaux, en commun avec ceux des
»autres sciences géophysiques, y sont indispensables.

»Ce n’est pas nécessaire d'entrer ici dans les détails de ces travaux géodésiques mais
»j'ose exprimer ma conviction qu'ils contribueront à la solution du problème fondamental
„de la constitution de notre globe par des moyens beaucoup plus variés et beaucoup plus
»efficaces qu'aucun de nous ne prévoit.

»C'est donc ma conviction intime qu'il est utile, au point de vue de la politique
»pratique, et au point de vue de sa valeur intrinsèque tant matérielle qu’intellectuelle, de
fixer l'attention des pouvoirs législatifs sur la démarche faite par les géologues auprès de
»l'Association géodésique, afin de susciter ou promouvoir une coopération internationale dans
»l'étude d’intéressantes questions géologiques et je suis convaincu qu'on pourra solliciter
>avec succès une augmentation des contributions pour l'extension nécessaire des travaux
» géodésiques”’.

D'après cette lettre il semble que pour le moment M. CHAMBERLIN ne Se prononce pas
pour un changement quelconque dans l’arrangement et le but des opérations géodésiques,
mais il pense que l’avancement des études géologiques constitue aussi une raison pour sol-
liciter la continuation des dotations à notre Association géodésique. Les considérations qui
exerceront une influence sur les opinions des membres des gouvernements populaires, aux-
quels il fait allusion, auront probablement la même influence sur les opinions des ministres
des gouvernements plus centralises.

À mon avis l'Association géodésique pourrait satisfaire au désir exprimé par l’Asso-
ciation des Académies dans le paragraphe B en adoptant une résolution environ dans
ces termes.

Avec grand intérét l’Association géodésique internationale a pris connaissance des

Aide ctslbalinmababadiradieaeasan

 

 
