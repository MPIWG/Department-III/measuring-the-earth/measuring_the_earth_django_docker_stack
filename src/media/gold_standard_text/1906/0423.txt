j
if
48

er ea ame
STO OTTER RTI PRET

Von I zu II von da zu III u.s. w. fortschreitend kann man also die Differenz:

Ge). Ge),

für jeden Punkt N bestimmen, welcher mit dem Anfangspunkte I durch eine gehörige Anzahl
von Zwischenpunkten verbunden ist.

2 .
Da nun in solchen Punkten ( ee durch die Beobachtung bekannt ist und

Te a DU »U
ausserdem die Beziehung AU —=2»* besteht, so können für sie die Werte von Je’ I
2
und a durch Formeln dargestellt werden, welche nur den einen unbekannten Wert:
al enthalten.
ace
Durs Integration erhalten wir dann die Differenz der Lotrichtungen zweier Punkte

nach folgenden Formeln:

 

 

; il 1 U
aad ae m qe = for à
1 #0 l 9 U iC 92 U
a N d de
A A (5 ae I dy + =

   

wo die rechten Seiten dieser Gleichungen nur die eine Unbekannte (= enthalten.
Ci

Wenn also (w'—y), oder (A’—A) durch astronomische Beobachtungen bestimmt
wird, so kann auch diese Unbekannte berechnet werden. Die Aufgabe ist hierdurch gelöst.

Wird dieses Rechnungsverfahren auf ein geschlossenes Polygon angewendet, so bietet
sich zur Beurteilung seiner Zuverlässigkeit eine mehrfache Kontrolle.

Die Summen um das ganze Polygon gebildet sollen nämlich folgenden Bedingungen

genügen:
Mite) a 0
„al: ma (22) — 28) I

a d U ;
— ds = 0.
le

Diese Rechnungsmethode, welche ich probeweise auf ein sehr enges Netz von
Beobachtungen, in einem Zimmer des physikalischen Institutes, mit gutem Erfolge anwenden
konnte, hat sich bei. der Berechnung der Arader Beobachtungen als ungenügend erwiesen.
Da nämlich diese Methode auf die Bestimmung der gewöhnlich kleinen Differenzen der

 

 

dann soll oa.

Werte as gegründet ist, so bedarf sie so genauer und an so viel Stationen ausgeführter

Beobachtungen, wie dies im Freien in einem grösseren Gebiete kaum möglich ist,
I 48

 
