    

ee

ee ee

SS er
See nee

ETES

EE

Se Se

re

Se
SS

Bra

a a ee

=

 

DRE
oo

eee
ee

See
a ee

2
=

ere
en

Besen

Bes

En

me

216

3) From the errors in the triangulation — that is, in the lengths and angles fixed
by the triangulation.

(4) From the deflections of the vertical.

It is proposed to reduce the effects of errors from source (1) to a minimum by
deriving from the computation which is to be made the best possible values for the cor-
rections to the initial data and to the elements of the spheroid.

A careful examination for the actual case in hand, involving the astronomic obser-
vations and triangulation in the United States used in this investigation, shows that the
effects of the deflections of the vertical (4) upon the quantities (PD, — ©), cos D' (A, — 4)
and — cot ' (a, — «) greatly exceed the effects of the errors in the astronomic observations
(2) and of errors in the triangulation (8).

Hence, it is proper to proceed with a least square solution on the basis stated .

above that is, to make 2 Di -- ED? a minimum.

In deriving the formule from which to compute the values of the coefficients k,,
ak, lee 0,, 0,) 03, the starting point was the equations numbered (36), shown
on page 249 of the » Account of the Principal Triangulation of the Ordnance Trigono-
metrical Survey of Great Britain and Ireland, by Capt. A. R. Crarxs, London, 1858”
Space is not available here to show the derivation of these formule. They are —

 

 

 

 

 

 

 

S sin? ®
kl arg u) sine |, = Zero
k, — — sin ® sina 1, == —cos@’
an
ee cone aie © i, zero
sin a, sin é
s sin a, (1 + cosw) 100 À
DM ea wee EE ES ae er
= R a 2 sin”; (4; — x) 2 a sin 1” R en
cos &’ 608 a, Sin @ 100 :
n, = n, = ——— 77 $sing,
a sin &r 4 asin l
er cot ©’ sin 2,08 w 100 pe
Ya SiN ay 2 2 eme a
0 N ae -# cos
1 = R 90000 sn l(1—e/ sing) —°
i 1
= '__@){t + 2 sin? /
a pe UE ln) REPARER Ab Mu
sin? À : (1 —e? sin? D}: cos? © |.
% = 20000 sin 17 (1 —e*sin’Q) mart ooo een
os sin? ® : (le sin O)co 2 ns +
= Nat a

f

The symbols in these formulae have the following meanings: 9, A, # and Dyn, &
have already been defined as referring, respectively, to the adopted latitude, longitude, and

haku dl

dr D 8 rebond

 
