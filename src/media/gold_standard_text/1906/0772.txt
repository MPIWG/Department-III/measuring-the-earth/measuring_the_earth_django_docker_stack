 

294

10

B. Geschäftliche Tätigkeit.

1.

Der Dotationsionds wurde wie bisher verwaltet.

Seine Bewegung

im

Jahre 1907 stellt sich, vorbehaltlich der konventionsmäßigen genauen Nachweisung
der Einnahmen und Ausgaben, wie folgt:

68751 M., wenn 12 900 M. Vorausbezahlung an Betriebskosten für

Einnahmen.
Bestand des Fonds Ende 1906
Beiträge für 1906 .
Beiträge für 1907

Zinsen: Von der Kur- und Nlmaikeischen Hitkeestmafikinten

Darlehnskasse in Berlin .

, +: Von der Königlichen Seehandlung (Pr alliwehd se

bank) in Berlin

M.

”

52 759,74

10 600,00
59 900,82

533,70

1 171,40

EEE ee

M. 124 965,66

Summa:

Ausgaben.

Indemnität des ständigen Sekretärs

Für den Internationalen Breitendienst (orkkpärallei

„aan, 2 > (Südparallel)

Für andere wissenschaftliche Arbeiten oy Schwerkraft und
Erdgestalt)

Für Beschaffung bezw. Teefeiatile von sbintstremiehtii

Für Druckkosten

Kracht, Porto, on endihestaprtan

Summa:
Demnach war der Bestand Ende 1907 gleich

Hiervon befanden sich:
bei der Kur- und Neumärkischen Ritterschaftlichen Dar-
lehnskasse in Berlin
bei der Königlichen Seehandlung (Pr Aasleene Sam)
in Berlin ; : wen
und zum Betriebe in ach bébé Eos Pret
Summa:

An Beiträgen sind für 1901/07 rückständig 15 200: M.*)

M.

”

”

M.
M.

M.

M.

5 000,00
44 339,08
17 114,75

1 211,00
185,42
265,25
996,00

69 111,50

55 854,16

15 262,00

39 092,16
1 500,00
55 854,16

+) Die Gesamtsumme der disponiblen Fonds stellt sich Ende 1907 rechnungsmäßig auf rund

Oncativo a 1908 zu dem Kassenbestand von 55 854 M. addiert werden.

Gaithersburg, Ukiah, Bayswater und

a bide ail aa Label tu dla a mans

 

 
