 

i
Vy
|

i h
b
|
|

i
4
Fo

D D =

 

mm mm

des autres que de 0,1 ou 0,2 on élimine toute chance de faute sur les portées successives ;
il semble done que l’on serait en droit d'attendre un accord plus grand entre les mesures
et il y a certainement lieu de se demander si les fils n’ont pas subi des modifications, soit
par l'effet du voyage, soit pour toute autre cause. Eu rapprochant les mesures faites avec

 

le même fil on obtient les tableaux suivantes:

Segment: Ouest.

 

Segment: Est.

96-31). .4530,92957 | 340... .#3685.70001 | seu
14 ; 919.9 14 2 767,40
* ) 49/2... .4530,91600 Joe "la ee - 74390 a
ee re
1
690,000 78,000

 

 

 

 

1a $26-81/1. ..4530,96290 | 953.97 13 $ 45/2... .3685,79152 | 778.93
Be. ‚94508 4118 198. ‚76633 2
en ao
254,000 146,000
: 1-00). 4500804) nn 3219... 3685, (0143 | 22%
15 871,74 15 53,88
670 ‚86297 | ATA asst
+ 17,48 4 15,10
4 a 1 54 |
259,000 244,000 i

 

Moyenne = 914,98

 

 

Moyenne = 766,74

Toutes les secondes mesures donnent systématiquement une. valeur de la base plus
courte que les premières; tout semble donc se passer comme si les fils avaient subi des
allongements; mais des résultats demandent encore à être examinés et discutés avant d’en

tirer des conclusions.

On a procédé en Russie, et plus particulièrement en Crimée d’une part, et en Boukharie

de l’autre, à la mesure de deux bases.

La base de Crimée, dans l’Isthme de Pérékop près de la ville d’Armjansk, et une

base de contröle de 6392m,851 de long; elle a été mesurée aller et retour avec l’appareil
de Scuusurt qui est analogue a celui de Srruve et composé de 4 régles de fer, rondes,
enfermées dans des boites en bois; l’intervalle entre deux régles consécutives se mesure au
moyen d’une lame & coulisse. Cet appareil a déja servi à mesurer plusieurs bases et a,

RO . Q 7 2 1 Le.
paraît-il, toujours donné de bons résultats. L'erreur relative de la mesure est du 880,000"

L'appareil de SoxuBerr a été comparé à l’étalon construit en 1859 dans les ateliers de

Poulkovo.
