ir

DUR PRE RENCE nn LS LIÉE MARIA
RS ES TEE

abri rer
ANETTE mar

sett

it
Am

Tuer

m

OCTO PRET CE

5
3
=
=
B
x
2
E
zc
ge
11
ae
33

 

79

réseau fondamental, ou d’une fraction tout au moins, formant elle-même un véritable réseau
à mailles fermées, au besoin complété par quelques lignes secondaires, nivelées avec les mêmes
soins que les lignes primaires et franchissant les principaux cols des montagnes du pays.
Toutefois, en pratique, la réitération d’une partie, même assez restreinte, du réseau de
ler ordre d’un grand pays peut exiger un certain nombre d'années; pour la clarté des conclu-
sions, il y aurait donc avantage à laisser, entre les deux opérations, un délai d'au moins 30 ans.
Les probabilités acquises de la sorte se transformeraient en quasi-certitudes si, après
un nouvel intervalle d’un tiers de siècle, le même nivellement ayant été répété une troisième
fois, on retrouvait encore, par rapport à la seconde opération, des écarts analogues et de

mêmes signes.

Un exemple numérique très simple montrera l'importance relative des diverses erreurs
à craindre et donnera une idée de la grandeur que devraient atteindre les mouvements
présumés du sol pour être décelables par la réitération des nivellements.

Soit un itinéraire de 600 km de longueur, aboutissant à la mer par ses deux extré-
mités et franchissant, vers son milieu, un col de 2000m d’altitude. Nous supposerons la
ligne divisée en 6 sections de 100 km, nivelées chacune deux fois, c’est-à-dire aller et retour,
avec les taux d'erreurs ci-après, que l’on peut considérer comme des minima pour les terrains

accidentés, savoir :
0,8 par km, pour l'erreur accidentelle probable;
0,15 par km, pour l'erreur systématique probable;

0,015 par mètre, pour l'erreur probable d'étalonnage des mires.

Entre le co! et chacune des extrémités du cheminement, l’erreur accidentelle totale
aura pour valeur probable: wine
+ 0,8 V300 = + 14mm,

Pour l’ensemble des trois sections, l’erreur systématique probable atteindra:

+ 0,15 X 100 X V3 = + 26mm,

Enfin l'erreur probable d'étalonnage sera:

mm
+ 0,015 & 2000 = + 30mm
et l’erreur résultante: 1
+ VI + 262 + 30 = + 42mm,
Pour la moyenne des deux déterminations répondant à chacun des versants de la

montagne, l’erreur probable sera:
42mm

Ehe gr
V2
et, pour l’&cart entre la moyenne deduite de la première opération et celle tirée de la
seconde, l’erreur deviendra:

 
