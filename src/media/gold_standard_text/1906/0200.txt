 

 

176

 

mötres; pour augmenter la précision, on a inséré dans chaque voyage comme point de rac-
cordement une station, qui est également un point du réseau trigonometrique.

La latitude est déterminée par la méthode des distances zénithales circumméridiennnes.

Aux stations de raccordement Soeroelangoen, Boenga Mas et Moeara Enim les ob-
servations astronomiques furent exécutées sur les piliers mémes de la triangulation; aux
autres stations les lieux d’observation furent rattachés aux piliers par des polygones, dont les
angles et les côtés furent mesurés directement.

Comme la latitude géodésique des points de raccordement est connue, on a pu déter-
miner la déviation relative de la verticale par rapport à la station G. Dempoe — latitude
5° 659.05 S--, le point du réseau principal dont la latitude, déterminée directement, a
servie de base pour le caleul des coordonnées géodésiques. L’ellipsoïde employée est celle
de Bessel.

Dans le tableau B sont indiqués les résultats des déterminations de latitude aux
8 stations de raccordement, ainsi que la latitude géodésique et la déviation relative de
ja verticale.

Les latitudes ne sont pas corrigées de l'influence de la flexion de la lunette; dans
la moyenne des résultats trouvés par les étoiles au Sud et au Nord du zénith cette influence

est suffisamment éli minée.

 

 

 

 

 

Tableau A.

cn nn == = = m —— —
stone Mol. Erreurs de

S Constructeurs des instruments Be en = & |clôture de chaque |
& } cS ees bee 3 oR = > trianele 2
Ss employés et 3982-25 Bon ws A“
= diamètre du cercle horizontal Pace - 8 E 2533 ee A ee
. erraten
il Pistor et Martins, 27 em. ie. 2 24 0.44 | 0.1936
2 n ie 2 ne ee We
3 1 1 2 24,25 0.44 q | 0.1936
4 1 i 2 24.25 0.63 0.3969
5 7 Ie 2 24,15 Oak 4 | 0.0961
6 | Wegener, Pistor ct Martins, 27 em. | 27,1 2 24,25 065 | 04225
gl u On 9 24,25 0:58 -| 0.3364
8 1! Deel 2 24,25 0.93 0.8649
9 1 ie 2 94,25 0.21 0 0441
10 1! 91 9 24 1.74 3.0276
u 1! 9. 2 24 0.29 0.0841
12 1! Yi 2 24 0.04 0.0016
13 1 I 2 24 1.20 | 1.4400
14 Pistor et Martens, 27 em. À 2 24 0.73 0 5329
15 | Wegener, Pistor et Martins, 97 em. |: 21 2 24 4931: | 1497689
ie u gar 9 24 1.93 | 37949
Ly 1 Dols 2 24 ar | 192528
18 " gra 2 24 11,767 1873.0976
19 Pistor et Martins, 27 em. I 2 24 2.002 | 5.5696
20 1 1" 2 24 | 1.70-—|--2:5900
91 | Wegener, Pistor et Martins, 27 em. Qe 2 24 | 2.06 1.1236
22 1! oe] 2 24 0.38 0.1444
23 Wegener, 27 em. 2 9 94 0.03 0.0009

 

 

 

 

 

 

 

ddl ni

rer tee ernennen cnet

 
