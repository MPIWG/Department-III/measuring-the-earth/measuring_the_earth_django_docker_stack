i
8
&
ë

 

395

An allen Orten, wo wir die Drehwage aufstellten, haben wir auch die magnetischen
Intensitätskomponenten bestimmt, ja wir haben sogar die Lücken zwischen entfernten Sta-
tionen durch relative Bestimmungen der Horizontalintensität und der Deklination ausgefüllt.
Das reiche Beobachtungsmaterial harrt teilweise noch der Bearbeitung und es wäre hier
auch nicht am Platze dasselbe eingehender zu behandeln. Erwähnen will ich nur, dass ich
an manchem Orte magnetische Störungen fand, welche den Angaben der Drehwage ent-
sprechend auf Wirkungen magnetischer Gesteine zurückführbar sind. Doch fand ich im
Gebiete der Fruska Gora auch solche sich mit auffallender Regelmässigkeit weit erstreckenden
magnetischen Störungen, deren Grösse und räumliche Verteilung eine Erklärung durch
magnetische Gesteine kaum zulässt. Wahrscheinlich haben wir es hier mit Massen viel
grösserer Susceptibilität, also mit Hisenerzen zu tun, vielleicht mit den Wirkungen von
Erdströmen. Bei der Auiklärung solcher Rätsel wird die Drehwage gute Dienste leisten.

Zum Schlusse will ich noch bemerken, dass es auf Grundlage der Formeln 18.), 19.), 20.)
möglich wird, im Falle homogener Magnetisierung, deren Komponenten x, 8,7 auch für
ganz unregelmässig gestaltete Massen in .deren unmittelbaren Nähe mit der Drehwage und
mit magnetischen Messinstrumenten zu bestimmen.

un TEE litt i

N

 
