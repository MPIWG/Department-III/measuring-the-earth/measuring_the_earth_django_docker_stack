 

38

von Verseez nach Alibunär, das Gebirg in seiner Fortsetzung unter den allu-
vialen Aufschüttungen der Ebene verfolgend.

Im Jahre 1906 sind die schon früher begonnenen Arbeiten bei Arad fortgesetzt
worden, und erstreckten sich bis Ende November auf weitere 84 Stationen.

Den grössten Teil dieser Arbeiten besorgte Herr Dr. D. PikAr, der mir seit Beginn
dieser meiner Versuche hülfreich zur Seite stand. Zusammen mit ihm beobachteten in den
Jahren 1902-1904 Herr Dr. L. STEINER, der besonders die gleichzeitig fortgeführten
magnetischen Messungen besorgte, und seit 1905 Herr J. Fexere. An den ersten Ver-
suchen am Balaton nahmen auch die Herren Prof. L. v. Löczy, Prof. Dr. v. KÔVESLIGETHY,
Prof, J. v. Cuotnoxy und Baron Hankanyr teil.

So häufte sich ein ansehnliches Beobachtungsmaterial zusammen, dessen vollständige
Zusammenstellung nicht die Aufgabe dieser meiner Abhandlung sein kann. Ich will es
aber versuchen eine kurze Darstellung zu geben:

erstens von der Theorie und der Ausführung meiner Methode;

zweitens von ihrer Anwendung zur Ermittlung geodetischer Daten ;

drittens von ihrem Werte zur Beantwortung von Fragen, welche sich auf die
Massenverteilung in der Erdkruste beziehen und somit in das Gebiet der Geologie fallen ;

viertens, werde ich noch auf die engen Beziehungen hinweisen, welche zwischen
den von mir gewonnenen Daten und den magnetischen Störungen bestehen.

Im Laufe dieser Ausführungen wird sich Gelegenheit finden einzelne Beobachtungs-

gebiete beispielsweise ausführlicher zu behandeln.

I. THEORIE UND AUSFÜHRUNG DER METHODE.

1. THEORETISCHES.

In meinen oben erwähnten Abhandlungen habe ich nachgewiesen, dass die Torsions-
wage in Folge der räumlichen Veränderungen der Schwerkraft eine Drillung erleidet.
Unter Annahme der im Raume linear varlirender Schwerkraft, welche in den engen
Grenzen der Apparate zulässig ist, lässt sich diese Drillung theoretisch leicht bestimmen.
Ihre Abhängigkeit von der Form und der Massenanordnung des Gehänges habe ich in den
schon erwähnten Abhandlungen behandelt; hier will ich nur zwei Formen ins Auge
fassen, welche mir bei den zu besprechenden Beobachtungen dienstlich waren.

Die erste Form ist die eines horizontal schwebenden
cylindrischen Hohlstabes, von geringem Querschnitt und an
beiden Enden mit hineingeschobenen eylindrischen Massen be-
lastet (Fig. 1). Die zweite unterscheidet sich von der ersten
nur dadurch, dass die belastende Masse an einem Ende durch
a Aufhängung tiefer gelegt ist (Fig. 2). Es sei nun U die Poten-

ic. Halfunktion der Schwerkraft, bezogen auf ein rechtwinkliges
dessen Anfangspunkt im Schwerpunkte des Gehänges liegt und

 

Koordinatensystem xy 2,

wre detente ind ada el gerne

 

 
