 

334

Höhe der Ostsee und des Bottnischen Meerbusens, da die Ebbe- und Flutherscheinungen an
der Küste Schwedens ziemlich unbedeutend sind. Erst in der letzten Zeit hat man auch die
resp. Flutheonstanten zu bestimmen in Angriff genommen,

Dabei traten bald die Ungelegenheiten herrührend von dem Mangel einer Station
im mittleren oder nördlichen Kattegatt und einer Station nördlich von Qvarken merklich
hervor, Lücken die gar nicht durch ähnliche Registrierstationen in den Nachbarländern
ausgefüllt sind. Die Regierung hat aber jetzt dem Reichstage einen Antrag vorgelegt, die
nöthigen Geldmittel für eine neue Mareografstation an Hallé in der Nahe von Lysekil zu
bewilligen. Hoffentlich wird auch dieser bewilligt, da dieser Station ausserordentliche Dienste
für das Studium der Stromverhältnisse an den Scheeren des Bohusläns zu leisten verspricht.

Die Mareografstation Ratan scheint nicht auf ganz festem Grunde gebaut zu sein.
Der Boden wird durch Seitendruck längs einer unterliegenden, etwas geneigten Felsenfläche
allmählich hinausgleiten. Die kleine Bewegung wird doch durch regelmässige, zweimal
jährlich ausgeführte Nivellements gefolgt und hoffentlich wird später dieser Übelstand durch
Neubau der Station ein Ende gemacht werden.

Die Lage der Nullpunkte der resp. Mareografen werden alljährlich durch Nivellements
an naheliegende Fixpunkte des Precisions-Nivellements verbunden und kontrollirt.

Da die Mareografen Schwedens, besonders an den inneren Scheeren verlegt werden
müssten, war es eine Hauptsache bei der Konstruktion der zu benützenden Apparate dass
sie möglichst einfach sind und geringe Möglichkeiten derangiert zu werden bieten möchten.
Dies hat man auch überhaupt erreicht wie die beigefügte Abbildung zeigt, aber da sie immer
von den Schwimmern abhängig sind, haben sie stets Kontrolle nöthig und machen oft Reise
von Seiten der Zentralbehörde nöthig. Um diese oft mühsame und kostbare Reisen zu
entgehen, ist bei sämmtlichen Mareographen die Errichtung von festen Kontrollpegeln eben
im Gange, die von den Beobachtern jedesmal bei Stillwasser abgelesen werden sollen.

Ausser an den genannten Registrierapparaten werden die Wasserhöhen noch an fünf
Pegelstationen durch direkte Ablesung an der Scala täglich 8 Uhr morgens beobachtet. Diese
Stationen sind Holmö Gadd, Grönskär, Ölands Norra Udde, Grafvarne und Strömstad; sie
sind sämmtlich ausser Holmö Gadd in das Netz der Preeisionsnivellements aufgenommen.

Die Publikation der schon tabulierten und reducierten Beobachtungsresultate durch
das Bureau war bis jetzt aus Mangel an den dazu nöthigen Geldmitteln leider unterblieben.
Endlich hat doch jetzt die Regierung bei dem tagenden Reichstage Antrag um die Bewilligung
von den dazu nöthigen Mitteln gemacht und werden diese hoffentlich bald zur Verfügung gestellt.

Im Mälar-see sind sechs Pegelstationen mit Scala-ablesung errichtet und an ver-
schiedenen anderen Örtern werden schon regelmässige Beobachtungen an festen Wasserscalen
angestellt.

Für die Entwicklung der diesbezüglichen Organisation werden jetzt grosse An-
strengungen gemacht, indem die Vorarbeiten für die Organisation eines das ganze Süss-
wassergebiet umfassenden hydrographischen Instituts zum Abschluss gebracht worden sind,
und es jetzt nur von der Entscheidung des Reichstages abhängt, ob die Organisation schon
im Jahre 1908 realisiert werden kann”. Dr. C. G. Fineman.

Wh ndash dln esestaene

sacs lati

 
