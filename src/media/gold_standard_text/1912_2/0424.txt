 

 

402

 

condition pour la determination des 8 inconnus 2, y et z, nous parait bien facheuse, car,
malgré tous les soins qu’on à pris dans l’organisation du service des latitudes pour éliminer,
autant que possible, toutes les causes d'erreurs systématiques, on ne peut pas admettre
que le but a été parfaitement atteint. Toute cause d'erreurs systématiques dans les résultats
des observations exécutées aux différentes stations aura sur les valeurs des inconnus une
influence d’autant plus petite que le nombre des déterminations superflues est plus grand.
On ne pourra non plus éviter entièrement des perturbations temporaires dans la marche des
observations à chacune des stations — on peut déjà regarder comme telle l'influence d’un
changement d’observateur —, et une telle perturbation aura indubitablement une plus grande
influence sur les valeurs des inconnus dans le cas de 4 stations, que dans le cas de 6 stations.

Sur l'hémisphère austral, les observations à Johannesburg (latitude — 26° 11°) ont
été continuées sans modifications, et dans les trois premières années d'observations com-
plötes, 1910/11, 1911/12 et 1912/1913: 3387, 3105 et 2215 couples d'étoiles ont été observés.
A présent les observations sont continuées, et, d’après les communications du Directeur de
l'Union Observatory, l’Union Astronomer Prof. Dr. R. T. A. Innus, elles seront continuées
encore pendant plusieurs années aux frais de l'Observatoire. Comme nous disposons déjà des
résultats immédiats des observations faites pendant trois années entières — du 22 Mars 1910,
jusqu’au 19 Mars 1913 — le Bureau central a commencé la discussion de ces résultats et
il espère publier, cet été (1914), un premier résultat de cette série d'observations exécutées
dans des conditions climatologiques favorables. Dans le Bureau central on calcule toujours
les réductions des déclinaisons moyennes aux déclinaisons apparentes, dont on se sert dans
cette station; c’étaient surtout Mme Hgxse et Mile LINDEMANN qui se sont occupées des
calculs pour l’année 1915.

La seconde série d'observations, qui fut exécutée dans l’hémisphère austral à Santiago
de Chili (latitude — 33° 84), a été interrompue soudainement par la mort du Directeur
de l'Observatoire, M. le Prof. Dr. F. W. Risrenparr, le 9 Avril 19138. Il est probable que
les observations ont cessé. Par l'intermédiaire de M. le Dr. R. PRAGER, qui est retourné de
Santiago, le Bureau central est seulement entré en possession des résultats des observations
de 1088 couples d'étoiles exécutées pendant l’année 1912, qui indiquent bien une variation
de la latitude conforme à celle qu’on a déduite des observations du service international,
mais qui perdent beaucoup de leur valeur par le fait qu’elles n'embrassent que °/, de l’année
(25 Mars à 30 Décembre), de sorte qu'on n’en peut déduire l'erreur annuelle de clôture.

Dans l’hémisphère boréal on a continué sans modifications la série d'observations
de haute valeur, qui a été commencée au mois de Septembre 1904 à la grande lunette
zénitale de l'observatoire de Poulkovo, à laquelle on a joint, depuis l’année 1908, une
série d'observations exécutées à l'observatoire annexe d’Odessa. On y a continue aussi pendant
toute l’année, de même qu’à Christiania, les observations courantes de l'étoile zénitale à Cas-
siopée. Pour ce qui concerne les résultats de ces séries d'observations qui s'accordent fort
bien avec les résultats du service international des latitudes, on peut comparer les volumes

he bia antun

bear

 
