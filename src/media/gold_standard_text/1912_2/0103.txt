 

 

| |
Observateurs Stations!) n | q | p | my Mu My a à
| | | |
—— mm nn nn nn

Grand cercle azimutal & quatre microscopes BRUNNER fréres n°, 371),
(employé 13 fois)

|

 

 

 

 

 

 

 

 

12 Culangal. 6 8 24 + 2,759 4 3,872 + 4,079 | + 1,681 | + 2,628
13 Cayambe 2 20 20 2,990 | 2,990 | 1,605 1,784
15 Pambamarca 6 8 24 4,928 3 (OIL 3,367 2,035 1,897
15bis Pambamarca 5 8 20 3,832 3,916 3,817 2,664 1,992
15bis id. rattachement de Sin-| 2 20 20 es ‚402 3,402 1,698 2,085
Maurain - cholagua sud.
16 Pichincha 5 8 20 3,097 3,092 3,743 OO 2,424
18 Panecillo. 4 10 20 2,669 3,225 3,289 1,538 2,056
20 Corazon . 6 8 24 6,597 4,205 3,454 1 SO 2,044
21 Sincholagua sud 6 8 24. 5,612 3,949 3,465 1,664 2,149
24 Latacunga 2) B]| [8] [20] [5,236]] [3512] [3,072]| [2186] [1,526]
53 Machala. 2 20 7,451 7,451 4,170 4,367
Perte 156 Guachanama 5 8 20 3,661 4, 590 4,753 3 024 2,094
57 Colambo. 4 10 20 4,374 4,476 4,490 3.901 toe

Grand cercle azimutal à quatre microscopes BRUNNER fréres n° 47),
(employé 8 fois).

 

 

42 Naupan . 5 8 20 3,093 6,509 6,927 2,349 4,608

J44 Bueran 5 8 20 6,268 6,950 1.078 2,580 4,660

148 Soldados. 4 102,189 220 6,740 6,579 6,557 5,367 2,664

Demon 52 Tinajillas 6 6 18 6,332 1,592 7 968 2,617 5,322
: v3 ılerro Urcu: 6 |6&12| 22,82 32,4| 8,811 6,447 6, ‚293 2,432 4,104
58 La Masa. 4 12 24 4,506 4,507 4,506 2,131 2,808

59 Los Pozos 10 20 102. 02) | 125, >4188| 508

(65 Punta Arena 3 8 12 3,222 4,296 4,399 1,872 2,815

Grand cercle azimutal à quatre microscopes Huerz n°. 11),

 

. El Arenal 14 21 9,532 6,824 6,736 4,758 32
64 Terme moyen de la base 45) 10 20 3,804 6,258 6,501 4,544 3,288
de Fonlongue a |
> 68 Payia 1. 3) 14 21 7,561 8,764 8,721 4,468 5,296
68 id. rattachement dela 2 | 20 20 126) on som| Asss
mire 3) meridienne. |
NOTES:

1) Les stations dont les noms sont en italiques ne font pas partie du réseau primordial assurant l’enchaînement entre
les stations astronomiques extrêmes Tulcan et Payta.

2) Les nombres relatifs de la station de Latacunga sont donnés seulement à titre d'indication, les observations y ayant
été faites en deux fois, par des observateurs et avec des cercles différents (voir tableau A. I).

3) Mire méridienne sud de la station astronomique Payta.

4) La direction Cahuito—Yana Ashpa, observée de ses deux extrémités ne coneourt à former aucun triangle utilisable,
5) Y compris la direction d’un signal provisoire, non conservé.

6) Ÿ compris la direction du centre astronomique.

7) Voir les caractéristiques des instruments, note (8) du tableau A.I.

 
