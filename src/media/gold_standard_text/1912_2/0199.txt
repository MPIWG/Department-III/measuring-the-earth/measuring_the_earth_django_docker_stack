187

1%, Les repères fondamentaux (fig. 7 et 8), espacés de 5 à 10 Kilomètres les uns des
autres et constitués par des boulons en bronze scellés horizontalement dans des murs ; au centre,
est ménagé un canal cylindrique dans le-
quel, au moment d’une opération, se place
une tige d'acier servant d’appui à la mire.

Le repère fondamental de Kristiania

(fig. 9, 10 et 11) est constitué par un bloc
de labradorite assis sur un sol stable. Au
milieu de ce bloc est fixé un boulon, sup-
portant, a l’extérieur (fig. 12), une sphére
de bronze sur laquelle on pose la mire. Fig. 7. — Vue de face. Fig. 8. — Coupe verticale.
2°. Les repöres de 2% ordre, répartis au nombre de 1 à 2 entre deux repères

Repère fondamental norvégien.
(Echelle: 4 grandeur naturelle environ).

     

 

    

 

  
 
    
   
  
 

 

 

PERS]
CL

EN
10

  
 

 

 

 

 

Repére fondamental de Kristiania.
Echelle: 5 grandeur naturelle environ).

;
a
Li

 

 

   

ei a
i %
Fig. 9. — Elevation. Fig. 10. -— Vue horizontale Fig. 11. — Socle de la pyramide.
au niveau de ab, la pyramide Coupe suivant cd.

étant snpposée enlevée.

 

 

 

  

Vig. 13 et 14. — Supports de mires
+ grandeur naturelle).

 
