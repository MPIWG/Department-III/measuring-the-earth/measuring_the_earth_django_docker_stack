 

 

 

6

neue ersetzt zu werden, um für weitere 6 Jahre eine genügende Kompensation der

positiven und negativen Differenzen der Zenitdistanzen herbeizuführen. Das neue Stern-
programm ist den Stationen unter dem 7. November 1911 mitgeteilt worden.

Da inzwischen in Band IV der „Resultate des Internationalen Breitendienstes“
bei Gelegenheit der definitiven Bearbeitung der Jahrgänge 1906—1908 Verbesserungen
für die angenommenen mittleren Deklinationen der Sternpaare abgeleitet worden sind,
so konnte nunmehr bei denjenigen 80 Sternpaaren, welche sowohl dem bisherigen als
auch dem neuen Beobachtungsprogramm angehören, von diesen verbesserten Werten
Gebrauch gemacht werden.

Die Reduktionen der mittleren Deklinationen der Sternpaare auf den scheinbaren
Ort sind im wesentlichen von Herrn Rechnungsrat E. Mexpezson und Fräulein Laxpemann
berechnet und die Verzeichnisse der scheinbaren Deklinationen vom 5. Januar bis
6. Dezember 1912, für die Zeiten der Greenwicher Kulmination interpoliert, unter dem
4. Dezember 1911 den Stationen zugesandt worden, um den Beobachtern die Möglichkeit
zu bieten, sich über den Ausfall ihrer Beobachtungen durch Reduktion derselben selbst
Rechenschaft geben zu können.

Gleichwie in den Vorjahren habe ich auch in diesem Jahre eine provisorische
Ableitung der Bahn des Poles für das Zeitintervall von 1910.0—-1911.0 auf Grundlage
der in Band IV der „Resultate des Internationalen Breitendienstes“ abgeleiteten Ver-
besserungen der angenommenen mittleren Deklinationen der Sternpaare ausgeführt und
deren Resultate in Nr. 4504 der Astronomischen Nachrichten publiziert. Dadurch ist
die Möglichkeit gegeben, die im Jahre 1910 ausgeführten astronomischen Beobachtungen
und astronomisch-geographischen Ortsbestimmungen schon jetzt vom Einfluß der Breiten-
variation befreien und auf eine mittlere Lage des Poles reduzieren zu können.

Die Reduktion des IV. Bandes der „Resultate des Internationalen Breiten-
dienstes“, welcher die Resultate der Beobachtungen in den Jahren 1906-1908 auf den
sechs Stationen des Nordparallels und den zwei Stationen des Südparallels enthält, ist
im Frühjahr 1911 zum Abschluß gebracht und der Druck desselben so gefördert worden,
daß die Versendung dieses Bandes noch vor Abschluß des Berichtsjahres erfolgen konnte.

Beobachtungen auf dem Südparallel in — 31° 55’ Breite sind gleichwie im
Vorjahre nur auf der argentinischen Station Oncativo erfolgt. Sie erstrecken sich aber
nur auf solche von 29 Sternpaaren in der ersten Hälfte des Januars seitens des Herrn
Gômez, und von 750 Sternpaaren in den Monaten März, April und Mai durch Herrn
E. Cuauper aus Cordoba und sind Anfang Juni ganz eingestellt worden. Die Tätigkeit
dieser Station hat damit ihren Abschluß erreicht.

Die laufende Reduktion dieser Beobachtungen wurde unter der Leitung von
Herrn Prof. Wayaca von den Rechnern Lehrer A. Wısanowskr und Frau Hesse aus-
geführt, während die Berechnung der Reduktionen auf den scheinbaren Ort im wesent-
lichen durch die Herren Rechnungsrat E. Mexperson und O. Scuénrecp erfolgt ist.

 

werner

pme ah adnan ns, nn
eat eA STUN Sas WTI m

en RANN

 
