 

 

   

‘Tableau n°. 15. — Danemark et Prusse.

 

214

 

| | 1341

Helsingör ! 1496

! | 1497
Helsingborg |
fi »

 

Luna

m
Kronborg, Frederic d. III. 10,974

Bastion

Kronborg Telegraftaarnet 31,256

Düetaarnet

” ”
718, Mog. | Pälsjö Klint

Pälsjö Leje
Vikingsberg

26,239
30,576
10,758
24,862

+ Niveau
moyen (?)

m
10,880

31,162
26,145
30,479
10,664
24.768

m

1. 0,094

-+ 0,094
4. 0,094
+ 0,097
+ 0,094
0,094

 

 

4 — — = - = = EE
j Numéros matricules | Altitudes définitives
j des reperes an orthometriques
i Noms des on on Désignation de | nn Differences
11 emplacement des |
1 localités : es | Danemark! Prusse D-P
i Danemark Prusse | reperes |
I | | | D 2 |
|
. | | ARUN rennen
k st x je : : A à m m nl
P larning 4 8609 | Greensepille, lige over, 35,798 35,549 -+ 0,249
h for tysk Milepæl
a eee ba I gat | at)
4 Ved Hvidding | » 8588 Grensepille ved den 9,495(?), 9,249 - 0,246
i Station danske Greense
Foldingbro ; 8632 | Græn$epille ved Told-| 20,839 20,598 - 0,241
kammerbygningen |
Kalvsltind i Baer | Grien$epille ved Bav-| 21,988(?)| 21,743 - 0,245
| negaard | | |
Différence moyenne générale: D-P = -+ 0,245
Tableau n°. 16. — Danemark et Suède.
Numéros matricules Altitudes définitives Différences
s repèr 3% ; orthometriques D-S
4 des mepencs Désignation de 4
Noms des lin MILE Bulk u a tum Jun une
l’emplacement des |
localités | à Danemark, Suède |. | Moyennes
Danemark |, Suede Ip Partielles par
D S localite
1 2 | 3 4 | 5 6 M | 8

ao

| | + 0,094

0,096
| -+ 0,094

Différence moyenne générale: D-S = + 0,095

i
}
i
j

 
