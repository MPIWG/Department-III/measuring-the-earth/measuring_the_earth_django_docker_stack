 

|
;

 

 

202
Aux deux extrémités de chaque ligne, on a établi un groupe de trois repéres
souterrains disposés, toutes les fois que les conditions locales l’ont permis, sur un
méme cercle avec la borne extérieure correspondante, de maniére que, du centre, les
différences respectives de niveau entre les quatre points puissent étre obtenues, avec
une méme portée de niveau.
3°, A la demande de la station impériale sismologique de Strasbourg, on doit réitérer
& nouveau, en,1913, le nivellement de la ligne d’Alexanderschanze & Stockach, sur
laquelle se trouve, à Sigmaringen, l’épicentre du tremblement de terre du 16
novembre 1911.

k. WURTEMBERG.

En vue de l’étude des mouvements du sol, la ligne de nivellement de Boblingen-
Lustnau, nivelée pour la deuxième fois en 1907, l’a été pour la troisième en 1912.
On publiera en même temps les résultats des deux opérations.

6. FRANCE.

À la suite du tremblement de terre du 11 juin 1907, en Provence, on a réitéré, en
1910, dans la région dévastée, 464 km de nivellements exécutés une première fois en 1888
et 1908.

Ainsi qu'il est expliqué dans le Rapport special sur les travaux du Service du
Nivellement général de la France de 1909 à 1912 (voir la carte insérée dans ce rapport),
la comparaison des nivellements antérieurs et postérieurs au séisme a montré:

1° que dans son ensemble, le sol de la Provence n’a subi aucun changement appréciable
d'altitude à la suite du tremblement de terre;

20, qu’au voisinage de l’épicentre à Rognes et aux abords de Pélisanne, sur de petites
plages de 2 à 6 kilomètres d’étendue, il a pu se produire un très léger soulèvement
du sol, atteignant au plus 4 centimètres. Toutefois ceci n’est qu’une simple hypo-
thèse, les chiffres relevés étant trop proches de la limite admissible pour les erreurs

du nivellement.

8. ITALIE.

Conformément au vœu exprimé par la Commission gouvernementale instituée à la
suite du tremblement de terre survenu le 28 décembre 1908, en Calabre et en Sicile, l’In-
stitut géographique militaire a réitéré les nivellements ci-après, exécutés par'lui de 1898
à 1908, savoir:

1° en Calabre, sur la ligne de Melito di Porto Salvo 4 Gioia-Tauro (87 kilomètres);
20 en Sicile, entre Messina, Castanea, Gesso et Faro Peloro (28 kilomètres),

 

{

 
