 

PRE ei at He una

 

re

) à j
l'erreur de fermeture du polygone total et les erreurs de fermeture des triangles partiels sont dus
uniquement aux erreurs accidentelles de l'observation.

|
’
i
i
i
j

Corrections des orientations et des longueurs des cotés

Avec les valeurs des 9» inconnues x et les valeurs correspondantes de a on forme les
produits ax et on applique les formules

d'Or tr Le an mx
2 1 2 SD ia oe
ao, = Sa...
4 2 "3 4 $ Lae, an ye
ds ds
4910 x : a — | x r
6 u, a el oe eee
Comme vérification on doit obtenir
dQ =. AO
2n
dis ay |
mal, a 8
j

Ces valeurs se rapportent seulement aux côtés diagonaux du réseau.

steered

Pour les cotés latéraux, on déduit de la figure la formule générale

DD =, d'Or

2k+1 2k 2k+2

On connait ainsi les corrections des orientations de tous les côtés.
On a ensuite, pour un triangle quelconque ABC,

UE — 44 cotg 4 = À — dB cotg B=“ — à cotg O

{ oe die NT is EE aile aps da LE 5 Whig eet ise RE . Sdn

 
