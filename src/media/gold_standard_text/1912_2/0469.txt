 

N

ie

On considère maintenant les visuelles diagonales qui sont indiquées par des traits pleins

en

dans la figure (2) et on exclue celles qui sont représentées por des traits pointillés. Les premié
res définissent un parcours équivalent au polygone intérieur.

On calcule, pour les diagonales de ce parcours, les valeurs de X’ — X, Y’ — Y, dans le
sens convenable, et on les multiplie par le coefficient 0,485. Les produits sont les valeurs de
E",n”. On a ainsi

 

A XX Y—Y = ne

2 + 0,3096 1 0,0874 Oa + 0,042

4 2 02778 + 0,1360 0135 + 0,066

6 E 0200 | 4+ 02615 + 0,155 0 12%

8 = 094159 — 0,0241 — 0,202 6) 012

12 op ji + 0,3507 = 0.072 010

14 — 0,0637 — 0,4153 — 0,031 — 0,201

18 — 0,2801 + 0,2749 08 la

20 — 0,1762 — 0,3587 — 0,085 — 0,174

22 — 0,0626 + 0,2888 — 0,030 +. 0,140

24 — 0,1165 — 0,2809 — 0,057 — 0,136
| 28 — 0,2012 — 0,0006 — 0,098 — 0,000
30 —- 0,1310 — 0,1959 + 0,064 — 0,095
34 + 0,1941 00: 4+. 0,094 oi

36 + 0,3275 + 0,1190 + 0,159 + 0,058

40 + 0,2287 pbs + ot — 0,060

42 + 0,2382 1 02557 ı 0116 170113

 
