 

 

 

326

Dehra Dun Pend. 137 Pend. 138 Pend. 139 Pend. 140 Mittel

1908, Nov. 0°.507 2572 0°.507 5002 0°.507 1589 0°.5070866 0°.507 2507
1908, Dez. 2568 4995 1570 0861 _ 2499
1909, Mai 2558 4987 1581 0857 2496
1909, Nov. 2546 4968 1569 0857 2485
1910, April 2575 4971 1581 0856 2496.

Bei den beiden Reihen yon 1908—09 und 1909—10 beträgt der m. F. des Stations-
mittels der Schwingungsdauer im Durchschnitt + 2°.6. 10-7; die mittlere Unsicherheit einer
relativen g-Bestimmung würde danach + 1.4.10? em sek? sein.

Die topographische Reduktion wurde für alle Stationen, wo sie merkbare Werte
erhält, nach den vorhandenen Karten berechnet; sie ist aber in den eingangs genannten
Grundlagen zumteil nicht getrennt angegeben, sondern mit der Bougvzr’schen Plattenreduktion
vereinigt worden, aus der ich sie für unsre Tabelle hergeleitet habe. Es wäre jedoch zu
wünschen, dass diese wichtige Reduktionsgrösse in den Publikationen und Mitteilungen der
Survey of India wiederum gesondert aufgeführt würde.

Gruppe 4. Messungen in Südafrika.

(Ref. Stat. Kapstadt).

Die nachstehenden Angaben sind einem Separatabdruck aus den Transactions of the
Royal Society of South Africa, Vol. II, part 2, 1911 entnommen worden.

In den Jahren 1903 und 1910 führte Herr Dr. LenreLpr, Professor der Physik am
Transvaal University College in Johannesburg, relative Messungen der Schwerkraft auf den
Stationen Johannesburg, Vereeniging, Kapstadt und Lourenco Marques in portug. S.0.-Afrika
aus. Er benutzte dazu einen STERNECK-SCHNEIDER’schen Pendelapparat des Hydr. Amtes in
Pola mit 3 Pendeln und einem Pfeilerstativ, das auf allen Stationen in Anwendung kam. Die
Schwingungsdauern der Pendel wurden auch in Pola bestimmt, es fehlt jedoch die Angabe,
wann dies geschehen und in welchem Umfange die Messungen stattfanden. Als Konstanten
der Pendel für Temperatur und Luftdichte wurden die in Pola ermittelten Werte: 49,26
und 542,0 Einh. d. 7. Dez. der Sternzeitsekunde angewendet.

Die folgende Zusammenstellung enthält das gesamte Beobachtungsmaterial.

Pend. 23 Pend. 44 Pend. 62

Johannesburg 1908, Juni 20 — — 0°.508 1774
(School of Mines) — 20 — — 1837
— 20 — — 1831

— 90 — — 1867

— 20 — — 1847

— 20 — — 1847

 

sate era lad lulkaas Libélin ait esse.

 
