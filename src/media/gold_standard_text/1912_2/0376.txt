 

ANNEXE B. XIV.

NOTE

sur les marées de l'écorce terrestre et sur la rigidité du globe.

PAR

M. Ch, LALLEMAND.

Le coefficient moyen de rigidité du globe a fait l’objet de nombreuses déterminations,
dont les résultats sont passablement discordants.

Suivant les hypothéses faites sur la constitution interne de notre planéte, on a, pour
le coefficient en question, trouvé des valeurs variant du simple au triple.

A mon tour, dans la précédente conférence de l’Association Géodésique Internationale
(Londres, 1908; Volume II des Comptes-rendus, page 291), puis dans le Bulletin astrono-
mique (Tome 28, 1911), j’ai tenté d’aborder ce même problème et, sans faire aucune hypothèse
sur la variation de la densité ou de l’élasticité des matières constitutives du globe avec la
profondeur, j'ai pu montrer qu'entre les marées théoriques du géoïde, calculées pour une
Terre absolument rigide, d’une part, et les marées effectives de ce même géoide, celles de
l’Océan et celles de l’écorce, d’autre part, il existe des relations d’une extrême simplicité.

Cette théorie a été critiquée par le Dr. ScHwEYDAR, dans une récente publication de
l’Institut Géodésique de Prusse !).

Avant d'aborder l’examen de ces critiques et pour la clarté des explications qui vont
suivre, je crois devoir tout d’abord résumer brièvement ma démonstration. La voici:

La petite déviation imprimée aux verticales par l’attraction lunaire (ou solaire) déforme
les surfaces de niveau qui leur sont perpendiculaires et notamment le yéoide.

Si la Terre était absolument rigide, ces surfaces, supposées originairement sphériques,
se transformeraient en des ellipsoides, dont le très faible allongement relatif 4, proportionnel
à la force perturbatrice moyenne, se calcule aisément et sert de mesure à cette force.

Mais la Terre étant élastique, tout changement dans la surface de niveau entraîne,
pour la surface libre, une altération correspondante, atténuée dans un rapport #,, dépendant
de l’élasticité du solide et variant de 1, pour la fluidité parfaite, à zéro pour la rigidité absolue.

Inversement, toute modification de la surface libre se répercute sur la surface de
niveau, mais dans une mesure restreinte. Le rapport #, de réduction dépend de la constitution
du corps et varie de 0,6 pour un solide homogène, à zéro pour un corps ayant toute sa
masse condensée au centre.

1) Untersuchungen über die Gezeiten der festen Erde. Neue Folge, N°. 54, 1912.

 

ahnt asia

bist

 
