 

1
i

a ee a

Se

 

ER

220

8. ITALIE.

Pour des raisons diverses, les marégraphes d’Aneöne, Livourne, Messine, Venise et
Gênes, ont cessé de fonctionner, mais les dispositions nécessaires ont été prises pour leur

prochaine remise en service.
Un nouveau marégraphe (type Ricuarp) a été installé dans les dépendances de

l'Institut géographique militaire à Mazzara (Sicile).
9. Norvägk.

Le repère de contrôle du marégraphe de Kristiana est un boulon de bronze scellé
sur les fondations d’une grue de 20 tonnes, située à quelque distance de l'appareil. Pour
le contrôle journalier, on a fixé, dans le voisinage du marégraphe, une échelle en bois, dont
le zéro doit rester au même niveau que le centre du boulon.

11. PorrTüGar.

Pour achever de relier au réseau les marégraphes de Cascaes et de Lagos, on a
nivelé les lignes de Beja à Faro et de Faro à Lagos.

168%. Tunisie.

Deux médimarémètres ont été installés, l’un à Sousse, l’autre à Sfax.

21. MEXIQUE.

Un médimarémètre, établi à la Vera Oruz, fonctionne depuis peu de temps.
Un autre est en voie d'installation à Tampico, dans le Rio Panuco, 4 500 mètres
environ de l'embouchure de ce fleuve. Un canal siphoïde amène l'eau de la mer dans le

puits en béton armé, contenant le médimarémètre.
Les altitudes des repères de référence des médimarémètres n’ont pas encore été calculées.

24. OIL.

Sont actuellement en fonction sur les côtes chiliennes:

1°. Dans la région nord, deux marégraphes Suisr-Furss, 4 Iquique et Antofagasta et trois
médimarémètres à Arica, Pisagua et ‘l'ocopilla.

20, Dans la région sud, un médimarémètre établi à Talcaguano, à côté du marégraphe
Negretti & Zambra de la Marine.

Avant d'installer d’autres appareils, on a cherché si, du niveau moyen annuel calculé

 

ad à a
