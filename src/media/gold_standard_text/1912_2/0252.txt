 

240

A,.la discordance des résultats des deux nivellements, relevée entre deux repenes
consécutifs de la maille ou de la section considérée ;
r, Vespacement des deux repéres en question;
Nr, le nombre des repéres de la ligne ou de la section, laissant entre eux (N;— 1)
intervalles ;
Np, le nombre total des mailles du réseau, y compris le eas enveloppe ;
Nz, le nombre total des repères du réseau, laissant entre eux (Nr + N)— 1) intervalles;
f, l'écart de fermeture d’une maille, après application de la correction orthométrique,
liée au défaut de parallélisme des surfaces de niveau du globe terrestre ;
P, le périmètre d’un polygone;
yx, l'erreur accidentelle kilométrique probable de la moyenne des deux nivellements
de la ligne ou de la section;
yr, Verreur accidentelle kilométrique probable correspondante, pour l’ensemble du réseau :
y, la mème erreur, déduite des écarts f de fermeture des mailles, y compris celui
(Zf) du polygone enveloppe;
ox, l'erreur systématique probable par kilomètre de la ligne ou de la section considérée;

Cy, ,, 5 , 3 s évaluée directement pour l’ensemble
du réseau ;
OR, 5 5 4 5 h calculée, pour l’ensemble du réseau,

d’après les écarts de fermeture comparés avec l’erreur accidentelle moyenne.

II]. ForMULES ADOPTEES POUR LE CALCUL DES ERREURS.

A. lèe Phase. — Calcul de lerreur accidentelle probable,
effectué sans tenir compte, ni des erreurs systématiques, ni du
poids de chacune des erreurs individuelles.

L'influence des erreurs systématiques étant supposée négligeable, l'erreur accidentelle
kilométrique probable d’une ligne ou d’un réseau peut se déduire, soit des discordances de
repère à repère !), constatées entre les résultats de deux nivellements de même sens ou de
sens opposés, soit des écarts de fermeture des polygones formés par ces nivellements.

On a, pour cela, les formules connues ci-après ”):

1 Ne
à MT D
2) „= Burn
: 2 : ; : à à 5 oe -1) — m
de . ° . ° . . = me D p

1) On a parfois aussi employé dans le même but les discordances relevées entre les extrémités de diverses lignes
nivelées formant ou non des réseaux; mais, généralement, ces lignes sont en nombre trop restreint pour permettre une
correcte application du calcul des probabilités. \

2) Pour la démonstration de ces formules, voir C. R. Confce de Berlin, 1895, 2&me Vol. op. cit. II et „Nivellements
de haute précision par CH. LALLEMAND. 2e edition. — Supplements. Note II. BÉRANGER. Paris et Liège, 1912.

 

 
