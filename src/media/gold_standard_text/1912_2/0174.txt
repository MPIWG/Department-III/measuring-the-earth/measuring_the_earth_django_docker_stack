 

 

162

nombre supérieur assurément à l’erreur d’etalonnage, qui se justifie peut-être par une série
de très petits déplacements des trépieds au moment du passage d’un fil à l’autre.
La correction de pesanteur est, d’après la formule relative à l'emploi de poids tenseurs :

 

 

A CET Ju
& 9
5 mm
égale par portée à: — 0,07 X 0,03,
en supposant : Dirt 38100, ion — 958000.

Sur la base, cette correction est de — 0,7.
En résumé, la règle a donné:

Aller Retour
8481,74946 8481,76106 moyenne: 8481,75526

les fils:
ee oe moyenne: 8481,7486.

L’écart entre les deux moyennes est

1
de l’ordre de 7,400,000’ ce qui est en faveur de la qualité de l'une et de l’autre valeur.

ANNEXE n°, 2.
Note sur l'appareil de M. le Colonel DEINERT.

Frappé de la grandeur de l'écart qu'il y a entre la température réelle de l’appareil
de mesure et la température lue au thermomètre, M. le Colonel Dainerr, actuellement
Chef du Service Géographique militaire au Chili, a imaginé un procédé spécial d'emploi
des rubans et l’a utilisé en particulier à la base de Chinigüe-San Francisco el Monte
(Melipillo), en 1908.

La base se développe le long d’un rail de chemin de fer parfaitement aligné en
plan, mais cette condition favorable n’est pas indispensable, car on peut, avec quelques
modifications à la méthode de mesure, opérer sur un terrain quelconque.

Le principe de l'appareil est le suivant: on déduit la température du ruban, ou
mieux encore, la correction de longueur dûe à la dilatation thermique, non pas des indications
d’un thermomètre, mais de la variation de tension (mesurée au dynamomètre), éprouvée
par un ruban identique et soumis aux mêmes variations de température.

Le ruban est en acier ordinaire, a 50 m de long, 19mm,87 de large et Omm,44

 

4
|
i
i
4
i
3
3
i
j
{
1

{

 
