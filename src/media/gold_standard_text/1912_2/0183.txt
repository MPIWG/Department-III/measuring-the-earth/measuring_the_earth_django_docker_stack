enr TTS

ANNEXE B, VIII.

RAPPORT GÉNÉRAL

sur les nivellements de précision (Période de 1909 à 1912),

PAR

M. Ch. LALLEMAND.

Le présent Rapport fait suite 4 ceux que, en 1903, 1906 et 1909, j’ai eu Vhonneur -
de présenter aux précédentes Conférences.

En vue de sa rédaction, j’ai fait adresser, en avril 1912 au délégué qualifié de
chacun des Etats assoeies, un questionnaire a l’effet:

1°. de recueillir les données indispensables pour me permettre de dresser un tableau
donnant, à la date du ler janvier 1912, la situation des travaux;

2°, de provoquer les observations utiles sur une modification éventuelle à faire subir
à la definition des nivellements de précision.

Pour la célérité des recherches et des comparaisons, j’ai classé, dans l’ordre même
adopté pour le Rapport de 1909, les renseignements ainsi recueillis, et ceux puisés dans
diverses publications officielles.

Toutefois, pour faciliter la discussion relative à la nouvelle définition proposée pour
les nivellements de précision, j'ai réuni, dans un paragraphe final exceptionnel, les avis
recueillis à ce sujet et le tableau des principales formules employées dans divers pays pour
le calcul des erreurs des nivellements.

La division du Rapport est donc, cette année, la suivante:

I. Renseignements généraux ;
IT. Longueur des lignes nivelées, nombre des repères créés, précision des résultats
obtenus ;

Ill. Repères, instruments et méthodes;

IV. Mesure des changements altimétriques, lents ou brusques, subis par l'écorce

terrestre ;

 
