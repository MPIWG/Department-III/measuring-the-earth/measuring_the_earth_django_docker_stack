BEILAGE B. XII.

BERICHT

über den Stand der Beobachtungen der unter dem Einfluss von Mond
und Sonne erfolgenden Deformation des Erdkörpers,

VON

0, HECKER.

Auf der Konferenz der Internationalen Erdmessung in London—Cambridge wurden
Mittel zur Erweiterung der Beobachtungen über die Deformation des Erdkörpers unter dem
Einfluss von Mond und Sonne bereitgestellt. Es war hier besonders an die Gründung einer
Station im Erzbergwerk zu Pfibram in Böhmen gedacht, wo die Möglichkeit vorlag, den
‘Horizontalpendelapparat in einer Tiefe von 1100 m aufzustellen, und wo somit, wie anzu-
nehmen war, erhebliche Einflüsse, soweit sie durch Strahlungseinwirkungen der Sonne her-
vorgerufen wurden, nicht mehr erwartet werden konnten. Die Attraktionswirkung musste
hier also reiner zur Darstellung kommen.

Weiter wurde der Wunsch ausgedrückt, vergleichende Beobachtungen von Horizontal-
pendeln mit Drahtaufhängung mit solchen mit Spitzenaufhängung vorzunehmen.

In der Werkstätte des Geodätischen Institutes in Potsdam wurde hieraufein Horizontal-
pendelapparat mit Drahtaufhängung nebst Registrierapparat für die Internationale Erdmes-
sung hergestellt, der in seiner Konstruktion genau dem im Besitz des Geodätischen Institutes
befindlichen Apparate entsprach.

Der zuletzt genannte Apparat wurde inzwischen an der neu eingerichteten Station
des Geodätischen Institutes im Bergwerk in Freiberg i. Sachsen parallel mit einem Apparate
mit Aufhängung der Pendel auf Spitzen aufgestellt und zwar in einer Kammer, die sich
in 189 m Tiefe befand. 5

Beide Apparate registrierten auf demselben Registrierapparat, was die Bedienung
erschwerte und wobei sich erhebliche Störungen einstellten, die besonders dadurch hervor-
gerufen wurden, dass bei der Korrektur der Lage der Pendel Verwechselungen der 6 auf der
Registrierwalze sichtbaren Lichtpunkte vorkamen, wobei .dann häufiger beide Instrumente
gestört wurden.

Immerhin ergaben die Beobachtungen mit Sicherheit die Ueberlegenheit der Draht-
aufhängung gegenüber der Spitzenaufhängung.

 

i
i
j
i
i
j

scarred an bh ee

 
