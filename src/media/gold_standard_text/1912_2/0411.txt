9 389

7

einem Pendelapparat des Geodätischen Instituts relative Schweremessungen auszuführen.
Das Zentralbureau hielt es für zweckmäßig, die Gelegenheit zu benutzen, auch die
Sternwarte in Moskau erneut anzuschließen, was mit Genehmigung des Herrn Direktors
Ceraski und mit Unterstützung der Arbeit durch die Sternwarte auf der Rückreise von
Kasan erfolgte. Vor der Reise und nach der Rückkehr am 7. Juli führte Herr Professor
Haasemann Anschlußbeobachtungen in Potsdam aus. Die Kosten wurden für den ersten
Teil der Reise von russischer Seite, für den zweiten vom Zentralburean getragen.

Im Jahre 1910 hatte Herr Prof. Haaszemann mit einem Dreipendelapparat des
Geodätischen Instituts Anschlußmessungen in Potsdam ausgeführt, um die Britische Ant-
arktische Expedition von 1910 des Captain Scorr in Stand zu setzen, in der Antarktis
Schweremessungen im Anschluß an Potsdam auszuführen. Damals übernahm der Physiker
Herr C. 8. Wricur den Apparat samt einer Uhr des Geodätischen Instituts. Demselben
sind auch Beobachtungen auf Roß Island gelungen, und er kam nun im Monat Oktober
nach Potsdam zurück, um erneute Anschlußmessungen zu machen und die Apparate
wieder abzuliefern. Es ist erfreulich zu sagen, daß die drei Pendel sich nicht nennens-
wert geändert haben und man also wichtige Ergebnisse erwarten darf.

F

=
E
E

i;
i
dE
» Bs
i
|
i
5
i

Im Beginn des Jahres machte Herr Prof. Hassemann auch erneut eingehende
Bestimmungen der Temperaturkoeffizienten für den 4-Pendelapparat der Dänischen
Gradmessungskommission. Späterhin ermittelte er ferner erneut die Temperaturkoeffizienten
für den Apparat der Ungarischen Gradmessung mit 4 Pendeln. (Vergl. den Jahresbericht
des Direktors des Kgl. Geod. Inst. 1912/13, S. 25/26). In beiden Fällen*bestätigen die

neuen Bestimmungen recht gut die früheren Ergebnisse.

 

 

Der Ingenieur der Niederländischen Gradmessungskommission, Herr Venxing-Meınesz
is führte in den Monaten November und Dezember Beobachtungen mit 2 Pendelapparaten
im G. I. aus, einem Apparat nach Derrorcss und einem nach Sreankor (von Srückrarn). U. a.
war es seine Absicht die Einrichtungen des Instituts zu benutzen, um die Temperatur-
koeffizienten des französischen Pendels zu ermitteln.

 

Im Monat Dezember führte endlich noch Herr Dr. Fageruoum aus Stockholm mit
einem neuen Feennerschen 4-Pendelapparat Anschlußmessungen im Geodätischen Institut
aus, wobei Herr Prof. Haasemann sehr eifrig mitwirkte, da die Zeit drängte.

4,
Isostatische Schwerereduktionen
Herr Dr. Hüsxer hat im Auftrage des Zentralbureaus nachstehende isostatische
Schwerestörungen für 13 Küstenstationen von Afrika unter Annahme der Hypothese Prarr-

Hayrorp abgeleitet. Die beobachteten Störungen sind in der Tabelle den berechneten
gegenübergestellt.

   
