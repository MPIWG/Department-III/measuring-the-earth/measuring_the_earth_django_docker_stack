an

2 ROR TIO

een pn

23

bridge abgehaltenen 16. Allgemeinen Konferenz der Internationalen Erdmes-
sung, redigiert vom Ständigen Sekretär H. G. van DE SANDE BAKHUYZEN.

II. Teil. Spezialberichte und Berichte über die Tätigkeit des Zentral-

bureaus in den Jahren 1908, 1909 und 1910. Mit 17 lithographischen

Raten und Rare 4
Il. Teil: Spezialbericht über die relativen Schweremessungen mit
2 Karten. ee... ,
14. Resultate des Internationalen Breitendienstes. Band IV. Von Tr. Ansreon
und B, Wanach. Mit 6 Daten. 7 =>)

»

15. Annual Report of the meteorological and the seismological observations made
at the international latitude Observatory of Mizusawa for the “can 1910 . 80

22

C. Inventaire des instruments et d’autres objets de l’Association
géodésique déposés au Bureau central.

Voir les rapports de 1901, 1904, 1905, 1909 et 1910.

Un appareil enregistreur a été acheté pour la station de Pribram,

La pendule Srrasser et Roups N°. 301, qu'on avait pretee à la station de latitude
Bayswater (Australie occidentale), lors de son installation, mais qui après la démolition de

cette station est restée encore pendant quelque temps à Perth, est retournée à Potsdam dans
le courant de l’année.

Le nombre des livres de la bibliothèque est monté à 740.

Potsdam, Février 1912. F. KR. Hezmerr.

 

 
