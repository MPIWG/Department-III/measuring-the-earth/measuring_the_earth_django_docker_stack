EN errungen |

E
F
F

 

 

299

Tabelle VII. Schweizerische Messungen.

Gruppe 2. Messungen der Schweizerischen Gradmessungs-Kommission
nach dem Jahre 1900.

(Ref. Stat. Basel).

Ueber den Fortschritt der Schweremessungen in der Schweiz liegen uns folgende
Publikationen und schriftliche Mitteilungen vor:

1) Astronomisch-geodätische Arbeiten in der Schweiz, XII. Band, Schwerebestimmungen in
den Jahren 1900-1907, Zürich 1910;

2) Astronomisch-geodätische Arbeiten in der Schweiz, XIII. Band, Polhöhen- und Schwere-
bestimmungen bis zum Jahre 1910, Zürich 1911;

3) Procès-verbaux de la Commission géodésique suisse, 57—58ne séance, 1911— 191% ;

4) ein Handschreiben des Herrn Prof. Gautier vom 11. Mai 1912, nebst einer tabel-
larischen Mitteilung der Ergebnisse der Schweremessungen im Jahre 1911.

Die definitiven Resultate der g-Messungen aus den Jahren 1900— 1907, welche in
der unter 1) genannten Publikation zur Veröffentlichung gelangten, sind bereits in unsern
Ber. 1909, S. 172 u. f. aufgenommen worden (vergl. dort auch den Zusatz auf S. 168).
Nachzutragen bleiben unsrerseits noch die Resultate der Schwerebestimmungen auf 9 Sta-
tionen im Simplontunnel, die in der genannten Publikation S. 334 u. f. mitgeteilt werden.
Um das gegebene Material unsrer Tabelle anzupassen, müsste man aus dem auf der Tunnel-
station P (Meereshöhe 7) beobachteten Werte 4 den Beobachtungswert 7, für den in der
Lotlinie von P liegenden Oberflächenpunkt P, (Meereshöhe 77) berechnen kônnen, und von
diesem Werte ausgehend, die Grössen (g,), und (7,) nach dem Bougver’schen Verfahren
ableiten '). Dazu müsste man unter anderm die topographische Korrektion 7, der horizon-
talen Platte H,—H für den Punkt P, kennen, die in den Ausdrücken für y, und (g,), auf-

1) Es sei P die Tunnelstation, P, der in der Lotlinie von P liegende Oberflächenpunkt; ferner seien H und
H, die Meereshöhen von P und P,. Sind dann g und g, die Schwerebeschleunigungen in P und P,, wie man sie in
diesen Punkten unter dem Einflusse der gesamten Erdmasse beobachten würde, und bezeichnen 7 und 7, die Werte der
topographischen Korrektion der horizontalen Platte H,—H in den Punkten P und P,, so besteht, bei Beschränkung
auf Glieder 1. Ordnung, zwischen g und g, die Beziehung:
ee

woraus man nach BouUGuER die Grössen

 

H 8 H,-H à
— 9) Bi 1 a
CN te ee Le Ge a0

 

+2)—@+ 7)

 

H oe eG Che EE es Flt)
Ga) GN ig) ee 1 7
(g1)8 = 9 I> aut ir)

finden würde. Diese Formeln zeigen, dass zur vollständigen Reduktion einer Tunnelstation nach BouGVER nur die Grössen
T und 7,, also die Vertikalanziehungen der über dem Horizonte 7, vorhandenen und unter ihm fehlenden Massen auf
die Punkte P und P,, zu ermitteln sind, wobei für beide dieselben, der Karte zu entnehmenden Höhen gebraucht werden. B.
