 

Die mit einem (4) versehenen älteren y-Werte sind sehr fehlerhaft (Ber. 1909, 8. 30)
und können nicht zur Vergleichung dienen; ich habe sie hier nur der Vollständigkeit wegen
mit aufgeführt.

Herr Prof. Atussio war stets eifrig bemüht, unter den vielseitigen und oft schwie-
rigen Verhältnissen, die sich ihm auf seiner Forschungsreise darboten, einen möglichst hohen
Genauigkeitsgrad seiner Beobachtungen zu erreichen; und seine Bemühungen sind, wie ein
Vergleich seiner Resultate mit den besten neueren Ergebnissen zeigt, auch von Erfolg ge-
wesen, sodass seine Arbeit als ein wertvoller Beitrag zur Festigung und zum weiteren Ausbau
des internationalen Schwerenetzes begrüsst werden muss.

Die relative Schwerebestimmung Genua—Padua 1909 führte Prof.
ALzssI0 gemeinsam mit Dr. Sırva aus. Sie ist eine teilweise Wiederholung der Bestimmung
Padua—Genua-—-Venedig von 1904, einer Erstlingsarbeit Auzssıo’s, wobei ihm noch keine
praktische Erfahrung über relative 9-Messungen, besonders aber keine genügende Kenntnis
seines Apparates zur Seite stand. In Genua war inzwischen ein besonderes Lokal für Pendel-
beobachtungen dicht neben dem Gebäude des R. Istituto Idrografico errichtet worden, das
die denkbar günstigsten Bedingungen für derartige subtile Arbeiten darbietet (Koordinaten
des Pendelpfeilers: ® = 44° 25.15, A = 8° 55.3 IT (Pendellinse) = 97 m). In diesem Lokal
fanden die Beobachtungen von 1909 statt. Auch in Padua schwangen die Pendel nicht auf
dem Lonznzoxr’schen Standort (#7 = 19 m), sondern in einem zu ebener Erde gelegenen
Turmzimmer (7 = 15m), wo die Verhältnisse wesentlich besser waren als dort, besonders
hinsichtlich der Temperatur. Die Ergebnisse der Messungen aber wurden auf LORENZONTS
Standort (4 = 19 m) reduziert.

Auf dem Dreipendelstativ wurden bei diesen Messungen nur die beiden sich gegen-
über stehenden Lager benutzt, für die allein das Mitschwingen o in einwandsfreier Weise
ermittelt werden kann. Die Endwerte der beobachteten Schwingungsdauer waren:

1909 Pend. 32 Pend. 33 Pend. 34 Pend. 35 Mittel
Genua I 0°.507 6208 0°.507 6506 0°.507 5343 0°.507 7248 0°.507 6326
Padua 5951 6253 5071. 6987 6066
Genua JI 6208 6508 5349 7245 6326,

sie zeigen eine vollkommene Konstanz der Pendel und in den einzelnen Unterschieden -

S,—S, eine gute Uebereinstimmung. Zur Ableitung des Schwereunterschiedes 9.9; wurden
die Mittelwerte

S(Genua) = 0°.507 6326 + 2.10~7 sek
S(Padua) = 0 .507 6066 + 2. »

verwendet. Für den m. F. der relativen g-Bestimmung Genua—-Padua findet Herr Prof.
Arzssıo den Wert + 0.0011 cm sek-?, Eine Zusammenstellung der beiden Auzssıo’schen
g-Werte von 1904 und 1909 für Genua mit den Resultaten anderer Beobachter ist am
Schluss der nächsten Gruppe, S. 820, gegeben.

 

ended aaa

 
