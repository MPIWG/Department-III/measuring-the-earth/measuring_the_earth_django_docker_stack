 

 

156

La base de Blida (restant de la base) a donné les nombres suivants:

l'ère mesnre fils n® [8.4 .

. 9040,064.89

 

IT 061.43
Lek a: 063.50
506; 080.54
una... 081.09
ala: 078.02

26 mesure ls me. 10.7 . L 037.08
14. 031.31
He 097.4
206: L 026.26
Se 5. 016.03
308.2... 031.05
Moyenne: .. 9040,050.75

Les écarts vis-à-vis de la moyenne ne suivent nullement la loi de Gauss, mais
accusent une différence systématique entre les allers et les retours qui ne se rencontrent pas

dans les mesures du segment-étalon.

Or les étalonnages effectués sur le segment-étalon montrent que les fils se sont tous

allongés, du 26 février au 10 mars, 4 savoir:

mm

le fil n°. 13 de 0,20
14 „ 0,82

in , 0.20

06, 0,07

307 „ 0,34

>08 , 022

Ces nombres expliquent en grande partie les

mm

Moyenne = 0,80

écarts qui affectent la base entière; si l’on

étudiait segment par segment, on arriverait à la même conclusion, sauf pour l’un d’eux où le
changement de signe à peut-être pour cause un choc, passé inaperçu, donné à l’un des trépieds.
Voici d’ailleurs le tableau des sections avec l'écart ,aller moins retour” qui y

correspond :

Longueur

IPorme sud: 4 1,056 m
ee 1.969 ,
Bee 2,089. ,,
(Ferme mord 2. elon,
es 1492, ,

Aller moins retour
1 7mm
mn,
Es
Eo ,
+22 „

Pour calculer l’erreur, je suppose d’abord qu’en accouplant les mesures aller et retour pour

 

 

i
i
3
i
3
i
i
i

dh ana

ku bu
