|

|
FE

li

 

195

sur 1 pied de hauteur, au sommet duquel est ménagé un évidement central de 6 pouces
carrés, fermé par une plaque de cuivre portant cette inscription :

GT. Survey
O
Upper Mark.

@

Le centre de cette plaque est percé d’un trou exactement à l’aplomb du cercle
figurant au milieu de l'inscription gravée, au-dessous, dans la pierre.

AMÉRIQUE DU NORD.
21. Erars-Unis.

Calculs. — Les altitudes ont toutes subi la correction orthométrique.

22. Mexiqun.

Niveaue. — Les caractéristiques des niveaux utilisés sont les suivantes:
Modele du Nivellement General Modele du Coast and
de la France Geodetic Survey
Lunette. — grossissement: . 26 fois, 25 et 38 fois suivant l’oculaire utilisé,
Nivelle. — rayon de courbure: 60 métres, 163 métres.

D’aprés les résultats obtenus (Voir $ ID, les erreurs systématiques auraient été no-
tablement plus fortes avec le second instrument qu'avec le premier.

AMERIQUE DU SUD.

24, CHILI.
Mire. — Les mires sont comparées chaque jour à un mètre étalon en acier.
Niveau. — Deux modèles sont en usage:

1° le niveau du Coast and Geodetic Survey, construit en invar (acier-nickel), et employé

de préférence dans le Nord, 4 cause des grandes variations de la température dans
cette région;

2° le niveau de la Landesaufnahme.

Ces deux instruments présentent les caractéristiques suivantes:

Niveau du Coast and Niveau de la
Geodetic Survey Landesaufnahme
Valeur angulaire d’une division: . . . . . Q” 4",
Rayon de courbure de la file: . . . environ 200 métres environ 100 mètres.

 
