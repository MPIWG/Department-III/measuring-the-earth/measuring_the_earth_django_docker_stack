 

 

depuis un grand nombre d’années en Europe, Il est à espérer que la coopération, que nous
avons demandée aux différents états, ne nous manquera pas, afin de faire avancer les travaux
plus rapidement.

2. SERVICE INTERNATIONAL DES LATITUDES.
Le service international des latitudes sur le parallèle de + 89° 8’ dans l'hémisphère

boréal a de nouveau fonctionné sans interruption pendant l’année 1913.
Pendant cette année on a observé au total:

à Mizousawa. . . . 2034 couples d'étoiles
4 1:chardjout + 1980 3 2
a Varloiorte 93047 ; 1
a Gaithersburg.. . . 1866 N ®
a Dincinnatt. 1 7 1199 , .
alkiah. 1 1928 55 5

Pendant l’année 1918 les observateurs étaient :

à Mizousawa; M. le Prof. Dr. H. Kimura et M. le Dr. M. Hasximoro:

à Techardjoui: M. le Lieut.-Col. KREMLIAKOW jusqu’au mois de Juin et M. le
Capitaine Maximovirsox depuis le mois de Juin;

à Carloforte : M. le Dr. G. Bemporap et M. le Dr. V. Fontana;

& Gaithersburg: M. le Dr. Frank E. Ross et depuis le mois de Février aussi

Na ie De © R. Duvarı;
Cincinnati : Me Pro Dr ). OC. Porter et M. le Dr. D. ). Yowaon;
Ukiah: M. le Dr. W. F. Meyer.

9»

9,

De même que dans les années précédentes, les réductions ordinaires ont été faites
immédiatement après la réception des cahiers originaux des observations par M. le Prof.
Wanacx, observateur à l’Institut géodésique, avec le concours des calculateurs: M. A.
WisanoweKi, M. O. Scxünrezp et Mme Hess.

Pour les déclinaisons moyennes des 80 couples d'étoiles, qui appartiennent tant à l’ancien
programme qu’au programme qu’on a introduit le 5 Janvier 1912, on a utilisé les corrections
des valeurs adoptées des déclinaisons déduites des réductions des observations exécutées pendant
les années 1906—1908 (voir Vol. IV des „Zesuliate des Internationalen Breitendienstes”
pag. 161). Pour les 16 couples nouvellement introduits dans le programme, on a determine
seulement les declinaisons moyennes d’aprés les positions empruntées aux catalogues d’étoiles.

La plus grande partie des quantités nécessaires à la réduction des positions moyennes
aux positions apparentes a été calculée par M. O. ScHönreLp et Mme Hgsse. Afin de mettre
les observateurs en état de réduire eux-mêmes leurs observations et de se rendre compte
de la précision de leurs résultats, les relevés des éphémérides des déclinaisons apparentes,
du 7 Décembre 1913 au 7 Décembre 1914, interpolés pour les temps de culmination à
Greenwich, ont été envoyés, le 26 Novembre 1913, à toutes les stations.

 
