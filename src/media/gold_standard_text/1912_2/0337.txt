 

 

 

|
|

 

315

 

 

 

I à | 1. Satz 2. Satz | 3. Satz | 4. Satz 9. Satz 6. Satz 1. Satz | 8. Satz
To | Spies. À | Spies. B | Spieg. A | Spies. D Spieg‘ N Spiec. (By Spies, AY) Spires B
| | | |
it | oe | Pra Ee} | p.'sael pa P. 34 Di 30 P. 35
Tee 33 | 3 | 34 | 34 35 35 32 32
IE | 34/10} 34 | 39 | 39 32 32 33 33
Zr > 34. | 34 | SD Li 30 32 32 33 33
II | 33 aoe | 34 | om | 35 35 30 32
en 32 a oon | 33 |: 34 Se 35 35

 

 

 

 

 

Es sei noch erwähnt, das jedes Pendel 2 Spiegel A und B (A aufder Vorderseite, B auf
der Rückseite) hat, sodass es auf jedem Lager in 2 verschiedenen Positionen schwingen
kann. Zwischen 3 Lagern und 4 Pendeln sind daher 24 Kombinationen möglich, und da
jede Kombination von 3 Pendeln sowohl in direkter als in retrograder Folge der Pendel
beobachtet wurde, so ergeben sich für jede Station 48 Bestimmungen der Schwingungsdauer.

Das Mitschwingen (s) für die gegenüber stehenden Lager I und II (Hauptschwingungs-
ebene) ist nach der Zweipendelmethode direkt bestimmt worden, und zwar fanden auf jeder
Station 8 solcher Bestimmungen — für jeden Satz eine — statt. Für das Lager III, das
kein Gegenüber hat, musste 7 auf indirektem Wege ermittelt werden; es geschah dies durch
Vergleichung der auf diesem Lager beobachteten 16 Schwingungsdanern mit ihren korres-
pondierenden Werten auf den Lagern I und I, für die das Mitschwingen gegeben war !).
Auf den beiden ersten Stationen (Kingston und La Plata) bestimmte Prof. ALEssio das
Mitschwingen in 2 um 90° verschiedenen Positionen des Pendelapparats auf seinem Pfeiler;
er gab dann aber dieses etwas umständliche und nicht immer anwendbare Verfahren auf und
beschränkte sich auf den übrigen Stationen auf eine Stellung des Apparats,

Auf 10 Stationen konnten die Pendelbeobachtungen in astronomischen Observatorien
angestellt werden, welche die Zeitbestimmungen zur Ableitung des Ganges der Koinzidenzuhr
lieferten. Nur in Kingston und Callao war der Pendelapparat in Privatgebäuden unter-
gebracht. Der Gang der Koinzidenzuhr musste hier durch Vergleichung mit 3 guten Bord-
chronometern (Dent 55124, Dent 53150, Kullberg 5320), deren Gänge bekannt waren, er-
mittelt werden. In La Plata, Callao, San Franeisco, Melbourne, Perth und Sydney standen
Kellerräume, an den übrigen Orten meist grössere Räume zu ebener Erde zur Verfügung.
Abgesehen von Hongkong war die Temperatur der Beobachtungsräume überall sehr konstant;
in Hongkong, wo der Pendelapparat im magnetischen Häuschen des Observatoriums Kowloon
stand, schwankte die Pendeltemperatur während der ganzen Beobachtungsdauer auch nur
zwischen den extremen Werten von 30°.0 bis 33°.4. Einige Bodenunruhe infolge von Erd-
beben machte sich in Manila, Tokyo und Zikawei bemerkbar; für Tokyo und Zikawei ist

1) Obgleich diese Ableitung von oc im Hinblick auf die Erfahrungstatsache, dass dasselbe Pendel auf einem
andern Lager nicht immer dasselbe Verhalten zeigt, wohl nicht ganz einwandsfrei erscheint, so kann man doch annehmen,
dass die im einzelnen Fall entstehenden Fehler sich im Mittel aus 16 Schwingungswerten zum grossen Teil aufheben werden.
Da ausserdem das Verfahren zur Bestimmung von co, für alle Stationen (einschliesslich der Ref. Stat.) vollkommen
identisch war, so ist ein etwaiger konstanter Fehler in c,, auf die relative g9-Bestimmung ohne Einfluss,
