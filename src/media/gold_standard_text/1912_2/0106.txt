 

Observateurs

 

|
Stations!) N | p | m

Grand cercle azimutal à quatre microscopes BRUNNER frères n°. 26).
(employé 3 fois).

| | \V
. .

8 Yana Urcu, rattachement de Cunrro #) 2 10 = 13,045
Lallemand obs Pichmeha 0... ... 00... 2 20 7,607
18bis Panecillo, rattachement de Sincholagua sud. | 2 20 4,470

Grand cercle azimutal à quatre microscopes BRUNNER frères n°. 3).
(employé une fois).

Maurain non re nn. 4 2 | 20 | 2,889

NOTES:
1) La station dont le nom est en italiques ne fait pas partie du réseau primordial assurant l’enchaînement entre
les stations astronomiques extrêmes Tulcan et Payta.
2) Mire méridienne sud de la station astronomique Tulcan,
3) Mire méridienne nord de la station astronomique Tulcan.
4) Mire méridienne sud de la station astronomique Pinllar.
5) Y compris la direction prise comme référence, qui ne fait pas partie de la triangulation.
6) Voir les caractéristiques des instrumeuts, note (8) du tableau A. I.

Erreur moyenne d’une observation de direction pour l’ensemble des observations effec-
tuées par la méthode des directions:

 

|: m A067 = + 1,545.

i Poids moyen d’une direction finale dans l’ensemble des observations effectuées par
1 la méthode des directions:
à 19,64.

Erreur moyenne d’une direction finale dans l’ensemble des observations effectuées par
la méthode dés directions:

| alt
mË = + V 19,64 [m] = +1",075 = + 07,348
2

Résumé:

Méthode des angles Méthode des directions

69 cas); „(16 cas)
Erreur moyenne d’une observation de direction 239,229 — = 1,692 = LOT a hod
Erreur moyenne d’une direction finale observée 21129 0,500 E10 = + 0,948
Poids moyen d’une direction finale observée 21,40 19,64

(unité de poids: poids d’une observation de
direction ou d’une observation de couple).

Poids moyen d’une direction finale pour l’ensemble de la triangulation: 21,14.

 

 

at da ct ht LM
