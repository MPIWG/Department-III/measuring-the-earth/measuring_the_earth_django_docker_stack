 

i
h

 

404.

avec un appareil tripendulaire de l'Institut géodésique, afin de mettre l’expédition antarctique,
de 1910 du Capitaine Scorr, en état de faire dans la région antarctique des déterminations
relatives de la pesanteur par rapport à Potsdam. Le physicien ©. S. Wricur a alors emporté
cet appareil avec une pendule de l’Institut géodésique. [1 a pu faire les déterminations à Ross
Island, et il est retourné, au mois d'Octobre, 4 Potsdam afin d'y renouveler les observations
avec les mêmes pendules et de nous rendre les instruments. Je suis heureux de pouvoir
constater que les trois pendules n’ont pas subi de changements appréciables, de sorte que
l’on peut s’attendre à de bons résultats.

Au commencement de l’année, M. le Professeur HAASEMANN a fait à nouveau des
déterminations fort exactes du coefficient thermométrique pour l’appareil à quatre pendules
de la Commission géodésique danoise. Ensuite il a déterminé à nouveau le coefficient thermo-
métrique pour l'appareil à quatre pendules de la Commission géodésique de la Hongrie (voir le
rapport annuel du directeur de l’Institut géodésique royal 1912/13 pag. 25/26). Dans les deux
cas les nouvelles déterminations se sont accordées fort bien avec les résultats obtenus auparavant.

Pendant les mois de Novembre et de Décembre, l'ingénieur de la Commission géodé-
sique néerlandaise M. Vexin Maingsz a exécuté dans l’Institut géodésique des observations
avec deux appareils de pendule, l’un d’après DerrorGes, l’autre d’après STERNECK, construit
par SrückraTH. C'était son intention, entre autres, d'utiliser les installations de l’Institut
pour déterminer le coefficient thermométrique du pendule français.

Enfin au mois de Décembre, M. le Dr. FacerHoLm de Stockholm a exécuté dans
l’Institut géodésique, avec un nouvel appareil à 4 pendules de FECHNER, des déterminations
relatives pour obtenir la jonction avec Potsdam; comme le temps pressait, M. le Prof.
FHHAASEMANN a coopéré fort activement à ces travaux.

4, RÉDUCTION ISOSTATIQUE DE L’'INTENSITÉ DE LA PESANTEUR.

Le Bureau central a chargé M. le Dr. Hügner de calculer les perturbations iso-
statiques de la pesanteur à 13 stations sur la côte d'Afrique, en partant de l'hypothèse Prarr-
Hayrorp. Dans le tableau suivant on trouve les perturbations observées et calculées.

Perturbation de la pesanteur

Latitude Longitude Caves sleulde
se ne Cin Secm: cm. SCG

Tanger 2255 40 5 490 — 0,040 + 0,011
Dakar + 14 40 17 25 > Se DH + 0,072
Bathurst 419 27 16 34» + 0,122 + 0,036
Freetown 1 16 20 13 14» + 0,085 + 0,033
Monrovia aon 10 49 » + 0,086 + 0,055
Lagos + 6 28 3 26E + 0,059 + 0,035
Libreville + 0 22 9 27 » — 0,030 + 0,016
Cap Lopez — 04 8 48» + 0,051 + 0,043
Banana — 6 1 12 23 » + 0,040 + 0,016
Lobitobucht — 12 20 13 35 » = 0,059 + 0,002
Gr. Fischbucht — 16 24 11 43 » + 0,049 + 0,034
Lüderitzbucht — 26 39 15 10 -+ 0,036 + 0,002
Moçambique —15 2 38 25 » + 0,078 + 0,051

a bia ath aia

 
