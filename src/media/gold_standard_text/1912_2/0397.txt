 

 

if

À

IE

LE

je

|

ı

x

|

|

|

a

i
F

 

 

 

375

Mais ce premier point étant définitivement élucidé, on s’est demandé si un fil déter-
mine à l’aide d’une base murale se retrouve, sur le terrain, dans des conditions d'observation
assez semblables pour que l’on puisse admettre sans correction la valeur obtenue dans son
étude préalable. Cette question est, en effet, indépendante de la concordance entre les valeurs
d’une même base fournie par des fils divers, aussi bien que par l'égalité de valeur de chacun
de ces fils, déterminé avant et après leur emploi sur le terrain. Le mode de tension,
l'éclairage, l'orientation des réglettes terminales, même du modèle créé au Bureau inter-
national, pourraient réaliser, dans les deux cas, des conditions assez différentes pour entraîner
des divergences mesurables, Telle est du moins l'opinion exprimée par quelques géodésiens,
et que seules peuvent appuyer ou infirmer des expériences directes, consistant à mesurer
la même base à la règle et au fil.

Les résultats de deux déterminations ainsi faites en 1911 sont parvenus à notre
connaissance; ils sont fort instructifs, ainsi qu’on va le voir.

4. — La mesure de la base de Copenhague, faite sous la direction du Capitaine
N. P. JOHANSEN, et qui avait donné, pour quatre fils étudiés au Bureau international, des
valeurs concordant au millionième, avait conduit à un résultat différant de iooo de Celui
qu'avait fourni la mesure de la même base, faite en 1838, au moyen de l’appareil de
BEssez, et avait rendu dès lors désirable le contrôle des fils sur le terrain >

Une détermination récemment exécutée de la base de 240 m installée à Potsdam à
donné, par les quatre fils:

Fil n°. Base à
mm mm
279 240 019,88 +- 0,01
280 240 019,89 -+ 0,02
281 240 019,98 + 0,11
282 240 019,71 016

mm
Moy. 240 019,87
soit encore une concordance moyenne faisant ressortir le millionième avec une parfaite certitude.
D'autre part, la même base a été déterminée au moyen de l’appareil bimétallique
de BRUNNER, sous la direction du Professeur E. Borrass, avec les résultats suivants:

Dates Valeurs à

% mm mm
Printemps 1903 240 019,41 — 0,58
Eté 1904 240 020,91 + 0,92
Automne 1905 240 919,65 084

mm
Moy. 240 019,99

La concordance des deux résultats moyens est donc aussi parfaite qu’on puisse l’espérer.
L'écart entre les trois valeurs trouvées à des intervalles un peu supérieurs à une année

1) Bericht von Dänemark, Comptes rendus de la dix-septième Conférence générale de l'Association géodésique
internationale. Vol. I, pag. 239.

N

 
