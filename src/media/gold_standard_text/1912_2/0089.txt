81

 

Station

N°. of

© O I O OU WDD

eS
ad

11
12
18
14
15
16
4
18
19
20
21
22
23
24
25
26
27
28

Anm.: Die hier gegebenen Längen der Stationen 1 bis 13 weich

1
9

“a

|
|

|
Name of Station |
|

|

| Katha
| _(Mandalay Meridional Series) |

eu Rose le are .

| Ubyètaung :
(Mandalay Meridional Series) |

Taungkalat
Sinpitaung
Tangte
Lakar Bum
Loi lem

Pumpri Bum
Loi Paleng
Loi Song
Loi Hsamsip (Loi Hsam-Hsip).

ef ete Peu cercle he

ee a eee trees el
Se etes te fa he

bois Au Hpanıı =. te) .

ol Won. nn. .
Poibengyess ... ie
Cha-tzu-shu (Ki’ao-hto-Shan).
Loi Kang mong
Loi Taow
Loi Maw
Loi Hpa tan
| Loi
Loi
Loi
Loi
Loi
Loi
Loi
| Loi
Loi
Loi
Aunglawn

a
Chang
Hsimu

ee ot een ete ete
ee el eine ten ris
ste tee ie ee

Bemone |

 

 

24 1154 |

|

Latitude |

|

93 4052 |

4331 |
29 49

55 48
49,98
93 34
59 58
1414
49,19
93 35

43 3

29 12
26 4 |
42 58
1725
11 54
90 58
55.36
38 29
Alo) 41
27 29
157
46 36 |
39 30
57 25
22 1395
21 4010
42 31

bo
bo

21

 

22 691

 

den im Bericht von 1903 Seite 239 verôffentlichten Werten ab.

| cane pala 2.0.0. %
| (Great Indus Series)

| Kharko

|__ (Great Indus Series)
| Kutta-ka-kabar
| Machali

Oe eae ot ver ete fe:

EPMO ele tell ere les dei 0e

Series Kalat Longitudinal.

97 95 4°
3545

30 22
45 54

67 3044
33.13

1954
22 20

Capt. Turner

”

2

 

 

 

 

en infolge eines Fehlers in den Originalrechnungen um 1”

T. & 8. 12" No.2 | 190406
> | 490506
; 1905-06
à 1905—06

INDIEN.
| | Poe lee
4 de ce
Longitude! Name of Observer Instrument >
en S | zZ oS
| ee ve
Series Great Salween.
|
96 15 16 Capt. Turner a2 2 N23 100201 3
95 57 43 : : 1901—01 | 2
96 40 57 » 5 1900—01 | 2
45 46 | Capt. Turner & Capt. Wood : 1900—01 | 2
54 56 : T. &S. Na 3a & 9, 1900-03 | 2
97 1047 Capt. Turner 1 & 3. N38 1900—01 | 2
18 27 Capt. Wood i So 5. NO. 2 1902—03 | 2
24 8 à As 1902—03 | 2
37 25 is oy 1902—03 | 2
42 0 : ; 1902—03 | 2
5835 | Capt. Wood & Lieut. Cardew | T. & S. Nos 9 & 4 1902-03 | 9
1907—08 | *
98 832 > 5 1902—03 9
1907—08
21 23 Capt. Wood Dre S No 1902—03 | 2
32 5 > y 1902—03 | 2
39 58 + ” 1902—03 | 2
2341 Lieut. Cardew T, & 8. No, 4 1907—08 | 2
1116 “ oy 1907—08 | 2
28 28 5 5 -1741907—08 | 2%
053 > 1907—08 | 2
439 ; à 1907—08 | 2
19 26 Capt. Browne We oS. 125 No 8 (0009| 3
97 9125 à 5 1908—09 | 2
98 431 5 oe 1908—09 | 2
25 40 | 5 5 1908—09 | 2
97 5443 . 5 1908—09 | 2
AO 45 |, 8 s 1908—09 | 2
98 4715 } es ‘s 1908—09 | 2
47 29 Not observed at — == un
99 1818 og — _ —
22 3 . — == =

von

 
