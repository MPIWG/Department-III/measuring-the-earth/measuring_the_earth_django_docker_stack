 

 

228
XIV. Heft. (Erweiterte Ausgabe des IV. Heftes).
l. Die Elbe von der sächsischen Grenze bis zur Seevermündung.
Il. Die Saale von der anhaltischen Grenze bis zur Mündung:
III. Die Jeetzel von Siedenlangenbeck bis zur Mündung.
IV. Die Ilmenau von Lüneburg bis zur Mündung.

3
|
!
i
i
i

XV. Heft. Das Wesergebiet (Weser, Fulda, Diemel, Itter, Aller, Leine und
Innerste). 1911.

XVI. Heft. Das Warthe- und Netzegebiet (Warthe, Kanalisierte obere Netze, Brom-
berger Kanal, Brahe, Netze von Nakel bis zur Mündung, Küddow und
Drage). 1911.

Feinnivellement der Küsten und Wasserstrassen in Schleswig-Holstein. I. Mitteilung.
Nordseeküste und Hider. 1909.

Hohen tiber N.N. von Festpunkten im linksniederrheinischen Gebiet zwischen Uer-
dingen und Honnepel. 1910.

4. DANEMARK.

Den danske Gradmaaling. Ny Række. Hefte n°. 8.
Precisions nivellement. Fyn, Sjolland og Falster. Kjöbenhavn 1911

et sous presse:

Den danske Gradmaaling. Ny Rekke. Hefte n°. 9.
Vandstandsmaalingen, Normalhöjdepunktet for Danmark, Sammeligning of
projssiske og svenske Nivellements-koter danske, m.m.
et
Den danske Gradmaaling. Ny Rekke. Hefte n°. 10. |
Résumés des cahiers nos 1 à 9. |

and nn

D. ESPAGNE.

Résumés des altitudes des lignes suivantes:
Huesca à Lérida,
Voie ferrée de Madrid à Zaragoza,

id. Zaragoza & Alsasua,
id. Alicante à Murcia,
id. Sevilla à Huelva,
id. Medina del Campo à Zamora.
6. FRANCE.
Cu. LALLEMAND. — Nivellement de haute précision, 2e édition. BÉRANGER, éditeur. —

Paris, 1912. }

 
