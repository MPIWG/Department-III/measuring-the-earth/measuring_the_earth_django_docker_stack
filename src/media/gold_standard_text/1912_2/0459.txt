 

 

EEE REN ENT

SRT ORT

Corrections des angles observés

Lorsqu'on-a obtenu, comme il vient d'être indiqué, les valeurs des 2» inconnues x, on en

déduit les corrections des deux angles correspondants de chaque triangle. Ces corrections se rap-

portent, il ne faut pas l'oublier, aux angles déjà corrigés et dont la somme est exactement égale,

dans chaque triangle, a 180° plus l'excès sphérique. di |

Les corrections sont les valeurs mêmes de x lorsque le côté latéral du triangle appartient
au polygone qui limite le réseau. à l'intérieur et les ‘valeurs de —-x lorque ce côté latéral appar-
tient au polygone extérieur. i .

La correction du troisieme angle de chaque triangle est alors égale et de signe contraire a la
somme des deux autres. - Doe no a

Il suffit ensuite de comparer lesangles ainsi corrigés aux angles observés pour en deduire
les corrections finales de ces derniers angles.

€

Précision des mesures

La précision des mesures angulaires d'une triangulation géodésique est définie, sans am-
biguité, par les erreurs de fermeture des triangles successifs et l'erreu” probable de chaque angle
est le quotient par V3 de l'erreur probable de fermeture des triangles.

Mais on peut aussi déduire l'erreur probable des’angles de l’ensemble des corrections ob-
tenues après la compensation. En effet si l'on déduit, de l'ensemble des corrections, la valeur de la
correction probable, sow produit par \/3 représente aussi l'erreur probable des angles observés.

Pour le démontrer il suffit d'observer que la correction prélimineire dés trois angles de
chaque triangle équivaut à une première compensation où le nombre des équations de condition
est égal 4 celui des triangles. Dans chacun d'eux la correction d'un angle est le tiers de l'erreur de
fermeture du triangle correspondant; par consequent la correction probable de l’ensemble des an-
gles est le tiers de l’erreur probable de fermeture. Il résulte que la correction probable est le quo-
tient, par 4/3, de l'erreur probable des angles. Cela équivaut à la relation indiquée plus haut.

La compensation, qui tient compte de l'erreur de fermeture du polygone, ajoute quatre nou-
velles équations aux » premières et l'on conçoit que;la correction probable, qui correspond au
système des » + 4 équations, doitêtre du même ordre de grandeur que la première, si toutefois

 
