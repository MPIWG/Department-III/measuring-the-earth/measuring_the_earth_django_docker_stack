 

1875.

1880.
1887.

1888.

1889.

1890.
1892.

) 1895.
1898.
1908.

1906.

1909.

1912.

 

 

370

 

Verh. P.K. $. 111—143. Y. Virrarceau: Nouveaux théorèmes sur les attractions
locales et applications à la détermination de la vraie figure de la Terre. Second
théorème sur les attractions locales et première détermination de la vraie figure
de la Terre, fondée sur la comparaison des nivellements géodésiques et géomé-
triques. Troisième théorème sur les attractions locales et seconde détermination de
la vraie figure de la Terre obtenue sous le concours des nivellements proprement
dits. Nouveau mode d’application du troisième théorème sur les attractions locales
au contrôle des réseaux géodésiques et à la détermination de la vraie figure de
la Terre. Remarques concernant l'emploi des séries trigonométriques dans la
représentation des effets des attractions, et l’intégration de l’équation différentielle
des surfaces de niveau.

Verh. A. K. Generalber. S. 7, 8. ÆE. Apan: Note sur la figure de la Terre.

Verh. P. K. Annexe If. F. R. Hrrmert: Rapport sur les deviations de la verticale.
58 8.

Beilage Ia. F. R. Hrrmert: Bericht über Lotabweichungen. 53 S., 3 K.

Verh. P.K. S. 17—22, 32, 383. Anlage I. F, R. Hrımert: Vorschläge zur Vervoll-
ständigung des astronomischen Netzes. 6 5., 2 K.

Verh. A. K. Anlage Vle. F. R. Hsımerrt: Bericht über Lotabweichungen. 4 S.
Annexe VIf. F. R. Hutmurr: Rapport sur les deviations de la verticale. 5 8.
Annexe X. F. Tisserand: Mémoire sur les méthodes employées pour déterminer
l’aplatissement terrestre. 9 8.

Verh. P. K. $. 24—26, 74—76. F. R. HezmerT: Bericht über Lotabweichungen.

Verh. A.K. 8.506—517. Beilage Vb. F. R. Hrumert: Bericht über Lotabweichungen.
Rapport sur les deviations de la verticale.

Verh. A. K. I. S.180—191. Beilage A. VII. F. R. Hermert: Bericht über die Lot-
abweichungs-Bestimmungen, 189».

Verh. À. K. $. 257-276. Beiïlage A. Ile. A. Bogrscx: Bericht über die Lotabwei-
chungs-Bestimmungen. 1898.

Verh. A. K. II. S. 399—426. Beilage B. XVII]. A. Bozrscu: Bericht über Lotab-
weichungen.

Verh. A. K. I. 8. 337—395. Beilage A. XIX. Baron Roranp Eörvös: Bestimmung
der Gradienten der Schwerkraft und ihrer Niveauflächen mit Hilfe der Drehwage.
II. 8. 133—161. Beilage B. X. A. Borscu: Bericht über Lotabweichungen (1906).

Verh. A. K. Il. 8. 255—284. Beilage B. XIII. A. Börsch: Bericht über Lotabwei-
chungen. (1909).

Verh. A. K. 1. 8. 319—357. Beilage A. XIX. Baron Rozann Börvös: Bericht über
die geodätischen Arbeiten in Ungarn, besonders über Beobachtungen mit der Dreh-

wage U.S. W.
Verh. A. K. Il. 8. 259—260. Beilage B. X. F. R. Hrımerr: Bericht über Lotab-

weichungen,

i
’
i
i
i
j

ben is diana
