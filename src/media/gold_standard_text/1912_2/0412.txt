 

 

390 10

Schwerestörung

a Lane beobachtet berechnet
Tanger —+ 35° 46’ 5° 49° W — 0.040 + 0.011
Dakar +14 40 ir 25 W. —- 0.116 + 0.072
Bathurst ie 27 16 34 W 012 20086
Freetown + 8 29 13 14 W —+ 0.085 + 0.033
Monrovia + 6 19 10 49 W — 0.086 — 0.055
Lagos + 6 28 3 20 + 0.059 + 0.085
Libreville 1 0 22 9 20H —0.080 +0.016
Kap Lopez — 0 42 8 48 HE + 0.051 + 0.048
Banana — 6 1 12, 29 m + 0.040 + 0.016
Lobitobucht —12 20 15 50 i + 0.059 + 0.002
Gr. Fischbucht — 16 24 iE 23H —+ 0.049 + 0.034
Liideritzbucht — 26 39 ia 10 2 —- 0.036 + 0.002
Mocambique — 15 2 38 25 BR, — 0.078 40.051

 

 

Im vorigen Tätigkeitsbericht waren die Störungen für 9 Küstenstationen angegeben.
Im Mittel hat man für die insgesamt 22 Küstenstationen, von denen 19 in Afrika liegen:

Schwerestörung beobachtet = + 0.042 cm - sec”?
berechnet = +0.026 , ,

»

Die Vergleichung der beobachteten und berechneten Werte weist auf bedeutende
regionale Anomalien hin.

Herr Dr. Hüsser hat in seiner Doktordissertation (Berlin, März 1913), die den
Titel führt: „Beitrag zur Theorie der isostatischen Reduktion der Schwerebe-
schleunigung“, hauptsächlich die streng „sphärische“* Rechnung mit der „ebenen“ Rech-
nung verglichen und die erste auf diese zurückgeführt.

Wegen anderweiter Arbeiten sind die Berechnungen isostatischer Reduktionen
jetzt abgebrochen worden.

OL

Beobachtungen zur Bestimmung der Bewegung des Lotes unter dem Einfluß von
Mond und Sonne.

Herr Dr. Scuweyvar hat nachstehende beiden Veröffentlichungen des Königlich
Preußischen Geodätischen Instituts verfaßt:

Untersuchungen über die Gezeiten der festen Erde und die hypothetische
Magmaschicht;

Harmonische Analyse der Lotstörungen durch Sonne und Mond.

 

 

danish pase

died
