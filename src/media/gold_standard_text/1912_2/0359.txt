em

TE

 

 

findet und wohl durch ungewöhnlich grosse Störungen in der lokalen Massenanordnung
veranlasst sein mag.

Den durchsebnittlichen m. F. einer relativen g-Bestimmung gibt Mr. Bows (1. c. p. 13)
zu 0.0027 cm sek-? an und glaubt, dass der wirkliche Fehler bei keiner Station den
4fachen Betrag dieses Wertes überschreiten werde. Diese obere Fehlergrenze würde auch
für den grössten Teil der in unserm Ber, 1909, 8. 248, Gruppe 2 nachgewiesenen y-Bestim-
mungen gelten.

In seinem Bericht für die 17. Allgemeine Konferenz der I. E. bemerkt Herr Super-
intendent Tirrmann, dass für die nächste Zukunft keine weiteren relativen Schweremessungen
in den Vereinigten Staaten geplant sind.
