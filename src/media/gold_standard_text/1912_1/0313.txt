 

(TT

WPT OWT CTE RTT PETE

Tr

283

Les déterminations de différence de longitude ont été faites par application de la
méthode primitivement indiyuée par M.M. Cnaupz, Drrencourr et FErnif. Les appareils
employés pour la transmission et la réception des signaux ont été les mémes que ceux dont
se sont servis les officiers du Service géographique. Pour Paris—Bizerte, dans chacune des
deux stations, les observateurs des deux services ont employé pour déterminer l’heure, une
même pendule.

Pour la détermination de l’heure les instruments étaient deux petites lunettes méri-
diennes portatives appartenant au Bureau des longitudes et construites par la maison GAUTIER
de Paris; chacune d'elles est pourvue d’un micromètre impersonnel avec moteur, tout à fait
analogue à celui qui fonctionne déjà depuis plusieurs années à l'Observatoire de Paris au
Cercle méridien du Jardin. On a toujours eu soin d'employer la méthode de retournement
de la lunette entre les deux parties de l'observation de chaque étoile, avec nivellement dans
chaque position, de manière à éviter le calcul de la collimation, celui de la valeur de la
vis micrométrique, et à éliminer les erreurs provenant des irrégularités de la vis et de
l'inégalité des tourillons. Les observations de passages étaient enregistrées sur un chrono-
graphe de Hipp, placé sur une table chronographique extrêmement simple, puisqu'elle ne
comportait aucun appareil auxiliaire, tel que relais, milli-ampèremètre ou rhéostat.

Pour comparer les pendules, on prenait pour origine de la seconde en chaque point
le moment où la palette de la plume de la pendule venait frapper l’électro-aimant. Supposant
que, au moment même où la seconde commence, le circuit de la plume des observations
soit fermé, Vindication fournie par cette plume ne coïncidera pas exactement avec l’origine
de la seconde; il y aura un retard, dont il fallait tenir compte dans les calculs de réduction,
et que par suite il était absolument nécessaire de mesurer. À cet effet un dispositif très
simple imaginé par M. RENAN, après des études auxquelles a participé M. Juzes Bammavp,
a permis de réaliser cette condition de faire fermer le circuit de la plume des observations
au moment où la seconde commence; quatre fois par soirée, l’observateur effectuait cette
mesure pendant une quarantaine de secondes, et cette installation, qui a toujours fonctionné
sans aucune difficulté, a permis de tenir compte de cette correction.

Dans une première série d'observations, M. LanCErIN était à Bizerte, M. Tsarsopouros
à Paris.

Première Série. Deuxiéme Série.
19/1 a0] 14 2920 525,38 Mai 18 29m 523,38
In 229% 52 20 al 20 52,99

19 29 52 38 23 29 52,98

20.29 32,21 25 Ao) oa

2a 29 52.48 29 29 52. 48

26 29 52,87
29 29 52,41
moyenne pondérée 29m 523,396 29m 928,406

 
