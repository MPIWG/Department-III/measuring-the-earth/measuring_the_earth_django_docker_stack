 

 

174

imaginée qu’on suppose placée au niveau de la mer. Comme on sait, les considérations sur
la distribution des perturbations de la pesanteur ont donné un grand appui à la théorie de
Prarr sur l’isostasie de la croûte terrestre, question dont je me suis occupé à plusieurs
reprises depuis une trentaine d'années. A l’aide des observations de M. Oscar HEcKER sur
les Océans, d’aprés la méthode de M. Moun, exécutées par linitiative du Bureau central, ce
Bureau a pu démontrer positivement que cette isostasie existe dans le cas des masses conti-
nentales par rapport aux profondeurs des Océans. Le Bureau central a publié les travaux
de M. Heoker sur ce sujet, et, dans un mémoire présenté à l’Académie des sciences à Berlin,
j'ai examiné d’un point de vue critique les résultats, afin d’elucider les conséquences que
Von peut en déduire. Dans un article que j'ai publié dans le 6° Volume de l'Encyclopédie
mathématique, j'ai exposé les principaux résultats qu'on a pu déduire de l’étude des déter-
minations de la pesanteur‘). Dans cet article j'ai aussi tâché d'évaluer combien, d’après la
théorie de Sroxgs, le géoide s'éloigne, dans la direction radiale de l’ellipsoide général de
rotation. À mon instigation l’ancien membre du Bureau central, M. R. Schumann (& present
à Vienne) a publié les résultats d’une recherche analogue dans les mémoires de l’Académie
de Vienne. Ces déviations du géoide de l’ellipsoide terrestre, n’atteindront guere 100 mötres,
conformément à mes recherches antérieures, mais en général elles seront beaucoup moins grandes,

D’après le projet de M. Forrérer, la Conférence générale de l’Association géodésique
internationale avait confié en 1895 au Bureau central la tâche d'organiser sur un même
parallèle un service international des latitudes, et de déduire les résultats des observations
faites dans les différentes stations. Dans ce but, ainsi que nous l'avons déjà communiqué, une
somme assez forte de la caisse des dotations fut mise à la disposition du Bureau central.
Déjà en 1889 ce Bureau s'était préparé à ces travaux, lorsqu'il s’agissait de démontrer d’abord
qu’en réalité les latitudes sont variables. On a organisé alors des séries d'observations simul-
tanées à Berlin, à Potsdam, à Prague et à Strasbourg, et après en avoir étudié les résultats,
on a préparé ensuite une expédition à Honolulu avec des observations correspondantes à
Berlin. M. Apotex Marcusz, muni d’une lunette zénitale construite d’après le projet du
Bureau central, était chargé de l’expédition 4 Honolulu, tandis que M. Barrmrmann devait
observer à l’altazimut de l'Observatoire de Berlin. Ües premiers essais, ainsi que les séries
d'observations organisées à la suite de l’expédition à Honolulu par le Coast and Geodetic
Survey en trois stations, ont, comme on sait, conduit à la création du service international
des latitudes recommandé par M. FOERSTER.

D'abord on a, encore tâché de décider, au moyen de plusieurs séries d'observations,
s'il fallait employer la méthode visuelle ou bien la méthode photographique. En considé-
ration des ressources disponibles, on s’est décidé à employer la méthode visuelle qui dans la
pratique semblait offrir moins de difficultés que la méthode photographique *). Les résultats
ont démontré la justesse de nos prévisions.

1) F.R. Heımerr: „Die Schwerkraft und die Massenverteilung der Erde” (Enzyklopaedie der mathematischen
Wissenschaften VI, 1). — Sitzungsberichte der Berliner Akademie 1912, p. 308 et suivants.
2) Comptes rendus de la Conference generale de 1898, p. 247 et 248.

 

1

ä
=
ä
3
ä
a
1

il

LL LT

Jan. aba pa aa Ma A LL A ate |

es roma Et

 
