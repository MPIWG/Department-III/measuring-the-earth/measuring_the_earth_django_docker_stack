TEEN

87

3°, Ein Telegramm des Herrn BAcKLunD, mit der Nachricht dass er nicht kommen
kann; ferner einen Brief desselben Kollegen, von anderer Hand geschrieben,
jedoch von ihm selbst unterzeichnet, die Nachricht enthaltend dass er vor
einigen.Wochen ernstlich erkrankte und sein Gesundheitszustand, wenn dieser
sich auch wesentlich gebessert hat, ihm die Reise noch nicht gestattet; es tut
ihm sehr leid dass er die in Hamburg versammelten Kollegen nicht sehen
kann, besonders da er einige wichtige Mitteilungen zu machen hätte;

4°, Briefe einiger zu unseren Sitzungen geladenen Herren, unserer früheren Kollegen,
General v. STUBENDORFF und General ARTAMONOFF, ferner Prinz GALITZINE,
Präsident und Prof. von KövksLieErHy, Sekretär der Vereinigung für Erdbeben-
kunde, welche allen zu ihrem Bedauern, verhindert sind den Sitzungen bei-
zuwohnen;

5°, Ein Telegramm des Herren Prof. v. KövssLiegErky, mit den besten Wünschen
für den guten Erfolg unserer Arbeiten.

Der Sekretär schlägt vor, den Herren Darwin und Backlund in Telegrammen unser
Bedauern wegen ihrer Abwesenheit und unsere besten Wünsche für ihre Genesung zum
Ausdruck zu bringen.

Dieser Vorschlag mit Sympathie begrüsst, wird einstimmig angenommen,

Der Präsident erteilt dem ständigen Sekretär das Wort zur Verlesung seines geschäft-
lichen Berichts über die 3 letzten Jahren 1909-1912.

Meine Herren,

In meinem in der ersten Sitzung vorgetragenen Berichte habe ich versucht einen
geschichtlichen Überblick von dem Entstehen und der Entwicklung unserer Erdmessung
während der letzten fünfzig Jahre zu geben; jetzt ist meine Aufgabe bescheidener, da ich
nur einen geschäftlichen Bericht über die drei letzten Jahre, seit unserer Konferenz in
London und Cambridge, vorzubringen habe,

Ich muss Ihnen zuerst Bericht erstatten über die Änderungen in dem Verzeichnis
unserer Delegierten. Mit Freude lesen wir darin die Namen neuer Kollegen, welche mit uns
die Interessen unserer Assoziation fördern werden, aber leider vermissen ‘wir auch die Namen
verschiedener Kollegen, und darunter die der ausgezeichnetsten, welche uns durch den Tod
entrissen sind.

Am 21. Dezember 1909 verlor die Französische Kommission ihr ältestes Mitglied,
Herrn Bovquar DE LA Grye. Geboren am 29. Mai 1827, studierte er an die Ecole poly-
technique und trat dann in den bydrographischen Dienst der Marine, welcher ihm während
einer längeren Jabresreihe höchst verdienstvolle Arbeiten sowohl in Frankreich als auch in
anderen Weltteilen zu verdanken hatte. Er beschäftigte sich nicht ausschliesslich mit der
eigentlichen Hydrographie, sondern auch mit Erdbebenkunde, Pendelbeobachtungen, und
besonders mit den Gezeiten, welchen er verschiedene Abhandlungen widmete,

 
