 

58

de premier ordre, et 2° pour présenter & la prochaine Conférence un rapport sur cette
question. 2

M. le Président fait observer que cette commission devra s’occuper non seulement
de la précision des mesures angulaires, mais aussi des bases, comment elles doivent étre
mesurées et quelle sera leur distance, les grandeurs et les formes des triangles, les déter-
minations astronomiques aux différentes stations etc. Il propose ensuite que cette Com-
mission soit composée comme suit:

MM. Tirrmann,
TURNER,
LEYVA, :
POMERANTZEFF,
HELMERT,
LAUNHARDT,
BOURGEOIS,
SUGIYAMA,
CELORIA.

Cette proposition est adoptée.

M. le Président donne la parole à M. Gautier qui lit son rapport sur les travaux
exécutés en Suisse et indique sommairement une construction un peu modifiée du pendule
de Séerneck. (Noir Annexe A. XX XVIIT.

M. le Président donne la parole 4 M. Mier qui présente son rapport sur les travaux
exécutés en Espagne. (Voir Annexe À. XXXIX).

Après avoir remercié M. Mier, M. le Président donne la parole à M. Æütvüs qui
présente un rapport intéressant sur les observations faites avec le pendule à torsion dans
différentes parties de la Hongrie. (Voir Annexe A. XL).

Après avoir remercié M. Bötvös, M. le Président demande si le Secrétaire ou l’un des
délégués a reçu un rapport sur les travaux faits au Portugal et en Roumanie.

Personne n’a reçu ces deux rapports.

M. le Président dit que nous avons terminé les rapports des différents États et que,
dans la séance de Vendredi, on présentera et discutera les rapports des difiérentes commissions.
Il fait savoir ensuite que la Commission des latitudes se réunira aujourd’hui à 8 heures
et la Commission des finances, à 5 heures, dans une des salles de l'Hôtel Atlantic.

La séance est levée à 1 heure 5 min.

a

a Mala |

Da CNT TE

tabou

|

A tonal nano Midland path 9

 

 
