ii

tee TTT TTT

!
¥
Ë

13

sique internationale compte 24 États. Vous en êtes, Messieurs, les dignes représentants: vous

vous unirez à moi pour saluer la mémoire de l’illustre fondateur de notre Institution.
Mais notre reconnaissance, Messieurs, ne doit par s'arrêter au Général BAEvER:

s’il fût le premier ouvrier, d’autres survinrent qui eurent à honneur de tenir haut et

ferme le drapeau de la science géodésique. Voyez ce qu'ont fait les savants qui, tout

d'abord, ont figuré dans la Commission permanente, qui se réunissait tous les ans, —

_et c'était une nécessité dans les débuts de l’Association, — mesurez les progrès réalisés

dans les méthodes d’observation, dans les instruments, dans les calculs, et comparez les œuvres
modernes, comme précision, avec celles qui étaient exécutées auparavant. Voilà le grand
service rendu à la science par l’Association géodésique internationale pendant son premier
cycle de 90 ans.

Elle en a produit un autre, et bien précieux: celui de provoquer l’émulation entre
les offices géodésiques des différentes nations pour apporter à l'œuvre commune des résultats
de plus en plus précis.

Kt n’est-ce pas la, Messieurs, une rivalité vraiment bienfaisante pour la science, que
celle qui nous anime tous aujourd’hui dans nos efforts?

Déjà de grandes opérations géodésiques ont été conçues, exécutées ou sont en cours
d'exécution, qui n'auraient peut-être pas été entreprises sans le besoin qui nous domine
tous dans l’Association de travailler toujours dans l'intérêt commun. Ces œuvres modernes,
c'est la nouvelle méridienne de France, c’est la jonction géodésique de l'Espagne avec
l'Algérie, la méridienne de Laghouat prolongée en plein désert, c’est le nouvel arc Russo-
Suédois au Spitsberg, le nouvel arc de Quito, puis l'immense triangulation du 100° méridien
aux États-Unis qui se prolongera un jour dans l'Amérique du Sud jusqu’au cap Horn, et
enfin le grand are commencé en Afrique qui part du Cap pour aboutir à Alexandrie et ira
ensuite se souder à l’arc Russe.

Ainsi se trouvent en voie de réalisation les vastes projets que faisait déjà entrevoir,
il y a 25 ans, en 1887, a la session de Nice, notre ancien et vénéré Secrétaire perpétuel
M. Hirscx, quand le délégué du Portugal, le Général Marquis D’Avira, annonça l’exécution
d’une triangulation de précision le long de la côte occidentale de l’Afrique. , Cette nouvelle,
»dit M. Hirscu, si l’exemple précieux du Gouvernement portugais est suivi par les Gouver-
»nements des autres nations ayant des possessions sur le continent noir, cette nouvelle fait
„entrevoir que les générations suivantes de géodésiens verront un jour se réaliser le grand
»fait de la jonction entre Greenwich et le cap de Bonne Espérance, c’est-à-dire de la mesure
»de méridiens embrassant les deux hémisphères, puisque déjà la Méditerranée, se trouve
»traversée par des triangles entre l'Espagne et l'Algérie”.

Et AM. Hirson ajoutait: ,Quel magnifique développement de l’entreprise géodésique
,congue il y a 25 ans par le Général Baxyer, limitée à l’origine dans l’Europe centrale,
„et dont on entrevoit aujourd’hui l’extension de la région polaire du nord à travers l’Équateur
„jusqu’au Cap. C’est alors qu'on connaîtra avec la dernière précision la forme et les dimen-
»Sions du globe terrestre”.

Messieurs, j'ai évoqué cette prophétie pour vous montrer le chemin que nous avons

 
