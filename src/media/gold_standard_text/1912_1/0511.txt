 

IT

A A ROUT A HM WALLA HNC BL 1
Seni TONNE, ST

mi

ANNEXE A, XKXVIII.

SUISSE. .

Rapport sur les travaux exécutés en Suisse, depuis la 16° Conférence
de Londres— Cambridge.

PAR

R. GAUTIER.

Dans cette année 1912 où l'Association géodésique internationale célèbre le cinquan-
tenaire de sa fondation sur l'initiative prise par le général BAEYER, il me sera permis de
rappeler, au début de ce rapport, que la Commission géodésique suisse célèbre aussi son cin-
quantenaire. Nommée en 1861 à la réunion de Lausanne de la Société helvétique des sciences
naturelles, elle a eu sa première séance le 11 avril 1862 à l'Observatoire de Neuchâtel. Y
assistaient: Rupozrx Wozr, directeur de l'Observatoire de Zurich, président, le Général Durour,
ADOLPHE Hirscu, directeur de l'Observatoire de Neuchâtel, et l'ingénieur Denzrer. Enre
Rirrer, de Genève, qui en faisait également partie, était mort peu avant cette séance et y
fut remplacé par Emine Puanramour, directeur de l’Observatoire de Genève. Aucun des
membres fondateurs de la Commission géodésique suisse, n’assiste malheureusement au
cinquantenaire de la Commission et de l'Association géodésiques.

Je suis heureux de constater que, depuis 1908, la Commission géodésique suisse n’a
perdu aucun de ses membres; et, tout récemment, elle a acquis un collaborateur nouveau
dans la personne de M. F. BAgsCuLIN, professeur de géodésie à l’École polytechnique fédérale
à Zurich, l’élève et le successeur du regretté RoSENMuND.

Le dernier rapport que j’ai eu l'honneur de présenter à la 16e Conférence de l’Asso-
ciation géodésique internationale, à Londres, rendait compte, d'une façon abrégée des princi-
paux travaux exécutés en Suisse de 1906 à 1909. Ce rapport-ci sera consacré à un exposé
sommaire des travaux qui ont suivi, dans les années de 1909 à 1912.

Les Procès-verbaux des séances de la Commission géodésique suisse des 80 avril 1910
et 6 mai 1911, qui ont été déjà distribués, et celui de la dernière séance du 4 mai 1912, qui
vient d’être remis aux participants à cette Conférence, vous ont renseignés sur nos travaux.
Vous avez aussi reçu les volumes XII et XIII de nos publications, Travaux astronomiques

 
