 

 

282

lunette, au moyen d’un interpolateur graphique. On n'a donc pas besoin de connaître la
valeur d’une division de l’échelle du tube.

Jusqu'au mois de juillet dernier, on avait nivellé en double 495 km au nord et
323 km au sud, soit 818 km au total, dont 110 km pour la jonction de la base de Pintados
au marégraphe d’Iquique et 165 km pour la jonction de Chacayal au médimarémètre de
Talcahuano.

Les opérations du nivellement ont donné dans le nord 875 cotes, avec 53 repères
métalliques et 277 repères naturels. Dans le sud le nombre des cotes est de 497, dont 58
correspondent à des repères métalliques et 216 à des points spéciaux.

Pour les 495 km du nord on a fait 11978 stations avec l'instrument à niveller et
521 comparaisons pour chacune des deux mires avec le mètre en acier. Au sud, pour les
323 km, on a 6865 stations et 287 comparaisons.

Quelques parties du nivellement ont été executées sur des chemins de fer de 3'/,°%o
de pente et avec un trafic intense.

L’erreur moyenne kilométrique au nord a varié, pour les segments du nivellement,
entre + 0,99 mm et + 1,67 mm, le maximum correspond au nivellement de précision qui
traverse le champ de sel de Pintados (Planche II), exécuté pour obtenir la jonetion de la
base avec le nivellement general.

Au sud, l’erreur moyenne kilométrique des différents segments oscille entre + 0,66 mm
ey © 134 mm,

On a cru qu’il serait plus raisonnable d’attendre, avec la continuation du nivellement
du nord vers le sud, jusqu'à ce que la construction du chemin de fer longitudinal soit
achevée, parce qu’a présent l’absence d’eau dans une région pleine de champs de sel et de
sable fortement chauffé pendant le jour, fait monter beaucoup le prix du kilomètre de
travail et cause au personnel une fatigue inutile.

TRAVAUX DE LA ,Secrion TriconomérriQue DE L'Erar MAJOR DE L'ARMÉE.

Depuis la date de la Conférence de 1909, la Section Trigonométrique s’est pré-
occupée de faire quelques révisions dans les stations de la ,Gran Red Central” et dans
le réseau de la base. On a travaillé aussi à l'étude du prolongement de la triangulation
vers le sud.

Les caleuls de compensation des stations et de la compensation generale de la „Gran
Red Central” ont été achevés par M. le Directeur Col. Farıx DEINERT, qui presentera sur
ce sujet un rapport special.

Le reseau de premier ordre de l'État Major, nommé ,Gran Red Central”, est compris
entre les paralléles de 32° 22' et de 33° 42’, au sud de l’équateur, et les méridiens de 70° 26°
et 71° 38’ à l’ouest de Greenwich. Il comprend 24 points trigonométriques où l’on a observé
106 directions avec un instrument universel coudé de huit pouces de BAMBERG.

Pour la compensation générale on a obtenu 38 équations de condition, parmi les-
quelles 10 sont des équations de côtés, il en resulte + 0,9 comme erreur moyenne d’une

 

 

3

shui.

hdd ath aah

Mbuidis

418841111808

tet se

Huit

ja ma bn a Ma ou aan à
