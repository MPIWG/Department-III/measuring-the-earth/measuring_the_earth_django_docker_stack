Ser

AL LU DUR
D PONT pr

ANNEXE A. I,

RAPPORT

sur les travaux du Bureau central pendant les 50 premières années
de l’Association géodésique internationale.

Messieurs,

Aujourd’hui, que nous avons le bonheur de commémorer avec satisfaction le grand
développement de l’Association géodésique internationale, vous voudrez bien me permettre
de vous présenter un résumé de l’activité du Bureau central.

La première Conférence générale, en 1864, à Berlin, prit la résolution d'établir, à
côté de la Commission permanente, un Bureau central, et de confier la direction de ce
Bureau à M. le Lieut.-Général Dr. Jomann JacoB Bazyer, fondateur de l’Association pour
la mesure des degrés dans l’Europe centrale, qui, quoique déjà bien âgé, — il était né le
5 Novembre 1794, — était encore plein de vigueur. Un des travaux les plus importants dont
on avait l'intention de charger le Bureau central était la publication annuelle d’un rapport
général sur les opérations géodésiques. M. le Général Bazyer avait déjà rédigé deux rapports
pour les années 1862 et 1863, dans lesquels il avait donné un aperçu des travaux géodésiques
accomplis et des projets qu’on avait formés pour les continuer dans l’Europe centrale depuis
la Suède et la Norvège au nord jusqu’à l'Italie au sud.

Comme la Commission permanente n’avait pas à sa disposition de ressources finan-
cières, le Gouvernement de Prusse se chargea de la création de ce Bureau, qui fut inauguré
le ler Avril 1866. Dans les premières années jusqu’en 1868, nous rencontrons parmi les
membres du Bureau central le plus âgé de nos délégués, l’ancien Directeur de l'Observatoire
de Berlin, M. W. Forrstsr, qui a tant fait pour notre Association, comme le disait tout à
l'heure M. v. D. Sanne BAKHUYZEN, et que nous avons le bonheur de voir parmi nous.
Depuis l’année 1864 il était déjà le collaborateur de M. le Général Bazyer.

Un autre membre du Bureau central, qui y. est entré dès le commencement et
qui est encore en pleine activité, est M. le Prof. ALBRECHT que vous connaissez tous. Déjà
au mois de Février 1866 il travaillait au Bureau sous la direction du Général Banyzr et

le ler Mai, il fut nommé assistant. Par de nombreux travaux il à beaucoup contribué au
succès de nos opérations géodésiques, mais c’est surtout dans les dernières années qu’il s’est

 
