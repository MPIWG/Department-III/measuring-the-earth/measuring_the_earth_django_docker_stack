BEILAGE A, XXX.

BERICHT

über die in den Jahren 1909-1911 in Norwegen ausgeführten
geodätischen Arbeiten

VON

Herrn Oberst KNOFF.

1) Mit 2 Invardrähten hat „Norges geografiske opmaaling’”’ (Die Landesaufnahme
Norwegens) auf der Insel Vigra bei Aalesund eine alte Grundlinie, die früher mit Metall-
drähten nach JÄDErIns ursprünglichen Prinecip gemessen worden war, nachmessen lassen.
Die Ergebnisse der neuen Messung müssen — die Schwierigkeiten des Geländes in Betracht
gezogen — als befriedigend angesehen werden.

2) „Norges geografiske opmaaling’” hat im nördlichen Norwegen den letzten Abschnitt
der Dreieckskette 1. Ordnung der Küste entlang bis zur russisch-skandinavischen Gradmes-
sungskette beobachtet. Bei der Beobachtung wurde die Winkelmessung in allen Kombinationen
nach SCHREIBER zum erstenmal in Norwegen angewandt. Die Gewichtszahl wurde doch von
24 auf 16 herabgesetzt. Die Ergebnisse des Anschlusses an die Gradmessungskette sind dem
Berichterstatter über die Triangulationen übersandt worden.

Die Dreieckskette durch das Tromsö Amt wird in kommenden Jahren nach Osten
und Süden ausgedehnt werden. Mehrere Anschlusspunkte mit der Gradmessungskette werden
dann vorliegen.

Im südlichen Norwegen sind die Beobachtungen im zweiten Abschnitt des Netzes
quer über das Land von Kristiania bis Bergen abgeschlossen; die Berechnungen sind aber
noch nicht fertig. In dem westlichsten und dritten Abschnitt dieses Netzes sind die Vor-
arbeiten ungefähr abgeschlossen und die Beobachtungen angefangen. In beiden Abschnitten
wurde nach ScHreisers Methode beobachtet, beziehungsweise mit Gewichtszahlen 16 und 24.
Ausserdem ist die Recognoscierung eines Netzes im Anschluss am und nördlich vom obener-
wähnten Netze angefangen.

In den trigonometrischen Punkten sind sowohl im nördlichen als im südlichen Nor-
wegen materielle Signale angewandt worden; in den höchsten Punkten ein Prisma oder Oylinder

 

ANT ART

TNT

TORTI OWT TIT ww

eR

45
LE
if
ie

=

 
