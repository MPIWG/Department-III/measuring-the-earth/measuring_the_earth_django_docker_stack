 

BEILAGE A. XXXYII.

SCHWEDEN.

Bericht tiber die Wasserstandsbeobachtungen.

VON

C. G. FINEMAN.

Schweden hat 9 Stationen für Wasserstandsbeobachtungen, die mit automatisch-
registrierenden Instrumenten versehen sind, und 6 Stationen, wo direkte Beobachtungen
angestellt werden, längs der Küste des Landes verteilt, wozu 6 Pegelstationen im Binnensee
Mälaren kommen. Die geographischen Koordinaten der Stationen sind Sir GEORGE DARWIN
brieflich mitgeteilt und wahrscheinlich in seinem Berichte aufgenommen worden,

Eine für den Bottenwiek, d. h. nördlich von der Meerenge Kvarken, geplante Ma-
reografenstation ist noch nicht gebaut worden.

Die neun Mareografenstationen des Landes sind während der letzten drei Jahren
ohne wesentlichen Störungen in Betrieb erhalten. Am Insel Nord-Koster habe ich direkte
Stundenbeobachtungen an einem festen Pegel während 16 Monate ausführen lassen und sind
diese nachher durch tägliche Beobachtungen 8 a.m., 12, 4 p.m. und 8 p.m. fortgesetzt
worden. An den übrigen Pegelstationen wurden die Beobachtungen wie früher fortgesetzt.

Stundenbeobachtungen für die Jahre 1907—1908 sind in einer besonderen Publica-
tion veröffentlicht worden, die in der nächsten Zeit zur Verteilung gelangen wird. Eine bei
der Ablesung der Mareogramme gefundene, von der Konstruktion der benutzten Registrier-
apparate herrührende periodische Deformirung der Tageskurven ist durch eine geeignete Ver-
besserung eliminiert worden.

Einige neue Bestimmungen der Hebung der Westküste Schwedens sind in der. letzten
Zeit gemacht worden und werden, sobald die übrigen zu findenden älteren Wasserstandsmarken
an den westlichen und südlichen Küsten Schwedens so weit wie möglich neu einnivelliert
worden sind, in einer besonderen Publication zusammengefasst werden. _

Hamburg, 23/9 1912.

verbinde hrns äh nu

 

i
4
i
i
i
;
1
j

 
