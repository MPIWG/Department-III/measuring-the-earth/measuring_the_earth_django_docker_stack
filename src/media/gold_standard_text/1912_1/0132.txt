 

126

Diesem Wunsche wird man Folge leisten (Beiläge B. XV).

Der Präsident erwähnt, dass nach der Tagesordnung jetzt Vorschläge für den Ort der
nächsten Konferenz vorgebracht werden müssen. Wir haben nicht abzustimmen, denn es liegt
dem Präsidium ob, den Beschluss zu fassen.

Herr General Pomerantzef’ schlägt im Namen der russischen Regierung vor, die nächste
Konferenz der internationalen Erdmessung in St. Petersburg abzuhalten.

Herr Knof ladet die Delegierten ein, sich im Jahre 1915 in Christiania zu ver-
sammeln; sie können eines herzlichen Empfangs versichert sein,

Der Präsident teilt mit, dass das Präsidium zu einer geeigneten Zeit den von ihm ge-
wählten Ort zur Kenntnis der Delegierten bringen werde.

Der Präsident erklärt, dass wir jetzt am Schluss unserer Konferenz angelangt sind,
und er spricht der Stadt Hamburg und Herrn Senator von Melle seinen wärmsten Dank
aus für den Empfang, der uns in der schönen Stadt Hamburg bereitet worden ist, er hofft,
dass die Herren Delegierten sich diesem Danke auschliessen werden, der in unserem Protokoll
wird aufgenonnmen werden (Allgemeiner Beifall).

Der Prässdent hat noch Herrn Schorr herzlich zu danken, der sich während aller unserer
schönen Reisen und Ausflüge in bewunderenswerter Weise bemüht hat, uns zu helfen und uns
zur Seite zu stehen; aber besonders bringen wir unseren Dank dem Kameraden, dem liebens-
würdigen immer zum Dienste bereiten Kollegen, dessen Erinnerung wir in unseren Herzen
aufbewahren werden.

Nachdem Herr Madsen dem Präsidenten und dem Sekretär für ihre Leitung gedankt
hat, erklärt der Prasident die 17. Generalkonferenz für geschlossen.

Die Sitzung wird 11 Uhr aufgehoben.

 

<r

-

se
4
3:
ae
=
35
32
a
i
3
35
a
i

er

Aa,
rune

wi

à nadia nu Halala nat ada nn +

i
i
|
|

 
