TROISIÈME SÉANCE

Samedi, 21 Septembre.

Salle du »Vorlesungssebäude”.

Présidence du Général Bassot.

La séance est ouverte à 9 heures et quart.

Sont présents:

les délégués, Jeanne, Deinert, Greve, Madsen, Johansen, Petersen, Sand, Foerster, Albrecht,
Bauschinger, Borrass, Finsterwalder, Galle, Haid, Hecker, Helmert, v. Knauer, Kohlschütter,
Launhardt, Schmidt, Schorr, Schweydar, Seiht, v. Wegerer, Weidner, Bassot, Baillaud, Bourgeois,
Hanusse, A. Lallemand, Ch. Lallemand, Messala, Turner, Wade, Celoria, Reina, Hirayama,
Sugiyama, Leyva, Bakhuyzen, Heuvelink, Muller, Knoff, Weiss, Schumann, Pomerantzef’, Rosén,
Fineman, Gautier, Mier, v. Eötvös, Tittmann, Bowie ;

les invités: Jäderin, Kovatchef, Wild, de la Baume Pluvinel, Dengel, Dolberg, Graf,
Gurlitt, Klasing, O. Repsold, Schwassmann.

Le procès-verbal de la deuxième séance est lu et adopté.

M. le Secrétaire communique le contenu de deux lettres qu’il a reçues, l’une de notre
ancien Collégue M. Hayford qui regrette de ne pouvoir venir à Hambourg pour assister à
nos séances auxquelles il avait été invité, l’autre de M. Celoria, annonçant qu'il a regu une
dépêche de M. le Général Gliumas qui, à cause de sa santé, est empêché de venir à la Confé-
rence et présente ses hommages au Bureau et à ses collègues.

M. le Secrétaire annonce en outre que M. Foerster a mis encore à la disposition des
délégués un certain nombre d'exemplaires de la note de MM. Benoit et Guillaume dont il à
parlé dans la dernière séance.

M. le Président donne la parole à M. le Colonel Bourgeois pour la lecture de son

rapport sur les bases.

 
