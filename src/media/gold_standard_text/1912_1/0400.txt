 

344

 

7. Dis UNTERSUCHUNG ÜBER DIE LÄNGENVERÄNDERUNG DER INVARDRAHTE.

Die Längen der Invardrähte sind, wie oben gesagt, mit den Comparator-Basen sorg-
fältig geprüft worden. Die Drähte waren in Tokyo auf eine Trommel gewickelt aufbewahrt,
und während der Basismessung (Nov.-Dez.) waren sie an einer Wand aufgehängt. Bei der
Vergleichung mit der Comparator-Basis haben wir die Teilung der Drahtskalen mit dem
Rapsonv’schen Mikroskope abgelesen. Nach der Art und Weise der Spannung und Hand-
habung zeigte jeder Draht kleine Veränderungen der Länge, z.B. wenn der Draht mit den
10 kg Gewichten gespannt mehr als 10—15 Minuten hängt, so verkürzt er seine Länge
merkwürdigerweise um 0.04—0.08 mm gegen die Länge, die er hatte, als er gleich nach
der Belastung gemessen wurde. Damit erfuhren wir, dass bei der Etalonnierung des Drahtes
es notwendig ist, die Skalen der beiden Enden des Drahtes so schnell wie bei der Feld-
messung abzulesen.

Nachstehend sind die Ergebnisse der Etalonnierung der Drähte:

 

Länge etalonniert
O

Länge etalonniert

 

Datum | Diff.
|

 

 

 

 

—0.37 Juli 1905 + 0.17

Apparal in Sagamino Datum in Sevres |
ee ee
No | 22 D 0 | ov 1000 | 22-045. | Den 1009 | + 0.07
308 —} (06 5 - > + 0.07
| 0:10

394 — 0.60 : — 0.70
56 —0.20 » |

M. SUGIYAMA.

 

i
1
i
i
3
i
ii
ii
i

uaa +

ÉTTIE TT

CT TT

Tee

 
