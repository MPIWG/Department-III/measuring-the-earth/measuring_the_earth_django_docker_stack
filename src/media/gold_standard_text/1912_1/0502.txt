 

408

Obgleich diese Abweichungen grösser ausgefallen waren als wünschenswert war, hegte
ich doch bei Wiederaufnahme der Frage im Jahre 1906 die Hoffnung, dass eine mit grösster
Sorgfalt ausgeführte Revision und Neuberechnung die vorläufig berechneten Abweichungen
möglich zu einem befriedigen Betrag bringen sollten. Denn teils war die Form der Dreiecke
in der Regel gut, teils der allgemeine mittlere Fehler eines Winkels nach der internationalen
Formel — + 1",2 gefunden, wozu noch kam, dass die Resultate der Basismessungen bei
Kopenhagen und Laholm sich ziemlich gut im Netze angeschlossen hatten,

Ich erlaube mich jetzt eine kurze Übersicht der Inhalt meiner Abhandlung —
Meridiangradmätning vid Sveriges västra kust — zu geben und dabei besonders die Aufmerk-
samkeit zu richten auf Punkte, welche von grösserem Interesse sein können.

Diese Gradmessung streckt sich längs der westlichen Küste Schwedens in einer eirca
380 km langen Kette mit Anfang von der über Oeresund gehenden Dreiecksseite Malmö—
Kopenhagen bis an diejenige Seite, welche die beiden in der Nähe von dem Meerbusen
Svinesund gelegenen Punkte Wagnarberg und Dragonkullen vereinigen.

Die geodätischen und astronomischen Arbeiten im Felde sind folgende: 1. Messung
einer Basis in der Nähe von der Stadt Laholm in der Landschaft Halland; 2. Horizontal-
winkelmessungen an 46 Beobachtungsstationen; 3. astronomische Bestimmungen von Breite
und Azimut an drei Punkten — Himmelskullen, Göteborg und Marstrand —, wozu noch
kommt eine Längenbestimmung von Göteborg. Diese Arbeiten sind von schwedischen Astro-
nomen und Geodäten ausgeführt.

Die Längen der Grenzseiten der Nachbarländer stützen sich resp. auf die in 1838
gemessene Grundlinie bei Amager (in der Nähe von Kopenhagen) und auf die 1864 gemessene
Grundlinie bei Egeberg (in der Nähe von Kristiania).

Von Professor Frarnty, in Kristiania, sind Bestimmungen der Breite und des Azimuts
von dem nördlichsten Punkte des Netzes Dragonkullen in den Jahren 1877— 78 gemacht worden.
Es war zwar wünschenswert gewesen ähnliche Bestimmungen auch von dem südlichsten Punkte
des Netzes Malmö zu bekommen; dies aber war ganz ausgeschlossen, weil die dazu nölige
Stationspunkte von Malmö, Kopenhagen oder Dagstorp nicht mehr zu finden waren.

Für Kopenhagen waren zwar ältere Bestimmungen von Breite, aber keine von Azimut
vorhanden.

Es war nicht zu erwarten, dass die geodätischen Winkelmessungen mit den dazu
gehörenden Operationen welche sich über einen langen Zeitraum von mehreren Dezennien
erstrecken, von grossen Zwischenzeiten unterbrochen und von verschiedenen Beobachtern mit
verschiedenen Instrumenten ausgeführt, ein völlig homogenes Beobachtungsmaterial zur Ver-
fügung stellen sollten. Es war deshalb sehr schwer und nicht zu rathen einen strengen Wert
der Genauigkeit der Horizontalwinkel zu bestimmen, sondern man musste sich mit einem
Näherungswert begnügen. Wenn man in Betracht zieht, dass die geschlossenen Dreiecke
durch ihre grosse Zahl die Hauptrolle bei den Winkeln in der Kette spielen, konnte der
gesuchte Wert des mittleren Fehlers am bequemsten durch die Anwendung der internatio-
nalen Formel ermittelt werden.

Für die Gruppen von geschlossenen Dreiecken zwischen der Basis bei Laholm und

tend iat

 

|
i
;
i
i
}
i
i
j

 
