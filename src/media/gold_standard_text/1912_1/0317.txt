ns

AL rer

il

TAF 1

ait id

(TR LI

ANNEXE A. XVII.

RAPPORT

sur les Travaux du Service du Nivellement Général de la France,
de 1909 a 1912,

M. Ch. LALLEMAND,
Membre de l’Institut de France,

DIRECTEUR DU SERVICE,

I. — EXECUTION DE NIVELLEMENTS.

(Voir la carte ci-annexée).

A. — ÜHEMINEMENTS NOUVEAUX.

De 1909 à 1912, on a exécuté environ 12.400 kilomètres de nivellements géomé-
triques, savoir :

1°, — 5.800 kilomètres de nivellements de 3ème ordre, dans le Midi de la France;

2°. — 6.600 kilomètres de nivellements de 4ème ordre, dans diverses régions.

Au 31 Décembre 1912, la situation générale des travaux du Nivellement général de
la France sera la suivante:

Opérations effectuées avant 1884 et non réitérées depuis:

Longueurs
Nivellements divers. 4 0. 971998 (6 7 74,000 dam,
Nivellement de: Bourdalouë . . : 2.0 22.212000 5

Ensemble 15.000 km.

 
