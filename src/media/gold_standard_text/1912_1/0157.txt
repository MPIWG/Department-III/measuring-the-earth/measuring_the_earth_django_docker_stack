TORTE

a ee

CCE

ne

en

Zusammenstellung der Literatur der Gradmessungs-Arbeiten. Herausgeben von dem
Zentralbureau der Europäischen Gradmessung. Berlin (P. Stankiewicz) 1876.
32 Seiten in &°. (Vorwort von J. J. Baeyer.)

Register der Protokolle, Verhandlungen und Generalberichte für die Europäische
Gradmessung vom Jahre 1861 bis zum Jahre 1880. Bearbeitet von Prof. Dr.
M. Sadebeck. Berlin (P. Stankiewicz) 1883. 81 Seiten in 4°.

Ferner als Manuskript gedruckt:

Wissenschaftliche Begründung der Rechnungsmethoden des Zentralbureaus der Euwropä-
ischen Gradmessung. 1.—3. Heft. Berlin 1869—1871. (Von J. J. Baeyer.)

1. Heft: I. Die Methode der kleinsten Quadrate. II. Die Anwendung der-
selben auf die Geodäsie. 73 Seiten in 4°.

2. , : III Allgemeine Auflösung der sphäroidischen Dreiecke. IV. Reduktion
der Winkel eines sphäroidischen Dreiecks auf die eines ebenen
oder sphärischen von gleichen Seiten. (Von J. Weingarten.)
76 Seiten in 4°. :

3. , : Ausgleichung eines geometrischen Nivellements (Erweiterung des § 9
im II Abschnitt). — Geometrisches Nivellement über ungangbare
Strecken. — Tafeln zur Verwandlung von Bögen in Tangenten und
umgekehrt. (Von ©. Bremiker.) 22 Seiten in 4°.

Ausgleichung eines Dreiecksnetzes nach der Bessel’schen Methode und Aufklärung
einiger gegen dieselbe erhobenen Bedenken. Berlin. 17 Seiten in 4°. (Von
J. J. Baeyer.)

Ueber die Verteilung der Fehler nach Winkelgleichungen und nach Seitengleichungen.
Berlin 1871. 3 Seiten in 4°. (Von J. J. Baeyer.)

Vergleichung einiger Hauptdreiecksketten der Königlichen Landestriangulation mit
der bessel’schen Methode. Ein Beitrag zur Verteidigung des Geodätischen Institutes
gegen die in der Sitzung des Abgeordnetenhauses vom 18. Januar 1879 gegen
dasselbe gerichteten Angriffe. Berlin 1879. 12 Seiten in 4°. (Von J. J. Baeyer.)

Ueber die Nivellements-Arbeiten. im Preußischen Staate und die Darstellung ihrer
Resultate in richtigen Meereshöhen. Berlin 1881. 8 Seiten in 4°. (Von J. J.
Baeyer.)

Ueber die Höhenmessungen in Preußen und Berliner Normal-Null. Berlin 1884.
6 Seiten in 4°. (Von J. J. Baeyer.)

Ueber die Messung von Grundlinien. Berlin 1884. 6 Seiten in4°. (Von J. J. Baeyer.)

Verhandlungen - des Wissenschaftlichen Beirats des Königl. Geodätischen Imstitutes
zu Berlin im Jahre 1878. Berlin 1879. 48 Seiten in 4°.

Desel. um Jahre 1879. Berlin 1879 19 , „a.

im, Jahre 1880, Berlin 1880. 18 2 +

um Jahre 1881, Berlin lesl 8 5 à 4,

im Jahre 1562. Berlin 1882. 12. SER,

im Jahre 1883. Berlin 1883. 32° „2 und: le Tafel.

21

 
