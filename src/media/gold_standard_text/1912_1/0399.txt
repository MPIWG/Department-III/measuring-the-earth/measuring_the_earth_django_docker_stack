un,

A er

UNE

rt HI

Te

TE PRP

mir]

 

343

 

 

Datum. | Apparat. Ergebnis.
|

 

Sept. 1902| 5m in Eis eingehüllte Stahlstange 100 m + 35,918 mm.
Nov. 1910 | 5m Nickelstahlstange 100  —+- 55,889
> ss 4m Stahlstangen 100 + 35,517

Mit dem Repsonp’schen Apparate sind nur die beiden äusseren 25 m Comparator-
Strecken gemessen, da die mittleren Teile durch die Mikroskoppfähle für die Messung nicht
geeignet waren.

5. Dır Massungen per 600 m. Compararon-Bagis.

Die 600 m Comparator-Basis ist in zwei gleiche Teile geteilt worden. Jeder Teil ist mit
dem GurLLAuME’schen, Rgpsorp'schen und HiLéarp’schen Apparate je 4 mal gemessen, d.h.
zweimal bei steigender und zweimal bei fallender Temperatur in jedesmal umgekehrter Richtung.
Mit den Invardrähten ist die ganze Strecke 14 mal gemessen. Die Ergebnisse sind wie folgt:

 

 

Datum. Apparat Messung | Ergebnis
Nov. 1910 »m Nickelstahlstange 4 mal 599,979 759 m
» » 5 m Stahlstange 5; 599,979 302 9
A ss 4m Stahlstangen 4 239979
Noy.-Dez. 1910 | 25m Invardrahte 14 mal | 599,978 70

6. Diz ERGEBNISSE DER MESSUNG DER SAGAMINOER GRUNDLINIE.

Die ganze Strecke der Sagaminoer Grundlinie ist 8 mal mit vier Invardrähten
gemessen, d. h. je zweimal mit jedem Drahte in jedesmal umgekehrter Richtung. Die Er-
gebnisse sind:

Datum Draht Ergebnis
Nov. 1910 N°. 882 5210,045 64 m
Dez). „ 398 5210,040 65
Noy.-Dez. 1910 384 5210,043 44
Nov. 1910 56 5210,040 28

Mittel —= 5210,042 50
. Das 1882 mit dem Hıngarn’schen Apparate gemessene Ergebnis war 5210,0376 m.

1) Provisorisches Ergebnis, da die Temperaturkoéfficienten noch nicht sicher bestimmt sind.

 
