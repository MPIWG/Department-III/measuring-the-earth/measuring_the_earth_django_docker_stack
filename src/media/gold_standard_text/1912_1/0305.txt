ST TE UE

 

Mapa

TORRE PRIETO NT aT NT Tr IE

CW

 

275
avec Dakar; 8° un chronographe installé dans une baraque dans le fossé du Château, près
du pilier supportant l’astrolabe à prisme; 4° des communications électriques reliant au
chronographe le top du pilier d'observation, l'horloge fondamentale et l'horloge auxiliaire;
on pouvait ainsi inserire sur la bande du chronographe les battements de l’horloge fonda-
mentale et l'instant du passage des étoiles sur le cercle de hauteur de 60 degrés, ou simul-
tanément les battements des deux horloges fondamentale et auxiliaire pour leur comparaison.

À Dakar, la station astronomique fut installée dans les locaux de la marine.

Les opérateurs disposaient du câble de 9h. 1/2 à 10 heures du soir, pour l’échange
des signaux de comparaison.

Pour ces signaux, il fallait tenir compte de ce que le recorder n’a qu’une seule
plume et de ce qu'il ne permet pas l’enregistrement direct sur la bande de la station
d'émission du signal envoyé à la station de réception. Pour cela, on a supprimé à la station
d'émission tout enregistrement sur la bande du recorder, en substituant aux signaux émis
à des instants quelconques des signaux d’heure connue envoyés automatiquement grâce à
un dispositif spécial; c’était l'horloge auxiliaire qui lançait ces signaux à une heure convenue.

A la station de réception, le recorder et l'horloge auxiliaire du poste étaient mis en
série, dans le circuit d’une pile locale; la plume du recorder inscrivait alors sur la bande
les secondes de l’horloge auxiliaire et cette plume inscrivait en outre les signaux de com-
paraison venant du poste d'émission par le câble, signaux qui s’intercalaient entre ceux
correspondant aux secondes de l’horloge.

Les opérations d’une série complète comportaient:

1° Une première série d'observations d'étoiles à l’astrolabe à prisme, l'instant de la
coïncidence des images directe et réfléchie de chaque étoile s'inscrivant sur la bande du
chronographe au-dessous du tracé des battements de l’horloge fondamentale ;

2° Une première série de comparaisons locales des horloges fondamentale et auxiliaire,
faites l’une sur les secondes paires, l’autre sur les secondes impaires de l’horloge auxiliaire,
le tracé des battements de l'horloge auxiliaire s'inscrivant sur la bande du chronographe
au-dessous de celui des battements de l’horloge fondamentale.

3° Immédiatement après cette première série de comparaisons, l’échange des signaux
de comparaison entre les deux stations, par le câble sous-marin.

Pour ces signaux, la succession des échanges était la suivante:

A) En transmission à Brest et réception à Dakar. — W’horloge auxiliaire de Brest
envoyait à Dakar une série de 20 signaux, espacés chacun de quatre secondes; on connaissait
par conséquent à Brest l’heure exacte de l’envoi de ces signaux et le siphon-recorder de
Dakar inscrivait à la fois sur sa bande et à l’aide de la même plume les secondes de l'horloge
auxiliaire de Dakar et les signaux de 4 en 4 secondes émis par Brest et transmis par le câble.

B) En réception à Brest et transmission à Dakar. — L’horloge auxiliaire de Dakar
envoyait deux séries consécutives de 20 signaux espacés chacun de quatre secondes, Le

 

 
