 

76

bis Alexandrien fortgesetzt werden wird und sich dann dem russischen Meridianbogen
anschliessen kann.

So werden die grossen Pläne verwirklicht werden, welche unser früherer verehrter
ständiger Sekretär Prof. Hırsch schon vor 25 Jahren, 1887, bei der Versammlung in Nizza
in Gedanken erblickte, als der Delegierte Portugals, General Marquis D’Avira, die Aus-
führung einer genauen Dreiecksmessung die westliche Küste Afrikas entlang, erwähnte,
„Falls das wertvolle Beispiel”, sagt Herr Hırsch, „der portugiesischen Regierung von den
„anderen Regierungen befolgt wird, welche Kolonien längs den Küsten des schwarzen Kontinents
„besitzen, würde dies zur Folge haben dass die künftigen Geodäten eines Tages die Verbindung
„von Greenwich mit dem Kap der guten Hoffnung als Tatsache werden verzeichnen können,
„oder mit anderen Worten, dass ein Meridianbogen gemessen werden kann, der fast die beiden
„Halbkugeln umschliesst; denn man darf nicht vergessen dass schon jetzt das Mittelländische
„Meer von Dreiecksketten überspannt wird zwischen Spanien und Algier”.

Und Herr Hırsca fügt hinzu „Welche prächtige Entwicklung des geodätischen Unter-
„nehmens, welches General Baryer vor 25 Jahren ins Leben gerufen hat, indem er es erst
„auf Zentral-Europa beschränkte und welches nun von den Nord-Polarregionen über den
„Equator hinaus bis nahe zum Kap, sich auszudehnen im Begriffe ist”.

Meine Herren, ich habe diese Prophezeiung erwähnt um auf den schon zurückgelegten
Weg, und auf die Fortschritte, welche wir jetzt schon gemacht haben, hinzuweisen, welche
bei dem Fortschreiten auf unserem Weg immer bedeutender werden sollen.

Jetzt, nach einer Arbeit eines halben Jahrhunderts, dürfen wir mit Recht stolz auf die
von unserer Erdmessung erzielten Resultate hinblicken. Aber uns liegt auch die Pflicht ob,
welche wir gerne erfüllen, unsere Vorgänger zu ehren und vor allen den Stifter unserer
Assoziation General BABYER,

Der Präsident erteilt Herrn van de Sande Bakhuyzen das Wort, der folgenden Bericht
über den Ursprung und Entwicklung der Erdmessung von 1862 bis 1912 verliest.

Meine Herren,

Nach der schönen Jubiläumsrede unseres Präsidenten, sei es Ihrem Sekretär gestattet
in einem kurzen Bericht Ihnen das Entstehen und die Entwicklung unserer Assoziation
vor Augen zu führen.

Den älteren Herren unter Ihnen wird wohl manches davon bekannt sein, aber in
50 Jahren hat sich vieles geändert, und unter unseren jetzigen Delegierten findet sich nur
ein einziger, der vom Anfange ab an der Entwicklung der Gradmessung teilgenommen hat.
Ich hoffe also, dass es für viele nicht ganz uninteressant sein wird zu hören, in welcher
Weise wir zu unserer jetzigen Organisation gekommen sind; vielleicht kann solch eine
Darstellung auch von allgemeinerem Interesse sein, da unter den internationalen wissenschaft-
lichen Assoziationen die Erdmessung eine der ältesten war, und daher den späteren als
Beispiel diente,

 

3
3

side

18 Li.) Me hdi à Maui ds os

und

al md

|
|
|

 
