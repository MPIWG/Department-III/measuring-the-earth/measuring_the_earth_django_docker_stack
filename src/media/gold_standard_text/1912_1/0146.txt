 

138

In den Berichten des Zentralbureaus über die relativen Pendelmessungen er-
scheint die örtliche Störung der Schwerebeschleunisung in doppelter Form, als totale
Störung und als Störung durch eine im Meeresniveau gedachte, also ideelle, störende
Massenschicht. Die Betrachtung der Verteilung der Schwerestörungen hat bekanntlich
die Prarrsche Theorie von der Isostasie der Erdkruste ganz erheblich gestützt, mit
welcher Frage ich mich wiederholt seit 30 Jahren beschäftigt habe. Ein direkter Nach-
weis der Isostasie für die Kontinentalmassen in ihrer Beziehung zu den Meeresräumen
im ganzen gelang dem Zentralbureau durch die von ihm ausgegangenen Beobachtungen
auf dem Weltmeere durch Oskar Hecker nach dem Prinzip der Methode von Mon.
Das Zentralbureau hat hierüber die Abhandlungen Hecxers veröffentlicht, und ich habe
mich kürzlich in einer Abhandlung der Berliner Akademie der Wissenschaften kritisch
mit den Ergebnissen beschäftigt, um die Beweiskraft derselben hervorzuheben. Ein von
mir verfaßter Artikel im sechsten Bande der mathematischen Enzyklopädie stellt die
wesentlichsten Errungenschaften auf dem Gebiete der Schweremessungen zusammen. *)
Hier habe ich auch mittels der Theorie von Stores zu schätzen versucht, wieviel etwa
die radialen Abweichungen des Geoids vom allgemeinen Umdrehungs-Erdellipsoid betragen
werden. Eine ähnliche Untersuchung hat auf meine Anregung gleichzeitig das obengenannte
frühere Mitglied des Zentralbureaus R. Scnumanw (jetzt in Wien) kürzlich in den Berichten
der Wiener Akademie der Wissenschaften veröffentlicht. Die radialen Abweichungen
des Geoids vom Erdellipsoid dürften übereinstimmend mit meinen früheren Untersuchungen
kaum 100m an irgend einer Stelle erreichen, meist aber erheblich kleiner sein.

Durch die Allgemeine Konferenz der Internationalen Erdmessung wurde dem
Zentralbureau 1895 die Aufgabe gestellt, gemäß dem Plane von Wırneıum Forrsrer auf
einem Breitenparallel der Erde einen internationalen Breitendienst einzurichten
und die Ergebnisse desselben abzuleiten. Wie erwähnt, wurden dazu erhebliche inter-
nationale Fonds zur Verfügung gestellt. In die Vorbereitung zu diesen Arbeiten war
das Zentralbureau schon 1889 eingetreten, als es sich zunächst um die sichere Fest-
stellung der Tatsache der Breitenvariation handelte. Zuerst wurden die simultanen
Beobachtungsreihen in Berlin, Potsdam, Prag und Straßburg organisiert und nach dem
Erfolg dieser Studie als weiterer Schritt die Expedition nach Honolulu mit korrespon-
dierenden Beobachtungen in Berlin veranlasst. Erstere führte Avorr Marcuse mit einem
nach Angaben des Zentralbureaus gebauten Zenitteleskop aus, letztere bewirkte H. Barren-
many am Altazimut der Berliner Sternwarte. Diese ersten Versuche führten bekanntlich
im Verein mit den von der Coast and Geodetic Survey aus Anlaß der Honolulu-Expedition
veranlaßten Reihen von Beobachtungen an drei verschiedenen, geeigneten Stationen dazu,
den von Forrstrr empfohlenen Breitendienst zu verwirklichen.

Zuerst wurde noch die Frage durch mehrere Beobachtungsreihen geklärt, ob bei
den Beobachtungen visuell oder photographisch vorzugehen sei. Mit Rücksicht auf die

*) F, R. Heımert, Die Schwerkraft und die Massenverteilung der Erde. (Enzyklopädie der
mathematischen Wissenschaften, VI, 1.) — Sitzungsberichte der Berliner Akademie 1912, 8S. 308 ff.

10

 

lk

114

 

a A I

Pads vs init

wb fii hie li

ju au hahah hs Hohl ta ie :

 
