 

ANNEXE A. XVI,

LABORATOIRE D’ASTRONOMIE GEODESIQUE DE
_ L'ÉCOLE DES HAUTES ÉTUDES, À PARIS.

Rapport sur deux déterminations de différences de longitude.
PRÉSENTÉ PAR

M. B. BAILLAUD.

Par décision de M. le Ministre de l’Instruction publique en date du 8 février 1911,
il a été créé à Paris un laboratoire d'astronomie géodésique rattaché à l’École des Hautes
ftudes: M. B. BarLzLAUD, directeur de l'Observatoire de Paris a été nommé directeur de ce
laborätoire auquel un budget spécial a été attribué.

Les circonstances ont amené le directeur de ce laboratoire à porter les premiers efforts
vers la reprise, par les astronomes de l'Observatoire de Paris des déterminations de différence
de longitude en profitant des installations faites par le génie militaire à la Tour Eifel et à
l'Observatoire de Paris pour l'émission de signaux par télégraphie sans fil.

La première détermination a été celle de la différence de longitude entre Paris et
Bizerte faite en avril et mai 1911 par M. Lancztin, astronome adjoint à l'Observatoire de Paris,
et M. Tsarsorounos, alors stagiaire au même observatoire, sous la direction immédiate de
M. H. Renan astronome titulaire, en même temps qu’elle était faite par les officiers du
Service géographique sous la direction de M. le Colonel Bourezois.

De mai a juillet 1912, M.M. Denporre et Vimnnur astronomes adjoints à Uccle et à
Paris, sous la direction immédiate de M.M. Lecoinrs et Renan, ont déterminé la différence
de longitude entre Paris et Uccle. Nous ne saurions trop remercier M. le commandant
Ferré, chef du service de la Tour Eiffel pour l’activité inlassable avec laquelle il a procédé
aux installations nécessaires et en à constamment assuré le parfait fonctionnement, et aussi
M. le Colonel Bourazois et les officiers du Service géographique pour la cordialite avec la-

A

quelle ils ont prêté à nos observateurs, en toutes circonstances, le concours le plus empressé.

 

htmntètes

euere

Hahn

un a ann

 
