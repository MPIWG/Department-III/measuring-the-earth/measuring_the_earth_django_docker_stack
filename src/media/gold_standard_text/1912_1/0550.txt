 

452

Recognition of an anomalous refraction of this kind leads to a rule which might be
advantageously adopted by meridian circle and latitude observers. For declination measures
several bisections of the star should be made, separated by as great intervals of time as
practicable; for right ascensions, the thread system should bs widely spaced.

Previous experience with zenith instruments for measuring small zenith distances
has disclosed anomalies of scale value corresponding to abrupt and unexplained changes in
focal length. In order to test the stability of the scale value of the photographic zenith
tube at Gaithersburg, three groups of stars have been selected, each group containing about
18 stars, scattered over several hours in right ascension. When a complete group has been
observed, the scale value is determinable for that night by a least square solution. Up to
the present time a complete series of observations on only one group has been obtained.
The results are given below; x is the arc value of one revolation of the comparator screw:

 

 

Date. Temperature. n v

 

de oe 298 19.9553 + .0005

ee 06 9529 + .0029

OO 905 9563 — .0005

noon one ad 9577 — .0019

Hehe Fe. ig 9557 + .0001

et 5 9568 — .0010
iMeansı 2. — 9) ee le a nes

 

 

 

The declinations of the stars, which for the determination of » should be known
with preeision, were determined by Prof. R. H. Tucker with the meridian circle of the
Lick Observatory, through the courtesy of Director W. W. CAMPBELL.

The meteorological conditions on the nights when the scale value has been determined
were unusually bad. Notwithstanding this, the agreement from night to night is seen to
be remarkably close and far better than corresponding determinations with visual instru-
ments. We can safely conclude that there are no abnormal changes of scale value apparent.

From a comparison of these results with some values secured during the previous
summer there does not seem to be any appreciable variation of scale value with temperature.
In order to obtain the temperature effect independently, samples of the focusing rod and
photographic plates were forwarded to the United States Bureau of Standards for determi-
nation of their thermal coefficients. The temperature coefficient of the E1izabeTH THomeson
Comparator has been obtained in the office of the United States Coast and Geodetic Survey
by measuring plates in rooms at temperatures of 0° and 40°, respectively.

There is a class of systematic error usually present in instruments for observing

 

 
