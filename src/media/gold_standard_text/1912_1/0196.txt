 

188

April 1892 bis April 1893. Berlin 1895. 22 pages in 8°,
„ 1092 , »,:1894, 3 1392 527 Hy
HSE u 1898, x 1898: 39, „ana
„1399 „. „ 1896. Potsdam 1806,42%2. 158 1528%
O9. 2: EI, bs LOOT. Bie Wand el,
ip ESOT gs 4 1000. > 1828.23 22,8%
1098... ,..1899, n 1899.327 sonen.

c) Publications de l’Institut géodésique royal de Prusse et du Bureau central
de l’Association géodésique internationale.

Die Schwerkraft im Hochgebirge insbesondere in den Tyroler Alpen in geodätischer und geolo-
gischer Beziehung von F. R. Helmert. Mit vier lithographischen Tafeln Berlin. (P.
Stankiewiez) 1890. 52 pages in 4°.

Die Europäische Längengradmessung in 52 Grad Breite von Greenwich bis Warschau.

I. Heft. Hauptdreiecke und Grundlinienanschlüsse von England bis Polen. Heraus-
gegeben von F. R. Helmert. Mit zwei lithographierten Tafeln. Berlin
(P. Stankiewiez) 1893. 263 pages in 4°.

II. Heft. Geodätische Linien, Parallelbogen und Lotabweichungen zwischen Feaghmain
und Warschau. Von A. Börsch und L. Krüger. Berlin (P. Stankiewicz)
1896. 205 pages in 4°. :

Bemerkungen zu der Schrift: „Die Erforschung der Intensität der Schwere im Zusammenhange
mit der Tektonik der BErdrinde als Gegenstand gemeinsamer Arbeit der Kulturvölker. (Kgl. Ges.
d. Wissenschaften zu Göttingen. Juni 1894)”. Von F. R. Helmert. 1894. 8 pages in 16°.

Beiträge zur Theorie des Reversionspendels von F. R. Helmert. Mit einer Tafel. Potsdam
(B. G. Teubner in Leipzig) 1898. 92 pages in 4°.

Beiträge zur Berechnnng von Lotbaweichungssystemen von Prof. Dr. L. Krüger, Abteilungs-
vorsteher am Königl. Geodätischen Institut. Potsdam (B. G. Teubner in Leipzig) 1898.
106 pages in 4°.

Stern-Ephemeriden auf das Jahr 1892 zur Bestimmung von Zeit und Azimut mattels des trag-
baren Durchgangsinstruments im Vertikale des Polarsterns. Von W. Döllen. Berlin
(P. Stankiewicz) 1891. 39 pages in 8°.

d) Publications de l’Institut géodésique royal de Prusse (Nouvelle Série).

1. Die Polhöhe von Potsdam, Il. Heft. Mit drei lithographierten Tafeln. Berlin (P. Stan-
kiewicz) 1900. (Von O. Hecker). 58 pages in 4°.

2. Das Mittelwasser der Ostsee bei Travemünde, Marienleuchte, Wismar, Warnemiinde, Arkona
und Swinemünde in den Jahren 1882/97. Bearbeitet von Prof. Dr. A, Westphal. Mit
2 Tafeln. Berlin (P. Stankiewiez) 1900. 143 pages in 4°,

A A ANAM à Man ed dca al SEES

A1 ib

a.

Aa a lh a ul

lla i.

1.10 hdl aos no,

 
