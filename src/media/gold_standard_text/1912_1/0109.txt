u krass Bina
AT TTP

a

103

Station Oncativo, welche von dem Dienste auf den Südparallel Oncativo— Bayswater übrig
geblieben war, leider wegen Beobachtermangel geschlossen werden; es gelang noch nicht,
sie wieder zu eröffnen. Einen gewissen Ersatz bietet aber das Observatorium zu Johannes-
burg in Transvaal, wo nach dem Beschlusse der vorigen Konferenz eine Beobachtungsreihe
begonnen hat, deren Ausführung mit 4000 M. jährlich unterstützt wird.

Indem ich Herrn Albrecht den ausführlichen Bericht über die Tätigkeit des Zentral-
bureaus in der Angelegenheit des Studiums der Breitenvariation überlasse, gedenke ich aber
noch mit grösstem. Danke der Bemühungen der Beobachter und der Unterstützung der
Behörden und Institutionen in den in Betracht kommenden Ländern, welche dem Zentral-
bureau die Fortführung des Dienstes ermöglichten.

Die Erforschung der Grösse der Schwerkraft auf dem Meere hat ihren vorläufigen
Abschluss durch Bearbeitung und Herausgabe der von Herrn Hecker erzielten Ergebnisse
in einem Druckwerke gefunden. Die Beweiskraft dieser Resultate für die allgemeine Isostasie
der Erdkruste habe ich neuerdings in einer, in den Sitzungsberichten der Berliner Akademie
der Wissenschaften zu Berlin erschienenen Abhandlung erörtert.

Gelegentlich der letzten Reise auf dem schwarzen Meere konnte Herr Hecker auch
auf den Stationen Odessa, Tiflis und Bukarest relative Pendelmessungen ausführen; in
Bukarest ergab sich bis auf 1 Einheit der letzten Stelle der früher von Herrn Borrass
ermittelte Wert, was eine erwünschte Bestätigung der hohen Genauigkeit der vom Zentral-
bureau ausgeführten relativen Pendelmessungen ergibt,

Herr Borrass hat auch den Bericht über die relativen Pendelmessungen für das
Säkulum 1808—1909 als Teil III der London-Cambridger Verhandlungen herausgegeben.
Hier ist die Schwerebeschleunigung, auf Potsdam bezogen, für 2398 wesentlich verschiedene
Orte angegeben.

In einem grösseren Artikel der mathematischen Enzyklopädie habe ich die wesent-
lichsten Ergebnisse der Schweremessungen für Erdgestalt und Massenverteilung der Erdkruste
zusammengefasst. Zur Prüfung der isostatischen Hypothese von Pratt-Hayford wurden 18
Stationen entsprechend reduziert.

Wie schon wiederholt früher, wurden auch in den letzten Jahren von Herrn Haasemann
Konstanten verschiedener Pendelapparate auf Wunsch auswärtiger Gradmessungskommissionen
bestimmt und verschiedenes andere zur Förderung der Schwerkraftsmessungen geleistet. Die
britische antarktische Expedition von 1910 (Kapitän Scott) erhielt einen Pendelapparat mit
Pendeluhr von Seiten des Geodätischen Instituts dargeliehen. Eine Nachricht des Beobachters
C. 8. Wright aus Ross Island, Oktober 23, 1911, lässt einen guten Fortgang der Schwere-
messungen daselbst erwarten. Auch die deutsche Südpolarexpedition von 1911 (Oberleutnant
Filchner) wurde im Geodätischen Institut für Pendelarbeiten ausgerüstet.

Die Aufstellung des europäischen Lotabweichungssystems wurde durch Bearbeitung
des astronomisch-geodätischen Netzes in 48° Breite von Astrachan bis Wien gefördert. Für
Österreich-Ungarn fehlen allerdings noch einige astronomische Beobachtungsdaten. Da auch
noch in dem weiter westlich liegenden bayerischen Netzteil 2 Laplace’sche Punkte angelegt
werden sollen, so sind diese Lotabweichungsrechnungen jetzt unterbrochen.

 

nn

ere este zus

 

 

. nn ae

rn a

Di seine un A a

}
4
#
i

Éd can go Loge nn eier ne

 
