 

ar art

MAN D PDU EEE MNT LOUE CT 1

MORE TL ET

en est confiée aux agents des services locaux des Ponts et Chaussées, mais le dépouillement
et les calculs sont centralisés au Service du Nivellement General de la France.

Le port de la Goulette étant fondé sur un sol instable conquis sur la mer, le médi-
marémétre établi sur ce point en 1889, s'était, en 1909, soit 20 ans après, abaissé d'environ
Ow,14; ce qui représente en moyenne, un affaissement de 7 millimètres par an. La comparaison
des niveaux moyens bruts, calculés pour le port de la Goulette et pour les ports d'Oran
et de Bône, a montré — comme c'était à prévoir — que l’affaissement en question s’est
produit, d’une manière non pas brusque, mais progressive. On a, dès lors, admis que, de
1888 à 1910, sa valeur avait crû proportionnellement au temps écoulé depuis l’origine des
observations et celles-ci ont été rectifiées en conséquence.

Le tableau ci-après donne, à la date du ler janvier 1912, et par rapport au zero de
Tunis, les altitudes du niveau moyen de la mer, fourni par les médimarémètres de la Goulette,
Bone, Alger et Oran. Comme pour les appareils de la métropole, les résultats sont corrigés
de Verreur systématique d’observations.

Altitudes du niveau moyen de la mer sur le littoral Algéro-
Dumidien-à 14 date du femmes 1902

 

 

 

 

 

 

 

De Nombre d'années Aude
Postes Nature d'entrée en ge par rapport au
d’observations de l’appareil. fonctions je sinus zéro de Tunis.
: normal.
ie 2. - 3% 4. 5.
cm.
Sax. te Médimarémètre. 1910 9 ans — 15
Soussesr à = @0, — 1910 2 — — 1
la-Gouletiec =. ==... a : 1889 22 — + 4
Bone. Gee Ce eee — do — 1889 92 — +11
Alger’. Go jaye a su — do, — 1904 8 — je ©
Oran 2e ou enr cu 1890 D — 2
Niveau moyen général de Ja Méditerranée le long de la cote Algéro-Tunisienne,
pour la pertode de i889 a 10 . .:. . ... 2... + 4

Relation provisoire de hauteur entre le zéro normal frangais et
le zéro de Tunis. — A supposer que les influences atmosphériques ne créent pas une
dénivellation systématique entre les niveaux moyens de la Méditerranée à Marseille et à
la Goulette, le zéro de Tunis, d’après les données actuelles, se trouverait à 5 centimètres
environ au-dessus du zéro normal du Nivellement Général de la France.

 
