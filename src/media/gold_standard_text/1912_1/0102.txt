 

Nach den Besprechungen von Prof. Hzckers Bericht über die Bewegung der Lotlinie
durch die Anziehung von Sonne und Mond, hat die Konferenz in Cambridge folgenden
Wunsch geäussert:

1°. Zur Kontrolle und zur Vergleichung möchten dergleichen Beobachtungen mit ver-

schiedenen Instrumenten in verschiedenen Ländern, besonders auf dem Boden
der tiefen Gruben in Pfibram in Böhmen angestellt werden;

2°, die internationale Assoziation für Erdbebenforschung möge, so weit es ihr möglich

sei, ihre Beihilfe zu diesen Uutersuchungen verleihen.

Diesen Wunsch habe ich dem Generalsekretär der Assoziation für Erdbebenforschung
Prof. Dr. v. Kövasuigeruy mitgeteilt, der mir sofort aufs freundlichste erwiderte, dass die
Delegierten dieser Assoziation in Antwort auf diesen verehrenden Antrag sicher ihre Mit-
wirkung nicht verweigern würden.

In der 4. Sitzung der permanenten Kommission für Erdbebenforschung, vom 18. bis
zum 22. Juli 1911 in Manchester abgehalten, ist zur Beratung dieser Angelegenheit eine Kom-
mission ernannt worden, welche beantragt hat, eine Summe von 10000 Mark zur Disposition
der Spezialkommission für die Deformation der Erde zu stellen, mit dem Zwecke 4 Stationen
einzurichten und während einer Periode von 2 Jahren zu erhalten. Diese Kommission machte
den Antrag, eine von diesen Stationen in der Mitte des nordamerikanischen Kontinents,
eine in der Mitte des russischen Reiches, eine in Paris, und eine in der südlichen Halb-
kugel einzurichten. Dieser Antrag ist von der Permanenten Kommission für Erdbeben-
forschung angenommen worden, sodass wir von der Kooperation der beiden internationalen
Assoziationen schöne Resultate erwarten dürfen.

In Bezug auf eine sehr wichtige Angelegenheit hat die Konferenz in Cambridge
in seiner letzten Sitzung folgenden Beschluss gefasst.

Die Konferenz ersucht die Vermessungsbehörden von Russland und Indien in Erwägung
zu ziehen, ob nicht an der Westseite des Zentralasiatischen Hochlandes eine Dreieckskette
zur Verbindung des grossen indischen Dreiecksnetzes mit den russischen Dreiecken in Turkestan
zu ermöglichen wäre, was von immenser Bedeutung für die Erforschung der Massenverteilung
in Zentralasien sein würde.

Dieser Wunsch, welche den Vertretern Russlands und Gross-Brittanniens mitgeteilt
worden ist, gab Veranlassung zu einer ziemlich ausgedelinten Korrespondenz von welcher
ich, angesichts der grossen Wichtigkeit des Problems, einige Hauptsachen mitteilen werde.

In einem Brief vom 22. Juli 1910 teilte der russische Botschafter in Berlin, Herr
Graf OSTEN SACKEN, mir mit, dass das Kriegsministerium den Beschluss gefasst hatte im
Laufe des Jahres dem, von der 16. Generalkonferenz ausgesprochenen, Wunsch Folge zu
leisten, und sandte mir einen, zu diesem Zwecke von General Arramoxorr nach dem Ent-
wurf des Oberstleutnants TCHEIKInE ausgearbeiteten Plan, nebst einer Karte. Am Ende fügte
er hinzu: Meine Regierung erwartet jetzt die Antwort der kompetenten englischen Autoritäten
in Indien auf diesen Plan.

Von englischer Seite hatte ich noch nichts gehört, als am 31. Januar 1911 der
russische Botschafter mich bat, ihm so bald wie möglich mitteilen zu. wollen, zu welchen

Te a Male. |. Hd ul

Tl

toto!

Rabie b=

à dau eam ie ae

i
|
|

 

 
