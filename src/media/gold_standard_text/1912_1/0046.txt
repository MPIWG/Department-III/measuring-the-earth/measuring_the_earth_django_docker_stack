 

40

Wright, de Vile de Ross, en date du 23 Octobre 1911, on peut espérer que les déterminations
de l’intensite de la pesanteur y auront de bons résultats. Le Bureau central a aussi fourni
à l'expédition allemande vers le pôle sud de 1911 (Premier-lieutenant Fichner) les différents
appareils pour les observations de pendule.

La réduction du. réseau astronomique géodésique sous le parallèle de 48°, depuis
Astrachan jusqu’à Vienne, a contribué à développer le système des déviations de la verticale
en Europe; il nous manque encore quelques données astronomiques en Autriche-Hongrie.
Comme l’on veut établir encore deux points de Laplace dans la partie occidentale du réseau
en Bavière, on a suspendu pour le moment ces calculs de la déviation de la verticale.

Je dois rappeler ici qu’à notre grand regret M. le Prof. Dr. 4. Bürsch a été obligé
de donner sa démission pour cause de santé. Le Bureau central lui doit de nombreux travaux,
surtout sur des sujets de géodésie mathématique. À la suite de la dernière Conférence
générale il a encore élaboré un rapport sur les nouveaux calculs de la déviation de la
verticale dans les divers États, appartenant à l’Association géodésique internationale.

A present M. le Prof. Krüger, qui s’est occupé déjà des calculs tant théoriques que
pratiques du système des déviations de la verticale en Europe, a entrepris de rassembler
les résultats qui ont été déduits, jusqu’à présent, des calculs du Bureau central; deux jeunes
gens l’assisteront dans ce travail.

Quelques calculs sur l’influence de la distribution irrégulière des masses dans la croûte
terrestre sur la figure de la terre, dans l’arc du parallèle de 52°, ont été exécutés par moi
et sous ma direction. Je me suis aussi occupé de composer le système des formules néces-
saires aux calculs de la correction isostatique à apporter à la direction de la verticale.

Dans le rapport sur les travaux géodésiques exécutés par l’Institut géodésique en
Prusse je signalerai aussi quelques travaux d’un intérêt plus général. J’ai encore à rappeler
ici le rapport de M. Albrecht sur les longitudes, latitudes et azimuts, et le rapport de M. Gale
sur les triangulations, publiés dans les Comptes rendus de la Conférence de Londres-Cambridge.

Dans les trois dernières années le Bureau central a dü s’occuper de nombreuses
affaires administratives, en partie relatives à l'expédition de publications géodésiques, en
partie aussi relatives à la gestion du fonds international des dotations.

J'ai apporté ici les comptes des recettes et des dépenses pour les années 1909, 1910
et 1911, afin qu'ils puissent être examinés par la Commission des finances. A la fin de
l’année 1912 le total des fonds disponibles montera probablement à 52500 francs environ.
Jl faut en déduire une somme d’environ 11250 francs, dont la dernière Conférence a déjà
disposé, mais qui n’a pas encore été dépensée. Puisque les parts contributives des Etats n’entrent
que peu à peu dans le courant de l’année, et qu’il nous faut au commencement de chaque
année des sommes considérables pour la subvention du service des latitudes, il faut prendre
garde, en décidant de nouvelles allocations, qu’à la fin de l’année le montant disponible ne
descende jamais au dessous d’un minimum d'environ 87500 francs.

Après la lecture du rapport M. le Président demande si un des délégués a des obser-

x

vations à présenter; personne ne demandant la parole, M. le Président remercie M. Helmert

 

LPC Te er enr

ern Te ae Mai

pet

tab ten

to tant

1m ai on Mall doaats ad ann nn

|

 
