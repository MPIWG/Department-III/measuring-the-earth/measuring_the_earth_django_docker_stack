 

(oT

4

BEILAGE A, V.

BERICHT VON DANEMARK.

In den Verhandlungen der in London und Cambridge abgehaltenen sechzehnten Con-
ferenz der internationalen Erdmessung habe ich erwähnt, dass die ersten Hefte der neuen
Serie unserer Publikationen erschienen waren. Seit 1909 sind noch 5 neue Hefte heraus-
gegeben worden, sodass jetzt von der neuen Folge nachstehende 10 Hefte vorliegen:

Hei 1.

Heft 2.
Heft 3.

Heth 4.

Heft

Heft 6.

Hei 7.

Hett à.

Heft 9.

Heit 0,

Heft 9 enthält sämmtliche bis 1910 inclusive bei uns ausgeführten Wasserstands-

*

(DY

Anknüpfung der tychonischen Ruinen an das europäische Gradmessungsnetz, nebst
einer neuen Triangulationsverbindung zwischen Dänemark und Schweden von Herrn
Oberstleutnant M. J. Sann. 1908.

Relative Schweremessungen; Bornholm nebst Fünen und anliegenden Inseln von
Herrn Kapitän N. P. Jonansen. 1908.

Präeisionsnivellement, Jütland, nach dem Entwurf des Generals ZACHARIAE zusammen-
bearbeitet von Herrn Oberstleutnant N. M. Prrersen. 1909.

Nivellement über breitere Wasserstrecken nach dem Manuscript des Generals
ZACHARIAE herausgegeben von der Danischen Gradmessung. 1909.

6 Breitenbestimmungen nach der Methode Horrzsow, ausgeführt in den Jahren
1890—92 nebst Resultate von späteren Breitenbestimmungen der Gradmessung
von Herrn Oberstleutnant M. J. Sano. 1909.

17 Breitenbestimmungen nach der Methode HorrEBOW, ausgeführt in 1895 — 1905
von Herrn Oberst M. J. San». 1910.

18 Breitenbestimmungen nach dem Verfahren v. STERNECK, ausgeführt in 1897 —
1899 von Herrn Oberst M. J. Sanp. 1911.

Präcisionsnivellement: Fünen, Seeland und Falster von Herrn Oberstleutnant N.
M. Perersen. 1911.

Wasserstandsbeobachtungen, Normalhöhenpunkt für Dänemark, Vergleich von preus-
sischen und schwedischen Nivellementskoten mit dänischen, etc., von Herrn Oberst-
leutnant N. M. PETERSEN. 1912.

Résumé des cahiers I—IX herausgegeben von der dänischen Gradmessung. 1912.
Die Hefte 6 & 7 schliessen sich an das von demselben Verfasser bearbeitete Heft 5
an, so wie Heft N® 8 sich dem schon 1909 erschienenen Heft 3 anschliesst.

29

 

ï
:

t

iq
if

new CaT RI

 
