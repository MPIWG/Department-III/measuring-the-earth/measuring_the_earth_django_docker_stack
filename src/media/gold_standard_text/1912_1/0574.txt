 

Vorschläge der Herren Pomerantzeff und Knoff die nächste Konferenz in St. Petersburg, resp. in

466

Christiania abzuhalten

Dankesvotum im Namen der Kontron an da Stadt ne Senator v. Melle und Pret Saher

für ihren herzlichen Empfang. 5
Dank des Herrn Madsen an dem Bureau für Mine lee
Schluss der 17. Generalkonferenz .

A. Rapports des délégués sur l’avancement des travaux géodésiques

ANNEXES — BEILAGEN.

dans leurs pays.

 

A. Berichte der Delegierten über die Fortschritte der Erdmessungs-

Beilage A. J.

Annexe A. J.

Beilage A. TI.
Beilage A. II®.
Beilage À. IT.
Annexe A. ITT.
Beilage A. IV.

Beilage A. V.
Beilage A. VT.

Beilage A. VII.
Beilage A. VIII.

Beilage A. 1X.

Beilage A. X.

Beilage A, XJ.

Beilage A. XI.
Beilage A. XIII.

 

arbeiten in ihren Ländern.

Helmert. Das Zentralbureau während der ersten 50 Jahre der Internationalen
Erdmessung

Helmert. Rapport sur les ie An Biren cel on is 50 premicres
années de |’Association géodésique internationale :

Albrecht. Beilagen zum Bericht über den internationalen Breitendienst. A Resi
tate der Breitenbeobachtungen in Oncativo in den Jahren 1909—1911
Albrecht. Beilagen zum Bericht über den internationalen Breitendienst. B. Resul-

tate der nen in Johannesburg.

Albrecht. Beilagen zum Bericht über den ann Breitendichst, C. Resiil-
tate der Beobachtungen in Lissabon. i

Chili. Rapport sur les travaux géodésiques au Chili, ee 6 planches par
Ernesto Greve .

Chili. Geodätische aber des Elilenischen Cas chines aus de
Bericht) von Felix Deinert. Mit einer Karte . :

Dänemark. Bericht von Dänemark von V. H. O. Madsen . scene

Dänemark. Photographischer ae für Schworemessungen, von
vH. 0. Madsen.

Dänemark. Statischer ad ine pirat, von Yv. ET, 0. Madsen

Deutschland. Bericht der Trigonometrischen Abteilung der Kön. Preussischen
Landesaufnahme über ‘ihre Arbeiten in den Jahren 1909—1912, von
Launhardt. Mit drei Karten.

Deutschland. Preussen. Königliches oe A in Potsdam Bericht
über die Arbeiten 1910—1912, von Helmert .

Deutschland. Gezeiten und Starrheitskoeffizient » der festen Erde, re
aus den Registrierungen der Horizontalpendel in Freiburg i. B. ind Durlach
vom 1. November 1907—1908, von M. Haid. Mit einer Tafel .

Deutschland. Bericht über die in den Jahren 1910 bis 1912 in Bayern aus-
geführten Erdmessungsarbeiten von Dr. A. Finsterwalder und Dr. M. Schmidt

Deutschland. Bericht über die Hamburgische Vermessung von Klasing

Deutschland. Bericht über die Arbeiten in Elsass-Lothringen von J. Bauschinger.

Page, Seite
126

126

126

126
129—164
165—200
201—211
212—220
221—222
223 — 254
935—237
239 —242
243 — 244
245 — 248
949—252
9538—254
955—258
259— 261
262— 265
266
