 

32

Vinfluence des mouvements du bateau sur son appareil pour la determination de la pesanteur.

Afin d’obtenir une bonne jonction entre les nivellements de pays limitrophes, la
Conférence avait exprimé le vœu de voir hâter, ‘le plus possible, l'achèvement des travaux
sur le terrain et des calculs relatifs aux jonctions prévues des réseaux de nivellement:

a. entre l'Espagne et la France à Port Bou,

b. entre l'Autriche d’une part, la Russie et l'Italie d'autre part.

J'ai communiqué ce vœu aux représentants des Gouvernements de ces cinq États
à Berlin.

Monsieur l'Ambassadeur de France m'a répondu que, en ce qui touche la France, les
travaux de terrain et de calcul relatifs au raccordement à Port Bou des réseaux hypsomé-
triques français et espagnol sont terminés depuis 1896, et qu'il n'appartient donc qu'à
l'Espagne seule de compléter cette jonction.

M. le Général ARTAMoNOYF m'a répondu, en date du 4 Octobre 1910, que la Direction
topographique militaire conférait à propos de la jonction avec l’Institut géographique de Vienne.

M. l'Ambassadeur d'Espagne m'a fait savoir qu'il avait porté le contenu de ma lettre
à la connaissance de son Gouvernement.

Après les discussions du rapport de M. le Prof. HEOKER sur les mouvements de Ia
verticale dus à l'attraction luni-solaire, la Conférence générale à Cambridge a exprimé le vœu:

19. qu’à titre de contrôle et de comparaison, des mesures analogues soient exécutées

avec des instruments divers, dans d’autres pays eb notamment au fond des
mines profondes de PYibram, en Bohême;

20, que l'Association sismologique internationale veuille bien, dans la mesure de ses

moyens, coopérer à ces expériences.

J'ai communiqué ce vœu au Secrétaire général de l'Association sismologique, M. le
Prof. De Kôvesrieeray à Budapest, qui a eu la bonté de me répondre immédiatement de la
manière la plus courtoise, que les délégués de l'Association internationale de sismologie,
flattés de cette invitation honorable, ne refuseraient certainement pas leur concours.

Pour étudier cette question on a nommé dans la quatrième séance de la Commission
permanente de l'Association de sismologie, tenue à Manchester du 18 au 22 Juillet 1911,
une commission spéciale, qui a proposé de mettre à la disposition de la Commission
spéciale pour la déformation de la croûte terrestre la somme de 10000 Mark pour l’orga-
nisation et l'exploitation de 4 stations pendant une période de deux années. La commission
a proposé en outre d'installer ces stations au centre du continent de l'Amérique du Nord,
au centre de l’Empire russe, à Paris, et dans l'hémisphère sud. Cette proposition a été
adoptée par la Commission permanente, Nous pourrons espérer ainsi de beaux résultats de

la coopération des deux Associations internationales.

A propos d’une question fort intéressante, la Conférence à Cambridge a pris dans
sa dernière séance la résolution suivante:

La Conférence prie les Services géodésiques de la Russie et des Indes de prendre en
considération le fait, qu'il serait d’une supérieure importance pour les études de la distri-
bution des masses continentales dans l'Asie centrale, de rechercher les moyens d'établir une

(CITE TRI

ALU Led CAT TUE VAR

Labor

hen!

2

 

 
