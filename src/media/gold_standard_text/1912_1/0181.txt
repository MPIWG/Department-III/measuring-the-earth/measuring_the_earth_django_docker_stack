a ATMO Lt
Sieg

anne sé
per TRETEN"

OT

wart

TTT TTT STV FRET IT EEF teem

une vingtaine d’années, quant à la valeur absolue de l’intensité de la pesanteur, a été cause
que le Bureau central s'est aussi occupé de ce problème. M. F. KÜHNEN et moi, nous avons
déterminé plus exactement l'influence de la flexion du pendule (Voir ma publication , Bei-
träge zur Theorie des Reversionspendels 1898). Sur la base de la théorie de BEgsEL que
M. Derrorces et moi avions complétée, M. Künnen et M. Pairpp FuRTwANGLER ont
déterminé l'intensité de la pesanteur à Potsdam par des travaux de plusieurs années, dont
les résultats ont été publiés, en 1906, dans un ouvrage volumineux. Grâce au grand nombre
de bons rattachements à d’autres stations où l’on avait déterminé aussi cette intensité absolue,
on à obtenu une comparaison de toutes les déterminations les plus intéressantes avec celles de
Potsdam, ce qui fait présumer, ainsi qu'on peut aussi le déduire de l’accord des observations
de Potsdam entre elles, que l'erreur relative de la valeur absolue de y est au maximum
de ‘50000 Probablement moins encore.

J'ai réuni les résultats des observations internationales des pendules dans plusieurs
tableaux publiés dans les Comptes rendus de l’Association géodésique des années 1887, 1889,
1895, 1898 et 1900. Le dernier rapport est très complet et contient aussi les résultats
obtenus antérieurement, au commencement du 19e siècle en partie nouvellement réduits.
Ensuite M. Bornass a publié des additions en 1903 et en 1906, et a rédigé un rapport
tres complet et très détaillé contenant tous les résultats depuis 1808 jusqu’à 1909, Partant
d’un réseau compensé des stations principales, toutes ces valeurs ont été rattachées à la
valeur absolue de l'intensité de la pesanteur à Potsdam.

J'avais déjà publié en 1884, dans le 1e: Volume de ma Géodésie supérieure, de nouvelles
valeurs des constantes de la formule de OLaiRauT, qui indique la relation entre l'accélération
de la pesanteur et la latitude de la station; ces constantes avaient été déterminées d’après une
nouvelle méthode en utilisant les déterminations de la pesanteur connues alors dans un peu plus
de 100 stations. Les riches matériaux contenus dans mon rapport de 1900 me permirent
de donner plus de précision aux valeurs trouvées auparavant. La nouvelle formule de 1901
qui, quelques années plus tard, à été corrigée encore un peu pour tenir compte de la valeur
de lintensité absolue à Potsdam, ne subira plus que de légers changements.

La valeur de l’accélération de la pesanteur est employée dans les travaux concernant
la géodésie et plusieurs autres sciences; la valeur de la réduction au niveau de la mer, qui
ne représente pas toujours la même quantité, dépend de la nature des travaux dont on s'occupe.
J'ai publié sur ce sujet quelques mémoires, un entre autres en 1890 sur la pesanteur dans les
hautes montagnes („Schwerkraft im Hochgebirge”). Un nivellement de précision exécuté dans
les Alpes y pouvait être réduit pour la première fois d’une manière rigoureuse, en utilisant
les observations de M. v. SreRNECk d’après une théorie que j’avais élaborée déjà antérieurement.

Dans les derniers temps, j'ai indiqué comment on peut tenir compte, à l’aide de la
pesanteur, de la courbure des verticales dans les déterminations locales de la forme du géoide ;
cette méthode a été employée à présent dans la détermination de la forme du géoide au Harz.

Dans les rapports du Bureau central sur les déterminations relatives de la pesanteur,
la perturbation de l’accélération de la pesanteur est donnée de deux manières, d’abord comme
perturbation totale, ensuite comme perturbation causée par l'attraction d’une couche massive

 
