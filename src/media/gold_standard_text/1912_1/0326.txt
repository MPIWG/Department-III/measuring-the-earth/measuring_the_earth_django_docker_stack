 

ANNEXE A, XVIII.

GRECE.

Rapport sur les travaux exécutés par le Service cartographique
de l’Armée 1909-1912.

Le Service cartographique de l’Armée n’a pas eu l’occasion de s'occuper des travaux
de triangulation de ler ordre, car ce réseau est déjà achevé.
La base d’Eleusis a été définitivement calculée et sa longueur est:

L = 4924,654 933 m
lag 15 3,06, 923 756,5 m.

Une nouvelle loi a réorganisé le Service cartographique, auquel a été ajouté une
section spéciale pour l'exécution du cadastre d’aprés les instructions et le concours bien-
veillant du professeur M. Ham.

La section de triangulation s’est occupé pendant les dernières années du réseau de
dme et 4me ordre selon les nécessités des travaux topographiques et cadastrales; néanmoins
nous avons étudié définitivement la question du nivellement de précision et de l'installation
des marégraphes et médimarémétres.

Depuis l’année dernière il existe prés de l’Observatoire d’Athénes une station radio-
télégraphique, de sorte que les travaux pour la détermination exacte de la longitude d'Athènes
seront facilités au point de vue du contrôle, Le Gouvernement Royal de Grèce est disposé
de dépenser les sommes nécessaires pour cette détermination et espère obtenir le concours
bienveillant des membres de l'Association Géodésique Internationale.

| Le Chef du Service cartographique
Athènes—Hambourg de l’Armée

Septembre 1912. HK. Mussata.
Lieut.-Colonel.

 

vent sites dis hdd dis Mahé bah sidi

he biais

ame ne Ma bal ad dus

 
