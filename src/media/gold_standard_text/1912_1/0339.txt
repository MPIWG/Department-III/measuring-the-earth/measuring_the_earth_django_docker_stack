een IHISE |

i

UL RI

petit

CORRE RTT eae ae T

MO Ty

307

Major H. L. Crosthwait has computed for one hundred latitude stations in India,
according to Hayford’s method*, the deflections of the plumb-line in the meridian that
would exist if the hypothesis of isostasy were correct. In the results given below the
negative sign will denote a deflection towards the north, and the positive sign one towards
the south. He has also computed after the same method the deflections in the prime
vertical at 18 longitude stations.

in the following tables the values of the deflection have been derived as follows:

Topographic deflections. Major Crosthwait calculated for each of his 118 stations the
deflection which all topographical features within a radius of 2564 miles (4126 kilometres)
would produce, if the density of the superficial crust were everywhere 2°8. The topo-
graphic deflection of Kalianpur was calculated by him to be —32”"4 As Kalianpur is our
standard station, the topographic deflections entered in the tables are differential from
Kalanpur. Thus for Birond, Major Crosthwait found that the total absolute ‘topographic
deflection without compensation’ was —75”3, and that the topographic deflection at
Birond with regard to Kalianpur was — 753 + 3274 = - 4279,

Following Hayford’s method deflections were computed on the ee that the
compensation 1s complete at 70 miles (113°7 kilometres) below the surface. The deflection
at Kalianpur does not enter, because its value in the meridian at Kalianpur is only — 004
as determined according to Hayford’s hypothesis. The values so determined represent
the deflections that would exist in nature if all topographic features were isostatically
compensated.

The observed deflections were obtained by deducting the geodetic values of latitude
and longitude from the astronomical values. The geodetic values were computed through
the triangulation on the assumption that the deflection at Kalianpur is +4” in the
meridian, and 0” in the prime vertical. The computations are based on the Clarke-Bessel
spheroid +, with equatorial radius 6,378,190 metres, and ellipticity 1/299'15.

India is divided by geologists into three great regions (vide Plate attached):
(i) First region: Tertiary Mountains, Himalaya, Tibet, Baluchistan.

(ii) Second region: the extensive alluvial plains that lie between the tertiary
mountains on the north and the pre-tertiary plateau on the south.

Gu) Third region: the elevated pre-tertiary Peninsula.

I will now summarise the geodetic evidence that has been accumulated as to the
three geological regions.

* The Figure of the Earth and Isostasy, 1909; Supplementary Investigation, 1910, U.S. Coast and Geodetic
Survey.
+ Philosoph. Transact. Royal Society, A, Vol. 205, 1905, page 218.

 
