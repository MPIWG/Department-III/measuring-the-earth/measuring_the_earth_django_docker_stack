 

378

Polhöhe aus den Meridian-Beobachtungen:

vom Juli 1. p.m,: @ = 47°47' 35".25. Gewicht 12.0
A 0 pu O7, 91.06, 5 11.8
Fe 4 pm 021 #41 00.21. ; 12.0
NU, 29 mo A, 1100.04. > JA

 

Mittel: @ = 47° 47’ 36.26. + 0.38. (48 Sterneinstellungen).

Polhöhe nach der Methode HorreBow-TarcorT:

o —= 47° 47 35.68 + 0.15. (120 Doppeleinstellungen).
Reduktion: + 0".18.

Ableitung des Endwertes der Polhöhe:
Polhöhe aus den Zirkummeridianbeobachtungen: @ = 47° 47’ 36.87. Gew. 60.

 

3 » „ Mieridianbeobachtungen:.... @—47 47 36-26. —,, 48
ss nach der Methode HoRREBOW-TALCOTT: O47 47 35 8. ,„ 120,
Beobachtete Polhöhe (Mittel): @ = 47° 47 867.18 + 07.82.
Reduktion’... ... + 0".83.

Polhöhe des trigonometrischen Punktes Szatmär-Nemetiin Ungarn:

47° 4787.05 + 0.32 (Epoche 1911.53).

ZAVINOHRADI.
Ungarn, Komitat Nyitra, '/, Stunde nördlich von Komjät auf dem mit Weingärten
bedeckten Rücken. Markierungsstein 4 —= 0.20 m, #' == 0,91 m.
Polhöhe nach der Methode Horrnsow-Tauconr:

D — 4609 14045 + 0.16.
Reduktion: = — 0.58.

@ = 48° 9'16".87 + 0”.16 (Epoche 1910.72). (68 Doppeleinstellungen).

 

 

Polhéhe aus den Meridian-Beobachtungen:
vom Mai 2) p.m... O40. 9' 18”,50. Gewicht 11.6
5 » D De OC 48 9 16 .89. x 12.0
oe pen @=48 9 16 .63. 5 11.8
3102.09. 2,8. 5 12.0
„ ,,ı2m: 9. 2 2 16 85, . 115
vn Anm. 0 2e 917 08 ï 12.0
Mittel: @ = 48° 9'17".038 + 0".32
Reduktion: ~ 2. 5. - 70;

 

o = 48° 9’ 16".33. + 0.32 (Epoche 1912.34). (72 Sterneinstellungen).

Ue endl aad dais li as

 

5
3
i
1
i
i
+
i
{
j

 
