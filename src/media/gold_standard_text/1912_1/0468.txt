 

394

La différence de niveau entre les Mers Noire et Caspienne, d’après le nivellement
effectué entre Poti et Bakou est de

— 24.553 + 0,058.

La première liaison du niveau des Mers Noire et Caspienne fut effectuée entre No-
vorossisk, sur la Mer Noire, et Pétrovsk, sur la Mer Caspienne. La hauteur du niveau
moyen de la Mer Caspienne, au-dessus du zéro de l'échelle de port de Novorossisk, fut de

— 24.451.

On ne dispose pas actuellement de données relatives au zéro de l’échelle de port de
Novorossisk, par rapport au niveau moyen de la mer, ce qui interdit, en conséquence, d’ex-
poser le résultat de la différence de niveau entre la Mer Noire à Novorossisk et la Mer
Caspienne à Pétrovsk. Cette lacune sera prochainement comblée,

Le désir de relier les nivellements russes et autrichiens fut exprimé à la dernière
Conférence de l'Association géodésique internationale.

Jusqu'à présent, ces nivellements étaient liés en deux points: Belzec et Radziwilow.

Au cours de la période considérée dans ce rapport, l’Institut Militaire Géographique
de Vienne prit sur lui d’effectuer la liaison de ses nivellements avec les nivellements russes
à Woloczyska, et la Section Topographique de l'État-Major Général Russe effectua la liaison
de ses nivellements avec les nivellements autrichiens à Novosielica.

Ce dernier nivellement fut exécuté entre le repère établi à Gnivan, sur le territoire
russe, et celui posé en 1911 par des officiers russes sur le bâtiment de la gare autrichienne
de Novosielica.

La hauteur du dit repère à Novosielica, d’après les nivellements russes, s'élève à
+ 139m.708.

Tableau N° 5. — Autriche-Hongrie et Russie.

 

Numéros matricules Altitudes définitives
des reperes je : ; à
el P + a an Orthométri- | Non ortho- ‘ic
Autriche- une gues métriques oe
: Russie Autriche-
Hongrie Hongrie Russie
Or
i 2 a A 5. 6. is
Belzec 6546 — Repère russe sur la frontière 289.0698 288.215 + 0.855
de l’Empire entre Belzec et
Tomachow.
Radziwilow 6655 — Repère russe dans le voisi- | 240.1224 239.230 | —+ 0.892

nage immédiat de la frontière
de l’Empire, sur une colonne
en pierre.

Podwoloczyska 1130 = Batiment de la gare russe | 321.2588 320.801 + 0.458
(Station des pompes).

Nowosielica Repère établi par un officier 139.708
russe sur le bâtiment de la
gare autrichienne.

 

 

 

 

 

 

 

i
j
4
i
3
4
À

 
