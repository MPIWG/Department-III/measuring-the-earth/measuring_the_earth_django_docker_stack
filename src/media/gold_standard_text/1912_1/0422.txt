 

360

Quand le résultat de la nouvelle vérification des fils sera connu ces résultats subiront
probablement une petite correction.

Pour corriger les differences de niveau d’apres les lectures de la lunette de nivelle-
ment, on a déterminé tous les 240 m. la hauteur du repere au dessus d’un piquet en bois
enfoncé dans le sol: les différences de niveau de ces piquets ont été déterminées par un
nivellement spécial.

Mesure des angles.

Les angles ont été mesurés d’après la méthode du Général Schreiwer de la même
manière qu'à Sumatra. Les opérations sont terminés à deux stations.

Observations astronomiques.

€

Pour l'orientation du réseau géodésique on a exécuté des déterminations de latitude
et d’azimut au station Montjong Lawe P 1; le calcul des résultats n’est pas encore termine.

La différence de longitude entre le phare à Macassar et Soerabaja (Java) a été déterminée
en 1891 au moyen du télégraphe.

Détermination des altitudes.

Comme point de départ pour la mesure des altitudes servira le repère en pierre
maçonné au côté nord-ouest de l’enceinte du fort Rotterdam”, près de la ville de Ma-
cassar. Suivant l'inscription le trait horizontal se trouve à 3 m. au dessus du niveau
moyen de la mer. L’altitude sera contrôlée par des observations de marée pendant une
courte période, dont on pourra déduire le niveau moyen, les constantes de la marée à
Macassar étant connues.

A. van Lira.

sach

Ah huitani la,

 

 
