ee ~

ane

Rennes ere

61

que, lorsque l’on fera parvenir au gouvernement français par voie officielle le vœu en
question, il en soit avisé officiellement par M. le Secrétaire perpétuel.

M. le Secrétaire indique que tous les vœux émis depuis qu’il occupe le Secrétariat
ont été transmis par lui aux gouvernements intéressés par voie diplomatique, et M. Gautier
fait observer qu’en effet la Suisse en a bien été avisée officiellement.

Le vœu est adopté.

M. Albrecht présente le vœu suivant:

La Conférence est d’avis qu'après les progrès considérables réalisés dans les dernières
années, dans les méthodes pour déterminer les différences de longitude, il serait fort im-
portant de faire une nouvelle détermination de la différence de longitude entre l'Europe et
l'Amérique du Nord. Elle est convaincue qu’une telle détermination pourrait contribuer d’une
manière efficace à l’avancement de nos connaissances concernant l’histoire du développement
de notre globe terrestre.

Le vœu est adopté.

M. Turner propose le vœu suivant:

La 1% Conférence générale de l'Association géodésique internationale estime qu’il
serait hautement désirable qu’une jonction entre les triangulations de la Russie et des Indes
soit entreprise à travers la Perse aussitôt que les circonstances le permettront.

Il est d’ailleurs à espérer que le service géodésique des Indes pourra aussi compléter
la jonction à travers le Cashmire et le Pamir malgré les difficultés reconnues que présente
cette jonction.

Le vœu est adopté.

M. Muller propose le vœu suivant:

La 17e Conférence générale de l’Association géodésique internationale émet le vœu
que dans les triangulations exécutées aux Indes orientales Néerlandaises on fasse un plus
grand nombre de déterminations astronomiques de latitude et d’azimut, afin qu’elles puissent
aussi contribuer à faire avancer nos connaissances de la figure de la terre.

La proposition est adoptée.

M. Helmert propose les vœux suivants:

1° La Conférence ayant appris avec une grande satisfaction le développement des
triangulations au Chili, espère que les opérations puissent être bientôt continuées
de telle sorte qu’un prolongement de la mesure de l’arc de méridien à l’Équateur
à travers le Pérou soit possible,

20. C’est avec un grand intérêt que l'Association géodésique internationale a pris
connaissance des grandes opérations géodésiques qu’on se propose d'exécuter
dans la République Argentine et qui seront d’une haute importance pour la
science. Elle espère que les autorités compétentes pourront bientôt faire com-
mencer ces travaux et pourront prendre les mesures nécessaires pour les con-
tinuer énergiquement.

Ces deux vœux sont adoptés.

 
