313

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

TABLE X.
Northern portion of the Peninsula.
€ | Computed deflection in
meridian Observed
Station Latitude Longitude Height deflection
in meridian
Topographic ‘ Hayford ’
Bithnek  .. 27005 (RAI 235 metres + 6” + 1” + 8”
Chendwar ... 29 Of. 60 29 | 4 5 — 11” 0 a
Daadhanr | 22938 17749 |, 500. + 27 0 De
Gurwant . | 24 | 82290" | 62 ae +) oe
Hurilaong ... 245 2 84/04 | 11 5 = 2 3 157
Kankra ee 25° 38 76° 10 290 7 =» — 1" + 4”
Khankaria ... 24° 37 20 504 110, - 5” 0 + 6”
Kesri che 25° 47’ 17° 43 446  ,, + 2” +1" + 10”
Pahargarh ... 24° 56° 17° 44° 492 5 + 0 +1" + CE
Rewat os 26. 54 ti 1 463 „ + 47 0 + 6”
Saugor = 235 507 18 A192 Ol@ 4 + 4” +1" + 4"
Thob 2. 0% 1.22 25° Da 5 0 — 1 + 8"
TABLE XI.
Belt of negative deflections across the central portion of the Peninsula.
Computed deflection in
meridian : | Observed
Station Latitude Longitude Height deflection
| in meridian
Topographic ‘ Hayford’

E Badgaon _... 20° 44’ 70 SO! 338 metres =. 0 D
= Boum > 1790. 18 34 99a es — 6” — 1" — 5”
| Chaniana ... 24 7 2.55 20 — 47. — 3" — 8”
IE Colaba ER 18° 54’ 127 01 Da 5 — 4” 0 = OF
® Cuttack a 207 29 85° 54° 40: ;, — 237% =o ee
F Deesa a 24° 15 oe 127 132 , — 6” — l” — 4”
Kem a. or Il 15 21 DD Op. — 3 0 — 2”
Khanpisura... 18° 46’ 14 49 DD — 5” = 97 — 7"
Ladi ee Dar 2 14 45. 563 - , + 1° a 209%
Valvadi nz 20° 44’ 15 14’ 30 | + 4” + 1” — 4”
Lingmara ... 21° 43’ SO; lay 420, — 6” le — 5”
F Voi BR 19e 7 Ce xi 132 2, — 2" + — 47
Ë Mal oe 18° 47’ 84° 33’ 146. = 25% 0% ee
Mandvi Lee 18 98 13.95 1946 , - 5 — 1" u
Nialamarı | 17,2 19° 46. 343% 5, 0 = D. 1.
Nitali oe Se Toe O37 D | 0 | = 9.
Parampudi ... 7 19 81° 15 207 — 16" m — 57
Rawal ie 18° 32° 83° 36° 266 ,, = 24% = 37 eo
Sanjib = | 1101 82° 44 643 ,, I BAUME D — 9” — 6”
Vanakonda .... 17 0 09 29 499: - ,; Li Qi 1 — 6”
Vizagapatam 18 89216. De — 287% — 5” — 5"
Waltair a ii AS , 8922 6 , 0e = 6” =

 

* Stations situated on the east coast of India,

PME EEE AN COOP Tr

PETER

ir

mr

 
