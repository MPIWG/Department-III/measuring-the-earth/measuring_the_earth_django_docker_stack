 

 

436

j
j
j
j
j
j

ihrer Längsrichtung parallel und normal verlaufen, und entsprachen auch bezüglich der
Grösse der Krümmungsstörungen den Forderungen theoretischer Berechnung.

Zur näheren Erklärung dieser Verhältnisse müssen wir die Grösse: & zu Rate ziehen.
Bezeichnen wir die Koordinaten eines Punktes längs und normal der Talrichtung mit / und £,
die ebenso gerichteten Hauptkrümmungsradien mit p, und p,, so wird:

: ee

 

u 230 ie I )
= I
Pt fi

wo für g der Wert der Beschleunigung in der Breite von 46° 37’ und einer Höhe von
1500 Metern zu setzen ist, also:

9 — 980,3.

Der grésste Wert von 2, den wir am Südrande des Tales, dicht am Lago bianco fanden,
war nun: % — 1487.10.’0.G5, der kleinste in seinem mittleren Teile; A = 7354.10
0.G,S. Dementsprechend erhalten wir am Südrande des Tales:

>

iL 1
LOU
Pi Pt
und in der Talmitte:
1 EE
=. 142,102
Pl p!

Nehmen wir nun, wie oben, an, dass der Krümmungsradius p in der Längsrichtung |
des Tales wenig von seinem normalen Werte verschieden sei und setzen dem Orte und
Azimute entsprechend:

ie

Pl

erde

so ergibt sich am Südrande des Tales:

À
— = 0,0495.10—? also p, — 206685.105c

Pt

und in der Talmitte:

— = 0,8177.10- also 6; == 12267.10%
t

Dieser Krümmungsradius quer zum Tale erreicht somit am Rande den dreissigfachen
und in der Mitte noch nahezu den zweifachen Wert seiner normalen Grösse.

Weitere Eigentümlichkeiten dieses so stark verbogenen Stückes der Niveaufläche sollen
gelegentlich der vollständigen Veröffentlichung der Beobachtungen besprochen werden.

 
