TRETEN

ere

tt pq

#
re
t
Ë
F
t

al

Erweiterung des Zentralbureaus, die auf internationale Kosten erfolgen müßte, schreiten
will, so wird es dazu hervorragender Mitwirkung der einzelnen Länder selbst bedürfen,
die ich auch aus praktischen Gründen für das beste Mittel halte. Welche Menge Arbeit
da zu bewältigen ist, zeigt die im letzten Dezennium in den Vereinigten Staaten von
Amerika unter Trrmanxs und Havrorvs Leitung von der „Coast and Geodetic Survey“
ausgeführte Bestimmung des Geoids und Erdellipsoids.

Die Aufgabe der systematischen Lotabweichungsberechnungen führte das Zentral-
bureau selbstverständlich auch dazu, sich eingehend mit dem europäischen Dreiecks-
netz zu beschäftigen, wobei General Ferkero als Berichterstatter für die Triangulationen
bis zum Jahre 1900 mehrfach Anregungen gab. Auf seinen Wunsch stellte 1888 im
Zentralbureau A. Borscu die Grundlinien nach Größe und Azimut für seine große euro-
päische Dreieckskarte zusammen und gab 1887 eine Liste der noch vorhandenen Prä-
zisions-Toisenmaßstäbe; sodann untersuchte das Zentralbureau 1891 die Verbindung der
Dreiecksnetze zwischen den Nachbarländern. Auf der Allgemeinen Konferenz im Jahre
1892 leste das Zentralbureau ferner einen ausführlichen, von F. Kümen bearbeiteten
Bericht über die Vergleichung der benachbarten Grundlinien mittels der sie verbindenden
Dreiecksketten vor, und zu dem Triangulationsbericht von 1895 gab es eine Zusammen-
stellung über den mittleren Fehler einiger Basisvergrößerungen.

Auf Anregung des Zentralbureaus wurden für den Parallel in 48° Breite
neue Dreiecke in Rumänien und Südwestrußland, sowie in Südbayern und Tirol gelegt.

Zur Darstellung des Standes der Publikationen der Dreiecksmessungen in Europa
dienten 1888, 1895 und 1903 im Zentralbureau entworfene Karten. Weitere Ergänzungen
zu dem Ferreroschen Triangulationsbericht wurden vom Zentralbureau in den Verhand-
lungen der Allgemeinen Konferenz von 1900 (Polygon um das Mittelländische Meer), 1903
und 1909 gegeben (Hernerr, A. Borsce, Kriaur, Gaus).

Zu den im vorstehenden genannten Arbeiten, die sich auf die geometrische Be-
stimmung der Erdfigur, auf die eigentlichen Gradmessungen beziehen, kamen mit der Zeit
in steigendem Maße Schwerkraftsbestimmungen. Schon die Erste Allgemeine Kon-
ferenz nahm solche in ihren Arbeitsplan auf; recht in Gang kam dieser Zweig der Erd-
messung aber erst nach den epochemachenden Arbeiten von Derrorers und v. Srerneck.
Besonders die von Srernsox zuerst benutzten und dann auch von Menpestarz verwendeten
inyariablen Halbsekundenpendel gaben infolge der Einfachheit und Sicherheit ihrer An-
wendung überall den Anreiz zu Schwerkraftsmessungen. Sobald das Zentralbureau mit
dem Geodätischen Institut nach Potsdam in das neue Dienstgebäude übergesiedelt war,
wandte es sich auch diesem Zweig der Erdmessung zu. Verschiedene Abänderungen in
der Konstruktion wurden ausgeführt und ganz besondere Aufmerksamkeit den Methoden
zur Bestimmung des Mitschwingens der Konsole infolge der Elastizität des Stativs bezw.
des Pfeilers und Untergrundes gewidmet. So konnte die Unsicherheit in der relativen

Bestimmung der Beschleunigung der Schwerkraft auf wenig mehr als ein Milliontel ihres
Betrags eingeschränkt werden. Mit dieser Genauigkeit sind im preußischen Staat über
200 Stationen bestimmt worden (Haasemany, Borrass, Scuumans) und 21 Stationen im Aus-

7

 
