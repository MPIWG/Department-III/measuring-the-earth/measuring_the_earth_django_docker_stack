 

rin

TOOT TETRA OWT CUNT TT eT

wr

ag.

La colatitude obtenue par les mémes observations & l’astrolabe, comprenant 25 étoiles
fondamentales environ pour chaque série est:

2semale ee 39°6'30",30
26 mal meer 30. ,05°
Qi man ne. 30 ‚10
28 ma on. 30 ‚25

39%6 202,17 + 01,09

4° Paris-Bron. —- Par le même procédé et avec le même matériel colonial, le Service
Géographique a déterminé en 1911, la longitude de la station de Bron, dont l'emplacement
est situé à 268 mètres du terme ouest de la base de Lyon et dont les coordonnées astro-
nomiques seront ramenées à ce terme.. Les opérations, faites après la mesure de la base,
ont duré 4 jours environ. Les observations seront prochainement réduites.

5° Longitude par la télégraphie sans fil dans le Sahel d'Alger. — Les signaux horaires
réguliers de l'Observatoire de Paris, émis deux fois par jour par la Tour Eiffel, ne sont
en général employés que par les navigateurs pour faire le point à la mer.

Il était nécessaire de rechercher dans quelle mesure un explorateur, qui en raison des
difficultés de transport aux colonies ne disposerait que d’un matériel léger (récepteur, antenne
de campagne et astrolabe), pourrait utiliser soit les signaux horaires, soit les signaux rythmés,

C’est dans ce but que la Section de Géodésie du Service Géographique de l’Armée
a procédé, en fin 1911 et commencement 1912, à des expériences dans les environs d'Alger.
Les stations choisies furent celles de Colonne-Voirol, Bouzaréa, Dely-Ibrahim, Cheraga et
Amirauté, les résultats devant être utilisés pour compléter l'étude de la déviation de la
verticale dans cette région, étude dont les premiers résultats sont consignés dans le Rapport
de la 16e Conférence (page 179). Pour ces cinq stations, la détermination des différences
de longitude entre Paris a été faite par les signaux radiotélégraphiques rythmés émis par
la Tour Eiffel de 9 heures à 9 h. 35 du soir suivant un programme bien défini, la réception
de ces signaux se faisant à Paris comme à Alger par la méthode des coïncidences. Pour
trois de ces stations (Chéraga, Amirauté et Voirol), les différences de longitude ont été
déterminées en employant les signaux horaires et les résultats sont assez satisfaisants pour
qu'on puisse accepter l'emploi de cette méthode aux colonies, dans toutes les circonstances
où on sera forcé de le faire.

A Paris, les coincidences étaient prises à l'Observatoire entre les signaux radiotélé-
graphiques et les battements directs de la pendule de la salle méridienne, l'état de la pendule
étant fourni par l'Observatoire; aux stations des environs d'Alger, les coincidences étaient
prises avec le chronomètre, dont l’état était fourni, avant et après les réceptions radiotélé-
graphiques, par des observations à l’astrolabe d’une durée de 1 heure à 1 h. 1/2.

L’antenne en parapluie obtenue à l’aide d’un mât démontable a été installée aux
stations de Bouzaréa, Dely-Ibrahim et Chéraga. À Colonne-Voirol, les conditions d'installation
étant défavorables, on eut recours à une antenne supplémentaire; enfin à l’Amirauté, la

proximité du phare permit d’utiliser ce dernier comme porte-antenne.
* 34

 
