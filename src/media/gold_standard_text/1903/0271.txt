 

=

TEER TTR ORT TTT RTT Pan TTT

245

x x

N°. 3 et montrent que l’erreur moyenne en question peut monter à peu près jusqu’à 0.007
centimètres. [a cause de cette erreur relativement grande est sans doute dû à la circonstance
que les variations des pendules sont loin d'être aussi régulières qu’on les a supposées, en
employant une formule de »contraction” basée sur des observations exécutées par groupes
à de courts intervalles pendant toute la durée de la campagne.

Pour remédier à cet inconvénient, le lieutenant Johansen, qui à lui seul a fait
toutes les observations dans les 9 stations de 1902, a choisi une de ces 9 stations, qu'il
a employée comme station centrale, L’horloge & pendule y a été établie pour toute la durée
de la campagne et fut lié successivement aux stations d’observation par des lignes télé-
graphiques. Tous les soirs, quand l’état de l'atmosphère l’a permis, M. Johansen y a fait des
observations astronomiques pour la détermination de l’heure et, de temps en temps, des
observations de pendule également distribuées sur toute la durée de la campagne. Par ce
procédé, on à pu suivre d’une manière plus satisfaisante les variations des pendules et ap-
pliquer par conséquent une correction de contraction plus concordante qu'auparavant avec
les variations réelles de ces pendules. La conséquence en a été une réduction de l'erreur
moyenne en question à une valeur un peu supérieure à 0.005 centimètres.

Cependant, les variations observées en 1902 montraient des irrégularités dans la
contraction” qui firent soupçonner que les couteaux d’agate n'étaient pas fixés assez so-
lidement aux pendules. À la suite d’une correspondance avec M. Helmert, les pendules furent
envoyés à Potsdam où le mécanien de l’Institut géodésique, M. Fechner, a corrigé ce défaut,

C'est avec les pendules modifiés que M. Johansen a fait ses observations en 1903
dans 11 stations en Fionie. Les calculs ne sont pas assez avancés pour permettre de donner
les résultats définitifs de cette campagne, mais il semble que l'amélioration a été effective
et que les variations des pendules provenant de la »contraction”’ sont moindres qu'auparavant.

LV. MARÉGRAPHES.

En partant de la cote 35.553 + NN pour le repère Prussien N°. 8609 et en se
servant des observations des marégraphes jusqu’à la fin de l’année 1902, on obtient les
résultats suivants.

Esbjerg: 20. 0 0 NN — O. 200
Hirishals. 0 » — O0. 288
Frederikshavn. : 2, . 3. >» — O0. 28
Aarhus.) 60.0 » — 0. 269
Fredericia ,., 2. 2 > — 0. 258
Slipshava 2. 02.5. 05 » —Q. 248
Koss se 2. »> — 0. 220
Hornbaks ss ee > 0) 292
Kijébentayve, 0, 0: » —0. 216
Gjedser st » — 0, 235,
Moyenne des 10 marégraphes . . . NN — O. 245.

 
