nn in |

da PTIT

FR TT

TPR OT OTF WT

TOR

TEN

Xil. MESSUNGEN DURCH FRANZÖSISCHE BEOBACHTER.
XlIc. MESSUNGEN DURCH DEN SERVICE GÉOGRAPHIQUE.

In dem der XIV. Allgemeinen Conferenz erstatteten Rapport sur les travaux géodé-
siques exécutés par le Service Géographique de l'Armée de 1900 à 1903 teilt Herr Komman-
dant Bourceors mit, dass Schweremessungen auf 3 Stationen in der Umgebung Algiers, wo
sich beträchtliche Lotabweichungen gezeigt haben, angestellt worden sind.

Ausserdem hat der Service Géographique, gelegentlich der Neumessung des Meridian-
bogens von Quito, auch Schweremessungen in Riobamba ausgeführt.

Die Resultate dieser Arbeiten liegen zur Zeit noch nicht vor.

Xlle. MESSUNGEN DURCH VERSCHIEDENE BEOBACHTER MIT PENDELN DES
SERVICE GÉOGRAPHIQUE.

Herr Professor M. I. Corzer hat in den Comptes rendus von 1902, T. CXXXV,
p. 774 und p. 956, zwei neue Mitteilungen über seine relativen Schweremessungen längs
des Parallèle moyen veröffentlicht. Die erste Mitteilung (p. 774) enthält eine Tabelle der
beobachteten Schwingungsdauern des benutzten Reversionspendels und eine Untersuchung
über ihre Genauigkeit; in der zweiten werden die abgeleiteten g gegeben, und ihre auf den
Meereshorizont reduzierten Werte mit der Formel von Derrorezs verglichen. Wir entnehmen
daraus die nachstehenden g9-Werte nebst den zu ihrer Reduktion erforderlichen Daten.

D H 6 g
Io Marseille 0 43°18'3 bl 20
2, Cap Ferre . 44 38.8 6.2 695
3. Bordeaux... . 42 503 11 2 614
2, Valencee. 2... . 44 56 125 19 619
5, La Berarde 2: AA 530 1738 2. 200
b.Aumlae.. 0... 44 56.8 00 208 540
(. SainteAgreve: 2 | 45 0.0 1058 27 438
8. be bautaret . ...._ 45 2.9 2058 204 124
A 45 A0 De 2 588
10; Grenoble, 45 45 N 4 210 256 593
11. Saint-Pierre-le-Chastel. . . 45 48 53 243 962
12. Pans, Observatoire. 2 = 48 50.2 60> 2 981.000

(Referenz-Station)

 
