 

 

Pend. 49

— 56

1901. Strassburg + 2117
Schlettstadt 2098
Weiler 2116
Markirch 2400
Leberau 2107
Kaysersberg 2092,
Diedolshausen 2404
Colmar 2083
Drei Ahren 2095
Strassburg 2117

Die Anderungen des mittleren Pendels wurden der Zeit proportional angenommen.
Wie aus den mitgeteilten Werten des Mitschwingens hervorgeht, sind diese für ein Schnei-
der’sches Wandstativ keineswegs konstant oder gar verschwindend klein. Herr Prof. BECKER
bezeichnet die Messungen von 1901 als unsicher.

Zu den Messungen von 1903 wurde wieder das Sc
es vorher vom Mechaniker des Geodätischen Instituts, M. Fronner, verbessert worden war,
mit A neuen Fechner’schen Pendeln benutzt. Unter diesen zeigte F, bei den Ausgangs-
beobachtungen in Strassburg eine plötzliche Änderung von 56 Einh. d, 7. Dez.; später hat

168

49

—61

+ 5903
5885

5900
5890
5899
5889
5898
5881
5902

5918

56—64
+ 3786

es sich, wie auch die übrigen 3 Pendel, gut gehalten.

1903. Strassburg, März 20—27

Pend. F,

» April 28—Mai 9:
Anderung :

Fi —Fo F,—F3
1903. Strassburg 0 5%
» — 798
Zabern 574 819
Saarburg 572 805
Dieuze 568 199
Chäteau-Salins 563 800
Saargemünd 565 800
Saarunion 570 199
Strassburg 579 2197

Der Sprung von F, ist zwischen März 25 und 27 eingetreten. Am letztgenannten
Tage war das Wandstativ an einem absichtlich schwach gebauten Pfeiler befestigt. Der
erste Wert von F, ist das Mittel aus 8 Bestimmungen, der zweite (05.5077822) ist eine

03.507 7024

Pend. Fy
03.507 7591
7018 1593
— 6 +2
F,—F, Fo — F3
— F9 — 287
a 934
520 245
514 233
520 Don
514 237
513 238
SI 229
520 299

3787
3784
3790
3792
3191
3797
3798
3807
3801

 

Mitschwingen

hneider’sche Wandstativ, nachdem

Pend. F, Pend. F, Sätze
78
05.507 ot 05.507 75386 9
7815 7538 9
G7) +2
We, BB Fy Mitschwingen
+55 + 342 3 Wand
— 286 10 Pfeiler
54 299 6
58 294 6
48 279 4
49 286 6
52 287 6
By 286 5
Ba) Dun. 5 ) Wand
42 ) Pfeiler

 

 

 

Se
=

ran id 6 Mala à

Lbnuiuci

bi dae dd

ul M ha da at at e nu Be

|
