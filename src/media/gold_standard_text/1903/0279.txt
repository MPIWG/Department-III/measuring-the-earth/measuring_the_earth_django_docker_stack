 

rpm TNT

 

TOT TT a

253

Rapport de M. Darwin sur les marégraphes (Annexe B XVI). | >
Resolutions ä propos de la decharge des I pour l’exercice de 1900, 1901 ei 1902 ;
Proposition de M. Helmert en ans ae

Cinquiéme séance, Jeudi, 13 Aoüt 1903 .

Lecture du procès-verbal de la quatrième séance.

Communications administratives. :

Rapports présentés par M. Heuvelink sur Ian ne aux ee The . aux ‘aly orientale:
(Annexe A Villa et A VIII)

Communication de M. Helmert sur les travaux péodésiques le ie a on. ae 30° de lene
en Afrique :

Communication de M. Durban au ae ces aaa :

Discussion de MM. Züttmann, Darwin et Helmert sur les ann ne la Den de Malacın
au Birma et ä Singapore

Proposition de M. le Secrétaire au lei de: fava se en Afrique”

Communication de M. Lallemand sur un théodolite . :

Note de MM. Benoit et Guillaume sur Vemploi des fils Tagen neice par M. Te Général ad
(Annexe B Wil) - : :

Propositions de M. le Président à de M. Fond co nerient cone nl Digeusion: sur Te rapport
de MM. Benoit et Guillaume

Rapport de M. le Général Bassot sur le mesures i bas une B XIV).

Rapport de M. Lallemand sur les nivellements (Annexe B XV) .

Observations de M. Haid sur les nivellements aux frontières de Bade et de ja ie

Rapport de M. Borsch sur les déviations de la verticale (Annexe B XV HE)

Observations et proposition de M. Helmert au sujet de ce rapport.

Lecture du projet des travaux du bureau central pour les prochaines années (Annexe B ID

Suspension de la séance .

Reprise de la séance .

Remarque de M. Foerster au u des do sur I ne ne ces ie rapper
de la commission des finances; observations de M. le Président et de M. le Secrétaire .

Discussion sur la proposition de la commission des finances concernant le service international des
latitudes et sur la continuation de la dotation des ftats pour une nouvelle période.

Discussion sur la proposition de la commission des finances concernant l'étude de la ae. en
mer et sur les cötes. :

Les propositions de la commission abs fuaneen pour a ean endin ine des exercices pooh:
sont adoptées . : ee

Proposition de M. on concernant a contingation clu ue international es ltd après
l’année 1906 .

Cette proposition, d’après une ‘nouvelle redaction proposée par MM. Foerster Hee a Poincaré
est adoptée es

Remplacement de M. le Gr Basser par M. i Comm Bourges comme rapporteur bone] sur
les mesures des bases :

Proposition de M. Lallemand sur lie de a Amwehe.aa oo nee et eh sur elle en

Allocution de M. le Président pour remercier 8. M. le Roi de Danemark, son gouvernement, le pré-
sident du Landsting, M. le Général Zachariae et le comité d'organisation

Réponse du Général Zachariae et du Col. Rasmussen

Votes de remerciement pour M. le Président et M. le Sebréte 0

Clôture de la XIVe conférence générale

Page
39
39
39

40—51

40
40

40—4]

41
41—42

42
42
42

42

43—44,
44:
44
44
44,
44—45
45
45
46

46
46— 48
48 —49

49

49

49

49—50
50

50—51
50—51
51
51

 
