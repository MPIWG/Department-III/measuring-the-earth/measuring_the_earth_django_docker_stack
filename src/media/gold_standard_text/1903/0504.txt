 

BE ay

BEILAGE B XIII.

BERICHT
über die Triangulationen
VON

F. R. HELMERT u. L. KRÜGER,

Der letzte grosse Bericht des Generals Frrrero über den Stand der Triangulationen
der Internationalen Erdmessung ist im Jahre 1898 in den Stuttgarter Verhandlungen er-
schienen. Er enthält, wie bekannt, Angaben über die Hauptdreieckspunkte der einzelnen
Ketten und Netze, ihre geographischen Positionen, das Jahr der Stationsbeobachtung, die
Namen der Beobachter und die von ihnen benutzten Instrumente u. a. m. Die Genauigkeit
der Messungsergebnisse wird, ausser durch eine besondere Zusammenstellung der aus den
Ausgleichungen folgenden mittlern Fehler, durch die Dreiecksabschlüsse der Winkel und
den aus ihnen nach der internationalen Näherungsformel abgeleiteten mittleren Winkelfehler
gekennzeichnet. Den Übersichten der einzelnen Länder sind historische Bemerkungen zu-
gefiiot, denen General Ferrero grossen Wert beimass. Eine Karte, welche die über Europa
und Nordafrika gelegten Dreiecke zur Darstellung bringt, bildet den Schluss des Berichts.

Nach FErrero’s Tode übernahm es das Centralbureau, in derselben Weise wie bisher,
von Zeit zu Zeit einen Abriss über das Fortschreiten der Triangulierung zu bringen.

Auf die Anfragen an die Herren Delegierten über die Arbeiten zur Ergänzung und
Weiterführung der Dreiecksmessungen in ihren Ländern sind eine Reihe von Berichten
eingelaufen, die nachstehend zum Abdruck gelangen. Zu ihnen sei noch, zum Teil an der
Hand der Begleitschreiben, kurz folgendes bemerkt.

In Österreich-Ungarn hat das militär-geographische Institut die Messungen der
Hauptdreiecke beendigt und die Ergebnisse der Netzausgleichung bereits fiir die ganze
Monarchie mit Ausnahme von Tirol veröffentlicht.

Ebenso sind in Deutschland die Triangulationen erster Ordnung im wesentlichen
zu Einde geführt und die Ausgleichungen der einzelnen Netze publiziert worden. Die baye-
rische Erdmessungs-Kommission hat, einem Wunsche des Centralbureaus entsprechend, im
Interesse der Längengradmessung in 47° u. 48° Breite noch einige Anschlussmessungen für
ihre südwestlichen Dreiecke an das württembergische Netz und für ihre südöstlichen Dreiecke

 

Si
=
=
a
x
=
=
x

11 11 jee

 

2. miissen nas Al |

 
