 

458

Résidus. — Dans l'appareil que j'ai construit, la lame est immobilisée pour le
transport à peu près dans sa position d'équilibre naturelle sans charge; quand on la libère,
elle doit donc reprendre sa flexion de 18° environ; ce retour se fait lentement, et exige
plusieurs heures. C’est beaucoup trop, et il y a évidemment à gagner en immobilisant la
lame pour le transport, sensiblement dans la position où elle s'arrête pour les mesures.
Mon appareil actuel, qui a déjà subi de nombreux remaniements pour les essais successifs
ne se prête pas bien à cet arrangement; je vais pourtant en faire l'essai.

Voiei done où j'en suis:

La preeision des lectures sur la lame libre atteint le 200000° pour g, en un quart

d’heure ou vingt minutes, quand l'appareil est en place, et la lame en liberté depuis au-

moins 12 heures. Il paraît possible d'obtenir ce résultat plus rapidement, au moyen d’un
amortisseur convenable que j'essayerai l'hiver prochain. Je pense qu'en immobilisant la
lame à peu pres dans sa position de service, les résidus seront minimes et disparaîtront
rapidement.

L'influence de la température. paraît, assez minime pour être facile à corriger.

L'appareil entier ne pèse pas 4 kilogrammes; une table bien calée, l'appui d’une
fenêtre, une console ou une cheminée suffisent comme supports; on peut done s'installer
instantanément dans une salle d'école ou même d’auberge quelconque. Mon intention est
d'étudier le mode d'arrêt et l'encastrement, jusqu’à ce que j'aie obtenu la suppression des
résidus, ou leur régularité assez complète pour qu’une station n’exige pas plus d’une heure,
et j'ai bon espoir d’y arriver.

Etalonnage. — VL'étalonnage exact de l'appareil pourra être effectué de deux ma-
nieres différentes,

19, par des mesures répétées entre deux stations où la différence relative de gravité
est bien déterminée par le pendule.

A titre de renseignement sur la sensibilité, je me propose, lorsque je serai bien
maître des résidus, de faire des séries multipliées en haut et en bas de la tour Eifel.

20, L'appareil peut être étalonné directement en valeur absolue. La construction
permet la lecture exacte de la flexion totale de la lame et celle de l'influence d’une sur-
charge, soit en angles, soit en déplacements du repère.

II. MESURE DES PETITS ANGLES PAR LA DOUBLE REFRACTION.

Principe. — Entre un polariseur et un analyseur à l'extinction, à 45° du plan de
rotation, sont placés un spath fixe épais, et une lame demie onde fixe, suivis d’un spath
mobile parfaitement égal au spath fixe.

Si l’égalité et le parallélisme des spaths sont parfaits, ainsi que la planéité de leurs
faces, leur ensemble, séparé par la lame demie onde à 45° qui échange les rayons ordinaire
et extraordinaire, donne une différence de marche nulle pour tous les rayons (à la dispersion
près de la lame demie onde seule). Observé en lumière parallèle, l’ensemble conserve l'ex-

 

=
u
3:
as
=e
=:
FE
a

3
x:
a
3
u
Bz

Abii bebe tele

Ludo iahs

DATENT

masses nid

 
