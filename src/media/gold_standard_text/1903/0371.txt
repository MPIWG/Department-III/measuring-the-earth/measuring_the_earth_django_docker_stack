Tin m en

 

WL ANT

it

TT

Pe N Tg ar

Fir

87

dans la mesure de l’arc de méridien qu’une mission française y exécute en ce moment, Cet
étalonnage a été exécuté, en effet, au printemps de 1901, par le personnel du Bureau in-
ternational, avec la collaboration de deux officiers du service, le capitaine Lacomse et le
lieutenant Perrier. La règle a ensuite servi à la mission pour la mesure de la base de
Riobamba, prés de Quito; elle a été ensuite rapportée en France par le commandant Bour-
sois, chef de la mission, et est revenue à Breteuil au commencement de 1903, pour y être
soumise à un nouvel étalonnage: celui-ci a été exécuté avec le concours de deux autres
officiers du service, les capitaines PréÉvosr et AuBermin.

Mais, dans l'intervalle, nous avions reçu une autre règle géodésique appartenant à
la Direction générale des arpentages d'Égypte, qui nous l’avait envoyée pour en faire l’étude
complète. Cette règle, construite par la maison BRUNNER, est un appareil bimétallique exac-
tement semblable à celui de l’Institut géographique d'Espagne, et aussi, à quelques détails
de construction près, à ceux des Services géographiques de France et d’Allemagne. Il s’agis-
sait, dans ce cas, non seulement d’un étalonnage à la température ambiante, qui est suffi-
sant pour des régles dont les dilatations ont été déja déterminées antérieurement, mais d’une
étude compléte, comprenant aussi la mesure de ces dilatations. Or, lorsque, il y a dix-sept
ans environ, nous avions fait au Bureau la détermination des premiers appareils de ce genre
qui nous aient été soumis, nous avions tâché de copier aussi exactement que possible les
procédés d’expérimentation que nous avions adoptés pour les prototypes fondamentaux en
platine iridié. Nous avons fait ainsi, à cette époque et postérieurement, sur les règles géo-
désiques de France, d'Allemagne et d’Espagne, un certain nombre de séries, ces règles étant
immergées dans une solution d'un sel alcalin, liquide qui, après divers essais très laborieux,
nous avait paru préférable à tout autre; l’eau pure étant dans ce cas exclue, parce qu’elle
eût attaqué presque immédiatement et profondément détérioré les nombreuses pièces de fer
ou d’acier qui, entrent dans la construction de ces instruments. Cette immersion dans un
liquide (que nous pratiquons constamment dans les études des prototypes en platine ou des

étalons en nickel ou acier-nickel) à l’avantage de donner une incomparable garantie au point |

de vue de l’exactitude des mesures des températures. Il à aussi celui de permettre de passer,
dans un temps relativement court (en quelques semaines), par plusieurs températures entre
des limites assez étendues, en chauffant ou refroidissant artificiellement le bain où plongent
les rêgles. Par contre, il a le grave inconvénient, avec des appareils d’une structure com-
pliquée et délicate, dans la constitution desquels entrent de nombreuses pièces qui doivent
être très exactement ajustées tout en permettant des glissements absolument libres des
règles sur leurs supports ou entre leurs guides, d’encrasser ces divers organes, dont le net-
toyage parfait est ensuite très difficile, sinon impossible, sans un démontage eb un remon-
tage complets, qui annuleraient sûrement la valeur de l'étude faite. L’alcalinité du bain n’est
d’ailleurs qu’un palliatif, qui ne réussit assez souvent qu'’imparfaitement à empêcher les
pièces oxydables d’être attaquées plus ou moins profondément, Malgré tous nos soins et
les précautions que nous avions prises, nous avons eu, à ces divers points de vue, dans
certaines de nos opérations, des déboires assez sérieux pour que nous ayons cru devoir

raser ac

Sr

TR D ee

EST TE

er:

;
|
f
|
N

 

 
