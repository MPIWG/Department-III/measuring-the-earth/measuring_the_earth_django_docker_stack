Tin ee a en ro

 

=

F DOCTORAT RCD TT I TOUT

Wiederaufnahme der Sitzung 8 Uhr 10 Minuten.

Der Herr Präsident fordert Herrn Pörstör auf zur Mittheilung des Berichts der
Finanzcommission.

Herr Förster macht zuerst die Bemerkung, dass, nach Art. 11 der Übereinkunft, die
Abstimmungen bei allen geschäftlichen Enntscheidungen geschehen nach Staaten, wobei jeder
Staat eine Stimme hat. Dagegen haben in der Sitzung von 10 August an den Abstim-
mungen über die zwei Anträge: dem Herrn Director des Centralbureaus für seine Verwal-
tung volle Entlastung zu ertheilen, und die administrative Oberleitung durch das Präsidium
der internationalen Erdmessung zu billigen, alle Mitglieder theilgenommen, und nicht einer
für jeden Staat.

Herr Förster fragt den Herr Präsidenten, ob es nicht nöthig sei in das Protokoll
eine Bemerkung in diesem Sinne aufnehmen zu lassen, damit das in Art. 11 der Überein-
kunft niedergelegte Princip hoch gehalten würde.

Der Herr Président meint, dass, da alle Delegirten einstimmig dasselbe Votum abge-
geben haben, auch kein Zweitel tiber die Abstimmung nach Staaten übrigbleibt. Herr Aörster’s
Bemerkung wird in das Protokoll aufgenommen werden.

Der Secretär ist derselben Meinung, es würde überflüssig sein eine Erklärung, wie
Herr Förster vorgeschlagen hat, abzugeben.

Der Herr Präsident bittet Herrn Förster seinen Bericht weiter fortzusetzen,

Herr Förster erinnert, dass nach Artikel 13 der Übereinkunft, in gemischten oder
zweifelhaften Fällen die Abstimmung nach Staaten erfolgen muss, sobald dies von sämmt-
lichen Delegirten eines Staates verlangt wird. Nun gehören die Anträge am Schlusse des
Finanzberichts in gewisser Beziehung zu den gemischten Fällen, theilweise wissenschaftlicher,
theilweise administrativer Art, und da sich vielleicht Meinungsverschiedenheiten ergeben
werden, muss man bestimmen ob die Abstimmung nach Staaten geschehen soll.

Der Herr Präsident meint, es sei nicht nöthig sich schon jetzt über die Art der
Abstimmung auszusprechen; er schlägt vor die Anträge der Commission Artikel nach Artikel
zu besprechen, und danach den Abstimmungsmodus zu bestimmen. =

Da alle Delegirten den gedruckten Bericht haben lesen können, fragt Herr Förster,
ob es noch nöthig sei den ganzen Bericht vorzulesen. |

Der Herr Präsident glaubt, dass die Vorlesung, die übrigens von keinem der Dele-
girten verlangt wird, überflüssig ist. Herr Aörster theilt darauf folgenden ersten Antrag
der Finanzcommission mit.

1°. Der internationale Breitendienst wird bis zum Schlusse des Jahres 1906 fortge-
setzt werden; dieser Zeitpunkt wird bestimmt durch den Umstand, dass, nach der Überein-
kunft, über die Frage der Dotation der internationalen Erdmessung, vom Jahre 1907 ab,
von den sämmtlichen betheiligten Staaten ein neuer Beschluss gefasst werden muss.

Im Anschluss hieran bemerkt der Berichterstatter folgendes:

Nach unserer Übereinkunft ist eine jährliche Dotation von 60000 Mark bewilligt,
aber für eine dauernde oder vorübergehende Erhöhung dieser Summe bedarf es einer

 
