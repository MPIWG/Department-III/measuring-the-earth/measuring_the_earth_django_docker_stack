 

{
rt
na
a

38
les observations elles-mêmes n’était pas aussi satisfaisant qu'aux autres stations: à Tschardjoui
les dimensions de l'instrument sont inférieures à celles des autres télescopes zénithaux (ouver-
ture de l'objectif 68 mm., distance focale 87 cm.) et les variations de la température y
sont plus grandes (la température maximum pendant les observations + 32°.0, le 5 Juillet
1900, la température minimum — 23°.8, le 21 Janvier 1900) mais ces circonstances
ne suffisaient pas à expliquer entièrement le désaccord entre les observations. Afin de
trouver la source des erreurs, l’instrument fut examiné consciencieusement dans toutes
ses parties au mois d’Aout 1901, et on constata que les deux niveaux fonctionnaient
mal, probablement à cause de petites particules déposées à la surface intérieure des fioles.
On put constater aussi que l'éclairage au moyen de lampes exerçait une influence nui-
sible sur la qualité des observations. Cet éclairage était probablement aussi la cause de
la grande différence de température à lintérieur et à l'extérieur, qui montait jusqu'à 4°.7,
nonobstant la grande largeur des trappes de 1 mètre. L’éclairage fut changé et les niveaux
employés jusque là furent remplacés par d’autres. La comparaison des résultats avant et
après ces changements montre en effet une telle augmentation de la précision, qu'on peut
considérer les causes nuisibles comme éliminées. Les observations à Tschardjoui, nonobstant
les dimensions moindres de l'instrument, auront après les changements le même poids que
celles qui seront faites aux autres stations.

De ce que nous avons dit il résulte que ce ne sera que depuis le commencement
de 1902 qu’on pourra considérer les résultats des six stations comme exempts d’erreurs
systématiques. Il nous paraît donc indiqué de considérer séparément les observations avant
et après cette époque, et on commencera sous peu ‘la réduction et l'étude de toutes les obser-
vations jusqu'à la fin de 1901. Les résultats seront communiqués dans une publication
détaillée du Bureau central, qui contiendra aussi toutes les communications concernant
les stations que le Bureau central aura reçues.

On avait déja commencé ce travail au printemps de 1901, mais, comme nous l’avons
dit, nous avons dû l’abandonner à cause des erreurs dans les observations de Mizousawa
et Tschardjoui, qui auraient rendu illusoire l'intérêt d’une étude approfondie du mouve-
ment du pôle. Afin de fournir les réductions à la position moyenne du pôle, nécessaires
pour la correction des observations astronomiques et géodésiques faites pendant l’année 1900,
j'ai déduit, avec le concours de M. Waxacx, au moyen d’une compensation cyclique des
oroupes d'étoiles, des résultats approximatifs pour le mouvement du pôle, en combinant les
observations du service international de latitude avec les observations dues à la coopération
libre des observatoires. Ces résultats ont été publiés dans un mémoire imprimé dans le
N°. 3734 (Vol. 156, p. 209) des „Astronomische Nachrichten” intitulé: Resultate des inter-
nationalen Breitendienstes uud der freiwilligen Cooperation in der Zeit von 1899.8 bis 1901.0.
Déja dans ce travail les avantages du service international des latitudes se montraient indu-
bitablement. Auparavant l’erreur moyenne d’une coordonnée de la courbe décrite par le pôle
(voir Astronomische Nachrichten N°. 3633 Vol. 152, p. 129) oscillait entre + 0".027 et
+ 0.033; d’apres les observations faites aux 10 stations, cette erreur moyenne a été réduite
à 0.019: cet heureux résultat est dû en premier lieu à l’influence que l'identité du pro-
gramme a exercée sur les observations de toutes les stations du service international.

 

bail Male ddl uk
vw en nm

var

   

 

wart

3
a
ü

{afb

auto ml

}

rudes mem A|

 
