 

1
à
M
i

138

Schlussbeobachtungen in Wien für die 100-tägige Dauer der Campagne nachstehende, in
Einh, d. 7. Dez. angegebene Änderungen der Schwingungszeiten:

Anschlussmessungen in Wien.

1901. Ausgangsbeob. Schlussbeob. Änderung in
Aug. 10/11 Nov. 18/19 100 Tagen
Pendel VII 05.507 6951 03.507 6879 — 72
— VII 0.508 2573 0.5082410 — 163
— Ix 0.507 9582 0.507 9452 — 130
— X 0.507 4870 0.507 4821 — 49
— XI 0.508 1889 0.5081836 — 58
— Xu 0 5076385 0.507 6301  — 34.

Diese Anderungen wurden der Zeit proportional auf die Stationsergebnisse der
einzelnen Pendel verteilt. Herr von Srurneck hilt es für ausgeschlossen, dass Lagenände-
rungen der Schneidenkörper in ihren Fassungen die Ursache der konstatierten Verkürzungen
gewesen sein können, und lässt deshalb, um die Erscheinung aufzuklären, seit 1902 das
Pendel VIII öfter beobachten; 4 solcher Kontrollmessungen haben jedoch eine völlige Über-
einstimmung mit der Schlussmessung im November 1901 ergeben ').

Bei den Stationsbeobachtungen wurde besondre Sorgfalt auf die Temperatur- und
Zeitbestimmung verwendet. Jedes Pendel hing schon vor seiner Beobachtung etwa 6 Stunden
auf dem Stativ, wodurch die Übereinstimmung seiner Temperatur mit der Thermometer-
angabe gewährleistet war. Um das Stationsmittel der Schwingungszeiten möglichst frei von
Unregelmässigkeiten im Uhrgange zu erhalten, wurden die Pendel in Intervallen von 8
Stunden beobachtet; das erste gleich nach der ersten Zeitbestimmung etwa um 10 Uhr
abends, das zweite um 6 Uhr früh und das dritte um 2 Uhr nachmittags. Wenn auch bei
dieser Verteilung die mittlere Epoche der Pendelmessungen nicht genau mit der mittleren
Epoche der einschliessenden Zeitbestimmungen zusammenfällt, so wird der beabsichtigte
Zweck doch wohl immer hinlänglich erreicht sein, zumal die Messungen, in demselben
Intervall fortschreitend, oft über mehrere Tage ausgedehnt wurden. Da jeder Beobachter
über 2 Uhren, einen Nardin’schen Chronometer und eine Hawelk’sche Pendeluhr verfügte,
die beide mit Stromunterbrechern versehen waren, so wurde die Schwingungsdauer eines

1) Ohne uns ein Urteil über den vorliegenden Fall erlauben zu wollen, neigen wir der Ansicht zu, dass kleine
Lagenänderungen der Schneide, hervorgerufen durch einen in der Fassung von Hause aus bestehenden und sich durch den
Gebrauch der Pendel allmählich auslösenden Spannungszustand, doch vielleicht den Hauptanteil an obiger Erscheinung haben
möchten. Denn erwägt man, dass die Fassungen erst kurze Zeit (etwa 1 Monat) vor Beginn der Messungen hergestellt
waren, dass ferner in der Befestigungsvorrichtung der Schneiden Druckschrauben vorkommen, die in der Längsrichtung der
Pendelstange wirken, und dass schliesslich zwischen Beginn und Schluss der Messungen ein Zeitraum von 3; Monaten
liegt, in dem sich ein allmählicher Temperaturfall von 8° vollzogen hat, so wird man derartige Vorgänge immerhin für
möglich halten müssen, zumal es sich hier um sehr kleine Längenänderungen handelt, die im Maximum den Betrag von
16 Mikron erreichen. Auch der Umstand, dass das Pendel VIII in Wien später keine Veränderung mehr gezeigt hat, scheint
uns eher für als gegen diese Möglichkeit zu sprechen.

 

3
=
a
z

er 1 188411 11e HUIAE I

Lake nike

bu her |

niet date Alida add laa un.

Dean mom ML |

 
