vai
muraux; d’après les expériences faites au moyen de dynamomètres, les oscillations synchro-
niques de ces supports peuvent être considérées comme égales à zéro.
En 1900, dans les gouvernements de Koursk et de la Vistule le colonel Illiachevitch
a déterminé la pesanteur dans 9 stations; les observations ont été faites à l’aide d’un
support de pilier.

|
|
t

Déterminations de la pesanteur par M. Illiachevitech pendant l’année 1900.
Point de départ Pulkova.

 

 

 

 

 

 

 

Longitude Alk |) Valour

Stations Latitude de H observée Jo—Yo
Greenwich g

m. cm. aa
(Pulkova). 59° 46'.3 302 1977 a 981.907 + 0.052
1. Lubimovka 51 30.2 35 392 203 981.165 + .047
2. Oboiane 5 49.6 30 16.7 246 981.114 ae UB
3. Kotchetovka ot 15 36 25.9 224, 981.097 4.097
4. Nepkhaïevo »0 u 5 36 35 8 203 981.115 + .059
5. Keltze BO) oot 20 SZ 279 981.108 + .070
6. Miekhov 0, 213 20 18 308 981.047 + .063
7. Bendine 00 495 19 Sl 256 981.066 + .069
8. Varsovie 69 419,1 20 13 111 981.231 + .022
9. Tchenstokhov | 50 48.6 19 7.8 248 981.096 = 092

Au Caucase M. le général Guédéonoff a déterminé la pesanteur dans 5 stations.

 

Déterminations de la pesanteur par M. Guédéonoff pendant l’année 1900.
Point de départ Tiflis (d’après les déterminations de M. Kulbery).

 

(Tiflis) 44° 43/1 44° 481.6 449 980.140 — 0.032

1. Alexandropol 40 47.0 43 49.7 4519 |. 979.750 + 001

F 2. Karse 40 36.5 43 38 1750 979.708 ı 0%
€ 3. Sary—Kamych| 40 19.9 19 840 | MM 979.606 | + .100
F 4. Jelenovka 40 32.8 Ak 58.4 1947 979.687 + .091
5. Erivane | 20 10 7 4h 32.8 990 979.845 = 012

En 1901, dans les gouvernements de St. Pétersbourg et de Livland, le lieutenant-
F colonel Serguiévski a déterminé la pesanteur dans 4 stations.

F Déterminations de la pesanteur par M. Serguiévski pendant l’année 1901.
Point de départ Pulkova.

F (Pulkova) 59° 46’.3 90 1977 72 981.907 + 0.052
| 1. St. Betersboure , 59 56 3 30. 10.0 2 981.944 + .055
2. Riabova 67225 30 39.4 61 981.936 + .057
3. Jouriev 58.928 26 43.2 90 981.801 + .051
4. Valk Die 46 26 1.8 94 981.762 + .045

pew vier

TUTTE TRA OMT aT

PTT TT

 

a a en ver nee ee

 
