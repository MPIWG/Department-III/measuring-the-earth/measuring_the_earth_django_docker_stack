 

ia am

Ta N ll

operation to complete the work, for the latitude was always approximately known before
setting up the Observatory on the line.

The Officers employed up to the end of September, 1859, were as follows:

Superintendent .... R. L. J. ELLERy, from 1st August, 1858
Nssistanes 3). R. Perry, » oo > >

» À, C ALLAN, > April »

» E. DE VERDON, » » »

» P. CHaunory, » » »

» T. W. Poeyniemr, > 200 July 1859
Computer P. Crenon, > April >

The total expenditure up to this date for salaries, wages, equipment, instruments,
building stations, &c., was £5451.

It had been early found by experience that the process of connecting distant lo-
calities with the chief meridian by running and measuring meridians through great lengths
of country not required for survey was not sufficiently expeditious with the strength at
command, and it was soon found desirable to push forward a primary triangulation so as
to more rapidly reach the distant distriets and connect the work generally. Already trigo-
nometrical points had been erected on prominent hills northwards from Melbourne to the
Murray, and a considerable number in the Western District also, which had involved a
great amount of labour and cost in clearing mountain tops and so on. This work had
been set on foot by Captain Clarke, R. E., two or three years previously, the party of
sappers and miners from England already mentioned having been placed in charge of the
clearing parties for this purpose.

In January, 1860, a site for a base line was selected on the Werribee Plains,
4,942 miles long. The one end (South end) being within the Railway enclosure, about two
miles E. of the Werribee station, the N. end bearing 304° 36’ 31", 26091,826 feet, or
4,9416 miles, and in a direct line with a hill named Green Hill, on the west side of the
Werribee River, 5,651 miles distant from the N. end. On this hill a permanent point was
established, and ie measured base was ultimately extended by triangulation to this point,
which: was named the Green Hill extension.

The ends of the measured base were marked by solid masses of masonry built five
feet into the ground, capped by a heavy stone with large plugs of gun metal, having
platinum centres on which was marked a fine terminal dot. Over this cap-stone was placed
a heavy cover-stone for protection, and above all a timber pyramid with pinnacle carefully
centered over the platinum point to help in alignment and subsequent triangulation for
checking, and for extension and expansion of the base. The Green Hill extension terminus
was marked by a heavy sunk stone with centre similar to the ones already described. The
actual measure of the base was commenced on 29th January, 1860, and ended 29th May,
and minor triangulations for extension to Green Hill was completed June 12th,

First a complete measurement of the 4,942 mile base was made by level bars, and

33

 
