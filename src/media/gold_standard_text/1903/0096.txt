 

90

Herr Haid theilt ferner im Namen der Herren Hammer und Koch den Bericht
über die württembergischen geodätischen Arbeiten mit. Beilage AXIX.

Herr Becker verliest den Bericht über diein Elsass-Lothringen ausgeführten Arbeiten.
Siehe Beilage AXX.

| Herr Förster giebt einige Mittheilungen über einen fiir die Normal-Aichungscom-
mission in Berlin construirten Comparator, indem er eine Sammlung von Photographien
dieses Apparats den Herren Delegirten zur Ansicht vorlegt. Siehe Beilage B VIII.

Herr Bakhuyzen fragt Herrn Forster, welche die Expositionszeit fiir die photogra-
phische Aufnahme der Theilstriche des Comparators ist.

Herr Förster kann die Expositionszeit nicht genau angeben, doch meint er, dass es
Augenblicksphotographieén sind.

Herr Anguiano verliest seinen Bericht über die in Mexico ausgeführten Arbeiten.
Siehe Beilage A VI.

Herr Rosen theilt zwei Berichte mit, den einen über die in Schweden, den anderen
über die in Spitzbergen ausgeführten geodätischen Arbeiten. Siehe Beilage A XXIT und B X.

Der Herr Präsident dankt Herrn Rosen für seine überaus wichtigen Mittheilungen
und fordert den Secretär auf, eine kurze französische Übersetzung dieser Berichte zu geben.

Nachdem der Seeretär dieser Einladung Folge geleistet hat, weist er hin auf das
grosse Gewicht der in Spitzbergen ausgeführten geodätischen Operationen und stellt den
Antrag, der schwedischen Regierung und den schwedischen Geodäten im Namen der Erd-
messung den Dank darzubringen für die so interessanten Arbeiten (Beifall).

Der Herr Präsident schliesst sich den Worten des Herrn Secretärs an und fordert
die Delegirten auf ihre Stimmen abzugeben über den Antrag der schwedischen Regierung
und den schwedischen Geodäten, welche sich an den Arbeiten in Spitzbergen betheiligt haben,
zu danken für die grossen der Geodäsie dadurch erwiesenen Dienste.

Der Antrag des Präsidenten wird einstimmig genehmigt.

Der Herr Präsident theilt mit, dass das Dankesvotum durch Vermittlung des Bureaus
der schwedischen Regierung mitgetheilt werden wird.

Herr Nissen liest seinen Bericht über die Arbeiten in Norwegen. Beilage AIX.

Herr Helmert bittet ums Wort um die Herren Delegirten aufzufordern den folgenden
Wunsch in Bezug auf die scandinavischen Arbeiten zu äussern.

Es ist erwünscht, dass die südschwedischen Dreiecke veröffentlicht werden, welche
zur Verbindung der dänischen Anschlussdreiecke mit den bereits publicirten nördlich ge-
legenen schwedischen und norwegischen Dreiecken dienen, um dadurch Schweden und Nor-
wegen direkt mit Centraleuropa verbinden zu können.

Dieser Antrag, wird einstimmig angenommen.

Herr Helmert stellt noch den Antrag die Conferenz möge folgenden Wunsch, die
Triangulationen für den Parallel von 47°—48° betreffend, aussprechen.

In der Dreieckskette für den Parallel von 47°—48° Breite in Europa zwischen
Brest und Astrachan ist die Verbindung zwischen Österreich und dem russischen Bogen in
47'|,° Breite mit minderwerthigen Dreiecken ausgefüllt. Die Ersetzung dieser Dreiecke durch

 

ddl uk

si Hai) Mailand |

a TI

Lab à fuit il

ni lanta!

snail i alibi ld oy»

Au nnmmaisansiinmnn, mau A |
