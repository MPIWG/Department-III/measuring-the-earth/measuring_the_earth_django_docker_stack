In re de s

 

[EM ur Ik

Wl | A]

Ta wT yen aT

 
 

DUAR TTA

I

dh

Chaque lecture de mire est immédiatement suivie d’une observation de la position
de la bulle.

Les deux series d’observations obtenues par chaque opérateur doivent s’accorder A
moins de 0p,006 (2 millimétres).

Dès que le second opérateur a calculé la différence moyenne de niveau des deux
points consécutifs d’après ses deux séries d'observations, il la communique à sen collègue,
qui compare ce résultat avec sa propre moyenne. Si les deux valeurs ne concordent pas à
moins de 0?,005 (1,5 mm) la station est recommencee.

On n’opére que par temps favorable, La longueur moyenne nivelée varie de 1'/, mille
à 2 milles (2 à 8 kilomètres) par jour.

#

JAPON.

Repères. — Les repères sont constitués par des marques gravées sur des bornes
de granit enterrées de 1 mêtre environ dans le sol.

Mire. — La mire, de 3 mètres de longueur, est divisée en demi-centimêtres et
porte deux chiffraisons croissant l’une de bas en haut, l’autre de haut en bas.

Chaque mire est pourvue d’un mêtre étalonné en acier, avec lequel on détermine
chaque jour, la longueur de la division.

On emploie simultanément deux mires.

?

Niveau. — Le niveau, construit par BAMBERG, a pour caractéristiques:

Nivelle: Valeur angulaire d’une division: de 3/ à 57.
Ouverture de l'objectif: 40 mm

Lunette: | Distance focale de l’objectif: 0,44 m
Grossissement de la lunette: 36 fois.

ÉTATS-UNIS DE L’'AMÉRIQUE DU NORD.

> = \
A. PERIODE ANTÄRIEURE A 1899.

I

Mire. — La mire est en bois saturé de paraffine pour rendre sa longueur indépen-
dante des variations hygrométriques.

Elle mesure un peu plus de 8 m de longueur et présente une section en forme de croix.

La graduation est formée d’une série de plaquettes métalliques de 5 mm de dia-
mètre, fixées dans la face de la mire, à des intervalles de 2 centimètres. Une échelle mil-
limétrique, entraînée par un voyant, permet d’estimer, au '/,, de millimêtre près, l’appoint
en regard d’un trait de repère gravé sur chaque plaquette.

Le voyant se déplace au moyen d’une chaîne tendue derrière la mire et passant
sur une poulie.

La face de la mire porte une division peinte, directement visible de la lunette, et

 
