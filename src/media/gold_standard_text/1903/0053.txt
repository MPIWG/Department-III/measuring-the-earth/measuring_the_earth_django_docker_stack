 

|
|
|
}

PT PAP PP PT PTE En Tt

rer ge

Irre open

Terre wart

men

 

47
conference generale et sa ratification par tous les gouvernements intéressés. Cette ratifica-
tion n’est pas nécessaire quand la méme dotation est continuée pour une nouvelle période
de dix ans, car l’art. 15 de la Convention indique que les articles de la présente Conven-

tion restent en vigueur jusqu'à ce qu’ils soient modifiés par une nouvelle entente des

États. À son avis le plus simple serait done que, quelque temps avant la prochaine
conférence, le ‘bureau s’adresse aux différents états et leur demande s'ils ont l'intention de
continuer leurs dotations pour une nouvelle période de dix ans. Seulement, si l’on voulait
introduire un changement, il faudrait une nouvelle entente DL voie diplomatique, qu’il
ne saurait approuver.

M. le Président croit qu’il n’y aura aucune difficulté à obtenir la continuation de
la dotation. Il est dans’ l’intention du Bureau de consulter les gouvernements pour leur

.demander s'ils ne voient aucune difficulté à continuer la dotation actuelle sans augmenta-

tion, et d’en prévenir en même temps nos collègues afin que, s'ils sont consultés par leurs
gouvernements, ils puissent donner leur avis en pleine connaissance de cause. Il espère
que cette manière de procéder aura l’approbation des délécués.

À son avis la première proposition de la commission des finances est parfaitement
admissible en ce qui concerne le côté administratif; il n'y aurait d’autres observations à
présenter que celles qui concernent le côté scientifique.

M. le Secrétaire perpétuel, ayant demandé la parole pour s'expliquer sur le côté
scientifique, s’exprime ainsi.

Il y a dans la question des latitudes deux côtés, qui tous les deux sont importants,
mais puisqu'il y a peut-être quelques-uns de nous qui attachent plus d'intérêt à l’un qu’à
l’autre, il faut les considérer séparément.

1°. Quelle est la cause des variations des latitudes ?

Quelle est la quantité précise de la variation des latitudes moyennes à une
certaine date ?

Je crois que la première question est celle qui nous intéresse le plus pour le mo-
ment. La question de la valeur de la variation n’a pas tant d'importance pour la géodésie, c’est
une question qui intéresse surtout les astronomes pour la réduction de leurs observations.
Nous ne pouvons, pour le moment, donner une réponse satisfaisante n1 à l’une ni à l’autre
des deux questions. Donc, je crois qu’il est urgent, et pour la géodésie et pour l’astronomie,
de continuer nos recherches. Peut-être pourra-t-on plus tard, quand la cause des variations
sera connue, abandonner l’étude de l’autre question aux astronomes.

M. Tanakadate fait observer qu'il a présenté un rapport sur les observations météo-
rologiques à Mizousawa, qui pourra servir à l’étude de l'influence des conditions météoro-
logiques sur les résultats des observations de latitude.

Il existe, à son avis, un lien intime entre l'astronomie et la géodésie, Il croit qu'il
est nécessaire de continuer les études sur les latitudes et propose de compléter le service des
latitudes en installant de nouvelles stations sur un même parallèle autre que celui de 39° &.

M. Förster est aussi d’avis qu’il faut continuer le service des latitudes et que, vu le
grand intérêt que présente ce service pour la géodésie et pour l’astronomie il faudra lui donner

 
