 

100

Anfrage der Generalkonferenz und der Annahme derselben von sämmtlichen betheiligten
Regierungen. Diese Annahme ist nicht nöthig, wenn dieselbe Dotation für eine neue Periode
von 10 Jahr verlängert wird, denn Art. 15 der Übereinkunft lautet: »Die vorstehenden
Bestimmungen dieser Übereinkunft haben solange Giltigkeit, bis sie durch eine neue Ver-
ständigung der Staaten abgeändert werden. Seiner Ansicht nach wäre es am einfachsten,
wenn rechtzeitig vor der neuen Conferenz das Präsidium sich an die verschiedenen Staaten
wendete, und sie ersuchte die bisherige Dotation unverändert auf zehn Jahre zu verlängern.
Würde eine Veränderung beauftragt, so ist eine neue Verständigung auf diplomatischem Wege
nöthig; das möchte Herr Förster abrathen.

Der Herr Präsident glaubt, dass es nicht schwer sein würde die Verlängerung der
Beiträge zu erhalten. Es ist die Absicht des Bureaus bei den betheiligten Regierungen anzu-
fragen, ob sie den jetzigen jährlichen Beitrag ohne Erhöhung weiter fortzusetzen wünschen,
und zu gleicher Zeit den Herren Delegirten diese Anfrage mitzutheilen, damit, wenn ihre
Regierung ihre Meinung fragt, sie diese mit voller Sachkenntniss abgeben können. Er hofft,
dass die Delegirten mit diesem Verfahren einverstanden sein werden.

Was die administrative Seite betrifft, kann seiner Meinung nach der erste Antrag
der Finanzcommission ohne weiteres angenommen werden; es sind jedoch vielleicht Bemer-
kungen zu machen über die wissenschaftliche Seite des Antrags.

Der ständige Secretär theilt in folgender Weise seine Ansicht über diese wissen-
schaftliche Seite mit. |

In dieser Angelegenheit der Breitenvariation sind zwei Punkte zu berücksichtigen,
welche beide sehr wichtig sind, aber da vielleicht einige der Delegirten grösseres Gewicht auf
den einen als auf den anderen Punkt legen, ist es nöthig sie gesondert zu betrachten, nähmlich :

1°. Welche ist die Ursache der Breitenänderung ?

90, Welcher ist an einem bestimmten Zeitpunkt der genaue Betrag der Abweichungen
von der mittleren Breite..

Ich glaube, dass die erste Frage uns jetzt am meisten interessirt. Die Frage nach dem
genauen Betrage der Breitenänderung ist für die Geodäsie nicht so wichtig, sie ist haupt-
sächlich für die Astronomen, der Bearbeitung ihrer Beobachtungen wegen, von Interesse.
Jetzt können wir jedoch weder die eine noch die andere Frage in befriedigender Weise be-
antworten; es ist also sowohl für die Geodäsie als für die Astronomie nöthig unsere Unter-
suchungen fortzusetzen. Vielleicht kann man später, wenn die Ursache der Breitenvariation
festgestellt ist, die Beantwortung der zweiten Frage den Astronomen überlassen.

Herr Tanakadate theilt mit, dass er einen Bericht über die meteorologischen Beobach-
tungen in Mizusawa unter die Delegirten hat vertheilen lassen ; diese Beobachtungen können
vielleicht dazu dienen den Einfluss der meteorologischen Verhältnisse auf die Resultate der
Breitenbestimmungen näher zu studiren.

Seiner Ansicht nach, sind die Astronomie und die Geodäsie sehr nahe mit einander
verbunden. Er slaubt, dass es nöthig ist die Breitenuntersuchungen fortzusetzen und den
Breitendienst zu erweitern durch die Einrichtung neuer Stationen in einem zweiten Parallele,
in einer anderen Breite als 39° 8.

Sinha cdl dab us 4

 

au td 1 0084 a Met ah à dak

Lake aba

oh!

osent mess dl |

 
