TVA

nt ae PA PPT PTT TN, vr a Here

HD OT TP

TTT CT

TTT TT

NTN NR TTR UTE TTF RTT TTT

Sari

93
il est vissé et rivé, puis soudé; cette pièce est, à son tour, vissée et soudée sur la réglette.
Grâce à ces précautions, nous avons pu réaliser un mode d'attache sur la sécurité duquel
nos expériences ne paraissent laisser aucun doute.

Comme on le sait, l’invar éprouve, dans le cours du temps, de faibles variations,
que l’on atténue beaucoup par un étuvage préalable systématiquement conduit. D'ailleurs,
de quelque métal qu’ils soient, les fils écrouis se modifient lentement pendant très longtemps,
si on ne les a pas fait vieillir artificiellement par une exposition A des températures suffi-
samment élevées. Avant leur emploi, nos fils ont donc été soumis à un traitement consistant
à les laisser séjourner, pendant une semaine environ, à une température voisine de 100
puis à les laisser refroidir très lentement, de manière à n’atteindre la température ambiante
qu'en trois ou quatre semaines. Autant que cela a été possible, les fils étaient ensuite con-
servés pendant des mois à une température supérieure à 25°, de manière à compléter l'effet
de cet étuvage.

Il nous paraît utile d’ajouter ici que, dans le cours des deux dernières années, plu-
sieurs services géodésiques nous ont confié la mission de faire construire pour leur usage
des fils conformes aux modèles que nous avons établis. Or, s’il fallait, en toute occasion,
répéter les longues opérations qui viennent d’être décrites, l'achèvement de chaque appareil
exigerait de longs mois. Nous avons done pensé agir dans l'intérêt général en constituant,
au Bureau international, un dépôt permanent de fil, que nous soumettons d’abord, en assez
grande quantité, à toutes les actions de nature à augmenter leur stabilité, et que nous
gardons en réserve, de manière à pouvoir répondre rapidement aux demandes qui nous sont
adressées. La Société de Commentry-Fourchambault et Decazeville, qui a participé avec la
plus grande libéralité aux études faites au Bureau sur les aciers au nickel, a bien voulu
mettre à notre disposition les quantités de fil nécessaires pour l'établissement de notre dépôt.

Établissement d'une base murale. — Nous avions pensé tout d’abord que, pour obtenir
un témoin permanent de la longueur de 24 mètres, généralement adoptée pour les fils dont
nous nous occupons, il serait nécessaire d'installer, sur un robuste support constituée par
une poutre treillagée, une règle métallique faite d’un petit nombre de pièces convenablement
aboutées et invariablement réunies ensemble. Nous avions déjà étudié les plans relatifs à cet
appareil, complété par des microscopes et des tenseurs constituant par leur ensemble un
véritable comparateur de 24 mêtres de longueur; mais nous avons été arrêtés dans l’exé-
cution de ce projet par les frais considérables qu'il aurait entraînés; et nous avons adopté
un projet plus modeste, qui était destiné seulement, dans notre première pensée, à nous
permettre de faire une série d’expériences préliminaires propres à nous donner un simple
aperçu de la tenue des fils dans certaines conditions déterminées.

L'observatoire du Bureau international comprend, dans la partie antérieure du
bâtiment, deux murs parallèles très épais, de près de 50 mètres de longueur, reposant, à
une profondeur de 8 mètres environ, sur le roc qui constitue en partie la colline sur

14

 

 

 

 
