 

VII. MESSUNGEN DER DANISCHEN GRADMESSUNGS-KOMMISSION.

Ueber die Arbeiten der Kommission hat uns Herr Generalleutnant von ZACHARIAE
“eine tabellarische Zusammenstellung zugehen lassen, welche die Resultate der in den Jahren
1901 und 1902 in Dänemark ausgeführten ern Schweremessungen enthält, und in der
folgenden Tabelle in etwas andrer Reihenfolge der Stationen wiedergegeben worden ist.
Danach wurden im ganzen 26 neue Stationen an Kopenhagen angeschlossen, und zwar 18
im Jahre 1901 durch die Herren Kapitän N. M. Perersen und Leutnant N. P. JoHAnsEn,
die übrigen 8 im Jahre 1902 durch Herrn N. P. Jonansnn.

Für die Referenz-Station Kopenhagen wird g— 981.576 em in der Meereshöhe
von lm angegeben, was der Annahme im Bericht von 1900 (g = 981.575 cm, Meeres-
höhe 17 m) entspricht (vergl. Bericht von 1900, 5, 218, N° 20),

Ausserdem liegt eine von Herrn Generalleutnant von ZACHARIAR verfasste Abhandlung
über die Genauigkeit der Pendelmessungen mit dem der dänischen Gradmessungs-Kommission
gehörigen Schneider'schen Pendelapparat N°. 14 vor, worin besonders das Beobachtungs-
material von 1901 einer eingehenden Diskussion unterworfen wird. Die Abhandlung ist
unter dem Titel: Om Middelfejlsbestemmelsen ved relative Pendulmaalinger med den Danske
Gradmaalings Schneiderske Apparat N°. 14 af Generalmajor ZACHARIAE in der » Oversigt over
det Kgl. Danske Videnskabernes Selskabs Forhandlinger, 1903, N’. 3” erschienen; wir ent-
nehmen dem dieser Arbeit in französischer Sprache beigegebenen Resume nachstehende, auf
die Variabilität der Pendel N°. 51, 53 und 55 bezügliche Angaben:

»A l'art. IX sont traitées les mesures de rattachement à Copenhague entre 1897
et 1900, au nombre d'environ 250, réunies en 11 groupes. Elles montrent une diminuation
évidente du temps d’oscillation, s’adaptant en moyenne à la formule

K — 1805.76 (1—e-0187), 10-7,

T étant le temps à partir du 14 juin 1894 et exprimé en unités de 100 jours. Cette formule
est déterminée par un caleul de compensation de 11 équations de condition à trois éléments
inconnus, et donne pour l'erreur moyenne (x) d’une des onze valeurs

—_ 147,

(a)? =

La susdite mal exponentielle ne peut étre appliquée au-dela de Vannée 1900,

car, pendant l’hiver de 1901, les constantes de l’appareil furent déterminées de nouveau,
et les pendules soumis à des grandes variations de température qui semblent en avoir dérangé

 

a
3
a
z
=

4 Led, 1 188 4111188

a foe)

tts abo hd ah

i
|
|

 
