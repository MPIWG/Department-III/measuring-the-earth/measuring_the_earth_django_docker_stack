 

ST PTT TP RPP TT ee TT

OT PO pm he

27

outre deux déterminations excellentes de la différence de longitude Paris-Bonn qui s'accordent
à 002 près; Bonn a été lié par plusieurs déterminations de longitude à Berlin, tandis que
la différence de longitude Berlin-Potsdam a été déterminée aussi avec une fort grande exactitude,

M. Tittmann demande si le résultat des opérations du Coast and geodetic Survey
pour déterminer la différence de longitude Paris-Brest-Greenwich s'éloigne beaucoup des
résultats récemment obtenus.

M. Albrecht ne saurait indiquer exactement la différence des résultats, mais il croit
qu’en général la valeur trouvée par le Coast and geodetic Survey s'accorde assez bien avec
les valeurs obtenues par M. Bakhuyzen dans sa compensation du réseau des longitudes.

M. le Président donne la parole à M. Helmert qui, au nom du Bureau central, pré-
sente un rapport provisoire sur les déterminations de la pesanteur. Voir Annexe B XVIII.

M. Helmert ayant terminé la lecture de son rapport s'exprime comme suit:

Qu'il me soit permis d'exprimer quelques vœux qui, quoiqu’ils ne soient pas imme-
diatement réalisés, peuvent peut-être indiquer les travaux que l'Association pourra entre-
prendre si les fonds le permettent,

Au moyen des observations de pendule publiées dans le rapport de 1900 j'ai déter-
miné les constantes de la formule de CLarraur, d’abord en me servant des observations faites
dans les stations situées près des côtes, et j'ai tâché de combiner les résultats en introduisant
dans la formule de Crairaut un terme basé sur l'hypothèse de Praïr. À mon avis ce serait
fort intéressant de continuer ces études en se servant des valeurs de l’intensité de la pesan-
teur déterminées dans un grand nombre de stations situées près des côtes. Plusieurs de ces
déterminations ont été faites, mais il faut les compléter, surtout sur les côtes de l'Amérique
du Sud, et je crois que ce travail ne pourra être exécuté que quand l’Association décide
de s’en occuper elle-même.

M. Lallemand prend la parole pour proposer de soumettre l'étude des communications
faites par M. Helmert à la commission spéciale qu'on vient de nommer pour l’examen du
programme des travaux à entreprendre.

Cette proposition est mise aux voix et adoptée à l’unanimité.

M. le Président donne lecture d’un rapport sur les règles géodésiques et les nou-
velles mesures faites au Bureau international des poids et mesures par MM. AR. Benoît et
C. E. Guillaume. .

Ce rapport se trouve a l’Annexe B V.

M. le Président, ayant fini la lecture, reléve le grand intérêt des recherches pour-
suivies par MM. Benoît et Guillaume et l'importance des grands services qu'ils ont rendus
à l’œuvre de la géodésie, et propose de prier M. Förster de vouloir bien leur transmettre
les remerciements et les félicitations de l’Association,

Cette proposition est adoptée à l’unanimité.

M. le Secrétaire donne lecture d’un rapport de M. le général Artamonojf sur les
travaux exécutés en Russie. Voir Annexe A I.

 
