NES

 

MALO ART

 

z
=
=

IFIP

astronomiques que je viens de mentionner et, en outre, dans une série de stations qui sont,
exclusivement, des stations de pendule. Je ne reléverai ici que les quelques points suivants:

Les stations de Bale et de Zurich qui nous servent en Suisse en quelque sorte de
stations fondamentales peuvent être considérées comme rattachées définitivement à Potsdam
où notre ingénieur a été obligeamment admis au printemps de l’année 1902 à déterminer
à nouveau les constantes des pendules de Sterneck appartenant à la Commission géodésique
suisse, Je suis heureux d’adresser ici de nouveau l'expression de nos remerciements à M. le
Directeur du Bureau central pour sa complaisance. j

Les mesures de la pesanteur ont continué aux environs du Simplon et dans l’inté-
rieur du tunnel, mais ces derniéres sont encore insuffisantes: on ne pouvait jusqu'ici les
exécuter que deux ou trois fois par année, aux jours de vérification de la direction de l'axe,
lorsque le travail était complètement suspendu. Il en résultait d’ailleurs de sérieux incon-
vénients, Dès cette année, elles seront poursuivies par notre ingénieur aussi à des dimanches
de suspension partielle où l’on ne travaille qu’à l'avancement. Il vaut done mieux ajourner
la communication des résultats, actuellement incomplets, jusqu’au moment où ils formeront
un tout bien ordonné. Les mesures vont être poussées activement en 1903 et en 1904.

Nous avons continué à déterminer les variations de la pesanteur dans les parties
les plus accidentées de notre pays. Les résultats obtenus l’année dernière dans la vallée de
Zermatt méritent une mention spéciale. Il résulte des calculs exécutés par M. Niethammer
que le défaut de masse relatif qui s’observe partout sous le massif des Alpes centrales,
diminue assez sensiblement lorsqu'on s'en éloigne au sud du côté de la chaîne élevée des
Alpes valaisanes. C’est ainsi que le défant de masse qui se chiffre encore par environ
150 unités de la 5me décimale dans le fond de la vallée du Rhône, à Brigue et à Viège,
se rapproche graduellement du chiffre de 100 unités seulement aux stations beaucoup plus
élevées situées près de la haute chaîne du Mont Rose. Il résulte d'autre part d’un rapport
sommaire que je viens de recevoir de notre observateur que, à Belalp, au nord de Brigue,
sur le versant méridional des Alpes bernoises, le chiffre correspondant au défaut de masse
dépasserait sensiblement 150 unités de la 5e décimale, Ceci correspond d’ailleurs tout à fait
aux résultats précédemment obtenus.

Nivellement de précision. — Le Service topographique fédéral (désignation officielle
nouvelle du Bureau topographique fédéral) qui, depuis de nombreuses années, poursuit, avec
l'appui de la Commission géodésique, les opérations du nivellement en Suisse, a continué
ses travaux d’une facon réguliére. Des nivellements de lignes nouvelles ont été exécutés sur
une longueur de plus de 200 kilomètres pour compléter le réseau de l’ancien nivellement
de précision de la Suisse.

Le Service topographique a aussi opéré de nombreux nivellements de contrôle pour
des lignes présentant des écarts trop forts. Ces écarts trouvent presque tous leur explication dans
le fait que, durant les premières années du nivellement, les mires n'étaient pas suffisamment
stables et qu’elles n'étaient pas assez souvent vérifiées. Quelques discordances entre les anciennes
mesures et les nouvelles doivent aussi résulter du fait que certaines lignes ont subi un

 
