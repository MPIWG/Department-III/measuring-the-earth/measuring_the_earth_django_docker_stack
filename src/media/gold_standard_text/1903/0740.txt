 

 

424

VEREINIGTE STAATEN VON AMERIKA.

Zwei sehr bedeutende Beiträge sind während der Berichtsjahre_ durch die Vereinigten
Staaten von Amerika geliefert worden, nämlich: » The Transcontinental Triangulation and
the American Arc of the Parallel. Washington 1900” !) und »The Eastern Oblique Are of
the United States and Osculating Spheroid. Washington 1902”), beide bearbeitet von
Herrn ©. A. Scuorr.

Der 4225 km lange Parallelbogen in 39° Breite enthält 109 Breiten-, 73 Azimut-
und 37 Längenstationen, für die die entsprechenden Lotabweichungen gegen einen passend
gewählten geodätischen Zentralpunkt, sowohl unter Annahme der Crarxe’schen (1866) als
auch der Bussnr’schen Elemente, abgeleitet sind. Werden von den 37 Längenstationen je
drei in der Umgebung von Washington und von San Franeisco zu je einem Punkt zu-
sammengefasst und fünf andere Stationen wegen starker lokaler Lotstörung in Länge (im
Betrage von 18” bis 24”) ausgeschieden, so ergeben die dann verbleibenden 28 Lotabwei-
chungen in Länge, dass die mittlere Krümmung dieses Teils des 39. Parallels zwischen die
Werte, die den Elementen von Ozarks und von BesseL entsprechen, fällt, und dass ferner
die Krümmong der östlichen Hälfte des Bogens mit den Crarkr’schen und die der west-
lichen mit den Bisser’schen Elementen nahe übereinstimmt. Dieser Bogen wird auch dazu
mitbenutzt, um aus den damals bekannten 3 Meridian- und 2 Parallelbogen in Amerika
durch 5 Combinationen zu je zweien von ihnen fünf Systeme vorläufiger Natur für die
Erdelemente abzuleiten. Die grosse Achse erhält hiernach Werte zwischen 6377577 m und
6379822 m und die Abplattung solche zwischen 1 : 288,6 und 1: 305,9.

Der östliche schiefe Bogen hat eine Ausdehnung von 2612,3 km im südwestlichen
Azimut von 57° 39,7 (oder von 23!/,° im Bogen grössten Kreises) und erstreckt sich über
15°13,75 in Breite und 22° 47,4 in Länge. Er enthält 71 Stationen mit Breiten-, 17 mit
Längen- und 55 mit Azimutbestimmungen. Für die geodätischen Positionen wurde derselbe
Ausgangspunkt wie für den Parallelbogen in 39° Breite gewählt; die Lotabweichungen sind
ebenfalls für das Ellipsoid von Crarke (1866) abgeleitet. Dieser schiefe Bogen wurde nun
dazu benutzt, um aus ihm allein ein sich ihm am besten anschmiegendes Rotationsellipsoid
zu bestimmen. Zu diesem Zwecke wurden 36 Breiten-, 14 Längen- und 34 Azimutstationen
ausgewählt, die möglichst gleichmässig über den ganzen Bogen verteilt sind. Es wurden 4
Ausgleichungen vorgenommen, je nachdem für die Azimutbestimmungen die Gewichte 1,
1/,, !/, und !/, angenommen wurden, während man für die Breiten und Längen das Gewicht
1 beibehielt. Die Resultate weichen nicht stark voneinander ab, jedoch wird das dritte
System für das beste gehalten; dieses ergibt

@—05/810(m, 2 — 1; 304,09,

 

1) Treasury Department. U. S. Coast and Geodetic Survey. Hunry S. Prirscuerr, Superintendent. Special Pu-
blication N° 4. Cuas. A. Scuorr. The Transcontinental Triangulation and the American Arc of the Parallel. Washington
1900. S. 831—871.

2) Treasury Department. U. S. Coast and Geodetic Survey. O. H. Tırrmann, Superintendent. Special Publication
N° 7. Cuas. A. Somorr. The Eastern Oblique Arc of the United States and Osculating Spheroid, Washington 1902,
S, 369—394,

 

m

zur hd 1 CBA DM NE AR th a A tee th MPS
: RS MERE DATE EE À CC AU EURE

Jed

Lab Luk a

db.) iuu ia.

PP

PN

 
