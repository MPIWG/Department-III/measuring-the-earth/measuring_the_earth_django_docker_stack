 

410

der Umgebung und in der Nähe des Meridians von Rom in den Jahren 1899/1902 fort-
gesetzt und zum vorläufigen Abschluss gebracht '). Hierdurch sind die Lotabweichungen in
Breite und in Azimut fiir 11 Punkte (für einen dieser Punkte, Monte Cavo, jedoch nur
die in Breite) bestimmt worden. Die &£ wachsen danach von Süden nach Norden zwischen
44°28’ und 44°9' Breite von — 0,1 bis + 18,5 und -- 17,1, während à sin B, abge-
sehen von den beiden nördlichsten Stationen Monte Carpegna und Bertinoro, ziemlich
konstant (etwa —= — 2”) bleibt. Zu Grunde gelegt sind die Besser’schen Erdelemente und
Ausgangspunkt der geodätischen Positionen ist Castanéa bei Messina.

Herr Professor Reina hat auch schon auf Grund seiner Ergebnisse nach einem
besonderen Verfahren, das freilich nicht die günstigsten Resultate liefert, das Geoidprofil
für den gebrochenen Linienzug zwischen den einzelnen Stationen von Monte Pisarello bis
Bertinoro abgeleitet, indem er dabei von Monte Pisarello bis Monte Mario auf zwei Wegen,
einmal über Fiumicino und das andere mal über Monte Cavo und San Pietro in Vincoli,
fortschritt. Für Monte Mario wurden dabei als Abstand des Geoids vom Ellipsoid die beiden
ziemlich voneinander abweichenden Werte — 0,09 m und — 0,53 m gefunden. Die Depres-
sion des Geoids gegen das Ellipsoid steigt nach Norden zu bis 11,09 m oder 11,40 m.

In der nachfolgenden Tabelle treten die beiden Centralpunkte Genua und Castanéa
noch gesondert auf; es ist aber nach einem Beschluss der italienischen geodätischen Kom-
mission vom Jahre 1900 ?) in Aussicht genommen, demnächst für ganz Italien den Monte
Mario bei Rom als Ausgangspunkt der geographischen Koordinaten einzuführen.

Lotabweichungen in Italien.

 

 

 

a | Besser’s Ellipsoid
Länge gegen

 

 

 

Station . Geogr. Breite| Meereshühe -
Greenwich £ | à | D
4. Ausgangspunkt Genua.

: m er | = 5
Bologna 11 21,5 44° 29.8 — SF 647 | 3,74 | — 0,95
Livorno 10 185 43% 51.0 + 10,00 — 634 —
Guardia Vecchia 9 24.0 Zu 19 — + 9,64 + 1,18 |— 2,01
Cagliari 9 71 DJ 19,2 — — 9,55 |— 1,94 |— 5,26
Carloforte Brise | 39° 81 — — 0,31 — — 2,923)

1) V. Reina. Determinazioni astronomiche di latitudine e di azimut eseguite lungo il meridiano di Roma. Fi-
renze 1903. S. 60—64; siehe hierüber auch: Lo studio dell’ andamento del geoide lungo il meridiano di Roma (Ri-
vista geografica italiana. Anno X. Firenze 1903. S. 455 --456). Ausserdem vergl.: V. Reims. Determinäzioni di latitudine
e di azimut eseguite nel 1898 nei punti Monte Mario, Monte Cavo, Fiumicino. Firenze 1899. S. 53—55, und die fol-
genden in den Atti della Reale Accademia dei Lincei, Roma, serie quinta, enthaltenen Abhandlungen desselben Ver-
fassers: Determinazione astronomica di latitudine e di azimut eseguita a Monte Pisarello nel 1899, IX, 1, 1900, S. 189—
196; Determinazione astronomica di latitudine e di azimut eseguita a Monte Soratte nel 1900. X, 1, 1901, S. 284—
291 u. 346—351; Determinazioni astronomiche di latitudine e di azimut eseguite a Roma (S. Pietro in Vincoli), a Monte
Cimino ed a Monte Peglia negli anni 1900 e 1901. XI, 1, 1902, S. 431—433.

2) Processo verbale delle sedute della Commissione geodetica italiana, tenute in Milano nei giorni 5 e 6 set-
tembre 1895 e nei giorni 26, 27 e 28 giugno 1900, Firenze 1900, S. 16,

8) Vorläufiger Wert,

=
3
me
=
3
a
=:
3
>

 

db hihi dl tet ie bestellen

dl bah lstchsbuhtl dich

yell banda nn

amsn ni L

 
