a a ap r

 

TV ART

en

TUNER a.

PTIT

American and European stations in 1900 by an officer of the Coast and Geodetic Survey !),
The eclipse expedition to Sumatra in 1901 by the Massachusetts Institute of Technology,
under the leadership of Prof. Alfred E. Burton, carried a halfsecond pendulum apparatus
belonging to the Coast and Geodetic Survey, and made determinations of gravity at Sin-
gapore, at the station which had previously been occupied by the Transit of Venus party
of 1882, and at Sawah Loento, in Sumatra. The pendulums were swung at Washington
before and after the trip.

During the summer of 1902 a rare opportunity occurred to determine the difference
of gravity at the surface of the earth and at a great depth in a mine. Prof. F. W. MeNair,
President of the Michigan School of Mines, at whose request the observations were taken,
had made the necessary arrangements with the mine owners to obtain access to the North
Tamarack Mine at Calumet, Mich. The Coast and Geodetic Survey furnished the instruments
and an observer. Pendulum swings were made with two sets of half-second apparatus at
Washington before and after the visit to the mine. At the mine simultaneous observations
were made at the surface and a point 1400 meters below the surface, and later at the
surface and at a point about at sea level, 360 meters below the surface. The computation
of these results is not yet complete. 2

PRECISE LEVELING.

At the beginning of the season of 1900 two precise levels of a new design *) which
had recently been constructed were put into use. The distinguishing peculiarities of this
new type of precise level, as stated in the report to the association in 1900, are, »that it
stands very low on the tripod head; that the level vial is fixed relatively to the telescope
and is placed as near as possible to the line of collimation, being in fact countersunk into
the barrel of the telescope; that the telescope does not rest in wyes, but instead is supported
by trunnions in front of the middle point and by a micrometer screw near its eye end.
The middle half of the telescope, including the level vial, is completely shielded by an
outer metallic tube, within which it is free to move as constrained by its trunnions and
the micrometer screw. The nickel-iron alloy has been used almost entirely in the construction
of the telescope and adjacents parts. The device for reading the bubble from the eye end
agrees in its essential principles with that used on the Berthelemy level. In its design it
differs radically from it. The distance between the eyepiece of the telescope and that of the
reading device is adjustable to fit the distance between the eyes of the observer, so that
no movement whatever is necessary to transfer the attention from the rod to the bubble”.

The results which have been obtained with this instrument and the simple method
of observation used with it are sufficiently remarkable as a rare combination of rapidity,
economy, and accuracy to be worthy of being specially called to the notice of the association.

1) Appendix 5, Report for 1901. , Determination of relative value of gravity in Europe and the United States’,
by G. R. Putnam, Assistant.

2) It is proposed to exhibit one of these instruments before the conference.

 
