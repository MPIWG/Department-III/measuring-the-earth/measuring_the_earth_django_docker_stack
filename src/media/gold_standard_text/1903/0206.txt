 
   

186

ultimately extend from latitude 17° in Mexico to latitude 60° or more in Canada. The
reconnaissance on this are has, during the period covered by this report, to June 23, 1903,
been extended from latitude 42° 38’ in the northern part of Nebraska to latitude 45° 39’
in South Dakota, an extension of 3° a or about 335 kilometers. The measures of angles
have been extended from latitude 42° 25’ in Northern Nebraska to latitude 43° 42’ in South
Dakota, and from latitude 38° 30° in central Kansas to latitude 30° 49’ in central Texas.
These two extensions cover 8°58, or 995 kilometers of the arc. In 1900, nine primary
bases were measured, which, together with a tenth base which had already been measured
at the intersection of this are with the thirty-ninth parallel are, will control the lengths
on about 1800 kilometers of the ninety-eighth meridian triangulation.

No astronomic determinations have been made along the ninety-eigth meridian,
save one longitude determination for a special purpose. The astronomic observations have
been postponed because it is believed they can be made more economically by special parties
after the triangulation has been completed than by the triangulation parties during the
progress of the work.

The measurements of nine primary bases along the ninety-eighth meridian, referred
to above, were all made by a single party in one season of six months. The feat accom-
plished by this party may be summarized as follows: Ten persons in six months standar-
dized their base apparatus twice and measured with a primary degree of accuracy nine
bases scattered over 1400 kilometers of the ninety-eighth meridian. These bases have an
acgregate length of 69.2 kilometers, the separate lengths varying from 6.0 to 12.9 kilo-
meters. The probable error of the measured length of each base is upon an average one
part in 1200000, and the largest probable error was one part in 690 000. The cost of
the field work was but $1231 per base, or $ 160 per kilometer upon an average, inclusive
of the standardization of the apparatus, the transportation to and from the field and between
bases, and the salaries of all oflicers and men.

Five sets of apparatus were used on each base, namely, the Himbeck duplex base
bars having separate brass and steel components, two 100-meter steel tapes, and two 50-
meter steel tapes. About one-fifth of the measurements were made with each apparatus.
Each set of apparatus was standardized in the field at the beginning and end of the season
under conditions approximating as closely as possible in every respect to the average con-
ditions under which they were used in the base measures. The iced bar Bır, 5 meters long,
was used as the standard of comparison. One kilometer of each base, known as the test
kilometer, was measured with all five pieces of apparatus. Each of the remaining sections
of each base was measured independently once in the forward and once in the backward
direction, the bars being used on both measures of certain sections and the two measures
of each of the remaining sections being made with different tapes. The probable errors
quoted above were computed from residuals which included the discrepancies between measures
of the same sections by different apparatus, and therefore are much more significant of the
true accuracy than probable errors as ordinarily computed from the discrepancies which
arise merely from the failure of a given apparatus to repeat its Own measures.

 

=
+
3
=:
Se
ai
=

==
3
3
a
ai

 

3

41 1 Hat

db nuul

 

; Ë
i
|
|
