j
|

|

 

444
3e serie. 4e série.
Dénsité de I, L, L Densité de L, i NG
Pair. mm. mm. mm. Vair. mm. mm. mm.
0.940  994,122.6 994,267.6 994,195.1 0.940 994,123.2 994,235.4 994,179.83
0.050 ‚118.7 ‚269.2 ‚194.0 0.050 ‚10.1 ‚241.6 ‚178.8

Les valeurs L, ont été déterminées par les oscillations du pendule quand le poids
lourd se trouvait prés du prisme marqué d’un point, tandis que pour les déterminations des
valeurs L, le poids lourd se trouvait de l’autre coté. Vu la précision que l’on peut obtenir avec
les pendules à demi-seconde, l'accord des valeurs de L est assez satisfaisant, mais les grandes
différences avec les valeurs obtenues avec le pendule à seconde (en moyenne 994.226 mm.)
est fort remarquable. Il n’est pas moins curieux de voir les grandes variations qui existent
dans les différences L, —L,; on trouve dans les différentes déterminations:

1e série. 2e série. Be série. 4e serie.
& KB be £
+ 1.4 3 2.1450 1122
+ 6.2 = 60.0
+ 10.6 2 1909 -_ 1259
Moyenne + 6.1 — 89.0 — 147.8 — 118.8

Les quatre séries d'observations se distinguent en partie en ce que les prismes en
agate avaient de positions différentes, en partie aussi en ce qu’on avait donné au pendule une
rotation de 180° autour de l’axe vertical. Nous n’avons pas encore pu déterminer avec
certitude la cause de ces variations; seulement nous avons pu constater que dans le courant
des années les prismes en agate sur lesquels les pendules oscillent se sont courbés, l’un est
devenu convexe, l’autre concave. Les rayons de courbure (en faisant abstraction du signe) sont
les mêmes pour les deux prismes, puisque les prismes adhèrent quand on les presse l’un
contre l’autre. On pourrait expliquer cette déformation en admettant que les prismes qui
dans le roc étaient situés probablement d’une manière symétrique par rapport à leurs surfaces
d'oscillation se trouvaient d’abord dans un état de tension d’où ils sont lentement revenus.
Cette courbure des prismes peut avoir eu fort bien une influence sur les valeurs isolées de
L, et L, puisque, quand le poids se trouve en haut, le pendule oscille sur d’autres parties
du couteau que quand le poids se trouve en bas. Il semble que cette influence est éliminée
dans la valeur L; en répétant les observations, après avoir fait polir les prismes de sorte
que les surfaces soient de nouveau absolument planes, nous pourrons voir si en vérité cebte
source d'erreur a été entièrement éliminée.

L'influence de la densité de l’air sur la longueur réduite du pendule, qu’on a con-
statée dans les expériences avec le pendule italien, se montre aussi dans les observations avec
le pendule à demi-seconde, mais avec un signe contraire dans les valeurs de L, et de L,.
Quand on fait l'hypothèse que la longueur du pendule varie d’une manière linéaire avec
la densité de l'air, on trouve pour les différents pendules les valeurs suivantes:

©

 

¥
3
3

Lad: i A LA A dh ld dt
t LA EN À LR MAIRIMAANN MAMRNMRNTE

destiné

ja ti il

Lil. lui

lb tu bu
m DEA

yma Ls a died dl ie

 
