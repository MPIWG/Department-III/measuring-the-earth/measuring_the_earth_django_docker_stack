 

ER:

re

: = Fe
& 2 jis
i

ee
x a

84

. ständigen Secretär für die in seinem Berichte dem verstorbenen Herrn Hirsch gewidmeten

Worte und verliest seinen Bericht. Siehe Beilage A VIT.

Der Herr Präsident dankt Herrn Gautier für seinen so werthvollen und inhalts-
reichen Bericht, und ertheilt Herrn Darwin das Wort für seinen Bericht über die in Gross-
Britannien ausgeführten Arbeiten.

Herr Darwin erklärt, dass er seinen Bericht später dem Herrn Secretär wird zu-
kommen lassen. Jetzt hat er Herrn Lallemand einen Bericht über die Nivellements und Herrn
Helmert einen Bericht über die Triangulationsarbeiten in Indien und Gırı’s Bericht über die
Triangulationen in Stid-Afrika mitgetheilt, Der Bericht findet sich Beilage A XII.

Es freut ihn hier Herrn Geheimrath Helmert und den anderen Gelehrten des
seodätischen Instituts in Potsdam im Namen seiner Regierung den Dank darbringen zu
können für die dem Indian geodetic Survey erwiesenen Dienste, indem sie den nach Pots-
dam gesandten Ingenieuren dieser Survey alle Anweisungen und alle für den Gebrauch
der Schwerependel erforderlichen Instructionen in der freigiebigsten Weise zur Verfügung
gestellt haben.

Nachdem der Herr Präsident Herrn Darwin für seine Mittheilungen gedankt hat,
wird die Sitzung um 11 Uhr während einer Viertelstunde. unterbrochen, Wiederaufnahme
der Sitzung 11 Uhr 15. :

Der Herr Präsident ertheilt Herrn Aikitsu Tanakadate das Wort.

Herr Tanakadate verliest die Berichte über die in Japan ausgeführten geodätischen
Arbeiten. Siehe Beilage AVa und À Vb.

Auf Anfrage des Herrn Präsidenten verliest Herr Zachari® einen vorläufigen Bericht
über die dänischen Arbeiten. Den definitiven Bericht wird er vor Hinde dieses Jahres dem
Herrn Secretär zukommen lassen. Siehe Beilage AXXI.

Herr Weiss verliest in seinem und in Herrn von Sterneck’s Namen die Berichte
über die in Oesterreich ausgeführten Arbeiten. Siehe Beilage AXIlI« und A XIIld.

Herr Bodola de Zdgon verliest den Bericht über die Arbeiten in Ungarn. Siehe
Beilage A III.

Herr Tittmann vertheilt unter die Herren Delegirten seinen Bericht über die geo-
dätischen Arbeiten in den Vereinigten Staaten, und bespricht einige Hauptpunkte. Der
Bericht findet sich Beilage A XI.

Der Herr Präsident dankt Herrn Tittmann für das so interessante wissenschaftliche
Material, das er der Conferenz dargeboten hat und erwähnt, dass Herr Tittmann in einem
der benachbarten Säle einige Instrumente zur Besichtigung für die Herren Delegirten hat
aufstellen lassen.

Der Herr Präsident ertheilt Herrn Bakhuyzen das Wort für die Mittheilung einiger
Bemerkungen über die Breitenvariation.

Herr Bakhuyzen drückt sich in folgender Weise aus.

In der Leidener Sternwarte sind die Beobachtungen zur Bestimmung der Breiten-
variation, seit 1899 Juni bis 1900 Juli von Herrn Dr. Sruix, und seit 1900 Juli von
Herrn Dr. Zwıers angestellt worden. Der Zenitteleskop hat eine Objectivôfinung von 81 mm.

 

ea Me mk u ddl ul,

LL ed,

Lil

urn m 1

à Mle a a my 9

i
|
|

 
