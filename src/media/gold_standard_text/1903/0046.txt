 

CINQUIÈME SÉANCE

Jeudi, 13 août 1903.

Présidence de M. le Général Passot.
La séance est ouverte à 9 heures 1/,.
Sont présents :

I. Les délégués: MM. Albrecht, Angwiano, Bakhuyzen, Bassot, Becker, Bodola, Börsch,
Bouquet de la Grye, Bourgeois, Celoria, Darboux, Darwin, Fenner, Förster, Gautier, Guar-
ducci, Haid, Helmert, Heuvelink, Lallemand, Matthias, Nissen, Poimerantzeff, Poincaré,
v. Richthofen, Sanchez, Schmidt, Schorr, Sugiyama, Tanakadate, Tittman, Valle, Weiss, Zacharie.

II. Les invités: MM. de Anda, Buchwaldt, Johansen, le Maire, Momberg, Omori,
Paulsen, Petersen, Rasmussen, Sand, H. Thiele.

M. le Président donne la parole à M. le Secrétuiré pour la lecture du procès-verbal
de la dernière séance.

Le procès-verbal de la quatrième séance est lu et adopté.

M. le Président indique l’ordre du jour qui porte principalement :

1°. Le dernier des rapports nationaux, celui des Pays-Bas.

20, Communication d’une note de MM. Benoît et Guillaume au sujet d’un procédé
pour la mesure des bases.

3, Rapports généraux sur les mesures de base, sur les nivellements et sur les
déviations de la verticale.

4%, Projet du directeur du Bureau central au sujet de l’activité du Bureau pour
les prochaines années.

5°. Propositions de la commission des finances.

M. le Président, pour satisfaire a une demande de quelques-uns des délégués an-
nonce que le constructeur de l’astrolabe 4 prisme est M. Vion, constructeur d’instruments
de precision, 38 rue de Turenne, a Paris.

Il donne ensuite la parole à M. Heuvelink pour donner lecture d’un rapport sur
les travaux géodésiques aux Pays-Bas et dans les Indes orientales néerlandaises,

 

su td LL à Ma du ide ul

A ELLE. os.

Lt

Lathe Pl

Lu to bi |

Wh

nl bu Aa aia 4

Dr mimsauns mama A|

 
