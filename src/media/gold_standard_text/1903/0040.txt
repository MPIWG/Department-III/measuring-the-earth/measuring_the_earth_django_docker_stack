 

34

quoiqu'il résulte des observations dans les six stations qu’aussi pour la même latitude la
valeur de z n’est pas partout la méme.

On pourrait peut-étre faire une objection contre l'emploi, dans la réduction des
observations de Leyde, des valeurs de x et de y que M. Albrecht a déterminées pour une latitude
de 39° 8’. Si une partie de la variation de la latitude x cos À + y sin A était produite non par
un changement de la direction de l’axe terrestre, mais par un déplacement du centre de
gravite, les valeurs de + et de y seraient une fonction de la latitude. Je ne saurais indiquer
quelle est cette fonction, mais nous pouvons faire une hypothèse extrême, c’est-à-dire, que toute
la variation de la latitude fut causée par un déplacement du centre de gravité, la différence
de la latitude géocentrique et de la latitude géographique restant la même; dans ce cas
les valeurs de x et y seraient proportionelles au sinus de la latitude. Partant de cette
hypothèse, qui en réalité ne se présentera jamais, j'ai de nouveau calculé les z de Leyde
et quoiqu'ils diffèrent un peu de ceux que j'ai trouvés auparavant, le désaccord avec les
z de M. Albrecht existe toujours.

Dans cette phase du problème de la variation des latitudes il me semble qu'il
est du plus grand intérêt de faire des déterminations de latitude sous des latitudes fort
différentes, à l'hémisphère nord et à l’hémisphère sud, afin d'examiner de quelle manière 2
dépend de la latitude. De cette étude ou pourra peut-être conclure quelle est l’origine de
ce terme dans la variation de la latitude.

M. Darwin demande à M. Bakhuyzen s'il a fait quelques caleuls sur la quantité
de glace entassé près des pôles produisant un déplacement du centre de gravité tel qu'il
serait nécessaire pour expliquer la valeur de z dans le mouvement apparent du pôle.

M. Bakhuyzen fait remarquer que les glaces nageant dans les mers polaires ne
peuvent pas, en se dégelant, changer la position du centre de gravité. Ce sont seulement

les glaces entassées sur les continents qui entreront en ligne de compte, et ce sera surtout

près du pôle austral, où les continents sont beaucoup plus étendus que près du pôle boréal,
que l’amoncellement des glaces peut avoir une influence sensible. Quoiqu'il n’ait pas fait
des calculs exacts, il ne croit cependant pas qu’on pourrait expliquer ainsi une partie impor-
tante de la valeur trouvée pour la quantité 2.

. M. Helmert fait observer que le déplacement du centre de gravité causé par les
olaces près des pôles est une question fort délicate. D'après ses calculs il fallait admettre
sur le continent près du pôle austral une épaisseur de glace d’environ un kilomètre pour
pouvoir expliquer une variation de la latitude près de l'équateur d’un dixième de seconde.
Encore il fallait adopter que la terre fût absolument rigide ce qui n’est pas le cas. Par
la grande pression exercée par ceb amas de glace le continent serait comprimé et déformé
et il est fort difficile d'indiquer la quantité de cette déformation, qui donnerait au centre de
gravité un déplacement en sens inverse. Avec M. Bakhuyzen, il est aussi d’avis qu'on ne
saurait trouve: l'explication de la valeur de z dans le dégel des glaces polaires. Il croit
que l'étude de cette question à une telle importance qu'il faut y employer l’argent disponible
de l'Association.

 

dd ul

Lu

sarah: sah. La AHLEN ail tds du

ak all i il

to |

ib

a a Mad pay »

Di uaens mem wil |
