  

 

 

ma ART Jam ann

i

TR I FR Bag

pm

mir

ANNEXE B IX,

RAPPORT

sur les opérations géodésiques de l’Equateur,

PAR

M. POINCARE.
(Avec une carte).

L’Association connait, ‘depuis le dernier congres, les details du projet de revision
de l’arc meridien de Quito; je voudrais lui rendre compte de l’état actuel de cette entre-
prise et des premiers résultats obtenus. L’avant-garde de la mission, comprenant MM. les
capitaines MAurAın et LarzemanD, s’est rendue à l'Équateur au commencement de l’année
1901 et a consacré les premiers mois de cette année aux préparatifs indispensables. Le gros
de la mission, commandé par M. Bourerois et comprenant M. le capitaine Lacouse, M. le
lieutenant Pzrrier, M. le médecin aïde-major Rivar, plusieurs sous-officiers, caporaux et
soldats, débarqua à Guayaquil le ler juin avec le matériel. Six officiers équatoriens furent
adjoints à la mission pour aider à l’organisation des convois et leur entremise facilita beaucoup
les rapports avec les autorités locales et avec les indigènes.

Grâce au zèle de tout le personnel, tout le matériel, y compris la règle bimétallique,
put être transporté à Riobamba le 13 juillet.

Les opérations commencèrent par la mesure de la base principale & Riobamba.
Cette mesure, ainsi que les opérations astronomiques principales et les observations pendu-
laires autour de Riobamba, occupérent la mission jusqu’à la fin de novembre.

Pendant ce temps, M. Mauraix reconnaissait les stations du rattachement et y
construisait les signaux ainsi qu’aux sommets de trois des triangles du grand réseau. M. le
capitaine Lacomse acheva de rattacher la base de Riobamba au réseau dans les derniers
mois de 1901.

Kn décembre, le chef de la mission, accompagné du capitaine LALLEMAND, du lieute-
nant Perrier et de M. River, se rendit dans la région du Nord pour les déterminations
astronomiques et la mesure de la base de vérification. Dans les projets primitifs, cette base
devait se trouver sur le territoire colombien, mais les événements politiques dont cette région
fat le théâtre à cette époque rendit impossible la prolongation de l’are en dehors de la

 
