 

Die an den vier letzten Punkten, in der Nähe von Darjeeling, gefundenen & zeigen,
dass die grossen im Bezirk Dehra Dtin gefundenen Werte nicht abnorm sind; spatere
Beobachtungen aus dem Jahre 1903 (bei Naini Tal), die noch nicht scharf reduziert sind,

 

 

 

 

STATION
Ce eee eee eee ae EEE IEE

Patna
Dariapur
Karia
Hathbena
Ramai
Pathaidi
Dalea

Amita
Khankharia
Didawa
Viraria
Lunki
Rojhra
Chänga -
Khori
Alamkhan
Kanothol
Akbar
Ranjitgharh
Madhupur
Charaldanga
Chanduria
Lohagara
Jalpaiguri
Siliguri
Kurseong
Senchal
Tonglu
Phallut

haben dies bestätigt.

Lokale Lotabweichungen in Breite und in Azimut fir die Umgebung

N ne nn
Meereshöhe in |

 

 

 

 

von Kaliänpur, bestimmt 1898/99.

Station

a

Daiadhari
Salot 1)
Surantal

Sironj (N. E. End of the Base)

Bhaorasa

 

|

Länge gegen
Greenwich

77° 40
11 14
1m 44
da ON
foe

 

Geogr. Breite

24° 38
24 15
24 14
24 9
24 8

 

engl. Fuss

1867
1834
4802
1481
1387

1) Die Azimutbestimmung in Salot stammt bereits aus dem Jahre 1849.

 

 

418
ee nn

Länge gegen er

no Geogr. Breite £
Bre 12 91° A7. 55
a7 55 21 Al 1,06
a 7 19 19 = 9,32
89 4 19 52 + 026
So. à 90 57 016
e217 21 49 | — 2,89
8 2 D9 00, | 481
80 29 24 0 4 318
71 56 94 37 L 99
A 3 24 51 — 93
> 94 57 38
70 42 94 58 — 45
0 in | 02 81 — 0,4
6052 | 22 09 — 04
ao 25 À — 18
68 46 | 24 50 | — 1,5
En 149
aa 0 2 | 4,7
an 2353| _ 5,6
ee | 28 oT 3,9
so 2 | ir
u. m A u
au 2 4} 19
88 47 Je À | = 6.0
8e 21 6 D 20
88 18 56 52 =
88 20 26 59 = 35,8
88 D 2 = 129
88 | a 13 = 00

5

1,32

1,13
9,00
1,48

|

EEE

asin B

+
es
+

 

2.97
0,73
2,93
9,43
1,39

 

N

2
a
=
as
3
3
a

a

SE
35
3:
a
qi
u

Lab uid il

u hi |

Lui

su bh ig badd dl us pr

à mama il |

 
