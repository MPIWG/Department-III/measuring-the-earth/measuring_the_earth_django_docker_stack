|
}
!
\
i

 

PR TITI

FT

POTN TTR OTT

WTR TT

101

Herr Förster meint auch, dass der Breitendienst weiter fortgeführt werden muss,
Wegen der grossen Bedeutung, welche der Breitendienst für die Geodäsie und die Astronomie
hat, ist es erwünscht denselben zu erweitern, wie die Finanzcommission in seinem zweiten
Antrag vorgeschlagen hat.

Herr Helmert schliesst sich der Meinung der verschiedenen Delegirten über die
Fortsetzung des Breitendienstes bis zum Ende des Jahres 1906 an; er möchte jedoch noch
eine Resolution vorschlagen um die Meinung der internationalen Erdmessung zum Ausdruck
zu bringen, dass auch nach dem Jahre 1906 die Beobachtungen zur Bestimmung der Be-
wegung der Erdachse ausgeführt werden müssen, da die Resultate in gleicher Weise der
Astronomie, der Geologie und der Geodäsie zu gute kommen. Er ist übrigens überzeugt,
dass man in irgend welcher Weise die dazu benöthigten Mittel finden werde, und möchte eine
Resolution vorschlagen, die vielleicht mit einigen Aenderungen von der Conferenz gebilligt
werden kann. ‘

Der Herr Präsident bemerkt, dass es sich jetzt nur handelt um die Fortsetzung des
Beobachtungsdienstes bis Ende 1906, und schlägt vor Herrn Helmert’s Antrag über die
nach 1907 auszuführenden Beobachtungen später zu behandeln.

Herr Förster stimmt der Meinung des Herrn Präsidenten. bei.

Der Herr Präsident schlägt vor in dem ersten Antrag den Worten »der internatio-
nale Breitendienst”” hinzuzufügen die Worte »wie dieser jetzt eingerichtet ist.

Der erste Antrag mit der vom Herrn Präsidenten vorgeschlagenen Abänderung wird
genehmigt.

Der Herr Präsident eröffnet die Diseussion über den zweiten Antrag und schlägt
vor diesen auf folgende Worte zu beschränken:

Das Präsidium der Erdmessung wird ersucht eine Erweiterung der Breitenbeobach-
tungen vorzubereiten,

da der übrige Theil nur die Gründe worauf der Antrag beruht und einige Andeu-

tungen über den Antrag selbst enthält.

Der Herr Präsident bittet Herrn Helmert eine Erklärung der angedeuteten Summe
von 10000 M. geben zu wollen. Es scheint ihm a priori, dass diese Summe im Verhältniss zu
den Schwierigkeiten der Beobachtungen in der Gegend des Equators gering sei.

Herr Helmert hatte sich gedacht, dass der photographische Refractor, welche der
Association gehört, Sir Davin Gizz am Kap zugeschickt werden kônnte, da dieser Ge-
lehrte auf diesem Gebiete die meisten Erfahrungen hat. Das würde nicht viele Kosten
verursachen, sodass einige Tausend Mark genügen würden.

Herr Albrecht empfiehlt als weitere Station eine in den Ostindischen Inseln, oder bei
der Sternwarte in Quito. Falls wir eine Station in einer der Indischen Inseln heranziehen,
sind wir auf die Mitwirkung. des Herrn Bakhuyzen angewiesen.

Herr Bakhuyzen antwortet, dass das Centralbureau in dieser Angelegenheit, welche
ihn sehr interessirt, natürlich auf seine Mitwirkung rechnen kann, nur wird es unmöglich
sein mit einigen Tausend Frances, nicht jährlich sondern im Ganzen, eine Station einzurichten
und einen Beobachter auf mindestens zwei Jahre zu bezahlen.

 
