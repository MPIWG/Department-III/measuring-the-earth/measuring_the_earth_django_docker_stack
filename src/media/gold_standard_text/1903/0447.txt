nur

ann

TTA WaT TT

PAO NTE

159
untersuchte Gebiet bestätigt. Es hat sich ferner herausgestellt, dass zwischen g(Flachsee)
und g(Tiefsee) ganz ähnliche Verhältnisse bestehen, wie zwischen g (Küste) und g(Fest-
land). In der in der Einleitung dieses Berichts, 9. 134, erwähnten Akademie-Abhandlung
giebt Herr Prof. Hrımert den Unterschied 9 (Küste) — g(Festland) zu ++ 0.036 cm an,
während Prof. Hecker aus der Gesamtheit seines hierfür in Betracht kommenden Beobach-
tungsmaterials die entsprechende Relation

9 (Flachsee) — g (Tiefsee) = + 0,028 em + 0.018 cm
abgeleitet hat.

Herr Prof. Hacker hebt hiernach als wichtigstes Resultat seiner Untersuchung her-
vor, dass die Intensität der Schwerkraft auf den Tiefen des Atlantischen
Oceans zwischen Lissabon und Bahia nahezu normal ton an? der von
Prof. Heımerr aufgestellten Schwereformel von 1901 entspricht, und
dass sich somit die von Prarr aufgestellte, dann von Favs ebenfalls an-
genommene und von Harmerr eingehender begründete Hypothese vom
isostatischen Gleichgewicht der Lagerung der Massen der Erdkruste
für diesen Teil des Atlantischen Oceans bestätigt.

3) Prof. Borrass bestimmte in den Jahren 1900 und 1901 den Schwereunterschied
zwischen Potsdam und den Stationen Bukarest, Tiglina bei Galatz, Wien (Kaiserl. Stern-
warte), Charlottenburg bei Berlin und Pulkowa bei St. Petersburg. Gelegenheit zur Bear-
beitung der ausserdeutschen Stationen boten die astronomischen Längenbestimmungen Pots-
dam— Bukarest (1900) und Potsdam —Pulkowa (1 901), an denen der Beobachter teilnahm.

Für die Stationen Bukarest, Tiglina und Wien hat Herr Prof. HrıLmerr bereits im
Bericht von 1900 (Tab. Vd, N°. 103—105) vorläufige Resultate mitgeteilt, die von den
definitiven nur bei Tiglina um 0.002 cm infolge eines kleinen Rechenfehlers abweichen.
Die definitiven Resultate der Schweremessungen liegen jetzt im Druckmanuskript vor und
werden demnächst zur -Veröffentlichung gelangen.

: Zu den Messungen diente ein Stückrath’scher Pendelapparat älterer Konstruktion,
zu dem 4 mit den Nummern 5 bis 8 versehene Pendel gehören. Der Apparat ist in der
Veröffentlichung des Königl. Preussischen Geodätischen Instituts: Bestimmung der Polhöhe
und der Intensität der Schwerkraft auf 22 Stationen von der Ostsee bei Kolberg bis zur
Schneekoppe, Berlin 1896, 8. 184 u. f. eingehend beschrieben, und auch die Werte der
Pendelkonstanten sind dort angegeben. Was die Pendel betrifft, so haben sich dieselben im

Laufe von 7 Jahren ausserordentlich konstant gehalten, wie aus den nachstehenden Werten.

ihrer Potsdamer Schwingungsdauer hervorgeht.

Potsdam Pend. 5 Pend. 6 Pend.7 Pend. 8 Mittel Beobachter.
1894/95: 08,502 3480 3958 4905 5057 05.502 4350 ') Borrass u. Kiihnen

1) Im Schwerebericht von 1900 sind die Schwingungsdauern der 4 Pendel für 1894/95 irrtümlich um den Betrag
des Mitschwingens in Potsdam, der 104 Einh. d. 7. Dez, betrug, zu gross angegeben worden,

 
