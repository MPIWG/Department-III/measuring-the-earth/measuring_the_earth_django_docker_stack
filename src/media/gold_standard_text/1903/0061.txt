TE ger nn

un nn TT

EROFFNUNGS-SITZUNG

Priisident: Herr General Bassot, provisorischer Vice-Prisident der internationalen

Erdmessung.

Die Generalconferenz wird um 2'/, Uhr in dem Versammlungssaal der ersten Kammer

eröffnet.
Anwesend sind:
A. Seine Königliche Hoheit der Kronprinz von Dänemark.
S. Excellenz der Kabinetschef, Minister der auswärtigen Angelegenheiten.
S. Excellenz der Kriegsminister.
B. Die Delegirten:
I. DEUTSCHLAND.
1. Herr Geheimrath Dr. W. Förster, Professor, Direetor der Sternwarte, in Berlin.
2. Herr Hofrath Dr. M. Haid, Professor an der Technischen Hochschule, in Karlsruhe.
3. Herr Dr. Max Schmidt, Professor an der Technischen Hochschule, in München.
4, Herr Geheimrath Dr. Th. Albrecht, Professor, Abtheilungsvorsteher im König]. Geodä-

OX

12

tischen Institut, in Potsdam.

. Herr Dr. A. Bérsch, Abtheilungsvorsteher im Kônigl. Geodatiechen Institut, in Potsdam.
. Herr Geheimrath Dr. #, A. Helmert, Professor, Director des Kénigl. Geodätischen In-

stituts, in Potsdam.

. Herr Matthias, Oberst, Chef der Trigonometrischen Abtheilung der Kanal Landesauf-

nahme, in Bele

. Herr Dr. E. Becker, Professor, Director der Sternwarte, in Strassburg i. E.
. Herr Dr. R. Schorr, Professor, Director der Sternwarte, in Hamburg.

10.
LE

Herr P. Fenner, Professor an der Technischen Hochschule, in Darmstadt.
Herr Dr. Freiherr von Richthofen, Geheimer Regierungsrath, Professor, in Berlin.

II. OESTERREICH.

Herr Hofrath Dr. E, Weiss, Professor, Director der Sternwarte, in Wien.

 
