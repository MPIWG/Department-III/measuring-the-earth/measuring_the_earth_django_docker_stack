 

 

 

154
schlägigen Arbeiten des Instituts mehrfach beschriebenen Wippmethode bestimmt; in den
Jahren 1900 und 1901 geschah die Bestimmung sowohl nach der Wippmethode als auch
nach der ebenfalls bekannten Zweipendelmethode unter Anwendung eines schweren ScHU-
mann’schen Hilfspendels; 1902 und 1903 aber wurde die Zweipendelmethode allein angewandt.

Die mittlere Unsicherheit der Stationsergebnisse liegt innerhalb der Grenzen von
+ 0.003 cm.

2) Prof. Hecker unternahm im Jahre 1901 infolge eines vorangegangenen Be-
schlusses des Präsidiums der Internationalen Erdmessung eine Reise von Hamburg nach
Rio de Janeiro, um den Verlauf der Schwerkraft auf dem Atlantischen Ozean durch Ver-
gleichung von Quecksilberbarometern mit Siedethermometern zu bestimmen. Bei dieser Ge-
legenheit fiihrte er auch relative Schweremessungen durch Pendelbeobachtungen in Rio de
Janeiro und später, bei der Rückkehr, in Lissabon und Madrid im Anschluss an Potsdam
aus. Die wissenschaftlichen Ergebnisse dieser Reise sind in der Veröffentlichung des König].
Preussischen Geodätischen Institutes: Bestimmung der Schwerkraft auf dem Atlantischen
Ozean sowie in Rio de Janeiro, Lissabon und Madrid von O. Hacker, Berlin 1903, niedergelegt.

Zu den Pendelbeobachtungen benutzte Prof. Hacker einen Stückrath’schen Drei-
pendelapparat, dessen Einrichtung er in der eben genannten Publikation beschrieben und
durch stereoskopische Abbildung erläutert hat. Hier sei nur erwähnt, dass diese Apparate
vermöge eines weitgehenden thermischen Schutzes der Pendel eine scharfe Temperatur-
bestimmung und ausserdem auch eine genaue Bestimmung des Mitschwingens — wenigstens
für die diametral gegenüber liegenden Pendellager — ermöglichen, Um sicher zu gehen
nahm Prof, Hecker 6 mit den Nummern 21, 28, 46, 47, 40 und 41 versehene Stückrath-
sche Pendel mit, von denen die ersten 4 mit Messingstangen, die beiden letzten mit Stangen
aus Aluminiumbronze versehen waren. Zwei von diesen Pendeln, N°. 21 und 28, hatten
sich bei früheren Versuchen als veränderlich gezeigt; die übrigen waren noch nicht ge-
braucht, aber bereits mehrere Jahre alt, so dass man sie als frei von Spannungen ansehen
durfte.

Die Bestimmung der Temperaturkoeffizienten, die nach Beendigung der Reise in
Potsdam stattfand, ergab nachstehende, in Einh. d. 7. Dez. der Sternzeitsekunde ange-
gebene Werte.

Temperaturkoeffizient

Pendel N°. 40: 43.65 + 0.15
» > Al: 44.09 + 0.15

> >» 10% 48,48 + 0.23

» 2 11. 48.62 0.15

» 2 21° 49.08 + 0.82

» > AS HOSS se 1:08

H

Zur Reduktion der Beobachtungen wurde bei den Pendeln N°. 40 und 41 das
Mittel ihrer Temperaturkoeffizienten: 43.87 und ebenso bei den Pendeln N°. 46 und 47
das Mittel: 48.55 angewendet; bei den bereits als verdächtig bekannten Pendeln N°. 21

 

ge
¥
=
3
=e
=e
as
=

35
=e
=z
ès
4

gh stead:

see [17 EB

jeu ebm JA Lu &

i
|
|
