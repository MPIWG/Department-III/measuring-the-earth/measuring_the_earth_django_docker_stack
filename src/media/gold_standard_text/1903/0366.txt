 

82

Excédent des recettes sur les dépenses.

M. Er.
ae... 22,928. 00 28,660. 00
oa. ........ .  4,515,40 5,30. 70
anne. 4447.97 5,559. 96

 

Portal... 31.089. 97 39.611. 71

La grande différence entre le résultat de l’exercice de 1900 et des deux autres années
s’explique par le fait que pendant la première année la rentrée d’un grand nombre d’anciennes
contributions arriérées a eu lieu par les soins du bureau de l’Association. En effet, pendant
l'année 1900, ces rentrées ont atteint la somme de 19,867. 43 M. (24,834. 29 fr.), tandis que
dans les deux autres années la somme des contributions versées et des rentrées de contri-
butions arriérées est restée au dessous du montant des contributions réglementaires.

Les contributions annuelles réglementaires, atteignant à présent 67,400.00 M.
(84,250. 00 fr.), augmentées des petites recettes assez régulières, c’est-à-dire des intérêts
bonifiés et des recettes provenant de la vente des publications, représentent notre recette
annuelle régulière, s’élevant en somme ronde à 69,000 M. ou 86,250 ir.

Comme la moyenne des dépenses annuelles pendant la période triennale 1900 —1902,
d’après le petit tableau ci-dessus, se trouve, en somme ronde, égale. à 63,000 M. ou 78,750 fr.,
‘on peut déduire de tous ces chiffres, qu’en moyenne la gestion de ces trois exercices
annuels a réalisé des économies de 6,000 M. ou 7,500 fr. par an. On peut attendre
d’après les communications de M. le Directeur du Bureau central, un résultat analogue pour
l’année actuelle, de sorte que nos ressources disponibles vers la fin de l’année 1903
s’élèveront probablement à plus de 100,000 M. ou 125,000 fr. Selon une résolution de la
conférence générale de Stuttgart, une somme de 60,000 M. ou 75,000 fr. devait être réservée
pour assurer le fonctionnement régulier du service géodésique international. Notre actif
véritablement disponible s'élève donc actuellement à plus de 40,000 M. ou 50.000 fr.

Ein presence de cette situation financière et sur la base des discussions qui ont eu

lieu dans les dernières séances de la conférence et’dans le sein de la commission des

finances elle-même sur la question des latitudes, et afin de tenir compte des résolutions
présentées à la dernière séance de la conférence par la commission spéciale nommée pour
les questions de la pesanteur, la commission des finances fait les propositions suivantes:

1°. Le service international des latitudes sera continué jusqu’à la fin de l’année
1906, époque déterminée par la circonstance que, d’après la Convention, la question de la
dotation de l'Association géodésique internationale, à partir de l’année 1907, doit être de
nouveau prise en considération par les Hauts Gouvernements.

20, Le Bureau de l'Association est prié de pourvoir à une extension des observa-
tions des latitudes, qui est désirable dans l'intérêt d’une étude approfondie des variations
annuelles mises en lumière par les résultats du Service international des latitudes d’après
la découverte de M. Kimura. Une somme de 10,000 M. (12,500 fr.) parait d’abord suffi-

 

Len tt

Lt he |

i
|
|

arzt a 1 11118 A LA ect aia

 
