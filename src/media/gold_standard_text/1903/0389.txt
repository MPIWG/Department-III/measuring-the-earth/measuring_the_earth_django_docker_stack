{YN aoe ee

 

INT AT

TOUT RR TTR Ome TNE aT PTT TTT

WT TPF

105

D'autres observateurs, opérant en divers points, ont trouvé de pareilles concordances
et leurs moyennes s’écartent aussi peu des latitudes exactes déterminées ultérieurement.
Pour les états du chronomètre, on constate des accords du même ordre entre ceux obtenus
par ce procédé et ceux fournis par la lunette méridienne.

On ne saurait donc douter de l'excellence d’une méthode qui permet d'atteindre à
une telle exactitude avec un simple sextant et par trois observations seulement. Toutefois,
on est en droit de se demander pour quelles raisons son usage est si peu répandu en dépit
de son origine illustre et des efforts tentés par ceux qui ont pu en apprécier la valeur !).

Il faut sans doute placer en première ligne les difficultés que présentent au début
les observations d'étoiles à l’horizon artificiel. Ce n’est que par un entraînement progressif
en commençant par les étoiles les plus brillantes qu’on arrive à mettre aisément les deux
images d'une même étoile dans le champ et à les y maintenir. On a inventé des dispositifs
particuliers pour faciliter ces deux opérations, un petit niveau qui se fixe sur l’alidade et
un pied articulé porté par trois vis calantes ou à défaut un sabot de bois portant des en-
tailles à différentes hauteurs. Mais outre que ces accessoires ne se trouvent pas dans le
commerce et qu’on doit les faire faire spécialement, il faut une trés grande habitude ou un
talent d’observateur peu ordinaire pour espérer obtenir, méme avec un instrument qui en
est muni, des résultats d’une précision comparable & celle indiquée plus haut.

C’est qu’en effet le sextant se préte mal aux observations qui réclament une grande
précision. L’erreur d’appréciation du temps d’une coïncidence est, pour une étoile donnée,
d’autant plus faible, toutes choses égales d’ailleurs, que les images sont plus nettes et le
grossissement de la lunette plus fort, La netteté des images dépend de la bonne fabrication
de l'optique et de la manière dont l’achromatisme a été réalisé. Quant au grossissement
dans les instruments à réflexion qui sont destinés à être tenus à la main, il est nécessaire-
ment très limité: on est obligé de laisser un certain champ à la lunette sous peine d’aug-
menter outre mesure les difficultés de recherche de l'étoile et d'observation. Malgré l’emploi
dans la lunette astronomique du sextant d’oculaires négatifs qui donnent, comme on le sait,
le plus fort grossissement pour une même valeur du champ, on ne dépasse guère 7. Lors-
que Vinstrument doit être placé sur un support, on peut substituer, à l’oculaire ordinaire,
un oculaire grossissant un peu plus et aller jusqu’à 12 ou 18; c’est à peu près la limite,
Dans ces conditions, on ne saurait obtenir une grande précision dans l'estimation des temps
des coincidences. |

Peut-on du moins arriver à réduire notablement l'influence des erreurs commises
sur les heures notées des coïncidences en multipliant les observations d’une même soirée ?
Car le principe des hauteurs égales sur lequel repose la méthode de Gauss s’applique évi-
demment à un nombre quelconque d'étoiles et le problème se résont très aisément par la
méthode générale des équations de condition. Avec le sextant, les étoiles de 3ème grandeur

1) En France particulièrement, la méthode des hauteurs égales est rarement appliquée. Elle n’est même guère

connue que depuis la publication dans le tome 1V des annales du Bureau des Longitudes du mémoire de M. le comman-
dant Perrin.

 
