   

 

ee ee ea eireiee

 

164

a 7e;:

le rösultat de la determination directe est done plus petit de 1".48.

Dans le voisinage de l’&quateur les deviations de la verticale ne peuvent avoir qu’une
influence trés faible sur les azimuths; la difference constatée doit done étre attibuée aux
erreurs des observations astronomiques et des positions des étoiles, mais surtout à l’accu-
mulation des erreurs dans le réseau des triangles. Le chemin le plus direct qui relie le
point Basis West au point Tor Si Mangambat contient deux côtés du réseau de la base

et huit côtés du réseau principal.

IV. DÉTERMINATIONS DE LATITUDE ET D'AZIMUTH AU POINT G. Dempoz.

Une deuxième détermination de latitude et d’azimuth a été faite par le capitaine
Wackers, en juillet et août 1897, a la station G. Dempoe, située dans la résidence Lam-
pongsche Districten, % 5° 659" S. et 1° 52 55" O. de Batavia.

L'altazimuth de Pistor et Martins, dont il a fait usage, était le même qui avait
servi aux observations astronomiques à Tor Si Mangambat; la valeur angulaire du niveau
sur Vaxe horizontal était maintenant 2”.91, et celle du niveau fixe 2”.78. La methode
d'observation était d’ailleurs la même.

Les positions apparentes des étoiles horaires ont été empruntées aux éphémérides
du Berliner Astronomisches Jahrbuch; les distances zénithales étaient comprises entre ie]
et 39%,8; la valeur moyenne est 26°.8.

Le chronométre employé était Hohwii 667, réglé pour le temps sidéral; la marche
par heure a été réguliérement déterminée pendant les observations, et variait entre — 0s.081
et + 03.036.

* Des observations circumméridiennes pour la détermination de la latitude ont été
faites du 23 juillet au 2 août, ainsi que le 17 et le 18 août; en 9 soirées on a effectué
16 observations complètes, dont la moitié se rapportent à des étoiles au nord, l’autre moitié
à des étoiles au sud, tout-à-fait comme à Tor si Mangambat.

On a observé les étoiles suivantes :

au nord:
e Ajele,
« Aigle,
à Flèche,
é Dauphin,
æ Dauphin,
0 Dauphin,
31 Pégase,
70 Pégase;
et au sud:

§ Ophiuchus,
# Sagittaire,

 

=
a
=
=
a
4

Ju ad.

dal nb

 

i
i
|
|

 
