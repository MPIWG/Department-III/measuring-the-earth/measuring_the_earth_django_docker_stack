 

66

Hartl, geboren 1840 zu Brunn, studirte an der technischen Hochschule in Wien,
trat 1859 freiwillig in die k. u. k. Armee und trat 1865 als Lieutenant in das kim.
militär-geographische Institut ein, wo er bis 1898 verblieb. Während dieser Jahre ent-
wickelte Hartl grosse Arbeitskraft auf dem Gebiete der Geodäsie und Meteorologie.

Im Institute war er wesentlich mit Triangulirungen und Basismessungen beschäftigt,
von denen besonders seine Basismessung bei Kronstadt, welche ihm zu bemerkenswerthen Un-
tersuchungen über die Ausdehnung der Messstangen Veranlassung gab, die Verbindung des
Dreiecksnetzes in Dalmatien mit dem in Italien, quer über das Adriatische Meer, sowie
seine grosse Triangulirung I. Ordnung über ganz Griechenland, durch welche Arbeit die
Gradmessung eine erfreuliche Erweiterung gegen Süden erfahren hat, hervorgehoben seien.
Seine Untersuchungen über trigonometrische Höhenmessungen, über terrestrische Refraction
und über barometrische Höhenmessungen sind nicht weniger werthvoll, wie aus verschiedenen
Abhandlungen über diesen Gegenstand hervorgeht.

Hartl hat sich auch beschäftigt mit Studien über Landkartenprojectionen und
mit historischen Untersuchungen über die Ausmessung und die Kartographie Oesterreichs
und hat dadurch viel zur Klärung der Verhältnisse beigetragen.

Die grosse Zahl von Hartl’s Schriften über alle diese verschiedenen Gegenstände
sind Zeugen seiner viel umfassenden Kenntnisse, und die Verleihung der akademischen
Doctorwürde durch die Wiener Universität, so wie seine Ernennung zum ordentlichen öf-
fentlichen Professor an dieser Universität, waren wohl verdiente ehrenvolle Anerkennungen
seiner eifrigen Bemühungen. Leider hat er sich nur wenige Jahren dieser neuen Arbeit
widmen können; er starb am 3. April 1903 zum grossen Bedauern aller derjenigen, welche
unseren gelehrten sympathischen Collegen kannten.

Nach unserer Pariser Oonferenz erhielt das Bureau folgende Mittheilungen, Aen-
derungen in dem Personalbestand der Delegirten und in den Mitgliedern der permanenten
Commission enthaltend. Zum grösseren Theile sind sie den Herren Delegirten durch Cireu-
larbriefe vom 23 August 1901 und vom 9 Juli mitgetheilt worden.

1°. Brief von der Botschaft der Vereinigten Staaten Nordamerika’s in Berlin (9 Februar
1901) die Nachricht enthaltend, dass der Superintendent of the U. States Coast and geodetic
Survey, Otto H. Tittmann, von der Regierung der Vereinigten Staaten an Stelle des Herrn
Dr. Henry Pritchett zum Mitglied der permanenten Commission ernannt worden ist.

2°. Von der königlich schwedisch-norwegischen Gesandtschaft in Berlin die Mit-
theilung dass, an Stelle des verstorbenen Herrn Oberstlieutenant Haffner, der Director
des geographischen norwegischen Instituts, Oberstlieutenant Per Nissen, als Mitglied der
permanenten Commission ernannt worden ist.

3°. Von der schweizerischen Gesandtschaft in Berlin die Mittheilung dass, an
Stelle des verstorbenem Professors Hirsch, Prof. Raoul Gautier, Director der Genfer Stern-
warte, zum Mitglied der permanenten Commission ernannt worden ist.

4°. Vom auswärtigen Amte in Berlin die Nachricht (9 Dezember 1901) dass Prof.
Borrass vom kön. preuss. Geod. Institute zum Delegirten des deutschen Reichs zur General-
conferenz der internationalen Erdmessung bestellt worden ist.

sah Ma HENRI til dd he eh ll

1) Jill,

ll

I Lan N

wi |

2. us sea A|

 

 
