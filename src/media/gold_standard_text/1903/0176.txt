 

162

à l’ouest: on avait donc fait en tout 48 observations avec la lunette nord et autant avec
la lunette sud.

Les 24 observations complètes se composent de quatre séries, observées chacune
dans six positions différentes du cercle, distantes de 30° l’une de l’autre.

On a observé les étoiles suivantes:

Vierge.
Hydre (quatre fois),

alt: Série I =
a
B Balance (deux fois).
3
a

Serie 1.
a l'ouest: Série IM.
Série IV.

Orion.
Baleine (cinq fois),
y Orion (une fois).
Les distances zénithales des étoiles à l’est variaient entre 67°.7 et 81°.0, les azimuths
entre 78°.2 et 99°.4; les distances zénithales des étoiles à l’ouest entre 69°.3 et 86°.4 et

leurs azimuths entre 269°.0 et 276°.1.
Des observations relatives aux étoiles & l’est on déduisit comme valeur moyenne de

Vazimuth du signal de nuit:
311° 44° 17.30;
les valeurs extrémes des résultats, déduites de deux observations d’une méme étoile, faites
immédiatement l’une après l’autre dans les positions différentes de la lunette, sont 311° 44’ 13.25
or oil AL 21017.
Pour les étoiles à l’ouest on obtient comme moyenne:
ar AR TSV Do,
avec les valeurs extrémes, déduites comme tantöt: 211944 12720 et 311.44’ 182.12.
Comme résultat final on a pris la moyenne des résultats obtenus par les étoiles
à l’est et par les étoiles à l’ouest:
A — 311° 44 16".41.

Qi l’on considère séparément les résultats obtenus avec les étoiles à l’est et à l’ouest,
on obtient comme erreur moyenne du résultat de deux observations dans les positions diffé-
rentes de la lunette:

MU == Ze 17,99.
Mais, si l’on détermine cette erreur moyenne au moyen des erreurs apparentes rela-

tives au résultat définitif, on trouve:
m= + 1".82.

Si l'on prend cette dernière valeur, pour être sûr que l'exactitude obtenue ne sera
pas évaluée d’une façon trop favorable, on déduit pour l'erreur moyenne du résultat définitif,
dont le poids est 48:

 

=
=
a
=
=
=
=
a

na

ak talk

 

4
1
|

 
