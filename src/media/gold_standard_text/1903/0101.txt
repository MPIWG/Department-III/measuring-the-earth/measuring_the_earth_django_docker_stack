TE Eee ES lee

Na ee TEE

Pear

ITT TET.

genen

POOR TRIN OMT ION ATT

ET FAI

95

Herr Darwin giebt darauf eine bejahende Antwort.

Herr Tittmann fragt Herrn Darwin, ob die Triangulationen in der Malayischen
Halbinsel bei Singapore und in Birma Dreiecksmessungen 1° Ordnung sind.

Herr Darwin kann es nicht versichern.

Herr Helmert antwortet Herrn Littmann, dass, obwohl er es nicht bestimmt sagen
kann, diese Ketten seiner Meinung nach alle gleicher Güte sind, Herr Major Burrarp
hat einen Bericht über die Lothabweichungen im ersten Verticale in Birma in den Ver-
handlungen der 13. Conferenz der Erdmessung, Bd. I 8. 110, publicirt; es finden sich darin
Abweichungen bis zu 60”. Nach Herrn Helmert’s Meinung sind diese Werthe nicht richtig,
sondern gefälscht durch eine Anhäufung von Fehlern in der Verbindungsdreieckskette bei
Kalianpur, wie übrigens Herr Major Burkarn am Ende seines Berichtes mittheilt. Ferner
wird bei der Bestimmung der Lothabweichungen im ersten Vertikal der Einfluss von Fehlern
in den Horizontalwinkeln dureh Multiplication mit dem Coéfficienten Cot.® stark vergrössert.

Aus diesen Abweichungen kann man ungefähr schliessen, wie genau die Dreiecks-
messungen überhaupt sind; aber am besten kann man dies nachsehen in den Messungs-
resultaten der Dreiecke, welche sich im geodätischen Institute in Potsdam befinden ie

Der Herr Präsident dankt Herrn Helmert für seine Mittheilungen über die Arbeiten
in Afrika und spricht im Namen der internationalen Erdmessung Sir Davip Gri. seinen
Dank aus.

Der Seeretär stellt den Antrag, der englischen Regierung und Sir Davın Gitı den
Dank der internationalen Erdmessung darzubringen für die schon ausgeführten und noch
weiter zu unternehmenden Arbeiten in Süd-Afrika (Beifall).

Der Antrag des Seeretärs wird genehmigt.

Herr Zallemand giebt mit Hülfe von unter den Delegirten ausgetheilten Abbildungen
eine Beschreibung eines Theodoliten, der nach seinen Angaben für den technischen Dienst
des Kadasters construirt worden ist, und betont die Vortheile dieses Apparats.

Der Herr Präsident giebt eine Übersicht der wichtigsten Theile eines Berichts tiber
die Untersuchungen im internationalen Maass- und Gewichtsbureau betreffend die Messungen
mit Drähten nach der Methode von JÄnsrın, welcher zur Aufnahme in die Abhandlungen
bestimmt ist. Der Herr Präsident bittet Herrn Förster, den Vorsitzenden des Maass- und
Grewichtscomités, den Herren Benoît und Gurrraume für ihre wichtigen wissenschaftlichen
Beiträge zu danken.

Herr Förster glaubt es sei nützlich, so bald wie möglich von den beiden interes-
santen, vom Herrn Präsidenten mitgetheilten Berichten der Herren Brxoir und GvrtLaumn
Separatabdrücke anfertigen, und dieselben den Herren Delegirten zukommen zu lassen.

Der Herr Präsident stimmt dieser Meinung bei, und beauftragt den Secretär für
die Drucklegung zu sorgen.

In Bezug auf den Bericht der Herren Brnxoir und GvıLLaums macht Herr Helmert
folgende Bemerkungen.

1) Siehe den Bericht Beilage B XIII über die Triangulationen.

 
