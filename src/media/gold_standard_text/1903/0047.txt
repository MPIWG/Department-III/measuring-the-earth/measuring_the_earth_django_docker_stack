 

|
}

MOM EERE. ny ee

ther tare roto ep

eek

Fremen warnen

ke
è
=
¢

 

a

M. Heuvelink lit les rapports suivants. Voir Annexe A Villa et A VIIID,

M. Helmert comme rapporteur sur les triangulations, ayant regu de M. Darwin le
rapport de Sir Davin Ginu concernant les travaux géodésiques le long du méridien de 30° de
longitude en Afrique, donne le résumé suivant de ce rapport.

Les triangulations dans la colonie du Cap et dans le Natal, depuis le parallèle de
35° jusqu’au parallèle de 28°, pour la jonction du Cap au méridien de 30° sont terminées.
Vient alors le Transvaal; dans cette contrée jusqu’au parallèle de 22° on n’a pas encore
fait de mesures, mais M. Gizs a organisé les opérations de sorte qu’on pourra bientôt les
commencer. Plus au nord dans la Rhodésia, entre les parallèles de 22° et de 16°, on a
terminé en partie les mesures à peu près pour 4°, dans la partie boréale il reste encore
à mesurer un polygone, eb au sud une chaîne de 2° de longueur. Dans cette contrée on a
rencontré beaucoup de difficultés: d’abord par les fièvres dont surtout le chef de l'expédition
a eu beaucoup à souffrir, ensuite par suite du transport des instruments et de tout ce qui
était nécessaire à l'expédition, qui ne pouvait plus se faire au moyen de chariots à bœufs mais
au moyen de porteurs. On vient à présent d'organiser les opérations depuis le Zambési
jusqu’au lac de l'anganyika, qui ont éte confiées à M. le Dr. Rugin, lequel à pris part à
l'expédition du Spitzberg. A la fin du mois d'Avril l'expédition est partie du Cap pour se
rendre par bateau à Chinde.

Comme il s’agit des triangulations entre les paralléles de 16° et de 8° a
une longueur d’environ 7°, & peu pres la même étendue que l’arc de méridien mesuré à
l’Équateur, il s’écoulera quelques années avant que ces opérations puissent être terminées.
On aura done en Allemagne et dans l’État libre du Congo le temps nécessaire pour préparer
les travaux destinés à continuer les triangulations vers le nord.

M. Helmert fait connaître que, conformément aux résolutions de l'Association géo-
désique internationale et de l'Association des académies, on a invité les gouvernements de

XN

9 sue

Empire allemand et de l'État libre du Congo à continuer les mesures le long du lac de

Tanganyika et encore plus loin jusqu’a la latitude de 1°$. Jusqu'à présent les difficultés finan-
cières ont empêché les autorités allemandes de s’occuper de cette affaire, mais 1l espère que, grâce
à l’appui de l’Académie des sciences à Berlin, on la prendra en main dans l’année prochaine.
Il a eu déjà l’occasion de se procurer des données fort intéressantes sur les nécessités finan-
cières, scientifiques et techniques de l'expédition, grâce à M. le capitaine Hermann qui a dirigé
des triangulations dans les parties voisines des frontières au sud et au nord du lac Taganyika.
Il ne doute pas qu’en Allemagne les opérations seront organisées et, d'après ce qu’il a appris,
l'État libre du Congo serait prêt à favoriser une telle entreprise scientifique.

Quant aux mesures des bases elles seront faites au moyen d’appareils JÄnerın. A 2° au

sud du Zambèsi prés de Salisbury une base de 13!/, milles anglaises a été mesurée en 1900

au moyen de fils qui avant et après les mesures ont été étalonnés au Cap. M. Âelmert ajoute
qu'un grand nombre de fils ont été employés dans ces mesures au nord de Rhodésia, entre
autres deux fils d’invar. ;
M. le Secrétaire donne un résumé en français du rapport de M. Helmert.
M. Darwin ajoute que Sir Davin GiLz a demandé au gouvernement russe de lui
À 6

 
