 

448

8.

RAPPORTS A LA CONFERENCE GENERALE DE COPENHAGUE.

La composition des rapports suivants, dont un apergu a été donné a Copenhague,
exigeait un travail assez considérable pendant l’hiver passe.

a) Rapport sur les opérations astronomiques (longitudes, latitudes et azimuts) par
M. le Prof. Arprecur. À ce rapport sont jointes deux cartes, d’après la projection conforme
conique. M. le Prof. ArBrecx1 est, en outre, occupé à calculer une nouvelle compensation
du réseau des longitudes en Europe, motivée par la nouvelle détermination de la différence
de longitude entre Potsdam et Greenwich par l’Institut géodésique royal de Prusse.

b) Rapport sur les triangulations par M. le Prof. Dr. Krücer et par moi. A ce
rapport seront jointes deux cartes démonstratives, indiquant l’état des triangulations dans
l'Europe et dans l'Amérique du Nord. Par l’intermédiaire du Colonel Gore l’Indian Survey
à Dehra Dun à fourni 870 exemplaires d’une carte de la triangulation dans les Indes.

c) Rapport sur les déviations de la verticale par M. le Prof. Dr. Bôrscx.

d) Rapport sur les déterminations relatives de la pesanteur au moyen de pendules
par M. le Prof. Borrass et par moi. La compensation du réseau des stations principales
et des stations de rattachement a été retardée. A présent on se propose de faire un rapport
définitif sur les déterminations relatives de la pesanteur exécutées pendant la 19e siècle,

B. Gestion administrative.

ir

Le fonds des dotations a été géré comme d’habitude. En nous reservant le
dépôt conventionnel des comptes exacts des récettes et des dépenses, nous donnons ci-dessous
un aperçu du mouvement des fonds pendant l’année 1903:

Récettes.
Solde acc des fonds a la fin de 1902. . ......., ..« .. M. 95.594,37
Condbntons pour 1901 et 1902 4 5e eek 4 000.00
Poorabnton- pont 1903 5... > 63 507.08
Monte de CBC UONS 0 0 M... 144.00
Intérêts du Kur- und Neumärkische Dahrlehnskasse à Berlin . » 428.00
» » Königliche General-Direktion der Seehandlungs-
Olea Baln. ........ 2... > 1 228.55

Total: M. 164 902.00

 

At Lt) A A ba ted cot dolls

aula

ld

Lak. u tun Lil

tu dia Li

san Mlle ahi lh aa à

 
