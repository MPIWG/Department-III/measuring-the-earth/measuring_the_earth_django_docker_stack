ee

km

III ART

rn

Lu

RT TT

OI TEN

321

ETATS-UNIS D’AMERIQUR.

Les échelles de marée ont été rattachées aux nivellements de précision dans les
points suivants: -

Biloxi (Miss.), Sandy Hook (N. J.), Boston (Mass.), Washington (D. C.), Morehead
City (N. C.), Brunswick (Ga.), Old Point Comfort (Va.), Annapolis (Md.), Cedar Keys and
St. Augustine (Fla.).

*
* *

Le nombre des points de jonction entre les réseaux européens de nivellement est

aujourd’hui suffisant pour que l’on puisse, avec une approximation déjà grande, fixer les
relations de hauteur qui existent entre les zéros hypsométriques des divers pays. Il y aurait
done un grand intérêt à ce que les États dont les lignes de rattachement ne sont pas
encore calculées voulussent bien procéder à cette opération dans le plus bref délai possible.
Cela nous permettrait de présenter à la prochaine Conférence Générale un tableau d'ensemble
donnant, par rapport à une même origine, les hauteurs actuelles les plus probables des
points de jonction des différents réseaux et celles des repères fixes, placés à côté des maré-
graphes ou médimarémètres, et auxquels sont directement rapportés les niveaux moyens
déduits des indications de ces appareils.

VI. Publications, parues depuis 1895, relatives aux nivellements
de précision.

ALLEMAGNE.

19, LANDESAUFNAUME.

»Die Nivellements- Ergebnisse der Trigonometrischen Abteilung der Kéniglich Preussi-

schen Landesaufnahme’’.

Fascicules I, I, HL WI. . Annee 1295

» IV; V, VE Ve 1897
» IX, 2, XP KE 2 1898
» Xi... 0,0 0005 1900

2°. MinisteRE pus Travaux Pusuics.
Bureau des Nivellements de précision.

À. Publications émanant du Directeur du Bureau,
Geheimer Regierungsrat Prof. Dr. Skısr.

l. Die hydrostatische Diferentialwage. (System Seibt-Fuess). Zentralblatt der Bauverwal-

tung. — Berlin, année 1896, Page 162.

ag

Dr

oe
4
|
|
a
A
4
4
A
m
H
u

 
