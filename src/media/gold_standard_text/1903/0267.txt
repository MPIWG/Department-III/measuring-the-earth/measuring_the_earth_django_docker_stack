en

TE Er ——

Ine TANT

an

rine

1 FWA

TER

241

Diedolshausen, wo in Ermangelung einer Höhenmarke in der nächsten Umgebung an den

5'/, km. entfernten und 277 m. tiefer gelegenen Bahnhof von Schwierlach angebunden
werden musste, einen grösseren Umfang annahm.

Bei der Ableitung der in der nachstehenden Tabelle gegebenen Resultate sind die
Koefficienten fiir Temperatur und Luftdiehte für die Reihe 1900 nach gefälliger Mitteilung
von Herrn Geh. Hofrat Haid in Karlsruhe, für die Messungen 1901 nach den Wiener Be-
stimmungen (1894) angenommen worden; für die neuen Fechner’schen Pendel hat Herr Prof.
L. Haasemann in Potsdam die Gefälligkeit gehabt die Konstanten zu bestimmen. Die Be-
rechnung der topographischen Korrektionen verdanke ich für 1900 und 1901 den Herren
Dr. Furtwängler und Prof. Haasemann, für 1903 Herrn Dr. Scholz in Potsdam. Die An-
gaben für die geologische Bodenbeschaffenheit wurden mir von Herrn Prof. Dr. Benecke
und Herrn Prof. Dr. Bticking freundlichst mitgeteilt.

Wie man aus der Tabelle, in der für Strassburg selbst der von Herrn Haid über-
tragene Wiener Wert (von Oppolzer) zu Grunde gelegt ist, ersieht, halten sich die Unter-
schiede zwischen dem beobachteten und dem theoretischen Wert der Schwere im Allgemeinen
innerhalb enger Grenzen. Unverkennbar treten aber in der Rheinebene kleine Defekte, im
Durchschnitt von etwa — 0.007 em. auf, in den Bergen dagegen Überschüsse bis zu etwa
dem dreifachen dieses Betrages. Ein auffälligeres Verhalten zeigen die beiden Stationen
Zabern und Saarburg, wo die ideelle störende Schicht eine Stärke von nahe drei einhalb-
hundert Meter und mehr erreicht. Erst spätere Messungen in den benachbarten Gebieten
werden entscheiden können, ob und in wie weit hier ein Zusammenhang mit dem »geolo-
gischen” Aufbau angenommen werden darf.

Strassburg, 1903 August. BEoxER.

31

 
