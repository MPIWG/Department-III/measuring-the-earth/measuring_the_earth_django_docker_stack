 

200
denzen verglich. Letzterer behielt, um Gangstörungen zu verhüten, seinen Standort während
der ganzen Dauer der Stationsbeobachtungen unverändert bei. Aus diesen gleichmässig über
die Dauer der Pendelbeobachtungen verteilten Uhrvergleichungen wurde, unter Voraussetzung
konstanten Ganges für Plaskett, der momentane Gang von Frodsham für die Epochen der
Pendelsätze hergeleitet. In Turin wurde der Gang von Frodsham durch Vergleichen mit der
Hauptuhr des Observatoriums ermittelt.

Aus der innern Übereinstimmung der Schwingungszeiten findet Herr Prof. Armo-
nerm den m. F, einer Schwingungsdauer S zu + 13, den m. F. des Stationsmittels aller
S im Durchschnitt zu + 8 Einh. d. 7. Dez. und den m. F. einer relativen Schwerebestim-
mung g—g, zu + 0.004 en.

XVe. MESSUNGEN DURCH HERRN PROFESSOR VENTURI

Herr Prof. A. Venturı hat 1899 im Westen Siciliens Schwerkraftsbestimmungen
auf 6 Stationen ausgeführt: Determinazioni di gravità nella regione occidentale della Sicilia
(Atti della R. Accademia, VI), Palermo 1901. Die Tabelle ist bereits in den Nachtrag zum
Schwerebericht von 1900, S. 384, aufgenommen und hier nur mit x, (1901) umgerechnet
worden. Auf S. 381 u. f. wird dazu folgendes bemerkt:

»Es wurde ein Schneider’scher Apparat, der dem Geod. Institut der Universität
Palermo gehört, mit 4 Pendeln (N°. 116—119) benutzt. Die Konstanten hat Herr Oberst
von Srerneck bestimmt zu 49.3 und 575 für St.Z. Die Gänge der Hawelk’schen Pendel-
uhr wurden durch einen Chronometer von Weichert kontrolliert. Zu den Zeitbestimmungen
kam ein grosser Theodolit zur Verwendung, mit dem Sternzenitdistanzen im I. Vertikal
gemessen wurden. Die Beobachtungen verteilen sich immer auf mindestens 3 Tage; jedes
Pendel wurde 4-mal bestimmt. In Palermo wurde zu Anfang der Reihe (im Juli 1899) und
1 Jahr nach ihrer Beendigung (im Sept. 1900) beobachtet; die Schwingungszeiten der
Pendel zeigten für diesen Zeitraum eine Abnahme in der 7. Dezimalstelle im Betrage von
20, 10, 88 u. 47 Einheiten. Diese Änderungen konnten jedoch wegen der langen Zwischen-
zeit keine Berücksichtigung finden. Auch der Anschluss an Wien ist nur einseitig.

Nach der Übereinstimmung der Ergebnisse der 4 Pendel untereinander würde der
m, F, einer Bestimmung von g relativ zu Wien etwa + 0.005 cm betragen. Hierbei ist
nicht berücksichtigt die etwaige Änderung der Pendel in der Zeit von den Wiener Beobach-
tungen bis zu den Stationsbeobachtungen, Juli—Sept. 1899. Von dem Einfluss des Mit-
schwingens des Pendellagers spricht der Verfasser nicht; vielleicht war der Apparat mit
einem Wandstativ versehen.

Die vom Verfasser gegebenen topogr. Korrektionen habe ich etwas abgeändert; er
berücksichtigt nämlich auch den Einfluss des Meeres, den ich aber weggelassen habe”.

XV}, Mussunaun DURCH HERRN FREGATTEN-KAPITÂAN CAGNI.

Unsere Angaben entstammen der Schrift: Determinazioni di gravità relativa, esequite

 

I
=
3
a
a
i

ANd A AAA Aly 1) 9

1 1h bl Adda fon

i
|
|

 
