 

|
|
$
|

A PTT TUT TN To}

DEUXIEME SEANCE.

Mercredi, 5 août 1903.

Présidence de M. le Général Bassot.
Sont présents:

I. Les deleguös: MM. Albrecht, Anguiano, Bakhuyzen, Bassot, Becker, Bodola, Bersch,
Bouquet de la Grye, Bourgeois, Celoria, Darboux, Darwin, Fenner, Ferster, Gautier, Guar-
ducci, Haid, Helmert, Heuvelink, Lallemand, Matthias, Nissen, Pomerantzeff, Poincaré,
v. Richthofen, Rosén, Sanchez, Schmidt, Schorr, Sugiyama, Tanakadate, Tittman, Valle, Weiss, .

Zacharie.

II. Les invités: MM. de Anda, Arrhenius, Johansen, le Maire, Momberg, Nyholm,
Omori, Paulsen, Pechiile, Petersen, Rasmussen, K. Rosen, Sand, T. Thiele, F1. TDhnele,

La séance est ouverte neuf heures et demie.

M. le Président invite le secrétaire à lire le procès-verbal de la première séance.
M. le Secrétaire donne lecture du procès-verbal, qui est ensuite mis aux voix et adopté.

M. le Président donne la parole à M. Helmert pour la lecture de son rapport sur
les travaux du Bureau central.

M. Helmert présente le rapport suivant des travaux du Bureau central, depuis la
13e conférence à Paris en 1900.

Messieurs,

Depuis la conférence de Paris en 1900, j'ai publié trois rapports sur les travaux
du Bureau central pendant les années 1900, 1901 et 1902. I suffit d’en donner aujourd’hui
un court aperçu. :

L'objet principal de nos travaux a été le service international des latitudes et la
réduction des observations fournies par ce service, afin d’en déduire le mouvement de l'axe
terrestre à l'intérieur du globe. Grâce à l’activité et à la persévérance des observateurs,
et aussi à l'appui des autorités des différents états, les résultats du service des latitudes ont été
satisfaisants. M. le prof. ALBRECNT a publié successivement plusieurs rapports complets sur

 
