Ten |

1 4 RR TAT

 

==
an
sr
iS

209
intendent der C. a. 6. 8., Herr ©. H. TITTMANN, über relative Schweremessungen, die in
der North Tamarack Mine bei Calumet (Michigan) in verschiedenen Tiefen mit doppeltem
Anschluss an Washington ausgeführt wurden. Es wurde eine Reihe gleichzeitiger Beob-
achtungen an der Oberfläche und in 1400 m (?) Tiefe, und später eine zweite Reihe an der
Oberfläche und in der Tiefe des Meeresniveaus, 360 m (?) unter der Oberfläche, vorgenommen.
Die Ergebnisse dieser interessanten Arbeiten liegen zur Zeit noch nicht vor.

 
