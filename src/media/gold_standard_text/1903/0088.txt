 

 

 

 

 

 

 

DRITTE SITZUNG

Freitag den 7. August 1903.

Präsident: Herr General Bassot.

Anwesend sind:

I. Die Herren Delegirten: Albrecht, Anguiano, Bakhuyzen, Bassot, Becker, Bodola,
Bôrsch, Bouquet de la Grye, Bourgeois, Celoria, Darbour, Darwin, Fenner, Förster, Gautier,
Guarducci, Haid, Helmert, Heuvelink, Lallemand, Matthias, Nissen, Pomerantzeff, Poincaré,
v. Richthofen, Rosen, Sanchez, Schmidt, Schorr, Sugiyama, Tanakadate, Tittman, Valle, Weiss,
Lacharie.

Il. Die Herren Geladenen: de Anda, Buchwaldt, Johansen, le Maire, Momberg,
Nyholm, Omori, Paulsen, Pechiile, Petersen, Rasmussen, K. Rosén, Sand, H. Thiele, T. N. Thiele.

Die Sitzung wird um 9 Uhr 30 eröffnet.
Der Secretär verliest das Protokoll der zweiten Sitzung, welches genehmigt wird.

Der Herr Präsident theilt mit, dass nach der Tagesordnung heute mehrere Landes-
berichte zur Verlesung kommen werden und ertheilt zunächst Herrn Bourgeois das Wort
zur Mittheilung seines Berichtes über die in Frankreich von der geographischen Abtheilung
des Generalstabs in den Jahren 1900 bis 1903 ausgeführten geodätischen Arbeiten. Siehe
Beilage A Xa.

Der Herr Präsident dankt Herrn Bourgeois für seinen werthvollen Bericht, be-
sonders fiir die auf die Messung des Meridianbogens bei Quito beztiglichen Mittheilungen,
und ertheilt Herren Bouguet de la Grye und nachher Herrn Bourgeois das Wort zur Be-
schreibung und Erklärung eines Instrumentes, des Prismenastrolabiums von den Herren Craupr
und Darrexcourr, zur Bestimmung der Breite nach der Methode gleicher Sternhöhen von
Gauss. Mit Hülfe einer grossen Abbildung und unter den Delegirten vertheilten Heliogra-
vüren erklärt Herr Bourgeois die verschiedenen Theile des Instrumentes und macht die Mit-
theilung, dass er die Absicht habe mit diesem Instrumente Breitenbestimmungen in allen
trisonometrischen Stationen des französischen Dreiecksnetzes anstellen zu lassen. Nach seiner
Ansicht wird die Anwendung dieser Methode auch die Breitenbestimmungen in Ecuador
sehr erleichtern.

au pi em i Bb Ak a Da al

t Til

Lil

kur ml

j iii

screens name Mil |

 

 
