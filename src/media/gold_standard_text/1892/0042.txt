|
|
|
|
|
|
|
|

 

30

La Conférence générale adopte sans discussion et à l'unanimité les propositions de lu
Commission permanente, concluant à une diminution de 10 °/ des valeurs initiales des con-
lributions conventionnelles et ratifie La nouvelle échelle des parts contribulives pour la période
1893-95, établie conformément à celte réduction.

M. le Secrétaire, en continuant son compte-rendu, rapporte que deux des anciens et
dignes collègues de l’Association célébreront dans quelques jours leurs anniversaires, M. le
eénéral baron Wrede son 90e et M. le colonel Andreæ son 80e. La Commission pense que la
Conférence générale, réunie en ce moment, voudra témoigner à ces vénérables confrères de
ses sentiments de sympathie en chargeant son Bureau de leur faire parvenir à la date voulue
des lettres de félicitations.

M. le Président croit être l'interprète de l’Assemblée en constatant que celte propo-
sition obtient un assentiment unanime.

Passant à un autre sujet, concernant l’organisation de Association, la Commission
permanente à établi que, quant à son renouvellement périodique, cinq des anciens neuf
membres ayant été désignés à la Conférence de Paris comme membres sortants, ce sont
maintenant les quatre autres membres, savoir : MM. Faye, von Kalmär, Stebnitzki et Zacha-
riæ qui doivent sortir cette fois. Mais, comme la Conférence de Paris à augmenté Jusqu'à
onze le nombre des membres, en nommant MM. Davidson et Hennequin, la Commission a
décidé de faire désigner par le sort le nom de celui de ces deux membres qui doit sortir de
la Commission ; c’est le nom de M. Davidson qui est sorti de Purne.

La Commission propose à la Conférence de ne pas remplacer M. Davidson imme-
diatement, de réserver en principe sa place pour un géodésien des Etats-Unis et de charger
la Commission permanente de choisir par cooptation le successeur de M. Davidson, après
avoir pris les renseignements nécessaires à Washington.

Quelques membres ayant demandé et obtenu des explications sur la situation ac-
tuelle de M. Davidson, comme unique délégué des États-Unis, la proposition de la Commis-
sion est adoptée par neuf voix contre trois.

On procède ensuite à l'élection des quatre membres prescrite par la Convention pour
le renouvellement périodique de la Commission.

M. le Président appelle les uns après les autres les douze Etats représentés à la
Conférence, en priant les délégués de chaque Etat de designer celui d’entre eux qui est chargé
de porter la voix de l'Etat. Il fait délivrer à ces douze membres des bulletins sur lesquels il
les prie d'écrire les quatre noms de leur choix. Le dépouillement du serutin donne le résultat
suivant : un des bulletins étant déclaré nul par le bureau, M. Faye est nommé par one voix,
M. von Kalmar par onze voix, M. Zacharie par onze voix, M. Stebnitzki par qix vor, Mod Ar-
rillaga a obtenu une voix.

M. Fuye remercie ses collègues de la confiance qu’ils viennent de lui continuer.

MAM HME |) Mdm A hi do tik lb

wei) eam A 1 188411.) 188

Lak ludo

to ml

à 4 éme ah a, had u

 
