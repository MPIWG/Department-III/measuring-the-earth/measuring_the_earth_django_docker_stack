 

HHlnetisbseisstnt

Eset nate rere poet TT TET Cte

AA

Au contraire, à la condition d’être entourées d’une enveloppe isolante d’épaisseur
convenable, les régles bimétalliques donnent des résultats beaucoup plus certains.

Cette double condition se trouve précisément réalisée dans les mires à compensation
du colonel Goulier, employées au nivellement général de la France, de l'Algérie et de la Bel-
gique. — La règle bimétallique servant d’étalon est, en effet, noyée dans l’âme même de la
regle en bois, ce qui met les deux tiges métalliques à l'abri des variations brusques de tem-
pérature et leur assure un élat thermométrique relativement stable.

La mire à compensation du colonel Goulier présente donc, à ce point de vue, des
avantages incontestables sur les autres dispositifs de mires, usités pour les nivellements de
précision.

M. von Kalmär répond aux observations de M. Lallemand que les incertitudes sur la
température des mires ont, d’après l’expérience, une influence moins considérable sur les
résultats que d’autres sources d’erreur. Après cette remarque, la proposition qui termine le
rapport de M. von Kalmar est adoptée.

M. le President remercie l’auteur de son travail considérable et, renvoyant aux séan-
ces ultérieures les autres rapports spéciaux et les communications annoncées, 1l propose
de commencer aujourd’hui encore avec les rapports sur les travaux des différents pays, et, en
suivant l’ordre alphabétique des pays, il prie MM. les délégués autrichiens, et en premier lieu
M. von Kalmär, de rendre compte des travaux en Autriche.

M. von Kalmar rend compte des travaux de Plnstitut géographique militaire y
compris le rapport sur les observations astronomiques, par M. le lieutenant-colonel À. von
Sterneck.

M. le colonel Hartl ajoute la partie concernant les travaux trigonométriques. (Voir
Aniexe Bl a, Uc.)

La quatriéme partie concernant les mesures de la pesanteur par M. von Sterneck,
ainsi que le rapport de la Commission géodésique autrichienne par M. Weiss sont renvoyés
à la prochaine séance.

M. le Président remercie les délégués autrichiens de leurs intéressants rapports et,
à la demande de M. Helmert, il fixe la prochaine séance à mardi à 2 heures.

La séance est levée à cinq heures et demie.

ASSOCIATION GEODESIQUE — 6

 
