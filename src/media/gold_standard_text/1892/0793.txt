 

en ges yr —

ti |

 

vi

IM i)

map mi

AURA DUO M LU pam Li

ane |

TABLE DES MATIERES

INHALTSVERZEICHNISS

Procès-verbaux des séances de Ja dixième Conférence générale de PAssociation géodésique

A

internationale, réunie à Bruxelles dn 26 septembre au 7 octobre 1893.

Premiere séance, 27 septembre 1892.

Liste des délégués .

Liste des invités. de

Discours d'ouverture de s E. Monsieur
Bernaert, chef du cabinet

Réponse de M. président de Pise.
ciation . Se ee ans

Nomination de M. le Cel Hennequin,
comme président de la Conférence .

Discours présidentiel de M. Hennequin.

M. le Gal Ferrero et M. le prof. Foerster
sont désignés comme vice-présidents

Différentes communications d’affaires du
Président wc

Rapport de M. Hirsch, Secrétaire per-
pétuel sce sateen a ace

Rapport de M. Melmert, directeur du
Bureau central

Liste des expéditions de publication ae
tes par le Bureau. ;

M. Albrecht dépose le mémoire cone
nant les résultats de l’expedition de Ho-
nolulu

Deuxiéme séance, 29 septembre 1892.

Lecture et adoption du procès-verbal de
la Are séance .

Ordre du jour

Résumé du Rapport de M. le Dr Mare cuse
sur les observations de Honolulu et
leurs résultats (voir Annexe C. I)

Notice de M. Cornu, et discussion sur les
précautions a prendre dans les obser-
vations astronomiques (v. Annexe C. IT)

Pag.
3-20
3.4
a, 6
6, 7
7-9

9
9-13

13

20
21-30

21
22

|

 

Communication de M. Defforges sur le
remplacement du niveau par l’observa-
tion directe et réfléchie au moyen de
deux lunettes fixées invariablement à
l’axe. ee

Rapport de M. le Gal Ferrero sur les
triangulations dans les différents pays
et leurs erreurs (voir Annexe A. [.).

Rapport de M. le Ce! Bassot sur les me-
sures des bases (voir Annexe A. II).

Troisiéme séance, 3 octobre 1892.

Lecture et adoption du procès-verbal de
la 2e séance ne

Réponse de M. Beer 3 quelques re-
marques formulees dans le rapport de
M. Bassot 2

Communication de M. Mendenhall, par
M. Helmert, sur un nouvel appareil de
bases {mesures dans la glace), et dis-
cussion .

Communication d’ ee eb propositions
de la Commission permanente, faites
par le Secrétaire.

La Conférence adopte la na de
10 0/0 des contributions annuelles et
la nouvelle échelle des contributions
pour la période de 1893-95

La Conférence décide de réserver le Ate
siege de la Commission permanente a
un géodésien des Etats-Unis, et charge
la Commission permanente de désigner
par cooptation le successeur de M. Da-
vidson .

21,28

32, 33

3h, 35

36

36

 
