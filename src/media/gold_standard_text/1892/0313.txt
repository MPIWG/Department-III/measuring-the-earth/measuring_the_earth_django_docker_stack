 

 

 

 

 

S15
ee Elimination des
U), | 5 5 r . TIM 1 Y Ts r
“| Année et mois. équations TITRE DE LA PUBLICA TION REMARQUES
| personnelles. ee
1867 juillet, ! Détermination di- Plantamour, Wolf et Hirsch. Dé- Opérationssimultanées entre Neu-
août. recte. | termination télégraphique de châtel, Zurich et Riehi-Kulm.

 

     

Al | 1868 juillet. Plantamour et Hirsch, Détermi-
4 I août. nation téléoraphique de la dif-
el, ference de longitude entre les

|

| 1869 juillet.

|| 1870 juin,
| juillet.

D
1 1872 juillet, |
, août.

 

 

{
|
|
|

 

 

Plantamour et Hirsch. Détermi-

Plantamour et Wolf. Détermina- |

la différence de longitude en-
tre la station astronomique du
Righi et les Observatoires de
Zurich et de Neuchatel. Gene-
ve et Bâle, 1871.

Stations suisses. Genève et

Bale, 1872.

nation télégraphique de la dif-
ference de longitude entre les
Observatoires de Milan et de
Neuchâtel et la station astro-
nomique du Simplon. Genève
et Bâle, 1875.

tion télégraphique de la diffé-
rence de longitude entre l’Ob-
servatoire de Zurich et les sta-
tions astronomiques du Pfän-
der ef du Gabris.

 

Les trois différences de longi-
tude reposent en grande partie
sur les mémes observations :
en outre on les à compensées
pour la clôture du triangle.

Opérations simultanées entre Neu-

chätel, Simplon et Milan. Les
trois différences de longitude
reposent en grande partie sur
les mémes observations: en
outre, on les à compensées
pour là clôture du triangle.

Opérations simultanées entre Zu-

rich, Gäbris et Bregenz (Pfin-
der). Les trois différences de
longitude reposent en grande
partie sur les mêmes observa-
tions; en outre. on les a com-
pensées pour la clôture du
triangle.

 

 
