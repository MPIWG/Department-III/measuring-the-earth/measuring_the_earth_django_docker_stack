 

PPT PTT SE it FY

qq

PTT EEE ech se
RT TE à

UT OPEN

4

At

Annexe A. II.

RAPPORT SUR LA MESURE DES BASES

Par M. ze CoLoNsr BASSOM

i. — Les bases mesurées depuis la dernière Conférence générale sont au nombre de
SIX, SAVOIr :
Trois en France,
Une en Grece,
Une en Prusse,
Une dans la Colonie anglaise du Cap.

Les renseignements concernant ces bases sont résumés dans le tableau suivant, en
tous points conformes aux tableaux publiés antérieurement.

Les trois bases de France appartiennent au réseau de la nouvelle Méridienne; deux
sont nouvelles, celles de Paris et de Cassel: la troisième, celle de Perpignan, est l’ancienne
base mesurée par Delambre avec l’appareil de Borda. La différence signalée entre l’ancienne
et la nouvelle mesure de cette base est assez considérable; elle atteint le 1/40000e; la nouvelle
mesure ayant été contrôlée par une triangulation spéciale, établie sur les deux segments, le
désaccord provient probablement d’une erreur matérielle faite par Delambre dans son opération
de mesurage; peut-être pourrait-on l’attribuer à un mouvement du sol. On sait que la trian-
gulation de Delambre n’avait pu accorder les deux bases de Melun et de Perpignan, tandis
que celle de la nouvelle Méridienne accorde à 1/250000€ près la base de Paris et la nouvelle
mesure de la base de Perpignan. l’ancienne base de Melun, située dans le voisinage immédiat
de la base de Paris, se trouve d’ailleurs vérifiée trés exactement par la triangulation; on ne
saurait donc attribuer l'écart trouvé à la base de Perpignan à une erreur sur les règles de
Borda, et on ne peut le justifier que par une des deux hypothèses indiquées plus haut.

La base mesurée en Grèce est la base fondamentale de la nouvelle triangulation établie
dans ce pays, sous la direction de notre collègue autrichien, M. le Lieutenant-Colonel Hartl.
Elle ne peut être rapportée au metre international, Pappareil autrichien dont on s’est servi
n'ayant pas encore été étalonné à Breteuil.

! L’annexe A. I, qui devait étre le Rapport de M. le Ga! Ferrero sur les Triangulations européen
nes, à été imprimé à Florence par les soins de son auteur; et comme il est très volumineux, pour ne pas sur-
charger outre mesure ce volume des Comptes-Rendus, nous avons dû nous décider à faire paraître ce Rapport
sous forme de brochure à part, qui accompagnera les Comptes-Rendus et sera distribuée en mêmetemps. A. H.

 

 
