 

STATION

91 Kharkow.Ancien Obser-
vatoire del’ Université.

Kichinef. Döme de la
cathédrale

&
Û

93 Routchouk. Clocher de
la nouvelle cathédrale.

34 Orenbourg. Pilier, stat.
astron. du 52™¢ paral.

95 Orsk. Pilier, stat. astr.
du 52e parallèle.

96 Tiflis. Petite tour de
l'Observatoire physi-
a 5.

97 Tiflis. Petite tour de
l’Observatoire physi-
que as

28 Bakou. Minaret du palais
du Khan. ee

29 Bakou. Minaret du palais
du Khan. see

30 Chémakha. Dôme de la
cathédrale

31 Nowo-Alexandria. Cou-
pole de la chapelle

32 Vloclavsk.
couvent.

Döme du

33 Tchenstokhov. Dome de
l’église de paroisse

 

 

Latitude.

49 5990

43 50 39.2

51 45 27,4

ul iS 59

40 21 59,5
40 21 58,6

40 37 37,6

52 39 28,8

50 48 51,1

Erreur
probable.

uv

+ 0,28

ie 0, 14

+ 0,14

L 0,14

 

 

OBSERVATEURS

Fedorenko.

+ 0,14 | Miontchinski et Zamotch-

nikof.
»
Bonsdorf.
»

Oblomievski.

Gladychef.

Polanovski, Mion-
tehinski.

Kuhlbere.

Polanovski, Mion-
tehinski.

»

vertical de Repsold.

Distances

sold.

 

 

Instrum. des passages.

Distances zenithal® Cel:
ele vertic. de Repsold.

»

| Instrum. des passages.

Méthode d’égales distan-
ces zénithales. Cercle

I
|

 

Méthodes et Instruments
employés.

|
} |
|

zénithales, |
Cercle vert. de Rep:

 
