i
i
i

 

A

 

Annexe C. V.

NOTE

SUR LES

VARIATIONS DE LONGUEUR DES MIRES DE NIVELLEMENT

par aM. OH. LALEEMAND

Ingenieur en Chef du Service du Nivellement general de la France.

EXPOSE

L’une des causes d’erreur qui affectent les nivellements de haute précision est la va-
riation de longueur du bois des mires sous diverses influences.

Sur la proposition du regretté Colonel Goulier, la Commission du Nivellement général
de la France avait, en 1880, inserit dans son programme l'étude de ces influences, la re-
cherche des espèces de bois les moins sensibles à ces variations et celle des modes de prépa-
ration les plus propres à les atténuer.

M. le Colonel Goulier a été chargé de ces études et, avec le concours de M. le Lieute-
nant-Colonel du Génie Richard, il a effectué à ce sujet, de 1884 à 1886, une série complete
d'expériences, dont la relation devait figurer dans un Mémoire en préparation, mais dont
il nous semble opportun de faire connaître succinctement, dès aujourd’hui, les détails et les
résultats.

Tel est l’objet de la présente note.

I. CAUSES DES VARIATIONS DE LONGUEUR DU BOIS DES MIRES.

Trois causes principales paraissent influer sur les variations de longueur d’une mires
be Son:

 

 
