 

   

oS : vil. — Grande Bretagne.

nee

 

 
 
  
  
 
  
 
  
  
  
  
  
  
  
 
  
 
 
 
  
 
  
  
 
  
 
  
 
  
  
  
  

the direction of Sir Henry James — were the extension of the Triangulation through
the North of France into Belgium in 1861; the comparison of the various national stan-
dards of length; the positions of foe and Haverfordwest Longitude stations on
the great arc of Parallel, accounts of which, drawn up by Colonel Clarke, were published
_ in 1863, 1866, and 1867 on. — These complete the contributions ni the Ordnance
a D Survey of Great Britain to the geodesy of Europe, and towards the
determination of the figure and dimensions of the Earth.
In further explanation it may be stated that the Triangulation of England and
Wales and extension into Scotland was at first carried on for the purpose of supplying
_ Bases for the construction of the series of maps on the scale of one inch to à mule.
m 1825, however, the Government having ordered that county maps of Ireland should
be ade on a scale of 6 inches to a mile, operations in England and Scotland were
: partially suspended till the completion of the Irish Survey in 1841. And as it was
_ then decided that the maps of the North of England and Scotland must be on the same
scale as the Irish maps, it was found necessary to revisit most of the Trigonometrical
Stations. The exigencies of progress and economy induced Colonel Colby to instruct,
under the superintendence of his then executive officer, Captain Yolland, non-commis-
-sioned Officers to make observations with the large Instruments. During che years 1842,
1843 and 1844 commissioned Officers observed with Airy’s zenith sector. The obser-
vations from 1845 till 1850 were made by Serjeants J. Steel and W. Jenkins, Royal
Sappers and Miners. Up to 1842 the chord method of calculating the angles and sides
+ O1 triangles was in use: since then the more simple, well known, method of Legendre
has been adopted.

Southampton, 24% June 1887. | 2 2 Wer,
L: 'Oolonel R. E. for D. G. 0. 8.

J

 

ORDNANCE SURVEY OFFICE,
Southampton, 8% May 1889.

Sir,

| Referring to your communication of the 28 December last, relative to the adoption
‘by the permanent Committee of the International Geodetic en of the formula
proposed by General Ferrero as the approximate measure of the exactitude of Trian-
gulations, I have the honor to state in reply to your ee that as regards the

Triangulation of Great Britain the value Of ips ae can = 1.” 79 derived from 476
independent triangles.
I have the honor to be,

Si, . ;
Your obedient Servant,
C. W. Wison Col,
To Director General of the Ordnance Survey
Professor F. R. Hemer, of Great Britain.

‘Berlin.

   

Lida.
