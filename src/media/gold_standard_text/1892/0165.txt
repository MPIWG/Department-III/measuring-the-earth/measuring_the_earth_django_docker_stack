 

near a

EE SH

ii fac

EE Teen
DT re

pds his Aether: a x
PASS TON ER MATE QU RRQ 2

à:

my

107

14. Italien (seit 1876) . . . . 4630 km. doppelt, in entgegengesetzter Rich-
| tung.

15. Niederlande (1875-86) . . . 2152 » doppelt, in entgegengesetzter Rich-
lung.

16. Portugal (seit 1882) . . . . 1353 » doppelt, in entgegengesetzter Rich-
lung.

17. Russland (seit 1875) circa . . 10300 » doppelt, in entgegengesetzter Rich-
tung, mit Ausnahme von 590 km.

18. Schweiz (1865-82) . . 4354» 3002 km. doppelt, davon 2130 lam:

in entgegengesetzter Richtung,
dann 98 km., theils drei, theils

vierfach.

19. Schweden (seit 1886) . . . 3970 » doppelt, in entgegengesetzter Rich-
tung.

20. Norwegen Seit I, = 338 » davon 4 km. doppelt bis vierfach,

der Rest einfach.

Zusammen eirca : 109800 km.

Wenn man von der im letzten Berichte (1889) stehenden Summe, 116000 km., die
Länge des Nivellement Bourdaloué mit 15000 km. und jene des älteren Nivellements in Bel-
gien mit 12500 km. abzieht, so erhält man die Zahl 88500 km., welche mit obiger Summe
verglichen, die Vermehrung der Länge der Nivellements im Laufe der letzten drei Jahre mit
rund 14300 km. gibt.

Um den Ausgangshorizont fiir das Nivellement in Afrika festzulegen, wurde in Bone,
wenige Meter von dem dort functionirenden Médimarémétre, am sogenannten « Rocher du
Lion », eine Haupthohenmarke errichtet, welche durch einen Monolith geschützt ist. In
Norwegen wurde im Hofe des geographischen Institutes in Christiania eirca zwei Meter
lief auf festem Felsengrund ein viereckiger Granitblock von 1"5 Höhe und 0”8 Quer-
schnittseite angebracht und auf diesen ein Block von Labrador mittelst Cement fest verbunden.
Der Labradorblock ist 16 hoch und ragt zur Hälfte über den Boden heraus.

Nicht nur die vier Seitenflächen sondern auch die obere Fläche desselben sind platt
geschliffen und polirt.

In die obere Fläche ist ein kupferner Bolzen mit convexem Kopfe eingelassen
dessen Oberfläche den Normalpunkt für das Präcisions-Nivellement markirt.

Zum Schutze dieses Bolzens ist er mit einer kleinen abhebbaren Pyramide aus La-
brador gedeckt.

Der Normalpunkt ist durch ein Nivellement mit dem selbstregistrirenden Pegel im
Hafen zu Ghristiania verbunden.

Die mittlere Höhe des Meeres ist durch Beobachtungen an diesem Pegel in den letzten
9 Jahren (1886-1890) ermittelt worden. Aus diesen Beobachtungen resultirt, dass der Null-

 
