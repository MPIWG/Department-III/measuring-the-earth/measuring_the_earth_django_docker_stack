HN RS

(RP pT Tee HN

na ar À HAUT UT

Il

TER a ET tier

201
leré ne pas vous présenter aujourd’hui les résultats d’un travail incomplet, mais de faire
imprimer les résultats de mes calculs dans une annexe à mon rapport.

Je tiens pourtant à mettre sous vos yeux et à soumettre à voire avis les bases de
mes calculs, c’est-à-dire les valeurs et les poids des différences de longitude qu'il faut y in-
troduire.

La détermination des poids est toujours un peu arbitraire, puisque dans la plupart
des cas les erreurs probables qui sont indiquées dans les publications ne sauraient nous
guider, Dans mes caleuls, j'ai représenté presque toujours les poids des différentes longitudes
par des nombres entre { et 3; le poids là n’a été attribué qu’à fort peu de résultats. Le
poids 3 a été donné à des opérations faites dans de bonnes conditions avec échange
des observateurs et avec des instruments identiques, ou bien avec échange des instruments.
Quand l'équation personnelle n’a pas été éliminée par l'échange des observateurs mais par
une détermination directe, où quand les instruments étaient d’une construction différente,
j'ai diminué le poids jusqu’à 2 ou 1 ; J'ai aussi adopté une valeur plus petite pour le poids
quand les observations n'étaient pas publiées, quand des anomalies s'étaient présentées pen-
dant les observations, ou quand le nombre des soirées était fort restreint.

J'ai dû faire abstraction de ces règles dans l'appréciation de la plupart des différences .

de longitude déterminées en Russie, puisque je n’ai pu examiner en détail les résultats des
observations publiées dans les annales et les journaux russes; j'ai donné à tous ceux-ci le
meme poids: 1. Il se peut que dans beaucoup de cas ce poids soit trop petit, mais influence de
celle erreur sur la compensation du réseau sera minime, puisque les stations russes ne sont
reliées qu’en peu de points aux autres parties du réseau.

Une question pas facile à résoudre était d'indiquer avec quel poids il faut introduire
dans le réseau les différences de longitude obtenues pendant la même période par les mêmes
observateurs entre plus de deux Stations, puisqu’alors ces différences ne sont pas indépen-
dantes les unes des autres. Aussitôt que pour chaque soirée on a obtenu des observations
dans toutes les stations A, B, C, etc., et que l'erreur de l'échange des signaux peut-être négli-
gée vis-à-vis des autres erreurs, on peul traiter la question comme celle de Ja compensation
des angles entre un point initial et un certain nombre de signaux, en considérant comme in-
connus non pas les angles, mais les directions, et en introduisant une erreur Constante pour
la direction initiale. M. Helmert a agi de la sorte dans un petit calcul de compensalion publié
dans les G. R. des séances de la Commission permanente à Salzbourg, pag. 60.

Quand on n’a pas toujours employé les mêmes soirées pour toutes les différences de
longitude entre A, B, C, ete., et c’est le cas general, on serait libre de traiter la question
d’après la même méthode, si l’on pouvait seulement indiquer de quelle manière le poids
d’une différence de longitude dépend du nombre des soirées et du nombre des éloiles obser-
vées; j'avoue que cela m'a paru impossible, puisque les équations personnelles et l’état
particulier des instruments, qui restent en partie constants durant la période entière des
observations, ont une fort grande influence sur l’exactitude des résultats.

Afin de trancher la question, je me suis laissé diriger par la considération suivante,
c’est qu’en général les erreurs systématiques, dépendantes de l'observateur et de l’instrument,

ASSOCIATION GÉODÉSIQUE — 26

14

|

|
ue

 
