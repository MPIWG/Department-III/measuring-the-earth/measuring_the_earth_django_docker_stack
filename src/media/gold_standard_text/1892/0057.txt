 

gl

 

=

ARC ANGLAIS-FRANGAIS-
ESPAGNOL ee | ARG SCANDINAVE-RUSSE
16°-20° Par le Brocken Par Vienne

à l'Est de Vile de Fer 27°-28° de longitude. 32°-34° de longitude. 419-479 de longitude.

 

A partir de 74° de latitde

 

A partir de 68 '/2°

 

A partir de 64° de latitde Norvege

Norvege Suede

 

À partir de 60 3/,° de latde Norvège Suède Finiande

 

Danemark

Iles Shetland Allemagne Allemagne Russie

i \
(Brocken) Autriche

(Vienne)

Angleterre jusqu’à 45° de latitude

: Italie supérieure
France (Paris) E ;
Dalmatie
Corse
Italie du Sud

Sardaigne Sicile

 

Espagne ee Malte
Tunis

Algérie jusqu’a 36° de latitude
ge

 

jusqu’a 33° de latitude

jusqu’à 32 1/20 de la- —
titude

 

 

Amplitude = 28 1/,°

 

 

 

 

Au Nord, tous ces ares sont limités par la mer, tandis qu’au Sud un prolongement
est possible. En étudiant de plus prés ces arcs, on reconnait le grand intérét qu’ils offrent pour
la connaissance des courbures spéciales du continent.

Le troisiéme are (par Vienne) qui traverse toute la Suede, sera bientöt termine et
calculé complètement pour l'Allemagne, l'Autriche et P’ltalie ; pour la Suède aussi, où l’on pos-
sède déjà beaucoup d’observations. Dans ces circonstances, il serait extrêmement regrettable
que l’on ne se mit pas sans retard à lexécution des rattachements et jonctions, puisqu'il
faut toujours quelques années pour tout terminer.

M. Rosen répond au désir exprimé par M. Helmert que les jonctions demandées dans
là partie sud-ouest de la Suède sont certainement très désirables. Cependant il peut déjà

 
