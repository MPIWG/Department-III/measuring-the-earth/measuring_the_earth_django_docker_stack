ee en

Te:

 

a UN

li) ua] | in!

ld

=
a
=
as
oz
=
—
=
=

033

gewichten durch einfache Flaschenzüge von Innen aus bequem geöffnet und geschlossen
werden. Zur Herstellung einer kräftigen Ventilation waren an den unteren und oberen Enden
der äusseren Wände besondere Ventilationsöffnungen angebracht worden. Ausserdem be-
fanden sich unter der nördlichen und südlichen Dachklappe noch zwei vertikale Doppelklappen,
welche ebenso wie die ersteren vor Beginn der Beobachtungen geöffnet wurden. An der
nordöstlichen Ecke des Beobachtungshauses war schliesslich noch eine Doppelthüre angebracht,
die äussere von Holz und die innere aus leichtem Drahtgitter. Durch Oeffnung der ersteren
war es möglich, den fast regelmässig wehenden Nord-Ost Passatwind sowohl am Tage als
auch während der nächtlichen Beobachtungsstunden das Gebäude durchstreichen zu lassen.
Durch geeignete Hochlegung des Fussbodens um 073 über der Erdoberfläche war dafür ge-
sorgt, dass auch unter dem Beobachtungshause eine beständige Luftdurchstreichung statt-
fand. Um den Einfluss der Sonnenstrahlen möglichst zu vermindern, wurde das ganze Ge-
baude mit weisser Farbe tiberzogen.

Fortlaufend ausgeführte Thermometerablesungen sowohl im Innern als ausserhalb
des Beobachtungshauses, auf welche später eingegangen werden soll, haben den Beweis
geliefert, dass durch die erwähnten besonderen Einrichtungen die Temperaturausgleichung
während der Beobachtungsstunden nahezu vollständig erreicht worden ist. Daher dürfte das
soeben in seiner Construction beschriebene Beobachtungshaus als ein Modell für ein Feld-
Observatorium in tropischen Ländern gelten können.

Es sei gleich hier bemerkt, dass die amerikanische Station ganz nach dem Plane der
unsrigen eingerichtet worden ist. Der Platz für das Beobachtungshaus der U. S. Coast Survey
Expedition wurde dicht neben demjenigen des Centralbureau’s der Erdmessung, nur 9"45 in
Breite und 9739 in Länge entfernt, abgesteckt. Zur Eliminirung etwaiger lokaler Anomalien
der Strahlenbrechung wäre es vielleicht vortheilhafter gewesen, wenn die amerikanische
Station in weiter Entfernung von derjenigen des Centralbureaus, vielleicht sogar auf einer
anderen der hawaiischen Inseln errichtet worden wäre. Besondere Umstände machten jedoch
den engen Anschluss beider Stationen nothwendig. In Washington war nämlich gewünscht
worden, dass der amerikanische Beobachter die von uns ausgewählte Liste von Sternpaaren
nach genau gleichem Programm beobachten sollte, und unsererseits musste dies auch schon
aus dem Grunde willkommen sein, weil die Ausführung der Breitenbeobachtungen für das
Gentralbureau ebenfalls nur auf einem einzelnen Beobachter beruhte. Lediglich durch eine
enge Cooperation der beiden Astronomen konnte daher das Unternehmen gegen Unter-
brechungen durch höhere Gewalt gesichert werden. Glücklicherweise sind solche Störungen
persönlicher oder instrumentaler Art, auf die man im tropischen Clima und bei so grosser
Entfernung von mechanischen Werkstätten immerhin vorbereitet sein musste, nicht ein-
gelrelen.

Das Instrument, mit welchem die Breitenbeobachtungen in Honolulu ausgeführt
worden sind, war das von Julius Wanschaff in Berlin 1887 erbaute Zenithtelescop des Geo-
dätischen Instituts. Seine Einrichtung findet sich bereits mehrfach und zuletzt in dem von
Herrn Professor Albrecht der hohen Versammlung vorgelegten Bericht « über die Resultate
der Beobachtungsreihen in Honolulu » beschrieben. An jener Stelle befindet sich auch die

ASSOCIATION GEODESIQUE — 80

 
