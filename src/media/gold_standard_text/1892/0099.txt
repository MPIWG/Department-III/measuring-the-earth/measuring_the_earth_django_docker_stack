 

93
| auf der ganzen Basislänge etwas niedrigere Pfähle gesetzt, um die 15 Meter langen transpor- .
tablen Eisenschienen zu tragen, auf welchen man die Basisstangen vermitlelst Räder fortbe-
| wegen kann. Auf diese Weise genügen 8 Personen für die Beobachtungen und Hilfsleistungen,

und an einem Tage kann man eine Länge von 900 Meter messen.

Die Strecke von 100 Meter wurde zu verschiedenen Zeiten sei es 12, sei es 10 Mal
hintereinander gemessen; auf diese Weise liess sich ein gewisser Einfluss der Richtung, in
welcher die Messung stattfand, erkennen, so wie auch eine geringe Verschiebung der End-
punkte mit der Zeit. Der wahrscheinliche Fehler einer einzelnen Messung ist ungefähr or
der Länge. Die Akm lange Linie wurde in vier Strecken von je 250 Meter getheilt; sie
wurde zu verschiedenen Zeiten in beiden Richtungen gemessen. Das erste Mal war der Boden
durch eine Ueberschwemmung durchweicht, und die beiden ersten Messungen zeigten daher
einen Unterschied von ungefähr I"; hingegen, nachdem der Boden ausgetrocknet war, sank
der Unterschied der Doppelmessung auf 0" 4 herab; und der wahrscheinliche Fehler einer
einzelnen Messung der 1%“ langen Grundlinie betrug oe der Länge.

: Der Herr General Ferrero erkennt keinen Vortheil für die Geodäsie in der Ueber-
Î treibung der Genauigkeit für die Basismessungen, da die bei den besten Triangulationen
unvermeidlichen Fehler den Einfluss der Fehler der Grundlinien auf das Endresultat bedeu-
tend übersteigen. Nach seiner Meinung wäre es daher vorzuziehen die Anzahl der Grundlinien
in einem grossen Dreiecksnetze zu vermehren, als die Basismessungen noch weiter ver-
vollkommenen zu wollen, deren jetzige Genauigkeit schon mehr als genügend ist; wichtig ist
es ferner alle diese Grundlinien vergleichbar zu machen, indem man die Gleichungen der
angewandten Stäbe und Einheiten so genau als möglich bestimmt. Vom praktischen Stand-
punkte aus endlich würde Herr Ferrero es vorziehen, eine grosse Anzahl Grundlinien von
senügender Genauigkeit, deren Messung nicht zu theuer zu stehen kommt, auszuführen,
als bedeutende Summen für eine einzige Grundlinie auszugeben, deren fast ideale Genauigkeit
doch illusorisch bleibt, weil sie bei der Uebertragung auf grosse Distanzen vermittelst der
Winkel verloren geht.

had ai Di eo lu dd

Herr Helmert stimmt dem Vorredner bei, dass die Genauigkeit der modernen, in
Europa gemessenen Grundlinien genügend ist und diejenige der Winkelmessungen verhält-
We nissmässie übertrifft. Im Interesse des geodiitischen Fortschrittes wäre es aber erwünscht,
diese beiden hauptsächlichen Elemente zu vervollkommnen.

in pope

Herr Oudemans ist der Ansicht, dass Herr Ferrero dem von Herrn Helmert soeben
beschriebenen neuen Apparaie nicht vollständig gerecht wird. Es ist bekannt, dass die Haupt-
schwierigkeit in der Kenntniss der Temperatur besteht, und selbst, wenn man annehmen
will, dass die Temperatur der Stäbe genau von den Thermometern angegeben wird, man
doch der Länge derselben nicht vollständig sicher ist. Der General Comstock hat bemerkt,
dass ein Stab aus Zink länger ist, wenn derselbe aus einem heissen Raume kommt, und
kürzer, wenn er vorher in einem kalten Raume sich befunden hat. Für Eisenstäbe ist dieser
Unterschied geringer, aber doch ebenfalls vorhanden.

Herr Woodward hat also einen wirklichen Fortschritt erziehlt, indem er die Stäbe

ABA as
Kippe tint ie

ak A AAMAS a dU. u
TEE TU FARM 9 CNT UE

à

re
me min

u

 
