 

a ee SER Ë
|

SI

992

 

nôtre, les stations de premier ordre sont formées pour le plus grand nombre par des clochers.
Comme centre, on prend presque loujours le sommet de la flèche, mais il est indispensable
d’avoir un repère dont la position par rapport au centre soit déterminée exactement. Sans
cette précaution on risque de voir, au bout de quelques années, le point en question perdu
pour toujours.

Dans notre pays, nous prenons la précaution de fixer au pied de chaque clocher un
boulon en bronze, avec un trou vertical, dont la position par rapport au sommet de la flèche
est déterminée par une triangulation auxiliaire et par lequel on peut toujours reconstruire
le centre exact de la station, quoiqu'il arrive à la flèche.

Pour vous convaincre de la nécessité de prendre une telle précaution, 1l suffit de
rapporter le fait suivant que nous avons rencontré dans la triangulation dont je viens de
parler. Le raccordement de la triangulation de la Belgique avec celle de l’Institut géodésique
repose surtout sur le quadrilatère formé par les quatre stations : Tongres, Peer, Ruremonde,
et Ubagsberg, dont les trois premières sont des clochers pour lesquels on n’a pas pris Îles
précautions dont j'ai parlé. Actuellement aucune de ces trois stations ne peut plus être re-
trouvée avec l’exactitude voulue. La flèche du clocher de Tongres est démolie; la foudre a
frappé cette année le clocher de Ruremonde et a incendié la flèche, qui est brülée
jusqu'à la maçonnerie. Il en résulte que les centres de ces deux stations ne peuvent plus être
reconstruits A un metre près. Le sommet de la flèche du clocher de Peer a subi aussi il y à
quelques années une transformation, parce qu’elle avait été endommagée par la foudre, de
sorte que sur la position exacte de ce point, il existe aussi une incertitude de quelques
décimètres.

Des quatre points que je viens de citer, qui forment le raccordement de deux trian-
eulations, et qui devaient servir aussi pour le raccordement avec la nôtre, il n’en existe donc
à présent qu’un seul, savoir Ubagsberg, grâce à ce que le centre de cette station avail été
fixé par une marque souterraine.

Afin de prévenir la perte des centres des stations, je crois qu'il serait utile que la
Conférence se prononçät dans ce sens que, pour les stations formées par des clochers, il est
nécessaire de fixer dorénavant la position du centre par des repères placés au pied des clo-
chers, et pour les clochers qui ne possèdent pas encore de ces repéres, de les en pourvoir
aussitôt que possible.

he M. SCHOLLS:

 

i
i
3
1
3
E

 
