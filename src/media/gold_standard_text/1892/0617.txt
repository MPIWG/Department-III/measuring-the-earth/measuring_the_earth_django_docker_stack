 

MR

jdn LA

ijl id

i
==
=
a=
oz
oR
=

in

 

Beilage B. XII.

SCHWEDEN

Bericht über die astronomisch-geodätischen Arbeiten in den Jahren 1890-92.

(Mit einer Uebersichtskarte 1).

Im Anschlusse an meinen 1889 der Conferenz zu Paris erstatteten Bericht, habe ich
für die vorgenannte Zeitperiode Folgendes mitzutheilen :

I. PUBLICATIONEN

1. « Om lodafirkelser i Sverige af P. S. Rosén ». Bihang till Svenska Vetenskaps
Akademiens Handlingar. Band 15. Afdeln. I. Nr 7, 1889.

2. Längenbestimmungen zwischen den Sternwarten in Stockholm, Kopenhagen und
Christiania, von D. G. Lindhagen. Kongle Svenska Vetenskaps Akademiens Handlingar. Band
24. Nr 4. 1890.

3. Astron.-Geod. Arbeiten der topogr. Abtheilung des schwedischen Generalstabes,
von P. G. Rosen. Band I. Heft 3, 1890.

4. Geodätische Azimutbestimmung auf der Sternwarte in Lund und trigonometrische
Verbindung der Sternwarte mit dem Hauptdreiecksnetz des k. Generalstabes von D. G. Lind-
hagen, 1891. Bihang ull K. Svenska Vet.-Akad. Handlingar. Band 17. Af. I, Nr 7.

II. TRIANGULATIONEN

Im Jahre 1890 wurden die restirenden Winkelbeobachtungen auf 7 Stationen der
nördlichsten Parallelkette Schwedens vollendet. Diese Kette erstreckt sich auf dem 65° Paral-
lele zwischen der Seite Gippmahkobben-Heligfjall in der Meridiankette durch Westerbottens
Lappmarker bis zu dem Médimarémétre bei dem Bottnischen Meerbusen in der Nähe von
der Stadt Skellefteä.

! Sıehe Tafel 14.
