ee

ee a

rey Se nen Se EE N

  

dan

  
 

ee eo ee

metischen Mittel der je 4 Mikrometerablesungen gebildet und diese Werthe alsdann wegen der
Neigung, der Krümmung des Parallels und des Fehlers der Schraube verbessert wurden.
Die Niveaucorreetion wurde auf Grund der Formeln:

| +oRoo8s [(a+b) — 40°] für Niveau I
| +o.o078 [(c+-d) —140?] > sn I
i
berechnet, nachdem die Niveautheile in Einheiten der Schraubenrevolution — nach der Methode

Br

wiederholter Einstellungen eines gut sichtbaren irdischen Objeetes mittelst des beweglichen Fadens
bei veränderten Ständen der Niveaus — zu bezw. o!or7o und o!o156 bestimmt worden waren. Da
beide Niveaus während der ganzen Dauer der Beobachtungsreihe in durchaus befriedigender
Weise functionirten, wurde als endgiiltige Correction das arithmetische Mittel der aus den
Ablesungen beider Niveaus hervorgegangenen Einzelwerthe angenommen.

Die Correetion wegen der Krümmung des Parallels wurde, ausgehend von den Aequatorial-
Fadendistanzen F® der Mitten zwischen je zwei Fäden des festen Fadennetzes, unter Anwendung

NEE TEE

nn

GER

der Formel:

 

= 2 Sin 1 Do pe i Kreis Ost
2 R 7 Le West
berechnet.

Die Correction wegen des Fehlers der Schraube ist auf Grund der aus der Schraubenunter-
suchung im Jahre 1889 hervorgegangenen Zahlwerthe!) bewirkt worden. Jene Untersuchung
hatte ergeben, dass die Schraube innerhalb der Grenzen von einigen Hundertelsecunden als frei
von periodischen Fehlern anzusehen sei und dass es somit genüge, bei der Reduction der Beob-
achtungen nur den fortschreitenden Fehlern der Schraube Rechnung zu tragen. Für diese
Letzteren waren die in der nachstehenden Tabelle enthaltenen Correctionen ermittelt worden,
von denen bei der Reduction der Beobachtungen Gebrauch gemacht wurde.

a,
i
4
\
N

 

 

 

 

oRo —-oRoo62 So - 989007 ıoRo oRoo00o 12 © +ofoo1z
2 +0.0061 2 +0.0007 2 0.0000 2 —-0.0013
4 40.0061 4 20.0007 4 —-0.0001 4 +0.0014
6 4-0.0060 6 —o.0006 6 0.0003 6 +0.0015
8 +0.0058 8 —-0.0006 8 --0.0007 8 +0.0017
no +-0.0055 6.0 +0.0006 Tr © —-0.0010 16.0 —+0.0019
2 +0.0052 2 0.0006 2 +0.0013 2 +0.0022
4 +0.0048 4 0.0006 4 —-0.0017 4 +0.0025
6 +0.0042 6 0.0007 6 +0.0020 6 +0.0029
8 + 0.0037 8 +0.0008 8 +0.0022 8 +0.0033
2,0 0.0032 70 —-0.0009 1260 —-0.0023 17.0 +0.0038
2 —-0.0028 2 —-0.0011 2 +0.0024 2 +0.0043
4 +0.0025 4 —-0.0013 4 0.0025 4 +0.0049
6 +0.0022 6 +0.0015 6 +o0.0024 6 +0.0055
8 —-0.0019 8 +0.0017 8 +0.0023 8 -0.0058
250 0.0017 8.0 +0.0020 13.0 —+0.0022 18.0 +0.0059
2 +0.0015 2 Lo Cor 2 00020 2 +0.0057
4 +0.0013 4 +0.0022 4 +o.o0018 4 —-0.0054
6 —-o.oo12 6 +0.0022 6 +0.0017 6 +0.0049
8 40.0011 8 —0.0021 8 0.0015 8 +0.0043
4.0 +0.0010 9.0 0.0019 14.0 40.0014 19.0 40.0038
2 —-0.0009 2 +o,0016 2 —o.0013 2 +0.0033
4 +0.0008 4 AEOMOONT 4 +0.0013 4 +0.0028
6 +0.0008 6 +0.0006 6 —-0.0013 6 --0.0024
8 0.0007 8 --0.0002 8 +0.0013 8 + 0.0020
5.0 0.0007 10.0 0.0000 15.0 0.0013 20.0 +0.0017

1) Vergl. die Veröffentlichung des Königl. Preussischen Geodätischen Institutes: Astronomisch-Geodaetische Arbeiten
I. Ordnung. Telegraphische Längenbestimmungen in den Jahren 1888 und 1889 etc., Berlin 1890, Seite 221—224.

 

 

 
