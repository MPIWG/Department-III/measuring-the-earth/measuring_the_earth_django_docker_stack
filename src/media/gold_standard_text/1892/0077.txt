 

Apps

RL Lun

DL ES

{
à
=
IE
4
se
ie
a
©

71

verlassen hatte, ging er nach Frankreich, wurde im Jahre 1822 in die Schule von Saint-Cyr
aufgenommen und verliess im Januar 1827 die Generalstabsschule. Nach seiner Ruckkehr
nach Brüssel, bot er seine Dienste Belgien an und veröffentlichte unter der Leitung von
Adolphe Quetelet seine ersten wissenschaftlichen Arbeiten. Später knüpfte er freundschaftliche
Beziehungen mit dem General Baeyer an, und folgte von da an dem wissenschaftlichen Einflusse
des hochverehrten Gründers unserer Vereinigung. Er war einerder Ersten, welcher in unserem
Lande das Beobachtungsverfahren und die Rechnungsmethoden der deutschen Geodäsie
würdigte, welche im französischen Sprachgebiete zu verbreiten der leider verstorbene Gene-
ral Liagre durch die Publication seines Buches « Ueber die Wahrscheinlichkeitsrechnung und
Theorie der Fehler » so mächtig beigetragen hat. Daher lässt denn auch das Werk Neren-
burgers im höchsten Grade diesen wissenschaftlichen Character erkennen, welcher der Geo-
däsie unseres Jahrhunderts eigenthümlich ist. -

« Ein Umstand verdient in der Laufbahn Nerenburgers als Director des Kriegs-De-
pöts, hinsichtlich der Wirkungen dieses Strebens nach Einigung in wissenschaftlichen Dingen,
von dem der Herr Finanz-Minister vor einigen Augenblicken sprach, hervorgehoben zu
werden. Ich spiele auf die Thatsache an, dass zu unseren Basismessungen bei Lommel und
Ostende im Jahre 1852 und 1853 der General ermächtigt wurde, den Apparat von Bessel
anzuwenden, welcher dem preussischen Generalstabe gehört.

« Aber die Wissenschaft ist nicht stationär, und jeder Tag führt eine Stufe weiler
auf dem Wege des Fortschritts, eine nothwendige — ich hätte beinahe gesagt, verhängniss-
volle Entwicklung —die man sich aber leicht erklärt, denn jede Generation zieht Vortheil von der
ürbschaft, welche ihr die vorangegangenen Generationen hinterlassen haben. Die Instrumente
vervollkommnen sich, die Methoden werden strenger, Glieder, welche früher vernachlässigt
wurden, werden in Rechnung gezogen, um genauere Resultate zu liefern, hieraus geht jene
wissenschaftliche Bewegung hervor, deren internationalen Organismus die Erdmessung bildet.

«In solchen Versammlungen wie die, welche heute eröffnet wurde, nehmen zu-
weilen solche Fortschritte ihren Ursprung, manchmal gewinnen sie dori an Umfang, ein
anderes Mal ist es möglich, in denselben neue wissenschaltliche Erwerbungen zu verzeichnen.
Möge unsere jetzige Vereinigung in Brüssel nicht weniger fruchtbar an Resultaten sein, als
die vorangegangenen allgemeinen Gonferenzen. » |

Die Sitzung wird während einer Viertelstunde vertagt.

Nach Wiedereröffnung ernennt der Präsident, indem er von dem Rechte Gebrauch
macht, welches sein Amt ihm verleiht, zu Vicepräsidenten der Versammlung die Herrn Ge-
neral Ferrero und Professor Ferster.

Der Herr Präsident macht hierauf verschiedene geschäftliche Mitiheilungen und giebt
einige Erklärungen, welche die Besuche und Ausflüge betreffen. Er bringt unter anderem
zur Kenntniss, dass Seine Majestät der König die Conferenz gnädigst am Freitag den 30. Sep-
termnber in seinem Schlosse zu Ostende empfangen wird; S. M. hat auch die Erlaubniss
ertheilt, sein Palais zu Laeken zu besuchen; dieser zweite Ausflug wird Donnerstag den
9. October, von 9 bis 4 Uhr, stattfinden.

ee

Sean

 
