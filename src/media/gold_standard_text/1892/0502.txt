Numeros

 

 

 
 

   
 

 

  

 

 
 
   

Name der Sta-

 

i

!

i
I
i
hi
i

}
hi
ft
‘a
|
|
vi
I
i
|
i
0
|
i

LAND tion.
PAYS Stations.
Neuseeland..| Auckland...

Australien ..
Indien......

 

Syduey....-
Singapur...

Boklo......

/ San Francisco

United States Washington, D.C.
\ Hoboken N.Y

Breite. Latitude.

—36 51,8) 474 46,8

—3351,7| 151 49,7
116.8 103 50,2

 

Länge gegen Green- ||
wich.
Longitude a partir
de Greenwich.

I © 1

 

 

58 af
41,5|- 74 2

   

Altitude.
Beobachtungs-

Meereshohe

En

B

|

80

43
AK

| 444

  
 

 

\

6

 

United States

»

Hawaiische Inseln

United States.

Hawaiische Inseln
»

|
> Afrika, Angola
Alankar.  .. |
St Helena... |
»
Ascension Island
»
Barbadoes..
Bermudas... |

United States

»

 

Brit, Columbia
ÿ | United States

 

 

Washington, D.C.

San Francisco,

Honolulu...

ahaina ....|
‘Caroline Island

Mount Hamilton
Lick Observatory
at es
Palkavao....

Koanda 4

Capetown...
Jamestown. .
Longwood. .
Georgetown.
Green Mountain
Bridgetown.
St. Georges..

Washington,D.C.

(San Francisco

Cal.
Fort Simpson
Junean. 12

21 18-157 52
20 52
— 10 0|-15044

37 20,4 -121 38,6

20 561-136 20
90 431-1864

Se 4340
556) 18 29
15 551-643 7
15 871-6 41 5

757- 14 22

a 503%
3a Gk 40

38 53,2|- 77 0,5
37 474-122 25,7

54 33,61-130 25,5
BRAT |=13204

 

53,3\- 71 1,1

47,4 1-12:

 

25,7

ve

5

-156 41

1 56,- 44 25

1282

117
3001

4.6
1A

533

d
686
18

18

 

 

9

Cr ©

 

   

2H

R

 

werth.

| Valeur observée
LOY
gl (4 À
B Bodenanziehung
Attract. du terrain
DichtigkeitdesBodens||

8 |
= |

|
|

 

 

     

Beobachter.

| Observateurs

 

Densité du sous-sol. |

 

  

Jahr der Beob. 1

Annéede l'observation

  

 

  
   

Bemerkungen

Remarques.

 

 

 

 

 

 

| || E. Smith and 4882|  Katersche unveränder-
| \ N.S. Pritchett.| | Iche Pendel Nes 4, 6 ||
| | | und 11.
| )) 1883)
| 5 » | Für N: 30-35 vergl.
| | die Publikation in den
| | Report of the Coast and
| | Geodetic Survey for18~4 |
| | Appendix 14. Siehe auch
| den Pendelbericht in den |f
| Verbandiungen von Niz- |
| za. — Im Anschluss an
| | diese Reihe vergi. die in
| | | den Phil. Transactions ||
| | | 1890. p. 537-558 ange- |f
| gebenen Messungen. |
)) ))
| |) E. Smith N.S. »
| I‘ Pritchett E. F.
| ) Dickins.
| } E. Smith and | 4884
|) Farguhar. |
RSR : 9
iG. Ss) Peirce: ‘
1833/7

| E.D. Preston.

| ))

| »
))
) E. D. Preston
\

Brown.
|

) E D. Preston
\ Keeler.
| E.D. Preston

))

— —

\\ F. Morse.
))

))

I BE DI Preston.
\ G R. Putnam. |
\J.G Mendenhall

+9 90

1

1883 7

©
{I

1883

1887

 

Peirce’ Reversionspendel

Für N: 37-44 vergl. |f
die Publikation in dem
Report of the Coast and |
Geodetic Survey for 1888, }
Appendix 14.

IR
Siehe auch den Pen- I
delbericht in den Ve.- IN
handlungen in Paris. IB

 

Für N° 45-52 vergl. ||
die Publikation in dem ||
Report of the Coast and
Geodetie Survey for 1891,
Appeı dix 19:

© Halbsekundenpendel,
Salz a. ||

Nicht pul licirt.

 

 

In

haha Aura dl nu
