ee
v

 

imma im! i in iyi] ll

lila ll

a

624
einiger auch südlich von Bredskär gelegenen Dreieckspunkte, wozu hoffentlich im nächsten
Sommer Gelegenheit sich darbieten wird.

Die Resultate der ım Berichte von 1889 erwähnten Längenbestimmungen zwischen
Stockholm, Hernösand und Haparanda sind jetzt fertig berechnet und in den Publicationen
des Gentralbureaus mitgetheilt. Die ausführliche Publication derselben steht bevor, da deren
Redaction theilweise fertig ist.

IV. PRECISIONSNIVELLEMENT.

Die Nivellements des südlichen Schwedens sind jetzt abgeschlossen und umfassen
eine Länge von 2950 Kilometer. Sie enthalten sowohl gewöhnliche Nivellements und Nivelle-
ments mit Zieltafel, als auch trigonometrische Höhenmessungen, miltelst Universalinstru-
ment und gegenseitiger Beobachtungen, über breitere Sunde oder Uebergänge. Da nämlich
einige von den Pegeln und alten Marken ziemlich weit von der Hauptnivellementslinie sich
befinden und durch Uebergänge von Sunden und Meeresbusen his zu 3" Breite mit
jenen verbunden werden mussten, wurden die genannten umständlicheren Messungsoperationen
nolhwendig. Die Länge dieser Nebennivellements beträgt allein im südlichen Schweden 670
Kilometer, welche also in der vorgenannten totalen Länge mit enthalten sind.

Durch diese Messungen sind jetzt im südlichen Schweden 5 selbstregistrirende Pegel,
13 Pegel mit Scalenablesung und 5 in Felsen früher eingehauene Marken mit einander ver-
bunden. Die vorläufigen Berechnungen haben bisher keine grösseren Höhenunterschiede der
Punkte in Ostsee und Kattegatt geliefert, welche nicht durch die zu befürchtenden Fehler
des Nivellements und die zufälligen Abweichungen der Wasserhöhen der Pegelstationen er-
klärt werden können.

Zur Ausführung des Nivellements im nördlichen Schweden sind, vom Jahre 1891
an, Mittel von der Staatsregierung bewilligt. Nach dem Plan dieser Arbeiten sollen zwei Ni-
vellementslinien hauptsächlich parallel mit der Küste bis zum Torneaelf sich erstrecken, und
zwar die eine längs den Eisenbahnen, die andere auf den Landstrassen. Dazu kommen so-
wohl Verbindungslinien zur Bildung von Polygonen, als auch zwei Linien bis zur Grenze
Norwegens für eventuelle Verbindung mit Nivellements und Mareographen in diesem Lande.
Ausser diesen Hauptnivellements sind auch hier Nebenlinien zu den zwei selbstregistrirenden
Pegeln bei Björn, Feuer und Ratau, nebst einigen bei der Küste in Felsen eingehauenen
älteren und neueren Marken ın Aussicht genommen. Die ganze Arbeit soll nach dem Pro-
gramm binnen 8 Jahren ausgeführt werden.

In den Sommern 1891, 92 wurde eine Doppelnivellementslinie in entgegengeselzler
Richtung von 1020 km., und eine Linie von circa 300 km. einfach gemessen.

 
