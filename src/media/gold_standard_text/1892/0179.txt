nenne

nm

à

did li te aa

iz
ü
&
1s
z
À
=
=
=
i
+
=

I

Seit genanntem Zeitpunkte sind 14 Latten (7 Lattenpaare) in Gebrauch gewesen,
N° 1 und 2, 3 und 4, 5 und 6, 9 und 10, 41 und 12, 13 und 14, 15 und 16. Die Latten
sind Kastenlatten von 3,02m Länge, 110" Breite, und 37mm Dicke. Sie bestehen aus zwei
Im starken Brettern von Erlenholz, die durch zwei eingeschobene Leisten aus Tannenholz,
von derselben Länge wie die Latten, von 15™™ Breite und 15™™ Dicke verbunden sind.
Sämmtliches Holz wird erst dann verwendet, wenn es völlig trocken geworden ist.

Während der Wintermonate werden die Latten, sobald sie sich nicht beim Mecha-
niker befinden, in einem mässig ventilirten, niemals geheizten Keller des Generalstabsgebäudes
aufbewahrt, dessen Temperatur eine sehr gleichmässige ist und niemals unter 10° C. sinkt.

Ks sei zunächst hervorgehoben, dass ohne Ausnahme sämmtliche Latten in jedem
Sommer während der Feldarbeiten eine Verlängerung erfahren haben, deren Maass aus fol-
gender Zusammenstellung hervorgeht :

        

   

 

 

| abl der | Pie grösste Zunahme des Lattenmeters während eines
| No der Gebrauchs- . Sommers in Mikron.
| Gebrauchs- |
lee he ahre i -
wee jahre durchschnittlich geringster Werth qresster Werth
|
1 | 1879-1880 9 290 220 (1886) 400 (1880)
2 | 1884-1890 9 280 230 (1886) 390 (1880)
ee. 10 290 180 (1885) 150 (1881)
ee 10 240 180 (1885) 340 (1881)
5 | 1879-1880 8 230 140 (1887) 450 (1880)
6 | 1884—1889 8 260 150 (1887) | 490 (4880)
OO ee 12 220 100 (1885) 370 (1884)
mat an 12 240 90 1885) | 430 (1884)
Me 3 290 260 (1880) | 330 (1882)
wen 3 270 210 (1880) | 310 (1882)
13 | 1879, 1880, 3 400 300 (1879) | 510 (1880)
ee 1890 3 360 260 (1890) | 460 (1880)
| 15 | 18841883 h 260 170 (1890) 310 (1883)
| 16 | 1890 h 270 170 (1890) 320 (4881)

 

 

Aus dieser Tafel ist ersichtlich, dass die beiden Latten eines und desselben Paares
eine sehr gleichmässige Verlängerung zeigen. Dies tritt noch mehr hervor, wenn die Ergeb-
nisse für jedes Jahr zusammengestellt werden. Nur bei dem Lattenpaar N° 34 zeigt sich für
jedes der 10 Gebrauchsjahre eine Differenz der Verlängerung in dem gleichen Sinne, nämlich
durchschnittlich um 0,05 %m, Hieraus möchte zu schliessen sein, dass die Individualität der

 
