Eigenbew.
in Mech

nes en —

Bradley Pulkowa Grösse AR,1892.0 Decl 18926 Quelle

”

10 «6-2 10* --10° 31 36.05 - 02038 Greenw 10h Tose
8 54 +32 o 14.47 +0.012 Greenw. 10Y. 1662

m 8

oo

Gruppe VIII Paarr À Leonis 1405 1885
22 Leonis min. 1418 1598

=

2 44 Leonis — 1624
33 Leonis min. 1461 1640

19 34 920 1972 Greenw. 10 Y. 1687
2543 +32 56 1.39 -+o0.o22 Greenw ıoN 17cn

3. 38 Leonismm. rar, 1056
34 Sextantis 1484 1667

32 58 -38 28 22.45 —o.oı8 Romberg 2321
37 3 #4 3 50.353 +0.033 Geeenw ron.

54 48 +39 47 31.31 —o.orı Rombers 226
Il I 24 +2 32 29.71 =©.0b0 Creenw oh

4 49 Ursae may. 1624 1713
p* Leonis 1530 27

Ce) Ce SES OO

5. 55 Ursae maj 1555 17435

13 15 +38 46 40.77 —0.068 Greenw ao 160
82 Leonis 1566 1754

1 20 6 + 3 53 46.01 —0.030o Auwers 1566

6 MNarnall 1875 —
58 Ursae maj. 1674 1703

© 22 2% 16208 Yarnall 4875
24 40 +43 45° 57.67 -0.070 Jahrbuch 77

a Fon aa an Chics)
© oO

7 9 Nonne 1601 1789 3 40 18 -+ 7 8 3.87 2.165 Greemy ie 135,
Pulkowa 1795 — 1795 3 Ag 5 35 51 sı ge Pulkowa 1795

8 o Leonis rons) 1602 .6 50 9 +16 14 51.75 +0.018 Greenw ıoW rag)
4 Comae Ber. 1630 1827 Oo 12 6 22 +26 28 18.84 0.037 Greeny to teas

Die Declinationen der Sterne sind insoweit, als sie dem Berliner Astronomischen Jahrbuche,
dem Brcxer’schen Cataloge'), dem Romeere’schen Cataloge®), der Auwers’schen Neubearbeitung
des Braprey’schen Cataloges*), dem Pulkowaer Cataloge*) und dem Greenwicher Jahrescataloge
für 1887°) entnommen worden sind, unverändert so angenommen, wie sie sich aus den genannten
Quellen ergeben haben. Dagegen sind die aus dem Greenwicher ro Year Cataloge‘) entnommenen
Deelinationen unter Anwendung der auf Seite (17) der Vorrede des Ronsere’schen Cataloges
enthaltenen Reductionen auf das Declinationssystem des Rougere’schen Cataloges bezogen worden,
welches nach Seite (14) der Vorrede des genannten Cataloges als identisch mit dem Declinations-
system des Fundamental-Öataloges anzusehen ist. Da nach dem Beobachtungsplane die Verän-
derungen der Polhöhe frei von den Unsicherheiten der angenommenen Declinationen der Sterne
erhalten werden, so bedurfte es in betreff der Ausgangspositionen der Sterne keiner weiter-
gehenderen Maassnahmen.

Eigenbewegungen sind bei allen denjenigen Sternen angesetzt und bei der Berechnung der
mittleren und scheinbaren Declinationen angewendet worden, bei welchen solche in dem neuen
Braprev’schen, dem Rongere’schen und dem Sarrorv’schen Cataloge angegeben sind. Wie aus
der obigen Tabelle hervorgeht, war dies bei 96 unter 126 Sternen der Fall, so dass also für
76°/, der Sterne Eigenbewegungen in Rechnung gezogen werden konnten.

 

1 TI TAN] LH HT TR

it

Wi

*) Beobachtungs-Ergebnisse der Königlichen Sternwarte zu Berlin. Heft 1. Resultate aus Beobachtungen von
521 Branuev’schen Sternen am grossen Berliner Meridiankreise von Dr. EK. Becxer, Berlin 1881.

2) Catalog von 5634 Sternen fiir die Epoche 1875.0 aus den Beobachtungen am Pulkowaer Meridiankreise wah-
rend der Jahre 1874—1880 von H. Romzerg, St. Petersburg 1891.

3) Neue Reduction der Bravixy’schen Beobachtungen aus den Jahren 1ı750—1762 von Arruur AUWERS, dritter
Band, St. Petersburg 1888.

4) Positions moyennes de 3542 Étoiles déterminées à l’aide du cercle méridien de Poulkowa dans les années
1840—1869 et réduites à l’époque 1855.0, St. Pétersbourg 1886.

5) Results of the Astronomical Observations made at the Royal Observatory Greenwich in the Year 1887,
London 1889.

6) Ten-Year Catalogue of 4059 stars deduced from Observations extending from 1877 to 1886 at the Royal Ob-
servatory Greenwich reduced to the epoch 1880.0, London 1889.

PET IPTC DOUTER 1 Pt WaT ET eer iT

 
