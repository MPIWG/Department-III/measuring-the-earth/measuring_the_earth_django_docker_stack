Te Vo eanpytrrrensee fy ++

I la ee HH AS

HEMT) Ui

ma An!

bi

Wi

Hii

on
3
=
=
=
2
=
=

991

 

mit dem Log. : 3,039 3522.5 siehe Rhein Dreiecknetz, S. 13,

hierzu 0,2898199.3 Verwandlungslog. der Toisen in Meter

und 38.4 Reductionsfactor auf internat. Meter. Siehe
Längengradmessung.

giebt 3,029 1760.2 oder den

Numerus 2133,9096 m.

Im Sommer 1892 ist unmittelbar neben der alten Basis die Neumessung einer
Grundlinie vorgenommen worden, und zwar ist die Linie 4 mal von der Landesaufnahme
und 2 mal von dem Geodätischen Institut gemessen worden. Das Resultat der Landesauf-
nahme liegt bereits vor, und zwar ist hiernach die Länge der alten Basis 2133,8813 m.; die
Reduction auf internationale Meter geschieht durch den Faktor 58 Einheiten der 7. Stelle des
Log. = 13,35 mm. pro km. Darnach wird die Lange :

2153,9098 m., mit dem Leg.

9,929 17605 m.

Das ist eine absolute Uebereinstimmung beider Messungen.

2. Grundlinie bei Göttingen ; gemessen 1880, Reduktionsfaktor + 55 Einh. der 7.
Stelle des Log. Siehe Längengradm. Länge 5192,926 ; Siehe « Königl. Preuss. Landes-Trian-
gulation » (Uebersicht), S. 211.

3. Grundlinie bei Meppen; gem. 1883. Reduktionsfaktor + 58 Einh. der 7. Stelle
des Log., Länge 7039,480. Landes-Triang. Uebers., S. 32.

4. Grundlinie bei Braak; gem. 1871. Der Reductionsfactor in Einh. der 7. Stelle des
Log. setzt sich zusammen aus:

1) + 97,7 Reduction des Bessel’schen Apparates auf internationale Meter.
2) —- 18,5 Correction wegen der Stahlkugeln, die bei der Aichung angewandt und
4 399 ursprünglich nicht genau in Rechnung gezogen sind. Siehe « Sächsische
Gradmessung I, Grossenhainer Grundlinie », Nachtrag, oder die « Publi-
kation des geodätischen Instituts, Lothabweichungen. » Heft I, S. 39.

Länge : 875,298 m. Landes-Triang. II, S. 59.

9. Grundlinie bei Berlin; gem. 1846 von General Baeyer. Reduktionsfactor + 70 Einh.
der 7. Stelle des Log. Siehe Längengradm. Länge 2336,392 ın.; Siehe Küstenvermessung
und ihre Verbindung mit der Berliner Grundlinie, S. 42.

6. Grundlinie bei Strehlen, gem. 1854 von General Baeyer; Reduktionsfaktor + 57

1 Der genaue Titel heisst ausführlich: « Die Königl. Preuss. Landes-Triangulation,
I. Verzeichniss der Druckwerke der trigon. Abtheil. der Landesaufn.
II. Dreiecksmessungen I. Ordnung 1876-1887.
III. Die Ergebnisse der Hauptdreieckmessungen 1876-1885. » Berlin, 1887.

Im folgenden ist das Heft als Landes-Triang. Uebersicht eitirt.

 
