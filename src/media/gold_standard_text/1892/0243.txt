 

  
 

 

 

 

 

|
| | Elimination des | | |
[Année et mois. équations | TITRE DE LA PUBLICATION | REMAROUES |
| personnelles. |
|
1876 juillet, | Détermination di- Pas encore publiée. Resultat définitif communiqué
septembre. recte. | par M. v. Orff. Il n’y avait pas
d’echange de signaux direct
entre Berlin et Munich, mais |
on s'est servi des échanges |
| Greenwich-Berlin, Geenwich- |[*
| | Munich, Vienne- Berlin et
| Vienne-Munich.
nem Pi mai, » | Plantamour et von Orff : Déter-
A| juin. mination télégraphique de la
| difference de longitude entre
| les Observatoires de Genève et
| de Bogenhausen. Geneve 1879.
I
Il

 

la 1853 novem- | Echange des ob-| Airy. Memoirs of the R. astr.

la | bre, dé- servateurs. | society. Vol. XXIV.
il cembre.
|
, || 1857 avril, | Détermination di- | Encke : Abhandlungen der Aka-
| mai, octo- recte. | demic. Berlin 18958 2. Owele-
|| bre. | let: Annales de l’Observatoire
de Bruxelles pour 1858.
|
|| 1864 août, Echange desobser- | Publications de la section topo- Détermination simultanée de la
se Septembre. vateurs v.Forsch graphique de l'état - major difference de longitude Bonn-
| et Tiele. M. Zy-| russe. Vol. XLVI. Descrip- Greenwich et Nieuport-Green-
| linsky restait a tion de la mesure du degré du wich. MM. v. Forsch et Tiele
| Erzenwich Dé: parallèle de 52. observaient ensemble d'abord
termination di- à Bonn, puis à Nieuport. M.
‘ect | Zylinsky restait 4 Greenwich.
no A Nieuport, le pilier se trouvait de
0°001 A l’est de la Tour des tem-
pliers. A Bonn, le pilier était de
0°062 & l’ouest du centre de

 

 

 

|

|

| l’observatoire.
|

 
