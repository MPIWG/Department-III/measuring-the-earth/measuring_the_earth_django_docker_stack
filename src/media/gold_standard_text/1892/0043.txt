 

st ses Lila in

cy, aba MA EU"

TI ne

 

a

Quant 4 Pélection d’un onzième membre, pour remplacer feu le général Ibanez, elle
est renvoyée à une séance ultérieure.

M. le Secrétuire perpétuel donne ensuite lecture d'un projet de résolution rédigé par
les membres présents de la Commission spéciale des latitudes nommés à Florence, au sujet
de la poursuite des études concernant la variabilité des latitudes. En voici le texte:

Les observations organisées ou provoquées pas la Commission Permanente pendant
les trois dernières années à Berlin, Potsdam, Prague, Strasbourg et Honolulu, de même que les
observations nouvellement exécutées en corrélation avec celles de Berlin et Honolulu à
Washington, enfin les observations de Poulkowa, faites d’aprés des méthodes essentiellement
différentes de celles des observations précitées, ont constaté, avec un tres haut degré de pro-
babilité, des déplacements sensibles et plus ou moins périodiques de l'axe de rotation de la
Terre dans le globe terrestre. En effet, les relations simples trouvées entre la marche des va-
rialions simultanées des latitudes dans les différents lieux d'observation et les longitudes géo-
oraphiques de ces lieux démontrent que certaines sources d’erreurs fortuites et systématiques
de ces déterminations n’ont pu exercer qu’une influence secondaire sur ces series d’obser-
vations, et que la cause premiere et commune des dites variations réside réellement dans un

“

faible déplacement de l’axe terrestre.
Dans cet état de choses, la Conférence autorise la Commission permanente à conti-

nuer ses efforts pour élucider aussi complètement que possible cette importante question et
pour assurer aux travaux astronomiques et géodésiques de l’Association la connaissance
exacte el prompte des corrections qui désormais seront à appliquer aux observations de
latitude, de longitude et d’azimut, afin de les réduire & une méme position initiale ou
moyenne de l’axe terrestre et de rendre ainsi strictement comparables entre elles les déter-
minations de ce genre faites à différentes époques.

Comme actuellement il n’est pas encore possible de créer une organisation spéciale
pour ces nouveaux travaux d'une portée à la fois scientifique et pratique, il suffira, pour le
moment, que le Bureau central de l'Association, qui d’ailleurs aura entre ses mains les ré-
sultats des observations régulières des latitudes déjà commencées à Observatoire de l'Institut
géodésique de Potsdam, continue à servir dintermédiaire entre les différents Observatoires
disposés à coopérer à cette œuvre (comme par exemple Poulkowa, Strasbourg, Washington,
ete.), et qu’il s’aide à concentrer et 4 publier les résultats de ces observations. Dans ce but,
on mettra à sa disposition jusqu’à 3000 M. (3790 fr.) pour l'exercice prochain.

Cependant, la Commission permanente sera appelée à envisager aussi Pavenir ulte-
rieur de ce genre de recherches et de travaux, afin de trouver les méthodes les plus exactes
ainsi que la distribution et l’organisation de travail les plus rationnelles, non seulement
pour l'étude complète de ces grands phénomènes, y compris les variations séculaires,
mais aussi pour l'application prompte et sûre des résultats de cette étude aux différents
travaux scientifiques et pratiques intéressés et notamment à la géodésie. Dans ce but, il sera
indiqué qu’elle s’entende avec les autres institutions el organisations scientifiques analogues,

 

 
