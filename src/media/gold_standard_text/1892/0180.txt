 

Hadiar ds Mein hs À

182

um, 1188411.) 88

Latte bei der Verlängerung nur eine geringe Rolle spielt, dass vielmehr die Verwendung
unter ganz gleichen äusseren Verhältnissen entscheidend gewesen ist. Dies folgt auch darans,
dass in der Mehrzahl der Fälle die in den Spalten 5 und 6 obiger Zusammenstellung gegebenen
extremen Werthe für beide Latten eines Paares auf einen und denselben Tag fallen.

Ob die Latten eines Paares aus einem Stück angefertigt sind, in welchem Falle auf
eine gleichartige Natur des Holzes geschlossen werden könnte, hat sich nicht mehr feststellen
lassen; doch ist es nicht wahrscheinlich.

Die Verlängerung der Latien während der sommerlichen Feldarbeit dürfte vornelhm-
lich ihre Ursache haben in dem Einflusse
1. der Wärme, |

2. der Feuchtigkeit.

Die Wärme wirkt schnell; sie theilt sich binnen einiger Stunden vollkommen dem
Holze mit. :

Der Rinfluss der Feuchtigkeit macht sich erst nach einer Reihe von Tagen vollständig -
geltend.

Wärme und Feuchtigkeit wirken jedoch während der Feldarbeiten meistens im
entgegengeselzten Sinne, insofern als während des Sommers in heissen Wetterperioden die
Feuchtigkeit der Luft sich vermindert, und umgekehrt.

Die lineare Ausdehnung, welche Tannenholz innerhalb 0° und 100° C. durch Wärme
erleidet, beträgt nach Gauss’ 5 stelliger Logarithmentafel 0,00000352 seiner Länge für jeden
Grad; die Ausdehnung von Erlenholz ist um ein Geringeres grösser. Wir nehmen daher als
Ausdehnungs-Coefficienten fir die Latte 0,000004 an!. Da der Unterschied zwischen der

Lab 1 lu

un tu du |

 

i höchsten und tiefsten Temperatur, bei welcher die Latten im Sommer untersucht werden,
etwa 15° beträgt, so würde allein durch die Wärme eine Zunahme des Lattenmeters um
| höchstens 0,06wm, also einen kleinen Theil der durchschnittlichen Verlängerung eintreten
| konnen.

Es ist somit anzunehmen, dass die Verlängerung der Latten hauptsächlich durch die
Feuchtigkeit der Luft bewirkt wird. Diese Annahme wird dadurch bestätigt, dass bei feuchten,

a uhhh da a hs a 4 à

reonerischem Wetter eine Verlängerung der Latten sehr merklich hervortritt, dass dagegen

| die Latten sich verkürzen, wenn sich wieder anhaltend trockenes Wetter einstellt. Hierbei

tritt die Erscheinung hervor, dass einerseits die Latten, wenn sie einen gewissen Feuchtig-

7 keitsgrad erreicht haben, sich nicht weiter verlängern, dass andererseits auch bei trockenem

| Wetter die Latten nie ganz auf die Winter-Länge zurückgehen?. Dies tritt erst ein, wenn

; die Latten nach Schluss der Feldarbeiten die freie Luft mit der Kellerluft vertauschen. Ein

N kurzer vereinzelt auftretender, wenn auch heftiger Regen bleibt, selbst wenn er die Latten un-

| mittelbar trifft, ohne besonders erkennbare Wirkung; dagegen hat es den Anschein, als ob

Latten, deren Bemalung nach längerer Benützung schadhaft geworden ist, die also der

| Feuchtigkeit leichter zugänglich sind, schneller den Einflüssen der Luft folgen.

| ! Siehe Oesterreich-Ungarn und Frankreich.

h 2 Siehe Bayern und Frankreich. :
| |
| |

 
