 

i
#
|
i

646

proximité du visage et le contact de la main de Vobservateur dilatent nécessairement d’une
quantité sensible et variable avec la saison, car la température du corps humain est constante
et celle de l'instrument varie dans nos climats de près de 30°. Voilà donc une source d'erreur
périodique presque irréductible à moins de précautions toutes spéciales.

Enfin, pour terminer, nous examinerons l'influence de la température de Pair au
voisinage des lunettes tant à l’intérieur qu’à l’extérieur.

Tant qu’on s’est contenté d’une approximation voisine de la demi-seconde d’arc dans
la mesure des déclinaisons d’étoiles, la loi de la répartition de la température de Pair au
voisinage de l'objectif de la lunette à pu être négligée : la réfraction de l'air se calcule en
effet en partant de l'hypothèse des couches homogènes et horizontales, représentant à la fois
les surfaces isohares et isothermes : dans ces conditions la connaissance de la température
à l’entrée du faisceau lumineux dans la lunette suffit pour le calcul de la correction de réfrac-
tion. Mais lorsqu'on veut atteindre les petites fractions de seconde qui figurent dans la dis-
cussion de la variabilité des latitudes, le problème de la réfraction se complique singu-
lierement : toutes les inégalités calorifiques qui modifient l’homogénéité des couches et
Vhorizontalité des surfaces isotbermes interviennent pour altérer la correction moyenne de
réfraction. Le calcul dans un cas particulier très simple donne immédiatement une idée de
l’ordre de grandeur de l’erreur à cramdre.

Imaginons que sur le trajet des rayons lumineux, soit à l’intérieur de la lunette, soit
à l'extérieur, la temperature d’une couche d’air diffère d’un nombre de degrés égal à 6 de
celle des couches voisines : admettons pour simplifier que la surface de séparation soit formée
par deux plans formant un angle A.

On trouve aisément que la déviation A produite par ce prisme d'air est représentée
par

Ne oN Ge a 1) a Ô

— 1,000294 étant l'indice moyen et « — = le coefficient de dilatation de l'air.

97

n
13
La substitution de ces valeurs numériques donne :

©

m 9000001077 0 — 092 À 0

Cette expression montre que si le produit de langle A du prisme d’air par la diffe-
rence de température 9 est égal a l’unité, la déviation atteint près d’un quart de seconde d’urc
c’est-à-dire la demi-amplitude des variations observées pour les latitudes. Or, le produit
Ad est égal à l’unité dans des conditions très réalisables.

Pour 1° centigrade l’angle du prisme est de 60°,

pour 2° l'angle À — 30°,

pour 9 langle A = 12°.

Mais des differences de 3,4 et 5 degrés dans les couches d’air d’une salle méridienne
ne sont pas rares; il suffirait donc que l’obliquité des couches isothermes fût appréciable
pour produire une déviation sensible. Et comme ce genre de différence de température varie

 

 

as dau ali ani lh ae
