 

194 i
Zn i
In der Zeitschrift für Instrumentenkunde, September 1881, sind solche beinahe tag-
lichen Bestimmungen als Mittelwerthe für ein Lattenpaar veröffentlicht. Macht man aus diesen
Bestimmungen halbmonatliche Mittel, so ergeben sich nachfolgende, äusserst geringe, meist
14 tagige Aenderungen des Lattenmeters :
former 2 aie Jmlibis zur id: Wallile Angust 1879 . . . . . . . — 8
~ i > Aue >» 2.» » » a te 9)
» 7 » D. Sept. ee A O0
edlen len 00. . .. 2... . ewe we 10
Done alle Jum bis zur 2. Hälfte Junı 1880. -. . -....2.22..4 16
D 2 > » » an D 2 ns. a: … 2 LE 6
ve.» Juli >» D ee Do
D D D » » Ol L :; LL 19
|». Aug. >» Very.) Deren à di
D À) D De oe |
PORTUGAL:
i Die aus den Vergleichen in Bern (1881 und 1887), sowie in Lissabon (1891 und 1892)
4 abgeleiteten Veränderungen der Lattenlängen veranschaulicht nachstehende Tabelle :
i
1 IW I OF iG
a von 1081 von 1887 von 1891
| batten “his 1891. bis 1891 bis 1899
| ee es |) + © |
i 2 == OÙ — + 100
| D ern — _
4 22 105 — —
( = + 97 =
1 2 _ + 188 + 98
# Bezüglich der Veränderlichkeit im Laufe der Sommerarbeiten wurden die Latten
| nicht untersucht.
| RUSSLAND :
| Vom Jahre 1885 sind immer vor und nach der Feldarbeit, d. i. im Frühling und im
| Herbste, im Bureau Lattenvergleiche gemacht worden.

Nachfolgende Tabelle giebt die Veränderungen der Lattenmeter vom Beginn bis zum

Schluss der Feldarbeit, wie sie aus den angefiihrten Vergleichen hervoregehen :
I © a

{
€

SS PARENT SIL AU RE TAS

 
