  
   
   

INDICATION

VII. — Grande Bretagne.

 

DIRECTEURS ©

 

INSTRUMENTS

 

 

   

  

     

oe Ne 'UDE
: DES POINTS LATITUDE | LONGITUDE EPOQUE ET OBSERVATEURS -
DAL 160 | Lundy Fsland...... 5121022, 90507297 1845 Caporal Stewart.
[OL 161 |:Lyun Old. Tr....... 245 2 115328 1843 Serg, Bay.
SOI "162 | Lyons Hill........ DS 1725 MERS T4 1829 Génér. Portlock.
OI 163 | Maker Ch, Tr. ....| 50 20 50 | 13 28 44 1846 ~Serg. Donelan.
LO) 164 | Malvern. 0. 52 ol lol Sh 1803-44 Woolcot, caporals Cosgro-
ve, Stewart.
Ol 165 :Mamsull 3.23. 57.16 48.) 12.32 36 .1848 Serg. Winzer.
OI 166 | Mendip: <3 2.5. 5118 6 | 15 710 | 1798-1844  Göner. Madee, we
: capor. Stewart,
"Ol 167 | Merrick... 55.821 13 11 45 1841-52 Capit. Hornby, serg. jon
kins.
ol 168 | Merrington Ch....| 54 40 39 | 16 4 10 1854 Cahill.
al 169 | Micktield Ch. Tr...| 52 12 44 | 18 47 14 1845 Serg. Steel.
mi GE 10 | Mk mil... BL 22.34 | 15 AS AL 1850 Serg. Donelan.
TI + 171 | Base de Misterton | 53 31 46 | 16 45 30 1801 Woolcot.
oo: Carr (Terme N.).
TE 2 Base de Misterton | 53 27 29 | 16 4423} 1801 id.
¥ Carr (Terme S.).
Vl ge 173 | Moelfre Issa ...... | 53 14 4081-14 328 1806 Génér. Colby, Woolcot.
"I ge 174 | Monach...-....... 58.21.22, 11 2 5 1839 Colonel Robinson, capit.
1 Hornby.
VI 1 175 | Mordington....... 55 48 28 , 15 35 38 1846 Serg. Winzer.
tl 176 | Mormouth ........ 57 36 9 | 15 37 53 | 1814-47 Gardner, serg. Donelan.
rl = 177 | Mount Battock....\56 56 57 | 14 5521 1814-47 Génér. Colby, Gardner,
serg. Donelan.
FI 178 | Mt. Leinster...... 5231. 110 2
VI 791 Mb Sandy... ..... 55 1055: 103 53 |, 1829: Col. Dawson, capit. Hen-
derson, lieut. Murphy.
3 [ Mowcopt.......... 53. 6.53 0 24 iol Serg. Jenkins.
sl Naseby Ch. Tr.....| 52 23 48 | 16 40 30 || 1800-42-43 Woolcot, serg. Steel,
 [ Naughton Ch. Tr... 52 6 6 | 18 36 56 1845 id.
I Nephin‘.... . ..... 54 048, 810 5 1828 Génér. Portlock.
I Nine Hi... 60 47 33 | 16.52 48 1847 . Serg. Jenkins.
I Nodes Beacon..... 50 40:20 16 22 1845 Sergs. Winzer, Steel.
I North Rona....... 59. 7 16: LE 50 58 1850 Serg. Steel
I Nort Ronaldsay ...| 59 23 3 | 15 17 36
= Phare.
[ = 187 | Norwich Spire 52 37 54 | 18 57 55 1844 id.”
E 22 ..:88.)Norwood......... 51-24 51 1 J8 235 1844 id: >
[ _  ]89 | Old Lodge... 51.8 -4 410-0229 1849 Sergs. Jenkins, Donelan.
[ 190 | Qld Sarum Castle .| 51 5 35 | 15 51 32 1849 Serg. Donelan.
[ 1902| Old sarum Gun...| 51 5 44 | 15 51 57 | 1793-1849 u Mudge, serg. Do-
nelan.
Lo 191 | Orme’s Head......| 53 19 59 | 13 48 38
[ € 192 | Orford Castle ..... 52.539 | 19 11.21 1843 id.
[ 193 | Otley Ch. Tr...... 52 855 1090 2 1844 Sergs. Steel, Beaton.
[ : 1524| Over HIN... -.... 5/7 1b. 19 4 10.38 25 1817 Gardner.
[ Hs 194 | Paddlesworth ..... 51 6 49 | 18 48 8 1844 Serg. Donelan.
195 | Paracombe........ 5) 10:32 | 13 48 Eh 1804-45 Woolcot, capor. Stewart.
196 | Pendle Hill....... 53-52 6 | 19.21.00 1841 Lieut. Da Costa.
| 197 | Penninis Windmill. | 49 54 29 | II 21 82 1850 Caporal Wotherspoon.
298: | Pertinny 2:15. | 50 6 24 | 12 1 7| 1796-1845 | Génér. Mudge, Wooleot,
sere. Donelan.
115b Peterhead Old
W.m. 2... 51-30 44-| 15:52: 15° 1850 Serg. Steel.
199 | Pillesdon ......... 50 48 24 | 14 49 50 | 1795-1845 sas Mudge, serg. Do
nelan.
= -200 | Plynlimmon....... ‚5228 1 | 12.0202 1805 Générs. Colby, Mudge.
a 201 | Precelly ....:..... 51.56 46 | 12.53 221) 1803-45 Woolcot, lieut. Luyken.
®  _115c) Reform Momment..| 57 29 31 | 15 51 56 |

 

   

MINT

Li

ik

\

=
=
=
=
=
=

 

 

 

 

 

 

 

 

Ramsden de 3 p.
Troughton de 2 Pp.
Ramsden de 3 p.
ide
id

id.
i.
id.
Theod. de 12 p.
Ramsden de 18 p.
Ramsden de 3 p.
vd.
ide

id.
id.

id,

id.

id.
Troughton de 2».

Ramsden de 3 p

Ramsden de 3 p. et |

18. pP.
id.

Ramsden de 3 Dp.

Theod. de 7 p.
Theod. de 18 p.
Airy’sZenith Sector. |

Théod. de 18 p.

id.

 Théod. de 3 p. :

id.
id.

Ramsden d3p. |

Ramsden de 18 p.

Ramsden de 3 D-
de ke

: UO aN foes

Troughton de 2 p.

, Ramsden de 18 p

Ramsden de oie |
Théod. de 7p. |
Ramsden de 3 p.

id.
A

 
   
   
 
 
  
 
 
 
 
  
 
  
  
  
  
 
 
 
  
  
  
  
  
   
 
  
 
  
  
 
  
  
  
  
  
 
  
 
  
  
  
 
 
  
  
  
 
  
   
  
    
 
  
 
   

 

 
