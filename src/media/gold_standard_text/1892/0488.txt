 

490

Zur Zeit wird man erst noch die Production der nächsten Jahre, namentlich aber
auch die Mittheilung der Ergebnisse für die zahlreichen unpublieirten Messungen abzuwarten
haben.

Den Hauptantheil an der gegenwärlig so reichen Ausbeute verdankt man der Ein-
führung der invariablen Halbsekundenpendel durch Herrn Oberstheutenant von Sterneck.
Diese Pendel werden jetzt in verschiedenen Staaten Europas sowie in den Vereinigten Staaten
von Nord-Amerika benutzt. Herr Oberstlieutenant von Sterneck selbst hatte bis Ende 1891 132
Stationen mit der Referenzstation Wien in Verbindung gebracht, wovon die Mehrzahl theils
auf die Tyroler Alpen und die Anschlusslinien nach München und Mantua, theils auf Böhmen
kommt, der Rest aber verschiedene Orte der oesterreichisch-ungarischen Monarchie betrifft.
Im Jahre 1899 sollten hierzu noch über 100 Stationen treten. Unter diesen sind Berlin (Bessels
Platz) Potsdam (Geodätisches Institut) und Hamburg (Seewarte) besonders erwähnenswerth.

Reihe I der nachstehenden Tabellen giebt die Messungen des lierrn Oberstlieutenant
von Sterneck bis zum Ende des Jahres 1881 nach einer gefälligen Originalmittheilung. Die
angegebenen Längen des mathematischen Sekundenpendels sind nur als relative aufzufassen,
obwohl sie sich in guter Uebereinstimmung an die beiden absoluten Bestimmungen für Wien
(Türkenschanze), durch Oppolzer, und München (Sternwarte), durch von Orff anschliessen.
Denn die Anschlüsse an andere absolute Bestimmungen, welche Herr von Sterneck noch aus-
führte (vergl. seinen eigenen Bericht in diesen Verhandlungen, geben erhebliche Unter-
schiede). Es erscheint daher nach wie vor gerathen, für geodälische Zwecke thunlichst die
Pendelmessungen nur als relative zu verwerthen. — Die Tabelle enthält alle diese Anschluss-
stationen noch nicht; dieselben können später im Zusammenhange mit den Messungen von
1892 aufgeführt werden.

Reihe II giebt die von Herrn Professor Rosen mit Sterneck’schen Pendeln beobach-
teten und projektirten schwedischen Stationen, sowie eine ältere absolute Bestimmung für
Stockholm, nach gefälliger Mittheilung des Herrn Rosen. Im October 1892 führte Herr Rosen
auch mit einem Sterneck’schen Pendel Anschlussheobachtungen in Potsdam aus.

Reihe III giebt die in der Schweiz mit dem Repsold’schen Reversions-Sekunden-
pendel absolvirten Stationen nach gefälliger Mittheilung von Herm Professor Hirsch. Im
Jahre 1892 hat man auch hier begonnen, Sterneck’sche Pendel zu verwenden.

Reihe IV giebt nach gefälliger Mittheilung des Herrn General Derrecagaix eine Ueber-
sicht der Pendelstationen, welche der Service géographique de l'Armée frangaise seit 1884
absolvirt hat.

Wie aus den Verhandlungen der Permanenten Commission der Internationalen
Erdmessung zu Nizza und den späteren Verhandlungen der Erdmessung bekannt ist, bedient
sich der Service géographique sowohl für absolute wie relative Bestimmungen besonderer
Reversionspendel nach der Construktion des Herrn Kommandanten Deflorges. Bei den ab-
soluten Bestimmungen werden Pendel verschiedener Länge, aber gleichen Gewichts zur Eli-
mination der systematischen Fehler verwandt. Die erste veröffentlichte Bestimmung dieser Art
bezieht sich auf das Maass- und Gewichts-Bureau in Breteuil. Die relativen Bestimmungen
dienen zum Theil dem Zwecke, einen Beitrag zu der thatsächlich stattfindenden Abnahme der

ah Mh he ars

 

 
