SS eS AE Se DR ANS TS

aes

|
fF
N
I
;

 

50

des deux microscopes obtenues avec une seule el meme position de la lunette. Cette erreur
est assez petite si la partie supérieure de l'instrument est bien fixée à l'axe vertical; mais
aussitôt qu'il existe un petit jeu entre cet axe et le cône creux qui porte la lunette, l'erreur
devient beaucoup plus grande ;

50 Les erreurs moyennes des divisions dans les limites d’un degré, par la mesure de
douze divisions de cinq minutes; les ingénieurs du Service géographique de Java exécutaient
en général cette mesure pour 7 degrés, une fois pour 20 degrés.

M. le colonel Hennequin occupe le fauteuil de la présidence et prie M. le Secrélaire
de donner lecture des rapports sur les travaux exécutés en Portugal et en Russie, rapports
qui lui ont été envoyés par M. d’Avila et M. Stebnitzki. (Noir Annexes B. IX et B. XI.)

M. le Président donne la parole à M. Rosén pour lire son rapport sur les travaux géo-
désiques et astronomiques exécutés en Suède pendant les années 1890 à 1892. (Voir Annexe

EX)

A l’occasion de ce rapport, M. Helmert est heureux de pouvoir constater le grand
progrès réalisé pendant les dernières années dans les travaux de géodésie en Suède, dont M.
le professeur Rosén vient de fournir la preuve. Mais il se permet d’attirer l'attention sur une
lacune regrettable qui existe encore dans le réseau des triangles suédois, en ce sens que la
Suède n’est reliée directement a la grande triangulation européenne que par la Russie. Il
serait désirable que la Suède fùt également raccordée au réseau européen par le Danemark.

Pour appuyer ce qu'il vient de dire, M. Helmert demande la permission d'exposer
brièvement l’état actuel des quatre principaux arcs de méridien qui traversent l'Europe, et
dont les deux moyens, sans la jonction entre le Danemark et la Suéde, seraient formés de
deux parties disjointes.

M. Helmert donne le tableau schématique suivant des ces quatre arcs méridiens, dans
lequel les longitudes sont comptées, comme dans la carte du général Ferrero, à partir de Pile
de Mer:

UIT Ee ee

ut ed, 1 1884011188

Lab nude

ju eu dun |

Jy phoma bas jn abl Lad ed pat 4

 
