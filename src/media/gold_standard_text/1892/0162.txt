 

 

ee

re

on

RS

>

ca

 

Pee ee er ea er oo sara eee

EI

164

approchée de 2"198 ä la temperature de 25°C. Il n’a pas été comparé au mètre interna-
tional.

Le Portugal se propose de mesurer une nouvelle base avec un appareil construit par
Repsold sur le modèle de l’appareil néerlandais.

Prusse. -— L'étalon est la Toise de Bessel, qui a été récemment élalonnée à Breteuil
et dont la longueur primitive, déduite de sa comparaison ancienne avec la toise du Pérou, à
subi une correction de + 262.

Cette correction se reporte sur tous les étalons qui dérivent de la toise de Bessel.

La Prusse possède, en outre, un appareil bimétallique construit par Brunner, qui à
été étalonné également à Breteuil.

Toutes les bases prussiennes peuvent done étre rapportées au metre international.

Suède el Norvège. — Les bases sont rapportées à une règle prototype de deux toises

environ de longueur, qui a été comparée en 1862 à l’étalon normal de Pulkowa. Cette com-

paraison n’a pas élé refaite dans la suite, et on n’a pas formé le projet d’en entreprendre une
nouvelle avec le mélre international.

Suisse. — Les trois bases suisses ont été mesurées avec la règle monométallique
d'Espagne et sont exprimées en fonction du mêtre international, au moyen de l'équation de
cette règle déterminée à Breteuil.

Russie. — La Russie a fait usage de plusieurs élalons pour la mesure de ses bases;
elle possède : 1° étalon normal de Pulkowa, N°, qui vaut deux loises; 2" la sagéne n° 10,
qui est un peu plus longue qu’une toise; 3° Pétalon R, qui a été comparé à N_; 4° l'étalon
P, qui a été comparé également à N_; 5° l’étalon N et enfin, 6° la double sagène de la trian-
gulation du Caucase. ©

Aucun de ces élalons n’a été comparé au métre prototype. Les discordances signalées
sur les bases russes par rapport aux bases de l'Europe centrale semblent indiquer un motif
impérieux de procéder à celle comparaison.

Wurtemberg. — L'ancienne base de Solitude-Ladwigsburg est abandonnée. La nou-
velle triangulation wurtembergeoise, faite en vue de la jonction du Abin avec la triangulation
bavaroise, ne s’appuie pas encore sur une mesure indépendante.

En résumé, lorsque les comparaisons entreprises à Breteuil sur les règles de Borda,
la règle bimétallique d’Espagne el la toise de Belgique, seront terminées, il ne restera à exé-
euter que celles des étalons de l'Autriche et de la Russie, et il est permis d'espérer que l’on
pourra, dans un délai peu éloigné, rapporter toutes les bases des Etats associés au mètre
international, et assurer ainsi à la triangulation générale de l'Europe l'unité et l'harmonie
qui lui manquent encore.

III. — Quelques rectifications et additions au tableau général des bases publié
en 1889 nous ont été communiquées par nos collègues. Il en sera tenu compte lorsqu'il y
aura lieu de rééditer ce tableau.

jaheseubnis

ae a de da a à.

 

 
