Rr cS

A a ner An Wey

MORIN

ulm An!

Vieni) 1)

id

i

A
~
3x
as
oa
os
=

605

Le Secrétaire a recu de M. le prof. Oudemans, le 17 avril 1893, juste à temps pour
l’imprimer dans ce Volume, la rectification suivante de quelques erreurs qui s’étaient glissées
dans sa note «Sur la triangulation de Java », qu'il avait communiquée à la Conférence de
TS.

Nous nous empressons, suivant le désir de M. Oudemans, de l’insérer à cette place.

à
ı H
BERICHTIGUNG

zu der Notiz über die Triangulation von Java, in den Verhandlungen der vom 8. bis 17.
October 1891 zu Florenz abgehaltenen Conferenz der Permanenten Commission (Siehe
Verhandlungen, S. 195-208). |

Am Schluss obengenannter Notiz ist eine Formel mitgetheilt, mittels welcher aus
einer beobachteten Kimmtiefe die Höhe des Beobachtungsortes über See abgeleitet werden
kann. Leider ist diese Formel durch einen Schreibfehler entstellt, welcher sich auch im
Texte wiederholt findet (R? statt R) während dieser noch einen zweiten enthält (1— = k)
statt (1 —%k); überdies ist aber der Factor von tg‘d nicht genau genug entwickelt, factisch
mehr als anderthalb Mal zu gross.

Auf bekannte Weise findet man den ganz genauen Ausdruck

RG 9 4 BEN 1 2—k /
H—2R sin d. sin ——— d. sec. dd... .».... (1)

 

wobeï die constante Correction, von welcher im Texte die Rede war, ausser Acht gelassen
ist; entwickelt man diesen Ausdruck nach den Potenzen von ég d, so findet man :

RA ee .
a 2 D À à à UC
TE ren 2)

2

Bei der Discussion der beobachteten Kimmitiefen, in Verbindung mit den bekannten

Höhen der Beobachtungsörter, konnte das letzte Glied dieser Gleichung vernachlässigt werden.

Es wurde für den Log. des Factors von ig? d gefunden 6,56546, und für das constante Glied

— 2475. Nimmt man für k den mittleren Krümmungshalbmesser der Erde (y R N) für 7°
nach Bessel, nämlich 6,80324 !, so findet man

2 —2k=1,1289, ask 0133

Es wird dann aber der Log. des Factors von tg* d= 5, 89716, (d. h. um 0,19793

kleiner als log. wie in der Notiz angegeben ist), und für Java würde also die voll-

R
6’
ständige Formel heissen :

1 Diese Zahl soll eigentlich 6,8032323 sein.

 
