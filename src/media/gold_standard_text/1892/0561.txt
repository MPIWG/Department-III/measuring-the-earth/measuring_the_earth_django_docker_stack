El Bl ae ern ae

Ic TTS sen

Anal Gti

Wi i fn i

Veau

à

À k Py SLA) VBR TY VAG a
PO OP TPH OEE Ep 1)

Beilage B. [¢. |

Bericht des Oberstlieutenant R. von Sterneck über die ausgeführten
Schwerebestimmungen 2,

Die in mehrfacher Hinsicht interressanten Resultate der im vorjährigen Herbste aus-
geführten relativen Schwerebestimmungen auf der Linie München und Borgoforte in Italien,
quer über die Alpen, sind im XII. Bande der Mittheilungen des k. u. k. militär-geogra-
phischen Institutes veröffentlicht, und ich habe mir erlaubt im Monate Juni den Herren De-
legirten Sonderabdrücke dieser Publication zuzusenden.

Ich glaube daher, dass es genügt, wenn ich hier nur mit wenigen Worten die wich-
tigsten Ergebnisse erwähne,

Der Einfluss der durch die Alpen gestörten Schwerkraft auf die Ergebnisse des Ni-
vellements hat sich als sehr klein erwiesen, er beträgt zwischen Mantua und München nur
18 mm. Die Ursache der Kleinheit dieses Betrages liegt grösstentheils darin, dass auf einem
grossen Theile der durchforschten Strecke die Störungen das entgegengesetzte Vorzeichen
aufweisen, und sich daher ihre Wirkungen zum grossen Theile aufheben.

Der Verlauf der Geoidfläche unter den Alpen zeigt sich durch die Masse derselben
nicht sehr stark beeinflusst, es dürfte die Erhebung des Geoides über das Ellipsoid daselbst
kaum mehr als 5 Meter betragen.

Als das interessanteste Ergebniss glaube ich anführen zu können, dass die Schwer-
kraft in den Alpen nicht überall kleiner gefunden wurde als ihr normaler Werth, wie dies
nach den neuesten Forschungen zu erwarten gewesen wäre, sondern, dass sie im südlichen
Theile der Alpen grösser angetroffen wurde. Es erscheint demnach die Annahme von der
Compensation der sichtbaren Gebirgsmassen durch unterirdische Massendefecte nicht allge-
mein zutreffend. Auch in der italienischen Po-Ebene zeigen sich systematische Aenderungen
der Schwerkraft, nahezu von gleichem Betrage, wie in den Alpen.

Aus mehrfachen Erscheinungen glaube ich schliessen zu können, dass die Ursachen
der Schwerestörungen, also die angenommenen Mässendefecte und Anhäufungen, sich in

' Der Bericht des Herrn Weiss über die Arbeiten der œsterreichischen geodätischen Commission ist dem
Sekretär nicht eingeschickt worden. H. M.

2 Siehe Uebersichtskarte, No 8.

 
