 

ir a a a if

ppp

eis

à

N he

 

mehrere Druckschriften vertheilt, deren Titel in dem Bericht des Herrn Ferrero erwähnt
sind. (Siehe Anhang B. VII.)

Der Herr Präsident dankt Nerrn General Ferrero und ertheilt das Wort Herrn
Lallemand, um eine Arbeit des Herrn Oberst Goulier über die Ausdehnung der Holzlatten
zu verlesen.

Nachdem die Versammlung den grössten Theil dieser Abhandlung angehört hat,
bittet der Herr -Präsident Herrn Lallemand, da die Zeit schon sehr weit vorgerückt ist, den
Schluss dieser Mittheilung bis zur nächsten Sitzung zu verschieben. Er giebt noch Aufschluss
über die Tagesordnung der letzten Sitzung, welche Freitag den 6. October um 9'/, Uhr
Vormittags stattfinden wird.

Aufdie Bitte des Herrn Andonowits ertheilt der Herr Präsidentihm das Wort, um einige
kurze Erklärungen, welche sich auf den Stand der Arbeiten in Serbien beziehen, zu geben.
Da man in Serbien mit den Catasterarbeiten beschäftigt ist, so hat man auch in diesem Jahre
sich mit den Vorbereitungen für die Triangulation I. Ordnung begnügt. Den Andeutungen
gemäss, welche in der Sitzung zu Florenz gegeben worden sind, ist das Einverständniss über
den Anschluss an das Netz von Oesterreich-Ungarn zu Stande gekommen, sodass man hoffen
kann, dass derselbe im Laufe des nächsten Jahres ausgeführt wird.

Um zu derselben Zeit, wo die Triangulationen beginnen werden, auch eine Basis
inessen zu können, hat sich Serbien an die französische Regierung gewendet, damit diese
die Güte habe, ihm den Apparat Brunner für diese Operation zu leihen, und, Dank der
Bereitwilligkeit dieser Regierung, wird die Basismessung ebenfalls im Jahre 1893 ausgeführt
werden können.

Nachdem der Herr Präsident dem Herrn Delegirten Serbiens gedankt hat, schliesst er

die Sitzung um 9b 40m,

 
