 

DNA Ne ait

ACIDE HAL NE

ern

TROISIÈME SÉANCE

Lundi 3 octobre.

Présidence de M. le colonel Hennequin.
La séance est ouverte à 9 h. 5 minutes.
Sont présents :

Les membres de la Commission Permanente : MM. Faye, Ferrero, Færster, Hirsch,
von Kalmar et van de Sande-Bakhuyzen.

Les délégués : MM. Albrecht, Andonowits, d'Arrillaga, Bassot, Defforges, Derréca-
quiz, van Diesen, Folie, Haid, Hartl, Lallemand, Morsbach, Oudemans, Rosen, Schols et

Weiss.

Les invités de la Commission Permanente : MM. Becker, Cornu, Guarduca et
Marcuse.

Les invités belges : MM. Beving, Brewer, Colin, Denecker, Gofjart, Henrionnet,
Ch. Lagrange, Æ. Lagrange, Nieslen, Pasquier, Ronckar, Rousseau, Serrane, Terby, Ter-

linden.
Les secrétaires-adjoints : MM. Fubry, Gillis, Hoffmann el Levieux.

M. le Secrétaire donne lecture du procès-verbal français de la deuxième séance, qui
est approuvé sans observation. Gomme le Secrétaire s'engage à publier la rédaction en langue
allemande qu’il a du reste préparée, conformément à celle qui vient d’être approuvée, la
Conférence renonce à la lecture du procès-verbal allemand.

M. le Président fait quelques communications d’affaires; il donne lecture entre
autres d’une lettre de M. le Ministre de l'Intérieur et de l’Instruction publique, qui s'excuse
de ne pouvoir assister à la représentation au théâtre, à laquelle les membres de l'Assemblée
sont Invités.

M. le Président donne la parole à M. Helmert qui, dans la dernière séance, avait

 
