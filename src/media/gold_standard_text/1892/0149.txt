 

Per TTT TTT Sern yee i

höhe

amitrét. ais dam dm

f
i
:
ie
'
E
3
#
4
+

 

DRITTE SITZUNG

October 1899.

Vorsitz des Herrn Fuye.

Gegenwärtig sind die Herren : van de Sande Bakhuyzen, Ferrero, Foerster, Helmert,
Hirsch, von Kalmar.

Ausserdem wohnen der Sitzung bei die Herren Commissare : Hartl, Schols, Weiss.
Die Sitzung wird um 9b 45m eröffnet.

Das Protocoll der zweiten Sitzung wird verlesen und ohne Weiteres angenommen.

Auf die Wahl zum Ersatz des elften Mitgliedes zurückkommend, fragt der Präsident
Herrn General Ferrero, ob er sich über den Gegenstand mit dem Herrn von Arrillaga hat
besprechen können.

Herr Ferrero bedauert sehr, den spanischen Collegen nicht angetroffen zu haben,
und setzt die ziemlich schwierige tale auseinander, in welcher sich die Commission hetrétt
dieser Frage befindet.

Herr Forster wünscht den Gegenstand auf die nächste Sitzung verschoben zu sehen.

Herr Hirsch ist derselben Ansicht, umsomehr, als aus dem 1. Artikel der Convention,
den er verliest, hervorgeht, dass normal die Denen nicht ohne Weiteres durch Coop
tation nee kann, sondern dass die Conferenz selbst das elfte Mitglied zu wählen hat.
Um aber der Bel leihen Lage, wie sie bei der Conferenz in Paris eingetreten ist, zu ent-
gehen, ist es nothwendig, das Nöthige vorzubereiten, um einer möglichst grossen Ein-
stimmigkeit sicher zu sein.

Die Commission ist mit der Verschiebung dieser Frage einverstanden.

Herr Forster bedauert nicht die Zeit gefunden zu haben, um den versprochenen
schriftlichen Bericht über die Reduction der Jahres-Beiträge vorzubereiten. Indessen da so-
wohl das Princip als auch der Betrag dieser Reduction um 10°/, der anfänglichen Beiträge
von der Commission bereits gebilligt sind, so biltet er um die Erlaubniss, sich mit dem Bu-
reau der Commission über die Fassung Vorschlages verständigen zu dürfen, welcher der
General-Conferenz zu unterbreiten ist. (Siehe Protokoll der dritten Sitzung, pag. 94.)
