 

iG |

einzelnen Anschlüsse innerhalb der je zwei in Betracht kommenden Sterngruppen und der ent-
sprechenden Zeitabschnitte die arithmetischen Mittel aller Einzelwerthe von Cc’ + Dd’ und zieht
beide Mittelwerthe von einander ab, so erhält man als Gesammtwirkung der Aberration auf die

    

i Anschliisse zwischen je zwei benachbarten Gruppen die nachstehenden Betrage:

| I 11 III IV V VI NET VIII I

| 50 ot 4.3 5 0 ee 0 As a +45

j Geht man nun von der Aberrationsconstante A auf den Betrag A + 4A über, so sind die
F 711 Dae
obigen Zahlwerthe nur mit dem Factor a 2m multiplieiren und die Producte zu den aut
| ud

h Seite 95 gegebenen Gruppenreduetionen zu addiren, um unmittelbar dem Einfluss einer ver-
änderten Annahme der Aberrationsconstante Rechnung zu tragen.

4 In welchem Verhältnisse diese beiden Ursachen bei Entstehung des Schlussfehlers von
| —o’211 mitgewirkt haben, ist allerdings aus dem vorhandenen Material zunächst nicht zu ersehen.

i Indess weisen doch eine Reihe neuerer Bestimmungen der Aberrationsconstante auf die Noth-
wendigkeit einer Vergrösserung des Srruve’schen Werthes von 2074451 hin, welcher den Con-

I stanten des Berliner Astronomischen Jahrbuches zu Grunde gelegt ist. So leitet Nyrén!) aus
‘ ö = /

i den Beobachtungen der Zenitdistanzen des Polarsternes am Pulkowaer Verticalkreise in den
| Jahren 1842—44 (Prrers), 1863—70 (Gviofn) und 1871—73 (Nyr£x) den Werth 20/495 ab; aus
= seinen eigenen Beobachtungen im I. Vertical am grossen Pulkowaer Passageninstrumente wihrend
I} Oo oO O D ;

i der Jahre 1880 8r findet derselbe Autor?) 20’517; die Beobachtungen der Rectascensionen des
4 Polarsternes von Lixpnacen’) am grossen Pulkowaer Passageninstrumente in den Jahren 1842—

a oO D -
18 ergeben den Werth 20'498. Auch die Polhöhenbestimmungen in Berlin, Potsdam und Prag‘)
q während der Jahre 1889 und 18g0 weisen auf die Nothwendigkeit einer Vergrösserung der
i Soruve’schen Aberrationsconstante um +0’07 hin. Man wird bei dieser Sachlage berechtigt |

sein, bis auf weiteres als wahrscheinlichsten Werth der Constante den Betrag 20’5o anzusehen
und wird erwarten können, dass die diesem Werthe noch anhaftende Unsicherheit den Betrag
einiger Hundertelsecunden nicht überschreitet.

Wenn hiernach eine kleine Unsicherheit in der Annahme der Aberrationsconstanten noch
bestehen bleibt, so tritt doch im vorliegenden Falle der günstige Umstand ein, dass aus dieser
Unsicherheit keinerlei Zweideutigkeit hinsichtlich der Vertheilung des Schlussfehlers erwachst.
Infolge der subtropischen Lage der Station sind nach Ausweis der Zahlen auf Seite 95 die
Gewichte der einzelnen Gruppenanschlüsse nicht sehr von einander verschieden und da nach
den Zahlen auf Seite 96 auch die Vertheilung desjenigen Antheiles am Schlussfehler, der aus
der unrichtigen Annahme der Aberrationsconstante hervorgeht, annähernd gleichmässig auf die
einzelnen Anschlüsse vorzunehmen ist, macht es keinen wesentlichen Unterschied, ob man einen
kleineren oder einen grösseren Antheil am Schlussfehler der einen oder der anderen Ursache
zur Last legt.

Um dies zahlenmässig zu begründen, ist nachstehend berechnet, wie sich die Vertheilung
des Schlussfehlers gestaltet, wenn man die Aberrationsconstante bezw. zu 20.48, 20.50 und 20:52

1), Die Polhéhe von Pulkowa von Dr. Maaxus Nyrin, St. Pétersbouig 1873, Seite 39.

2) D’aberration des ctoiles fixes par Macyus Nyrin, St. Pétersbourg 1883, page 40.

8) Mémoires des sciences mathématiques et physiques de l'Académie de St. Pétersbourg, I. section, Tome VII, page 323.
4) Provisorische Resultate der Beobachtungsreihen in Berlin, Potsdam und Prag betreffend die Veränderlichkeit

der Polhöhe von Tu, Ausrecur, Berlin 1850, Seite 51.

tal ta dd a ch

 

 
