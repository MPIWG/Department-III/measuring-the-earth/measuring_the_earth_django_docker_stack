 

I
|
É
|
h
F

lh
N
À
i
i
h

 

61%
I. Die Ausführung des neuen Belgischen Nivellements machte es erforderlich, den
Anschlüssen der beiderseitigen Netze erneut die Aufmerksamkeit zuzuwenden.
I. Anschluss bei Eupen (Dolhain).

Die Höhe des Bolzens N' 5881 in dem dortigen Nivellements-Grenzpleiler war nn
Jahre 1878 zu 301,93% m über Normal-Null ermittelt. Auf Grund dieser Höhe ergab sich der
Belgische repère principal am Zollhause, 280 m von N" 5881 entfernt, zu

994,612 m über Normal-Null.
II. Anschluss bei Herbesthal { Welkenrædt,.

Es ergaben sich durch die Messungen des verflossenen Sommers folgende endgültige
Höhen über Normal-Null. :

a) Belgischer repère principal an der Kapelle bei Welkenraedt — 282,095 m,
b) Belgischer repère principal an der Kirche zu Welkenraedt — 259,164 m,
c) Preussischer Mauerbolzen am Hause Nr 99 in Herbesthal — 260,124 m,
d) Preussischer Mauerbolzen am Pächterhause in Gut Weide

(maison Teller N 144) — 00.0210 m,
e) Preussische Höhenmarke am herrschaftlichen Wohnhause

in Gut Weide = 20.090 In,

3. Anschluss ber Beho.

Von den Bolzen 5710 und 5714 der Linie Imgenbroich-Pallien wurde je ein doppel-
tes Nivellement nach St. Vith geführt, nachdem jene Anschlussbolzen bezüglich ihrer unver-
änderten Höhenlage zuvor geprüft waren.

Die hierdurch entstandene Schleife hat bei einem Umfange von 44 km. einen Schluss-
fehler von 10 mm.

Von der in genannter Schleifenlinie liegenden Höhenmarke St. Vith ausgehend, wurde
nun ein vierfaches Nivellement von 13 km. Länge bis zur Belgischen Grenze gegenüber
Beho geführt.

Für den als gemeinsamen Anschlusspunkt dienenden, auf Preussischem Boden hart
an der Grenze stehenden Nivellements-Grenzpfeiler N" 8690 ergab sich die endgültige Höhe
des Bolzens zu 514,893 m über Normal-Null.

VEROEFFENTLICHUNGEN.

Im Jahre 1892 hat die Trigonometrische Abtheilung veröffentlicht:
Abrisse, Koordinaten und Höhen sämmtlicher von der Trigonometrischen Abtheilung
der Landesaufnahme bestimmten Punkte. Zehnter Theil. Regierungsbezirk Posen. Berlin, 1892.
Brüssel, den 9. Oktober 1892.
MORSBACU, Oberst.

alk ML ltd a ll i

 
