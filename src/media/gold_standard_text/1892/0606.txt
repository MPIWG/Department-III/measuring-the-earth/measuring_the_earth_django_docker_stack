 

 

608

L'orientation de la base sera approximativement S.E-N.0.

L'appareil dont on se servira à été construit par M. Repsold ; il est composé de
quatre règles, dont deux de quatre mètres et deux de deux mètres, celles-ci armées de mi-
croscopes. Les règles sont formées de deux barres, dont une en acier Krupp, et Pautre en
zinc avec des échelles en cristal.

On exécute maintenant une comparaison de ces règles avec un mètre normal d’une
composition identique à celle des règles, et qui fait partie de l'appareil; ce mètre normal a
été comparé à Berlin en 1884, et sera encore comparé avec le prototype qui a été attribué
au Portugal par la Conférence internationale des Poids et Mesures. Cette premiere compa-

raison est faite à la température moyenne de 23°.

LATITUDES ET AZIMUTS.

On calcule en ce moment les observations astronomiques de la latitude et d’un azi-
mut, qui ontété faites au point géodésique Buszaco, près Coimbra.
La méthode employée a été celle suivie pour les observations de latitude et d’azimut

dans l'Obzervatorio do Castello de S. Jorge à Lisbonne ; c’est-à-dire :

1. Pour les latitudes, les hauteurs cireumméridiennes d'étoiles au nord et au sud
du zénith, les passages au premier vertical, et les mesures micrométriques d’après le système
de Horrebow-Talcott.

9 Pour les azimuts, les mires méridiennes et lobservation directe de l’angle

entre la Polaire el le signal.
NIVELLEMENT DE PRÉCISION.

On a nivelé à double et en sens inverse 1353 kilomètres, jusqu'à la de 1808.
Dans l’année courante, on à déjà nivelé à peu près 100 km. et on a fait la troisième jonction
de nos nivellements avec le réseau espagnol dans la Fregeneda.

Dans le commencement de cette année, on a installé dans notre bureau un compara-
teur de règles géodésiques de Repsold, et on à débuté par comparer, avec le mêtre normal,
trois des mires qui sont maintenant en service, et nous nous proposons de les comparer de
nouveau à la fin de la campagne.

Je dois ajouter que quelques-unes de ces mires, outre leur comparaison à Berlin,
lors de leur acquisition, avaient été aussi comparées l’année dernière à l'Observatoire Royal
de Lisbonne, et que les résultats de ces comparaisons, qui ont été envoyés a M. de Kalmar
par M. le Directeur général des Travaux géodésiques, quoique bien loin d’être suffisants pour
en déduire des lois sur la variation de nos mires, nous donnent déjà cependant des éléments
intéressants pour l'étude de cet important problème.

Lisbonne, septembre 1892.
COMTE D’AVILA

Délégué du Portugal.

hd aan ds

ui

vu rad.

a nd an ny lll Addl m

1
|
|

 

 
