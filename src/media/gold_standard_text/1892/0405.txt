 

à

GR

   
  
  
    
  
    
     
   
  
    
  
 
 
   
  
 

  

   
  

 
   

un!

   

 

 

|
El
fl
i
|

 

|

|

| Méthodes et Instru-
| | ments employés.

Angle entre la Po-
laire etle signal.
Instr. universel.

Angle entre la Po- |

laire etle signal.
Théodolite de
Reichenbach, a
lunette brisée,
cercle de 12 pou-
ces.

"| Angle entre la Po- |
laire etle signal. |
Instr. universel.

»

 

|
|

 

Année et mois.

1868 octobre. |

1863 août,
septembre,
octobre.

1864 septemb.

octobre.

1864 septemb.

1864 septemb.

1865 juillet,
aoüt.

1865 août,
septembre.

1866 mai,
juin.

1866 septemb.
octobre,

1263 avril,
mai.

407

 

AZIMUTS

 

 

TIIRE DE LA PUBLICATION

Littrow. Bericht über die Bestim-

mung der Breite und des Azi-
| muths in Dablitz. Denkschr.
| der Wiener Akad. 32. Bd.

General-Bericht über die Mittel-
Europäische Gradmessung für
das Jahr 1866.

Littrow. Bestimmung der Breite
und des Azimuths auf
|  Laaerberg bei Wien.

Europäische Gradmessung für
das Jahr 1866.

»

tar-geogr. Instituts, IV. Band,

das Jahr 1866.

Pas encore publié.

Astron.-geod. Arbeiten des mili-
tär-geogr. Institutes, IV. Band.

 

General-Bericht über die Mittel- |
Europäische Gradmessung für |

dem |

(seneral-Bericht über die Mittel- |

Astron.-geod. Arbeiten des Mili- |

 

|
|

 

REMARQUES

 

Le résultat de la mesure directe
de l’angle entre Bösig et Don-
nersberg était’$2°15 11" 64

Le résultat est la moyenne de l'a-

zimut Gerkov-Bömerwall de-
termine directement et de celui
que l’on à obtenu par l’azimut
Cerkov-Tillenberg en y ajou-

tant l'angle: Tillenberg, Cer-
kov, Böhmerwall.

 

 
