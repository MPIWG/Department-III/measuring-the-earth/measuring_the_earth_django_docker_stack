  
 
   
     
     

  

     
  

125

    

 

DIAGRAMMES INDIQUANT LA FREQUENCE DES ERREURS DE CLOTURE DANS LES TRIANGLES DES RESEAUX DU CADASTRE ITALIEN.

(Les abscisses representent les erreurs de clôture exprimées

i

Les courbes en rouge représentent la fréquence théorique des erreurs d'après la loi de Gauss.

Régions: £mila
Instruments employés: Troughton & Simms à verniers. Diam. de 133 15°
Les angles ontetémesurés 4 #s :
Erreur moyenne de clöturedes triangles (my )- 74703
Nombre des triangles- 662.

 

200

150

 

 

 

Regions: Admont, Ennlia, Sardaigne, Sicile, Rome.
g I 2

Instruments employés: Troughton & Simms. Diam. de 13 à 17076.

Les angles ontétémesurés 6 frs
Erreur moyenne de clature des triangles (mA/-1441 ). 10/94
Nombre des triangles- 2204 .

 

Régions: Venise

Instruments employés: Sa/moiraghi Diam. 164”

Les angles ont été mesurés 4 #s :

Erreur moyenne de os des triangles (m 4/82 (al). 14,92
Nombre des triangles: 73

 

 

Regions: Sardaigne, Hemant, Emilia
nstruments employes: /roughton & Simms, Diam. 12,5 à 1527
es angles ontétémesurés 4 fois
ur moyenne de clôture des triangles (ml )- 14/82
N des triangles= 7579.

 

Es

 

100 |-

    

Ré gions: Home et Venise.

Instruments employés: Troughton & Simmes. Diam. de 18.4 15°" is
Les angles ontétemesurés 4 ris, 125 |
Erreur moyenne de clôture des triangles (n\/ 09). 13/32.
Nombre des triangles- 706.

100

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Régions: Sardaigne, Fiémont, Emilia.
Instruments employés: Troughton & Simms 3 verniers. Diem. de 12,3 a 1577
Les angles ont été mesurés rs
Erreur moyenne de clöturedes triangles mE). 06
= Nombre des triangles -3096

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

  

 

 

 

Régions: Bari

Instruments employés: Salmoiraghi Diam. 142”

Les angles ontétémesurés 8 fis

Erreur moyenne de clôture des triangles (m4/ 2) 7,93
Nombre des triangles- 226

en secondes et les ordonnees la fréquence Pees de ces erreurs.)

 
 
  
    

 
 

Re gions: /émont, Sardaigne, Sicile, Emilia.

Instruments employés: Troughton & Simms. Diam. de 13.4 12000,
Les angles ontétémesurés 6 fs.

Erreur moyenne de clôture des triangles (m ya). 9/22.
Nombre destriangles- 7508.

 

 

 

 

 

 

 

 

 

 

 

 

 

  

 

  
   
  
 
  
   
 
 

  
   
  
 
 
  
   

    
   
   
  

Re gions: Home, Venise, Torir, Cagliari.

Instruments employés: Troughton & Simms. Diam. de 12, 5 a 15°”
at Les arigles ont été mesurés 4/5.

\ —  Erreurmoyenne de cloture des triangles (m\/ 44) D 25 8.

‘\ Nombre des triangles- 3802.

 

 

 

       
       
      

 

 

 

Re gians: Serle et Sardargne

Instruments employes: Starke. Diam.de 21a 26°°, Troughton & Sıimms. Diam.de 12,5 315°”
Les angles ont etömesures ae 48 15 faıs

Erreur moyenne de clôture des triangles (m (m: Re] }- EUR

Nombre des triangles: 479.

 

 

 

 

 

 

 

 

 

 

 

 

 

8 eglons: Fmlız.
Deco employes: Troughton & Simms averniers. Diam. de 1731527
Les angles ont été mesurés /%. Sn
Erreur moyenne de cléture des triangles (m- ya IR,

Nombre des triangles- 1377.

 

 

 

 

 

 

 

      
        

 

 

 

 
    

Ré gions: Memont, Emilia, 8 ardaıgne, Sierle
Instruments employés: De & Simms a Vas Diam. de 13 à 17775.
Les angles ontétémesurés 666

Erreur moyenne de cléture des triangles (m )- 70,93

Nombre des triangles- 2770.

   
   
 

 

 

 

 

 

 

 

 

 

 

 

  

 

an
Ze

ee er PIE

 

 

 

 

 
