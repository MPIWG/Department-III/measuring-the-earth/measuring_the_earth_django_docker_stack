 

1 RH PRT Waar HT

Peary) vit

Vp NT

1

eat

N

fib liad,

25
5
3
2
a:
=
z
2
=
=
=
“=
22
IE
a=
AB
*

 

209

m

Bonn-Mannheim — 5 27,203 poids 1 1?
| 21281 » 2
moyenne 92a >» 3,

La différence de longitude Altona-Wilhelmshaven a été déterminée 2 fois en 1878,
Prusse 47, 48 et 49 :

Altona-Wilhelmshaven + 7 11,199 poids 2
11158 >» à
moyenne + 211,181 5055

La difförence de longitude entre Schönsee et Trockenberg a été déterminée deux
fois en 1889, Prusse 67, 68 et 69.

Schonsee-Trockenberg + 0 5,118 poids 2
10 » 2
moyenne 1.0 6,152 » 4

Il y a deux déterminations de longitude entre Moscou et Pulkowa de 1863 et 1872,
Russie | et 16.

Moscou-Pulkowa 28 58,46 poids 1
08,49 09
moyenne 28 58,453 » 3

La différence de longitude entre Kieff et Varsovie a été déterminée deux fois en 1876
et 1877, Russie 97 et 98:

m Ss
Kieff-Varsovie + 37 53,35 poids 1
53,25 | à» 4
moyenne + Mo) à À

ÉLIMINATION DES STATIONS QUI NE SONT RATTACHÉES QU'A UNE STATION
OU A DEUX STATIONS

On peut commencer par écarter toutes les déterminations, dont l’une des stations a
été reliée seulement à une autre. Ce sont les déterminations suivantes : Autriche 38, Belgique
9, France 8, 7, 8, 10, 11, 12, 13, 14, 96, 36, 38, 39, 48,49, 51, Italie 20006) Norvège
3, 4, Portugal 1, Prusse 2, 14, 60, 74, Roumanie 2, Ruse 10, 12013, 14, 17,19, 5%
42, 43, 44, 45, 99, 61, 62, Saxe 1, 4, 13, Suede 6, 10, Suisse 5, 6 et aussi Espagne 1, puis-
que la position du cercle méridien à Madrid par rapport au centre de l’Observatoire n’est
pas communiquée. Par suite de cette élimination, les déterminations suivantes disparaissent
par la même cause : France 5, Italie 1, 27, Prusse 28, Russie 59, 60, Suède 9.

ASSOCIATION GÉODÉSIQUE — 27

 
