 

 

RES

Année et mois.

1867 août,
septembre.

1568 avril.

mai.

 

 

1870 juillet
et1871 juil-
let, août.

Pi août.
septemb.

 

1 |i 1872 juin.
juillet.

1874 juin.

18/4 juin,
juillet.

|| 1874 juillet,
aoüt.

 

—= =

 

Elimination des
équations
personnelles.

 

TITRE DE LA PUBLICATION

 

Echange des obser
vateurs et des
instruments.

Détermination
directe.

Determination di-
recte.

»

Echange des obser-
vateurs.

Détermination di-

recte.

Echange des ob-
servateurs.

|

 

Peters, Bestimmung des Längen-
unterschiedes zwischen den
Sternwarten von Altona und
Kiel. Kiel, 1873.

Bruhns, Bestimmung der Län-
gendifferenz zwischen Berlin
und Lund. Lund, 1870.

Publication des K. Preuss. geod.
Inst. : Astron.-geod. Arbeiten
im Jahre 1871. Leipzig, 1873.

Publicalion des K. Preuss. geod.

Instituts : Bruhns. Astron.-
geod. Arbeiten in den Jahren
1872, 1869 und 1867. Leipzig,
1874.

Publication des K. Preuss. geod.
Instituts : Albrecht, Astron.-

geod. Arbeiten in den Jahren |
1875 und 1874. Berlin, 1875.

»

 

 

Dans la publication du résultat,
il s’est glissé une erreur qui
est corrigée dans Astr.-geod.
Arb im Jahre 1876, page 118.

 

 
