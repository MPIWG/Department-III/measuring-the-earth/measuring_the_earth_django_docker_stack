Fe ee re

2k ea pr Pe SEHE

Raa ee

ll) pin im!

[RL

a
a

 

m pot] a) ni

 

651

Dans le pendule du Service g&ographique, D 2
.—h

environ, et, par conséquent

Cy
ms

9
J

oF
a
|

comme, pour ce pendule

 

3 (= yo)
Jo 7

N ga Me rar ho ou
ey 2 7

 

L'application du principe de Bohnenberger triplerait done, dans le rapport calculé
des pesanteurs, l'erreur provenant de la détermination du temps. C’est, au moins en appa-
rence, une infériorité du pendule inversable. Il resterait cependant à examiner si la certi-
tude de l’invariabilité de la longueur, si facile à constater, ne compenserait pas largement
ce défaut.

Mais on peut améliorer singuliérement le résultat en modifiant la méthode de calcul.
On remarquera que l'accroissement du coefficient de l’erreur vient, tout entier, dans le se-
cond calcul, de l'erreur de la correction

h’
— (T—T’
nn ( )

Il est clair que si T—T’ était affecté d’une erreur négligeable, le coefficient de l’er-
reur résiduelle serait le même dans les deux cas. Or, dans la méthode de Bessel, l'expression
de T — T’ est assez compliquée. Si l'on désigne par T T,, TT. les quatre durées essen-
tielles qu’on doit déterminer séparément, autour de l’un et de l’autre couteau, avant et après

l’échange des couteaux, le développement de ces durées en fonction des forces perturbatrices
est

 

 

 

 

 

| ene wh, 2. p he, peta)
Jer contea T, = % ye le ae ie eg ı no + 2% |
Avant l'échange a : ; , pi
Ime conten T, = T doi a = Se Lo
we g l Uhr‘ ah 2W À 2 1e

 
