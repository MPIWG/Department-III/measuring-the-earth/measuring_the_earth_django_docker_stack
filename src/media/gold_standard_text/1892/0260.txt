 

 

O2

=D

 

STATION

2 Naples. Observatoire de

Capodimonte, centre.

Palerme. Cercle ıneri-
- dien
Milan. Centre de la

grande coupole.
Simplon. Point géodési-
que

Milan. Centre
grande coupole.

Neuchatel. Cercle méri-
dien .

de la

Naples. Observatoire de
Capodimonte.

Termoli

Milan. Centre de 1a
grande coupole. :

Vienne. Nouvel Obser-
vatoire.

Nan, Contre. de la

grande coupole.
Munich. Observatoire de
Bogenhausen

Padoue.Quadrant mural.
Milan. ‘Centre de la

grande coupole.

 

DIFF, DE LONGITUDE,

En temps.

4 89.169

-H

1 359,980

Voir
Autriche 26

Veir
Baviere 7.

+ 10 43,205

| + 0,013

 

Erreur
probable.

= 0,021)

 

+ 0,020)

 

OBSERVATEURS

Nobile, Tacchini.

Geloria. Plantamour.

@Geloria; Hirsch,
Schmidt.

Fergola, Nobile.

Lorenzoni, Celoria.

 

Instruments employés.

A Naples, lunette méri-| |R

dienne de Reichen-

bach. | 1

A Palerme, cercle mérid. |
de Pistor et Martins. |

A Milan,

 

 

 

 

 

 

 

 

Anne

i
instrument! |

 

des passages à lunette! |

brisée. | À

Au Simplon, théodo-|

lite d’Ertel. iii

 

 

Avo: Milan,
des passages à lunette | |
brisée. I |

A Neuchatel, lunette mé- | | |
ridienne. |

À Naples, lunette mé-
ridienne de HReichen- | |
bach. | |

A Termoli, instrument] |}

|

 

des passages à lunette |
brisée de Repsold.

| ||
||
Instruments des passa- | N

ges à lunette brisé

d’Ertel et de Repsold. | | |

I
i
|
|
|
|

=

 

 

 

 

 

 

instrument | | Bi
