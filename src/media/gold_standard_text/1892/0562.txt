;
ba
Est
I
hi
|

 

964
keiner grossen Tiefe unter der Erdoberfläche befinden. Es wäre demnach nicht unmöglich,
dass die Stellen der wesentlichsten Aenderungen der Schwerkraft, auch im Terrain auf der
Erdoberfläche kenntlich seien, wie dies thatsächlich in dem durchforschten Gebiete zu
sein scheint.

Die Wichtigkeit der auf diesem, man kann wohl sagen, erst neu erschlossenen
Forschungsgebiete erhaltenen Resultate, von welchen man vorläufig noch nicht entscheiden
kann, ob dieselben mehr der Geodäsie oder der Geologie angehören, ist erfreulicher Weise
in weiteren Kreisen erkannt worden, und es ist jetzt Aussicht vorhanden, dass in der nächsten
Zeit meine bisherigen, nur vereinzelten Forschungen eine namhafte Erweiterung erfahren
werden, und zwar nicht nur in dieser Richtung, sondern es dürften jetzt die relativen
Schwerebestimmungen, nach langer Pause, überhaupt wieder zur Geltung kommen und
eifriger betrieben werden. Ich erlaube mir diesbezüglich nachfolgend zu berichten : Im Laufe
des vergangenen Winters 1891-92 sind in Wien bei dem Mechaniker Schneider zahlreiche
Bestellungen auf Pendelapparate meiner Construction aus dem In-und Auslande eingelangt,
und wurde ich gleichzeitig ersucht, mit den mir zur Verfügung stehenden Hillsapparaten
die Constanten der Apparate, sowie die Schwingungszeiten der Pendel in Wien zu be-
stimmen.

Diesen mich sehr ehrenden Aufträgen bin ich im Frühjahre nachgekommen, indem
ich die Constanten von 7 Pendelapparaten und die Schwingungszeiten von 25 dazugehörigen
Pendeln bestimmt habe. Ich hoffe hierdurch zur Vereinheitlichung der Angaben über die
Schwerkraft beigetragen zu haben.

Eine wesentliche Förderung verlanken die relativen Schwerebestimmungen der
Initiative der k. u. k. österreichisch-ungarischen Kriegs-Marine, welche sich bewogen ge-
fühlt hat, durch die Schiffe in Mission im Auslande zahlreiche Schwerebestimmungen
ausführen zu lassen. Zu diesem Zwecke wurde ein Pendel-Apparat mit 9 Pendeln sammt
Steinpfeiler und sonstigem Zugehöre angeschafft, welcher sich bereits an Bord S. M. Cor-
vette « Saida » in See befindet, und wird während deren 16 monatlicher Reise im indischen
und stillen Oceane der k. u. k. Linienschiffslieutenant Ritter von Müller auf etwa 30 Stationen
relative Schwerebestimmungen ausführen.

. Ueber Auftrag der k. u. k. Kriegs-Marine hat ferner der k. u. k. Linienschifislieute-
nant August Gratzl an Bord des französischen Transportaviso « Manche » eine Reise nach
Norden unternommen und hierbei mit dem Pendelapparate N° 1 des mililär-geographischen
Institutes in Edinburg, Jan Mayen, auf Spitzbergen und in Tromsö, also bis nahezu unter
80° Breite Schwerebestimmungen ausgeführt.

Beide Herren haben sich im Laufe des vergangenen Winters unter meiner Leitung
mit dem Pendelapparate, den Beobachtungen und deren Reductionen vollkommen vertraut
gemacht und es sind daher verlässliche Resultate zu erhoflen.

Mit Bewilligung des k. u.k. Reichs-Kriegs-Ministeriums habe ich im Monate Juli
|. J. in Berlin, Potsdam und Hamburg mit meinem Apparate relative Schwerebestimmungen
ausgeführt. Im Vereine mit den im vergangenen Jahre in München, Padua und Wien (Turken-
schanze) ausgeführten Messungen, bin ich nun in der Lage aus den beobachteten Unter-

sald soroh

hi Mand Ladd

 

 
