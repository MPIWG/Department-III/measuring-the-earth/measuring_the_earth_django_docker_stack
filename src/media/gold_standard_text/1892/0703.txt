ee are

1 TET PR en pee

LMI LU

LU CR

wiry hen TOP ML Pa TTT

nt

 

0.3

Nach Verbesserung der Mikrometerablesungen wurden alsdann die Polhöhen unter Anwendung
der Formel:

Ca J Stern Süd, Kreis Ost; Stern Nord, Kreis West.
Sam 1

— 4 A ane ) sre
p=3(& +4) EI Rom —m) + | Stern Süd, Kreis West; Stern Nord, Kreis Ost.

lo

berechnet, in welcher d, und d, die scheinbaren Declinationen des südlichen bezw. nördlichen
Sternes, m, und im, die Mikrometerablesungen bei Einstellung auf den Süd- bezw. Nordstern in
Einheiten der Schraubenumdrehung ausgedrückt, R den Winkelwerth einer Schraubenrevolution,
y, und 7», die Refractionsbeträge für den südlichen und nördlichen Stern bezeichnen.

Die Berechnung der Reductionen vom mittleren auf den scheinbaren Ort ist auf Grund der

Constanten des Berliner Astronomischen Jahrbuches erfolgt. Hierbei wurde eine Vereinfachung
der Rechnung dadurch herbeigeführt, dass unter Anwendung der Formel:

 

 

tn En arte + Bet ae erh a p Tr ae men
2 2 2 2 2 2

die Reductionen gleich für das arithmetische Mittel der Declinationen jedes Sternpaares berechnet
wurden. Dieses Verfahren ist zwar insofern nicht ganz streng, als man hierbei die Sternzeit-
Constanten dA, B, C und D nicht mit den Rectascensionen des ersten bezw. zweiten Sternes,
sondern mit dem arithmetischen Mittel der Rectascensionen beider Sterne entnimmt; doch kann
der Fehler, welcher dadurch begangen wird, innerhalb der hierbei in Betracht kommenden
Grenzen der ’Declinationen der Sterne niemals den Betrag einer Tausendtel-Bogensecunde
überschreiten. Der Winkelwerth einer Schraubenrevolution wurde gleichwie bei der Reduction
der Potsdamer Beobachtungen vom Jahre 1889/90 zu 60.160 angenommen, nachdem wiederholte
gelegentliche Prüfungen dieses Werthes während der Beobachtungen in Honolulu — nach der
Methode der Durchgangsbeobachtungen von Polsternen zur Zeit der grössten Digression —
die Unveränderlichkeit desselben ausser Zweifel gestellt hatten. Der numerische Werth des
Refractionsgliedes wurde der Hülfstafel 29 meiner: Formeln und Hülfstafeln für Geographische
Ortsbestimmungen, Leipzig 1879 entnommen.

In der folgenden Tabelle sind die Reductionen in unmittelbar ersichtlicher Weise zusamınen-
gestellt. Nur in betreff der Correctionen wegen der Neigung, der Krümmung des Parallels und
des Fehlers der Schraube in der 5.—7. Columne ist noch zu erwähnen, dass dieselben in
Tausendtheilen der Schraubenrevolution angesetzt sind.

 
