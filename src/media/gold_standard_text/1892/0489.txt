ee ee ie mec

ger pre

 

Man u

iu] int

IR aT am LITE

491

Schwerkraft mit der Höhe im Gebirge zu liefern. Solche Studien wurden in der Gegend von
Nizza, in den Pyrenäen und längs eines Meridianbogens in Nordafrika angestellt.

Die Reihe V wurde von Herrn Generallientenant Stebnitzki gefälligst mitgetheilt.
Die Länge des Sekundenpendels in St. Petersburg ist aus mehreren unabhängigen Bestim-
mungen abgeleitet.

1. Aus den Beobachtungen des Akademikers Sawitsch 1865 in St. Petersburg mit
den der Akademie der Wissenschaften gehörenden Reversionspendeln von Repsold und mit
Berücksichtigung der vom Oberst Kuhlberg bestimmten Correktion für die Schwankung des
Stativs.

2. Durch Reduktion der in Pulkowa vom Akademiker Sawitsch und Oberst Smyssloff
1866, von Professor Zwinger 1874 und von General Stebnitzki 1876 mit denselben Pendeln
gemachten Bestimmungen auf St. Petersburg und bei Berücksichtigung derselben Correktion
für die Schwankung des Stativs.

3. Durch Reduction auf St. Petersburg der Differenz der Längen des Sekunden-
pendels in Kew und Pulkowa, welche mit demselben Apparat von Kapitän Heaviside in Kew
1873-74 und von Professor Zinger in Pulkowa 1874 bestimmt worden sind; und

4. Aus der Differenz der Längen des Sekundenpendels, welche von Admiral Graf
Lütke 1826-29 in London und St. Petersburg mit einem invariablen Pendel bestimmt worden
sind; dabei wurde die Länge desselben in London gleich 39,13749 englischen Zollen ange-

nommen, nach den Beobachtungen von Sabine in Greenwich 1829 und Heaviside in Kew
1874.

Siehe den Artikel von J. Stebnitzki : « Die wahrscheinlichste Länge des Sekunden-
pendels in St. Petersburg ; die Länge desselben in den Punkten der russischen Meridiangrad-
messung. Resultate aus den Beobachtungen während der Weltumsegelung des Grafen Lütke
1826-1829. Publicirt in den « Memoiren der Akademie der Wissenschaften in St. Peters-
burg », Band XLVI, zweites Buch, 1884.

In Russland wurden in neuerer Zeit Beobachtungen mit drei verschiedenen Appa-
raten angestellt, die der Akademie der Wissenschaften in St. Petersburg, dem Konstatinov’-
schen Messinstitut in Moskau und der Kaiserlichen Geographischen Gesellschaft gehören.
Diese Apparate sind Repsold’sche Reversionspendelapparate von 3/4 Sekunden Schwingungs-
dauer ; jeder derselben ist mit mehreren Pendeln ausgestaltet.

Mit der älteren Reihe von Lütke und Reinecke liegen insgesammt zur Zeit 40 Sta-
tionen in Europa und 7 ausserhalb desselben vor. Auf den russisch-skandinavischen Meridian-
bogen fallen 11, aus den Jahren 1865-68, auf den Parallelbogen in 52° Breite und dessen
Nähe ebenfalls 11, aus den Jahren 1888-90. Dazu treten ausser den Anschlussstationen St.
Petersburg und Pulkowa noch 7 verschiedene Stationen in verschiedenen Gegenden des eu-
ropäischen Russlands, darunter eine in 72°23’ Breite, sowie 9 Stationen im Kaukasus.

: ; Me 4 3 21 Al
Die Attraktion des Terrains ist einfach nach der Formel ao berechnet.

Kür die Beobachtungen zu No 35-39 einschliesslich der Anschlussmessungen in
Pulkowa ist die Publikation zu vergleichen :

« Mittheilungen der Kaiserlich russischen Geographischen Gesellschaft. Materialien

 

 

 
