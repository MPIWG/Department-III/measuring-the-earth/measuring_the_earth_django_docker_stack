 

BESSER

RR ENS ELT SS SET

oR TE

sn

RER TS

BESSA Sg ELE

ee

BERTEE

9

i
É i
| |
N
R
a
H
5
i
i
a :

Nr nee

 

LEN SESE PUS PRE eee

ENT

 

II. Vergle

902

ichung von Höhennullpunkten.

 

 

 

 

 

 

 

 

 

 

| Höhe über dem Mittelwasser
| in Amsterdam.
Nr. ORT PUNKT | GELTUNGSBEREICH DES NULLPUNKTS u |
I. Aus- | IL Aus- | III. Aus-
| gleichung. | gleichung. | gleichung. |
| : m | m | m |
| | |
A Swinemünde | Mittelwasser der Ostsee | Aeltere Gracmessungsnivellements des Geo- |+ 0,099 4 0,040 ==
dätischen L.:tituts; Sachsen ; Hessen | |
2 Berlin Normal-Nullpunkt (N. N.), Der grösste Theil Deutschlands ; + 01465 0,105 — |
| 37 m unter dem Normal- Sachsen, gleichzeitig mit Nr. 4 | |
Höhenpunkt an der Stern-
warte | | |
3 Wismar Nullpunkt des aiten Pegels Mecklenburg, zugleich mit Nr. 2 — 0,085— 0142 — |
4 Amsterdam | Nullpunkt des Pegels, A. P. | Niederlande + 0,162+ 0,162+ 0,162
5 Ostende Mittelwasser der Nordsee Belgien — 0,224 — 0,446\— : 0,146]
6 Alicante Fixpunkt N. P. 4. Spanien I 38. u, 8.097
6a » Mistelwasser des Mittel - » + 0,466 nt 0,066
meeres | |
1 Marseille » Frankreich — 0,168 — 0,2454 0,069)
8 Genova Nullpunkt der provisori- Italien — 0,482 — 0,649 — 0,303)
schen Höhen | |
» Mittelwasser des Mittel- » — 0,455 — 0,292 0,022
meeres | |
Triest Mittelwasser Oesterreich-Ungarn — 0,093) — 0,197 =
des Adriatischen Meeres |
Genève Pierre du Niton Schweiz -+- 373,571|-++ 373,538 + 373,740
| |

 

 

Potsdam, im September 189.

 

Dr A. BORSCH.

 

|
|
4
i

 
