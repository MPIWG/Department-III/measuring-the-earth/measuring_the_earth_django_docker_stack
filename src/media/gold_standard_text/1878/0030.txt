 

22

Herr Barozzi erstattet den Bericht über den Fortschritt der Gradmessungs-
Arbeiten in Rumänien, bei welcher Gelegenheit er bedauert, dass wegen der kriegeri-
schen Ereignisse die Fortschritte nicht so weit hätten gehen können, wie er ge-
wünscht habe.*)

Herr von Forsch berichtet über die in Blisland ausgefiithrten Arbeiten.**)

Der Präsident Herr Zbañez dankt den Herren Berichterstattern und. übergiebt

den Vorsitz an Herrn von Bauernfeind.

Herr Bruhns berichtet über die Arbeiten in Sachsen***) und theilt mit, dass er
in Leipzig einen Nullpunkt für das Nivellement, so genau es jetzt möglich sei, auf 115,0
Meter über dem Meere errichten werde und alle Coten von diesem Nullpunkte mit der
Addition von 115 Meter angegeben würden; man habe dadurch für topographische und
geographische Zwecke alle Höhen über der Meeresfläche schon jetzt bis auf etwa 0'1
genau, und diese letzte Grösse könne erst präcis ermittelt werden, wenn die Frage über
den allgemeinen Nullpunkt des Europäischen Präcisionsnivellements beantwortet sei.

In Betreff der Karten beabsichtigt derselbe neben dem Landesmeridian auf jedem
Blatte auch die Länge.von Greenwich oder Paris anzugeben und er wagt den Wunsch
auszusprechen, dass auf allen. Karten, weil sie dadurch leichter vergleichbar werden, die
Längendifferenz des Landesmeridians mit Greenwich oder Paris angegeben werden
möchte. Welcher von diesen beiden Meridianen den Vorzug verdiene, glaube er Jedem
nach eigenem Ermessen überlassen zu müssen.

Herr Hirsch erstattet Bericht über die Schweizerischen Gradmessungsarbeiten
und legt das Protokoll der letzten Sitzung der Schweizerischen geodätischen Commis-
sion vor. +)

Herr Ibanez berichtet über den Fortschritt der Gradmessungarbeiten in Spa-

- nien++) und übergiebt den anwesenden Herren den 2. Band: „Memorias del instituto

geografico y estadistico, Madrid 1878,“ welcher die Fortsetzung des Dreiecksnetzes und
astronomische Arbeiten enthält, ferner zwei Karten dazu und legt zwei neue Serien der
topographischen Karten Spaniens vor.

Der Präsident Herr von Bauernfeind dankt den Berichterstattern, ganz besonders
aber Herrn Ibanez fiir das werthvolle inhaltreiche Geschenk.

Herr MHilgard giebt einen Bericht über die Fortschritte der Geodäsie in den
Vereinigten Staaten Nordamerikas. Die Aenderung des Titels des Institutes in „Coast
and geodetic Survey“ hat Vortheile für die Organisation.

Während des vergangenen Jahres ist der Abschluss der Triangulation der Ost-
küste von Maine bis nach Georgia, welcher einen Bogen von etwa 12° Breite und 18°

*) Siehe Generalbericht, Rumänien.
**) Siehe Generalbericht, Russland.
***) Siehe Generalbericht, Sachsen.
+) Siehe Generalbericht, Schweiz.
++) Siehe Generalbericht, Spanien.

 

=
=
=
=
a
=
a
=
>
=
x

vl,

ak nl

mi!

tt, phn bsg aa nn 1

2.) sodustéant, named si |

 
