| rn oF =r

 

ei

an AR

N A A

LTT

89

c) Jonction de l'Algérie avec l'Espagne.

Les deux gouvernements de France et d'Espagne ont décidé, d’un commun accord,
que les opérations relatives à la jonction de l'Algérie avec l'Espagne, seraient exécutées
en 1879, sous la direction, pour l'Espagne, de M. le Général Ibanez, et, pour la France,
de M. le Commandant Perrier.

Le Programme des opérations à exécuter, discuté en commun par MM. Ibanez
et Perrier, comprend:

1° La mesure des angles du quadrilatère méditerranéen, formé par les sommets
de Mulhacen et Tetica en Espagne, Filhaoussen et M’Sabiha en Algérie.

2° La mesure d’une latitude et d’un azimut aux deux stations de Tetica et
M’Sabiha, ainsi que la détermination, & l’aide de signaux lumineux rythmes, de la diffe-
rence de longitude entre ces deux stations.

Les angles du quadrilatere doivent étre mesurés simultanement aux quatre
stations, afin de pouvoir profiter de toutes les circonstances atmosphériques favorables,
Pour les observations géodésiques et astronomiques, les instruments employés et les
méthodes adoptées doivent étre identiques de part et d’autre.

En vue de cette opération, M. le Général Ibatiez avait déja fait contruire par
Mr. Brunner des cercles azimutaux semblables aux nôtres, pourvus d’un réticule mobile
et pouvant se prêter, avec une égale facilité, aux observations de jour et de nuit.

Des cercles méridiens portatifs avaient été aussi commandés à M. Brunner, mais
cet habile artiste n’a pas pu les livrer en temps opportun. Nous avons été heureux de
pouvoir mettre à la disposition des astronomes espagnols, l’un de nos cereles méridiens
portant le No. 2 et identique avec le cercle No. 1, qui était destiné à la station de
M’Sabiha.

A cause de l’énorme distance qui sépare les sommets de l'Espagne de ceux de
l’Algérie, nous avons dû faire construire des appareils spéciaux, pour produire des
signaux lumineux d’une portée optique considérable. Tous ces appareils ont été con-
struits à Paris.

Pour les observations de jour, nous avons fait fabriquer des héliotropes du modèle
adopté au dépôt de la guerre, dont les miroirs ont trois décimètres de côté.

Pour les observations de nuit, aux collimateurs optiques ordinaires reconnus
insuffisants à des distances supérieures à cent vingt kilomètres, nous avons substitué des
miroirs en verre argenté de cinquante centimètres de diamètre, et de soixante centi-
mètres de distance focale, imaginées par le Colonel Mangin. A la lumière du pétrole, nous
avons substitué la lumière électrique, maintenue au foyer de l’appareiïl réflecteur par un
régulateur Serrin, et produite par une machine électrique de Gramme, actionnee par un
moteur à vapeur. On obtient ainsi des effets lumineux d’une puissance incomparable.

Tous ces appareils ont été livrés à l’époque convenue, dans les premiers jours
de Juin, après avoir été étudiés et vérifiés à Paris, de concert, par les officiers espagnols

12

 
