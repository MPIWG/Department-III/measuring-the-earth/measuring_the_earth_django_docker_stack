 

ti
8
j
IE
i
N
i
“
|
bi

EE a ee ee

fF

11
bi
1
EL

 

LEE u RR
x

M. Elie David de Genève.

M. II. Sainte-Claire Deville, Membre de l’Institut de Paris.

M. le Colonel Emile Gauthier de Genève.

M. le Major Maggia, Chef de section des travaux géodésiques de Florence.
M. Maunoir de Genève.

M. le Dr. Meyer, Aide- Astronome à l'Observatoire de Genève.
M. Raoul Pictet de Genève.

M. le Directeur G. Rümker de Hambourg.

M. Edmond Sarasin de Genève.

M. le Professeur Soret de Geneve.

M. Charles Soret de Genève.

M. Léon Tessier de Paris.

M. le Professeur Thury de Genève.

M. l'Ingénieur Veynassat de Genève.

Local des séances
Hôtel de Ville de Genève, Saile de l’Alabama.

Programme
pour la réunion de la Commission permanente

à Genève en 1879,

1. Rapports annuels de la Commission permanente et du Bureau central.
92. Rapports de Messieurs les délégués sur l'avancement des travaux dans leurs
Days en 1879.

3. Rapport de la Commission nommée & Hambourg, pour s’occuper des travaux
présentés par M. le Lieutenant-Colonel Adan (Rapporteur M. C. A. I’. Peters).
4. Choix de l’endroit et de l’époque pour la 6™° conférence générale; désignation

de rapporteurs spéciaux chargés de rendre compte à la prochaine conférence
générale de l’état actuel dans les différents pays:
a) des déterminations de latitudes, longitudes et azimuts;
b) des triangulations et des calculs de compensation des réseaux;
c) des travaux de nivellements de précision et des résultats fournis par les
maréographes ;
d) des mesures de l'intensité des pesanteurs;
e) des publications concernant la mesure des degrés en Europe.
5. Propositions individuelles.

 

4
i
;
+
4
:
i
3
3

rer de der-dén et

coche ue pe Dh ha Ab bb ddl anna

 
