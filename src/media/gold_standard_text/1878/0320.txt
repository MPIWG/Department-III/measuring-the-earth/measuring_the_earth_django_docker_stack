 

 

128

Anhang.

Relationen zwischen dem Fundamental-Catalog für die Zonenbeobachtungen, dem
Gradmessungs- und dem Safford’schen Catalog.

Im Generalbericht für 1877 sind auf Seite 34—36 die Relationen gegeben,
welche zwischen den Declinationen des Vierteljahrsschrifts-, des Gradmessungs- und des
Safford’schen Cataloges bestehen. Während bei der damaligen Untersuchung hinsicht-
lich des erstgenannten Cataloges nur die provisorischen Positionen des Cataloges im
4. Bande der Vierteljahrsschrift der astronomischen Gesellschaft zu Grunde gelegt wer-
den konnten, ist inzwischen unter dem Titel: „Fundamental-Catalog für die Zonen-
beobachtungen am nördlichen Himmel, herausgegeben von Auwers, Leipzig 1879“ der
definitive Catalog erschienen und es dürfte daher von Interesse sein, die Relationen zu
ermitteln, welche zwischen den Declinationen dieses Fundamental-Cataloges einerseits
und denen der beiden anderen oben genannten Cataloge andererseits bestehen.

Hält man an dem Verfahren fest, welches seiner Zeit von Herrn Geheimrath
Bruhns und mir in Anwendung gebracht worden ist — vergl. Verhandlungen der vom
27. September bis 2. October 1877 zu Stuttgart abgehaltenen fünften allgemeinen Con-
ferenz der Europäischen Gradmessung u. s. w., Berlin 1878 Seite 23—87 —, so bedarf
es in jener Ableitung nur einer Substitution der Declinationen des Fundamental-Cataloges
an Stelle der auf 29-33 aufgeführten Declinationen des Vierteljahrsschrifts-Cataloges,
um die gewünschten Relationen zu erhalten.

Vereinigt man die Differenzen zwischen den Positionen je zweier Cataloge für
diejenigen 242 Sterne, welche nach Ausschluss der Sterne « Can. min., y Virginis und

4eLyrae in wenigstens zwei der genannten Cataloge vorkommen, in Gruppen von 5
zu 5° der Declination zu Mittelwerthen, so erhält man die folgenden Differenzen:

Declination Fundamental—Gradmessung Wahrsch. Fehler Anzahl
— 134 — 0733 a 0.05 6
2 1 — 0.32 + 0.04 6

+ 314 — 0.38 + 0.04 5
+ 8 6 — 0.27 + 0 O7 10

 

NVP VENT NT rh PTT DST EREEN TE

rrwey elt depict es.

rl Lies

 
