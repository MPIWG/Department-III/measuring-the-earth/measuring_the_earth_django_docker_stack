 

eh
i
N

hi
IN
Ki
i

 

94

Polygone I. Nieuwe schans, Weener, Lengen, Nordhorn, Denekamp, Oldenzaal,
Deventer, Assen.  Circonférence 348 kilomètres, dont 231 kilomètres appartiennent
au nivellement neerlandais, 117 kilomètres au nivellement de la Preussische Landes-Auf-
nahme. Erreur de raccordement: 26 millimètres.

Polygone II. Amersfoort, Deventer, Oldenzaal, Bentheim, Burgsteinfurt, Wesel,
Straelen, Dammerbruck, Venlo, Arnhem. Circonference 425 Kilometres dont 259 appar-
tiennent au nivellement neerlandais, 166 Kilométres au nivellement de la Preussische Landes-
Aufnahme. Erreur: 17 Millimötres.

Polygone III. Amersfoort, Deventer, Oldenzaal, Salzbergen, Lohne, Hamme,
Neuss, Venlo, Arnhem. Circonférence 635 kilometres, dont 271 Kilometres appartiennent
au nivellement neerlandais et 364 Kilométres au nivellement du Bureau Central de
l'association géodésique. Erreur: 22 Millimètres.

La partie de ce polygone appartenant au nivellement neerlandais est avec l’ex-
ception de la section Bentheim-Salzbergen la même que celle du polygone précédent.

Polygone IV. Amsterdam, Amersfoort, Arnhem, Roermond, Eindhoven, Boxtel,
Bois-le-Duc, Bommel, Utrecht. Circonférence 375 Kilomètres appartenant en totalité au
nivellement neerlandais. Erreur: 17 Millimètres.

Les raccordements au Nieuwe schans, Denekamp et Venlo ont servi à déter-
miner le rapport entre le zéro du nivellement Prussien et l’Amsterdamsch Peil.

M. M. van de Sande Bakhuyzen et van Diesen ont lintention d’operer dans cette
année encore, deux raccordements savoir ceux à Elten et & Dammerbruch pres de Venlo.

La Commission neerlandaise.

Fr. J. Stamkart, J. Bosscha,

President. Secrétaire.

Hessen.

Bericht über die im Jahre 1879 für die Europäische Gradmessung
ausgeführten Arbeiten.

Nach dem Rücktritt des Herrn Obersteuerdireetor Dr. Hügel ist dem Unterzeich-
neten das Commissorium bei der Europäischen Gradmessung für das Grossherzogthum

Hessen übertragen worden.

In Bezug auf die Gradmessungsarbeiten im Grossherzogthum sei erwähnt,
dass die Winkelmessungen auf den beiden trigonometrischen Hauptpunkten Melibocus
und Taufstein schon vor mehreren Jahren vollendet wurden. Da sich ausserdem kein

)

 

ee her ae dede

enter sdb ts

rene a dal a ede

 
