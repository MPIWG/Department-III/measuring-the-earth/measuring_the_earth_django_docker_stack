 

GT ee EEE LETTER

at

Re EN

en

EEE

Ban ee rn

Sasa See ee

:
|
i

—

108
haupt in dem Netz auftretenden Dreiecke ermittelt und mit seiner theoretischen Winkel-
summe (180° + sphirischer Excess) verglichen worden. Das aus 36 Punkten mit 131
gegenseitig beobachteten Richtungen bestehende Netz weist im Ganzen 197 Dreiecke
nach, von denen 131 — 36 + 1= 96 als unabhingig von einander zur Aufstellung der
Winkelgleichungen zu benutzen sind. Die Fehler der vier grössten zusammenhängenden
Dreiecke, welche fast das ganze Land bedecken, berechnen sich folgendermaassen:

mm

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Winkel
"Dreieck. | Standpunkt. nach der Ausgleichung
| auf der Station.
a RE | 50° 26° 19'226
egrohlebeır ......... Of 6a 414 +701
Koulenber® .....-... | 65 49 44.196
Summe = | 180 0: 324
| 180% 2 1,480 2.01.8480
| Fehler — | + 0.033
2 | Wenlonberg ....-..- 81 9 5.664
Hédéahleberenon, Jai 3% | 553591 10:342
| Bolmesanun.mı Sale ig O30 (hi 52.991
| Summe = | 180 0 8.997
180° + E. 150, 0 2,156
| Fehler = 0.180
3. Colmar | AD 11 59.167
Kanlebere . ...:.-.. | 83 27 12.467
Helene un de... 5421. 1294
| ne Seamen's
| Summe = | 180 0 12-868
180. Be 180» «0° 19-191
Fehler = | — 0.323
cos orice | 60 15 21.237
ne 0... | 42° 82 1.039
Peer. | te 52 2-314
Summe = | 180 O 14-580
| 180° + E. | 180 O 14.561
| peter TY igi

 

 

 

 

It Mma |

a édite

14h!

CPE mn 111

(htt nd a a

 
