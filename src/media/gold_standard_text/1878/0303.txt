it]

4. Kiew—Rostow am Don.

16. Octob. 02 6250297 A 7, November 02 ac 5077 \ ee
90, : 51.01 (halb-Abd.) | Du | in 14. : 50.75 / Rylke in Kiew.
a | 50.85 Ris
99. : 50.94 Rylke in Ro-
oa 50.87 | oy
02 86250233 OF 562 0070

Persönliche Gleichung Rylke—Pomeranzeff = + 0°09. (Aus direeten Bestimmungen -+ 0°13.)
Direete Bestimmungen der persönlichen Gleichung:

Am Anfange der Exped. aus 4 Abenden Rylke—Pomeranzeff = -+ 0°20 \ somit fürMoskau —Kiew

aus zwei Abenden (25. und 27. Juni) : + oi Rylke-Pomeranzeff = —- 0515

aus drei Abenden (12., 14. und 15. Juli) , — ir

aus zwei Abenden (10. und 11. August) „ _ | für Kiew — Warschau + 0.13
aus drei Abenden (25., 27. und 28, August) „ 0.18, 3. ; e -

aus drei Abenden (26., 28. und 30. Septbr.) „ +0. 10 } fir Kiew—Nikolajew ol
aus drei Abenden (2., 5. und 6. Decbr) , + 0.16 } für Kiew—Rostow + 0.13

Die oben angegebenen Werthe sind nur die genäherten Werthe, weil bei der Rechnung die
Gewichte der Chronometer bei der Zeitübertragung nicht berücksichtigt worden sind.

Capitain Rylke.
Pegel im Baltischen Meere.

. Oronstadt: gewöhnlicher Pegel einfachster Construction. Beobachtungen vorhanden für
mehr als 30 Jahre.

. Reval: Pegel einfachster Construction. Beobachtungen gesammelt von 1842 bis 1866
und von 1873 bis 1879. Ich habe noch nicht constatiren können, ob die Be-
obachtungen der zwei Perioden mit einander vergleichbar sind. Die Beobach-
tungen der letzten Periode sind zuverlässig, sie werden im Auftrage des physi-
kalischen Observatoriums ausgeführt.

. Riga—Diünamiünde: Es giebt zwei gewöhnliche Pegel einfachster Construction; der
eine gehört dem Marine-Ressort, der andere ist von der Riga’schen Naturforscher-
Gesellschaft aufgestellt. Beobachtungen gesammelt von 1866 bis 1879. In diesem
Sommer hat das Riga’sche Borsen-Comité einen selbstregistrirenden Apparat
(Mareograph) in Dünamünde aufgestellt. Dieser Apparat ist von Hasler und
Escher in Bern construirt.

. Windau: Pegel einfachster Construction. Beobachtungen gesammelt für die letzten

EN

 

©

Qs

an m

HN

10 Jahre.
\ 5. Libau: Pegel einfachster Construction. Beobachtungen gesammelt für die letzten 13
: Jahre.

Der Erbauer des Hafens in Libau beabsichtigt einen eisernen (cylindrischen)
Pegel aufzustellen. Ausserdem habe ich gehört, dass die Kaiserl. Academie der
Wissenschaften einen Mareographen in Libau, das physikalische Observatorium
einen solchen in Hangö-udd aufzustellen beabsichtigt.

Capitain Rylke.

DDI eee

HR 1 RR Li 1 JR Ni

 
