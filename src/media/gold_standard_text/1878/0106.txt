 

De cette manière, laltitude du Galineiro sera déterminée avec précision du
côté du Portugal, et si l’on considère la position géographique de ce sommet, qui domine
la rade de Vigo, et la proximité d’une des lignes de nivellement géométrique projetées
par linstitut géographique de l’Espagne, on voit, selon nous, l'importance de cette
détermination.

On a déterminé aussi l'altitude de S. Nomedio par rapport aux repères
de Caminha.

On a choisi tous les points nécessaires à la fixation de la ligne hypsométrique
depuis S. Pais jusqu'à Peneda, qui est un des sommets le plus élevés du Portugal.

Enfin, on a terminé les observations qui manquaient encore à l'exécution du
valeul de l'altitude de S. Felix, d’après un nivellement géodésique spécial déjà men-
tionné dans notre rapport antérieur.

Observations de marées.

Dans le but d'obtenir le zero pour les nivellements portugais, on a observé
tous les jours les maréomètres de Villa do Conde. Ces échelles ont été reliées par des
nivellements géométriques à des repères très solides et convenablement éloignés.

Travaux divers.

nes reconnaissances concernant l’organisation des triangulations secondaires ont
avancé d’une manière régulière; et dans ce but on a construit 85 pyramides en macon-
nerie, et, en sus, on a adopté 25 points, qui appartiennent à d’autres constructions.

La position relative de ces sommets secondaires à été déterminée (dans le seul
but d'apprécier les conditions des triangles) au moyen de 3172 rumbs, qu’on a mesuré
avec des théodolites de Troughton très portatifs.

En employant des théodolites plus parfaits, on a mesuré définitivement sur 390
sommets secondaires les angles de 611 triangles et les distances zénithales respectives.

On a fait les calculs concernant les opérations géodésiques exécutées dans la
campagne de l’année antérieure.

Par les motifs présentés dans notre dernier rapport (1877), nous ne rendons
pas compte ici des services variés qui sont à la charge de l'établissement scientifique
sous notre direction, et qui se rapportent directement à la géologie, à la topographie,
à la navigation et à la publication des cartes. Néanmoins nous dirons, en finissant,
qu'une des choses qui à attiré beaucoup notre attention a été l'envoi de tous les pro-
duits de cet établissement à l'Exposition universelle de Paris, où ils ont recu deux
grands prix, recompense de l’ordre le plus élevé.

Lisbonne, le 31 décembre 1878.

Per. da Silva, contra almirante.

 

1

yt iad me ik

nl

al

ro LR

MMM ma 11

nut ih bes a À

3
i
|
|

 
