a mre <r

(Pa FOR |

Amann

am

A ge

PTIT T

27
que la conférence générale prochaine puisse avoir lieu & Munich. Ce désir est sans
doute unanime; en effet, il sait qu'il est partagé par M. le Général Baeyer. Par con-
séquent M. Hirsch propose de prier notre collégue M. von Bauernfeind de s’informer
des dispositions de son gouvernement et de les porter à la connaissance du bureau qui
à son tour serait chargé de faire ensuite les démarches nécessaires.

Cette proposition est adoptée à l'unanimité.

Quant à l’époque, la Commission se borne à la fixer à Pautomme, en laissant au
bureau le soin de préciser ultérieurement la date.

M. le Président propose à la Commission de s'occuper de la nomination de
rapporteurs spéciaux chargés de rendre compte à la prochaine conférence générale de
l'état actuel des travaux dans les différents pays.

Apres s’étre concerté avec ses collègues et s’étre assuré de leur consentement,
le Président propose de distribuer les travaux de la façon suivante :

1. Triangulation, '. 2... .:. Coond Fran
2. Bases : 1 „on mens 2 Commandant Perrier,

qui voudra bien, sans doute, se charger de compléter son premier travail.
3. Calcul de Compensations . . . . . Professeur Peters.
4. Nivellements de précision . . . . . Dr. Ad. Hirsch,
D. Longitudes 60 la von Oppolzer. Ces deux messieurs
6. Latitudes et Azimuts . . . . . . Professeur Brudne. eee a :
t. Pendule „u... 2 00.002 Proescen Bia a
8. Publications de VAssociation . . . . Professeur Sadebeck.
9. Quant aux Maréographes . . . . M. le Général Ibanez

est prêt à s’en charger.

Ces désignations sont ratifiées à l’unanimité par la Commission.

M. le Président fait observer encore à ce sujet qu’il est entendu que les rappor-
teurs désignés ont le droit de demander par des questionnaires des renseignements
détaillés aux délégués de tous les pays qui voudront bien les fournir en temps utile.

M. Pruhns demande la parole pour dire que l’ancien réglement de Stuttgart
demeurera sans doute en vigueur pour la prochaine conférence générale. Quant au
programme, il se permet de proposer le projet suivant:

Projet de Programme
pour la 6” Conférence générale des délégués de l'Association géodésique
internationale.
I. Rapport de la Commission permanente; Rapport du Bureau central.
II. Rapports des délégués des différentes nations sur les travaux exécutés pendant
l’année.
IIT. Des déterminations astronomiques de coordonnées :
a) des longitudes rapporteur M. d’Oppolzer.
b) des latitudes rapporteur M. Bruhns.
c) des azimuts

4*

 
