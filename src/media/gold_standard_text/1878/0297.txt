[YEN we m rr oF

eer mn |

CL ST AT

MLL AWE

NL!

vr A A a

105

Jedes Zehntel hat eine ohngefähre Länge von 276 Meter. Die Vorwärts-Messung
geschah zwischen den Temperaturen 16°—-30° Celsius; bei der Rückwärts-Messung lagen
die Temperaturen zwischen 8° und 24° Celsius.

Fischer.

III. Section des Professor Dr. Börsch.

Nach dem bereits früher ausgesprochenen Plane, die Pegel von Swinemünde und
Amsterdam und die südlichsten Punkte unsres Nivellements, Basel und Constanz, durch
ein zuverlässiges und einheitliches Nivellement unter sich zu verbinden, und dadurch eine
Grundlage für alle weiteren Höhenbestimmungen zu erlangen, wurde im vergangenen
Sommer 1879 von dem Sectionschef Börsch die Linie Angermünde Berlin nochmals
doppelt nivellirt und bei dieser Gelegenheit auch der Fixpunkt ,,Normal-Héhenpunkt
bezw. „Normal-Null“ an der Sternwarte hierselbst mit herangezogen, ohne jedoch den-
selben einseitig als Fundamentalpunkt für die Europäische Gradmessung einführen zu
wollen. Die Berechnung der nivellirten Linie wurde im Winter 1879/80 ausgeführt, so
dass einem endgültigen Abschlusse des Nivellements von Swinemünde bis Berlin nichts
mehr im Wege steht.

Von dem Assistenten Seibt wurde die Linie Frankfurt—Bebra—Bitterfeld noch-
mals doppelt nivellirt und bei dieser Gelegenheit eine grosse Anzahl neuer Höhenmarken
eingesetzt, und ist derselbe gegenwärtig noch mit der Berechnung beschäftigt. Die bei
diesem Nivellement benutzten Nivellirlatten wurden vor, während und nach den Feld-
arbeiten wiederholt auf ihre absoluten Längen geprüft und ergaben die Prüfungen nach-
folgende Resultate. Vor dem Beginn der Feldarbeiten, den 7. Juni, wurde durch Ver-
gleichungen mit den Glasmetern No. 7 und 9 gefunden:

1 Lattenmeter = 1.000343 Schweizermeter,
nach Schluss der Feldarbeiten, den 31. October, dagegen:
1 Lattenmeter = 1.000485 Schweizermeter.

Während der Nivellements im Felde wurden mit einem eisernen Maassstabe die
nachfolgenden Längen ermittelt:

den 5. Juli 1 Lattenmeter = 1.000477 Schweizermeter
„ 17. Ausust 1 . — 1.000558 .
» 20. September 1 5 — 1.000539 „

Auch die für die Linie Angermünde— Berlin benutzten Latten wurden vor und
nach den Feldarbeiten von pp. Seibt untersucht und von dem Unterzeichneten die Berech-
nung ausgeführt, das Resultat ist:

den 18. April 1 Lattenmeter = 0.999976 Schweizermeter,
ot October 1 a = 1.000004 i

Zur völligen Herstellung der geplanten Nivellements-Grundlinien sind ausser ver-

schiedenen Revisionen nochmals doppelt zu nivelliren die Linien: Berlin—Magdeburg,
14

 
