 

18 H. SAINTE-CLAIRE DEVILLE ET E. MASCART.

Le kilogramme adopté comme unité dans ces opérations est le kilo-
gramme de l’Observatoire, « kilogramme conforme à la loı du 18 ger-
minal an III et présenté le 4 messidor an VII », signé « de Fortin ».
Son volume est 48%, 5333 à zéro.

Le kilogramme employé dans les pesées a été le kilogramme de
Regnault, en laiton, dont le volume est 124,65 à zéro. Son équation
dans le vide est

kilog. Obs. — kilog. Regnault — 7"*, 32,

Les poids divisionnaires sont de MM. Collot frères ('); ils sont en
laiton platiné. Leur volume total à zéro est 125 centimètres cubes,
correspondant à une densité égale à 8, ou plus exactement 7,998. La
boite se compose d’un poids de 5oo grammes et de ses subdivisions
jusqu’au gramme. Chacun des poids de cette boite a été comparé au
poids de 500 grammes pris pour unité, lequel à pour équation, par
rapport au kilogramme de l'Observatoire, |

kil, Obs, =='2 >< S008 =- 12%", 41

ll faut remarquer que, dans le cas où les poids marqués et les ma-
tières à peser présentent exactement le même volume, toutes les varia-
tions des pesées tiennent uniquement aux imperfections de la balance :
quand les volumes sont inégaux, comme par exemple lorsque nous
avons enlevé l’eau contenue dans le tube et que nous y avons fait le
vide, les variations sont dues d’abord au changement de densité de
l'air, puis aux imperfections de la balance. On à déterminé celles-ci
fort exactement; quant à la variation apparente des poids, on l’élimine
en ramenant les pesées à ce qu’elles seraient dans le vide, c’est-à-dire
en ajoutant à la valeur de chaque poids le poids du volume d’air qu'il
déplace dans chaque expérience.

On peut souvent annuler toute influence des poids excédants sur la

(‘) Ils sont désignés dans nos cahiers de procès-verbaux sous le nom de poids de la
boite A.

(2) Nous avonsfait toutes les comparaisons nous-mêmes; mais elles ont été recommencées
par M. Broch avec une excellente balance de M. Sacré. Les petites différences qui existent entre

nos résultats et ceux de M. Broch nous donnent une-grande confiance dans cet étalon-

nage. Mais nous avons admis, sans les modifier, les chiffres donnés par M. Broch, comme
devant inspirer plus de confiance que les nôtres.

 

Abd u CIE TRE TE EP

118 411.188

eh

a dau

 

 
