   

SEES TREE

EEE

j
Ki
Fs
hi
IF
1
i

ARR ee re rl

76

unter dem Titel: , Détermination télégraphique de la différence de longitude
entre les observatoires de Genève et de Bogenhausen‘ exécutée en 1877 par
H. Plantamour et C. v. Orff; Geneve—Bale—Lyon, 1879.

Ueber die geodätischen Arbeiten des Jahres 1879 ist folgendes zu berichten:

L,

Im Frühjahre wurden die in den Sommermonaten 1877 und 1878 zwischen
dem Döbraberge in Bayern und dem Kapellenberge in Sachsen ausgeführten
Refractionsbeobachtungen noch erweitert und ergänzt, soweit es zur Fest-
stellung der aus zweijährigen Beobachtungen hervorleuchtenden Gesetzmässig-
keit nothwendig schien. Diese im Laufe des Winters 1879/80 reducirten
Refractionsbeobachtungen sind bereits für den Druck bearbeitet und werden
nächstens der mathematisch-physikalischen Classe der Königl. Bayer. Akademie
der Wissenschaften zur Aufnahme in deren Denkschriften vorgelegt werden.
Die hierauf bezügliche Abhandlung hoffen wir den Herren Commissären noch
vor der sechsten allgemeinen Conferenz zusenden zu können.

. Ebenfalls im Frühjahre 1879 wurde ein den Hohen-Peissenberg umziehendes

Polygon des Bayerischen Präcisions-Nivellements, welches ebenso wie die
Fichtelgebirgsschleife einen grösseren als unter normalen Verhältnissen zu
erwartenden Schlussfehler zeigte, zum dritten Male nivellirt, wobei sich heraus-
stellte, dass dieser etwas mehr als 3™ pro Kilometer betragende Fehler wohl
theilweise der Massen-Anziehung des Gebirgs, mehr aber noch den starken
Steigungen der Poststrassen, welche zu dem Nivellement benützt werden
mussten, zuzuschreiben ist, wie der Assistent des unterzeichneten Bericht-
erstatters, Privatdozent Dr. Haid, in seiner Habilitationsschrift”) zur Genüge
nachgewiesen hat.

. Nachdem auf diese Weise das Bayerische Präcisions-Nivellement zum Abschlusse

gekommen war, wurde die fünfte Mittheilung über dasselbe zum Drucke vor-
bereitet und auch noch im Sommer des verflossenen Jahres gedruckt. Darauf
erfolgte sofort die Vertheilung des Schlussheftes an die Herren Mitglieder
der Europäischen Gradmessungsgesellschaft. Aus diesem Hefte ergiebt sich,
dass das Bayerische Präcisionsnivellement aus 7 geschlossenen Polygonen
und mehreren nach den Nivellementslinien der Nachbarstaaten gerichteten
meist sehr langen Strecken besteht, welche in einer Länge von 2394 Kilo-
meter oder 342,62 geographischen Meilen nach den strengsten Anforderungen
doppelt nivellirt und im Ganzen mit 1597 Fixpunkten ausgestattet worden
sind, so dass durchschnittlich auf 1,5 Kilometer ein solcher Punkt trifft. Von
diesen 1597 Fixpunkten bestehen 276 aus starken Messingbolzen, 1313 aus
wagrecht abgearbeiteten und mit Rinnen eingefassten Steinflichen, 8 aus den

*) „Untersuchung der Beobachtungsfehler und der Genauigkeit des Bayerischen Präcissions-
Nivellements“, München 1880, Akademische Buchdruckerei von F. Straub.

 

hl aan

ran shall du

 
