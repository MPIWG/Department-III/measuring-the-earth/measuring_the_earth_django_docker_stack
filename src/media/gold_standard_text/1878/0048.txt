 

  

ET ENTE RE A

re

FREE ART

EEE

SR eee EN

RO PA TON TEL

El
Di
a
F
et
B
x
tf
fy
i
E
I

hydrographe de l’amiraut
réographe.
voisinage du théatre; comme le
travail, Sir Fitzharding Maxse, accordait tou
le plus grand empressement, j'ai fait dresser deux
sur le bord de la falaise, au moyen desquels on p
différence de niveau par rapport au point astronomique.

REIN RE MR PPS PEN Te PP
== ST ET 7 RESP ES
2 ae rn

40

 

é impériale, afin de choisir sur les lieux l’emplacement du ma-
Nous étions de suite d'accord, que le meilleur endroit se trouvait dans le
gouverneur de l’île, qui prenait un vif intérêt à notre
t ce qui pouvait faciliter notre tâche avec
piliers, l’un au point choisi, et l’autre
ourra déterminer la distance et la

Suivant l'opinion de M. von Hagen les frais pour le forage du puit et pour la
une conduite au-dessous du niveau le plus bas de la mer, seraient tellement
considérables, que nos moyens n'y suffiraient pas. En attendant, on nous à soumi
plusieurs projets pour éviter la conduite au-dessous du niveau inférieur de la mer.
Entre autre, un de M. Reitz lui meme, Yinventeur du maréographe qui promet de
donner de bons résultats. Dans cette supposition on peut espérer voir le maréographe

installé dans un avenir pas trop éloigné.

pose d’

C. Etalonnages.

irer du rapport du Bureau Central, soumis à la Com-

On m'a demandé de ret
Comme

mission permanente à Hambourg, la partie qui se rapporte aux étalonnages.
les membres de la Commission ont déjà connaissance des faits qui y avaient été con-
contrer leur approbation en me conformant à cette demande, et en

signés, je crois ren
suivrai désormais dans l'emploi des

indiquant en même temps le point de vue que je
différents travaux pour la mesure des degrés.
Après que la première Conférence générale, dans le but de rendre tous les tra-
vaux devant concourir à la mesure des degrés aussi uniformes et comparables que
possible, avait accepté et recommandé la méthode de Bessel, j'ai cru, en ma qualité de
Président du Bureau Central, devoir signaler tous les écarts qu'on commettait en prin-
cipe contre cette méthode. Mais, au lieu d'obtenir des rectifications, je m’attirai des
criminations et des attaques qui ont fini par absorber et dépasser mes forces.

Pour alléger ma tache, j’ai résolu de procéder désormais autrement, savoir de
ne pas faire concourir, de ma part, & la mesure des degrés en Europe, tous les travaux
qui abandonnent en principe la méthode de Bessel, tels que les compensations faites
en introduisant comme équation de condition l'accord des bases, des triangles iden-

différents, les comparaisons d’etalons exécutées dans l'air etc. et
ermanente l'adoption

ré

tiques avec angles
de laisser à leurs auteurs le soin de demander a la Commission p

de leurs travaux.
Quant à la base de Braake, j’ai fait exécuter une comparaison de la mesure

de Schumacher révisée par Peters et Andrae, avec les valeurs déduites des bases de

Berlin et de Königsberg.

 

11111186 lu

wi af

ak ily

Io 1

mh es ba Haut artsy mm 01

 
