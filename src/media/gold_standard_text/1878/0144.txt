 

|
I

14 H. SAINTE-CLAIRE DEVILLE ET E. MASCART.

ont bien voulu nous préter. Les divisions du tambour donnent directe-
ment le micron et permettent d’en estimer le dixieme.

Au-dessous des microscopes se meut un chariot qui porte les deux
auges où reposent sur des galets roulants les tubes de platine iridié.

Chacune de ces auges est composée d’une double boite à faces rec-
tangles dont les parois supérieures sont formées par des couvercles
vissés un peu écartés l’un de l'autre, l’augette intérieure étant d’ail-
leurs fixée solidement à l’auge extérieure.

Celle qui contient le petit tube (7) de platine doit seulement recevoir
de la glace : le tube est entouré d'un manchon de cuivre rouge percé
de trous, destiné à supporter le poids de la glace, et le toutest enfermé
dans l’augette centrale, fermée de son couvercle, de sorte que le tube
est refroidi par deux enveloppes concentriques remplies de glace.

Le gros tube (T), de x litre environ de capacité, est enfermé de la meme
manière dans une auge plus grande, portée sur le même chariot.

Les auges sont percées de deux trous par lesquels passent deux tubes,
soit en caoutchouc, soit en fer, avec fermeture hydraulique au mercure,
qui isolent complétement les traits et permettent d'introduire dans
Vauge soit de la glace, soit la vapeur d’un liquide en ébullition, sans
que la glace, l’eau ou cette vapeur puisse toucher le trait. C’est par
cette ouverture, dont on éclaire le fond avec la lumière sur un miroir,
qu'on peut viser les traits des tubes. La largeur des traits est de 5 à
> microns, et les microscopes grossissent 4o a bo fois.

Le gros tube, soigneusement jauge et rempli d'azote pur, doit être
porté successivement à des températures croissantes, depuis le point de
fusion de la glace jusqu’au point d’ébullition de Peau, en faisant pé-
nétrer la glace ou les vapeurs de liquides convenablement choisis dans
les cavités comprises entre les différentes parois de l’auge.

Ce tube communique avec un manometre et sert de thermomètre à
air; sa température est donnée par une pression où par un poids de
mercure. En comparant la distance de ses deux traits avec celle des
traits du petit tube témoin £ constamment maintenu dans de la glace,
on a tous les éléments nécessaires pour calculer le coefficient de dilata-
tion de la matiere.

Ces déterminations pourront toujours être contrôlées d'une manière
absolue et à un moment quelconque, si l’on a le moyen de retrouver le
millimètre qui à servi aux mesures; nous verrons plus loin comment

 

Y
3

MA hia a A Med | MA se

AM hu

ha dl A.

Lili

Lil Lulu

 

 
