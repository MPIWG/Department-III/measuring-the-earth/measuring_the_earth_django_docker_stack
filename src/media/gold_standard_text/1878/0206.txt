|
Y
|

a

PR PPS

RER

    

Re Net

ee

er

Su Sa

ne

Feen

   

TA

EEE EEE

EX
Beet
it
Peis
i
;
i
i
i
Der
a
u

 

14

Ce rapport qui touche la question de la compensation des réseaux de nivelle-
ments et l'influence que l’attraction locale exerce sur ces opérations, provoque une dis-
cussion à laquelle prennent part MMrs. Plantamour, Bauernfeind, Hirsch et Ferrero.

Les délégués suisses insistent sur l'expérience qu'ils ont acquise dans leurs
nivellements conduits à travers les puissants massifs des Alpes et d'après laquelle les
polygones se ferment parfaitement dans les limites voulues, lorsqu'on tient compte con-
venablement des erreurs systématiques dûes soit au tassement soit aux variations des
mires, et sans qu’on soit obligé de faire intervenir la déviation de la verticale.

Ils conviennent sans difficulté que cette dernière constitue une cause de per-
turbation lorsqu'il s’agit de déterminer la différence de niveau entre deux points dont
Yun est situé au pied, l’autre au sommet d’une montagne. Cette différence sera néces-
sairement augmentée par l'effet de l'attraction locale; il n’en est plus de même lorsqu'il
s'agit de fixer la différence de hauteur de deux points situés de part et dautre
d’un massif.

M. von Bauernfeind ayant observé qu'en tous cas les changements des mires
peuvent introduire une erreur systématique qu'on ne peut assimiler aux erreurs fortuites
susceptibles d’être compensées d’après la méthode des moindres carrés, M. {irsch répond
que, pour cette raison, on a tenu en Suisse à déterminer avec tous les soins, au commen-
cement et à la fin de chaque campagne, la longueur des mires employées.

Heureusement celles qui ont servi aux nivellements Suisses ont montré une
constance si remarquable de leur longueur moyenne et une variabilité si iaıble (02207
par mètre) qu'elles ne constituent point une difficulté pour le calcul des polygones
suisses, |

M. Ferrero confirme ce que l’on vient de dire en constatant que les mires ita-
liennes faites par le méme constructeur, M. Kern d’Aarau, ont montré la même stabilité
ainsi que cela résulte des comparaisons faites à cinq ans de distance, soit au bureau
fédéral des poids et mesures à Berne, soit à Florence.

M. le Président après avoir remercié M. von Bauernfeind de son rapport accorde
la parole à M. le Lieutenant-colonel Adan qui donne lecture et fait la distribution du
rapport autographié rendant compte des travaux belges.*)

M. Adan ayant mentionné la nomination de M. le Général Liagre aux hautes
fonctions de Ministre de la Guerre en Belgique, le Président se félicite pour l’Associa-
tion géodésique de ce qu'un de ses membres qui s'intéresse vivement aux pro-
blèmes scientifiques que nous étudions, soit appelé à une haute position qui lui permettra
de favoriser dans son pays les travaux géodésiques.

M. le Président, vu que l'heure est déjà trop avancée, renvoie la suite des
rapports à la prochaine séance. Toutefois il donne encore la parole à M. Sainte-
Claire Deville qui doit repartir demain pour Paris.

*) Voir au Rapport Général pour 1879, Belgique.

 

|
j

 
