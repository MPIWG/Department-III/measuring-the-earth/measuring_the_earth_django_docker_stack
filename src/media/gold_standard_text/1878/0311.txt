MRC

 

ame

at

Loy lim!

‘WL Dana a

Stuttgarter Conferenz von 1877, S. 69 ff.) enthalten, weshalb ich mich auf wenige ein-
leitende Bemerkungen beschränken kann. Das Württembergische Präeisionsnivellement
hatte zugleich den Zwecken der Königl. Eisenbahn-Bau-Commission zu dienen, welche
daher den bei Weitem grössten Theil der Kosten trug; hiervon war die Folge, dass viel
mehr Linien in das Netz aufgenommen werden mussten, als für die Zwecke der Grad-
messung erforderlich gewesen wäre.

Es wurden in den einzelnen Jahren nivellirt:

1868 259 Klm. in 216 Tagen mit einer durchschn. tägl. Leistung von 1,20 Klm.
1869 376 29 29 294 29 3? 19 29 29 29 29 1,28

29

1870-18 Se 4 : à 5 : „.106 ,
871.189: 87 ees tO : 5 he . 5 „Le,
1872 202,2... 6 sé 5 oo „ 569
188 232. ., 64 wae ‘ i a 3 LA .
LSTA EAI eee  . „ > 5 n » 192,
1845 396%, 0 5 st A 5 . i A hl,
1811 280, „lo _ n 5 n ; 5 » Led.
1848 218 60, be. * 5 = 5 1a
zusammen:
2454 „ „LO 5 > 5 s „ Mittelleistungv. 1,44 ,,

Bei der Berechnung der verwendeten Anzahl von Tagen wurde die je zwischen
dem Beginn und Schluss eines Nivellements liegende Anzahl von Tagen berechnet; die-
selbe enthält also ausser den Arbeitstagen noch die Sonntage und Regentage, welche
meist zu andern das Nivellement betreffenden Geschäften (Reinigen der Instrumente,
Fertigen von Abschriften u. dergl.) verwendet wurden. In Betreff der grossen Werthe
der täglichen Leistung in den letzten Jahren ist daran zu erinnern, dass in dieser Zeit
die Beobachtungsmethode dadurch vereinfacht wurde, dass, wenn die Latte auf der
dreieckigen Bodenplatte stand, nur in einer Lage des Fernrohrs beobachtet wurde, und
nur bei der viereckigen Platte in zwei Fernrohrlagen.

Von den obigen 2454 Kilometern wurden nivellirt:

1528 Kilometer einfach in 932 Tagen,
452 5 doppelt in 622
74 . dreifach in 149

29

39

Was die bei dem Württembergischen Präcisionsnivellement benützten zwei Paare
Nivellirlatten betrifft, welche aus der Werkstätte von Kern in Aarau bezogen waren, so
kam das eine Paar bis zum Jahre 1872 zur Verwendung (Nummer 2 und 3), während
das zweite Paar (A und B) seit 1872 benützt wurde. Leider wurde erst im Verlaufe
der Messung ein Comparator zur Verfügung gestellt, mittelst dessen eine Vergleichung
der Lattenlänge möglich war. Aus den im Winter 1878/79 angestellten längeren Be-

 
