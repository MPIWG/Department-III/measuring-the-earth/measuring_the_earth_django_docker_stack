 

 

8 H. SAINTE-CLAIRE DEVILLE ET E. MASCART.
On en déduit :

Proportions. Densité à zéro. Volume.

 

Platine iridié à 10 pour 100....... 99,33 D1 O70 4,603
Mila on exzees. ..... 02: .1.10,23 22,380 0,010
Beam ee ke 0,18 12,000 0,015
RONeBUM..:+........... 2.220,70 12,201 0,008
ler. ...... ...... 2.0.2 %280,00 7,700 0,008
99,90 4,644

Densité à zéro calculée d’après l’analysel....... 2,0, 0:10

Densité à zéro calculée d’apres l’analyse Il...... 21,819

qui concordent parfaitement avec les résultats des analyses.

En fabriquant un pareil alliage avec une telle pureté, M. Matthey a
résolu un problème de métallurgie des plus difficiles et des plus com-
pliqués. On ne peut s’imaginer, à moins qu'on ne connaisse dans tous
leurs détails les procédés si pénibles employés à la purification de l'iri-
dium et même du platine, combien il a fallu d'intelligence, de patience
et de dévouement à la science pour réussir dans une pareille œuvre.
C'est aussi au désintéressement de M. G. Matthey et de ses associés
que nous devons d’avoir pu, les premiers, utiliser une matière dont,
d’après M. Dumas, « la production enrichit l’outillage scientifique d'un
alliage doué de propriétés précieuses >.

C'est aussi l’avis publiquement exprimé par un juge aussi sévère que
bienveillant, M. Stas, le célèbre chimiste de Bruxelles, dont le suffrage
a été un encouragement précieux pour les habiles fabricants de
Londres. Si les membres de l'Association le permettent, je ferai par-
venir à M. G. Matthey le témoignage de leur satisfaction.

L’alliage de la règle géodésique est beaucoup plus dur, plus résis-
tant que le platine. Sa rigidité est telle qu’on ploie difficilement même
une lame de 1 à 2 millimètres d'épaisseur. Il présente alors une par-
faite élasticité ; la lame ne se ploie que sous un grand effort, revient à
sa position primitive, sans changement, à moins que l’angle sous lequel
on l’a ployée ne soit un peu grand.

M. Phillips a bien voulu me donner l'expression numérique de ces
effets, en déterminant le coeflicient d’élasticité du platine iridié de la
règle, coefficient qui est considérable, et sa limite d’allongement per-
manent.

did

uud ua hu bdd) it | ua.

Lab 1 fad i

 

|
|

 

 
