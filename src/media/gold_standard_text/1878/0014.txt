 

1

<
=
=
=
a
=
:

el

6

rad,

sammlungsorte der Commission in den Protokollen verzeichnet zu sehen, und
wir freuen uns nicht weniger der Gelegenheit, den Begründern und Fort-
führern des Unternehmens persönlich unsere Hochachtung bezeugen zu
können. Wir hegen den lebhaften Wunsch, dass Ihre Bemühungen und Ihre
Berathungen auch ferner guten Fortgang haben und mit Erfolg gekrönt sein
mögen. Und wenn ich dann noch einen Wunsch hinzufügen darf, so ist es
der, dass es Ihnen auch ausserhalb der Sitzungen in unserer Stadt wohlge-
fallen möge. Sie finden freilich eine Stadt, die weder in politischer, noch in
wissenschaftlicher Beziehung der Mittelpunkt eines grossen Reiches ist, eine ;
Stadt, die Ihnen weder den Glanz eines fürstlichen Hofes, noch den für

Männer der Wissenschaft so anziehenden Reiz einer Universität zu bieten

vermag, eine Stadt vielmehr, deren ganzes Leben und Treiben sich um eine
Aufgabe ganz anderer Art dreht — aber eine Aufgabe, welche ich keinen =
Anstand nehme, gleichfalls als eine grosse und schöne zu bezeichnen. Denn E
die Aufgabe des Seehandels ist es, unter den Nationen, welche die Meere trennen, =
das einende Band zu knüpfen, und die Aufgabe des Welthandels ist seit
Jahrhunderten eine civilisatorische, indem er fernen Küsten des Oceans
europäische Cultur zuführt. Das ist ein Verdienst, welches der Handel mit
der Wissenschaft theilt; mit noch so verschiedenem Streben erreichen Beide
so vielfach ein gleiches Ziel — und die Stadt, welche, wenn irgend eine auf
dem Continent, den Welthandel zu vertreten berufen ist, wird hoffen dürfen,
dass auch eine Versammlung von eminent wissenschaftlicher Bedeutung in
ihren Mauern sich behaglich fühlen wird. In dieser Hoffnung habe ich die
Ehre, Sie Namens des Senates hier herzlich willkommen zu heissen.

 

Herr Ibanez antwortet mit folgenden Worten:

Hochgeehrter Herr Bürgermeister!

Im Namen nicht nur der permanenten Commission, sondern der Europäischen
Gradmessung habe ich die Ehre, Eurer Magnificenz und dem Hohen Senat
der Stadt Hamburg unseren tiefgefühlten Dank auszusprechen für die freund-
liche Einladung, unsere Jahres-V ersammlung diesmal in Ihrer schönen Stadt
abzuhalten. — Es gereicht uns zur grössten Befriedigung, Herr Bürgermeister,
in der freien Hansestadt Hamburg zu tagen, welche mit Recht so allgemein
geschätzt und geachtet wird, welche nicht nur durch ihre grosse historische
Vergangenheit, sondern auch durch die hervorragende mächtige Stellung,
welche sie noch heute im Grosshandel der Welt einnimmt, allgemeines Interesse

einflösst, und die auch auf dem Gebiete der Geodäsie der Wissenschaft
Dienste geleistet hat.

oi cata, masses di |

 
