 

 

416

degrés; j'ai cité plus haut les plus importants de ces arcs; il serait à Hosmer que la nou-
velle détermination eût un poids comparable.

Or, il est évident que ce poids sera d'autant plus grand que l’arc sera plus étendu.
La principale cause d'erreur est, en effet, l'incertitude sur les latitudes extrêmes, en raison
des attractions locales qui peuvent faire dévier la verticale. Cette déviation et par conséquent
cette incertitude, toutes choses égales d’ailleurs, seront indépendantes de l’amplitude de
Varc, de sorte que Verreur relative qui en résultera variera en raison inverse de cette
amplitude.

A ce compte, en réduisant l’are de 6° à 4°,5, on réduirait d’un quart sa valeur
scientifique; mais, en réalité, ces sortes d’appréciations ne peuvent se traduire par des

“chiffres. Quand une détermination devient deux fois plus précise, est-il vrai que sa valeur

scientifique devient seulement deux fois plus grande? Tous les savants répondront que la
progression est beaucoup plus rapide, que pour avoir deux fois autant de précision ils
devront dépenser beaucoup plus de deux fois autant de peine, et qu’ils ne la regretteront pas.

D'un autre côté, plusieurs membres de la Commission ont émis l’avis que, les frais
généraux restant les. mêmes, la dépense ne serait réduite que d’un sixième.

Mais un examen plus approfondi du devis montre que cette évaluation est encore
très exagérée.

Il n’y aurait aucune économie ni sur la mesure des bases, ni sur les indemnités
d'entrée en campagne, ni sur le transport du personnel et du matériel de France en Amé-
rique, ni sur le nivellement de précision, ni sur l’achat des mules.

En ce qui concerne la mesure des angles, l’économie n’est pas la même, si l’on
veut rattacher la triangulation à la mer, au point dit , l’ancien Phare”, dont j'ai parlé plus
haut, ou si l’on renonce à ce rattachement.

Si l’on veut rejoindre la mer, jonction dont l'intérêt est manifeste, on ne pourra
supprimer que neuf stations, à savoir les cinq stations péruviennes et celles d’Acacana,
Pisaca et Ama. Je ne parle pas des deux stations situées aux extrémités de la base, qui
se trouveraient supprimées également, mais qui devraient être remplacées par deux stations
analogues aux extrémités de la basse de Targui.

Si l’on renonce à joindre la mer, on pourra supprimer en plus Chilla, Mullepungo,
Minas et l’ancien Phare; de sorte qu’on économiserait en tout treize stations.

La durée de l’opération serait ainsi diminuée de deux mois.

Pour les mesures astronomiques, la station principale du sud seroit supprimée,
ainsi que la station secondaire du Cerro Chacas, mais la station secondaire de Purin devrait
devenir principale.

En somme, on aurait quatre stations secondaires au lieu de six; d’où une nouvelle
économie de deux mois.

L'entretien du personnel français pendant un mois montant à 4175 fr., l’économie
en argent serait de 16700fr. Cette évaluation est très exagérée, car le séjour de chaque
membre de la mission ne serait pas réduit de quatre mois, et, en particulier, l'officier
supérieur chef de la mission devrait néanmoins rester un an à l’Equateur. Quant à une

 

|
|
|
|
1

Ao has ash mies heats
