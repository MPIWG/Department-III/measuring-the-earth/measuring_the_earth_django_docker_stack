KA

ba ABI

a

D CRE LL UML ia

ad

sich vonhausaus schon die Ankunft des Herrn Dr. Kınurı in Mizusawa, so daß er
erst am 13. October daselbst eintreffen konnte. Gleichzeitig kam Dr. Naxano dahin,
und ebenso traf das Zenitteleskop zu dieser Zeit ein. Nach den unumgänglichen Vor-
bereitungen konnten bereits im November Breitenbeobachtungen in einer provisorischen
Aufstellung erhalten werden. Doch sind Beobachtungsbücher noch nicht im Central-
bureau eingegangen. (Während des Druckes dieses Berichts trafen indessen solche ein.)

Von der I. E. erhielt die Station Mizusawa durch das Centralbureau folgende
Instrumente:

1. Ein Zenitteleskop von Wanscnare (Fernrohr 130 cm Brennweite,
Oeffnung 108 mm, Reversions- und gewöhnliches Ocular mit 104-facher
Vergr., gewöhnliche und elektrische Beleuchtung des Gesichtsfeldes).
Das Instrument gestattet mit Feldbeleuchtung gut die Messung von
Sternen 8. Größe.
Einen guten Marine-Chronometer nach Sternzeit von Enkuichn in
Bremerhaven, No. (79.
3. Eine Sternzeit-Taschenuhr in silbernem Gehäuse von der Association
Ouvriere de Locle.
4. Einen Barograph von Rıcnarn Früres (Junes Ricnarp) in Paris, nebst
Registrirpapier für 3 Jahre.

5. 20 Stück 4-Volt-Lämpchen, 2 elektr. Handlampen und einen Rheostat.

ID

Diese Apparate nebst 10 Beobachtungsbüchern (2 weitere folgten am 18. 7.,
o nahm Herr Dr. Kınura selbst mit), gingen am 12. Juli vom Bremerhaven über Hongkong
nach Tokio ab (gleichzeitig mit dem japanischen Pendelapparat, siehe weiterhin).
Herr Dr. Kımura, der sich längere Zeit im Centralbureau in Potsdam aufgehalten und
die Instrumente besichtigt hatte, reiste mit derselben Fahrgelegenheit. Die zoll-
freie Einfuhr in Japan war vermittelt worden. Die Ankunft erfolgte programmgemäß
Ende August in Tokio; ein kurzes Telegramm: „Good“ verkündete dem Centralbureau
die glückliche Ankunft. Allerdings stellte sich später heraus, daß sich die obere
Hälfte des äußeren Rohres gelöst hatte, welchem kleinen Uebelstand aber leicht abzu-
helfen war. Im übrigen muß der ausführliche Bericht aus Japan abgewartet werden.
Herr Dr. Kınura erkrankte leider nach seiner Rückkehr nach Japan (siehe vorher),
was auf die Einrichtung der Station verzögernd wirkte.

Die Hülfslinse zur Mireneinstellung wurde nach Herrn Tir40o’s Angabe vom
23.12.1898 auf 100m Abstand der Mire bemessen; die Prüfung, welche auf dem
Terrain des Centralbureaus ausgeführt worden ist, zeigte die correcte Ausführung.

Tschardjui.
(Centralasien.)
Die Station ist von der militär-topographischen Abtheilung des russischen
Generalstabes auf Veranlassung Sr. Excellenz des Herrn Generalleutnants vox Srugexporrr

DE

 
