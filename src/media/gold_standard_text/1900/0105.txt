et rE TROT a TL

ul

i

ae

min

Fi im

99

Herr Schmidt (Baiern)überreicht dem Bureau den Bericht über die bairischen geodäti-
schen Amßeiten (Siehe Beilage A. XII* und XII’) und zu gleicher Zeit den würtembergischen
Landesbericht, den Herr Hammer, Delegirter von Wurtemberg, welcher verhindert war zur Con-
ferenz zu kommen, dem Herrn Secretar hat zukommen lassen (Siehe Beilage A. XUI* und XIII).

Herr Matthias (Preussen) verliest den Bericht über die Arbeiten der Landesaufnahme
in Berlin (Siehe Beilage A. XIV‘).

Herr Albrecht (Preussen) verliest den Bericht über die Arbeiten des geodätischen
Instituts in Potsdam (Siehe Beilage A. XIV»).

Der Herr Secretär erinnert daran, dass er den Empfang eines Berichtes von Seiten
des Delegirten von Griechenland (Siehe Beilage A. III), Mittheilungen über die Dreiecks-
messungen enthaltend, bereits am Ende der zweiten Sitzung angezeigt hat.

Herr Celoria (Italien) legt der Conferenz den Bericht vor über die geodätischen
Arbeiten in Italien seit der Stuttgarter Conferenz (Siehe Beilage A. XV). Ausserdem hat er
unter die Herren Delegirten den Bericht über die Verhandlungen der italienischen geodätischen
Commission in den Jahren 1895 und 1900 vertleilen lassen. Diese Verhandlungen haben
einen besonderen Werth, da darin nicht allein die im Laufe des Jahres, sondern die gesammten,
vom Anfang an von der Commission ausgeführten Arbeiten discutirt worden sind.

Herr Bakhuyzen verliest den Landesbericht der Niederlande. (Siehe Beilage A. XVl).

Herr Albrecht hat mit grosser Befriedigung gehört, dass die Bestimmungen der
Breitenvariation in Leiden angestellt werden. Die bis jetzt durch die Cooperation der Stern-
warten erhaltenen Resultate sind von grosser Bedeutung, und es ist sehr werthvoll die
Breitenbeobachtungen nicht allein auf den internationalen Stationen, sondern auch auf Sta-
tionen unter einem anderen Parallel anzustellen, damit so viel wie möglich die systema-
tischen Fehler eliminirt werden. Er äussert den Wunsch, dass in Leiden die Beobachtungen
noch während einiger Zeit fortgesetzt werden mögen.

Der Herr Präsident macht die Bemerkung, dass man diesen Punkt bei der Behand-
lung von N°. 5 der Tagesordnung weiter besprechen wird.

Herr /saae Winston (Vereinigte Staaten) verliest den Bericht über die in den beiden
letzten Jahren in den Vereinigten Staaten ausgeführten geodätischen Arbeiten (Siehe Beilage
Ae VEL)

Der Herr Seeretär verliest, auf Anfrage des Herrn Winston, in französischer Sprache
folgenden kurzen Bericht über die von der U. $. Coast and Geodetic Survey ausgeführten
geodätischen Hauptarbeiten.

Der Bogen des 39 Parallels (1871—1898) hat eine Länge von 4225 Kilometer,
und. umfasst:

109 Breitenstationen,
73 Azimutstationen,
10 Grundlinien,
39 Telegraphische Längenstationen.
Alle Beobachtungen, Reductionen und weitere Berechnungen sind beendet und das

 
