 

 

78

glaubt nicht dass die Regierung der Vereinigten Staaten jetzt diese Messungen anordnen werde.
Herr Anguiano ist mit Herrn Bassot volkommen einverstanden über den grossen
Werth einer Breitengradmessung in Amerika von Acapulco bis Canada. In seinem Berichte
wird er darüber Einiges mittheilen.
Infolge dieser Discussionen stellt Herr Hirsch einen Antrag in folgender Fassung:

1101 Led, LUN TMT ie a dd
TIES EEE BT SETS gerne

Ft

„Die internationale Erdmessung, nachdem sie mit dem grössten Interesse eine Mit-
N theïilung von Sir David Gill über das Fortschreiten der geodätischen Arbeiten in Afrika
| angehört hat, spricht ihre Sympathie aus für den ihr vorgelegten Plan einer Breitengrad-
messung den 30en Meridian entlang, und äussert den Wunsch die verschiedenen dabei
| interessirten Regierungen möchten durch ihre wohlwollende Unterstützung die Verwirklichung
ll des grossen Unternehmens so viel wie möglich fördern.”

| Der Antrag wird einstimmig genehmigt.

i Der Herr Präsident ertheilt Herrn“ Bourgeois das Wort zur Verlesung seines Berichts

über die in Frankreich ausgeführten geodätischen Arbeiten (Siehe Beilage A. IV*).

Der Herr Präsident fordert die Delegirten auf die Finanz-Commission zu ernennen,
und schlägt als Mitglieder vor: die Herren Zachariae, Tinter und Ferster.

Der Vorschlag wird angenommen.

Der Secretär macht folgende Mittheilungen:

Herr À. Gautier aus Genf schreibt, dass er zu seinem Bedauern aus Gesundheits-
rücksichten verhindert ist den Sitzungen beizuwohnen ;

Herr Contostaolos, Commandant des kartographischen Dienstes in Griekenland, theilt
der Versammlung mit, dass auf Wunsch des Herrn Hartl, welcher verhindert ist der Con-
ferenz beizuwohnen, der Bericht über die trigonometrischen Beobachtungen in Griechenland
(Siehe Beilage A. III) dem Bureau zugesandt worden ist. Dieser Bericht wird dem Herrn General
Ferrero zur Kenntnissnahme für seinen Bericht über die Triangulationen mitgetheilt werden.
4 Der Herr Präsident erklärt die Tagesordnung für erschöpft, und schlägt vor die
| nächste Sitzung Samstag 29 September zwei Uhr abzuhalten.

! Dieser Vorschlag wird angenommen.

D : Die Tagesordnung lautet:

; 1°. Discussion über die Thitigkeit des Centralbureaus im Jahre 1899.

| 2°. Bericht des Herrn Helmert über die Schwerebestimmungen.

‘ 3°. Bericht des Herrn Haid über seine Pendelbeobachtungen.

4°, Bericht über die geodätischen Arbeiten in Rusland.

N 5°. Bericht über die geodätischen Arbeiten in Rumänien.

: 6°. Bericht des Herrn Helmert über die Dreiecksmessungen im Namen des Herrn
Generals Ferrero.

ee

ud

EEE EHEN

A. cst taba a a da la dau a uses +
a

 

| Die Sitzung wird um 12 Uhr geschlossen.

sie
uns vont il

 

 
