wy

ee RT

Mb MEME LT

Tan TI

DOM LU OR CT JL

je 45
sique M. le Prof. VoGzz eut l’obligeance de mettre à notre disposition une lunette convenable :
je tiens à lui exprimer ici mes remerciments.

Les positions moyennes des étoiles appartenant aux groupes VII à I, dont on
avait d’abord besoin, furent envoyées aux stations au mois de Juillet,»celles des groupes
IT et III furent expédiées le 24 Août et le reste suivit au mois de Novembre. A ce
dernier envoi fut ajouté une correction de la liste des étoiles du groupe VIII, que l’on
n’avait pas encore observé.

On avait aussi formé un catalogue des étoiles, situées dans le voisinage de celles
que l’on devait observer et qui pouvaient conduire en erreur les observateurs; cette liste
fut expédiée avec la dernière partie du catalogue des étoiles de latitude.

Enfin les observateurs reçurent en même temps les réductions provisoires aux positions
apparentes de trois paires de chacun des groupes XII à VI (comprenant la périodede Septembre
1899 jusqu'à Mars 1900) qui leur permettent de se former un jugement sur l’accord des obser-
vations. On n’enverra les réductions définitives de toutes les paires d'étoiles qu'après que les
valeurs exactes des positions moyennes et des mouvements propres auront été déterminées. Ce
travail, que le Bureau central à confié à M. le Dr. Cox de Königsberg, pourra être
termine au mois de Mars 1900. Le calcul des réductions aux positions apparentes par
d’autres aides-calculateurs sera pour la plus grande partie terminé aussi vers ce temps.

Les cahiers d'observation furent fournis aux stations par le Bureau central. Généra-
lement le premier envoi contint 10 exemplaires, dont 5 pour les copies; un second envoi
de 25 exemplaires suivit vers la fin de l'année (la station au Japon reçut un plus grand
nombre qui lui permit de faire préparer deux copies); cette quantité suffira aux stations
jusqu'au commencement de 1901. Chacun de ces cahiers d'observation, destinés au Bureau
central et devant servir aux observations d’un mois !), contiendra en détail les observations
de latitude et seulement les resultats de toutes les autres observations secondaires.

Ces cahiers d’observation originaux, après avoir été copiés, doivent être envoyés au
Bureau central aussi tôt que possible après la fin de chaque mois.

La réduction des observations se fait à l’Institut, où pour le moment deux calcula-
teurs spéciaux sont occupés de la partie schématique. de ces calculs. Chaque station a
pourtant le droit de faire aussi elle-même les réductions. =

4.

Les mesures absolues de la pesanteur au moyen de pendules furent continuées par
M.M. les docteurs KÜHNEN et FurRTWANGLER.

1) Il faut donc que les observateurs commencent chaque mois un nouveau cahier, sans
s'inquiéter des combinaisons des groupes. Qu'il soit rappelé ici encore une fois que le changement des
groupes doit avoir lieu exactement aux dates indiquées dans le »Anleilung” p.18, parce que c’est la
première condition d’une codpération des six stations, que chaque soirée on observe les mêmes étoiles
dans toutes les stations. Au contraire c’est une question secondaire, que le rattachement des groupes dans
une station soit satisfaisant ou non.

II 3

 
