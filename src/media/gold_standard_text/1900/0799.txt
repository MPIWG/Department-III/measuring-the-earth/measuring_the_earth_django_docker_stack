tree en |

ET Keen

Mh | ABI

MIR) 1 NV PRIMAL tL) BUG DUC LIU

395

UEBERSICHT ÜBER DIE ASTRONOMISCH-GEODÄTISCHEN ARBEITEN FÜR DAS
POLYGON UM DAS WESTLICHE MITTELMEERBECKEN ).

I. ITALIEN,

Die in Frage kommenden Dreecksketten sind sämmtlich beobachtet und berechnet,
wenn auch nur erst theilweise veröffentlicht ?). Die Verbindung mit dem französischen
Dreiecksnetz bei Nice und mit den tunesischen und algerischen Dreiecken in Nordafrika
ist ebenfalls bereits vorhanden.

Astronomische und zwar insbesondere Larvaor’sche Punkte sind in genügender Anzahl
etablirt worden; sie sind im allgemeinen auch günstig vertheilt. Es sind dies: Genova,
Firenze (Arcetri), Roma *), Napoli *), Castanéa und Palermo. An Stelle von Firenze (Arcetri)
kann auch Livorno treten, fiir das zwar bis jetzt nur Breite und Länge bestimmt sind, eine
Azimutbestimmung jedoch für die nächste Zeit vorgesehen ist). Die Längendifferenz Nice-
Genova ist ausreichend, wie schon daraus hervorgeht, dass, nach den bereits vom Central-
bureau ausgeführten Rechnungen, die Larracr’sche Gleichung für die Linie Nice-Genova
einen Schlussfehler von nur — 3”.4 hat. Wünschenswerth dagegen ist, dass für Castanda,
wo nur Breite und Azimut beobachtet ist, auch die Länge bestimmt wird. Da Castanéa bis
jetzt Ausgangspunkt für die Berechnung der geographischen Coordinaten in ganz Süditalien
gewesen ist, so wäre seine Bestimmung in Länge wohl auch an und für sich angebracht,
Von sehr grossem Werthe würde es aber sein, wenn die directe Längenbestimmung Palermo-

Carthage, die für den Schluss des Polygons in Länge noch fehlt, beobachtet werden könnte
(siehe weiter unten). -

Il. Tunis unp ALqimr.

Die Winkelbeobachtungen sind beendet. Zur Möglichkeit der Ausführung der defini-
tiven Berechnung fehlt nur noch die Neumessung der Grundlinien bei Oran, Blida und
Bone mit dem Brunner’schen Basisapparat und die Messung einer Basis bei Tunis, deren
Ausführung bereits in Aussicht genommen ist.

1) Dieses Material ist von Prof. Börsch zusammengetragen worden.

2) „Processo verbale delle sedute della Commissione geodetica italiana, tenute in Milano nei giorni 5-e 6
Settembre 1895 e nei giorni 26, 27 e 28 Giugno 1900. Firenze, 1900.” S. 43 und Tafel I. :

8) Rom ist freilich noch kein eigentlicher Lapnacn’scher Punkt, da das Observatorium auf dem Capitol, fiir
das die Linge bestimmt ist, keine Azimutbestimmung besitzt. Indessen soll der dicht bei Rom gelegene trigonometrische
Punkt Monte Mario, fiir den die Breite und das Azimut schon mehrfach beobachtet sind, nochmals ganz neu, auch in
Lange, astronomisch festgelegt werden, da er als allgemeiner Ausgangspunkt fiir die geographischen Coordinaten in ganz
Italien in Aussicht genommen ist. Vergl. „Processo verbale ete. 1895 e 1900. Firenze, 1900,” S. 16. 5

4) Das Azimut auf der Sternwarte Capodimonte, wofür die Länge bestimmt ist, rührt noch yon Brroscut aus dem
Jahre 1820 her. In der Nähe (3.6 Km. entfernt) liegt auch der Punkt Pizzofalcone, für den Breite und Azimut bekannt ist.

5) „Processo verbale etc. 1895 e 1900. Firenze, 1900.” S. 15.

 
