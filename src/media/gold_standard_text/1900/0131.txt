ee

FO | ei |

‘In each case the results of the group have falsified predictions based with confidence
on the results of all India. It is worth noting that the 2’.65 in the last line but one of
the foregoing table (derived by multiplying 1.19 by the cotangent of the latitude)
is only 0.35 less than the deflection of 3” derived independently from the longitudes
of India.

16. The contradictions which we now face, cause us to consider the following
questions:

(a) Can any inequality in the distribution of matter in the immediate locality of
Kalianpur he conceived, that can deflect the plumb-line 3.5 to the south west at Kalianpur
itself, and yet allow the mean deflection, resultant from eight surrounding stations in the
vicinity, to be half a second to the north-east?

(6) Can there exist any external source of attraction affecting the plumb-line at every
station of the group, and rendering the mean determination of the deflection in error by 4”?

Of recent years we have been perhaps too apt to attribute differences between
astronomical and geodetic values to mere local deviations of gravity and to regard
them as due to local irregularities in the distribution of matter in the immediate
neighbourhood of the stations of observation. The method of treating these differences by
minimum squares can only be justified on the ground that they are purely local and
accidental, and its practice has tended to strengthen that belief. The discovery moreover
that deflections of the plumb-line occur in flat unbroken plains, and the theory which that
discovery necessitated, namely that these deflections are due to invisible subterranean causes,
have also helped of late years to give rise to the idea, now generally prevalent, that local
attractions obey no explicable law and .that no result however contradictory need excite
surprise. It is only in the presence of some enormous visible mass, such as the Himalayan
Mountains, and when large constant deflections of gravity occur, that an external source of
attraction affecting large areas is admitted, and that the method of minimum squares is
considered inapplicable.

The group of observations at Kalianpur has necessitated a review of the geodetic
position in India, and in answer to the two questions propounded at the beginning of this
paragraph, I propose now to show that the evidence, of which we are in possession, tends
to indicate:

First, that the Kalianpur group gives the most probable value of the deflection
of the plumb-line at Kalianpur with reference to the mean direction of gravity in central
India, apart from external sources of attraction affecting all Central India.

Secondly, that the existence of any inequality in the distribution of matter in the
immediate locality of Kalianpur, such as could produce a deflection of 3”.5 to the south
west, is improbable.

Thirdly, that the excess of negative values of (O—C) in latitude which prevails
all over India cannot therefore be explained on the theory, hitherto undisputed, that the
initial latitude of Kalianpur is too great.

16

 
