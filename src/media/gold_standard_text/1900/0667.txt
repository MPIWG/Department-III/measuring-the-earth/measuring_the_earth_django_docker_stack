Ser RE

HAMA ILL Ad

anal

‘iui EU

Wier LON RAIA LAN 0 LL Len

261
%
ihn auch für N°. 1 und 2 angewandt, und in der That dürfte das ziemlich zutreffend sein.
Bei den Srterxeor’schen Pendeln ist der Faktor bekanntlich 1.5; Bary fand selbst fir
Pendel aus Cylindern mit kugelförmigen Gewichten 1.9 bis 2, aber die Cylinder waren dabei
stirker als fiir N°. 1 und 2 (nämlich 2 und 4" gegen !/,”). Da nun überhaupt auf einige
Zehntel nichts ankommt, so habe ich zur Bequemlichkeit 1.6 angewandt, also 0.6 der schon
angebrachten Reduktion nochmals beigefügt. Der Betrag dieser Korrektion lag zwischen 3.76
und 4.03 Schwingungen, seine Anbringung hätte also mit Rücksicht auf die Unsicherheit
der Messungen überhaupt unterbleiben können.
Im folgenden sind die so verbesserten Einzelergebnisse auf den Stationen aufgeführt.
Zugleich sind geschätzte mittlere Fehlerquadrate beigefügt, nach der Formel

W=D+2)+3)+4+6),
woraus M in Schw. erhalten wird und die Terme (1) bis (8) die Beiträge der einzelnen Fehler-
quellen bezeichnen:
(1) entspricht der Unsicherheit der Vergleichung von Zähluhr und Chronometer,
im Mittel der aufeinanderfolgenden Einzelbeobachtungen (meistens 11). Es wurde (1) = 18: (@")?

gesetzt, falls bei der Berechnung nur die Anfangs- und Schlussvergleichung benutzt sind;
wurden noch innerhalb liegende benutzt, so ist gesetzt:

(1) = 18: 1.5 (@");
i ist das Intervall zwischen den dussersten Vergleichen in Stunden. Für Port Jackson wurde
(1) 4-fach genommen.

(2) entspricht der Unsicherheit wegen des unregelmässigen Ganges der Chronome-

ter. Im allgemeinen wurde gesetzt:

(2) = 36 : uw (2),
worin ~ die Anzahl der Chronometer ist. In Paris, wo eine Sternzeit-Pendeluhr als Ausgang
diente, ist nur !/,, des Werthes der Formel genommen; während in Rio de Janeiro am 10.
und 11. Aug. 1820 wegen starker Differenzen das 4-fache angesetzt wurde,

(3) ist der Antheil aus der Zeitbestimmung und wurde im alleemeinen gleich '/,,
bei Rawak und Falkland gleich 1 genommen.

(4) ist der Antheil aus dem Einfluss des Lagers bei verschiedenartigem Aufsetzen ;
er wurde zu 1 geschätzt. Es erwies sich nöthig, (4) bei jedem Einzelergebnis einzuführen.

(5) endlich stellt die Antheile wegen des Mitschwingens der Konsole, wegen der

'Veränderlichkeit der Pendel und wegen der Unsicherheit des Temperaturkoefficienten dar;

für die Temperaturdifferenz T zwischen Paris und der Station wurde der Antheil zu (0.02 Ty?
angenommen. Die Antheile (5) sind erst bei den Stationsergebnissen für das Mittel der
Pendel in Ansatz gebracht.

Bei der Verbindung der Einzelergebnisse wurde es berücksichtigt, wenn einzelne
Fehlerursachen konstant wirkten, was bei (3) zweimal eintrat.

Die Pendel N°. 1—3 sind hier mit römischen Ziffern bezeichnet.

33

 
