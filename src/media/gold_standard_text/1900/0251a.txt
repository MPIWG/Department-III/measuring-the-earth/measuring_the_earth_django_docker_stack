 

Nivellement général de la France. - Réseau fondamental.

 

 

 

“DIAGRAMME FIGURATIF DES CIRCONSTANCES D'EXÉCUTION DU NIVELLEMENT. _ COMPARAISON DES RÉSULTATS DES DIVERSES OPÉRATIONS.
CAMPAGNE 1884. —- JET 2° BRIGADES.

 

 

 

 

 

Numéros des repères principaux :

 

 

 
   

Lettres indicatrices des sections :

 

 

> Versailles,

 

Profil du cheminement :

 

 

 

Longueur nivelée par la brigade depuis le début de la campagne:

  

 

  

 

  

 

 

 

  

 

 

 

 

 

 

  

 

 

 

Température moyenne des deux mires :

 

 

 

fi rh er
Dates des opérations: UIN>-MAt> JUILLET sk |

      

 

 
   
 

  
  
   
  

   

     

 

 

 

 

 

 

 

 

Variation mötrique moyenne de longueur des deux mires:

 

 

 

 

 

 

 

 

 

  
 

 

 

 

  

 

 

   

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Ecarts ENTRE LES ALTITUDES MOYENNES

 

déduites des opérations primitives

 

   

 

ET LES ALTITUDES RESULTANT DES DIVERSES OPERATIONS
ou, combinaisons d’opérations ci-après:

 

 

 

4° Opérations du Chef de brigade.
Opération d’aller.

 

 

 

 
 

 

 

 

 

 

 

 

   
   

 

Moyenne entre cet aller et le retour correspondant. ——

 

 

  

 

 

 

 

 

 

 

 

 

   

  
 

 

 

 

    

 

 

 

 

 

2° Opérations du lecteur.
Opération d'aller.
Moyenne entre cet aller et le relour correspondant. ,

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

3° Moyenne des deux opérations d'aller du Chef de brigade

 

 

 

 

 

 

 

 

 

et du lecteur. Vérifications.

 

 

 

 

 

 

 

Opons ou Vérifons admises pour le calcul des altitudes définitves

 

 

 

D
<
FD
2
a
om!
©, |

 

\

 

  
  

K

ax
D

Opérations primitives ayant uniquement servi à déterminer
a différence totale de niveau pour un tronçon de ligne.

DRE RCE

À No]
N

SRE

RS

N

NUN

 

N

N

NN

N
N

N

NN

 

N

Vérifications ayant servi, dans le cas précédent, à fixer les
altitudes définitives des repères intermédiaires.

eS =
N

NUN
NS

=
, EUR
} De 5 STERN

N

PS
Le
5

 

sa
E
N
=
EN

/

 
   
     

 

 

 

 

 

 

 

 

 

 

 

 

 

 

(
|

N

18

Q

\

| \
À

A
:
REE
LL I

 

 

 

 

 

 

Époque des vérifications:

 

ae

Aue

 

 

 

 

 

 

zen

 

 

 

 

 

 

ja

 

 

 

 

 

 

 

 

 

 

 

 

PL1.
1°: BRIGADE a | père BRIGADE
° 45 0 10 15 30 : ‚20 3040 50 60
1 en Ih Hier 1 il | I | ji
| GHL te DGH Fee
| 1 |
| | | ad
| GK Altitudes
200m
100
42 0
[ Températures
i | AL 590
ee os ab
CAL AG
LM LLLLE ALLEL PAP TRAIT
718 40.22 24 zıl I ee. n | we 212 al 2
pt: aes > Se oie Variation
| 4 ie | |du métre des mire:
| ! fe Fe a ere
| N a al 1. 20cm
I. en ZA + 10
AZ 7 LA 0
| |
i = É au
T za |
--1- “NIVEAU Ne & 4 -———-—-——| Ei==2 >
| Mines) No fe: Mines Ners0per 2 Discordances
| | | | | | en centimètres
de Pe et Ohu : - M" Lauren. ,Gonduet' des Ponts-et-Chaussées, Chef }.de.brigade|-— |---|.)
hef de brikade! iG | | | du Cher! Pl Ie ae en
oS ZZ ne 0
à DES
| _ 20m
« des Pl et|Che}:} À
|
7 : 7 Sr -| | ar yom
| HL) 7 A :
= |
er
1 a cm
Mir
| 16m
| N
| ; - +15
“il | |
{ | E
21 2 |
| |
| | + 10
an nn + |
saul eee =
|
! es
we E
J ns primitive 3 = 0
LINEN
TAT
5

 

 

 

 

 

 

 

 

 

 

 

 

 

 

À er 7 0m |
See
Imp. J. Marchadier & C'*, Paris.

 
