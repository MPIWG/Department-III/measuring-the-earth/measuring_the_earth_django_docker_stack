 

id
i

So EE EE ES ORI

pere

RE EEE NS OTIS

SAE SS eS EN

292

Pag.

Observation de M. Albrecht sur les déterminations de latitude à Leyde . . . . te 47
Rapport de M. Winston sur les travaux géodésiques exécutés aux Etats-Unis dans le done der-

nières années (Voir Annexe A. XVIT) . . . . : : : AT
Lecture en francais par M. le Secrétaire du résumé des ara géodésiques cent par le U. S.

Coast and Geodetic Survey . . ak : . . 47—48

M. Oudemans présente une carte jan les song won en ala be a a . 48—49
Proposition de M. Bouquet de la Grye de designer M. le prof. Darwin comme a sur

les marégraphes. Cette proposition est adoptée. . . . 49
Proposition de M. Helmert, lu par M. le Secrétaire, cn ee dane cle

édition de la Bibliographie géodésique . . 2 Be 49
Propositions relatives à la désignation de la ville où se den A en ae ee 49

Discussion et projet de vote concernant l’invitation aux observatoires de pr endre part aux obser-
vations pour la détermination de la variation de la latitude. La a de M. Bakhuyzen

est Adoptee. is) ae . 49—50
Remerciments exprimés par ae de dr ff au re aa eh à M. i Doseadout

de la République. . . : 50
Remerciments de M. Celoria a An anelan eine 3 ancaise a a M. M. bes rier et AL o1
M. Foerster au nom de la conférence exprime les meilleurs voeux pour Ja santé de M. Faye et

temercia le Seenetamen m Nie ne ne ie ne een ee 51
Oldture de la Si Conference Senerale, ni ic som he + er QU Ne or 51

Sitzungsberichte der Verhandlungen der dreizehnten allgemeinen
Conferenz der internationalen Erdmessung, abgehalten vom 25 September bis
6 October 1900 in Paris.

röfnaner Bimuae Doewwis 2 7 0868 1000 ce ae ee ers et

Liste der Delegirten und Eingeladenen. . . . 55—57
Eréffnungsrede 8. E. des Herrn Minister des Mertlichen Unit and den schünen usé, . 58—59
Antwort des Herrn Prof. Foerster im Namen der fremden Delegirten . . . . . . . . . . 59
Antwort des Herrn Faye Präsident der internationalen Erdmessung . . 59—61
Die Herren Prof. Foerster, General Bassot, Prof. Darwin, General von Stubonden ff aa Boat

de la Grye werden zu Vice-Präsidenten ernannt. . . - a nee erh 60
Bericht. des provisorischen Secretärs Herrn v. d. Sande Babee | Sure hens cee ea eee ie ee Oe
Wahl eines ständigen Secretärs . . . Poe ye ey 69
Herr v. d. Sande Bakhuyzen wird soll, er ml ies hae die ANG Le de due 70
WPagesordnung, der zweiten SUZUNE wie ks Siem Ans ee we 70
Zweite Sitzung Mittwoch 26 September 4900. °. „2. „2 ste la len en + . 71—78
Verlesung des Protokolls der ersten Sitzung und geschäftliche Mittheilungen. . . . : : 71
Kurzer Bericht der Thätigkeit des Centralbureaus in den beiden letzten Jahren von Kern bios

fessor Helmert. . . 72—73
Bericht des Herrn Prof. Albr Bet über oh inter nationalen Polkchen heist (Sehe. Di B. v). 74

Mittheilung des Herrn Dr Gill, Director der Cap-Sternwarte, über eine Breitengradmessung in Afrika. 74—76
Discussion von den Herren Hirsch, Oudemans, Bassot, Winston und Anguiano über diese Mittheilung. 76—78
Antrag des Herrn Prof. Hirsch über diesen Gegenstand, welche genehmigt wird . . . . 78
Bericht des Herrn Bourgeois über die in Frankreich ausgeführten geodätischen Arbeiten. (Siehe

eh enge

 

|
7
!
1
|

reset

 
