end

VS rt TER TT |

{AW Aik

FU DOS | (BURGH Um | Tem Fett

229

quelles les latitudes et l’azimut de plusieurs points ont été déterminés pour lesquels les
angles géodésiques sont également mesurés. Pour les latitudes de ces deux stations on a
trouvé, en employant les deux méthodes des distances zénithales et du premier vertical, les
valeurs suivantes:

Brigue ® —46° 19° 95”,78, ce qui combiné avec la valeur géodésique donne pour la

déviation eo cn 508
Iselle ®— 46° 1225",70, ce qui combiné avec la valeur géodésique donne pour la
deyaation .... oe am a. 5 0
De méme on a mesuré les déviations en azimut:
pour les directions Brigue-Rosswald . . . . — 2,95

Brigue-Birgischwald . . . —_ 8",42
Iselle-Gennina  . 4:2 2,5 11,55

Je mentionne à cette occasion que notre observateur à Brigue a constaté, comme
on l’a fait à Potsdam, des variations diurnes de réfraction, qui donnent pour la latitude un
maximum dans la nuit et un minimum vers le matin, et qui font supposer des inclinai-
sons des couches atmosphériques, changeant avec les différences, variables suivant les heures,
des températures au-dessus du Rhône et sur les versants de la vallée.

D’après un rapport que je viens de recevoir, un ingénieur est actuellement occupé
à déterminer la pesanteur à Brigue et à Iselle et profitera de chaque jour d’interruption des
travaux pour la déterminer également à l’intérieur du tunnel. $i le temps ne lui permet plus de
la déterminer aussi sur un point du sommet, cette opération sera faite l’année prochaine.

Les mêmes recherches sur les déviations de la verticale et sur la pesanteur ont
été poursuivies dans un certain nombre d’autres stations de la Suisse, ainsi, en comparant
les latitudes et les azimuts astronomiques aux coordonnées géodésiques, on à pu établir les
déviations suivantes :

Station. altitude. aD oA
Zugerberg 981M.+ 4%61 —
Stanserhorn 1845 +1704 —
Brienzer Rothhorn 2348 3/,89 oe
Bühl + 137,92 —
Moudon — 2,48 + 1”,61.

On peut remarquer que ces nouvelles déviations s’accordent, d’une maniére trés
satisfaisante, avec celles que nous avions auparavant trouvées dans les mêmes régions de
notre pays, et qui sont en partie déjà publiées. Non seulement l'influence générale des
Alpes se manifeste partout, mais encore la supposition d’un vide relatif qui se trouverait
au dessous de cette puissante chaîne se confirme de plus en plus; à mesure que nous
réussirons à multiplier ces études de déviations, non seulement sur les deux versants des
Alpes, mais encore à des altitudes considérables, il deviendra possible de mieux se rendre
compte de la situation et des dimensions approximatives de ce vide souterrain, ce qui sera

 
