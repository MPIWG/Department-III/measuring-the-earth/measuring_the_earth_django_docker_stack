 

i
|
k

GE PANNE SAT DAC EE ARE ea

na

ER PORN ENT JEAN A Ian nr

I}
il

88.

mules de réduction. La première question que nous nous sommes posés était de savoir
quelle était l'influence d’un petit déplacement de l’axe d’oscillation parallèle a lui-
même, le pendule étant en repos. (Avec un pendule muni de couteaux cette expé-
rience n’est pas possible, à moins de changer le pendule.) Nous avons trouvé qu’un
déplacement parallèle de 0.02 mm, faisait varier le temps d’oscillation du pendule
autrichien lourd, avec poids lourd en haut, en moyenne d’une unité de la 7° décimale.
En se servant du pendule à demi-seconde cette variation se montre déjà quand
le déplacement n’est que de 0.01 mm. (Cette variation n’est linéaire qu'entre des
limites déterminées.) Ces expériences ne peuvent donner un résultat certain que
lorsque pendant l’oseillation du pendule toutes les conditions restent invariablement
les mêmes. Cette invariabilité des différentes conditions peut être obtenue d’une
manière suffisante, exception faite pour celle de l’air ambiant, ce qui ne laissait
= d’être gênant, puisque dans, les réductions et dans les formules, qui servaient

4 déterminer l’influence d’un déplacement du prisme, il a toujours fallu faire entrer
comme deuxième quantité inconnue |’influence de la densité de l’air. C’est pourquoi
nous avons pour le moment interrompu les expériences, jusqu'à ce que nous ayons
pu déterminer d’une manière certaine la constante de l’influence de la densité de l’air
sur le temps d’oseillation de chaque pendule. Nous avons déjà depuis longtemps
chargé notre mécanicien de construire un appareil destiné à cette détermination,
mais étant surchargé d’autres travaux il n’avance que très lentement.

En attendant nous avons déterminé encore une fois la longueur du
pendule à seconde, au moyen du pendule à demi-seconde muni de ses propres
couteaux, puisque les déterminations faites avec ce pendule méritaient moins de
confiance que les autres. Tous les résultats (obtenus jusqu'à présent avec les
différents pendules sont inscrits dans le tableau suivant; pour chaque pendule
on s’est servi d'au moins deux paires de couteaux, et à l'exception du pendule
à demi-seconde tous les pendules ont oscillé sur deux supports différents; les
expériences ont été faites à des températures élevées et à des températures basses.

Longueur du pendule de Nombres des

seconde au lieu d'observation déterminations
Pendule à demi-seconde...... 994.229 mm. 12
Pendule leger autrichien. . . . . . 233 4
Pendule lourd autrichien...... 229 4
Pendule Ttaliena <j.) 237 20
Pendule de Vinstitut géodésique. . 230 12

(L’erreur moyenne d’une détermination est d’environ + 5 z.).

On n’a pas apporté à ces longueurs quelques petites corrections dont la valeur
exacte n'a pas encore été déterminée définitivement, mais qui ne dépasseront pas
quelques microns, et qui en partie se compenseront réciproquement.

 

 

ai
i
3
4
i
ti
i

A nbachihtsthanerbidiccirrserpbiii

state be de
