 

he
EEE NT

TEL LE
Ra AU EU TA

an

   

di eins anak ei im Ar mide a od ‘
ETT PC TRE TUR ART OBE LLG OTR Tr

TROISIÈME SEANCE |

Samedi, 29 Septembre 1900.

Présidence de M. Darwin, Vice-président.
Sont présents:

I. Les délégués: MM. Aldrecht, Anguiano, Bakhuyzen, Bassot, Bodola, Borsch, Bouquet
de la Grye, Bourgeois, Bratiano, Celoria, Faye, Fwrster, Guarducei, Haid, Helmert, Hewelink
Hirsch, Lallemand, Matthias, Mohn, Nagaoka, Poincare, Rimniceano, Rosen, Schmidt, v. Stuben-
dorf, Tinter, Valle, Vigano, Westphal, Zachariae.

IT, Les invités: MM. Backlund, Benoit, Bouillet, Caspari, Chappuis, Deslandres, de
Saint-Arroman, Gautier, Guillaume, Gore, Hatt, Lacombe, le prince Roland Bonaparte.

La séance est ouverte à 2 heures 1}.

Le procès-verbal de la deuxième séance est lu et adopté après une remarque de M. Fwrster.

M. le Secrétaire annonce 1°. la réception d’une lettre de M. le Ministre des affaires
étrangères de Prusse, nous informant que M. le general von Oberhoffer est empêché par
son service d'assister à nos séances, et que M. le lieutenant-colonel Matthias présentera le
rapport du Landesaufnahme, 2°. que M. le professeur Nagel est empêché, par l’état de
sa santé, de venir à Paris.

M. le Président ouvre la discussion sur le rapport du directeur du Bureau central.

M. Foerster fait remarquer que M. Æelmert indique dans son rapport que les obser-
vations de latitude ont été interrompues provisoirement à Potsdam, parce que l’on avait
reconnu que les conditions n'étaient pas favorables à une réfraction normale dans les environs
de Potsdam. Il demande 4 M. Helmert de vouloir bien indiquer aux délégués quelles sont
ces conditions défavorables qu'il a en vue.

M. Helmert répond comme suit:

Pendant la période d’une année on détermine la latitude au moyen de différents
groupes d’étoiles, dont deux sont observés, autant que possible, pendant chaque soirée.
Admettant, ce qui est bien probable, que la latitude n’ait pas changé dans le courant de
la soirée, la comparaison des deux résultats d’une même soirée fait connaître la différence

 
