   

263
the machine now employed for the same purpose. An account of a Tidal Indicator was
furnished to the Association in 1898. Since then one has been established at San Francisco,
so that there are now three in actual use, the other two being at New York and Phila- |
delphia. They all work satisfactorily and furnish very useful information to the maritime
community.

HYDROGRAPHY.

an md, mA iin Rm fk

The principal work of the Coast and Geodetic Survey is the execution of hydro-
graphy. Fifteen vessels are occupied in continual surveys, and their work covers, on the
one hand, the Eastern Coast and Porto Rico, and on the other, the Western Coast and
Alaska. Mention is not made here of Hawaii and the Philippines, in the former of which
work has been done with very beneficial results to commerce. It will thus be seen that
the hydrographic work has been extended into new fields, through the necessity for
an early development of the commercial facilities of our recently acquired insular ter-
ritories. ; |

A party on the steamer Buaxe inaugurated the work in Porto Rican waters shortly
after the close of the Spanish War, and during the first season developed the approaches
to the larger part of the southern shores of the Island. The work developed an excellent
harbor at Port Jobos that had not heretofore been noted. During the second season, closing
in June of the present year, the Brake resumed work in the vicinity of Culebra Island,
assisted by a party on the schooner Haeru. During this season Point San Juan, on the
Island, was connected by the hydrographic triangulation with the cable station at St.
Thomas. In the coming season this work will doubtless be extended to the Port of San
Juan, fixing the relative position of all the Islands in the two groups.

The steamer PATHFINDER, that had been built and put in commission in the spring
of 1899, was engaged during the following winter in an examination of a number of the
harbors in the Hawaïian Islands and the passages between them, basing the work upon
the excellent trigonometrical surveys which had already been executed by the Hawaïian
Government.

The discovery of the gold field in the Klondike Region, Alaska, and British
Columbia, had necessitated an examination of the many mouths of the Yukon River, in

Ah mi

search of a practicable entrance for ocean-going steamers. The later discovery of gold fields |
at Cape Nome greatly enhanced the volume of work urgently demanded in the Bering |
Sea region. The steamer Parrurson was therefore despatched to the Bering Sea in the |
summer of 1899, and cooperated with the parties on the small vessels engaged in the j
development of the mouth of the Yukon River. The result of these operations is the deli- À

neation of the edge of the Great Yukon Bank, from Cape Romanzof to Stewart Island,
and the determination of the depth of water in the channels through this Great Bank into
the Yukon, clearly demonstrating that there is no available channel by which ocean steamers
can enter the Yukon River. The work in Bering Sea, during the present season, is being

 
