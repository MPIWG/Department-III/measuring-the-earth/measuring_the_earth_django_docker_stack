 

 

a Sea ee ER Eee

PE D D RS ENS

i
i
fl

Es

70

Inzwischen haben wir nochmals die Secundenpendellange mit dem
Halbsecundenpendel und den zugehörigen Schneiden ermittelt, da die Be-
stimmungen mit diesem Pendel den übrigen gegenüber das geringste Gewicht
besaßen. Im Ganzen haben wir bis jetzt mit den Pendeln folgende Resultate
erhalten, dabei sind für jedes Pendel wenigstens zwei Paar Schneiden in
Anwendung gekommen; mit Ausnahme des Halbsecundenpendels haben alle
Pendel auf zwei verschiedenen Consolen geschwungen, und die Versuche
sind sowohl bei hoher als auch bei niedriger Temperatur ausgeführt worden.

Länge des . Anzahl
Secundenpendels der
am Beobachtungsort Bestimmungen
Halbsecundenpendel; 2 8.2. 2a. 994.229 mm 12
Leichtes österreichisches Pendel . . 2893, +
Schweres 5 = at 229, 4
Italienssches Pendel, hou, Data 20
Pendel des Geodätischen Instituts . Do 12,

(Der mittlere Fehler für eine Bestimmung beträgt etwa # 5 u.)

Bei diesen Werthen sind einige kleine Correctionen, deren genauer
Betrag noch nicht feststeht, jedoch höchstens einige a erreicht, und die
sich zum Theil gegenseitig compensiren, noch nicht angebracht worden.

Combinirt man das leichte österreichische Pendel mit dem etwa

 

. : A, Wis A,
gleich schweren Halbsecundenpendel nach der Formel L= Nee welche

für zwei Pendel von gleichem Gewicht, verschiedenem Schneiden-Abstand 4
und der reducirten Schwingungszeit = gilt, so erhält man L = 994.234.
Mittelt man die übrigen Werthe ohne Rücksicht auf die Anzahl der Be-
stimmungen, so erhält man 994.2320. Dieser Werth ist vielleieht für die
Secundenpendellänge etwas zu klein, weil das Halbsecundenpendel und das
schwere österreichische Pendel kleinere Werthe als die übrigen ergeben,
und bei. richtiger Combination der Werthe eine Extrapolation erfolgt,
vergl. Heruerr, Beiträge zur Theorie des Reversionspendels, Seite 87. Mittelt
man dagegen die Werthe nach der Anzabl der Bestimmungen, so erhalt
man 994.2338. :

Man kann daher nach dem jetzigen Stande der noch nicht ab-
geschlossenen Untersuchungen die Länge des Secundenpendels für Potsdam,
Pendelsaal des Geodätischen Instituts, zu a

L = 994.234 mm

 

{
i
|
j

 
