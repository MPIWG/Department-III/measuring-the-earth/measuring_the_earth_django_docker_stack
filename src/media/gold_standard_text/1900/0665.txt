sp be ae à

LUNA AU

à LUE

MI | NT Ana Vd HO LU

Ueber die Lokalitäten, insbesonders die geogr. Lage ist ausser p. 12, 13, 86 u. f.
noch Bd. 1 u. 2 des Werkes, enthaltend Navigation et Hydrographie, mit Atlas (P. 1826),
zu vergleichen. (Der Atlas war mir nicht zur Hand).

Kapitän Freycmer führte bei der von ihm geleiteten Weltreise u.a. drei von Furrın
gebaute invariable Messingpendel mit Stahlschneiden mit sich. N°. 1 u. 2 hatten cylindri-
sche Stangen, N°. 8 eine flache Stange. Mit den Stangen aus einem Stück hergestellt war
die Pendellinse. Im allgemeinen hatten die Pendel die Karer’sche Form. Ein viertes Pendel
mit Holzstange erwies sich als stark veränderlich. Die Achatlager wurden auf einem starken
eisernen Dreifuss von der Form einer abgestumpften, dreiseitigen Pyramide angeschraubt.
Der Fundirung des Dreifusses wurde grosse Sorgfalt gewidmet und in der Regel zum
Zwecke derselben ein mindestens 5’ tiefes Mauerwerk im Boden hergestellt; auf Rawak und
Falkland kam ausserdem eine Befestigung des Bodens durch eingetriebene Pfähle zur Aus-
führung. Die Beobachter bewegten sich um den Apparat herum, der in einem Glaskasten
stand, auf einem besonderen Holzfussboden, der den Erdboden innerhalb eines Umkreises
von 6—7’ Radius freiliess.

Zur Bestimmung der Schwingungsdauer von annäherend 1 Sekunde dienten keine
Koineidenzbeobachtungen sondern Abzählungen mit Hülfe einer Pendeluhr, deren Linse
sich so verstellen liess, dass die Schwingungen ihres Pendels mit denen des invariablen
Pendels nahezu gleiche Dauer hatten. Schwangen nun beide gleich, so wurden bei etwa 11
aufeinanderfolgenden Minuten des einen Chronometers an der Zähluhr die Zeiten abgelesen
und dabei die Zehntelschwingungen des Pendels geschätzt (p. 9).

Diese Vergleichung wurde dann etwa von 40 zu 40 Minuten wiederholt, im
Ganzen durchschnittlich 6 Stunden lang. Die Zähluhr erhielt, wenn nöthig, kleine Gang-
berichtigungen mit der Hand, um sie immer mit dem invariablen Pendel gleichschwingend
zu erhalten. Die Zähluhr befand sich an der Wand oder auf dem Podium der Beobachter;
sie konnte durch ihre Schwingungen das invar. Pendel nicht beeinflussen.

Die simmtlichen Chronometer wurden vor und nach jeder Beobachtungsreihe ver-
glichen, auch sonst noch vielfach. Die Zeitbestimmungen erfolgten aus Sonnenhöhen thun-
lichst früh und abends, die Gangberechnungen aus Morgen- und Abendhöhen für sich.

Die Temperaturen kamen an 2 gut verglichenen Thermometern zur Ablesung; man
wählte stets Lokale aus, die der Sonne nicht ausgesetzt waren — in der That ist nirgends
ein schädlicher Temperaturgang zu bemerken. Ehe die Beobachtungen begannen, überliess
man den dazu vorbereiteten Apparat mehrere Stunden im Glaskasten der Temperaturaus-
gleichung.

Bei den Beobachtungen waren ausser FReyomner eine ganze Reihe Beobachter thätig,
p- 98, die ich in der Tabelle nicht besonders namhaft gemacht habe.

Die Ergebnisse der Expedition Frrvomar’s zeigen nicht ganz die Genauigkeit, die
man nach der unzweifelhaft grossen Sorgfalt der Beobachtungen erwarten sollte. An der
Methode der Bestimmung der Schwingungsdauer mit Hülfe einer Zähluhr kann es nicht
gelegen haben, wiewohl sie ja nicht die beste ist; denn einerseits zeigt sich eine genügende
Uebereinstimmung der verschiedenen Beobachtungen innerhalb der einzelnen Reihen, ande-

 
