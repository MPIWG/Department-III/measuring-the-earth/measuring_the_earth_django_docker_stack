rhe haus 8s

ee Ti

fer AL ah

WU CAR

D EN À LL à et 1

Fr!

83

M. WaANACH avec le concours de M.M. W. Huxsn, calculateur, et M. K. RIETDORF,
étudiant. Afin d’avoir la plus grande certitude que les cahiers d'observation arri-
veront à temps, on est convenu avec la station du Japon, dont les cahiers envoyés
par la poste nous n'arrivent qu’un mois après l'expédition, que l’on enverrait
outre le cahier d’observation original, une copie de ce cahier par le premier
bâteau partant après l'expédition de l'original. Autrement on perdrait trop de
temps, au cas qu’on dût remplacer un cahier perdu par une copie.

Pendant tout le temps depuis le commencement des observations jusqu’à
la fin de 1900 les Messieurs suivants out fonctionné comme observateurs :

le Prof. Dr. H. KimurA et le Dr. T. NakaAno 4 Mizusawa,

le Lieutenant-Colonel Ogsrporr. . 3 3 lchardiut,

le Prof. Dr. @. Ciscaro et le Dr. G. Brancut » Carloforte,
Bun ma 2. 0 Gaithersburg,
le Pron De. 3. & Porn. . en 9, Ülmemmali,
le De, BP, Boumemen — 5 .... ee ae

Au commencement de l’année 1901 il y aura un changement d’observa-
teur & Gaithersburg: 4 partir de cette époque les observations seront faites par
M. le Dr. Herman 8. Davis, qui pendant les années 1893-1899 à pris part
aux observations de l'observatoire du Columbia-College à New-York. Afin d’as-
surer la continuité des observations, M. le Dr. Davis à déjà fait des observa-
tions de latitude à Gaithersburg depuis le commencement de novembre 1900.
D'après ce qu’on à appris, il y aura probablement aussi un changement d’ob-
servateur à Tchardjui.

A cause de l'entière conformité du programme des observations sur
toutes les 6 stations, la connaissance exacte des positions des étoiles n’est pas
nécessaire pour la réduction des observations et pour les déterminations des
coordonnées du mouvement du pôle. On a pourtant entrepris, en se servant des
catalogues existants, une détermination des valeurs exactes de la déclinaison et du
mouvement propre des étoiles, afin de ne pas employer dans les caleuls des décli-
naisons trop erronées, et surtout parce qu’il semblait désirable de connaître, du moins
approximativement, les mouvements propres de toutes les étoiles qui figurent
dans le programme des observations. Le Bureau central ne pouvant pas disposer
de personnes propres à ce travail, M. le Dr. F. Cox, astronome-adjoint à l’obser-
vatoire de Königsberg, conformément au vœu du Bureau, l’a exécuté sous sa propre
responsabilité. Afin de ne pas donner trop d'extension à cette détermination, on
a fait observer à M. le Dr. Con qu'il ne s'agissait pas du plus haut degré
(exactitude et que dans tous les cas que l’étoile se trouvait dans un grand
nombre de catalogues, il suffisait de se servir de 12 & 15 des catalogues les plus
exacts dont les époques étaient distribuées convenablement, :

M. le Dr. Coun a rempli cette tâche avec une zèle et un habileté dignes

 
