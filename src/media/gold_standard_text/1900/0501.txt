a

re oe eee

Dar

lm

   

101

beträgt. In dem Zeitraum von 127 Tagen sind in Summa a
erhalten worden, aber es kommt im Verlaufe dieser Periode n
welchem auf allen 6 Stationen Beobachtungen erlangt worde
Beobachtungsmaterials wird daher auch in Zukunft nicht auf Grund einzelner Tagesresultate
vorgenommen werden können, sondern man wird in gleicher Weise wie bisher dar
wiesen sein, grossere Zeitperioden zusammenzufassen.

Um ferner einen Ueberblick iiber die

n 122 Tagen Beobachtungen
icht ein einziger Tag vor, an
n waren. Die Bearbeitung des

auf ange-

Resultate zu geben, sind nachstehend für alle
6 Stationen die von Herrn B. Wanach berechneten Polhöhen zusammengestellt, welche in

der Zeit vom 5. Januar bis 11. Mai erhalten worden sind. Zu Grunde gelegt sind hierbei
die Declinationen, welche von F. Cohn in der Publication des Centralbureaus : »Ableitung
der Declinationen und Eigenbewegungen der Sterne für den internationalen Breitendienst”
abgeleitet worden sind. Die Resultate sind nur insofern noch nicht als endgültig anzusehen,

weil gegenwärtig hinsichtlich der definitiv anzuwendenden Instrumental-Constanten noch

keine Entscheidung getroffen werden kann. Doch sind erheblichere Aenderungen der Zahlen-
werthe ausgeschlossen.

 

 

 

Mizusawa. A= —141°,9

1900, u IS 10. 20-3199 09.0, 25 20.30. 0800 90 31 3%
Jan. 9. 8998 — 3/71 4°16 3/43 3°61 3°74 314 4.87 3.86 8.25 — 406 850 430 494 3.91
13. peel a ee aie ee ee en
17, 402 3.49 3.97 3.20 3.59 4.06 3.25 4.4] 3.56 3.23 4’40 8.95 4.06 3.75 4.66 3.74

19. 3.63 8.17 — 3,34 3.64 3.56 3.25 3 92 58 021 GO 8250 5b 07 4 Os

24. ne 3068050650 gan nn an. nn

26. ea og 3.75 3.34[5..07]3.82 4.10 3.87 5.16 3.70

27. BU en Se nenn
28. Un = dd 9 re een
30. D es ee ar an
3.82 3.45 (4.06) 3.32 3.61 3.75 3.34 (4.75) 3.58 3.27(4.04) 3 .84 (4.18) 3.99 4.92 3.78

20.80 20.28 90 90. 27 8 88. 4414 8 86 47 48 gg 40

Jan. 81. 3908" 8:46 3.24 — 3/54 8°98 4°17 449 810 ee a gs,
Febr. 2, BBL 4095 Le ky 319 — 2°61 867 4°06 — 3°09 —
4. 3.61 3.44 403 3.67 3.94 388 — __ A a

9. 200.8 48 3.0.3.0 20. 4 don 2 BOT D 708 fae ee a

14. 3.118 108.08 9.88. 2 300.0, 2 ee ee

15. — 866 = 404 590 90606 22 D 91

16. a ee se — — 2.64 3.57 4.22 4”56 3.20 3.99

19. 3.84 3.45 3.59 3.40 4.84 4.16 5.18 3 45 3.34 3°96 2.39 3.61 3.66 4.49 3.62 3.65

23. 3.46 3.85 845 — 4.11 4.21 5 19 3 68 2.69 4.01 2.66 3.51 8.59 _ __ 390

24. na an lee ae a ae
3.61 3.44(3 53) 3.61 (4.22)4.10 4.08 3 48 3.04 (4.02) 2.56 3.58 3.93 (4.52) 3.56 3.74

IT 13

 
