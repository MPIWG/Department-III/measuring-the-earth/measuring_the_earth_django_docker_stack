411

Quito posséde un observatoire muni de bons instruments. Le Gouvernement francais
a mis à la disposition du Gouvernement équatorien pour une période de cing ans un de
nos plus habiles astronomes, M. Gonnessrar, de l’observatoire de Lyon. Ce savant va prendre
la direction de l’observatoire de Quito.

Cette combinaison, si heureuse au point de vue de l'influence extérieure de la
France, a été rendue possible par la munificence de deux donateurs anonymes, et je suis
heureux d'avoir l’occasion de rendre ici hommage à leur généreuse pensée,

En tout cas, cette circonstance facilitera singulièrement les opérations de la Mission.
Pendant que les officiers opéreront dans les stations extrêmes, M. GonNæssrar fera des ob-
servations simultanées à Quito. Cette simultanéité, indispensable pour les observations de

longitudes, sera aussi précieuse pour la mesure de la latitude et nous fera connaître les
différences de latitude avec une très grande précision.

a hal een re TS PTI

iui

LONGITUDES.

Le télégraphe rejoint maintenant Quito à Guayaquil et à un point très voisin de la
Station astronomique nord. Entre Quito et Guayaquil, il y a un relais; il y en a un égale-
ment entre Quito et la station nord. Mais il sera facile de supprimer ces relais en em-
ployant un nombre suffisant de piles.

Vers le sud, le télégraphe est poussé moins loin et il s’arréte A une assez grande
distance de la station astronomique; mais on travaille actuellement à le prolonger et il est
certain qu'au moment du besoin il pourra être facilement relié à la station sud.

Les différences de longitude entre Quito et les deux stations astronomiques princi-
pales, et entre Quito et Guayaquil pourront ainsi être déterminées sans relais télégraphique,
c’est-à-dire avec une grande précision:

Guayaquil est relié par des câbles sous-marins au réseau télégraphique général; on
pourra donc connaître sa différence de longitude avec l'Amérique du Nord. Mais ici l'emploi

de relais sera inévitable et la précision sera moindre. Elle sera d’ailleurs beaucoup moins
nécessaire.

il Hl | Ami

ul ı

ASTRONOMIE SECONDAIRE.

IM. amd a Lian gen

Outre les trois stations astronomiques principales, il sera établi six stations astrono-
miques secondaires où l’on mesurera des latitudes différentielles et des azimuts secondaires.
Ces stations seront sensiblement espacées de degré en degré.

L'une d'elles, Chuyuj, est voisine de la base principale.

I

NIVELLEMENTS.

Les bases devant étre réduites au niveau de la mer, il importe de connaitre leur
altitude avec une assez grande exactitude.

 
