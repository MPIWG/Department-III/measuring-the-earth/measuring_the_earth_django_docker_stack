 

312
geben die S. 41—67 Auskunft. Hiervon ist besonders bemerkenswerth die Aufstellung des
Pendeldreifusses. Sie erfolgte bei

Greenwich u. Maranham auf gepflastertem Boden,

Trinidad auf Cementfussboden,

Montevideo u. Para (?) auf dem natürlichen Boden,

Staten Island, South Shetland, St. Helena, Ascension, Porto Bello auf Holzklötzen von
2’ im Gevierte u. 3” Dicke, die in den Boden eingelassen wurden,

Fernando de Noronha auf entsprechend versenkten Eisenmassen,

Kap Horn auf 10—12" tief in den morastigen Boden eingetriebenen Pfahlen,

Kap d. @. H. auf einer extra gemauerten Unterlage.

Es ist zu befürchten, dass diese Aufstellungsweise, ebenso wie die frühere in Por?
Bowen, mehrfach ein nicht unerhebliches Mitschwingen der Aufstellung, also der Konsole,
herbeigeführt hat. Selbst der gepflasterte Boden in Greenwich, in dem Transit Room für
Beobachtung der Jupiterstrabanten, scheint ‚nicht besonders fest gewesen zu sein: denn N
wurde kleiner als wie in London erhalten, während es nach SABINE’s Beobachtungen mit
Benutzung von Wandkonsolen grösser sein müsste.

In Kapstadt wurde im Royal Observatory beobachtet und ein besonderes Fundament
aufgemauert. Die Meereshöhe wird von Foster und MaoLEAR zu ca. 11 m. angegeben, die
österreichischen Marineofliziere setzen H=15, was ich eingeführt habe. Die Zeit erlaubte
die Eisen- und Kupferpendel sowohl im magnetischen Meridian, wie senkrecht dazu schwingen
zu lassen. Im letzteren Falle ergaben sich, bis auf 0.1 Schw. übereinstimmend, die 4 Werthe
von N um 0.58 Schw. grösser als im ersten (p. 56). Barty bezweifelt, dass dies als eine
elektro-magnetische Wirkung zu betrachten sei, angesichts der grossen Beobachtungsfehler —
weichen doch 2 Gruppen von Reihen für N°.10 im Mittel um 0.84 Schw. von einander ab.
Ich möchte den Unterschied von 0.58 Schw. hauptsächlich als eine Folge der Verschiedenheit
des Mitschwingens der Konsole in beiden Fällen betrachten (obwohl sich der Unterschied im
Schwerpunktsabstande der Schneiden A u. B anscheinend in den A N nicht geltend macht).

Besonders bedenklich dürfte das Unterlegen der Holzklötze (ebenso wie das grosser
Steine, Port Bowen) gewirkt haben. :

Die Aufstellung konnte in der Regel in Gebäuden stattfinden; nur auf Staten Island,
South Shetland u. Kap Horn musste das tragbare Observatorium aushelfen. Die Temperatur
wurde an 3 Thermometern in verschiedener Höhe am Apparat abgelesen; über die Korrek-
tionen der Thermometer scheint die Abhandlung Baızy’s nichts zu enthalten. Der Tempe-
raturgang war im Mittel der Messungen an jedem Pendel für jede Station immer gering;
. nur in Greenwich waren die Verhältnisse sehr ungünstig, es blieb hier aber doch bei N°. 10
nur + 0°.4, bei N°. 11 + 1°.0 F. mittlerer stündlicher Temperaturgang, was indessen auch
nur 0.1 bis 0.2 Schw. in N giebt.

Der Umfang der Messungen war überall ein bedeutender. BaıtLY giebt an, dass er
etwa fünfmal so gross wie bei SABINE, FreyCIner und Dupkrrey gewesen sei. Die Anzahl
der beobachteten Reihen von Koineidenzen zu 2'/, Stunden Dauer betrug für jedes Pendel,
bezw. für die Schwingungen um die Schneiden A oder B ausserhalb .Zondon’s, mindestens

 

i
u
i
|
|
|
i
;
|

{
|

|

:

ersehen har dmà

 
