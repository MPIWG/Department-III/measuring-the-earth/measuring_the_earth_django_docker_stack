DIAGRAMME FIGURATIF DES CIRCONSTANCES D’EXECUTION DU NIVELLEMENT. _COMPARAISON DES RESULTATS DES DIVERSES OPERATIONS.
Nivellement general de la France, - Réseau Onde den. ne CAMPAGNE 1889.— 2° BRIGADE.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

      

 

 

 

 

 

   
    
 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

   
  
 

   
 
   
   

 

 

 

 

 

     
  
 

 

 

 

 

 

 

 

 

 

 

 

 

      
     
  
   

  

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

   

 

        

 
 

 

 

 

     

 

 

 

 

 

 

 

 

   

 

   

 

   
 

 

   

 
  
 
 
  

 

 

 

 

 

 

 

 

 

5 à 80 35 5 6 7 75 8 OOo F5 | 1 2 1 15 ie 1 30, +0 , 5 a 70 7 8] 1 tr a ; = 6 Se ir = en En m | 20 25 Bae 85 40 50 = 60 er | 80 =
2 ‘ Numéros des repères principaux : i | Ar = ine uli gl] nm NE ln hl Lit Tan mh (Pane einen tere al if al nie li aha urlaub ul thu ul Huhu ul | f
| Ogu | nee ola Bt 0-75 it odu [ar uv | vx sue. \
| | | | el es | | |
| | ; et | | 1 i
Lettres indicatrices des sections: | 00 | | | | Oki | AM | QUI RU | : U en | tue
| | fe l | | | hateaulin | ie | ir ee
200" dE te bp | tt : a a on
: Chateauli ndérna : Camaret. M Chäteaubrjant Gar in a Roche-%Yon
. : as | re ES Fra | ——
100 Retion aa nar TG Rd DIRT: 3 e AT Pe 100
Yan i ; | 670"
| Profil du cheminement : Gele 5 1 | 10 | 200 Ze] | 390 | ‘iB OL | 40 | | 5 oi BIO Tamperatures
Longueur nivelée par la brigade depuis le début de la campagne: , 5° L i | | ll ' | N | PVN NOMA en me
+10 2e es _ E | / Z 2 of 14 [16 ee men a as BER Id a 22. Lu 29 30 aa af
Température moyenn i 8 = f : ¢ °
p yenne des deux mires 9 0 Po 12]a ssh 2e: | + do |n [315] ‘17] 19p125] 27] 92] 2 nai sn [m nsjme Ti 22124 | "26h82 1 "781 oe TZ > +10
a: el tt ea = re ap = bo a : 7 a 0
Dates des opérations: 1 | | ! BB do be CIRE AE s | ? Variation
el lr eee ear | i: 07 | po a m a = ~| du métre des mires |
See
i cmm ee 5 4 Pee Eales Sal 2 ® LL. pa ACT
zo: + | | | ' ! \ lee 7
i | Mis ee [eect ee ee | | = | nee IN > +10
+70 = u L_ > 7 1 pa
AE : 6 WA . i = 0
Variation métrique moyenne de longueur des deux mires, 0 — mr emm
SIR } | a \- x ze i RE i ! jose) a a Salt dg
10 Tat T 1 A SS = | | 5 Al Y | A | Mrs 0 |
à pur ibe E a LE Be eee eee eee ee ee ee Discordances,
| | BR | re | N alle en centimétres
Ge -+ —— i = I ee ee ee ee ee 4 lu ——— L - pie lf D EE 6 = + L aoe ler 4 PE == f= ~— |. — 4 Fe te —| tn = ne |
+13 m ' | le] | | 1 = ;
| ar . | 1 2 * = + Lay - ie = Re } Ld ds Peat +12"
| le | 1 : | | aa | le a je Spas = =
ol lrRois | > [ke si gu bh. | pf MM EB Ne 21 Dier 02,8 ——_ ——
| a +10
| ECARTS ENTRE LES ALTITUDES MOYENNES ro 3 1 L
déduites des opéralions primitives [ °
Le
ET LES ALTITUDES RÉSULTANT DES DIVERSES OPÉRATIONS Tai si Le nef | ot |
ou combimaisons d’op6rations ci-apres:' EEE Est | ee Au 7 er Pen |
a Net — a A +
ga | is
+5 = ra RS sn ++ AS
ae =» 24 ln I IE |
Ke. Blears ag avai EE |
| = | | | IR
u ne - - nn |
4° Opérations du Chef de brigade. at nn) mare le i Tole le 5
Oo one a 4 oyenn en rale de atr 0 DH
Moyenne entre cet aller et le retour correspondant, ———— ae ÊTES ny e des | Z CH YL, ty
“dx opérations —| du IEEE,
le i | sees AR |S on ee
gem Loa | | por ALE a - en
2 em Be er | El 2 | |
= 1 | Wea Je —| | — | jen es — ; + + _ an .— [een ee | | SE t m |
a en El | nan | | an wel ee | i [ve | JE] nen le a 2)
= Ser See poe Be | a a a te ee à er ei „ Commis des Ponts| all IM Zaurem, Tone
| 2 | j ea] | | | j | | | | | | | Ba rn a ee:
de —— are one Usa oe Gauthier, commis des Ponts Lf = IE — | i | rc - al |-etiChaussées, | ec en | de ‘ides Pöntstet-Chausseks, lecteur.
: À eas | | | Keen AR ea ee ee lea) Dt +
a | I ee 4 Se Bee st ee I I an
Do ee | ior L a | od | | ste Ope 1 me oS = ite ae fal
an. 2 RS ee PR ER | lee ee ij a 1,7 2 a
| | operalions ‚du G =] | A } |
: PS epee | te eae A | I Z LIT? ALL 7
2° Opérations du lecteur. + Mayenne Tr AE KA YY HOY, MSE: 2 = 0 5
A ne o Le OPERA SL TA LARGE LE ee CIOS, L
DER Ex ST 7 | 7 Ai] ] ] | | | | ‘|
Moyenne entre cet aller et le relour correspondant, —— i | À bee eh « ——— er Je 1 - À
| : a ae 2 Ben | Jo |
| Les je | |
| Tre! | |
| | 1 I at Ri
À | - i
= § aac à | FT | D 5
| : a: | | |
| | | | L et EL oe ee
a so esa El Er | el à | P a Ss ei
| [sa | al SE a LVF DIESER SAREE
- 9 em — a ae | iY / TH, DIT 2 >
i : | | i + GIG, DÉS 7 70, AG
] 5 ata 3 = Ele = ‚oyenn na: SER RS ae f 7 L TA 4
a! ns —— — : | ie We 7
| 3° Moyenne des deux opérations d'aller du Chef de brigade Be Ly y ? >> CH AZ
| et du lecteur. à La WAGE AD LAL r 0
1! Le De | L 4 ]
| | | | |
1 = | | | a eee mere ere
q = 3 ETDS SIERT DS SEALS SORTER LE WS AEA 5 EIER EEE = — a = wip. de Harchadier & Gre, Paris,

 

 

 

 

 
