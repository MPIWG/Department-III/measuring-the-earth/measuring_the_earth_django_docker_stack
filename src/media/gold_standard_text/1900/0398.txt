 

|
|
|
|
|
|

|

 

6

Aus dem Bericht des Herrn Professors Herserr über die Lothab-
weichungen von 1892*) folgt übrigens, dab

À — lo — 05 (Crarke) oder — A — 2.3 (Besser)

ist, so daß also der Unterschied zwischen Ar und A, keinen bedeutenderen
Einfluß ausüben wird.

Die Uebereinstimmung für (4) und (5) ist im Absolutglied unter
den gegebenen Verhältnissen als gut anzusehen, während die immerhin nur
geringen Unterschiede in den Coefficienten der andern Glieder durch die
Verschiedenheit der Rechnungswege zu erklären sein dürften. Der Erfolg
der Rechnung beruht übrigens hauptsächlich auf der Ausnutzung der Larracr-
schen Controlgleichungen.

Eine etwa um 4"5 windschiefe Lage der Bogen der beiden euro-
päischen großen Breitengradmessungen gegeneinander ist hiernach wohl
mit einer gewissen Sicherheit festgestellt. Allgemeinere Schlüsse aus dieser
Thatsache hat Herr Professor Herserr schon früher und neuerdings wieder
an einer anderen Stelle bereits gezogen (Vortrag auf dem VI. inter-
nationalen Geographen-Congreß in Berlin, October 1899).

Endlich wurden die Rechnungen im Meridian von Bonn nach
Norden und nach Nordosten hin erweitert. Es wurden nämlich folgende
Linien erledigt:

Bonn — Ubagsberg
Ubagsberg — Nottuln
Nottuln — Wilhelmshaven
Wilhelmshaven — Kaiserberg
Kaiserberg — Kiel
Kiel — Knivsberg.

Nottuln und Kaiserberg sind keine astronomischen Punkte; sie wurden
nur eingeschaltet, weil an diesen Stellen verschiedene Dreiecksketten zu-
sammenstoßen. Für diese Linien wurde auch noch die Einschaltung des
astronomischen Punktes I. Ordnung Wilhelmshaven in die erste, ohne
Zwang ausgeführte Ausgleichung der Hannoverschen Dreieckskette der
Königlich Preußischen Landesaufnahme vorgenommen (2 Bedingungs-
gleichungen, mittlerer Richtungsfehler — +157). Die Resultate der
hierzu nöthigen Winkelmessungen sind uns von Herrn Geheimrath
Arsercnt zur Verfügung gestellt worden.

*) Verhandlungen d. I. E. in Brüssel 1892, Berlin 1893, 8. 507.

 

i
|
}

sew lade ala li ann

 
