 

eS aS Se

Sea nn

ad
il
GE
Ih
un
ty
it
IH
Ht
Hi
ik:
hr
ii

BEILAGE A, XI.

BAYERN.

Bericht über die in den Jahren 1898-1900 ausgeführten
Erdmessungs-Arbeiten.

In den Sommermonaten der Jahren 1898 und 1899 wurden für die Mehrzahl der
in der Nähe des Münchener Meridians gelegenen Stationen, auf welchen im Jahre 1897
Schweremessungen ausgeführt worden sind, die Polhöhen bestimmt. Da es sich um Statio-
nen II. Ordnung handelte, so wurde ausschliesslich die Methode der Circummeridianhohen
geeigneter südlicher Sterne, combinirt mit Zenitdistanzen des Polarsternes, in Anwendung
gebracht. Die Resultate einer vorläufigen Berechnung sind in der nachfolgenden Tabelle
zusammengestellt, wobei der gegenseitigen Beziehungen halber auch die Ergebnisse der
Schweremessungen vom Jahre 1897 beigesetzt wurden.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

Breite | Länge sett Höhe Unter. oe | ee
STATION v. Greenwich oS grunds Tes | pee
: A : ‘ Dichte cnwere | Teite
m m/m |
Coburg: 50 1531-1010. 60 297 2.4 + 0.22 — 3.90
Lichtenfels ° || 50. on LL 5 265 2.4 + 0.16 — 0.4
Bamberg AQ | bet 10.4 54 279 2.4 + 0.28 — DM
| Forchheim 40 aoe 11 4 267 2.4 + 0.19 426:
Nurnberg 49.1 27.4 11] a 312 2.4 + 0.21 — 8.94 |
Roth a/Sand 49 14.8, 11 6 342 2.4 +010 | + 0.6
Pleinfeld 49 6.4| 11 0 395 2.4 Oe | oo 21
Hichstatt ASN Bee D) 1e 389 2.4 +027 | — 23
Ingolstadt 48. AGO 11... 26 374 2.15 | 0.30 +402
Pfaffenhofen 148: ol 11 31 428 2.2 + 0.02 10
München 48 8.0, el 36 525 2.15 — 0.17 + 0:00
Holzkirchen 44.1 82.20. 4 AS 685 2.2 — 0.36 + 3.98
Benediktheuern | 47 | 42.5; 11 | 25 624 2.4 — 0.51 4. 29.00
Mittenwald 47. 1.20.5211 1% 913 2.4 — 0.54 + 11.44
Innsbruck 47 169% 25 580 2.4 — 0.82 | —

 

i
|
|

Semi

un un nam en as
rer PV Fr

 
