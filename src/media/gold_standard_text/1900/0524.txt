 

h
t
i

hi
i
i
\

pourra y ajouter le niveau moyen calculé au moyen des observations faites sur des échelles
(1816—96) à Kolbergermünde, station située à l'Est de Swinemünde.
En joignant ces résultats à ceux trouvés de 1882 à 1897 on a les chiffres suivants:

Travemünde, Marienleuchte, Wismar, Warnemünde, Arkona, Swinemünde, Kolbergermünde.

— 0,1387 — 0,1264 — 0,1230 — 0,1068 — 0,0470 — 0,0655 — 0,0580

On remarquera que le niveau monte à mesure que l’on s’avance dans l'Est et
l'on peut admettre avec une certaine vraisemblance que cela est dû à l'influence du veut.

On s’occupera dans le courant des années prochaines d'éliminer cette influence du
vent (et peut-être aussi de la pression barométrique) pour avoir le vrai niveau moyen de
la mer sur les côtes allemandes.

On n’a pu constater ni surélévation ni abaissement du niveau moyen provenant de
la configuration de la côte.

Les calculs pour la détermination des diverses ondes ne sont pas encore achevés,
les grandeurs moyennes annuelles de l'onde solaire et de l’onde lunaire sont les suivantes:

Travemünde Marienleuchte Arkona
Onde solaire 3 1,0 94,; 5 1 io
Onde lunaire 95,3 64,1 20,7

Dans le courant de l’année prochaine on publiera a partir de 1898 tout ce qui
regarde les neuf stations placées sous la direction de l’Institut royal géodésique.

ETATS UNIS DE L'AMERIQUE DU NORD.

Le Coast and Geodetic Survey aux Etats Unis s'occupe avec une grande activité de
la question des marées. L'annuaire publié par lui et les publications qui l’accompagnent,
montrent le labeur qui a été nécessaire pour arriver à donner des chiffres exacts pour le
mouvement des eaux dans les deux océans qui baignent les Etats Unis.

Le nombre de points où des observations maréographiques ont été continuées pen-
dant nne période d’au moins une année s'élève à 62; dans plusieurs d’entre eux les séries
se sont prolongées pendant deux ou trois années et enfin dans quelques ports les obser-
vations ont duré jusqu’à 25 et 50 ans.

En dehors de cette liste de postes maréographiques qui nous a été obligeamment
communiquée par M. Trrrmann, Acting Superintendent du Geodetic Survey, et que nous
reproduisons ci-aprés, on a fait dans plusieurs milliers de stations des séries temporaires de
hauteur des marées.

1 Eastport’ Me. 4 Boston Mass.
2 Pulpit Harbor Me. 5 Newport R. I.
3 Portland Maine. 6 Bristol R. I.

i
#
|
1

 

ne Lédanuhaiaaaalanans at sans à

restate baa

 
