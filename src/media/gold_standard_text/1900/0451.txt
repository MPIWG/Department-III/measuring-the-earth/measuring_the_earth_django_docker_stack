27

 

Insuras in Mien, XV. Banda - un  .,..1,...,,..22820 ,

a 55

13. Du Bureau central & Postdam: Bericht über die Dängen-, Breiten- und Azimut-
bestimmungen, erstattet vom Centralbureau durch Herrn Prof. Auprucut. Extrait
= des comptes-rendus de Stuttgart 1898 . . . . .. “. 10 Ex,
I 14. Die astronomisch-geodätischen Arbeiten des K. u. K. Militär. hen |
F Instituts in Wien. XILL. Band. . . . . ; an, i
15. Die astronomisch-geodätischen Arbeiten des K. u. K. “Militar. en i
Instituts in Wien. XIV. Band. . . . . a no da on. !

| 16. Die astronomisch-geodätischen Arbeiten des K uU. on Militär- Geographischen

C. Inventaire des instruments et appareils appartenant & V’assoeiation géodésique
internationale, et déposés au Bureau central.

Pour le service des latitudes ont été achetés :
1 Armoire pour les dossiers, M. 182,00. :
6 Niveaux-Horrebow de C. Rutcuu en réserve, M. 600,00, avec caisse, M. 15.00. ‘

Du reste voir l’inventaire dans le rapport annuel pour 1897.

(Les appareils expédiés aux stations des latitudes ont été mentionnés au commen-

iy BALA ob

‚iR

cement de ce rapport).
La Bibliothéque compte 286 ouvrages.

Potsdam, Février 1900.

F. R. HELMERT.

en eee

na mi au

awit

TOR) |e

a een

 
