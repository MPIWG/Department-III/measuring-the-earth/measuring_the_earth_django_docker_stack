 

236

wo es auf Holzboden aufgestellt werden musste, wurde nach PLANTAMOUR’s Versuchen die
Korrektion erhöht und zu + 0'".0861 angesetzt.

Um diesen Ansatz zu prüfen, habe ich die Angaben für die Schwingungszeiten bei
schwerem Gewicht unten und oben benutzt. Im Sinne u.-o. sind die Unterschiede in Ein-
heiten der 7. Decimalstelle der Schwingungszeiten:

Korrigirt.
NOT N°. II H Nos INSSTE
Wladikawkas 2391 728 693 2322 659
Batum 2359 629 2 2359 629
auf Pfeilern: : Jelisawetpol 2439 741 427 2396 698
Schemacha 2410 727 715 2338. 655
Baku 2355 637 7 2354 636
Se ee RAGES Gudaursk 0744. A104. 2247 2519 879
; | Duschet 2600 885 846 2519 800

Diese Unterschiede hängen von Mitschwingen, Luftdichte und Temperatur ab (letz-
teres auch noch, obgleich schon auf eine Normaltemperatur redueirt ist, die aber wohl der
linearen Ausdehnung des Metalls entspricht). Indem ich aber von dem kleinen Tlemperatur-
einfluss absah und das Mitschwingen als Quelle zufälliger Fehler betrachtete, konnte ich
aus den 5 Unterschieden auf Pfeilern den Einfluss der Luftdiehte in Form eines von H
abhängigen linearen Gliedes bestimmen, dessen Koeflcient + 0.1 ist. Damit nehmen die
Unterschiede die oben als „korrigirt” bezeichneten Werthe an. Im Mittel ist auf Pfeilern
bezw. 2354 und 654, auf Holzboden 2517 und 839. Der Unterschied des Mitschwingens
giebt im Mittel 174.10” Sec.; in der aus schweren Gewicht oben und unten abgeleiteten
Schwingungszeit wird er somit etwa 280.10” und in g 70 Einheiten der 3 Decimalstelle,
Anstatt dessen ist um 0.021 in L oder 47 Einheiten in 7 korrigirt. Das ist immerhin
eine leidlich gute Uebereinstimmung. Zugleich zeigt sich auf Pfeilern, dass der veränder-
liche Einfluss des Mitschwingens weniger als rund + 0.010 em. in g. beträgt.

Bei allen Messungen (die nach Zineur’s Vorgang in Pulkowa bewirkt wurden),
blieben die Schneiden in unveränderter Lage; die Ergebnisse wurden aber nach HEAVISIDE’s
Beobachtungen mit kleinen Reduktionen wegen der Schneidenform versehen; sie bilden mit
dem in Xd. angenommenen Werth für Pulkowa ein System. Mittelst des leichten Pendels
fand Kunnprr¢ 1885 die Korrektion von L fiir das Mitschwingen gleich + 0.0665, also
nur 0”.0015 grösser als früher; Astr. Nachr. N°. 2689 Bd. 113.

Die Beobachtungen von 1879—83 erstrecken sich auf jeder Station über mehrere

Tage, und auf die Zeitbestimmungen ist viel Sorgfalt verwandt. Für jede Schwingungszeit

jeder der beiden Pendel liegen für beide Schneiden 3 Werthe vor (auf 2 Stationen sogar
5), wonach der m. F. eines Mittels aus 3 Bestimmungen + 21.10” Sec. wird.

Die Schneidenabstände wurden an 8 Orten bestimmt und zeigen sich fast konstant ;
ebenso entsprechen die Unterschiede der T beider Pendel für gleiche Gewichtslage gut der
oben angegebenen Genauigkeit. Hiernach wird der m. F. eines Ergebnisses für y auf einer
-+ 0.008 em. angenommen werden können. Hierbei ist freilich der Einfluss der

Station zu

 

und has urn unse Ee:

 
