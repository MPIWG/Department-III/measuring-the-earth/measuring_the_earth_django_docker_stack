 

38 10
%

l'association géodésique internationale (Juin/Juillet 1899). La station a été erigée et est dirigée
par le gouvernement impérial du Japon, sous le contrôle de M. le Ministre de l’enseignement
public, et la correspondance avec le Bureau central se fait en général par l'intermédiaire de
la commission géodésique du Japon, dont le président M. le Prof. Tirao est directeur de
l'observatoire astronomique & Zokio. Quand le Bureau central fait parvenir directement à
Mizusawa des communications officielles urgentes, il est obligé d'envoyer en même temps
une communication à M. le Président. Dans tous les cas importants les envois sont dirigés
ı la fois & Tokio et & Mizusawa en se servant de differents moyens de transport.

Par décrèt impérial du 22 Septembre 1899 M. le Docteur Hisasar Kimura a été
nommé directeur de la station: il fonctionne aussi comme observateur. En outre on à
nommé comme observateur M. le Docteur Toxuro Naxano. On a l'intention de nommer
encore deux observateurs-adjoints et un copiste.

Le gouvernement impérial a acheté le terrain sur lequel l'observatoire a été établi
et y a fait bâtir non seulement une maisonnette pour l’instrument, mais aussi une maison
pour les observateurs. D'après une communication de M. le Prof. TÉrao du 21 Decembre 1899
les observations définitives dans l'observatoire ont été commencées depuis le 9 Decembre.
Les difficultés présentées par le terrain et la peine qu'on avait à se procurer les matériaux dans
un endroit situé à grande distance des centres industriels avaient retardé jusqu’à ce temps l’in-
stallation de l'observatoire. Au moment où cette communication nous a été faite on ne s'était
pas encore servi de la mire méridienne. Une maladie forcait M. le Dr. Kimura de retarder
son départ pour Mizusawa ou il n’arrivait que le 13 Octobre, en méme temps que M. le Dr.
Nıkano; vers ce temps le télescope génital arrivait aussi à sa destination. Après les
préparatifs indispensables on pouvait déjà au mois de Novembre commencer les observations
dans une installation provisoire. Les cahiers des observations n’ont pas encore été reçus
au Bureau central. (Ces cahiers nous arrivèrent pendant la publication de ce rapport)

,

La station de Mizusawa reçut de l’association géodésique internationale, par l’inter-

médiaire du Bureau central, les instruments suivants :

1. Un telescope z6nital de Wanscuarr (distance focale de l’objectif 130 cm.,
ouverture 108 mm., oculaire ordinaire et à réversion avec un grossissement
de 104; éclairage ordinaire et électrique du champ). L’instrument permet
Vobservation des étoiles de la 8° grandeur dans le champ éclairé.

2. Un bon chronométre de marine (temps sidéral) de Emrricn a Dremer-
haven, N°. 779.

3. Une montre en argent (temps sidéral) de l'Association ouvrière de Locle.

4, Un baromètre enrégistreur de Ricuarp FRÈRES (Juzes Ricxarp) à Paris

avec feuilles pour trois ans.
5. 20 lampes électriques de 4 volts, 2 lampes électriques à main et un rhéostat.

Ces appareils et 10 cahiers d'observation (on envoyait encore 2 cahiers le
18 Juillet et M. le Doct. Kimura en prenait 5 avec lui) furent envoyés le 12 Juillet de
Bremerhaven à Tokio, par Hongkong, en même temps que l'appareil de pendule du Japon

 

i
i
3
LE
3
ii

rasé:

ah

am eh na he hei as

 
