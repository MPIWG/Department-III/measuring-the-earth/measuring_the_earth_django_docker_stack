 

ft
i
i
H
if
IE
H

150

latitude la valeur de 1”.22 et en longitude de 1”.52, La valeur linéaire correspondante
est de 47.7 m., ce qui répond à une erreur relative de 1 : 48969, le périmètre du polygone
étant de 2336 km.

Cette erreur a été supprimée par les corrections suivantes de l’azimut des lignes
géodésiques :

De Lavrova à Lipetzk — 5.82
De Lipetzk à Chiroki Bouérak — 10".47
De Petrowskaia & Axaiski + 7.98
D’Axaiski 4 Sarepta 721002

De Sarepta 4 Chiroki Bouérak — 117.27.

Pour le calcul des triangulations situées à l’ouest de l’arc de méridien on a pu
aussi former deux polygones, quoique de dimensions bien moins grandes que les précédents.
Le premier polygone se détache de l’arc de méridien au même point de Tarassowtzi, pour
sy joindre de nouveau au point de Tortchine, après avoir passé par Koustintzi (près de
Grodno), l'observatoire de Varsovie et Kaventchine. Le second polygone se rattache au
premier à Varsovie et à Zbendowitze et passe par les points Ratzionjek et Mirov situés
près de la frontière prussienne.

Comme le calcul de ces deux polygones a été sous tous les rapports semblable au
calcul des deux polygones situés à l’est de l’arc de méridien, nous n’en donuerons ici que
les résultats.

PREMIER POLYGONE.
Différences des azimuts calculés et observés:

De Tarassowtzi à Koustintzi ey ie
De Koustintzi 4 Varsovie + 67.387

Premières corrections de l’azimut:

I: 1.

 Parassowizi + 141 Koustintzi — 3.18
Koustintzi — 17.41 Varsovie + 37.29

Erreur de fermeture à Varsovie:

en latitude 0.37

en longitude 0.41

linéaire 13.77 mi:

relative 1: 74167 (périmétre = 1016 km.)

Corrections définitives de l’azimut:

 

mir men

Le MER MANIERE | dl
t Amir Mwst em

Ti
RE TRE EME

MU AE ET ere

Las Til.
rarer n

Ir em!
Te

A. cst mal ik, Mian pa 4 =
a nenne

 
