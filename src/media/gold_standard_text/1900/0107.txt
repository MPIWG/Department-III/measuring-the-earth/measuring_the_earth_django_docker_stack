DS EE ER À

Fl

Tann aa | uw

101

Bestimmung der Refraetions-Aenderungen im Laufe des Jahres. Die Beobachtungsreihe ist
abgeschlossen, aber die Endreductionen sind noch nicht fertig.

Der Beobachter hat gefunden, dass die vorläufigen Reductionen einen beträcht-
lichen Unterschied zwischen den grössten und den kleinsten Werthen der Refractionscoefhi-
cienten auf dieser Station zeigen, und er ist der Meinung, derartige Beobachtungen sollten
in jeder Sternwarte ausgeführt werden.

Herr Oudemans legt eine Karte vor, worauf die in Java beobachteten localen Breiten-
abweichungen verzeichnet sind. Er macht die Bemerkung, dass in ziemlich kleinen Entfer-
nungen die Abweichungen manchmal recht gross sind, bis zu 37, und dass es in einem
solchen bergichten Lande unbedingt nothwendig ist, auf einer grossen Anzahl von Stationen
Länge und Breite zu bestimmen.

Herr Bouquet de la Grye, der zweimal von der Association beauftragt war, einen
Bericht über die Mareographen zu erstatten, erwähnt, dass man gegenwärtig die Bewegungen
des Meeres durch die harmonische Analyse bestimmt. Nach seiner Meinung müsste also
Herr Darwin, der Vater dieser schönen Methode, in der Zukunft mit der Berichterstattung
beauftragt werden, und da Herr Darwin keinen Einwand dagegen erhebt, schlägt Herr Bouquet
de la Grye vor ihn als Berichterstatter zu wählen.

Dieser Antrag wird angenommen.

Der Herr Secretär verliest, im Namen des Herrn Helmert, folgende Erklärung:

Das Centralbureau publicirte vor ungefähr zehn Jahren eine geodätische Bibliographie
zusammengestellt von Herrn Prof. Börsch, dem Vater unseres jetzigen Collegen. In einigen
Jahren muss diese Bibliographie vervollständigt werden. Ich habe jedoch gehört dass Herr Prof.
Gore aus Washington, der ebenfalls eine Bibliographie herausgegeben hat, die Absicht habe
diese zu vervollständigen und eine neue Ausgabe davon zu besorgen; die United States Coast
and Geodetic Survey wäre geneigt die Kosten dieser Veröffentlichung: zu bezahlen, wenn die
Erdmessung diese neue Ausgabe wünscht.

Das Centralbureau, das schon genug mit Arbeiten belastet ist und keine genügende
Zahl von wissenschaftlichen Hülfsarbeitern zu seiner Verfügung hat, ist‘ gern bereit die
Veröffentlichung einer neuen Bibliographie einem anderen Gelehrten zu überlassen. Ich habe
daher die Ehre folgenden Antrag zu stellen:

„Die Conferenz äussert den Wunsch, Prof. Gore möge die Publication einer neuen
vollständigen Ausgabe seiner geodätischen Bibliographie auf sich nehmen.”

Dieser Antrag wird genehmigt.

Der Herr Präsident fordert die Delegirten auf, Vorschläge zu machen über den
Versammlungsort der nächsten Conferenz. Die Wahl des Ortes und des Zeitpunkts liegt
dem Präsidium ob, aber das Präsidium hat immer gemeint etwaige Wünsche provozieren
zu müssen, denen alsdann bei der ihm obliegenden Wahl Rechnung getragen werden kann.

Herr Bassot bemerkt, dass in Privatgesprächen die Namen Kopenhagen, Budapest,
Cambridge und Haag genannt worden sind.

Herr Winston äussert den Wunsch, die nächste Conferenz möge in einer der Städte

 
