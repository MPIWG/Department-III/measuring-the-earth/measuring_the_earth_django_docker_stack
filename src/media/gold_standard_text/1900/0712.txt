 

308

Im Unterschied der Schwerkräfte giebt dies den m. F. =# 0.005 em. Wenn dieser
Betrag auch abgesehen vom Mitschwingen des Untergrunds annähernd zutreffend sein dürfte,
so dürfte doch y für Paris wegen dieses Mitschwingens nicht unerheblich zu klein sein.

In den Jahren 1828 u. 1829 verband SABINE die Sternwarten zu (Greenwich und
Altona mit London (Phil. Transact. 1829 u. 1830). Bei dieser Operation schwang das Pendel,
N°. 12, an der Wand — fiir London und Altona ist dies ganz unzweideutig gesagt, für Green-
wich geht es aus Phil. Transact. 1830, p. 239, hervor, wo gesagt ist, dass fir Greenwich—
Altona „the same..planes and fixed support were used” als für Greenwieh— London. Die
Uebereinstimmung der Einzelergebnisse in sich an jedem Orte ist vorzüglich.

Zuerst schwang das Pendel in London, Mr. Brownn's Haus, dann in Greenwich;
dann wurde die Schneide nachgeschliffen und an beiden Orten erneut beobachtet, hierauf in
Altona und nochmals in Greenwich.

Die Ergebnisse sind:

Br F N beob. u. red. auf Rs R, Rs N red. auf 62°

1829 London 30.04 62.5- 8593.57 62 + 0.01 eS == 0.28. 85973:80
, Greenwich 29.66 58.92 85974.09 „ —0.08 oe 0.98 235993:
1899 London 29.65 74.9. 85965.04. 78 Ee +457 —0.27 85969.34
» Greenwich 29.58 61.5  85969.78 62 Ot os — 0.28 85969.49
„ Altona 29.94 58.32 85979.84 58.32 ern "0:00 00/01
Greenwich 29.99 63.53: 86969.33: 63.58 nr Ag EE 028 Bo.

Die beobachteten N sind bereits mit dem Temperaturkoefficienten 0.430 auf die
angegebene Temperatur und mit dem Koefficienten 0.355 für Lufteinfluss redueirt. R, giebt
die Verbesserung der vorläufigen Temperaturkorrektion mit 0.027, R, die Reduktion auf
62° mit 0.457, R, die Verbesserung des Lufteinflusses mit —!/,, der von SABINE ange-
brachten Korrektion.

Die Unterschiede der N sind:

London— Greenwich = — 0.43 und — 0.15; Mittel — 0.29;
Greenwich— Altona = — 8.25.

Die m. F. der zugehörigen Schwerkraftsunterschiede dürften + 0.005 em. kaum

überschreiten.

XVle. Messungen purcHh Haun 1822.

Letter from Captain Basil Hall to Captain Kater, communicating the details of
experiments made by him and Mr. Henry Foster, with an Invariable Pendulum; in London ;
at the Galapagos Islands in the Pacific Ocean, near the Kquator ; at San Blas de California
on the N. W. Coast of Mexico; and at Rio de Janeiro in Brasil. With an Appendix, con-

 

|
i
;
i
i
|
!

ad nah nn

nenne lkuhhhhche

 
