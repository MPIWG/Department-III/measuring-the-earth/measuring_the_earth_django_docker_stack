TEE ET TREE SE

He

Ea a ead

FE
i
be
a
te
BE
É
N
f

140

wobei auf Türkenschanze noch ein kleinerer Pfeiler benutzt wurde. (Vergl. Mittheilungen
V, p. 93 und Sehwerkr. im Hochgeb. 8. 18).

1887 fand sich Ay=-+3 nach Mitth. X, 8. A., p. 25 (ohne nähere Angabe).

1891 ergab sich aus 2 Tagen mit je 2 Reihen zu 4 Pendeln auf Türkenschanze und
doppelt soviel im mil.-geogr. Institut Ay—+ 10. Schliesst man das eine, stärker ver-
änderliche Pendel II aus, so folgt + 9. Der m. F. wird #2 nicht übersteigen.

1892 wurde auf Türkenschanze nur an einem Tage eine Reihe zu 4 Pendeln
beobachtet (wobei der Apparat auf dem kleineren Pfeiler Opporzer’s stand), während für
das mil.-geogr. Institut Beobachtungen in 4 Epochen mit zusammen 8 Tagen zu je 4
P. vorliegen. Ag wird + 15 oder + 12 je nachdem man für das mil.-geogr. Institut
das allgemeine Mittel oder nur den Nachbartag zuzieht. Der m. F. wird = 5 nicht
übersteigen.

Im Keller des mil.-geogr. Instituts wurde ein mit einer 0,2 m. starken Platte
abgedeckter Ziegelpfeiler von 0,9 m. Länge, 0,5 m. Breite und lm. Höhe benutzt, wobei
die Pendel in der Längsrichtung schwangen. Nach KÜHNEN kann man die Korrection
für Mitschwingen wegen des Pfeilers zu + 2 ansetzen (vergl. die Veröffentlichung das
Kénigl. Preuss. Geod. Institutes: Bestimmung der Polhöhe und der Intensitär der Schwer-
kraft auf 22 Stationen, Berlin 1896, p. 270; die Correction in der Schwingungszeit ist
(—84 + 78)5.10” bei 1,3 Kg. Pendelgewicht).

Auf Türkenschanze hat der Pfeiler 1,8 m. im Geviert und ist tief fundirt. Hier
ist das Mitschwingen wohl noch etwas kleiner als + 2. Das Mitschwingen des Pfeilers
vergrössert somit Ay um etwa 1 Einheit und Ay=10 erscheint ganz angemessen. Der
m. F. dieses Betrags wird auf + 3 zu erhöhen sein wegen des Umstandes, dass das
lose aufgesetzte Fusskreuz der Schneider’schen Pendelapparate auf verschiedenen Pfeiler-
oberflächen erfahrungsmässig manchmal etwas verschieden fest liegt.

: In neuester Zeit hat Herr Krassnow (Tab. X) auf beiden Wiener Stationen gepen-
delt; während er mit dem Pfeilerstativ wieder Ay—=10 erhielt, gab ein Mauerstativ null.

Ich möchte aber hieraus nicht schliessen, dass Ay wirklich null ist, höchstens dass es ein

wenig kleiner als 10 sei; denn der Gang der störenden Schicht in der Umgebung von
Wien (Tab. II) spricht wohl gegen Ag = 0, passt aber gut zu einem Werthe von Ay = 10.

Für die genaue Beschreibung der Lokalitäten der anderen Haupt-Stationen muss
auf die Originalabhandlungen verwiesen werden. Rs wird darauf eventuell auch bei dem
Zusammenschluss der verschiedenen Reihen zurückgekommen werden.

Für Strassburg ist H mit Herrn Prof. Haım zu 143 m. angesetzt.

In Paris, Observatoire, hat Herr von STERNECK, wie er mir auf Anfrage gütigst
mittheilte, auf demselben Pfeiler wie Herr DErroRGES beobachtet (Tour de l’Est, rez-de-
chaussée). Ich habe daher die Meereshöhe nach der französischen Angabe zu 60 m. ange-
nommen; vergl. weiterhin XIIa.

Für Potsdam und Berlin ist H nach den neuesten Bestimmungen angesetzt. Für
Pulkowa wurde nach SokoLow 70 m. angenommen.

Im allgemeinen sind die Werthe von g etwas durch die mangelnde Korrection

 

SS eee lint ca A ease ga EERE ee SEE TS 2

asin ll ase

 
