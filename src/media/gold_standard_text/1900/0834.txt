 

 

428

alliages fortement dilatables, puis la dilatation s’abaisse graduellement et rapidement, passe,
vers 36 p. 100, par un minimum, et augmente ensuite plus lentement. Les dilatations ne
dependent pas uniquement de la teneur en nickel, mais aussi des additions de carbone, de
manganèse, de chrome, enfin du travail mécanique subi à chaud ou à froid.

Le tableau ci-dessous, établi par des mesures faites, en 1896, sur les premiers
alliages fabriqués, ne peut donc être consulté qu'à titre d'exemple. Dans les nombreuses
coulées faites depuis cette époque, il n’a pas toujours été possible d'atteindre les dilatations
les plus basses indiquées ici, tandis qu’au contraire, avec des précautions particulières, des
dilatations 6 ou 7 fois plus faibles ont pu exceptionnellement être obtenues. En revanche,
l'allure du coefficient du second terme de la formule a toujours été retrouvée, dans les
limites des erreurs possibles sur ces nombres trés difficiles 4 déterminer.

Teneur pour 100 Ni. COEFFICIENTS DE LA DILATATION MOYENNE
ENTRE 0 ET 6°('),

— —

26,2 (13,103 + 0:021 28%) 10°

27,9 (11,288 + 0,028 894) —

28,7 | (10,387 + 0,030 046) —

30,4 ( 4,570-+ 0,011 94.4) —

31,4 ( 8,895 + 0,008 85 6) —

34,6 (1,373 40,002 87 9

35,6 9,877 20001379 =

37,3 ( 8,457 — 0,006 474) —

39,4 ( 5,357 — 0,004 48 6) —

43,0 7,459 20,00

44,4 ( 8,508 — 0,002 514) —

48,3 ( 9,848 + 0,000 13 4) —

100 (2) (12,514 + 0,006 744) —
34,8 + 1,5 Cr. ( 8,580 0,001 324) —
85,7 + 1,7 Cr: ( 8,878 + 0,001 654 —
86,4 + 0,9 Cr. (4,488: 02008 92 Hy. —

Si l’on rapproche, des nombres de ce tableau, les dilatations des métaux et alliages
usuels, on voit que la série des nouveaux alliages donne d’abord les dilatations de la plu-
part d’entre eux, puis, vers 29 p. 100, passe au dessous des plus faibles dilatations connues,
pour atteindre & 86 pour 100 le dixième de celle du platine, et le vingtième de celle du
laiton. Ce dernier alliage a été désigné, par M. Taury sons le nom d’invar, qu’il a conservé.

1) Échelle du thermomètre à hydrogène.
2) Nickel pur du commerce; moyenne de plusieurs échantillons.

SRE

ne EE RS ER en AT

AP 2h D A A TE AE PE PE A Sr

da dar he ala La TEEN

 
