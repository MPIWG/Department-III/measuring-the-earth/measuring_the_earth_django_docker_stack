 

Ten ee ne nn

 

Se a a a

 

nn u en

|
|

 

 

46

général d'aider, aussi à l’avenir, l’Association géodésique dans les études de la pesanteur au
moyen de pendules, en lui fournissant les éléments métrologiques des instruments employés.”

M. Bourgeois n’est pas de l’avis de M. Zachariae. Il est très facile d’avoir à Bre-
teuil l'heure de l'Observatoire de Paris, en reliant, au moyen d’un fil télégraphique, Breteuil
à Paris ou à Meudon, où M. Bourgeois à fait l’an dernier une station de pendule.

M. Benoit croit au contraire que la question de l’heure n’est pas si facile à résoudre.
Autrefois une installation a été faite par M. le commandant Defforges au moyen d’un fil
particulier passant par Saint-Cloud et une ligne volante, maïs ces installations ont été détruites
et le rétablissement nécessiterait de grands frais.

L'Observatoire de Paris a offert de donner à Breteuil un signal le dimanche matin,
jour très incommode pour le bureau de Breteuil. M. Defforges recevait des signaux plusieurs
fois par jour, et un signal par semaine serait absolument insuffisant.

' M. Bourgeois ne comprend pas pourquoi on offrait la communication de l’heure une
fois par semaine. Les communications de l’heure ne sont nécessaires que quand les observateurs
sont installés.

M. Pwrster croit qu'une détermination indirecte de l'heure n’est pas suffisante
pour les besoins de la détermination de la pesanteur, qui exige un haut degré d’exactitude.
Selon lui il faudrait organiser des déterminations directes de l’heure, ce qui exigerait une
organisation spéciale.

M. Haid confirme ce qui a été dit par M. Ferster quant au degré d’exactitude de
la détérmination de l'heure. Lies observations qu’il a faites cette année-ci en diverses stations
lui en ont fait connaître la nécessité.

M. Perster fait observer que l'importance d’une station centrale pour comparer les
déterminations absolues de la pesanteur a beaucoup diminué par le grand nombre de jonc-
tions et de rattachements au moyen d'opérations différentielles et relatives. Dans ces condi-
tions il semble qu'il n’y a pas une grande nécessité d'organiser une station centrale.

Personne ne demandant plus la parole, la proposition de M. Zachariae est mise aux
voix et adoptée à la majorité.

Personne ne demandant plus la parole au sujet du projet de l’activité du Bureau
central pour les prochaines années, le projet est approuvé par la Conférence.

M. le Secrétaire invite par ordre alphabétique les délégués des différents Etats, qui
ne l’ont pas encore fait, de présenter leurs rapports.

M. le général Zachariae (Danemark) enverra plus tard le rapport détaillé. Il annonce
seulement qu'au moyen de grandes opérations sur le Belt et le Sund, les nivellements du
Danemark ont été liés au nivellement de la Suède. En outre on a envoyé une expédition
en Islande, en Norvège et en Amérique, pour y faire des observations de latitude et de
pendule (Voir le rapport complet Annexe A. XIX).

M. Schmidt (Baviére) présente le rapport des travaux exécutés en Bavidre (Voir
Annexe A. XII” et XII’) et celui des travaux exécutés au Wurtemberg que M. Hammer,
délégué du Wurtemberg, empêché de venir à Paris, a envoyé à M. le Secrétaire (Voir
Annexe A, XIII* et XIII).

id lk

buat sit Maeda |
ma EEE ESS

   

=

m
#

inte

tal

tela h
HMS

wit

 

Vans u Vi

Dk, statin ada aaa un +
