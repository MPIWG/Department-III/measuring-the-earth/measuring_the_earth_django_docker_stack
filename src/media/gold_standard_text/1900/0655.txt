2 PT 7° Same

KA 0

Lei

in

li

Jana UI

je

COR RTE TILL El

aol

Näheres über die Stationen, auch mit Rücksicht auf geologische Beschaffenheit,
giebt das oben genannte Werk auf S. 39—45; vergl. auch 8. 164 u. £.

Als Anschlussstation für die Messungen in der Schweiz diente die Sternwarte in
Zürich, wo der Kellerpfeiler kein Mitschwingen verrieth. Da der Anschluss an Wien anfangs
nur ein einseitiger war (1892, Türkenschanze), wurde im Frühjahr 1897 eine guter An-
schluss durchgeführt an Wien, militär-geogr. Institut, mit Beobachtungen in Zürich un-
mittelbar vor- und nachher. Hieraus folgte der in der Tabelle angesetzte Werth für y in
Zürich, dessen m. F. etwas kleiner als + 0.002 cm. berechnet wird. Er dürfte in der That
auch bei reichlicher Schätzung der Fehlereinflüsse nur zu # 0.003 em. anzunehmen sein.
Bemerkt sei noch, dass bei dieser Operation das. Fusskreuz des Pendelstativs an beiden
Orten an die Pfeilerdeckplatte festgegipst wurde. Sonst ist das Angipsen unterblieben ;
bei Versuchen konnte ein Einfluss des Angipsens nicht erkannt werden. |

Den mittleren Fehler der Ergebnisse schätzt Dr. Messerscamivr auf $. 191 nach Mass-
gabe der einzelnen Fehlerquellen; bemerkt sei, dass auch die Wiener Magazinthermometer
einer Prüfung der Nullpunkte und theilweisen Kontrolle der Theilung unterlagen. Darnach
ist der m. F. in g relativ zu Zürich:

1892 u. 93 + 0.012 cm.
1894—96 + 0,004 cm.

Den Haupteinfluss haben hierauf Schwankungen im Uhrgang. Derselbe ist nach den übrig-
bleibenden Fehlern bemessen, welche die Ausgleichung der Gruppenmittel (meist für Zürich)
bei der Ableitung der Interpolationsformeln für die Veränderung der Schwingungszeiten mit
der Zeit ergiebt. Für die Jahre 1892/93 sind diese Abweichungen etwa 3-mal so gross als
für die folgenden Jahre; sie weisen auf die m. F. + 30.107 Sec. und + 10.107 See. hin.

Ich habe nun aus den Unterschieden der Reihenmittel, die an demselben oder be-
nachbarten Tagen erhalten sind, den m. F. eines Reihenmittels zu rund + 20.10” See. ge-
funden, wobei kein Unterschied zwischen 1892/93 u. 1894/96 hervortrat (aber ein ganz
besonders grosser Unterschied von 150.10” in Basel noch ausgeschlossen ist). Bedenkt man,
dass jene Gruppenmittel annähernd 4 Reihenmittel enthalten, so zeigt sich, dass man den
m. F. +10 der Gruppenmittel von 1894 u. später durch die Schwankungen + 20 der
Reihenmittel erklären kann. Dagegen bleibt für 1892/93 noch ein grösserer Einfluss einer |
Ursache, die nach 5. 16. a.a. O. in den Bigenschaften und der Behandlungsweise des älteren
Chronometers zu suchen ist.

Wie dem auch sei, jedenfalls muss der m. F. für die älteren Messungen nach der Zahl
+ 80.10” bemessen werden, wobei auf die Anzahl der Reihen nichts weiter ankommt. Dagegen
ist letztere bei den später ausgeführten Messungen zu beachten. Nimmt man hier im Durch-
schnitt 1'/, Reihen an, so ist der m. F, # 20.107: V 15, di. #17.10°,. Dem ER. in g
sind darnach fiir 1892/93 gleich + 0.012 cm.; für 1894 und später gleich + 0.007 em.

Aus 11 Unterschieden bis auf wenige Km. bei einander liegender Punkte (meist
aus den späteren Jahren) folgt + 0.009 cm, als m. F. des einzelnen Punktes, was vorste-
hende Schätzungen bestätigen dürfte.

Genf wurde 1892 und 1894 mit 0.019 cm. Unterschied, Neuenburg 1893 und 1896

 
