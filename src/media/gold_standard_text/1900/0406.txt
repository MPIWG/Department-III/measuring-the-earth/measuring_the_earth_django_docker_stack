 

|
i
|

 

 

14
verbunden. Der Oberbau des Treppenhauses wurde zum Zwecke des Aufbaus des
Beobachtungshäuschens entfernt und das Treppenhaus durch ein starkes Gewölbe
etwas unterhalb der Plattform abgeschlossen. Auf diesem steht der Beobachtungspfeiler.
Das Häuschen wurde auf der Umfassungsmauer des Treppenhauses errichtet, sein
Fußboden liegt ca. 10 m, das Fernrohr des Zenitteleskops 12 m über dem Terrain.
Eine außen am Thurm angelegte Treppe giebt den Beobachtern, die in der nahen
Stadt wohnen, direeten Zugang; als Warte- und Instrumenten-Zimmer dienen 2 Zimmer
der 2. Etage, welche noch gemiethet und direet mit der Plattform verbunden wurden.
Daselbst ist auch die Pendeluhr untergebracht.

Leider ist die Gegend nicht ganz fieberfrei, doch dürften daraus keine Schwierig-
keiten erwachsen, da zwei Beobachter sich in die Messungen theilen.

Das eiserne Beobachtungshäuschen wurde von der Berliner Firma C. Hoppe
verfertigt und zunächst probeweise in Potsdam aufgestellt (mit Ausnahme der äußeren
in Italien beschafften Holzjalousien). Hier wurde es von Herrn Professor Ciscaro bei
einem Besuche Potsdams im Juni 1899, ebenso wie das Zenitteleskop, besichtigt. Es
besteht aus 6 Theilen mit 3200 kg Gesammtgewicht, wovon je 800 kg auf beide Dach-
hälften entfallen. Das Häuschen wurde Mitte Juli nach Genua gesandt und ist ohne
Unfall und ohne besondere Schwierigkeiten von da bis zur Station und an seinen Platz
gebracht worden. Die zollfreie Einfuhr war vermittelt worden; derselben erfreuten
sich auch die von Potsdam übersandten Apparate.

Von der I. E. erhielt die Station Carloforte durch das Centralbureau folgende
Instrumente:

1. Ein Zenitteleskop von Wanscuarr (wie Mizusawa, vergl. S. 11).
9. Eine Secunden-Pendeluhr von Srrasser & Ronpe in Glashütte.
3. Eine Sternzeit-Taschenuhr in silbernem Gehäuse von der Association

Ouvriere de Locle.
4. Einen Barograph von Riomarp FRÈRES (Juzes Ricuarp) in Paris, nebst
Registrirpapier fitr 3 Jahre.
5. 290 Stiick 4-Volt-Lampchen, 2 elektr. Handlampen und einen Rheostat.
Diese Instrumente wurden Mitte August abgesandt und langten den 21. Sep-
tember in Carloforte an, wo bereits Haus und Meridianmire fertig waren. Anfang
October konnten die vorbereiteten Beobachtungen beginnen; die Beobachtungsbücher
für die drei letzten Monate des Jahres 1899 sind bereits im Centralbureau.
Zur Ausrüstung der Station besorgte die italienische Gradmessungs-Commission
außer der Mire und der dort sehr nothwendigen Sicherung des Häuschens gegen
Blitzgefahr durch Blitzableiter noch u. a. einen Contact-Boxchronometer, sowie

Chronograph, Barometer, Thermometer, Anemometer und Cupron- Elemente nebst

elektr. Leitungen.

Einige Schwierigkeiten machte die Mire. Sie liegt 106 m horizontal vom Zenit-
teleskop entfernt, innerhalb des umgebenden Weingartens, und ca. 16 m tiefer, erscheint
also in 99° Zenitdistanz. Dies gab zunächst zu Bedenken Anlaß; da indessen die

 

tasche hehe

 
