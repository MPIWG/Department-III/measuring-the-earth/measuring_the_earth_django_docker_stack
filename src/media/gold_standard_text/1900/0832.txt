 

 

426

temps. C’est peut-être, néanmoins, à tout prendre, la solution la plus parfaite qui ait été
donnée jusqu'ici de la question des températures dans la mesure des bases.

Dans sa méthode très expéditive de mesure des bases à l’aide de fils tendus sous
un effort constant, M. JÂDERIN est revenu, comme on sait, au système bimétallique et,
comme la grande étendue des portées et le but même de la méthode ne comportent pas
l'emploi de l’encombrant matériel des baraques, il cherche à assurer l'égalité des tempé-
‘atures en rendant la surface des fils aussi identique que possible.

Nous verrons plus loin quel est l’effet des erreurs de température sur les résultats
dans cette méthode comme dans toute méthode bimétallique. Il est évident que, en raison
de la faible masse des fils, de l’impossibilité de les protéger suffisamment, et des difficultés
qu’entrainerait l’emploi du platine iridié, à cause de sa forte densité imposant des tensions
exagérées, les erreurs de température sont relativement plus redoutables dans le procédé
JÂDERIN que dans les mesures par les règles de bases.

Cette rapide revision montre bien les simplifications qui résulteraient dans la con-
struction et l’emploi des appareils de bases d’une diminution importante des dilatations.
Nous allons voir qu’il en peut être ainsi.

PREMIÈRE PARTIE,

PROPRIÉTÉS DES ACIERS AU NICKEL.

Préliminaires. — C’est au regretté J. Hopkinson que l’on doit d’avoir attiré pour la pre-
mière fois l’attention sur les propriétés magnétiques singulières et les changements permanents
de volume des alliages de fer et de nickel contenant environ 25 pour 100 de ce dernier métal.

En 1895, M. Bexoir découvrit qu’un alliage contenant 22 p. 100 de nickel et
3 p. 100 de chrome était aussi dilatable que le laiton. L’année suivante, je constatai qu’un
alliage à 80 pour 100 de nickel était moins dilatable que le platine. Ces diverses constata-
tions, faites au hasard des mesures, m'eng'agèrent à poursuivre l’étude de la bizarre ano-
malie qu’elles faisaient pressentir.

Les recherches, entreprises avec la coopération de la Société de Commentry-Four-
chambault, s’etendirent aux variations de volume, aux propriétés magnétiques, à l’élasticité
et a la résistivité électrique des aciers au nickel, ainsi qu'à sa résistance à l'oxydation ou
à la facilité de son travail mécanique d’une importance particulière au point de vue de
leur emploi en Géodésie.

L’ensemble de ces recherches a montré que les anomalies des aciers au nickel sont
reliées entre elles à un tel point que, connaissant la marche de l’une d’elles, on peut, dans
une certaine mesure, prévoir les autres, ainsi que je l’ai indiqué dans diverses publications
auxquelles je renverrai pour le détail !)}, désirant me limiter ici à la description des pro-

1) Voir, en particulier, plusieurs notes publiées dans les Procès-verbaux du Comité international des Poids
ot Mosures, sessions de 1897, 1899, 1900; des articles publiés dans les Archives de Genève, (de s. t. V, p. 255 et 305,
1898) et dans le Bulletin de la Société d'encouragement pour l'Industrie nationale (6° s. t. III, p. 250; 1898) un

|
4a
|
i

eS CEE ee

bce eased Rema Deter ce

pI NESTE EY POPPI EA PY MIELE EN i ETC LS CR VS RE RS Sa D GP A EE

Aa a aa dé re

|
|
|
|
|
}
|
i

 
