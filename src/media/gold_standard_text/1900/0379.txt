 

ir

Fall am. L mil wT

ner [al Ham

 

ERRATA.

Pages Lignes Au lieu de: Lire:

Seiten Zeilen Anstatt: zu lesen:
78 14 A IVa A Ve
TA, 22 167% 1902

Page 157. Entre le petit tableau des valeurs de So —7, pour Bélin etc. et les mots:
NIVELLEMENTS DE PRÉCISION ajoutez :

Seite 157. Zwischen der kleinen Tabelle der So —70 Werthe für Bélin u.s.w. und
den Worten: NIVELLEMENTS DE PRÉCISION hinzuzufügen:

Les valeurs g,’-y, pour Bélin, Kremenetz etc. sont extraites du rapport du Prof.
Hermerr dans les comptes-rendus de la XI" conférence générale réunie & Berlin 1895
(Bericht über die relativen Messungen der Schwerkraft mit Pendelapparaten Tabelle XI),
Afin d’éviter des conclusions exagérées par rapport à l’excès de la pesanteur dans la region
des anomalies magnétiques au gouvernement de Koursk, il faut remarquer que les valeurs
mentionnées de g,’-y,, se rattachant aux mesures faites avec le pendule à réversion de
REPSOLD, exigent une correction de + 0.00050 m. à peu près, pour qu'elles puissent être
comparées avec les valeurs obtenues par M. Ixcacnewiron avec le pendule de STERNEOK.

37

 
