LE nee ns

LL

Pe VARIN Oa tow FTL

mir LT

341

Werth etwas zu gross, da er die im Endresultat eliminirten Uhrgangschwankungen mit
enthält, so ist er doch wieder zu klein wegen des Rinflusses der Unsicherheit der Zeit-
bestimmungen und der Bestimmung des Mitschwingens, sowie wegen der Veränderlichkeit
der Pendel und der Unsicherheit des Temperaturkoeflicienten. Diese Einflüsse sind jedoch
gering. Erhöht man den m. F. auf + 0.004 cm. für die relativen Werthe zu Washington,
so ist dies reichlich gerechnet. Mr. Purnam schätzt (App. 1, p. 23) die wahrscheinliche
Unsicherheit höchstens zu vorstehendem Betrage, was also annähernd mit meiner Annahme
übereinstimmt. Allerdings wird man wegen der fehlenden Zeitbestimmungen zu Ashe Inlet
und Niantilik etwas mehr nehmen müssen, etwa — 0.010 cm. (In Norris war 1894 das
Beobachtungsprogramm auf !/; reducirt; indessen wird auch hier der allgemeine Betrag
des m. F. ausreichen).

Zur Charakterisirung der Unveränderlichkeit der Pendel mit Achatplaiten mögen
noch folgende Werthe der Schwingungszeiten zu Washington bei verschiedenen Epochen
platzfinden :

A, A, A,
1894 Januar 0:,5008405 0:.5006670 0°.5006312 Auf Schneide I
März 8405 6658 6296
April 8406 6662 6300
Mai 11 8404 6666 6304
Mai/Juni 8408 6664 6302
Juni 24 8408 6662 6302
Okt./Nov. 8400 6656 6306
1895 Januar = 6681 6298
August — 6662 6310
1896 Januar Le 6668 6315
Oktober = 6665 6303
1894 Juni 0°.5008395 0:,5006652 0°.5006283 Auf Schneide II
Nov. 8388 6650 6286
1895 Januar 8396 — =
August 8380 — —
1896 Januar 8383 = =.
Oktober 8392 6665 | 6303
1898 Januar 8396 6685 6290.

Im Durchschnitt fand sich 1894 die Schwingungszeit auf II um 13 Binh. der 7.
Decimalstelle kleiner als auf I (Ano. ke Dir EOYs

In Austin, Tex., stellte Mr. Purnam fest, dass die Schwingungsrichtung der Pendel
gleichgültig ist (App. 6, p. 311), indem die Schwingungszeiten im 1. Vertikal und im
Meridian für die einzelnen Pendeln nur um +4 Einh. der 7. Deeimalstelle voneinander
abweichen, im Mittel gar nicht.

IL 43

 
