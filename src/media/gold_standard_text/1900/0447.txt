autant) LUN

in

wi

Fi

mir | tien

an 51

 

de Bror, Frevcinet, Kater, SaBınz et autres du commencement de ce siècle ; il me semblait
dicté par l'intérêt de la science, et par le respect dû à ces savants, de ne pas mettre de coté
les résultats de ces recherches sur la valeur de la pesanteur, qui rendent témoignage d’un
zèle à peu prés indestructible de ces hommes illustres.

À cause des résultats contradictoires des observations de pendule faites par des obser-
vateurs différents même dans les derniers temps, et des nouveaux résultats intéressants, que l’on
est en droit d'attendre bientôt, la détermination de l’aplatissement terrestre d’après l’ensemble
des observations de pendule a peu de valeur, surtout puisque M. Iwanorr a déduit d’une
grande partie de ces observations la valeur de l’aplatissement 1/297, qui s'accorde avec la
valeur tirée de la constante de la précession par CazcanDrmau, Wrecuerr et G. H. Darwin.

B. Activité administrative.

1,

Le Fonds de dotations a été géré comme d’habitude. En réservant le dépôt
conventionnel des comptes exacts des recettes et des dépenses, nous donnons ci-dessous un
aperçu du mouvement du fonds pendant l’année 1899:

Récettes
Solde actif du Fonds à la fin de 1898, incluse une somme de
M. 800.— due en 1899 et payée d'avance : ete SYOM21.84:896.00
Contributions pour: 1898..." , + + 00 9 0 in on 475,00
an wi 090 2. Mo u a Era
Vente do publications 2.0000 a, "yo Ser ee „ 19,50
Intéréts eg = 1 660,30

Total: M. 161 582,00

Dépenses
Indemnité au Secrétaire perpétuel. ee 3. 2000.00
Pour le service international des latitudes . . ... 0 » 80432430
Pour d'autres travaux scientiligues. . . 2 47 0. 4 666,68
PONS d'impression. 2 6 591,08

Frêt, frais d'expedition et divers NUS 1 781,89
Total: M. 98477,00

Par conséquent, à la fin de 1899 le solde actif était de M. 63 105,00. Sur cette
somme un montant de M. 61 605,00 est déposé auprès de la banque et M. 1500,00 se
trouve dans la caisse du Bureau central pour les frais de l’administration.

Si ee

FE AD NI A or pal

 
