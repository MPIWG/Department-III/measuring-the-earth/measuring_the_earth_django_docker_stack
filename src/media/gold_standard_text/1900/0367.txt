es D qe en

a

ar

en

mar

TTL ACTE IN

difference between the maximum and minimum value of the coefficient at this station and
indicate that special observations of this character should be made at all astronomical ob-
servatories,

In closing I beg to express my regret that I am unable to attend the Paris meeting
of the International Geodetic Association on account of the pressure of other duties. This
regret is the greater for the reason that I shall in the near future resign the position of
Superintendent of the Coast and Geodetic Survey to accept the Presidency of the Massa-
chusetts Institute of Technology. I shall, however, continue to feel the highest interest in
the work of the Association and the greatest pleasure in its success.

Henry Prirongrr,
Superintendent.

 
