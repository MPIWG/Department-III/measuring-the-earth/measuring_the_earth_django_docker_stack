meme T= |

AR

in

Pwr wT iT ww

Mir TRAM T

 

ANNEXE A, IV.

ROUMANIE

Note sur les opérations géodésiques et astronomiques entreprises
pour lexécution de la carte du royaume de Roumanie présentée a la XIIIe
Conférence de l'Association géodésique internationale, tenue
à Paris en 1900

PAR

M. le General BRATIANU.
(Avec trois planches).

MARCHE DES TRAVAUX.

Puisque c’est la première fois qu’il nous est donné d'exposer, devant l'Association
internationale, l’ensemble des travaux géodésiques roumains, je crois utile d’en faire tout
d’abord brièvement l'historique.

Bien que, avant la constitution en un seul état des pays du bas Danube, la loi de
1832 ait consacré le principe de la constitution d’un service géographique et de la création
d’une carte, ce n’est qu’en 1872 que les travaux géodésiques ont pu commencer.

À cause de l'extrême urgence d’avoir une carte, on fut forcé d'adopter les prin-
cipes suivants:

1. Les opérations géodésiques qui exigent de grandes dépenses et beancoup de
temps, telles que la mesure des bases et les déterminations astronomiques, seraient ajournées
jusqu'au moment où on disposerait de temps, d'argent et surtout d’un personnel plus nom-
breux et suffisant pour des opérations aussi délicates et coûteuses.

En attendant, la triangulation géodésique du pays s’appuierait sur les données
techniques de départ des triangulations antérieures exécutées en Roumanie, et dans les pays
limitrophes par les géographes étrangers.

2. Les levées topographiques auraient comme base cette triangulation.

3. Le figuré du terrain s’appuierait sur le nivellement géodésique.

4. Pour le développement de la carte on adopterait la projection de Flamsteed
modifiée par Bonne, avec les éléments de V'ellipsoide de Bessel, comme méridien d’origine
celui de 25° à l’Est de Paris, et comme parallèle, base du développement, celui de 46°30’.

5. Enfin ce n’est que lorsque la carte du royaume serait ainsi complétée et capable
de satisfaire aux besoins de la nation, qu’on procéderait aux opérations de fermeture, dans le
but d'appuyer les triangulations précédentes sur des bases plus sûres,
