 

 

ANNEXE O, I.

Les aciers au nickel et leurs applications a la Géodésie

PAR

M. CH. ED. GUILLAUME,

Adjoint au Bureau international des Poids et Mesures.

Depuis que les mesures des dimensions et de la forme de la Terre prétendent à
un haut degré de précision, il est devenu banal de dire que les erreurs dues aux dilatations
ont été un constant obstacle au perfectionnement des mesures géodésiques: mais rien n’en
montre l'importance comme les eflorts faits depuis un siècle par les géodésiens pour diminuer
ces erreurs, par une réalisation de plus en plus parfaite de l’uniformité de la température,
et par sa mesure de plus en plus précise.

À une époque où, en plus des dificultés inhérentes au problème de l'emploi des
étalons en plein air, le thermomètre était un instrument peu digne de confiance, Borpa
créait la méthode de la règle bimétallique, et l’appliquait à la construction des quatre
doubles-toises constituées chacune par deux bandes superposées, respectivement de laiton et
de platine, ce dernier aggloméré, par compression de la mousse, en petites lames soudées
ensuite entre elles, et aboutées de manière à obtenir la longueur voulue. Ainsi donc, à
une époque où la précision des mesures géodésiques était loin d’atteindre celle que l’on
exige aujourd’hui, l’un des créateurs de la Géodésie moderne ne reculait ni devant le prix
élevé, ni devant les grandes difficultés du travail d’un métal encore réfractaire à la fusion,
dans le seul but de descendre d’un cinquième environ au-dessous des dilatations des métaux
usuels tels que l’acier, et de réduire dans une proportion encore peu importante les erreurs
dues aux dilatations.

Les deux lames formant chacune des règles de Borpa étaient directement super-
posées, et portées par une pièce de bois que recouvrait, à une petite distance, une planche
les protégeant contre les effets trop directs de la radiation.

Plus tard, Porno plaça côte à côte, dans une boîte de bois presque complètement
fermée, une règle d'acier et une de laiton, dans son appareil dont la portée était de 3 mètres.

Puis, lorsque, profitant des progrès de la technique moderne on entreprit de grands
travaux géodésiques en Espagne, et bientôt après dans quelques autres pays, on recourut
encore au procédé de BoRDa; mais, craignant avec juste raison que la superposition directe
de deux bandes longues et de faible épaisseur produisissent entre elles des adhérences
susceptibles de les déformer, on porta chacune d’elles, dans des appareils admirablement

|
|
|
}
i
}
i

as

A ED DE CS GE PE D VE

la a Rd Rens al a OS

 
