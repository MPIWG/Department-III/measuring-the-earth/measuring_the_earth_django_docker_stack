Pre
DE SE

re Dé Sérénité a mad minute
EP EPP ST PET

+ th ddr ere nee
iW ae er ie

a

sisemese

EEE ET =
TEE TR UE |

67

7°. Ein Brief von Prof. Mo4n aus Christiania mit der Mittheilung dass seine
Regierung ihn beauftragt hat als Delegirter von Norwegen an der internationalen geodäti-
schen Conferenz theilzunehmen.

8°. Eine Depesche von dem Herrn Minister der auswärtigen Angelegenheiten in Paris,
in welcher er unserem Präsidenten zur Kenntniss bringt dass, nach einer Mittheilung des
spanischen Gesandten, General Carlos Barraguer, Director des geographischen statistischen
Instituts in Madrid, den Auftrag erhalten hat der Generalconferenz in Paris beizuwohnen.

9°. Ein Brief des Grafen d’Avila vom 27” August in welchem er mittheilt, dass er
durch einen schmerzlichen Verlust in seiner Familie verhindert ist den Sitzungen beizuwohnen.

Ausser den Delegirten der verschiedenen Regierungen sind noch zur Generalcon-
ferenz eingeladen die folgenden Herren:

FRANZÖSISCHE AKADEMIE DER WISSENSCHAFTEN.

Herr Berthelot.

Herr Darbous, Doyen der Facultät der Wissenschaften in Paris,
Herr Janssen, Director der Sternwarte in Meudon (Seine-et-Oise).
Herr Lewy, Director der Sternwarte in Paris.
Herr Wolf.

Herr Callandreau.

Herr Radau.

Herr Grandidier.

Herr de Bussy.

Herr Guyou.

Herr Hatt.

Herr Hermite.

Herr Jordan.

Herr Picard.

Herr Appell.

Herr Fouque.

Herr Gaudry.

Herr Hautefeuilte.

Herr Bertrand (Marcel).

Herr Michel-Levy.

Herr de Lapparent.

Herr Cornu.

Stiindige Secretiire.

 

Abtheilung Astronomie.

Abtheilung
Geographie und nau-
tische Wissenschaften.

Abtheilung Mineralogie.

Abtheilung
Allgemeine Physik.

Herr Mascart, Director des meteorologischen Centralbureaus.

Herr Lippmann.

Herr Bischofsheim.

Herr Oberst  Zaussedat, Director des Conservatoire des arts-et-
métiers.

|
Abtheilung Geometrie.
|
(

 
