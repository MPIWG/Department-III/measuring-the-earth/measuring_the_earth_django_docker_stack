a À

Pe ue eee

ee

M a LE à pm

Wim) | NT PRIA

980.7500 cm., nachdem noch die Priifung des Wiener Magazinthermometers bei 3° einen
Fehler von 0°,8 in der Reduktionstabelle hatte erkennen lassen. 7

Erneute Beobachtungen zu Wien, von Seiten des Herrn Dr. Anpıng, im März 1899,
führten aus 3 Pendeln für München zu dem Ergebnis 980.7475 cm.

Hiernach ist für München im Mittel der 3 Bestimmungen g = 980.749 angesetzt
worden.

Herr von STERNEOK fand (auf H= 525 m.) 980.736 cm., was aber weit weniger
sicher ist, namentlich wegen Unkenntniss des Mitschwingens und wegen eventueller Ver-
schiedenheit des Gleitens des Fusskreuzes auf den Pfeileroberflächen in Wien und München.

Die Stationen Koburg und Innsbruck sind nahezu identisch mit den gleichnamigen
Stationen von Tabelle Vd und II, mit denen sie in der Schwerestörung Unterschiede von :
0.001 em. und 0.039 cm. zeigen. Im letzten Falle liegt wahrscheinlich ein starkes Mit-
schwingen des Pfeilers und Untergrundes bei der österr. Messung vor.

Im Jahre 1877 bestimmte Herr General von Orrr in München auf dem ovalen
Mittenpfeiler des Meridiansaals (H — 529 m.) die Grösse y absolut, unter Anwendung des
schweren Meterpendels des österreichischen Pendelapparats von Rersorp, vergl. Bestimmung
der Länge des einfachen Sekundenpendels auf der Sternwarte zu Bogenhausen (Abhandl. der
K..b. Akad. der Wissenschaften, II Cl, Bd. 14, 1883, 3 Abth.). Auf H =525 reducirt, ist
g = 980.737 cm. Relativ ist dieser Werth mit der Wiener Bestimmung OPpPuLzer’s nicht
streng vergleichbar, da Orrr das Mittschwingen mit dem Fadenpendel, OPPoLZER mit einem
starren Meterpendel vom halben Gewicht des schweren Pendels bestimmte.

Neuerdings sind noch 4 Stationen erledigt worden, wovon eine mit einer österrei-
chisen zusammenfällt. Im Herbst von 1900 sind weitere 11 Stationen bearbeitet worden.

VI. Messungen DURCH HerrN Prorgesor Kocx.

In Württemberg sind neuerdings relative Pendelmessungen auf 10 Stationen des
Tübinger Meridians im Anschluss an Stuttgart, und das neue geodätische Institut in Karls-
ruhe, ausgeführt worden. Herr Prof. Kock hat hierzu interessante Vorstudien gemacht,
vergl. die Abhandlung „Ueber relative Schwerebestimmungen”. (Zeitschr. f. Instrumentenkunde,
1398, p. 293). Die Reihe der Messungen sieht ihrer Veröffentlichung entgegen. Vorläufig
hat mir Herr Prof. Koch die Tabelle der Ergebnisse zur Verfügung gestellt. Für 1901
stehen Messungen auf 10 Stationen des Pariser Parallels (innerhalb Württemberg) in Aus-
sicht. Die eisernen Wandkonsole sind bereits eincementirt,

VlIe. MESSUNGEN DURCH HaRrRN PRorEssor Har.

Die Ergebnisse von 1898 sind entlehnt dem Bericht über die 27 Versammlung des
Oberrhein. geolog. Verein zu Landau am 29 März 1894, die von 1897 gefälligen hand-

 
