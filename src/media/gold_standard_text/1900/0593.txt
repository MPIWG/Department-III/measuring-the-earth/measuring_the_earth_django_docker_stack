MA dr a rb
2 ne Ten nin |

HAM ON ON)

WoW Bt

ly)

it ema iit

MI LIE RAN VI. dus Lib Ou

189

Die Zeitbestimmungen erfolgten im Vertikale de

S Polarsterns und sind sehr befrie-
digend. 5 Chronometer wurden zur

Gangbestimmung der Beobachtungsuhr während jeder
Pendelbeobachtungsreihe benutzt. Als Koincidenzuhr diente ein Registrirchronometer von

NARDIN, nur 2 mal eine Sekundenuhr von HAwELK. Die 2 Pendelreihen fallen entweder beide
auf dieselbe Tageszeit oder auf Vorm. u. Nachmittag. Letzteres fand 10-mal statt und es ergab
sich im Mittel dabei nur 6 Einheiten der 7 Deeimalstelle Unterschied der Schwingungszeiten.

Ueber die Lokalitäten der Beobachtung ist Theil II, p. 7 und 8 zu vergleichen,
woselbst auch Angaben über die Dichte des Untergrunds enthalten sind.

Der Temperatur-Koëfficient der Pendel ist für St. Z. 49,26, der Luftdichte-Koëff-
cient 542,0.

Die Aenderung der Schwin
zeit nach den Messungen in Pola

und 63), Einheiten der 7 Dee. $t.

Die an Zahl geringeren Anschlussmessungen in Suez gaben übereinstimmend damit — 4,

Die Endresultate für die nördliche Hälfte wurden später noch verbessert und ge-
meinsam mit denen für die südliche Hälfte jn der Schrift abgeleitet: Eipedition 8. M.
Schf Pola in das rothe Meer. Südliche Hälfte. September 1897— März 1898, Wissenschaft-
liche Ergebnisse, XII, Relative Schwerebestimmungen, ausgeführt von ANTON EDLEN von TRIULZI,
hk. u. ky Linienschiffs-Lieutenant. (69 Bd. der Denkschriften der math.-naturw. Classe der
kaiserl. Akad. d. W.). Wien 1899,

Diese Schrift verdanke ich dem k. u. k. hydrographischen Institut zu Pola.

Der Apparat und die Beobachtungsmethoden waren wesentlich dieselben wie bei der
nördlichen Hälfte. Vergl. über die Lokalitäten und geologischen Angaben 8. 10 der Abhand-
lung. Die Stabilität der Pfeiler wurde durch Wippen geprüft (8. 3); eine Korrektion konnte
wegen der Geringfügigkeit der Beträge unterbleiben. Es dürfte indessen dabei wohl irgend
ein Missverständniss obwalten, wodurch die Stabilität der Pfeiler grösser erschien, als sie ist.
Bis auf weiteres scheint es mir gut, der m. F. der Endresultate deswegen etwas zu erhöhen.

Die Endresultate, auch der nördlichen Hälfte, wurden wegen des täglichen Tem-
peraturganges der Pendel (nach Borrass), sowie wegen einer andern täglichen Periode der
Ergebnisse, die wohl auf tägliche Perioden in den zur Gangbestimmung der Beobachtungsuhr
dienenden Hülfsuhren zurückzuführen ist, verbessert.

Der m. F. der zu Pola relativen Ergebnisse wird sodann je nach den Lokalitäten

zu + 0.003 bis + 0.005 cm. abgeleitet (8. 9). Im Mittel nehme ich mit Rücksicht auf
die Stabilitätsbestimmung (8. 0.) + 0.006 em. an.

Die Aenderung der 4 P
Einheiten der 7 Deeim. St. der
24, 28, 35 und 63).

Von Einzelheiten bemerkenswerth ist, dass
bei einem ersten Besuch bei starkem Wind und ho
erhalten wurde, als bei einem zweiten Besuch

benutzt. Im 1 Falle gerieth das Pendel von allein
II N

gungszeiten der Pendel beträgt bei 3°) Monat Zwischen-
nur —5+5—12 —3, Mittel — 9, (P..24, 28, 85

endel betrug bei der 2 Reise im Laufe von 8 Monaten in
Schwingungszeit —25 —16 —5 — 9, Mittel — 14 (P.

auf dem Korallenrif Dädalus (& 2 u. ff)
hem Seegange y um 0.031 cm. kleiner
bei ruhigem Wetter. Nur das 2 Ergebniss ist
in Schwingungen mit wachsenden Ausschlägen.
24

 
