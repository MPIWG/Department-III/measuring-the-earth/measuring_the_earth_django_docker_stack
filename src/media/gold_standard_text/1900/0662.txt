 

258

 

Meereshöhe; doch muss wohl dabei irgend ein Irrthum obwalten. (Mit Benutzung einer
brieflichen Mittheilung von Herrn Professor ÜoLLET in Grenoble).

Grössere Unsicherheit besitzen die Höhenangaben für Frgeae und Clermont, die
aber leicht zu heben sein würden, da der Beobachtungsplatz daselbst bekannt ist. CoLLET
setzt Figeae zu 225m. an (gegen 223 bei Bior). In Padua u. Mailand scheint, der geogr.
Breite nach, auf den Observatorien beobachtet zu sein; dann dürften die Höhen vielleicht
um ca. 1Om. zu vermindern sein, was ich vorläufig unterlassen habe.

Die Bodendichtigkeit ist für Mgeac und Clermont ebenfalls nach Corrgr ange
nommen, sonst nach verschiedenen anderen Quellen oder auch nur geschätzt (Klammer-
werthe).

Werden die Bıor’schen Messungen als absolute aufgefasst, so leiden sie wie die
von Borpa an dem Mangel, dass für die Einwirkung der Luft nur die Korrektion wegen
Auftrieb angebracht ist. Bekanntlich ist dies ungenügend. Prirck hat die absoluten Bestim-
mungen für Paris von Borpa und Bior nach Srorzs’ Formeln genauer reducirt, vergl.
U. S. Coast and Geodetic Survey, Report for 1881, App. 17. (1882) p. 105. Nach Bror ist
die Linge L des mathem. Sekundenpendels in Paris gleich 0.993845 m.; dazu die eben
erwähnte Korrektion + 0.000062.7 m. und + 0.000005.0 firs Mitschwingen der Konsole,
giebt L= 0.993913 m., oder y = 980.953 cm. (H = 75 m.).

Aus Borna’s Werth L= 0.993827 m. (letzte Stelle 6 nach Bior) leitet Princz ab
0.993918, was g= 980.958 cm. (H — 67 m.) giebt.

Auf die relativen Angaben gegen Paris haben die verbesserten Reduktionen wenig
Einfluss. Herr Dr. Furrwängter hat auf meinen Wunsch die Berechnung bewirkt und in
Einh. der 3. Decimalstelle von y gefunden: +1 für Bordeaux, O für Figeac und Clermont
+3 für Dünkirchen, Leith Forth und Unst. Ich habe diese Werthe vernachlässigt.

Am Schlusse des Recueil, p. 585—588, giebt Bror noch Mittheilungen von Beob-
achtungen in Paris und Greenwich, mit zwei invariablen Forri’schen Kupferpendeln. Bei
diesen Messungen wirkten Arago und Humsonpr mit. Busser hat dieselben sorgiältiger
reducirt (Untersuchungen über die Länge des einfachen Sekundenpendels, Art. 23). Da indessen
die Ergebnisse nach beiden Pendeln für den Unterschied von g um 0.034 cm. von einander
abweichen und für Paris-Greenwich eine gute Bestimmung von DrrroreEs vorliegt (jetzt
auch noch eine neuere von Purnam), habe ich diese vereinzelte Bestimmung weggelassen,
obwohl sie von jener sich nur um 0.010 em. unterscheidet.

X1ld. MESSUNGEN DURCH FREYOINET UND DURCH DUPERREY.

Voyage autour du monde pendant les années 1817, 1818, 1819 et 1820, ede. ; par
Louis DE Freyomer. Observations du Pendule. Paris 1826.

Notice sur les Bixperiences du Pendule invariable, faites dans la campagne de la cor-
vette d. 8. M. la Coquille, pendant les années 1822, 1828, 1824 et 1825. Par L. I. DuPERREY.
(Connaissance des Temps 1830, Additions, p. 83—99.) |

 

=|
:
a
{

®

Era

ARTE,

dns

RATE RS

1

ETAT UT

22 acuité e dau hailt aa label ai san 10 nt tte Weir dé pa dr Sn D LG RL SE i ni ÿ am + nr Vas
an. Lau 4 sua os PA eter dee be bb ihn nein déni ER ET Re FA a aha aa à da ee

|
4
|
|

 
