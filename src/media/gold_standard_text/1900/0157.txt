ee

ve ikl eae

IT

ent

 

am ee TdT MUL AR @

azimuths of India, because the cumulative azimuthal error of the triangulation is so large.

(d.) Though the difference in azimuth between two rays can be more accurately
determined by triangulation than by astronomical observations, when the rays are not distant
from one another, yet the errors of triangulation tend to accumulate, and at great distances
from the origin, such as Cape Comorin or Moulmein, the accumulated error of the triangu-
lation exceeds the error that local attraction is liable to produce in an observed azimuth.
It is in fact a question whether the triangulation of Burma should not be brought into
accord with an azimuth observed in Burma, and its orientation thus corrected. Though an
azimuth in southern latitudes does not furnish a reliable value of local attraction, yet,
considered only as an azimuth, it is more trustworthy for purposes of triangulation than
one observed in northern latitudes.

S. G. BURRARD,
Major R. B.

18

 
