 

der festgelegten Höhenmarken mit Angabe ihres Abstandes von der vorangehende Marke

238

VERZEICHNISS

und ihrer Meereshöhe.

© bedeutet Höhenmarke der ste Ordnung (Bolzen).

 

 

 

 

 

 

x „ ” ” Re » (Kreuz).
ART DER GEGEN- | HOHE ÜBER
en ORTSNAME SEITIGER ad ANMERKUNGEN
ABSTAND

© Holmen skydsstation. .... 7.0 Km. 1)| 192.253 Meter | 1) von Nyvolden
© Nesset Hos., a AAS 1898199 5. 3
© Südl. v. Bjorgevolden . ... 5.0 , | 182.596 ,
© Südl. v. Kirkestuen ... ... 5:B:,;, 182.800: 3,
© Promenes ol" ie IB, 183.044.
© Bletäd Hoss a2 2.0. 55, 40, 184.097:
© West u, Yaala bre 0 Da, 1944502
© HO NEO os. à Éd, 2040971;
© Fortetten plades . .. ...- 49, 188.120° ,
© West v. Listad skydsstation. | 8.9 , 210918 5,
© Ost: Bolbraa selor i) » As 238.802: »
© Beisel. Ga Bok 567. 243-098;
SL Bei Amundbakken 2. ... .. Di: SCIE;
© Kyam krche 2.0.02... Do + 259.097 7,
© Hegoerusten Hof. ...... Ain, 260.508 —,
© Kolben HOT ces ne ins 3:05 313.904  ;,
© Strandviken plads ...... 2.8, 295:607 :
© Sell Distriktsgefängniss 42, 286.862 ,
© Kringen Denkmalstätte. . . . | 2.6 , 384.148 55
© Skrädderrusten Hof. . . . .. D es 2 989.140 52,
© Sell Kirche... .. + . ae, 399.920, os
© Ulen-plaide De + 300572. 5
>< bauzsaara bo... . 02... CA, 302825. 5
© Sandbakken plade: ..,„. :,.:16 „ Does
© Stampesvingen : + , . . . .. Dors 309.189
© Oster. Rusten Hoi... 2.2. 33, : 4020404 >
© Brändhangen skydsstation . . | 5.2 „ AA OV
© Malleraas bro .: «1e GeO |, 45.888,
© Povie aeirche ss 2a He, 478.861 5,
© Toptemoen skydsstation . 20, 468.882 „
x Unterhalb Hjelle Hof ... Bl, Dee el oy
© Ost y. Holen pad... 1.9, 002.087, ,
© Domaas skydsstation..... 6 » | 642.690 …,
© Hondora bro 4. 2. D.) 938.120, 5
© Hokssuasen so, 02,3 en 981.525

 

 

 

 

 

 

 

|

ans abn na

 
