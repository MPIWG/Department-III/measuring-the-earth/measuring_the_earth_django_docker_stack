 

NACHTRAG.

I_IV. Oxsrerruicu-Unearn, p. 139.

Herr Oberst v. STERNECK hatte am 12. Okt. 1901 die Güte mir mitzutheilen, dass
gegenwärtig 42 Stationen in der Umgebung des Plattensce’s beobachtet werden. Diese
Stationen liegen in einem Raume von 80 Km. Länge und 60 Km. Breite.

V. NORDDEUTSCHE BEOBACHTER, p. 192.

V5. Neumayer’s Arbeit ist inzwischen erschienen: „Bestimmung der Länge des ein-
Jachen Sekundenpendels auf absolutem Wege, ausgeführt in Melbourne vom Juli bis Oktober
1863, von Dr. Georg Neumayer” (Abhandlungen der k. bayer. Ak. der Wiss. II. Ol. XXI.
Bd., 1901) 9.

Vd. Professor Borrass hat neuerdings noch Charlottenburg (1900) und die Haupt-
sternwarte Pulkowa (1901) an Potsdam angeschlossen. Herr HAASEMANN bestimmte 1901
noch 10 Stationen im Gebiete des Harzes und bei Stassfurt. Im Sommer 1901 hat Herr
Dr. Hecker auf meine Initiative hin den Versuch gemacht, die relative Grösse der Schwer-
kraft auf dem Meere auf der Route Hamburg— Lissabon— Rio de Janeiro durch Vergleichung
von Quecksilberbarometern und Siedethermometern zu bestimmen. Bei dieser Gelegenheit
sind auch Pendelbeobachtungen in fio, Lissabon und Madrid ausgeführt worden.

Vg. Die topographischen Reduktionen sind für

N°. 8. Kriegschiffhafen + 0.003 cm. | N°. 10. Debundscha + 0.007 em.
N°. 9. Kamerun + 0.001 em. N°. 20. Kapstadt ++ 0.004 cm.

VI. Sünnkursche BEOBACHTER, p. 208.

VIs. Herr Professor Kock hat für die in der Tabelle angegebenen Stationen endgültige
Werthe veröffentlicht in der Abhandlung:

1) In dem auf Seite 522 eitirten Schreiben, das ich am 20. März 1899 an Herrn N. gerichtet habe, steht
gleich anfangs ein störender Zwischensatz: „infolge eines Berichtes vom 18. September 1896”; dieser Satz dürfte aber
nicht von mir herrühren, da er in einer im Geod. Instit. verwahrten Abschrift des Schreibens sich nicht vorfindet.

 

 

|
|

RL SE PP PAR EN ni ai LÉ SE ERAS EEN To Eh TIE

aus chi a ha 0
A AA MMM LT THE

 
