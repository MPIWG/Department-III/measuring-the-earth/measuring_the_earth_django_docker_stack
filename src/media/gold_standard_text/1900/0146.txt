 

ER

138

50 yards wide by 150 long, the direction of the length being north and south. The station
is at the northern edge. Similar small hills are scattered over the country at intervals,
the nearest being about 1'/, miles to the north. With the exception of these small hills
the country is flat.

Bhaorasa is situated on the highest point of a small hillock of sand stone, 1387
feet above mean sea-level, which rises very gently from the general level of the plain to
the south and west, but falls more abruptly to the north and east. The Betwa river runs
by the eastern end of the hill at a distance of about 1'/, mile from the station. The
height of the station above the general level of the plain is under 100 feet.

N. E. End of the Base. The station lies in the plain to the east of Kalianpur and
is 1481 feet above mean sea level. The plain is perfectly flat and the horizon almost
unbroken, except to the west, where the edge of the plateau rises slightly above it.

Kalianpur. The station which is 1765 feet above mean sea-level is on the highest
of a series of rolling hills or downs, which form the eastern edge of an extensive plateau
about 170 feet higher than the plain to the east. The edge of the plateau runs north and
then north-east and disappears in the distance; it is somewhat higher than the central
parts and more undulating. The town of Sironj lies about 21/, miles to the south-east.

Surantal. The situation of the station is very similar to that of Kalianpur. It is
1802 feet above mean sea-level. It is from a point very near this station that the edge of
the plateau turns towards the east.

Losalli is situated 1749 feet above mean sea-level, in the middle of the plateau
on perfectly flat ground, which is slightly lower than the undulating country to the west.

Tinsia is situated very similarly to Kalianpur, but on the western edge of the
plateau. It is 1776 feet above mean sea-level. The station is surrounded for miles by
dense jungle but is not far from a track which runs from Sironj to the valley of the Parbatti.

Kamkhera is on a flat topped hill near the southern end of the plateau. Its height
is 1780 feet above mean sea-level.

Ahmadpur is 1715 feet above mean sea-level, on a conspicuous hill of almost solid
rock which rises to a height of over 200 feet out of the low plain to the south of the
Kalianpur plateau. The ascent from the east is easy but on the other sides somewhat

ROSES EE A ETE

CE POELE CITE TEE Ton el en

LT

ER EEE ER AR TEE LE EE M TPE TRS

NE

FR SE nn ur
laut
1st

er

: precipitous in places; there are many similar hills at intervals on every side but none 1

A so large. The nearest is a small one about two miles to the south-east. The plain between

A Kamkhera and Ahmadpur is 1480 feet above mean sea-level.

/ 38. If I may sum up the lessons that have been learnt from the Kalianpur obser- ij
vations they are as follows: |

(a.) It is unsafe to deduce a value of the latitude of Kalianpur from the observed
latitudes of India, because the latter are liable to a constant error, caused by external forces.
(2.) It is unsafe to deduce a value of the longitude of Kalianpur from the observed
longitudes of India, because the latter are too few and are too largely affected by errors
is in the adopted values of the earth’s axes.
(c.) It is unsafe to deduce a value of the fundamental azimuth from the observed

 
