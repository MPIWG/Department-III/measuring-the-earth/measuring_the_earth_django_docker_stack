 

444

 

Pag.
XIV. Messungen durch das geographisch-statistische Institut in Madrid. 4 - 286
XV. Messungen durch italienische Beobachter, . , . . . . . , .. 289
XVI. Englische Beobachter. . . . er 298
XVII. Messungen durch die Coast and Geodetic Suivey U. S. i a 330
XVII. Messungen durch japanische Beobachter. . . . . . 2 202 358
ANDERE a ne a ted 363 ;
Nachtrag... u. r‘; 378 |
Beilage B. X. Bericht über die im Jahre 1900 gusgolubilien rélativen Bendelbaobadhiungen
von Prot Drs Heid. fo 2: . + 3886—392 — |
Beilage B. X1. Bericht über die Triangulationen 1900. dine ia Sr. eh, des |
Generals A. Ferrero erstattet von F. R. Helmert und A. Börsch. Mit
einer Uebersichtskarte. . . . abi... 393—398
Annexe B. XII. Nouvelle mesure de l'arc du Pérou par M. Ie Gener al ‘Boek ES 399—402
Annexe B, XIII. Rapport sur le projet de revision de l'arc méridien de Quito par M. 5
ne se a he nee ee 107.
C. Memoires scientifiques. — Wissenschaftliche Mittheilungen.
Annexe CG. I. Sur un appareil zénitho-nadiral par MA Connu. =: 225 420—423
Annexe GC. Il. Les aciers au nickel et leurs applications à la Géodésie par M. Ch. Éd.
Guillaume, adjoint au Bureau international des poids et mesures. ... 424—438
TABLE DES CARTES ET DES PLANCHES.
VERZEICHNISS DER KARTEN UND TAFELN.
Pag.
Uebersichtskarte über den Stand der systematischen Lothabweichungs-Berechnungen in Centraleuropa. 28
Abbildung des Zenitteleskops. . . . eo 08
2 Uebersichtskarten der Messungen der hnackvafi Se ‘ 9

Uebersichtskarte tiber das geodätisch- astronomische Polygon um das este Miltshnenphecken ee

i
i
j
1
3
4
4

 
