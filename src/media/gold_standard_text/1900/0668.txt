 

264

Temp. Beob. und (1) + (2) + Mittel NE
Guam. auf20° red. (3) + (4)
I 1819 25. April 29.7 9022449 1.71)
oF eee Do 15 202289 0.80)

II 1819 28. April 29.9 89045:09 1.3). 89045.69 1579
Ill 1819 29. April 29.0 °90056.07. 165 90.056.07 1.67

1) Diese M,? enthalten (3) noch nicht, das erst am Stationsmittel anzubringen ist. Auf eine Vergrösserung von
(2) wurde auch hier verzichtet. .

Bei Guam ist ausser der verb. Vacuumred. noch — 1.38 wegen Uhrganges ange-
bracht. Ich finde nämlich aus den Abendzeitbest. den tägl. Gang der beiden angewandten
Uhren gleich 2.37 und 15%.31; aus den Morgenzeitbest. 8°.36 u. 16°.40, im Mittel 2°.87 u.
15°.86 gegen 4.28 u. 17°.10 nach Frevcmer’s Annahme. Hieraus folgt — 1°.32 Korr. im
Uhrgang, oder — 1.38 red. auf rund 90000 Schwingungen.

Maui.

I 1819 19. Aus, 27,7 0029626: 24. |
200 26.3 241.96 1.6 | 90 240.42 0.98

>

Port Jackson.
1: 1819.80 Nov. 201: 9027829 19 9027822 59

11:1819. 1. Dee: 208. 890976, 1,6 |

a) en
(IL 1819 ‘2 Dec 2153 W1lodıL 19

Be OR 1085. 0 | ie
Falklands-Inseln.

Il 1820 12. April 11.8. 89108.91 3.8 89 165.91 3.8.

Aus den Unterschieden vergleichbarer Nachbar-Beobachtungswerthe fand sich das
pach $ (1) + (2) + (8) + (4) } berechnete mittlere Fehlerquadrat für grosse und kleine Werthe
bis auf +1}, seines Betrages, im Mittel bis auf !/jo bestätigt. Für Paris sind im vorste-
henden 3 Werthe des M,? der Endmittel für jedes Pendel angegeben; der erste ist aus den
geschätzten m. Fehlerquadraten berechnet (a priori), der zweite aus den Verbesserungen der
Einzelbeobachtungen, der dritte aus den Partialmitteln. Von den letzteren beiden Be-
rechungen (a posteriori) ist der zweite sehr unsicher. Im Mittel geben die 3 Werthe a priori
0.19, die drei ersten a posteriori 0.18. Auch hier ist also gute Uebereinstimmung.

Auf den Stationen ausserhalb Paris ergiebt die Berechnung von M? a posteriori
aus den Einzelwerthen sich im Durchschnitt um '/, grösser als die Schätzung nach der

Formel a priori.

A A D D ED EG M 2 de LR ana. 4 ee

 

à à LG ER ou a hé D seb LI NTS i

 
