 

EE RER

|
|
|
|

ie

 

the earth’s axes adopted. On page 447 of Volume XV of the Great Trigonometrical Survey
of India, Colonel G. Strahan has calculated the probable deflections of the plumb-line at

f
i

 

the several stations, on the supposition that the plumb-line at Kalianpur is truly vertical. à
Omitting Peshawar, Quetta, Jalpaiguri and Dehra Dun, we can divide the remaining 20 zi
! stations into the following four classes: a
(a.) Stations near the meridian of Kalianpur, ii
i =
| Atınmar ....:. Deflection of plumb-line + 1.05 W.
i AeA i 5 5 5 32,85 W.
h Poly 3 5 2 — 1.30 D.
; BOLUM iw 10. 5 x = —. 0.03 BE.
À Bangalore, .... 5, x 5 2300ER:
i Nagarkoil...... ee yo APE.
: Mean + 07.59 W.
À (4.) Stations in Western India,
: BOmpay «5s Deflection of plumb-line -++ 3.30 W.
i Mensalors ..... ‘ 3 % 0.00
N Mooltlan 01722 È 3 — 9.60 E.
i Mara = ; i 3,103 +
N Dessa... 5. 0. ‘5 5 2 = 7.50 8: zi
L Mean — 4.88 E. =
À (e.) Stations in Eastern India, =
à Fyzabad cs aie. Deflection of plumb-line + 2.85 W.
f Jubbalpore : 4,2, 5 i 5 + 81:5); 5.
\ Made. ohn ; LA Y ae YAO,
i Waltaire 2.2... 5 5 5 + 07,75 W.
N Calcul 0.0 | : ; : 8.80... |
| Mean — 2.78 E. |
I (d.) Stations in Assam and Burma,
À Obittegoneen. Deflection of plumb-line — 1.50 E. jj
| AKyab es Ba, a 1
| Prome 3% ; : 5 3754 4
| Moulmein 225 es 4 ‘ = 3400:
. Mean — 2"48 E.
| The preponderance of apparent easterly deflections can be eliminated, if a westerly
hi deflection at Kalianpur of 3” be assumed; as however Captain Lenox-Conyngham’s obser-
I
i
\

 
