en

tet (Mur AU

bail mil I RUN

2 rl I Pet |

273

Mount Hamilton . . . . 0.037 em. Abw. — 0.024 cm.
San Francisco ..... BD a) in 45;
Washington . 25." 69, + 2
Donna sa en DAS + Ads
Salt Lake City ..... ale, — 34 ,
Chiang ie Lu al, Ae Oi oy

Mittel 0.061 cm.

Wollte man beiden Arten von Bestimmungen gleiche Sicherheit zuschreiben, so
würde als m. F. einer Bestimmung =+.0.007 em. folgen, als m. F. eines Anschlusses an
Washington 0.010 cm. Vielleicht wird eine endgültige Reduktion der Messungen von
Derrorgzs noch zur Verminderung der Unterschiede beitragen.

Die Mittheilung über die Ergebnisse in Russland, in Bd. 120 der C. R., ist eine
ganz vorläufige. Sie enthält nur aufs Meeresniveau reducirte Beobachtungswerthe von y aus
den relativen Pendelmessungen und gewisse normale Werthe; geogr. Positionen fehlen fast
ganz. Die geogr. Breiten sind aus den Normalwerthen ableitbar; aber für Taschkent ist eine
falsche Breite benutzt. Ausserdem ist der Beobachtungswerth für Pulkowa grob entstellt. Es wäre
zu wünschen, dass diese werthvolle Reïhe, bei welcher Herr Professor Wirrram die Zeitbestim-
mungen besorgte und die Hälfte der Pendelmessungen bewirkte, bald definitiv reducirt würde )).

XITe. MESSUNGEN DURCH VERSCHIEDENE BEOBACHTER-MIT PENDELN DES SERVICE GÉOGR.

Herr G. Bicourpan beobachtete 1893 gelegentlich der Expedition für die Sonnen-
finsternis mit einem invariablen Reversionspendel des Service g&ographique in Joal; vergl.

6. R. 1894 t. 120, p. 1095. Die Messungen fanden statt in einer doppelwandigen Hütte

mit nur 2° täglicher Temperaturschwankung. Anstatt einer machte man 4 Bestimmungen,
welche im Mittel einen m. F. von + 0.003 em. gaben (wobei zu bemerken ist, dass selbst-
verständlich für eine solche vereinzelte Pendelmessung nach der Theorie des Reversionspendels
gerechnet werden musste.) In Paris wurden vor und nach der Expedition Anschlussmessungen
ausgeführt, die völlig übereinstimmende Resultate ergaben. H ist nicht angegeben; ich habe
es aus den Vergleich der Werth von y im Meeresniveau und am Beobachtungsorte erschlossen.

Herr Professor J. Corner in Grenoble hat sich die Aufgabe gestellt, den Verlauf
der Schwerkraft in der Nähe des Parallöle moyen zu studiren. Er benutzte ein invariables
Reversionspendel Huerz N°. 3 nach Derrorces aus den Werkstätten des Service géographique.
Ueber seine Arbeiten ist berichtet ©. R. 1894 t. 119 p. 634—637, 1896 t. 122 p.
1265—1268, 1897 t. 124 p. 1088—1091 und 1900 +. 130 p. 642; ausserdem verdanke
ich Herrn Conte noch handschriftliche Mittheilungen und Korrekturen.

Den Ausgang bildeten Messungen in Paris, Observatoire, Platz von Derronces. Zur

irrthümlich auf der Westküste des Kaspischen Meeres angegeben,

 
