LE nn |

ipa arin) HU

inant it wt

Kai

au

a EICH

   

415

C'est que ce devis a été établi avec grand soin et avec un souci constant d'éviter
les doubles emplois et les dépenses inutiles. I] ne semble pas qu'il puisse être réduit.

Kn effet, on doit tenir compte de ce fait que, malgré la bonne volonté du Gou-
vernement de l’Équateur, son concours sera forcément limité.

Il pourra fournir à la mission une escorte et des hommes pour les transports.
Mais cette escorte et ces auxiliaires devront probablement être payés.

Sans doute, on peut espérer que le crédit demandé ne sera pas entièrement dépensé ;
les évaluations ont été faites largement et en tenant compte d’éventualités qui ne se pré-
senteront peut-être pas. Cela était nécessaire, afin d’être assuré que le devis ne serait pas
dépassé; mais en escomptant les circonstances heureuses et en réduisant d’avance le crédit,
on s’exposerait à des surprises. |

Il ne semble pas qu'aucun des articles du devis puisse donner lieu à une contestation.

Il était nécessaire, par exemple, de prévoir le cas où quelques-uns des officiers
auraient besoin d’un congé pendant une campagne si longue et si fatigante.

Il était nécessaire d’emmener un mécanicien pour -faire sur place les réparations
des instruments; car le renvoi des instruments en France, à cause du prix des transports
et des délais qui résulteraient d’un aussi long voyage, entraînerait des dépenses considérables.

Enfin, tandis que la mission principale partira au mois de février ou de mars
prochain en vue d’une campagne de quatre ans, deux officiers partiront six mois plus tôt,
en septembre 1900, afin de préparer les voies et d'acheter les animaux destinés aux transports.
Il est évident que cette disposition produira en définitive une économie notable.

ÉTENDUE DE L’ARC.

Il nous reste à traiter deux importantes questions, La première a été soulevée par
la Lettre ministérielle elle-même.

»Toutefois, dit M. le Ministre, il y a lieu de considérer que la dépense pourrait
»ètre réduite dans une assez forte proportion s'il était possible, sans inconvénient scientifique,
»de réduire l’arc actuellement prévue de 6° à 4°,5, de la base de Colombie à la base de
»Targui; on supprimerait ainsi la partie la plus difficile des travaux... Je prierais l’Aca-
„demie de me faire connaître son sentiment sur la question de l’amplitude de l’arc à mesurer
„et de me dire si la mesure d’un arc de 4°,5 lui paraîtrait répondre suffisamment aux
»besoins de la Science.”

L’are mesuré au XVIII’ siécle s’étendait de la station de Mira, par 0°35’ N., jusqu’à
la base de Targui, par 3°10’8. Il s’étendait ainsi sur environ 3°,5. Il est question de le
prolonger vers le nord jusqu’à Cerro de Pasto, par 1°12'N., soit de trois quarts de degré
environ, et vers le sud jusque sur le territoire péruvien par 4°55'S., soit d’& peu prés 1°,5.

La proposition visée dans le paragraphe que je viens de citer consisterait suppri-
mer le prolongement vers le sud, ce qui réduirait l’are à 4°,5.

Nous devons d’abord remarquer que l’are à mesurer doit être combiné avec des
ares de grande amplitude pris dans les latitudes moyennes et qui ont une vingtaine de

 
