 

E
5
=
E
E
:
E
È
=
=

nach ham A AMMA Msn

did
Tear

 

13
Comme, suivant l’Art. 11 de la Convention, les votes pour ces nominations ont
lieu au sein de la Conférence par États, nous ne doutons pas que les délégués de chaque
pays se rendront à Paris munis des instructions de leurs gouvernements à cet égard.
Nous avons done l'honneur de convoquer Messieurs les délégués de PAssociation
géodésique internationale à la XIII Conférence générale à Paris, pour le 25 septembre
prochain, 4 2 h. de l'après-midi, au Palais de la Nouvelle Sorbonne.

Le Secrétaire, Le Président,
Dr. An. Hirsch. H. Faye.

Le 8 août il Pa fait suivre de la circulaire suivante indiquant, d’après Art. à
de notre Convention, l’ordre du jour proposé pour nos séances:

Monsieur et très honoré Collègue,

Par la circulaire du Bureau de l'Association géodésique internationale, Paris et
Neuchâtel, le 12 mars 1900, Messieurs les délégués ont été convoqués à la XTIT Conférence
générale à Paris, le 25 septembre à 2 heures, au Palais de la Nouvelle Sorbonne.

D’après l’article 2 de la Convention de 1896 nous avons l'honneur de vous indiquer
l’ordre du jour de la session.

1. Ouverture de la séance par Monsieur le Ministre de l’instruction publique et
des beaux-arts.

. Rapport du secrétaire provisoire.

Nomination d’un secrétaire perpétuel.

Rapport du Directeur du Bureau central.

Rapport sur les observations de latitude dans les 6 stations internationales.

. Discussion sur les points 4 et 5.

Programme du Directeur du Bureau central pour les travaux des années suivantes.

. Rapport de la commission des finances.

. Budget provisoire pour les années suivantes,

. Rapports spéciaux :

OIA MAW

pas
(=)

a, sur les triangulations par M. le général Ferrero,

b. sur les mesures de base par M. le général Bassot,

c. sur les nivellements de précision par M. le vice-amiral von Kalmdr,

d. sur les maréographes par M. Bouquet de la Grye,

e. sur les déterminations de longitude, de latitude et d’azimut par le Bureau
central (M. le prof. Albrecht), :

f. sur les déviations de la verticale par le Bureau central (M. le Directeur Helmert),

sur les déterminations de la pesanteur par le Bureau central (M. le Direc-

teur Helmert).

>

11. Rapports nationaux.

Dr

EEE

 
