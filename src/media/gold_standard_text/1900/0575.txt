up min nme a ai names 9»
2 METTENT |

aan Kin

ten

Smet

ME LRO AL POBIAG HO UO

171

werden müssen, so liegt dies z. Th. an mangelhaften Uhrvergleichungen oder mangelhaften
Gang der Chronometer, z. Th. an der Anwendung nur eines Chronometers zum Transport
des Uhrstandes vom Bord an Land, z. Th. wohl auch an der Entnahme des Uhrganges
aus fremder Hand. In letzter Beziehung deutet HERRMANN auf Rangun hin, wo, wie die
Tabelle zeigt, drei missstimmige Werthe vorliegen.

‚ Vielleicht sind in einigen Fällen die Stationen zu nahe der brandenden See ge-
wesen und der Einfluss der Erschütterungen nicht beachtet, etwa weil er sich mit dem
mangelhaften Gange der Kontaktuhren vermengt hat oder aus Mangel an Zeit zu genauer
‘Ueberlegung:.

Da nun aber nach und nach alle Stationen wiederholt besucht werden dürften, so
wird doch allmählich ein einwandfreies Material entstehen, wie ja jetzt schon auf diese
Art gegen 20 Stationen sicher gestellt sind ; ausserdem unterliegen noch ebenso viele keinen

Bedenken. Zweifellose Bedenken wegen innerer Widersprüche sind durch ein Fragezeichen
angedeutet.

Es mögen jetzt einige Einzelheiten der 11 Reihen folgen.

b. Grarzu hatte 1892, 2 Pendel, deren mittlere Schwingungszeit Sm sich nur um
+ 14.1077 Sec. (P. N°. 23 um + 385 N°. 24 um —5) änderte, und eine Halbsekunden-
Pendeluhr von Hawkık zu den Koineidenzbeobachtungen. Er schloss doppelt an Wien an
und konnte überall (4 Stationen) gute Pfeiler und vorhandene Lokale benutzen. Aber ausser
Kdinburg beruhen seine Werthe auf der Annahme konstanten Uhrgangs für 3 Chronometer
in 32 Tagen. In der That stimmen die Vergleichungen derselben an den drei Stationen mit
dieser Annahme gut überein, ausserdem ist das Ergebnis in Zromsö mit dem norwegischen
Ergebnis (Tab. IX) ziemlich im Hinklang. Hiernach und mit Riicksicht auf die Missstim-
migkeiten der Reihenmittel Sm (S. auch Edinburg) wurde man den m. F. fir diese Stationen
in g relativ zu Wien auf nicht ganz + 0.020 cm. zu schätzen haben. Leider ist gerade in
Spitzbergen die Landung erschwert gewesen, Schnee und Regen drangen in den Beobacht-
ungsraum, und da nur ein Chronometer zum Uhrgangtransport diente, unterliegt das
Resultat Bedenken, wozu noch kommt dass wenig Zeit zur Verfügung war und die 4
Schwingungszeiten sehr starke Unterschiede aufweisen.

Spitzbergen ist daher als zweifelhaft bezeichnet.

In Edinburg sind an 3 Tagen je 2 Reihen zu 2 Pendeln genommen und an Stern-
wartenuhren angeschlossen. Der m. F. des Ergebnisses beträgt trotzdem etwa == 0.012 cm. in I:

c. Minter von ELBLEIN benutzte 1892—94 3 Pendel (N°. 25, 26 u. 27), deren
Sm in 2 Jahren um — 72.10 See. sich änderte, welcher Betrag proportional der Zeit ver-
theilt wurde. Im Anschlusse an Wien erledigte ELBLEIN ausser Pola 15 Stationen in Asien
und Australien, wobei er meist Souterrainlocale (P. Sandwich, Tangoa Holzhäuschen) be-
nutzte und Sorgfalt auf die Pfeilerfundirung verwandte. Nach der Aufstellung der Apparate
wurde in der Regel !/, Tag bis zum Beginn der Beobachtungen gewartet, bei denen als
Koineidenzuhr eine Halbsekunden-Pendeluhr von HawaLk diente, deren Gang in der Regel
für jede Reihe (meistens 3 oder 4) von 3 Pendeln mit ca. 3!/, Stunden Dauer durch

 
