er Tee TESTES wt

LL BI

LOA | me wah

117

the recent surrounding group of latitudes and azimuths had been observed. It had always
been noticeable that the value of (O—C) in latitude had a tendency to be negative, and
General Walker explained this tendency by the theory that the plumb-line at Kalianpur
was deflected 2” to the south. On page 804 of India’s Contribution to Geodesy he writes:
wit well be seen that, of the 148 astronomical latitudes available..... there are 90 cases
„of negative excess to 58 of positive excess; but if the latitude of Kalianpur is diminished
»by 2.0, the whole of the geodetic latitudes will be correspondingly diminished and this
„will make the number of positive and negative cases almost exactly equal”.

In comparing the number of cases of negative and positive excess it is very diffi-
cult to decide whether to reject certain latitudes or not. It frequently happens that two
or more astronomical latitudes have been observed within a few miles of each other, and
it is questionable whether in discussing the latitude of all India we should regard each
value as an independent latitude, or whether we should adopt. the mean of the group. For
example General Walker gives the cases of two latitudes observed within 5 miles of one
another at Madras (p. 782); of six latitudes observed within a radius of 4 miles near
Punnae (group 1, p. 778); and other cases of groups of four and of three latitudes
respectively (groups 2 and 3 p. 778). No question would arise in this connexion, were it
not that these groups have been mostly observed in the areas of positive excess, thereby
unfairly increasing the number of positive excesses; and when General Walker assigns
99 and 58 as the respective numbers of negative and positive excess, he is giving full
weight to every observed value of every group. But if we take the mean of a group spread
over a small area as a single value of latitude and thus give equal weights to equal
areas, we find that (including the latitudes observed in the last two years) there are 101
instances of negative excess and 45 instances of positive excess. Though this difficulty in
the matter of combination and rejection does exist, General Wadlker’s deduction of the
meridional deflection at Kalianpur from the Indian latitudes as a whole has met with
general approval, and of recent years a southerly attraction of 2” at Kalianpur has been
accepted as a working hypothesis in explanation of the excess of negative values.

7. In the case of the fundamental latitude General Wadker estimated the deflection
of the plumb-line after the Indian triangulation had been computed, but he deduced the
effect of local attraction on the fundamental azimuth before the reduction of the triangulation
had been carried out. Thus the triangulation is based on an observed latitude, uncorrected
for local attraction, and on a derived azimuth, corrected for local attraction. The computed
geodetic values of latitude are based on the observed latitude of Kalianpur, but the
computed geodetic values of azimuth are based on the derived value of Kalianpur. On
pages 137 to 141 of Volume II of the Great Trigonometrical Survey of India General
Walker explains that of 85 stations, situated in different parts of India where astronomical
azimuths had been observed, the value of (O—C) proved to be negative at 26, and positive
at 9; and he arrives at the conclusion — hitherto generally accepted — that the adopted
value of the fundamental azimuth at Kalianpur was too great. He then collected all the
stations at which azimuth had been observed, omitting those near the Himalayas, and

 
