ze a az

Ama

ii

MORE LOT de a Lib eu

it pea LE

333
geführt sind und von denen 8 von Parace selbst, je eine von Preston und Very mit seinen
Apparaten besucht wurden, habe ich in die neue Tabelle nicht aufgenommen, da wenig
Aussicht ist, dass die Ergebnisse abgeleitet werden.

XVIIé. Mussunern purcu Mr, B. D. Prusron 1883 und 1888—490,

Appendix N°, 14, Report for 1888: Determinations of latitude and gravity for the
Hawaiin Government. Hierin sind auch Beobachtungen von 1883 enthalten.

Appendix N°. 12, Report for 1890: Determinations of gravity etc. in connection with
the U. 8. Expedition to the west coast of Africa, 1889— 90.

Die Beobachtungen von 1883 und von 1889/90 wurden gelegentlich einer Sonnen-
finsternis-Expedition ausgeführt. Ueber die Lokalitäten von 1883 ist App, 14, p: Sal u.-f.
zu vergleichen, für die Messungen von 1888 aber App. 14, p. 474/5 und weiterhin p. 524
sowie p. 529/530 mit geologischen Angaben, endlich für 1889/90 App. 12, p. 627/628 u.
p. 632 u. f.. Auf St. Helena wurde 1890 Fosrgr’s Station wegen lokaler Verhältnisse nicht
genau wieder gewählt. Ueber die Höhenbestimmungen 1890 vergl. App. 14, p. 524 u. App.
12, p. 651. In Maui ist die Dichtigkeit der Oberflächengesteine im Mittel 2.09; für die
Inseln im Mittel 2.21. In der Tabelle ist indessen 2.6 angenommen entsprechend einer
Berechnung von Preston aus der Schwerestörung, vergl. Budd, N°. 11, p. 140.

Zu den Pendelmessungen wurde 1883 ein Yardpendel, Reversionspendel von PEIRCE
N°.3, auf Holzstativ benutzt. 1888 kam ausser N°. 3 noch ein Meterpendel gleicher Kon-
struktion, N°.4, zur Anwendung, 1889/90 N°.3 und ein Meterpendel N°. 2. Im letztern
Falle schwangen die Pendel zu Anfang und am Schluss der Reise in W. ashington, Smith-
sonian Institution. Die Pendel wurden in jeder Gewichtslage fast unausgesetzt zwei oder
mehr Tage schwingen gelassen; eine Schneidenvertauschung gestatteten sie nicht (invariabler

‘ Typus). Die Zeitbestimmungen sind mit Sorgfalt an Sternen bewirkt; als Beobachtungsuhr

diente ein Chronometer, dessen Gangschwankungen durch die Vertheilung der Beobachtungen
im Endresultat nahezu eliminirt sein müssen.

Bei der Reduktion der Pendelbeobachtungen wurden die von Prirch abgeleiteten
Reduktionsformeln und Koefficienten angewandt, im ubrigen aber alles nach der Theorie
des Reversionspendels verwerthet.

Nach den Angaben von App. 14, p. 533, und von App..12, p. 658, würde der
m. F. eines Werthes von y aus 1883 in der Tabelle auf wenig mehr als + 0.010 cm.
zu schätzen sein, während er für 1888 sich zu + 0.008 cm. ergiebt. 1839/90 würde der
m. F. nach den an a. O. mitgetheilten Einzelergebnissen nur + 0.006 cm. betragen, auch.
waren bei dieser Reihe die Umstände recht günstig; jedoch deutet eine p. 657 gegebene
Vergleichung sowie die Differenz der Ergebnisse in Washington vor und nach der Reise auf
etwas grössere Unsicherheit (App. 12, p. 681/2).

Um den Einfluss der Aufstellung und also auch des Mitschwingens thunlichst in
den relativen Werthen zu beseitigen, theilt Prusron die Ergebnisse von 1888 in 2 Gruppen,

II 42

 
