HAMA AU

À et

ui

MI I DRT RAUM ILDAINAI 6 LL Ce Lt Lu)

409

MASURE DES BASES.

Trois bases seront mesurées; chacune d’elles aura environ 8500", La base ne
sera située vers le milieu de l'arc, près de Riobamba, à une latitude d’à peu près 14°
sud et à une altitude d’environ 2500 m:

En outre, deux bases de vérification seront établies près des deux extrémités de
l'arc, l’une au nord, près de Cumbal, sur le territoire colombien; l’autre au sud, entre
Quiroz et Sullana, sur le territoire péruvien.

I importe que les mesures soient faites avec le même instrument qui a servi pour
la méridienne de France. Non seulement cet appareil a fait ses preuves, mais les résultats
doivent être comparables, et ils ne sauraient l’être si l'on n’opère avec l'instrument qui a
été employé dans les mesures précédentes (quitte à se munir d'appareils de réserve pour le
cas d'accident).

Depuis quelque temps, on a commencé à étudier des alliages de fer et de nickel
dont la dilatation est extrèmement faible. Il est possible que ces alliages, que nous devons
à M. GuILLAUME, rendent dans l'avenir les plus grands services pour la mesure des. bases.
Jusqu'ici, toutefois, leurs propriétés sont assez mal connues. Elles ne peuvent l'être qu’à
la suite d'expériences longues et dificiles.

On a proposé, non de substituer les règles en metal GUILLAUME aux règles bimé-
talliques, mais d’emporter à l’Équateur les appareils des deux systèmes afin de pouvoir
faire pour chaque base des mesures comparatives. La Commission n’a pas cru devoir adopter
cette proposition; cela serait en réalité greffer sur la mesure de l’arc de Quito les expé-
riences sur.la valeur de la nouvelle règle. Ces expériences sont nécessaires et elles se
feront; mais il vaut mieux qu’elles se fassent indépendamment, Faites en France, elles

coûteront moins cher et l’on pourra plus facilement y consacrer le temps nécessaire.

MESURE DES ANGLES.

Le territoire de la République de l'Équateur sur lequel on doit opérer se divise
en une série de zones d’altitudes très différentes qui sont, en partant de l’océan Pacifique:
1° une. plaine basse; 2° Ia chaîne des Cordillères occidentales; 8° le plateau de Quito;
4° la chaîne des Cordillères orientales; 5° la plaîne des hauts affluents de l’Amazone.

Les deux chaînes ont une altitude générale d’environ 4000 m. ou 4500 m., mais
audessus de laquelle s'élèvent un certain nombre de pics généralement volcaniques qui atteig-
nent ou même dépassent 6000 m. L’altitude moyenne du plateau est de 2500 m.

Toutefois les deux chaînes et le plateau s’abaissent notablement dans la partie sud.

La plaine du Pacifique reste à une basse altitude (800 m. à 400 m.) jusqu'aux premières
pentes des Cordillères; de même la plaine de l’Amazone commence assez brusquement au
pied des Cordillères orientales, avec des altitudes moyennes d'environ 500 m.

La largeur de ces différentes zones est naturellement assez variable.

 
