 

VERB. D.
RELATIV ABSOLUT WIENER"
SYSTEMS

Wien, mil.-geogr. Inst. . 980.876
Wien, Türkenschanze. . 980.869 980.862 Nach Opponzer, korr. wegen Biegung d. P. — 0.007
um — 0.004; (1 schweres u. 1 leichtes Meter-
pendel);im Detail noch nicht publicirt.
Potsdam, Geod. Inst. . 981.290 981.270 Nach KüHNEN n. FURTWäNGLER, vorläufiges — 0.020
Ergebnis aus Beob. mit schweren u. leichten
Meterpendeln und einem Viertelmeterpendel. |
5 3 ue 5 981.254 =+ 0.006 m. F.; nach den Beob. mit Bessur’s — 0.036
Fadenpendelapparat in Berlin, Königsberg u.
Güldenstein von BESsEL, SCHUMACHER und
‘ PETERS (5 Werthe).
Rom, Ingenieurschule. . 980.363 980.343 + 0.008 m. F.; nach Pisati u. Pucct, ber. — 0.020
v. REINA, mit einem Fadenpendelapparat im
wesentlichen nach BEssEz, aber mit manchen
Verbesserungen.
Paris, Observatoire . . . 980.960 980.970 Nach Derrorexs’ Beobachtungen mit einem + 0.010
Meter- u. einem Halbmeterpendel, korr. wegen
Biegung d. Pendel um — 0,030 (Korr. ziem-
lich unsicher |)

 

 

 

 

 

 

 

t Einfaches Mittel: — 0.015 |

Ich vermuthe, dass die Korrektion der obigen Annahme für Wien, mil.-geogr. Inst.,
zwischen dem einfachen Mittel — 0.015 cm. und dem Werthe — 0.020 em. liegt.

Zu den bei den Tabellen angegebenen vorläufigen Reduktions-
werthen auf „Wiener System” tritt also voraussichtlich noch ein Betrag
von —0.015 em. bis —0.020 em. hinzu.

Gut gesicherte Anschlussstationen sind jetzt in allen Ländern der Internationalen
Erdmessung vorhanden, wo Pendelmessungen im Gange sind; nur für Spanien fehlt noch
ein scharfer Anschluss. Die wichtigsten der Stationen sind (unter Beifügung eines vorläufig
ausgeglichenen Werthes von y im Wiener System):

Dänemark: Kopenhagen (981.575, H = 17%),

Deutschland: München (980.748, H = 525), Potsdam (981.290, H = 87), Strassburg
(980,919; H=—= 137). 3,

Frankreich: Paris (980.960, H = 61), ;

Grossbritannien: Greenwich (981.204, H=47), Kew (981.217, H=5), London
(981818, H=93; 0 2,

Italien: Florenz, Padua (980.677, H=19), Turin, Rom (980,363, H = 59) u. 2.

Japan: Tokio (Univ. 979.814, H = 15),

Niederlande: Zeiden,

Norwegen: Kristiania (981.948, H = 28),

Oesterreich: Wien (mil.-geogr. Inst. 980.876, H=183 u. Türkenschanze 980.867,
H = 236), Pola,

?

 

   

 

COU Cae nid dt sde à a

 
