hah Serena el ne HY

Te

etat

mir | II EHEN ONE CTI we

375

 

Rumänien: Bukarest,

Russland: Helsingfors, Moskau, Pulkowa (981,909, H = 75),
Schweden: Stockholm,
Schweiz: Zürich,
Ungarn: Budapest,
Vereinigte Staaten: Washington (C. a. G. Surv. 980.128, H= 14) u. a.

(noch etwas zweifelhaft),

Ein wesentlicher Theil der Bemühungen zur Herstellung eines einheitlichen Systems
ist nationalen Ursprungs; besondere Verdienste haben sich bekanntlich in dieser Sache die
Herren Oberst v. STERNECK u. Oberstlieutenant DerrorGEs erworben. Was noch fehlte haben
theils die Beamten des Centralbureaus besorgt (Prof. Borrass, Dr. Ktunzn, Haasemany,
Dr. Schumann), theils die Herren Professor Ham und Mr. Purnam im Auftrage bezw. mit
Unterstützung der Internationalen Erdmessung. Ueber alle diese Operationen giebt der
Bericht Auskunft.

Nicht alle Anschlussstationen sind übrigens für Anschlussmessungen völlig befrie-
digend geeignet, da z. Th. die 4 Hauptbedingungen einer guten Anschlussstation: gute
Uhren, guter Zeitdienst, konstante Temperatur des Beobachtungsraums, grosse Ruhe des
Bodens — mehr oder weniger ungenügend erfüllt sind. Das Centralbureau hat es sich
augelegen sein lassen, Einrichtungen zu treffen, die ziemlich weit gehenden Anforderungen
genügen dürften.

Zum Anschluss der älteren Beobachtungsreihen reicht übrigens in der Regel aus
verschiedenen Gründen eine einzelne Hauptstation nicht aus, sondern es muss auf alle mit
anderen Reihen gemeinsame Stationen Rücksicht genommen werden. Einer der nicht selten
auftretenden Gründe ist der Umstand, dass auf der Hauptstation bezüglich der Installation

wesentlich andere Verhältnisse bei den Beobachtungen obgewaltet haben, als auf den
Aussenstationen.

Als besondere Arbeit ist noch zu nennen: „Untersuchungen über den Zusammenhang
der Schwere unter der Erdoberfläche mit der Temperatur von Oberst ROBERT V. STERNECK.”
(Sitzungsber. der k. Akademie d. W. in Wien, 1899).

Hierin werden die Beobachtungsergebnisse für 4 Schächte mitgetheilt:

Joachimsthal in Böhmen, Weenerschacht, 3 Stationen, 416m. Tiefe.
Pribram 5 hy Adalbertschacht, 6 ‘ 10097 1.
Kuttenberg  , i Greiferschacht, 4 e 300 „ 53
Idria » Krain,  Franzschacht, 6 Bi DA 5

Die Beobachtungen waren sehr scharfe Differentialbeobachtungen. Die resultirenden

Werthe für 4. liegen alle zwischen 5 u. 5.7; Pfibram giebt im Mittel 5.53 + 0.05 (nach
meiner Rechnung)).

Zur Berechnung der Beobachtungen hat Herr Prof. Scurörz einen Beitrag
in seiner schon erwähnten Bearbeitung der Pendelmessungen der Fram-Expedition geliefert.

 
