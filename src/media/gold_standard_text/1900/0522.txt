 

iy
i
i

ANNEXE B, VIII.

RAPPORT

SUR

les Marées

PAR

M. A. BOUQUET DE LA GRYE.

L’étude des marées a fait dans ces derniéres années de grands progrés, en ce sens
que dans la majeure partie des états ayant accès à la mer, des marégraphes en grand
nombre ont été installés et que le dépouillement des courbes a été effectué en se servant
de la methode dite de l’analyse harmonique.

Sans vouloir critiquer en rien les procédés utilisés antérieurement, qui permettaient
d'arriver à une grande exactitude pour les prédictions des hautes et des basses mers, disons
que la nouvelle méthode a permis de construire et d'employer un instrument tragant direc-
tement les courbes entières des marées en un point quelconque où l’on a déterminé un
certain nombre de coefficients, ce qui facilite grandement la publication des annuaires. En
outre il y a un avantage évident à ce que sur tous les points où des observations ont été
faites on arrive à des équations de même forme permettant de comparer les grandeurs des
ondes similaires.

Leur genèse en sera plus facilement trouvée et des courbes cotidales montreront
le coté hydraulique du transport de ces mêmes ondes.

Dans le volume publié à la suite de la conférence de Stuttgart, j'ai pu fournir
grâce à l’obligeance de mes collègues les résultats obtenus dans certaint états, les résultats
ci-après peuvent être considérés comme un complément des premiers.

Décembre 1900.

 

4
|
1
i
3:
ii
3
3
#
i
i

TE

oo hina ida ih era u.

i
4
|

 
