 

58

2. Bestimmung der Winkel des sphärischen Dreiecks, welches mit dem
shpäroidischen gleiche Seiten hat, nach den Gleichungen (6.).

eos os" == 06321726 ..

treoa nn! .os” = 1

4,2870
‚2461822... 17,6272
— ds! = —19,9308

 

 

 

1,9836 ....1.0,2974541
j. ES = 1,0202845
1,3177386 .... 20,7845
Ok! = — 20,3372
OM) == Er:
lcosn! . Os! — 1,3549387 .... . 22,6432
lcosn. Os! = 0,5354666.... 3,4314
— Os" = — 24,9018
1,1728 ....1.0,0692240
1, ee = 1,0962861

1,1655101 .... 14,6390
Ok = — 14,1706

 

l.cosn!’. 0s! = 1,0468535.... 11,1392

 

on = 0,4684
l.eosn! .os! — 12523160... 10 8T19
— Os!" = — 31,5394
— 2,5223....1.0,4017967,
sins!
nn = 11710715
1,5728682, ....—37,3997
ok" — + 37,8554
on!" — 0,4557

Fügt man diese Verbesserungen den Winkeln 7’, n", n!" hinzu, so erhält man das sphärische

Dreieck, welches mit dem sphäroidischen gleiche Seiten hat, wie folgt:

n. + On —44 657,083
nm" + On" — 56° 1'15",050

“nll On" — 80° 5'11”,314

13'23” 447

Der l.sins’’ weicht von dem
siebenten Decimalstelle ab.

cplsin 0,1573212,3

l.sins’ = 8,9102479,1

sin (n" + On") — 9,9186807,7
l'émis"— 6,9802499, 1

0,1573212,3
8,9102479,1

sin(n!"" dn!) = 9,9934664,9
L.sins’” —= 9,0610356,3

oben gefundenen um 0,9, also sehr nahe um eine Einheit in der
Der Grund davon ist darin zu suchen, dass die Azimuthal-
Differenz zwischen der geodätischen Linie und dem vertikalen Schnitt auch durch den oben

 

11: 14) métal blé ba

 
