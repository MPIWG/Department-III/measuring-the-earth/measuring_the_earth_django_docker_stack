 

D

C. Bien que le Royaume de Naples soit entre les provinces d'Italie celle où les
travaux de Géodésie remontent à l’époque plus ancienne, c’est encore lA quil y a le plus
à faire à présent, et c'est là précisement que s’est concentrée depuis trois ans l’activité de
la section géodésique de notre État major. La triangulation du premier ordre n’est pas encore
étendue à tout le pays; et celle qui existe est un peu irrégulière et ne forme pas un système
bien coordonné. Néanmoins les observations en sont excellentes; ce qui simplifie beaucoup
le travail de complément et de régularisation. Cette triangulation partait d’une base mesurée
en 1818 près de Castel Volturno, et dont l'exactitude inspirait quelques doutes. On a com-
mencé par vérifier cette base au moyen d'une autre très petite (à peu près 1000") mesurée
près de Naples pour un travail local, avec un appareil de Bessel qui est parfaitement au
niveau des exigences de la science actuelle. Les résultats ont été très satisfaisants; mais on
ne s'en est pas tenu là Dès 1860 une autre base de 6000" de longueur avait été mesurée
près de Foggia, au moyen du même appareil et avec tous les soins, par M. le professeur
Schiavoni, ingénieur géographe attaché au bureau de l'État major. Dans l'été passé le
même, savant s’est occupé de la rattacher à la grande triangulation. Ce travail a été fini
pour ce qui a égard aux opérations sur le terrain, et il ne reste plus qu'à achever les calculs.

Depuis 1861 une triangulation du premier ordre partant des côtés Napolitains de la
Calabre a été étendue à toute la Sicile par le Colonel de Vecchi, mon Collègue près de
la Conférence. Le travail, un peu retardé par l'obligation contemporaine qu’on lui avait im-
posée de procéder à la détermination des points du seconde et troisième ordre nécessaires
aux lévés topographiques, est à deux tiers finie, et le sera complètement dans l'été prochain.
Le Colonel de Vecchi n’a pas négligé de rattacher ses réseaux à ceux que le baron de
Waltershausen avait confectionnés pour sa magnifique carte de l'Etne. Il a obtenu de la part
de ce savant le plus obligeant concours, dont nous nous faisons un devoir de le remercier ici.

D. ÆE. La triangulation de Toscane du Père Inghirami et celle de la Sardaigne du
Général de la Marmora se prètent aux mêmes observations. Elles n'avaient d'autre but que
celui d’asseoir les cartes au 556600 €t 250000 de ces deux provinces. Ce sont des ocuvres
d'un seul individu et d’un laps de temps très court, qui ont parfaitement rempli leur but, et
qui par cela méritent l'admiration du monde savant. Mais par la force des choses leur dé-
gré d’exactitude n’atteint pas, surtout en Toscane, le type qu'ont le devoir de s'imposer les
Corps scientifiques dépendants de l'État, et fournis de toutes ses ressources. Si ces triangu-
lations devaient entrer à faire partie intégrante du grand réseau Européen, on devrait les
refaire en entier.

Il nous reste à présent à participer à la Conférence quelles sont nos intentions sur
la direction ultérieure à donner aux travaux pour que ces différents éléments, coordonnés
ensemble et rectifiés là où 1l en est besoin, puissent être offerts comme bases des mesures
géodésiques de la région Italienne. Le Professeur Schiaparelli vous dira quelles sont les

directions dans les quelles il convient de mesurer dans cette région trois méridiens et autant

de parallèles. Ce but sera rempli en rattachant toutes les triangulations partielles par une

    

MMA 11 A dt | AR

IM

id a

al

ju tu ll |

vache abn, js lds AM ei lacs ly pny)

 
