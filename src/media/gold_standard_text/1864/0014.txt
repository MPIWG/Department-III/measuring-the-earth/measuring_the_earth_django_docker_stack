 

ane >

zeichen, welche den Anschluss der kurhessischen Dreiecke an die hannoverschen abgiebt, und
welcher nach Gerling beträgt:
3,7001998

um 52 Einheiten der letzten Stelle vergrössert werden müsste; diese Correction sei jedoch
nicht ausser allem Zweifel, da jeder Nachweis derselben fehle.

Professor Wittstein legte ein Heft No. 36 der von Gauss an die Königliche Regierung
in Hannover im Jahre 1848 eingeschickten Vermessungs-Materialien vor, aus welchem die
Data für die Anschlussseite Sternwarte — Meridianzeichen entnommen werden können.

Der Endpunkt Sternwarte ist der Theodolithplatz von 1823, dessen Coordinaten sind:

© — —5,907
yi == (0.
Der Endpunkt Meridianzeichen hat die Coordinaten:
x — — 5019,756
y = — 0,183

womit übereinstimmt, dass dieser Punkt aus dem Punkte Sternwarte unter 5",471 östlich vom
Meridian erscheint (siehe Gerlings Beiträge pag. 69).
Die Längen-Einheit ist das in Gerlings Beiträgen pag. 107 näher bestimmte Meter.

t

Es ist demnach die Seite Sternwarte — Meridianzeichen:
== 5014240
wovon der Logarithmus ist:
== 3,7002059.
Hieraus folgt, dass die Logarithmen aller Dreiecksseiten der kurhessischen Dreiecke um +61
Einheiten der siebenten Decimale vergrössert werden müssen, um sie den hannoverschen Drei-
ecken anzuschliessen. |
Die Richtigkeit der vorstehenden Schlüsse wurde von allen Anwesenden anerkannt.
Was die weitere Correction der Dreiecksseiten durch die von Professor Peters in
Altona vorgenommene Revision der Berechnung der holsteinschen Basis betrifft, so beschloss
man diesen Gegenstand auszusetzen, bis authentische Documente über diese Berechnung vor-
liegen werden. Nach dem was über diese Berechnung bekannt geworden ist, neigte sich
jedoch die Ansicht der Anwesenden entschieden dahin, dass eine neue Messung der holstein-

schen Basis höchst wünschenswerth, bezüglich nothwendig sein werde.

gez.: Wittstein, Dr. phil. und Professor.
Börsch, Dr. phil.

Kaupert.

 

i
=
=
x

pet

Lab (nike)

Visto, pth han dag Mella he a 4

 
