"HR PPV FT me

Hi (eM Orit

u

ul

ei!

Bl!

TET. ML LAL

2. Die Messung der Längenunterschiede zwischen den Hauptpunkten des astronomisch
geodätischen Netzes.

3. Die Bestimmung der Intensitäten der Schwere an allen Punkten, wo es erforderlich erscheint.

4. Die Ausführung aller Berechnungen zu denen die obigen Beobachtungen und Unter-
suchungen Veranlassung geben.

Nächstdem hat das Central-Bureau noch die allgemeine Aufgabe, sich in vollstän-
diger Kenntniss von allen wissenschaftlichen Erscheinungen auf diesem Gebiete zu erhalten,
und sowohl die theoretische als auch die praktische Fortbildung der Wissenschaft dergestalt
im Auge zu behalten, dass es als Organ der permanenten Commission und als ausführende
Spitze der mitteleuropäischen Gradmessung diese Stelle auch factisch ausfülle und ausser den
oben erwähnten auch allen Anforderungen im ganzen Umfange genügen könne, welche die
General-Conferenz in ihren Verhandlungen, unter den astronomischen und geodätischen

Wh

ragen an das Central-Bureau gemacht hat.

Personal.

Das Central-Bureau wird aus 3 Mitghedern, dem General Lieutenant z. D. Baeyer
als Praesident, dem Professor Dr. Foerster und dem Plankammer- Inspector Dr. Bremiker be-
stehen. Das Hülfspersonal besteht aus 4 jungen Gelehrten, welche als Theoretiker,' Physiker,
astronomische Beobachter und Rechner fungiren. Unter diesen Mitarbeitern muss sich eine
anerkannte Kraft der mathematischen Forschung in diesem Gebiete befinden, welcher der
Praesident eine bevorzugte Stellung einräumen kann.

Hülfsrechner werden nach dem Bedürfniss engagirt.

Für das eigentliche Bureau sind dann noch ein Registrator, der zugleich Schreiber

und Bibliothekar sein kann, und ein Hausdiener erforderlich.

Kostenanschlag.

a. Einmalige Ausgaben.

Unter der Voraussetzung, dass der Königliche Generalstab dem Central- Bureau das
18zöllige Universal-Instrument von Pistor und Martins nebst Observatorium und den beiden
Chronometern von Kessel und Tiede, so wie anch 2 Barometer zur Disposition stellen kann,

sind folgende Instrumente neu zu beschaffen.

1. Ein vollständiger Apparat von Reversions-Pendeln . . . + : . …. 1000 Thlr.
3, zwei zehnzöllige Universal-Instrumente von Pistor und Martins’: c.4 gas 1600 file
3 fir Bureau-Utensilien, Tische, Stühle, Acten und Biicher-Repositorien. . 200 Thlr.

Summa 2800 Thlr.
5%

 

 
