| rn mr or ve

wi

D oh A iP PPT YTS!

1 Gi un

WAR

DUR Len | LT FT)

den

Pistor und Martins. Ich kann nicht umhin diesen Herren hier meinen wärmsten Dank ab-
zustatten für die Pünktlichkeit, mit welcher sie das Instrument nach nur drei Monaten genau
an dem festgesetzten Tage in einem Zustande ablieferten, der nichts zu wünschen übrig liess.

Nachdem in den ersten Tagen des Monates August die Erlaubniss zur Benutzung des
Laaerberges eingegangen war, wurde am 14. August die Aufmauerung der Instrumentenpfeiler,
deren einer zur Sicherung eines so wichtigen Punktes mächtiger als gewöhnlich angelegt
werden musste, begonnen und am 18. durch Aufsetzen der Steinplatten beendigt. An dem-
selben Tage wurde die schon seit einigen Wochen fertige Observationshütte so wie zwei ge-
miethete Hütten für die Mannschaft aufgestellt.

Am 23. August wurde das am Tage vorher in Wien angekommene Pistor’sche Mit-
tagsrohr nebst den Uhren u.a. auf den Laaerberg gebracht. Da mit hydraulischem Kalke
gemauert war, so konnte schon am 25. die Aufstellung und erste Rectificirung dieser Instru-
mente ausgeführt werden; am nächsten heiteren Abende (31. Aug.) wurde die erste Zeitbe-
stimmung daran genommen. 5

Die Direction der Staatstelegraphen war rechtzeitig um ihre Mitwirkung gebeten
worden und stellte alles Nötbige mit gewohnter Zuvorkommenheit zur Verfügung. Es war
somit von unserer Seite alles zu der von Dir. Brubns anberaumten Zeit für die Längenbe-
stimmung zunächst mit Leipzig gehörig vorbereitet und hätte dieseibe in dieser Beziehung
sofort beginnen können. Leider aber konnte Hr. Director Bruhns ein gleiches Mittagsrohr
bis dahin nicht erhalten, da das, auf welches er gezählt hatte, unerwarteterweise noch auf
dem Bogen Orsk-Valentia benöthigt wurde. Zugleich fand es Hr. Leverrier seinen Verhält-
nissen entsprechender, die Längenverbindung mit Wien auf das J. 1865 zu verschieben.
Einigermassen zum Troste musste bei diesen Verzögerungen der Umstand gereichen, dass
das beispiellos triibe Wetter des J. 1864, wenn die Längenbestimmung in Gang kam, höchst
wahrscheinlich die Messung von Breite und Azimuth gehindert hätte, somit die Lösung der
für 1864 vorliegenden Aufgabe von anderer Seite unvollständig geblieben wäre. Hr. Director
Bruhns hat nun im October v. J. das fragliche Instrument bei Pistor und Martins förmlich
bestellt und Hr. Leverrier sich zu der gemeinsamen Arbeit für die Campagne 1865 bereit
erklärt, so dass ein Nachholen des Versäumten in Kürze zu hoffen steht.

Unter diesen Umständen wurden die Breiten- und Azimuthbestimmungen sofort ins
Auge gefasst. Das hierzu nöthige Universalinstrument mit zehnzölligem Höhen-, zwöltzölligem
Azimuthalkreise und gebrochenem Fernrohre von 24”' Oeffnung war zwar von Herrn Starke
in den ersten Tagen des Jahres geliefert worden, musste aber wegen gewisser daran noch
nöthigen Abänderungen dem Künstler wieder zugestellt werden. Herr Starke vollendete
diese Aenderungen am 5. September, und, da das Instrument noch mehreren Rectificationen
unterworfen werden musste, so konnten die Breitenbestimmungen nicht vor dem 10. September
ihren Anfang nehmen. Die Azimuthalmessungen erfuhren dann wieder von anderer Seite
unliebsamen Aufenthalt. Der Heliotropenständer auf dem Hundsheimer Berge bei Hainburg

an der Donau, dessen Azimuth eben bestimmt werden sollte, war zwar schon Anfangs

4*

 

 
