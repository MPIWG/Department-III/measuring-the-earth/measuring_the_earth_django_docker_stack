nn |

A AY

i
i
È
R
=
È
&
a
k
&
È
=
È
Ë
+

A

Auf beiden Stationen wurden dieselben Sterne beobachtet und deren Positionen aus
den Beobachtungen selbst abgeleitet.

Die Instrumente auf beiden Stationen waren gleiche Passagen-Instrumente aus der
Werkstatt von Pistor u. Martins mit gebrochenem Fernrohr von 870" Brennweite und
68™" Oeffnung. ie
Die Instrumentalfehler wurden bei den Beobachtungen an beiden Stationen auf gleiche

Art ermittelt und wiihrend eines jeden Abends das Fernrohr 6 mal umgelegt. :
Die einzelnen Tagesresultate, wie sie unmittelbar aus den Beobachtungen fol:

gen, sind: .
1870. Längendifferenz . Gewicht
Mai 19. 10 26,935 1
1.20. 26,806 1
al. 26,925 1
Db, 26,826 ay,
HAT. 26,760 |
„188: 26,840
à 98. 26,786 1
a D
Juni 5. 26,701 7
oh 26,937 1

wobei das Gewicht zu 1 angenommen ist, wenn an beiden Stationen alle oder sehr nahe alle
Zeitsterne (20) am Abende erhalten sind. Das Mittel aus diesen Zahlen mit Berücksichtigung
der Gewichte ist:
10” 26°,840+ 0°,017
Die persönliche Gleichung fand sich
in Bonn --+ 08,016
in Leiden — 05,011
im Mittel -+- 05,002 |
und bringt man noch die Reduction in Bonn vom Instrument auf das Centrum des grossen
Mittelpfeilers mit <+ 0,062, in Leiden mit + 0,051 an, so erhält man als Längendifferenz:
Bonner Sternwarte östlich von der Leidener Sternwarte:
10226,955r 0,07,
die sogenannte Stromzeit ergab sich aus den Signalen zu
+ 08,010 + 08,003.
2. Die Pendelbeobachtungen.

Mit demselben Reversionspendel, welches im vorigen Generalbericht pag. 29 erwähnt,
und nach nahe gleichem Schema wurden in Bonn in dem westlichen Meridiansaale, in Leiden
in dem Beobachtungssaale unter der nördlichen Kuppel, in Mannheim in dem sogenannten
Zenithsaale der Sternwarte Beobachtungen von Herrn Dr. Albrecht, in Bonn unter Assistenz

6*

 
