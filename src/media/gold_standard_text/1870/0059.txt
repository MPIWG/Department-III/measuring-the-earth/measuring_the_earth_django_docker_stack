|
|

 

Li à

Mikrometerapparate seine frühere Beweglichkeit wieder zu geben, die horizontale Ebene, auf
welcher bei der Beobachtung des langen Pendels die Toise, bei der des kurzen Pendels dieses
selbst seinen Stützpunkt hat, so vom Roste zu reinigen, dass die Fläche wieder völlig eben
und parallel der ursprünglichen wurde, — endlich die übrigen weniger wesentlichen Theile
vom Rost zu befreien. Als Alles vollendet war, wurde der Apparat an der früheren Stelle
befestigt, und die Beobachtungen fast genau in derselben Weise, wie sie Bessel beschrieben *),
ausgeführt. Doch zog ich vor, auch hier die Coincidenzen zu registriren, wodurch ich in den
Stand gesetzt wurde, bei dem kurzen Pendel alle vier Momente der Coincidenzen zu beob-
achten, während Bessel wegen der Kürze der Zeit sich mit zweien begnügen musste. Ferner
brachte ich auch für die Beobachtung dieses Pendels am Uhrpendel einen Rahmen an, ver-
mittelst dessen ich sowohl die Coineidenzen, als die Grösse der Schwingungsbögen durch
dasselbe Fernrohr sehen konnte; — endlich wurden die Zeitbestimmungen ebenso wie früher
registrirt, wodurch jede Uhrvergleichung wegfällt.

Bessel benutzte bekanntlich bei seinen ersten Beobachtungen als schwingende Körper
eine Messing- und eine Elfenbeinkugel, später nahm er einen Messingeylinder mit verschie-
denen Füllungen, ohne indess einen Unterschied im Resultate bei der Anwendung dieser
beiden Methoden zu finden. Ich wählte die erstere, und ‚liess mir daher zu der noch vor-
handenen Messingkugel eine Elfenbeinkugel von genau gleichem Durchmesser drehen (Bessel’s
Elfenbeinkugel war gänzlich zersprungen). Ferner benutzte ich von den drei von Bessel be-
schriebenen Aufhängungsarten nur die Schneide und den Abwickelungscylinder, und zwar
war der Beobachtungsplan folgender:

3 Beobachtungsreihen mit der Schneide und Messingkugel,

3 - - = - - Elfenbeinkugel,
3 - - dem Abwickelungscylinder und der Elfenbeinkugel,
3 - Sl gle - - , + Messingkugel.

Jede Beobachtungsreihe bestand in einer Vergleichung des langen Pendels mit dem kurzen,
und zwar in beiden Lagen des Fühlhebels am Mikrometerapparat; wegen der grösseren
Sicherheit der Beobachtungen des kurzen Pendels wurde jede solche in zwei Beobachtungen
des langen Pendels eingeschlossen, etwa in folgender Reihenfolge:

Langes Pendel, Fühlhebel rechts

- - - links
Kurzes -

- - : rechts
Langes 7 _ É -

- - 4 links.

Bei der Anwendung des hygroskopischen Elfenbeins war es dagegen wesentlich, die Verglei-

chung des langen mit dem kurzen Pendel möglichst rasch auszuführen, daher wählte ich hier

*) Bessel, Untersuchungen über die Länge des einfachen Secundenpendels. Berlin 1828.

 
