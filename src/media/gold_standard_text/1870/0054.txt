 

 

— ta

tungen in Berlin vorläufig zu übernehmen, wenn er von seiner Behörde dazu ermächtigt
würde. Durch Vermittelung des König]. Cultusministeriums in Berlin bei dem Königl. Baye-
rischen Staatsministerium wurde hierauf bereitwilligst dem Herrn Profs Voten: 1} jähriger
Urlaub, vom 1. Oktober 1870 ab, bewilligt.

Nachdem die Pariser Meter-Conferenz ohne Resultat geblieben war, fasste Steinheil

den Entschluss der nächsten Conferenz zwei richtige mit der Bessel’schen Toise verglichene

Glasmeter aus seiner Sammlung vorzulegen. Zu dem Ende sollte zunächst auf dem Berliner .

Comparator die Ausdehnung ihres Glases und dann die directe Vergleichung mit der Bessel’-
schen Toise ausgeführt werden. Er hielt zwar Bergkrystall für das beste Material zu einem Nor-
malmeter, da aber bis jetzt ein solches Meter noch nicht hat beschafft werden können, so
behauptete er, dass seine Glasmeter, die aus ein und derselben Glasplatte geschnitten waren,
die bei der Abkühlung mit besonderer Vorsicht behandelt worden war, viel genauere End-
flächen und einen gleichmässigeren Ausdehnungs-Üoefficienten hätten als Stäbe von Platin
oder anderem Metall. Die Meterfrage beschäftigte ihn lebhaft und die Erfindung der Mittel,
sie zu lösen, nahmen sein ganzes Interesse in Anspruch, so dass ich sicher sein konnte bei
vorkommenden Schwierigkeiten auf seine bereitwilligste Unterstützung rechnen zu können.

Ende August verliess ich München höchst befriedigt von dem erzielten Ergebniss
und mit der Hoffnung, dass das Centralbiireau mit dem Beginn des Jahres 1871 zur Ausfüh-
rung der Maassvergleichungen vollständig eingerichtet sein würde. Kaum 14 Tage nach
meiner Abreise erhielt ich die Nachricht von der Erkrankung Steinheil's und wenige Tage
darauf auch schon die Todesanzeige*). — Die Wissenschaft hat in ihm einen der genialsten
Forscher und ich einen sehr lieben Freund verloren. — Als eine besondere Gunst der Vor-
sehung müssen wir es ansehen, dass ihm vergönnt war sein letztes bedeutendes Werk, den
Fühlspiegel-Comparator, in der vollen Frische seines Geistes zu vollenden, so dass er mit
innerer Genugthuung auf die damit zu erzielenden Resultate blicken konnte. An uns ist es
nun, diesen ausgezeichneten Apparat für die Wissenschaft zu verwerthen und durch unsere
Leistungen damit sein Andenken zu ehren.

Gegenwärtig ist der Comparator von den Herren Professoren Dr. Sadebeck und
Dr. Voit aufgestellt und die wesentlichsten Constanten sind von ihnen bestimmt, so dass die
Maassvergleichungen und die Bestimmungen der absoluten Ausdehnungen in kürzester Frist
ihren Anfang nehmen können. Das Centralbüreau ladet daher die geodätischen Commissio-
nen der Europäischen Gradmessung ein, ihre Toisen und Meterstäbe, welche Endmaasse sind
(für Strichmaasse ist die Einrichtung noch nicht fertig), zur Vergleichung mit der Bessel’schen
Toise einzuschicken. Auch hofft das Centralbüreau, der nächsten allgemeinen Üonferenz
schon zwei richtige mit der Bessel’schen Toise verglichene Glasmeter, welche aus dem Nach-

lass von Steinheil erworben sind, vorlegen zu können.

*) Leider hat die Maassvergleichungs-Commission im Laufe eines Jahres ausser den schweren
Verlusten von Brix und Steinheil noch einen dritten nicht minder schmerzlichen zu beklagen, den sie durch
den am 13. März 1871 erfolgten Tod von A. Repsold erlitten hat.

Jim Ad shh A HM tht ete ju bi

oa bi

doers amend dn dada äh ha an

 

 
