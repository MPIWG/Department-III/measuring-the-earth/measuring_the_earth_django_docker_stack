1 TR À

mr

i
i
ë
5
:
E
2
k
È
=
:
E
:

ungünstiges Wetter unterbrochen, und so kam es, dass wir erst am 4. August nach ‘dem
Collm bei Oschatz im Königreiche Sachsen gehen konnten. Dieser Berg, 1 Meile westlich
von der genannten Stadt, ist ein weithin sichtbarer Kegel. Bei einer Seehöhe von 312 Met.
erhebt er sich etwa 210 Met. über die ihn umgebende Ebene. Der Dreieckspunkt liegt auf
dem massiven Aussichtsthurme, auf welchem von den sächsischen Geodäten ein Beobachtungs-
pfeiler aus Granit errichtet worden ist. Die Oberfläche dieses Pfeilers, nach einem von uns
ausgeführten trigonometrischen Nivellement 17,482 Met. über der am Fusse des Thurmes be-
festigten Höhenmarke, deren Seehöhe nach Weisbach 315,767 Met. beträgt, hat 333,249 Met.
Seehöhe. Die zu beobachtenden Richtungen waren: Leipzig, Hohburg, Herzberg, der Gross-
berg (%, Meilen südlich von Schlieben), Strauch und der Keulenberg bei Pulsnitz. Als Null-
punkt diente uns der 1 Meile entfernte Kirchthurm von Merkwitz. Das Wetter war hier noch
schlechter als auf den vorangegangenen Stationen; wir hatten fast täglıch Sturm und Regen,
und dabei ging die Temperatur selbst in den von uns bewohnten Zimmern des Aussichts-
thurmes bis + 9° R. herunter.

Einigermaassen erträglicher war das Wetter auf Station Strauch (2 Meilen nördlich
von Grossenhain und dicht an der Grenze zwischen Preussen und Sachsen), wohin wir uns
Anfang September begeben hatten. Der Dreieckspunkt liegt auf den die umgebende Land-
schaft nur 100 Met. überragenden Haidebergen. Auf dem höchsten Gipfel derselben steht
ein 9,75 Met. hoher massiver Aussichtsthurm und 60 Met. östlich von demselben ist auf
Anordnung der sächsischen Geodäten ein Hügel von 6 Met. Höhe aufgeschüttet worden,
dessen quadratische Grundfläche 18 Met. und dessen Scheitelfläche 3 Met. Seitenlänge hat.
Auf demselben steht ein gehörig fundamentirter Beobachtungspfeiler von Granit, dessen Ober-
fläche nach einer vorläufigen Rechnung 207,86 Met. Seehöhe hat. Die zu beobachtenden
Richtungen waren: der Collm, Herzberg, der Grossberg, der Brautberg bei Kalau, der Brand-
berg bei Spremberg und der Keulenberg. ‘ Als Nullpunkt diente eine bei der Ziegelei von
Krauschütz ('/, Meile südöstlich von Elsterwerda) an einem Pfahl befestigte Tafel. |

Ausser den Winkelmessungen sind auch astronomische Beobachtungen zur Bestim-
mung von Polhöhe und Azimuth angestellt worden. Hierzu hatten wir uns 19 Met. west-
lich vom Dreieckspunkte einen Pfeiler aus Backsteinen (1 Met. hoch) aufbauen lassen. Die
Polhöhe des Beobachtungsortes ist aus 60 Zenith-Distanzen von Polaris in der Nähe der obe-
ren und ebensovielen in der Nähe der unteren Culmination und aus 50 Zenith-Distanzen von
südlichen Sternen (@ Aquilae und « Leonis) bestimmt worden; sie beträgt für den Beobach-
tungsort 51° 23! 7,72 und für den Dreieckspunkt 51° 23’ 7,80. Auf geodätischem Wege ist
für dieselbe vom Petersberge her 51° 23' 9",80 gefunden worden. Das Azimuth der Marke
Krauschütz ist auf dem astronomischen Pfeiler aus Beobachtungen von Polaris in den beiden
Digressionen bestimmt und — 333° 28’ 57,01 gefunden worden. Auf den Dreieckspunkt hin
centrirt beträgt dasselbe 333° 19' 33",30, woraus das Azimuth des Collm 257° 17 22",33 ab-
geleitet worden ist. Zur Vergleichung des letzteren mit einer geodätischen Bestimmung fehlen

vorläufig noch die erforderlichen Elemente, weil das Dreiecksnetz noch nicht ausgeglichen ist.
General-Bericht f. 1870. 5

 
