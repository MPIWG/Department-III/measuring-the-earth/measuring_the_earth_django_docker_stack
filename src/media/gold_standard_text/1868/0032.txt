 

a MN

Nächstdem bemerke ich noch, dass man bei den Ausgleichungen auf der Station jedes
beliebige Objekt zum Nullpunkt machen kann; man erhält stets dieselben wahrscheinlichsten
Richtungen, nur die Gewichte ändern sich um etwas, wenn die Objekte ungleich oft beobachtet
worden sind.

Da die Seitengleichung in einem Viereck mit zwei Diagonalen auf zweierlei Weise
formirt werden kann, entweder mit der einen oder mit der andern Diagonale, so erhält man
etwas verschiedene Werthe, je nachdem man die eine oder die andere Formation wählt. Am
vortheilhaftesten ist es, diejenige zu wählen, welche die günstigsten, d.h. am wenigsten spitzen
Winkel enthält.

2. Pont des Herrn Dr. Bremiker über die im Sommer 1868 ausgeführten
geodätischen Arbeiten.

Zur Ausführung des von dem Präsidenten des Central-Büreaus der europäischen Grad-
messung Herrn Generallieutenant Baeyer erhaltenen Auftrages, die Dreiecks-Verbindung zwischen
Cöln und den Hessischen Punkten Hasserod und Dünstberg herzustellen, begab ich mich am
19. April in Begleitung des Herrn Dr. Fischer, welcher mir als Gehülfe überwiesen war,
zunächst nach Cassel, um von Hertn Börsch über den Punkt Hasserod nähere Auskunft zu
erhalten. Von Cassel reiste ich nach Hasserod, besuchte indess auf dem Wege dahin die nicht
entfernt liegenden Punkte Hercules, Hohelohr und Astenberg, um für die spätere Erweiterung
des Dreiecksnetzes über Westphalen schon jetzt Rücksicht zu nehmen. Auf Hohelohr fand
sich keine Spur des Punktes vor, wo zuletzt Vorländer im Jahr. 1834 beobachtete, auch war
keine Auskunft darüber zu erhalten. Auf Astenberg wurde ein Signal, in Form einer Stange
mit starkem Knopf erriehtet, um diesen später nicht zu umgehenden Punkt vorläufig zu mar-
kiren. Auf Hasserod fanden sich die von Börsch angegebenen Marken vor, es war aber kein
Beobachtungspfeiler vorhanden, der demnach zunächst zu erbauen war. Ich übernahm dieses
letztere Geschäft: und schickte. Fischer zur Recognoscirung der Punkte nach Dünstberg, Feld-
berg und Fleckert. Auf dem ersten dieser Punkte fand sich der Nassauische zum Aufstellen
des Instrumentes . taugliche Steinpfeiler und es waren nur die Richtungen auszuhauen, auf
Fleckert, einer kahlen Feldkuppe, war ebenfalls der Nassauische Pfeiler vorhanden. Auf dem
Feldberg dagegen, wo eine Frankfurter Gesellschaft ein Restaurations-Gebäude aufgeführt hat,
so nahe dem alten Punkte, dass eine Aufstellung auf demselben unmöglich ist, musste ein
neuer Pfeiler gebaut werden. Dieses zu besorgen ‘übernahm Fischer. Ferner wurde dem-
selben die Bereisung der Punkte Nürburg und Michelsberg übertragen, um .dort die Erbauung
von Steinpfeilern in Auftrag zu geben. Auf letzterem war dieses schon im Herbste 1867 an-
geordnet, die- Ausführung war aber unterblieben. Als Heliotropstand sind hier ausserdem zwei
Holzpfeiler, der eine für Morgen-, der andere für Nachmittags-Sonne errichtet.

Nachdem auf Hasserod ‘der zu dem Pfeiler bestimmte Sandsteinblock mit grossen
Schwierigkeiten auf den Berg geschafft war, der Bau vollendet und die Richtungen durch den

 

2

st inn hen, bdd a äh a a

{
i
À
À
|

 
