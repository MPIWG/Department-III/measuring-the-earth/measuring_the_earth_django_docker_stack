À ARE PTT OST pre RTL LE

Le

dies war mir nicht mehr môglich, als der Unfall erst drei Monate nachher zu meiner Kenntniss
kam. Noch im Monat December. hatte das Instrument seit der Längenbestimmung nicht die
mindeste Aenderung erfahren. Ich habe es in allen seinen Theilen streng untersucht, fand
alle Schrauben gehörig angezogen und selbst bei ziemlich beträchtlichem Stossen an die ver-
schiedenen Theile des Instrumentes keine Spur von Verrückung, welche die Beobachtungen
des Herrn Kam zu zeigen schienen.

Nachdem ich viele Untersuchungen vergebens angestellt hatte, ist mir noch eine Quelle
des Fehlers als nicht unmöglich vorgekommen. Die Hebel der Gegengewichte, welche das
Instrument unterstützen, ruhen nicht auf Messern, sondern drehen sich um dieke stählerne
Achsen. Die Reibung, welche hieraus entsteht, wird beträchtlich dadurch vergrössert, dass die
Umdrehungsachse des Instrumentes durch Federkraft zwischen zwei stählerne Platten gedrückt
wird. Die Achsen der Hebel lassen sich nicht ohne grosse Schwierigkeit reinigen und die
Reibung an denselben kann sich mit der Zeit so sehr vergrössern, dass sie nicht mehr von
dem Uebergewichte überwunden wird, welches man dem Instrumente gegeben hat. In diesem
Falle ist es möglich, dass das Instrument bei einer Drehung um seine Ache von seinen Lagern
aufgehoben wird, zwischen seinen Platten schweben bleibt und eine falsche Neigung und ein
unrichtiges Azimuth annimmt. Im Monat December war es weit kälter als im September und.
musste die Reibung am Instrumente weit grösser sein. Ich habe damals wiederholt das Fern-
rohr des Instrumentes bald in einem, bald im entgegengesetzten Sinne die ganze Circumferenz
herumgedreht und auf die Meridianzeichen gerichtet, aber, obschon das Niveau nicht angehängt
war, also das Instrument dadurch nicht auf seine Lager gedrückt werden konnte, war keine
Spur des genannten Schwebens zu entdecken. Dass aus dieser Ursache keine Versetzung
des Instrumentes Statt gefunden haben kann, lässt sich auch ableiten aus der unveränderlichen
Entfernung des Mittelfadens von seinem zurückgewörfenen Bilde, wie sie an den Beobachtungs-
tagen bestimmt ist. Dasselbe geht auch noch hervor aus Beobachtungen, welche sich zufällig
vorfinden. An einigen der schönen Tage vom 1. bis 11. September 1868 hat Herr van Hennekeler
ein Paar Stunden vor den Zeitbestimmungen des Herrn Kam einige Beobachtungen zur Deeli-
nationsbestimmung der Gradmessungssterne angestellt. Es ist mir nicht bekannt, warum diese
Beobachtungen an den meisten der genannten schönen Tage nicht angestellt sind, aber sie
finden sich auch an Tagen, an welchen das Instrument, Herrn Kam’s Beobachtungen nach, sich
um 10 Secunden geändert haben müsste. Die Ablesungen der einander gegenüberstehenden
Microscope an den verschiedenen Abenden zeigen dieselben Unterschiede und dies beweist am
deutlichsten, dass das Instrument kurz vor den Beobachtungen des Herrn Kam nicht von seinen

Lagern aufgehoben oder versetzt war.

Man hat mir vorgeworfen, dass das Fehlschlagen der Zeitbestimmungen des Herrn Kam
eine nothwendige Folge meiner Vorschrift ist, das Instrument jeden Tag umzulegen. Kann
der Leidener Kreis der Umlegung nieht widerstehen, so ist er völlig unbrauchbar und zu dieser
lächerlichen Behauptung wird man sich um so weniger geneigt finden, da man es mir schon
sehr übel aufgenommen hat, als ich kleine Unvollkommenheiten dieses Instrumentes nachzu-

 

 
