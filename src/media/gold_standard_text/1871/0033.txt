1 TT TEP VOTEN mn À

or

In Bw 7 er ot

OT TPE

r

wir

=

27

zione speciale, che sarà presentata alla Commissione internazionale: per ora ci limitiamo ad
accennare che al calcolo di essa base non si @ ancora dato seguito, per la ragione qui
appresso. |

L’Apparato di Bessel posseduto dallo Stato Maggiore italiano fu nell’ anno 1858 as-
soggettato alle comparazioni delle spranghe tra loro, e delle spranghe alla tesa. Ora consi-
derato che il periodo di tempo sin’ oggi trascorso à abbastanza lungo, e considerato pure che
esso Apparato ha subito diversi trasporti per le misure di 4 basi con esso eseguite; si & sti-
mato conveniente di rifare tutte le comparazioni. Laonde allorché tali comparazioni, le quali
sono gid cominciate, saranno compiute coll’ adoperare le temperature estive ed invernali, si
procederà al calcolo della base indicata.

Intorno alla base del Crati si va poi compiendo, in questo anno medesimo, la rete
chiusa tra’ punti Montea, Castellara, Trionto, Mostarico, Nocara, Alpi, Giagola; la quale
serve a collegare la base alla triangolazione generale; siffatta rete sarà menata a fine nel

corso dell’ anno volgente, ed immediatamente sarä& messa a compensazione.

Rete tra la base di Puglia e quella di Calabria.

In questo anno stesso si compiera pure la triangolazione che collega la base di Puglia
a quella di Calabria; la quale rete fu cominciata nell’ anno trascorso. Cosi che nell’ anno
seguente noi saremo in grado di procedere alla compensazione anche della rete compresa tra
le due dette bası.

Affin di compiere poi la compensazione deffinitiva della rete menzionata, si attenderä
che intieramente sia menato a fine il calcolo tra la base di Puglia e la Dalmazia: che allora
compensata la rete circostante alla base del Crati sino a’ lati Alpi-Giagola ed Alpi-Nocara,
e quella tra’ medesimi lati ed i tre altri Biccari-Ascoli, Ascoli-Cerignola, Cerignola-Torre
pietre, gi& resi fissi dalla compensazione superiore; si deducono e fissano i lati medii Alpi-
Giagola, Alpi-Nocara.

Dopo di che alla compensazione della rete circostante alla base del Crati, si aggiun-

_gono le due condizioni laterali derivanti; ed all’ altra rcte espasa fra le due basi, aggiungonsi

pure le sei condizioni derivanti. A tale modo tutta la rete della Dalmazia sino a’ punti
Montea, Castellara, Sordillo e Trionto sara definita.

Compiuta questa parte si compensa dappoi la rete tra’ precedenti lati fissi ed i due
Santacroce- Patti e Patti-Trefontane, i quali derivanti pure dalla compensazione di Sicilia,
diverranno anche fissi. Sicch® introdotte in essa rete le sei equazioni laterali che derivano,
e praticato lo stesso nella rete Siciliana; si avrà cos! che tutta la triangolazione procedendo
da Capo Passero sino alla Dalmazia, sar’ assoggettata alla stregua delle 4 basi, cioé di
Catania, di Calabria, di Puglia e di Dalmazia. |

Dato un rapido sguardo sul concetto di compensazione de’ lavori indicati, aggiun-
giamo poche altre righe per accennare ad alcuni lavori di campagna stati nel trascorso

anno iniziati.

4#

 
