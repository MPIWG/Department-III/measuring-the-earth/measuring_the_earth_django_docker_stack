47
auch die telegraphische Uhrvergleichung vorzunehmen, sondern wir mussten letztere immer
| zwischen zwei Zeitbestimmungen einschalten. Bei dieser Anordnung konnte eine Längendif-
| ferenz nur aus der Combination zweier Zeitbestimmungen und einer dazwischen liegenden
Uhrvergleichung abgeleitet werden, im nächsten Jahre aber ergab jede Zeitbestimmung mit
ihrer gleichzeitigen Uhrvergleichung eine unabhängige Längenbestimmung. Daher kömmt
es, dass in der Tabelle für 1865 durchschnittlich für jeden Punkt doppelt so viel Zahlen vor-
handen sind, als für 1864; jede dieser Zahlen hat aber nur das halbe Gewicht.

Europäische Längengradmessung 1864

vorläufige Längendifferenzen.

 

Referenzstation Berlin. Referenzstation Greenwich.
Beob. Förster. Beob. Zylinski.
Forsch. Tiele. Forsch. : Tiele.
Breslau — Berlin Bonn — Greenwich
| + 14” 345,15 + 14" 345,21 1. 98m 238,99 128m 235,10
F 34. 05 34.92 23.01 23. 30
34. 18 34. 20 29. 19 23.20
33. 96 34. 04 “10 23. 35
i > 34.09 ee 3419 u
23.23
Leipzig — Berlin Nieuwport — Greenwich
— 4m 05,79 — 4 05.55 11m 15.74 ee
0. 85 0. 76 Le I
0: 713 0. 70 1.60 1. 69
O80 io ere N 0.67. iu: | 1.06
| é Co. EGG 1.7 ı
Bonn — Berlin Greenwich — Greenwich
— 25” 11°.65 — 25" 11°. 46 + 0™ 08.06 -+. 0" 08.10
11. 57 14. 54 | =. 0.09. +0 0.22
11. 53 11.3 1 0 0
11.55 Fr 10 01 0740 Ba.
Lt. 46 11. 47 + 0.04 |
m 11. 10 Kaverford- West — Greenwich
11. 58 — 19m 518,17 — 19" 518,08
58. 18 51.04
Ob 14 51. 06
51. 15 51.12
bio 51. 15

 

51. 08

 
