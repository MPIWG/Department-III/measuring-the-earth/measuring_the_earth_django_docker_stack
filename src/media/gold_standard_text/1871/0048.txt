 

42
sondern mit dem Lohmeier’schen Reversionspendel; allein in Königsberg ist im verflossenen
Jahre sowohl mit dem Fadenpendel als mit dem Reversionspendel die Länge des einfachen
Sekundenpendels bestimmt worden, so dass sich also der aus einem der beiden Instrumente
hervorgehende Werth, auf den Werth, welchen das andere ergeben würde, reduciren lässt.

Die etwaigen Abweichungen der von Bessel und Schumacher gefundenen Längen des
einfachen Secundenpendels von den jetzigen werden Functionen der inzwischen eingetretenen
Veränderung der Länge der benutzten Toise und der etwa an den drei Beobachtungsorten
Königsberg, Berlin, Güldenstein statt gefundenen Veränderungen in der Intensität der
Schwerkraft sein.

Mein Sohn, dem die Ausführung der besprochenen neuen Pendelversuche übertragen
worden, ist gegenwärtig damit beschäftigt, die Reductions- Elemente zu bestimmen und die
Beobachtungen zu reduciren.

Präsident v. Fligely: Wir kommen zur Berichterstattung über die Arbeiten in
Russland.

Herr v. Forsch: Ich habe der Conferenz im Wesentlichen über drei Arbeiten zu be-
richten, die innerhalb der letzten vier Jahre bei uns theils angefangen, theils weitergeführt
wurden. Die erste Arbeit ist die Längenbestimmung zwischen Pulkowa-Helsingfors- Stock-
holm und erlaube ich mir darüber folgende Mittheilungen zu machen.

Im Jahre 1866 kam, auf Vorschlag des Herrn Professor Lindhagen, eine Ueberein-
kunft zu Stande zwischen den Astronomen Schwedens, Norwegens und der Sternwarte in
Pulkowa, die zum Zwecke hatte, die genauen Längenunterschiede zwischen den Sternwarten
Pulkowa, Helsingfors, Stockholm und Christiania zu bestimmen. Da diese Sternwarten schon
geodätisch mit einander verbunden sind, so würde sich unter dem 60. Breitengrade eine
Längengradmessung ergeben, die leicht eine Dimension von 27° Länge erreichen könnte,
wenn man sie nach Osten bis Nowaja Ladoga, (32° von Greenwich) und nach Westen bis
Bergen (5° von Greenwich) ausdehnen wollte.

Diese Arbeit wurde im Jahre 1868 in Angriff genommen, indem der ‚Director der
Helsingforser Sternwarte Professor Krüger und der Dirigent der geodätischen Arbeiten in
Finnland Oberst Järnefelt, im Sommer des erwähnten Jahres die Längenunterschiede von
Pulkowa-Helsingfors und Helsingfors- Abo bestimmten. Unmittelbar darauf ist noch eine Län-
genbestimmung zwischen Pulkowa und Helsingfors vom Oberst Järnefelt und Herrn Fuss,
Adjunct-Astronomen in Pulkowa gemacht worden, bei Gelegenheit der Einschaltung zweier
Zwischenpunkte, Wiborg und Lowisa, deren Längen für die geodätischen Arbeiten in Finn-
land erforderlich waren. — |

Als Programm zu diesen Arbeiten war Folgendes aufgestellt:

Die beiden Beobachter machen gleichzeitig eine Zeitbestimmung, vergleichen darauf
ihre Uhren vermittelst telegraphischer Signale; unmittelbar darauf wird eine zweite Zeit-
bestimmung gemacht und dann noch eine zweite Ührvergleickung. — Eine solclie Reihe

sollte als vollständige Bestimmung eines Tages gelten. Im Falle ungünstiger Witterung oder

 

|
|
|

 
