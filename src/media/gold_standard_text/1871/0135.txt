2 TT TT nt à

129

 

hagen, Christiania — Kopenhagen, Kopenhagen — Altona, Altona — Göttingen, Bonn — Mann-
heim '), Göttingen — Leipzig, Leipzig— Mannheim '), Wien — Fiume, Wien— Kremsmünster,
Neuenburg — Bern '), Altona — Kiel, Göttingen — Dangast, Dangast — Leiden , Leipzig —
Dresden, Leipzig — Freiberg, Leipzig — Jauernick, Neuenburg — Weissenstein '), Neuenburg
— Simplon, Simplon — Mailand, Neuenburg — Mailand, Florenz — Ancona. Auf dem Län-
gengradbogen Orsk — Orenburg — Samara — Saratow — Lipezk — Orel — Bobruisk — Grodno
— Warschau — Breslau — Leipzig — Bonn — Nieuwport — Greenwich und Kaverford- West
sind die vorläufigen Resultate grösstentheils auf pag. 47—49 des Berichtes enthalten. Die Be:
stimmung der Lingendifferenzen auf telegraphischem Wege zwischen Berlin und Frankfurt,
Berlin und Briissel, Stockholm und Upsala, Paris und Greenwich sind fir die Zwecke der
Europäischen Gradmessung nicht genau genug ermittelt. Die Längenbestimmungen Green-
wich — Briissel (Memoirs of the Royal Astron. Society Vol. XXIV, A—= 17" 28°.90), Green-
wich — (Feagh-Main) Valentia 1862 A=41” 285.37, 1866 1 = 41” 235.19 bedürfen noch einer
Revision und andere Liingenbestimmungen in England sind nicht aufgeführt, weil sie nicht

zum Zwecke der Europäischen Gradmessung angestellt wurden.

b. Polhöhenbestimmungen.

Breitenbestimmungen sind an sehr vielen Orten gemacht, doch sind sicher diejenigen
Breitenbestimmungen, welche im vorigen Jahrhundert und noch zu Anfang dieses Jahrhun-
derts angestellt wurden, schon wegen der damals angewandten Instrumente im Vergleich
zu den jetzigen, der Revision bedürftig. Ausser den Breiten der hauptsächlichsten Stern-
warten habe ich auf der Karte nur diejenigen aufgenommen, welche in der Bessel’schen und
Struve'schen Gradmessung, in dem Breitenunterschied von Göttingen und Altona von Gauss,
und in neuerer Zeit in besonderen Publikationen und theils in unsern Generalberichten auf-
geführt sind. Da England der Europäischen Gradmessung nicht beigetreten, sind in England
nur die Breiten von Greenwich und Helgoland aufgeführt, obwohl in der von James 1858
publieirten „Ordnance trigonometrical Survey of Great Britain and Ireland“ von 35 Orten
die Breiten und von 60 Orten die Azimuthe enthalten sind.

Die in die Gradmessung fallenden Sternwarten sind: Stockholm, Upsala, Lund, Chri-
stiania, Bergen, Kopenhagen, Abo, Helsingfors, Pulkowa, Dorpat, Moskau, Kasan, Kiew,
Warschau, Königsberg, Berlin, Altona, Breslau, Leipzig, Gotha, Göttingen, Bonn, Mann-
heim, München, Krakau, Kremsmünster, Wien, Pola, Leiden, Utrecht, Brüssel, Green-
wich, Paris, Marseille, Genf, Neuenburg, Bern, Zürich, Mailand, Padua, Florenz, Rom,
Neapel, Palermo, Madrid, San Fernando bei Cadiz, Lissabon.

Zu diesen Breiten kommen hinzu in Norwegen: Naeverfjeld bei Lillehammer, Höst-
bergkampen, Husbergoe bei Christiania, Jonsknut bei Konsberg, Horvikfield bei Bergen; in
Schweden: Fuglenäs, Stuor-oivi, Torneä; in Russland: Kilpi-mäki, Hogland, Mäki-päälis,

') Die Resultate sind in der Druckerei.
Generalbericht für 1871. ; 17

 
