ET RATE ET 1 MITT YT errr TS

35

Ce ES Ue as i

Mitwirkung der Oberlieutenants Robert v. Sterneck, Heinrich v. Sterneck, Heinrich Hartl und
Rudolf Raudhartinger erst am 16. September beginnen konnte, welche zweimal wiederholt am
6. October in 14 Arbeitstagen beendet wurde.
Nach vorläufiger Rechnung stellte sich folgendes Resultat heraus:
1. Messung . . 1668.158 Wr. Klafter
2. : ‘11. GBS. M62 4s. LE
Differenz 0.004 Wr. Klafter.

Während der Zeit, als die Vorarbeiten stattfanden, hat Ober-Lieutenant Robert
». Sterneck auf dem östlichen Endpunkte der Grundlinie die Polhöhe nach zwei Methoden
und das Azimuth aus Horizontaldistanzen des Polarsterns mit dem Pöstlingberge bestimmt.

Die dazu benutzten Instrumente waren ein 14" Universalinstrument und ein Passa-
eenrohr von 30 Linien Oeffnung. Der Vorgang bei den Beobachtungen ist ähnlich jenem, wie
er auf früheren Stationen schon stattgefunden hat und in.den Generalberichten bereits be-
sprochen worden ist.

Zur Bestimmung der Polhöhe aus Zenithdistanz-Beobachtungen wurden die Sterne
c Ursae min., 8 Urs. min., & Bootis, # Tauri, 6 Orionis, & Orionis, y Geminorum, « Canis
minoris, 8 Geminorum gewählt; im 1. Vertikal aber die Durchgänge der Sterne 0 Persei,
a Aurigae, @ Cygni, 32 Cygni und No. 5279 Radel. beobachtet.

Richtungs- und Zenithdistanz - Beobachtungen haben auf den beiden Endpunkten der
Grundlinie, und auf noch vier anderen Punkten theils vor, theils nach der Messung der

Grundlinie stattgefunden.

C. Die Beobachtungen in der Banater Militär-Grenze sind heuer in östlicher Rich-
tung fortgesetzt worden und ist die Verbindung des in Tafel IV. ersichtlichen vom Haupt-
mann Vergimer und von Schlayer mit einem 12" Repetitions-Theodoliten beobachteten mit
jenem aus der Grundlinie von Arad abgeleiteten im Jahre 1854 und 1855 ausgeführten Drei-

ecksnetze vollkommen hergestellt.

D. In Siebenbürgen sind heuer für Gradmessungszwecke keine Messungen ausgeführt
worden; hingegen sind mit Steinpfeilern oberirdisch und ausserdem unterirdisch markirt wor-

den die Punkte: Incu, Közreshavas, Virauerstein, Czibles, Tojanatomi und Gogosa.

Schliesslich ist noch zu erwähnen die Publikation des 1. Bandes der „astronomisch-
geodätischen Arbeiten des militär-geographischen Institutes“ und die mit den in diesem Buche
angegebenen Elementen berechnete definitive Länge der Grundlinie von Sign mit folgenden
äusserst günstigen Resultaten:

1. Messung. 1805.334627 Wr. Klafter
2. 1305.334413 , x

N

) „1808.33443 Di
folglich Differenz 0.000214 Wr. Klafter
oder 0.4300 Millimeter.

 
