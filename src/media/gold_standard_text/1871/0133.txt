en

rn TE

127

Vierter Anhang.

Zusammenstellung der ausgeführten astronomischen Bes anna:
welche zur Europäischen Gradmessung gehören.

Von Prof. 6. Bruhns.

In der vierten Sitzung der astronomischen Commission in Wien (siehe pag. 104 des
Berichtes) wurde der Wunsch ausgesprochen, die für die Europäische Gradmessung ausge-
führten astronomischen Bestimmungen in einer Karte graphisch darzustellen und selbige alle
drei Jahre durch die neu ausgeführten zu ergänzen. Auf ein von mir an die Herren Com-
missare gerichtetes Circular, mir gefälligst über die ausgeführten Bestimmungen Bericht zu
erstatten, sind nur von wenigen Seiten Antworten eingegangen; ich kann daher nur, so weit
es aus dem eingegangenen Material und den mir zugänglichen Publikationen möglich ist,
die Zusammenstellung in der anfolgenden Tafel VII geben. Ich hoffe jedoch, dass sie nahe
vollständig ist und bitte mich auf die etwa vorgekommenen Irrthümer aufmerksam machen

zu wollen.

a. Die Längenbestimmungen,

Da für die Europäische Gradmessung nur die telegraphischen Längenbestimmungen
als zulässig angesehen werden, sind die bis jetzt ausgeführten, auf pag. 78, 79 und 80 an-
gegebenen, durch gerade Linien zwischen den betreffenden Orten eingezeichnet. Es sind
ausgeführt auf telegraphischem Wege über 60 Längenbestimmungen; davon sind publicirt
die folgenden:

1. Altona— Schwerin. Peters, Bestimmung des Längenunterschiedes zwischen Altona

und Schwerin. Altona 1861. (Längendifferenz À — 5" 545,56).

2. Berlin — Leipzig. Bruhns und Foerster, Bestimmung der telegraphischen Längen-
differenz zwischen Berlin und Leipzig. Leipzig 1865. (1— 4" 05.89.)

3. Berlin — Lund. Bruhns, Bestimmung der telegraphischen Längendifferenz zwi-
schen Berlin und Lund. Leipzig 1870. (Ur Lunds Univ.-Arsskrift Tom. VIL.)
(= 0m 49 29)

4. Berlin — Wien. Bruhns, Bestimmung der Längendifferenz zwischen Berlin und
Wien. Leipzig 1871. (= 11% 56478.)

 
