nn

 

Fe TN

I WA

B
a
a
=
à
=
3
:
3
:
B
E

115

Leverrier, die fast identisch ist, entschieden genauer als die Bessel’sche Variatio saecularis
sich findet. Mit der Struve'schen Praecession sind daher alle Sterne zuerst auf die Epoche
1870 reducirt, die auf die oben erwähnte Art abgeleitete Eigenbewegung ist dann noch für
die Zeit von der Epoche der neueren Beobachtungen bis 1870 angebracht und dadurch sind
schliesslich die definitiven Positionen für 1870 erhalten.

Jedoch nicht alle Sterne kamen im Bradley vor, und um die Eigenbewegung zu be-
stimmen, wurde bei den fehlenden in erster Linie der Katalog der Vierteljahrschrift der astron.
Gesellschaft benutzt und für die Sterne y Camelopardalis, « Camelopardalis, British Association
Catalogue 2722 die Eigenbewegung direct diesem Kataloge entnommen. In zweiter Linie
wurde der Mädler’sche Katalog gebraucht, in ihm fanden sich k Bootis und der Stern Rad-
cliffe 6035 und fiir selbige wurde die Eigenbewegung nach Kaiser direct adoptirt. Für die
dann noch fehlenden Sterne wurde der Katalog von Groombridge zu Hülfe gezogen, die Sterne
dieses Katalogs zunächst nach den Angaben in den Astronomischen Nachrichten No. 1536 auf
Auwers mittleres System bezogen und aus den so erhaltenen Positionen und den aus Leiden,
Leipzig und Greenwich abgeleiteten die Eigenbewegung berechnet. Es ist dies geschehen
für die Sterne: Radcliffe 247, 482, 2571, 2572, 3443, 5554, 5892, British Association Cata-
logue 510, 1062, 4122, 4407, 4699, 6372, 6723, 6928, 7294, 7365. In allen diesen Katalo-
gen fehlte endlich der Stern Radcliffe 5279, bei welchem die Position aus Radcliffe I zuerst
auf Auwers mittleres System nach Astronomischen Nachrichten No. 1536 reducirt und aus
der so gefundenen Position und der aus den*Leidener, Leipziger und Greenwicher Beobach-
tungen dann die Eigenbewegung ermittelt wurde.

In der folgenden Tafel I sind in der 1. Columne die Namen der Sterne, in der 2.,
3., 4. und 10. die Epochen für die Leidener, Leipziger, Greenwicher und Bradley’schen Be-
obachtungen enthalten, in der 5., 6. und 7. Columne finden sich die Declinationen der Sterne
reducirt mit der Siruve'schen Praecession und zunächst ohne Berücksichtigung der Eigenbe-
wegung für das Jahr 1870, in der 8. Columne ist die mittlere Epoche für die Leidener, Leip-
ziger und Greenwicher Beobachtungen, in der 9. Columne die mittlere Position für dieselben
Beobachtungen, letztere beide abgeleitet mit Berücksichtigung der Gewichte, in der 11. Co-
lumne befinden sich die Bradley’schen Positionen für 1755, wie sie Herr Professor Auwers
angegeben hat, endlich in der 12. Columne die Minuten und die Seeunden dieser Bradley-
schen Position, reducirt mit der Struve’schen Praecession auf die Epoche 1870. Die Differenz
der Positionen in der 9. und 12. Columne ist der Betrag der Eigenbewegung fiir die Anzahl
von Jahren, welche sich aus der Differenz der 8. und 10. Columne ergiebt.
