nn

N TRITT PT RT TY

mr

101

Ferner werden vom Berichterstatter die Erweiterungen der Längenbestimmungen auf
Holland, England, Frankreich, Spanien und Russland erwähnt; hierüber berichtet ausführlich
die von Herrn Karlinski ausgearbeitete Vorlage.

Hierauf geht, der Berichterstatter über auf die, betreffs der Methoden der Längen-
bestimmung gefassten Beschlüsse der Subcommission. Als Methoden werden empfohlen Re-
cistrir- und Coineidenzmethode, letztere besonders bei beschränkter Zeit für die telegraphische
Verbindung. Längenbestimmungen durch Chronometer und mit Lichtsignalen sind für Haupt-
verbindungen zu vermeiden. Ueber Lichtsignale machen die Herren Fearnley und Siruve
einige Bemerkungen; nach ersteren erhält man mit Lampenlicht keine ganz günstigen, nach
letzteren sehr günstige Resultate mit reflectirtem Sonnenlicht.

Werden Polarsterne mit Auge und Ohr beobachtet, die Zeitsterne registrirt, so ent-
stehen nach Bemerkungen der Herren Bruhns und Siruve merkbare, nach Herrn Hirsch keine
wesentlichen Differenzen in der Auffassung der Polarsternantritte bei diesen verschiedenen
Methoden. Die Section meint jedem Beobachter die Entscheidung hierüber zu überlassen, ob
eine solche Combination der Beobachtungen zulässig ist, nur wird gewünscht, dass im Falle
dieser Combination der verschiedenen Beobachtungsmethoden die Reductionsgrösse auf die
Registrirung ermittelt und mitgetheilt werde. Herr Peiers bemerkt, dass die Registrirung
der Polarsterne bei ihm wesentlich bessere Resultate geliefert hat, als die Beobachtung mit
Auge und Ohr.

Was die persönliche Gleichung betrifft, so ist es erwünscht, dass entweder die Beob-

achter ihre Gleichung durch Wechsel der Stationen eliminiren, oder vor und nach den Län-

‘senbestimmuneen ihre Gleichung ermitteln. Was letztere Bestimmungsweise betrifft, so ent-
£ 5 8 8 9

wickelt sich hierüber eine Discussion zwischen den Herren Peters, Bruhns und Hirsch; von
ersterem wird empfohlen, um die Hast bei der Beobachtung desselben Sternes an den ver-
schiedenen Fäden zu vermeiden, Rectascensionsdifferenzen zweier nahe im gleichen Parallel
stehender Sterne zu bestimmen, so zwar, dass an einem Abend der eine Beobachter den vor-
angehenden, der andere den nachfolgenden Stern beobachtet, welche Anordnung der Beob-
achtung an dem anderen Abende umgekehrt wird. Herr Hirsch findet es zweckmässiger, dass
während der Passage die Beobachter wechseln, hebt aber die grosse Schwierigkeit hervor,
die durch die Verschiedenheit der Augen der Beobachter bedingt wird und bezeichnet diese
Fehlerquelle bei nicht genau centraler Beleuchtung als so wesentlich, dass dieselbe gleicher
Ordnung mit der zu bestimmenden Grösse ist.

Vierte Sitzung am 26. September von 2 bis 4 Uhr.

Nach Lesung des Protokolles der Sitzung vom 25. September eröffnet Herr Hirsch
die Discussion über den am Schlusse der gestrigen Sitzung angeregten Gegenstand, indem
derselbe den Fehler bespricht, der sich bei ungenauer Ocularstellung und nicht centraler Be-
leuchtung des Sehfeldes in der Auffassung des Ortes des Fadens ergiebt, welche aus diesen

Ursachen um mehrere Zehntel Sekunden variirt. Ohne auf die theoretische Erklärung dieses

Phänomens in der Commissionssitzung einzugehen, theilt er das von ihm eingeschlagene Ver-

 
