BE ee a Senna nu a UF ung gan aa mm a nn en ae

 

=

Sg ee

en

EEE

a ee

En

cher Ansicht die Section beitritt. Man beschliesst, dass die Herren Bruhns, Herr, Struve unter
Mitwirkung der Berichterstatter berathen und letztere für die Plenarversammlung über diesen
Gegenstand mit Berücksichtigung des dabei gewonnenen Materials einen umfassenden Plan
ausarbeiten, mit besonderem Hinweis auf die einerseits nothwendig erscheinenden, andererseits
wünschenswerth sich erweisenden Längenbestimmungen. Herr Bruhns wünscht, dass diese so
gebildete Subcommission sich auch mit der Frage der Methoden der Längenbestimmungen
befasse und in ähnlicher Weise das gewonnene Material der Breiten- und Azimuthbestimmun-
gen in vorbereitende Berathung nehme.

Dritte Sitzung am 25. September von 10 Uhr 45 Minuten bis 12 Uhr.

Nach Lesung und Genehmigung des Protokolles der Sitzung vom 22. September be-
richtet Herr Karlinski über die Ansichten und Beschlüsse, welche die Subeommission in Rück-
sicht auf Erweiterung und Methoden der Längenbestimmungen gefasst hat. Es wird empfoh-
len, dass jeder Ort mit Ausnahme der Endpunkte mindestens einen dreifachen Anschluss er-
halte. Herr Hirsch empfiehlt hierbei die Längenbestimmungen zwischen drei und mehreren
Orten womöglich gleichzeitig auszuführen und findet in diesem Verfahren eine zweckmässige
Controle und Zeitersparniss. Die Herren Oppolzer, Struve, Bruhns äussern Bedenken über
die Zulässigkeit dieses Verfahrens und es wird ausserdem auf die meteorologischen Schwierig-
keiten und beschränkte Disponibilität der telegraphischen Linien hingewiesen. Herr Karlinski
weist auch darauf hin, dass die persönliche Gleichung schwieriger zu eliminiren sei. Herr
Hirsch formulırt seinen Antrag dahin: a

Sobald an einem astronomischen Punkte die Längenbestimmung mit mehreren an-
deren Punkten zu machen ist, erscheint es wünschenswerth, dass, wenn es die Um-
stände gestatten, die Längenbestimmung gleichzeitig wenigstens mit zwei anderen
Punkten ausgeführt werde.

Der Antrag wird mit 6 gegen 4 Stimmen abgelehnt.')

Herr Karlinski berichtet weiter und erwähnt für die österreichischen Stationen einige
Erweiterungen und hierbei wird auch auf den russischen Anschluss Rücksicht genommen und
die Schliessung einer Schleife über Kertsch empfohlen.

Herr Bruhns erklärt im Namen des Herrn Dr. Siemens ın Berlin, dass derselbe die
über Kertsch nach Ostindien gehende Telegraphenlinie zu einer Längenbestimmung mit Be-
reitwilligkeit offerirt; nach einer Discussion an der sich die Herren Bruhns, Hirsch und Struve
betheiligen und in der die Wichtigkeit der Bestimmung mit Hülfe einer directen Lei-
tung (ohne Relais) hervorgehoben wird, einigt sich die Section einstimmig dahin: das An-
erbieten des Herrn Siemens mit Dank anzunehmen und der Commission die Benutzung und
Verwerthung dieser Linie zu empfehlen und zwar zur directen Bestimmung zwischen Ost-
indien und einer mitteleuropäischen Station.

Schliesslich bemerkt Herr Bruhns, dass es wünschenswerth erscheint bei Zeitbestim-

. mungen zur Bestimmung von Längendifferenzen eine mindestens 60fache Vergrösserung an-

N

zuwenden, welcher Ansicht sich die Section anschliesst.

i
|
3
=
i
a
3
I
À
3
3
3

je eh es a Bad Aa ab lh Aad edt a

 

 
