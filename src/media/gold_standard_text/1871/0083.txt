nn

1 TRITT |

aid ML LA DOME

peter

8
=
=
5
£
k
8
=
=
F
F

 

77
der permanenten Commission die Anbringung von vier registrirenden Pegeln empfohlen wird,
eine im Tyrrhenischen Meer in Paola, zwei im Jonischen Meer in Rossano und Taranto und
einer im Adriatischen Meer in Brindisi. Diese vier Punkte würden, wenn sie sowohl durch
ein Präcisions-Nivellement, als auch durch ein trigonometrisches verbunden würden, G@ele-
genheit geben zu erforschen, ob zwischen den drei Meeren ein Unterschied im Niveau statt-
findet und durch Wiederholung der Nivellements liesse sich auch die Frage beantworten, ob
das so vielfach von Erderschütterungen heimgesuchte Calabrien seine Höhe verändert. Auch

über den stündlichen Gang der Ebbe und Fluth würden Resultate erhalten werden können.

Ferner wünscht Herr Schiavoni, dass die Conferenz den Municipien von Venedig und
Neapel das Anbringen registrirender Fluthmesser an passenden Oertlichkeiten anempfeh-
len möchte.

Herr Hirsch: Ich erlaube mir den Antrag des Herrn Schiavoni zu unterstützen,
glaube jedoch, dass das trigonometrische Nivellement weggelassen werden könnte, weil ich
der Ansicht bin, dass nur ein geometrisches Nivellement im Stande ist, die Niveaudifferen-
zen der Nullpunkte der Pegel mit derjenigen Genauigkeit zu ermitteln, welche bei diesen
Untersuchungen nöthig ist.

Präsident vo. Fligely: Aus dem Antrage des Herrn Schiavoni, sowie aus seinen münd-
lichen Mittheilungen scheint mir hervorzugehen, dass er die Anwendung beider Nivellirme-
thoden wünscht, besonders zum Zweck einer Vergleichung derselben untereinander, und zur
Erforschung der Refractionsverhältnisse. |

Herr Schiavoni schliesst sich dieser Ansicht an und seine beiden Anträge:

1. Die Conferenz der Europäischen Gradmessung richte an die königlich italienische
Regierung das Gesuch, selbige möge registrirende Fluthmesser in Paola, Rossano,
Taranto und Brindisi herstellen, die Nullpunkte derselben sowohl durch geometri-
sche als trigonometrische Nivellements mit einander verbinden und diese Nivelle-
ments von 10 zu 10 Jahren wiederholen lassen.

2. Die Conferenz der Europäischen Gradinessung möge die Municipien von Venedig
und Neapel ersuchen, zum Zwecke genauer Ermittelung der mittleren Meereshöhe
in ihren Häfen selbstregistrirende Fluthmesser zu errichten,

werden einstimmig angenommen.
Präsident v. Fligely: Wir gelangen zu dem Berichte der astronomischen Commission

und ich ersuche Herrn Peters die Berichterstattung zu veranlassen.

Herr Peters: Zunächst wird Herr Karlinski über die Längenbestimmung, dann Herr
Weiss über Breiten- und Azimuthbestimmung, darauf Herr Bruhns über die Bestimmung der
Declination der bei den Beobachtungen benutzten Fixsterne und über die auszuführenden

Intensitätsbestimmungen der Schwere berichten.

Herr Karlinski: Der Bericht der astronomischen Commission über die Längenbestim-

mungen, den ich hiermit zu erstatten die Ehre habe, betrifft 1) die schon ausgeführten Ar-
