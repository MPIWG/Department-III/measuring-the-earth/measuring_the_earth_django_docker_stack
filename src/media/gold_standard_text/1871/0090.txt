 

ee

eee

i
Fr
a

|
:
|

2. Alle Orte, an denen bei Gelegenheit früherer Gradmessungen Breiten- und Azi-
muthbestimmungen vorgenommen wurden.

Nach diesen Beschränkungen sind in den Generalberichten der Europäischen Grad-
messung bis incl. 1870 noch folgende astronomische Ortsbestimmungen zweiter Ordnung

verzeichnet:

in Belgien Lommel.

„ Mecklenburg Granzin.

„ Preussen Schneekoppe.

à 5 Brocken.

; n kleiner Fallstein.
„ Gotha Seeberg.

un Inselsberg.

» Pachsen Kahleberg.

. = Fichtelberg.

4 Capellenberg.

„ Oesterreich Spieglitzer Schneeberg.
Hoher Schneeberg.

» ”
n A Cerkow.

| is Kunieticka- Hora.

» » Rappotitz.

5 a Wieternik.

> 5 Buschberg.

i . Wiener Neustadt (nördlicher Basisendpunkt).
: > Sebenico.

» 5 Sasseno.

> | u Durazzo.

5 h » Monte Hum.

An 16 unter diesen 22 Orten ist die Polhöhe durch beide Methoden (Circummeridian-
höhen und Durchgänge im ersten Vertical) bestimmt; an 5 (nämlich Lommel, kl. Fallstein,
Sasseno, Durazzo und Monte Hum) blos durch Circummeridianhöhen und an einem (Spieg-
litzer Schneeberg) ‚blos durch Beobachtungen im ersten Vertical.

Ueberdies wurde von Professor Bruhns in der Nähe von Leipzig ein Polygon von
5 Orten nach Polhöhe und Azimuth festgelegt, um zu untersuchen, ob in jener Gegend Lo-
calattractionen vorhanden seien.

/ Anlangend die bei den Beobachtungen anzuwendenden Methoden und Instrumente,
erschienen wesentliche Aenderungen an dem von der permanenten Commission in den Jahren
1864 und 1867 aufgestellten Programme nicht nöthig; nur glaubt man heute auf Grundlage
neuer Erfahrungen, einige der dort berührten Punkte nochmals hervorheben zu sollen und

zwar zunächst bei Breitenbestimmungen:

 

a

sty hs lA lh i nn

 
