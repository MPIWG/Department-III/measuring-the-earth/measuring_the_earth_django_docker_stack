IF
|
|
R

BE EL TED AM RG RET RUT

Sa a a

|
te
ll
a
ia

 

106 |

Hiermit ist der zweite Punkt erledigt und es kommt der dritte Punkt zur Verhand-
lung, nämlich die Pendelbeobachtungen, wobei zunächst erwähnt wird, dass das von Herrn
Peters übernommene Referat in der Plenarsitzung an Herrn Bruhns von ersterem übertra-
gen wurde.

Herr Bruhns bemerkt, dass die in der vierten Sitzung der zweiten allgemeinen Üon-
ferenz vom 3. October 1867 angegebenen Maassnahmen festgehalten werden können, erwähnt,
dass verlässliche Reversionspendel von Repsold (ungeachtet des Ablebens des frübern Vor-
standes dieser Firma) geliefert werden und wünscht, dass man sich mit allfälligen Bestellun-
gen an diese Firma wende und auf die von Herrn Hirsch in Vorschlag gebrachten Verbesse-
rungen der Einrichtungen Rücksicht nehme.

Herr Hirsch bemerkt, dass die durch die Beobachtung der Schwingungen und durch die
directe Messung gefundenen Ausdehnungs-Coefficienten für das Pendel und die Maassstäbe nicht
in genügender Uebereinstimmung stehen und glaubt, diese Differenz könne dadurch erklärt
werden, dass die Länge sowohl als die Ausdehnungscoefficienten möglicher Weise verschieden
seien, je nachdem die Stäbe vertical stehen oder horizontal liegen; er hält es daher für sehr
wünschenswerth, die Ausdehnungscoefficienten direct mit Hülfe eines neu zu construirenden
Apparates in verticaler Richtung zu bestimmen. Ferner ıst es sein Wunsch, dass die Maass-
stäbe und Pendel, die in den verschiedenen Ländern in Anwendung kommen, mit einander
verglichen werden, etwa durch das Oentralbüreau. Er fasst seine Wünsche in dem folgenden
nach einer Diskussion zwischen den Herren Bruhns, Hirsch und Seidel vereinbarten Antrage
zusammen: |

Es wird dem Centralbüreau und der permanenten Commission empfohlen, auf die
Herstellung eines Apparates Bedacht zu nehmen, mit welchem die in den verschie-
denen Ländern angewandten Pendel und ihre Maassstäbe verglichen und ihre ab-
soluten Ausdehnungscoefficienten bestimmt werden können und zwar in verticaler
Lage.

Dieser Antrag wird einstimmig angenommen. Vor Durchführung dieser Maassregel
und um jedem Beobachter das Mittel in die Hand zu geben, den Ausdehnungscoeflicienten
des Pendels und der Maassstäbe zu bestimmen, wird die von Herrn Plantamour angewendete
Methode der empirischen Bestimmung aus den Schwingungszeiten bei sehr verschiedenen
Temperaturen empfohlen.

Schliesslich wird auf Anregung des Herrn Siruve die Vergleichung aller Apparate
durch unmittelbare Vergleichung ihrer Schwingungszeiten an ein und demselben Orte zum

Commissionsbeschluss erhoben.

Herr Struve fügt hinzu, dass es wünschenswertl sei, einen Plan auszuarbeiten über die
Punkte, an denen Intensitätsbestimmungen der Schwere vorzunehmen wären. Herr Bruhns
meint, die Anzahl sei möglichst zu beschränken (nicht an allen Punkten erster Ordnung), hin-
gegen besonders an Orten mit voraussichtlicher Localabweichung derartige Beobachtungen zu

vermehren.

i
i
3
À
4
x
i
3
i

hl

aces ahmed ki i hi u an nn

 

 
