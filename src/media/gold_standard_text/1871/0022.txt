 

200

Im Juni 1869 benachrichtigte mich Steinheil, dass er über die Grundidee des neuen
Comparators im Klaren sei, und dass ich nach München kommen möchte, um mit ihm das
ganze Detail der zu lösenden Aufgaben durchzugehen. Ich folgte seinem Rufe und nach
14 Tagen waren wir mit Allem im Reinen. Der Mechanikus Stollenreuther in München über-
nahm die Ausführung im Lauf eines Jahres und unter Steinheil’s specieller Aufsicht. Im Juni
1370 waren, dem Versprechen gemäss, auch alle einzelnen Theile des Comparators fertig
geworden und ich ging im Juli wieder nach München um der Zusammensetzung unter Stein-
heil’s Anleitung beizuwohnen und um den Apparat im allen seinen Theilen nebst der Bestim-
mung der Constanten genau kennen zu lernen. Hierbei zeigte es sich, dass die Bessel'sche
Methode des Anschiebens zur Ermittelung der Linge der Vorlage-Cylinder bei Glas-Cylin-
dern nicht anwendbar ist, und dass diese Ermittelung nur durch eine Theilmaschine mit Sı-
cherheit zu bewerkstelligen sei. Herr Stollenreuiher übernahm den Auftrag, nach Steinheil's
Angabe eine solche bis zum Frühjahr 1571 zu liefern.

Zur Aufstellung des Apparates wollte Steinheil selbst nach Berlin kommen. Für die
Ausführung der Beobachtungen in der nächsten Zeit erhielt Herr Professor Voit vom Poly-
technikum in München einen 1’/,jährigen Urlaub. (Generalbericht pro 1870.) |

Nachdem voraussichtlich Alles auf das Beste geordnet war, reiste ich Ende August
von München ab, und kaum drei Wochen später erhielt ich schon die traurige Nachricht
vom Tode Steinheil's.

Am 1. October traf Professor Voit mit dem Apparat in Berlin ein. Er und Professor
Sadebeck stellten ihn im Laufe des Winters auf, wobei noch mancherlei Schwierigkeiten zu
überwinden waren, und bestimmten sodann die Brennweite des Fernrohrs und den Nullpunkt
des Mikrometers.

Die Einrichtung des Comparators ist im Generalbericht pro 1869 von Steinheil selbst
beschrieben, ich bemerke daher hier nur noch, dass die Vergleichungen in einer Flüssigkeit
und nicht in der Luft gemacht werden, was besonders wichtig und nothwendig ist bei Stäben
von sehr verschiedener Wärmeleitung.

Ueber die Genauigkeit der Messungen giebt der Generalbericht pro 1810 Seite 51
Aufschluss.

 Steinheil hatte die Absicht seine Glasmeter in Berlin mit der Besselschen Toise zu
vergleichen, um sowohl der Gradmessungs-Conferenz als auch der nichsten Meter-Conferenz
richtige Meter fertig vorlegen zu können. Um für die Gradmessung diese seine Absicht zu
realisiren, habe ich aus seinem Nachlass für das Centralbüreau zwei Glasmeter erworben.
Steinheil hatte dieselben früher schon mit einer Schumacher'schen Toise verglichen, konnte
aber auf seinem Comparator nicht die Ausdehnung des Glases messen. Herr Professor Voit
hat jetzt die Beobachtungen zur Bestimmung der Ausdehnung des Glases auf dem neuen
Comparator ausgeführt, auch hat er die beiden Glasmeter mit meiner Copie der Besselschen
Toise verglichen, die schon in Pulkowa und in Southampton zu Vergleichungen gewesen ist;

es können aber bis jetzt noch keine definitiven Resultate gegeben werden, weil Herr Siollen-

Eh ar Mas)

ea

A abd ded

dau di

 
