 

a ee

Lo RR

HUIT ART

genumi

rer

ach nina Gan hibiine dee aid ed
dd QE TT

re

129

Par l'accord de ces 48 valeurs, on obtient, pour une hauteur polaire individuelle,
erreur probable + (409 et pour erreur moyenne — 0168.

P

On constate que ces valeurs ne dépendent nullement des positions du cercle, attendu

que ow— pr — — 0'044.
En rapportant les moyennes de soirées des hauteurs polaires par la réduction
or — 1 = — 0.246, au groupe I, on obtient les valeurs moyennes suivantes :
1895 juin 6 16,244 5 16,244 la den i
ı dnote > : ioc. oO! À
12 16,062 5 16,064 1 16,062 «6 anne
13 16,020 « 16,154 3 OO
22 16,239 4 16,172. « 10200 3.) len ;
27 16,120 : 16,119 4 ww. cn
juillet 2 60 > 16,314 3 16,258 >
y 16,094 5 fe. oe

La moyenne générale est + 52° 30 16155 (48), el en tenant compte des poids, mais
en négligeant pour le moment la variation éventuelle du pôle, on obtient les erreurs suivantes :

err. prob. + 0'053
err. moy. + 0,079
err. prob. + 0,019
err. moy. -- 0,098

Pour la moyenne d’une soirée, déduite de 6 couples }

eS

Pour le résultat déduit de 48 couples

m,

rs

On constate que les valeurs moyennes de soirée, aussi peu que les valeurs indivi-
duelles, ne dépendent en aucune facon des positions du cercle, attendu qu’on a ici:
gw — GE = — 0-018.

Les couples d’étoiles qui ont servi 4 ces mesures ont pour les grandeurs optiques et
pour les différences de distance zénithale les valeurs suivantes :

 

It | 0203 293% Tec 1 28°60 05
2:38.13 .159 2 61 70 | 32

3 38,09 12 6106 1:96
40,56 21 4 DD, 060 45

0 0,0, 6.8 -10.0 ao ı 0 = 00
Moyenne + 049 Moyenne — 004

M. le Président remercie M. Marcuse de sa communication et propose de continuer
la lecture des Rapports spéciaux. En premier lieu, il invite M. le colonel Bassot à bien vou-
loir rendre compte des mesures de bases exécutées dans les trois derniéres années,

ASSOCIATION GEODESIQUE — 47

 
