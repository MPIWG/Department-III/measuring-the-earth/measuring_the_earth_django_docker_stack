 

|
4

—

258

2) Der ganze Art. 7 und das letzte Stück des Art. 9: « Beim Hinzutreten.... Betrage
zurückerstattet» sind zu streichen.

3) In Art. 8 fallen die Worte « auf nahezu 10 Jahre » weg, und die Worte «nahezu
dem 8!/,fachen », sowie die Jahreszahl «1887» ändern sich in «dem 2Öfachen» und
«1897 ».

4) In Art. 9 erster Linie sind die Worte «zur Zeit» zu streichen.

5) In Art. 11 sind die Worte « wobei jeder Staat eine Stimme hat» in « wobei jeder
Staat, je nachdem er der einen oder der andern der in Art. 8 festgestellten Gruppen a) b) c) d)
angehört bezw. 1, 2, 3 und 6 Stimmen hat» zu ändern.

6) Die Art. 8-15 erhalten die Nummern 7—14 und die im alten Texte der Ueberein-
kunft angeführten Artikelnummern sind in Uebereinstimmung hiermit zu bringen.

Die oben erwähnten Aenderungen haben den Zweck 1) die Begrenzung der Dauer
der Uebereinkunft und 2) die Bestimmung über eine Festsetzung des totalen Jahresbetrags der
Dotation aufzuheben. Durch die letztgenannte Aenderung entgeht man der Complication,
welche eine neue Vertheilung des Dotationsbetrages bei Hinzutreten bisher nicht betheiligter
Staaten mit sich führt. Durch die unter 5) erwähnte Aenderung wird der Einfluss der Stimme
eines Staates ungefähr mit dem Geldbeitrage des betreffenden Staates proportional werden.
Eine solche Aenderung scheint mir besonders gerecht bei der Wahl der Mitglieder der per-
manenten Commission, weil diese Commission mit der Verwaltung der Geldmittel der Dotation
betraut ist.

ZACHARIAE.

FRANCE
I. Premieres propositions.
COMMISSION GEODESIQUE

Extrait du procés-verbal de la séance du 20 novembre 1894.

1) La Commission permanente serait supprimée, el ses fonctions actuelles réparties
entre une commission administrative, chargée des questions financières, et plusieurs com-
missions scientifiques spéciales, ayant chacune un objet et un programme déterminés.

2) Le bureau de l'Association, composé d’un président, d’un vice-président et d’un
secrétaire général, serait élu par la Conférence générale des délégués des différents pays,
pour une durée égale à celle de la Convention elle-même. Le Directeur du Bureau central
ferait de droit partie du bureau.

3) Dans le vote par État, l'Allemagne, considérée comme un État homogène, ne dis-
poserait que d’une voix, comme la France, la Russie, l'Italie, l’Autriche-Hongrie, etc.

4) Chaque Etat pourrait nommer, pour le vote des questions scientifiques, un nombre
maximum de délégués en rapport avec sa population.

 

 

Alb ah oh A AA a hh Ah tN hie

nude

ib kassel

in am be Weed rue

|
|
|
