©p. 218

 

(EEE ES SACS

INDICATION

LATITUDE

 

ee.

VII. — Grande Bretagne.

DIRECTEURS

 

me mem mu amener

 

 

219

 

 

52

 

 

 

 

 

 

 

 

 

Jo " A n Ä
N oe LONGITUDE| EPOQUE One a INSTRUMENTS REMARQUES
|
202 Ron... 4 00082212. 10019. 87, 1821 Génér. Colby, capits. Vetch, | Ramsden de 3 p.
Drummond,
203 | Base de Rhuddlan. | 53 17 12 | 14 11 31 1806 Woolcot. id.
Terme Est.
204 | Base de Rhuddlan. | 53 17 4] | 14 451 1806 id. id.
Terme Ouest.
205 | Rn Bea. ........: 01 50 8 11 53 52 ‘1848 Serg. Winzer. id.
206. Byder’s Hl...... 50. 30 21 | 18 46 2 1845 Serg. Donelan. id.
207 | St. Agnes Beacon..| 50 18 24 | 12 26 49 1707. Génér. Mudge. id.
208 | St.AgnesLighthouse 49 53 33 | 1119 7 1850 Serg. Steel. Airy’s Zenith Sector.
- 209 | St. Ann’s HiM..... ol 201 17,82 1792 Génér. Mudge. Ramsden de 3 p.
210 | St. Martin’s Head..| 49 58 0 | 11 23 52 1850 Caporal Wotherspoon. Ramsden de 18 p.
211 | St. Paul’s, Tr. Lon-
(ole 0.2 21.8049 17 33 57 1848 Serg. Steel. id.
212 | St. Peter’s Oh. Tr.. 51 21 5|19 457 1844 id. id.
213 | Sawel .........; 54 49 11 | 10 37 29 1827 Génér. Portlock. Ramsden de 3 p.
214 | Saxavord ......... 60 49 39 | 16 49 25 1817-47 | Gardner, serg. Steel. Ramsden de 3 p. et
Théod. de 7 p.
215 | Sayrs Law........ 5p 50 49 | 14 59 38 1809-46 Gardner, serg. Winzer. Ramsden de 3 p.
216 Sca Fell.......... o4 01 15 | 1427 10 1841 Capits. Pipon, Craigie. id.
217 Scarabin......... 58 15 18 14 12 1839 Lieuts. Hornby, Robinson, id.
Pipon.
Scournalapich..... Di Peu 0 | 1206 16 1846 cole Donelan. id.
Severndroog ...... OL 21 59 ly AB 27 1822-48 Génér. Colby, capit. Kater, id.
Gardner, serg. Donelan. |
88c| Shanklin Down ....| 50 37 6 | 16 27 24 1846 Serg. Steel. Ramsden de 18 p. | Sehr nahe an 88,
220 | Slieve Donard..... 54 10 49 | 11 44 36 1826-45 Gener. Portlock, sergs. | Ramsden de 3 p.
Forsyh, Winzer, caporal
Stewart.
221 | Slieve League..... D4 739° 4412 8 5726 1828 Génér. Portlock. id.
222 | Slieve More....... 54 0835| 230 19 1831 Serg. Doul. Theod, de 2 p.
223 | Slieve Snaght..... 55 11 47 | 10 19 48 | 1827 Génér. Portlock. Ramsden de 3 p.
224 | Snowdon.......... 59446 128 171 1842 Capit. Pipon, lieut. Stanley. id.
225 | Southampton...... 50 54 49 | 16 15 39 1845 Soldat Scott. Tneod. de 10.p.
226 | South Berule..... 54 8 58 | 12 59 42 1845 Sergs. Steel, Forsyh. Ramsden de 3 p.
227 | South Lopham Ch. :
Mr... 5223 441, 18 39 37 1844 Serg. Bay. Troughton de 2 p.
228 | South Ronaldshay.| 58 46 55 | 14 43 15 1821 Capits. Vetch, Drummond. | Ramsden de 3 p.
229 | Southwold Ch. Tr..| 52 19 41 | 19 20 33 1844 Serg. Bay. Troughton de 2 p.
230 | Start Lighthouse..| 59 16 40 | 15 17 16 |
231 | Stoke Ch. Tr...... 5 592, 18 33 15 1844 id. id. |
232 | Stoke Hill........ Sl 16 19 | 1b 36 4 1849 Serg. Jenkins. Ramsden de 3 p.
233 | Store... 2. 01 30 25 ı Il 28 5 1847-48 Serg. Winzer. id.
234 | Btronsay.......... 09 6 88 ilo. Wall 182] Capits. Vetch, Drummond. id.
235 | Swaffham Ch. Sp..| 52 38 53 | 18 21 14 1843 Serg. Steel. Ramsden de 18 p.
236 | Swyre Barrow....| 50 36 21 | 15 34 11 1845 Serg. Donelan. Ramsden de 3 p.
231, Vara oo. - 924106 112647 1829 Génér. Portlock. id.
152b| Tarbathy ......... 07.2122 15 25539 1817 Gardner. id. || Nahe bei 152.
238 | Daur......2..... D2 1418 | 952 17 1832 Gener, Portlock. id >
239 | Tawnaghmore..... a4 171 40 | 8 358 1829 Serg. Doul. Theod..de 12 p.
240 | Telegraph Tower..| 49 55 44 | 11 21 37 1850 Caporal Wotherspoon. Théod. de 18 p.
24l , Tharfield ......... D2. 1.2 17 81 45 1843 Serg. Donelan. Theod. de 3.p.
242 | Fhaxted Ch. Sp. ..| 51 57 15 | 18 021 1844 Serg. Steel. Theod. de 18 p.
PAS TION...) D2 38 42 | 16 47 49 1800-43 Woolcot, serg. Mulligan. Théod. de 3 p.
244 | Tofts Ch. Tr. ..... 52 30 41 19 14 14 1845 Serg. Bay. Troughton de 2p. !
245 | Trevose Head..... 50 32 54 | 12 37 52 | 1797-1846 | Génér. Mudge, serg. Do- | Ramsden de 3p. |
nelan.
240) EEOStAR. 82). 7... Do 2 444 Ti 307382 | 1827 Gener. Portlock. id.
247 | Upcot Down ...... 51 28 44 | 15 51 24 | 1850 Serg. Jenkins. id.
248 | Viears Carn....... 54 17 564 | 11 A 3 1827 Génér. Portlock. id.
249 | Walpole, St. Peter’s 43 43 | 17 53 13 | 1843 Serg. Bay. | Troughton de 2 p.

 

rede ehe ee

ossi lisse
