 

I
h
|
ig
h
h

Ont voté oui: le Danemark, la France, la Hesse, l'Italie, la Norvège, l'Espagne, la
Suêde, la Suisse et les Etats-Unis.

Ont voté non : le grand-duché de Bade, la Bavière, la Belgique, les Pays-Bas, l'Au-

€

triche-Hongrie, la Prusse, le Wurtemberg et le Japon.

L'article 5 ainsi modifié est ensuite adopté sans opposition. Îl est conçu comme suit:

«Le President et le Vice-président de l'Association et le Secrétaire perpétuel sont
élus par la Conférence générale pour la durée de la Convention.

«En cas de vacances, le remplacement sera fait par la Commission permanente, par
voie de correspondance ou, s’il le faut, en séance, par celte Commission convoquée ad hoc. »

A l’article 6, M. Haid propose de rédiger le dernier alinéa de la manière suivante :

« La distribution de la dotation entre ses divers groupes de dépenses est préparée par le
bureau de l'Association et soumis à la ratification de la Conférence générale. »

r

M. le Président et M. Hirsch s'étant opposés à cet amendement, il est retiré par son
auteur.

M. Bakhuyzen explique que, dans le n° 4 de l’article 6, on a supprimé dans le con-
tre-projet la première phrase afin d'arriver à une rédaction aussi générale que possible.
Malgré cette suppression, le n° 4 de Particle 6 comprend cependant tout ce que la Commis-
sion permanente a eu l’intention d'exprimer.

A près cette remarque, l’article 6 est adopté dans la rédaction du contre-projet à
l'appel nominal, par 10 voix contre 7 qui se sont prononcées pour celle du projet de la Com-
mission permanente.

Ont voté pour le texte du contre-projet (voir Appendice No Ib): le grand-duché de
Bade, la Bavière, la France, la Norvège, les Pays-Bas, l’Autriche-Hongrie, l'Espagne, la Suède,
les États-Unis et le Wurtemberg.

Ont voté pour le texte de la Commission permanente : la Belgique, le Danemark, la
Hesse, l'Italie, le Japon, la Prusse et la Suisse.

On passe ensuite à l’article 8 qui est adopté sans opposition dans la rédaction du
contre-projet. (Voir Appendice N° IV,.)

Dans la discussion sur Particle 9, M. Weiss considérant que dans l’article 7 la dota-
tion est déterminée par un minimum et non par une somme fixe, de sorte qu’une augmen-
tation éventuelle n’est pas exclue, désire que lon introduise dans l'article 9 une clause
déterminant la répartition de pareilles augmentations soit temporaires, soit permanentes.

 

A nd lee

ran d.

cuits

à sai Amel dahin Lohan baba ad na mu

 
