 

TN TT

Kama ove

remy YT

bis trad Ae MANNA Qe Uh Ak ya
eC A RSI

43
Schwerkraft in Honolulu durch Herrn Preston auch kleine Variationen sich gezeigt haben.
Seines Wissens sei hinterher noch eine Beziehung zur Fluth aufgefunden worden ; vielleicht
kann Herr Titimann darüber nähere Auskunft geben.

Herr Tittmann erwidert, dass die Resultate der Untersuchungen über den Zusammen-
hang dieser Schwereschwankungen mit den Erscheinungen von Ebbe und Fluth durchaus
negativer Art gewesen seien.

Den letzten Bericht über die Schwere-Messungen der österreichisch-ungarischen
Seeoffiziere, während der Jahre 1892— 1894, erstaltet Herr von Kalmar:

«Die betreffenden Arbeiten sind von der Marine-Centralstelle in einem Bande ver-
öffentlicht, welcher vertheilt worden ist.

«Derselbe enthält 39 Stationen in Dalmatien, 55 in Italien, 25 in Indien, auf den
Südsee-Inseln und ın Australien ; 9 Stationen im Atlantischen Ocean und 4 im Norden.

«Der nächste wird die auf Gorfu gemessene Station, sowie die bis dahin von den 5
jetzt auf Mission befindlichen Schiffen eingelangten Schwere-Messungen enthalten. Aus der
Uebereinstimmung der Resultate, welche verschiedene Beobachter zu verschiedenen Zeiten,
mit demselben Instrumente, oder mit verschiedenen Instrumenten, aber von derselben Gon-
struction, und bei Anwendung derselben Beobachtungs-Methoden, auf den gleichen Beobach-
tungs-Orten, erhalten haben, lässt einen Schluss auf die Güte der Instrumente und Methoden
ziehen. » (Siehe Beilage B. X ©.)

Für Preussen erstattet zunächst Herr von Schmidt den Bericht der K. Landesauf-
nahme. (Siehe Beilage B. Xla mit Anhang.)

Fur das preussische geodätische Institut erstattet Herr Helmert den Bericht über die
Arbeiten des Jahres 1895. (Siehe Beilage XI».)

Aufeine Anfrage des Herrn Hersch, ob die in dem Bericht des Herrn Helmert er-
wähnten Schwankungen des Bodens im Betrage von 1 ©m Jahresschwankungen seien, ertheilt
Herr Helmert Auskunft dahin, dass dies die äussersten Schwankungen sind, welche im Laufe
der 1!/, jährigen Beobachtungsperiode überhaupt wahrgenommen wurden. Um die Frage
der Periode erörtern zu können, haben die betreffenden Beobachtungen noch nicht genügende
Dauer.

Herr Forster, in seiner Eigenschaft als preussischer Delegirter, wünscht den Bericht
des Herrn Marcuse durch einige Mittheilungen zu ergänzen :

« Die Ergebnisse der bisherigen Versuche zur photographischen Bestimmung der
Polhöhenschwankungen können bis jeizt noch nicht als massgebend angesehen werden, da
die Anzahl der Beobachtungen noch zu gering ist. Es sind auch noch einige Fragen zu er-
ledigen, bevor diese Methode als feste Grundlage einer internationalen Organisalion solcher
Beobachtungen dienen kann. Insbesondere ist die Vergleichung der unmittelbar erlangten

 
