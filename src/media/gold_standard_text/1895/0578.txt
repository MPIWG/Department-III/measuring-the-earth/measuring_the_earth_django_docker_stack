 

358

geführt von der Trigonometrischen Abtheilung des Generalstabes. Herausgegeben von J. J.
Baeyer, Oberst und Abtheilungsvorsteher im Generalstabe und Dirigent der Trigonometrischen
Abtheilung. Berlin, 1849.

A. Die Verbindung der Preussischen und Russischen Dreiecksketten bei Thorn und
Tarnowitz. Ausgeführt von der Trigonometrischen Abtheilung des Generalstabes. Heraus-
gegeben von J. J. Baeyer, Generalmajor von der Armee und Dirigent der Trigonometrischen
Abtheilung. Berlin, 1857.

9. Die Triangulation von Thüringen. Ausgeführt in den Jahren 1851 bis 1855 von
der Trigonometrischen Abtheilung des Königl. Preussischen Generalstabes. Berlin, 1859.

6. Die Königlich Preussische Landestriangulation. Triangulation der Umgegend von
Berlin zwischen 52° 12’ und 92° 48’ Breite und 30° 30° und 31° 30’ Länge. Herausgegeben
vom Bureau der Landestriangulation. Berlin, 1867.

7. Die Königlich Preussische Landestriangulation. Haupidreiecke. Erster Theil.
Hauptdreiecke in der Provinz Preussen, an der Weichsel und östlich derselben. Herausgegeben
vom Bureau der Landestriangulation. Berlin, 1866. (Ersetzt durch Nr. 8.)

8. Die Königlich Preussische Landestriangulation. Hauptdreiecke. Erster Theil.
Zweite vermehrte Auflage. Herausgegeben vom Bureau der Landestriangulation. Berlin, 1870.

9. Die Königlich Preussische Landestriangulation. Haupldreiecke. Zweiter Theil.
Erste Abtheilung : Die Haupttriangulation in Schleswig-Holstein. Herausgegeben vom Bureau
der Landestriangulation. Berlin, 1873.

10. Die Königlich Preussische Landestriangulation. Hauptdreiecke. Zweiter Theil.
Zweite Abtheilung: Die Märkisch-Schlesische und die Schlesisch-Posensche Kette und deren
Ergänzungen. Herausgegeben vom Bureau der Landestriangulation. Berlin, 1874.

11. Die Königlich Preussische Landestriangulation. Hauptdreiecke. Dritter Theil.
Herausgegeben von der Trigonometrischen Abtheilung der Landesaufnahme. Berlin, 1876.

12. Die Königlich Preussische Landestriangulation. Hauptdreiecke. Vierter Theil.
Die Elbekette. Gemessen und bearbeitet von der Trigonometrischen Abtheilung der Landes-
aufnahme. Mit einer Tafel. Berlin, 1891.

13. Die Königlich Preussische Landestriangulation. Hauptdreiecke. Fünfter Theil.
A. Die Schlesische Dreieckskette (Neubestimmung der Punkte Bischofskoppe, Annaberg und
Pschow). B. Der Anschluss bei Tarnowilz. C. Der Oesterreichische Anschluss. D. Das Schle-
sisch-Posensche Dreiecksnetz. E. Die Markisch-Schlesische Dreieckskette. F. Die Schlesisch-
Posensche Dreieckskette. Gemessen und bearbeitet von der Trigonometrischen Abtheilung
der Landesaufnahme. Mit einer Tafel. Berlin, 1893.

14. Die Königlich Preussische Landestriangulation. Haupidreiecke. Sechster Theil.
A. Die Hannoversch-Sächsische Dreieckskette. B. Das Basisnetz bei Gottingen. C. Das Säch-
sische Dreiecksnetz. Gemessen und bearbeitet von der Trigonometrischen Abtheilung der
Landesaufnahme. Mit einem Uebersichtsblatt und siebenundzwanzig Skizzen. Berlin, 1894.

15. Die Königlich Preussische Landestriangulation. Hauptdreiecke. Siebenter Theil.
Erste Abtheilung: Das Thüringische Dreiecksnetz. Zweite Abtheilung : Die‘ Ergebnisse der
älteren Messungen. Mit Anhang: Alphabetisches Verzeichniss der in Hauptdreiecke I. bis

 

sh aa

blauer

 
