 

13

bo

4

support en regles et töles de fer, au-dessus duquel se trouve une conduite de gaz avec 40
petites flammes. Dans l’intérieur du tube pénétrent deux thermométres, divisés en degrés
jusqu’a 100°, et en outre un flotteur en verre pour indiquer le niveau de lhuile. Un robinet
établit la communication avec une pompe pneumatique, et un manomètre indique la pression
à l’intérieur. Un autre robinet fait ea le tube avee un vase dans lequel on peut
préalablement chauffer l'huile. L'espace entre la partie RÉ du tube et la tôle de fer
du support peut être rempli de sable dans le cas où le chauffage ne se ferait pas assez unifor-
mément. Tous les joints sont faits en cuir.

« Après avoir dans un premier essai obtenu la ion de l'huile seulement
dans les couch nes superficielles du bois, on a réussi, dans une seconde expérience, à forcer
l'huile jusqu'à l’intérieur. Chez une des règles on a remarqué que limprégnation n’était
pas aussi complète d’un côté que de lautre. Cela s'explique par le fait que la règle en
question avait déjà été trempée dans l’huile à la première expérience, mais seulement en
partie, parce que par mégarde on l'avait introduite obliquement dans lappareil; or, comme
dans ce premier essai l'huile avait du moins faiblement pénétré, cela est devenu un obstacle
pour limprégnation ultérieure. Cette couche extérieure d'huile a empêché probablement la
sortie de l'air et de la vapeur contenus dans l’intérieur du bois, et d’autre part l'entrée libre
de l’huile ; tout cela fait bien augurer pour la réussite.

« Enfin, je dirai encore quelques mots sur la seconde expérience.

« Après avoir introduit la règle dans le tube vide, et fermé ce dernier au moyen
d’une vis, on a chauffé l'air du tube jusqu’à 90° environ et en même temps vidé lair, par la
pompe, jusqu’à 150 "2% de pression. Après un intervalle de repos, pendant lequel le thermo-
mêtre était tombé à 80°, on l’a chauffé de nouveau jusqu'à 105°. En méme temps, le mano-
mètre avait monté un peu, probablement par suite de la vapeur et de l'air sortis de la règle.
Ensuite, on a de nouveau diminué la pression jusqu’a 100™™ tout en maintenant la tempéra-
ture au-dessus de 100°. Puis on a laissé refroidir le tout peu à peu, tout en maintenant la
pression. Environ 11/, heure après le commencement de expérience, j'ai fait entrer l’huile
de lin, chauffée d’avance à 35°. Ensuite, on a porté la température à 65° et comprimé l'air
dans le tube à 1!/, atmosphère. Le tout est resté pendant °/, d’heure dans ces conditions,
puis on a laissé écouler l’huile. Enfin, après avoir refroidi le tube, on en a sorti la règle,
qui a été séchée à l'air et au soleil. »

D. Résumé des expéditions de publications géodésiques, faites par le Bureau central.

Depuis la Conférence d’Innsbruck en 1894, le Bureau central à reçu, pour être dis-
me: les publications suivantes :
. De M. le professeur D' Th. Albrecht, à Potsdam: Bericht über den gegenwärtigen
Stand oe Gr ioestag der Breitenvariation . . .. 0900 exemplaires
2. De la division trigonométrique de la Pande bu royale fe
Prusse, 4 Berlin: Abrisse, Koordinaten und Höhen, XII. Theil, nebst Er-
gäanzungsblätiern= ......

I
D
vy

 

3
3
|
3
3
i
i

edle

huis

bis

us, amd a a alu AL bade dl addi.

 
