 

2

Auch bei der gegenwärtigen Bearbeitung habe ich meine Untersuchung auf
Monatsmittel begründet, oder wo mir solche nicht zu Gebote standen, Mittelwerthe !
für angenähert S0tägige Perioden gebildet, weil nur bei einer Zusammenfassung der
Resultate über längere Zeitabschnitte hinaus eine hinreichende Elimination der Haupt-
fehlerquelle derartiger Messungen: der durch die allgemeine Witterungslage bedingten
Refractionsstörungen, zu erwarten stand.

Diese Ausgangswerthe sind in Columne 3 eingetragen. Die am Schlusse dieser
Columne verzeichnete mittlere Polhöhe habe ich auf die Weise gebildet, dass ich
unter Anwendung des im Folgenden näher erläuterten Rechnungsverfahrens mittelst
successiver Annäherungen die Abweichungen der beobachteten Polhöhen von einem
mittleren Werthe derselben innerhalb des Zeitraumes von 1890.0—95.0 zu bestimmen
suchte, die einzelnen Monatsmittel um die erhaltenen Beträge corrigirte und alsdann
‘erst die von dem Hauptantheil der Polhöhenschwankung befreiten Zahlenwerthe zu
Mittelwerthen vereinigte. Durch Subtraction der so erhaltenen mittleren Polhöhen von
den Einzelwerthen habe ich hierauf die in Columne 5 enthaltenen Abweichungen
vom Mittel gebildet.

Statt dass ich nun aber entsprechend dem bisher vorwiegend angewandten
Verfahren diese Werthe 9—g, durch einen mehrgliedrigen Ausdruck nach Art des-
jenigen von Cnanprer darzustellen suchte, habe ich von der Erwägung ausgehend, dass
man bei der mangelhaften Kenntniss der wahren Ursachen der Breitenvariation in
betreff der mathematischen Form eines solchen Ausdruckes auf mehr oder weniger
i willkürliche Annahmen angewiesen ist, den im gegenwärtigen Stadium der Unter- i
i suchung wohl rationelleren Weg eingeschlagen, durch Combination gleichzeitiger Beob- 3
\ achtungen auf verschiedenen Stationen die wahre Bahn des Poles zu bestimmen und
von dieser ausgehend die den einzelnen Beobachtungsorten entsprechenden Differenzen
zwischen der Momentan-Polhöhe und der mittleren Polhöhe zu berechnen.

Dieses Verfahren ist für kleinere Zeitabschnitte bereits von Kosrinskr in seiner
im Jahre 1893 erschienenen Schrift: ,,Sur les variations de la latitude de Poulkovo
observées au grand Instrument des passages, établi dans le Premier Vertical“, sowie
von Marcuse in einem der Innsbrucker Conferenz erstatteten Bericht — vergl. Bei-
lage A. III. der „Verhandlungen der Innsbrucker Conferenz‘‘ — angewendet worden.

Damals lag aber noch ein weniger umfangreiches Beobachtungsmaterial vor,
l während dasselbe gegenwärtig ausreichend erscheint, um eine Ableitung der Bahn
| des Poles für den ganzen 5jährigen Zeitraum von 1890.0—95.0 mit Aussicht auf
| Erfolg vornehmen zu können. Ich habe daher unter Mitwirkung der Herren
I Dr. Gazze und Dr. Scaumanx auf Grund aller zur Zeit vorliegenden Messungsergebnisse
i von Neuem eine solche Bestimmung ausgeführt und daran anschliessend die den
| successiven Aenderungen in der Lage des Poles entsprechenden Variationen der
i Polhöhe für die einzelnen Beobachtungsorte berechnet.

Hinsichtlich der speciellen Rechnungsmethode habe ich in Anbetracht des
Umstandes, dass zur Zeit von einem Theile des Beobachtungsmateriales erst pro-

ar hin Dadi ada ed

rend

 

sasssscamalh Ach she at pe

 
