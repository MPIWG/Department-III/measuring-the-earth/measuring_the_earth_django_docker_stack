 

tiv eee
Lenses,

oven rm qe ar

TRE
a RL ee ur

u erh
ARTE

aa

di
VISTA

À Bai: :

ail HE

Ali cts À nd CCM di i IE
TERRA AO RER A A EE

“hui

81

«Controlle» Anstoss erregen, so könnte man dafür vielleicht einen passenderen Ausdruck
ausfindig machen.

Herr Seeliger schliesst sich den Ausführungen des Herrn Haid ausdrücklich an, da
er keinen Fortschritt in dem Amendement Celoria erblicken kann. Die Berathung sei gegen-
wartig zu denselben Schwierigkeiten zurückgekehrt, welche man gestern durch die nahezu
einstimmige Annahme des Amendements Bakhuyzen überwunden habe. Die Meinung der
grossen Mehrheit sei gestern deutlich zum Ausdruck gebracht worden, und er könne nicht
einsehen, wie die Unterzeichner des Amendements Geloria hoffen dürften, auf Grund dieses
ganz enlgegengesetzten Standpunktes eine Einigung zu erzielen.

Was das Wort «Controlle » beireffe, so sei das doch nicht ein Nachrechnen, sondern
bedeute eine gewisse Oberaufsicht in gemässigter Form. Wenn also die Herren des Präsidiums
mit den Vorschlägen des Directors des Centralbureaus nicht einverstanden sind, so können
sie den Wunsch nach einer Aenderung aussprechen und es findet dann eine Berathung statt.
Redner stellt dem Begriff « Controlle » den der « Initiative » gegenüber; die Initiative bei
dem Arrangement, die Details der Ausführung, Auswahl der Instrumente ete. sollen allein
dem Director des Gentralbureaus zustehen.

Herr Forster weist darauf hin, dass eine so gedeutete Controlle über den eigent-
lichen Sinn schon hinausgehe. Soviel er den gegenwartigen Director des Centralbureaus
kenne, werde derselbe gern mit seinen drei anderen Collegen sich ins Einvernehmen setzen
und ihren Rath und ihre Mitwirkung bereitwillig annehmen. Er begreife daher den Wunsch,
hier an dieser Stelle nicht noch ausdrücklich auf die Oberleitung hinzuweisen, da diese ohne-
hin durch die Organisationsprinzipien an anderer Stelle bereits gesichert sei. Es verstehe
sich eigentlich von selbst, dass das Gentralbureau die wissenschaftliche Leitung und Verant-
wortlichkeit für den im Bureau auszuführenden Antheil haben werde, ebenso wie es sich
nach anderen Bestimmungen der Convention von selbst verstehe, dass das Präsidium und die
Generalconferenz die allgemeine Oberleitung besitzen. Mithin stelle er anheim, die spezielle
Erwähnung des Centralbureaus in dem Art. 7 an dieser Stelle gänzlich zu beseitigen.

Herr Hirsch wendet sich zunächst gegen die Behauptung des Herrn Bakhuyzen, dass
in der bisherigen Convention von einer Oberleitung nicht die Rede sei. Im Art. 6 der be-
stehenden Convention ist ausdrücklich gesagt: « Um der permanenten Commission die ihr
zugewiesene Oberleitung des Centralbureaus sowie uberhaupt die wissenschaftliche und ge-
schäftliche Förderung des Unternehmens zu ermöglichen... » u. s. w.

Herrn Helmert gegenüber müsse er darauf hinweisen, dass der von demselben ihm
gestern überreichte Text einer seiner Ausführungen den Passus enthalte: «Der Director des
Gentralbureaus unterliegt jedoch der Oberleitung des Präsidiums der Erdmessung, zu welchem
er selbst gehört. » Daraus gehe hervor, dass die in der bisherigen Convention vorgesehene
Überleitung des Centralbureaus durch eine Commission oder, wie es später heissen wird, das
Präsidium der Erdmessung, auch für die Zukunft von Herrn Helmert ausdrücklich zugestan-
den wird.

ASSOCIATION GÉODÉSIQUE 11 —

 
