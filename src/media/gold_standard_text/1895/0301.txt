ama ane

3
i

 

 

 

rey

lassen, zunächst aber die noch übrigen
Berichte entgegenzunehmen. . . .
Bericht des Herrn Helmert tiber die Mes-
sungen der Schwerkraft (siehe Beilage
A ER te

eee des Herrn Te han den

Einfluss der Temperatur auf die Elastici-
tat der Korpetiewd 4 ))) « : -
Das Centralbureau wird von a
ermächtigt, bei der Physikalisch-Techni-
schen Reichsanstalt die Fortsetzung
dieser Untersuchungen zu beantragen .
Die Herren Kühnen und Helmert sprechen
über die aus der elastischen Biegsam-
keit des Pendels folgenden Fehler der
Schweremessungen . . . ;
Verlesung der Jahresberichte für:

Dänemark, von Herrn General von Za-
chariae (siehe Beilage B. IV).

Spanien, von Herrn d’Arrillaga (siehe
Annexe B. XY).

Staaten, von Herrn Titt-
mann (siehe Annexe B. XVI).

Auf Vorschlag des Herrn Helmert be-
schliesst die Conferenz, Herrn General
Duffield zu der Messung einer Drei-
eckskette längs dem mittleren Meri-
dian von Nord-Amerika zu beglück-
wünschen ©...

Vereinigte

Schweden, von Herrn Professor Rosen
(siehe Beilage B. XII).

Russland, von Herrn General Stebnitski,
verlesen von Herrn Hirsch (siehe An-
nexe B. XIV).

Schweiz, von Herrn Professor Hirsch

(siehe Annexe B. XIV).

Im Namen der schweizerischen geodäti-
schen Commission stellt Herr Hirsch
den Antrag, dass die Erdmessung
künftig die in den verschiedenen Län-
dern unternommenen magnetischen
Aufnahmen unterstütze. Nach statt-
gefundener Discussion wird in den
Gegenstand vorläufig nicht einge-
PÉTER ed Cu. à.

Württemberg, von Herrn Koch (siehe

Beilage B. XVID).
Herr Helmert gibt einen kurzen Auszug

Pag.

58-59

60

60

60

60-61

64

62-64

295

 

 

aus seinem Bericht über die Lothab-
weichungen (siehe Beilage A. VII)
Die Discussion über die Conventions-
Entwürfe wird auf die nächste Sitzung
verschoben sa. 094 paar .

Sechste Sitzung, 10. October 1895.

Der Herr Präsident schlägt vor, zunächst
über die Art. 6—9 des Entwurfs zu be-
rathen.. sn 40e En

Herr d’Arrillaga erklärt. die Gründe,
wesshalb die spanische Delegation sich
vorläufig der Abstimmung über Budget-
fragen enthalten werde 41 ess 09.

Herr Hennequin stellt, im Namen von
sechs Abgeordneten, den Antrag, zuerst
über die Art. 6—9 und alsdann über
Art. 2 abzustimmen . .

Auf Anfrage des Herrn Weiss Sr der
Herr Präsident Auskunft über den Cha-
racter der von ihm redigirten Denk-
schrift betreff der Erhöhung des Budgets

Herr Faye verlang!, dass zuerst über
Art. 2 abgestimmt werde; wenn die
permanente Commission nach den fran-
zösischen Vorschlägen angenommen
wäre, würde Frankreich für die erhöhte
Dotalion, Summen 2. 7

Die Herren Forster und Hirsch geben
nochmals die Gründe an, wesshalb in
erster Linie über die Dotationsfrage ab-
zustimmen sei . . ee

Herr Bakhuyzen theilt sein in französi-
scher Sprache gedrucktes Amendement
in deutschem Text mit; dasselbe stellt
die Verwendung der 44 000 M. unter die
Leitung und Verantwortlichkeit des
Gentralbuneaus 2...

Herr Lallemand schlägt vor, das gesammte
Projekt zwei Lesungen und Abstim-
mungen zu unterziehen. . .

Der Herr Präsident ist a st
bittet also zuerst über Art. 6-9 zu be-
schliessens sin cis sian ; ag

Herr Bakhuyzen en ie ie
für sein Amendement . a

Herr Foerster schlägt vor, aus dem Amen-
dement Bakhuyzen die Worte « unter

7.2 «

64

©
o>

66-67

67-68

68

68-69

69

70

 
