 

 

INDICATION

8.

VI. — France et Algérie.

a Y

DIRECTEURS

INSTRUMENTS |

|

 

 

 

 

 

 

 

 

 

 

 

 

 

de Lenoir de 0™.36 |

 

les. — Pour chaque |
angle furent exécu- ||
tées 20 répétitions. |

|

répétiteurs

chaqueangle furent |
exécutées 20 répé- |

Ne j ‘i s
2 DES POINTS FO LORGITIDE ol. ET. OBSERVATEURS
Triangulation de la Méridienne de Dunkerque. (Partie Sud — Suite.)
0. | Sermur........... 49.584.384 202 5.114
sl Orsnab...... ...,. “6 0 2,1048 15
2. Bordesv .......... 45 51 49 | 19 46 53
3 | La Fagitiere....... 45 44 Al | 19 54 42
o4 | Hermant.......... 45 45 14 | 20 13 54
sa Borne 4524 020 714
50:2 Melmae.. .......... : 45 34 14 | 19 47 10
Oi | ‘AUDASSIN . 45 11 4 | 19 48 20
58 | Violan. 0... 45. 100120 5 7 |
09 | La Bastide ........ 44 50 8 | 19 46 54 |
60 | Montsalvy ......... 44 42 53 | 20 9 58
61 | Rieupeyroux....... 44 19 .2 | 19 53 38 | a
62 | Rodez. ne 44 21 5 20 14 15 2 Cercles répétiteurs
63 | La Gaste. 0. 44 7 57 | 20 18 10
64 | Puy St. Georges...| 44 0 42 | 19 56 16 et 0,42 avec 4 ver-
65 | Cambatjou......... 43 ol 59 20 18 23 \ 1792 | niers qui donnent
66 | Mont-Redon ....... 43 43 1 | 19 58 16 at Sn ee | Delambre et Méchain. chacun la lecture
67 | Montalet .......... 43 41 0 | 20210) ol de 20” centesima-
68 | St. Pos AS 31 34 | 20 23 40 |
69 Nore... 2... à à 43.29 29 20.5 31
TOs AAV. 0 AD 8 94 100 17026
71 | Carcassonne ....... 31255120 0%
12 , Bugarach.......... 42 51 51 120 229
Vo A POMC ee 42 54 38 | 20 20 31
742. Borceral.......... 42 43 38 | 20 21 44
29. Bspira: 42 49 35 | 20 27 47
(OO Verne. ....2..2 42 43 17 | 20 32 55
Terme austral.
a7 Verneb......... 42 49 28 | 20 34 46
Terme boreal.

78 | Puy de la Estella.. 42 30 54 | 20 12 57
79 | Puy Camellas...... 42 26 27 | 20 29 26 |

|

Triangulation de la Méridienne de Fontainebleau à Bourges.

31 | Bois Commun......
42: Bourges... Déjà mentionnés.
29 | Chapelle de Ja Reine
80 | Haut du Ture...... r 2049702 202102592 Cercles
Si L'Gien.... 441 9) 20 15 40 avec 4 verniers qui
82. ASSIGN... io. | 47 25 56 | 20 25 51 a donnent chacun la
83 | Humbligny........ 21116 8,0 8 4 1826-27 _ Capitaine Delcros, lecture de 20" cen-
84 | Montargis......... 4 59 59 20 23 27 a lieut.t Roset et Stamati. | tesimales. — Pour
85 | Les Bezards....... 4748 19 20/2450
86 | Montifaux......... 4136 32 20, 35 15
Sis Bony. 6 oe ae 47 29 11 | 20 49 43 titions.
88 | La Charite ........ | 47 10 41 | 20 40 48
89 | Saligny-le-Vif. .. | 41 2 44 | 20 25 50 |

Î

Triangulation du Parallèle d’ Amiens. (Partie Orientale.)

12 | Villers-Bretonneux. De | |
10: | Sonrdon........... \ éjà mentionnés. eo Corabasır
14a} Beauquéne......... 1821-22. fi se | Id.
00,| Tihons 7.0.06... | 49° 49’ 35") 20°25’ 41” |

 

|

 

i
——————
i
1

| Général de la Gueire
I.

 

REMARQUES |
i

pe |
Memorial du Depö;

Tome

Id.

hehe sehn la sh

Td.

 
