 

as

VI.— France et Algérie.

 

 

 

 

 

 

 

 

 

 

 

 

 

INDICATION | DIRECTEURS
N° ; LATITUDE |LONGITUDE 5 ig MARQUES
DES POINTS | ee ET OBSERVATEURS IND BENARSDES
Triangulation Méditerranéenne. (Suite.)
429 | Cette.............. 452410") 21920"51 Cercles répétiteurs |. Mémorial du Dépôt
430 | Cabreres........... AS 35 A321 LI avec 4 verniers qui done vi" Guerre,
431 | Béziers......... .. | 43.20 511 20 52 23 donnent chacun la |
70 | Alarie. nn...) DAR mentionnes. | 65 | | Major Deloros, | Heskmnles - Baur
= hmm a (148 19181 20 %0 ieut.* Rembault et Pissis. ao u
AUCH.) 7. nos > , exécutées 20 répé-
72 | Bugarach.......... | Deja mentionnés. ae
Triangulation de Jonction avee l’Angleterre.
23 Dunkerque renee ea eyes 50 2 8.202 2 25 | Mémorial du Dé-
21 | Uassel 0. BO 47 5771 20 9 8 | ae a sue”,
443 | Gravelines......... 50 59 12 | 19 47 22 ne Paris 1e65
444 | Harlettes.......... 50 43 O0 | 19 38 15 | a a
445 | St. Inglevert...... OÙ 52 14) 1022 3 1861-62 Colonel Levret, Id
446 | Mont Lambert..... | 50 43 1 | 19 18 53 ne | capit.s Beaux et Perrier.
AA Ooldhame >, 0610| 135159
448 | Ramlicht.... .... 20 52 >38 | 18 10 57
449 | St, Peters......... ol 21 55 19 4155 |
| I
riangulation de Vile de Corse.
T gulation de l’île de Corse
450 | Monte Stello....... AOA Wiad 4059, | Memorial du De-
451 | Monte Capanna....| 42 46 15 | 27 49 57 eo ee ce
452 | Monte Castello..... 4 255 27284 vpn Paris, 1875.
453 | Monte S. Angelo ...| 42 27 46 | 27 4 22 |
454 | Monte Asto........ 42 31 27 | 26 92 26 |
455 | San Pietro ........ ı 42 23 45 | 26 89 28 |
456 | Traunato.......... 42 25 13 | 26 44 41 |
Ani 400nn 00e 2220 9 2021575 ze |
458 | Paglia Orba....... | 42 20 33 | 26 32 34 1863. oo a Théodolite de 8 p. |
459 | Monte Artiea...... AZ 159 591) 20% 4 ae i
460 | Linscinosa......... | 42 13 9 | 26 30 45 |
461 | Monte Cervello..... 42 8 26 | 26 33 41 |
462 | Saltelle.. ......... AD DY | ZO 25 20 |
A063... Vilullo............ 42 ia 37 | 20 21 2 |
464 | Carghöse .......... | 42 7 55 | 26 15 14 |
465 PIPalo. 4216 9126 207 |
Triangulation Primordiale Algérienne. (Partie Orientale.)
Base de Blidah....| 36°29' 18} 20°28' 21" 1860 | Capit. Versigny et Vernet. | Théod. n° 13 avec 4 | Mémorial du Dépôt
Terme Sud. verniers qui don- Fete re,
| nent chacun la lec- |
cues ea
Base de Blidah....| 36 34 35 | 20 26 58 || 1860-64 Capit. Versigny, Perrier | Cercle répét. n° 13. |
Terme Nord. | | et Roudaire. |
Atom 0 ks 86270 34 20 | 1860 Capitaine Versieny. Id. |
L'MOUZaia 1 20 22858 | 20.28 18 | 1860-61-64 | Capitaines Versigny, Per- | an ie no se
| rier et Roudaire. |
| Ferroukha......... | 36 27 38 | 20 35 24 | 1860-61 |
nl 33 oe 14 | 20 dee) el (| Capitaine Versigny. | Cercle répét. n° 13, |
| Saebkah Chergui...| 36 8 420 46 42 | 1861 |
| |

 

ne dau Lada lé nanas

 
