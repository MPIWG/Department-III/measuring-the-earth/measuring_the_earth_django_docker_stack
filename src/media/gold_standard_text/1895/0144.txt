 

 

On est en outre occupé au Bureau international des Poids et Mesures à utiliser les
méthodes de Michelson pour la détermination des unités micrométriques, savoir du milli-
métre et du centimetre, que l’on ne pourrait déduire, avec la même exactitude, directement
de la longueur du mètre. Il n’est pas impossible que, dans lavenir, on ne puisse, en perfec-
tionnant encore davantage les mesures terrestres de la vitesse de la lumière, obtenir par les
longueurs des grandes distances, comme on les mesure en géodésie, un contrôle suffisant
pour d’autres mesures linéaires et angulaires.

M. Albrecht exprime le désir qu'on puisse désormais, pour les observations des hau-
teurs polaires, réunir les méthodes photographique et optique, si possible, par le même
instrument. Les observations que M. Marcuse a faites avec un instrument d’une ouverture de
435™™ ne sont pas sans autre comparables avec les observations de Honolulu auxquelles à
servi une lunette d’une ouverture sensiblement plus petite. Il faudrait si possible répéter des
observations comparatives aussi dans un autre point pour se rendre compte de ce que la
méthode photographique peut donner dans les conditions les plus favorables; 1l serait même
à désirer que ces observations fussent faites par un autre astronome, afin d'arriver à une
opinion absolument incontestable. M. Albrecht ajoute que les observations optiques ont déjà
conduit à des exactitudes non seulement égales, mais supérieures même à celles indiquées par
M. Marcuse pour la méthode photographique. Une série de M. Becker a donné une erreur
moyenne de + 0/13 ; une autre série, exécutée par M. Albrecht lui-même en 1888, avec une
lunette de 68mm, à donné, pour un couple d'étoiles, une erreur moyenne de + Doi
enfin une troisième série, exécutée par M. Schnauder à Potsdam, avec le même instrument,
montre une erreur moyenne de + 016. Si ces données ne suffisent pas à prouver une
supériorité de la méthode optique, d'un autre côté la méthode photographique présente le
sérieux défaut qu’elle ne se préte pas à constater la période diurne de la hauteur polaire.
En tout cas, il serait désirable d’arriver 4 des résultats aussi comparables que possible, par
exemple en instituant en méme temps des observations directes de latitude dans le premier
vertical, au moyen de l'instrument universel.

M. Fœrster regrette de ne pas trouver dans l’exposé de M. Albrecht l’appreciation
juste des avantages qui, sans aucun doute, doivent être reconnus à la méthode photographi-
que, entre autres surtout Phomogénéité que cette méthode permet d'atteindre dans la com-
paraison de différentes observations.

M. Marcuse fait remarquer que, dans l'appareil de mensuration exposé, le grossisse-
ment du microscope pour l'échelle est de 25, tandis que les clichés sont mesurés avec un
erossissement de 12. On pourrait employer pour ceux-ci également un grossissement plus
fort, et obtenir ainsi un plus grand accord intérieur dans les résultats. Par contre, il ne croit
pas possible de faire avec une lunette zénithale photographique aussi des observations
oculaires.

M. von Sterneck trouve à la méthode photographique un défaut sérieux en ce qu’elle

 

er bias ehe

Peer d

Abbas hs methine

him bb a hs ah na is

i
|
|

 
