 

Lee conne nee nee armee dd were mins PEE À
AP PTT om"

et

Mn

ime YF IAT OT TT

=
=

Wi

Hérasaerer ans
nes

309
Aus der inneren Uebereinstimmung dieser 48 Werthe ergiebt sich für eine einzelne
Polhöhe der wahrscheinliche Fehler + 0110, der mittlere Fehler + 0.165.
Eine Abhängigkeit der obigen Poihöhen von der Reihenfolge der Kreislagen am
Instrument isi nicht vorhanden, da 9, — 2, = — 0.044 ist.
Bezieht man die Abendmittel der Polhöhen, mit Hülfe der Reduction eo,
— 07246, auf Gruppe I, so ergeben sich folgende Mitidiwerike::

1895 Juni 6. 16,244 5 ee.
D 16218 > de 71 à
12. 16,002 = 16,064 4 POO sake .
45. 16,020 % 16,154 3 16,077 : 16,070 (13)
29 10,299 à 10,142 : 16,202 s ) 6463
91. 16,120 3 16,119 4 16,119 7 \ ,109 (15)
Juli 2. 16,175 2 16,314 3 16,258 5) 16476 m
4. 16,094 5 1601 51)
Das Gesammtmittel ist + 59° 30° 4 6 155 (48), und mit Beriicksichtigung der Ge-

 

_wichte, aber unter Vernachlassigung einer etwaigen Polhohenvariation, ergeben sich folgende

Fehlerbetrage :
ae . . F. + 0,053.
Für ein Abendmittel aus 6 Paaren . | a IR ee 0.079.
Ta wi. 2 0,019.
Für das Resultat aus 48 Paaren . La us

Ebensowenig wie die einzelnen Polhöhenwerthe zeigen die Abendmittel irgend welche

Abhängigkeit von der Reihenfolge der Kreislagen, da hier ¢,. — 9, = — 0.018 ist.

Die Sternpaare, welche obigen Polhéhen zu Grunde liegen, weisen folgende optische

Helligkeiten und Differenzen von Meridianzenithdistanzen auf :

 

[Pot 63 63 1a pi 28 00 0
D 38, 43 ı 159 2 6) 70 à

a 3 5 1 3 01 66 + 96

a 03 56 91 a 30 060 15

5 55 68 4106 5 60 70 — 8.0

Im Mittel + 0112 Im Mittel — 004.

Die den obigen Resultaten zu Grunde liegenden Messungen auf den photographischen
Platten sind mit dem Mess- Apparat in seiner ursprünglichen Einrichtung erhalten worden, bei

 
