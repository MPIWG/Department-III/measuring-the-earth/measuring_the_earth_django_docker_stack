 

I
N
ll
"m

re

 

190 :

Die Azimute sind von Süden nach Westen gerechnet. Die Lage des Anfangspunktes
jedes Azimuts ist in den vorhergehenden Tabellen zu finden.

Kamies Sector Berg — Louis Fontein ist Maclear’s Linie reduecirt auf das System
der neuen Triangulirung, mit Maclear’s Astronomischer Messung.

In [ | eingeschlossene Resultate beziehen sich auf Stationen nahe Cape Town, wo,
wie es scheint, eine beträchtliche Störung der Richtung der Schwerkraft stattfindet, die
wahrscheinlich der Nachbarschaft des Tafelberges zuzuschreiben ist.

Anmerkung : Es folgen hier noch die wahrscheinlichen Fehler der Dreieckswinkel der Vermessung,
abgeleitet nach Ferrero’s Formel :
> 4
SN

 

Wahrscheinlicher Fehler — + 0.6745 1

wo & A? die Summe der Quadrate der Dreiecksschlussfehler und N die Anzahl der Dreiecke bedeutet.

N WE.
Verifikation der Natal-Basis (bei Nacht beobachtet), mit dem 18-zölligen Theodolit Dy tee 0,14
Verlängerung der Port-Elisabeth-Basis, mit dem A8-zölligen Theodolit . . . . 24 0,23
Hauptdreiecke, gemessen mit dem Repsold’schen 10-zôlligen Theodolit . . . . 434 0,38
Hauptdreiecke, gemessen mit dem 18-zülligen Troughton & Simms . . . . . 100 0,49
Maclear s Dreiecke welche mu. benalzt sind - . . .... 2.2.2.2. 68 0,62
Maclear’s Hauptdreiecke, welche mit benutzt sind . . . . 208 0,76

Das ganze System ist so korrigirt, dass die berechneten Bosialdn ch mit den gemessenen durch jede
Kette hindurch übereinstimmen und alle anderweiten Zusammenschlüsse passen.

Die Gesammtsumme der neueren Leistungen auf dem Gebiete der Lothabweichungen
ist nach dem Vorstehenden eine bedeutende, namentlich in Europa sind wichtige Beiträge
zu dem in dem Lothabweichungsbericht von 1887 verwertheten Material hinzugekommen.
In einigen Jahren wird es sich lohnen, das europäische System der Lothabweichungen zu
erneuern, wenn noch einige Berechnungen ausgeführt sein werden. Dazu gehört namentlich
die Ausnutzung der Wiener Meridiankette für die Kentniss von Lothabweichungen vom
Riesengebirge über Wien und Dalmatien bis Sicilien, ferner die Auswerthung der Lothab-
weichungen in Breite längs der europäischen Längengradmessung in 52° Breite und vielleicht
auch der Zusammenschluss der Systeme von Norwegen, Schweden und Dänemark.

Zu den merkwürdigsten und wichtigsten neueren Ergebnissen rechne ich die Fest-
stellung der Thatsache, dass längs des 52. Breitenkreises in Europa die Krümmung wesent-
lich stärker ist, als nach der bisher erlangten Kenntnis von der Erdgestalt zu erwarten war
(Verhandlungen zu Brüssel, S. 508). Mit um so grösserer Spannung muss man daher den
Ergebnissen der Berechnung der transkontinentalen Parallelkette in 39° Br. in Nordamerika
entgegensehen. Derartige Erscheinungen erhöhen aber auch die Wichtigkeit der Aufstellung
zusammenhängender Lothabweichungssysteme für ausgedehnte Flächengebiete, wie es mit
Hülfe der Laplace’schen Punkte möglich ist, im Gegensatz zu der Untersuchung einzelner
Meridian- und Parallelbögen ohne Rücksicht auf ihren Zusammenhang. (Vergl. u. a. die Ver-
handlungen in Paris, Annexe XI.)

 
