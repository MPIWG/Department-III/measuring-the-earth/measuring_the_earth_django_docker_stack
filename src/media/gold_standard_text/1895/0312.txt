 

 

Procès-verbanx des séances

Première séance, 27 septembre 1895

Liste des membres présents

Discussion sur les élections réglementai- :

res pour la Commission permanente, à
faire par la Conférence générale

A la demande du Président, M. Fœrster
fournit des renseignements sur la fixa-
tion et l’ordre du jour des premières
séances de la Conférence générale

Après discussion, on décide de s’occuper,
dans la prochaine séance, du renouvelle-
ment de la Convention, sur la base de
l'avant-projet de la Commission spé-
ciale :

Le rapport dits ae m. Hemört est
renvoyé à une Commission de deux
membres

Renseignements die He M. ser
sur les résultats obtenus par la méthode
photographique pour les mesures de la-
titude :

Deuxième séance, 23 septembre 1895

Le procès- verbal de la première séance est
adopté après quelques observations.

Discussion générale sur le renouvellement
de la Convention. On s'occupe d’abord
des articles concernant l’organisation .

M. Fœrster montre qu'un Comité interna-
tional de sept membres tient le juste
milieu entre les propositions extrêmes
des Pays-Bas et de la France

M. Bakhuyzen motive la proposition d'un
bureau administratif de mem-
bres. :

M. von Kalmar iat Whe hue Geile
proposition differe essentiellement de
celles de la Commission néerlandaise .

M. Hennequin envisage un Comité de
quatre membres comme insuffisant .

M. Fœrster estime qu’il faudrait aussi al-
tribuer à ce Comité des compétences

Pag.
223-226

223

223-224

221-225

225-226

226

226

227-231

227

221

228

228-229

228

228-229

306

 

de la Commission permanente.

scientifiques; de nombreuses Commis-
sions spéciales ne seraient pas prati-
ques à

M. Hirsch be ice de LL Gomis pape
dans l’avant-projet correspond aux au-
tres organisations internationales; une
Commission permanente de vingt-huit
membres ne pourrait être convoquée
chaque année .

M. Faye justifie la cn fancies
par l'intention d’attribuer à chaque État
le même droit d’être représenté.

M. Fœrster croit qu'une Commission aussi
nombreuse ne différerait pas de la Con-
férence générale; du reste les membres
de la Commission permanente y repré-
sentent la science et non leurs pays

M. Hirsch estime que la nouvelle propo-
sition française équivaut à l’ancienne
qui supprimait la Commission perma-
nente

MM. Foerster et arsch eu dard
pour remplacer le terme de Comité in-
ternational par l’ancienne désignation
de Commission permanente .

Le do alinéa de Part. 5 est à
par 4 voix contre 3

Troisième séance, {er octobre 1895

Suite de la discussion sur l’art. 5.

Le quatrième alinéa est adopté dans la
forme suivante: « Le Président et le
Vice-Président sont nommés pour trois
ans par la Conférence générale ».

M. Foerster fait, au sujet du droit de vote
des Etats (Art. 14), la déclaration offi-
cielle d’après laquelle l'Empire alle-
mand a l'intention d'entrer lui-même,
en lieu et place des États allemands,
dans l'Association géodésique inter-
nationale.

La Commission décide Ste ar a
de conserver la disposition actuelle qui

 

Pag.

229

230

230

230

232-238

233

233-234

4
3

sod hs ei

Peer

ach has eh bh ka ty

|
|
|
|

 
