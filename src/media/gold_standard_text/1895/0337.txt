ra

 

| Perera °° TTT

HH EL FAM uni All

ii]

ii
iR
=
i=

=

 

17

Die Resultate vom Juni 1893 ab sind mit den Ergebnissen auf den übrigen Beobachtungs-
stationen in keiner Weise in Einklang zu bringen. Es muss daher eine zur Zeit noch unbekannte
Ursache den Gang der Werthe auf dieser Station in erheblichem Grade beeinflusst haben. Zur Ab-
leitung der mittleren Polhöhe und zur Bestimmung der Coordinaten der Polbewegung sind aus diesem
Grunde nur die Resultate vor 1893.4 hinzugezogen worden.

Wien. A= 10 22.
Datum. Jahr. Polhöhe. Paare. ho ni - Differenz.

VON STERNECK und KRIFKA.

 

1892 Nov. 15 1892.87 49° 12 40.08 103 +0104 0717 —0"13
Dec. r6 96 40.13 III —+0.09 +0.15 —0.06
1893 Jan, 16 1893.04 40.11 103 —o.o7 —o.1o —0.03
Febr. 14 12 40.12 115 4.0.08 +0.03 +0.05
Marz 16 21 40.09 155 +0.05 —0.04 —+0.09
April 15 29 40.05 128 40.01 —0.I0 0.11
Mai 16 37 39.94 130 —0.IO —0.16 +0.06
Juni 15 46 39.90 104 —0.14 —0.19  —+o.os
Juli 16 54 39.87 TOR —0.17 —0.16 —0.01
Aug. 16 62 39.98 128 —0.06 —0.10 0.04
Sept. 15 71 40.06 141 0.02 —0.01I +0.03
Oct. 16 79 40.05 159 0.01 4.0.05 —0.04
Nov. 15 87 40.08 68 —+0.04 40.11 ‘ —0.07
Dee... 16 96 40.15 gt +o.11 40.12 —0.01
40.04 1639

Monatsmittel aus No. 3219 der Astronomischen Nachrichten. Als Datum ist auf Grund der Mit-
theilung des Herrn Oberst von Srernecx, dass die Mittelwerthe der Polhöhe sehr nahe den jedes-
maligen Monatsmitten entsprechen, genau der mittelste Tag jedes Monats angenommen worden.

Die definitiven Resultate sind auch publicirt in: Mittheilungen des Militär-Geographischen
Institutes, XIII. Band 1893, Wien 1894.

Prag. A = —14° 25/
: > Abweichung Reduction :
Datum. Jahr. Polhöhe. Paare. om Mill en Differenz.
WeEINER und Gruss.

1889 Marz 6 1889.18 Sop 5 1585 84 —0.04 ==
April 16 29 15.96 69 —+o.o7 — —
Mai v7 38 15.0% 202 40.04 — —
Juni 16 46 16.00 237 +o.11 — —
Jul 78 55 16.05 61 0.16 — —
Aug. 18 63 10.07 109 +o.1 — —
Sept. 15 73 15.98 86 0.09 — —
Oct vo 77 16.00 76 +o. “ —
Nov. 13 87 154719 126 —0.10 — —

3
