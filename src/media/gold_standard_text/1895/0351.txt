na il ne a hen nr
eng
AN TPE ENT NT"

babar
RENE TA

DT

ATP CTI

5
ze
sz
se

=
a8
ce
11
a
2

Beilage A. IV.

BERICHT

PRACISIONS-NIVELLEMENT IN EUROPA

fe e < > 1 d
mit einer Uebersichtskarte in 75 ORO GON

von

Al von ALMA Ry

(Mit 4 Karte. — Siehe No. V.)

Hochverehrte Versammlung !

Die Ausführung von Präeisions-Nivellements hat auch in den letzten Jahren anhal-
tende Fortschritte gemacht.

Die folgende Tabelle giebt für die einzelnen Staaten :

die Jahre, während welcher nivellirt wurde ;

die Länge der bis inel. 1895 nivellirten Linien in Kilometern, dann
die Länge der einfach, doppelt und mehrfach nivellirten Strecken ;

die Anzahl der Höhenmarken 1. und Il. Ordnung, dann die Summe derselben und
ihre mittlere Entfernung von einander;

auch gesondert

endlich die wahrscheinlichen Kilometerfehler, aus Doppel-Nivellements und aus
Polygon-Schlüssen gerechnet.

 
