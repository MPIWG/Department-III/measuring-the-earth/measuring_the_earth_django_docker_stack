 

ne
He

Ht
th

 

274

internationalen Erdmessung unter der Oberleitung des Präsidenten und in Gemeinschaft mit
dem Director des Centralbureaus ist Aufgabe des ständigen Secretars.

Art. 9. — Der Präsident, der Vicepräsident und der ständige Secretär werden für
die Dauer der Convention von der Generalconferenz gewählt.

Eintretende Vacanzen werden in der nächstfolgenden General-Versammlung ausgefüllt.

Art. 6. — Für die internationale Erdmessung wird eine fortlaufende Dotation aus-
gesetzt, welche durch die Beiträge der sämmtlichen betheiligten Staaten aufgebracht wird.
(Art. 9.)

Diese Dotation soll hauptsachlich in foleender Weise verwendet werden:

ae

. für die Publications- und Verwaltungskosten ;
. für die Besoldung des ständigen Secretärs ;

3. für die Unterstützung oder Remunerirung einschlägiger theoretischer, rechneri-
scher oder experimenteller Arbeiten, welche durch einen besonderen Beschluss der General-
conferenz für wünschenswerth oder nützlich erklärt werden;

A. fiir die Ausführung internationaler wissenschaftlicher Unternehmungen solcher
Art, welche für die Erleichterung oder Sicherung der Erdmessungsarbeiten aller einzelnen
Lander von allgemeinem Interesse sind.

Die Vertheilung der Dotation auf die vorstehend angegebenen Verwendungsgruppen
wird dem Präsidium unter der Gontrolle der Generalconferenz übertragen.

) DD

Art. 7. — Der Jahresbetrag der Dotation wird durch die in Art. 9 festgeseizten
Beiträge der zur Zeit betheiligten Staaten normirt; sie soll für eine Dauer von zehn Jahren
nicht weniger als 16000 Mark (20000 Franes) jährlich betragen.

Sollte die Generalconferenz wünschen, dass Untersuchungen oder Beobachtungen
ausgeführt werden, wolür die verfügbaren Geldmittel nicht ausreichen, so hat sie zur Vor-
berathung eine Specialcommission nach den Bestimmungen des $ 12 einzusetzen, welche ihr
ausser der wissenschaftlichen Seite der Frage auch über die finanzielle eingehend Bericht zu
erstalten hal.

Der Beschluss der Generalconferenz wird hierauf den betheiligten Regierungen zur
Annahme und Bewilligung der Geldmittel vorgeleot.

Die Nachweisung über die Verwendung der Dotation wird in den Verhandlungen der
Generalconferenz publicirt.

Die in einem Jahre nicht verbrauchten Einnahmen dürfen zu den Ausgaben der
folgenden Jahre mitverwendet werden.

Art. 8. — Die Beiträge der betheiligten Staaten werden alljährlich im Beginne des
Jahres geleistet.

Die Einzahlungen der Beiträge erfolgen durch die Vertreter der betheiligten Staaten
zu Berlin an die Legalionskasse daselbst.

In Uebereinstimmung mil letzterer Festsetzung haben alle Communicationen des
Präsidiums mit den betheilisten Regierungen durch die Vermittlung ihrer diplomatischen
Vertreter in Berlin zu erfolgen.

 

4

ide

Hh iby dodo

SR ha hd Aa

Jab bsoasstauhuc

sat bib a Hd ea Asti se

|
|
|
