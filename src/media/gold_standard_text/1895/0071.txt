 

a A A A 0 LL
A MT TN |

bab asec à
TPFRPOT TEIL EU

ND

ALCATEL

a

SECHSTE SITZUNG

Donnerstag, 10. Oktober 1895.

Präsident : Herr Fwrster.
Es sind gegenwärtig :

Die Herren Mitglieder der permanenten Commission : Fuye, Forster, Helmert, Hen-
nequin, Hirsch, von Kalmar, van de Sande-Bakhuyzen, von Zachariae.

Die Herren Delegirten : Albrecht, d’Arrillaga, Bassot, Bouquet de la Grye, Celoria,
Cobo de Guzman, Fergola, Ce Guarducei, Haid, Koch, Lallemand, Lew, Lo-
renzont, Miyaoka, Nell, Omori, von Orff, Rajna, Rosen, von oi Schols, Seeliger von
Sterneck, Tisserand, Tiltmann, Weiss, Westphal.

Die Herren Eingeladenen : Heck 7, Marcuse, Matthius, Mertens, Schumann, Schwarz.
Die Sitzung wird um 2 Uhr 15 Minuten eröffnet.

Auf der Tagesordnung steht die Fortsetzung der Berathung der beiden Entwürfe be-
treffend die Erneuerung der Convention; in den Exemplaren des Gegenentwurfes sind durch
ein Versehen der Druckerei die Nicilerlande nicht aufgeführt.

Der Herr Präsident schlägt vor, zunächst die allgemeine Discussion der Art. 6—
einschliesslich vorzunehmen, nach Beendigung derselben zur Abstimmung zu schreiten, und
zwar zuerst über Art. 2, über welchen in der 4. Sitzung bereits eine allgemeine Discussion
stattgefunden hat, und sodann über Art. 6—9.

Herr d’Arrillaga ergreift das Wort, um sich über seine Stellung zu den die Dotation
und das Budget der internationalen en Association betreffenden Artikeln auszu-
sprechen, nachdem er über den anderen Punkt, nämlich über die allgemeine Organisation,
soweit sie in den ersten Artikeln des Entwurfs der permanenten Commission entwickelt ist,
neulich bereits die Anschauungen der spanischen Delegirten dargelegt hat. Er führt aus:

ASSOCIATION GEODESIQUE — 9

 
