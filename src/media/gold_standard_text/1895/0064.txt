 

i
ay
:

re

ee ee a

EEE

ee

pre

FÜNFTE SITZUNG

Mittwoch, 9. October 1895.

oy

Der Prasident Herr Æwrster erôffnet die Sitzung um 3 Uhr 15 Minuten.
Es sind anwesend :

Die Herren Mitglieder der permanenten Commission : faye, Forster, Helmert, Henne-
quin, Hirsch, von Kalmar, van de Sande Bakhuyzen, von Zachariae.

Die Herren Delegirten : Albrecht, d’Arrellaga, Bassot, Bouquet de la Grye, Celoria,
Cobo de Guzman, Fergola, Geelmuyden, Guarducei, Haid, Karlinski, Koch, Lallemand,
Löw, Lorenzoni, Miyaoka, Nell, Oberhoffer, Omori, von Orff, Rajna, Rosen, von Schmidt,
Schols, Seeliger, von Sterneck, Tisserand, Tittmann, Westphal, Weiss.

Die Herren Eingeladenen : Börsch, Erfurth, Galle, Haasemann, Hecker, Hegemann,
Kühnen, Matthias, Marcuse, Mertens, Dr. Meyer, Schnauder, Schumann, Seibl, Stadthagen,
Thiesen, Vogler, Weingarten, Weinstein.

Der ständige Secretär Herr Hirsch verliest das Protokoll der vorigen Sitzung.

Zu einer Reklamation des Herrn Bakhuyzen, welcher der Herr Secretär alsbald
Rechnung trägt, fügt letzterer die allgemeine Bemerkung hinzu, dass es im Interesse der
Kürze des Protokolls nicht nur gestattet, sondern sogar geboten sei, mehrfache Ausführungen
desselben Redners mit einander zu verbinden. Das Protokoll wird alsdann von der Versamm-
lung genehmigt.

Da es nicht möglich gewesen ist, die Vorschläge der niederländischen, französischen
und österreichischen Delegirten, an deren Abfassung auch noch andere Vertreter betheiligt
sind, rechtzeitig im Druck fertig zu stellen, so schlägt der Herr Präsident vor, zunächst einen
Bericht des Herrn Helmert über die Messungen der Schwerkraft anzuhören. Es empfehle sich
das umsomehr, als einige Gäste der Versammlung beiwohnen, für welche dieser Gegenstand
ein besonderes Interesse biete, und welche unter Umständen geneigt sein könnten, auch
ihrerseits Mittheilungen zu dem Gegenstande zu machen.

 

bhiliee

ah

 

ib u telle Js J ub dos

A mama en aa ad ible à

 
