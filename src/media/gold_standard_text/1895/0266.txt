 

260

projet déterminé, d'organiser et de diriger un travail, assurément vaste et important, mais
n'occupant qu’un certain nombre d’États et dont on pouvait prévoir la fin, le programme
actuel embrasse le monde entier et l’Association internationale pour la mesure de la Terre
ne connait dans l’étendue et la durée de son œuvre d'autre limite que celle qui s’imposerait
naturellement, lorsque l'intérêt des Gouvernements viendrair à se relâcher, ou qu'il se mani-
festerait une disproporuon évidente entre les moyens réclamés et les résultats acquis.

Lors des renouvellements de ses règlements, l'Association a nécessairement dù tenir
compte des conditions modifiées de sa tâche. Les dispositions qui tendaient à confier la
haute direction scientifique à une commission composée de savants, reconnus éminemment
compétents, à imposer l’uniformité dans les méthodes et le choix des instruments, purent
avoir leur utilité à une époque où l'importance du plus haut degré de précision n’était pas
généralement appréciée, et pour une entreprise relativement restreinte ; il n’en est plus
ainsi dans l’état actuel de la géodésie et pour le programme presque illimité de la présente
Association.

Le développement qu’ont pris, depuis trente ans, les travaux géodésiques dans pres-
que tous les pays civilisés, — la diffusion de la science géodésique, accrue considérablement
par l’échange international et spécialement par la distribution des publications parmi tous
les membres de l’Association, — les relations personnelles nées entre les géodésiens de diffe-
rentes nationalités, l’émulation créée entre les divers pays pour contribuer à l’œuvre com-
mune des travaux de la plus haute valeur, ont rendu superflue, et par cela même nuisible,
la centralisation de la direction scientifique, et en même temps causé des difficultés plus
grandes dans le choix des membres d’un Conseil directeur.

D'un autre côté, la participation de plus en plus générale des Etats, dont les Gou-
vernements peuvent avoir des vues ou des dispositions différentes, ont rendu à peu près
illusoire une direction proprement dite, qui n’aurait pas, par un vote formel, reçu l’assenti-
ment de la Gonference generale.

L’Association a constamment rempli la premiere condition de sa durabilite en se
conformant, sinon formellement au moins pratiquement, aux conditions modifiées de son
existence. De fait, ses réunions ont de plus en plus pris le caractère de Congrès scientifiques
périodiques, ayant toutefois le précieux avantage de la continuité et des résultats pratiques
de son œuvre.

Aussi, lorsqu'on compare le texte du Règlement de la Commission permanente, arrêté
en 1889, à celui de 1864, on remarque un progrès significalif dans le sens d’une décentrali-
sation de la direction scientifique.

D’après le Reglement de 1889, la Commission permanente ne juge plus, comme le
veut le Règlement de 1864, «les travaux qui lui sont transmis par le Bureau central, suivant
leur utilit& pour la mesure des degrés», et si elle se met encore, «soit directement par le
Secrétaire perpétuel, soit par l'entremise du Bureau central, en communication avec les délé-
gués des divers États, afin d'amener la plus grande uniformite dans la forme et l'étendue
des publications que ces États feront sur les travaux géodésiques se rapportant à l’entreprise
internationale », c’est sous la réserve expresse que l’on tiendra compte «des circonstances

 

4

des

dd

ER aM Aa PT

he

À sata ph a be ha ne

i
|
|

 
