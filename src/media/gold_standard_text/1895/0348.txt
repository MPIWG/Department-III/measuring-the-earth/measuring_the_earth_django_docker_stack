 

 

ee

ee

Fe

 

 

28

provinces d’Asie par la mesure d’une base qui sera effectuée avec l’appareil bimétallique
français et avec le concours d'officiers français. Un iradié récent prescrit la constitution d’un
service géographique ottoman et l’entreprise immédiate des opérations précitées.

II. — Dans mon rapport de 1892, j'avais signalé la situation acquise à cette date
par l’étalonnage des appareils de base employés par les différents États associés.

J'avais indiqué qu'il ne restait à exécuter que la comparaison des étalons de la
Russie, de ceux de l’Autriche et de celui de la Belgique ; j'avais montré, en outre, la nécessité
de procéder à un nouvel étalonnage de l'appareil bimétallique d'Espagne.

La Conférence apprendra avec satisfaction que presque toutes ces comparaisons ont
été effectuées depuis 1892 à l’établissement international des poids et mesures de Breteuil.

L’étalon russe N a été comparé en 1893. C’est une double toise en fer à bouts, ap-
partenant à l’observatoire de Poulkovo, qui fut construite en 1827 à Jurief (Dorpat) et éta-
lonnée en 1828 d’après la toise du Pérou, par l’intermédiaire d’une toise de Fortin envoyée
de Paris en 1821, avec un certificat d’Arago.

La valeur adoptée jusqu'ici pour cette rêgle était :

1728,01249 lignes à 16,95
soit 3 898 101 ?

en partant de la valeur anciennement admise pour la toise du Pérou (1 toise — 1, 9490366
214025)
et 3 898 208"

en prenant l’équation rectifiée de cette toise, trouvée par M. Benoit (1 toise — 17949 090
à 16,25).
Les comparaisons de l’étalon N, faites à Breteuil, donnent :

3 897 760 P

 

Nour 0 —
et en adoptant le coefficient de dilatation donné par F.-G.-W. Struve, 107° x 11,394
Na 29 = 3898162.

Ces résultats sont extraits du certificat délivré par M. Benoit, Directeur du Bureau
international des Poids et Mesures.

L'appareil de l'Institut géographique militaire de Vienne a été comparé à Bretemil
en 1893 et 1894. Les quatre régles ont les équations suivantes :

Étalon I = 3.901501 (1 - 0,000 014 407 t + 0,000 000 005 89 +?)
» I — 3,901 400 (1 + 0,000 011 367€ + 0,000 000 00492 t?)
» 1 — 3.901521 (1 + 0,000 011 287€ + 0,000 000 008 08 #2)
» W = 3,901 449 (1 + 0,000 011 448 t + 0,000 000 008 12 t)

 

hd ns.
