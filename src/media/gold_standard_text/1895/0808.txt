 

 

    

age

VIII. — Italie.

 

 

r

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

INDICATION | |
N° DES POINTS LATITUDE LONGITUDE| EPOQUE DIRECTEURS | OBSERVATEURS INSTRUMENTS | | REM MARQUES
281 | Rioburent........ ı 44937: 09"| 240.36! 46" 1879 Col. Ferrero. Ing. Jadanza. -| Starke di 27 cm.
280 | M. @Agn0 7... At Az 10 | 26 00 57 1877 id. |» . Dereht. id.
288 | Brie Torniola.... 44 49 20 | 25 38 20 1877 id. | id. id. I
28% | VIEone.. :.......- | 44 50 36 | 25 09 82 1878 id. | id. id. |
290 | Gran Cournour... | 44 50 59 | 24 45 20 1879 id. id. id. |
291) Bortona.......... 44 53 35 | 26 32 04 1877 id. Ing. Pucci. id. |
295 | Chaberton........: 44 57 52 | 24 24 51 1879 id. » Cloza. id.
299 | Superga.......... 45 04 49 | 25 95 51 | 1878 id. | » Derchi. | id.
29h 101100....2...0. 45 04 15 | 25 20 56 1885 . | Col. De u » De Berar- | id. |
dinis. |
BOOS OTC (i) ic eres . | 45 05 42 | 25 56 10 1878 > Ferrero. » Jadanza. id.
301 | M, Musine........ 45 06 49 | 25 07. 05 1879 id. | id. id. |
902 | DE. Tabor. 45 06 49 | 24 13-37 1879 id. | Ing. Cloza. id. |
sera... 45 11 04 | 26 48 59 | 1879 id. » Jadanza. id. |
305 | Roceiamelone..... | 45 12 11 | 24 44 25 1879 id. » Cloza. id. |
306 | Vigevano ......:.. 45 19 00 | 26 31 14 1878 id. >, Dereli. | id. |
309 Me Vesco 3. 45:19 26/29 80 10 1878 id. » Jadanza. | id. |
310 M.sSoslio ..... .. 45 22 17/25 11 38 1879 id. » Guarducei. | id. |
312 | Levanna oceident.° | 45 24 49 | 24 49 31 1880 id. id. | id.
814 | NOVATA. 45 26 54 | 26 16 58 1879 id. id. | id.
: : \Ing. Jadanza. :
2 29 ee D
315 | Biandrate ........ 45 27 18 | 26 07 32 | 1879-81 id. i Gap. Dia id.
316 | Milano (Duomo) .. | 45 27 50 | 26 51 19 1879 id. | Ing. Guarducci. | id.
316b| Milano (Brera) ... | 45 28 15 | 26 51 10 1879 id. id. | id.
320 | Base di Somma... | 45 34 22 | 26 23 16 1879 id. Ing. Pucei. ' Brunner di 42 cm.
. Estw S.-E. |
322 | M. Colma......... 45 35 08 | 25 33 23 | 1879-81 id. | 2 NS (Serra cm |
\ >» Puech IP Pistor di 28 cm. |
324 | Busto Arsizio..... | 45 36 42 | 26 30 58 |1879-80-81 id. ) » Guarducci. Starke a a em.
Cap. Bona.
325 | Base di Somma... | 45 37 03 | 26 23 08 || 1879 id. | Ing. Pucci. | Brunner en |
Punto medio | |
\ > D Atri. id.
328 | Marano Ticino.... | 45 37 53 | 26 17 43 1879 id. Cap. Bona. id.
ling. Guarducci. \ Starke di 270m.
329 | Base di Somma... | 45 39 45 | 26 23 00 1879 id. >  Bucer. | Brunner di 42 cm. |
Hst.~° N.-0. i | |
( » Cloza. |
335 | M. Briasco ....... 45 AT 34 | 25 58 49 1879-80-81 id. » Derchi. {Starke di 27 Cm, |
| » Domeniconi |
339 | M. Palanzuolo.... | 45 51 42 | 26 51 54 1881 id. » Ginevri. id. |
340 | Campo dei Fiori.. | 45 52 07 | 26 25 33 | 1879-81 id. | » Domedieent| id. |
Vil.
REGION A L’OUEST DE LA MERIDIENNE DE MILAN.
Réseau a sud du paralléle moyen.
250 | M. Grammondo... | 43°50’ 30") 25°10’ 23") 1878 | Col. Ferrero. | Ing. Derchi. |Starke di 27 cm. |
251 | M. Bignone ...... 43 52.28 202349 1878 id. | » Jadanza. id. |
258.1 M. Torre......... | 43 58 44 | 25 40 49 1880 id. | id. id. I
255 | M. Tournairet.... | 44 00 56 | 24 53 49 1878 id. | Ing. Derchi. id. |
250 | M. Yacche........ | 44 01 04 | 25 19 12 1878 id. 12» :Cloza, id. I
259 | M. Clapier ....... 44 06 52 | 25 04 58 1878 id. | »  Derchi. id. |
262 | M. Monnier ...... 44 09 14 | 24 38 06 1878 id. | » Jadanza. id.
263 | Mongioie......... 44 10 25 | 25 26 55 1877 id. id. id.
264 | Capo Noli........ 44 11 35 | 26 04 56 1877 id. Ing. D'Atri. Brunner di 42 em. |
265  M, Matto....°.... 44 13 33 | 24 55 10 1878 id. » Cloza. Starke di 27 cm. |
266 | M. Settepani ..... 44 14 43 | 25 51/89 1877 10; » Jadanza. id. |
267 | M. Bezimauda.... | 41 15 00 | 25 15 48 | 1878 id. id. id. |

ne bh he äh anna u

lib
