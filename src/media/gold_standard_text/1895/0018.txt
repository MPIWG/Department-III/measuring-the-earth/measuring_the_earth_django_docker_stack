 

 

42
maassen und Basismessstangen verdankt, eine Anregung gewährt und eine Subvention in
Aussicht zu stellen vermocht für neue Untersuchungen über die zu Messstangen geeignelsten
Metalle und Metall-Legierungen, und wir sind bereits in der Lage, Ihnen ein recht erhebliches
Ergebniss dieser Untersuchungen vorlegen zu können, nämlich den Nachweis, dass voraus-
sichtlich reines Nickel ein treffliches Material hierfür abgeben wird, in zweifellosem Vorzug vor
den Eisenlegierungen, die bisher so allgemein angewandt wurden. Experimentelle Unter-
suchungen über Nivellierlatten sind in Gang gesetzt, ebenso Untersuchungen zur Vorbereitung
von differentiellen Schweremessungen, insbesondere von differentiellen Schweremessungen
auf hoher See.

« Die Vorbereitungen zur Einrichtung einer Vergleichungs-Station für Pendelmes-
sungs-Apparate im internationalen Maass- und Gewichts-Bureau sind, ebenfalls mit entspre-
chender Förderung durch unsere Hilfsmittel, nahezu beendiet.

« In ähnlicher Weise, wie sich hier eine gegenseilige Förderung internationaler
Institutionen ergeben hat, dürfen wir auch von der Organisation der rein astronomischen
Arbeiten immer reichere Früchte erwarten.

« Das, was die internationale astronomische Gesellschaft bereits für uns alle geleistet
hat, gehört einem glänzenden Blatte der Geschichte der Wissenschaft an. Die Organisation
der photographischen Aufnahmen des Himmels-Inventars, welche wir Frankreich verdanken,
wird einst auch für die Erdmessungsarbeiten ihre Früchte tragen. Neuerdings geht von Paris
die Anregung zu einem internationalen Zusammenwirken der nationalen Mittelpunkte der
astronomischen Vorausberechnungen aus. Hiervon und von den gemeinsamen Arbeiten, die
sich nothwendig daran knüpfen werden, darf die Erdmessung eine sehr wesentliche Vervoll-
kommnung der astronomischen Grundlagen ihrer Arbeiten erwarten.

«Alle diese Rückblicke und alle diese Ausblicke in die Zukunft legen aber die Frage
nahe : Ist es nicht bloss eine Mode des Tages, dieses immer umfassendere, internationale
Organisieren-Wollen der wissenschaftlichen Arbeit? Leidet nicht schliesslich dabei die indi-
viduelle Initiative der Persönlichkeit, welche doch die Quelle alles Originalen, alles wahrhaft
Schôpferischen ist ?

« Erleidet nieht auch der vernünftige nationale Ehrgeiz dabei eine gewisse Dämpfung,
welcher in dem Wettbewerb der Völker auch ein Kulturelement bildet? Wird nicht allzuviel
Zeit und Arbeit dabei auf das blosse Organisieren und Verwalten verbraucht, während man
den natürlichen Ausgleichungen der freien Entwickelung das Meiste davon überlassen könnte?
Und besteht nicht endlich die Gefahr, dass auch in allen grösseren Organisalionen wissen-
schaftlicher Art die Mehrheiten unter der Leitung der bloss rednerischen und administrativen
Talente die Enscheidungen übernehmen und die tieferen Antriebe und Mahnungen der bedeu-
tendsten Geister noch mehr, als sonst schon geschieht, überhört werden ?

« Es würden viele Stunden der Rede dazu gehören, alle diese Zweifel und Bedenken
auf Grund der bereits vorliegenden Erfahrungen über die Wirksamkeit der bereits vorhan-
denen internationalen wissenschaftlichen Institutionen und auf Grund sorgsamsien Nach-
denkens über diese Dinge zu beantworten. Jedenfalls ist es rathsam, dass Alle, die dazu
berufen sind, bei der Leitung und bei der Erweiterung jener Organisationen mitzuwirken,

|
3
3
3
4
3
3
3
a
=
3
3
3
a
q

Las ts Laub

 

 
