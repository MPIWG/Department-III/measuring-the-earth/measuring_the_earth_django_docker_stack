 

|
i

Munn An ana

FPT

TN TT

|
|

 

 

IV. Intensité de la pesanteur en differents points du Japon.

Tokio.

Longitude... . . 439 4, 5 8

Latitude. :. 2. 35 DD 0 NN
Hauteur au-dessus du niveau de la mer: 5 métres.
G — 979,84 c. s. — Obs. T.-C. Mendenhall 1880.

Sapporo.
Longitude... >, . 144 007 7 pp
latitude... . 283 > 0 à
Hauteur au-dessus du niveau de la mer : Q1 metres.
G = 980,510 e. s. — Obs. Tanakadate, Fujisawa et Tanaka 1881.

Naha (Okinawa).

Longitude

Latitude
Hauteur au-dessus du niveau de la mer : ca. 6 métres.
G = 979,165 c. s. — Obs. Sakai et Tanakadate 1882.

Kagosima.

Lonswde. ... Se 332)
Latitude

Hauteur au-dessus du niveau de la mer : 66.

G. = 979,561 c. s. — Obs. Sakai et Tanakadate 1882.

Ogasawara sima.

longitude... .  : 144 D 22 a
Éatitude .  .  07 2 D 0

Hauteur au-dessus du niveau de la mer : 29,
G = 979,472 c. s. — Obs. Sawai et Tanakadate 1884.
