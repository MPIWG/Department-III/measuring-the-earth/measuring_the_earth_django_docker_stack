 

 

234

L’Empire allemand u Vintention @entrer lui-même, en lieu et place des États alle-
mands, dans l’ Association geodesique internationale, à partir de l’époque où la Convention
actuelle prendra fin. Dans ces circonstances, le Gouvernement prussien suppose que la Confé-
rence générale n'hésitera pas à rayer celle question de son ordre du jour.

M. le Président reconnaît avec une grande satisfaction que c'est la solution la plus
simple et la plus satisfaisante pour faire disparaître les difficultés qui ont préoccupé quel-
ques pays.

M. le Secrétaire demande et obtient une interruption de séance de quelques minutes,
afin de traduire en langue française le texte de la déclaration officielle que M. Foerster
vient de lire en allemand.

Après avoir soumis la traduction (transcrite ci-dessus) à M. Fœrster qui la trouve
conforme à l'original, le Secrétaire en fait lecture à l’Assemblée.

La Commission permanente est unanime pour remplacer Particle 11 de Pavant-projet
par l’article 11 de la Convention actuelle et pour conserver la disposition qui attribue une
voix à chaque Etat de l'Association géodésique internationale.

La Commission passe ensuite à la dotation et à l’organisation financière de l’Associa-
tion, qui font l’objet des articles 6, 7, 8 de l’avant-projet, dont le Secrétaire donne lecture.

M. le Président ouvre la discussion sur l’article 6.

M. Bakhuyzen fait remarquer que le paragraphe 4 de cet article n’est pas approuvé
par la Commission néerlandaise. Gelle-ci est d’avis que lAssociation devrait se borner dans
Vavenir aussi bien que dans le passé à couvrir les frais des calculs nécessaires pour combiner
les mesures exécutées dans les différents pays, ainsi que les frais des publications, de l’ad-
ministration, etc. Pour cela, la dotation de 20 000 fr. s’est montrée suffisante; mais PAs-
sociation devrait s’abstenir d'entreprendre à frais communs des travaux internationaux qui
auraient pour effet de diminuer l'initiative et l'indépendance des commissions, des obser-
vatoires et des savants dans les différents pays, et que l’Association ne serait pas en mesure
de diriger et de contrôler efficacement.

M. Fœrster constate que M. Bakhuyzen est d’accord que l'Association poursuive son
activité comme jusqu’à présent; or, c’est précisément ce que l’article 6 a en vue, en désignant
comme objet de l’activité de l'Association les travaux qu’elle a exécutés depuis son renouvel-
lement en 1886 avec de beaux succès. M. Fœrster rappelle particulièrement l'expédition de
Honolulu. L'expérience du passé a montré que l’Association est parfaitement capable d’orga-
niser et de diriger utilement des entreprises internationales. Ces dernières n’empêchent du
reste nullement l’activité personnelle ou nationale. Même dans le projet de l’organisation
internationale du Service des latitudes par quatre Observatoires situés sous le même parallèle,

 

Dave ee hide Aal

Habe

css bes idl hend nu

1
|

 
