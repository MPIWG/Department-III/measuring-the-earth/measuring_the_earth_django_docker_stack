 

 

Ainese D UD

BELGIQUE

Des considérations relatives à des réductions de personnel et de budget ont amené
l’Institut cartographique militaire à différer certains travaux d’un intérêt plus particulié-
rement scientifique.

En ce qui concerne la copie n° 11 de la Toise de Bessel, l’étalonnage de cette copie
vient d’être demandé au Bureau international des Poids et Mesures, à Breteuil.

Quant à des travaux de triangulation, ceux qui ont été effectués, en 1895, par la
section géodésique de l’Institut, n’ont qu'un but cartographique et sont d'intérêt local. La
vérification de l’azimut de départ de notre triangulation ainsi que les déterminations de diffé-
rences de longitude dont il a été question dans mon rapport de 1892, 4 Bruxelles, ont été
remises à une époque actuellement indéterminée.

Le répertoire graphique de notre nivellement de précision est sous presse ; il paraîtra
au commencement de l’année prochaine.

Au mois de juin dernier, un double cheminement a été exécuté pour vérifier la sta-
bilité de l’estacade Est du port d’Ostende, à l'extrémité de laquelle sont établis le maréographe
de l Administration des Ponts et chaussées et notre médimarémétre. Par suite d’un tassement,
cette estacade, reconstruite en 1889, s’est affaissée de 0016 depuis 1891, époque à laquelle
un cheminement analogue avait été fait pour installer le médimarémetre.

Le maréographe d’Ostende a fonctionné — non sans interruptions — jusqu’au mois
de décembre 1894; mais, dans ce mois, il est survenu des tempétes extraordinaires sur la
mer du Nord, et les eaux ont balayé la cabane abritant le maréographe et la partie supé-
rieure du médimarémètre. Le premier de ces instruments a été détruit, et le second, dété-
riore.

Le placement d’un nouveau maréographe du système Van Rysselberghe et la réinstal-
lation du médimarémétre ont eu lieu à la fin de juillet dernier. Les deux appareils fonction-
nent régulièrement depuis lors, et des dispositions ont été prises pour les mettre à l’abri de
nouveaux accidents.

Le niveau moyen de la mer adopté provisoirement et déduit des sept premières

4

années d'observation au maréographe est à 3658 au-dessus du buse de l'échelle du Bassin

{
|
|
j

a aaa

 
