   

 

| 6

| 1892.7 (+16) (+16) à

i 8 +17.9 + 5.2 | Kasan, Pulkowa, Berlin, Strassburg, À
9 +15.8 no) Bethlehem. 2: 1-5

if 93.0 10,4 == 10.7 g 1

| i i ae ae Kasan, Pulkowa, Wien, Karlsruhe,

| 3 +. m 8 Strassburg, Bethlehem 3.3 25

i 4 te LO 7, = 4.3 ) !

i BD os dr.

! a el Kasan, Wien, Karlsruhe, Strassburg, i

ee Bethlehem 3:0 14 |

| NO cg (Par

| 6 rad,

i 940 +12.6 + 4.9

I + 8.4 + 1.6

i Ree yo Bd en

i] 3 11. 1:

| in

‘| ee ihe dam, Karlsruhe, Strassbur

il = ER asan, Potsdam, Karlsruhe, Strassburg

I) : ae ae Bethlehem. 3.1 4

| 8 — 9.1 — 10

| oo

i) 95.0 — 3.3 ate 07

| ee

| 2 — 9.0 + 8.9

Wie aus den Zahlenwerthen in den beiden letzten Columnen der obigen
Tabelle ersichtlich ist, stellen sich die Gewichte p, durchgängig niedriger als die
Gewichte 9. Dies ist dadurch veranlasst, dass gegenüber der regen Betheiligung
der europäischen Sternwarten an den einschlägigen Untersuchungen eine weniger
umfassende Antheilnahme seitens der amerikanischen Sternwarten und eine gänzliche
Ausschliessung der asiatischen und australischen Beobachtungsstationen zu constatiren
ist. Um so erfreulicher ist es aber bei dieser Sachlage, dass sowohl das Observatorium
des Columbia College in New York, als auch Prof. Doouırrue (früher in Bethlehem,
gegenwärtig in Philadelphia) ihre Bereitwilligkeit ausgesprochen haben, auch ferner
an diesen gemeinsamen Untersuchungen mitzuwirken, da es dem Centralbureau erst
dadurch ermöglicht wird, eine hinreichend sichere Bestimmung der beiden Coordinaten
der Polbewegung vorzunehmen. Gegenwärtig ist für die letzten anderthalb Jahre der
Genauigkeitsgrad in der Bestimmung der y-Coordinate aus dem Grunde ein
i geringerer als bei einer späteren definitiven Bearbeitung, weil die Resultate der New
| Yorker Beobachtungsreihe noch nicht in dem Umfange ermittelt worden sind, als dass
| sie bei der Ableitung der Coordinaten mit hinzugezogen werden konnten. Sobald
| diese Resultate vorliegen werden, stellen sich die Gewichte p, vom Beginn der New
| Yorker Beobachtungsreihe, also von 1893.5 ab, auf durchschnittlich 2.5 und ent-

Vu a ba lh ae ee

ertryeseesslllh Lilet

| sprechen daher dann annähernd den Gewichten p..

| Infolge dieser Verschiedenheit der Gewichte stellen sich auch die mittleren
Fehler der durch die obigen Coordinaten definirten Curvénpunkte recht verschieden
heraus. Sie betragen fiir die einzelnen Combinationen:

 
