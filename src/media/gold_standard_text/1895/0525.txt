 

DE ee

nn A4 wie VENTURE
LAN

CP EM Gib] AU

| Vill i 1H

83.

té

in
iB
5
5
a

=

RB
=
2e

205

NO ees

sur le rôle des erreurs systématiques dans les Nivellements de précision,

Par M. CH. LALLEMAND.

Pour caractériser la précision d’un réseau de nivellement, on se contente habituel-
lement de donner son erreur accidentelle moyenne kilométrique, déduite, soit de la compa-
raison des multiples opérations exécutées sur chaque section, soit des écarts de fermeture des
polygones du réseau.

Or, le plus souvent, aux erreurs accidentelles des nivellements s'ajoutent des erreurs
systématiques, dont l'influence, inappréciable sur de petits parcours, reste aisément inapercue,
bien qu’elle dépasse généralement de beaucoup celle des erreurs accidentelles pour des
lignes de grande longueur, comme, par exemple, celles qui relient entre elles les diverses
mers de l’Europe.

[ y a quelques années déjà !, nous avons signalé la nécessité de rechercher les erreurs
systématiques dans les nivellements de précision, et nous avons indiqué une méthode gra-
phique très simple, permettant de les mettre en évidence et d’en mesurer la grandeur.

Nous avons entrepris cette recherche pour le réseau fondamental du Nivellement
général de la France, et, à titre de comparaison, pour quelques-uns des principaux réseaux
européens de nivellements de précision.

La présente note a pour objet de faire connaître le résultat de ce travail.

Mais, auparavant, pour fixer les idées, nous rappellerons en quelques mots les
conditions d'exécution du Réseau fondamental français; puis nous indiquerons les moyens
employés pour déterminer successivement :

1° Le coefficient d'erreur systématique des nivellements doubles, d’après la compa
raison des résultats des deux opérations :

2° La valeur correcte de l’erreur accidentelle proprement dite;

3° Enfin, la valeur moyenne a posteriori du coefficient d’erreur systématique d’un
réseau de nivellement, d’après les écarts de fermeture des polygones.

! Nivellement de haute précision, nes 87 à 90, Paris, 4889, Baudry éditeur.

 
