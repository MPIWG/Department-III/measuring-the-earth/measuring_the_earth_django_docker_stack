(i

Een See TEST

WL mia

iit

TPE

2
IE
im
14 4
up
43
ee
dE
a
ee

 

Annexe B. XVI.

UNITED STATES

Report on the present state of geodetic operations in the United States.

Before a delegate had been appointed on behalf of the United States to attend the
eleventh Conference of this Association, certain data had been prepared by the United States
Coast and Geodetic Survy Office. These have already been transmitted and placed before you
in reports from the Central Bureau.

I have the honor, however, of submitting, in general terms, the following brief sta-
tement concerning geodetic work in the United States.

The primary triangulation extending from the Atlantic to Pacific Coast along the
39th. parallel of latitude, has been completed by the recent occupation of Pike’s Peak and
Uncompahgre Mountain stations, more than 4200 metres above sea level. The arc of this
parallel comprises 48°78 in longitude and is 4226 kilometres long.

Four or five base lines remain to be measured along this are but all the necessary
azimuth, latitude and longitude observations have been made, except such additional ones as
may perhaps be required for a study of the local deflections, developed in the process of
computation, and it may be remarked that large deflections appear to exist at the junction of
the plains’ triangulation and that brought from the Pacific Ocean across the mountains.

As to the scale and accuracy of the work the following provisional values are sub-
mitted :

The average length of the sides of the Rocky Mt., Sierra Nevada and Coast Ranges,
triangulation is 133 km. as deduced from 86 sides, and many of these exceed 200 km. in
length. Taking 73 cases (all we have at present) in this great triangulation across the moun-
tains, it was found that the mean closing error of the triangles is + 061, whence the mean
error of anangle m= + 0 35. In the smaller triangulation between Kansas and the Mississipi
River, with sides averaging 25 kilometres in length, and to the eastward of the Mississipi with
sides averaging 29 kilometres in length, the values of m are as follows:
