|
ts
i
A.
N
te
i
|

 

ed

VIII. — Italie.

 

Furono eseguite le osservazioni angolari a tutti i punti di 1° ordine della rete geo-
detica compresi nell’ Italia continentale, nella Sardegna e nella Sicilia; mancano sola-

mente quelle ai punti Capraia e Montecristo, perché interessanti il progettato colle-
gamento della Sardegna al continente. -

Le posizioni geografiche sono calcolate di punto in punto adottando gli elementi
di Bessel; le longitudini sono contate a partire dal meridiano dell’ isola del Ferro.

I confronti ottenuti con gli Stati limitrofi, dopo compensate le reti, sono riportati
nello specchio che segue. Come vedesi, le differenze sono di poca entitä e mantengonsi
costanti, cid che depone in favore dell’ accordo fra le varie triangolazioni.

 

 

 

 

 

 

 

 

WV Al @ Boar
DELLE LATITUDINI COMUNI DELLE LONGITUDINI COMUNI
seo Der | DIPRERENZA le Sean eee  DIRKERENZÄ
ITALIA STATI LIMITROFI ar ITALIA STATI LIMITROFI De
oi E 7 E
Francia. Francia.
Monte Dabor 0. 45°06! 49" 45°06! 51" ee ey ee 24313 39, — 2"
» Chaberten. 0" 44 a7 52 44 57 54 nn | 2a TE 24 24 53 — 2
Rocaamelono | 45 12 11 45 12 13 2,24 24 44 28 = 3
Svizzera. | Svizzera.
Limidario o Ghiridone..| 46°07 23" 46° 07' 26" ni 26° 18' 41" en.
Basodme.i...2 0. 46 24 41 46 24 44 So | 26 26 07 59 2e
@ramesmo #700 46 21 46 46 21 49 oe 26 30 28 a
Menone 0 40 ua 0 46 07 24 AG Oy 27 — 3 | 26 48 29 26 48 34 — 5
Austria. Austria.
Pélagosa 000. 420 23! 30" 42023 33" a | Boa ler 33°55! 48" — 6"
TOME eee nee 42 01018 42 07.21 — 8 | So 10) 16 83. 1022 — 6
Grovanmicehio 4.53...) ASS OO 41 50 04 — 3 | 33838 21 83 38126 — 5
Ballons at cou 45 Aly | 45 42, 24 — 8 | 28 29 3° 28 29 46 — 7
PAIS UNO ee wen Als Av 92 45 AT 35 — 8 | 28 50, 25 28 50.32 — 7
Grade 2 45 52 24 45 52 27 8 | 2s Seo
Udime en. aan en 46 03 53 Ao 0858 — 2 | 30 5402 30.54 10 — 8
|

 

|
|
}

 

 

a lh a asm

ELLE
