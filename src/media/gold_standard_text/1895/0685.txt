+ items nn À

MT IM ABA A

ten

al

Em mE = TUR Am |W

 

        

nn

IL. — Baviere ‘et Palatinat.

 

Ueber die Genauigkeit der Winkelmessungen, welche dem Dreiecksnetze der
Bayerischen Landesvermessung zu Grunde liegen, nach General Ferrero’s
Vorschlag.

In der fünften Sitzung, der von 21. bis 29. Oktober vorigen Jahres zu Nizza abgehal-
tenen Konferenz der Permanenten Kommission der Internationalen Erdmessung, hat
Herr General Ferrero den Antrag gestellt, es solle dem Bericht über die Triangula-
tionen jedem Dreiecksnetz eine Grösse m beigegeben werden, welche aus den Wider-
sprüchen w der Winkelsummen den n möglichen Dreiecke des Netzes nach der Formel

berechnet wird = 4/ N
on

Diese Grösse m solle dann als Genauigkeitsmass für die zugehörige Winkelmessung
dienen.

Der Antrag des Herrn General Ferrero bezog sich zwar hauptsächlich auf solche
Dreiecksnetze, welche einer strengen Ausgleichung noch nicht unterzogen sind, gleich-
wohl dürfte aber die Ermittlung des Betrages m auch für bereits ausgeglichene Netze
von Interesse sein, weil nur hiedurch für die Güte der noch nicht ausgeglichenen ein
geeigneter Massstab geschaffen wird.

Für das Dreiecksnetz der Bayerischen Landesvermessung hat Herr Generalmajor
von Orff in dem offiziellen Werke Die Bayerische Landesvermessung in ihrer wissenschaft-
lichen Grundlage, umfangreiche Untersuchungen angestellt, dieselben beziehen sich aber,
der Hauptsache nach, auf die durch Ausgleichung des ganzen Netzes (bezw. durch jedes
der 32 Polygone) erhaltenen Resultate. (Auf diese Untersuchungen soll jedoch hier nicht
näher eingegangen werden).

Was nun die Widersprüche ‘der einzelnen Dreieckssummen anbelangt, so sind die-
selben in dem genannten Werke direkt nicht enthalten, können aber, Dank der Aus-
führlichkeit der Veröffentlichung der Ausgleichung des Netzes, unschwer den für jedes
Polygon mitgeteilten Winkel-Bedingungsgleichungen der Dreiecke entnommen werden
(S. 341 bis 477 der B. L. V., wo sie als ganz bekannte Glieder stehen). So ist, z. B.,
(S. 341, Polygon I) im A @. O. P. der sphärische Excess = 07,767. Die Winkel dieses
Dreiecks sind nach Pag. 309 und 310 der B. L. V. gemessen zu:

 

 

P= 46 10 500

O = 0928 2

G.= 74 4 2
sunme .... u 6 |.
Sphär. Exeess; — 0, 767

US + 0”,433

 
