 

oe

XV. = Russie. (Suite.)

 

 

42% TRIANGLES DE RATTACHEMENT DU PARALLÈLE 47 ‘/o° À CELUI DU 52°.

a) La chaîne de l'Est, entre Sarepta et Saratow.

En 1857 et 1858, sous la direction du colonel Wasiliew, a été exécutée la triangu-
lation de 1* ordre le long du Volga entre la ville de Kazan et celle de Tsaritsyn. C’est
à la série principale de cette triangulation que nous avons emprunté les 38 triangles, qui
constituent le rattachement de l’Est des deux parallèles.

 

 

 

 

 

 

NOMBRE |

SUBDIVISIONS DE L’ARC ÉPOQUE DES ZA? m | REMARQUES
TRIANGLES |
a) Serie des triangles entre Sarepta et |
SAPALON Le. cag ee 1857-58 48 78, 6222 20,4 |
b) Série des triangles entre Saratow et |
la base de Wolsk du parallèle 52. 1857-58 12 24, 4380 se 0 32 |

 

 

b) La chaîne de Kharkow, entre Petrowskoïé et Lawrowo.

Les triangles de ce rattachement ne présentent pas une telle homogénéité comme
c'est le cas pour les triangles de la chaîne de l'Est, car ils appartiennent à des diverses
triangulations, effectuées à des différentes époques. Le nombre des triangles est de 85,
dont 35 sont compris entre Petrowskoïé et Kharkow et 50 entre Kharkow et Lawrowo.

 

 

 

NOMBRE |

SUBDIVISIONS DE L’ARC , ÉPOQUE DES ZA? Mm | REMARQUES
TRIANGLES |
| |
a) Triangles entre Lawrowo et Khar- |
KO nn 1848-57 50 119, 6401 5.080 |
b) Triangles entre Kharkow et Petrow- |
SKOI ce 1852-54 >» 109, 5995 oe, 02.

i}
Il

 

 

 

 
