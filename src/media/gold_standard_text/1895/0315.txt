 

 

à

 

re

14 MT N

Et

DUR

pester

iB
=
=
HB
um
m

=

Ta,

Ib.

IIa.

Eb:

Gee
TIT»,
IVe
IV»,

Va.

Vb,

309

ANHANGE — APPENDICES

Circular des Centralbureaus an sämmtliche Herren Delegirten zur internationalen Erdmessung
und an die nationalen geodätischen Commissionen

Circulaire du Bureau central aux délégués de l’Association de et aux | Commesone
géodésiques nationales .

Zusammenstellung der bei dem Ba a Ms bloc beit effond dip. Er-
neuerung der Erdmessungs-Convention vom Oktober 1886

Résumé des propositions reçues par le Bureau central au sujet du noel a als la
Convention géodésique d’octobre 1886

Entwurf der permanenten Commission für die nel le Fo eee
Convention

Projet de la on on permanente pole ie ON lle at i la aan onesie
internationale

Gegen-Entwurf für die Boone der interna doualen En on
Contre-projet pour le renouvellement de la Convention géodésique internationale .

Neue Uebereinkunft der internationalen Erdmessung, angenommen von der XI. Generale
Conferenz in Berlin, Oktober 1895

Nouvelle Convention géodésique oe 0 0 par ie XIe Conférence conte 3 a
Berlin, en octobre 1895

Conto von der Verwaltung des Do la Ba) Cora m dor iterate.
nalen Erdmessung fiir das Triennium 1893/1895

Compte de gestion du fonds de dotation de la Gomera. ae d an jan
géodésique internationale pendant la période triennale 1893/1895

ERRATA

Page 21, ligne 13, lire Anhang Ia et Ib au lieu de Beilage C. I.
Page 46, ligne 9, lire C. ZZ au lieu de C. IV.
Page 69, ligne 17 d’en bas lire G0 000 M. au lieu de 66 000.

Pag.

254

255
257-264
257-264
265-268
269-272
273-276
271-279
280-283
284-287

288-289

290-291

 
