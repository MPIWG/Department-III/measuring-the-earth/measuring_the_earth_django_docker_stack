 

 

 

Stelle der deutschen Einzelstaaten, in
die internationale Erdmessung einzu-
treten ».

Die permanente Commission beschliesst da-
rauf, die bisherige Bestimmung, welche
jedem Staat der Erdmessung eineStimme
zuertheilt, aufrecht zu erhalten

Discussion der finanziellen Fragen (Art. 6,
7 und 8) un

Ilerr Rony en halt die jibe Dota-
tion für genügend und erklärt sich gegen
internationale Arbeiten he
Kosten . : :

Herr Forster ist les es ac nur um
Fortsetzung und Entwickelung der bis-
her mit gutem Erfolg ausgeführten Ar-
beiten der Erdmessung handelt .

lerr Zallemand entwickelt die Gründe,
wesshalb die französische Commission
sich gegen eine bedeutende Vermehrung
der Dotation erklärt hat

Herr Hirsch wünscht, dass man vor ie
sich über die im A. Paragraphen ent-
haltene Aufgabe, die Breitenvariationen
durch A astronomische Stationen zu
studiren, erklärt. Von Fall zu Fall Cre-
dite von allen Staaten zu verlangen sei
unmöglich . :

Herr Foerster erinnert dar: an, dass Be
Länder eine wirksamere Entfaltung der
Erdmessungs- Arbeiten als Bedingung
ihres ferneren Beitritts aufstellen

Herr Tisserand meint, dass die vier
Breitenstationen von den Ländern, wo
sie errichtet werden, zu erhalten seien

Herr Bassot erklärt, dass wenn eine dieser
Stationen nach Algier verlegt würde,
Frankreich die Kosten decken werde

Art. 6 wird mit 5 Stimmen gegen 1 ange-
nommen

Der erste Absatz died Abe 1, le die
Dotation auf 60 000 M. orhaht: wird
mit 4 gegen 3 Stimmen angenommen ;
der Rest des Artikels mit 5 gegen 2

Vierte Sitzung, 4. Oktober 1895

Verlesung und Annahme des Protokolls der
dritten Sitzung

304

Pag.

199-200

200-205

200-204

204

204-203

203

no
Oo
=

19
>
=

204

205

206-211

206

 

iirster Bericht der Finanz-Commission

Die Rechnungen des Jahres 1894 werden
einstimmig genehmigt und dem Director
des Centralbureaus Entlastung ertheilt .

Die vorgeschlagene Ausgabenvertheilung
fiir das Jahr 1896 wird gebilligt und ein
ausserordentlicher Credit von 2000 M.
fiir die Publication der Breitenbeobach-
Lungen genehmigt ; :

Es wird einstimmig Den die Ber
träge des laufenden Jahres für 1896 bei-
zubehalten .

Herr Foerster giebt Doch Eee, a
über den befriedigenden Zustand der
Finanzen der Erdmessung. ;

Art. 8 des Conventions-Entwurfs wird ein-
stimmig angenommen ;

Betreff der Abstufung der Be irae in
Art. 9 wird von Herrn Hirsch eine
Stufenleiter von sechs Klassen, anstatt
der vier Klassen des Entwurfs vorge-
schlagen. Nach stattgefundener Discus-
sion wird die Stufenleiter des Entwurfs,
nachdem der Beitrag der dritten Klasse
von 2800 auf 3000 M. gebracht ist, mit
5 gegen 2 Stimmen angenommen

Die zwei letzten Absätze des Art. 9 wer-
den einstimmig beseitigt

Es wird beschlossen, dass die Bäitfäße
durch den Eintritt eines neuen Staates
nicht abgeändert werden :

Art. 10 wird ohneBemerkung angenommen

Man beschliesst, in Art. 16 des Entwurfs
die Ernennung des Präsidenten und
Vicepräsidenten durch die jetzige Con-
ferenz aufzunehmen .

Fünfte Sitzung, 7. Oktober 1895

Verlesung und Annahme des Protokolls der
vierten Sitzung

Discussion über die zwanzig a Danas
der Convention, welche im Projekt vor-
geschlagen ist :

Herr Bakhuyzen glaubt, fee de Resin:
rungen sich nicht auf so lange Zeit bin-
den möchten .

Herr Foerster antwortet, des nach Ar 15
eine Revision der Uebereinkunft zu jeder

 

Pag.
206-207

“ue

sh

ide

207

ira

40.4 d

207

207-208

208

Sabb babel

210
210

210-211

212-215

212

a md ah a ha aa Ah nas nn

213-214

213-214

'
|
|
|

 
