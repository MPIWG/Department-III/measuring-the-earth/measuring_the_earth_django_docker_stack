 

 

IH
i
ji
i
|
i
N
}

 

CINQUIEME SEANCE

Lundi, 7 octobre 1895.

Présidence de M. Faye.

Sont présents : Les membres de la Commission permanente : MM. Bakhuyzen, Faye,
Forster, Helmert, Hennequin, Hirsch, von Kalmär, von Zachariae.

Les délégués : MM. Bassot, Bouquet de la Grye, Fergola, Geelmuyden, Lallemand,
Nell, Rajna, Schols, Tisserand.

La séance est ouverte 4 dix heures et quart.
M. le Président prie le Secrétaire de lire le procès-verbal de la dernière séance.

M. Bakhuyzen désire voir consigner à la fin du procès-verbal sa demande de sup-
primer le premier alinéa de l’article 15 et d’en faire une nouvelle rédaction, suivant les
changements adoptés au cours de la discussion.

M. le Secrétaire tiendra compte de cette observation.
Le procès-verbal est adopté à l’unanimité.

M. Fœrsler regrette que, par suite du repos dominical dans les ateliers d'imprimerie,
il ne soit pas encore en possession des épreuves du projet de Convention, que l'imprimerie
avait promises pour ce matin; il espère cependant qu’on les apportera avant la fin de la
séance.

Quant à l’article 15, puisque la disposition concernant le quorum est déjà comprise
dans l’avant-projet, il ne reste des anciens statuts qu’un seul article à transporter dans la
nouvelle Convention, c’est celui qui stipule : « Les membres absents peuvent déléguer leur
voix par une lettre adressée au Président, à un des membres présents. Cependant aucun des
membres ne doit accepter plus d’une délégation de ce genre. » Gette clause trouvera sa place
naturelle a la fin du quatrième alinéa de l’article 5. On verra qu’elle y figure déjà dans le
texte imprimé.

La Commission se déclare d'accord.

M. Bassot croit important de discuter le second alinéa de l’article 15, fixant à vingt

 

3
3
ä
2
3
3
3
{

Hu td.

ui

Lalkunsskssuhusl

aa Mo A Lu La dl nas, Le

4
4
=
1

 
