 

een

en es

SHES EEA

 

SLI SEE

 

Annexe B. VIII.

PAYS-BAS

Triangulation. Les opérations pour la triangulation des Pays-Bas ont été poursuivies
cet élé par trois brigades d'ingénieurs. Deux de ces brigades étaient chargées de la mesure
des angles et ont fait les observations aux stations de : Velmoe, Amersfoort, Utrecht, Rhenen,
Zull-Bommel, Gorinchem, Dordrecht, Ondgastel et Bergen op Zoom. Les ingénieurs de la
troisième brigade se sont occupés de la construction des installations sur les stations qui doi-
vent servir pour les observations de l’année prochaine et de la poursuite des reconnaissances
pour le réseau primordial, qui touchent maintenant à leur fin.

Travaux astronomiques. On a continué les réductions des observations faites en 1893
pour la différence de longitude entre Leyde et Ubagsberg et pour la latitude d’Ubagsberg et
l’azimut Ubagsberg-Sittard et on a observé au cercle méridien de Leyde les déclinaisons des
étoiles employées pour les observations faites à Ubagsberg d’après la méthode Horrebow-
Talcott. Jusqu'à présent tous ces calculs devaient être faits par les astronomes de l’Observa-
toire de Leyde; mais comme on vient justement de nommer un calculateur spécial pour la
réduction des observations géodésiques, nous espérons posséder bientôt les résultats défi-
nitifs.

Maréographes. On a continué la réduction des observations faites aux maréographes
de Helder, Ymuiden et Hock van Holland. Pour ce dernier point, on a aussi calculé les
moments et les hauteurs des hautes eaux pour l’année prochaine.

Observations de pendule. La Commission néerlandaise a reçu cette année-ci un
pendule d’après le système de M. Defforges pour la détermination de l'intensité relative de la
pesanteur. M. le Lieutenant-Colonel Defforges a fait avec ce pendule une détermination de
l'intensité de la pesanteur à Paris et nous avons fait immédiatement après l’arrivée de l’ap-
pareil une détermination analogue à l'Observatoire de Leyde, de sorte que la différence des
deux intensités est à présent déterminée deux fois avec des appareils différents, en 1892 et en
1895, ce qui donne un contrôle précieux.

H.-G. van pE SANDE BAKHUYZEN.
Ch.-M. SCHOLS.

ah an
