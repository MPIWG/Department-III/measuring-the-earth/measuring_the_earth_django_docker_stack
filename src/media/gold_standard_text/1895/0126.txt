 

les Rapports sur les triangulations (M. le général Ferrero), sur les bases (M. le colonel
Bassot), sur les déterminations astronomiques (M. le prof. Albrecht), et sur les mesures de la
pesanteur et les déviations de la verticale (M. Helmert).

€ 59 M. Albrecht présentera le Rapport sur le résultat des recherches indépendantes
des observations concernant le mouvement de l’axe terrestre dans l’intérieur du globe. Je
note ici en passant que le Bureau central lui-même participe à ces recherches dès la fin de
1893, au moyen d’une série irréprochable de mesures de latitude, exécutées depuis le com-
mencement de cette année dans une cabane particulièrement appropriée à ce but.

« Le crédit accordé en faveur des recherches de latitude a été employé en partie à
l’avancement des calculs de réduction.

« 6° L'étude des mires en bois pour les nivellements en est encore à ses débuts, puis-
que nous n'avons réussi que tout dernièrement à imprégner convenablement ces mires. M. le
Dr Stadthagen a fait confectionner pour le Bureau central 27 règles en bois de sapin, d’une
longueur de 1 mêtre et de 18 millimètres carrés de section. Une partie d’entre elles ont
été imprégnées complètement d'huile de lin par M. Fuess, au moyen d’un appareil construit
dans ce but.

« Ensuite on a exécuté des mesures de longueur et des pesées sur les règles im-
prégnées et sur les autres afin de reconnaitre l'influence de ’humidite.

« Une partie du crédit affecté à cette étude a été dépensée.

« Le rapport de M. le Dr Stadthagen suivra plus loin sous lit. C.

€ 7° On n’a pas perdu de vue les mesures de la pesanteur au moyen d'appareils
à élasticité, mais on ne peut pas encore indiquer de résultats.

B. Affavres administratives.

« 1° Le Fonds de dotation a été géré comme par le passé.

« 20 Les propositions de MM. les délégués pour le renouvellement de la Convention
ont été rassemblées et, sur le désir de la Commission spéciale, elles ont été imprimées et
expédiées à tous les intéressés.

« 3° De même, on a distribué, comme les années précédentes, de nombreuses publi-
cations géodésiques. (Voir plus loin la liste sous lit. D.)

C. Rapport sur les premières éludes concernant l'influence de la température et de l'humidité
sur les règles en bois, par M. le D' IT. Sladthagen.

« La tâche qui m'a été confiée il y a un an, de la part du Bureau Central de l’Associa-
tion géodésique internationale, peut se résumer dans les trois points suivants :

« 1° Chercher les moyens de diminuer directement l'influence de l'humidité sur les
règles en bois ;

« 2 Rechercher l'influence de l'humidité relative et absolue sur les variations de
longueur de règles diversement traitées ;

 

;
3
i
3
3
i
3

Habbansshaiubich

4

A nm ha a Mt D di ana

 
