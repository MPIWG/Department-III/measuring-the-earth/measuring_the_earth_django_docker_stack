|
it
4

 

LE en SER

fil

a LL

OUT DT TTT

er pry

9

Um den Grad der Annäherung der beobachteten und berechneten Werthe noch
übersichtlicher hervortreten zu lassen, habe ich auf Tafel II eine graphische Darstellung
der beobachteten Monatsmittel und des aus der Polbewegung hervorgehenden Verlaufes
der Polhöheneurve — entsprechend den Zahlenwerthen in der 5. und 6. Columne der
Zusammenstellung der Beobachtungsresultate — gegeben.

In der Natur der Aufgabe ist es begründet, dass sich die Curve der be-
rechneten Werthe an die- beobachteten Monatsmittel für eine Station wie Bethlehem
besser anschliesst als beispielsweise für die europäischen Stationen, weil die Kenntniss
der für den erstgenannten Beobachtungsort hauptsächlich in betracht kommenden
y-Coordinate vorwiegend auf den Beobachtungen dieser einen Station basirt.

Für die europäischen Stationen wird man aber berechtigt sein, aus dem Grade
der Annäherung auf die Güte der einzelnen Beobachtungsreihen schliessen zu können,
und wird insbesondere auch aus demselben ersehen, ob und inwieweit die Resultate
auf den einzelnen Stationen noch von systematischen Störungsursachen beeinflusst
sind. Freilich wird es sich in dieser Beziehung gegenwärtig nur um einen vorläufigen
Aufschluss handeln, da das vorliegende Material infolge der geringen Zahl der Stationen
und des theilweise provisorischen Characters der abgeleiteten Polhöhen noch keine
definitiven Schlussfolgerungen ermöglicht.

Aus der graphischen Darstellung auf Tafel II ersieht man, dass die ver-
bleibenden Reste vielfach den Character systematischer Abweichungen zeigen und
dass daher thatsächlich das Bedürfniss vorzuliegen scheint, in noch peinlicherer Weise
als bisher auf Beseitigung aller Einflüsse, aus denen eine systematische Störung der
Resultate hervorgehen könnte, bedacht zu sein.

Die mittleren Abweichungen der beobachteten Monatsmittel vom Verlauf der

Curve stellen sich, nach der Formel VE berechnet, für die verschiedenenStationen auf:
: N

Kasam es ee ea a ee Ho%054
Palkowa (bis 1893.30) 5 3.5 8 0.056
Wien ©... ..2. ee. 0.065
Drag a 202 sun nn =; =E0.074
Ben oe ee ee =2-0,.052
POSTE a 8.2 a o.o49
Karlsmhei. zn... 2.02.02. 20028. ..\. Eo .086
' Strassburg . ee Re ie Ho.o6r
Bethlehem an Our lo, ue 0.041
Rockville, (bis; 2802-22). uam. Eo .082
San Francisco Pa ee 20.065
Honolulu, Int. Erdm. Do 40.029
Honolulu; ©. a: Grouly. 22 73 250,050)

Was die angenommene mittlere Lage des Poles anlangt, so wiirde man dieselbe
nachträglich dadurch verificiren können, dass man die arithmetischen Mittel der aus
der Rechnung hervorgegangenen Goordinaten der einzelnen Polpunkte bildet und den

i)

a

 
