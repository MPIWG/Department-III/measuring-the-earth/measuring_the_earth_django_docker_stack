Hauer
Ten eee

 

I Ti im! in FIL HIT

i

5
i=
4
=

x

>
se
i
as
a

 

_

les circonstances. Les chiffres définitifs seront consignés dans un rapport financier qui

sera
soumis aux Hauts Gouvernements et publié dans les Comptes-rendus.

La Conférence procède ensuite aux élections. Les membres de la Commission perma-
nente sortants cette année sont : MM. van de Sande Bakhuyzen, Ferrero, Fœrster et Henne-
quin : d’après les statuts, ils sont rééligibles. En 1899, étaient sortis suivant l’ordre établi :
MM. Faye, von Kalmär, Stebnitski et von Zachariae. Il n’a pas été repourvu aux sièges de M.
Davidson et de feu le général Ibafiez. A Bruxelles, la Commission permanente a élé autorisée a
pourvoir a ces deux sièges par voie de cooptation; mais pour des raisons particuliéres elle n’a

pas fait usage du droit qui lui avait été conféré, de sorte que des élections seront également
nécessaires pour combler ces deux vides.

M. le Président propose de voter, dans un premier tour de scrutin, sur les quatre
membres sortant réglementairement, et de pourvoir, dans un second tour, aux deux vacances
de M. Davidson et du général Ibañez.

Les élections ont lieu au scrutin ; M. le Président prie MM. Celoria et Weiss de se
charger des fonctions de scrutateurs. Il invite M. le Secrétaire à faire l'appel des États repré-
sentés à la Conférence et MM. les porte-voix désignés par les déléoués de ces États à déposer
leur bulletin dans l’urne placée sur le bureau.

Au premier tour de scrutin pour l'élection destinée à remplacer les quatre membres
sortants, 17 bulletins, contenant chacun 4 noms, ont été déposés dans lurne. Leur dépouille-
ment a donné les résultats suivants :

MM. van de Sande Bakhuyzen a obtenu 16 voix.

Ferrero » 17 »
Foerster » 1h)
Hennequin » Aa »
Tisserand » 135

En conséquence, les membres sortants ont tous été réélus.

MM. Bakhuyzen, Fœærster et Hennequin acceptent leur élection avec remerciement ;
le bureau demandera à M. Ferrero s’il accepte sa nomination!.

Le remplacement de M. Davidson et du général Ibañez a lieu également au scrutin ;

17 bulletins sont rentrés, dont 1 ne porte qu’un nom. Le dépouillement du scrutin donne
le résultat suivant :

! M. le général Ferrero a, par dépêche télégraphique, accepté son mandat. A. H.
