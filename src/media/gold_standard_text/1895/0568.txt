 

i

a

=

248
Körtvelyes, Johannesberg,* Baj temetes, Viniënivrch, Nagy Perkätaj-hegy, Meleghegy, Oerög
Futone, Szärhegy, Retschek, Garabhegy, Hardihegy, Cserned, Oerüghegy, Jakobsberg, Zen-
govar, Cserhathegy, Csoka und Harsäny.

Beobachtungen, und zwar theils Ergänzungen, theils Neumessungen, wurden aus-
geführt auf den Stationen Magoshegy, Säghegy, Köröshegy, Körlvelyes, Johannesberg, Baj
lemetes, Viniénivrch, Nagy Perkataj-hegy, Meleghegy, Oerdg Futone, Szärhegy, Retschek,
Garabhegy, Hardihegy, Cserned, Oerdghegy, Jakobsberg, Zengôvar, Cserhäthegy, Csoka und
Harsäny.

Die Triangulirungs-Abtheilung, die im südlichen Theile der Polygonkette die Beob-
achtungen durchzuführen hatte, übersiedelte nach Vollendung derselben, anfangs August,
nach Werschetz, um in der Nähe dieser Stadt ein für die Messung einer Grundlinie von ca.
4 km. Länge geeignetes Terrain auszumitteln, das Terrain zur Messung vorzubereiten und
die Grundlinie auf die Dreieckseite 1. Ordnung Kudritzerkopf, Antina livada, zu entwickeln.

Für das Entwicklungsnetz wurden Signalbau und Beobachtungen auf dem östlichen
und westlichen Basis-Endpunkte, auf Retisova, Lagerdorf, Kudritzerkopf, Antina livada,
Plesiva mare und Dumäcsa durchgeführt.

Zu der Messung der Grundlinie wurde, wie bei allen früheren Basismessungen in
Oesterreich-Ungarn, der Basis-Messapparat des k. u. k. militär-geographischen Institutes
benützt, dessen Stangenlängen und Ausdehnungs-Coefficienten in den Jahren 1893 und 1894
im Bureau international des Poids et Mesures zu Breteuil neu bestimmt worden sind.

Die Messung begann am 10. Oktober und währte, vielfach durch Regen verhindert,
bis 4. November. Die Berechnung der Messungsergebnisse wird im Winter 1895/96 vorge-
nommen werden.

Wien, im Dezember 1895.
HARTL, Oberst.

D. BERICHT DES OBERST VON STERNECK ÜBER DIE AUSGEFÜHRTEN
SCHWEREBESTIMMUNGEN FÜR 1895.

Die im vorigen Jahre 1894 begonnene systematische Untersuchung grosser Land-
flächen bezüglich der Schwere wurde im heurigen Sommer mit 2 Pendelapparaten forigesetzt,
und es gelangten die Länder Mähren und Schlesien und der östliche Theil von Böhmen mit
65 Stationen zur Dotirung. Auf diesen Stalionen wurde der Gang der Pendeluhren durch
telegraphische Zeitsignale bestimmt. Dieselben wurden früh und abends von Centralstationen
abgegeben, in welchen ein Observatorium mit einem Passagenrohr etablirt war. Hiedurch
war es möglich, die Pendelbeobachtungen ganz unabhängig von der Witterung auszuführen.

Es benöthigte jede Station nur 2 Tage, und wurden thatsächlich die 65 Stationen in 74 Tagen
absolvirt.

Eine sehr wesentliche Neuerung war die Verwendung eines neuarligen Pendelstatives
— des sogenannten Wandstatives —, welches mit Schraubenbolzen an die festen Mauern der

 

base aaa

 
