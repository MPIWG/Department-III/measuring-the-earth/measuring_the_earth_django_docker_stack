a

Kil. — Prusse, -B.

 

 

DAS HAUPTDREIECKSNETZ der Trigonometrischen Abtheilung der
Königlich Preussischen Landesaufnahme nebst Vorbemerkungen,
Verzeichniss der Instrumente und der Veröffentlichungen.

Aufgestellt im Januar 1885.

Vorbemerkungen. — Im Jahre 1863 wurde durch Königliche Kabinetsordre
die Herstellung eines über den ganzen Staat zu legenden trigonometrischen Netzes
von 10 Punkten auf die geographische Quadratmeile angeordnet, und die Trigonome-
trische Abtheilung des Generalstabes mit der Ausführung beauftragt. Dieses Netz sollte
sowohl allen Anforderungen der Wissenschaft, als auch den Verwaltungsbedürfnissen
genügen. Preussen war damals schon mit Dreiecksketten fast ganz überzogen; von diesen
wurden aber nur die Gradmessung in Ostpreussen und die nach deren Muster ausge-
führten Ketten für genügend erachtet, als Theile des dem neuen allgemeinen Netze zu
Grunde zu legenden Hauptdreiecksnetzes beibehalten zu werden. Letzteres ist seitdem
unausgesetzt weitergeführt worden und wird in einigen Jahren fertig sein. Etwa nach
acht Jahren wird auch das allgemeine Netz beendigt werden.

Das unten folgende Verzeichniss enthält sämmtliche Punkte des Hauptdreiecksnetzes,
soweit sie bereits endgültig bestimmt sind. Jede unter besonderer Benennung zusam-
mengefasste Gruppe (Kette oder Netz) ist als Ganzes für sich, jedoch im völligen
Anschluss an die älteren Gruppen, ausgeglichen worden.' Das ganze Netz ist, mit
alleiniger Ausnahme der von Bessel ausgeführten , Gradmessung in Ostpreussen,” von
der Trigonometrischen Abtheilung* gemessen und bearbeitet worden. In der Kolumne
„ Directeurs ” findet sich demgemäss der jeweilige Chef dieser Abtheilung gennant.

Bezüglich der Gliederung der Messungen ist zu bemerken, dass dieselben in drei
Ordnungen, und diese wiederum in mehrere Rangklassen zerfallen. Insbesondere besteht
die erste Ordnung aus drei Rangklassen : zur ersten gehören nur diejenigen Ketten und
Netze, deren Fehler sich über alle Theile des allgemeinen Netzes mehr oder weniger
fortpflanzen, während die „Füllnetze” und die „Zwischenpunkte erster Ordnung,”
deren Fehler auf ein verhältnissmässig kleines Gebiet beschränkt bleiben, die zweite und
dritte Rangklasse bilden. Das Hauptdreiecksnetz setzt sich le'iglich aus den Ketten und Netzen
der ersten Rangklasse zusammen.

 

 

! Ausser dieser für eine Landesvermessung unentbehrlichen Ausgleichung mit Anschluss ist bei mehreren
(seit 1876 bei allen) Ketten, bezw. Netzen, noch eine solche ohne jeden Anschlusszwang ausgeführt worden.
Ü 5 2 2 : ee i ‘J : : Spa :
? Infolge von Neuorganisationen wurde im Jahre 1865 der bisherigen ., Trigonometrischen Abtheilung des
a = : : ” TR 8 >
Generalstabes” die Benennung „Bureau der Landes-Triangulation,” und im Jahre 1875 dem letzteren die Be-
nennung: „Irigonometrische Abtheilung der Landesaufnahme,” beigelegt.

 

te eal hs ae

 
