IE

|

urn

Pen

it
4

=
=
iS
im
=
>
dE
=
=

 

Annexe C. I.

RAPPORT

de M. le D' Ch, Guillaume à M. le Professeur Fœrster sur les recherches faites
au Bureau international des Poids et Mesures concernant les métaux pro-
pres à la confection des règles étalons.

À la suite des recherches faites au Bureau international sur les métaux destinés à la
construction des régles étalons, recherches communiquées au Comité international des Poids
et Mesures dans sa session de 1899, la Société Genevoise pour la construction d'instruments
de physique entreprit la confection de règles de 1 mêtre en bronze blanc, le nickel ne parais-
sant pas, à cette époque, pouvoir être obtenu industriellement à un état d’homog£neite suffi-
sante. Le bronze employé par la Société Genevoise était lalliage 4 40 °/o de nickel, breveté
par la maison Baasse et Selve à Altena (Westphalie), sous le nom de Constantan. Les expé-
riences faites au Bureau, sur des fils ou des lames de ce métal, avaient donné des résultats
satisfaisants. Mais on reconnut bientôt que les barres de plus fortes dimensions contenaient
souvent des pailles ou fissures qui les rendaient absolument impropres à la confection d’éta-
lons de 1 mêtre de longueur. Des essais faits au Bureau international sur de forts barreaux
atteignant 6em de diamètre et destinés à la construction de poids étalons ne furent pas plus
satisfaisants. Même après un nouveau tirage et un forgeage, ces barres contenaient des
cavités de plusieurs centimètres cubes. Certaines parties du métal purent être ulilisées, mais
le reste dut être mis au rebut.

En présence de ces insuccès répétés, il parut désirable de reprendre, de concert
avec des établissements industriels, des essais avec d’autres alliages de nickel, et du nickel
pur. Ges essais ont conduit à des résultats encourageants que j’indiquerai dans ce Rapport,
en même temps que quelques essais sur le constantan.

Constantan. Les essais ont porté sur une lame de 2m d'épaisseur, de 30 cm de

longueur et de 12""5 de largeur. On a trouvé pour le module d’elasticit du metal
16 600 kg/mm? |

 
