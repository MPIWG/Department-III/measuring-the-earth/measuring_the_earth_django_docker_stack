 

ao
ame

vw

rd Be RER a ar
“CV PPT WPAN] CTT

RST terete

mr

we

ih i halte
ET TOP NETH BRET UP ET FFU TTT

ie aa

oll

Herr Hirsch wünscht, dass diese Vorschläge, als formulirte Artikel präeisirt, zunächst
der permanenten Commission zur Vorberathung übergeben würden, weil es in einer kleineren
Versammlung leichter ist, sich zu verständigen.

Der Herr Präsident hält es formell für bedenklich, dass die permanente Commission
sich mit anderen Vorschlägen noch beschäftigt, nachdem sie sich in ihrer Mehrzahl über den
vorliegenden Entwurf geeinigt habe. Es stellt daher anheim, die formulirten Vorschläge dem
Bureau bis zur nächsten Sitzung zu übergeben.

Herr Tisserand findet es gleichfalls nicht rathsam, die permanente Commission noch
weiter mit diesen Vorschlägen zu beschäftigen, weil von ihr bereits der Entwurf herrühre,
sondern empfiehlt, sie direkt an das Bureau der Conferenz gelangen zu lassen, damit dasselbe
das Weitere veranlassen könne. Wenn die Mitglieder der Conferenz auch bisher die Erlaubniss
gehabt haben, an den Verhandlungen der permanenten Commission Theil zu nehmen, so
war doch nur ein Theil der Herren in der Lage, den Verhandlungen der Commission beizu-
wohnen.

Herr Hirsch zieht seinen Vorschlag zurück und ersucht die Herren Antragsteller,
ihre formulirten Vorschläge dem Bureau recht bald, möglichst am Mittwoch Morgen, mitzu-
theilen, damit dieselben in beiden Sprachen gedruckt und an die Mitglieder vertheilt werden
könnten.

Die nächste Sitzung wird auf Mittwoch den 9. Oktober, 3 Uhr Nachmittags, anbe-
raumt.

Schluss der Sitzung 4 Uhr.

ASSOCIATION GEODESIQUE — 8

 
