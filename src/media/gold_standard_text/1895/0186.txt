 

i
i}
i
i
i

 

M. d’Arillaga demande si, à Vavenir, les Etats pourront encore capitaliser leurs
contributions.

M. le Président répond que ce mode de payement ne serait plus pratique avec Paug-
mentalion du budget adopté.

Enfin, la Résolution finale est acceptée, dans la forme du contre-projet, sans oppo-
sition. (Voir celte Résolution à l’'Appendice N° IV.)

Vu l’heure avancée et considérant que la Conférence générale a encore à s'occuper
d’un certain nombre d'objets importants, M. le Président propose de fixer, pour le samedi 12
octobre, une dernière séance avec l'ordre du jour suivant : Votation définitive sur l’ensemble
de la nouvelle Convention ; élections nécessitées par la Convention actuelle et par la Résolu-
on finale qui vient d’être adoptée; décision à prendre à l'égard des contributions pour
l’année 1896; enfin Rapport financier de la Commission permanente.

À la demande de M. Haid si les deux lectures qu’on a prévues dans la dernière
séance s'appliquent seulement aux articles 2 et 7 ou si elles concernent tous les articles du
projet, M. le Président répond que la Conférence n’a eu l'intention de faire intervenir une
seconde lecture que pour les deux articles en question.

M. von Sterneck désirerait que le vote définitif sur l’ensemble de la nouvelle Conven-
tion püt encore avoir lieu dans la séance de ce jour.

L'Assemblée s'étant déclarée d'accord, la Conférence générale adopte, à l’appel no-
minal et à l'unanimité, l’ensemble de la nouvelle Convention.

Ce résultat est salué par des acclamations générales.

À la demande de plusieurs délégués, la prochaine séance, qui sera en mème temps
la dernière, est fixée au lendemain, à 10 heures du matin.

La séance est levée à 5 heures et demie.

 

ren ee

tm bn pa, Mal LA tnt ua à

|
|
