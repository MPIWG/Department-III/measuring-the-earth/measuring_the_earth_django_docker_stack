 

 

      

age:

71. France et “Algerie.

 

Des observations astronomiques ont été exécutées jusqu’ici, pour la latitude, la lon-
gitude et l’azimut, à Bône, Nemours, Tunis, Biskra, Laghouat, Geryville, Guelt-es-Stel
et M’ Sabiha par MM. Perrier, Bassot et Defforges.

En 1879, la triangulation algérienne a été reliée avec la triangulation de l’Espagne
par un vaste quadrilatére. Les résultats de cette opération sont consignés dans un mé-
moire spécial rédigé en commun par MM. le général Ibañez pour l'Espagne et le co-
lonel Perrier pour la France (voir aussi Tome XIII du Mémorial).

Comme, d’un autre côté, cette triangulation aboutit vers l’est aux triangles Jetés
par les officiers italiens entre la Sicile et la Tunisie, on voit que le bassin occidental
de la Méditerranée peut être considéré comme entouré d’une chaine continue de
triangles.

Disons, en terminant, que l’arc de méridien anglo-franco-hispano-algérien s'étend
maintenant sans interruption depuis les îles Shetland, au nord, jusqu’à Laghouat, au sud,
par une amplitude supérieure à 28 degrés et qu’il pourra un jour être prolongé, vers
le sud, encore d’un degré jusqu’au territoire des M’zabites, ainsi qu’il résulte d’une
reconnaissance récemment exécutée par le commandant Defforges.

 

Ao Ml ad aa
