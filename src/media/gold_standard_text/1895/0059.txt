 

21: eis anc 8 adh tah 11480
TE RE RITTER "STERNEN":

ETAT TNT

À bei

es um
DR TOR OP PTE A OTT PRET TIT

Pure

oo

sprechen, eine ungerade Anzahl von Mitgliedern zu wählen, ganz abgesehen davon, dass
infolge von Verhinderungen bei Krankheitsfällen oder dringenden Amtsgeschäften sich häufig
das administrative Comit6 auf 3 oder 2 Mitglieder reduziren könnte ; es sei denkbar, dass in
einem bestimmten Falle dieses administrative Comite, welches eine gewisse Controlle über die
ganze Verwaltung ausüben soll, gerade aus den beiden permanenten Mitgliedern bestehe,
deren Amtsführung zu controlliren wäre.

Was den französischen Vorschlag anlange, so sei ebenfalls im Schosse der perma-
nenten Commission mehrfach auseinander gesetzt worden, dass derselbe mit dem ursprüng-
lichen französischen Vorschlage, die permanente Commission einfach zu beseitigen, im Grunde
identisch sei, insofern als eine permanente Commission von 21 Miteliedern nichts Anderes
darstelle, als die Generalconferenz; die Majorität in dieser, in welcher nach Staaten abge-
stimmt wird, würde genau auf denselben 21 Stimmen beruhen, wie diejenige in der perma-
nenten Commission. Noch weniger würde aber eine solche Commission permanent sein, da
sie sich wahrscheinlich nur alle drei Jahre versammeln dürfte.

Eine weitere Schwierigkeit liege in den jährlichen Zusammenkünften dieser 21 Mit-
glieder, deren Zweckmässigkeit durch alle Erfahrungen, sowohl in der eigenen wie in andern
internationalen Organisationen, als zweifelhaft erscheint. Es sei schon schwierig, nur alle drei
Jahre die Generalconferenz ziemlich zahlreich beschickt zu sehen ; seien doch selbst diesmal
von 28 Staaten nur 17 vertreten. Eine solche Regelung habe deshalb der permanenten Com-
mission unpraktisch und in gewissem Grade sogar unmöglich erscheinen müssen. Wenn
aber die permanente Commission wirklich unterdrückt werden und nur die Generalconferenz
bestehen bleiben sollte, dann sei es allerdings nöthig, eine Art administrativer Körperschaft
von ö oder 4 Mitgliedern zu ernennen. Die Gründe, welche dagegen sprechen, seien bereits
vorher entwickelt, und es empfehle sich zwischen den beiden Extremen den vermittelnden
Vorschlag im vorliegenden Entwurf anzunehmen.

Wenn der Vorschlag der permanenten Commission, wie er hoffe, von der Mehrzahl
der Delegationen angenommen werde, so lässt sich erwarten, dass die französischen Dele-
girten dieser Thatsache Rechnung tragen, damit man nicht zu befürchten brauche, dass eines
der in der Geodäsie hervorragendsten Länder von der geodätischen Vereinigung zurücktreten
könne.

| Schliesslich kann Herr Hirsch die Bemerkung nicht unterdrücken, dass eine offizielle
Erklärung von Seiten einer Regierung, wie eine solche am Ende des französischen Vor-
schlages enthalten ist, grossen Bedenken ausgesetzt sei, da dieselbe nicht nur die betreffende
Regierung in einer ihr selbst später vielleicht keineswegs wünschenswerthen Weise binde,
sondern auch, wenn diese Erklärung als eine unumgängliche Bedingung für den Beitritt der
betreffenden Regierung bezeichnet wird, dadurch der Anschein entstehen könnte, als ob auf
die übrigen ebenfalls souveränen Staaten ein Druck ausgeübt werden sollte.

Eine solche Versammlung wie die jetzige Conferenz, in welcher die Vertreter einer
Anzahl unabhängiger Staaten berufen sind den Entwurf einer gemeinsamen Uebereinkunft
auszuarbeiten, kann in der That nur dann mit Erfolg diese Aufgabe lösen, wenn die Instruk-
lionen der einzelnen Delegationen keinen absolut bindenden Character haben, weil alsdann

 
