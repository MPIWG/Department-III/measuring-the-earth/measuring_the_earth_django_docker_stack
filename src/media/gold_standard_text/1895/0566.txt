 

|
x
;
ie
5
;

RP... Te aig

246

Ausserdem wurden in Böhmen 65 km. als zweite und dritte Messungen nivellirt.

Durch diese Arbeiten ist das Nivellement im westlichen Theile der Monarchie zum
Abschlusse gebracht worden. Die definitive Bearbeitung desselben ist bereits in Angriff ge-
nommen und dürften die Resultate bald zur Publication gelangen.

Auch im heurigen Jahre wurden die Veränderungen der Lattentheilungen während
der Feldarbeit durch relative Bestimmungen ermittelt, Die erhaltenen Resultate sind in der
nachfolgenden Tabelle zusammengestellt.

Die Latten A’, D’ und F’ waren heuer nicht in Verwendung, sondern in Wien deponirt.

Um auch die Veränderungen dieser Latten evident zu halten, wurden dieselben
gleichzeitig mit den übrigen Latten in den Monaten Mai und Oktober verglichen.

Es zeigt sich, dass dieselben ihre Länge gar nicht geändert haben, während jene,
welche in den Monaten Mai bis September im Felde im Gebrauche waren, ein Anwachsen des
mittleren Lattenmeters bis zum Schlusse der Feldarbeit im August aufweisen.

Von da an zeigt sich wieder eine successive Verkürzung derselben. Im Allgemeinen
ist der Verlauf der Lattenveränderungen ganz ähnlich jenem der früheren Jahre.

In der nachfolgenden Tabelle sind die Ergebnisse der Latienvergleiche übersichtlich
zusammengestellt.

   
  

   
 

  
   
 

 

]

 

{

    

 

 

 

  
  

 

        

 

 

 

 

 

    

 

      

 

 

    

 

 

        

 

 

 

 

 

    

| | | |
Latte A’ | Late RB | Latte D’ | Latte E | Latte BR” | Latte G’ | Latte H’ |
ke Il || à |] | a
ZEIT DES Abweichung des Lattenmeters vom Normalmeter in Mikrons, nach den
VERGLEICHES ||abso-| rela- || abso- rela- | abso- rela. | abso- | rela- | abso- | rela- ||abso- | rela- || abso- rela-
: luten | tiven | luten | tiven | luten | tiven | luten | tiven |luten | tiven I luten | tiven | luten | tiven |
Lattenvergleichungen.
| | | | || |
Mitte Mai 1895 ||4+528 II+ 490 | + 642 + 594 | + 443 + 458 4418
» Juni » |. [#523] | | + 6231 | + 500 + 442
» Juli » | 7544| | | + 6781 | + 553|| +474 |
|| {| eal Bean
» August » | + 563| i + 691)! | + 571 | + 505
» September » | +549) | + 670} | + 556) + 485 |
» October » ||+539 | +523 +518) + 639 || + 624 + 640||-+ 457 (+ 502|+ 541||+ 464 |-+ 474
| | || || || |
joe | | | (| |

 

 

 

 

 

  

 

Die Ergebnisse der ausgeführten provisorischen Ausgleichung der nordöstlichen
Schleifengruppe des Präcisions-Nivellement der österreichisch-ungarischen Monarchie sind
im XIV. Bande der Mittheilungen des militär-geographischen Institutes publicirt.

B. ASTRONOMISCHE ARBEITEN

Dieselben waren auf die Reduction jener Beobachtungen beschränkt, welche während
der Sommerarbeiten früherer Jahre gesammelt worden sind und bisher der definitiven Bear-
beitung nicht zugeführt werden konnten.

 

AA ad à

di whl a Lu à a

ii Lu

 
