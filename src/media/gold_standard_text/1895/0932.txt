 

 

 

   

 

a

 

he

XV. — Russie.

 

INDICATION | | DIRECTEURS

To | | | ¥ INSTRUMENTS REMARQUES
DES POINTS | ee a au ET OBSERVATEURS Q

 

 

Triangulation de Finlande (Suite.)
208 | Murto-Mäki .......,

209 | Lehto-vaara ....... | > > 2 5 s |
210 Otan-Mäki VS Eee ae 64 7 5 44 46 6 ||
211 | Rupukka-vaara .... | 64 14 33 | 45 37 39 |

212 | Saukko-vaara...... | 64:26 52 | 45 52 28 à |
213 Kives-vaara. eo... | 64 27 36 45 12 27 a an |
214 Rokua-vaara sa relire es | 64 33 52 44 9 39 3 5 ae |
215 Puokio-vaara See Dust 64 44 51 45 0 39 © ac a

216:| Beiri-harım........ | 64 40 40 | 45 37 56 | ie ance „a

217 | Halosen-vaara ..... | 64 43 31 | 43 33 33. aS =

218 Repo-Kangas eo 64 48 31 | 43 49 35 | = À 2a

219 | Revonpesämaa ..... | 54 49 38 | 44 90 12 | = Bb © 2

220 Palo-vaara ........ | 6 49 57 | 44 54 20 | = So

¢ * * | AE > |
22] | Linnunsilma....... a dB or 30 a 4e BE

Peo | Pitkäselka ........ 6455 36 | 43 35.49 | 36 ne ms

223 Laton-Maki........ 64 49 36 42 49 30 | ‘a aS a n

224 | Sarvl-Kangas...... 65 02 148 17 13 | aS .E |
225 | Hyypän-Mäki......| 65 3 48 | 42 27 58 Zee = 8

226 Isoniemi. .......... | 65 943 | 4254 9 oo. 5 5 ||
207 Ronti De ee | 65 20 41 42 54 A] ie = 5 |
228 Ulkogrunni Brea ea cialis 65 23 19 42 30 16 = $a
229 A jos Se te ao eee | 65 40 1 42 83 8 En

230 Kivalo ee | 65 49 21 ! 42 40 21 |
231 Tornea en 65 49 45141 49 31 | 2 |
232 | Kaakama-vaara .... G66. 8 25 A 5 |

 

 

 

 

 

 

 

 

_ Triangulation de Pologne. Jonction à Thorn.

 

 

 

{ | Publiés dans le
Lopath | Déjà mentionnés. “Volume XXIIE de

233 | Nowoselki......... | bee 16! 19" 1 42° 9! 55 | section: topographi-

234 | Skopowo.......... | 53 29 13 | 41 50 54 | | que de l’Etat-Major.

239. Preetlakl.......... 0317 57. 41 214 15

256 , Mohllary. ......... 53 24 54 | Al 33 28

231 | Krynkl...... ..... 55 15 1141 2718

238 | Kusehtschinetz..... 58.54 55 41 17 39

239. SOKOlKR ........... 53.20.20 1A4l 153 6

240 | Schadowa ......... 538938 40.88 27 =

241 | Suhowol........... DS 34-38 | 40 47 23 a

21 | Dingilug .§.....:.. 53.95 21 | 40 50 37 a a

PAS Rasta 53.27 020 3647 EX Re

244 | Wissowaty......... | 53 17 52 | 40 22 43 5 =

245 | Krapiwnitza....... b3 55 2021 ia > un

246 | Kripnitza ......... 53 13 55 | 40 4 0 ib = 2

247 | Dobrzinewka ...... 53.3 381 41 1.2 3 a

247a| Mohnata-Gura..... DD on le | 40) 40 aa = = =

248 | Rieniki .. ........ 52,50 3 |41 215 © =

24).| Skrzipki........... 52 43 37 | 40 Al 56 + m

200 | Suhowoltze........ 5223644 | al 0 14 © Q

251 | Gornowo.......... 52 32 53.40 40 3 a

232  Koriiziny.......... 52.88 2 |:40.21 80

255 | Semlatlzi.......... 52 24200 | 10.28 51

aA, Kalısll............ 521917 40 8.8

220 lAGEANO. 6) ce . 52 32 60 | 40 13 14

286 | Korabie........... 52 20 48 | 39 51 33

Bol: Kuteski......... De 82.28 90,21 20

298 , Drguitsch.......... 28121089230

moo Ischaple......%.... 52 23 11 | 39 32 46

 

 

 

 

 

 

hl Bile te st ve

sert Lahde a

000 do lid od Ml Ahh dv Aie du chtis
