rares

ris mhnszaun.

nn TUST Te |

mu at

iene

AN TRE CONTENT

D
=

Zu beachten ist, dass in diesen Tabellen in betreft der Polhöhe und des
Azimutes die Vorzeichen im Sinne Momentanwerth minus Mittlerer Werth angenommen
sind und dass daher, wenn man die auf den geodätischen Stationen beobachteten
Polhöhen und Azimute auf die mittleren Beträge reduciren will, die aus der Tabelle

entnommenen Reductionen mit dem umgekehrten Vorzeichen an die aus den Beob-

achtungen hervorgegangenen Resultate anzubringen sind.
Gemäss den obigen Darlegungen wird man die bezüglichen Reductionen aus

diesen Tabellen mit einem mittleren Fehler von durchschnittlich #0'045 entnehmen
können.

Th. Albrecht.

 
