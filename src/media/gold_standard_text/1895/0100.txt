 

in

 

Kl}

RP mo te

94
Die Ersatzwahl für Herrn Davidson und General Ibanez geschieht gleichfalls durch
Stimmzettel. Es sind 17 Stimmzettel abgegeben worden, von welchen einer nur einen Namen
enthält. Es haben erhalten die Herren

Titian, 2. 4... ui. ....10 Stimmen.
dal ;... .,.10 »
Cobe de Guzman, . .- --2 »
Pec nen. al »
Ceelmupden ss. sess 21 »

Sonach sind die Herren Tittmann und d’Arrillaga gewählt und erklären sich zur
Annahme der Wahl bereit.

Während der Ermittlung der Wahlresultate macht der Herr Präsident! davon Mit-
theilung, dass auf Wunsch von mehreren Delegirten nach dem Schluss der Sitzung die
Räume des Reichstagsgebäudes den Theilnehmern an der Gonferenz zur Besichtigung geöff-
net sein werden.

Sobald der Herr Secretär auf Grund der Materialien, die sich in seinen Händen be-
finden, die exakte Redaction des neuen Vertragsentwurfs in beiden Sprachen fertiggestellt
haben wird, werde derselbe durch den Druck vervielfältigt und sobald als möglich in je
> Exemplaren jedem der Herren Delegirten nach seinem Wohnort zugesandt werden.

Es folgt die Wahl des künftigen Präsidiums der internationalen Erdmessung für die
Dauer der neuen Convenlion. Dieselbe findet in der Weise statt, dass zunächst die Wahl des
Präsidenten und des Vicepräsidenten gemeinsam auf demselben Stimmzettel und demnächst
in einer weiteren Wahl die Ernennung des ständigen Secretärs vorgenommen wird.

Im ersten Wahlgang werden 17 Stimmzettel abgegeben, und es erhalten als Präsident:
Herr Faye 16 Stimmen, Herr Ferrero 4 Stimme ; als Vicepräsident : Herr Ferrero 15 Sum-
men, Herr von Kalmär 1 Stimme, Herr Bakhuyzen I Stimme.

Die somit erfolgte Wahl des Herrn Faye zum Präsidenten und des Herrn Ferrero
zum Vicepräsidenten wird von der Gonferenz mit lautem Beifall aufgenommen.

Herr Faye dankt für das Vertrauen, welches die Gonferenz durch die Wahl zum
Präsidenten ihm erneut bezeuge ; er hoffe, dass er sein Amt noch einige Zeit ausfüllen könne,
und verspreche auch ferner mit ganzem Herzen und mit voller Hingebung das ihm uber-
Iragene Amt zu verwalten.

Bei der nunmehr stattfindenden Wahl des ständigen Secretärs der internationalen
Erdmessung für die Dauer der neuen Convention werden 17 Stimmzettel abgegeben. Es
haben erhalten :

 

1
4
3
3
3
3
4
3
1

tee sda.

ln

=

 
