a _.....

eae mare
[pee

183

stimmte sehr befriedigend. Die Lothabweichung in Länge gegen Pachino hat
Betrag von 17 6. 10

In Portugal hat man nach den « Verhandlungen zu Freiburg, 1890,» S. 144, mit
Lothabweichungsstudien begonnen. In dem genannten Werke sind relative Werthe in Breite
zwischen Lissabon, Coimbra und Badajoz, sowie im Azimut zwischen Lissabon und Badajoz
angegeben. Kine Lothstérung A—G = ca. + 5” in Lissabon, die auch der Terrainform
entsprechen wiirde, ist darnach wahrscheinlich.

Ueber die astronomisch-geodätischen Ergebnisse auf dem neugemessenen französi-
schen Meridianbogen berichten die « Verhandlungen in Brüssel, 1899 », S. 580/581, jedoch
sind Lothabweichungen in Breite nicht angegeben. Es ist vielmehr nur über die Krümmungs-
verhältnisse gesprochen, sowie der Widerspruch für 5 Laplace’sche Gleichungen zwischen

Stationen des Meridianbogens mitgetheilt. Derselbe ist durchaus befriedigend und im
Maximum nur gleich 1”.

den erheblichen

Pa will ea

Eee TT TTT TT
’

 

In Belgien wurden die älteren astronomischen Stationen Lommel und Nieuport in
Breite und Azimut neubestimmt; ausserdem wurde die neue Station Hamipré bei Neuf-
chateau in der Siidostecke Belgiens angelegt. Die relativen Lothabweichungen sind in den
« Verhandlungen in Paris, 1889», und den «Verhandlungen zu Freiburg, 1890 » mitge-
theilt. !! Die Neumessungen in Lommel und Nieuport bestätigen die früher für die Lothab-
weichung in Breite gefundenen Werthe im Betrage einiger Sekunden, reduciren aber die
starken Lothabweichungen im Azimut erheblich, sodass diese nur noch ca. 5” betragen. Auch
in Hamipr6 übersteigen die relativen Lothabweichungen gegen Brüssel nicht einige Sekunden.
Für letzteren Ort scheint eine Azimutstörung von einigen Sekunden angedeutet.

In Dänemark ist die Breite für 4 Punkte des Hauptdreiecksnetzes, die in der Nähe
des Brockenmeridians liegen, bestimmt und fiir Skagen neubestimmt worden. (Vergl. « Ver-
: handlungen in Briissel, 1892 », S. 571.) Da die Triangulation fertig berechnet und publicirt

ist, lassen sich die Lothabweichungen in Breite sofort ohne viele Rechnung ableiten.

Für Norwegen liegt eine umfangreiche Arbeit von Geelmuyden vor, '!? die für 11 Punkte
E des Gradmessungsnetzes die relativen Lothabweichungen in Azimut, für 9 derselben auch in
Breite, gegen die norwegisch-schwedische Station Dragonkollen im Süden des Landes giebt.
Ausserdem sind mit Benutzung minderwerthiger Dreiecksnetze noch die relativen Lothab-
weichungen in Azimut für 12 Punkte, für 3 derselben auch in Breite, abgeleitet. Für
Christiania und den Nachbarpunkt Husbergöen konnte auch eine genügend stimmende La-
place’sche Gleichung aufgestellt werden; die erforderliche Längenbestimmung wurde durch
optische Signale bewirkt. Die totalen Werthe der Zenitabweichungen gehen bei den Haupt-
netzpunkten bis 8”, bei den Nebenpunkten bis 13”. Sie korrespondiren nach der Angabe

mu 1

ar

10 V. Reina. L’attrazione locale nella specola geodetica di S. Pietro in Vincoli in Roma. Roma, 1895.
(Rendiconti della R. Accad. dei Lincei.)

11 Ueber die Station Hami
Bruxelles, 1887.

12 Astronomische Beobachtungen und Vergleichung der astronomischen und geodätischen Resultate.
Christiania, 1895. (Publ. der Norwegischen Kommission der Europäischen Gradmessung.)

Te

pre, vergl. auch das Werk: Triangulation du Royaume de Belgique, VI.

Al

5
E
=
se
z=
==
==
z
È
=
==
>
se
23]
se
an
=

 

 
