 

66

Die spanischen Delegirten sind nicht im Stande, einer Vermehrung des Budgels zu-
zustimmen; sie wollen aber auch nicht in ablehnendem Sinne votiren und enthalten sich
demzufolge der Abstimmung. |

Sie wünschen nicht in negativem Sinne zu stimmen, um ihrer Regierung — obgleich
sie stets vollständige Freiheit des Handelns hat — es zu erleichtern, eine elwaige Vermehrung
zu bewilligen, in Anbetracht der Abstimmung der anderen auf der Conferenz vertretenen
Staaten und im Hinblick zugleich auf die preussische Regierung, welche das kel. preussische
geodatische Institut, dessen Kinrichtungen der internationalen Erdmessung in weitem Um-
fange zu Gute kommen, mit so reichen Mitteln ausstattet.

Herr Hennequin halt es für wünschenswerth, dass die Conferenz sich über die Art
der Abslimmung klar wird, da gegenwärtig zwei verschiedene Entwürfe und ausserdem noch
ein Amendement vorliegen. Er stelle deshalb folgenden Antrag :

« Namens einer gewissen Anzahl von Delegirten habe ich die Ehre, zur Geschäfts-
ordnung vorzuschlagen, dass zuerst über die Art. 6—9 des Entwurfs der permanenten Com-
mission abgestimmt wird, weil je nach dem Ausfall dieser Abstimmung sie in der Lage
sein würden, den Art. 2 des Gegenentwurfs, welcher sich auf die permanente Commission
bezieht, anzunehmen, und weil auf diese Weise, indem damit der von den französischen
Delegirten in der 4. Sitzung abgegebenen Erklärung Genüge gcthan wird, eine möglichst voll-
ständige Uebereinstimmung erzieli werden könnte. »

Auf erfolgte Anfrage antwortet Herr Hennequin, dass sein eben verlesener Antrag
ausser von ihm noch im Namen der Herren Celoria, Hirsch, von Orff, Nell nnd Tittmann
gestellt sei, und dass ausserdem Herr von Zachariae erklärt habe, denselben in der Sitzung
zu unterstützen.

Herr Tisserand ist der Ansicht, dass nach dem Gebrauche der Association zunächst
über denjenigen Punkt abgestimmt werden müsste, dessen Discussion in der 4. Sitzung
stattgefunden hat. Im Uebrigen schlägt er vor, dass die Discussion sich über den Entwur
und den Gegenentwurf gemeinsam erstreckt. Ausserdem müsste nach allgemeinem Brauchf
zuerst über die Amendements abgestimmt werden.

Herr Horsch erklart, es hinge durchaus von der Versammlung ab, in welcher Reihen-
folge sie die verschiedenen Anträge und Amendements zur Abstimmung hringen wolle. Zu
dem Ende mache der Präsident Vorschläge, und wenn die Versammlung damit nicht einver-
standen sei, so habe sie selbst die Entscheidung in der Hand. Im Uehrigen empfehle er, die
kostbare Zeit nicht mit Geschäftsordnungsfragen allzuschr in Anspruch zu nehmen.

Herr Weiss ersucht das Präsidium um Beantwortung folgender Anfrage :

Aus der Denkschrift von Herrn Geheimrath Professor Foerster geht hervor, dass die
ganze Erhöhung der Dotation der Erdmessung lediglich zur Einrichtung eines entsprechenden
cobachtungssystemes zur fortgesetzten Bestimmung der Lage der Erdaxe verwendet werden

 

4

i

bad Ailsa Ud db dee dw

ese hile

rn d

ln

3

 

 
