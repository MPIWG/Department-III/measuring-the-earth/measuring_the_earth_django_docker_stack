 

166

qu'aurait aux yeux des administrations supérieures des États contractants la nouvelle
Commission permanente de 21 membres, sans qu’elle soit qualifiée de consultative.

M. Bakhuyzen retire sa proposition.

La discussion étant ainsi épuisée, on procède à la votation sur l’article 2 du contre-
projet.

Ont voté pour : le grand-duché de Bade, la Bavière, le Danemark, la France, la
Hesse, l’Ttalie, les Pays-Bas, la Norvège, l’Autriche-Hongrie, la Prusse, l'Espagne, la Suède,
la Suisse, les Etats-Unis, le Wurtemberg et le Japon. A voté contre : la Belgique.

Par conséquent, l’article 2 du contre-projet est adopté provisoirement par 16 voix
contre 1, dans la rédaction suivante :

«Art. 2. L’organe superieur de l’Association géodésique est la Conférence générale
des délégués des Gouvernements intéressés. Cette Conférence se réunit au moins une fois tous
les trois ans.

€ Dans l’intervalle des sessions l’exécution des décisions de la Conférence générale et
la gestion des affaires administratives sont confiées au bureau de l'Association, composé du
Président et du Vice-président de l'Association, du Secrétaire perpétuel et du Directeur du
Bureau central.

€ Pour les affaires administratives non prévues, le bureau de PAssociation prendra
par correspondance l'avis d’une Commission permanente consultative composée des délégués
désignés à cet effet officiellement par chaque État, à raison d’un délégué par État.

«Il appartient au bureau de l’Association de fixer la date et le lieu des Conférences
générales, ainsi que d’y convoquer les délégués des États contractants, en indiquant l’ordre
du jour de la session. »

M. le Président annonce que M. Karl von Steinen, président de la Société de géogra-
phie, a eu l’amabilité d'inviter les membres de la Conférence à une séance de celte Société,
qui aura lieu le samedi 12 octobre, à 7 heures et demie du soir, dans la maison des archi-
tectes, Wilhelmstrasse, 93.

La prochaine séance est fixée à vendredi 11 octobre, à 2 heures: son ordre du jour
porte la suite de la délibération sur le projet de nouvelle Convention et la votation défini-

tive sur cette Convention.

La séance est levée a 4 heures 25 minutes.

 

EP

iii ee

te 14-000 À

ini me bé na Ha A tnt saute nn

|
|

 
