 

Las iain pre u rh means nt 2€ À
PE ATP wo

de

dB à à
BRUT VE

kl ai il
AOF OTT

A die End abat tré
|

Ss
| OW

+ 52° 30/ I Il
ae... à à u, ef
1895 Juni 6. 16,20 16,00 16,43 16,50 16,09

7. 16,23 16,32 16,02 16,24 16,28 :

12. 16,04 16,03 16,12 16,03 16,09 ; na

13. 16,19 15,95 16,02 15,92 16,37 16.54 16,29 , :

22, 16,31 16,27 46,17 16,18 16,48 16,10 16,68 16,41

97. 16,03 16,04 16,29 16,15 16.52 16,36 16,43
Juli 2. 16,11 16,24 16,65 16,48 16,55

h. 16,37 16,36 16,44 16,17 16,36

« Aus der inneren Uebereinstimmung dieser 48 Werthe ergiebt sich für eine einzelne
Polhöhe der wahrscheinliche Fehler + 0'109, der mittlere Fehler + 0'168.

« Eine Abhängigkeit der obigen Polhöhen von der Reihenfolge der Kreislagen am
Instrument ist nicht vorhanden, da gw— 9. — — 0.044 ist.

« Bezieht man die Abendmittel der Polhöhen, mit Hülfe der Reduction 9 — 911 =
— (0.246, auf Gruppe I, so ergeben sich folgende Mitelwerthe :

 

1895 EO 16,2 5 2G Sk ”
ey : eat : 1% 2 | 15,281 m)
A an, clk
LE MA ann is nee | 16,070 0
a hang: ang na ol int
ue À non ol | 1. : 16,176 «o)

« Das Gesammtmittel ist + 52° 30 16,155 (48) und mit Berücksichtigung der Ge-
wichte, aber unter Vernachlässigung einer etwaigen Polhöhenvariation, ergeben sich folgende
Fehlerbeträge :

ee pir Hades 059,
Fur ein Abendmittel aus 6 Paaren | m. F. + 0,079.
2 w. F. + 0,019.
4" 4 a

Fur das Resultat aus 48 Paaren m. F. + 0,098.

« Ebenso wenig wie die einzelnen Polhöhenwerthe zeigen die Abendmittel irgend welche
Abhängigkeit von der Reihenfolge der Kreislagen, da hier Ow — DE — — 0'018 ist.

« Die Sternpaare, welche obigen Polhöhen zu Grunde liegen, weisen folgende optische
Helligkeiten und Differenzen von Meridianzenithdistanzen auf :

 
