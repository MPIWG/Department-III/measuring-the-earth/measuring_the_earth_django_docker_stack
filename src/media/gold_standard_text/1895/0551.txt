a

Hire
Les

1 ON RRR AD IM ARENA HHT
°C PPT PF TPR RPL PTT TTT

Ha
IPT TTT

DCR COMENT
IPB

5
13
=
aS
a
1%
ze
se
==
aS
ae
==

Annexe B. VII.

ITALIE

RAPPORT
sur les travaux exécutés par la Commission géodésique italienne en 1895.

(Avec 3 cartes. — Voir nos IX-XI.)

TRAVAUX DE L'INSTITUT GÉOGRAPHIQUE MILITAIRE.

Triangulation (voir planche 1). — Après la dernière réunion de la Commission per-
manente (Innsbruck, 1894) on a exécuté les observations angulaires aux stations suivantes
pour compléter le réseau de premier ordre de la Sicile :

Monte San Giuliano, Marsala, Capo, Granitola, Salemio, Monte delle Rose, Bonifato,
Roccaficuzza, Busamhra, Monte Cuccio, Montalfano, San Calogero, Cammarata, Giafaglione,

Castelluccio, Durra, Pollina, — et on a répété les observations, pour ce qui concerne le
réseau occidental de la Sicile, à Grecuzzo, Santa Croce, San Salvatore, Chiebbo et Monte Cane.
Nivellement de précision (voir planche ID). — En 1895 on a nivelé à double :

A: La:liene Foggia-Barı 'deirp ur Alpen 22 1:1384lkmı
2309153 259. >Napolr-Anlettades a mann 19259
et la ligne Parma-Guaställa. de wi ee, men 2; 899.3

la dernière pour la substituer à la ligne de Reggio-Guastalla, pour laquelle on a des raisons
de croire que le terrain ne présente pas les meilleures conditions de stabilité.

Maregraphes (voir planche Il). — Les marégraphes en activité sont indiqués sur la
planche IT; on a poursuivi les dépouillements des diagrammes du marégraphe de Gênes jus-
qu'au mois d'avril 1894.

Travaux de calcul (voir planche I). — On a achevé les calculs de compensation des
réseaux XV et XVI, de sorte qu’à l'exception de celui de la Sicile occidentale (XVII), tous les
réseaux trigonométriques de lftalie sont compensés.

 
