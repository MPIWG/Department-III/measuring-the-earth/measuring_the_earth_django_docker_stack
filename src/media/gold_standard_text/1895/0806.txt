EEE DELETE WEL

 

on

VII. — Italie.

0 ec.

 

 

: INDICATION - ;
N° DES POINTS LATITUDE |LONGITUDE EPOQUE DIRECTEURS OBSERVATEURS INSTRUMENTS
| vn
a un . (Ing. D’Atri, | Reichenbach di 32 cm.
196 | Motta o Tavernola. | 41°34' 23") 33°09' 14" | 1864-69 | Col. De Vecchi. » Arabia. Starke di 27 em.
an mags! ia. \ Reichenbach di 32 cm.
> 3 : ng ri : ;
197 | Montedoro........ 41 34 40 | 32 57 55 || 1864-67 id. oe ao ee | Pistor di 28 em.
200 | M. S. Angelo ..... 114228 333701) 1868 id. ee ee id.
20] | Ruccolo eo: “| 41 4321 352 33 20 | 136% id. Ing. D’Atri. 1 Repsold di 32 em.
202 | Montenero.. .... ai 41-43 34 | 32058 1868 id. Cap. D Vita. | Pistor di 28 em.
206 | Serracapriola..... | 41 48 12 | 32 49 23 | 1868-69 id. lee ae Ma, en
207 | Giovannicchio .... ı 41 49 59 | 33 28 20 || 1868-69 id. Cap. De Vita. | Pistor di 28 em.
210 . Termoli........... 220032, 2.29% 1868 id. id. id.
215 | Isola Tremiti..... 42 07 16 | 33 10 16 1869 id. id. Starke di 27 cm.
223 | Isola Pelagosa.... | 42 23 28 | 33 55 42 1869 10e id. id.
229 | Isola Lagosta..... 42 45 02 | 34 31 31 1869 id. id. id.
235 | Isola Lissa ....... 43 0145 | 38 46 42
III.
DÉVELOPPEMENT DE LA BASE DU CRATI EN CALABRE.
66 | Serra Castellara.. | 39°25' 11”| 34°00' 38") 1871 Col. Chio. Ing. D’Atri. Repsold di 27 em.
67 | Cozzo Sordillo.... | 39 25 13 | 34 15 33 1871 id. a id.
7) 2. Drionto | 39 37. 14 | 34 25 81 1871 id. id.
À ns 5 à Cap. ee Pistor di 27 cm.
72 | Schiayonia ....... 39 39 03 | 341217) 1871 id. D Se a
73 | 8. Salvatore ...... 33 3923,325806, Jen id. Cap. Maggia. | id.
7A  Montea.... ..... 39 39 33 | 33.50.00 1871 id. Lieut. Mottura. | Starke di 27 cm.
a : < : Cap. Maggia. : ;
76 | Pollinara......... 39 41 06 | 34 02 55 | 1871 id. he at Don, | Pistor di 27 cm.
77 | Base del Crati.... | 39 42 49 | 34 05 58 1871 id. | id. | id.
Est. S.-E.
78 | Base del Crati.... | 39 43 43 | 34 04 18 | 1871 id. id. ! id.
Est.~° N.-O.
79 | Buffaloria........ 39 44 52 | 34706 42) 1871 id. } id. id.
8 | Vassano........... 39 46 A8 | 33 57 82 1871 dd. Cap. Maggia. | id.
83 | Mostarico ........ 39 53 16 | 34 09 36 1871 id. id. id.
84 | Jagola o Giagola.. | 39 54 10 | 33 32 07 1871 id. Lieut. Mottura. | Starke di 27 cm.
85 | Pollino: 3% 54123 | 28 ol Il 1871 id. id. id.
93 | Bulgaria ......... 40 04 07 | 33 05 44 1872 id. Cap. Colucci. id.
OA) NOCArA 22.5.2... 40 05 59 | 34 08 31 1871 id. | Lieut. Mottura. id.
ion Maggia.
: < ” 3 > Colncer :
95: Le Alpi...:...... 40 07 12 33 3855|, 1810 71 id. | Lient Mottura, id.
Ing. Garbolino.
IV.
RESEAU DE CALABRE ET JONCTION AVEC LA SICILE.
fan0 ne : ,
da Pod. ulm
45 | 8. Angelo di Patti. Sa
48 | Montalto......... 38°09) 2641, 38°30. 04: 1869 Col. De Vecchi. | Ing. D’Atri. | Pistor di 27 cm.
49 | Castania.......... Déjà mentionné. |
50 | Milazzo.......... 38 16 09 | 32 53 43 | 1865-72 id. De id.
“OD
5l | Gremi....... 4.188 20 AU 30 08,45 1869 id. Ing. D’Atri. | Repsold dr 21. cm,
52 | S. Angelo diLipari. | 38 29 18 | 32 35 50 1872 Col. Chio. Cap. Maggia. | Pistor di 27 cm.

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

REMARQUES

Triang.” Autr.enne

 

aba ah as
