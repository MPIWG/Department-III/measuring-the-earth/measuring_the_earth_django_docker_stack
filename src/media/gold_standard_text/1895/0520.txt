 

Beilage B. IV.

DANEMARK

Bericht über die geodätischen Arbeiten im Jahre 1895.

1. TRIGONOMETRISCHE ARBEITEN.

Die in früheren Berichten erwähnte Revision der trigonometrischen Stationen ist im
laufenden Jahre zu Ende gebracht worden.

Auf der Insel Bornholm wurde eine kleine Basismessung mit dazu gehörender
Triangulation ausgeführt, um die Anknüpfung der Insel an die schwedische Ostseeküste zu
kontrolliren. Die benutzten schwedischen Punkte waren nämlich auf dem Felde so unbe-
stimmt markirt, dass man ihren Platz nicht mit gehöriger Genauigkeit hat angeben können.
Die Ergebnisse der erwähnten Kontrolle können erst später, nach beendeter Berechnung vor-
gelegt werden.

9. DAS PRACISIONS-NIVELLEMENT.

Man hat das bevorstehende Nivellement über den grossen Belt mit der kleinen Insel
Sprogö als Zwischenstation vorbereitet. Die Punkte Knudshoved auf Fühnen und Höjklint auf
Seeland sind festgelegt worden, und die Zwischenstation auf Sprogö wurde durch Triangu-
lation so bestimmt, dass sie oi Meter genau denselben Abstand von den beiden Punkten
Knudshoved und Höjklint bekam.

Die Linie Kopenhagen-Kronberg (bei Helsingör) ist mit den bei unserem Nivellement
üblichen früher beschriebenen Fixpunkten versehen worden; ungefähr die Hälfte der Punkte
wurde wie gewöhnlich unterirdisch angebracht.

3. MAREOGRAPHEN.

Die Ergebnisse der fünf mit dem Präeisionsnivellement verbundenen Mareographen
sind von den im vorigen Berichte angegebenen nicht verschieden. Die drei an der Ostküste

 

ali

 
