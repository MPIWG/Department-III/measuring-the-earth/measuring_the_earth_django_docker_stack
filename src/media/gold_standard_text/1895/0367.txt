 

 

 

Elimination des
equations
personnelles.

 

TITRE DE LA PUBLICATION

   

     

 

‚71892 septem-
bre.

1888 Octob.. -

su Novemb.
MN
WI

(6 pi

1888-89 Dec...
| January.

E
1889 Janu-
AE ary.

 

| Echange des obser-

 

Echange des obser-
vateurs.

Determination di-
recte.

vateurs.

Interchange of ob-
servers. |

 

Coast and Geodetic Survey

 

 

Mémoires (Zapiski) de la section |
topographique de l'état-major, |
IM.

>
mie
(>

port for 1889, Appendix N° 8

Not yet published.

 

General remarks. — To avoid the marking
of a sign to the difference of longitude, the
western station is invariably wr itten first and
above the eastern one.

When no special local reference is made
the result refers to the position of the transit |
instrument. The new transit instruments of
1888, made in the instrument shop of the
Survey, are described and figured in Coast |
and Geodetic Survey Report for 1889, Ap- |
pendix No 9.

The first telegraphic difference of longi-
tude given above i is number 184 of the whole |
series beginning in October 1846 with Waz- |
shington, D. C., and ‚Philadelphia. The mea- |
sures are arranged in chronological order.

 

 
