Me NT ann

THAT

D

en

LOU À LL

 

ZUSAMMENSTELLUNG

der Winkelschlussfehler sämmtiicher von der Trigonometrischen Abtheilung der
Königlich Preussischen Landes aufnahme gemessenen Hauptdreiecke einschlies-
slich derjenigen Mehreeke, welche nieht in Dreiecke zerlegbar waren in weleken
jedoch sämmtliche Winkel gemessen sind.’

‘ In der kolumne A? ist bei den Mehrecken als Beitrag zu der Fehlerquadratsumme der Werth Sa,

eingetragen, in welckem A, den Winkelschlussfehler des Polygons und in die Anzahl der Winkel desselben
bedeutet. In der kolumne A ist sodann der Werth der Wurzel daraus gesetzt. Bei N. XXV ist bezüglich A?
nach demselben Princip verfahren, unter A jedoch der wirkliche Winkelschlussfehler des Polygons angegeben.

 

 

 
