À

NL France et Algérie.
annees comment Man ee
et pour l’aplatissement
ee À
7 308,64
valeur résultant de la combinaison de l’arc du Pérou avec Vare français entre Dun-

a

kerque et Formentera.

A ces opérations se rattachent les noms de Delambre, Méchain, Biot, Arago, co-
lonels Henry, Bonne, Corabœuf, Brousseaud, Peytier, Hossard, Levret, commandants Du-
rand et Delcros. (')

La triangulation frangaise a été reliée en 1861 et 1862 a la triangulation anglaise
par dessus le Pas de’ Calais, simultanément et séparément par des officiers francais
et des ingénieurs anglais. La mission française comprenait trois officiers de l’ancien
corps d’État-Major: MM. le colonel Levret, les capitaines Beaux et Perrier.

Cette même triangulation a été rattachée aussi avec la triangulation belge, avec
celle de la Suisse, avec celle de l’Espagne par plusieurs contacts le long de la chaîne
des Pyrénées, et enfin, plus récemment, avec la nouvelle triangulation du royaume d'Italie.

Les calculs ont été exécutés par les anciennes méthodes, sans qu’on ait opéré la
compensation des quadrilatères et l’accord des bases. Ce complément indispensable de
toute triangulation scientifique est réservé à un avenir prochain.

Il importait, en effet, avant d'entreprendre la réfection des calculs, de réviser l’œuvre
reconnue imparfaite de Delambre et Méchain, et de procéder à une nouvelle mesure de la
chaîne comprise entre Dunkerque et Perpignan. L'opération confiée au capitaine Perrier
a été entreprise en 1870 et continuée les années suivantes: elle a été terminée en 1888.

Les directions ont été observées par voie de réitération à l’aide des cercles azimu-
taux perfectionnés de Brunner, munis à l’oculaire d’un fil mobile comme les cercles
astronomiques. — Aux signaux en bois ou en maçonnerie ont été substitués des héliotropes
pendant le jour et des collimateurs optiques pendant la nuit; — c’est à l’occasion de
cette mesure que les observations de nuit ont été introduites d’une manière systéma-
tique dans la pratique des observations, à partir de 1875, et ont donné les résultats les
plus satisfaisants. La chaîne comporte 88 stations ; elle s'appuie sur les anciennes bases
de Melun et de Perpignan, mesurées par Delambre. Une nouvelle base fondamentale,
remplaçant celle de Melun qu'il est impossible de remesurer, a été fixée entre Villejuif
et Juvisy ; une base de vérification est établie, au nord de la chaîne, près de Cassel; on
a remesuré la base de Perpignan. . |

Les observations ont été faites par M. le general Perrier (35 stations), M. le L.’ co-
lonel Bassot (32 stations) et M. le comm Defforges (21 stations).

(') Voir les Tomes VI et VII du Memorial du Dépôt de la Guerre rédigés par le colonel Puissant, et le Tome IX
rédigé par Peytier et Levret.

 

bath Au ui ha ddd el eats

 
