 

Leitung und Verantwortlichkeit des
Centralbureaus » fortzulassen
Herr Tisserand erklärt, dass Brankreioh
für die Dotation von 60 000 M. stimmen
wird, falls die übrigen Bestimmungen
des Gegenprojekts Annahme fänden
Herr Hirsch schlägt vor, in dem Amende-
ment Bakhuyzen die Worte « unter der
Leitung des Präsidiums der internatio-
nalen Erdmessung » einzuführen . .
Herr Helmert erklärt sich damit im We-
sentlichen einverstanden. ‘
Herr Weiss gibt im Namen der tn
reichischen Delegirten die Erklärung ab,
dass sie für die Erhöhung der Dotation
in der Form des Amendements Bak-
huyzen stimmen werden . . .
Auf Wunsch des Herrn Präsidenten und
des Secretärs überreicht Herr Helmert
seine abgegebene nn in schrift-
licher Fassung . . :
Herr Lallemand schlägt vor, Gon son
«unter der Leitung und Verantwort-
lichkeit des Centralbureaus » im Amen-
dement Bakhuyzen durch den Zusatz
«und unter der Controlle des Präsidiums
der Erdmessung » zu vervollständigen .
Herr Seeliger unterstützt diesen Antrag .
Die alsdann folgende Abstimmung über
das Amendement Bakhuyzen-Schols mit
dem Zusatzantrage des Herrn Lallemand
ergiebt dessen Annahme durch 13 Stim-
men gegen 3 . a
Discussion und Abstimmung über u a
tikel 2 La ee ee
Derselbe wird mit 16 gegen 4 Stimme in
der Fassung des Gegenentwurfs provi-
sorisch angenommen. .

Siebente Sitzung, 11. October 1895

Auf Anfrage des Herrn Dt Neumayer wird
beschlossen, demselben das lebhafteste
Interesse der Erdmessung an den
Schweremessungen in den antarktischen
Gegenden mitzutheilen . . . 4

Herr Celoria, im Namen mehrerer Dele:
girten schlägt ein Amendement zu Art.7
vor, worin im Anfang der Gesammt-

Pag.

70

7A

7A

73

73
73

13-714

74-75

74-75

76-90

—
D
1
=3
+

296

 

betrag der Dotation angegeben wird
und dem Präsidium der Erdmessung
nicht nur die Controlle, sondern auch
die Oberleitung der Arbeiten zuerkannt
WIRG SG oxy a ee
Herr Hirsch unterstiitzt diesen Antrag
Herr Foerster glaubt. dass die Regierungen
der betheiligten Staaten die Oberleitung
durch das Präsidium verlangen würden
Herr Haid wünscht, dass zuerst über die
Fassung « unter der Controlle und Ober-
leitung des Präsidiums » abgestimmt
werde . : ah
Herr Helmert erklärt so us den Zu-
satz « Oberleitung », welche bereits
durch Art. 2 eesichert sei
Die Herren Bakhuyzen und Beige oe!
chen gegen das Amendement Celoria
Die Herren Foerster und Hirsch schlagen
vor, die Erwähnung des Centralbureaus
aus Art. 7 fortzulassen

Herr Tisserand wiederholt die EU ne

der französischen Delegirten, für die
Dotation von 60000 M., nach dem

Amendement Bakhuyzen mit dem Zusatz
Lallemand, stimmen zu wollen

Herr Hennequin erklärt seine Abstim-
mung in erster Lesung -

Herr Lallemand giebt Aufklärung dr
den Sinn seines Zusatz-Antrages

Der Herr Präsident giebt auf Anfrage des
Herrn Hennequin und unter Zustim-
mung der Conferenz die Erklärung ab.
dass die « Controlle » sich nicht nur auf
die ausgeführten, sondern auch auf die
in Ausführung begriffenen Arbeiten und
auf die zu fassenden Beschlüsse beziehe

Abstimmung über Art. 7 :

Der erste Theil des Amendements quiere
wird mit 16 Stimmen angenommen

Herr d’Arrillaga motivirt die Enthaltung
der spanischen Delegirten

Der zweite Theil wird in der Fassung des
Amendements Bakhuyzen mit dem Zu-
satz Lallemand mit 14 gegen 2 Stimmen
angenommen a

In der Gesammtabstimmung wird der so
modifizierte Art. 7 mit 16 Stimmen

Pag.

J 3
~

I

80

80-81

81-82

ee he

en Aa ati.

dubai

ntm Le aa Had A LA tt aa à

|
|

 

 
