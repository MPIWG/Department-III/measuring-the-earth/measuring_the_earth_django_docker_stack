c = get_config()

# Notebook server config below
# Kernel config
c.IPKernelApp.pylab = 'inline'  # if you want plotting support always

# Notebook config: ip address and port
c.ServerApp.ip = '0.0.0.0'
c.ServerApp.port = 4444

# disables the browser
c.ServerApp.open_browser = False
c.ServerApp.allow_root = True
c.ServerApp.allow_origin = '*'