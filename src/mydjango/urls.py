"""mydjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.urls import include, path
from django.contrib.auth.decorators import login_required
from myapp.views import ShowHome
from myapp.views import ShowVolume
from myapp.views import ShowSearch
from myapp.views import ShowAnnotate
from myapp.views import ShowPersons, ShowOrganisations, ShowPlaces, ShowTerms, ShowNotebooks
from myapp.views import ShowBatches, ShowEntity, pull_entity_network
from myapp.views import ShowBatch, add_page_to_batch, remove_page_from_batch, page_in_batch, set_current_batch, export_batch, export_batch_en, export_batch_as_jsonl, export_batch_entities
from myapp.views import DeleteBatch

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', ShowHome.as_view()),
    url(r'^search/', ShowSearch.as_view()),
    url(r'^delete/(?P<pk>\d+)/$', DeleteBatch.as_view(),name='delete_batch'),
    url(r'^export_batch_en/(?P<pk>\d+)/$', export_batch_en,name='export_batch_en'),
    url(r'^export_batch/(?P<pk>\d+)/$', export_batch,name='export_batch'),
    url(r'^export_as_jsonl/(?P<pk>\d+)/$', export_batch_as_jsonl,name='export_as_jsonl'),
    url(r'^export_batch_entities/(?P<pk>\d+)/$', export_batch_entities,name='export_batch_entities'),
    url(r'^persons/', login_required(ShowPersons.as_view())),
    url(r'^terms/', login_required(ShowTerms.as_view())),
    url(r'^organisations/', login_required(ShowOrganisations.as_view())),
    url(r'^places/', login_required(ShowPlaces.as_view())),
    path('volumes/<int:pk>/', ShowVolume.as_view(), name='show_volume'),
    path('batches/', login_required(ShowBatches.as_view()), name='show_batches'),
    path('notebooks/', login_required(ShowNotebooks.as_view()), name='show_notebooks'),
    path('annotate/<str:language>', login_required(ShowAnnotate.as_view()), name='annnotate'),
    path('batches/<int:pk>/', login_required(ShowBatch.as_view()), name='show_batch'),
    path('entities/<int:pk>/<str:menu_item>', login_required(ShowEntity.as_view()), name='show_entity'),
    path('ajax/entities/<int:pk>/pull_network', pull_entity_network, name='pull_entity_network'),
    path('ajax/batches/<int:batch_id>/add_page/<int:page_id>', add_page_to_batch, name='add_page_to_batch'),
    path('ajax/batches/<int:batch_id>/remove_page/<int:page_id>', remove_page_from_batch, name='remove_page_from_batch'),
    path('ajax/batches/<int:batch_id>/set_current', set_current_batch, name='set_current_batch'),
    path('ajax/batches/<int:batch_id>/page_in_batch/<int:page_id>', page_in_batch, name='page_in_batch'),
    path('accounts/', include('django.contrib.auth.urls')),
]
# urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),

        # For django versions before 2.0:
        # url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns