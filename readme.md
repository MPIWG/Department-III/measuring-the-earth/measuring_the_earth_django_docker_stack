# Measuring the Earth Django / Docker Stack

## About
This project will provide an open repository of primary sources for the history of late modern geodetical research. Moreover, it will study the application of digital methods to the historical analysis of international cooperation, scientific diplomacy, and interdisciplinary debate in the field.

The primary goal of the project at this stage is to make the materials produced by the precursors organzations of the current International Association of Geodesy available for digital inquiry through a digitization process made possible by a cooperation between the libraries of the MPIWG and the GFZ in Potsdam. Some material can already be reached via the catalog of the library of the GFZ here and here. More material will be made publicly available soon.

## Numpy, Scipy and Pandas
In the [**`Dockerfile`**](https://github.com/ruddra/docker-django/blob/master/compose/django/Dockerfile), there are detailed instructions on how to install data science dependencies.

**PS:** Here is a [**__`gist`__**](https://gist.github.com/ruddra/870d7a51238ddfa4b50375086c12a4f5) which is more useful for Numpy, Pandas, Scipy etc. And it is usable with this project's [`docker-compose.yml`](https://github.com/ruddra/docker-django/blob/master/docker-compose.yml) file. Just you need to replace the `Dockerfile` from [*./compose*](https://github.com/ruddra/docker-django/blob/master/compose) directory with the one given in the *gist*.

## Prodigy
to retrain the model in the prodigy container, do
    prodigy train ner mte ./trained/ --output ./trained

    prodigy train ner de de_core_news_md --output /app/models/de/
    prodigy train ner fr fr_core_news_md --output /app/models/fr/
    prodigy train ner it it_core_news_sm --output /app/models/it/
    prodigy train ner en  en_core_web_lg --output /app/models/en/

    prodigy train ner de blank:de --output /app/models/de/
    prodigy train ner fr blank:fr --output /app/models/fr/
    prodigy train ner it blank:it --output /app/models/it/
    prodigy train ner en blank:en --output /app/models/en/

    prodigy train ner de /app/models/de/ --output /app/models/de/
    prodigy train ner fr /app/models/fr/ --output /app/models/fr/
    prodigy train ner it /app/models/it/ --output /app/models/it/
    prodigy train ner en /app/models/en/ --output /app/models/en/

## Reindex Elasticsearch Index

    $ ./manage.py search_index --rebuild

## Basic Usage
1. First run **`make build`** inside root directory.
2. Then run **`make up`** to start up the project for first time.
3. Use/update environment variables from [**`.envs`**](https://github.com/ruddra/docker-django/blob/master/.envs) folder.

## Commands
To use this project, run this commands:

1. `make up` to build the project and starting containers.
2. `make build` to build the project.
3. `make start` to start containers if project has been up already.
4. `make stop` to stop containers.
5. `make shell-web` to shell access web container.
6. `make shell-db` to shell access db container.
7. `make shell-nginx` to shell access nginx container.
8. `make logs-web` to log access web container.
9. `make logs-db` to log access db container.
10. `make logs-nginx` to log access nginx container.
11. `make collectstatic` to put static files in static directory.
12. `make log-web` to log access web container.
13. `make log-db` to log access db container.
14. `make log-nginx` to log access nginx container.
15. `make restart` to restart containers.
